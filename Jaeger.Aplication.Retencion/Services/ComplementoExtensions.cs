﻿using System;

namespace Jaeger.Aplication.Retencion.Services {
    public class ComplementoExtensions {
        #region metodos privados
        public decimal FormatoNumero(decimal numero, int decimales) {
            string str = "".PadRight(decimales, '#');
            string str1 = string.Format(string.Concat("{0:0.", str, "}"), numero);
            return Convert.ToDecimal(str1);
        }

        public decimal ShowDecimalRound(decimal Argument, int Digits = 6) {
            return decimal.Round(Argument, Digits);
        }

        public decimal Ajustar(decimal value, int decimales = 2) {
            var _variante = new decimal(0.00000000001);
            return Math.Round(value + _variante, decimales, MidpointRounding.AwayFromZero);
        }
        #endregion
    }
}
