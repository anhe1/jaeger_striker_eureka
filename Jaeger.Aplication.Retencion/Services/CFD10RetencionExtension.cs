﻿using System;
using System.ComponentModel;
using Jaeger.SAT.CRIP.Complemento.PagosAExtranjeros.V10;
using Jaeger.SAT.CRIP.Complemento.TimbreFiscal.V10;
using Jaeger.Domain.Retencion.Entities;
using Jaeger.Domain.Retencion.Entities.Complemento;

namespace Jaeger.Aplication.Retencion.Services {
    public class CFD10RetencionExtension {
        #region ver 1.0
        public ComprobanteRetencionDetailModel Create(SAT.CRIP.V10.Retenciones cfdi) {
            if (cfdi != null) {
                var _response = new ComprobanteRetencionDetailModel {
                    Version = cfdi.Version,
                    Folio = cfdi.FolioInt,
                    NoCertificado = cfdi.NumCert,
                    ClaveRetencion = cfdi.CveRetenc,
                    Descripcion = cfdi.DescRetenc,
                    FechaEmision = cfdi.FechaExp
                };

                if (cfdi.Periodo != null) {
                    _response.Ejercicio = cfdi.Periodo.Ejerc;
                    _response.MesFinal = cfdi.Periodo.MesFin;
                    _response.MesInicial = cfdi.Periodo.MesIni;
                }

                if (cfdi.Emisor != null) {
                    _response.EmisorCURP = cfdi.Emisor.CURPE;
                    _response.EmisorNombre = cfdi.Emisor.NomDenRazSocE;
                    _response.EmisorRFC = cfdi.Emisor.RFCEmisor;
                }

                if (cfdi.Receptor != null) {
                    _response.ReceptorExtranjero = cfdi.Receptor.Nacionalidad == SAT.CRIP.V10.RetencionesReceptorNacionalidad.Extranjero;
                    if (cfdi.Receptor.Item.GetType() == typeof(SAT.CRIP.V10.RetencionesReceptorExtranjero)) {
                        var _extranjero = (SAT.CRIP.V10.RetencionesReceptorExtranjero)cfdi.Receptor.Item;
                        _response.ReceptorNumRegIdTrib = _extranjero.NumRegIdTrib;
                        _response.ReceptorNombre = _extranjero.NomDenRazSocR;
                    } else if (cfdi.Receptor.Item.GetType() == typeof(SAT.CRIP.V10.RetencionesReceptorNacional)) {
                        var _nacional = (SAT.CRIP.V10.RetencionesReceptorNacional)cfdi.Receptor.Item;
                        _response.ReceptorCURP = _nacional.CURPR;
                        _response.ReceptorNombre = _nacional.RFCRecep;
                        _response.ReceptorRFC = _nacional.RFCRecep;
                    }
                }

                if (cfdi.Totales != null) {
                    _response.MontoTotalExento = cfdi.Totales.montoTotExent;
                    _response.MontoTotalGravado = cfdi.Totales.montoTotGrav;
                    _response.MontoTotalOperacion = cfdi.Totales.montoTotOperacion;
                    _response.MontoTotalRetencion = cfdi.Totales.montoTotRet;
                    if (cfdi.Totales.ImpRetenidos != null) {
                        _response.ImpuestosRetenidos = this.Create(cfdi.Totales.ImpRetenidos);
                    }
                }

                if (cfdi.Complemento != null) {
                    if (cfdi.Complemento.Pagosaextranjeros != null) {
                        _response.Pagosaextranjeros = this.Create(cfdi.Complemento.Pagosaextranjeros);

                    }

                    if (cfdi.Complemento.TimbreFiscalDigital != null) {
                        _response.TimbreFiscal = this.Create(cfdi.Complemento.TimbreFiscalDigital);

                    }
                }
                return _response;
            }
            return null;
        }

        private BindingList<RetencionImpuestosDetailModel> Create(SAT.CRIP.V10.RetencionesTotalesImpRetenidos[] impRetenidos) {
            if (impRetenidos != null) {
                var _response = new BindingList<RetencionImpuestosDetailModel>();
                foreach (var item in impRetenidos) {
                    var _impuesto = new RetencionImpuestosDetailModel();
                    if (item.ImpuestoSpecified) {
                        _impuesto.TipoImpuesto = item.Impuesto;
                    }

                    if (item.BaseRetSpecified) {
                        _impuesto.Base = item.BaseRet;
                    }
                    _impuesto.MontoRetenido = item.montoRet;
                    _impuesto.TipoPago = item.TipoPagoRet;

                    _response.Add(_impuesto);
                }
                return _response;
            }
            return null;
        }

        #endregion

        #region complementos
        public ComplementoTimbreFiscal Create(TimbreFiscalDigital timbreFiscalDigital) {
            var _response = new ComplementoTimbreFiscal {
                FechaTimbrado = timbreFiscalDigital.FechaTimbrado,
                UUID = timbreFiscalDigital.UUID,
                RFCProvCertif = null,
                NoCertificadoSAT = timbreFiscalDigital.noCertificadoSAT,
                Leyenda = null,
                SelloCFD = timbreFiscalDigital.selloCFD,
                SelloSAT = timbreFiscalDigital.selloSAT,
                Version = timbreFiscalDigital.version,
            };
            return _response;
        }

        public ComplementoPagosaextranjeros Create(Pagosaextranjeros pagosaextranjeros) {
            var _response = new ComplementoPagosaextranjeros {
                EsBenefEfectDelCobro = pagosaextranjeros.EsBenefEfectDelCobro.ToString(),
                Version = pagosaextranjeros.Version
            };
            if (pagosaextranjeros.NoBeneficiario != null) {
                _response.NoBeneficiario = new Domain.Retencion.Entities.Complemento.PagosaextranjerosNoBeneficiario {
                    DescripcionConcepto = pagosaextranjeros.NoBeneficiario.DescripcionConcepto,
                    ConceptoPago = pagosaextranjeros.NoBeneficiario.ConceptoPago.ToString(),
                    PaisDeResidParaEfecFisc = pagosaextranjeros.NoBeneficiario.PaisDeResidParaEfecFisc.ToString()
                };
            }
            if (pagosaextranjeros.Beneficiario != null) {
                _response.Beneficiario = new Domain.Retencion.Entities.Complemento.PagosaextranjerosBeneficiario() {
                    ConceptoPago = pagosaextranjeros.Beneficiario.ConceptoPago.ToString(),
                    CURP = pagosaextranjeros.Beneficiario.CURP,
                    DescripcionConcepto = pagosaextranjeros.Beneficiario.DescripcionConcepto,
                    NomDenRazSocB = pagosaextranjeros.Beneficiario.NomDenRazSocB,
                    RFC = pagosaextranjeros.Beneficiario.RFC
                };
            }
            return _response;
        }

        #endregion

        #region ver 20
        public ComprobanteRetencionDetailModel Create(SAT.CRIP.V20.Retenciones cfdi) {
            if (cfdi != null) {
                var _response = new ComprobanteRetencionDetailModel {
                    Version = cfdi.Version,
                    Activo = true,
                    ClaveRetencion = cfdi.CveRetenc,
                    Descripcion = cfdi.DescRetenc,
                    LugarExpRetenc = cfdi.LugarExpRetenc,
                    Folio = cfdi.FolioInt, NoCertificado = cfdi.NoCertificado
                };

                if (cfdi.Periodo != null) {
                    _response.Ejercicio = cfdi.Periodo.Ejercicio;
                    _response.MesFinal = cfdi.Periodo.MesFin;
                    _response.MesInicial = cfdi.Periodo.MesIni;
                }

                if (cfdi.Emisor != null) {
                    _response.EmisorCURP = string.Empty;
                    _response.EmisorNombre = cfdi.Emisor.NomDenRazSocE;
                    _response.EmisorRFC = cfdi.Emisor.RfcE;
                    _response.RegimenFiscalE = cfdi.Emisor.RegimenFiscalE;
                }

                if (cfdi.Receptor != null) {
                    _response.ReceptorExtranjero = cfdi.Receptor.NacionalidadR == SAT.CRIP.V20.RetencionesReceptorNacionalidadR.Extranjero;
                    if (cfdi.Receptor.Item.GetType() == typeof(SAT.CRIP.V20.RetencionesReceptorExtranjero)) {
                        var _extranjero = (SAT.CRIP.V20.RetencionesReceptorExtranjero)cfdi.Receptor.Item;
                        _response.ReceptorNumRegIdTrib = _extranjero.NumRegIdTribR;
                        _response.ReceptorNombre = _extranjero.NomDenRazSocR; 
                    } else if (cfdi.Receptor.Item.GetType() == typeof(SAT.CRIP.V20.RetencionesReceptorNacional)) {
                        var _nacional = (SAT.CRIP.V20.RetencionesReceptorNacional)cfdi.Receptor.Item;
                        _response.ReceptorCURP = _nacional.CurpR;
                        _response.ReceptorNombre = _nacional.RfcR;
                        _response.ReceptorRFC = _nacional.NomDenRazSocR;
                        _response.ReceptorDomicilioFiscal = _nacional.DomicilioFiscalR;
                    }
                }

                if (cfdi.Totales != null) {
                    _response.MontoTotalExento = cfdi.Totales.MontoTotExent;
                    _response.MontoTotalGravado = cfdi.Totales.MontoTotGrav;
                    _response.MontoTotalOperacion = cfdi.Totales.MontoTotOperacion;
                    _response.MontoTotalRetencion = cfdi.Totales.MontoTotRet;

                    if (cfdi.Totales.ISRCorrespondienteSpecified) {
                        _response.ISRCorrespondiente = cfdi.Totales.ISRCorrespondiente;
                    }

                    if (cfdi.Totales.UtilidadBimestralSpecified) {
                        _response.UtilidadBimestral = cfdi.Totales.UtilidadBimestral;
                    }

                    if (cfdi.Totales.ImpRetenidos != null) {
                        _response.ImpuestosRetenidos = this.Create(cfdi.Totales.ImpRetenidos);
                    }
                }

                if (cfdi.Complemento != null) {
                    if (cfdi.Complemento.Pagosaextranjeros != null) {
                        _response.Pagosaextranjeros = this.Create(cfdi.Complemento.Pagosaextranjeros);

                    }

                    if (cfdi.Complemento.TimbreFiscalDigital != null) {
                        _response.TimbreFiscal = this.Create(cfdi.Complemento.TimbreFiscalDigital);

                    }
                }
                return _response;
            }
            return null;
        }

        public ComplementoTimbreFiscal Create(SAT.CRIP.Complemento.TimbreFiscal.V11.TimbreFiscalDigital timbreFiscalDigital) {
            if (timbreFiscalDigital != null) {
                var _response = new ComplementoTimbreFiscal {
                    Version = timbreFiscalDigital.Version,
                    FechaTimbrado = timbreFiscalDigital.FechaTimbrado,
                    NoCertificadoSAT = timbreFiscalDigital.NoCertificadoSAT,
                    UUID = timbreFiscalDigital.UUID,
                    SelloSAT = timbreFiscalDigital.SelloSAT,
                    SelloCFD = timbreFiscalDigital.SelloCFD,
                    Leyenda = timbreFiscalDigital.Leyenda,
                    RFCProvCertif = timbreFiscalDigital.RfcProvCertif
                };
                return _response;
            }
            return null;
        }

        private BindingList<RetencionImpuestosDetailModel> Create(SAT.CRIP.V20.RetencionesTotalesImpRetenidos[] impRetenidos) {
            if (impRetenidos != null) {
                var _response = new BindingList<RetencionImpuestosDetailModel>();
                foreach (var item in impRetenidos) {
                    var _impuesto = new RetencionImpuestosDetailModel();
                    if (item.ImpuestoRetSpecified) {
                        _impuesto.TipoImpuesto = item.ImpuestoRet;
                    }

                    if (item.BaseRetSpecified) {
                        _impuesto.Base = item.BaseRet;
                    }
                    _impuesto.MontoRetenido = item.MontoRet;
                    _impuesto.TipoPago = item.TipoPagoRet;

                    _response.Add(_impuesto);
                }
                return _response;
            }
            return null;
        }
        #endregion
    }
}
