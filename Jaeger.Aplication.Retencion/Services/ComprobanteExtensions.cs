﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Retencion.Entities;
using Jaeger.Domain.Retencion.Entities.Complemento;

namespace Jaeger.Aplication.Retencion.Services {
    public class ComprobanteExtensions : ComplementoExtensions {
        #region ver 10
        public SAT.CRIP.V10.Retenciones Create(ComprobanteRetencionDetailModel model) {
            var _cfd = new SAT.CRIP.V10.Retenciones {
                Version = model.Version,
                Receptor = this.Receptor(model),
                Emisor = this.Emisor(model),
                CveRetenc = model.ClaveRetencion,
                DescRetenc = model.Descripcion,
                FechaExp = Convert.ToDateTime(model.FechaEmision.ToString("yyyy-MM-dd\\THH:mm:ss-06:00")),
                FolioInt = model.Folio,
                Periodo = this.Periodo(model),
                Totales = this.Totales(model)
            };

            if (model.Pagosaextranjeros != null) {
                var _pagoExtranjeros = this.Create(model.Pagosaextranjeros);
                if (_cfd.Complemento == null) {
                    _cfd.Complemento = new SAT.CRIP.V10.RetencionesComplemento();
                }
                _cfd.Complemento.Pagosaextranjeros = _pagoExtranjeros;
            }

            return _cfd;
        }

        private SAT.CRIP.V10.RetencionesTotales Totales(ComprobanteRetencionDetailModel model) {
            var _response = new SAT.CRIP.V10.RetencionesTotales() {
                ImpRetenidos = this.Impuestos(model.ImpuestosRetenidos),
                montoTotExent = this.Ajustar(model.MontoTotalExento),
                montoTotGrav = this.Ajustar(model.MontoTotalGravado),
                montoTotOperacion = this.Ajustar(model.MontoTotalOperacion),
                montoTotRet = this.Ajustar(model.MontoTotalRetencion)
            };
            return _response;
        }

        private SAT.CRIP.V10.RetencionesTotalesImpRetenidos[] Impuestos(BindingList<RetencionImpuestosDetailModel> impuestosRetenidos) {
            if (impuestosRetenidos != null) {
                var _response = new List<SAT.CRIP.V10.RetencionesTotalesImpRetenidos>();
                foreach (var item in impuestosRetenidos) {
                    var impuesto = new SAT.CRIP.V10.RetencionesTotalesImpRetenidos {
                        BaseRet = this.Ajustar(item.Base),
                        montoRet = this.Ajustar(item.MontoRetenido),
                        BaseRetSpecified = item.Base > 0,
                        TipoPagoRet = item.TipoPago,
                        Impuesto = item.TipoImpuesto,
                        ImpuestoSpecified = !string.IsNullOrEmpty(item.TipoImpuesto)
                    };
                    _response.Add(impuesto);
                }
                return _response.ToArray();
            }

            return null;
        }

        private SAT.CRIP.V10.RetencionesPeriodo Periodo(ComprobanteRetencionDetailModel model) {
            var _response = new SAT.CRIP.V10.RetencionesPeriodo() {
                Ejerc = model.Ejercicio,
                MesFin = model.MesFinal,
                MesIni = model.MesInicial
            };
            return _response;
        }

        private SAT.CRIP.V10.RetencionesEmisor Emisor(ComprobanteRetencionDetailModel model) {
            var _response = new SAT.CRIP.V10.RetencionesEmisor {
                RFCEmisor = model.EmisorRFC,
                CURPE = (string.IsNullOrEmpty(model.EmisorCURP) ? null : model.EmisorCURP),
                NomDenRazSocE = model.EmisorNombre
            };
            return _response;
        }

        private SAT.CRIP.V10.RetencionesReceptor Receptor(ComprobanteRetencionDetailModel model) {
            var _response = new SAT.CRIP.V10.RetencionesReceptor();
            _response.Nacionalidad = (model.ReceptorExtranjero == true ? SAT.CRIP.V10.RetencionesReceptorNacionalidad.Extranjero : SAT.CRIP.V10.RetencionesReceptorNacionalidad.Nacional);
            // si es extranjero
            if (model.ReceptorExtranjero == true) {
                _response.Item = new SAT.CRIP.V10.RetencionesReceptorExtranjero() {
                    NomDenRazSocR = model.ReceptorNombre,
                    NumRegIdTrib = model.ReceptorNombre
                };
            } else {
                _response.Item = new SAT.CRIP.V10.RetencionesReceptorNacional() {
                    CURPR = model.ReceptorCURP,
                    NomDenRazSocR = model.ReceptorNombre,
                    RFCRecep = model.ReceptorRFC
                };
            }

            return _response;
        }
        #endregion

        #region version 20
        public SAT.CRIP.V20.Retenciones CreateV20(ComprobanteRetencionDetailModel model) {
            if (model != null) {
                var _response = new SAT.CRIP.V20.Retenciones {
                    Version = model.Version,
                    CveRetenc = model.ClaveRetencion,
                    DescRetenc = model.Descripcion,
                    FechaExp = Convert.ToDateTime(model.FechaEmision.ToString("yyyy-MM-dd\\THH:mm:ss-06:00")),
                    FolioInt = model.Folio,
                    LugarExpRetenc = model.LugarExpRetenc
                };

                _response.Emisor = this.EmisorV20(model);
                _response.Receptor = this.Receptor20(model);
                _response.Totales = this.TotalesV20(model);
                _response.CfdiRetenRelacionados = this.CFDIRelacionado(model.Relacionados);

                if (model.Pagosaextranjeros != null) {
                    var _pagoExtranjeros = this.Create(model.Pagosaextranjeros);
                    if (_response.Complemento == null) {
                        _response.Complemento = new SAT.CRIP.V20.RetencionesComplemento();
                    }
                    _response.Complemento.Pagosaextranjeros = _pagoExtranjeros;
                }

                return _response;
            }
            return null;
        }

        private SAT.CRIP.V20.RetencionesCfdiRetenRelacionados CFDIRelacionado(RetencionesCfdiRetenRelacionados relacionados) {
            if (relacionados != null) {
                var _response = new SAT.CRIP.V20.RetencionesCfdiRetenRelacionados();
                _response.TipoRelacion = relacionados.TipoRelacion;
                _response.UUID = relacionados.UUID;
                return _response;
            }
            return null;
        }

        private SAT.CRIP.V20.RetencionesEmisor EmisorV20(ComprobanteRetencionDetailModel model) {
            if (model != null) {
                var _response = new SAT.CRIP.V20.RetencionesEmisor {
                    RegimenFiscalE = model.RegimenFiscalE,
                    NomDenRazSocE = model.EmisorNombre,
                    RfcE = model.EmisorRFC
                };
                return _response;
            }
            return null;
        }

        private SAT.CRIP.V20.RetencionesReceptor Receptor20(ComprobanteRetencionDetailModel model) {
            if (model != null) {
                var _response = new SAT.CRIP.V20.RetencionesReceptor();
                _response.NacionalidadR = (model.ReceptorExtranjero == true ? SAT.CRIP.V20.RetencionesReceptorNacionalidadR.Extranjero : SAT.CRIP.V20.RetencionesReceptorNacionalidadR.Nacional);
                // si es extranjero
                if (model.ReceptorExtranjero == true) {
                    _response.Item = new SAT.CRIP.V20.RetencionesReceptorExtranjero() {
                        NomDenRazSocR = model.ReceptorNombre,
                        NumRegIdTribR = model.ReceptorNumRegIdTrib
                    };
                } else {
                    _response.Item = new SAT.CRIP.V20.RetencionesReceptorNacional() {
                        CurpR = model.ReceptorCURP,
                        NomDenRazSocR = model.ReceptorNombre,
                        RfcR = model.ReceptorRFC,
                        DomicilioFiscalR = model.ReceptorDomicilioFiscal
                    };
                }
                return _response;
            }
            return null;
        }

        //falta la informacion del ISR
        private SAT.CRIP.V20.RetencionesTotales TotalesV20(ComprobanteRetencionDetailModel model) {
            var _response = new SAT.CRIP.V20.RetencionesTotales() {
                ImpRetenidos = this.ImpuestosV20(model.ImpuestosRetenidos),
                MontoTotExent = this.Ajustar(model.MontoTotalExento),
                MontoTotGrav = this.Ajustar(model.MontoTotalGravado),
                MontoTotOperacion = this.Ajustar(model.MontoTotalOperacion),
                MontoTotRet = this.Ajustar(model.MontoTotalRetencion),
                UtilidadBimestral = this.Ajustar(model.UtilidadBimestral),
                UtilidadBimestralSpecified = model.ISRCorrespondiente > 0,
                ISRCorrespondiente = this.Ajustar(model.ISRCorrespondiente),
                ISRCorrespondienteSpecified = model.ISRCorrespondiente > 0
            };
            return _response;
        }

        private SAT.CRIP.V20.RetencionesTotalesImpRetenidos[] ImpuestosV20(BindingList<RetencionImpuestosDetailModel> impuestosRetenidos) {
            if (impuestosRetenidos != null) {
                var _response = new List<SAT.CRIP.V20.RetencionesTotalesImpRetenidos>();
                foreach (var item in impuestosRetenidos) {
                    var impuesto = new SAT.CRIP.V20.RetencionesTotalesImpRetenidos {
                        BaseRet = this.Ajustar(item.Base),
                        MontoRet = this.Ajustar(item.MontoRetenido),
                        BaseRetSpecified = item.Base > 0,
                        TipoPagoRet = item.TipoPago,
                        ImpuestoRet = item.TipoImpuesto,
                        ImpuestoRetSpecified = !string.IsNullOrEmpty(item.TipoImpuesto)
                    };
                    _response.Add(impuesto);
                }
                return _response.ToArray();
            }

            return null;
        }

        #endregion

        #region complemento Pagos A Extranajeros V10
        private SAT.CRIP.Complemento.PagosAExtranjeros.V10.Pagosaextranjeros Create(ComplementoPagosaextranjeros pagosaextranjeros) {
            if (pagosaextranjeros != null) {
                var _response = new SAT.CRIP.Complemento.PagosAExtranjeros.V10.Pagosaextranjeros {
                    Version = pagosaextranjeros.Version,
                    EsBenefEfectDelCobro = pagosaextranjeros.IsBenefEfectDelCobro ? SAT.CRIP.Complemento.PagosAExtranjeros.V10.PagosaextranjerosEsBenefEfectDelCobro.SI : SAT.CRIP.Complemento.PagosAExtranjeros.V10.PagosaextranjerosEsBenefEfectDelCobro.NO
                };
                if (pagosaextranjeros.IsBenefEfectDelCobro) {
                    _response.NoBeneficiario = this.Create(pagosaextranjeros.NoBeneficiario);
                } else {
                    _response.Beneficiario = this.Create(pagosaextranjeros.Beneficiario);
                }
                return _response;
            }
            return null;
        }

        private SAT.CRIP.Complemento.PagosAExtranjeros.V10.PagosaextranjerosBeneficiario Create(PagosaextranjerosBeneficiario beneficiario) {
            if (beneficiario != null) {
                var _response = new SAT.CRIP.Complemento.PagosAExtranjeros.V10.PagosaextranjerosBeneficiario {
                    ConceptoPago = beneficiario.ConceptoPago,
                    CURP = beneficiario.CURP,
                    DescripcionConcepto = beneficiario.DescripcionConcepto,
                    NomDenRazSocB = beneficiario.NomDenRazSocB,
                    RFC = beneficiario.RFC
                };
                return _response;
            }
            return null;
        }

        private SAT.CRIP.Complemento.PagosAExtranjeros.V10.PagosaextranjerosNoBeneficiario Create(PagosaextranjerosNoBeneficiario noBeneficiario) {
            if (noBeneficiario != null) {
                var _response = new SAT.CRIP.Complemento.PagosAExtranjeros.V10.PagosaextranjerosNoBeneficiario {
                    ConceptoPago = noBeneficiario.ConceptoPago,
                    DescripcionConcepto = noBeneficiario.DescripcionConcepto,
                    PaisDeResidParaEfecFisc = noBeneficiario.PaisDeResidParaEfecFisc
                };
                return _response;
            }
            return null;
        }
        #endregion
    }
}
