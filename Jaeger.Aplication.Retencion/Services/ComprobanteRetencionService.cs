﻿using System;
using System.Linq;
using System.ComponentModel;
using Jaeger.DataAccess.Repositories;
using Jaeger.Aplication.Base;
using Jaeger.Domain.Retencion.Contracts;
using Jaeger.Domain.Retencion.Entities;
using Jaeger.Domain.Retencion.ValueObjects;
using Jaeger.Aplication.Retencion.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Base.Contracts;

namespace Jaeger.Aplication.Retencion.Services {
    public class ComprobanteRetencionService : CertificacionService, IComprobanteRetencionService, ICertificacionService {
        protected IComprobanteRetencionRepository repository;
        protected IEditaBucketService s3;

        public ComprobanteRetencionService() {
            this.repository = new SqlSugarComprobanteRetencionRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.s3 = new EditaBucketService();
        }

        public BindingList<RetencionSingle> GetList(int year, int month) {
            return new BindingList<RetencionSingle>(this.repository.ComprobanteSingles(CFDISubTypeEnum.Emitido, year, month).ToList());
        }

        public BindingList<RetencionSingle> GetSearch(string folio) {
            var _folio = int.Parse(folio);
            return new BindingList<RetencionSingle>(this.repository.GetSearch(_folio).ToList());
        }

        public ComprobanteRetencionDetailModel GetComprobante(int index) {
            return this.repository.GetComprobante(index);
        }

        public ComprobanteRetencionDetailModel Save(ComprobanteRetencionDetailModel model) {
            model = this.repository.Save(model);
            return model;
        }

        public ComprobanteRetencionDetailModel New() {
            var response = new ComprobanteRetencionDetailModel {
                EmisorRFC = ConfigService.Synapsis.Empresa.RFC,
                EmisorNombre = ConfigService.Synapsis.Empresa.RazonSocial,
                RegimenFiscalE = ConfigService.Synapsis.Empresa.RegimenFiscal,
                FechaEmision = System.DateTime.Now,
                SubTipo = CFDISubTypeEnum.Emitido,
                LugarExpRetenc = ConfigService.Synapsis.Empresa.DomicilioFiscal.CodigoPostal
            };
            return response;
        }

        public ComprobanteRetencionDetailModel GetComprobante(string fileName) {
            var _c = new CFD10RetencionExtension();
            var _cfd = SAT.CRIP.V10.Retenciones.Load(fileName);
            var _response = _c.Create(_cfd);
            _c = null;
            _cfd = null;
            _response.IdDocumento = null;
            _response.FechaTimbre = null;
            _response.ReceptorNombre = null;
            _response.FechaEmision = DateTime.Now;
            return _response;
        }

        public ComprobanteRetencionDetailModel Procesar(ComprobanteRetencionDetailModel model) {
            if (model.Version == "1.0") {
                var _response = this.Timbrar(this.comprobanteExtensions.Create(model));
                if (_response != null) {
                    model.TimbreFiscal = this.cfdRetencion10Ext.Create(_response.Complemento.TimbreFiscalDigital);
                    model.NoCertificado = _response.NumCert;
                    model.Version = _response.Version;
                    model.XML = _response.OriginalXmlString;
                    model.Estado = "Vigente";
                    model.Status = "Vigente";
                    model = this.Save(model);
                    _response.Save(ManagerPathService.JaegerPath(PathsEnum.Comprobantes, model.KeyName() + ".xml"));
                    model.FileAccuseXML = this.s3.Upload(ManagerPathService.JaegerPath(PathsEnum.Comprobantes, model.KeyName() + ".xml"), string.Concat(model.KeyName(), ".xml"));
                    this.repository.UpdateUrlXml(model.Id, model.FileXML);
                }
            } else if (model.Version == "2.0") {
                var _response = this.comprobanteExtensions.CreateV20(model);
                _response.Save(@"C:\Jaeger\Jaeger.Temporal\pruebaRet2.xml");
                //var _response = this.Timbrar(this.comprobanteExtensions.CreateV20(model));
                //if (_response != null) {
                //    model.TimbreFiscal = this.cfdRetencion10Ext.Create(_response.Complemento.TimbreFiscalDigital);
                //    model.NoCertificado = _response.NoCertificado;
                //    model.Version = _response.Version;
                //    model.XML = _response.OriginalXmlString;
                //    model.Estado = "Vigente";
                //    model.Status = "Vigente";
                //    model = this.Save(model);
                //    _response.Save(ManagerPathService.JaegerPath(PathsEnum.Comprobantes, model.KeyName() + ".xml"));
                //    model.FileAccuseXML = this.s3.Upload(ManagerPathService.JaegerPath(PathsEnum.Comprobantes, model.KeyName() + ".xml"), string.Concat(model.KeyName(), ".xml"));
                //    this.repository.UpdateUrlXml(model.Id, model.FileXML);
                //}
            }
            return model;
        }

        public ComprobanteRetencionDetailModel Load(string localFile) {
            var d1 = SAT.CRIP.V10.Retenciones.Load(localFile);
            var d2 = new CFD10RetencionExtension();
            var d3 = d2.Create(d1);
            d3.XML = d1.OriginalXmlString;
            this.Save(d3);
            return d3;
        }

        public bool Create() {
            return this.repository.Create();
        }
    }
}
