﻿using System;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.Retencion.Contracts;
using Jaeger.Certifica.Contracts;
using Jaeger.Certifica.Services;
using Jaeger.Crypto.Services;
using Jaeger.DataAccess.Certificado.Repositories;
using Jaeger.Domain.Certificado.Entities;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Retencion.Entities;

namespace Jaeger.Aplication.Retencion.Services {
    /// <summary>
    /// servicio de certificacion de comprobantes fiscales (CFDI)
    /// </summary>
    public partial class CertificacionService : ProveedorAutorizado, ICertificacionService {
        protected ISqlCertificadoRepository certificadoRepository;
        protected IProveedorCertificacion servicio;
        protected CFD10RetencionExtension cfdRetencion10Ext;
        protected ComprobanteExtensions comprobanteExtensions;

        public CertificacionService() {
            this.cfdRetencion10Ext = new CFD10RetencionExtension();
            this.comprobanteExtensions = new ComprobanteExtensions();
            this.certificadoRepository = new SqlSugarCertificadoRepository(ConfigService.Synapsis.RDS.Edita);
            this.Certificacion = ConfigService.Synapsis.ProveedorAutorizado.Certificacion;
            this.Certificacion.Provider = ServiceProvider.EnumServicePAC.SolucionFactible;
            this.Cancelacion = ConfigService.Synapsis.ProveedorAutorizado.Cancelacion;
        }

        public string Mensaje {
            get; set;
        }

        public int Codigo {
            get; set;
        }

        protected object Timbrar(ComprobanteRetencionDetailModel cfdi) {
            if (cfdi != null) {
                if (cfdi.Version == "1.0") {
                    var item = this.comprobanteExtensions.Create(cfdi);
                    if (item != null) {
                        return this.Timbrar(item);
                    }
                } else if (cfdi.Version == "2.0") {
                    //var item = this.cfd40Ext.Create(cfdi);
                    //return this.Timbrar(item);
                }
            }
            return null;
        }

        /// <summary>
        /// certificacion de un objeto comprobante fiscal
        /// </summary>
        public SAT.CRIP.V10.Retenciones Timbrar(SAT.CRIP.V10.Retenciones cfdi) {
            if (cfdi != null) {
                cfdi = this.Sellar(cfdi);
                if (cfdi != null) {
                    if (this.Certificacion.Provider == ServiceProvider.EnumServicePAC.SolucionFactible)
                        this.servicio = new SolucionFactibleService(this.Configura(this.Certificacion));
                    else if (this.Certificacion.Provider == ServiceProvider.EnumServicePAC.FiscoClic) {
                        //this.servicio = new FiscoClic(this.Configura(this.Certificacion));
                    }
                    else if (this.Certificacion.Provider == ServiceProvider.EnumServicePAC.Factorum) {
                        //this.servicio = new Factorum(this.Configura(this.Certificacion));
                        this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
                    } else {
                        this.Codigo = 2;
                        this.Mensaje = "Error al generar el comprobante fiscal (XML)";
                        cfdi = null;
                    }

                    cfdi = this.servicio.TimbrarRetencion(cfdi.Serialize());
                    this.Codigo = this.servicio.Codigo;
                    this.Mensaje = this.servicio.Mensaje;

                    // resultado
                    if (cfdi != null)
                        return cfdi;
                } else {
                    this.Codigo = 3;
                    this.Mensaje = "No se envío un objeto válido";
                }
            }
            return null;
        }
        public SAT.CRIP.V20.Retenciones Timbrar(SAT.CRIP.V20.Retenciones cfdi) {
            return null;
        }
        //public CFDI.Cancel.CancelaCFDResponse Cancelar(string idDocumento, string clave) {
        //    if (this.Cancelacion.Provider == ServiceProvider.EnumServicePAC.SolucionFactible) {
        //        // obtener información de certificado y llave
        //        var info = this.InfoCertificados(EnumOrigenCertificado.BaseDeDatos);
        //        this.servicio = new SolucionFactibleService(this.Configura(this.Cancelacion));
        //        this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
        //        this.servicio.CerBase64 = info.CerB64;
        //        this.servicio.KeyBase64 = info.KeyB64;
        //        this.servicio.PassKey = info.KeyPassB64;
        //    } else if (this.Cancelacion.Provider == ServiceProvider.EnumServicePAC.FiscoClic) {
        //        this.servicio = new FiscoClic(this.Configura(this.Cancelacion));
        //        this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
        //    } else if (this.Cancelacion.Provider == ServiceProvider.EnumServicePAC.Factorum) {
        //        this.servicio = new Factorum(this.Configura(this.Cancelacion));
        //        this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
        //    } else {
        //        this.Codigo = 1;
        //        this.Mensaje = "No se especifico Proveedor Autorizado de Certificación";
        //        return null;
        //    }

        //    var response = this.servicio.Cancelar(idDocumento, clave);
        //    this.Codigo = this.servicio.Codigo;
        //    this.Mensaje = this.servicio.Mensaje;
        //    if (response != null) {
        //        return CFDI.Cancel.CancelaCFDResponse.LoadXml(response.Xml());
        //    }
        //    return null;
        //}

        //public CFDI.Cancel.CancelaCFDResponse Cancelar(ComprobanteFiscalDetailModel cfdi, string clave) {
        //    if (cfdi != null) {
        //        if (this.Cancelacion.Provider == ServiceProvider.EnumServicePAC.SolucionFactible) {
        //            // obtener información de certificado y llave
        //            var info = this.InfoCertificados(EnumOrigenCertificado.BaseDeDatos);
        //            this.servicio = new SolucionFactible(this.Configura(this.Cancelacion));
        //            this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
        //            this.servicio.CerBase64 = info.CerB64;
        //            this.servicio.KeyBase64 = info.KeyB64;
        //            this.servicio.PassKey = info.KeyPassB64;
        //        } else if (this.Cancelacion.Provider == ServiceProvider.EnumServicePAC.FiscoClic) {
        //            this.servicio = new FiscoClic(this.Configura(this.Cancelacion));
        //            this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
        //        } else if (this.Cancelacion.Provider == ServiceProvider.EnumServicePAC.Factorum) {
        //            this.servicio = new Factorum(this.Configura(this.Cancelacion));
        //            this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
        //        } else {
        //            this.Codigo = 1;
        //            this.Mensaje = "No se especifico Proveedor Autorizado de Certificación";
        //            return null;
        //        }

        //        var response = this.servicio.Cancelar(cfdi.TimbreFiscal.UUID, clave);
        //        this.Codigo = this.servicio.Codigo;
        //        this.Mensaje = this.servicio.Mensaje;
        //        if (response != null)
        //            return CFDI.Cancel.CancelaCFDResponse.LoadXml(response.Xml());
        //    } else {
        //        this.Codigo = 1;
        //        this.Mensaje = "No se envio un objeto válido";
        //    }
        //    return null;
        //}

        #region metodos privados

        private SAT.CRIP.V10.Retenciones Sellar(SAT.CRIP.V10.Retenciones cfdi, string codSha = "sha256") {
            // obtener información de certificado y llave
            var info = this.InfoCertificados(1);

            if (info != null) {
                Certificate cert = new Certificate();
                PrivateKey key = new PrivateKey();
                //// carga de informacion de llave y certificado
                cert.CargarB64(info.CerB64);
                key.CargarB64(info.KeyB64, info.KeyPassB64);
                cert.CargarLlavePrivada(key);
                // agregar no. de serie del certificado y el certificado en base64
                cfdi.NumCert = cert.NoSerie;
                cfdi.Cert = cert.ExportarB64();
                
                // sellar el comprobante
                if (codSha == "sha1") {
                    cfdi.Sello = key.SellarCadenaSha1(cfdi.CadenaOriginalOff);
                } else {
                    cfdi.Sello = key.SellarCadenaSha256(cfdi.CadenaOriginalOff);
                }

                if (key.CodigoDeError != 0) {
                    this.Codigo = key.CodigoDeError;
                    this.Mensaje = string.Concat("Sello de Comprobante: ", key.MensajeDeError);
                    return null;
                }
                return cfdi;
            } else {
                this.Codigo = 3;
                this.Mensaje = "No existe información válida para llave y certificado";
            }
            return null;
        }

        private CertificadoModel InfoCertificados(int origen) {
            if (origen == 1) {
                var info = this.certificadoRepository.GetCertificado();
                return info;
            } else if (origen == 0) {

            }
            return null;
        }

        private Certifica.Entities.ProviderService Configura(Domain.Empresa.Contracts.IServiceProvider conf) {
            var response = new Certifica.Entities.ProviderService {
                User = conf.User,
                Password = conf.Pass,
                Production = conf.Production,
                RFC = conf.RFC,
                Strinct = conf.Strinct
            };
            if (conf.Provider == ServiceProvider.EnumServicePAC.Factorum)
                response.Provider = Certifica.Entities.ProviderService.ServiceProviderEnum.Factorum;
            else if (conf.Provider == ServiceProvider.EnumServicePAC.FiscoClic)
                response.Provider = Certifica.Entities.ProviderService.ServiceProviderEnum.FiscoClic;
            else if (conf.Provider == ServiceProvider.EnumServicePAC.SolucionFactible)
                response.Provider = Certifica.Entities.ProviderService.ServiceProviderEnum.SolucionFactible;
            else if (conf.Provider == ServiceProvider.EnumServicePAC.Interno)
                response.Provider = Certifica.Entities.ProviderService.ServiceProviderEnum.Interno;
            else
                response.Provider = Certifica.Entities.ProviderService.ServiceProviderEnum.Ninguno;
            return response;
        }

        #endregion
    }
}
