﻿using System.ComponentModel;
using Jaeger.Domain.Retencion.Entities;

namespace Jaeger.Aplication.Retencion.Contracts {
    public interface IComprobanteRetencionService {

        ComprobanteRetencionDetailModel GetComprobante(int index);

        ComprobanteRetencionDetailModel GetComprobante(string fileName);

        ComprobanteRetencionDetailModel Save(ComprobanteRetencionDetailModel model);

        ComprobanteRetencionDetailModel Procesar(ComprobanteRetencionDetailModel model);

        ComprobanteRetencionDetailModel New();

        BindingList<RetencionSingle> GetList(int year, int month);

        BindingList<RetencionSingle> GetSearch(string folio);

        ComprobanteRetencionDetailModel Load(string localFile);

        bool Create();
    }
}
