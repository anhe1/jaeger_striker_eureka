﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///IMPUESTOSFISCALES
   ///</summary>
   [SugarTable("impuestosfiscales")]
   public partial class IMPUESTOSFISCALES
   {
      public IMPUESTOSFISCALES(){
      }

      private int _NUMIMPUESTO;
      private string _UUIDTIMBRECFDI;
      private string  _IMPUESTO;
      private int? _TIPOIMPUESTO;
      private int? _ESLOCAL;
      private double? _BASE;
      private string _TIPOFACTOR;
      private double? _TASACUOTA;
      private double? _IMPORTE;

      /// <summary>
      /// obtener o establecer NUMIMPUESTO
      /// </summary>
      [DataNames("NUMIMPUESTO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "numimpuesto", ColumnDescription = "Clave", IsNullable = false)] 
      public int NUMIMPUESTO {get { return this._NUMIMPUESTO; } set { this._NUMIMPUESTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer UUIDTIMBRECFDI
      /// </summary>
      [DataNames("UUIDTIMBRECFDI")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "uuidtimbrecfdi", ColumnDescription = "UUID timbre del cfdi", IsNullable = false, Length = 36)] 
      public string UUIDTIMBRECFDI {get { return this._UUIDTIMBRECFDI; } set { this._UUIDTIMBRECFDI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IMPUESTO
      /// </summary>
      [DataNames("IMPUESTO")]
      [SugarColumn(ColumnName = "impuesto", ColumnDescription = "Nombre del impuesto", Length = 3)] 
      public string IMPUESTO {get { return this._IMPUESTO; } set { this._IMPUESTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOIMPUESTO
      /// </summary>
      [DataNames("TIPOIMPUESTO")]
      [SugarColumn(ColumnName = "tipoimpuesto", ColumnDescription = "tipo del impuesto (0 Trasladado, 1 Retenido)")] 
      public int? TIPOIMPUESTO {get { return this._TIPOIMPUESTO; } set { this._TIPOIMPUESTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ESLOCAL
      /// </summary>
      [DataNames("ESLOCAL")]
      [SugarColumn(ColumnName = "eslocal", ColumnDescription = "Tiene impuesto local( 1 si y 0 no)")] 
      public int? ESLOCAL {get { return this._ESLOCAL; } set { this._ESLOCAL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BASE
      /// </summary>
      [DataNames("BASE")]
      [SugarColumn(ColumnName = "base", ColumnDescription = "Base del impuesto")] 
      public double? BASE {get { return this._BASE; } set { this._BASE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOFACTOR
      /// </summary>
      [DataNames("TIPOFACTOR")]
      [SugarColumn(ColumnName = "tipofactor", ColumnDescription = "Tipo factor (tasa ,cuota o exento)", Length = 6)] 
      public string TIPOFACTOR {get { return this._TIPOFACTOR; } set { this._TIPOFACTOR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TASACUOTA
      /// </summary>
      [DataNames("TASACUOTA")]
      [SugarColumn(ColumnName = "tasacuota", ColumnDescription = "Valor de la tasa o cuota")] 
      public double? TASACUOTA {get { return this._TASACUOTA; } set { this._TASACUOTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IMPORTE
      /// </summary>
      [DataNames("IMPORTE")]
      [SugarColumn(ColumnName = "importe", ColumnDescription = "Importe")] 
      public double? IMPORTE {get { return this._IMPORTE; } set { this._IMPORTE = value; this.OnPropertyChanged(); }}
   }
}
