﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///HOJAS
   ///</summary>
   [SugarTable("hojas")]
   public partial class HOJAS
   {
      public HOJAS(){
      }

      private string _TITULO;
      private string _HOJAXLS;
      private string _TIPOHOJA;

      /// <summary>
      /// obtener o establecer TITULO
      /// </summary>
      [DataNames("TITULO")]
      [SugarColumn(ColumnName = "titulo", ColumnDescription = "Titulo")]
      public string TITULO {get { return this._TITULO; } set { this._TITULO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer HOJAXLS
      /// </summary>
      [DataNames("HOJAXLS")]
      [SugarColumn(ColumnName = "hojaxls", ColumnDescription = "Hoja")]
      public string HOJAXLS {get { return this._HOJAXLS; } set { this._HOJAXLS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOHOJA
      /// </summary>
      [DataNames("TIPOHOJA")]
      [SugarColumn(ColumnName = "tipohoja", ColumnDescription = "Tipo hoja")]
      public string TIPOHOJA {get { return this._TIPOHOJA; } set { this._TIPOHOJA = value; this.OnPropertyChanged(); }}
    }
}
