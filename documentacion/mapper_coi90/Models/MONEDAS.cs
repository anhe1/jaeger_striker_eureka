﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///MONEDAS
   ///</summary>
   [SugarTable("monedas")]
   public partial class MONEDAS
   {
      public MONEDAS(){
      }

      private int _MONEDA;
      private string _NOMBRE;
      private string _PREFIJO;
      private DateTime? _FECHA;
      private double? _TIPOCAMBIO;
      private double? _MONTO;
      private string _DESPAUTO;
      private string _CLASSAT;

      /// <summary>
      /// obtener o establecer MONEDA
      /// </summary>
      [DataNames("MONEDA")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "moneda", ColumnDescription = "Moneda", IsNullable = false)]
      public int MONEDA {get { return this._MONEDA; } set { this._MONEDA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOMBRE
      /// </summary>
      [DataNames("NOMBRE")] 
      [SugarColumn(ColumnName = "nombre", ColumnDescription = "Descripcion")]
      public string NOMBRE {get { return this._NOMBRE; } set { this._NOMBRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PREFIJO
      /// </summary>
      [DataNames("PREFIJO")] 
      [SugarColumn(ColumnName = "prefijo", ColumnDescription = "Prefijo")]
      public string PREFIJO {get { return this._PREFIJO; } set { this._PREFIJO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHA
      /// </summary>
      [DataNames("FECHA")] 
      [SugarColumn(ColumnName = "fecha", ColumnDescription = "Fecha registro")]
      public DateTime? FECHA {get { return this._FECHA; } set { this._FECHA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOCAMBIO
      /// </summary>
      [DataNames("TIPOCAMBIO")] 
      [SugarColumn(ColumnName = "tipocambio", ColumnDescription = "Tipo de cambio")]
      public double? TIPOCAMBIO {get { return this._TIPOCAMBIO; } set { this._TIPOCAMBIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTO
      /// </summary>
      [DataNames("MONTO")] 
      [SugarColumn(ColumnName = "monto", ColumnDescription = "Monto")]
      public double? MONTO {get { return this._MONTO; } set { this._MONTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DESPAUTO
      /// </summary>
      [DataNames("DESPAUTO")] 
      [SugarColumn(ColumnName = "despauto", ColumnDescription = "Desplazamiento automatico")]
      public string DESPAUTO {get { return this._DESPAUTO; } set { this._DESPAUTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CLASSAT
      /// </summary>
      [DataNames("CLASSAT")] 
      [SugarColumn(ColumnName = "classat", ColumnDescription = "Clave fiscal")]
      public string CLASSAT {get { return this._CLASSAT; } set { this._CLASSAT = value; this.OnPropertyChanged(); }}
   }
}
