﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///POLDINAMICA
   ///</summary>
   [SugarTable("poldinamica")]
   public partial class POLDINAMICA
   {
      public POLDINAMICA(){
      }

      private string _TITULO;
      private string _RUTAARCHIVO;
      private string _TIPO;
      private string _CLAS_USR;

      /// <summary>
      /// obtener o establecer TITULO
      /// </summary>
      [DataNames("TITULO")]
      [SugarColumn(ColumnName = "titulo", ColumnDescription = "Titulo del archivo", IsNullable = false, Length = 256)]
      public string TITULO {get { return this._TITULO; } set { this._TITULO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RUTAARCHIVO
      /// </summary>
      [DataNames("RUTAARCHIVO")]
      [SugarColumn(ColumnName = "rutaarchivo", ColumnDescription = "Ruta del archivo", IsNullable = false, Length = 512)]
      public string RUTAARCHIVO {get { return this._RUTAARCHIVO; } set { this._RUTAARCHIVO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPO
      /// </summary>
      [DataNames("TIPO")]
      [SugarColumn(ColumnName = "tipo", ColumnDescription = "Tipo", Length = 5)]
      public string TIPO {get { return this._TIPO; } set { this._TIPO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CLAS_USR
      /// </summary>
      [DataNames("CLAS_USR")]
      [SugarColumn(ColumnName = "clas_usr", ColumnDescription = "Clasificacion segun el usuario", Length = 256)]
      public string CLAS_USR {get { return this._CLAS_USR; } set { this._CLAS_USR = value; this.OnPropertyChanged(); }}
   }
}
