﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///OPEIET
   ///</summary>
   [SugarTable("opeiet")]
   public partial class OPEIET
   {
      public OPEIET(){
      }

      private string _TIPOPOL;
      private string _NUMPOL;
      private DateTime _FECHAPOL;
      private int _NUMPART;
      private string _NUMCTA;
      private string _RFCPROVE;
      private int? _TIPOPE;
      private double? _MONCONIVA;
      private double? _MONDEDISR;
      private double? _ACTOS15;
      private double? _IVAOP15;
      private double? _ACTOS10;
      private double? _IVAOP10;
      private double? _ACTOSCERO;
      private double? _ACTOSEXENT;
      private double? _IVARETENID;
      private double? _IVATRASLAD;
      private double? _IVADEVOLU;
      private DateTime? _PERCAUSAC;
      private double? _IVANOAC15;
      private double? _IVANOAC10;
      private string _ESIMPORTA;
      private double? _OTRASRET;
      private int? _ESDEVOL;
      private double? _ISRRETENID;
      private int? _IVAGENERAL;
      private int? _IVAFRONTERIZO;
      private int? _IDCONCEP;
      private double? _INGREXENTO;
      private int? _IDCONCEPIVAT;
      private int? _IDOPEIET;
      
      /// <summary>
      /// obtener o establecer TIPOPOL
      /// </summary>
      [DataNames("TIPOPOL")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "tipopol", ColumnDescription = "Tipo", IsNullable = false, Length = 2)]
      public string TIPOPOL {get { return this._TIPOPOL; } set { this._TIPOPOL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMPOL
      /// </summary>
      [DataNames("NUMPOL")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "numpol", ColumnDescription = "Numero", IsNullable = false, Length = 5)]
      public string NUMPOL {get { return this._NUMPOL; } set { this._NUMPOL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHAPOL
      /// </summary>
      [DataNames("FECHAPOL")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "fechapol", ColumnDescription = "Fecha poliza", IsNullable = false)]
      public DateTime FECHAPOL {get { return this._FECHAPOL; } set { this._FECHAPOL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMPART
      /// </summary>
      [DataNames("NUMPART")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "numpart", ColumnDescription = "Numero de partida", IsNullable = false)]
      public int NUMPART {get { return this._NUMPART; } set { this._NUMPART = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMCTA
      /// </summary>
      [DataNames("NUMCTA")]
      [SugarColumn(ColumnName = "numcta", ColumnDescription = "Numero de cuenta", Length = 21)]
      public string NUMCTA {get { return this._NUMCTA; } set { this._NUMCTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFCPROVE
      /// </summary>
      [DataNames("RFCPROVE")]
      [SugarColumn(ColumnName = "rfcprove", ColumnDescription = "RFC proveedor", Length = 40)]
      public string RFCPROVE {get { return this._RFCPROVE; } set { this._RFCPROVE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOPE
      /// </summary>
      [DataNames("TIPOPE")]
      [SugarColumn(ColumnName = "tipope", ColumnDescription = "Tipo operacion")]
      public int? TIPOPE {get { return this._TIPOPE; } set { this._TIPOPE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONCONIVA
      /// </summary>
      [DataNames("MONCONIVA")]
      [SugarColumn(ColumnName = "monconiva", ColumnDescription = "Monto con IVA")]
      public double? MONCONIVA {get { return this._MONCONIVA; } set { this._MONCONIVA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONDEDISR
      /// </summary>
      [DataNames("MONDEDISR")]
      [SugarColumn(ColumnName = "mondedisr", ColumnDescription = "Monto de ISR")]
      public double? MONDEDISR {get { return this._MONDEDISR; } set { this._MONDEDISR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ACTOS15
      /// </summary>
      [DataNames("ACTOS15")]
      [SugarColumn(ColumnName = "actos15", ColumnDescription = "Total de actos al 15 por ciento")]
      public double? ACTOS15 {get { return this._ACTOS15; } set { this._ACTOS15 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IVAOP15
      /// </summary>
      [DataNames("IVAOP15")]
      [SugarColumn(ColumnName = "ivaop15", ColumnDescription = "Porcentaje de iva pagado al 15 por ciento")]
      public double? IVAOP15 {get { return this._IVAOP15; } set { this._IVAOP15 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ACTOS10
      /// </summary>
      [DataNames("ACTOS10")]
      [SugarColumn(ColumnName = "actos10", ColumnDescription = "Total de actos al 10 por ciento")]
      public double? ACTOS10 {get { return this._ACTOS10; } set { this._ACTOS10 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IVAOP10
      /// </summary>
      [DataNames("IVAOP10")]
      [SugarColumn(ColumnName = "ivaop10", ColumnDescription = "Porcentaje de iva pagado al 10 por ciento")]
      public double? IVAOP10 {get { return this._IVAOP10; } set { this._IVAOP10 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ACTOSCERO
      /// </summary>
      [DataNames("ACTOSCERO")]
      [SugarColumn(ColumnName = "actoscero", ColumnDescription = "Actos en cero")]
      public double? ACTOSCERO {get { return this._ACTOSCERO; } set { this._ACTOSCERO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ACTOSEXENT
      /// </summary>
      [DataNames("ACTOSEXENT")]
      [SugarColumn(ColumnName = "actosexent", ColumnDescription = "Actos")]
      public double? ACTOSEXENT {get { return this._ACTOSEXENT; } set { this._ACTOSEXENT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IVARETENID
      /// </summary>
      [DataNames("IVARETENID")]
      [SugarColumn(ColumnName = "ivaretenid", ColumnDescription = "Porcentaje de iva retenido")]
      public double? IVARETENID {get { return this._IVARETENID; } set { this._IVARETENID = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IVATRASLAD
      /// </summary>
      [DataNames("IVATRASLAD")]
      [SugarColumn(ColumnName = "ivatraslad", ColumnDescription = "Porcentaje de iva trasladado")]
      public double? IVATRASLAD {get { return this._IVATRASLAD; } set { this._IVATRASLAD = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IVADEVOLU
      /// </summary>
      [DataNames("IVADEVOLU")]
      [SugarColumn(ColumnName = "ivadevolu", ColumnDescription = "Porcentaje de iva devuelto")]
      public double? IVADEVOLU {get { return this._IVADEVOLU; } set { this._IVADEVOLU = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PERCAUSAC
      /// </summary>
      [DataNames("PERCAUSAC")]
      [SugarColumn(ColumnName = "percausac", ColumnDescription = "Cusacion")]
      public DateTime? PERCAUSAC {get { return this._PERCAUSAC; } set { this._PERCAUSAC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IVANOAC15
      /// </summary>
      [DataNames("IVANOAC15")]
      [SugarColumn(ColumnName = "ivanoac15", ColumnDescription = "Porcentaje de iva no acreditable al 15 por ciento")]
      public double? IVANOAC15 {get { return this._IVANOAC15; } set { this._IVANOAC15 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IVANOAC10
      /// </summary>
      [DataNames("IVANOAC10")]
      [SugarColumn(ColumnName = "ivanoac10", ColumnDescription = "Porcentaje de iva noa creditable al 10 por ciento")]
      public double? IVANOAC10 {get { return this._IVANOAC10; } set { this._IVANOAC10 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ESIMPORTA
      /// </summary>
      [DataNames("ESIMPORTA")]
      [SugarColumn(ColumnName = "esimporta", ColumnDescription = "Es importacion", Length = 1)]
      public string ESIMPORTA {get { return this._ESIMPORTA; } set { this._ESIMPORTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer OTRASRET
      /// </summary>
      [DataNames("OTRASRET")]
      [SugarColumn(ColumnName = "otrasret", ColumnDescription = "Porcentaje de otras retenciones")]
      public double? OTRASRET {get { return this._OTRASRET; } set { this._OTRASRET = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ESDEVOL
      /// </summary>
      [DataNames("ESDEVOL")]
      [SugarColumn(ColumnName = "esdevol", ColumnDescription = "Indica si es devolucion")]
      public int? ESDEVOL {get { return this._ESDEVOL; } set { this._ESDEVOL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ISRRETENID
      /// </summary>
      [DataNames("ISRRETENID")]
      [SugarColumn(ColumnName = "isrretenid", ColumnDescription = "Porcentaje de ISR retenido")]
      public double? ISRRETENID {get { return this._ISRRETENID; } set { this._ISRRETENID = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IVAGENERAL
      /// </summary>
      [DataNames("IVAGENERAL")]
      [SugarColumn(ColumnName = "ivageneral", ColumnDescription = "Iva genera")]
      public int? IVAGENERAL {get { return this._IVAGENERAL; } set { this._IVAGENERAL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IVAFRONTERIZ
      /// </summary>
      [DataNames("IVAFRONTERIZ")]
      [SugarColumn(ColumnName = "ivafronterizo", ColumnDescription = "Iva fronterizo")]
      public int? IVAFRONTERIZO {get { return this._IVAFRONTERIZO; } set { this._IVAFRONTERIZO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IDCONCEP
      /// </summary>
      [DataNames("IDCONCEP")]
      [SugarColumn(ColumnName = "idconcep", ColumnDescription = "Identificador del concepto")]
      public int? IDCONCEP {get { return this._IDCONCEP; } set { this._IDCONCEP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer INGREXENTO
      /// </summary>
      [DataNames("INGREXENTO")]
      [SugarColumn(ColumnName = "ingrexento", ColumnDescription = "Ingreso excento")]
      public double? INGREXENTO {get { return this._INGREXENTO; } set { this._INGREXENTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IDCONCEPIVAT
      /// </summary>
      [DataNames("IDCONCEPIVAT")]
      [SugarColumn(ColumnName = "idconcepivat", ColumnDescription = "Identificador del concepto de iva")]
      public int? IDCONCEPIVAT {get { return this._IDCONCEPIVAT; } set { this._IDCONCEPIVAT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IDOPEIET
      /// </summary>
      [DataNames("IDOPEIET")]
      [SugarColumn(ColumnName = "idopeiet", ColumnDescription = "Identificador de operaciones IETU")]
      public int? IDOPEIET {get { return this._IDOPEIET; } set { this._IDOPEIET = value; this.OnPropertyChanged(); }}
   }
}
