﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///CCOSTOS
   ///</summary>
   [SugarTable("ccostos")]
   public partial class CCOSTOS
   {
      public CCOSTOS(){
      }

      private int _ID;
      private string _DESCRIPCION;

      /// <summary>
      /// obtener o establecer ID
      /// </summary>
      [DataNames("ID")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "id", ColumnDescription = "Identificador", IsNullable = false)] 
      public int ID {get { return this._ID; } set { this._ID = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DESCRIPCION
      /// </summary>
      [DataNames("DESCRIPCION")]
      [SugarColumn(ColumnName = "descripcion", ColumnDescription = "Descripcion", Length = 40)] 
      public string DESCRIPCION {get { return this._DESCRIPCION; } set { this._DESCRIPCION = value; this.OnPropertyChanged(); }}
   }
}
