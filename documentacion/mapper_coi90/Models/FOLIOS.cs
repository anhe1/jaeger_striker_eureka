﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///FOLIOS
   ///</summary>
   [SugarTable("folios")]
   public partial class FOLIOS
   {
      public FOLIOS(){
      }

      private string _TIPPOL;
      private int _EJERCICIO;
      private string _FOLIO01;
      private string _FOLIO02;
      private string _FOLIO03;
      private string _FOLIO04;
      private string _FOLIO05;
      private string _FOLIO06;
      private string _FOLIO07;
      private string _FOLIO08;
      private string _FOLIO09;
      private string _FOLIO10;
      private string _FOLIO11;
      private string _FOLIO12;
      private string _FOLIO13;
      private int? _ASIG01;
      private int? _ASIG02;
      private int? _ASIG03;
      private int? _ASIG04;
      private int? _ASIG05;
      private int? _ASIG06;
      private int? _ASIG07;
      private int? _ASIG08;
      private int? _ASIG09;
      private int? _ASIG10;
      private int? _ASIG11;
      private int? _ASIG12;
      private int? _ASIG13;
      private int? _ASIG14;
      private string _FOLIO14;

      /// <summary>
      /// obtener o establecer TIPPOL
      /// </summary>
      [DataNames("TIPPOL")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "tippol", ColumnDescription = "Tipo poliza", IsNullable = false, Length = 2)]
      public string TIPPOL {get { return this._TIPPOL; } set { this._TIPPOL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer EJERCICIO
      /// </summary>
      [DataNames("EJERCICIO")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "ejercicio", ColumnDescription = "Ejercicio", IsNullable = false)]
      public int EJERCICIO {get { return this._EJERCICIO; } set { this._EJERCICIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO01
      /// </summary>
      [DataNames("FOLIO01")] 
      [SugarColumn(ColumnName = "folio01", ColumnDescription = "Numero de folio para el mes 1", Length = 5)]
      public string FOLIO01 {get { return this._FOLIO01; } set { this._FOLIO01 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO02
      /// </summary>
      [DataNames("FOLIO02")] 
      [SugarColumn(ColumnName = "folio02", ColumnDescription = "Numero de folio para el mes 2", Length = 5)]
      public string FOLIO02 {get { return this._FOLIO02; } set { this._FOLIO02 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO03
      /// </summary>
      [DataNames("FOLIO03")] 
      [SugarColumn(ColumnName = "folio03", ColumnDescription = "Numero de folio para el mes 3", Length = 5)]
      public string FOLIO03 {get { return this._FOLIO03; } set { this._FOLIO03 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO04
      /// </summary>
      [DataNames("FOLIO04")] 
      [SugarColumn(ColumnName = "folio04", ColumnDescription = "Numero de folio para el mes 4", Length = 5)]
      public string FOLIO04 {get { return this._FOLIO04; } set { this._FOLIO04 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO05
      /// </summary>
      [DataNames("FOLIO05")] 
      [SugarColumn(ColumnName = "folio05", ColumnDescription = "Numero de folio para el mes 5", Length = 5)]
      public string FOLIO05 {get { return this._FOLIO05; } set { this._FOLIO05 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO06
      /// </summary>
      [DataNames("FOLIO06")] 
      [SugarColumn(ColumnName = "folio06", ColumnDescription = "Numero de folio para el mes 6", Length = 5)]
      public string FOLIO06 {get { return this._FOLIO06; } set { this._FOLIO06 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO07
      /// </summary>
      [DataNames("FOLIO07")] 
      [SugarColumn(ColumnName = "folio07", ColumnDescription = "Numero de folio para el mes 7", Length = 5)]
      public string FOLIO07 {get { return this._FOLIO07; } set { this._FOLIO07 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO08
      /// </summary>
      [DataNames("FOLIO08")] 
      [SugarColumn(ColumnName = "folio08", ColumnDescription = "Numero de folio para el mes 8", Length = 5)]
      public string FOLIO08 {get { return this._FOLIO08; } set { this._FOLIO08 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO09
      /// </summary>
      [DataNames("FOLIO09")] 
      [SugarColumn(ColumnName = "folio09", ColumnDescription = "Numero de folio para el mes 9", Length = 5)]
      public string FOLIO09 {get { return this._FOLIO09; } set { this._FOLIO09 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO10
      /// </summary>
      [DataNames("FOLIO10")] 
      [SugarColumn(ColumnName = "folio10", ColumnDescription = "Numero de folio para el mes 10", Length = 5)]
      public string FOLIO10 {get { return this._FOLIO10; } set { this._FOLIO10 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO11
      /// </summary>
      [DataNames("FOLIO11")] 
      [SugarColumn(ColumnName = "folio11", ColumnDescription = "Numero de folio para el mes 11", Length = 5)]
      public string FOLIO11 {get { return this._FOLIO11; } set { this._FOLIO11 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO12
      /// </summary>
      [DataNames("FOLIO12")] 
      [SugarColumn(ColumnName = "folio12", ColumnDescription = "Numero de folio para el mes 12", Length = 5)]
      public string FOLIO12 {get { return this._FOLIO12; } set { this._FOLIO12 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO13
      /// </summary>
      [DataNames("FOLIO13")] 
      [SugarColumn(ColumnName = "folio13", ColumnDescription = "Numero de folio para el mes 13", Length = 5)]
      public string FOLIO13 {get { return this._FOLIO13; } set { this._FOLIO13 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ASIG01
      /// </summary>
      [DataNames("ASIG01")] 
      [SugarColumn(ColumnName = "asig01", ColumnDescription = "Indica que ya se ha asignado folio al mes 1")]
      public int? ASIG01 {get { return this._ASIG01; } set { this._ASIG01 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ASIG02
      /// </summary>
      [DataNames("ASIG02")] 
      [SugarColumn(ColumnName = "asig02", ColumnDescription = "Indica que ya se ha asignado folio al mes 2")]
      public int? ASIG02 {get { return this._ASIG02; } set { this._ASIG02 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ASIG03
      /// </summary>
      [DataNames("ASIG03")] 
      [SugarColumn(ColumnName = "asig03", ColumnDescription = "Indica que ya se ha asignado folio al mes 3")]
      public int? ASIG03 {get { return this._ASIG03; } set { this._ASIG03 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ASIG04
      /// </summary>
      [DataNames("ASIG04")] 
      [SugarColumn(ColumnName = "asig04", ColumnDescription = "Indica que ya se ha asignado folio al mes 4")]
      public int? ASIG04 {get { return this._ASIG04; } set { this._ASIG04 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ASIG05
      /// </summary>
      [DataNames("ASIG05")] 
      [SugarColumn(ColumnName = "asig05", ColumnDescription = "Indica que ya se ha asignado folio al mes 5")]
      public int? ASIG05 {get { return this._ASIG05; } set { this._ASIG05 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ASIG06
      /// </summary>
      [DataNames("ASIG06")] 
      [SugarColumn(ColumnName = "asig06", ColumnDescription = "Indica que ya se ha asignado folio al mes 6")]
      public int? ASIG06 {get { return this._ASIG06; } set { this._ASIG06 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ASIG07
      /// </summary>
      [DataNames("ASIG07")] 
      [SugarColumn(ColumnName = "asig07", ColumnDescription = "Indica que ya se ha asignado folio al mes 7")]
      public int? ASIG07 {get { return this._ASIG07; } set { this._ASIG07 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ASIG08
      /// </summary>
      [DataNames("ASIG08")] 
      [SugarColumn(ColumnName = "asig08", ColumnDescription = "Indica que ya se ha asignado folio al mes 8")]
      public int? ASIG08 {get { return this._ASIG08; } set { this._ASIG08 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ASIG09
      /// </summary>
      [DataNames("ASIG09")] 
      [SugarColumn(ColumnName = "asig09", ColumnDescription = "Indica que ya se ha asignado folio al mes 9")]
      public int? ASIG09 {get { return this._ASIG09; } set { this._ASIG09 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ASIG10
      /// </summary>
      [DataNames("ASIG10")] 
      [SugarColumn(ColumnName = "asig10", ColumnDescription = "Indica que ya se ha asignado folio al mes 10")]
      public int? ASIG10 {get { return this._ASIG10; } set { this._ASIG10 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ASIG11
      /// </summary>
      [DataNames("ASIG11")] 
      [SugarColumn(ColumnName = "asig11", ColumnDescription = "Indica que ya se ha asignado folio al mes 11")]
      public int? ASIG11 {get { return this._ASIG11; } set { this._ASIG11 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ASIG12
      /// </summary>
      [DataNames("ASIG12")] 
      [SugarColumn(ColumnName = "asig12", ColumnDescription = "Indica que ya se ha asignado folio al mes 12")]
      public int? ASIG12 {get { return this._ASIG12; } set { this._ASIG12 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ASIG13
      /// </summary>
      [DataNames("ASIG13")] 
      [SugarColumn(ColumnName = "asig13", ColumnDescription = "Indica que ya se ha asignado folio al mes 13")]
      public int? ASIG13 {get { return this._ASIG13; } set { this._ASIG13 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ASIG14
      /// </summary>
      [DataNames("ASIG14")] 
      [SugarColumn(ColumnName = "asig14", ColumnDescription = "Indica que ya se ha asignado folio al mes 14")]
      public int? ASIG14 {get { return this._ASIG14; } set { this._ASIG14 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO14
      /// </summary>
      [DataNames("FOLIO14")] 
      [SugarColumn(ColumnName = "folio14", ColumnDescription = "Numero de folio para el mes 14", Length = 7)]
      public string FOLIO14 {get { return this._FOLIO14; } set { this._FOLIO14 = value; this.OnPropertyChanged(); }}
   }
}
