﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///DESIET
   ///</summary>
   [SugarTable("desiet")]
   public partial class DESIET
   {
      public DESIET(){
      }

      private string _TIPOPOL;
      private string _NUMPOL;
      private DateTime _FECHAPOL;
      private int _NUMPART;
      private int _INDICE;
      private double? _MONTO;
      private string _CUENTAGAS;
      private double? _MONTODED;
      private double? _PORCDED;
      private int? _IDCONCEP;

      /// <summary>
      /// obtener o establecer TIPOPOL
      /// </summary>
      [DataNames("TIPOPOL")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "tipopol", ColumnDescription = "Tipo", IsNullable = false, Length = 2)] 
      public string TIPOPOL {get { return this._TIPOPOL; } set { this._TIPOPOL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMPOL
      /// </summary>
      [DataNames("NUMPOL")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "numpol", ColumnDescription = "Numero", IsNullable = false, Length = 5)] 
      public string NUMPOL {get { return this._NUMPOL; } set { this._NUMPOL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHAPOL
      /// </summary>
      [DataNames("FECHAPOL")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "fechapol", ColumnDescription = "Fecha de registro", IsNullable = false)] 
      public DateTime FECHAPOL {get { return this._FECHAPOL; } set { this._FECHAPOL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMPART
      /// </summary>
      [DataNames("NUMPART")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "numpart", ColumnDescription = "Numero de partida", IsNullable = false)] 
      public int NUMPART {get { return this._NUMPART; } set { this._NUMPART = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer INDICE
      /// </summary>
      [DataNames("INDICE")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "indice", ColumnDescription = "Indice", IsNullable = false)] 
      public int INDICE {get { return this._INDICE; } set { this._INDICE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTO
      /// </summary>
      [DataNames("MONTO")]
      [SugarColumn(ColumnName = "monto", ColumnDescription = "Monto")] 
      public double? MONTO {get { return this._MONTO; } set { this._MONTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CUENTAGAS
      /// </summary>
      [DataNames("CUENTAGAS")]
      [SugarColumn(ColumnName = "cuentagas", ColumnDescription = "Numero de cuenta", Length = 21)] 
      public string CUENTAGAS {get { return this._CUENTAGAS; } set { this._CUENTAGAS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTODED
      /// </summary>
      [DataNames("MONTODED")]
      [SugarColumn(ColumnName = "montoded", ColumnDescription = "Monto de deduccion")] 
      public double? MONTODED {get { return this._MONTODED; } set { this._MONTODED = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PORCDED
      /// </summary>
      [DataNames("PORCDED")]
      [SugarColumn(ColumnName = "porcded", ColumnDescription = "Porcentaje de deduccion")] 
      public double? PORCDED {get { return this._PORCDED; } set { this._PORCDED = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IDCONCEP
      /// </summary>
      [DataNames("IDCONCEP")]
      [SugarColumn(ColumnName = "idconcep", ColumnDescription = "Identificador del concepto")] 
      public int? IDCONCEP {get { return this._IDCONCEP; } set { this._IDCONCEP = value; this.OnPropertyChanged(); }}
   }
}
