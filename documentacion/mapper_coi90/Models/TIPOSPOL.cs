﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///TIPOSPOL
   ///</summary>
   [SugarTable("tipospol")]
   public partial class TIPOSPOL
   {
      public TIPOSPOL(){
      }

      private string _TIPO;
      private string _DESCRIP;
      private int? _CLASSAT;

      /// <summary>
      /// obtener o establecer TIPO
      /// </summary>
      [DataNames("TIPO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "tipo", ColumnDescription = "Tipo", IsNullable = false, Length = 2)]
      public string TIPO {get { return this._TIPO; } set { this._TIPO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DESCRIP
      /// </summary>
      [DataNames("DESCRIP")]
      [SugarColumn(ColumnName = "descrip", ColumnDescription = "Descripcion", Length = 15)]
      public string DESCRIP {get { return this._DESCRIP; } set { this._DESCRIP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CLASSAT
      /// </summary>
      [DataNames("CLASSAT")]
      [SugarColumn(ColumnName = "classat", ColumnDescription = "Clasificacion fiscal")]
      public int? CLASSAT {get { return this._CLASSAT; } set { this._CLASSAT = value; this.OnPropertyChanged(); }}
   }
}
