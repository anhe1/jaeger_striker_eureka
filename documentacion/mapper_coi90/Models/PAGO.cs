﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///PAGO
   ///</summary>
   [SugarTable("pago")]
   public partial class PAGO
   {
      public PAGO(){
      }

      private int _IDPAGO;
      private string _UUIDTIMBREPAGO;
      private DateTime? _FECHAPAGO;
      private string _FORMAPAGOP;
      private string _MONEDAP;
      private double? _TIPOCAMBIOP;
      private double? _IMPORTE;
      private string _NUMOPERACION;
      private string _RFCEMISORCTAORD;
      private string _NOMBREBANCOORDEXT;
      private string _CTAORDENANTE;
      private string _RFCEMISORCTABEN;
      private string _CTABENEFICIARIO;
      private string _TIPOCADPAGO;
      private string _CERTPAGO;
      private string _CADPAGO;
      private string _SELLOPAGO;
      private string _VERSION;

      /// <summary>
      /// obtener o establecer IDPAGO
      /// </summary>
      [DataNames("IDPAGO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "idpago", ColumnDescription = "Id del pago incremental", IsNullable = false)]
      public int IDPAGO {get { return this._IDPAGO; } set { this._IDPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer UUIDTIMBREPAGO
      /// </summary>
      [DataNames("UUIDTIMBREPAGO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "uuidtimbrepago", ColumnDescription = "id de complementos de pago", IsNullable = false, Length = 36)]
      public string UUIDTIMBREPAGO {get { return this._UUIDTIMBREPAGO; } set { this._UUIDTIMBREPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHAPAGO
      /// </summary>
      [DataNames("FECHAPAGO")]
      [SugarColumn(ColumnName = "fechapago", ColumnDescription = "Fecha del pago")]
      public DateTime? FECHAPAGO {get { return this._FECHAPAGO; } set { this._FECHAPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FORMAPAGOP
      /// </summary>
      [DataNames("FORMAPAGOP")]
      [SugarColumn(ColumnName = "formapagop", ColumnDescription = "Forma de pago del complemento", Length = 3)]
      public string FORMAPAGOP {get { return this._FORMAPAGOP; } set { this._FORMAPAGOP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONEDAP
      /// </summary>
      [DataNames("MONEDAP")]
      [SugarColumn(ColumnName = "monedap", ColumnDescription = "Moneda", Length = 3)]
      public string MONEDAP {get { return this._MONEDAP; } set { this._MONEDAP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOCAMBIOP
      /// </summary>
      [DataNames("TIPOCAMBIOP")]
      [SugarColumn(ColumnName = "tipocambiop", ColumnDescription = "Tipo de cambio")]
      public double? TIPOCAMBIOP {get { return this._TIPOCAMBIOP; } set { this._TIPOCAMBIOP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IMPORTE
      /// </summary>
      [DataNames("IMPORTE")]
      [SugarColumn(ColumnName = "importe", ColumnDescription = "Importe")]
      public double? IMPORTE {get { return this._IMPORTE; } set { this._IMPORTE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMOPERACION
      /// </summary>
      [DataNames("NUMOPERACION")]
      [SugarColumn(ColumnName = "numoperacion", ColumnDescription = "Numero de operacion", Length = 50)]
      public string NUMOPERACION {get { return this._NUMOPERACION; } set { this._NUMOPERACION = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFCEMISORCTAORD
      /// </summary>
      [DataNames("RFCEMISORCTAORD")]
      [SugarColumn(ColumnName = "rfcemisorctaord", ColumnDescription = "RFC emisor de la cuenta ordenante", Length = 40)]
      public string RFCEMISORCTAORD {get { return this._RFCEMISORCTAORD; } set { this._RFCEMISORCTAORD = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOMBREBANCOORDEXT
      /// </summary>
      [DataNames("NOMBREBANCOORDEXT")]
      [SugarColumn(ColumnName = "nombrebancoordext", ColumnDescription = "Nombre del banco extranjero", Length = 300)]
      public string NOMBREBANCOORDEXT {get { return this._NOMBREBANCOORDEXT; } set { this._NOMBREBANCOORDEXT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTAORDENANTE
      /// </summary>
      [DataNames("CTAORDENANTE")]
      [SugarColumn(ColumnName = "ctaordenante", ColumnDescription = "Cuenta ordenante", Length = 50)]
      public string CTAORDENANTE {get { return this._CTAORDENANTE; } set { this._CTAORDENANTE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFCEMISORCTABEN
      /// </summary>
      [DataNames("RFCEMISORCTABEN")]
      [SugarColumn(ColumnName = "rfcemisorctaben", ColumnDescription = "RFC emisor de la cuenta beneficiaria", Length = 40)]
      public string RFCEMISORCTABEN {get { return this._RFCEMISORCTABEN; } set { this._RFCEMISORCTABEN = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTABENEFICIARIO
      /// </summary>
      [DataNames("CTABENEFICIARIO")]
      [SugarColumn(ColumnName = "ctabeneficiario", ColumnDescription = "Cuenta beneficiaria", Length = 50)]
      public string CTABENEFICIARIO {get { return this._CTABENEFICIARIO; } set { this._CTABENEFICIARIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOCADPAGO
      /// </summary>
      [DataNames("TIPOCADPAGO")]
      [SugarColumn(ColumnName = "tipocadpago", ColumnDescription = "Registra la clave del tipo de cadena de pago que genera la entidad receptora del pago", Length = 50)]
      public string TIPOCADPAGO {get { return this._TIPOCADPAGO; } set { this._TIPOCADPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CERTPAGO
      /// </summary>
      [DataNames("CERTPAGO")]
      [SugarColumn(ColumnName = "certpago", ColumnDescription = "Certificado que corresponde al pago", Length = 50)]
      public string CERTPAGO {get { return this._CERTPAGO; } set { this._CERTPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CADPAGO
      /// </summary>
      [DataNames("CADPAGO")]
      [SugarColumn(ColumnName = "cadpago", ColumnDescription = "Cadena original del comprobante de pago generado por la entidad emisora de la cuenta beneficiaria", Length = 100)]
      public string CADPAGO {get { return this._CADPAGO; } set { this._CADPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SELLOPAGO
      /// </summary>
      [DataNames("SELLOPAGO")]
      [SugarColumn(ColumnName = "sellopago", ColumnDescription = "Sello digital que se asocie al pago", Length = 50)]
      public string SELLOPAGO {get { return this._SELLOPAGO; } set { this._SELLOPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer VERSION
      /// </summary>
      [DataNames("VERSION")]
      [SugarColumn(ColumnName = "version", ColumnDescription = "Version", Length = 50)]
      public string VERSION {get { return this._VERSION; } set { this._VERSION = value; this.OnPropertyChanged(); }}
   }
}
