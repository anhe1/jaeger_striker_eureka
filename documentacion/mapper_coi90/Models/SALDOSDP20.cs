﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///SALDOSDP20
   ///</summary>
   [SugarTable("saldosdp20")]
   public partial class SALDOSDP20
   {
      public SALDOSDP20(){
      }
      
      private string _NUM_CTA;
      private int _EJERCICIO;
      private int _DEPTO;
      private double? _INICIAL;
      private double? _INICIALEX;
      private double? _CARGO01;
      private double? _ABONO01;
      private double? _CARGOEX01;
      private double? _ABONOEX01;
      private double? _CARGO02;
      private double? _ABONO02;
      private double? _CARGOEX02;
      private double? _ABONOEX02;
      private double? _CARGO03;
      private double? _ABONO03;
      private double? _CARGOEX03;
      private double? _ABONOEX03;
      private double? _CARGO04;
      private double? _ABONO04;
      private double? _CARGOEX04;
      private double? _ABONOEX04;
      private double? _CARGO05;
      private double? _ABONO05;
      private double? _CARGOEX05;
      private double? _ABONOEX05;
      private double? _CARGO06;
      private double? _ABONO06;
      private double? _CARGOEX06;
      private double? _ABONOEX06;
      private double? _CARGO07;
      private double? _ABONO07;
      private double? _CARGOEX07;
      private double? _ABONOEX07;
      private double? _CARGO08;
      private double? _ABONO08;
      private double? _CARGOEX08;
      private double? _ABONOEX08;
      private double? _CARGO09;
      private double? _ABONO09;
      private double? _CARGOEX09;
      private double? _ABONOEX09;
      private double? _CARGO10;
      private double? _ABONO10;
      private double? _CARGOEX10;
      private double? _ABONOEX10;
      private double? _CARGO11;
      private double? _ABONO11;
      private double? _CARGOEX11;
      private double? _ABONOEX11;
      private double? _CARGO12;
      private double? _ABONO12;
      private double? _CARGOEX12;
      private double? _ABONOEX12;
      private double? _CARGO13;
      private double? _ABONO13;
      private double? _CARGOEX13;
      private double? _ABONOEX13;
      private double? _CARGO14;
      private double? _ABONO14;
      private double? _CARGOEX14;
      private double? _ABONOEX14;
      private int? _MOV01;
      private int? _MOV02;
      private int? _MOV03;
      private int? _MOV04;
      private int? _MOV05;
      private int? _MOV06;
      private int? _MOV07;
      private int? _MOV08;
      private int? _MOV09;
      private int? _MOV10;
      private int? _MOV11;
      private int? _MOV12;
      private int? _MOV13;
      private int? _MOV14;

      /// <summary>
      /// obtener o establecer NUM_CTA
      /// </summary>
      [DataNames("NUM_CTA")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "num_cta", ColumnDescription = "Numero de cuenta", IsNullable = false, Length = 21)]
      public string NUM_CTA {get { return this._NUM_CTA; } set { this._NUM_CTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer EJERCICIO
      /// </summary>
      [DataNames("EJERCICIO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "ejercicio", ColumnDescription = "Ejercicio", IsNullable = false)]
      public int EJERCICIO {get { return this._EJERCICIO; } set { this._EJERCICIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DEPTO
      /// </summary>
      [DataNames("DEPTO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "depto", ColumnDescription = "Departamento", IsNullable = false)]
      public int DEPTO {get { return this._DEPTO; } set { this._DEPTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer INICIAL
      /// </summary>
      [DataNames("INICIAL")]
      [SugarColumn(ColumnName = "inicial", ColumnDescription = "Saldo incial")]
      public double? INICIAL {get { return this._INICIAL; } set { this._INICIAL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer INICIALEX
      /// </summary>
      [DataNames("INICIALEX")]
      [SugarColumn(ColumnName = "inicialex", ColumnDescription = "Saldo inicial extranjero", DefaultValue = 0)]
      public double? INICIALEX {get { return this._INICIALEX; } set { this._INICIALEX = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGO01
      /// </summary>
      [DataNames("CARGO01")]
      [SugarColumn(ColumnName = "cargo01", ColumnDescription = "Cargo para el mes 1", DefaultValue = 0)]
      public double? CARGO01 {get { return this._CARGO01; } set { this._CARGO01 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONO01
      /// </summary>
      [DataNames("ABONO01")]
      [SugarColumn(ColumnName = "abono01", ColumnDescription = "Abono para el mes 1", DefaultValue = 0)]
      public double? ABONO01 {get { return this._ABONO01; } set { this._ABONO01 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGOEX01
      /// </summary>
      [DataNames("CARGOEX01")]
      [SugarColumn(ColumnName = "cargoex01", ColumnDescription = "Cargo extranjero para el mes 1", DefaultValue = 0)]
      public double? CARGOEX01 {get { return this._CARGOEX01; } set { this._CARGOEX01 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONOEX01
      /// </summary>
      [DataNames("ABONOEX01")]
      [SugarColumn(ColumnName = "abonoex01", ColumnDescription = "Abono extranjero para el mes 1", DefaultValue = 0)]
      public double? ABONOEX01 {get { return this._ABONOEX01; } set { this._ABONOEX01 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGO02
      /// </summary>
      [DataNames("CARGO02")]
      [SugarColumn(ColumnName = "cargo02", ColumnDescription = "Cargo para el mes 2", DefaultValue = 0)]
      public double? CARGO02 {get { return this._CARGO02; } set { this._CARGO02 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONO02
      /// </summary>
      [DataNames("ABONO02")]
      [SugarColumn(ColumnName = "abono02", ColumnDescription = "Abono para el mes 2", DefaultValue = 0)]
      public double? ABONO02 {get { return this._ABONO02; } set { this._ABONO02 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGOEX02
      /// </summary>
      [DataNames("CARGOEX02")]
      [SugarColumn(ColumnName = "cargoex02", ColumnDescription = "Cargo extranjero para el mes 2", DefaultValue = 0)]
      public double? CARGOEX02 {get { return this._CARGOEX02; } set { this._CARGOEX02 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONOEX02
      /// </summary>
      [DataNames("ABONOEX02")]
      [SugarColumn(ColumnName = "abonoex02", ColumnDescription = "Abono extranjero para el mes 2", DefaultValue = 0)]
      public double? ABONOEX02 {get { return this._ABONOEX02; } set { this._ABONOEX02 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGO03
      /// </summary>
      [DataNames("CARGO03")]
      [SugarColumn(ColumnName = "cargo03", ColumnDescription = "Cargo para el mes 3", DefaultValue = 0)]
      public double? CARGO03 {get { return this._CARGO03; } set { this._CARGO03 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONO03
      /// </summary>
      [DataNames("ABONO03")]
      [SugarColumn(ColumnName = "abono03", ColumnDescription = "Abono para el mes 3", DefaultValue = 0)]
      public double? ABONO03 {get { return this._ABONO03; } set { this._ABONO03 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGOEX03
      /// </summary>
      [DataNames("CARGOEX03")]
      [SugarColumn(ColumnName = "cargoex03", ColumnDescription = "Cargo extranjero para el mes 3", DefaultValue = 0)]
      public double? CARGOEX03 {get { return this._CARGOEX03; } set { this._CARGOEX03 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONOEX03
      /// </summary>
      [DataNames("ABONOEX03")]
      [SugarColumn(ColumnName = "abonoex03", ColumnDescription = "Abono extranjero para el mes 3", DefaultValue = 0)]
      public double? ABONOEX03 {get { return this._ABONOEX03; } set { this._ABONOEX03 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGO04
      /// </summary>
      [DataNames("CARGO04")]
      [SugarColumn(ColumnName = "cargo04", ColumnDescription = "Cargo para el mes 4", DefaultValue = 0)]
      public double? CARGO04 {get { return this._CARGO04; } set { this._CARGO04 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONO04
      /// </summary>
      [DataNames("ABONO04")]
      [SugarColumn(ColumnName = "abono04", ColumnDescription = "Abono para el mes 4", DefaultValue = 0)]
      public double? ABONO04 {get { return this._ABONO04; } set { this._ABONO04 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGOEX04
      /// </summary>
      [DataNames("CARGOEX04")]
      [SugarColumn(ColumnName = "cargoex04", ColumnDescription = "Cargo extranjero para el mes 4", DefaultValue = 0)]
      public double? CARGOEX04 {get { return this._CARGOEX04; } set { this._CARGOEX04 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONOEX04
      /// </summary>
      [DataNames("ABONOEX04")]
      [SugarColumn(ColumnName = "abonoex04", ColumnDescription = "Abono extranjero para el mes 4", DefaultValue = 0)]
      public double? ABONOEX04 {get { return this._ABONOEX04; } set { this._ABONOEX04 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGO05
      /// </summary>
      [DataNames("CARGO05")]
      [SugarColumn(ColumnName = "cargo05", ColumnDescription = "Cargo para el mes 5", DefaultValue = 0)]
      public double? CARGO05 {get { return this._CARGO05; } set { this._CARGO05 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONO05
      /// </summary>
      [DataNames("ABONO05")]
      [SugarColumn(ColumnName = "abono05", ColumnDescription = "Abono para el mes 5", DefaultValue = 0)]
      public double? ABONO05 {get { return this._ABONO05; } set { this._ABONO05 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGOEX05
      /// </summary>
      [DataNames("CARGOEX05")]
      [SugarColumn(ColumnName = "cargoex05", ColumnDescription = "Cargo extranjero para el mes 5", DefaultValue = 0)]
      public double? CARGOEX05 {get { return this._CARGOEX05; } set { this._CARGOEX05 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONOEX05
      /// </summary>
      [DataNames("ABONOEX05")]
      [SugarColumn(ColumnName = "abonoex05", ColumnDescription = "Abono extranjero para el mes 5", DefaultValue = 0)]
      public double? ABONOEX05 {get { return this._ABONOEX05; } set { this._ABONOEX05 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGO06
      /// </summary>
      [DataNames("CARGO06")]
      [SugarColumn(ColumnName = "cargo06", ColumnDescription = "Cargo para el mes 6", DefaultValue = 0)]
      public double? CARGO06 {get { return this._CARGO06; } set { this._CARGO06 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONO06
      /// </summary>
      [DataNames("ABONO06")]
      [SugarColumn(ColumnName = "abono06", ColumnDescription = "Abono para el mes 6", DefaultValue = 0)]
      public double? ABONO06 {get { return this._ABONO06; } set { this._ABONO06 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGOEX06
      /// </summary>
      [DataNames("CARGOEX06")]
      [SugarColumn(ColumnName = "cargoex06", ColumnDescription = "Cargo extranjero para el mes 6", DefaultValue = 0)]
      public double? CARGOEX06 {get { return this._CARGOEX06; } set { this._CARGOEX06 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONOEX06
      /// </summary>
      [DataNames("ABONOEX06")]
      [SugarColumn(ColumnName = "abonoex06", ColumnDescription = "Abono extranjero para el mes 6", DefaultValue = 0)]
      public double? ABONOEX06 {get { return this._ABONOEX06; } set { this._ABONOEX06 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGO07
      /// </summary>
      [DataNames("CARGO07")]
      [SugarColumn(ColumnName = "cargo07", ColumnDescription = "Cargo para el mes 7", DefaultValue = 0)]
      public double? CARGO07 {get { return this._CARGO07; } set { this._CARGO07 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONO07
      /// </summary>
      [DataNames("ABONO07")]
      [SugarColumn(ColumnName = "abono07", ColumnDescription = "Abono para el mes 7", DefaultValue = 0)]
      public double? ABONO07 {get { return this._ABONO07; } set { this._ABONO07 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGOEX07
      /// </summary>
      [DataNames("CARGOEX07")]
      [SugarColumn(ColumnName = "cargoex07", ColumnDescription = "Cargo extranjero para el mes 7", DefaultValue = 0)]
      public double? CARGOEX07 {get { return this._CARGOEX07; } set { this._CARGOEX07 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONOEX07
      /// </summary>
      [DataNames("ABONOEX07")]
      [SugarColumn(ColumnName = "abonoex07", ColumnDescription = "Abono extranjero para el mes 7", DefaultValue = 0)]
      public double? ABONOEX07 {get { return this._ABONOEX07; } set { this._ABONOEX07 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGO08
      /// </summary>
      [DataNames("CARGO08")]
      [SugarColumn(ColumnName = "cargo08", ColumnDescription = "Cargo para el mes 8", DefaultValue = 0)]
      public double? CARGO08 {get { return this._CARGO08; } set { this._CARGO08 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONO08
      /// </summary>
      [DataNames("ABONO08")]
      [SugarColumn(ColumnName = "abono08", ColumnDescription = "Abono para el mes 8", DefaultValue = 0)]
      public double? ABONO08 {get { return this._ABONO08; } set { this._ABONO08 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGOEX08
      /// </summary>
      [DataNames("CARGOEX08")]
      [SugarColumn(ColumnName = "cargoex08", ColumnDescription = "Cargo extranjero para el mes 8", DefaultValue = 0)]
      public double? CARGOEX08 {get { return this._CARGOEX08; } set { this._CARGOEX08 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONOEX08
      /// </summary>
      [DataNames("ABONOEX08")]
      [SugarColumn(ColumnName = "abonoex08", ColumnDescription = "Abono extranjero para el mes 8", DefaultValue = 0)]
      public double? ABONOEX08 {get { return this._ABONOEX08; } set { this._ABONOEX08 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGO09
      /// </summary>
      [DataNames("CARGO09")]
      [SugarColumn(ColumnName = "cargo09", ColumnDescription = "Cargo para el mes 9", DefaultValue = 0)]
      public double? CARGO09 {get { return this._CARGO09; } set { this._CARGO09 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONO09
      /// </summary>
      [DataNames("ABONO09")]
      [SugarColumn(ColumnName = "abono09", ColumnDescription = "Abono para el mes 9", DefaultValue = 0)]
      public double? ABONO09 {get { return this._ABONO09; } set { this._ABONO09 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGOEX09
      /// </summary>
      [DataNames("CARGOEX09")]
      [SugarColumn(ColumnName = "cargoex09", ColumnDescription = "Cargo extranjero para el mes 9", DefaultValue = 0)]
      public double? CARGOEX09 {get { return this._CARGOEX09; } set { this._CARGOEX09 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONOEX09
      /// </summary>
      [DataNames("ABONOEX09")]
      [SugarColumn(ColumnName = "abonoex09", ColumnDescription = "Abono extranjero para el mes 9", DefaultValue = 0)]
      public double? ABONOEX09 {get { return this._ABONOEX09; } set { this._ABONOEX09 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGO10
      /// </summary>
      [DataNames("CARGO10")]
      [SugarColumn(ColumnName = "cargo10", ColumnDescription = "Cargo para el mes 10", DefaultValue = 0)]
      public double? CARGO10 {get { return this._CARGO10; } set { this._CARGO10 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONO10
      /// </summary>
      [DataNames("ABONO10")]
      [SugarColumn(ColumnName = "abono10", ColumnDescription = "Abono para el mes 10", DefaultValue = 0)]
      public double? ABONO10 {get { return this._ABONO10; } set { this._ABONO10 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGOEX10
      /// </summary>
      [DataNames("CARGOEX10")]
      [SugarColumn(ColumnName = "cargoex10", ColumnDescription = "Cargo extranjero para el mes 10", DefaultValue = 0)]
      public double? CARGOEX10 {get { return this._CARGOEX10; } set { this._CARGOEX10 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONOEX10
      /// </summary>
      [DataNames("ABONOEX10")]
      [SugarColumn(ColumnName = "abonoex10", ColumnDescription = "Abono extranjero para el mes 10", DefaultValue = 0)]
      public double? ABONOEX10 {get { return this._ABONOEX10; } set { this._ABONOEX10 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGO11
      /// </summary>
      [DataNames("CARGO11")]
      [SugarColumn(ColumnName = "cargo11", ColumnDescription = "Cargo para el mes 11", DefaultValue = 0)]
      public double? CARGO11 {get { return this._CARGO11; } set { this._CARGO11 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONO11
      /// </summary>
      [DataNames("ABONO11")]
      [SugarColumn(ColumnName = "abono11", ColumnDescription = "Abono para el mes 11", DefaultValue = 0)]
      public double? ABONO11 {get { return this._ABONO11; } set { this._ABONO11 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGOEX11
      /// </summary>
      [DataNames("CARGOEX11")]
      [SugarColumn(ColumnName = "cargoex11", ColumnDescription = "Cargo extranjero para el mes 11", DefaultValue = 0)]
      public double? CARGOEX11 {get { return this._CARGOEX11; } set { this._CARGOEX11 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONOEX11
      /// </summary>
      [DataNames("ABONOEX11")]
      [SugarColumn(ColumnName = "abonoex11", ColumnDescription = "Abono extranjero para el mes 11", DefaultValue = 0)]
      public double? ABONOEX11 {get { return this._ABONOEX11; } set { this._ABONOEX11 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGO12
      /// </summary>
      [DataNames("CARGO12")]
      [SugarColumn(ColumnName = "cargo12", ColumnDescription = "Cargo para el mes 12", DefaultValue = 0)]
      public double? CARGO12 {get { return this._CARGO12; } set { this._CARGO12 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONO12
      /// </summary>
      [DataNames("ABONO12")]
      [SugarColumn(ColumnName = "abono12", ColumnDescription = "Abono para el mes 12", DefaultValue = 0)]
      public double? ABONO12 {get { return this._ABONO12; } set { this._ABONO12 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGOEX12
      /// </summary>
      [DataNames("CARGOEX12")]
      [SugarColumn(ColumnName = "cargoex12", ColumnDescription = "Cargo extranjero para el mes 12", DefaultValue = 0)]
      public double? CARGOEX12 {get { return this._CARGOEX12; } set { this._CARGOEX12 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONOEX12
      /// </summary>
      [DataNames("ABONOEX12")]
      [SugarColumn(ColumnName = "abonoex12", ColumnDescription = "Abono extranjero para el mes 12", DefaultValue = 0)]
      public double? ABONOEX12 {get { return this._ABONOEX12; } set { this._ABONOEX12 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGO13
      /// </summary>
      [DataNames("CARGO13")]
      [SugarColumn(ColumnName = "cargo13", ColumnDescription = "Cargo para el mes 13", DefaultValue = 0)]
      public double? CARGO13 {get { return this._CARGO13; } set { this._CARGO13 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONO13
      /// </summary>
      [DataNames("ABONO13")]
      [SugarColumn(ColumnName = "abono13", ColumnDescription = "Abono para el mes 13", DefaultValue = 0)]
      public double? ABONO13 {get { return this._ABONO13; } set { this._ABONO13 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGOEX13
      /// </summary>
      [DataNames("CARGOEX13")]
      [SugarColumn(ColumnName = "cargoex13", ColumnDescription = "Cargo extranjero para el mes 13", DefaultValue = 0)]
      public double? CARGOEX13 {get { return this._CARGOEX13; } set { this._CARGOEX13 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONOEX13
      /// </summary>
      [DataNames("ABONOEX13")]
      [SugarColumn(ColumnName = "abonoex13", ColumnDescription = "Abono extranjero para el mes 13", DefaultValue = 0)]
      public double? ABONOEX13 {get { return this._ABONOEX13; } set { this._ABONOEX13 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGO14
      /// </summary>
      [DataNames("CARGO14")]
      [SugarColumn(ColumnName = "cargo14", ColumnDescription = "Cargo para el mes 14", DefaultValue = 0)]
      public double? CARGO14 {get { return this._CARGO14; } set { this._CARGO14 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONO14
      /// </summary>
      [DataNames("ABONO14")]
      [SugarColumn(ColumnName = "abono14", ColumnDescription = "Abono para el mes 14", DefaultValue = 0)]
      public double? ABONO14 {get { return this._ABONO14; } set { this._ABONO14 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGOEX14
      /// </summary>
      [DataNames("CARGOEX14")]
      [SugarColumn(ColumnName = "cargoex14", ColumnDescription = "Cargo extranjero para el mes 14", DefaultValue = 0)]
      public double? CARGOEX14 {get { return this._CARGOEX14; } set { this._CARGOEX14 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONOEX14
      /// </summary>
      [DataNames("ABONOEX14")]
      [SugarColumn(ColumnName = "abonoex14", ColumnDescription = "Abono extranjero para el mes 14", DefaultValue = 0)]
      public double? ABONOEX14 {get { return this._ABONOEX14; } set { this._ABONOEX14 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOV01
      /// </summary>
      [DataNames("MOV01")]
      [SugarColumn(ColumnName = "mov01", ColumnDescription = "Movimiento para el mes 1", DefaultValue = 0)]
      public int? MOV01 {get { return this._MOV01; } set { this._MOV01 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOV02
      /// </summary>
      [DataNames("MOV02")]
      [SugarColumn(ColumnName = "mov02", ColumnDescription = "Movimiento para el mes 2", DefaultValue = 0)]
      public int? MOV02 {get { return this._MOV02; } set { this._MOV02 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOV03
      /// </summary>
      [DataNames("MOV03")]
      [SugarColumn(ColumnName = "mov03", ColumnDescription = "Movimiento para el mes 3", DefaultValue = 0)]
      public int? MOV03 {get { return this._MOV03; } set { this._MOV03 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOV04
      /// </summary>
      [DataNames("MOV04")]
      [SugarColumn(ColumnName = "mov04", ColumnDescription = "Movimiento para el mes 4", DefaultValue = 0)]
      public int? MOV04 {get { return this._MOV04; } set { this._MOV04 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOV05
      /// </summary>
      [DataNames("MOV05")]
      [SugarColumn(ColumnName = "mov05", ColumnDescription = "Movimiento para el mes 5", DefaultValue = 0)]
      public int? MOV05 {get { return this._MOV05; } set { this._MOV05 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOV06
      /// </summary>
      [DataNames("MOV06")]
      [SugarColumn(ColumnName = "mov06", ColumnDescription = "Movimiento para el mes 6", DefaultValue = 0)]
      public int? MOV06 {get { return this._MOV06; } set { this._MOV06 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOV07
      /// </summary>
      [DataNames("MOV07")]
      [SugarColumn(ColumnName = "mov07", ColumnDescription = "Movimiento para el mes 7", DefaultValue = 0)]
      public int? MOV07 {get { return this._MOV07; } set { this._MOV07 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOV08
      /// </summary>
      [DataNames("MOV08")]
      [SugarColumn(ColumnName = "mov08", ColumnDescription = "Movimiento para el mes 8", DefaultValue = 0)]
      public int? MOV08 {get { return this._MOV08; } set { this._MOV08 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOV09
      /// </summary>
      [DataNames("MOV09")]
      [SugarColumn(ColumnName = "mov09", ColumnDescription = "Movimiento para el mes 9", DefaultValue = 0)]
      public int? MOV09 {get { return this._MOV09; } set { this._MOV09 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOV10
      /// </summary>
      [DataNames("MOV10")]
      [SugarColumn(ColumnName = "mov10", ColumnDescription = "Movimiento para el mes 10", DefaultValue = 0)]
      public int? MOV10 {get { return this._MOV10; } set { this._MOV10 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOV11
      /// </summary>
      [DataNames("MOV11")]
      [SugarColumn(ColumnName = "mov11", ColumnDescription = "Movimiento para el mes 11", DefaultValue = 0)]
      public int? MOV11 {get { return this._MOV11; } set { this._MOV11 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOV12
      /// </summary>
      [DataNames("MOV12")]
      [SugarColumn(ColumnName = "mov12", ColumnDescription = "Movimiento para el mes 12", DefaultValue = 0)]
      public int? MOV12 {get { return this._MOV12; } set { this._MOV12 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOV13
      /// </summary>
      [DataNames("MOV13")]
      [SugarColumn(ColumnName = "mov13", ColumnDescription = "Movimiento para el mes 13", DefaultValue = 0)]
      public int? MOV13 {get { return this._MOV13; } set { this._MOV13 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOV14
      /// </summary>
      [DataNames("MOV14")]
      [SugarColumn(ColumnName = "mov14", ColumnDescription = "Movimiento para el mes 14", DefaultValue = 0)]
      public int? MOV14 {get { return this._MOV14; } set { this._MOV14 = value; this.OnPropertyChanged(); }}
   }
}
