﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///LISTANEGRARFC
   ///</summary>
   [SugarTable("listanegrarfc")]
   public partial class LISTANEGRARFC
   {
      public LISTANEGRARFC(){
      }

      private string _RFC;
      private string _SITUACIONCONTRIBUYENTE;
      private DateTime? _PBPAGSAT;

      /// <summary>
      /// obtener o establecer RFC
      /// </summary>
      [DataNames("RFC")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "rfc", ColumnDescription = "RFC", IsNullable = false)]
      public string RFC {get { return this._RFC; } set { this._RFC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SITUACIONCONTRIBUYENTE
      /// </summary>
      [DataNames("SITUACIONCONTRIBUYENTE")] 
      [SugarColumn(ColumnName = "situacioncontribuyente", ColumnDescription = "Situacion del contribuyente")]
      public string SITUACIONCONTRIBUYENTE {get { return this._SITUACIONCONTRIBUYENTE; } set { this._SITUACIONCONTRIBUYENTE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PBPAGSAT
      /// </summary>
      [DataNames("PBPAGSAT")] 
      [SugarColumn(ColumnName = "pbpagsat", ColumnDescription = "Publicacion pagina SAT")]
      public DateTime? PBPAGSAT {get { return this._PBPAGSAT; } set { this._PBPAGSAT = value; this.OnPropertyChanged(); }}
   }
}
