﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///PRESUP20
   ///</summary>
   [SugarTable("presup20")]
   public partial class PRESUP20
   {
      public PRESUP20(){
      }

      private int _EJERCICIO;
      private string _NUM_CTA;
      private double? _PRESUP01;
      private double? _PRESUP02;
      private double? _PRESUP03;
      private double? _PRESUP04;
      private double? _PRESUP05;
      private double? _PRESUP06;
      private double? _PRESUP07;
      private double? _PRESUP08;
      private double? _PRESUP09;
      private double? _PRESUP10;
      private double? _PRESUP11;
      private double? _PRESUP12;
      private double? _PRESUP13;
      private double? _PRESUP14;

      /// <summary>
      /// obtener o establecer EJERCICIO
      /// </summary>
      [DataNames("EJERCICIO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "ejercicio", ColumnDescription = "Ejercicio", IsNullable = false)]
      public int EJERCICIO {get { return this._EJERCICIO; } set { this._EJERCICIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_CTA
      /// </summary>
      [DataNames("NUM_CTA")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "num_cta", ColumnDescription = "Numero de cuenta", IsNullable = false, Length = 21)]
      public string NUM_CTA {get { return this._NUM_CTA; } set { this._NUM_CTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRESUP01
      /// </summary>
      [DataNames("PRESUP01")]
      [SugarColumn(ColumnName = "presup01", ColumnDescription = "Presupuesto para el mes 1", DefaultValue = 0)]
      public double? PRESUP01 {get { return this._PRESUP01; } set { this._PRESUP01 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRESUP02
      /// </summary>
      [DataNames("PRESUP02")]
      [SugarColumn(ColumnName = "presup02", ColumnDescription = "Presupuesto para el mes 2", DefaultValue = 0)]
      public double? PRESUP02 {get { return this._PRESUP02; } set { this._PRESUP02 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRESUP03
      /// </summary>
      [DataNames("PRESUP03")]
      [SugarColumn(ColumnName = "presup03", ColumnDescription = "Presupuesto para el mes 3", DefaultValue = 0)]
      public double? PRESUP03 {get { return this._PRESUP03; } set { this._PRESUP03 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRESUP04
      /// </summary>
      [DataNames("PRESUP04")]
      [SugarColumn(ColumnName = "presup04", ColumnDescription = "Presupuesto para el mes 4", DefaultValue = 0)]
      public double? PRESUP04 {get { return this._PRESUP04; } set { this._PRESUP04 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRESUP05
      /// </summary>
      [DataNames("PRESUP05")]
      [SugarColumn(ColumnName = "presup05", ColumnDescription = "Presupuesto para el mes 5", DefaultValue = 0)]
      public double? PRESUP05 {get { return this._PRESUP05; } set { this._PRESUP05 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRESUP06
      /// </summary>
      [DataNames("PRESUP06")]
      [SugarColumn(ColumnName = "presup06", ColumnDescription = "Presupuesto para el mes 6", DefaultValue = 0)]
      public double? PRESUP06 {get { return this._PRESUP06; } set { this._PRESUP06 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRESUP07
      /// </summary>
      [DataNames("PRESUP07")]
      [SugarColumn(ColumnName = "presup07", ColumnDescription = "Presupuesto para el mes 7", DefaultValue = 0)]
      public double? PRESUP07 {get { return this._PRESUP07; } set { this._PRESUP07 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRESUP08
      /// </summary>
      [DataNames("PRESUP08")]
      [SugarColumn(ColumnName = "presup08", ColumnDescription = "Presupuesto para el mes 8", DefaultValue = 0)]
      public double? PRESUP08 {get { return this._PRESUP08; } set { this._PRESUP08 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRESUP09
      /// </summary>
      [DataNames("PRESUP09")]
      [SugarColumn(ColumnName = "presup09", ColumnDescription = "Presupuesto para el mes 9", DefaultValue = 0)]
      public double? PRESUP09 {get { return this._PRESUP09; } set { this._PRESUP09 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRESUP10
      /// </summary>
      [DataNames("PRESUP10")]
      [SugarColumn(ColumnName = "presup10", ColumnDescription = "Presupuesto para el mes 10", DefaultValue = 0)]
      public double? PRESUP10 {get { return this._PRESUP10; } set { this._PRESUP10 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRESUP11
      /// </summary>
      [DataNames("PRESUP11")]
      [SugarColumn(ColumnName = "presup11", ColumnDescription = "Presupuesto para el mes 11", DefaultValue = 0)]
      public double? PRESUP11 {get { return this._PRESUP11; } set { this._PRESUP11 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRESUP12
      /// </summary>
      [DataNames("PRESUP12")]
      [SugarColumn(ColumnName = "presup12", ColumnDescription = "Presupuesto para el mes 12", DefaultValue = 0)]
      public double? PRESUP12 {get { return this._PRESUP12; } set { this._PRESUP12 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRESUP13
      /// </summary>
      [DataNames("PRESUP13")]
      [SugarColumn(ColumnName = "presup13", ColumnDescription = "Presupuesto para el mes 13", DefaultValue = 0)]
      public double? PRESUP13 {get { return this._PRESUP13; } set { this._PRESUP13 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRESUP14
      /// </summary>
      [DataNames("PRESUP14")]
      [SugarColumn(ColumnName = "presup14", ColumnDescription = "Presupuesto para el mes 14", DefaultValue = 0)]
      public double? PRESUP14 {get { return this._PRESUP14; } set { this._PRESUP14 = value; this.OnPropertyChanged(); }}
   }
}
