﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///CTATER
   ///</summary>
   [SugarTable("ctater")]
   public partial class CTATER
   {
      public CTATER(){
      }

      private string _CUENTA;
      private string _TIPO;
      private string _RFCIDFISC;
      private double? _IVADEFAULT;
      private double? _PORCENTAJEIVA;
      private double? _PORCENTAJEISR;
      private int? _INCLUYEIVA;
      private int? _IDCONCEP;
      private int? _IDCONCEPIVAA;

      /// <summary>
      /// obtener o establecer CUENTA
      /// </summary>
      [DataNames("CUENTA")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "cuenta", ColumnDescription = "Numero de cuenta", IsNullable = false, Length = 21)]
      public string CUENTA {get { return this._CUENTA; } set { this._CUENTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPO
      /// </summary>
      [DataNames("TIPO")]
      [SugarColumn(ColumnName = "tipo", ColumnDescription = "Tipo", Length = 1)]
      public string TIPO {get { return this._TIPO; } set { this._TIPO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFCIDFISC
      /// </summary>
      [DataNames("RFCIDFISC")]
      [SugarColumn(ColumnName = "rfcidfisc", ColumnDescription = "RFC fiscal", Length = 40)]
      public string RFCIDFISC {get { return this._RFCIDFISC; } set { this._RFCIDFISC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IVADEFAULT
      /// </summary>
      [DataNames("IVADEFAULT")]
      [SugarColumn(ColumnName = "ivadefault", ColumnDescription = "Iva default")]
      public double? IVADEFAULT {get { return this._IVADEFAULT; } set { this._IVADEFAULT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PORCENTAJEIVA
      /// </summary>
      [DataNames("PORCENTAJEIVA")]
      [SugarColumn(ColumnName = "porcentajeiva", ColumnDescription = "Porcentaje de IVA")]
      public double? PORCENTAJEIVA {get { return this._PORCENTAJEIVA; } set { this._PORCENTAJEIVA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PORCENTAJEISR
      /// </summary>
      [DataNames("PORCENTAJEISR")]
      [SugarColumn(ColumnName = "porcentajeisr", ColumnDescription = "Porcentaje de ISR")]
      public double? PORCENTAJEISR {get { return this._PORCENTAJEISR; } set { this._PORCENTAJEISR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer INCLUYEIVA
      /// </summary>
      [DataNames("INCLUYEIVA")]
      [SugarColumn(ColumnName = "incluyeiva", ColumnDescription = "Indica si incluye iva")]
      public int? INCLUYEIVA {get { return this._INCLUYEIVA; } set { this._INCLUYEIVA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IDCONCEP
      /// </summary>
      [DataNames("IDCONCEP")]
      [SugarColumn(ColumnName = "idconcep", ColumnDescription = "Clave del concepto")]
      public int? IDCONCEP {get { return this._IDCONCEP; } set { this._IDCONCEP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IDCONCEPIVAA
      /// </summary>
      [DataNames("IDCONCEPIVAA")]
      [SugarColumn(ColumnName = "idconcepivaa", ColumnDescription = "Clave del concepto con iva")]
      public int? IDCONCEPIVAA {get { return this._IDCONCEPIVAA; } set { this._IDCONCEPIVAA = value; this.OnPropertyChanged(); }}
   }
}
