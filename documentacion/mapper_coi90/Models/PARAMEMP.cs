﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///PARAMEMP
   ///</summary>
   [SugarTable("paramemp")]
   public partial class PARAMEMP
   {
      public PARAMEMP(){
      }

      private int _IDEMP;
      private string _DIREC;
      private string _POBL;
      private string _COD_POS;
      private string _RFC_EMP;
      private string _CED_EMP;
      private int? _DIGCTA1;
      private int? _DIGCTA2;
      private int? _DIGCTA3;
      private int? _DIGCTA4;
      private int? _DIGCTA5;
      private int? _DIGCTA6;
      private int? _DIGCTA7;
      private int? _DIGCTA8;
      private int? _DIGCTA9;
      private int? _NIVELACTU;
      private string _GUION_SINO;
      private string _REDO_P;
      private string _CAPSALINI;
      private string _ASIGSECPOL;
      private string _CONTABLINEA;
      private int? _MESCIERRE;
      private string _SUBDIR;
      private string _CAPCTASPOL;
      private string _DEPTO_SINO;
      private string _TRASPASO;
      private string _CTMONEXT;
      private string _CSTIPOCMB;
      private string _CPTIPOCMB;
      private string _CQUEMONEXT;
      private string _CQUEMONCTB;
      private string _CTAIVA;
      private string _CTABANCO;
      private int? _CONMONTPOL;
      private int? _DRIVERCOI;
      private string _ALIAS;
      private int? _FOLIOUNICO;
      private string _CAPTURADOS;
      private string _POLDESCUAD;
      private string _DIRTRAB;
      private string _IVADEF;
      private int? _IVAENOPT;
      private int? _IVAENINGR;
      private string _BUFFCUECUA;
      private string _TIPOPERSONA;
      private string _NACIONALIDAD;
      private string _TIPOEMPRESA;
      private string _FCONSTITUCION;
      private string _REGIMEN;
      private string _NOMBREREPRE;
      private string _RFCREPRE;
      private string _CURPREPRE;
      private string _PODERNOT;
      private int? _TRASPASOSALDOS;
      private string _NOMBRE;
      private int? _CCOSTOS;
      private int? _CGRUPOS;
      private string _ETQCCOSTOS;
      private string _ETQCGRUPOS;
      private string _IVADEFIETU;
      private int? _TIPODECAPTURADIOT;
      private int? _TIPODECAPTURAIETU;
      private string _CUENTAIVAACREDITABLE;
      private string _CUENTAIVATRASLADADO;
      private string _TIPOPOLIZA;
      private string _TIPOPOLIZATER;
      private string _GIRO;
      private string _DEV_COMPRAS;
      private string _DEV_VENTAS;
      private string _GASTO_ACTIVO;
      private string _IEPS_POR_ACREDITAR;
      private string _IEPS_POR_TRASLADAR;
      private string _IMP_LOCAL_RETENIDO;
      private string _IMP_LOCAL_TRASLADADO;
      private string _ISR_RETENIDO;
      private string _IVA_ACREDITAR;
      private string _IVA_POR_TRASLADAR;
      private string _IVA_RETENIDO;
      private string _VENTAS;
      private string _RCERTIFICADO;
      private string _RLLAVEPRIVADA;
      private string _CONTRASENIALLAVEPRIVADA;
      private string _PROVEEDORTIMBRADO;
      private string _USUARIOTIMBRADO;
      private string _CONTRASENIATIMBRADO;
      private string _PROVEEDORCANCELACION;
      private string _USUARIOCANCELACION;
      private string _CONTRASENIACANCELACION;
      private string _ESPACIOASPEL;
      private string _USUARIOESPACIO;
      private string _CONTRASENIAESPACIO;
      private int? _IDCATEGORIAESPACIO;
      private string _NOMBRECATEGORIAESPACIO;
      private int? _ASIGCATEGORIA;
      private int? _GDIOTAPARTIRCFDI;
      private int? _GUARDAROPEAUTO;
      private int? _CFDIENTODASLASPARTIDAS;
      private byte[] _LOGO;

      /// <summary>
      /// obtener o establecer TRIAL Default_New: Nullable_New:False
      /// </summary>
      [DataNames("IDEMP")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "idemp", ColumnDescription = "Identificador de la empresa", IsNullable = false)]
      public int IDEMP {get { return this._IDEMP; } set { this._IDEMP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DIREC")] 
      [SugarColumn(ColumnName = "direc", ColumnDescription = "Direccion de la empresa", Length = 30)]
      public string DIREC {get { return this._DIREC; } set { this._DIREC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("POBL")] 
      [SugarColumn(ColumnName = "pobl", ColumnDescription = "Poblacion de la empresa", Length = 30)]
      public string POBL {get { return this._POBL; } set { this._POBL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("COD_POS")] 
      [SugarColumn(ColumnName = "cod_pos", ColumnDescription = "Codigo postal", Length = 5)]
      public string COD_POS {get { return this._COD_POS; } set { this._COD_POS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("RFC_EMP")] 
      [SugarColumn(ColumnName = "rfc_emp", ColumnDescription = "RFC de la empresa", Length = 30)]
      public string RFC_EMP {get { return this._RFC_EMP; } set { this._RFC_EMP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CED_EMP")] 
      [SugarColumn(ColumnName = "ced_emp", ColumnDescription = "Cedula de la empresa", Length = 30)]
      public string CED_EMP {get { return this._CED_EMP; } set { this._CED_EMP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DIGCTA1")] 
      [SugarColumn(ColumnName = "digcta1", ColumnDescription = "Digitos a capturar pora el nive 1")]
      public int? DIGCTA1 {get { return this._DIGCTA1; } set { this._DIGCTA1 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DIGCTA2")] 
      [SugarColumn(ColumnName = "digcta2", ColumnDescription = "Digitos a capturar pora el nive 2")]
      public int? DIGCTA2 {get { return this._DIGCTA2; } set { this._DIGCTA2 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DIGCTA3")] 
      [SugarColumn(ColumnName = "digcta3", ColumnDescription = "Digitos a capturar pora el nive 3")]
      public int? DIGCTA3 {get { return this._DIGCTA3; } set { this._DIGCTA3 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DIGCTA4")] 
      [SugarColumn(ColumnName = "digcta4", ColumnDescription = "Digitos a capturar pora el nive 4")]
      public int? DIGCTA4 {get { return this._DIGCTA4; } set { this._DIGCTA4 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DIGCTA5")] 
      [SugarColumn(ColumnName = "digcta5", ColumnDescription = "Digitos a capturar pora el nive 5")]
      public int? DIGCTA5 {get { return this._DIGCTA5; } set { this._DIGCTA5 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DIGCTA6")] 
      [SugarColumn(ColumnName = "digcta6", ColumnDescription = "Digitos a capturar pora el nive 6")]
      public int? DIGCTA6 {get { return this._DIGCTA6; } set { this._DIGCTA6 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DIGCTA7")] 
      [SugarColumn(ColumnName = "digcta7", ColumnDescription = "Digitos a capturar pora el nive 7")]
      public int? DIGCTA7 {get { return this._DIGCTA7; } set { this._DIGCTA7 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DIGCTA8")] 
      [SugarColumn(ColumnName = "digcta8", ColumnDescription = "Digitos a capturar pora el nive 8")]
      public int? DIGCTA8 {get { return this._DIGCTA8; } set { this._DIGCTA8 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DIGCTA9")] 
      [SugarColumn(ColumnName = "digcta9", ColumnDescription = "Digitos a capturar pora el nive 9")]
      public int? DIGCTA9 {get { return this._DIGCTA9; } set { this._DIGCTA9 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("NIVELACTU")] 
      [SugarColumn(ColumnName = "nivelactu", ColumnDescription = "Nivel actual de la cuenta")]
      public int? NIVELACTU {get { return this._NIVELACTU; } set { this._NIVELACTU = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("GUION_SINO")] 
      [SugarColumn(ColumnName = "guion_sino", ColumnDescription = "La cuenta maneja guiones o no los maneja", Length = 1)]
      public string GUION_SINO {get { return this._GUION_SINO; } set { this._GUION_SINO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("REDO_P")] 
      [SugarColumn(ColumnName = "redo_p", ColumnDescription = "Se redondea", Length = 1)]
      public string REDO_P {get { return this._REDO_P; } set { this._REDO_P = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CAPSALINI")] 
      [SugarColumn(ColumnName = "capsalini", ColumnDescription = "Indica si se captura saldo inicial", Length = 1)]
      public string CAPSALINI {get { return this._CAPSALINI; } set { this._CAPSALINI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("ASIGSECPOL")] 
      [SugarColumn(ColumnName = "asigsecpol", ColumnDescription = "Folio secuencial", Length = 1)]
      public string ASIGSECPOL {get { return this._ASIGSECPOL; } set { this._ASIGSECPOL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CONTABLINEA")] 
      [SugarColumn(ColumnName = "contablinea", ColumnDescription = "Indica si se contbiliza en linea o no", Length = 1)]
      public string CONTABLINEA {get { return this._CONTABLINEA; } set { this._CONTABLINEA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("MESCIERRE")] 
      [SugarColumn(ColumnName = "mescierre", ColumnDescription = "Mes de cierre")]
      public int? MESCIERRE {get { return this._MESCIERRE; } set { this._MESCIERRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("SUBDIR")] 
      [SugarColumn(ColumnName = "subdir", ColumnDescription = "Subidiraccion", Length = 255)]
      public string SUBDIR {get { return this._SUBDIR; } set { this._SUBDIR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CAPCTASPOL")] 
      [SugarColumn(ColumnName = "capctaspol", ColumnDescription = "Indica si se captura una cuenta al crear una poliza", Length = 1)]
      public string CAPCTASPOL {get { return this._CAPCTASPOL; } set { this._CAPCTASPOL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DEPTO_SINO")] 
      [SugarColumn(ColumnName = "depto_sino", ColumnDescription = "Indica si tiene depto o no", Length = 1)]
      public string DEPTO_SINO {get { return this._DEPTO_SINO; } set { this._DEPTO_SINO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("TRASPASO")] 
      [SugarColumn(ColumnName = "traspaso", ColumnDescription = "Trapaso", Length = 1)]
      public string TRASPASO {get { return this._TRASPASO; } set { this._TRASPASO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CTMONEXT")] 
      [SugarColumn(ColumnName = "ctmonext", ColumnDescription = "Indica si la cuenta es moneda extranjera", Length = 1)]
      public string CTMONEXT {get { return this._CTMONEXT; } set { this._CTMONEXT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CSTIPOCMB")] 
      [SugarColumn(ColumnName = "cstipocmb", ColumnDescription = "Indica si se captura tipo de cambio", Length = 1)]
      public string CSTIPOCMB {get { return this._CSTIPOCMB; } set { this._CSTIPOCMB = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CPTIPOCMB")] 
      [SugarColumn(ColumnName = "cptipocmb", ColumnDescription = "Indica si se captura tipo de cambio", Length = 1)]
      public string CPTIPOCMB {get { return this._CPTIPOCMB; } set { this._CPTIPOCMB = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CQUEMONEXT")] 
      [SugarColumn(ColumnName = "cquemonext", ColumnDescription = "Indica si tiene moneda extranjera", Length = 1)]
      public string CQUEMONEXT {get { return this._CQUEMONEXT; } set { this._CQUEMONEXT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CQUEMONCTB")] 
      [SugarColumn(ColumnName = "cquemonctb", ColumnDescription = "Indica si se captura tipo de cambio", Length = 1)]
      public string CQUEMONCTB {get { return this._CQUEMONCTB; } set { this._CQUEMONCTB = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CTAIVA")] 
      [SugarColumn(ColumnName = "ctaiva", ColumnDescription = "Iva de la cuenta", Length = 40)]
      public string CTAIVA {get { return this._CTAIVA; } set { this._CTAIVA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CTABANCO")] 
      [SugarColumn(ColumnName = "ctabanco", ColumnDescription = "Banco de la cuenta", Length = 50)]
      public string CTABANCO {get { return this._CTABANCO; } set { this._CTABANCO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CONMONTPOL")] 
      [SugarColumn(ColumnName = "conmontpol", ColumnDescription = "Indica monto poliza")]
      public int? CONMONTPOL {get { return this._CONMONTPOL; } set { this._CONMONTPOL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DRIVERCOI")] 
      [SugarColumn(ColumnName = "drivercoi", ColumnDescription = "Driver del sistema")]
      public int? DRIVERCOI {get { return this._DRIVERCOI; } set { this._DRIVERCOI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("ALIAS")] 
      [SugarColumn(ColumnName = "alias", ColumnDescription = "Alias del sistema", Length = 255)]
      public string ALIAS {get { return this._ALIAS; } set { this._ALIAS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("FOLIOUNICO")] 
      [SugarColumn(ColumnName = "foliounico", ColumnDescription = "Folio unico")]
      public int? FOLIOUNICO {get { return this._FOLIOUNICO; } set { this._FOLIOUNICO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CAPTURADOS")] 
      [SugarColumn(ColumnName = "capturados", ColumnDescription = "Si se encuentra capturado", Length = 1)]
      public string CAPTURADOS {get { return this._CAPTURADOS; } set { this._CAPTURADOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("POLDESCUAD")] 
      [SugarColumn(ColumnName = "poldescuad", ColumnDescription = "Indica si la poliza esta descuadrada", Length = 1)]
      public string POLDESCUAD {get { return this._POLDESCUAD; } set { this._POLDESCUAD = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DIRTRAB")] 
      [SugarColumn(ColumnName = "dirtrab", ColumnDescription = "Ruta de trabajo", Length = 255)]
      public string DIRTRAB {get { return this._DIRTRAB; } set { this._DIRTRAB = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("IVADEF")] 
      [SugarColumn(ColumnName = "ivadef", ColumnDescription = "IVA por omision", Length = 2)]
      public string IVADEF {get { return this._IVADEF; } set { this._IVADEF = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("IVAENOPT")] 
      [SugarColumn(ColumnName = "ivaenopt", ColumnDescription = "Iva para operaciones con terceros")]
      public int? IVAENOPT {get { return this._IVAENOPT; } set { this._IVAENOPT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("IVAENINGR")] 
      [SugarColumn(ColumnName = "ivaeningr", ColumnDescription = "Iva para ingresos")]
      public int? IVAENINGR {get { return this._IVAENINGR; } set { this._IVAENINGR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("BUFFCUECUA")] 
      [SugarColumn(ColumnName = "buffcuecua", ColumnDescription = "Cuenta de cuadre", Length = 21)]
      public string BUFFCUECUA {get { return this._BUFFCUECUA; } set { this._BUFFCUECUA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("TIPOPERSONA")] 
      [SugarColumn(ColumnName = "tipopersona", ColumnDescription = "Tipo de persona", Length = 10)]
      public string TIPOPERSONA {get { return this._TIPOPERSONA; } set { this._TIPOPERSONA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("NACIONALIDAD")] 
      [SugarColumn(ColumnName = "nacionalidad", ColumnDescription = "Nacionalidad", Length = 12)]
      public string NACIONALIDAD {get { return this._NACIONALIDAD; } set { this._NACIONALIDAD = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("TIPOEMPRESA")] 
      [SugarColumn(ColumnName = "tipoempresa", ColumnDescription = "Tipo de empresa", Length = 25)]
      public string TIPOEMPRESA {get { return this._TIPOEMPRESA; } set { this._TIPOEMPRESA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("FCONSTITUCION")] 
      [SugarColumn(ColumnName = "fconstitucion", ColumnDescription = "Fecha de constitucion", Length = 10)]
      public string FCONSTITUCION {get { return this._FCONSTITUCION; } set { this._FCONSTITUCION = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("REGIMEN")] 
      [SugarColumn(ColumnName = "regimen", ColumnDescription = "Regimen de la empresa", Length = 25)]
      public string REGIMEN {get { return this._REGIMEN; } set { this._REGIMEN = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("NOMBREREPRE")] 
      [SugarColumn(ColumnName = "nombrerepre", ColumnDescription = "Nombre de la empresa", Length = 60)]
      public string NOMBREREPRE {get { return this._NOMBREREPRE; } set { this._NOMBREREPRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("RFCREPRE")] 
      [SugarColumn(ColumnName = "rfcrepre", ColumnDescription = "RFC del representante legal de la empresa", Length = 13)]
      public string RFCREPRE {get { return this._RFCREPRE; } set { this._RFCREPRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CURPREPRE")] 
      [SugarColumn(ColumnName = "curprepre", ColumnDescription = "Curp del representante legal de la empresa", Length = 18)]
      public string CURPREPRE {get { return this._CURPREPRE; } set { this._CURPREPRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("PODERNOT")] 
      [SugarColumn(ColumnName = "podernot", ColumnDescription = "Poder notarial", Length = 30)]
      public string PODERNOT {get { return this._PODERNOT; } set { this._PODERNOT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("TRASPASOSALDOS")] 
      [SugarColumn(ColumnName = "traspasosaldos", ColumnDescription = "Indica si hay traspasos de saldos")]
      public int? TRASPASOSALDOS {get { return this._TRASPASOSALDOS; } set { this._TRASPASOSALDOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("NOMBRE")] 
      [SugarColumn(ColumnName = "nombre", ColumnDescription = "Nombre", Length = 60)]
      public string NOMBRE {get { return this._NOMBRE; } set { this._NOMBRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CCOSTOS")] 
      [SugarColumn(ColumnName = "ccostos", ColumnDescription = "Indica si habra centro de costos")]
      public int? CCOSTOS {get { return this._CCOSTOS; } set { this._CCOSTOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CGRUPOS")] 
      [SugarColumn(ColumnName = "cgrupos", ColumnDescription = "Indica si tendra grupos")]
      public int? CGRUPOS {get { return this._CGRUPOS; } set { this._CGRUPOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("ETQCCOSTOS")] 
      [SugarColumn(ColumnName = "etqccostos", ColumnDescription = "Etiqueta para centro de costos", Length = 40)]
      public string ETQCCOSTOS {get { return this._ETQCCOSTOS; } set { this._ETQCCOSTOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("ETQCGRUPOS")] 
      [SugarColumn(ColumnName = "etqcgrupos", ColumnDescription = "Etiqueta para grupos", Length = 40)]
      public string ETQCGRUPOS {get { return this._ETQCGRUPOS; } set { this._ETQCGRUPOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("IVADEFIETU")] 
      [SugarColumn(ColumnName = "ivadefietu", ColumnDescription = "IVA default para IETU", Length = 2)]
      public string IVADEFIETU {get { return this._IVADEFIETU; } set { this._IVADEFIETU = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("TIPODECAPTURADIOT")] 
      [SugarColumn(ColumnName = "tipodecapturadiot", ColumnDescription = "Tipo de captura para diot")]
      public int? TIPODECAPTURADIOT {get { return this._TIPODECAPTURADIOT; } set { this._TIPODECAPTURADIOT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("TIPODECAPTURAIETU")] 
      [SugarColumn(ColumnName = "tipodecapturaietu", ColumnDescription = "Tipo de captura para IETU")]
      public int? TIPODECAPTURAIETU {get { return this._TIPODECAPTURAIETU; } set { this._TIPODECAPTURAIETU = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CUENTAIVAACREDITABLE")] 
      [SugarColumn(ColumnName = "cuentaivaacreditable", ColumnDescription = "Cuenta de IVA acreditable", Length = 31)]
      public string CUENTAIVAACREDITABLE {get { return this._CUENTAIVAACREDITABLE; } set { this._CUENTAIVAACREDITABLE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CUENTAIVATRASLADADO")] 
      [SugarColumn(ColumnName = "cuentaivatrasladado", ColumnDescription = "Cuenta de IIVA trasladado de la cuenta", Length = 31)]
      public string CUENTAIVATRASLADADO {get { return this._CUENTAIVATRASLADADO; } set { this._CUENTAIVATRASLADADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("TIPOPOLIZA")] 
      [SugarColumn(ColumnName = "tipopoliza", ColumnDescription = "Tipo de la poliza", Length = 2)]
      public string TIPOPOLIZA {get { return this._TIPOPOLIZA; } set { this._TIPOPOLIZA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("TIPOPOLIZATER")] 
      [SugarColumn(ColumnName = "tipopolizater", ColumnDescription = "Tipo de poliza para terceros", Length = 2)]
      public string TIPOPOLIZATER {get { return this._TIPOPOLIZATER; } set { this._TIPOPOLIZATER = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("GIRO")] 
      [SugarColumn(ColumnName = "giro", ColumnDescription = "Giro de la empresa", Length = 6)]
      public string GIRO {get { return this._GIRO; } set { this._GIRO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DEV_COMPRAS")] 
      [SugarColumn(ColumnName = "dev_compras", ColumnDescription = "Devolucion de compras", Length = 21)]
      public string DEV_COMPRAS {get { return this._DEV_COMPRAS; } set { this._DEV_COMPRAS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DEV_VENTAS")] 
      [SugarColumn(ColumnName = "dev_ventas", ColumnDescription = "Devolucion de ventas", Length = 21)]
      public string DEV_VENTAS {get { return this._DEV_VENTAS; } set { this._DEV_VENTAS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("GASTO_ACTIVO")] 
      [SugarColumn(ColumnName = "gasto_activo", ColumnDescription = "Gasto activo", Length = 21)]
      public string GASTO_ACTIVO {get { return this._GASTO_ACTIVO; } set { this._GASTO_ACTIVO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("IEPS_POR_ACREDITAR")] 
      [SugarColumn(ColumnName = "ieps_por_acreditar", ColumnDescription = "Cuenta de IEPS por acreditar", Length = 21)]
      public string IEPS_POR_ACREDITAR {get { return this._IEPS_POR_ACREDITAR; } set { this._IEPS_POR_ACREDITAR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("IEPS_POR_TRASLADAR")] 
      [SugarColumn(ColumnName = "ieps_por_trasladar", ColumnDescription = "Cuenta de IEPS por trasladar", Length = 21)]
      public string IEPS_POR_TRASLADAR {get { return this._IEPS_POR_TRASLADAR; } set { this._IEPS_POR_TRASLADAR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("IMP_LOCAL_RETENIDO")] 
      [SugarColumn(ColumnName = "imp_local_retenido", ColumnDescription = "Cuenta de impuesto local retenido", Length = 21)]
      public string IMP_LOCAL_RETENIDO {get { return this._IMP_LOCAL_RETENIDO; } set { this._IMP_LOCAL_RETENIDO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("IMP_LOCAL_TRASLADADO")] 
      [SugarColumn(ColumnName = "imp_local_trasladado", ColumnDescription = "Cuenta de impuesto local trasladado", Length = 21)]
      public string IMP_LOCAL_TRASLADADO {get { return this._IMP_LOCAL_TRASLADADO; } set { this._IMP_LOCAL_TRASLADADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("ISR_RETENIDO")] 
      [SugarColumn(ColumnName = "isr_retenido", ColumnDescription = "ISR retenido", Length = 21)]
      public string ISR_RETENIDO {get { return this._ISR_RETENIDO; } set { this._ISR_RETENIDO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("IVA_ACREDITAR")] 
      [SugarColumn(ColumnName = "iva_acreditar", ColumnDescription = "IVA acreditar", Length = 21)]
      public string IVA_ACREDITAR {get { return this._IVA_ACREDITAR; } set { this._IVA_ACREDITAR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("IVA_POR_TRASLADAR")] 
      [SugarColumn(ColumnName = "iva_por_trasladar", ColumnDescription = "IVA por trasladar", Length = 21)]
      public string IVA_POR_TRASLADAR {get { return this._IVA_POR_TRASLADAR; } set { this._IVA_POR_TRASLADAR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("IVA_RETENIDO")] 
      [SugarColumn(ColumnName = "iva_retenido", ColumnDescription = "IVA retenido", Length = 21)]
      public string IVA_RETENIDO {get { return this._IVA_RETENIDO; } set { this._IVA_RETENIDO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("VENTAS")] 
      [SugarColumn(ColumnName = "ventas", ColumnDescription = "Cuenta de ventas", Length = 21)]
      public string VENTAS {get { return this._VENTAS; } set { this._VENTAS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("RCERTIFICADO")] 
      [SugarColumn(ColumnName = "rcertificado", ColumnDescription = "Ruta del certificado", Length = 1024)]
      public string RCERTIFICADO {get { return this._RCERTIFICADO; } set { this._RCERTIFICADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("RLLAVEPRIVADA")] 
      [SugarColumn(ColumnName = "rllaveprivada", ColumnDescription = "Ruta de la llave privada", Length = 1024)]
      public string RLLAVEPRIVADA {get { return this._RLLAVEPRIVADA; } set { this._RLLAVEPRIVADA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CONTRASENIALLAVEPRIVADA")] 
      [SugarColumn(ColumnName = "contraseniallaveprivada", ColumnDescription = "Contrasenia de la llave privada", Length = 256)]
      public string CONTRASENIALLAVEPRIVADA {get { return this._CONTRASENIALLAVEPRIVADA; } set { this._CONTRASENIALLAVEPRIVADA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("PROVEEDORTIMBRADO")] 
      [SugarColumn(ColumnName = "proveedortimbrado", ColumnDescription = "Proveedor de timbrado", Length = 256)]
      public string PROVEEDORTIMBRADO {get { return this._PROVEEDORTIMBRADO; } set { this._PROVEEDORTIMBRADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("USUARIOTIMBRADO")] 
      [SugarColumn(ColumnName = "usuariotimbrado", ColumnDescription = "Usuario de timbrado", Length = 256)]
      public string USUARIOTIMBRADO {get { return this._USUARIOTIMBRADO; } set { this._USUARIOTIMBRADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CONTRASENIATIMBRADO")] 
      [SugarColumn(ColumnName = "contraseniatimbrado", ColumnDescription = "Contrasenia del timbrado", Length = 256)]
      public string CONTRASENIATIMBRADO {get { return this._CONTRASENIATIMBRADO; } set { this._CONTRASENIATIMBRADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("PROVEEDORCANCELACION")] 
      [SugarColumn(ColumnName = "proveedorcancelacion", ColumnDescription = "Proveedor de cancelacion", Length = 256)]
      public string PROVEEDORCANCELACION {get { return this._PROVEEDORCANCELACION; } set { this._PROVEEDORCANCELACION = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("USUARIOCANCELACION")] 
      [SugarColumn(ColumnName = "usuariocancelacion", ColumnDescription = "Usuario de cancelacion", Length = 256)]
      public string USUARIOCANCELACION {get { return this._USUARIOCANCELACION; } set { this._USUARIOCANCELACION = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CONTRASENIACANCELACION")] 
      [SugarColumn(ColumnName = "contraseniacancelacion", ColumnDescription = "Contrasenia de cancelacion", Length = 256)]
      public string CONTRASENIACANCELACION {get { return this._CONTRASENIACANCELACION; } set { this._CONTRASENIACANCELACION = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("ESPACIOASPEL")] 
      [SugarColumn(ColumnName = "espacioaspel", ColumnDescription = "eSPACIO Aspel", Length = 256)]
      public string ESPACIOASPEL {get { return this._ESPACIOASPEL; } set { this._ESPACIOASPEL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("USUARIOESPACIO")] 
      [SugarColumn(ColumnName = "usuarioespacio", ColumnDescription = "Usuario de eSPACIO Aspel", Length = 256)]
      public string USUARIOESPACIO {get { return this._USUARIOESPACIO; } set { this._USUARIOESPACIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CONTRASENIAESPACIO")] 
      [SugarColumn(ColumnName = "contraseniaespacio", ColumnDescription = "Contrasenia de eSPACIO Aspel", Length = 256)]
      public string CONTRASENIAESPACIO {get { return this._CONTRASENIAESPACIO; } set { this._CONTRASENIAESPACIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
      /// </summary>
      [DataNames("IDCATEGORIAESPACIO")] 
      [SugarColumn(ColumnName = "idcategoriaespacio", ColumnDescription = "Categoria de eSPACIO Aspel", DefaultValue = 0)]
      public int? IDCATEGORIAESPACIO {get { return this._IDCATEGORIAESPACIO; } set { this._IDCATEGORIAESPACIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("NOMBRECATEGORIAESPACIO")] 
      [SugarColumn(ColumnName = "nombrecategoriaespacio", ColumnDescription = "Nombre Categoria de eSPACIO Aspel", Length = 256)]
      public string NOMBRECATEGORIAESPACIO {get { return this._NOMBRECATEGORIAESPACIO; } set { this._NOMBRECATEGORIAESPACIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("ASIGCATEGORIA")] 
      [SugarColumn(ColumnName = "asigcategoria", ColumnDescription = "Si permite indicar la categoría de un archivo")]
      public int? ASIGCATEGORIA {get { return this._ASIGCATEGORIA; } set { this._ASIGCATEGORIA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("GDIOTAPARTIRCFDI")] 
      [SugarColumn(ColumnName = "gdiotapartircfdi", ColumnDescription = "Indica si la captura del diot sera a partir de CFDIs")]
      public int? GDIOTAPARTIRCFDI {get { return this._GDIOTAPARTIRCFDI; } set { this._GDIOTAPARTIRCFDI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("GUARDAROPEAUTO")] 
      [SugarColumn(ColumnName = "guardaropeauto", ColumnDescription = "Indica si el guardado se realizara de forma silencionsa")]
      public int? GUARDAROPEAUTO {get { return this._GUARDAROPEAUTO; } set { this._GUARDAROPEAUTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
      /// </summary>
      [DataNames("CFDIENTODASLASPARTIDAS")] 
      [SugarColumn(ColumnName = "cfdientodaslaspartidas", ColumnDescription = "Indica si asocian los UUIDs a todas la partidas de póliza dinámica", DefaultValue = 0)]
      public int? CFDIENTODASLASPARTIDAS {get { return this._CFDIENTODASLASPARTIDAS; } set { this._CFDIENTODASLASPARTIDAS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("LOGO")] 
      [SugarColumn(ColumnName = "logo", ColumnDescription = "logo")]
      public byte[] LOGO {get { return this._LOGO; } set { this._LOGO = value; this.OnPropertyChanged(); }}
   }
}
