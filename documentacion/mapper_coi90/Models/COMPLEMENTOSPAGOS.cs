﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///COMPLEMENTOSPAGOS
   ///</summary>
   [SugarTable("complementospagos")]
   public partial class COMPLEMENTOSPAGOS
   {
      public COMPLEMENTOSPAGOS(){
      }

      private string _UUIDTIMBREPAGO;
      private string _SERIE;
      private string _FOLIO;
      private DateTime? _FECHA;
      private string _RFCEMISOR;
      private string _NOMBREEMISOR;
      private double? _MONTO;
      private string _TIPO_POLI;
      private string _NUM_POLIZ;
      private int? _PERIODO;
      private int? _EJERCICIO;
      private string _RUTAXML;
      private string _XML;

      /// <summary>
      /// obtener o establecer TRIAL Default_New: Nullable_New:False
      /// </summary>
      [DataNames("UUIDTIMBREPAGO")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "uuidtimbrepago", ColumnDescription = "UUID del complemento de pago", IsNullable = false, Length = 36)]
      public string UUIDTIMBREPAGO {get { return this._UUIDTIMBREPAGO; } set { this._UUIDTIMBREPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("SERIE")] 
      [SugarColumn(ColumnName = "serie", ColumnDescription = "Serie del complemento", Length = 25)]
      public string SERIE {get { return this._SERIE; } set { this._SERIE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("FOLIO")] 
      [SugarColumn(ColumnName = "folio", ColumnDescription = "Folio del complemento", Length = 40)]
      public string FOLIO {get { return this._FOLIO; } set { this._FOLIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("FECHA")] 
      [SugarColumn(ColumnName = "fecha", ColumnDescription = "Fecha del complemento")]
      public DateTime? FECHA {get { return this._FECHA; } set { this._FECHA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("RFCEMISOR")] 
      [SugarColumn(ColumnName = "rfcemisor", ColumnDescription = "RFC emisor", Length =36)]
      public string RFCEMISOR {get { return this._RFCEMISOR; } set { this._RFCEMISOR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("NOMBREEMISOR")] 
      [SugarColumn(ColumnName = "nombreemisor", ColumnDescription = "Nombre del emisor", Length = 300)]
      public string NOMBREEMISOR {get { return this._NOMBREEMISOR; } set { this._NOMBREEMISOR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("MONTO")] 
      [SugarColumn(ColumnName = "monto", ColumnDescription = "Monto total de los importes de los pagos")]
      public double? MONTO {get { return this._MONTO; } set { this._MONTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("TIPO_POLI")] 
      [SugarColumn(ColumnName = "tipo_poli", ColumnDescription = "Tipo", Length = 2)]
      public string TIPO_POLI {get { return this._TIPO_POLI; } set { this._TIPO_POLI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("NUM_POLIZ")] 
      [SugarColumn(ColumnName = "num_poliz", ColumnDescription = "Numero", Length = 5)]
      public string NUM_POLIZ {get { return this._NUM_POLIZ; } set { this._NUM_POLIZ = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("PERIODO")] 
      [SugarColumn(ColumnName = "periodo", ColumnDescription = "Periodo")]
      public int? PERIODO {get { return this._PERIODO; } set { this._PERIODO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("EJERCICIO")] 
      [SugarColumn(ColumnName = "ejercicio", ColumnDescription = "Ejercicio")]
      public int? EJERCICIO {get { return this._EJERCICIO; } set { this._EJERCICIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("RUTAXML")] 
      [SugarColumn(ColumnName = "rutaxml", ColumnDescription = "Ruta del complemento de pago", Length = 512)]
      public string RUTAXML {get { return this._RUTAXML; } set { this._RUTAXML = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("XML")] 
      [SugarColumn(ColumnName = "xml", ColumnDescription = "XML Complementos de pago")]
      public string XML {get { return this._XML; } set { this._XML = value; this.OnPropertyChanged(); }}
   }
}
