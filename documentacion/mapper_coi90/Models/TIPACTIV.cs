﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///TIPACTIV
   ///</summary>
   [SugarTable("tipactiv")]
   public partial class TIPACTIV
   {
      public TIPACTIV(){
      }

      private int _CLAVE;
      private string _DESCRIP;
      private double? _DEDNORMAL;
      private double? _DEDIMED;
      private double? _MAXDED;
      private string _METODODEP;
      private double? _DEPPROY;
      private string _CTAAACTV;
      private string _CTADEPRE;
      private string _CTAGASTOS;
      private string _CTAPERGAN;
      private int? _QUEMONEDA;

      /// <summary>
      /// obtener o establecer CLAVE
      /// </summary>
      [DataNames("CLAVE")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "clave", ColumnDescription = "Clave", IsNullable = false)]
      public int CLAVE {get { return this._CLAVE; } set { this._CLAVE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DESCRIP
      /// </summary>
      [DataNames("DESCRIP")]
      [SugarColumn(ColumnName = "descrip", ColumnDescription = "Descripcion", Length = 20)]
      public string DESCRIP {get { return this._DESCRIP; } set { this._DESCRIP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DEDNORMAL
      /// </summary>
      [DataNames("DEDNORMAL")]
      [SugarColumn(ColumnName = "dednormal", ColumnDescription = "Deduccion normal")]
      public double? DEDNORMAL {get { return this._DEDNORMAL; } set { this._DEDNORMAL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DEDIMED
      /// </summary>
      [DataNames("DEDIMED")]
      [SugarColumn(ColumnName = "dedimed", ColumnDescription = "Metodo deducible")]
      public double? DEDIMED {get { return this._DEDIMED; } set { this._DEDIMED = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MAXDED
      /// </summary>
      [DataNames("MAXDED")]
      [SugarColumn(ColumnName = "maxded", ColumnDescription = "Maximo deducible")]
      public double? MAXDED {get { return this._MAXDED; } set { this._MAXDED = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer METODODEP
      /// </summary>
      [DataNames("METODODEP")]
      [SugarColumn(ColumnName = "metododep", ColumnDescription = "Metodo de depreciacion", Length = 1)]
      public string METODODEP {get { return this._METODODEP; } set { this._METODODEP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DEPPROY
      /// </summary>
      [DataNames("DEPPROY")]
      [SugarColumn(ColumnName = "depproy", ColumnDescription = "Porcentaje de Depreciacion proyectada")]
      public double? DEPPROY {get { return this._DEPPROY; } set { this._DEPPROY = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTAAACTV
      /// </summary>
      [DataNames("CTAAACTV")]
      [SugarColumn(ColumnName = "ctaaactv", ColumnDescription = "Cuenta del activo", Length = 21)]
      public string CTAAACTV {get { return this._CTAAACTV; } set { this._CTAAACTV = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTADEPRE
      /// </summary>
      [DataNames("CTADEPRE")]
      [SugarColumn(ColumnName = "ctadepre", ColumnDescription = "Cuenta del depreciacion del activo", Length = 21)]
      public string CTADEPRE {get { return this._CTADEPRE; } set { this._CTADEPRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTAGASTOS
      /// </summary>
      [DataNames("CTAGASTOS")]
      [SugarColumn(ColumnName = "ctagastos", ColumnDescription = "Cuenta de gastos", Length = 21)]
      public string CTAGASTOS {get { return this._CTAGASTOS; } set { this._CTAGASTOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTAPERGAN
      /// </summary>
      [DataNames("CTAPERGAN")]
      [SugarColumn(ColumnName = "ctapergan", ColumnDescription = "Cuenta de ganancias", Length = 21)]
      public string CTAPERGAN {get { return this._CTAPERGAN; } set { this._CTAPERGAN = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer QUEMONEDA
      /// </summary>
      [DataNames("QUEMONEDA")]
      [SugarColumn(ColumnName = "quemoneda", ColumnDescription = "Moneda")]
      public int? QUEMONEDA {get { return this._QUEMONEDA; } set { this._QUEMONEDA = value; this.OnPropertyChanged(); }}
   }
}
