﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///PLANTILLAXRFC
   ///</summary>
   [SugarTable("plantillaxrfc")]
   public partial class PLANTILLAXRFC
   {
      public PLANTILLAXRFC(){
      }

      private string _RFC;
      private string _CLASPLANTILLA;
      private string _NOMBREPLANTILLA;
      private string _NOMBREPLANTILLA2;
      private string _USOCFDI;

      /// <summary>
      /// obtener o establecer RFC
      /// </summary>
      [DataNames("RFC")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "rfc", ColumnDescription = "RFC del tercero", IsNullable = false, Length = 15)]
      public string RFC {get { return this._RFC; } set { this._RFC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CLASPLANTILLA
      /// </summary>
      [DataNames("CLASPLANTILLA")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "clasplantilla", ColumnDescription = "Tipo de cfdi para el que aplica la plantilla (1=factura,2=notacredito,3=credito,4=compPago)", IsNullable = false, Length = 5)]
      public string CLASPLANTILLA {get { return this._CLASPLANTILLA; } set { this._CLASPLANTILLA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOMBREPLANTILLA
      /// </summary>
      [DataNames("NOMBREPLANTILLA")]
      [SugarColumn(ColumnName = "nombreplantilla", ColumnDescription = "Nombre de la plantilla para 1a contabilizacion del UUID", Length = 256)]
      public string NOMBREPLANTILLA {get { return this._NOMBREPLANTILLA; } set { this._NOMBREPLANTILLA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOMBREPLANTILLA2
      /// </summary>
      [DataNames("NOMBREPLANTILLA2")]
      [SugarColumn(ColumnName = "nombreplantilla2", ColumnDescription = "Nombre de la plantilla para 2a contabilizacion del UUID", Length = 256)]
      public string NOMBREPLANTILLA2 {get { return this._NOMBREPLANTILLA2; } set { this._NOMBREPLANTILLA2 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer USOCFDI
      /// </summary>
      [DataNames("USOCFDI")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "usocfdi", ColumnDescription = "Uso cfdi", IsNullable = false, Length = 6)]
      public string USOCFDI {get { return this._USOCFDI; } set { this._USOCFDI = value; this.OnPropertyChanged(); }}
   }
}
