﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///REGPOL
   ///</summary>
   [SugarTable("regpol")]
   public partial class REGPOL
   {
      public REGPOL(){
      }

      private int _TRANSACCIO;
      private string _TIPOPOL;
      private string _NUMPOL;
      private DateTime? _FECHAPOL;
      private DateTime? _FECHAOPR;
      private string _SISTEMA;
      private int? _NUMUSR;
      private int? _OPERACION;
      private int? _STATUS;
      private string _TIPPOLINTR;
      private string _POLMODELO;
      private int? _REGCONCLI;
      private string _STATUSCLI;

      /// <summary>
      /// obtener o establecer TRANSACCIO
      /// </summary>
      [DataNames("TRANSACCIO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "transaccio", ColumnDescription = "Numero de transaccion", IsNullable = false)]
      public int TRANSACCIO {get { return this._TRANSACCIO; } set { this._TRANSACCIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOPOL
      /// </summary>
      [DataNames("TIPOPOL")]
      [SugarColumn(ColumnName = "tipopol", ColumnDescription = "Tipo poliza", Length = 2)]
      public string TIPOPOL {get { return this._TIPOPOL; } set { this._TIPOPOL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMPOL
      /// </summary>
      [DataNames("NUMPOL")]
      [SugarColumn(ColumnName = "numpol", ColumnDescription = "Numero de poliza", Length = 5)]
      public string NUMPOL {get { return this._NUMPOL; } set { this._NUMPOL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHAPOL
      /// </summary>
      [DataNames("FECHAPOL")]
      [SugarColumn(ColumnName = "fechapol", ColumnDescription = "Fecha poliza")]
      public DateTime? FECHAPOL {get { return this._FECHAPOL; } set { this._FECHAPOL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHAOPR
      /// </summary>
      [DataNames("FECHAOPR")]
      [SugarColumn(ColumnName = "fechaopr", ColumnDescription = "Fecha operacion")]
      public DateTime? FECHAOPR {get { return this._FECHAOPR; } set { this._FECHAOPR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SISTEMA
      /// </summary>
      [DataNames("SISTEMA")]
      [SugarColumn(ColumnName = "sistema", ColumnDescription = "Sistema", Length = 15)]
      public string SISTEMA {get { return this._SISTEMA; } set { this._SISTEMA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMUSR
      /// </summary>
      [DataNames("NUMUSR")]
      [SugarColumn(ColumnName = "numusr", ColumnDescription = "Numero de usuario de interfaz")]
      public int? NUMUSR {get { return this._NUMUSR; } set { this._NUMUSR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer OPERACION
      /// </summary>
      [DataNames("OPERACION")]
      [SugarColumn(ColumnName = "operacion", ColumnDescription = "Operacion")]
      public int? OPERACION {get { return this._OPERACION; } set { this._OPERACION = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer STATUS
      /// </summary>
      [DataNames("STATUS")]
      [SugarColumn(ColumnName = "status", ColumnDescription = "Estatus")]
      public int? STATUS {get { return this._STATUS; } set { this._STATUS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPPOLINTR
      /// </summary>
      [DataNames("TIPPOLINTR")]
      [SugarColumn(ColumnName = "tippolintr", ColumnDescription = "Tipo poliza interfaz", Length = 1)]
      public string TIPPOLINTR {get { return this._TIPPOLINTR; } set { this._TIPPOLINTR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer POLMODELO
      /// </summary>
      [DataNames("POLMODELO")]
      [SugarColumn(ColumnName = "polmodelo", ColumnDescription = "Poliza modelo", Length = 255)]
      public string POLMODELO {get { return this._POLMODELO; } set { this._POLMODELO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer REGCONCLI
      /// </summary>
      [DataNames("REGCONCLI")]
      [SugarColumn(ColumnName = "regconcli", ColumnDescription = "Registro de cliente")]
      public int? REGCONCLI {get { return this._REGCONCLI; } set { this._REGCONCLI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer STATUSCLI
      /// </summary>
      [DataNames("STATUSCLI")]
      [SugarColumn(ColumnName = "statuscli", ColumnDescription = "Estatus de clientes", Length = 1)]
      public string STATUSCLI {get { return this._STATUSCLI; } set { this._STATUSCLI = value; this.OnPropertyChanged(); }}
   }
}
