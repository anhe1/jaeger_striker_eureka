﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///RFCTER
   ///</summary>
   [SugarTable("rfcter")]
   public partial class RFCTER
   {
      public RFCTER(){
      }

      private string _RFCIDFISC;
      private int? _TIPO;
      private string _NOMBRE;
      private string _PAIS;
      private string _NACIONAL;
      private int? _TIPOOP;
      private string _CLAVESAE;
      private string _CUENTAGAS;
      private string _ESMONTO;
      private double? _MONPORC;
      private double? _IVADEFAULT;
      private double? _PORCENTAJEIVA;
      private double? _PORCENTAJEISR;
      private int? _INCLUYEIVA;
      private string _ESPROV;
      private string _CTABANCARIA;
      private int? _BANCO;
      private string _FRMPAGO;
      private string _BANCOEXTRANJERO;
      private int? _ESIDFISCAL;
      private string _PLANTILLAPOLDIN;
      private int? _ESRFCLISTANEGRA;
      private string _CUENTACLIENTE;

      /// <summary>
      /// obtener o establecer RFCIDFISC
      /// </summary>
      [DataNames("RFCIDFISC")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "rfcidfisc", ColumnDescription = "RFC fiscal", IsNullable = false, Length = 40)]
      public string RFCIDFISC {get { return this._RFCIDFISC; } set { this._RFCIDFISC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPO
      /// </summary>
      [DataNames("TIPO")]
      [SugarColumn(ColumnName = "tipo", ColumnDescription = "Tipo")]
      public int? TIPO {get { return this._TIPO; } set { this._TIPO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOMBRE
      /// </summary>
      [DataNames("NOMBRE")]
      [SugarColumn(ColumnName = "nombre", ColumnDescription = "Nombre", Length = 60)]
      public string NOMBRE {get { return this._NOMBRE; } set { this._NOMBRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PAIS
      /// </summary>
      [DataNames("PAIS")]
      [SugarColumn(ColumnName = "pais", ColumnDescription = "Pais", Length = 2)]
      public string PAIS {get { return this._PAIS; } set { this._PAIS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NACIONAL
      /// </summary>
      [DataNames("NACIONAL")]
      [SugarColumn(ColumnName = "nacional", ColumnDescription = "Nacional", Length = 40)]
      public string NACIONAL {get { return this._NACIONAL; } set { this._NACIONAL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOOP
      /// </summary>
      [DataNames("TIPOOP")]
      [SugarColumn(ColumnName = "tipoop", ColumnDescription = "Tipo de operacion")]
      public int? TIPOOP {get { return this._TIPOOP; } set { this._TIPOOP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CLAVESAE
      /// </summary>
      [DataNames("CLAVESAE")]
      [SugarColumn(ColumnName = "clavesae", ColumnDescription = "Clave SAE", Length = 5)]
      public string CLAVESAE {get { return this._CLAVESAE; } set { this._CLAVESAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CUENTAGAS
      /// </summary>
      [DataNames("CUENTAGAS")]
      [SugarColumn(ColumnName = "cuentagas", ColumnDescription = "Cuenta gastos", Length = 21)]
      public string CUENTAGAS {get { return this._CUENTAGAS; } set { this._CUENTAGAS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ESMONTO
      /// </summary>
      [DataNames("ESMONTO")]
      [SugarColumn(ColumnName = "esmonto", ColumnDescription = "Indica si es un monto", Length = 1)]
      public string ESMONTO {get { return this._ESMONTO; } set { this._ESMONTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONPORC
      /// </summary>
      [DataNames("MONPORC")]
      [SugarColumn(ColumnName = "monporc", ColumnDescription = "Monto")]
      public double? MONPORC {get { return this._MONPORC; } set { this._MONPORC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IVADEFAULT
      /// </summary>
      [DataNames("IVADEFAULT")]
      [SugarColumn(ColumnName = "ivadefault", ColumnDescription = "Iva default")]
      public double? IVADEFAULT {get { return this._IVADEFAULT; } set { this._IVADEFAULT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PORCENTAJEIVA
      /// </summary>
      [DataNames("PORCENTAJEIVA")]
      [SugarColumn(ColumnName = "porcentajeiva", ColumnDescription = "Porcentaje de iva")]
      public double? PORCENTAJEIVA {get { return this._PORCENTAJEIVA; } set { this._PORCENTAJEIVA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PORCENTAJEISR
      /// </summary>
      [DataNames("PORCENTAJEISR")]
      [SugarColumn(ColumnName = "porcentajeisr", ColumnDescription = "Porcentaje de ISR")]
      public double? PORCENTAJEISR {get { return this._PORCENTAJEISR; } set { this._PORCENTAJEISR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer INCLUYEIVA
      /// </summary>
      [DataNames("INCLUYEIVA")]
      [SugarColumn(ColumnName = "incluyeiva", ColumnDescription = "Indica si incluye iva")]
      public int? INCLUYEIVA {get { return this._INCLUYEIVA; } set { this._INCLUYEIVA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ESPROV
      /// </summary>
      [DataNames("ESPROV")]
      [SugarColumn(ColumnName = "esprov", ColumnDescription = "Indica si es proveedor", Length = 1)]
      public string ESPROV {get { return this._ESPROV; } set { this._ESPROV = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTABANCARIA
      /// </summary>
      [DataNames("CTABANCARIA")]
      [SugarColumn(ColumnName = "ctabancaria", ColumnDescription = "Cuenta bancaria", Length = 50)]
      public string CTABANCARIA {get { return this._CTABANCARIA; } set { this._CTABANCARIA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BANCO
      /// </summary>
      [DataNames("BANCO")]
      [SugarColumn(ColumnName = "banco", ColumnDescription = "Banco")]
      public int? BANCO {get { return this._BANCO; } set { this._BANCO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FRMPAGO
      /// </summary>
      [DataNames("FRMPAGO")]
      [SugarColumn(ColumnName = "frmpago", ColumnDescription = "Forma de pago", Length = 5)]
      public string FRMPAGO {get { return this._FRMPAGO; } set { this._FRMPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BANCOEXTRANJERO
      /// </summary>
      [DataNames("BANCOEXTRANJERO")]
      [SugarColumn(ColumnName = "bancoextranjero", ColumnDescription = "Banco extranjero", Length = 225)]
      public string BANCOEXTRANJERO {get { return this._BANCOEXTRANJERO; } set { this._BANCOEXTRANJERO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ESIDFISCAL
      /// </summary>
      [DataNames("ESIDFISCAL")]
      [SugarColumn(ColumnName = "esidfiscal", ColumnDescription = "Identifica si es un RFC extranjero", DefaultValue = 0)]
      public int? ESIDFISCAL {get { return this._ESIDFISCAL; } set { this._ESIDFISCAL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PLANTILLAPOLDIN
      /// </summary>
      [DataNames("PLANTILLAPOLDIN")]
      [SugarColumn(ColumnName = "plantillapoldin", ColumnDescription = "Plantilla polDinamica", DefaultValue = "0", Length = 512)]
      public string PLANTILLAPOLDIN {get { return this._PLANTILLAPOLDIN; } set { this._PLANTILLAPOLDIN = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ESRFCLISTANEGRA
      /// </summary>
      [DataNames("ESRFCLISTANEGRA")]
      [SugarColumn(ColumnName = "esrfclistanegra", ColumnDescription = "Esta en lista negra 1=si,0=no")]
      public int? ESRFCLISTANEGRA {get { return this._ESRFCLISTANEGRA; } set { this._ESRFCLISTANEGRA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CUENTACLIENTE
      /// </summary>
      [DataNames("CUENTACLIENTE")]
      [SugarColumn(ColumnName = "cuentacliente", ColumnDescription = "Cuenta al cliente que pertenece el RFC", Length = 21)]
      public string CUENTACLIENTE {get { return this._CUENTACLIENTE; } set { this._CUENTACLIENTE = value; this.OnPropertyChanged(); }}
   }
}
