﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///OTROSIMPUESTOSING
   ///</summary>
   [SugarTable("otrosimpuestosing")]
   public partial class OTROSIMPUESTOSING
   {
      public OTROSIMPUESTOSING(){
      }

      private int _IDOPEIET;
      private int _NUMREG;
      private string _NOMBRE;
      private double? _TASA;
      private double? _MONTO;
      private int _TIPO;
      private double? _BASE;
      private int _TIPOIMPUESTO;
      private int? _APLICAIC;
      private string _TIPOFACTOR;

      /// <summary>
      /// obtener o establecer IDOPEIET
      /// </summary>
      [DataNames("IDOPEIET")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "idopeiet", ColumnDescription = "Id de la tabla OPEIET", IsNullable = false)]
      public int IDOPEIET {get { return this._IDOPEIET; } set { this._IDOPEIET = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMREG
      /// </summary>
      [DataNames("NUMREG")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "numreg", ColumnDescription = "Numero de registro", IsNullable = false)]
      public int NUMREG {get { return this._NUMREG; } set { this._NUMREG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOMBRE
      /// </summary>
      [DataNames("NOMBRE")]
      [SugarColumn(ColumnName = "nombre", ColumnDescription = "Nombre del impuesto", IsNullable = false, Length = 120)]
      public string NOMBRE {get { return this._NOMBRE; } set { this._NOMBRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TASA
      /// </summary>
      [DataNames("TASA")]
      [SugarColumn(ColumnName = "tasa", ColumnDescription = "Tasa o cuota del impuesto")]
      public double? TASA {get { return this._TASA; } set { this._TASA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTO
      /// </summary>
      [DataNames("MONTO")]
      [SugarColumn(ColumnName = "monto", ColumnDescription = "Monto")]
      public double? MONTO {get { return this._MONTO; } set { this._MONTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPO
      /// </summary>
      [DataNames("TIPO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "tipo", ColumnDescription = "Indica si es impuesto trasladado ( 1= trasladado, 0 = retenido)", IsNullable = false)]
      public int TIPO {get { return this._TIPO; } set { this._TIPO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BASE
      /// </summary>
      [DataNames("BASE")]
      [SugarColumn(ColumnName = "base", ColumnDescription = "Base del impuesto")]
      public double? BASE {get { return this._BASE; } set { this._BASE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOIMPUESTO
      /// </summary>
      [DataNames("TIPOIMPUESTO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "tipoimpuesto", ColumnDescription = "Indica el tipo de impuesto (1=ISR, 2=IVA, 3=IEPS, 4= Impuesto local, 5= Otros impuestos, 6= Otras retenciones)", IsNullable = false)]
      public int TIPOIMPUESTO {get { return this._TIPOIMPUESTO; } set { this._TIPOIMPUESTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer APLICAIC
      /// </summary>
      [DataNames("APLICAIC")]
      [SugarColumn(ColumnName = "aplicaic", ColumnDescription = "Indica si el impuesto es involucrado en el calculo de Ia tabla IETU, (1= si, 0 = no)")]
      public int? APLICAIC {get { return this._APLICAIC; } set { this._APLICAIC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOFACTOR
      /// </summary>
      [DataNames("TIPOFACTOR")]
      [SugarColumn(ColumnName = "tipofactor", ColumnDescription = "Indica si es tasa o cuota (tasa , cuota)", IsNullable = false, Length = 6)]
      public string TIPOFACTOR {get { return this._TIPOFACTOR; } set { this._TIPOFACTOR = value; this.OnPropertyChanged(); }}
   }
}