﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///INFADIPAR
   ///</summary>
   [SugarTable("infadipar")]
   public partial class INFADIPAR
   {
      public INFADIPAR(){
      }

      private int _NUMREG;
      private string _FRMPAGO;
      private string _NUMCHEQUE;
      private int _BANCO;
      private string _CTAORIG;
      private DateTime _FECHA;
      private double? _MONTO;
      private string _BENEF;
      private string _RFC;
      private int _BANCODEST;
      private string _CTADEST;
      private string _BANCOORIGEXT;
      private string _BANCODESTEXT;
      private string _IDFISCAL;

      /// <summary>
      /// obtener o establecer NUMREG
      /// </summary>
      [DataNames("NUMREG")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "numreg", ColumnDescription = "Numero de registro", IsNullable = false)]
      public int NUMREG {get { return this._NUMREG; } set { this._NUMREG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FRMPAGO
      /// </summary>
      [DataNames("FRMPAGO")]
      [SugarColumn(ColumnName = "frmpago", ColumnDescription = "Forma de pago", Length = 5)]
      public string FRMPAGO {get { return this._FRMPAGO; } set { this._FRMPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMCHEQUE
      /// </summary>
      [DataNames("NUMCHEQUE")]
      [SugarColumn(ColumnName = "numcheque", ColumnDescription = "Numero de cheque", Length = 20)]
      public string NUMCHEQUE {get { return this._NUMCHEQUE; } set { this._NUMCHEQUE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BANCO
      /// </summary>
      [DataNames("BANCO")]
      [SugarColumn(ColumnName = "banco", ColumnDescription = "Clavel del banco", IsNullable = false)]
      public int BANCO {get { return this._BANCO; } set { this._BANCO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTAORIG
      /// </summary>
      [DataNames("CTAORIG")]
      [SugarColumn(ColumnName = "ctaorig", ColumnDescription = "Cuenta origen", Length = 50)]
      public string CTAORIG {get { return this._CTAORIG; } set { this._CTAORIG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHA
      /// </summary>
      [DataNames("FECHA")]
      [SugarColumn(ColumnName = "fecha", ColumnDescription = "Fecha", IsNullable = false)]
      public DateTime FECHA {get { return this._FECHA; } set { this._FECHA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTO
      /// </summary>
      [DataNames("MONTO")]
      [SugarColumn(ColumnName = "monto", ColumnDescription = "Monto")]
      public double? MONTO {get { return this._MONTO; } set { this._MONTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BENEF
      /// </summary>
      [DataNames("BENEF")]
      [SugarColumn(ColumnName = "benef", ColumnDescription = "Beneficiario", Length = 300)]
      public string BENEF {get { return this._BENEF; } set { this._BENEF = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFC
      /// </summary>
      [DataNames("RFC")]
      [SugarColumn(ColumnName = "rfc", ColumnDescription = "RFC", Length = 30)]
      public string RFC {get { return this._RFC; } set { this._RFC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BANCODEST
      /// </summary>
      [DataNames("BANCODEST")]
      [SugarColumn(ColumnName = "bancodest", ColumnDescription = "Banco destino")]
      public int BANCODEST {get { return this._BANCODEST; } set { this._BANCODEST = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTADEST
      /// </summary>
      [DataNames("CTADEST")]
      [SugarColumn(ColumnName = "ctadest", ColumnDescription = "Cuenta destino", IsNullable = false, Length = 50)]
      public string CTADEST {get { return this._CTADEST; } set { this._CTADEST = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BANCOORIGEXT
      /// </summary>
      [DataNames("BANCOORIGEXT")]
      [SugarColumn(ColumnName = "bancoorigext", ColumnDescription = "Banco origen extranjero", Length = 225)]
      public string BANCOORIGEXT {get { return this._BANCOORIGEXT; } set { this._BANCOORIGEXT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BANCODESTEXT
      /// </summary>
      [DataNames("BANCODESTEXT")]
      [SugarColumn(ColumnName = "bancodestext", ColumnDescription = "Banco destino extranjero", Length = 225)]
      public string BANCODESTEXT {get { return this._BANCODESTEXT; } set { this._BANCODESTEXT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IDFISCAL
      /// </summary>
      [DataNames("IDFISCAL")]
      [SugarColumn(ColumnName = "idfiscal", ColumnDescription = "Identificador Fiscal", Length = 40)]
      public string IDFISCAL {get { return this._IDFISCAL; } set { this._IDFISCAL = value; this.OnPropertyChanged(); }}
   }
}