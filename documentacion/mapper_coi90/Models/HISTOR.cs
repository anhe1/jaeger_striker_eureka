﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///HISTOR
   ///</summary>
   [SugarTable("histor")]
   public partial class HISTOR
   {
      public HISTOR(){
      }

      private int? _MONEDA;
      private DateTime? _FECHA;
      private double? _TIPCAMBIO;
      
      /// <summary>
      /// obtener o establecer MONEDA
      /// </summary>
      [DataNames("MONEDA")]
      [SugarColumn(ColumnName = "moneda", ColumnDescription = "Moneda")]
      public int? MONEDA {get { return this._MONEDA; } set { this._MONEDA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHA
      /// </summary>
      [DataNames("FECHA")]
      [SugarColumn(ColumnName = "fecha", ColumnDescription = "Fecha registro")]
      public DateTime? FECHA {get { return this._FECHA; } set { this._FECHA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPCAMBIO
      /// </summary>
      [DataNames("TIPCAMBIO")]
      [SugarColumn(ColumnName = "tipcambio", ColumnDescription = "Tipo de cambio")]
      public double? TIPCAMBIO {get { return this._TIPCAMBIO; } set { this._TIPCAMBIO = value; this.OnPropertyChanged(); }}
   }
}
