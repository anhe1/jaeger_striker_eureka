﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///RANGOS
   ///</summary>
   [SugarTable("rangos")]
   public partial class RANGOS
   {
      public RANGOS(){
      }

      private int _RANGO;
      private string _DESCRIP;

      /// <summary>
      /// obtener o establecer RANGO
      /// </summary>
      [DataNames("RANGO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "rango", ColumnDescription = "Rango", IsNullable = false)]
      public int RANGO {get { return this._RANGO; } set { this._RANGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DESCRIP
      /// </summary>
      [DataNames("DESCRIP")]
      [SugarColumn(ColumnName = "descrip", ColumnDescription = "Descripcion", Length = 100)]
      public string DESCRIP {get { return this._DESCRIP; } set { this._DESCRIP = value; this.OnPropertyChanged(); }}
   }
}