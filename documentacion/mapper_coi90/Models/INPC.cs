﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///INPC
   ///</summary>
   [SugarTable("inpc")]
   public partial class INPC
   {
      public INPC(){
      }

      private int _EJERCICIO;
      private double? _MES1;
      private double? _MES2;
      private double? _MES3;
      private double? _MES4;
      private double? _MES5;
      private double? _MES6;
      private double? _MES7;
      private double? _MES8;
      private double? _MES9;
      private double? _MES10;
      private double? _MES11;
      private double? _MES12;

      /// <summary>
      /// obtener o establecer EJERCICIO
      /// </summary>
      [DataNames("EJERCICIO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "ejercicio", ColumnDescription = "Ejercicio", IsNullable = false)]
      public int EJERCICIO {get { return this._EJERCICIO; } set { this._EJERCICIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MES1
      /// </summary>
      [DataNames("MES1")]
      [SugarColumn(ColumnName = "mes1", ColumnDescription = "INCP de Enero")]
      public double? MES1 {get { return this._MES1; } set { this._MES1 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MES2
      /// </summary>
      [DataNames("MES2")]
      [SugarColumn(ColumnName = "mes2", ColumnDescription = "INCP de Febrero")]
      public double? MES2 {get { return this._MES2; } set { this._MES2 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MES3
      /// </summary>
      [DataNames("MES3")]
      [SugarColumn(ColumnName = "mes3", ColumnDescription = "INCP de Marzo")]
      public double? MES3 {get { return this._MES3; } set { this._MES3 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MES4
      /// </summary>
      [DataNames("MES4")]
      [SugarColumn(ColumnName = "mes4", ColumnDescription = "INCP de Abril")]
      public double? MES4 {get { return this._MES4; } set { this._MES4 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MES5
      /// </summary>
      [DataNames("MES5")]
      [SugarColumn(ColumnName = "mes5", ColumnDescription = "INCP de Mayo")]
      public double? MES5 {get { return this._MES5; } set { this._MES5 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MES6
      /// </summary>
      [DataNames("MES6")]
      [SugarColumn(ColumnName = "mes6", ColumnDescription = "INCP de Junio")]
      public double? MES6 {get { return this._MES6; } set { this._MES6 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MES7
      /// </summary>
      [DataNames("MES7")]
      [SugarColumn(ColumnName = "mes7", ColumnDescription = "INCP de Julio")]
      public double? MES7 {get { return this._MES7; } set { this._MES7 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MES8
      /// </summary>
      [DataNames("MES8")]
      [SugarColumn(ColumnName = "mes8", ColumnDescription = "INCP de Agosto")]
      public double? MES8 {get { return this._MES8; } set { this._MES8 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MES9
      /// </summary>
      [DataNames("MES9")]
      [SugarColumn(ColumnName = "mes9", ColumnDescription = "INCP de Septiembre")]
      public double? MES9 {get { return this._MES9; } set { this._MES9 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MES10
      /// </summary>
      [DataNames("MES10")]
      [SugarColumn(ColumnName = "mes10", ColumnDescription = "INCP de Octumbre")]
      public double? MES10 {get { return this._MES10; } set { this._MES10 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MES11
      /// </summary>
      [DataNames("MES11")]
      [SugarColumn(ColumnName = "mes11", ColumnDescription = "INCP de Noviembre")]
      public double? MES11 {get { return this._MES11; } set { this._MES11 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MES12
      /// </summary>
      [DataNames("MES12")]
      [SugarColumn(ColumnName = "mes12", ColumnDescription = "INCP de Diciembre")]
      public double? MES12 {get { return this._MES12; } set { this._MES12 = value; this.OnPropertyChanged(); }}
   }
}
