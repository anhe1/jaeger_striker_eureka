﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///DOCTOSRELPAGOS
   ///</summary>
   [SugarTable("doctosrelpagos")]
   public partial class DOCTOSRELPAGOS
   {
      public DOCTOSRELPAGOS(){
      }

      private string _UUIDTIMBRECFDI;
      private string _UUIDTIMBREPAGO;
      private int _IDPAGO;
      private string _SERIE;
      private string _FOLIO;
      private string _MONEDADR;
      private double? _TIPOCAMBIODR;
      private string _METODODEPAGODR;
      private int? _NUMPARCIALIDAD;
      private double? _IMPSALDOANT;
      private double? _IMPPAGADO;
      private double? _IMPSALDOINSOLUTO;

      /// <summary>
      /// obtener o establecer UUIDTIMBRECFDI
      /// </summary>
      [DataNames("UUIDTIMBRECFDI")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "uuidtimbrecfdi", ColumnDescription = "UUID del CFDI", IsNullable = false, Length = 36)]
      public string UUIDTIMBRECFDI {get { return this._UUIDTIMBRECFDI; } set { this._UUIDTIMBRECFDI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer UUIDTIMBREPAGO
      /// </summary>
      [DataNames("UUIDTIMBREPAGO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "uuidtimbrepago", ColumnDescription = "UUID de complemento de pago", IsNullable = false, Length = 36)]
      public string UUIDTIMBREPAGO {get { return this._UUIDTIMBREPAGO; } set { this._UUIDTIMBREPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IDPAGO
      /// </summary>
      [DataNames("IDPAGO")]
      [SugarColumn(ColumnName = "idpago", ColumnDescription = "Id incremental del pago", IsNullable = false)]
      public int IDPAGO {get { return this._IDPAGO; } set { this._IDPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SERIE
      /// </summary>
      [DataNames("SERIE")]
      [SugarColumn(ColumnName = "serie", ColumnDescription = "Serie del CFDI", Length = 25)]
      public string SERIE {get { return this._SERIE; } set { this._SERIE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO
      /// </summary>
      [DataNames("FOLIO")]
      [SugarColumn(ColumnName = "folio", ColumnDescription = "Folio del CFDI", Length = 40)]
      public string FOLIO {get { return this._FOLIO; } set { this._FOLIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONEDADR
      /// </summary>
      [DataNames("MONEDADR")]
      [SugarColumn(ColumnName = "monedadr", ColumnDescription = "Moneda", Length = 50)]
      public string MONEDADR {get { return this._MONEDADR; } set { this._MONEDADR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOCAMBIODR
      /// </summary>
      [DataNames("TIPOCAMBIODR")]
      [SugarColumn(ColumnName = "tipocambiodr", ColumnDescription = "Tipo de cambio")]
      public double? TIPOCAMBIODR {get { return this._TIPOCAMBIODR; } set { this._TIPOCAMBIODR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer METODODEPAGODR
      /// </summary>
      [DataNames("METODODEPAGODR")]
      [SugarColumn(ColumnName = "metododepagodr", ColumnDescription = "Metodo de pago", Length = 50)]
      public string METODODEPAGODR {get { return this._METODODEPAGODR; } set { this._METODODEPAGODR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMPARCIALIDAD
      /// </summary>
      [DataNames("NUMPARCIALIDAD")]
      [SugarColumn(ColumnName = "numparcialidad", ColumnDescription = "Nombre del banco extranjero")]
      public int? NUMPARCIALIDAD {get { return this._NUMPARCIALIDAD; } set { this._NUMPARCIALIDAD = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IMPSALDOANT
      /// </summary>
      [DataNames("IMPSALDOANT")]
      [SugarColumn(ColumnName = "impsaldoant", ColumnDescription = "Importe del saldo anterior")]
      public double? IMPSALDOANT {get { return this._IMPSALDOANT; } set { this._IMPSALDOANT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IMPPAGADO
      /// </summary>
      [DataNames("IMPPAGADO")]
      [SugarColumn(ColumnName = "imppagado", ColumnDescription = "Importe pagado")]
      public double? IMPPAGADO {get { return this._IMPPAGADO; } set { this._IMPPAGADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IMPSALDOINSOLUTO
      /// </summary>
      [DataNames("IMPSALDOINSOLUTO")]
      [SugarColumn(ColumnName = "impsaldoinsoluto", ColumnDescription = "Importe insoluto(tdCFDI:t_importe)")]
      public double? IMPSALDOINSOLUTO {get { return this._IMPSALDOINSOLUTO; } set { this._IMPSALDOINSOLUTO = value; this.OnPropertyChanged(); }}
   }
}
