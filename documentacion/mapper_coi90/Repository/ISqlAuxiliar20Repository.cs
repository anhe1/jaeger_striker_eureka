    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlAuxiliar20Repository : IGenericRepository<AUXILIAR20> {}

        public class SqlFbAuxiliar20Repository : MasterRepository, ISqlAuxiliar20Repository {

    	    public SqlFbAuxiliar20Repository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(AUXILIAR20 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO auxiliar20 (tipo_poli, num_poliz, num_part, periodo, ejercicio, num_cta, fecha_pol, concep_po, debe_haber, montomov, numdepto, tipcambio, contrapar, orden, ccostos, cgrupos, idinfadipar, iduuid) 
                    			VALUES (@tipo_poli, @num_poliz, @num_part, @periodo, @ejercicio, @num_cta, @fecha_pol, @concep_po, @debe_haber, @montomov, @numdepto, @tipcambio, @contrapar, @orden, @ccostos, @cgrupos, @idinfadipar, @iduuid)"
                };
                item.TIPO_POLI = this.Max("TIPO_POLI");
                sqlCommand.Parameters.AddWithValue("@tipo_poli", item.TIPO_POLI);
                sqlCommand.Parameters.AddWithValue("@num_poliz", item.NUM_POLIZ);
                sqlCommand.Parameters.AddWithValue("@num_part", item.NUM_PART);
                sqlCommand.Parameters.AddWithValue("@periodo", item.PERIODO);
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@fecha_pol", item.FECHA_POL);
                sqlCommand.Parameters.AddWithValue("@concep_po", item.CONCEP_PO);
                sqlCommand.Parameters.AddWithValue("@debe_haber", item.DEBE_HABER);
                sqlCommand.Parameters.AddWithValue("@montomov", item.MONTOMOV);
                sqlCommand.Parameters.AddWithValue("@numdepto", item.NUMDEPTO);
                sqlCommand.Parameters.AddWithValue("@tipcambio", item.TIPCAMBIO);
                sqlCommand.Parameters.AddWithValue("@contrapar", item.CONTRAPAR);
                sqlCommand.Parameters.AddWithValue("@orden", item.ORDEN);
                sqlCommand.Parameters.AddWithValue("@ccostos", item.CCOSTOS);
                sqlCommand.Parameters.AddWithValue("@cgrupos", item.CGRUPOS);
                sqlCommand.Parameters.AddWithValue("@idinfadipar", item.IDINFADIPAR);
                sqlCommand.Parameters.AddWithValue("@iduuid", item.IDUUID);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(AUXILIAR20 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE auxiliar20 
                    			SET num_poliz = @num_poliz, num_part = @num_part, periodo = @periodo, ejercicio = @ejercicio, num_cta = @num_cta, fecha_pol = @fecha_pol, concep_po = @concep_po, debe_haber = @debe_haber, montomov = @montomov, numdepto = @numdepto, tipcambio = @tipcambio, contrapar = @contrapar, orden = @orden, ccostos = @ccostos, cgrupos = @cgrupos, idinfadipar = @idinfadipar, iduuid = @iduuid
                    			WHERE tipo_poli = @tipo_poli;"
                };
                sqlCommand.Parameters.AddWithValue("@tipo_poli", item.TIPO_POLI);
                sqlCommand.Parameters.AddWithValue("@num_poliz", item.NUM_POLIZ);
                sqlCommand.Parameters.AddWithValue("@num_part", item.NUM_PART);
                sqlCommand.Parameters.AddWithValue("@periodo", item.PERIODO);
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@fecha_pol", item.FECHA_POL);
                sqlCommand.Parameters.AddWithValue("@concep_po", item.CONCEP_PO);
                sqlCommand.Parameters.AddWithValue("@debe_haber", item.DEBE_HABER);
                sqlCommand.Parameters.AddWithValue("@montomov", item.MONTOMOV);
                sqlCommand.Parameters.AddWithValue("@numdepto", item.NUMDEPTO);
                sqlCommand.Parameters.AddWithValue("@tipcambio", item.TIPCAMBIO);
                sqlCommand.Parameters.AddWithValue("@contrapar", item.CONTRAPAR);
                sqlCommand.Parameters.AddWithValue("@orden", item.ORDEN);
                sqlCommand.Parameters.AddWithValue("@ccostos", item.CCOSTOS);
                sqlCommand.Parameters.AddWithValue("@cgrupos", item.CGRUPOS);
                sqlCommand.Parameters.AddWithValue("@idinfadipar", item.IDINFADIPAR);
                sqlCommand.Parameters.AddWithValue("@iduuid", item.IDUUID);
                return this.ExecuteScalar(sqlCommand);
            }

            public AUXILIAR20 GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM auxiliar20 WHERE tipo_poli = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<AUXILIAR20>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<AUXILIAR20> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM auxiliar20")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<AUXILIAR20>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }