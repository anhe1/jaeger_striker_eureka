    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlComplementospagosRepository : IGenericRepository<COMPLEMENTOSPAGOS> {}

        public class SqlFbComplementospagosRepository : MasterRepository, ISqlComplementospagosRepository {

    	    public SqlFbComplementospagosRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(COMPLEMENTOSPAGOS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO complementospagos (uuidtimbrepago, serie, folio, fecha, rfcemisor, nombreemisor, monto, tipo_poli, num_poliz, periodo, ejercicio, rutaxml, xml) 
                    			VALUES (@uuidtimbrepago, @serie, @folio, @fecha, @rfcemisor, @nombreemisor, @monto, @tipo_poli, @num_poliz, @periodo, @ejercicio, @rutaxml, @xml)"
                };
                item.UUIDTIMBREPAGO = this.Max("UUIDTIMBREPAGO");
                sqlCommand.Parameters.AddWithValue("@uuidtimbrepago", item.UUIDTIMBREPAGO);
                sqlCommand.Parameters.AddWithValue("@serie", item.SERIE);
                sqlCommand.Parameters.AddWithValue("@folio", item.FOLIO);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@rfcemisor", item.RFCEMISOR);
                sqlCommand.Parameters.AddWithValue("@nombreemisor", item.NOMBREEMISOR);
                sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
                sqlCommand.Parameters.AddWithValue("@tipo_poli", item.TIPO_POLI);
                sqlCommand.Parameters.AddWithValue("@num_poliz", item.NUM_POLIZ);
                sqlCommand.Parameters.AddWithValue("@periodo", item.PERIODO);
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@rutaxml", item.RUTAXML);
                sqlCommand.Parameters.AddWithValue("@xml", item.XML);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(COMPLEMENTOSPAGOS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE complementospagos 
                    			SET serie = @serie, folio = @folio, fecha = @fecha, rfcemisor = @rfcemisor, nombreemisor = @nombreemisor, monto = @monto, tipo_poli = @tipo_poli, num_poliz = @num_poliz, periodo = @periodo, ejercicio = @ejercicio, rutaxml = @rutaxml, xml = @xml 
                    			WHERE uuidtimbrepago = @uuidtimbrepago;"
                };
                sqlCommand.Parameters.AddWithValue("@uuidtimbrepago", item.UUIDTIMBREPAGO);
                sqlCommand.Parameters.AddWithValue("@serie", item.SERIE);
                sqlCommand.Parameters.AddWithValue("@folio", item.FOLIO);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@rfcemisor", item.RFCEMISOR);
                sqlCommand.Parameters.AddWithValue("@nombreemisor", item.NOMBREEMISOR);
                sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
                sqlCommand.Parameters.AddWithValue("@tipo_poli", item.TIPO_POLI);
                sqlCommand.Parameters.AddWithValue("@num_poliz", item.NUM_POLIZ);
                sqlCommand.Parameters.AddWithValue("@periodo", item.PERIODO);
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@rutaxml", item.RUTAXML);
                sqlCommand.Parameters.AddWithValue("@xml", item.XML);
                return this.ExecuteScalar(sqlCommand);
            }

            public COMPLEMENTOSPAGOS GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM complementospagos WHERE uuidtimbrepago = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<COMPLEMENTOSPAGOS>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<COMPLEMENTOSPAGOS> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM complementospagos")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<COMPLEMENTOSPAGOS>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }