    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlAdmperRepository : IGenericRepository<ADMPER> {}

        public class SqlFbAdmperRepository : MasterRepository, ISqlAdmperRepository {

    	    public SqlFbAdmperRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(ADMPER item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO admper (periodo, ejercicio, cerrado, ordenado, reqtrasp, ctpolizas) 
                    			VALUES (@periodo, @ejercicio, @cerrado, @ordenado, @reqtrasp, @ctpolizas)"
                };
                item.PERIODO = this.Max("PERIODO");
                sqlCommand.Parameters.AddWithValue("@periodo", item.PERIODO);
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@cerrado", item.CERRADO);
                sqlCommand.Parameters.AddWithValue("@ordenado", item.ORDENADO);
                sqlCommand.Parameters.AddWithValue("@reqtrasp", item.REQTRASP);
                sqlCommand.Parameters.AddWithValue("@ctpolizas", item.CTPOLIZAS);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(ADMPER item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE admper 
                    			SET ejercicio = @ejercicio, cerrado = @cerrado, ordenado = @ordenado, reqtrasp = @reqtrasp, ctpolizas = @ctpolizas 
                    			WHERE periodo = @periodo;"
                };
                sqlCommand.Parameters.AddWithValue("@periodo", item.PERIODO);
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@cerrado", item.CERRADO);
                sqlCommand.Parameters.AddWithValue("@ordenado", item.ORDENADO);
                sqlCommand.Parameters.AddWithValue("@reqtrasp", item.REQTRASP);
                sqlCommand.Parameters.AddWithValue("@ctpolizas", item.CTPOLIZAS);
                return this.ExecuteScalar(sqlCommand);
            }

            public ADMPER GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM admper WHERE periodo = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<ADMPER>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<ADMPER> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM admper")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<ADMPER>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }