    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlRfcterRepository : IGenericRepository<RFCTER> {}

        public class SqlFbRfcterRepository : MasterRepository, ISqlRfcterRepository {

    	    public SqlFbRfcterRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(RFCTER item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO rfcter (rfcidfisc, tipo, nombre, pais, nacional, tipoop, clavesae, cuentagas, esmonto, monporc, ivadefault, porcentajeiva, porcentajeisr, incluyeiva, esprov, ctabancaria, banco, frmpago, bancoextranjero, esidfiscal, plantillapoldin, esrfclistanegra, cuentacliente) 
                    			VALUES (@rfcidfisc, @tipo, @nombre, @pais, @nacional, @tipoop, @clavesae, @cuentagas, @esmonto, @monporc, @ivadefault, @porcentajeiva, @porcentajeisr, @incluyeiva, @esprov, @ctabancaria, @banco, @frmpago, @bancoextranjero, @esidfiscal, @plantillapoldin, @esrfclistanegra, @cuentacliente)"
                };
                sqlCommand.Parameters.AddWithValue("@rfcidfisc", item.RFCIDFISC);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@pais", item.PAIS);
                sqlCommand.Parameters.AddWithValue("@nacional", item.NACIONAL);
                sqlCommand.Parameters.AddWithValue("@tipoop", item.TIPOOP);
                sqlCommand.Parameters.AddWithValue("@clavesae", item.CLAVESAE);
                sqlCommand.Parameters.AddWithValue("@cuentagas", item.CUENTAGAS);
                sqlCommand.Parameters.AddWithValue("@esmonto", item.ESMONTO);
                sqlCommand.Parameters.AddWithValue("@monporc", item.MONPORC);
                sqlCommand.Parameters.AddWithValue("@ivadefault", item.IVADEFAULT);
                sqlCommand.Parameters.AddWithValue("@porcentajeiva", item.PORCENTAJEIVA);
                sqlCommand.Parameters.AddWithValue("@porcentajeisr", item.PORCENTAJEISR);
                sqlCommand.Parameters.AddWithValue("@incluyeiva", item.INCLUYEIVA);
                sqlCommand.Parameters.AddWithValue("@esprov", item.ESPROV);
                sqlCommand.Parameters.AddWithValue("@ctabancaria", item.CTABANCARIA);
                sqlCommand.Parameters.AddWithValue("@banco", item.BANCO);
                sqlCommand.Parameters.AddWithValue("@frmpago", item.FRMPAGO);
                sqlCommand.Parameters.AddWithValue("@bancoextranjero", item.BANCOEXTRANJERO);
                sqlCommand.Parameters.AddWithValue("@esidfiscal", item.ESIDFISCAL);
                sqlCommand.Parameters.AddWithValue("@plantillapoldin", item.PLANTILLAPOLDIN);
                sqlCommand.Parameters.AddWithValue("@esrfclistanegra", item.ESRFCLISTANEGRA);
                sqlCommand.Parameters.AddWithValue("@cuentacliente", item.CUENTACLIENTE);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(RFCTER item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE rfcter 
                    			SET tipo = @tipo, nombre = @nombre, pais = @pais, nacional = @nacional, tipoop = @tipoop, clavesae = @clavesae, cuentagas = @cuentagas, esmonto = @esmonto, monporc = @monporc, ivadefault = @ivadefault, porcentajeiva = @porcentajeiva, porcentajeisr = @porcentajeisr, incluyeiva = @incluyeiva, esprov = @esprov, ctabancaria = @ctabancaria, banco = @banco, frmpago = @frmpago, bancoextranjero = @bancoextranjero, esidfiscal = @esidfiscal, plantillapoldin = @plantillapoldin, esrfclistanegra = @esrfclistanegra, cuentacliente = @cuentacliente 
                    			WHERE rfcidfisc = @rfcidfisc;"
                };
                sqlCommand.Parameters.AddWithValue("@rfcidfisc", item.RFCIDFISC);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@pais", item.PAIS);
                sqlCommand.Parameters.AddWithValue("@nacional", item.NACIONAL);
                sqlCommand.Parameters.AddWithValue("@tipoop", item.TIPOOP);
                sqlCommand.Parameters.AddWithValue("@clavesae", item.CLAVESAE);
                sqlCommand.Parameters.AddWithValue("@cuentagas", item.CUENTAGAS);
                sqlCommand.Parameters.AddWithValue("@esmonto", item.ESMONTO);
                sqlCommand.Parameters.AddWithValue("@monporc", item.MONPORC);
                sqlCommand.Parameters.AddWithValue("@ivadefault", item.IVADEFAULT);
                sqlCommand.Parameters.AddWithValue("@porcentajeiva", item.PORCENTAJEIVA);
                sqlCommand.Parameters.AddWithValue("@porcentajeisr", item.PORCENTAJEISR);
                sqlCommand.Parameters.AddWithValue("@incluyeiva", item.INCLUYEIVA);
                sqlCommand.Parameters.AddWithValue("@esprov", item.ESPROV);
                sqlCommand.Parameters.AddWithValue("@ctabancaria", item.CTABANCARIA);
                sqlCommand.Parameters.AddWithValue("@banco", item.BANCO);
                sqlCommand.Parameters.AddWithValue("@frmpago", item.FRMPAGO);
                sqlCommand.Parameters.AddWithValue("@bancoextranjero", item.BANCOEXTRANJERO);
                sqlCommand.Parameters.AddWithValue("@esidfiscal", item.ESIDFISCAL);
                sqlCommand.Parameters.AddWithValue("@plantillapoldin", item.PLANTILLAPOLDIN);
                sqlCommand.Parameters.AddWithValue("@esrfclistanegra", item.ESRFCLISTANEGRA);
                sqlCommand.Parameters.AddWithValue("@cuentacliente", item.CUENTACLIENTE);
                return this.ExecuteScalar(sqlCommand);
            }

            public RFCTER GetById(string id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM rfcter WHERE rfcidfisc = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<RFCTER>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<RFCTER> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM rfcter")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<RFCTER>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }