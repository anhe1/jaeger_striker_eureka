    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlDoctosfiscalesRepository : IGenericRepository<DOCTOSFISCALES> {}

        public class SqlFbDoctosfiscalesRepository : MasterRepository, ISqlDoctosfiscalesRepository {

    	    public SqlFbDoctosfiscalesRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(DOCTOSFISCALES item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO doctosfiscales (uuidtimbrecfdi, rutaarchivo, total, subtotal, rfcemisor, rfcreceptor, fechaemision, serie, folio, versioncfdi, lugarexpedicion, moneda, numctapago, tipocambio, descuento, formapago, metodopago, totalimpuestostrasladados, totalimpuestosretenidos, nombreemisor, nombrereceptor, regimenfiscal, usocfdi, sellocfdi, nocertificado, nocertificadotimbre, condicionesdepago, sello, tipodecomprobante, xml) 
                    			VALUES (@uuidtimbrecfdi, @rutaarchivo, @total, @subtotal, @rfcemisor, @rfcreceptor, @fechaemision, @serie, @folio, @versioncfdi, @lugarexpedicion, @moneda, @numctapago, @tipocambio, @descuento, @formapago, @metodopago, @totalimpuestostrasladados, @totalimpuestosretenidos, @nombreemisor, @nombrereceptor, @regimenfiscal, @usocfdi, @sellocfdi, @nocertificado, @nocertificadotimbre, @condicionesdepago, @sello, @tipodecomprobante, @xml)"
                };
                item.UUIDTIMBRECFDI = this.Max("UUIDTIMBRECFDI");
                sqlCommand.Parameters.AddWithValue("@uuidtimbrecfdi", item.UUIDTIMBRECFDI);
                sqlCommand.Parameters.AddWithValue("@rutaarchivo", item.RUTAARCHIVO);
                sqlCommand.Parameters.AddWithValue("@total", item.TOTAL);
                sqlCommand.Parameters.AddWithValue("@subtotal", item.SUBTOTAL);
                sqlCommand.Parameters.AddWithValue("@rfcemisor", item.RFCEMISOR);
                sqlCommand.Parameters.AddWithValue("@rfcreceptor", item.RFCRECEPTOR);
                sqlCommand.Parameters.AddWithValue("@fechaemision", item.FECHAEMISION);
                sqlCommand.Parameters.AddWithValue("@serie", item.SERIE);
                sqlCommand.Parameters.AddWithValue("@folio", item.FOLIO);
                sqlCommand.Parameters.AddWithValue("@versioncfdi", item.VERSIONCFDI);
                sqlCommand.Parameters.AddWithValue("@lugarexpedicion", item.LUGAREXPEDICION);
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                sqlCommand.Parameters.AddWithValue("@numctapago", item.NUMCTAPAGO);
                sqlCommand.Parameters.AddWithValue("@tipocambio", item.TIPOCAMBIO);
                sqlCommand.Parameters.AddWithValue("@descuento", item.DESCUENTO);
                sqlCommand.Parameters.AddWithValue("@formapago", item.FORMAPAGO);
                sqlCommand.Parameters.AddWithValue("@metodopago", item.METODOPAGO);
                sqlCommand.Parameters.AddWithValue("@totalimpuestostrasladados", item.TOTALIMPUESTOSTRASLADADOS);
                sqlCommand.Parameters.AddWithValue("@totalimpuestosretenidos", item.TOTALIMPUESTOSRETENIDOS);
                sqlCommand.Parameters.AddWithValue("@nombreemisor", item.NOMBREEMISOR);
                sqlCommand.Parameters.AddWithValue("@nombrereceptor", item.NOMBRERECEPTOR);
                sqlCommand.Parameters.AddWithValue("@regimenfiscal", item.REGIMENFISCAL);
                sqlCommand.Parameters.AddWithValue("@usocfdi", item.USOCFDI);
                sqlCommand.Parameters.AddWithValue("@sellocfdi", item.SELLOCFDI);
                sqlCommand.Parameters.AddWithValue("@nocertificado", item.NOCERTIFICADO);
                sqlCommand.Parameters.AddWithValue("@nocertificadotimbre", item.NOCERTIFICADOTIMBRE);
                sqlCommand.Parameters.AddWithValue("@condicionesdepago", item.CONDICIONESDEPAGO);
                sqlCommand.Parameters.AddWithValue("@sello", item.SELLO);
                sqlCommand.Parameters.AddWithValue("@tipodecomprobante", item.TIPODECOMPROBANTE);
                sqlCommand.Parameters.AddWithValue("@xml", item.XML);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(DOCTOSFISCALES item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE doctosfiscales 
                    			SET rutaarchivo = @rutaarchivo, total = @total, subtotal = @subtotal, rfcemisor = @rfcemisor, rfcreceptor = @rfcreceptor, fechaemision = @fechaemision, serie = @serie, folio = @folio, versioncfdi = @versioncfdi, lugarexpedicion = @lugarexpedicion, moneda = @moneda, numctapago = @numctapago, tipocambio = @tipocambio, descuento = @descuento, formapago = @formapago, metodopago = @metodopago, totalimpuestostrasladados = @totalimpuestostrasladados, totalimpuestosretenidos = @totalimpuestosretenidos, nombreemisor = @nombreemisor, nombrereceptor = @nombrereceptor, regimenfiscal = @regimenfiscal, usocfdi = @usocfdi, sellocfdi = @sellocfdi, nocertificado = @nocertificado, nocertificadotimbre = @nocertificadotimbre, condicionesdepago = @condicionesdepago, sello = @sello, tipodecomprobante = @tipodecomprobante, xml = @xml 
                    			WHERE uuidtimbrecfdi = @uuidtimbrecfdi;"
                };
                sqlCommand.Parameters.AddWithValue("@uuidtimbrecfdi", item.UUIDTIMBRECFDI);
                sqlCommand.Parameters.AddWithValue("@rutaarchivo", item.RUTAARCHIVO);
                sqlCommand.Parameters.AddWithValue("@total", item.TOTAL);
                sqlCommand.Parameters.AddWithValue("@subtotal", item.SUBTOTAL);
                sqlCommand.Parameters.AddWithValue("@rfcemisor", item.RFCEMISOR);
                sqlCommand.Parameters.AddWithValue("@rfcreceptor", item.RFCRECEPTOR);
                sqlCommand.Parameters.AddWithValue("@fechaemision", item.FECHAEMISION);
                sqlCommand.Parameters.AddWithValue("@serie", item.SERIE);
                sqlCommand.Parameters.AddWithValue("@folio", item.FOLIO);
                sqlCommand.Parameters.AddWithValue("@versioncfdi", item.VERSIONCFDI);
                sqlCommand.Parameters.AddWithValue("@lugarexpedicion", item.LUGAREXPEDICION);
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                sqlCommand.Parameters.AddWithValue("@numctapago", item.NUMCTAPAGO);
                sqlCommand.Parameters.AddWithValue("@tipocambio", item.TIPOCAMBIO);
                sqlCommand.Parameters.AddWithValue("@descuento", item.DESCUENTO);
                sqlCommand.Parameters.AddWithValue("@formapago", item.FORMAPAGO);
                sqlCommand.Parameters.AddWithValue("@metodopago", item.METODOPAGO);
                sqlCommand.Parameters.AddWithValue("@totalimpuestostrasladados", item.TOTALIMPUESTOSTRASLADADOS);
                sqlCommand.Parameters.AddWithValue("@totalimpuestosretenidos", item.TOTALIMPUESTOSRETENIDOS);
                sqlCommand.Parameters.AddWithValue("@nombreemisor", item.NOMBREEMISOR);
                sqlCommand.Parameters.AddWithValue("@nombrereceptor", item.NOMBRERECEPTOR);
                sqlCommand.Parameters.AddWithValue("@regimenfiscal", item.REGIMENFISCAL);
                sqlCommand.Parameters.AddWithValue("@usocfdi", item.USOCFDI);
                sqlCommand.Parameters.AddWithValue("@sellocfdi", item.SELLOCFDI);
                sqlCommand.Parameters.AddWithValue("@nocertificado", item.NOCERTIFICADO);
                sqlCommand.Parameters.AddWithValue("@nocertificadotimbre", item.NOCERTIFICADOTIMBRE);
                sqlCommand.Parameters.AddWithValue("@condicionesdepago", item.CONDICIONESDEPAGO);
                sqlCommand.Parameters.AddWithValue("@sello", item.SELLO);
                sqlCommand.Parameters.AddWithValue("@tipodecomprobante", item.TIPODECOMPROBANTE);
                sqlCommand.Parameters.AddWithValue("@xml", item.XML);
                return this.ExecuteScalar(sqlCommand);
            }

            public DOCTOSFISCALES GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM doctosfiscales WHERE uuidtimbrecfdi = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<DOCTOSFISCALES>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<DOCTOSFISCALES> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM doctosfiscales")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<DOCTOSFISCALES>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }