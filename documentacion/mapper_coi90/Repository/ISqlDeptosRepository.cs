    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlDeptosRepository : IGenericRepository<DEPTOS> {}

        public class SqlFbDeptosRepository : MasterRepository, ISqlDeptosRepository {

    	    public SqlFbDeptosRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(DEPTOS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO deptos (depto, descrip) 
                    			VALUES (@depto, @descrip)"
                };
                item.DEPTO = this.Max("DEPTO");
                sqlCommand.Parameters.AddWithValue("@depto", item.DEPTO);
                sqlCommand.Parameters.AddWithValue("@descrip", item.DESCRIP);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(DEPTOS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE deptos 
                    			SET descrip = @descrip 
                    			WHERE depto = @depto;"
                };
                sqlCommand.Parameters.AddWithValue("@depto", item.DEPTO);
                sqlCommand.Parameters.AddWithValue("@descrip", item.DESCRIP);
                return this.ExecuteScalar(sqlCommand);
            }

            public DEPTOS GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM deptos WHERE depto = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<DEPTOS>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<DEPTOS> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM deptos")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<DEPTOS>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }