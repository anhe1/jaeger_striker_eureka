    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlCcostosRepository : IGenericRepository<CCOSTOS> {}

        public class SqlFbCcostosRepository : MasterRepository, ISqlCcostosRepository {

    	    public SqlFbCcostosRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(CCOSTOS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO ccostos (id, descripcion) 
                    			VALUES (@id, @descripcion)"
                };
                item.ID = this.Max("ID");
                sqlCommand.Parameters.AddWithValue("@id", item.ID);
                sqlCommand.Parameters.AddWithValue("@descripcion", item.DESCRIPCION);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(CCOSTOS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE ccostos 
                    			SET descripcion = @descripcion 
                    			WHERE id = @id;"
                };
                sqlCommand.Parameters.AddWithValue("@id", item.ID);
                sqlCommand.Parameters.AddWithValue("@descripcion", item.DESCRIPCION);
                return this.ExecuteScalar(sqlCommand);
            }

            public CCOSTOS GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM ccostos WHERE id = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CCOSTOS>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<CCOSTOS> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM ccostos")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CCOSTOS>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }