using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Repository {

    public interface ISqlTipospolRepository : IGenericRepository<TIPOSPOL> {}

    public class SqlFbTipospolRepository : MasterRepository, ISqlTipospolRepository {

        public SqlFbTipospolRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

        public int Insert(TIPOSPOL item) {
            var sqlCommand = new FbCommand{
                CommandText = @"INSERT INTO tipospol (tipo, descrip, classat) 
                            VALUES (@tipo, @descrip, @classat)"
            };
            sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
            sqlCommand.Parameters.AddWithValue("@descrip", item.DESCRIP);
            sqlCommand.Parameters.AddWithValue("@classat", item.CLASSAT);
            return this.ExecuteScalar(sqlCommand);
        }

        public int Update(TIPOSPOL item) {
            var sqlCommand = new FbCommand{
                CommandText = @"UPDATE tipospol 
                            SET descrip = @descrip, classat = @classat 
                            WHERE tipo = @tipo;"
            };
            sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
            sqlCommand.Parameters.AddWithValue("@descrip", item.DESCRIP);
            sqlCommand.Parameters.AddWithValue("@classat", item.CLASSAT);
            return this.ExecuteScalar(sqlCommand);
        }

        public TIPOSPOL GetById(string id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM tipospol WHERE tipo = @id")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<TIPOSPOL>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<TIPOSPOL> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM tipospol")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<TIPOSPOL>();
            return mapper.Map(tabla).ToList();
        }

    }
}