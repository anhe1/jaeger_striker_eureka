    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlConceptoRepository : IGenericRepository<CONCEPTO> {}

        public class SqlFbConceptoRepository : MasterRepository, ISqlConceptoRepository {

    	    public SqlFbConceptoRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(CONCEPTO item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO concepto (concepto, descrip) 
                    			VALUES (@concepto, @descrip)"
                };
                sqlCommand.Parameters.AddWithValue("@concepto", item.CONCEPTO);
                sqlCommand.Parameters.AddWithValue("@descrip", item.DESCRIP);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(CONCEPTO item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE concepto 
                    			SET descrip = @descrip 
                    			WHERE concepto = @concepto;"
                };
                sqlCommand.Parameters.AddWithValue("@concepto", item.CONCEPTO);
                sqlCommand.Parameters.AddWithValue("@descrip", item.DESCRIP);
                return this.ExecuteScalar(sqlCommand);
            }

            public CONCEPTO GetById(string id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM concepto WHERE concepto = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CONCEPTO>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<CONCEPTO> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM concepto")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CONCEPTO>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }