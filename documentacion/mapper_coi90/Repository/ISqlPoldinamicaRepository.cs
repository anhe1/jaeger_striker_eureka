    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlPoldinamicaRepository : IGenericRepository<POLDINAMICA> {}

        public class SqlFbPoldinamicaRepository : MasterRepository, ISqlPoldinamicaRepository {

    	    public SqlFbPoldinamicaRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(POLDINAMICA item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO poldinamica (titulo, rutaarchivo, tipo, clas_usr) 
                    			VALUES (@titulo, @rutaarchivo, @tipo, @clas_usr)"
                };
                sqlCommand.Parameters.AddWithValue("@titulo", item.TITULO);
                sqlCommand.Parameters.AddWithValue("@rutaarchivo", item.RUTAARCHIVO);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@clas_usr", item.CLAS_USR);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(POLDINAMICA item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE poldinamica 
                    			SET rutaarchivo = @rutaarchivo, tipo = @tipo, clas_usr = @clas_usr
                    			WHERE titulo = @titulo;"
                };
                sqlCommand.Parameters.AddWithValue("@titulo", item.TITULO);
                sqlCommand.Parameters.AddWithValue("@rutaarchivo", item.RUTAARCHIVO);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@clas_usr", item.CLAS_USR);
                return this.ExecuteScalar(sqlCommand);
            }

            public POLDINAMICA GetById(string id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM poldinamica WHERE titulo = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<POLDINAMICA>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<POLDINAMICA> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM poldinamica")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<POLDINAMICA>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }