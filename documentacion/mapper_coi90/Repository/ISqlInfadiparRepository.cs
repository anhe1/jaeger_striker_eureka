using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Repository {

    public interface ISqlInfadiparRepository : IGenericRepository<INFADIPAR> {}

    public class SqlFbInfadiparRepository : MasterRepository, ISqlInfadiparRepository {

        public SqlFbInfadiparRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

        public int Insert(INFADIPAR item) {
            var sqlCommand = new FbCommand{
                CommandText = @"INSERT INTO infadipar (numreg, frmpago, numcheque, banco, ctaorig, fecha, monto, benef, rfc, bancodest, ctadest, bancoorigex, bancodestex, idfiscal) 
                            VALUES (@numreg, @frmpago, @numcheque, @banco, @ctaorig, @fecha, @monto, @benef, @rfc, @bancodest, @ctadest, @bancoorigex, @bancodestex, @idfiscal)"
            };
            item.NUMREG = this.Max("NUMREG");
            sqlCommand.Parameters.AddWithValue("@numreg", item.NUMREG);
            sqlCommand.Parameters.AddWithValue("@frmpago", item.FRMPAGO);
            sqlCommand.Parameters.AddWithValue("@numcheque", item.NUMCHEQUE);
            sqlCommand.Parameters.AddWithValue("@banco", item.BANCO);
            sqlCommand.Parameters.AddWithValue("@ctaorig", item.CTAORIG);
            sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
            sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
            sqlCommand.Parameters.AddWithValue("@benef", item.BENEF);
            sqlCommand.Parameters.AddWithValue("@rfc", item.RFC);
            sqlCommand.Parameters.AddWithValue("@bancodest", item.BANCODEST);
            sqlCommand.Parameters.AddWithValue("@ctadest", item.CTADEST);
            sqlCommand.Parameters.AddWithValue("@bancoorigex", item.BANCOORIGEXT);
            sqlCommand.Parameters.AddWithValue("@bancodestex", item.BANCODESTEXT);
            sqlCommand.Parameters.AddWithValue("@idfiscal", item.IDFISCAL);
            return this.ExecuteScalar(sqlCommand);
        }

        public int Update(INFADIPAR item) {
            var sqlCommand = new FbCommand{
                CommandText = @"UPDATE infadipar 
                            SET frmpago = @frmpago, numcheque = @numcheque, banco = @banco, ctaorig = @ctaorig, fecha = @fecha, monto = @monto, benef = @benef, rfc = @rfc, bancodest = @bancodest, ctadest = @ctadest, bancoorigex = @bancoorigex, bancodestex = @bancodestex, idfiscal = @idfiscal 
                            WHERE numreg = @numreg;"
            };
            sqlCommand.Parameters.AddWithValue("@numreg", item.NUMREG);
            sqlCommand.Parameters.AddWithValue("@frmpago", item.FRMPAGO);
            sqlCommand.Parameters.AddWithValue("@numcheque", item.NUMCHEQUE);
            sqlCommand.Parameters.AddWithValue("@banco", item.BANCO);
            sqlCommand.Parameters.AddWithValue("@ctaorig", item.CTAORIG);
            sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
            sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
            sqlCommand.Parameters.AddWithValue("@benef", item.BENEF);
            sqlCommand.Parameters.AddWithValue("@rfc", item.RFC);
            sqlCommand.Parameters.AddWithValue("@bancodest", item.BANCODEST);
            sqlCommand.Parameters.AddWithValue("@ctadest", item.CTADEST);
            sqlCommand.Parameters.AddWithValue("@bancoorigex", item.BANCOORIGEXT);
            sqlCommand.Parameters.AddWithValue("@bancodestex", item.BANCODESTEXT);
            sqlCommand.Parameters.AddWithValue("@idfiscal", item.IDFISCAL);
            return this.ExecuteScalar(sqlCommand)
        }

        public INFADIPAR GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM infadipar WHERE numreg = @id")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<INFADIPAR>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<INFADIPAR> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM infadipar")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<INFADIPAR>();
            return mapper.Map(tabla).ToList();
        }

    }
}