    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlPlantillaxrfcRepository : IGenericRepository<PLANTILLAXRFC> {}

        public class SqlFbPlantillaxrfcRepository : MasterRepository, ISqlPlantillaxrfcRepository {

    	    public SqlFbPlantillaxrfcRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(PLANTILLAXRFC item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO plantillaxrfc (rfc, clasplantilla, nombreplantilla, nombreplantilla2, usocfdi) 
                    			VALUES (@rfc, @clasplantilla, @nombreplantilla, @nombreplantilla2, @usocfdi)"
                };
                sqlCommand.Parameters.AddWithValue("@rfc", item.RFC);
                sqlCommand.Parameters.AddWithValue("@clasplantilla", item.CLASPLANTILLA);
                sqlCommand.Parameters.AddWithValue("@nombreplantilla", item.NOMBREPLANTILLA);
                sqlCommand.Parameters.AddWithValue("@nombreplantilla2", item.NOMBREPLANTILLA2);
                sqlCommand.Parameters.AddWithValue("@usocfdi", item.USOCFDI);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(PLANTILLAXRFC item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE plantillaxrfc 
                    			SET clasplantilla = @clasplantilla, nombreplantilla = @nombreplantilla, nombreplantilla2 = @nombreplantilla2, usocfdi = @usocfdi 
                    			WHERE rfc = @rfc;"
                };
                sqlCommand.Parameters.AddWithValue("@rfc", item.RFC);
                sqlCommand.Parameters.AddWithValue("@clasplantilla", item.CLASPLANTILLA);
                sqlCommand.Parameters.AddWithValue("@nombreplantilla", item.NOMBREPLANTILLA);
                sqlCommand.Parameters.AddWithValue("@nombreplantilla2", item.NOMBREPLANTILLA2);
                sqlCommand.Parameters.AddWithValue("@usocfdi", item.USOCFDI);
                return this.ExecuteScalar(sqlCommand);
            }

            public PLANTILLAXRFC GetById(string id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM plantillaxrfc WHERE rfc = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<PLANTILLAXRFC>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<PLANTILLAXRFC> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM plantillaxrfc")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<PLANTILLAXRFC>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }