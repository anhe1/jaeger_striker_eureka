    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlOtrosimpuestosingRepository : IGenericRepository<OTROSIMPUESTOSING> {}

        public class SqlFbOtrosimpuestosingRepository : MasterRepository, ISqlOtrosimpuestosingRepository {

    	    public SqlFbOtrosimpuestosingRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(OTROSIMPUESTOSING item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO otrosimpuestosing (idopeiet, numreg, nombre, tasa, monto, tipo, base, tipoimpuesto, aplicaic, tipofactor) 
                    			VALUES (@idopeiet, @numreg, @nombre, @tasa, @monto, @tipo, @base, @tipoimpuesto, @aplicaic, @tipofactor)"
                };
                item.IDOPEIET = this.Max("IDOPEIET");
                sqlCommand.Parameters.AddWithValue("@idopeiet", item.IDOPEIET);
                sqlCommand.Parameters.AddWithValue("@numreg", item.NUMREG);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@tasa", item.TASA);
                sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@base", item.BASE);
                sqlCommand.Parameters.AddWithValue("@tipoimpuesto", item.TIPOIMPUESTO);
                sqlCommand.Parameters.AddWithValue("@aplicaic", item.APLICAIC);
                sqlCommand.Parameters.AddWithValue("@tipofactor", item.TIPOFACTOR);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(OTROSIMPUESTOSING item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE otrosimpuestosing 
                    			SET numreg = @numreg, nombre = @nombre, tasa = @tasa, monto = @monto, tipo = @tipo, base = @base, tipoimpuesto = @tipoimpuesto, aplicaic = @aplicaic, tipofactor = @tipofactor 
                    			WHERE idopeiet = @idopeiet;"
                };
                sqlCommand.Parameters.AddWithValue("@idopeiet", item.IDOPEIET);
                sqlCommand.Parameters.AddWithValue("@numreg", item.NUMREG);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@tasa", item.TASA);
                sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@base", item.BASE);
                sqlCommand.Parameters.AddWithValue("@tipoimpuesto", item.TIPOIMPUESTO);
                sqlCommand.Parameters.AddWithValue("@aplicaic", item.APLICAIC);
                sqlCommand.Parameters.AddWithValue("@tipofactor", item.TIPOFACTOR);
                return this.ExecuteScalar(sqlCommand);
            }

            public OTROSIMPUESTOSING GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM otrosimpuestosing WHERE idopeiet = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<OTROSIMPUESTOSING>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<OTROSIMPUESTOSING> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM otrosimpuestosing")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<OTROSIMPUESTOSING>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }