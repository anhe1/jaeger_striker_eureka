    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlCgruposRepository : IGenericRepository<CGRUPOS> {}

        public class SqlFbCgruposRepository : MasterRepository, ISqlCgruposRepository {

    	    public SqlFbCgruposRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(CGRUPOS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO cgrupos (id, descripcion) 
                    			VALUES (@id, @descripcion)"
                };
                item.ID = this.Max("ID");
                sqlCommand.Parameters.AddWithValue("@id", item.ID);
                sqlCommand.Parameters.AddWithValue("@descripcion", item.DESCRIPCION);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(CGRUPOS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE cgrupos 
                    			SET descripcion = @descripcion 
                    			WHERE id = @id;"
                };
                sqlCommand.Parameters.AddWithValue("@id", item.ID);
                sqlCommand.Parameters.AddWithValue("@descripcion", item.DESCRIPCION);
                return this.ExecuteScalar(sqlCommand);
            }

            public CGRUPOS GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM cgrupos WHERE id = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CGRUPOS>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<CGRUPOS> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM cgrupos")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CGRUPOS>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }