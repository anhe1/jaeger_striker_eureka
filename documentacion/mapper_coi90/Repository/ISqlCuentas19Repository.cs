    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlCuentas19Repository : IGenericRepository<CUENTAS19> {}

        public class SqlFbCuentas19Repository : MasterRepository, ISqlCuentas19Repository {

    	    public SqlFbCuentas19Repository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(CUENTAS19 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO cuentas19 (num_cta, status, tipo, nombre, deptsino, bandmulti, bandajt, cta_papa, cta_raiz, nivel, cta_comp, naturaleza, rfc, codagrup, capturacheque, capturauuid, banco, ctabancaria, capcheqtipomov, noincluirxml, idfiscal, esflujodeefectivo, bancoextranjero, rfcflujo) 
                    			VALUES (@num_cta, @status, @tipo, @nombre, @deptsino, @bandmulti, @bandajt, @cta_papa, @cta_raiz, @nivel, @cta_comp, @naturaleza, @rfc, @codagrup, @capturacheque, @capturauuid, @banco, @ctabancaria, @capcheqtipomov, @noincluirxml, @idfiscal, @esflujodeefectivo, @bancoextranjero, @rfcflujo)"
                };
                item.NUM_CTA = this.Max("NUM_CTA");
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@status", item.STATUS);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@deptsino", item.DEPTSINO);
                sqlCommand.Parameters.AddWithValue("@bandmulti", item.BANDMULTI);
                sqlCommand.Parameters.AddWithValue("@bandajt", item.BANDAJT);
                sqlCommand.Parameters.AddWithValue("@cta_papa", item.CTA_PAPA);
                sqlCommand.Parameters.AddWithValue("@cta_raiz", item.CTA_RAIZ);
                sqlCommand.Parameters.AddWithValue("@nivel", item.NIVEL);
                sqlCommand.Parameters.AddWithValue("@cta_comp", item.CTA_COMP);
                sqlCommand.Parameters.AddWithValue("@naturaleza", item.NATURALEZA);
                sqlCommand.Parameters.AddWithValue("@rfc", item.RFC);
                sqlCommand.Parameters.AddWithValue("@codagrup", item.CODAGRUP);
                sqlCommand.Parameters.AddWithValue("@capturacheque", item.CAPTURACHEQUE);
                sqlCommand.Parameters.AddWithValue("@capturauuid", item.CAPTURAUUID);
                sqlCommand.Parameters.AddWithValue("@banco", item.BANCO);
                sqlCommand.Parameters.AddWithValue("@ctabancaria", item.CTABANCARIA);
                sqlCommand.Parameters.AddWithValue("@capcheqtipomov", item.CAPCHEQTIPOMOV);
                sqlCommand.Parameters.AddWithValue("@noincluirxml", item.NOINCLUIRXML);
                sqlCommand.Parameters.AddWithValue("@idfiscal", item.IDFISCAL);
                sqlCommand.Parameters.AddWithValue("@esflujodeefectivo", item.ESFLUJODEEFECTIVO);
                sqlCommand.Parameters.AddWithValue("@bancoextranjero", item.BANCOEXTRANJERO);
                sqlCommand.Parameters.AddWithValue("@rfcflujo", item.RFCFLUJO);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(CUENTAS19 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE cuentas19 
                    			SET status = @status, tipo = @tipo, nombre = @nombre, deptsino = @deptsino, bandmulti = @bandmulti, bandajt = @bandajt, cta_papa = @cta_papa, cta_raiz = @cta_raiz, nivel = @nivel, cta_comp = @cta_comp, naturaleza = @naturaleza, rfc = @rfc, codagrup = @codagrup, capturacheque = @capturacheque, capturauuid = @capturauuid, banco = @banco, ctabancaria = @ctabancaria, capcheqtipomov = @capcheqtipomov, noincluirxml = @noincluirxml, idfiscal = @idfiscal, esflujodeefectivo = @esflujodeefectivo, bancoextranjero = @bancoextranjero, rfcflujo = @rfcflujo 
                    			WHERE num_cta = @num_cta;"
                };
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@status", item.STATUS);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@deptsino", item.DEPTSINO);
                sqlCommand.Parameters.AddWithValue("@bandmulti", item.BANDMULTI);
                sqlCommand.Parameters.AddWithValue("@bandajt", item.BANDAJT);
                sqlCommand.Parameters.AddWithValue("@cta_papa", item.CTA_PAPA);
                sqlCommand.Parameters.AddWithValue("@cta_raiz", item.CTA_RAIZ);
                sqlCommand.Parameters.AddWithValue("@nivel", item.NIVEL);
                sqlCommand.Parameters.AddWithValue("@cta_comp", item.CTA_COMP);
                sqlCommand.Parameters.AddWithValue("@naturaleza", item.NATURALEZA);
                sqlCommand.Parameters.AddWithValue("@rfc", item.RFC);
                sqlCommand.Parameters.AddWithValue("@codagrup", item.CODAGRUP);
                sqlCommand.Parameters.AddWithValue("@capturacheque", item.CAPTURACHEQUE);
                sqlCommand.Parameters.AddWithValue("@capturauuid", item.CAPTURAUUID);
                sqlCommand.Parameters.AddWithValue("@banco", item.BANCO);
                sqlCommand.Parameters.AddWithValue("@ctabancaria", item.CTABANCARIA);
                sqlCommand.Parameters.AddWithValue("@capcheqtipomov", item.CAPCHEQTIPOMOV);
                sqlCommand.Parameters.AddWithValue("@noincluirxml", item.NOINCLUIRXML);
                sqlCommand.Parameters.AddWithValue("@idfiscal", item.IDFISCAL);
                sqlCommand.Parameters.AddWithValue("@esflujodeefectivo", item.ESFLUJODEEFECTIVO);
                sqlCommand.Parameters.AddWithValue("@bancoextranjero", item.BANCOEXTRANJERO);
                sqlCommand.Parameters.AddWithValue("@rfcflujo", item.RFCFLUJO);
                return this.ExecuteScalar(sqlCommand);
            }

            public CUENTAS19 GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM cuentas19 WHERE num_cta = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CUENTAS19>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<CUENTAS19> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM cuentas19")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CUENTAS19>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }