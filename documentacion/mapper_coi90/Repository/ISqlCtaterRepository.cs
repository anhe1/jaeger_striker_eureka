    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlCtaterRepository : IGenericRepository<CTATER> {}

        public class SqlFbCtaterRepository : MasterRepository, ISqlCtaterRepository {

    	    public SqlFbCtaterRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(CTATER item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO ctater (cuenta, tipo, rfcidfisc, ivadefault, porcentajeiva, porcentajeisr, incluyeiva, idconcep, idconcepivaa) 
                    			VALUES (@cuenta, @tipo, @rfcidfisc, @ivadefault, @porcentajeiva, @porcentajeisr, @incluyeiva, @idconcep, @idconcepivaa)"
                };
                item.CUENTA = this.Max("CUENTA");
                sqlCommand.Parameters.AddWithValue("@cuenta", item.CUENTA);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@rfcidfisc", item.RFCIDFISC);
                sqlCommand.Parameters.AddWithValue("@ivadefault", item.IVADEFAULT);
                sqlCommand.Parameters.AddWithValue("@porcentajeiva", item.PORCENTAJEIVA);
                sqlCommand.Parameters.AddWithValue("@porcentajeisr", item.PORCENTAJEISR);
                sqlCommand.Parameters.AddWithValue("@incluyeiva", item.INCLUYEIVA);
                sqlCommand.Parameters.AddWithValue("@idconcep", item.IDCONCEP);
                sqlCommand.Parameters.AddWithValue("@idconcepivaa", item.IDCONCEPIVAA);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(CTATER item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE ctater 
                    			SET tipo = @tipo, rfcidfisc = @rfcidfisc, ivadefault = @ivadefault, porcentajeiva = @porcentajeiva, porcentajeisr = @porcentajeisr, incluyeiva = @incluyeiva, idconcep = @idconcep, idconcepivaa = @idconcepivaa 
                    			WHERE cuenta = @cuenta;"
                };
                sqlCommand.Parameters.AddWithValue("@cuenta", item.CUENTA);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@rfcidfisc", item.RFCIDFISC);
                sqlCommand.Parameters.AddWithValue("@ivadefault", item.IVADEFAULT);
                sqlCommand.Parameters.AddWithValue("@porcentajeiva", item.PORCENTAJEIVA);
                sqlCommand.Parameters.AddWithValue("@porcentajeisr", item.PORCENTAJEISR);
                sqlCommand.Parameters.AddWithValue("@incluyeiva", item.INCLUYEIVA);
                sqlCommand.Parameters.AddWithValue("@idconcep", item.IDCONCEP);
                sqlCommand.Parameters.AddWithValue("@idconcepivaa", item.IDCONCEPIVAA);
                return this.ExecuteScalar(sqlCommand);
            }

            public CTATER GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM ctater WHERE cuenta = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CTATER>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<CTATER> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM ctater")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CTATER>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }