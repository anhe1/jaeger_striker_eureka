    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlControlRepository : IGenericRepository<CONTROL> {}

        public class SqlFbControlRepository : MasterRepository, ISqlControlRepository {

    	    public SqlFbControlRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(CONTROL item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO control (ctconceptos, ctdeptos, ctmonedas, ctrangos, cttipactiv, ctregpol, verbasdat, ccostos, cgrupos, ctconcietu, ctconcivat, ctconcivaa, ctinfadipar, ctuuidcomp, ctidotrosimp, ctidotrosimping, ultimadesc_listanegra) 
                    			VALUES (@ctconceptos, @ctdeptos, @ctmonedas, @ctrangos, @cttipactiv, @ctregpol, @verbasdat, @ccostos, @cgrupos, @ctconcietu, @ctconcivat, @ctconcivaa, @ctinfadipar, @ctuuidcomp, @ctidotrosimp, @ctidotrosimping, @ultimadesc_listanegra)"
                };
                item.CTCONCEPTOS = this.Max("CTCONCEPTOS");
                sqlCommand.Parameters.AddWithValue("@ctconceptos", item.CTCONCEPTOS);
                sqlCommand.Parameters.AddWithValue("@ctdeptos", item.CTDEPTOS);
                sqlCommand.Parameters.AddWithValue("@ctmonedas", item.CTMONEDAS);
                sqlCommand.Parameters.AddWithValue("@ctrangos", item.CTRANGOS);
                sqlCommand.Parameters.AddWithValue("@cttipactiv", item.CTTIPACTIV);
                sqlCommand.Parameters.AddWithValue("@ctregpol", item.CTREGPOL);
                sqlCommand.Parameters.AddWithValue("@verbasdat", item.VERBASDAT);
                sqlCommand.Parameters.AddWithValue("@ccostos", item.CCOSTOS);
                sqlCommand.Parameters.AddWithValue("@cgrupos", item.CGRUPOS);
                sqlCommand.Parameters.AddWithValue("@ctconcietu", item.CTCONCIETU);
                sqlCommand.Parameters.AddWithValue("@ctconcivat", item.CTCONCIVAT);
                sqlCommand.Parameters.AddWithValue("@ctconcivaa", item.CTCONCIVAA);
                sqlCommand.Parameters.AddWithValue("@ctinfadipar", item.CTINFADIPAR);
                sqlCommand.Parameters.AddWithValue("@ctuuidcomp", item.CTUUIDCOMP);
                sqlCommand.Parameters.AddWithValue("@ctidotrosimp", item.CTIDOTROSIMP);
                sqlCommand.Parameters.AddWithValue("@ctidotrosimping", item.CTIDOTROSIMPING);
                sqlCommand.Parameters.AddWithValue("@ultimadesc_listanegra", item.ULTIMADESC_LISTANEGRA);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(CONTROL item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE control 
                    			SET ctdeptos = @ctdeptos, ctmonedas = @ctmonedas, ctrangos = @ctrangos, cttipactiv = @cttipactiv, ctregpol = @ctregpol, verbasdat = @verbasdat, ccostos = @ccostos, cgrupos = @cgrupos, ctconcietu = @ctconcietu, ctconcivat = @ctconcivat, ctconcivaa = @ctconcivaa, ctinfadipar = @ctinfadipar, ctuuidcomp = @ctuuidcomp, ctidotrosimp = @ctidotrosimp, ctidotrosimping = @ctidotrosimping, ultimadesc_listanegra = @ultimadesc_listanegra 
                    			WHERE ctconceptos = @ctconceptos;"
                };
                sqlCommand.Parameters.AddWithValue("@ctconceptos", item.CTCONCEPTOS);
                sqlCommand.Parameters.AddWithValue("@ctdeptos", item.CTDEPTOS);
                sqlCommand.Parameters.AddWithValue("@ctmonedas", item.CTMONEDAS);
                sqlCommand.Parameters.AddWithValue("@ctrangos", item.CTRANGOS);
                sqlCommand.Parameters.AddWithValue("@cttipactiv", item.CTTIPACTIV);
                sqlCommand.Parameters.AddWithValue("@ctregpol", item.CTREGPOL);
                sqlCommand.Parameters.AddWithValue("@verbasdat", item.VERBASDAT);
                sqlCommand.Parameters.AddWithValue("@ccostos", item.CCOSTOS);
                sqlCommand.Parameters.AddWithValue("@cgrupos", item.CGRUPOS);
                sqlCommand.Parameters.AddWithValue("@ctconcietu", item.CTCONCIETU);
                sqlCommand.Parameters.AddWithValue("@ctconcivat", item.CTCONCIVAT);
                sqlCommand.Parameters.AddWithValue("@ctconcivaa", item.CTCONCIVAA);
                sqlCommand.Parameters.AddWithValue("@ctinfadipar", item.CTINFADIPAR);
                sqlCommand.Parameters.AddWithValue("@ctuuidcomp", item.CTUUIDCOMP);
                sqlCommand.Parameters.AddWithValue("@ctidotrosimp", item.CTIDOTROSIMP);
                sqlCommand.Parameters.AddWithValue("@ctidotrosimping", item.CTIDOTROSIMPING);
                sqlCommand.Parameters.AddWithValue("@ultimadesc_listanegra", item.ULTIMADESC_LISTANEGRA);
                return this.ExecuteScalar(sqlCommand);
            }

            public CONTROL GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM control WHERE ctconceptos = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CONTROL>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<CONTROL> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM control")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CONTROL>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }