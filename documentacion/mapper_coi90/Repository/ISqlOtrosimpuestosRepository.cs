    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlOtrosimpuestosRepository : IGenericRepository<OTROSIMPUESTOS> {}

        public class SqlFbOtrosimpuestosRepository : MasterRepository, ISqlOtrosimpuestosRepository {

    	    public SqlFbOtrosimpuestosRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(OTROSIMPUESTOS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO otrosimpuestos (idopter, numreg, nombre, tasa, monto, tipo, base, tipoimpuesto, aplicadiot, tipofactor) 
                    			VALUES (@idopter, @numreg, @nombre, @tasa, @monto, @tipo, @base, @tipoimpuesto, @aplicadiot, @tipofactor)"
                };
                item.IDOPTER = this.Max("IDOPTER");
                sqlCommand.Parameters.AddWithValue("@idopter", item.IDOPTER);
                sqlCommand.Parameters.AddWithValue("@numreg", item.NUMREG);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@tasa", item.TASA);
                sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@base", item.BASE);
                sqlCommand.Parameters.AddWithValue("@tipoimpuesto", item.TIPOIMPUESTO);
                sqlCommand.Parameters.AddWithValue("@aplicadiot", item.APLICADIOT);
                sqlCommand.Parameters.AddWithValue("@tipofactor", item.TIPOFACTOR);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(OTROSIMPUESTOS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE otrosimpuestos 
                    			SET numreg = @numreg, nombre = @nombre, tasa = @tasa, monto = @monto, tipo = @tipo, base = @base, tipoimpuesto = @tipoimpuesto, aplicadiot = @aplicadiot, tipofactor = @tipofactor 
                    			WHERE idopter = @idopter;"
                };
                sqlCommand.Parameters.AddWithValue("@idopter", item.IDOPTER);
                sqlCommand.Parameters.AddWithValue("@numreg", item.NUMREG);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@tasa", item.TASA);
                sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@base", item.BASE);
                sqlCommand.Parameters.AddWithValue("@tipoimpuesto", item.TIPOIMPUESTO);
                sqlCommand.Parameters.AddWithValue("@aplicadiot", item.APLICADIOT);
                sqlCommand.Parameters.AddWithValue("@tipofactor", item.TIPOFACTOR);
                return this.ExecuteScalar(sqlCommand);
            }

            public OTROSIMPUESTOS GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM otrosimpuestos WHERE idopter = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<OTROSIMPUESTOS>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<OTROSIMPUESTOS> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM otrosimpuestos")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<OTROSIMPUESTOS>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }