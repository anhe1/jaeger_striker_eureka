    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlSaldos20Repository : IGenericRepository<SALDOS20> {}

        public class SqlFbSaldos20Repository : MasterRepository, ISqlSaldos20Repository {

    	    public SqlFbSaldos20Repository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(SALDOS20 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO saldos20 (num_cta, ejercicio, inicial, inicialex, cargo01, abono01, cargoex01, abonoex01, cargo02, abono02, cargoex02, abonoex02, cargo03, abono03, cargoex03, abonoex03, cargo04, abono04, cargoex04, abonoex04, cargo05, abono05, cargoex05, abonoex05, cargo06, abono06, cargoex06, abonoex06, cargo07, abono07, cargoex07, abonoex07, cargo08, abono08, cargoex08, abonoex08, cargo09, abono09, cargoex09, abonoex09, cargo10, abono10, cargoex10, abonoex10, cargo11, abono11, cargoex11, abonoex11, cargo12, abono12, cargoex12, abonoex12, cargo13, abono13, cargoex13, abonoex13, cargo14, abono14, cargoex14, abonoex14, mov01, mov02, mov03, mov04, mov05, mov06, mov07, mov08, mov09, mov10, mov11, mov12, mov13, mov14) 
                    			VALUES (@num_cta, @ejercicio, @inicial, @inicialex, @cargo01, @abono01, @cargoex01, @abonoex01, @cargo02, @abono02, @cargoex02, @abonoex02, @cargo03, @abono03, @cargoex03, @abonoex03, @cargo04, @abono04, @cargoex04, @abonoex04, @cargo05, @abono05, @cargoex05, @abonoex05, @cargo06, @abono06, @cargoex06, @abonoex06, @cargo07, @abono07, @cargoex07, @abonoex07, @cargo08, @abono08, @cargoex08, @abonoex08, @cargo09, @abono09, @cargoex09, @abonoex09, @cargo10, @abono10, @cargoex10, @abonoex10, @cargo11, @abono11, @cargoex11, @abonoex11, @cargo12, @abono12, @cargoex12, @abonoex12, @cargo13, @abono13, @cargoex13, @abonoex13, @cargo14, @abono14, @cargoex14, @abonoex14, @mov01, @mov02, @mov03, @mov04, @mov05, @mov06, @mov07, @mov08, @mov09, @mov10, @mov11, @mov12, @mov13, @mov14)"
                };
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@inicial", item.INICIAL);
                sqlCommand.Parameters.AddWithValue("@inicialex", item.INICIALEX);
                sqlCommand.Parameters.AddWithValue("@cargo01", item.CARGO01);
                sqlCommand.Parameters.AddWithValue("@abono01", item.ABONO01);
                sqlCommand.Parameters.AddWithValue("@cargoex01", item.CARGOEX01);
                sqlCommand.Parameters.AddWithValue("@abonoex01", item.ABONOEX01);
                sqlCommand.Parameters.AddWithValue("@cargo02", item.CARGO02);
                sqlCommand.Parameters.AddWithValue("@abono02", item.ABONO02);
                sqlCommand.Parameters.AddWithValue("@cargoex02", item.CARGOEX02);
                sqlCommand.Parameters.AddWithValue("@abonoex02", item.ABONOEX02);
                sqlCommand.Parameters.AddWithValue("@cargo03", item.CARGO03);
                sqlCommand.Parameters.AddWithValue("@abono03", item.ABONO03);
                sqlCommand.Parameters.AddWithValue("@cargoex03", item.CARGOEX03);
                sqlCommand.Parameters.AddWithValue("@abonoex03", item.ABONOEX03);
                sqlCommand.Parameters.AddWithValue("@cargo04", item.CARGO04);
                sqlCommand.Parameters.AddWithValue("@abono04", item.ABONO04);
                sqlCommand.Parameters.AddWithValue("@cargoex04", item.CARGOEX04);
                sqlCommand.Parameters.AddWithValue("@abonoex04", item.ABONOEX04);
                sqlCommand.Parameters.AddWithValue("@cargo05", item.CARGO05);
                sqlCommand.Parameters.AddWithValue("@abono05", item.ABONO05);
                sqlCommand.Parameters.AddWithValue("@cargoex05", item.CARGOEX05);
                sqlCommand.Parameters.AddWithValue("@abonoex05", item.ABONOEX05);
                sqlCommand.Parameters.AddWithValue("@cargo06", item.CARGO06);
                sqlCommand.Parameters.AddWithValue("@abono06", item.ABONO06);
                sqlCommand.Parameters.AddWithValue("@cargoex06", item.CARGOEX06);
                sqlCommand.Parameters.AddWithValue("@abonoex06", item.ABONOEX06);
                sqlCommand.Parameters.AddWithValue("@cargo07", item.CARGO07);
                sqlCommand.Parameters.AddWithValue("@abono07", item.ABONO07);
                sqlCommand.Parameters.AddWithValue("@cargoex07", item.CARGOEX07);
                sqlCommand.Parameters.AddWithValue("@abonoex07", item.ABONOEX07);
                sqlCommand.Parameters.AddWithValue("@cargo08", item.CARGO08);
                sqlCommand.Parameters.AddWithValue("@abono08", item.ABONO08);
                sqlCommand.Parameters.AddWithValue("@cargoex08", item.CARGOEX08);
                sqlCommand.Parameters.AddWithValue("@abonoex08", item.ABONOEX08);
                sqlCommand.Parameters.AddWithValue("@cargo09", item.CARGO09);
                sqlCommand.Parameters.AddWithValue("@abono09", item.ABONO09);
                sqlCommand.Parameters.AddWithValue("@cargoex09", item.CARGOEX09);
                sqlCommand.Parameters.AddWithValue("@abonoex09", item.ABONOEX09);
                sqlCommand.Parameters.AddWithValue("@cargo10", item.CARGO10);
                sqlCommand.Parameters.AddWithValue("@abono10", item.ABONO10);
                sqlCommand.Parameters.AddWithValue("@cargoex10", item.CARGOEX10);
                sqlCommand.Parameters.AddWithValue("@abonoex10", item.ABONOEX10);
                sqlCommand.Parameters.AddWithValue("@cargo11", item.CARGO11);
                sqlCommand.Parameters.AddWithValue("@abono11", item.ABONO11);
                sqlCommand.Parameters.AddWithValue("@cargoex11", item.CARGOEX11);
                sqlCommand.Parameters.AddWithValue("@abonoex11", item.ABONOEX11);
                sqlCommand.Parameters.AddWithValue("@cargo12", item.CARGO12);
                sqlCommand.Parameters.AddWithValue("@abono12", item.ABONO12);
                sqlCommand.Parameters.AddWithValue("@cargoex12", item.CARGOEX12);
                sqlCommand.Parameters.AddWithValue("@abonoex12", item.ABONOEX12);
                sqlCommand.Parameters.AddWithValue("@cargo13", item.CARGO13);
                sqlCommand.Parameters.AddWithValue("@abono13", item.ABONO13);
                sqlCommand.Parameters.AddWithValue("@cargoex13", item.CARGOEX13);
                sqlCommand.Parameters.AddWithValue("@abonoex13", item.ABONOEX13);
                sqlCommand.Parameters.AddWithValue("@cargo14", item.CARGO14);
                sqlCommand.Parameters.AddWithValue("@abono14", item.ABONO14);
                sqlCommand.Parameters.AddWithValue("@cargoex14", item.CARGOEX14);
                sqlCommand.Parameters.AddWithValue("@abonoex14", item.ABONOEX14);
                sqlCommand.Parameters.AddWithValue("@mov01", item.MOV01);
                sqlCommand.Parameters.AddWithValue("@mov02", item.MOV02);
                sqlCommand.Parameters.AddWithValue("@mov03", item.MOV03);
                sqlCommand.Parameters.AddWithValue("@mov04", item.MOV04);
                sqlCommand.Parameters.AddWithValue("@mov05", item.MOV05);
                sqlCommand.Parameters.AddWithValue("@mov06", item.MOV06);
                sqlCommand.Parameters.AddWithValue("@mov07", item.MOV07);
                sqlCommand.Parameters.AddWithValue("@mov08", item.MOV08);
                sqlCommand.Parameters.AddWithValue("@mov09", item.MOV09);
                sqlCommand.Parameters.AddWithValue("@mov10", item.MOV10);
                sqlCommand.Parameters.AddWithValue("@mov11", item.MOV11);
                sqlCommand.Parameters.AddWithValue("@mov12", item.MOV12);
                sqlCommand.Parameters.AddWithValue("@mov13", item.MOV13);
                sqlCommand.Parameters.AddWithValue("@mov14", item.MOV14);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(SALDOS20 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE saldos20 
                    			SET ejercicio = @ejercicio, inicial = @inicial, inicialex = @inicialex, cargo01 = @cargo01, abono01 = @abono01, cargoex01 = @cargoex01, abonoex01 = @abonoex01, cargo02 = @cargo02, abono02 = @abono02, cargoex02 = @cargoex02, abonoex02 = @abonoex02, cargo03 = @cargo03, abono03 = @abono03, cargoex03 = @cargoex03, abonoex03 = @abonoex03, cargo04 = @cargo04, abono04 = @abono04, cargoex04 = @cargoex04, abonoex04 = @abonoex04, cargo05 = @cargo05, abono05 = @abono05, cargoex05 = @cargoex05, abonoex05 = @abonoex05, cargo06 = @cargo06, abono06 = @abono06, cargoex06 = @cargoex06, abonoex06 = @abonoex06, cargo07 = @cargo07, abono07 = @abono07, cargoex07 = @cargoex07, abonoex07 = @abonoex07, cargo08 = @cargo08, abono08 = @abono08, cargoex08 = @cargoex08, abonoex08 = @abonoex08, cargo09 = @cargo09, abono09 = @abono09, cargoex09 = @cargoex09, abonoex09 = @abonoex09, cargo10 = @cargo10, abono10 = @abono10, cargoex10 = @cargoex10, abonoex10 = @abonoex10, cargo11 = @cargo11, abono11 = @abono11, cargoex11 = @cargoex11, abonoex11 = @abonoex11, cargo12 = @cargo12, abono12 = @abono12, cargoex12 = @cargoex12, abonoex12 = @abonoex12, cargo13 = @cargo13, abono13 = @abono13, cargoex13 = @cargoex13, abonoex13 = @abonoex13, cargo14 = @cargo14, abono14 = @abono14, cargoex14 = @cargoex14, abonoex14 = @abonoex14, mov01 = @mov01, mov02 = @mov02, mov03 = @mov03, mov04 = @mov04, mov05 = @mov05, mov06 = @mov06, mov07 = @mov07, mov08 = @mov08, mov09 = @mov09, mov10 = @mov10, mov11 = @mov11, mov12 = @mov12, mov13 = @mov13, mov14 = @mov14 
                    			WHERE num_cta = @num_cta;"
                };
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@inicial", item.INICIAL);
                sqlCommand.Parameters.AddWithValue("@inicialex", item.INICIALEX);
                sqlCommand.Parameters.AddWithValue("@cargo01", item.CARGO01);
                sqlCommand.Parameters.AddWithValue("@abono01", item.ABONO01);
                sqlCommand.Parameters.AddWithValue("@cargoex01", item.CARGOEX01);
                sqlCommand.Parameters.AddWithValue("@abonoex01", item.ABONOEX01);
                sqlCommand.Parameters.AddWithValue("@cargo02", item.CARGO02);
                sqlCommand.Parameters.AddWithValue("@abono02", item.ABONO02);
                sqlCommand.Parameters.AddWithValue("@cargoex02", item.CARGOEX02);
                sqlCommand.Parameters.AddWithValue("@abonoex02", item.ABONOEX02);
                sqlCommand.Parameters.AddWithValue("@cargo03", item.CARGO03);
                sqlCommand.Parameters.AddWithValue("@abono03", item.ABONO03);
                sqlCommand.Parameters.AddWithValue("@cargoex03", item.CARGOEX03);
                sqlCommand.Parameters.AddWithValue("@abonoex03", item.ABONOEX03);
                sqlCommand.Parameters.AddWithValue("@cargo04", item.CARGO04);
                sqlCommand.Parameters.AddWithValue("@abono04", item.ABONO04);
                sqlCommand.Parameters.AddWithValue("@cargoex04", item.CARGOEX04);
                sqlCommand.Parameters.AddWithValue("@abonoex04", item.ABONOEX04);
                sqlCommand.Parameters.AddWithValue("@cargo05", item.CARGO05);
                sqlCommand.Parameters.AddWithValue("@abono05", item.ABONO05);
                sqlCommand.Parameters.AddWithValue("@cargoex05", item.CARGOEX05);
                sqlCommand.Parameters.AddWithValue("@abonoex05", item.ABONOEX05);
                sqlCommand.Parameters.AddWithValue("@cargo06", item.CARGO06);
                sqlCommand.Parameters.AddWithValue("@abono06", item.ABONO06);
                sqlCommand.Parameters.AddWithValue("@cargoex06", item.CARGOEX06);
                sqlCommand.Parameters.AddWithValue("@abonoex06", item.ABONOEX06);
                sqlCommand.Parameters.AddWithValue("@cargo07", item.CARGO07);
                sqlCommand.Parameters.AddWithValue("@abono07", item.ABONO07);
                sqlCommand.Parameters.AddWithValue("@cargoex07", item.CARGOEX07);
                sqlCommand.Parameters.AddWithValue("@abonoex07", item.ABONOEX07);
                sqlCommand.Parameters.AddWithValue("@cargo08", item.CARGO08);
                sqlCommand.Parameters.AddWithValue("@abono08", item.ABONO08);
                sqlCommand.Parameters.AddWithValue("@cargoex08", item.CARGOEX08);
                sqlCommand.Parameters.AddWithValue("@abonoex08", item.ABONOEX08);
                sqlCommand.Parameters.AddWithValue("@cargo09", item.CARGO09);
                sqlCommand.Parameters.AddWithValue("@abono09", item.ABONO09);
                sqlCommand.Parameters.AddWithValue("@cargoex09", item.CARGOEX09);
                sqlCommand.Parameters.AddWithValue("@abonoex09", item.ABONOEX09);
                sqlCommand.Parameters.AddWithValue("@cargo10", item.CARGO10);
                sqlCommand.Parameters.AddWithValue("@abono10", item.ABONO10);
                sqlCommand.Parameters.AddWithValue("@cargoex10", item.CARGOEX10);
                sqlCommand.Parameters.AddWithValue("@abonoex10", item.ABONOEX10);
                sqlCommand.Parameters.AddWithValue("@cargo11", item.CARGO11);
                sqlCommand.Parameters.AddWithValue("@abono11", item.ABONO11);
                sqlCommand.Parameters.AddWithValue("@cargoex11", item.CARGOEX11);
                sqlCommand.Parameters.AddWithValue("@abonoex11", item.ABONOEX11);
                sqlCommand.Parameters.AddWithValue("@cargo12", item.CARGO12);
                sqlCommand.Parameters.AddWithValue("@abono12", item.ABONO12);
                sqlCommand.Parameters.AddWithValue("@cargoex12", item.CARGOEX12);
                sqlCommand.Parameters.AddWithValue("@abonoex12", item.ABONOEX12);
                sqlCommand.Parameters.AddWithValue("@cargo13", item.CARGO13);
                sqlCommand.Parameters.AddWithValue("@abono13", item.ABONO13);
                sqlCommand.Parameters.AddWithValue("@cargoex13", item.CARGOEX13);
                sqlCommand.Parameters.AddWithValue("@abonoex13", item.ABONOEX13);
                sqlCommand.Parameters.AddWithValue("@cargo14", item.CARGO14);
                sqlCommand.Parameters.AddWithValue("@abono14", item.ABONO14);
                sqlCommand.Parameters.AddWithValue("@cargoex14", item.CARGOEX14);
                sqlCommand.Parameters.AddWithValue("@abonoex14", item.ABONOEX14);
                sqlCommand.Parameters.AddWithValue("@mov01", item.MOV01);
                sqlCommand.Parameters.AddWithValue("@mov02", item.MOV02);
                sqlCommand.Parameters.AddWithValue("@mov03", item.MOV03);
                sqlCommand.Parameters.AddWithValue("@mov04", item.MOV04);
                sqlCommand.Parameters.AddWithValue("@mov05", item.MOV05);
                sqlCommand.Parameters.AddWithValue("@mov06", item.MOV06);
                sqlCommand.Parameters.AddWithValue("@mov07", item.MOV07);
                sqlCommand.Parameters.AddWithValue("@mov08", item.MOV08);
                sqlCommand.Parameters.AddWithValue("@mov09", item.MOV09);
                sqlCommand.Parameters.AddWithValue("@mov10", item.MOV10);
                sqlCommand.Parameters.AddWithValue("@mov11", item.MOV11);
                sqlCommand.Parameters.AddWithValue("@mov12", item.MOV12);
                sqlCommand.Parameters.AddWithValue("@mov13", item.MOV13);
                sqlCommand.Parameters.AddWithValue("@mov14", item.MOV14);
                return this.ExecuteScalar(sqlCommand);
            }

            public SALDOS20 GetById(string id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM saldos20 WHERE num_cta = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<SALDOS20>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<SALDOS20> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM saldos20")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<SALDOS20>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }