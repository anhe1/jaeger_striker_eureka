    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlTipactivRepository : IGenericRepository<TIPACTIV> {}

        public class SqlFbTipactivRepository : MasterRepository, ISqlTipactivRepository {

    	    public SqlFbTipactivRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(TIPACTIV item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO tipactiv (clave, descrip, dednormal, dedimed, maxded, metododep, depproy, ctaaactv, ctadepre, ctagastos, ctapergan, quemoneda) 
                    			VALUES (@clave, @descrip, @dednormal, @dedimed, @maxded, @metododep, @depproy, @ctaaactv, @ctadepre, @ctagastos, @ctapergan, @quemoneda)"
                };
                item.CLAVE = this.Max("CLAVE");
                sqlCommand.Parameters.AddWithValue("@clave", item.CLAVE);
                sqlCommand.Parameters.AddWithValue("@descrip", item.DESCRIP);
                sqlCommand.Parameters.AddWithValue("@dednormal", item.DEDNORMAL);
                sqlCommand.Parameters.AddWithValue("@dedimed", item.DEDIMED);
                sqlCommand.Parameters.AddWithValue("@maxded", item.MAXDED);
                sqlCommand.Parameters.AddWithValue("@metododep", item.METODODEP);
                sqlCommand.Parameters.AddWithValue("@depproy", item.DEPPROY);
                sqlCommand.Parameters.AddWithValue("@ctaaactv", item.CTAAACTV);
                sqlCommand.Parameters.AddWithValue("@ctadepre", item.CTADEPRE);
                sqlCommand.Parameters.AddWithValue("@ctagastos", item.CTAGASTOS);
                sqlCommand.Parameters.AddWithValue("@ctapergan", item.CTAPERGAN);
                sqlCommand.Parameters.AddWithValue("@quemoneda", item.QUEMONEDA);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(TIPACTIV item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE tipactiv 
                    			SET descrip = @descrip, dednormal = @dednormal, dedimed = @dedimed, maxded = @maxded, metododep = @metododep, depproy = @depproy, ctaaactv = @ctaaactv, ctadepre = @ctadepre, ctagastos = @ctagastos, ctapergan = @ctapergan, quemoneda = @quemoneda 
                    			WHERE clave = @clave;"
                };
                sqlCommand.Parameters.AddWithValue("@clave", item.CLAVE);
                sqlCommand.Parameters.AddWithValue("@descrip", item.DESCRIP);
                sqlCommand.Parameters.AddWithValue("@dednormal", item.DEDNORMAL);
                sqlCommand.Parameters.AddWithValue("@dedimed", item.DEDIMED);
                sqlCommand.Parameters.AddWithValue("@maxded", item.MAXDED);
                sqlCommand.Parameters.AddWithValue("@metododep", item.METODODEP);
                sqlCommand.Parameters.AddWithValue("@depproy", item.DEPPROY);
                sqlCommand.Parameters.AddWithValue("@ctaaactv", item.CTAAACTV);
                sqlCommand.Parameters.AddWithValue("@ctadepre", item.CTADEPRE);
                sqlCommand.Parameters.AddWithValue("@ctagastos", item.CTAGASTOS);
                sqlCommand.Parameters.AddWithValue("@ctapergan", item.CTAPERGAN);
                sqlCommand.Parameters.AddWithValue("@quemoneda", item.QUEMONEDA);
                return this.ExecuteScalar(sqlCommand);
            }

            public TIPACTIV GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM tipactiv WHERE clave = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<TIPACTIV>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<TIPACTIV> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM tipactiv")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<TIPACTIV>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }