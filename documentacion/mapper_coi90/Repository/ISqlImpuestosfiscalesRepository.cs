    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlImpuestosfiscalesRepository : IGenericRepository<IMPUESTOSFISCALES> {}

        public class SqlFbImpuestosfiscalesRepository : MasterRepository, ISqlImpuestosfiscalesRepository {

    	    public SqlFbImpuestosfiscalesRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(IMPUESTOSFISCALES item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO impuestosfiscales (numimpuesto, uuidtimbrecfdi, impuesto, tipoimpuesto, eslocal, base, tipofactor, tasacuota, importe) 
                    			VALUES (@numimpuesto, @uuidtimbrecfdi, @impuesto, @tipoimpuesto, @eslocal, @base, @tipofactor, @tasacuota, @importe)"
                };
                item.NUMIMPUESTO = this.Max("NUMIMPUESTO");
                sqlCommand.Parameters.AddWithValue("@numimpuesto", item.NUMIMPUESTO);
                sqlCommand.Parameters.AddWithValue("@uuidtimbrecfdi", item.UUIDTIMBRECFDI);
                sqlCommand.Parameters.AddWithValue("@impuesto", item.IMPUESTO);
                sqlCommand.Parameters.AddWithValue("@tipoimpuesto", item.TIPOIMPUESTO);
                sqlCommand.Parameters.AddWithValue("@eslocal", item.ESLOCAL);
                sqlCommand.Parameters.AddWithValue("@base", item.BASE);
                sqlCommand.Parameters.AddWithValue("@tipofactor", item.TIPOFACTOR);
                sqlCommand.Parameters.AddWithValue("@tasacuota", item.TASACUOTA);
                sqlCommand.Parameters.AddWithValue("@importe", item.IMPORTE);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(IMPUESTOSFISCALES item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE impuestosfiscales 
                    			SET uuidtimbrecfdi = @uuidtimbrecfdi, impuesto = @impuesto, tipoimpuesto = @tipoimpuesto, eslocal = @eslocal, base = @base, tipofactor = @tipofactor, tasacuota = @tasacuota, importe = @importe 
                    			WHERE numimpuesto = @numimpuesto;"
                };
                sqlCommand.Parameters.AddWithValue("@numimpuesto", item.NUMIMPUESTO);
                sqlCommand.Parameters.AddWithValue("@uuidtimbrecfdi", item.UUIDTIMBRECFDI);
                sqlCommand.Parameters.AddWithValue("@impuesto", item.IMPUESTO);
                sqlCommand.Parameters.AddWithValue("@tipoimpuesto", item.TIPOIMPUESTO);
                sqlCommand.Parameters.AddWithValue("@eslocal", item.ESLOCAL);
                sqlCommand.Parameters.AddWithValue("@base", item.BASE);
                sqlCommand.Parameters.AddWithValue("@tipofactor", item.TIPOFACTOR);
                sqlCommand.Parameters.AddWithValue("@tasacuota", item.TASACUOTA);
                sqlCommand.Parameters.AddWithValue("@importe", item.IMPORTE);
                return this.ExecuteScalar(sqlCommand);
            }

            public IMPUESTOSFISCALES GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM impuestosfiscales WHERE numimpuesto = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<IMPUESTOSFISCALES>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<IMPUESTOSFISCALES> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM impuestosfiscales")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<IMPUESTOSFISCALES>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }