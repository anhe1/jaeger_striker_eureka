    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlDoctosrelpagosRepository : IGenericRepository<DOCTOSRELPAGOS> {}

        public class SqlFbDoctosrelpagosRepository : MasterRepository, ISqlDoctosrelpagosRepository {

    	    public SqlFbDoctosrelpagosRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(DOCTOSRELPAGOS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO doctosrelpagos (uuidtimbrecfdi, uuidtimbrepago, idpago, serie, folio, monedadr, tipocambiodr, metododepagodr, numparcialidad, impsaldoant, imppagado, impsaldoinsoluto) 
                    			VALUES (@uuidtimbrecfdi, @uuidtimbrepago, @idpago, @serie, @folio, @monedadr, @tipocambiodr, @metododepagodr, @numparcialidad, @impsaldoant, @imppagado, @impsaldoinsoluto)"
                };
                item.UUIDTIMBRECFDI = this.Max("UUIDTIMBRECFDI");
                sqlCommand.Parameters.AddWithValue("@uuidtimbrecfdi", item.UUIDTIMBRECFDI);
                sqlCommand.Parameters.AddWithValue("@uuidtimbrepago", item.UUIDTIMBREPAGO);
                sqlCommand.Parameters.AddWithValue("@idpago", item.IDPAGO);
                sqlCommand.Parameters.AddWithValue("@serie", item.SERIE);
                sqlCommand.Parameters.AddWithValue("@folio", item.FOLIO);
                sqlCommand.Parameters.AddWithValue("@monedadr", item.MONEDADR);
                sqlCommand.Parameters.AddWithValue("@tipocambiodr", item.TIPOCAMBIODR);
                sqlCommand.Parameters.AddWithValue("@metododepagodr", item.METODODEPAGODR);
                sqlCommand.Parameters.AddWithValue("@numparcialidad", item.NUMPARCIALIDAD);
                sqlCommand.Parameters.AddWithValue("@impsaldoant", item.IMPSALDOANT);
                sqlCommand.Parameters.AddWithValue("@imppagado", item.IMPPAGADO);
                sqlCommand.Parameters.AddWithValue("@impsaldoinsoluto", item.IMPSALDOINSOLUTO);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(DOCTOSRELPAGOS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE doctosrelpagos 
                    			SET uuidtimbrepago = @uuidtimbrepago, idpago = @idpago, serie = @serie, folio = @folio, monedadr = @monedadr, tipocambiodr = @tipocambiodr, metododepagodr = @metododepagodr, numparcialidad = @numparcialidad, impsaldoant = @impsaldoant, imppagado = @imppagado, impsaldoinsoluto = @impsaldoinsoluto 
                    			WHERE uuidtimbrecfdi = @uuidtimbrecfdi;"
                };
                sqlCommand.Parameters.AddWithValue("@uuidtimbrecfdi", item.UUIDTIMBRECFDI);
                sqlCommand.Parameters.AddWithValue("@uuidtimbrepago", item.UUIDTIMBREPAGO);
                sqlCommand.Parameters.AddWithValue("@idpago", item.IDPAGO);
                sqlCommand.Parameters.AddWithValue("@serie", item.SERIE);
                sqlCommand.Parameters.AddWithValue("@folio", item.FOLIO);
                sqlCommand.Parameters.AddWithValue("@monedadr", item.MONEDADR);
                sqlCommand.Parameters.AddWithValue("@tipocambiodr", item.TIPOCAMBIODR);
                sqlCommand.Parameters.AddWithValue("@metododepagodr", item.METODODEPAGODR);
                sqlCommand.Parameters.AddWithValue("@numparcialidad", item.NUMPARCIALIDAD);
                sqlCommand.Parameters.AddWithValue("@impsaldoant", item.IMPSALDOANT);
                sqlCommand.Parameters.AddWithValue("@imppagado", item.IMPPAGADO);
                sqlCommand.Parameters.AddWithValue("@impsaldoinsoluto", item.IMPSALDOINSOLUTO);
                return this.ExecuteScalar(sqlCommand);
            }

            public DOCTOSRELPAGOS GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM doctosrelpagos WHERE uuidtimbrecfdi = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<DOCTOSRELPAGOS>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<DOCTOSRELPAGOS> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM doctosrelpagos")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<DOCTOSRELPAGOS>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }