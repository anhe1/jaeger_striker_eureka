    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlHojasRepository : IGenericRepository<HOJAS> {}

        public class SqlFbHojasRepository : MasterRepository, ISqlHojasRepository {

    	    public SqlFbHojasRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(HOJAS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO hojas (titulo, hojaxls, tipohoja) 
                    			VALUES (@titulo, @hojaxls, @tipohoja)"
                };
                sqlCommand.Parameters.AddWithValue("@titulo", item.TITULO);
                sqlCommand.Parameters.AddWithValue("@hojaxls", item.HOJAXLS);
                sqlCommand.Parameters.AddWithValue("@tipohoja", item.TIPOHOJA);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(HOJAS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE hojas 
                    			SET hojaxls = @hojaxls, tipohoja = @tipohoja
                    			WHERE titulo = @titulo;"
                };
                sqlCommand.Parameters.AddWithValue("@titulo", item.TITULO);
                sqlCommand.Parameters.AddWithValue("@hojaxls", item.HOJAXLS);
                sqlCommand.Parameters.AddWithValue("@tipohoja", item.TIPOHOJA);
                return this.ExecuteScalar(sqlCommand);
            }

            public HOJAS GetById(string id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM hojas WHERE titulo = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<HOJAS>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<HOJAS> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM hojas")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<HOJAS>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }