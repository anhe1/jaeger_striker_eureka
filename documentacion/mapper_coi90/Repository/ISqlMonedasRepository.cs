    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlMonedasRepository : IGenericRepository<MONEDAS> {}

        public class SqlFbMonedasRepository : MasterRepository, ISqlMonedasRepository {

    	    public SqlFbMonedasRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(MONEDAS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO monedas (moneda, nombre, prefijo, fecha, tipocambio, monto, despauto, classat) 
                    			VALUES (@moneda, @nombre, @prefijo, @fecha, @tipocambio, @monto, @despauto, @classat)"
                };
                item.MONEDA = this.Max("MONEDA");
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@prefijo", item.PREFIJO);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@tipocambio", item.TIPOCAMBIO);
                sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
                sqlCommand.Parameters.AddWithValue("@despauto", item.DESPAUTO);
                sqlCommand.Parameters.AddWithValue("@classat", item.CLASSAT);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(MONEDAS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE monedas 
                    			SET nombre = @nombre, prefijo = @prefijo, fecha = @fecha, tipocambio = @tipocambio, monto = @monto, despauto = @despauto, classat = @classat 
                    			WHERE moneda = @moneda;"
                };
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@prefijo", item.PREFIJO);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@tipocambio", item.TIPOCAMBIO);
                sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
                sqlCommand.Parameters.AddWithValue("@despauto", item.DESPAUTO);
                sqlCommand.Parameters.AddWithValue("@classat", item.CLASSAT);
                return this.ExecuteScalar(sqlCommand);
            }

            public MONEDAS GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM monedas WHERE moneda = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<MONEDAS>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<MONEDAS> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM monedas")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<MONEDAS>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }