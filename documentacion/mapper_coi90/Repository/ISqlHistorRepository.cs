    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlHistorRepository : IGenericRepository<HISTOR> {}

        public class SqlFbHistorRepository : MasterRepository, ISqlHistorRepository {

    	    public SqlFbHistorRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(HISTOR item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO histor (moneda, fecha, tipcambio) 
                    			VALUES (@moneda, @fecha, @tipcambio)"
                };
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@tipcambio", item.TIPCAMBIO);
            }

            public int Update(HISTOR item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE histor 
                    			SET fecha = fecha, tipcambio = tipcambio
                    			WHERE moneda = @moneda;"
                };
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@tipcambio", item.TIPCAMBIO);
                return this.ExecuteScalar(sqlCommand);
            }

            public HISTOR GetById(string id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM histor WHERE moneda = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<HISTOR>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<HISTOR> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM histor")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<HISTOR>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }