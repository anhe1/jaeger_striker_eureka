    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlPagoRepository : IGenericRepository<PAGO> {}

        public class SqlFbPagoRepository : MasterRepository, ISqlPagoRepository {

    	    public SqlFbPagoRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(PAGO item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO pago (idpago, uuidtimbrepago, fechapago, formapagop, monedap, tipocambiop, importe, numoperacion, rfcemisorctaord, nombrebancoordext, ctaordenante, rfcemisorctaben, ctabeneficiario, tipocadpago, certpago, cadpago, sellopago, version) 
                    			VALUES (@idpago, @uuidtimbrepago, @fechapago, @formapagop, @monedap, @tipocambiop, @importe, @numoperacion, @rfcemisorctaord, @nombrebancoordext, @ctaordenante, @rfcemisorctaben, @ctabeneficiario, @tipocadpago, @certpago, @cadpago, @sellopago, @version)"
                };
                item.IDPAGO = this.Max("IDPAGO");
                sqlCommand.Parameters.AddWithValue("@idpago", item.IDPAGO);
                sqlCommand.Parameters.AddWithValue("@uuidtimbrepago", item.UUIDTIMBREPAGO);
                sqlCommand.Parameters.AddWithValue("@fechapago", item.FECHAPAGO);
                sqlCommand.Parameters.AddWithValue("@formapagop", item.FORMAPAGOP);
                sqlCommand.Parameters.AddWithValue("@monedap", item.MONEDAP);
                sqlCommand.Parameters.AddWithValue("@tipocambiop", item.TIPOCAMBIOP);
                sqlCommand.Parameters.AddWithValue("@importe", item.IMPORTE);
                sqlCommand.Parameters.AddWithValue("@numoperacion", item.NUMOPERACION);
                sqlCommand.Parameters.AddWithValue("@rfcemisorctaord", item.RFCEMISORCTAORD);
                sqlCommand.Parameters.AddWithValue("@nombrebancoordext", item.NOMBREBANCOORDEXT);
                sqlCommand.Parameters.AddWithValue("@ctaordenante", item.CTAORDENANTE);
                sqlCommand.Parameters.AddWithValue("@rfcemisorctaben", item.RFCEMISORCTABEN);
                sqlCommand.Parameters.AddWithValue("@ctabeneficiario", item.CTABENEFICIARIO);
                sqlCommand.Parameters.AddWithValue("@tipocadpago", item.TIPOCADPAGO);
                sqlCommand.Parameters.AddWithValue("@certpago", item.CERTPAGO);
                sqlCommand.Parameters.AddWithValue("@cadpago", item.CADPAGO);
                sqlCommand.Parameters.AddWithValue("@sellopago", item.SELLOPAGO);
                sqlCommand.Parameters.AddWithValue("@version", item.VERSION);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(PAGO item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE pago 
                    			SET uuidtimbrepago = @uuidtimbrepago, fechapago = @fechapago, formapagop = @formapagop, monedap = @monedap, tipocambiop = @tipocambiop, importe = @importe, numoperacion = @numoperacion, rfcemisorctaord = @rfcemisorctaord, nombrebancoordext = @nombrebancoordext, ctaordenante = @ctaordenante, rfcemisorctaben = @rfcemisorctaben, ctabeneficiario = @ctabeneficiario, tipocadpago = @tipocadpago, certpago = @certpago, cadpago = @cadpago, sellopago = @sellopago, version = @version 
                    			WHERE idpago = @idpago;"
                };
                sqlCommand.Parameters.AddWithValue("@idpago", item.IDPAGO);
                sqlCommand.Parameters.AddWithValue("@uuidtimbrepago", item.UUIDTIMBREPAGO);
                sqlCommand.Parameters.AddWithValue("@fechapago", item.FECHAPAGO);
                sqlCommand.Parameters.AddWithValue("@formapagop", item.FORMAPAGOP);
                sqlCommand.Parameters.AddWithValue("@monedap", item.MONEDAP);
                sqlCommand.Parameters.AddWithValue("@tipocambiop", item.TIPOCAMBIOP);
                sqlCommand.Parameters.AddWithValue("@importe", item.IMPORTE);
                sqlCommand.Parameters.AddWithValue("@numoperacion", item.NUMOPERACION);
                sqlCommand.Parameters.AddWithValue("@rfcemisorctaord", item.RFCEMISORCTAORD);
                sqlCommand.Parameters.AddWithValue("@nombrebancoordext", item.NOMBREBANCOORDEXT);
                sqlCommand.Parameters.AddWithValue("@ctaordenante", item.CTAORDENANTE);
                sqlCommand.Parameters.AddWithValue("@rfcemisorctaben", item.RFCEMISORCTABEN);
                sqlCommand.Parameters.AddWithValue("@ctabeneficiario", item.CTABENEFICIARIO);
                sqlCommand.Parameters.AddWithValue("@tipocadpago", item.TIPOCADPAGO);
                sqlCommand.Parameters.AddWithValue("@certpago", item.CERTPAGO);
                sqlCommand.Parameters.AddWithValue("@cadpago", item.CADPAGO);
                sqlCommand.Parameters.AddWithValue("@sellopago", item.SELLOPAGO);
                sqlCommand.Parameters.AddWithValue("@version", item.VERSION);
                return this.ExecuteScalar(sqlCommand);
            }

            public PAGO GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM pago WHERE idpago = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<PAGO>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<PAGO> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM pago")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<PAGO>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }