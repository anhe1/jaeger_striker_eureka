    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlActivos20Repository : IGenericRepository<ACTIVOS20> {}

        public class SqlFbActivos20Repository : MasterRepository, ISqlActivos20Repository {

    	    public SqlFbActivos20Repository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(ACTIVOS20 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO activos20 (clave, tipo, descrip, localiz, montorig, montorigex, fechaadq, maxded, valmer, depacu, vidaut, metdep, tasdep, tasdepfis, status, fechaelim, numdepto, fechaultre, numserie, bandedinm, fecinidep, fecinidepf, depacufisc, observacio, tipocambio, polizaseg, compasegu, agenteseg, telsinies, tipocober, montoaseg, primatota, deducible, fecvigenc, fecproxma, fecultman, periodoma, costomant, respactfijo) 
                    			VALUES (@clave, @tipo, @descrip, @localiz, @montorig, @montorigex, @fechaadq, @maxded, @valmer, @depacu, @vidaut, @metdep, @tasdep, @tasdepfis, @status, @fechaelim, @numdepto, @fechaultre, @numserie, @bandedinm, @fecinidep, @fecinidepf, @depacufisc, @observacio, @tipocambio, @polizaseg, @compasegu, @agenteseg, @telsinies, @tipocober, @montoaseg, @primatota, @deducible, @fecvigenc, @fecproxma, @fecultman, @periodoma, @costomant, @respactfijo)"
                };
                item.CLAVE = this.Max("CLAVE");
                sqlCommand.Parameters.AddWithValue("@clave", item.CLAVE);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@descrip", item.DESCRIP);
                sqlCommand.Parameters.AddWithValue("@localiz", item.LOCALIZ);
                sqlCommand.Parameters.AddWithValue("@montorig", item.MONTORIG);
                sqlCommand.Parameters.AddWithValue("@montorigex", item.MONTORIGEX);
                sqlCommand.Parameters.AddWithValue("@fechaadq", item.FECHAADQ);
                sqlCommand.Parameters.AddWithValue("@maxded", item.MAXDED);
                sqlCommand.Parameters.AddWithValue("@valmer", item.VALMER);
                sqlCommand.Parameters.AddWithValue("@depacu", item.DEPACU);
                sqlCommand.Parameters.AddWithValue("@vidaut", item.VIDAUT);
                sqlCommand.Parameters.AddWithValue("@metdep", item.METDEP);
                sqlCommand.Parameters.AddWithValue("@tasdep", item.TASDEP);
                sqlCommand.Parameters.AddWithValue("@tasdepfis", item.TASDEPFIS);
                sqlCommand.Parameters.AddWithValue("@status", item.STATUS);
                sqlCommand.Parameters.AddWithValue("@fechaelim", item.FECHAELIM);
                sqlCommand.Parameters.AddWithValue("@numdepto", item.NUMDEPTO);
                sqlCommand.Parameters.AddWithValue("@fechaultre", item.FECHAULTRE);
                sqlCommand.Parameters.AddWithValue("@numserie", item.NUMSERIE);
                sqlCommand.Parameters.AddWithValue("@bandedinm", item.BANDEDINM);
                sqlCommand.Parameters.AddWithValue("@fecinidep", item.FECINIDEP);
                sqlCommand.Parameters.AddWithValue("@fecinidepf", item.FECINIDEPF);
                sqlCommand.Parameters.AddWithValue("@depacufisc", item.DEPACUFISC);
                sqlCommand.Parameters.AddWithValue("@observacio", item.OBSERVACIO);
                sqlCommand.Parameters.AddWithValue("@tipocambio", item.TIPOCAMBIO);
                sqlCommand.Parameters.AddWithValue("@polizaseg", item.POLIZASEG);
                sqlCommand.Parameters.AddWithValue("@compasegu", item.COMPASEGU);
                sqlCommand.Parameters.AddWithValue("@agenteseg", item.AGENTESEG);
                sqlCommand.Parameters.AddWithValue("@telsinies", item.TELSINIES);
                sqlCommand.Parameters.AddWithValue("@tipocober", item.TIPOCOBER);
                sqlCommand.Parameters.AddWithValue("@montoaseg", item.MONTOASEG);
                sqlCommand.Parameters.AddWithValue("@primatota", item.PRIMATOTA);
                sqlCommand.Parameters.AddWithValue("@deducible", item.DEDUCIBLE);
                sqlCommand.Parameters.AddWithValue("@fecvigenc", item.FECVIGENC);
                sqlCommand.Parameters.AddWithValue("@fecproxma", item.FECPROXMA);
                sqlCommand.Parameters.AddWithValue("@fecultman", item.FECULTMAN);
                sqlCommand.Parameters.AddWithValue("@periodoma", item.PERIODOMA);
                sqlCommand.Parameters.AddWithValue("@costomant", item.COSTOMANT);
                sqlCommand.Parameters.AddWithValue("@respactfijo", item.RESPACTFIJO);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(ACTIVOS20 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE activos20 
                    			SET tipo = @tipo, descrip = @descrip, localiz = @localiz, montorig = @montorig, montorigex = @montorigex, fechaadq = @fechaadq, maxded = @maxded, valmer = @valmer, depacu = @depacu, vidaut = @vidaut, metdep = @metdep, tasdep = @tasdep, tasdepfis = @tasdepfis, status = @status, fechaelim = @fechaelim, numdepto = @numdepto, fechaultre = @fechaultre, numserie = @numserie, bandedinm = @bandedinm, fecinidep = @fecinidep, fecinidepf = @fecinidepf, depacufisc = @depacufisc, observacio = @observacio, tipocambio = @tipocambio, polizaseg = @polizaseg, compasegu = @compasegu, agenteseg = @agenteseg, telsinies = @telsinies, tipocober = @tipocober, montoaseg = @montoaseg, primatota = @primatota, deducible = @deducible, fecvigenc = @fecvigenc, fecproxma = @fecproxma, fecultman = @fecultman, periodoma = @periodoma, costomant = @costomant, respactfijo = @respactfijo 
                    			WHERE clave = @clave;"
                };
                sqlCommand.Parameters.AddWithValue("@clave", item.CLAVE);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@descrip", item.DESCRIP);
                sqlCommand.Parameters.AddWithValue("@localiz", item.LOCALIZ);
                sqlCommand.Parameters.AddWithValue("@montorig", item.MONTORIG);
                sqlCommand.Parameters.AddWithValue("@montorigex", item.MONTORIGEX);
                sqlCommand.Parameters.AddWithValue("@fechaadq", item.FECHAADQ);
                sqlCommand.Parameters.AddWithValue("@maxded", item.MAXDED);
                sqlCommand.Parameters.AddWithValue("@valmer", item.VALMER);
                sqlCommand.Parameters.AddWithValue("@depacu", item.DEPACU);
                sqlCommand.Parameters.AddWithValue("@vidaut", item.VIDAUT);
                sqlCommand.Parameters.AddWithValue("@metdep", item.METDEP);
                sqlCommand.Parameters.AddWithValue("@tasdep", item.TASDEP);
                sqlCommand.Parameters.AddWithValue("@tasdepfis", item.TASDEPFIS);
                sqlCommand.Parameters.AddWithValue("@status", item.STATUS);
                sqlCommand.Parameters.AddWithValue("@fechaelim", item.FECHAELIM);
                sqlCommand.Parameters.AddWithValue("@numdepto", item.NUMDEPTO);
                sqlCommand.Parameters.AddWithValue("@fechaultre", item.FECHAULTRE);
                sqlCommand.Parameters.AddWithValue("@numserie", item.NUMSERIE);
                sqlCommand.Parameters.AddWithValue("@bandedinm", item.BANDEDINM);
                sqlCommand.Parameters.AddWithValue("@fecinidep", item.FECINIDEP);
                sqlCommand.Parameters.AddWithValue("@fecinidepf", item.FECINIDEPF);
                sqlCommand.Parameters.AddWithValue("@depacufisc", item.DEPACUFISC);
                sqlCommand.Parameters.AddWithValue("@observacio", item.OBSERVACIO);
                sqlCommand.Parameters.AddWithValue("@tipocambio", item.TIPOCAMBIO);
                sqlCommand.Parameters.AddWithValue("@polizaseg", item.POLIZASEG);
                sqlCommand.Parameters.AddWithValue("@compasegu", item.COMPASEGU);
                sqlCommand.Parameters.AddWithValue("@agenteseg", item.AGENTESEG);
                sqlCommand.Parameters.AddWithValue("@telsinies", item.TELSINIES);
                sqlCommand.Parameters.AddWithValue("@tipocober", item.TIPOCOBER);
                sqlCommand.Parameters.AddWithValue("@montoaseg", item.MONTOASEG);
                sqlCommand.Parameters.AddWithValue("@primatota", item.PRIMATOTA);
                sqlCommand.Parameters.AddWithValue("@deducible", item.DEDUCIBLE);
                sqlCommand.Parameters.AddWithValue("@fecvigenc", item.FECVIGENC);
                sqlCommand.Parameters.AddWithValue("@fecproxma", item.FECPROXMA);
                sqlCommand.Parameters.AddWithValue("@fecultman", item.FECULTMAN);
                sqlCommand.Parameters.AddWithValue("@periodoma", item.PERIODOMA);
                sqlCommand.Parameters.AddWithValue("@costomant", item.COSTOMANT);
                sqlCommand.Parameters.AddWithValue("@respactfijo", item.RESPACTFIJO);
                return this.ExecuteScalar(sqlCommand);
            }

            public ACTIVOS20 GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM activos20 WHERE clave = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<ACTIVOS20>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<ACTIVOS20> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM activos20")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<ACTIVOS20>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }