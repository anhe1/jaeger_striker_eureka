    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlOpeietRepository : IGenericRepository<OPEIET> {}

        public class SqlFbOpeietRepository : MasterRepository, ISqlOpeietRepository {

    	    public SqlFbOpeietRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(OPEIET item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO opeiet (tipopol, numpol, fechapol, numpart, numcta, rfcprove, tipope, monconiva, mondedisr, actos15, ivaop15, actos10, ivaop10, actoscero, actosexent, ivaretenid, ivatraslad, ivadevolu, percausac, ivanoac15, ivanoac10, esimporta, otrasret, esdevol, isrretenid, ivageneral, ivafronterizo, idconcep, ingrexento, idconcepivat, idopeiet) 
                    			VALUES (@tipopol, @numpol, @fechapol, @numpart, @numcta, @rfcprove, @tipope, @monconiva, @mondedisr, @actos15, @ivaop15, @actos10, @ivaop10, @actoscero, @actosexent, @ivaretenid, @ivatraslad, @ivadevolu, @percausac, @ivanoac15, @ivanoac10, @esimporta, @otrasret, @esdevol, @isrretenid, @ivageneral, @ivafronterizo, @idconcep, @ingrexento, @idconcepivat, @idopeiet)"
                };
                sqlCommand.Parameters.AddWithValue("@tipopol", item.TIPOPOL);
                sqlCommand.Parameters.AddWithValue("@numpol", item.NUMPOL);
                sqlCommand.Parameters.AddWithValue("@fechapol", item.FECHAPOL);
                sqlCommand.Parameters.AddWithValue("@numpart", item.NUMPART);
                sqlCommand.Parameters.AddWithValue("@numcta", item.NUMCTA);
                sqlCommand.Parameters.AddWithValue("@rfcprove", item.RFCPROVE);
                sqlCommand.Parameters.AddWithValue("@tipope", item.TIPOPE);
                sqlCommand.Parameters.AddWithValue("@monconiva", item.MONCONIVA);
                sqlCommand.Parameters.AddWithValue("@mondedisr", item.MONDEDISR);
                sqlCommand.Parameters.AddWithValue("@actos15", item.ACTOS15);
                sqlCommand.Parameters.AddWithValue("@ivaop15", item.IVAOP15);
                sqlCommand.Parameters.AddWithValue("@actos10", item.ACTOS10);
                sqlCommand.Parameters.AddWithValue("@ivaop10", item.IVAOP10);
                sqlCommand.Parameters.AddWithValue("@actoscero", item.ACTOSCERO);
                sqlCommand.Parameters.AddWithValue("@actosexent", item.ACTOSEXENT);
                sqlCommand.Parameters.AddWithValue("@ivaretenid", item.IVARETENID);
                sqlCommand.Parameters.AddWithValue("@ivatraslad", item.IVATRASLAD);
                sqlCommand.Parameters.AddWithValue("@ivadevolu", item.IVADEVOLU);
                sqlCommand.Parameters.AddWithValue("@percausac", item.PERCAUSAC);
                sqlCommand.Parameters.AddWithValue("@ivanoac15", item.IVANOAC15);
                sqlCommand.Parameters.AddWithValue("@ivanoac10", item.IVANOAC10);
                sqlCommand.Parameters.AddWithValue("@esimporta", item.ESIMPORTA);
                sqlCommand.Parameters.AddWithValue("@otrasret", item.OTRASRET);
                sqlCommand.Parameters.AddWithValue("@esdevol", item.ESDEVOL);
                sqlCommand.Parameters.AddWithValue("@isrretenid", item.ISRRETENID);
                sqlCommand.Parameters.AddWithValue("@ivageneral", item.IVAGENERAL);
                sqlCommand.Parameters.AddWithValue("@ivafronterizo", item.IVAFRONTERIZO);
                sqlCommand.Parameters.AddWithValue("@idconcep", item.IDCONCEP);
                sqlCommand.Parameters.AddWithValue("@ingrexento", item.INGREXENTO);
                sqlCommand.Parameters.AddWithValue("@idconcepivat", item.IDCONCEPIVAT);
                sqlCommand.Parameters.AddWithValue("@idopeiet", item.IDOPEIET);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(OPEIET item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE opeiet 
                    			SET numpol = @numpol, fechapol = @fechapol, numpart = @numpart, numcta = @numcta, rfcprove = @rfcprove, tipope = @tipope, monconiva = @monconiva, mondedisr = @mondedisr, actos15 = @actos15, ivaop15 = @ivaop15, actos10 = @actos10, ivaop10 = @ivaop10, actoscero = @actoscero, actosexent = @actosexent, ivaretenid = @ivaretenid, ivatraslad = @ivatraslad, ivadevolu = @ivadevolu, percausac = @percausac, ivanoac15 = @ivanoac15, ivanoac10 = @ivanoac10, esimporta = @esimporta, otrasret = @otrasret, esdevol = @esdevol, isrretenid = @isrretenid, ivageneral = @ivageneral, ivafronterizo = @ivafronterizo, idconcep = @idconcep, ingrexento = @ingrexento, idconcepivat = @idconcepivat, idopeiet = @idopeiet 
                    			WHERE tipopol = @tipopol;"
                };
                sqlCommand.Parameters.AddWithValue("@tipopol", item.TIPOPOL);
                sqlCommand.Parameters.AddWithValue("@numpol", item.NUMPOL);
                sqlCommand.Parameters.AddWithValue("@fechapol", item.FECHAPOL);
                sqlCommand.Parameters.AddWithValue("@numpart", item.NUMPART);
                sqlCommand.Parameters.AddWithValue("@numcta", item.NUMCTA);
                sqlCommand.Parameters.AddWithValue("@rfcprove", item.RFCPROVE);
                sqlCommand.Parameters.AddWithValue("@tipope", item.TIPOPE);
                sqlCommand.Parameters.AddWithValue("@monconiva", item.MONCONIVA);
                sqlCommand.Parameters.AddWithValue("@mondedisr", item.MONDEDISR);
                sqlCommand.Parameters.AddWithValue("@actos15", item.ACTOS15);
                sqlCommand.Parameters.AddWithValue("@ivaop15", item.IVAOP15);
                sqlCommand.Parameters.AddWithValue("@actos10", item.ACTOS10);
                sqlCommand.Parameters.AddWithValue("@ivaop10", item.IVAOP10);
                sqlCommand.Parameters.AddWithValue("@actoscero", item.ACTOSCERO);
                sqlCommand.Parameters.AddWithValue("@actosexent", item.ACTOSEXENT);
                sqlCommand.Parameters.AddWithValue("@ivaretenid", item.IVARETENID);
                sqlCommand.Parameters.AddWithValue("@ivatraslad", item.IVATRASLAD);
                sqlCommand.Parameters.AddWithValue("@ivadevolu", item.IVADEVOLU);
                sqlCommand.Parameters.AddWithValue("@percausac", item.PERCAUSAC);
                sqlCommand.Parameters.AddWithValue("@ivanoac15", item.IVANOAC15);
                sqlCommand.Parameters.AddWithValue("@ivanoac10", item.IVANOAC10);
                sqlCommand.Parameters.AddWithValue("@esimporta", item.ESIMPORTA);
                sqlCommand.Parameters.AddWithValue("@otrasret", item.OTRASRET);
                sqlCommand.Parameters.AddWithValue("@esdevol", item.ESDEVOL);
                sqlCommand.Parameters.AddWithValue("@isrretenid", item.ISRRETENID);
                sqlCommand.Parameters.AddWithValue("@ivageneral", item.IVAGENERAL);
                sqlCommand.Parameters.AddWithValue("@ivafronterizo", item.IVAFRONTERIZO);
                sqlCommand.Parameters.AddWithValue("@idconcep", item.IDCONCEP);
                sqlCommand.Parameters.AddWithValue("@ingrexento", item.INGREXENTO);
                sqlCommand.Parameters.AddWithValue("@idconcepivat", item.IDCONCEPIVAT);
                sqlCommand.Parameters.AddWithValue("@idopeiet", item.IDOPEIET);
                return this.ExecuteScalar(sqlCommand);
            }

            public OPEIET GetById(string id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM opeiet WHERE tipopol = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<OPEIET>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<OPEIET> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM opeiet")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<OPEIET>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }