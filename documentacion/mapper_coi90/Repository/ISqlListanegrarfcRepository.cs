    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlListanegrarfcRepository : IGenericRepository<LISTANEGRARFC> {}

        public class SqlFbListanegrarfcRepository : MasterRepository, ISqlListanegrarfcRepository {

    	    public SqlFbListanegrarfcRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(LISTANEGRARFC item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO listanegrarfc (rfc, situacioncontribuyente, pbpagsat) 
                    			VALUES (@rfc, @situacioncontribuyente, @pbpagsat)"
                };
                sqlCommand.Parameters.AddWithValue("@rfc", item.RFC);
                sqlCommand.Parameters.AddWithValue("@situacioncontribuyente", item.SITUACIONCONTRIBUYENTE);
                sqlCommand.Parameters.AddWithValue("@pbpagsat", item.PBPAGSAT);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(LISTANEGRARFC item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE listanegrarfc 
                    			SET situacioncontribuyente = @situacioncontribuyente, pbpagsat = @pbpagsat 
                    			WHERE rfc = @rfc;"
                };
                sqlCommand.Parameters.AddWithValue("@id", item.ID);
                return this.ExecuteScalar(sqlCommand);
            }

            public LISTANEGRARFC GetById(string id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM listanegrarfc WHERE rfc = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<LISTANEGRARFC>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<LISTANEGRARFC> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM listanegrarfc")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<LISTANEGRARFC>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }