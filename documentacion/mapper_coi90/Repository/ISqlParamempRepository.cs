    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlParamempRepository : IGenericRepository<PARAMEMP> {}

        public class SqlFbParamempRepository : MasterRepository, ISqlParamempRepository {

    	    public SqlFbParamempRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(PARAMEMP item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO paramemp (idemp, direc, pobl, cod_pos, rfc_emp, ced_emp, digcta1, digcta2, digcta3, digcta4, digcta5, digcta6, digcta7, digcta8, digcta9, nivelactu, guion_sino, redo_p, capsalini, asigsecpol, contablinea, mescierre, subdir, capctaspol, depto_sino, traspaso, ctmonext, cstipocmb, cptipocmb, cquemonext, cquemonctb, ctaiva, ctabanco, conmontpol, drivercoi, alias, foliounico, capturados, poldescuad, dirtrab, ivadef, ivaenopt, ivaeningr, buffcuecua, tipopersona, nacionalidad, tipoempresa, fconstitucion, regimen, nombrerepre, rfcrepre, curprepre, podernot, traspasosaldos, nombre, ccostos, cgrupos, etqccostos, etqcgrupos, ivadefietu, tipodecapturadiot, tipodecapturaietu, cuentaivaacreditable, cuentaivatrasladado, tipopoliza, tipopolizater, giro, dev_compras, dev_ventas, gasto_activo, ieps_por_acreditar, ieps_por_trasladar, imp_local_retenido, imp_local_trasladado, isr_retenido, iva_acreditar, iva_por_trasladar, iva_retenido, ventas, rcertificado, rllaveprivada, contraseniallaveprivada, proveedortimbrado, usuariotimbrado, contraseniatimbrado, proveedorcancelacion, usuariocancelacion, contraseniacancelacion, espacioaspel, usuarioespacio, contraseniaespacio, idcategoriaespacio, nombrecategoriaespacio, asigcategoria, gdiotapartircfdi, guardaropeauto, cfdientodaslaspartidas, logo) 
                    			VALUES (@idemp, @direc, @pobl, @cod_pos, @rfc_emp, @ced_emp, @digcta1, @digcta2, @digcta3, @digcta4, @digcta5, @digcta6, @digcta7, @digcta8, @digcta9, @nivelactu, @guion_sino, @redo_p, @capsalini, @asigsecpol, @contablinea, @mescierre, @subdir, @capctaspol, @depto_sino, @traspaso, @ctmonext, @cstipocmb, @cptipocmb, @cquemonext, @cquemonctb, @ctaiva, @ctabanco, @conmontpol, @drivercoi, @alias, @foliounico, @capturados, @poldescuad, @dirtrab, @ivadef, @ivaenopt, @ivaeningr, @buffcuecua, @tipopersona, @nacionalidad, @tipoempresa, @fconstitucion, @regimen, @nombrerepre, @rfcrepre, @curprepre, @podernot, @traspasosaldos, @nombre, @ccostos, @cgrupos, @etqccostos, @etqcgrupos, @ivadefietu, @tipodecapturadiot, @tipodecapturaietu, @cuentaivaacreditable, @cuentaivatrasladado, @tipopoliza, @tipopolizater, @giro, @dev_compras, @dev_ventas, @gasto_activo, @ieps_por_acreditar, @ieps_por_trasladar, @imp_local_retenido, @imp_local_trasladado, @isr_retenido, @iva_acreditar, @iva_por_trasladar, @iva_retenido, @ventas, @rcertificado, @rllaveprivada, @contraseniallaveprivada, @proveedortimbrado, @usuariotimbrado, @contraseniatimbrado, @proveedorcancelacion, @usuariocancelacion, @contraseniacancelacion, @espacioaspel, @usuarioespacio, @contraseniaespacio, @idcategoriaespacio, @nombrecategoriaespacio, @asigcategoria, @gdiotapartircfdi, @guardaropeauto, @cfdientodaslaspartidas, @logo)"
                };
                item.IDEMP = this.Max("IDEMP");
                sqlCommand.Parameters.AddWithValue("@idemp", item.IDEMP);
                sqlCommand.Parameters.AddWithValue("@direc", item.DIREC);
                sqlCommand.Parameters.AddWithValue("@pobl", item.POBL);
                sqlCommand.Parameters.AddWithValue("@cod_pos", item.COD_POS);
                sqlCommand.Parameters.AddWithValue("@rfc_emp", item.RFC_EMP);
                sqlCommand.Parameters.AddWithValue("@ced_emp", item.CED_EMP);
                sqlCommand.Parameters.AddWithValue("@digcta1", item.DIGCTA1);
                sqlCommand.Parameters.AddWithValue("@digcta2", item.DIGCTA2);
                sqlCommand.Parameters.AddWithValue("@digcta3", item.DIGCTA3);
                sqlCommand.Parameters.AddWithValue("@digcta4", item.DIGCTA4);
                sqlCommand.Parameters.AddWithValue("@digcta5", item.DIGCTA5);
                sqlCommand.Parameters.AddWithValue("@digcta6", item.DIGCTA6);
                sqlCommand.Parameters.AddWithValue("@digcta7", item.DIGCTA7);
                sqlCommand.Parameters.AddWithValue("@digcta8", item.DIGCTA8);
                sqlCommand.Parameters.AddWithValue("@digcta9", item.DIGCTA9);
                sqlCommand.Parameters.AddWithValue("@nivelactu", item.NIVELACTU);
                sqlCommand.Parameters.AddWithValue("@guion_sino", item.GUION_SINO);
                sqlCommand.Parameters.AddWithValue("@redo_p", item.REDO_P);
                sqlCommand.Parameters.AddWithValue("@capsalini", item.CAPSALINI);
                sqlCommand.Parameters.AddWithValue("@asigsecpol", item.ASIGSECPOL);
                sqlCommand.Parameters.AddWithValue("@contablinea", item.CONTABLINEA);
                sqlCommand.Parameters.AddWithValue("@mescierre", item.MESCIERRE);
                sqlCommand.Parameters.AddWithValue("@subdir", item.SUBDIR);
                sqlCommand.Parameters.AddWithValue("@capctaspol", item.CAPCTASPOL);
                sqlCommand.Parameters.AddWithValue("@depto_sino", item.DEPTO_SINO);
                sqlCommand.Parameters.AddWithValue("@traspaso", item.TRASPASO);
                sqlCommand.Parameters.AddWithValue("@ctmonext", item.CTMONEXT);
                sqlCommand.Parameters.AddWithValue("@cstipocmb", item.CSTIPOCMB);
                sqlCommand.Parameters.AddWithValue("@cptipocmb", item.CPTIPOCMB);
                sqlCommand.Parameters.AddWithValue("@cquemonext", item.CQUEMONEXT);
                sqlCommand.Parameters.AddWithValue("@cquemonctb", item.CQUEMONCTB);
                sqlCommand.Parameters.AddWithValue("@ctaiva", item.CTAIVA);
                sqlCommand.Parameters.AddWithValue("@ctabanco", item.CTABANCO);
                sqlCommand.Parameters.AddWithValue("@conmontpol", item.CONMONTPOL);
                sqlCommand.Parameters.AddWithValue("@drivercoi", item.DRIVERCOI);
                sqlCommand.Parameters.AddWithValue("@alias", item.ALIAS);
                sqlCommand.Parameters.AddWithValue("@foliounico", item.FOLIOUNICO);
                sqlCommand.Parameters.AddWithValue("@capturados", item.CAPTURADOS);
                sqlCommand.Parameters.AddWithValue("@poldescuad", item.POLDESCUAD);
                sqlCommand.Parameters.AddWithValue("@dirtrab", item.DIRTRAB);
                sqlCommand.Parameters.AddWithValue("@ivadef", item.IVADEF);
                sqlCommand.Parameters.AddWithValue("@ivaenopt", item.IVAENOPT);
                sqlCommand.Parameters.AddWithValue("@ivaeningr", item.IVAENINGR);
                sqlCommand.Parameters.AddWithValue("@buffcuecua", item.BUFFCUECUA);
                sqlCommand.Parameters.AddWithValue("@tipopersona", item.TIPOPERSONA);
                sqlCommand.Parameters.AddWithValue("@nacionalidad", item.NACIONALIDAD);
                sqlCommand.Parameters.AddWithValue("@tipoempresa", item.TIPOEMPRESA);
                sqlCommand.Parameters.AddWithValue("@fconstitucion", item.FCONSTITUCION);
                sqlCommand.Parameters.AddWithValue("@regimen", item.REGIMEN);
                sqlCommand.Parameters.AddWithValue("@nombrerepre", item.NOMBREREPRE);
                sqlCommand.Parameters.AddWithValue("@rfcrepre", item.RFCREPRE);
                sqlCommand.Parameters.AddWithValue("@curprepre", item.CURPREPRE);
                sqlCommand.Parameters.AddWithValue("@podernot", item.PODERNOT);
                sqlCommand.Parameters.AddWithValue("@traspasosaldos", item.TRASPASOSALDOS);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@ccostos", item.CCOSTOS);
                sqlCommand.Parameters.AddWithValue("@cgrupos", item.CGRUPOS);
                sqlCommand.Parameters.AddWithValue("@etqccostos", item.ETQCCOSTOS);
                sqlCommand.Parameters.AddWithValue("@etqcgrupos", item.ETQCGRUPOS);
                sqlCommand.Parameters.AddWithValue("@ivadefietu", item.IVADEFIETU);
                sqlCommand.Parameters.AddWithValue("@tipodecapturadiot", item.TIPODECAPTURADIOT);
                sqlCommand.Parameters.AddWithValue("@tipodecapturaietu", item.TIPODECAPTURAIETU);
                sqlCommand.Parameters.AddWithValue("@cuentaivaacreditable", item.CUENTAIVAACREDITABLE);
                sqlCommand.Parameters.AddWithValue("@cuentaivatrasladado", item.CUENTAIVATRASLADADO);
                sqlCommand.Parameters.AddWithValue("@tipopoliza", item.TIPOPOLIZA);
                sqlCommand.Parameters.AddWithValue("@tipopolizater", item.TIPOPOLIZATER);
                sqlCommand.Parameters.AddWithValue("@giro", item.GIRO);
                sqlCommand.Parameters.AddWithValue("@dev_compras", item.DEV_COMPRAS);
                sqlCommand.Parameters.AddWithValue("@dev_ventas", item.DEV_VENTAS);
                sqlCommand.Parameters.AddWithValue("@gasto_activo", item.GASTO_ACTIVO);
                sqlCommand.Parameters.AddWithValue("@ieps_por_acreditar", item.IEPS_POR_ACREDITAR);
                sqlCommand.Parameters.AddWithValue("@ieps_por_trasladar", item.IEPS_POR_TRASLADAR);
                sqlCommand.Parameters.AddWithValue("@imp_local_retenido", item.IMP_LOCAL_RETENIDO);
                sqlCommand.Parameters.AddWithValue("@imp_local_trasladado", item.IMP_LOCAL_TRASLADADO);
                sqlCommand.Parameters.AddWithValue("@isr_retenido", item.ISR_RETENIDO);
                sqlCommand.Parameters.AddWithValue("@iva_acreditar", item.IVA_ACREDITAR);
                sqlCommand.Parameters.AddWithValue("@iva_por_trasladar", item.IVA_POR_TRASLADAR);
                sqlCommand.Parameters.AddWithValue("@iva_retenido", item.IVA_RETENIDO);
                sqlCommand.Parameters.AddWithValue("@ventas", item.VENTAS);
                sqlCommand.Parameters.AddWithValue("@rcertificado", item.RCERTIFICADO);
                sqlCommand.Parameters.AddWithValue("@rllaveprivada", item.RLLAVEPRIVADA);
                sqlCommand.Parameters.AddWithValue("@contraseniallaveprivada", item.CONTRASENIALLAVEPRIVADA);
                sqlCommand.Parameters.AddWithValue("@proveedortimbrado", item.PROVEEDORTIMBRADO);
                sqlCommand.Parameters.AddWithValue("@usuariotimbrado", item.USUARIOTIMBRADO);
                sqlCommand.Parameters.AddWithValue("@contraseniatimbrado", item.CONTRASENIATIMBRADO);
                sqlCommand.Parameters.AddWithValue("@proveedorcancelacion", item.PROVEEDORCANCELACION);
                sqlCommand.Parameters.AddWithValue("@usuariocancelacion", item.USUARIOCANCELACION);
                sqlCommand.Parameters.AddWithValue("@contraseniacancelacion", item.CONTRASENIACANCELACION);
                sqlCommand.Parameters.AddWithValue("@espacioaspel", item.ESPACIOASPEL);
                sqlCommand.Parameters.AddWithValue("@usuarioespacio", item.USUARIOESPACIO);
                sqlCommand.Parameters.AddWithValue("@contraseniaespacio", item.CONTRASENIAESPACIO);
                sqlCommand.Parameters.AddWithValue("@idcategoriaespacio", item.IDCATEGORIAESPACIO);
                sqlCommand.Parameters.AddWithValue("@nombrecategoriaespacio", item.NOMBRECATEGORIAESPACIO);
                sqlCommand.Parameters.AddWithValue("@asigcategoria", item.ASIGCATEGORIA);
                sqlCommand.Parameters.AddWithValue("@gdiotapartircfdi", item.GDIOTAPARTIRCFDI);
                sqlCommand.Parameters.AddWithValue("@guardaropeauto", item.GUARDAROPEAUTO);
                sqlCommand.Parameters.AddWithValue("@cfdientodaslaspartidas", item.CFDIENTODASLASPARTIDAS);
                sqlCommand.Parameters.AddWithValue("@logo", item.LOGO);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(PARAMEMP item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE paramemp 
                    			SET direc = @direc, pobl = @pobl, cod_pos = @cod_pos, rfc_emp = @rfc_emp, ced_emp = @ced_emp, digcta1 = @digcta1, digcta2 = @digcta2, digcta3 = @digcta3, digcta4 = @digcta4, digcta5 = @digcta5, digcta6 = @digcta6, digcta7 = @digcta7, digcta8 = @digcta8, digcta9 = @digcta9, nivelactu = @nivelactu, guion_sino = @guion_sino, redo_p = @redo_p, capsalini = @capsalini, asigsecpol = @asigsecpol, contablinea = @contablinea, mescierre = @mescierre, subdir = @subdir, capctaspol = @capctaspol, depto_sino = @depto_sino, traspaso = @traspaso, ctmonext = @ctmonext, cstipocmb = @cstipocmb, cptipocmb = @cptipocmb, cquemonext = @cquemonext, cquemonctb = @cquemonctb, ctaiva = @ctaiva, ctabanco = @ctabanco, conmontpol = @conmontpol, drivercoi = @drivercoi, alias = @alias, foliounico = @foliounico, capturados = @capturados, poldescuad = @poldescuad, dirtrab = @dirtrab, ivadef = @ivadef, ivaenopt = @ivaenopt, ivaeningr = @ivaeningr, buffcuecua = @buffcuecua, tipopersona = @tipopersona, nacionalidad = @nacionalidad, tipoempresa = @tipoempresa, fconstitucion = @fconstitucion, regimen = @regimen, nombrerepre = @nombrerepre, rfcrepre = @rfcrepre, curprepre = @curprepre, podernot = @podernot, traspasosaldos = @traspasosaldos, nombre = @nombre, ccostos = @ccostos, cgrupos = @cgrupos, etqccostos = @etqccostos, etqcgrupos = @etqcgrupos, ivadefietu = @ivadefietu, tipodecapturadiot = @tipodecapturadiot, tipodecapturaietu = @tipodecapturaietu, cuentaivaacreditable = @cuentaivaacreditable, cuentaivatrasladado = @cuentaivatrasladado, tipopoliza = @tipopoliza, tipopolizater = @tipopolizater, giro = @giro, dev_compras = @dev_compras, dev_ventas = @dev_ventas, gasto_activo = @gasto_activo, ieps_por_acreditar = @ieps_por_acreditar, ieps_por_trasladar = @ieps_por_trasladar, imp_local_retenido = @imp_local_retenido, imp_local_trasladado = @imp_local_trasladado, isr_retenido = @isr_retenido, iva_acreditar = @iva_acreditar, iva_por_trasladar = @iva_por_trasladar, iva_retenido = @iva_retenido, ventas = @ventas, rcertificado = @rcertificado, rllaveprivada = @rllaveprivada, contraseniallaveprivada = @contraseniallaveprivada, proveedortimbrado = @proveedortimbrado, usuariotimbrado = @usuariotimbrado, contraseniatimbrado = @contraseniatimbrado, proveedorcancelacion = @proveedorcancelacion, usuariocancelacion = @usuariocancelacion, contraseniacancelacion = @contraseniacancelacion, espacioaspel = @espacioaspel, usuarioespacio = @usuarioespacio, contraseniaespacio = @contraseniaespacio, idcategoriaespacio = @idcategoriaespacio, nombrecategoriaespacio = @nombrecategoriaespacio, asigcategoria = @asigcategoria, gdiotapartircfdi = @gdiotapartircfdi, guardaropeauto = @guardaropeauto, cfdientodaslaspartidas = @cfdientodaslaspartidas, logo = @logo 
                    			WHERE idemp = @idemp;"
                };
                sqlCommand.Parameters.AddWithValue("@idemp", item.IDEMP);
                sqlCommand.Parameters.AddWithValue("@direc", item.DIREC);
                sqlCommand.Parameters.AddWithValue("@pobl", item.POBL);
                sqlCommand.Parameters.AddWithValue("@cod_pos", item.COD_POS);
                sqlCommand.Parameters.AddWithValue("@rfc_emp", item.RFC_EMP);
                sqlCommand.Parameters.AddWithValue("@ced_emp", item.CED_EMP);
                sqlCommand.Parameters.AddWithValue("@digcta1", item.DIGCTA1);
                sqlCommand.Parameters.AddWithValue("@digcta2", item.DIGCTA2);
                sqlCommand.Parameters.AddWithValue("@digcta3", item.DIGCTA3);
                sqlCommand.Parameters.AddWithValue("@digcta4", item.DIGCTA4);
                sqlCommand.Parameters.AddWithValue("@digcta5", item.DIGCTA5);
                sqlCommand.Parameters.AddWithValue("@digcta6", item.DIGCTA6);
                sqlCommand.Parameters.AddWithValue("@digcta7", item.DIGCTA7);
                sqlCommand.Parameters.AddWithValue("@digcta8", item.DIGCTA8);
                sqlCommand.Parameters.AddWithValue("@digcta9", item.DIGCTA9);
                sqlCommand.Parameters.AddWithValue("@nivelactu", item.NIVELACTU);
                sqlCommand.Parameters.AddWithValue("@guion_sino", item.GUION_SINO);
                sqlCommand.Parameters.AddWithValue("@redo_p", item.REDO_P);
                sqlCommand.Parameters.AddWithValue("@capsalini", item.CAPSALINI);
                sqlCommand.Parameters.AddWithValue("@asigsecpol", item.ASIGSECPOL);
                sqlCommand.Parameters.AddWithValue("@contablinea", item.CONTABLINEA);
                sqlCommand.Parameters.AddWithValue("@mescierre", item.MESCIERRE);
                sqlCommand.Parameters.AddWithValue("@subdir", item.SUBDIR);
                sqlCommand.Parameters.AddWithValue("@capctaspol", item.CAPCTASPOL);
                sqlCommand.Parameters.AddWithValue("@depto_sino", item.DEPTO_SINO);
                sqlCommand.Parameters.AddWithValue("@traspaso", item.TRASPASO);
                sqlCommand.Parameters.AddWithValue("@ctmonext", item.CTMONEXT);
                sqlCommand.Parameters.AddWithValue("@cstipocmb", item.CSTIPOCMB);
                sqlCommand.Parameters.AddWithValue("@cptipocmb", item.CPTIPOCMB);
                sqlCommand.Parameters.AddWithValue("@cquemonext", item.CQUEMONEXT);
                sqlCommand.Parameters.AddWithValue("@cquemonctb", item.CQUEMONCTB);
                sqlCommand.Parameters.AddWithValue("@ctaiva", item.CTAIVA);
                sqlCommand.Parameters.AddWithValue("@ctabanco", item.CTABANCO);
                sqlCommand.Parameters.AddWithValue("@conmontpol", item.CONMONTPOL);
                sqlCommand.Parameters.AddWithValue("@drivercoi", item.DRIVERCOI);
                sqlCommand.Parameters.AddWithValue("@alias", item.ALIAS);
                sqlCommand.Parameters.AddWithValue("@foliounico", item.FOLIOUNICO);
                sqlCommand.Parameters.AddWithValue("@capturados", item.CAPTURADOS);
                sqlCommand.Parameters.AddWithValue("@poldescuad", item.POLDESCUAD);
                sqlCommand.Parameters.AddWithValue("@dirtrab", item.DIRTRAB);
                sqlCommand.Parameters.AddWithValue("@ivadef", item.IVADEF);
                sqlCommand.Parameters.AddWithValue("@ivaenopt", item.IVAENOPT);
                sqlCommand.Parameters.AddWithValue("@ivaeningr", item.IVAENINGR);
                sqlCommand.Parameters.AddWithValue("@buffcuecua", item.BUFFCUECUA);
                sqlCommand.Parameters.AddWithValue("@tipopersona", item.TIPOPERSONA);
                sqlCommand.Parameters.AddWithValue("@nacionalidad", item.NACIONALIDAD);
                sqlCommand.Parameters.AddWithValue("@tipoempresa", item.TIPOEMPRESA);
                sqlCommand.Parameters.AddWithValue("@fconstitucion", item.FCONSTITUCION);
                sqlCommand.Parameters.AddWithValue("@regimen", item.REGIMEN);
                sqlCommand.Parameters.AddWithValue("@nombrerepre", item.NOMBREREPRE);
                sqlCommand.Parameters.AddWithValue("@rfcrepre", item.RFCREPRE);
                sqlCommand.Parameters.AddWithValue("@curprepre", item.CURPREPRE);
                sqlCommand.Parameters.AddWithValue("@podernot", item.PODERNOT);
                sqlCommand.Parameters.AddWithValue("@traspasosaldos", item.TRASPASOSALDOS);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@ccostos", item.CCOSTOS);
                sqlCommand.Parameters.AddWithValue("@cgrupos", item.CGRUPOS);
                sqlCommand.Parameters.AddWithValue("@etqccostos", item.ETQCCOSTOS);
                sqlCommand.Parameters.AddWithValue("@etqcgrupos", item.ETQCGRUPOS);
                sqlCommand.Parameters.AddWithValue("@ivadefietu", item.IVADEFIETU);
                sqlCommand.Parameters.AddWithValue("@tipodecapturadiot", item.TIPODECAPTURADIOT);
                sqlCommand.Parameters.AddWithValue("@tipodecapturaietu", item.TIPODECAPTURAIETU);
                sqlCommand.Parameters.AddWithValue("@cuentaivaacreditable", item.CUENTAIVAACREDITABLE);
                sqlCommand.Parameters.AddWithValue("@cuentaivatrasladado", item.CUENTAIVATRASLADADO);
                sqlCommand.Parameters.AddWithValue("@tipopoliza", item.TIPOPOLIZA);
                sqlCommand.Parameters.AddWithValue("@tipopolizater", item.TIPOPOLIZATER);
                sqlCommand.Parameters.AddWithValue("@giro", item.GIRO);
                sqlCommand.Parameters.AddWithValue("@dev_compras", item.DEV_COMPRAS);
                sqlCommand.Parameters.AddWithValue("@dev_ventas", item.DEV_VENTAS);
                sqlCommand.Parameters.AddWithValue("@gasto_activo", item.GASTO_ACTIVO);
                sqlCommand.Parameters.AddWithValue("@ieps_por_acreditar", item.IEPS_POR_ACREDITAR);
                sqlCommand.Parameters.AddWithValue("@ieps_por_trasladar", item.IEPS_POR_TRASLADAR);
                sqlCommand.Parameters.AddWithValue("@imp_local_retenido", item.IMP_LOCAL_RETENIDO);
                sqlCommand.Parameters.AddWithValue("@imp_local_trasladado", item.IMP_LOCAL_TRASLADADO);
                sqlCommand.Parameters.AddWithValue("@isr_retenido", item.ISR_RETENIDO);
                sqlCommand.Parameters.AddWithValue("@iva_acreditar", item.IVA_ACREDITAR);
                sqlCommand.Parameters.AddWithValue("@iva_por_trasladar", item.IVA_POR_TRASLADAR);
                sqlCommand.Parameters.AddWithValue("@iva_retenido", item.IVA_RETENIDO);
                sqlCommand.Parameters.AddWithValue("@ventas", item.VENTAS);
                sqlCommand.Parameters.AddWithValue("@rcertificado", item.RCERTIFICADO);
                sqlCommand.Parameters.AddWithValue("@rllaveprivada", item.RLLAVEPRIVADA);
                sqlCommand.Parameters.AddWithValue("@contraseniallaveprivada", item.CONTRASENIALLAVEPRIVADA);
                sqlCommand.Parameters.AddWithValue("@proveedortimbrado", item.PROVEEDORTIMBRADO);
                sqlCommand.Parameters.AddWithValue("@usuariotimbrado", item.USUARIOTIMBRADO);
                sqlCommand.Parameters.AddWithValue("@contraseniatimbrado", item.CONTRASENIATIMBRADO);
                sqlCommand.Parameters.AddWithValue("@proveedorcancelacion", item.PROVEEDORCANCELACION);
                sqlCommand.Parameters.AddWithValue("@usuariocancelacion", item.USUARIOCANCELACION);
                sqlCommand.Parameters.AddWithValue("@contraseniacancelacion", item.CONTRASENIACANCELACION);
                sqlCommand.Parameters.AddWithValue("@espacioaspel", item.ESPACIOASPEL);
                sqlCommand.Parameters.AddWithValue("@usuarioespacio", item.USUARIOESPACIO);
                sqlCommand.Parameters.AddWithValue("@contraseniaespacio", item.CONTRASENIAESPACIO);
                sqlCommand.Parameters.AddWithValue("@idcategoriaespacio", item.IDCATEGORIAESPACIO);
                sqlCommand.Parameters.AddWithValue("@nombrecategoriaespacio", item.NOMBRECATEGORIAESPACIO);
                sqlCommand.Parameters.AddWithValue("@asigcategoria", item.ASIGCATEGORIA);
                sqlCommand.Parameters.AddWithValue("@gdiotapartircfdi", item.GDIOTAPARTIRCFDI);
                sqlCommand.Parameters.AddWithValue("@guardaropeauto", item.GUARDAROPEAUTO);
                sqlCommand.Parameters.AddWithValue("@cfdientodaslaspartidas", item.CFDIENTODASLASPARTIDAS);
                sqlCommand.Parameters.AddWithValue("@logo", item.LOGO);
                return this.ExecuteScalar(sqlCommand);
            }

            public PARAMEMP GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM paramemp WHERE idemp = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<PARAMEMP>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<PARAMEMP> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM paramemp")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<PARAMEMP>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }