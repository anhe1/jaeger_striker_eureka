    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlDesietRepository : IGenericRepository<DESIET> {}

        public class SqlFbDesietRepository : MasterRepository, ISqlDesietRepository {

    	    public SqlFbDesietRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(DESIET item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO desiet (tipopol, numpol, fechapol, numpart, indice, monto, cuentagas, montoded, porcded, idconcep) 
                    			VALUES (@tipopol, @numpol, @fechapol, @numpart, @indice, @monto, @cuentagas, @montoded, @porcded, @idconcep)"
                };
                item.TIPOPOL = this.Max("TIPOPOL");
                sqlCommand.Parameters.AddWithValue("@tipopol", item.TIPOPOL);
                sqlCommand.Parameters.AddWithValue("@numpol", item.NUMPOL);
                sqlCommand.Parameters.AddWithValue("@fechapol", item.FECHAPOL);
                sqlCommand.Parameters.AddWithValue("@numpart", item.NUMPART);
                sqlCommand.Parameters.AddWithValue("@indice", item.INDICE);
                sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
                sqlCommand.Parameters.AddWithValue("@cuentagas", item.CUENTAGAS);
                sqlCommand.Parameters.AddWithValue("@montoded", item.MONTODED);
                sqlCommand.Parameters.AddWithValue("@porcded", item.PORCDED);
                sqlCommand.Parameters.AddWithValue("@idconcep", item.IDCONCEP);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(DESIET item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE desiet 
                    			SET numpol = @numpol, fechapol = @fechapol, numpart = @numpart, indice = @indice, monto = @monto, cuentagas = @cuentaga, montoded = @montoded, porcded = @porcded, idconcep = @idconcep 
                    			WHERE tipopol = @tipopol;"
                };
                sqlCommand.Parameters.AddWithValue("@tipopol", item.TIPOPOL);
                sqlCommand.Parameters.AddWithValue("@numpol", item.NUMPOL);
                sqlCommand.Parameters.AddWithValue("@fechapol", item.FECHAPOL);
                sqlCommand.Parameters.AddWithValue("@numpart", item.NUMPART);
                sqlCommand.Parameters.AddWithValue("@indice", item.INDICE);
                sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
                sqlCommand.Parameters.AddWithValue("@cuentagas", item.CUENTAGAS);
                sqlCommand.Parameters.AddWithValue("@montoded", item.MONTODED);
                sqlCommand.Parameters.AddWithValue("@porcded", item.PORCDED);
                sqlCommand.Parameters.AddWithValue("@idconcep", item.IDCONCEP);
                return this.ExecuteScalar(sqlCommand);
            }

            public DESIET GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM desiet WHERE tipopol = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<DESIET>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<DESIET> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM desiet")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<DESIET>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }