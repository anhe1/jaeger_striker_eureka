    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlFoliosRepository : IGenericRepository<FOLIOS> {}

        public class SqlFbFoliosRepository : MasterRepository, ISqlFoliosRepository {

    	    public SqlFbFoliosRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(FOLIOS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO folios (tippol, ejercicio, folio01, folio02, folio03, folio04, folio05, folio06, folio07, folio08, folio09, folio10, folio11, folio12, folio13, asig01, asig02, asig03, asig04, asig05, asig06, asig07, asig08, asig09, asig10, asig11, asig12, asig13, asig14, folio14) 
                    			VALUES (@tippol, @ejercicio, @folio01, @folio02, @folio03, @folio04, @folio05, @folio06, @folio07, @folio08, @folio09, @folio10, @folio11, @folio12, @folio13, @asig01, @asig02, @asig03, @asig04, @asig05, @asig06, @asig07, @asig08, @asig09, @asig10, @asig11, @asig12, @asig13, @asig14, @folio14)"
                };
                item.TIPPOL = this.Max("TIPPOL");
                sqlCommand.Parameters.AddWithValue("@tippol", item.TIPPOL);
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@folio01", item.FOLIO01);
                sqlCommand.Parameters.AddWithValue("@folio02", item.FOLIO02);
                sqlCommand.Parameters.AddWithValue("@folio03", item.FOLIO03);
                sqlCommand.Parameters.AddWithValue("@folio04", item.FOLIO04);
                sqlCommand.Parameters.AddWithValue("@folio05", item.FOLIO05);
                sqlCommand.Parameters.AddWithValue("@folio06", item.FOLIO06);
                sqlCommand.Parameters.AddWithValue("@folio07", item.FOLIO07);
                sqlCommand.Parameters.AddWithValue("@folio08", item.FOLIO08);
                sqlCommand.Parameters.AddWithValue("@folio09", item.FOLIO09);
                sqlCommand.Parameters.AddWithValue("@folio10", item.FOLIO10);
                sqlCommand.Parameters.AddWithValue("@folio11", item.FOLIO11);
                sqlCommand.Parameters.AddWithValue("@folio12", item.FOLIO12);
                sqlCommand.Parameters.AddWithValue("@folio13", item.FOLIO13);
                sqlCommand.Parameters.AddWithValue("@asig01", item.ASIG01);
                sqlCommand.Parameters.AddWithValue("@asig02", item.ASIG02);
                sqlCommand.Parameters.AddWithValue("@asig03", item.ASIG03);
                sqlCommand.Parameters.AddWithValue("@asig04", item.ASIG04);
                sqlCommand.Parameters.AddWithValue("@asig05", item.ASIG05);
                sqlCommand.Parameters.AddWithValue("@asig06", item.ASIG06);
                sqlCommand.Parameters.AddWithValue("@asig07", item.ASIG07);
                sqlCommand.Parameters.AddWithValue("@asig08", item.ASIG08);
                sqlCommand.Parameters.AddWithValue("@asig09", item.ASIG09);
                sqlCommand.Parameters.AddWithValue("@asig10", item.ASIG10);
                sqlCommand.Parameters.AddWithValue("@asig11", item.ASIG11);
                sqlCommand.Parameters.AddWithValue("@asig12", item.ASIG12);
                sqlCommand.Parameters.AddWithValue("@asig13", item.ASIG13);
                sqlCommand.Parameters.AddWithValue("@asig14", item.ASIG14);
                sqlCommand.Parameters.AddWithValue("@folio14", item.FOLIO14);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(FOLIOS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE folios 
                    			SET ejercicio = @ejercicio, folio01 = @folio01, folio02 = @folio02, folio03 = @folio03, folio04 = @folio04, folio05 = @folio05, folio06 = @folio06, folio07 = @folio07, folio08 = @folio08, folio09 = @folio09, folio10 = @folio10, folio11 = @folio11, folio12 = @folio12, folio13 = @folio13, asig01 = @asig01, asig02 = @asig02, asig03 = @asig03, asig04 = @asig04, asig05 = @asig05, asig06 = @asig06, asig07 = @asig07, asig08 = @asig08, asig09 = @asig09, asig10 = @asig10, asig11 = @asig11, asig12 = @asig12, asig13 = @asig13, asig14 = @asig14, folio14 = @folio14 
                    			WHERE tippol = @tippol;"
                };
                sqlCommand.Parameters.AddWithValue("@tippol", item.TIPPOL);
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@folio01", item.FOLIO01);
                sqlCommand.Parameters.AddWithValue("@folio02", item.FOLIO02);
                sqlCommand.Parameters.AddWithValue("@folio03", item.FOLIO03);
                sqlCommand.Parameters.AddWithValue("@folio04", item.FOLIO04);
                sqlCommand.Parameters.AddWithValue("@folio05", item.FOLIO05);
                sqlCommand.Parameters.AddWithValue("@folio06", item.FOLIO06);
                sqlCommand.Parameters.AddWithValue("@folio07", item.FOLIO07);
                sqlCommand.Parameters.AddWithValue("@folio08", item.FOLIO08);
                sqlCommand.Parameters.AddWithValue("@folio09", item.FOLIO09);
                sqlCommand.Parameters.AddWithValue("@folio10", item.FOLIO10);
                sqlCommand.Parameters.AddWithValue("@folio11", item.FOLIO11);
                sqlCommand.Parameters.AddWithValue("@folio12", item.FOLIO12);
                sqlCommand.Parameters.AddWithValue("@folio13", item.FOLIO13);
                sqlCommand.Parameters.AddWithValue("@asig01", item.ASIG01);
                sqlCommand.Parameters.AddWithValue("@asig02", item.ASIG02);
                sqlCommand.Parameters.AddWithValue("@asig03", item.ASIG03);
                sqlCommand.Parameters.AddWithValue("@asig04", item.ASIG04);
                sqlCommand.Parameters.AddWithValue("@asig05", item.ASIG05);
                sqlCommand.Parameters.AddWithValue("@asig06", item.ASIG06);
                sqlCommand.Parameters.AddWithValue("@asig07", item.ASIG07);
                sqlCommand.Parameters.AddWithValue("@asig08", item.ASIG08);
                sqlCommand.Parameters.AddWithValue("@asig09", item.ASIG09);
                sqlCommand.Parameters.AddWithValue("@asig10", item.ASIG10);
                sqlCommand.Parameters.AddWithValue("@asig11", item.ASIG11);
                sqlCommand.Parameters.AddWithValue("@asig12", item.ASIG12);
                sqlCommand.Parameters.AddWithValue("@asig13", item.ASIG13);
                sqlCommand.Parameters.AddWithValue("@asig14", item.ASIG14);
                sqlCommand.Parameters.AddWithValue("@folio14", item.FOLIO14);
                return this.ExecuteScalar(sqlCommand);
            }

            public FOLIOS GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM folios WHERE tippol = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<FOLIOS>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<FOLIOS> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM folios")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<FOLIOS>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }