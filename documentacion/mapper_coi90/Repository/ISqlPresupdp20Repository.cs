    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlPresupdp20Repository : IGenericRepository<PRESUPDP20> {}

        public class SqlFbPresupdp20Repository : MasterRepository, ISqlPresupdp20Repository {

    	    public SqlFbPresupdp20Repository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(PRESUPDP20 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO presupdp20 (ejercicio, num_cta, depto, presup01, presup02, presup03, presup04, presup05, presup06, presup07, presup08, presup09, presup10, presup11, presup12, presup13, presup14) 
                    			VALUES (@ejercicio, @num_cta, @depto, @presup01, @presup02, @presup03, @presup04, @presup05, @presup06, @presup07, @presup08, @presup09, @presup10, @presup11, @presup12, @presup13, @presup14)"
                };
                item.EJERCICIO = this.Max("EJERCICIO");
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@depto", item.DEPTO);
                sqlCommand.Parameters.AddWithValue("@presup01", item.PRESUP01);
                sqlCommand.Parameters.AddWithValue("@presup02", item.PRESUP02);
                sqlCommand.Parameters.AddWithValue("@presup03", item.PRESUP03);
                sqlCommand.Parameters.AddWithValue("@presup04", item.PRESUP04);
                sqlCommand.Parameters.AddWithValue("@presup05", item.PRESUP05);
                sqlCommand.Parameters.AddWithValue("@presup06", item.PRESUP06);
                sqlCommand.Parameters.AddWithValue("@presup07", item.PRESUP07);
                sqlCommand.Parameters.AddWithValue("@presup08", item.PRESUP08);
                sqlCommand.Parameters.AddWithValue("@presup09", item.PRESUP09);
                sqlCommand.Parameters.AddWithValue("@presup10", item.PRESUP10);
                sqlCommand.Parameters.AddWithValue("@presup11", item.PRESUP11);
                sqlCommand.Parameters.AddWithValue("@presup12", item.PRESUP12);
                sqlCommand.Parameters.AddWithValue("@presup13", item.PRESUP13);
                sqlCommand.Parameters.AddWithValue("@presup14", item.PRESUP14);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(PRESUPDP20 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE presupdp20 
                    			SET num_cta = @num_cta, depto = @depto, presup01 = @presup01, presup02 = @presup02, presup03 = @presup03, presup04 = @presup04, presup05 = @presup05, presup06 = @presup06, presup07 = @presup07, presup08 = @presup08, presup09 = @presup09, presup10 = @presup10, presup11 = @presup11, presup12 = @presup12, presup13 = @presup13, presup14 = @presup14 
                    			WHERE ejercicio = @ejercicio;"
                };
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@depto", item.DEPTO);
                sqlCommand.Parameters.AddWithValue("@presup01", item.PRESUP01);
                sqlCommand.Parameters.AddWithValue("@presup02", item.PRESUP02);
                sqlCommand.Parameters.AddWithValue("@presup03", item.PRESUP03);
                sqlCommand.Parameters.AddWithValue("@presup04", item.PRESUP04);
                sqlCommand.Parameters.AddWithValue("@presup05", item.PRESUP05);
                sqlCommand.Parameters.AddWithValue("@presup06", item.PRESUP06);
                sqlCommand.Parameters.AddWithValue("@presup07", item.PRESUP07);
                sqlCommand.Parameters.AddWithValue("@presup08", item.PRESUP08);
                sqlCommand.Parameters.AddWithValue("@presup09", item.PRESUP09);
                sqlCommand.Parameters.AddWithValue("@presup10", item.PRESUP10);
                sqlCommand.Parameters.AddWithValue("@presup11", item.PRESUP11);
                sqlCommand.Parameters.AddWithValue("@presup12", item.PRESUP12);
                sqlCommand.Parameters.AddWithValue("@presup13", item.PRESUP13);
                sqlCommand.Parameters.AddWithValue("@presup14", item.PRESUP14);
                return this.ExecuteScalar(sqlCommand);
            }

            public PRESUPDP20 GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM presupdp20 WHERE ejercicio = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<PRESUPDP20>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<PRESUPDP20> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM presupdp20")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<PRESUPDP20>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }