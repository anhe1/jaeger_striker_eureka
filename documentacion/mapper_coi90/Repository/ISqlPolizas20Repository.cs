    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlPolizas20Repository : IGenericRepository<POLIZAS20> {}

        public class SqlFbPolizas20Repository : MasterRepository, ISqlPolizas20Repository {

    	    public SqlFbPolizas20Repository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(POLIZAS20 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO polizas20 (tipo_poli, num_poliz, periodo, ejercicio, fecha_pol, concep_po, num_part, logaudita, contabiliz, numparcua, tienedocum, proccontab, origen, uuid, espolizapr, uuidop) 
                    			VALUES (@tipo_poli, @num_poliz, @periodo, @ejercicio, @fecha_pol, @concep_po, @num_part, @logaudita, @contabiliz, @numparcua, @tienedocum, @proccontab, @origen, @uuid, @espolizapr, @uuidop)"
                };
                item.TIPO_POLI = this.Max("TIPO_POLI");
                sqlCommand.Parameters.AddWithValue("@tipo_poli", item.TIPO_POLI);
                sqlCommand.Parameters.AddWithValue("@num_poliz", item.NUM_POLIZ);
                sqlCommand.Parameters.AddWithValue("@periodo", item.PERIODO);
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@fecha_pol", item.FECHA_POL);
                sqlCommand.Parameters.AddWithValue("@concep_po", item.CONCEP_PO);
                sqlCommand.Parameters.AddWithValue("@num_part", item.NUM_PART);
                sqlCommand.Parameters.AddWithValue("@logaudita", item.LOGAUDITA);
                sqlCommand.Parameters.AddWithValue("@contabiliz", item.CONTABILIZ);
                sqlCommand.Parameters.AddWithValue("@numparcua", item.NUMPARCUA);
                sqlCommand.Parameters.AddWithValue("@tienedocum", item.TIENEDOCUMENTOS);
                sqlCommand.Parameters.AddWithValue("@proccontab", item.PROCCONTAB);
                sqlCommand.Parameters.AddWithValue("@origen", item.ORIGEN);
                sqlCommand.Parameters.AddWithValue("@uuid", item.UUID);
                sqlCommand.Parameters.AddWithValue("@espolizapr", item.ESPOLIZAPRIVADA);
                sqlCommand.Parameters.AddWithValue("@uuidop", item.UUIDOP);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(POLIZAS20 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE polizas20 
                    			SET num_poliz = @num_poliz, periodo = @periodo, ejercicio = @ejercicio, fecha_pol = @fecha_pol, concep_po = @concep_po, num_part = @num_part, logaudita = @logaudita, contabiliz = @contabiliz, numparcua = @numparcua, tienedocum = @tienedocum, proccontab = @proccontab, origen = @origen, uuid = @uuid, espolizapr = @espolizapr, uuidop = @uuidop 
                    			WHERE tipo_poli = @tipo_poli;"
                };
                sqlCommand.Parameters.AddWithValue("@tipo_poli", item.TIPO_POLI);
                sqlCommand.Parameters.AddWithValue("@num_poliz", item.NUM_POLIZ);
                sqlCommand.Parameters.AddWithValue("@periodo", item.PERIODO);
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@fecha_pol", item.FECHA_POL);
                sqlCommand.Parameters.AddWithValue("@concep_po", item.CONCEP_PO);
                sqlCommand.Parameters.AddWithValue("@num_part", item.NUM_PART);
                sqlCommand.Parameters.AddWithValue("@logaudita", item.LOGAUDITA);
                sqlCommand.Parameters.AddWithValue("@contabiliz", item.CONTABILIZ);
                sqlCommand.Parameters.AddWithValue("@numparcua", item.NUMPARCUA);
                sqlCommand.Parameters.AddWithValue("@tienedocum", item.TIENEDOCUMENTOS);
                sqlCommand.Parameters.AddWithValue("@proccontab", item.PROCCONTAB);
                sqlCommand.Parameters.AddWithValue("@origen", item.ORIGEN);
                sqlCommand.Parameters.AddWithValue("@uuid", item.UUID);
                sqlCommand.Parameters.AddWithValue("@espolizapr", item.ESPOLIZAPRIVADA);
                sqlCommand.Parameters.AddWithValue("@uuidop", item.UUIDOP);
                return this.ExecuteScalar(sqlCommand);
            }

            public POLIZAS20 GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM polizas20 WHERE tipo_poli = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<POLIZAS20>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<POLIZAS20> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM polizas20")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<POLIZAS20>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }