    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlRegpolRepository : IGenericRepository<REGPOL> {}

        public class SqlFbRegpolRepository : MasterRepository, ISqlRegpolRepository {

    	    public SqlFbRegpolRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(REGPOL item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO regpol (transaccio, tipopol, numpol, fechapol, fechaopr, sistema, numusr, operacion, status, tippolintr, polmodelo, regconcli, statuscli) 
                    			VALUES (@transaccio, @tipopol, @numpol, @fechapol, @fechaopr, @sistema, @numusr, @operacion, @status, @tippolintr, @polmodelo, @regconcli, @statuscli)"
                };
                item.TRANSACCIO = this.Max("TRANSACCIO");
                sqlCommand.Parameters.AddWithValue("@transaccio", item.TRANSACCIO);
                sqlCommand.Parameters.AddWithValue("@tipopol", item.TIPOPOL);
                sqlCommand.Parameters.AddWithValue("@numpol", item.NUMPOL);
                sqlCommand.Parameters.AddWithValue("@fechapol", item.FECHAPOL);
                sqlCommand.Parameters.AddWithValue("@fechaopr", item.FECHAOPR);
                sqlCommand.Parameters.AddWithValue("@sistema", item.SISTEMA);
                sqlCommand.Parameters.AddWithValue("@numusr", item.NUMUSR);
                sqlCommand.Parameters.AddWithValue("@operacion", item.OPERACION);
                sqlCommand.Parameters.AddWithValue("@status", item.STATUS);
                sqlCommand.Parameters.AddWithValue("@tippolintr", item.TIPPOLINTR);
                sqlCommand.Parameters.AddWithValue("@polmodelo", item.POLMODELO);
                sqlCommand.Parameters.AddWithValue("@regconcli", item.REGCONCLI);
                sqlCommand.Parameters.AddWithValue("@statuscli", item.STATUSCLI);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(REGPOL item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE regpol 
                    			SET tipopol = @tipopol, numpol = @numpol, fechapol = @fechapol, fechaopr = @fechaopr, sistema = @sistema, numusr = @numusr, operacion = @operacion, status = @status, tippolintr = @tippolintr, polmodelo = @polmodelo, regconcli = @regconcli, statuscli = @statuscli 
                    			WHERE transaccio = @transaccio;"
                };
                sqlCommand.Parameters.AddWithValue("@transaccio", item.TRANSACCIO);
                sqlCommand.Parameters.AddWithValue("@tipopol", item.TIPOPOL);
                sqlCommand.Parameters.AddWithValue("@numpol", item.NUMPOL);
                sqlCommand.Parameters.AddWithValue("@fechapol", item.FECHAPOL);
                sqlCommand.Parameters.AddWithValue("@fechaopr", item.FECHAOPR);
                sqlCommand.Parameters.AddWithValue("@sistema", item.SISTEMA);
                sqlCommand.Parameters.AddWithValue("@numusr", item.NUMUSR);
                sqlCommand.Parameters.AddWithValue("@operacion", item.OPERACION);
                sqlCommand.Parameters.AddWithValue("@status", item.STATUS);
                sqlCommand.Parameters.AddWithValue("@tippolintr", item.TIPPOLINTR);
                sqlCommand.Parameters.AddWithValue("@polmodelo", item.POLMODELO);
                sqlCommand.Parameters.AddWithValue("@regconcli", item.REGCONCLI);
                sqlCommand.Parameters.AddWithValue("@statuscli", item.STATUSCLI);
                return this.ExecuteScalar(sqlCommand);
            }

            public REGPOL GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM regpol WHERE transaccio = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<REGPOL>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<REGPOL> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM regpol")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<REGPOL>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }