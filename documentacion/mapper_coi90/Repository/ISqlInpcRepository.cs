    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlInpcRepository : IGenericRepository<INPC> {}

        public class SqlFbInpcRepository : MasterRepository, ISqlInpcRepository {

    	    public SqlFbInpcRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(INPC item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO inpc (ejercicio, mes1, mes2, mes3, mes4, mes5, mes6, mes7, mes8, mes9, mes10, mes11, mes12) 
                    			VALUES (@ejercicio, @mes1, @mes2, @mes3, @mes4, @mes5, @mes6, @mes7, @mes8, @mes9, @mes10, @mes11, @mes12)"
                };
                item.EJERCICIO = this.Max("EJERCICIO");
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@mes1", item.MES1);
                sqlCommand.Parameters.AddWithValue("@mes2", item.MES2);
                sqlCommand.Parameters.AddWithValue("@mes3", item.MES3);
                sqlCommand.Parameters.AddWithValue("@mes4", item.MES4);
                sqlCommand.Parameters.AddWithValue("@mes5", item.MES5);
                sqlCommand.Parameters.AddWithValue("@mes6", item.MES6);
                sqlCommand.Parameters.AddWithValue("@mes7", item.MES7);
                sqlCommand.Parameters.AddWithValue("@mes8", item.MES8);
                sqlCommand.Parameters.AddWithValue("@mes9", item.MES9);
                sqlCommand.Parameters.AddWithValue("@mes10", item.MES10);
                sqlCommand.Parameters.AddWithValue("@mes11", item.MES11);
                sqlCommand.Parameters.AddWithValue("@mes12", item.MES12);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(INPC item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE inpc 
                    			SET mes1 = @mes1, mes2 = @mes2, mes3 = @mes3, mes4 = @mes4, mes5 = @mes5, mes6 = @mes6, mes7 = @mes7, mes8 = @mes8, mes9 = @mes9, mes10 = @mes10, mes11 = @mes11, mes12 = @mes12 
                    			WHERE ejercicio = @ejercicio;"
                };
                sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
                sqlCommand.Parameters.AddWithValue("@mes1", item.MES1);
                sqlCommand.Parameters.AddWithValue("@mes2", item.MES2);
                sqlCommand.Parameters.AddWithValue("@mes3", item.MES3);
                sqlCommand.Parameters.AddWithValue("@mes4", item.MES4);
                sqlCommand.Parameters.AddWithValue("@mes5", item.MES5);
                sqlCommand.Parameters.AddWithValue("@mes6", item.MES6);
                sqlCommand.Parameters.AddWithValue("@mes7", item.MES7);
                sqlCommand.Parameters.AddWithValue("@mes8", item.MES8);
                sqlCommand.Parameters.AddWithValue("@mes9", item.MES9);
                sqlCommand.Parameters.AddWithValue("@mes10", item.MES10);
                sqlCommand.Parameters.AddWithValue("@mes11", item.MES11);
                sqlCommand.Parameters.AddWithValue("@mes12", item.MES12);
                return this.ExecuteScalar(sqlCommand);
            }

            public INPC GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM inpc WHERE ejercicio = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<INPC>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<INPC> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM inpc")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<INPC>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }