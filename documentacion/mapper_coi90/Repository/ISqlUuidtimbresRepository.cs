    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlUuidtimbresRepository : IGenericRepository<UUIDTIMBRES> {}

        public class SqlFbUuidtimbresRepository : MasterRepository, ISqlUuidtimbresRepository {

    	    public SqlFbUuidtimbresRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(UUIDTIMBRES item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO uuidtimbres (numreg, uuidtimbre, monto, serie, folio, rfcemisor, rfcreceptor, orden, fecha, tipocomprobante, tipocambio, versioncfdi, moneda) 
                    			VALUES (@numreg, @uuidtimbre, @monto, @serie, @folio, @rfcemisor, @rfcreceptor, @orden, @fecha, @tipocomprobante, @tipocambio, @versioncfdi, @moneda)"
                };
                item.NUMREG = this.Max("NUMREG");
                sqlCommand.Parameters.AddWithValue("@numreg", item.NUMREG);
                sqlCommand.Parameters.AddWithValue("@uuidtimbre", item.UUIDTIMBRE);
                sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
                sqlCommand.Parameters.AddWithValue("@serie", item.SERIE);
                sqlCommand.Parameters.AddWithValue("@folio", item.FOLIO);
                sqlCommand.Parameters.AddWithValue("@rfcemisor", item.RFCEMISOR);
                sqlCommand.Parameters.AddWithValue("@rfcreceptor", item.RFCRECEPTOR);
                sqlCommand.Parameters.AddWithValue("@orden", item.ORDEN);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@tipocomprobante", item.TIPOCOMPROBANTE);
                sqlCommand.Parameters.AddWithValue("@tipocambio", item.TIPOCAMBIO);
                sqlCommand.Parameters.AddWithValue("@versioncfdi", item.VERSIONCFDI);
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(UUIDTIMBRES item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE uuidtimbres 
                    			SET uuidtimbre = @uuidtimbre , monto = @monto , serie = @serie , folio = @folio , rfcemisor = @rfcemisor , rfcreceptor = @rfcreceptor , orden = @orden , fecha = @fecha , tipocomprobante = @tipocomprobante , tipocambio = @tipocambio , versioncfdi = @versioncfdi , moneda = @moneda 
                    			WHERE numreg = @numreg;"
                };
                sqlCommand.Parameters.AddWithValue("@numreg", item.NUMREG);
                sqlCommand.Parameters.AddWithValue("@uuidtimbre", item.UUIDTIMBRE);
                sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
                sqlCommand.Parameters.AddWithValue("@serie", item.SERIE);
                sqlCommand.Parameters.AddWithValue("@folio", item.FOLIO);
                sqlCommand.Parameters.AddWithValue("@rfcemisor", item.RFCEMISOR);
                sqlCommand.Parameters.AddWithValue("@rfcreceptor", item.RFCRECEPTOR);
                sqlCommand.Parameters.AddWithValue("@orden", item.ORDEN);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@tipocomprobante", item.TIPOCOMPROBANTE);
                sqlCommand.Parameters.AddWithValue("@tipocambio", item.TIPOCAMBIO);
                sqlCommand.Parameters.AddWithValue("@versioncfdi", item.VERSIONCFDI);
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                return this.ExecuteScalar(sqlCommand);
            }

            public UUIDTIMBRES GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM uuidtimbres WHERE numreg = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<UUIDTIMBRES>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<UUIDTIMBRES> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM uuidtimbres")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<UUIDTIMBRES>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }