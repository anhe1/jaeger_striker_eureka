    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlCtaingRepository : IGenericRepository<CTAING> {}

        public class SqlFbCtaingRepository : MasterRepository, ISqlCtaingRepository {

    	    public SqlFbCtaingRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(CTAING item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO ctaing (cuenta, tipo, rfcidfisc, ivadefault, porcentajeiva, porcentajeisr, incluyeiva, idconcep, idconcepivat) 
                    			VALUES (@cuenta, @tipo, @rfcidfisc, @ivadefault, @porcentajeiva, @porcentajeisr, @incluyeiva, @idconcep, @idconcepivat)"
                };
                item.CUENTA = this.Max("CUENTA");
                sqlCommand.Parameters.AddWithValue("@cuenta", item.CUENTA);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@rfcidfisc", item.RFCIDFISC);
                sqlCommand.Parameters.AddWithValue("@ivadefault", item.IVADEFAULT);
                sqlCommand.Parameters.AddWithValue("@porcentajeiva", item.PORCENTAJEIVA);
                sqlCommand.Parameters.AddWithValue("@porcentajeisr", item.PORCENTAJEISR);
                sqlCommand.Parameters.AddWithValue("@incluyeiva", item.INCLUYEIVA);
                sqlCommand.Parameters.AddWithValue("@idconcep", item.IDCONCEP);
                sqlCommand.Parameters.AddWithValue("@idconcepivat", item.IDCONCEPIVAT);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(CTAING item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE ctaing 
                    			SET tipo = @tipo, rfcidfisc = @rfcidfisc, ivadefault = @ivadefault, porcentajeiva = @porcentajeiva, porcentajeisr = @porcentajeisr, incluyeiva = @incluyeiva, idconcep = @idconcep, idconcepivat = @idconcepivat 
                    			WHERE cuenta = @cuenta;"
                };
                sqlCommand.Parameters.AddWithValue("@cuenta", item.CUENTA);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@rfcidfisc", item.RFCIDFISC);
                sqlCommand.Parameters.AddWithValue("@ivadefault", item.IVADEFAULT);
                sqlCommand.Parameters.AddWithValue("@porcentajeiva", item.PORCENTAJEIVA);
                sqlCommand.Parameters.AddWithValue("@porcentajeisr", item.PORCENTAJEISR);
                sqlCommand.Parameters.AddWithValue("@incluyeiva", item.INCLUYEIVA);
                sqlCommand.Parameters.AddWithValue("@idconcep", item.IDCONCEP);
                sqlCommand.Parameters.AddWithValue("@idconcepivat", item.IDCONCEPIVAT);
                return this.ExecuteScalar(sqlCommand);
            }

            public CTAING GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM ctaing WHERE cuenta = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CTAING>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<CTAING> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM ctaing")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CTAING>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }