using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Repository {

    public interface ISqlCtarubRepository : IGenericRepository<CTARUB> {}

    public class SqlFbCtarubRepository : MasterRepository, ISqlCtarubRepository {

        public SqlFbCtarubRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

        public int Insert(CTARUB item) {
            var sqlCommand = new FbCommand{
                CommandText = @"INSERT INTO ctarub (rubro, cuenta, segpapa)
                            VALUES (@rubro, @cuenta, @segpapa)"
            };
            item.RUBRO = this.Max("RUBRO");
            sqlCommand.Parameters.AddWithValue("@rubro", item.RUBRO);
            sqlCommand.Parameters.AddWithValue("@cuenta", item.CUENTA);
            sqlCommand.Parameters.AddWithValue("@segpapa", item.SEGPAPA);
            return this.ExecuteScalar(sqlCommand);
        }

        public int Update(CTARUB item) {
            var sqlCommand = new FbCommand{
                CommandText = @"UPDATE ctarub 
                            SET cuenta = @cuenta, segpapa = @segpapa 
                            WHERE rubro = @rubro;"
            };
            sqlCommand.Parameters.AddWithValue("@rubro", item.RUBRO);
            sqlCommand.Parameters.AddWithValue("@cuenta", item.CUENTA);
            sqlCommand.Parameters.AddWithValue("@segpapa", item.SEGPAPA);
            return this.ExecuteScalar(sqlCommand);
        }

        public CTARUB GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM ctarub WHERE rubro = @id")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CTARUB>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<CTARUB> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM ctarub")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CTARUB>();
            return mapper.Map(tabla).ToList();
        }

    }
}