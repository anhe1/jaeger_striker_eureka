    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlComdRepository : IGenericRepository<COMD> {}

        public class SqlFbComdRepository : MasterRepository, ISqlComdRepository {

    	    public SqlFbComdRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(COMD item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO comd (cve_concep, tipo, concep, concepsae, cta_contab, estado, iva, clasificacion, plantilla) 
                    			VALUES (@cve_concep, @tipo, @concep, @concepsae, @cta_contab, @estado, @iva, @clasificacion, @plantilla)"
                };
                item.CVE_CONCEP = this.Max("CVE_CONCEP");
                sqlCommand.Parameters.AddWithValue("@cve_concep", item.CVE_CONCEP);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@concep", item.CONCEP);
                sqlCommand.Parameters.AddWithValue("@concepsae", item.CONCEPSAE);
                sqlCommand.Parameters.AddWithValue("@cta_contab", item.CTA_CONTAB);
                sqlCommand.Parameters.AddWithValue("@estado", item.ESTADO);
                sqlCommand.Parameters.AddWithValue("@iva", item.IVA);
                sqlCommand.Parameters.AddWithValue("@clasificacion", item.CLASIFICACION);
                sqlCommand.Parameters.AddWithValue("@plantilla", item.PLANTILLA);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(COMD item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE comd 
                    		SET tipo = @tipo, concep = @concep, concepsae = @concepsae, cta_contab = @cta_contab, estado = @estado, iva = @iva, clasificacion = @clasificacion, plantilla = @plantilla
                    		WHERE cve_concep = @cve_concep;"
                };
                sqlCommand.Parameters.AddWithValue("@cve_concep", item.CVE_CONCEP);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@concep", item.CONCEP);
                sqlCommand.Parameters.AddWithValue("@concepsae", item.CONCEPSAE);
                sqlCommand.Parameters.AddWithValue("@cta_contab", item.CTA_CONTAB);
                sqlCommand.Parameters.AddWithValue("@estado", item.ESTADO);
                sqlCommand.Parameters.AddWithValue("@iva", item.IVA);
                sqlCommand.Parameters.AddWithValue("@clasificacion", item.CLASIFICACION);
                sqlCommand.Parameters.AddWithValue("@plantilla", item.PLANTILLA);
                return this.ExecuteScalar(sqlCommand);
            }

            public COMD GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM comd WHERE cve_concep = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<COMD>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<COMD> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM comd")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<COMD>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }