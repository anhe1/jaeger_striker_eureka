    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlCteconcRepository : IGenericRepository<CTECONC> {}

        public class SqlFbCteconcRepository : MasterRepository, ISqlCteconcRepository {

    	    public SqlFbCteconcRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(CTECONC item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO cteconc (num_reg, num_cta, fecha_reg, fecha_inicio, fecha_fin, tot_cargos, tot_abonos, saldo) 
                    			VALUES (@num_reg, @num_cta, @fecha_reg, @fecha_inicio, @fecha_fin, @tot_cargos, @tot_abonos, @saldo)"
                };
                item.NUM_REG = this.Max("NUM_REG");
                sqlCommand.Parameters.AddWithValue("@num_reg", item.NUM_REG);
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@fecha_reg", item.FECHA_REG);
                sqlCommand.Parameters.AddWithValue("@fecha_inicio", item.FECHA_INICIO);
                sqlCommand.Parameters.AddWithValue("@fecha_fin", item.FECHA_FIN);
                sqlCommand.Parameters.AddWithValue("@tot_cargos", item.TOT_CARGOS);
                sqlCommand.Parameters.AddWithValue("@tot_abonos", item.TOT_ABONOS);
                sqlCommand.Parameters.AddWithValue("@saldo", item.SALDO);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(CTECONC item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE cteconc 
                    			SET num_cta = @num_cta, fecha_reg = @fecha_reg, fecha_inicio = @fecha_inicio, fecha_fin = @fecha_fin, tot_cargos = @tot_cargos, tot_abonos = @tot_abonos, saldo = @saldo 
                    			WHERE num_reg = @num_reg;"
                };
                sqlCommand.Parameters.AddWithValue("@num_reg", item.NUM_REG);
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@fecha_reg", item.FECHA_REG);
                sqlCommand.Parameters.AddWithValue("@fecha_inicio", item.FECHA_INICIO);
                sqlCommand.Parameters.AddWithValue("@fecha_fin", item.FECHA_FIN);
                sqlCommand.Parameters.AddWithValue("@tot_cargos", item.TOT_CARGOS);
                sqlCommand.Parameters.AddWithValue("@tot_abonos", item.TOT_ABONOS);
                sqlCommand.Parameters.AddWithValue("@saldo", item.SALDO);
                return this.ExecuteScalar(sqlCommand);
            }

            public CTECONC GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM cteconc WHERE num_reg = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CTECONC>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<CTECONC> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM cteconc")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CTECONC>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }