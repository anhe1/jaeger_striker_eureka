    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlSemaforoRepository : IGenericRepository<SEMAFORO> {}

        public class SqlFbSemaforoRepository : MasterRepository, ISqlSemaforoRepository {

    	    public SqlFbSemaforoRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(SEMAFORO item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO semaforo (histance) 
                    			VALUES (@histance)"
                };
                sqlCommand.Parameters.AddWithValue("@histance", item.HISTANCE);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(SEMAFORO item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE semaforo 
                    			SET histance = @histance 
                    			WHERE histance = @histance;"
                };
                sqlCommand.Parameters.AddWithValue("@histance", item.HISTANCE);
                return this.ExecuteScalar(sqlCommand);
            }

            public SEMAFORO GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM semaforo WHERE histance = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<SEMAFORO>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<SEMAFORO> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM semaforo")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<SEMAFORO>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }