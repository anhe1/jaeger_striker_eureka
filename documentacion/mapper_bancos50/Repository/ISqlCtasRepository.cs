    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlCtasRepository : IGenericRepository<CTAS> {}

        public class SqlFbCtasRepository : MasterRepository, ISqlCtasRepository {

    	    public SqlFbCtasRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(CTAS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO ctas (num_reg, num_cta, status, banco, sucursal, moneda, cta_contab, dia_corte, sig_cheque, fech_aper, che_fto, bmp_ban, funcionario, telefono, clabe, plaza, cve_banco, nombre_cta, saldo_ini, esbancoext, rfc) 
                    			VALUES (@num_reg, @num_cta, @status, @banco, @sucursal, @moneda, @cta_contab, @dia_corte, @sig_cheque, @fech_aper, @che_fto, @bmp_ban, @funcionario, @telefono, @clabe, @plaza, @cve_banco, @nombre_cta, @saldo_ini, @esbancoext, @rfc)"
                };
                item.NUM_REG = this.Max("NUM_REG");
                sqlCommand.Parameters.AddWithValue("@num_reg", item.NUM_REG);
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@status", item.STATUS);
                sqlCommand.Parameters.AddWithValue("@banco", item.BANCO);
                sqlCommand.Parameters.AddWithValue("@sucursal", item.SUCURSAL);
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                sqlCommand.Parameters.AddWithValue("@cta_contab", item.CTA_CONTAB);
                sqlCommand.Parameters.AddWithValue("@dia_corte", item.DIA_CORTE);
                sqlCommand.Parameters.AddWithValue("@sig_cheque", item.SIG_CHEQUE);
                sqlCommand.Parameters.AddWithValue("@fech_aper", item.FECH_APER);
                sqlCommand.Parameters.AddWithValue("@che_fto", item.CHE_FTO);
                sqlCommand.Parameters.AddWithValue("@bmp_ban", item.BMP_BAN);
                sqlCommand.Parameters.AddWithValue("@funcionario", item.FUNCIONARIO);
                sqlCommand.Parameters.AddWithValue("@telefono", item.TELEFONO);
                sqlCommand.Parameters.AddWithValue("@clabe", item.CLABE);
                sqlCommand.Parameters.AddWithValue("@plaza", item.PLAZA);
                sqlCommand.Parameters.AddWithValue("@cve_banco", item.CVE_BANCO);
                sqlCommand.Parameters.AddWithValue("@nombre_cta", item.NOMBRE_CTA);
                sqlCommand.Parameters.AddWithValue("@saldo_ini", item.SALDO_INI);
                sqlCommand.Parameters.AddWithValue("@esbancoext", item.ESBANCOEXT);
                sqlCommand.Parameters.AddWithValue("@rfc", item.RFC);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(CTAS item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE ctas 
                    			SET num_cta = @num_cta, status = @status, banco = @banco, sucursal = @sucursal, moneda = @moneda, cta_contab = @cta_contab, dia_corte = @dia_corte, sig_cheque = @sig_cheque, fech_aper = @fech_aper, che_fto = @che_fto, bmp_ban = @bmp_ban, funcionario = @funcionario, telefono = @telefono, clabe = @clabe, plaza = @plaza, cve_banco = @cve_banco, nombre_cta = @nombre_cta, saldo_ini = @saldo_ini, esbancoext = @esbancoext, rfc = @rfc 
                    			WHERE num_reg = @num_reg;"
                };
                sqlCommand.Parameters.AddWithValue("@num_reg", item.NUM_REG);
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@status", item.STATUS);
                sqlCommand.Parameters.AddWithValue("@banco", item.BANCO);
                sqlCommand.Parameters.AddWithValue("@sucursal", item.SUCURSAL);
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                sqlCommand.Parameters.AddWithValue("@cta_contab", item.CTA_CONTAB);
                sqlCommand.Parameters.AddWithValue("@dia_corte", item.DIA_CORTE);
                sqlCommand.Parameters.AddWithValue("@sig_cheque", item.SIG_CHEQUE);
                sqlCommand.Parameters.AddWithValue("@fech_aper", item.FECH_APER);
                sqlCommand.Parameters.AddWithValue("@che_fto", item.CHE_FTO);
                sqlCommand.Parameters.AddWithValue("@bmp_ban", item.BMP_BAN);
                sqlCommand.Parameters.AddWithValue("@funcionario", item.FUNCIONARIO);
                sqlCommand.Parameters.AddWithValue("@telefono", item.TELEFONO);
                sqlCommand.Parameters.AddWithValue("@clabe", item.CLABE);
                sqlCommand.Parameters.AddWithValue("@plaza", item.PLAZA);
                sqlCommand.Parameters.AddWithValue("@cve_banco", item.CVE_BANCO);
                sqlCommand.Parameters.AddWithValue("@nombre_cta", item.NOMBRE_CTA);
                sqlCommand.Parameters.AddWithValue("@saldo_ini", item.SALDO_INI);
                sqlCommand.Parameters.AddWithValue("@esbancoext", item.ESBANCOEXT);
                sqlCommand.Parameters.AddWithValue("@rfc", item.RFC);
                return this.ExecuteScalar(sqlCommand);
            }

            public CTAS GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM ctas WHERE num_reg = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CTAS>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<CTAS> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM ctas")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CTAS>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }