    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlMovaux2Repository : IGenericRepository<MOVAUX2> {}

        public class SqlFbMovaux2Repository : MasterRepository, ISqlMovaux2Repository {

    	    public SqlFbMovaux2Repository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(MOVAUX2 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO movaux2 (num_cta, num_mov_aux, num_reg_mov) 
                    			VALUES (@num_cta, @num_mov_aux, @num_reg_mov)"
                };
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@num_mov_aux", item.NUM_MOV_AUX);
                sqlCommand.Parameters.AddWithValue("@num_reg_mov", item.NUM_REG_MOV);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(MOVAUX2 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE movaux2 
                    			SET num_mov_aux = @num_mov_aux, num_reg_mov = @num_reg_mov 
                    			WHERE num_cta = @num_cta;"
                };
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@num_mov_aux", item.NUM_MOV_AUX);
                sqlCommand.Parameters.AddWithValue("@num_reg_mov", item.NUM_REG_MOV);
                return this.ExecuteScalar(sqlCommand);
            }

            public MOVAUX2 GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM movaux2 WHERE field = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<MOVAUX2>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<MOVAUX2> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM movaux2")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<MOVAUX2>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }