    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlFormpagoRepository : IGenericRepository<FORMPAGO> {}

        public class SqlFbFormpagoRepository : MasterRepository, ISqlFormpagoRepository {

    	    public SqlFbFormpagoRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(FORMPAGO item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO formpago (num_reg, descripcion, tipo, formapagosat) 
                    			VALUES (@num_reg, @descripcion, @tipo, @formapagosat)"
                };
                item.NUM_REG = this.Max("NUM_REG");
                sqlCommand.Parameters.AddWithValue("@num_reg", item.NUM_REG);
                sqlCommand.Parameters.AddWithValue("@descripcion", item.DESCRIPCION);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@formapagosat", item.FORMAPAGOSAT);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(FORMPAGO item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE formpago 
                    			SET descripcion = @descripcion, tipo = @tipo, formapagosat = @formapagosat 
                    			WHERE num_reg = @num_reg;"
                };
                sqlCommand.Parameters.AddWithValue("@num_reg", item.NUM_REG);
                sqlCommand.Parameters.AddWithValue("@descripcion", item.DESCRIPCION);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@formapagosat", item.FORMAPAGOSAT);
                return this.ExecuteScalar(sqlCommand);
            }

            public FORMPAGO GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM formpago WHERE num_reg = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<FORMPAGO>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<FORMPAGO> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM formpago")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<FORMPAGO>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }