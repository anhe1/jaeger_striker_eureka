    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlHistorRepository : IGenericRepository<HISTOR> {}

        public class SqlFbHistorRepository : MasterRepository, ISqlHistorRepository {

    	    public SqlFbHistorRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(HISTOR item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO histor (moneda, fecha, tipocambio) 
                    			VALUES (@moneda, @fecha, @tipocambio)"
                };
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@tipocambio", item.TIPOCAMBIO);
            }

            public int Update(HISTOR item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE histor 
                    			SET fecha = @fecha, tipocambio = @tipocambio 
                    			WHERE moneda = @moneda;"
                };
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@tipocambio", item.TIPOCAMBIO);
                return this.ExecuteScalar(sqlCommand);
            }

            public HISTOR GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM histor WHERE moneda = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<HISTOR>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<HISTOR> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM histor")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<HISTOR>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }