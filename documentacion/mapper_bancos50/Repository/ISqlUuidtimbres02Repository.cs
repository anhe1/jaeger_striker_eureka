    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlUuidtimbres02Repository : IGenericRepository<UUIDTIMBRES02> {}

        public class SqlFbUuidtimbres02Repository : MasterRepository, ISqlUuidtimbres02Repository {

    	    public SqlFbUuidtimbres02Repository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(UUIDTIMBRES02 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO uuidtimbres02 (num_regu, num_regp, uuidtimbre, monto, serie, folio, rfcemisor, rfcreceptor, orden, fecha, tipocomprobante, monedadr, tipocambiodr, metododepagodr, numparcialidad, imppagado, impsaldoinsoluto) 
                    			VALUES (@num_regu, @num_regp, @uuidtimbre, @monto, @serie, @folio, @rfcemisor, @rfcreceptor, @orden, @fecha, @tipocomprobante, @monedadr, @tipocambiodr, @metododepagodr, @numparcialidad, @imppagado, @impsaldoinsoluto)"
                };
                item.NUM_REGU = this.Max("NUM_REGU");
                sqlCommand.Parameters.AddWithValue("@num_regu", item.NUM_REGU);
                sqlCommand.Parameters.AddWithValue("@num_regp", item.NUM_REGP);
                sqlCommand.Parameters.AddWithValue("@uuidtimbre", item.UUIDTIMBRE);
                sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
                sqlCommand.Parameters.AddWithValue("@serie", item.SERIE);
                sqlCommand.Parameters.AddWithValue("@folio", item.FOLIO);
                sqlCommand.Parameters.AddWithValue("@rfcemisor", item.RFCEMISOR);
                sqlCommand.Parameters.AddWithValue("@rfcreceptor", item.RFCRECEPTOR);
                sqlCommand.Parameters.AddWithValue("@orden", item.ORDEN);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@tipocomprobante", item.TIPOCOMPROBANTE);
                sqlCommand.Parameters.AddWithValue("@monedadr", item.MONEDADR);
                sqlCommand.Parameters.AddWithValue("@tipocambiodr", item.TIPOCAMBIODR);
                sqlCommand.Parameters.AddWithValue("@metododepagodr", item.METODODEPAGODR);
                sqlCommand.Parameters.AddWithValue("@numparcialidad", item.NUMPARCIALIDAD);
                sqlCommand.Parameters.AddWithValue("@imppagado", item.IMPPAGADO);
                sqlCommand.Parameters.AddWithValue("@impsaldoinsoluto", item.IMPSALDOINSOLUTO);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(UUIDTIMBRES02 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE uuidtimbres02 
                    			SET num_regp = @num_regp, uuidtimbre = @uuidtimbre, monto = @monto, serie = @serie, folio = @folio, rfcemisor = @rfcemisor, rfcreceptor = @rfcreceptor, orden = @orden, fecha = @fecha, tipocomprobante = @tipocomprobante, monedadr = @monedadr, tipocambiodr = @tipocambiodr, metododepagodr = @metododepagodr, numparcialidad = @numparcialidad, imppagado = @imppagado, impsaldoinsoluto = @impsaldoinsoluto 
                    			WHERE num_regu = @num_regu;"
                };
                sqlCommand.Parameters.AddWithValue("@num_regu", item.NUM_REGU);
                sqlCommand.Parameters.AddWithValue("@num_regp", item.NUM_REGP);
                sqlCommand.Parameters.AddWithValue("@uuidtimbre", item.UUIDTIMBRE);
                sqlCommand.Parameters.AddWithValue("@monto", item.MONTO);
                sqlCommand.Parameters.AddWithValue("@serie", item.SERIE);
                sqlCommand.Parameters.AddWithValue("@folio", item.FOLIO);
                sqlCommand.Parameters.AddWithValue("@rfcemisor", item.RFCEMISOR);
                sqlCommand.Parameters.AddWithValue("@rfcreceptor", item.RFCRECEPTOR);
                sqlCommand.Parameters.AddWithValue("@orden", item.ORDEN);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@tipocomprobante", item.TIPOCOMPROBANTE);
                sqlCommand.Parameters.AddWithValue("@monedadr", item.MONEDADR);
                sqlCommand.Parameters.AddWithValue("@tipocambiodr", item.TIPOCAMBIODR);
                sqlCommand.Parameters.AddWithValue("@metododepagodr", item.METODODEPAGODR);
                sqlCommand.Parameters.AddWithValue("@numparcialidad", item.NUMPARCIALIDAD);
                sqlCommand.Parameters.AddWithValue("@imppagado", item.IMPPAGADO);
                sqlCommand.Parameters.AddWithValue("@impsaldoinsoluto", item.IMPSALDOINSOLUTO);
                return this.ExecuteScalar(sqlCommand);
            }

            public UUIDTIMBRES02 GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM uuidtimbres02 WHERE num_regu = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<UUIDTIMBRES02>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<UUIDTIMBRES02> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM uuidtimbres02")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<UUIDTIMBRES02>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }