    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlControlregmovRepository : IGenericRepository<CONTROLREGMOV> {}

        public class SqlFbControlregmovRepository : MasterRepository, ISqlControlregmovRepository {

    	    public SqlFbControlregmovRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(CONTROLREGMOV item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO controlregmov (cuenta, movimientos, partidas, uuids) 
                    			VALUES (@cuenta, @movimientos, @partidas, @uuids)"
                };
                sqlCommand.Parameters.AddWithValue("@cuenta", item.CUENTA);
                sqlCommand.Parameters.AddWithValue("@movimientos", item.MOVIMIENTOS);
                sqlCommand.Parameters.AddWithValue("@partidas", item.PARTIDAS);
                sqlCommand.Parameters.AddWithValue("@uuids", item.UUIDS);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(CONTROLREGMOV item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE controlregmov 
                    			SET movimientos = @movimientos, partidas = @partidas, uuids = @uuids 
                    			WHERE cuenta = @cuenta;"
                };
                sqlCommand.Parameters.AddWithValue("@cuenta", item.CUENTA);
                sqlCommand.Parameters.AddWithValue("@movimientos", item.MOVIMIENTOS);
                sqlCommand.Parameters.AddWithValue("@partidas", item.PARTIDAS);
                sqlCommand.Parameters.AddWithValue("@uuids", item.UUIDS);
                return this.ExecuteScalar(sqlCommand);
            }

            public CONTROLREGMOV GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM controlregmov WHERE cuenta = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CONTROLREGMOV>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<CONTROLREGMOV> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM controlregmov")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CONTROLREGMOV>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }