    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlMovauxRepository : IGenericRepository<MOVAUX> {}

        public class SqlFbMovauxRepository : MasterRepository, ISqlMovauxRepository {

    	    public SqlFbMovauxRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(MOVAUX item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO movaux (num_reg, num_cta, referencia1, referencia2, fecha, abono, cargo, rfc, revisado) 
                    			VALUES (@num_reg, @num_cta, @referencia1, @referencia2, @fecha, @abono, @cargo, @rfc, @revisado)"
                };
                sqlCommand.Parameters.AddWithValue("@num_reg", item.NUM_REG);
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@referencia1", item.REFERENCIA1);
                sqlCommand.Parameters.AddWithValue("@referencia2", item.REFERENCIA2);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@abono", item.ABONO);
                sqlCommand.Parameters.AddWithValue("@cargo", item.CARGO);
                sqlCommand.Parameters.AddWithValue("@rfc", item.RFC);
                sqlCommand.Parameters.AddWithValue("@revisado", item.REVISADO);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(MOVAUX item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE movaux 
                    			SET num_cta = @num_cta, referencia1 = @referencia1, referencia2 = @referencia2, fecha = @fecha, abono = @abono, cargo = @cargo, rfc = @rfc, revisado = @revisado 
                    			WHERE num_reg = @num_reg;"
                };
                sqlCommand.Parameters.AddWithValue("@num_reg", item.NUM_REG);
                sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
                sqlCommand.Parameters.AddWithValue("@referencia1", item.REFERENCIA1);
                sqlCommand.Parameters.AddWithValue("@referencia2", item.REFERENCIA2);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@abono", item.ABONO);
                sqlCommand.Parameters.AddWithValue("@cargo", item.CARGO);
                sqlCommand.Parameters.AddWithValue("@rfc", item.RFC);
                sqlCommand.Parameters.AddWithValue("@revisado", item.REVISADO);
                return this.ExecuteScalar(sqlCommand);
            }

            public MOVAUX GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM movaux WHERE num_reg = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<MOVAUX>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<MOVAUX> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM movaux")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<MOVAUX>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }