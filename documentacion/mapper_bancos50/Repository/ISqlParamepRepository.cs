    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlParamempRepository : IGenericRepository<PARAMEMP> {}

        public class SqlFbParamempRepository : MasterRepository, ISqlParamempRepository {

    	    public SqlFbParamempRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(PARAMEMP item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO paramemp (idemp, nombre, cvl_alta, cod_pos, passw, fvigencia, direc, pobl, rfc_emp, ban_inst, emp_coi, coi_linea, alt_coi, num_dec, int_sae, int_coi, sae_cxp, sae_cxc, emp_sae, abo_cxp, ant_cxp, abo_cxc, ant_cxc, act_trans, cpf_cxc, mon_base, lim_sgiro, pol_fto, mov_fto, cta_impsto, subdir, impuesto, win_cascada, show_monedas, tc_cxp, tc_cxc, saeesmultimon, desg_ctascoi, caprfc_iva, coi_solo_cheq, s_emp_coi, s_dat_coi, nver_coi, s_emp_sae, s_dat_sae, nver_sae, ced_emp, cta_iva_pagado, cta_iva_cobrado, cta_iva_x_pagar, cta_iva_x_cobrar, reclasificar_iva, ruta_ins_sae, ruta_ins_coi, empresa_sae, empresa_coi, ver_sae, ver_coi, dirtrab, part_cont_elec, uuidclpv, uuidiva, uuidbanco, uuidtodas, recpagolinea, pass_correo, servidor_correo, puerto, usuario_correo, requiere_aut, conexion_segura, proveedor, mostrartablero) 
                    			VALUES (@idemp, @nombre, @cvl_alta, @cod_pos, @passw, @fvigencia, @direc, @pobl, @rfc_emp, @ban_inst, @emp_coi, @coi_linea, @alt_coi, @num_dec, @int_sae, @int_coi, @sae_cxp, @sae_cxc, @emp_sae, @abo_cxp, @ant_cxp, @abo_cxc, @ant_cxc, @act_trans, @cpf_cxc, @mon_base, @lim_sgiro, @pol_fto, @mov_fto, @cta_impsto, @subdir, @impuesto, @win_cascada, @show_monedas, @tc_cxp, @tc_cxc, @saeesmultimon, @desg_ctascoi, @caprfc_iva, @coi_solo_cheq, @s_emp_coi, @s_dat_coi, @nver_coi, @s_emp_sae, @s_dat_sae, @nver_sae, @ced_emp, @cta_iva_pagado, @cta_iva_cobrado, @cta_iva_x_pagar, @cta_iva_x_cobrar, @reclasificar_iva, @ruta_ins_sae, @ruta_ins_coi, @empresa_sae, @empresa_coi, @ver_sae, @ver_coi, @dirtrab, @part_cont_elec, @uuidclpv, @uuidiva, @uuidbanco, @uuidtodas, @recpagolinea, @pass_correo, @servidor_correo, @puerto, @usuario_correo, @requiere_aut, @conexion_segura, @proveedor, @mostrartablero)"
                };
                sqlCommand.Parameters.AddWithValue("@idemp", item.IDEMP);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@cvl_alta", item.CVL_ALTA);
                sqlCommand.Parameters.AddWithValue("@cod_pos", item.COD_POS);
                sqlCommand.Parameters.AddWithValue("@passw", item.PASSW);
                sqlCommand.Parameters.AddWithValue("@fvigencia", item.FVIGENCIA);
                sqlCommand.Parameters.AddWithValue("@direc", item.DIREC);
                sqlCommand.Parameters.AddWithValue("@pobl", item.POBL);
                sqlCommand.Parameters.AddWithValue("@rfc_emp", item.RFC_EMP);
                sqlCommand.Parameters.AddWithValue("@ban_inst", item.BAN_INST);
                sqlCommand.Parameters.AddWithValue("@emp_coi", item.EMP_COI);
                sqlCommand.Parameters.AddWithValue("@coi_linea", item.COI_LINEA);
                sqlCommand.Parameters.AddWithValue("@alt_coi", item.ALT_COI);
                sqlCommand.Parameters.AddWithValue("@num_dec", item.NUM_DEC);
                sqlCommand.Parameters.AddWithValue("@int_sae", item.INT_SAE);
                sqlCommand.Parameters.AddWithValue("@int_coi", item.INT_COI);
                sqlCommand.Parameters.AddWithValue("@sae_cxp", item.SAE_CXP);
                sqlCommand.Parameters.AddWithValue("@sae_cxc", item.SAE_CXC);
                sqlCommand.Parameters.AddWithValue("@emp_sae", item.EMP_SAE);
                sqlCommand.Parameters.AddWithValue("@abo_cxp", item.ABO_CXP);
                sqlCommand.Parameters.AddWithValue("@ant_cxp", item.ANT_CXP);
                sqlCommand.Parameters.AddWithValue("@abo_cxc", item.ABO_CXC);
                sqlCommand.Parameters.AddWithValue("@ant_cxc", item.ANT_CXC);
                sqlCommand.Parameters.AddWithValue("@act_trans", item.ACT_TRANS);
                sqlCommand.Parameters.AddWithValue("@cpf_cxc", item.CPF_CXC);
                sqlCommand.Parameters.AddWithValue("@mon_base", item.MON_BASE);
                sqlCommand.Parameters.AddWithValue("@lim_sgiro", item.LIM_SGIRO);
                sqlCommand.Parameters.AddWithValue("@pol_fto", item.POL_FTO);
                sqlCommand.Parameters.AddWithValue("@mov_fto", item.MOV_FTO);
                sqlCommand.Parameters.AddWithValue("@cta_impsto", item.CTA_IMPSTO);
                sqlCommand.Parameters.AddWithValue("@subdir", item.SUBDIR);
                sqlCommand.Parameters.AddWithValue("@impuesto", item.IMPUESTO);
                sqlCommand.Parameters.AddWithValue("@win_cascada", item.WIN_CASCADA);
                sqlCommand.Parameters.AddWithValue("@show_monedas", item.SHOW_MONEDAS);
                sqlCommand.Parameters.AddWithValue("@tc_cxp", item.TC_CXP);
                sqlCommand.Parameters.AddWithValue("@tc_cxc", item.TC_CXC);
                sqlCommand.Parameters.AddWithValue("@saeesmultimon", item.SAEESMULTIMON);
                sqlCommand.Parameters.AddWithValue("@desg_ctascoi", item.DESG_CTASCOI);
                sqlCommand.Parameters.AddWithValue("@caprfc_iva", item.CAPRFC_IVA);
                sqlCommand.Parameters.AddWithValue("@coi_solo_cheq", item.COI_SOLO_CHEQ);
                sqlCommand.Parameters.AddWithValue("@s_emp_coi", item.S_EMP_COI);
                sqlCommand.Parameters.AddWithValue("@s_dat_coi", item.S_DAT_COI);
                sqlCommand.Parameters.AddWithValue("@nver_coi", item.NVER_COI);
                sqlCommand.Parameters.AddWithValue("@s_emp_sae", item.S_EMP_SAE);
                sqlCommand.Parameters.AddWithValue("@s_dat_sae", item.S_DAT_SAE);
                sqlCommand.Parameters.AddWithValue("@nver_sae", item.NVER_SAE);
                sqlCommand.Parameters.AddWithValue("@ced_emp", item.CED_EMP);
                sqlCommand.Parameters.AddWithValue("@cta_iva_pagado", item.CTA_IVA_PAGADO);
                sqlCommand.Parameters.AddWithValue("@cta_iva_cobrado", item.CTA_IVA_COBRADO);
                sqlCommand.Parameters.AddWithValue("@cta_iva_x_pagar", item.CTA_IVA_X_PAGAR);
                sqlCommand.Parameters.AddWithValue("@cta_iva_x_cobrar", item.CTA_IVA_X_COBRAR);
                sqlCommand.Parameters.AddWithValue("@reclasificar_iva", item.RECLASIFICAR_IVA);
                sqlCommand.Parameters.AddWithValue("@ruta_ins_sae", item.RUTA_INS_SAE);
                sqlCommand.Parameters.AddWithValue("@ruta_ins_coi", item.RUTA_INS_COI);
                sqlCommand.Parameters.AddWithValue("@empresa_sae", item.EMPRESA_SAE);
                sqlCommand.Parameters.AddWithValue("@empresa_coi", item.EMPRESA_COI);
                sqlCommand.Parameters.AddWithValue("@ver_sae", item.VER_SAE);
                sqlCommand.Parameters.AddWithValue("@ver_coi", item.VER_COI);
                sqlCommand.Parameters.AddWithValue("@dirtrab", item.DIRTRAB);
                sqlCommand.Parameters.AddWithValue("@part_cont_elec", item.PART_CONT_ELEC);
                sqlCommand.Parameters.AddWithValue("@uuidclpv", item.UUIDCLPV);
                sqlCommand.Parameters.AddWithValue("@uuidiva", item.UUIDIVA);
                sqlCommand.Parameters.AddWithValue("@uuidbanco", item.UUIDBANCO);
                sqlCommand.Parameters.AddWithValue("@uuidtodas", item.UUIDTODAS);
                sqlCommand.Parameters.AddWithValue("@recpagolinea", item.RECPAGOLINEA);
                sqlCommand.Parameters.AddWithValue("@pass_correo", item.PASS_CORREO);
                sqlCommand.Parameters.AddWithValue("@servidor_correo", item.SERVIDOR_CORREO);
                sqlCommand.Parameters.AddWithValue("@puerto", item.PUERTO);
                sqlCommand.Parameters.AddWithValue("@usuario_correo", item.USUARIO_CORREO);
                sqlCommand.Parameters.AddWithValue("@requiere_aut", item.REQUIERE_AUT);
                sqlCommand.Parameters.AddWithValue("@conexion_segura", item.CONEXION_SEGURA);
                sqlCommand.Parameters.AddWithValue("@proveedor", item.PROVEEDOR);
                sqlCommand.Parameters.AddWithValue("@mostrartablero", item.MOSTRARTABLERO);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(PARAMEMP item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE paramemp 
                    			SET nombre = @nombre, cvl_alta = @cvl_alta, cod_pos = @cod_pos, passw = @passw, fvigencia = @fvigencia, direc = @direc, pobl = @pobl, rfc_emp = @rfc_emp, ban_inst = @ban_inst, emp_coi = @emp_coi, coi_linea = @coi_linea, alt_coi = @alt_coi, num_dec = @num_dec, int_sae = @int_sae, int_coi = @int_coi, sae_cxp = @sae_cxp, sae_cxc = @sae_cxc, emp_sae = @emp_sae, abo_cxp = @abo_cxp, ant_cxp = @ant_cxp, abo_cxc = @abo_cxc, ant_cxc = @ant_cxc, act_trans = @act_trans, cpf_cxc = @cpf_cxc, mon_base = @mon_base, lim_sgiro = @lim_sgiro, pol_fto = @pol_fto, mov_fto = @mov_fto, cta_impsto = @cta_impsto, subdir = @subdir, impuesto = @impuesto, win_cascada = @win_cascada, show_monedas = @show_monedas, tc_cxp = @tc_cxp, tc_cxc = @tc_cxc, saeesmultimon = @saeesmultimon, desg_ctascoi = @desg_ctascoi, caprfc_iva = @caprfc_iva, coi_solo_cheq = @coi_solo_cheq, s_emp_coi = @s_emp_coi, s_dat_coi = @s_dat_coi, nver_coi = @nver_coi, s_emp_sae = @s_emp_sae, s_dat_sae = @s_dat_sae, nver_sae = @nver_sae, ced_emp = @ced_emp, cta_iva_pagado = @cta_iva_pagado, cta_iva_cobrado = @cta_iva_cobrado, cta_iva_x_pagar = @cta_iva_x_pagar, cta_iva_x_cobrar = @cta_iva_x_cobrar, reclasificar_iva = @reclasificar_iva, ruta_ins_sae = @ruta_ins_sae, ruta_ins_coi = @ruta_ins_coi, empresa_sae = @empresa_sae, empresa_coi = @empresa_coi, ver_sae = @ver_sae, ver_coi = @ver_coi, dirtrab = @dirtrab, part_cont_elec = @part_cont_elec, uuidclpv = @uuidclpv, uuidiva = @uuidiva, uuidbanco = @uuidbanco, uuidtodas = @uuidtodas, recpagolinea = @recpagolinea, pass_correo = @pass_correo, servidor_correo = @servidor_correo, puerto = @puerto, usuario_correo = @usuario_correo, requiere_aut = @requiere_aut, conexion_segura = @conexion_segura, proveedor = @proveedor, mostrartablero = @mostrartablero 
                    			WHERE idemp = @idemp;"
                };
                sqlCommand.Parameters.AddWithValue("@idemp", item.IDEMP);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@cvl_alta", item.CVL_ALTA);
                sqlCommand.Parameters.AddWithValue("@cod_pos", item.COD_POS);
                sqlCommand.Parameters.AddWithValue("@passw", item.PASSW);
                sqlCommand.Parameters.AddWithValue("@fvigencia", item.FVIGENCIA);
                sqlCommand.Parameters.AddWithValue("@direc", item.DIREC);
                sqlCommand.Parameters.AddWithValue("@pobl", item.POBL);
                sqlCommand.Parameters.AddWithValue("@rfc_emp", item.RFC_EMP);
                sqlCommand.Parameters.AddWithValue("@ban_inst", item.BAN_INST);
                sqlCommand.Parameters.AddWithValue("@emp_coi", item.EMP_COI);
                sqlCommand.Parameters.AddWithValue("@coi_linea", item.COI_LINEA);
                sqlCommand.Parameters.AddWithValue("@alt_coi", item.ALT_COI);
                sqlCommand.Parameters.AddWithValue("@num_dec", item.NUM_DEC);
                sqlCommand.Parameters.AddWithValue("@int_sae", item.INT_SAE);
                sqlCommand.Parameters.AddWithValue("@int_coi", item.INT_COI);
                sqlCommand.Parameters.AddWithValue("@sae_cxp", item.SAE_CXP);
                sqlCommand.Parameters.AddWithValue("@sae_cxc", item.SAE_CXC);
                sqlCommand.Parameters.AddWithValue("@emp_sae", item.EMP_SAE);
                sqlCommand.Parameters.AddWithValue("@abo_cxp", item.ABO_CXP);
                sqlCommand.Parameters.AddWithValue("@ant_cxp", item.ANT_CXP);
                sqlCommand.Parameters.AddWithValue("@abo_cxc", item.ABO_CXC);
                sqlCommand.Parameters.AddWithValue("@ant_cxc", item.ANT_CXC);
                sqlCommand.Parameters.AddWithValue("@act_trans", item.ACT_TRANS);
                sqlCommand.Parameters.AddWithValue("@cpf_cxc", item.CPF_CXC);
                sqlCommand.Parameters.AddWithValue("@mon_base", item.MON_BASE);
                sqlCommand.Parameters.AddWithValue("@lim_sgiro", item.LIM_SGIRO);
                sqlCommand.Parameters.AddWithValue("@pol_fto", item.POL_FTO);
                sqlCommand.Parameters.AddWithValue("@mov_fto", item.MOV_FTO);
                sqlCommand.Parameters.AddWithValue("@cta_impsto", item.CTA_IMPSTO);
                sqlCommand.Parameters.AddWithValue("@subdir", item.SUBDIR);
                sqlCommand.Parameters.AddWithValue("@impuesto", item.IMPUESTO);
                sqlCommand.Parameters.AddWithValue("@win_cascada", item.WIN_CASCADA);
                sqlCommand.Parameters.AddWithValue("@show_monedas", item.SHOW_MONEDAS);
                sqlCommand.Parameters.AddWithValue("@tc_cxp", item.TC_CXP);
                sqlCommand.Parameters.AddWithValue("@tc_cxc", item.TC_CXC);
                sqlCommand.Parameters.AddWithValue("@saeesmultimon", item.SAEESMULTIMON);
                sqlCommand.Parameters.AddWithValue("@desg_ctascoi", item.DESG_CTASCOI);
                sqlCommand.Parameters.AddWithValue("@caprfc_iva", item.CAPRFC_IVA);
                sqlCommand.Parameters.AddWithValue("@coi_solo_cheq", item.COI_SOLO_CHEQ);
                sqlCommand.Parameters.AddWithValue("@s_emp_coi", item.S_EMP_COI);
                sqlCommand.Parameters.AddWithValue("@s_dat_coi", item.S_DAT_COI);
                sqlCommand.Parameters.AddWithValue("@nver_coi", item.NVER_COI);
                sqlCommand.Parameters.AddWithValue("@s_emp_sae", item.S_EMP_SAE);
                sqlCommand.Parameters.AddWithValue("@s_dat_sae", item.S_DAT_SAE);
                sqlCommand.Parameters.AddWithValue("@nver_sae", item.NVER_SAE);
                sqlCommand.Parameters.AddWithValue("@ced_emp", item.CED_EMP);
                sqlCommand.Parameters.AddWithValue("@cta_iva_pagado", item.CTA_IVA_PAGADO);
                sqlCommand.Parameters.AddWithValue("@cta_iva_cobrado", item.CTA_IVA_COBRADO);
                sqlCommand.Parameters.AddWithValue("@cta_iva_x_pagar", item.CTA_IVA_X_PAGAR);
                sqlCommand.Parameters.AddWithValue("@cta_iva_x_cobrar", item.CTA_IVA_X_COBRAR);
                sqlCommand.Parameters.AddWithValue("@reclasificar_iva", item.RECLASIFICAR_IVA);
                sqlCommand.Parameters.AddWithValue("@ruta_ins_sae", item.RUTA_INS_SAE);
                sqlCommand.Parameters.AddWithValue("@ruta_ins_coi", item.RUTA_INS_COI);
                sqlCommand.Parameters.AddWithValue("@empresa_sae", item.EMPRESA_SAE);
                sqlCommand.Parameters.AddWithValue("@empresa_coi", item.EMPRESA_COI);
                sqlCommand.Parameters.AddWithValue("@ver_sae", item.VER_SAE);
                sqlCommand.Parameters.AddWithValue("@ver_coi", item.VER_COI);
                sqlCommand.Parameters.AddWithValue("@dirtrab", item.DIRTRAB);
                sqlCommand.Parameters.AddWithValue("@part_cont_elec", item.PART_CONT_ELEC);
                sqlCommand.Parameters.AddWithValue("@uuidclpv", item.UUIDCLPV);
                sqlCommand.Parameters.AddWithValue("@uuidiva", item.UUIDIVA);
                sqlCommand.Parameters.AddWithValue("@uuidbanco", item.UUIDBANCO);
                sqlCommand.Parameters.AddWithValue("@uuidtodas", item.UUIDTODAS);
                sqlCommand.Parameters.AddWithValue("@recpagolinea", item.RECPAGOLINEA);
                sqlCommand.Parameters.AddWithValue("@pass_correo", item.PASS_CORREO);
                sqlCommand.Parameters.AddWithValue("@servidor_correo", item.SERVIDOR_CORREO);
                sqlCommand.Parameters.AddWithValue("@puerto", item.PUERTO);
                sqlCommand.Parameters.AddWithValue("@usuario_correo", item.USUARIO_CORREO);
                sqlCommand.Parameters.AddWithValue("@requiere_aut", item.REQUIERE_AUT);
                sqlCommand.Parameters.AddWithValue("@conexion_segura", item.CONEXION_SEGURA);
                sqlCommand.Parameters.AddWithValue("@proveedor", item.PROVEEDOR);
                sqlCommand.Parameters.AddWithValue("@mostrartablero", item.MOSTRARTABLERO);
                return this.ExecuteScalar(sqlCommand);
            }

            public PARAMEMP GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM paramemp WHERE idemp = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<PARAMEMP>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<PARAMEMP> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM paramemp")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<PARAMEMP>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }