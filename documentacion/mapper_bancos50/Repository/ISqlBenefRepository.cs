    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlBenefRepository : IGenericRepository<BENEF> {}

        public class SqlFbBenefRepository : MasterRepository, ISqlBenefRepository {

    	    public SqlFbBenefRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(BENEF item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO benef (num_reg, nombre, rfc, cta_contab, tipo, inf_general, referencia, banco, sucursal, cuenta, clabe, cve_banco, esbancoext, bancodesc, rfcbanco) 
                    			VALUES (@num_reg, @nombre, @rfc, @cta_contab, @tipo, @inf_general, @referencia, @banco, @sucursal, @cuenta, @clabe, @cve_banco, @esbancoext, @bancodesc, @rfcbanco)"
                };
                item.NUM_REG = this.Max("NUM_REG");
                sqlCommand.Parameters.AddWithValue("@num_reg", item.NUM_REG);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@rfc", item.RFC);
                sqlCommand.Parameters.AddWithValue("@cta_contab", item.CTA_CONTAB);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@inf_general", item.INF_GENERAL);
                sqlCommand.Parameters.AddWithValue("@referencia", item.REFERENCIA);
                sqlCommand.Parameters.AddWithValue("@banco", item.BANCO);
                sqlCommand.Parameters.AddWithValue("@sucursal", item.SUCURSAL);
                sqlCommand.Parameters.AddWithValue("@cuenta", item.CUENTA);
                sqlCommand.Parameters.AddWithValue("@clabe", item.CLABE);
                sqlCommand.Parameters.AddWithValue("@cve_banco", item.CVE_BANCO);
                sqlCommand.Parameters.AddWithValue("@esbancoext", item.ESBANCOEXT);
                sqlCommand.Parameters.AddWithValue("@bancodesc", item.BANCODESC);
                sqlCommand.Parameters.AddWithValue("@rfcbanco", item.RFCBANCO);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(BENEF item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE benef 
                    			SET nombre = @nombre, rfc = @rfc, cta_contab = @cta_contab, tipo = @tipo, inf_general = @inf_general, referencia = @referencia, banco = @banco, sucursal = @sucursal, cuenta = @cuenta, clabe = @clabe, cve_banco = @cve_banco, esbancoext = @esbancoext, bancodesc = @bancodesc, rfcbanco = @rfcbanco 
                    			WHERE num_reg = @num_reg;"
                };
                sqlCommand.Parameters.AddWithValue("@num_reg", item.NUM_REG);
                sqlCommand.Parameters.AddWithValue("@nombre", item.NOMBRE);
                sqlCommand.Parameters.AddWithValue("@rfc", item.RFC);
                sqlCommand.Parameters.AddWithValue("@cta_contab", item.CTA_CONTAB);
                sqlCommand.Parameters.AddWithValue("@tipo", item.TIPO);
                sqlCommand.Parameters.AddWithValue("@inf_general", item.INF_GENERAL);
                sqlCommand.Parameters.AddWithValue("@referencia", item.REFERENCIA);
                sqlCommand.Parameters.AddWithValue("@banco", item.BANCO);
                sqlCommand.Parameters.AddWithValue("@sucursal", item.SUCURSAL);
                sqlCommand.Parameters.AddWithValue("@cuenta", item.CUENTA);
                sqlCommand.Parameters.AddWithValue("@clabe", item.CLABE);
                sqlCommand.Parameters.AddWithValue("@cve_banco", item.CVE_BANCO);
                sqlCommand.Parameters.AddWithValue("@esbancoext", item.ESBANCOEXT);
                sqlCommand.Parameters.AddWithValue("@bancodesc", item.BANCODESC);
                sqlCommand.Parameters.AddWithValue("@rfcbanco", item.RFCBANCO);
                return this.ExecuteScalar(sqlCommand);
            }

            public BENEF GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM benef WHERE num_reg = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<BENEF>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<BENEF> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM benef")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<BENEF>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }