    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlControlRepository : IGenericRepository<CONTROL> {}

        public class SqlFbControlRepository : MasterRepository, ISqlControlRepository {

    	    public SqlFbControlRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(CONTROL item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO control (ctmoned, ctctas, ctcomd, ctbeneficiarios, ctformpag, ctcteconc, cttransferencia, ver_basdat, num_emp) 
                    			VALUES (@ctmoned, @ctctas, @ctcomd, @ctbeneficiarios, @ctformpag, @ctcteconc, @cttransferencia, @ver_basdat, @num_emp)"
                };
                sqlCommand.Parameters.AddWithValue("@ctmoned", item.CTMONED);
                sqlCommand.Parameters.AddWithValue("@ctctas", item.CTCTAS);
                sqlCommand.Parameters.AddWithValue("@ctcomd", item.CTCOMD);
                sqlCommand.Parameters.AddWithValue("@ctbeneficiarios", item.CTBENEFICIARIOS);
                sqlCommand.Parameters.AddWithValue("@ctformpag", item.CTFORMPAG);
                sqlCommand.Parameters.AddWithValue("@ctcteconc", item.CTCTECONC);
                sqlCommand.Parameters.AddWithValue("@cttransferencia", item.CTTRANSFERENCIA);
                sqlCommand.Parameters.AddWithValue("@ver_basdat", item.VER_BASDAT);
                sqlCommand.Parameters.AddWithValue("@num_emp", item.NUM_EMP);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(CONTROL item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE control 
                    			SET ctctas = @ctctas, ctcomd = @ctcomd, ctbeneficiarios = @ctbeneficiarios, ctformpag = @ctformpag, ctcteconc = @ctcteconc, cttransferencia = @cttransferencia, ver_basdat = @ver_basdat, num_emp = @num_emp 
                    			WHERE ctmoned = @ctmoned;"
                };
                sqlCommand.Parameters.AddWithValue("@ctmoned", item.CTMONED);
                sqlCommand.Parameters.AddWithValue("@ctctas", item.CTCTAS);
                sqlCommand.Parameters.AddWithValue("@ctcomd", item.CTCOMD);
                sqlCommand.Parameters.AddWithValue("@ctbeneficiarios", item.CTBENEFICIARIOS);
                sqlCommand.Parameters.AddWithValue("@ctformpag", item.CTFORMPAG);
                sqlCommand.Parameters.AddWithValue("@ctcteconc", item.CTCTECONC);
                sqlCommand.Parameters.AddWithValue("@cttransferencia", item.CTTRANSFERENCIA);
                sqlCommand.Parameters.AddWithValue("@ver_basdat", item.VER_BASDAT);
                sqlCommand.Parameters.AddWithValue("@num_emp", item.NUM_EMP);
                return this.ExecuteScalar(sqlCommand);
            }

            public CONTROL GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM control WHERE ctmoned = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CONTROL>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<CONTROL> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM control")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<CONTROL>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }