    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlMovs01Repository : IGenericRepository<MOVS01> {}

        public class SqlFbMovs01Repository : MasterRepository, ISqlMovs01Repository {

    	    public SqlFbMovs01Repository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(MOVS01 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO movs01 (num_reg, cve_concep, con_part, num_cheque, ref1, ref2, status, fecha, f_cobro, band_prn, band_cont, act_sae, num_pol, tip_pol, sae_coi, monto_tot, monto_iva_tot, monto_ext, moneda, t_cambio, hora, clpv, cta_transf, fecha_liq, fecha_pol, cve_inst, mondifsae, tcambiosae, rfc, conc_sae, solicit, trans_coi, intsaenoi, x_obser, factor, formapago, anombrede, asociado, cta_contab_asoc, revisado, prioridad, doc_asoc, resaltar, anticipo, idtransf, sucursal, cuenta, clabe, multi_clpv, cve_banco_asoc, num_conc_auto, coi_depto, coi_ccostos, coi_proyecto, cuentaban_clpv, nombanco, rfcbancoord, formapagosat, numoperacion, id_cfdi, estado_cfdi, archivo_cfdi, uuidcfdi) 
                    			VALUES (@num_reg, @cve_concep, @con_part, @num_cheque, @ref1, @ref2, @status, @fecha, @f_cobro, @band_prn, @band_cont, @act_sae, @num_pol, @tip_pol, @sae_coi, @monto_tot, @monto_iva_tot, @monto_ext, @moneda, @t_cambio, @hora, @clpv, @cta_transf, @fecha_liq, @fecha_pol, @cve_inst, @mondifsae, @tcambiosae, @rfc, @conc_sae, @solicit, @trans_coi, @intsaenoi, @x_obser, @factor, @formapago, @anombrede, @asociado, @cta_contab_asoc, @revisado, @prioridad, @doc_asoc, @resaltar, @anticipo, @idtransf, @sucursal, @cuenta, @clabe, @multi_clpv, @cve_banco_asoc, @num_conc_auto, @coi_depto, @coi_ccostos, @coi_proyecto, @cuentaban_clpv, @nombanco, @rfcbancoord, @formapagosat, @numoperacion, @id_cfdi, @estado_cfdi, @archivo_cfdi, @uuidcfdi)"
                };
                item.NUM_REG = this.Max("NUM_REG");
                sqlCommand.Parameters.AddWithValue("@num_reg", item.NUM_REG);
                sqlCommand.Parameters.AddWithValue("@cve_concep", item.CVE_CONCEP);
                sqlCommand.Parameters.AddWithValue("@con_part", item.CON_PART);
                sqlCommand.Parameters.AddWithValue("@num_cheque", item.NUM_CHEQUE);
                sqlCommand.Parameters.AddWithValue("@ref1", item.REF1);
                sqlCommand.Parameters.AddWithValue("@ref2", item.REF2);
                sqlCommand.Parameters.AddWithValue("@status", item.STATUS);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@f_cobro", item.F_COBRO);
                sqlCommand.Parameters.AddWithValue("@band_prn", item.BAND_PRN);
                sqlCommand.Parameters.AddWithValue("@band_cont", item.BAND_CONT);
                sqlCommand.Parameters.AddWithValue("@act_sae", item.ACT_SAE);
                sqlCommand.Parameters.AddWithValue("@num_pol", item.NUM_POL);
                sqlCommand.Parameters.AddWithValue("@tip_pol", item.TIP_POL);
                sqlCommand.Parameters.AddWithValue("@sae_coi", item.SAE_COI);
                sqlCommand.Parameters.AddWithValue("@monto_tot", item.MONTO_TOT);
                sqlCommand.Parameters.AddWithValue("@monto_iva_tot", item.MONTO_IVA_TOT);
                sqlCommand.Parameters.AddWithValue("@monto_ext", item.MONTO_EXT);
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                sqlCommand.Parameters.AddWithValue("@t_cambio", item.T_CAMBIO);
                sqlCommand.Parameters.AddWithValue("@hora", item.HORA);
                sqlCommand.Parameters.AddWithValue("@clpv", item.CLPV);
                sqlCommand.Parameters.AddWithValue("@cta_transf", item.CTA_TRANSF);
                sqlCommand.Parameters.AddWithValue("@fecha_liq", item.FECHA_LIQ);
                sqlCommand.Parameters.AddWithValue("@fecha_pol", item.FECHA_POL);
                sqlCommand.Parameters.AddWithValue("@cve_inst", item.CVE_INST);
                sqlCommand.Parameters.AddWithValue("@mondifsae", item.MONDIFSAE);
                sqlCommand.Parameters.AddWithValue("@tcambiosae", item.TCAMBIOSAE);
                sqlCommand.Parameters.AddWithValue("@rfc", item.RFC);
                sqlCommand.Parameters.AddWithValue("@conc_sae", item.CONC_SAE);
                sqlCommand.Parameters.AddWithValue("@solicit", item.SOLICIT);
                sqlCommand.Parameters.AddWithValue("@trans_coi", item.TRANS_COI);
                sqlCommand.Parameters.AddWithValue("@intsaenoi", item.INTSAENOI);
                sqlCommand.Parameters.AddWithValue("@x_obser", item.X_OBSER);
                sqlCommand.Parameters.AddWithValue("@factor", item.FACTOR);
                sqlCommand.Parameters.AddWithValue("@formapago", item.FORMAPAGO);
                sqlCommand.Parameters.AddWithValue("@anombrede", item.ANOMBREDE);
                sqlCommand.Parameters.AddWithValue("@asociado", item.ASOCIADO);
                sqlCommand.Parameters.AddWithValue("@cta_contab_asoc", item.CTA_CONTAB_ASOC);
                sqlCommand.Parameters.AddWithValue("@revisado", item.REVISADO);
                sqlCommand.Parameters.AddWithValue("@prioridad", item.PRIORIDAD);
                sqlCommand.Parameters.AddWithValue("@doc_asoc", item.DOC_ASOC);
                sqlCommand.Parameters.AddWithValue("@resaltar", item.RESALTAR);
                sqlCommand.Parameters.AddWithValue("@anticipo", item.ANTICIPO);
                sqlCommand.Parameters.AddWithValue("@idtransf", item.IDTRANSF);
                sqlCommand.Parameters.AddWithValue("@sucursal", item.SUCURSAL);
                sqlCommand.Parameters.AddWithValue("@cuenta", item.CUENTA);
                sqlCommand.Parameters.AddWithValue("@clabe", item.CLABE);
                sqlCommand.Parameters.AddWithValue("@multi_clpv", itemMULTI_CLPV.);
                sqlCommand.Parameters.AddWithValue("@cve_banco_asoc", item.CVE_BANCO_ASOC);
                sqlCommand.Parameters.AddWithValue("@num_conc_auto", item.NUM_CONC_AUTO);
                sqlCommand.Parameters.AddWithValue("@coi_depto", item.COI_DEPTO);
                sqlCommand.Parameters.AddWithValue("@coi_ccostos", item.COI_CCOSTOS);
                sqlCommand.Parameters.AddWithValue("@coi_proyecto", item.COI_PROYECTO);
                sqlCommand.Parameters.AddWithValue("@cuentaban_clpv", item.CUENTABAN_CLPV);
                sqlCommand.Parameters.AddWithValue("@nombanco", item.NOMBANCO);
                sqlCommand.Parameters.AddWithValue("@rfcbancoord", item.RFCBANCOORD);
                sqlCommand.Parameters.AddWithValue("@formapagosat", item.FORMAPAGOSAT);
                sqlCommand.Parameters.AddWithValue("@numoperacion", item.NUMOPERACION);
                sqlCommand.Parameters.AddWithValue("@id_cfdi", item.ID_CFDI);
                sqlCommand.Parameters.AddWithValue("@estado_cfdi", item.ESTADO_CFDI);
                sqlCommand.Parameters.AddWithValue("@archivo_cfdi", item.ARCHIVO_CFDI);
                sqlCommand.Parameters.AddWithValue("@uuidcfdi", item.UUIDCFDI);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(MOVS01 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE movs01 
                    			SET cve_concep = @cve_concep, con_part = @con_part, num_cheque = @num_cheque, ref1 = @ref1, ref2 = @ref2, status = @status, fecha = @fecha, f_cobro = @f_cobro, band_prn = @band_prn, band_cont = @band_cont, act_sae = @act_sae, num_pol = @num_pol, tip_pol = @tip_pol, sae_coi = @sae_coi, monto_tot = @monto_tot, monto_iva_tot = @monto_iva_tot, monto_ext = @monto_ext, moneda = @moneda, t_cambio = @t_cambio, hora = @hora, clpv = @clpv, cta_transf = @cta_transf, fecha_liq = @fecha_liq, fecha_pol = @fecha_pol, cve_inst = @cve_inst, mondifsae = @mondifsae, tcambiosae = @tcambiosae, rfc = @rfc, conc_sae = @conc_sae, solicit = @solicit, trans_coi = @trans_coi, intsaenoi = @intsaenoi, x_obser = @x_obser, factor = @factor, formapago = @formapago, anombrede = @anombrede, asociado = @asociado, cta_contab_asoc = @cta_contab_asoc, revisado = @revisado, prioridad = @prioridad, doc_asoc = @doc_asoc, resaltar = @resaltar, anticipo = @anticipo, idtransf = @idtransf, sucursal = @sucursal, cuenta = @cuenta, clabe = @clabe, multi_clpv = @multi_clpv, cve_banco_asoc = @cve_banco_asoc, num_conc_auto = @num_conc_auto, coi_depto = @coi_depto, coi_ccostos = @coi_ccostos, coi_proyecto = @coi_proyecto, cuentaban_clpv = @cuentaban_clpv, nombanco = @nombanco, rfcbancoord = @rfcbancoord, formapagosat = @formapagosat, numoperacion = @numoperacion, id_cfdi = @id_cfdi, estado_cfdi = @estado_cfdi, archivo_cfdi = @archivo_cfdi, uuidcfdi = @uuidcfdi 
                    			WHERE num_reg = @num_reg;"
                };
                sqlCommand.Parameters.AddWithValue("@num_reg", item.NUM_REG);
                sqlCommand.Parameters.AddWithValue("@cve_concep", item.CVE_CONCEP);
                sqlCommand.Parameters.AddWithValue("@con_part", item.CON_PART);
                sqlCommand.Parameters.AddWithValue("@num_cheque", item.NUM_CHEQUE);
                sqlCommand.Parameters.AddWithValue("@ref1", item.REF1);
                sqlCommand.Parameters.AddWithValue("@ref2", item.REF2);
                sqlCommand.Parameters.AddWithValue("@status", item.STATUS);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@f_cobro", item.F_COBRO);
                sqlCommand.Parameters.AddWithValue("@band_prn", item.BAND_PRN);
                sqlCommand.Parameters.AddWithValue("@band_cont", item.BAND_CONT);
                sqlCommand.Parameters.AddWithValue("@act_sae", item.ACT_SAE);
                sqlCommand.Parameters.AddWithValue("@num_pol", item.NUM_POL);
                sqlCommand.Parameters.AddWithValue("@tip_pol", item.TIP_POL);
                sqlCommand.Parameters.AddWithValue("@sae_coi", item.SAE_COI);
                sqlCommand.Parameters.AddWithValue("@monto_tot", item.MONTO_TOT);
                sqlCommand.Parameters.AddWithValue("@monto_iva_tot", item.MONTO_IVA_TOT);
                sqlCommand.Parameters.AddWithValue("@monto_ext", item.MONTO_EXT);
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                sqlCommand.Parameters.AddWithValue("@t_cambio", item.T_CAMBIO);
                sqlCommand.Parameters.AddWithValue("@hora", item.HORA);
                sqlCommand.Parameters.AddWithValue("@clpv", item.CLPV);
                sqlCommand.Parameters.AddWithValue("@cta_transf", item.CTA_TRANSF);
                sqlCommand.Parameters.AddWithValue("@fecha_liq", item.FECHA_LIQ);
                sqlCommand.Parameters.AddWithValue("@fecha_pol", item.FECHA_POL);
                sqlCommand.Parameters.AddWithValue("@cve_inst", item.CVE_INST);
                sqlCommand.Parameters.AddWithValue("@mondifsae", item.MONDIFSAE);
                sqlCommand.Parameters.AddWithValue("@tcambiosae", item.TCAMBIOSAE);
                sqlCommand.Parameters.AddWithValue("@rfc", item.RFC);
                sqlCommand.Parameters.AddWithValue("@conc_sae", item.CONC_SAE);
                sqlCommand.Parameters.AddWithValue("@solicit", item.SOLICIT);
                sqlCommand.Parameters.AddWithValue("@trans_coi", item.TRANS_COI);
                sqlCommand.Parameters.AddWithValue("@intsaenoi", item.INTSAENOI);
                sqlCommand.Parameters.AddWithValue("@x_obser", item.X_OBSER);
                sqlCommand.Parameters.AddWithValue("@factor", item.FACTOR);
                sqlCommand.Parameters.AddWithValue("@formapago", item.FORMAPAGO);
                sqlCommand.Parameters.AddWithValue("@anombrede", item.ANOMBREDE);
                sqlCommand.Parameters.AddWithValue("@asociado", item.ASOCIADO);
                sqlCommand.Parameters.AddWithValue("@cta_contab_asoc", item.CTA_CONTAB_ASOC);
                sqlCommand.Parameters.AddWithValue("@revisado", item.REVISADO);
                sqlCommand.Parameters.AddWithValue("@prioridad", item.PRIORIDAD);
                sqlCommand.Parameters.AddWithValue("@doc_asoc", item.DOC_ASOC);
                sqlCommand.Parameters.AddWithValue("@resaltar", item.RESALTAR);
                sqlCommand.Parameters.AddWithValue("@anticipo", item.ANTICIPO);
                sqlCommand.Parameters.AddWithValue("@idtransf", item.IDTRANSF);
                sqlCommand.Parameters.AddWithValue("@sucursal", item.SUCURSAL);
                sqlCommand.Parameters.AddWithValue("@cuenta", item.CUENTA);
                sqlCommand.Parameters.AddWithValue("@clabe", item.CLABE);
                sqlCommand.Parameters.AddWithValue("@multi_clpv", itemMULTI_CLPV.);
                sqlCommand.Parameters.AddWithValue("@cve_banco_asoc", item.CVE_BANCO_ASOC);
                sqlCommand.Parameters.AddWithValue("@num_conc_auto", item.NUM_CONC_AUTO);
                sqlCommand.Parameters.AddWithValue("@coi_depto", item.COI_DEPTO);
                sqlCommand.Parameters.AddWithValue("@coi_ccostos", item.COI_CCOSTOS);
                sqlCommand.Parameters.AddWithValue("@coi_proyecto", item.COI_PROYECTO);
                sqlCommand.Parameters.AddWithValue("@cuentaban_clpv", item.CUENTABAN_CLPV);
                sqlCommand.Parameters.AddWithValue("@nombanco", item.NOMBANCO);
                sqlCommand.Parameters.AddWithValue("@rfcbancoord", item.RFCBANCOORD);
                sqlCommand.Parameters.AddWithValue("@formapagosat", item.FORMAPAGOSAT);
                sqlCommand.Parameters.AddWithValue("@numoperacion", item.NUMOPERACION);
                sqlCommand.Parameters.AddWithValue("@id_cfdi", item.ID_CFDI);
                sqlCommand.Parameters.AddWithValue("@estado_cfdi", item.ESTADO_CFDI);
                sqlCommand.Parameters.AddWithValue("@archivo_cfdi", item.ARCHIVO_CFDI);
                sqlCommand.Parameters.AddWithValue("@uuidcfdi", item.UUIDCFDI);
                return this.ExecuteScalar(sqlCommand);
            }

            public MOVS01 GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM movs01 WHERE num_reg = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<MOVS01>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<MOVS01> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM movs01")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<MOVS01>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }