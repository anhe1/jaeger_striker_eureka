    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlMonedaRepository : IGenericRepository<MONEDA> {}

        public class SqlFbMonedaRepository : MasterRepository, ISqlMonedaRepository {

    	    public SqlFbMonedaRepository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(MONEDA item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO moneda (num_reg, moneda, prefijo, fecha, tipocambio, ley_sing, ley_plur, termina, idioma, monedasat) 
                    			VALUES (@num_reg, @moneda, @prefijo, @fecha, @tipocambio, @ley_sing, @ley_plur, @termina, @idioma, @monedasat)"
                };
                item.NUM_REG = this.Max("NUM_REG");
                sqlCommand.Parameters.AddWithValue("@num_reg", item.NUM_REG);
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                sqlCommand.Parameters.AddWithValue("@prefijo", item.PREFIJO);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@tipocambio", item.TIPOCAMBIO);
                sqlCommand.Parameters.AddWithValue("@ley_sing", item.LEY_SING);
                sqlCommand.Parameters.AddWithValue("@ley_plur", item.LEY_PLUR);
                sqlCommand.Parameters.AddWithValue("@termina", item.TERMINA);
                sqlCommand.Parameters.AddWithValue("@idioma", item.IDIOMA);
                sqlCommand.Parameters.AddWithValue("@monedasat", item.MONEDASAT);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(MONEDA item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE moneda 
                    			SET moneda = @moneda, prefijo = @prefijo, fecha = @fecha, tipocambio = @tipocambio, ley_sing = @ley_sing, ley_plur = @ley_plur, termina = @termina, idioma = @idioma, monedasat = @monedasat 
                    			WHERE num_reg = @num_reg;"
                };
                sqlCommand.Parameters.AddWithValue("@num_reg", item.NUM_REG);
                sqlCommand.Parameters.AddWithValue("@moneda", item.MONEDA);
                sqlCommand.Parameters.AddWithValue("@prefijo", item.PREFIJO);
                sqlCommand.Parameters.AddWithValue("@fecha", item.FECHA);
                sqlCommand.Parameters.AddWithValue("@tipocambio", item.TIPOCAMBIO);
                sqlCommand.Parameters.AddWithValue("@ley_sing", item.LEY_SING);
                sqlCommand.Parameters.AddWithValue("@ley_plur", item.LEY_PLUR);
                sqlCommand.Parameters.AddWithValue("@termina", item.TERMINA);
                sqlCommand.Parameters.AddWithValue("@idioma", item.IDIOMA);
                sqlCommand.Parameters.AddWithValue("@monedasat", item.MONEDASAT);
                return this.ExecuteScalar(sqlCommand);
            }

            public MONEDA GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM moneda WHERE num_reg = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<MONEDA>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<MONEDA> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM moneda")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<MONEDA>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }