    using System.Linq;
    using System.Collections.Generic;
    using FirebirdSql.Data.FirebirdClient;
    using Jaeger.Domain.Contracts;

    namespace Jaeger.Domain.Repository {

    	public interface ISqlParmovs03Repository : IGenericRepository<PARMOVS03> {}

        public class SqlFbParmovs03Repository : MasterRepository, ISqlParmovs03Repository {

    	    public SqlFbParmovs03Repository(DataBaseConfiguracion configuracion) : base(configuracion) {}

    	    public int Insert(PARMOVS03 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"INSERT INTO parmovs03 (num_regp, num_mov, cve_concep, referencia, monto_iva, fact, monto_doc, monto_ext, orden, x_obser, numcargo, numcptopadre, tipocambiosae, tipocambiobanco, docto, no_partidasae, monedadoc, part_clpv, part_clpv_rfc, part_anombrede, iva_mc, part_cta_contab_asoc, part_concepsae, part_anticipo, part_cta_banco_asoc, part_cve_banco_asoc, impsaldoant) 
                    			VALUES (@num_regp, @num_mov, @cve_concep, @referencia, @monto_iva, @fact, @monto_doc, @monto_ext, @orden, @x_obser, @numcargo, @numcptopadre, @tipocambiosae, @tipocambiobanco, @docto, @no_partidasae, @monedadoc, @part_clpv, @part_clpv_rfc, @part_anombrede, @iva_mc, @part_cta_contab_asoc, @part_concepsae, @part_anticipo, @part_cta_banco_asoc, @part_cve_banco_asoc, @impsaldoant)"
                };
                item.NUM_REGP = this.Max("NUM_REGP");
                sqlCommand.Parameters.AddWithValue("@num_regp", item.NUM_REGP);
                sqlCommand.Parameters.AddWithValue("@num_mov", item.NUM_MOV);
                sqlCommand.Parameters.AddWithValue("@cve_concep", item.CVE_CONCEP);
                sqlCommand.Parameters.AddWithValue("@referencia", item.REFERENCIA);
                sqlCommand.Parameters.AddWithValue("@monto_iva", item.MONTO_IVA);
                sqlCommand.Parameters.AddWithValue("@fact", item.FACT);
                sqlCommand.Parameters.AddWithValue("@monto_doc", item.MONTO_DOC);
                sqlCommand.Parameters.AddWithValue("@monto_ext", item.MONTO_EXT);
                sqlCommand.Parameters.AddWithValue("@orden", item.ORDEN);
                sqlCommand.Parameters.AddWithValue("@x_obser", item.X_OBSER);
                sqlCommand.Parameters.AddWithValue("@numcargo", item.NUMCARGO);
                sqlCommand.Parameters.AddWithValue("@numcptopadre", item.NUMCPTOPADRE);
                sqlCommand.Parameters.AddWithValue("@tipocambiosae", item.TIPOCAMBIOSAE);
                sqlCommand.Parameters.AddWithValue("@tipocambiobanco", item.TIPOCAMBIOBANCO);
                sqlCommand.Parameters.AddWithValue("@docto", item.DOCTO);
                sqlCommand.Parameters.AddWithValue("@no_partidasae", item.NO_PARTIDASAE);
                sqlCommand.Parameters.AddWithValue("@monedadoc", item.MONEDADOC);
                sqlCommand.Parameters.AddWithValue("@part_clpv", item.PART_CLPV);
                sqlCommand.Parameters.AddWithValue("@part_clpv_rfc", item.PART_CLPV_RFC);
                sqlCommand.Parameters.AddWithValue("@part_anombrede", item.PART_ANOMBREDE);
                sqlCommand.Parameters.AddWithValue("@iva_mc", item.IVA_MC);
                sqlCommand.Parameters.AddWithValue("@part_cta_contab_asoc", item.PART_CTA_CONTAB_ASOC);
                sqlCommand.Parameters.AddWithValue("@part_concepsae", item.PART_CONCEPSAE);
                sqlCommand.Parameters.AddWithValue("@part_anticipo", item.PART_ANTICIPO);
                sqlCommand.Parameters.AddWithValue("@part_cta_banco_asoc", item.PART_CTA_BANCO_ASOC);
                sqlCommand.Parameters.AddWithValue("@part_cve_banco_asoc", item.PART_CVE_BANCO_ASOC);
                sqlCommand.Parameters.AddWithValue("@impsaldoant", item.IMPSALDOANT);
                return this.ExecuteScalar(sqlCommand);
            }

            public int Update(PARMOVS03 item) {
                var sqlCommand = new FbCommand{
                    CommandText = @"UPDATE parmovs03 
                    			SET num_mov = @num_mov, cve_concep = @cve_concep, referencia = @referencia, monto_iva = @monto_iva, fact = @fact, monto_doc = @monto_doc, monto_ext = @monto_ext, orden = @orden, x_obser = @x_obser, numcargo = @numcargo, numcptopadre = @numcptopadre, tipocambiosae = @tipocambiosae, tipocambiobanco = @tipocambiobanco, docto = @docto, no_partidasae = @no_partidasae, monedadoc = @monedadoc, part_clpv = @part_clpv, part_clpv_rfc = @part_clpv_rfc, part_anombrede = @part_anombrede, iva_mc = @iva_mc, part_cta_contab_asoc = @part_cta_contab_asoc, part_concepsae = @part_concepsae, part_anticipo = @part_anticipo, part_cta_banco_asoc = @part_cta_banco_asoc, part_cve_banco_asoc = @part_cve_banco_asoc, impsaldoant = @impsaldoant 
                    			WHERE num_regp = @num_regp;"
                };
                sqlCommand.Parameters.AddWithValue("@num_regp", item.NUM_REGP);
                sqlCommand.Parameters.AddWithValue("@num_mov", item.NUM_MOV);
                sqlCommand.Parameters.AddWithValue("@cve_concep", item.CVE_CONCEP);
                sqlCommand.Parameters.AddWithValue("@referencia", item.REFERENCIA);
                sqlCommand.Parameters.AddWithValue("@monto_iva", item.MONTO_IVA);
                sqlCommand.Parameters.AddWithValue("@fact", item.FACT);
                sqlCommand.Parameters.AddWithValue("@monto_doc", item.MONTO_DOC);
                sqlCommand.Parameters.AddWithValue("@monto_ext", item.MONTO_EXT);
                sqlCommand.Parameters.AddWithValue("@orden", item.ORDEN);
                sqlCommand.Parameters.AddWithValue("@x_obser", item.X_OBSER);
                sqlCommand.Parameters.AddWithValue("@numcargo", item.NUMCARGO);
                sqlCommand.Parameters.AddWithValue("@numcptopadre", item.NUMCPTOPADRE);
                sqlCommand.Parameters.AddWithValue("@tipocambiosae", item.TIPOCAMBIOSAE);
                sqlCommand.Parameters.AddWithValue("@tipocambiobanco", item.TIPOCAMBIOBANCO);
                sqlCommand.Parameters.AddWithValue("@docto", item.DOCTO);
                sqlCommand.Parameters.AddWithValue("@no_partidasae", item.NO_PARTIDASAE);
                sqlCommand.Parameters.AddWithValue("@monedadoc", item.MONEDADOC);
                sqlCommand.Parameters.AddWithValue("@part_clpv", item.PART_CLPV);
                sqlCommand.Parameters.AddWithValue("@part_clpv_rfc", item.PART_CLPV_RFC);
                sqlCommand.Parameters.AddWithValue("@part_anombrede", item.PART_ANOMBREDE);
                sqlCommand.Parameters.AddWithValue("@iva_mc", item.IVA_MC);
                sqlCommand.Parameters.AddWithValue("@part_cta_contab_asoc", item.PART_CTA_CONTAB_ASOC);
                sqlCommand.Parameters.AddWithValue("@part_concepsae", item.PART_CONCEPSAE);
                sqlCommand.Parameters.AddWithValue("@part_anticipo", item.PART_ANTICIPO);
                sqlCommand.Parameters.AddWithValue("@part_cta_banco_asoc", item.PART_CTA_BANCO_ASOC);
                sqlCommand.Parameters.AddWithValue("@part_cve_banco_asoc", item.PART_CVE_BANCO_ASOC);
                sqlCommand.Parameters.AddWithValue("@impsaldoant", item.IMPSALDOANT);
                return this.ExecuteScalar(sqlCommand);
            }

            public PARMOVS03 GetById(int id) {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM parmovs03 WHERE num_regp = @id")
                };
                sqlCommand.Parameters.AddWithValue("@id", id);
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<PARMOVS03>();
                return mapper.Map(tabla).SingleOrDefault();
            }

            public IEnumerable<PARMOVS03> GetList() {
                var sqlCommand = new FbCommand {
                    CommandText = string.Format("SELECT * FROM parmovs03")
                };

                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<PARMOVS03>();
                return mapper.Map(tabla).ToList();
            }

    	}
    }