﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///UUIDTIMBRES
   ///</summary>
   [SugarTable("uuidtimbres")]
   public partial class UUIDTIMBRES
   {
      public UUIDTIMBRES(){
      }

      private int _NUM_REGU;
      private int _NUM_REGP;
      private string _UUIDTIMBRE;
      private double? _MONTO;
      private string _SERIE;
      private string _FOLIO;
      private string _RFCEMISOR;
      private string _RFCRECEPTOR;
      private int? _ORDEN;
      private string _FECHA;
      private int? _TIPOCOMPROBANTE;
      private string _MONEDADR;
      private double? _TIPOCAMBIODR;
      private string _METODODEPAGODR;
      private int? _NUMPARCIALIDAD;
      private double? _IMPPAGADO;
      private double? _IMPSALDOINSOLUTO;

      /// <summary>
      /// obtener o establecer NUM_REGU
      /// </summary>
      [DataNames("NUM_REGU")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "num_regu", ColumnDescription = "Numero de registro", IsNullable = false)]
      public int NUM_REGU {get { return this._NUM_REGU; } set { this._NUM_REGU = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_REGP
      /// </summary>
      [DataNames("NUM_REGP")]
      [SugarColumn(ColumnName = "num_regp", ColumnDescription = "num_regp", IsNullable = false)]
      public int NUM_REGP {get { return this._NUM_REGP; } set { this._NUM_REGP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer UUIDTIMBRE
      /// </summary>
      [DataNames("UUIDTIMBRE")]
      [SugarColumn(ColumnName = "uuidtimbre", ColumnDescription = "UUID del comprobante", IsNullable = false, Length = 36)]
      public string UUIDTIMBRE {get { return this._UUIDTIMBRE; } set { this._UUIDTIMBRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTO
      /// </summary>
      [DataNames("MONTO")]
      [SugarColumn(ColumnName = "monto", ColumnDescription = "Monto", DefaultValue = 0)]
      public double? MONTO {get { return this._MONTO; } set { this._MONTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SERIE
      /// </summary>
      [DataNames("SERIE")]
      [SugarColumn(ColumnName = "serie", ColumnDescription = "Serie", Length = 100)]
      public string SERIE {get { return this._SERIE; } set { this._SERIE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO
      /// </summary>
      [DataNames("FOLIO")]
      [SugarColumn(ColumnName = "folio", ColumnDescription = "Folio", Length = 100)]
      public string FOLIO {get { return this._FOLIO; } set { this._FOLIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFCEMISOR
      /// </summary>
      [DataNames("RFCEMISOR")]
      [SugarColumn(ColumnName = "rfcemisor", ColumnDescription = "RFC emisor", Length = 30)]
      public string RFCEMISOR {get { return this._RFCEMISOR; } set { this._RFCEMISOR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFCRECEPTOR
      /// </summary>
      [DataNames("RFCRECEPTOR")]
      [SugarColumn(ColumnName = "rfcreceptor", ColumnDescription = "RFC receptor", Length = 30)]
      public string RFCRECEPTOR {get { return this._RFCRECEPTOR; } set { this._RFCRECEPTOR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ORDEN
      /// </summary>
      [DataNames("ORDEN")]
      [SugarColumn(ColumnName = "orden", ColumnDescription = "Orden de la partida")]
      public int? ORDEN {get { return this._ORDEN; } set { this._ORDEN = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHA
      /// </summary>
      [DataNames("FECHA")]
      [SugarColumn(ColumnName = "fecha", ColumnDescription = "Fecha", Length = 10)]
      public string FECHA {get { return this._FECHA; } set { this._FECHA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOCOMPROBANTE
      /// </summary>
      [DataNames("TIPOCOMPROBANTE")]
      [SugarColumn(ColumnName = "tipocomprobante", ColumnDescription = "Indica si es CFDi o Extranjero")]
      public int? TIPOCOMPROBANTE {get { return this._TIPOCOMPROBANTE; } set { this._TIPOCOMPROBANTE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONEDADR
      /// </summary>
      [DataNames("MONEDADR")]
      [SugarColumn(ColumnName = "monedadr", ColumnDescription = "Moneda que Devuelve SAE", Length = 5)]
      public string MONEDADR {get { return this._MONEDADR; } set { this._MONEDADR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOCAMBIODR
      /// </summary>
      [DataNames("TIPOCAMBIODR")]
      [SugarColumn(ColumnName = "tipocambiodr", ColumnDescription = "Tipo de cambio que Devuelve SAE")]
      public double? TIPOCAMBIODR {get { return this._TIPOCAMBIODR; } set { this._TIPOCAMBIODR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer METODODEPAGODR
      /// </summary>
      [DataNames("METODODEPAGODR")]
      [SugarColumn(ColumnName = "metododepagodr", ColumnDescription = "Metodo de pago que devulve SAE", Length = 5)]
      public string METODODEPAGODR {get { return this._METODODEPAGODR; } set { this._METODODEPAGODR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMPARCIALIDAD
      /// </summary>
      [DataNames("NUMPARCIALIDAD")]
      [SugarColumn(ColumnName = "numparcialidad", ColumnDescription = "Numero de parcialidad que Devuelve SAE")]
      public int? NUMPARCIALIDAD {get { return this._NUMPARCIALIDAD; } set { this._NUMPARCIALIDAD = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IMPPAGADO
      /// </summary>
      [DataNames("IMPPAGADO")]
      [SugarColumn(ColumnName = "imppagado", ColumnDescription = "Importe pagado")]
      public double? IMPPAGADO {get { return this._IMPPAGADO; } set { this._IMPPAGADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IMPSALDOINSOLUTO
      /// </summary>
      [DataNames("IMPSALDOINSOLUTO")]
      [SugarColumn(ColumnName = "impsaldoinsoluto", ColumnDescription = "IMPSALDOANT - IMPPAGADO")]
      public double? IMPSALDOINSOLUTO {get { return this._IMPSALDOINSOLUTO; } set { this._IMPSALDOINSOLUTO = value; this.OnPropertyChanged(); }}
   }
}
