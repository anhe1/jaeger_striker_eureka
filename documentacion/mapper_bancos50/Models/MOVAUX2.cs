﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///MOVAUX2
   ///</summary>
   [SugarTable("movaux2")]
   public partial class MOVAUX2
   {
      public MOVAUX2(){
      }

      private int _NUM_CTA;
      private int _NUM_MOV_AUX;
      private int _NUM_REG_MOV;

      /// <summary>
      /// obtener o establecer NUM_CTA
      /// </summary>
      [DataNames("NUM_CTA")]
      [SugarColumn(ColumnName = "num_cta", ColumnDescription = "num_cta", IsNullable = false)]
      public int NUM_CTA {get { return this._NUM_CTA; } set { this._NUM_CTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_MOV_AUX
      /// </summary>
      [DataNames("NUM_MOV_AUX")]
      [SugarColumn(ColumnName = "num_mov_aux", ColumnDescription = "num_mov_aux", IsNullable = false)]
      public int NUM_MOV_AUX {get { return this._NUM_MOV_AUX; } set { this._NUM_MOV_AUX = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_REG_MOV
      /// </summary>
      [DataNames("NUM_REG_MOV")]
      [SugarColumn(ColumnName = "num_reg_mov", ColumnDescription = "num_reg_mov", IsNullable = false)]
      public int NUM_REG_MOV {get { return this._NUM_REG_MOV; } set { this._NUM_REG_MOV = value; this.OnPropertyChanged(); }}
   }
}
