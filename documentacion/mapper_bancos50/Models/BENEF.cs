﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///BENEF
   ///</summary>
   [SugarTable("benef")]
   public partial class BENEF
   {
      public BENEF(){
      }

      private int _NUM_REG;
      private string _NOMBRE;
      private string _RFC;
      private string _CTA_CONTAB;
      private string _TIPO;
      private string _INF_GENERAL;
      private string _REFERENCIA;
      private string _BANCO;
      private string _SUCURSAL;
      private string _CUENTA;
      private string _CLABE;
      private string _CVE_BANCO;
      private int? _ESBANCOEXT;
      private string _BANCODESC;
      private string _RFCBANCO;

      /// <summary>
      /// obtener o establecer NUM_REG
      /// </summary>
      [DataNames("NUM_REG")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "num_reg", ColumnDescription = "num_reg", IsNullable = false)]
      public int NUM_REG {get { return this._NUM_REG; } set { this._NUM_REG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOMBRE
      /// </summary>
      [DataNames("NOMBRE")]
      [SugarColumn(ColumnName = "nombre", ColumnDescription = "nombre", Length = 60)]
      public string NOMBRE {get { return this._NOMBRE; } set { this._NOMBRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFC
      /// </summary>
      [DataNames("RFC")]
      [SugarColumn(ColumnName = "rfc", ColumnDescription = "rfc", Length = 15)]
      public string RFC {get { return this._RFC; } set { this._RFC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTA_CONTAB
      /// </summary>
      [DataNames("CTA_CONTAB")]
      [SugarColumn(ColumnName = "cta_contab", ColumnDescription = "cta_contab", Length = 40)]
      public string CTA_CONTAB {get { return this._CTA_CONTAB; } set { this._CTA_CONTAB = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPO
      /// </summary>
      [DataNames("TIPO")]
      [SugarColumn(ColumnName = "tipo", ColumnDescription = "tipo", Length = 20, DefaultValue = "BENEFICIARIO")]
      public string TIPO {get { return this._TIPO; } set { this._TIPO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer INF_GENERAL
      /// </summary>
      [DataNames("INF_GENERAL")]
      [SugarColumn(ColumnName = "inf_general", ColumnDescription = "inf_general", Length = 250)]
      public string INF_GENERAL {get { return this._INF_GENERAL; } set { this._INF_GENERAL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer REFERENCIA
      /// </summary>
      [DataNames("REFERENCIA")]
      [SugarColumn(ColumnName = "referencia", ColumnDescription = "referencia", Length = 20)]
      public string REFERENCIA {get { return this._REFERENCIA; } set { this._REFERENCIA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BANCO
      /// </summary>
      [DataNames("BANCO")]
      [SugarColumn(ColumnName = "banco", ColumnDescription = "banco", Length = 30)]
      public string BANCO {get { return this._BANCO; } set { this._BANCO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SUCURSAL
      /// </summary>
      [DataNames("SUCURSAL")]
      [SugarColumn(ColumnName = "sucursal", ColumnDescription = "sucursal", Length = 30)]
      public string SUCURSAL {get { return this._SUCURSAL; } set { this._SUCURSAL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CUENTA
      /// </summary>
      [DataNames("CUENTA")]
      [SugarColumn(ColumnName = "cuenta", ColumnDescription = "cuenta", Length = 30)]
      public string CUENTA {get { return this._CUENTA; } set { this._CUENTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CLABE
      /// </summary>
      [DataNames("CLABE")]
      [SugarColumn(ColumnName = "clabe", ColumnDescription = "clabe", Length = 30)]
      public string CLABE {get { return this._CLABE; } set { this._CLABE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CVE_BANCO
      /// </summary>
      [DataNames("CVE_BANCO")]
      [SugarColumn(ColumnName = "cve_banco", ColumnDescription = "cve_banco", Length = 30)]
      public string CVE_BANCO {get { return this._CVE_BANCO; } set { this._CVE_BANCO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ESBANCOEXT
      /// </summary>
      [DataNames("ESBANCOEXT")]
      [SugarColumn(ColumnName = "esbancoext", ColumnDescription = "esbancoext", DefaultValue = 0)]
      public int? ESBANCOEXT {get { return this._ESBANCOEXT; } set { this._ESBANCOEXT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BANCODESC
      /// </summary>
      [DataNames("BANCODESC")]
      [SugarColumn(ColumnName = "bancodesc", ColumnDescription = "bancodesc", Length = 20)]
      public string BANCODESC {get { return this._BANCODESC; } set { this._BANCODESC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFCBANCO
      /// </summary>
      [DataNames("RFCBANCO")]
      [SugarColumn(ColumnName = "rfcbanco", ColumnDescription = "rfcbanco", Length = 15)]
      public string RFCBANCO {get { return this._RFCBANCO; } set { this._RFCBANCO = value; this.OnPropertyChanged(); }}
   }
}
