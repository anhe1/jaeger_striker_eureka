﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///MONEDA
   ///</summary>
   [SugarTable("moneda")]
   public partial class MONEDA
   {
      public MONEDA(){
      }

      private int _NUM_REG;
      private string _MONEDA;
      private string _PREFIJO;
      private DateTime? _FECHA;
      private double? _TIPOCAMBIO;
      private string _LEY_SING;
      private string _LEY_PLUR;
      private string _TERMINA;
      private string _IDIOMA;
      private string _MONEDASAT;

      /// <summary>
      /// obtener o establecer NUM_REG
      /// </summary>
      [DataNames("NUM_REG")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "num_reg", ColumnDescription = "num_reg", IsNullable = false)]
      public int NUM_REG {get { return this._NUM_REG; } set { this._NUM_REG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONEDA
      /// </summary>
      [DataNames("MONEDA")]
      [SugarColumn(ColumnName = "moneda", ColumnDescription = "moneda", Length = 20)]
      public string MONEDA {get { return this._MONEDA; } set { this._MONEDA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PREFIJO
      /// </summary>
      [DataNames("PREFIJO")]
      [SugarColumn(ColumnName = "prefijo", ColumnDescription = "prefijo", Length = 4)]
      public string PREFIJO {get { return this._PREFIJO; } set { this._PREFIJO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHA
      /// </summary>
      [DataNames("FECHA")]
      [SugarColumn(ColumnName = "fecha", ColumnDescription = "fecha")]
      public DateTime? FECHA {get { return this._FECHA; } set { this._FECHA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOCAMBIO
      /// </summary>
      [DataNames("TIPOCAMBIO")]
      [SugarColumn(ColumnName = "tipocambio", ColumnDescription = "tipocambio", DefaultValue = 1)]
      public double? TIPOCAMBIO {get { return this._TIPOCAMBIO; } set { this._TIPOCAMBIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer LEY_SING
      /// </summary>
      [DataNames("LEY_SING")]
      [SugarColumn(ColumnName = "ley_sing", ColumnDescription = "ley_sing", Length = 10)]
      public string LEY_SING {get { return this._LEY_SING; } set { this._LEY_SING = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer LEY_PLUR
      /// </summary>
      [DataNames("LEY_PLUR")]
      [SugarColumn(ColumnName = "ley_plur", ColumnDescription = "ley_plur", Length = 10)]
      public string LEY_PLUR {get { return this._LEY_PLUR; } set { this._LEY_PLUR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TERMINA
      /// </summary>
      [DataNames("TERMINA")]
      [SugarColumn(ColumnName = "termina", ColumnDescription = "termina", Length = 4)]
      public string TERMINA {get { return this._TERMINA; } set { this._TERMINA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IDIOMA
      /// </summary>
      [DataNames("IDIOMA")]
      [SugarColumn(ColumnName = "idioma", ColumnDescription = "idioma", Length = 1, DefaultValue = "E")]
      public string IDIOMA {get { return this._IDIOMA; } set { this._IDIOMA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONEDASAT
      /// </summary>
      [DataNames("MONEDASAT")]
      [SugarColumn(ColumnName = "monedasat", ColumnDescription = "monedasat", Length = 5)]
      public string MONEDASAT {get { return this._MONEDASAT; } set { this._MONEDASAT = value; this.OnPropertyChanged(); }}
   }
}
