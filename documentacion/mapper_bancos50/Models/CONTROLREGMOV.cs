﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///CONTROLREGMOV
   ///</summary>
   [SugarTable("controlregmov")]
   public partial class CONTROLREGMOV
   {
      public CONTROLREGMOV(){
      }

      private int _CUENTA;
      private int _MOVIMIENTOS;
      private int _PARTIDAS;
      private int _UUIDS;

      /// <summary>
      /// obtener o establecer CUENTA
      /// </summary>
      [DataNames("CUENTA")]
      [SugarColumn(ColumnName = "cuenta", ColumnDescription = "cuenta", IsNullable = false)]
      public int CUENTA {get { return this._CUENTA; } set { this._CUENTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOVIMIENTOS
      /// </summary>
      [DataNames("MOVIMIENTOS")]
      [SugarColumn(ColumnName = "movimientos", ColumnDescription = "movimientos", IsNullable = false)]
      public int MOVIMIENTOS {get { return this._MOVIMIENTOS; } set { this._MOVIMIENTOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PARTIDAS
      /// </summary>
      [DataNames("PARTIDAS")]
      [SugarColumn(ColumnName = "partidas", ColumnDescription = "partidas", IsNullable = false)]
      public int PARTIDAS {get { return this._PARTIDAS; } set { this._PARTIDAS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer UUIDS
      /// </summary>
      [DataNames("UUIDS")]
      [SugarColumn(ColumnName = "uuids", ColumnDescription = "uuids", IsNullable = false)]
      public int UUIDS {get { return this._UUIDS; } set { this._UUIDS = value; this.OnPropertyChanged(); }}
   }
}
