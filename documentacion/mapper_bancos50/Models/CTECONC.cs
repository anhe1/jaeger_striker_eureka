﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///CTECONC
   ///</summary>
   [SugarTable("cteconc")]
   public partial class CTECONC
   {
      public CTECONC(){
      }

      private int _NUM_REG;
      private int _NUM_CTA;
      private DateTime _FECHA_REG;
      private DateTime _FECHA_INICIO;
      private DateTime _FECHA_FIN;
      private double _TOT_CARGOS;
      private double _TOT_ABONOS;
      private double _SALDO;

      /// <summary>
      /// obtener o establecer NUM_REG
      /// </summary>
      [DataNames("NUM_REG")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "num_reg", ColumnDescription = "num_reg")]
      public int NUM_REG {get { return this._NUM_REG; } set { this._NUM_REG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_CTA
      /// </summary>
      [DataNames("NUM_CTA")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "num_cta", ColumnDescription = "num_cta")]
      public int NUM_CTA {get { return this._NUM_CTA; } set { this._NUM_CTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHA_REG
      /// </summary>
      [DataNames("FECHA_REG")]
      [SugarColumn(ColumnName = "fecha_reg", ColumnDescription = "fecha_reg")]
      public DateTime FECHA_REG {get { return this._FECHA_REG; } set { this._FECHA_REG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHA_INICIO
      /// </summary>
      [DataNames("FECHA_INICIO")]
      [SugarColumn(ColumnName = "fecha_inicio", ColumnDescription = "fecha_inicio")]
      public DateTime FECHA_INICIO {get { return this._FECHA_INICIO; } set { this._FECHA_INICIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHA_FIN
      /// </summary>
      [DataNames("FECHA_FIN")]
      [SugarColumn(ColumnName = "fecha_fin", ColumnDescription = "fecha_fin")]
      public DateTime FECHA_FIN {get { return this._FECHA_FIN; } set { this._FECHA_FIN = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TOT_CARGOS
      /// </summary>
      [DataNames("TOT_CARGOS")]
      [SugarColumn(ColumnName = "tot_cargos", ColumnDescription = "tot_cargos")]
      public double TOT_CARGOS {get { return this._TOT_CARGOS; } set { this._TOT_CARGOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TOT_ABONOS
      /// </summary>
      [DataNames("TOT_ABONOS")]
      [SugarColumn(ColumnName = "tot_abonos", ColumnDescription = "tot_abonos")]
      public double TOT_ABONOS {get { return this._TOT_ABONOS; } set { this._TOT_ABONOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SALDO
      /// </summary>
      [DataNames("SALDO")]
      [SugarColumn(ColumnName = "saldo", ColumnDescription = "saldo")]
      public double SALDO {get { return this._SALDO; } set { this._SALDO = value; this.OnPropertyChanged(); }}
   }
}
