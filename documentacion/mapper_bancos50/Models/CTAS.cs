﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///CTAS
    ///</summary>
    [SugarTable("ctas")]
    public partial class CTAS
    {
      public CTAS(){
      }

      private int _NUM_REG;
      private string _NUM_CTA;
      private int? _STATUS;
      private string _BANCO;
      private string _SUCURSAL;
      private int? _MONEDA;
      private string _CTA_CONTAB;
      private int? _DIA_CORTE;
      private int? _SIG_CHEQUE;
      private DateTime _FECH_APER;
      private string _CHE_FTO;
      private string _BMP_BAN;
      private string _FUNCIONARIO;
      private string _TELEFONO;
      private string _CLABE;
      private string _PLAZA;
      private string _CVE_BANCO;
      private string _NOMBRE_CTA;
      private double? _SALDO_INI;
      private int? _ESBANCOEXT;
      private string _RFC;

      /// <summary>
      /// obtener o establecer NUM_REG
      /// </summary>
      [DataNames("NUM_REG")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "num_reg", ColumnDescription = "num_reg", IsNullable = false)]
      public int NUM_REG {get { return this._NUM_REG; } set { this._NUM_REG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_CTA
      /// </summary>
      [DataNames("NUM_CTA")]
      [SugarColumn(ColumnName = "num_cta", ColumnDescription = "num_cta", Length = 15)]
      public string NUM_CTA {get { return this._NUM_CTA; } set { this._NUM_CTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer STATUS
      /// </summary>
      [DataNames("STATUS")]
      [SugarColumn(ColumnName = "status", ColumnDescription = "status", DefaultValue = 1)]
      public int? STATUS {get { return this._STATUS; } set { this._STATUS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BANCO
      /// </summary>
      [DataNames("BANCO")]
      [SugarColumn(ColumnName = "banco", ColumnDescription = "banco", Length = 20)]
      public string BANCO {get { return this._BANCO; } set { this._BANCO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SUCURSAL
      /// </summary>
      [DataNames("SUCURSAL")]
      [SugarColumn(ColumnName = "sucursal", ColumnDescription = "sucursal", Length = 6)]
      public string SUCURSAL {get { return this._SUCURSAL; } set { this._SUCURSAL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONEDA
      /// </summary>
      [DataNames("MONEDA")]
      [SugarColumn(ColumnName = "moneda", ColumnDescription = "moneda", DefaultValue = 1)]
      public int? MONEDA {get { return this._MONEDA; } set { this._MONEDA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTA_CONTAB
      /// </summary>
      [DataNames("CTA_CONTAB")]
      [SugarColumn(ColumnName = "cta_contab", ColumnDescription = "cta_contab", Length = 40)]
      public string CTA_CONTAB {get { return this._CTA_CONTAB; } set { this._CTA_CONTAB = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DIA_CORTE
      /// </summary>
      [DataNames("DIA_CORTE")]
      [SugarColumn(ColumnName = "dia_corte", ColumnDescription = "dia_corte", DefaultValue = 30)]
      public int? DIA_CORTE {get { return this._DIA_CORTE; } set { this._DIA_CORTE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SIG_CHEQUE
      /// </summary>
      [DataNames("SIG_CHEQUE")]
      [SugarColumn(ColumnName = "sig_cheque", ColumnDescription = "sig_cheque", DefaultValue = 1)]
      public int? SIG_CHEQUE {get { return this._SIG_CHEQUE; } set { this._SIG_CHEQUE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECH_APER
      /// </summary>
      [DataNames("FECH_APER")]
      [SugarColumn(ColumnName = "fech_aper", ColumnDescription = "fech_aper", IsNullable = false)]
      public DateTime FECH_APER {get { return this._FECH_APER; } set { this._FECH_APER = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CHE_FTO
      /// </summary>
      [DataNames("CHE_FTO")]
      [SugarColumn(ColumnName = "che_fto", ColumnDescription = "che_fto", Length = 255)]
      public string CHE_FTO {get { return this._CHE_FTO; } set { this._CHE_FTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BMP_BAN
      /// </summary>
      [DataNames("BMP_BAN")]
      [SugarColumn(ColumnName = "bmp_ban", ColumnDescription = "bmp_ban", Length = 20)]
      public string BMP_BAN {get { return this._BMP_BAN; } set { this._BMP_BAN = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FUNCIONARIO
      /// </summary>
      [DataNames("FUNCIONARIO")]
      [SugarColumn(ColumnName = "funcionario", ColumnDescription = "funcionario", Length = 20)]
      public string FUNCIONARIO {get { return this._FUNCIONARIO; } set { this._FUNCIONARIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TELEFONO
      /// </summary>
      [DataNames("TELEFONO")]
      [SugarColumn(ColumnName = "telefono", ColumnDescription = "telefono", Length = 20)]
      public string TELEFONO {get { return this._TELEFONO; } set { this._TELEFONO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CLABE
      /// </summary>
      [DataNames("CLABE")]
      [SugarColumn(ColumnName = "clabe", ColumnDescription = "clabe", Length = 25)]
      public string CLABE {get { return this._CLABE; } set { this._CLABE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PLAZA
      /// </summary>
      [DataNames("PLAZA")]
      [SugarColumn(ColumnName = "plaza", ColumnDescription = "plaza", Length = 20)]
      public string PLAZA {get { return this._PLAZA; } set { this._PLAZA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CVE_BANCO
      /// </summary>
      [DataNames("CVE_BANCO")]
      [SugarColumn(ColumnName = "cve_banco", ColumnDescription = "cve_banco", Length = 30)]
      public string CVE_BANCO {get { return this._CVE_BANCO; } set { this._CVE_BANCO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOMBRE_CTA
      /// </summary>
      [DataNames("NOMBRE_CTA")]
      [SugarColumn(ColumnName = "nombre_cta", ColumnDescription = "nombre_cta", Length = 20)]
      public string NOMBRE_CTA {get { return this._NOMBRE_CTA; } set { this._NOMBRE_CTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SALDO_INI
      /// </summary>
      [DataNames("SALDO_INI")]
      [SugarColumn(ColumnName = "saldo_ini", ColumnDescription = "saldo_ini")]
      public double? SALDO_INI {get { return this._SALDO_INI; } set { this._SALDO_INI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ESBANCOEXT
      /// </summary>
      [DataNames("ESBANCOEXT")]
      [SugarColumn(ColumnName = "esbancoext", ColumnDescription = "esbancoext", DefaultValue = 0)]
      public int? ESBANCOEXT {get { return this._ESBANCOEXT; } set { this._ESBANCOEXT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFC
      /// </summary>
      [DataNames("RFC")]
      [SugarColumn(ColumnName = "rfc", ColumnDescription = "rfc", Length = 15, DefaultValue = "0")]
      public string RFC {get { return this._RFC; } set { this._RFC = value; this.OnPropertyChanged(); }}
   }
}
