﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///TRIAL
   ///</summary>
   [SugarTable("formpago")]
   public partial class FORMPAGO
   {
      public FORMPAGO(){
      }

      private int _NUM_REG;
      private string _DESCRIPCION;
      private int? _TIPO;
      private string _FORMAPAGOSAT;

      /// <summary>
      /// obtener o establecer TRIAL Default_New: Nullable_New:False
      /// </summary>
      [DataNames("NUM_REG")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "num_reg", ColumnDescription = "num_reg", IsNullable = false)]
      public int NUM_REG {get { return this._NUM_REG; } set { this._NUM_REG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("DESCRIPCION")] 
      [SugarColumn(ColumnName = "descripcion", ColumnDescription = "descripcion", Length = 30)]
      public string DESCRIPCION {get { return this._DESCRIPCION; } set { this._DESCRIPCION = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:-1 Nullable_New:True
      /// </summary>
      [DataNames("TIPO")] 
      [SugarColumn(ColumnName = "tipo", ColumnDescription = "tipo", DefaultValue = -1)]
      public int? TIPO {get { return this._TIPO; } set { this._TIPO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("FORMAPAGOSAT")] 
      [SugarColumn(ColumnName = "formapagosat", ColumnDescription = "formapagosat", Length = 5)]
      public string FORMAPAGOSAT {get { return this._FORMAPAGOSAT; } set { this._FORMAPAGOSAT = value; this.OnPropertyChanged(); }}
   }
}
