﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///MOVS01
   ///</summary>
   [SugarTable("movs01")]
   public partial class MOVS01
   {
      public MOVS01(){
      }

      private int _NUM_REG;
      private string _CVE_CONCEP;
      private int? _CON_PART;
      private int? _NUM_CHEQUE;
      private string _REF1;
      private string _REF2;
      private string _STATUS;
      private DateTime _FECHA;
      private DateTime _F_COBRO;
      private string _BAND_PRN;
      private string _BAND_CONT;
      private string _ACT_SAE;
      private string _NUM_POL;
      private string _TIP_POL;
      private int? _SAE_COI;
      private double _MONTO_TOT;
      private double? _MONTO_IVA_TOT;
      private double? _MONTO_EXT;
      private int? _MONEDA;
      private double? _T_CAMBIO;
      private int? _HORA;
      private string _CLPV;
      private int? _CTA_TRANSF;
      private DateTime? _FECHA_LIQ;
      private DateTime? _FECHA_POL;
      private int? _CVE_INST;
      private string _MONDIFSAE;
      private double? _TCAMBIOSAE;
      private string _RFC;
      private int? _CONC_SAE;
      private string _SOLICIT;
      private int? _TRANS_COI;
      private int? _INTSAENOI;
      private string _X_OBSER;
      private int _FACTOR;
      private int _FORMAPAGO;
      private string _ANOMBREDE;
      private string _ASOCIADO;
      private string _CTA_CONTAB_ASOC;
      private int? _REVISADO;
      private string _PRIORIDAD;
      private int? _DOC_ASOC;
      private int? _RESALTAR;
      private int? _ANTICIPO;
      private int? _IDTRANSF;
      private string _SUCURSAL;
      private string _CUENTA;
      private string _CLABE;
      private int? _MULTI_CLPV;
      private string _CVE_BANCO_ASOC;
      private int? _NUM_CONC_AUTO;
      private int? _COI_DEPTO;
      private int? _COI_CCOSTOS;
      private int? _COI_PROYECTO;
      private string _CUENTABAN_CLPV;
      private string _NOMBANCO;
      private string _RFCBANCOORD;
      private string _FORMAPAGOSAT;
      private string _NUMOPERACION;
      private string _ID_CFDI;
      private string _ESTADO_CFDI;
      private string _ARCHIVO_CFDI;
      private string _UUIDCFDI;

      /// <summary>
      /// obtener o establecer NUM_REG
      /// </summary>
      [DataNames("NUM_REG")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "num_reg", ColumnDescription = "num_reg", IsNullable = false)]
      public int NUM_REG {get { return this._NUM_REG; } set { this._NUM_REG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CVE_CONCEP
      /// </summary>
      [DataNames("CVE_CONCEP")]
      [SugarColumn(ColumnName = "cve_concep", ColumnDescription = "cve_concep", IsNullable = false, Length = 6)]
      public string CVE_CONCEP {get { return this._CVE_CONCEP; } set { this._CVE_CONCEP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CON_PART
      /// </summary>
      [DataNames("CON_PART")]
      [SugarColumn(ColumnName = "con_part", ColumnDescription = "con_part", DefaultValue = 0)]
      public int? CON_PART {get { return this._CON_PART; } set { this._CON_PART = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_CHEQUE
      /// </summary>
      [DataNames("NUM_CHEQUE")]
      [SugarColumn(ColumnName = "num_cheque", ColumnDescription = "num_cheque", DefaultValue = 0)]
      public int? NUM_CHEQUE {get { return this._NUM_CHEQUE; } set { this._NUM_CHEQUE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer REF1
      /// </summary>
      [DataNames("REF1")]
      [SugarColumn(ColumnName = "ref1", ColumnDescription = "ref1", Length = 20)]
      public string REF1 {get { return this._REF1; } set { this._REF1 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer REF2
      /// </summary>
      [DataNames("REF2")]
      [SugarColumn(ColumnName = "ref2", ColumnDescription = "ref2", Length = 20)]
      public string REF2 {get { return this._REF2; } set { this._REF2 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer STATUS
      /// </summary>
      [DataNames("STATUS")]
      [SugarColumn(ColumnName = "status", ColumnDescription = "status", IsNullable = false, Length = 1)]
      public string STATUS {get { return this._STATUS; } set { this._STATUS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHA
      /// </summary>
      [DataNames("FECHA")]
      [SugarColumn(ColumnName = "fecha", ColumnDescription = "fecha", IsNullable = false)]
      public DateTime FECHA {get { return this._FECHA; } set { this._FECHA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer F_COBRO
      /// </summary>
      [DataNames("F_COBRO")]
      [SugarColumn(ColumnName = "f_cobro", ColumnDescription = "f_cobro", IsNullable = false)]
      public DateTime F_COBRO {get { return this._F_COBRO; } set { this._F_COBRO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BAND_PRN
      /// </summary>
      [DataNames("BAND_PRN")]
      [SugarColumn(ColumnName = "band_prn", ColumnDescription = "band_prn", Length = 1, DefaultValue = "N")]
      public string BAND_PRN {get { return this._BAND_PRN; } set { this._BAND_PRN = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BAND_CONT
      /// </summary>
      [DataNames("BAND_CONT")]
      [SugarColumn(ColumnName = "band_cont", ColumnDescription = "band_cont", Length = 1, DefaultValue = "N")]
      public string BAND_CONT {get { return this._BAND_CONT; } set { this._BAND_CONT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ACT_SAE
      /// </summary>
      [DataNames("ACT_SAE")]
      [SugarColumn(ColumnName = "act_sae", ColumnDescription = "act_sae", Length = 1, DefaultValue = "N")]
      public string ACT_SAE {get { return this._ACT_SAE; } set { this._ACT_SAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_POL
      /// </summary>
      [DataNames("NUM_POL")]
      [SugarColumn(ColumnName = "num_pol", ColumnDescription = "num_pol", Length = 5)]
      public string NUM_POL {get { return this._NUM_POL; } set { this._NUM_POL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIP_POL
      /// </summary>
      [DataNames("TIP_POL")]
      [SugarColumn(ColumnName = "tip_pol", ColumnDescription = "tip_pol", Length = 2)]
      public string TIP_POL {get { return this._TIP_POL; } set { this._TIP_POL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SAE_COI
      /// </summary>
      [DataNames("SAE_COI")]
      [SugarColumn(ColumnName = "sae_coi", ColumnDescription = "sae_coi", DefaultValue = 0)]
      public int? SAE_COI {get { return this._SAE_COI; } set { this._SAE_COI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTO_TOT
      /// </summary>
      [DataNames("MONTO_TOT")]
      [SugarColumn(ColumnName = "monto_tot", ColumnDescription = "monto_tot", IsNullable = false)]
      public double MONTO_TOT {get { return this._MONTO_TOT; } set { this._MONTO_TOT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTO_IVA_TOT
      /// </summary>
      [DataNames("MONTO_IVA_TOT")]
      [SugarColumn(ColumnName = "monto_iva_tot", ColumnDescription = "monto_iva_tot", DefaultValue = 0)]
      public double? MONTO_IVA_TOT {get { return this._MONTO_IVA_TOT; } set { this._MONTO_IVA_TOT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTO_EXT
      /// </summary>
      [DataNames("MONTO_EXT")]
      [SugarColumn(ColumnName = "monto_ext", ColumnDescription = "monto_ext", DefaultValue = 0)]
      public double? MONTO_EXT {get { return this._MONTO_EXT; } set { this._MONTO_EXT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONEDA
      /// </summary>
      [DataNames("MONEDA")]
      [SugarColumn(ColumnName = "moneda", ColumnDescription = "moneda", DefaultValue = 1)]
      public int? MONEDA {get { return this._MONEDA; } set { this._MONEDA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer T_CAMBIO
      /// </summary>
      [DataNames("T_CAMBIO")]
      [SugarColumn(ColumnName = "t_cambio", ColumnDescription = "t_cambio", DefaultValue = 1)]
      public double? T_CAMBIO {get { return this._T_CAMBIO; } set { this._T_CAMBIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer HORA
      /// </summary>
      [DataNames("HORA")]
      [SugarColumn(ColumnName = "hora", ColumnDescription = "hora", DefaultValue = 0)]
      public int? HORA {get { return this._HORA; } set { this._HORA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CLPV
      /// </summary>
      [DataNames("CLPV")]
      [SugarColumn(ColumnName = "clpv", ColumnDescription = "clpv", Length = 10)]
      public string CLPV {get { return this._CLPV; } set { this._CLPV = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTA_TRANSF
      /// </summary>
      [DataNames("CTA_TRANSF")]
      [SugarColumn(ColumnName = "cta_transf", ColumnDescription = "cta_transf", DefaultValue = 0)]
      public int? CTA_TRANSF {get { return this._CTA_TRANSF; } set { this._CTA_TRANSF = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHA_LIQ
      /// </summary>
      [DataNames("FECHA_LIQ")]
      [SugarColumn(ColumnName = "fecha_liq", ColumnDescription = "fecha_liq")]
      public DateTime? FECHA_LIQ {get { return this._FECHA_LIQ; } set { this._FECHA_LIQ = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHA_POL
      /// </summary>
      [DataNames("FECHA_POL")]
      [SugarColumn(ColumnName = "fecha_pol", ColumnDescription = "fecha_pol")]
      public DateTime? FECHA_POL {get { return this._FECHA_POL; } set { this._FECHA_POL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CVE_INST
      /// </summary>
      [DataNames("CVE_INST")]
      [SugarColumn(ColumnName = "cve_inst", ColumnDescription = "cve_inst", DefaultValue = 0)]
      public int? CVE_INST {get { return this._CVE_INST; } set { this._CVE_INST = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONDIFSAE
      /// </summary>
      [DataNames("MONDIFSAE")]
      [SugarColumn(ColumnName = "mondifsae", ColumnDescription = "mondifsae", Length = 1)]
      public string MONDIFSAE {get { return this._MONDIFSAE; } set { this._MONDIFSAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TCAMBIOSAE
      /// </summary>
      [DataNames("TCAMBIOSAE")]
      [SugarColumn(ColumnName = "tcambiosae", ColumnDescription = "tcambiosae", DefaultValue = 1)]
      public double? TCAMBIOSAE {get { return this._TCAMBIOSAE; } set { this._TCAMBIOSAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFC
      /// </summary>
      [DataNames("RFC")]
      [SugarColumn(ColumnName = "rfc", ColumnDescription = "rfc", Length = 20)]
      public string RFC {get { return this._RFC; } set { this._RFC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CONC_SAE
      /// </summary>
      [DataNames("CONC_SAE")]
      [SugarColumn(ColumnName = "conc_sae", ColumnDescription = "conc_sae", DefaultValue = 0)]
      public int? CONC_SAE {get { return this._CONC_SAE; } set { this._CONC_SAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SOLICIT
      /// </summary>
      [DataNames("SOLICIT")]
      [SugarColumn(ColumnName = "solicit", ColumnDescription = "solicit", Length = 50)]
      public string SOLICIT {get { return this._SOLICIT; } set { this._SOLICIT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRANS_COI
      /// </summary>
      [DataNames("TRANS_COI")]
      [SugarColumn(ColumnName = "trans_coi", ColumnDescription = "trans_coi", DefaultValue = 0)]
      public int? TRANS_COI {get { return this._TRANS_COI; } set { this._TRANS_COI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer INTSAENOI
      /// </summary>
      [DataNames("INTSAENOI")]
      [SugarColumn(ColumnName = "intsaenoi", ColumnDescription = "intsaenoi", DefaultValue = 0)]
      public int? INTSAENOI {get { return this._INTSAENOI; } set { this._INTSAENOI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer X_OBSER
      /// </summary>
      [DataNames("X_OBSER")]
      [SugarColumn(ColumnName = "x_obser", ColumnDescription = "x_obser", Length = 255)]
      public string X_OBSER {get { return this._X_OBSER; } set { this._X_OBSER = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FACTOR
      /// </summary>
      [DataNames("FACTOR")]
      [SugarColumn(ColumnName = "factor", ColumnDescription = "factor", IsNullable = false)]
      public int FACTOR {get { return this._FACTOR; } set { this._FACTOR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FORMAPAGO
      /// </summary>
      [DataNames("FORMAPAGO")]
      [SugarColumn(ColumnName = "formapago", ColumnDescription = "formapago", IsNullable = false)]
      public int FORMAPAGO {get { return this._FORMAPAGO; } set { this._FORMAPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ANOMBREDE
      /// </summary>
      [DataNames("ANOMBREDE")]
      [SugarColumn(ColumnName = "anombrede", ColumnDescription = "anombrede", Length = 120)]
      public string ANOMBREDE {get { return this._ANOMBREDE; } set { this._ANOMBREDE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ASOCIADO
      /// </summary>
      [DataNames("ASOCIADO")]
      [SugarColumn(ColumnName = "asociado", ColumnDescription = "asociado", IsNullable = false, Length = 1)]
      public string ASOCIADO {get { return this._ASOCIADO; } set { this._ASOCIADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTA_CONTAB_ASOC
      /// </summary>
      [DataNames("CTA_CONTAB_ASOC")]
      [SugarColumn(ColumnName = "cta_contab_asoc", ColumnDescription = "cta_contab_asoc", Length = 40)]
      public string CTA_CONTAB_ASOC {get { return this._CTA_CONTAB_ASOC; } set { this._CTA_CONTAB_ASOC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer REVISADO
      /// </summary>
      [DataNames("REVISADO")]
      [SugarColumn(ColumnName = "revisado", ColumnDescription = "revisado", DefaultValue = 0)]
      public int? REVISADO {get { return this._REVISADO; } set { this._REVISADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRIORIDAD
      /// </summary>
      [DataNames("PRIORIDAD")]
      [SugarColumn(ColumnName = "prioridad", ColumnDescription = "prioridad", Length = 6, DefaultValue = "NORMAL")]
      public string PRIORIDAD {get { return this._PRIORIDAD; } set { this._PRIORIDAD = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DOC_ASOC
      /// </summary>
      [DataNames("DOC_ASOC")]
      [SugarColumn(ColumnName = "doc_asoc", ColumnDescription = "doc_asoc")]
      public int? DOC_ASOC {get { return this._DOC_ASOC; } set { this._DOC_ASOC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RESALTAR
      /// </summary>
      [DataNames("RESALTAR")]
      [SugarColumn(ColumnName = "resaltar", ColumnDescription = "resaltar")]
      public int? RESALTAR {get { return this._RESALTAR; } set { this._RESALTAR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ANTICIPO
      /// </summary>
      [DataNames("ANTICIPO")]
      [SugarColumn(ColumnName = "anticipo", ColumnDescription = "anticipo")]
      public int? ANTICIPO {get { return this._ANTICIPO; } set { this._ANTICIPO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IDTRANSF
      /// </summary>
      [DataNames("IDTRANSF")]
      [SugarColumn(ColumnName = "idtransf", ColumnDescription = "idtransf")]
      public int? IDTRANSF {get { return this._IDTRANSF; } set { this._IDTRANSF = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SUCURSAL
      /// </summary>
      [DataNames("SUCURSAL")]
      [SugarColumn(ColumnName = "sucursal", ColumnDescription = "sucursal", Length = 30)]
      public string SUCURSAL {get { return this._SUCURSAL; } set { this._SUCURSAL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CUENTA
      /// </summary>
      [DataNames("CUENTA")]
      [SugarColumn(ColumnName = "cuenta", ColumnDescription = "cuenta", Length = 30)]
      public string CUENTA {get { return this._CUENTA; } set { this._CUENTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CLABE
      /// </summary>
      [DataNames("CLABE")]
      [SugarColumn(ColumnName = "clabe", ColumnDescription = "clabe", Length = 30)]
      public string CLABE {get { return this._CLABE; } set { this._CLABE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MULTI_CLPV
      /// </summary>
      [DataNames("MULTI_CLPV")]
      [SugarColumn(ColumnName = "multi_clpv", ColumnDescription = "multi_clpv")]
      public int? MULTI_CLPV {get { return this._MULTI_CLPV; } set { this._MULTI_CLPV = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CVE_BANCO_ASOC
      /// </summary>
      [DataNames("CVE_BANCO_ASOC")]
      [SugarColumn(ColumnName = "cve_banco_asoc", ColumnDescription = "cve_banco_asoc", Length = 6)]
      public string CVE_BANCO_ASOC {get { return this._CVE_BANCO_ASOC; } set { this._CVE_BANCO_ASOC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_CONC_AUTO
      /// </summary>
      [DataNames("NUM_CONC_AUTO")]
      [SugarColumn(ColumnName = "num_conc_auto", ColumnDescription = "num_conc_auto")]
      public int? NUM_CONC_AUTO {get { return this._NUM_CONC_AUTO; } set { this._NUM_CONC_AUTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer COI_DEPTO
      /// </summary>
      [DataNames("COI_DEPTO")]
      [SugarColumn(ColumnName = "coi_depto", ColumnDescription = "coi_depto")]
      public int? COI_DEPTO {get { return this._COI_DEPTO; } set { this._COI_DEPTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer COI_CCOSTOS
      /// </summary>
      [DataNames("COI_CCOSTOS")]
      [SugarColumn(ColumnName = "coi_ccostos", ColumnDescription = "coi_ccostos")]
      public int? COI_CCOSTOS {get { return this._COI_CCOSTOS; } set { this._COI_CCOSTOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer COI_PROYECTO
      /// </summary>
      [DataNames("COI_PROYECTO")]
      [SugarColumn(ColumnName = "coi_proyecto", ColumnDescription = "coi_proyecto")]
      public int? COI_PROYECTO {get { return this._COI_PROYECTO; } set { this._COI_PROYECTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CUENTABAN_CLPV
      /// </summary>
      [DataNames("CUENTABAN_CLPV")]
      [SugarColumn(ColumnName = "cuentaban_clpv", ColumnDescription = "cuentaban_clpv", Length = 30)]
      public string CUENTABAN_CLPV {get { return this._CUENTABAN_CLPV; } set { this._CUENTABAN_CLPV = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOMBANCO
      /// </summary>
      [DataNames("NOMBANCO")]
      [SugarColumn(ColumnName = "nombanco", ColumnDescription = "nombanco", Length = 230)]
      public string NOMBANCO {get { return this._NOMBANCO; } set { this._NOMBANCO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFCBANCOORD
      /// </summary>
      [DataNames("RFCBANCOORD")]
      [SugarColumn(ColumnName = "rfcbancoord", ColumnDescription = "rfcbancoord", Length = 15)]
      public string RFCBANCOORD {get { return this._RFCBANCOORD; } set { this._RFCBANCOORD = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FORMAPAGOSAT
      /// </summary>
      [DataNames("FORMAPAGOSAT")]
      [SugarColumn(ColumnName = "formapagosat", ColumnDescription = "formapagosat", Length = 5)]
      public string FORMAPAGOSAT {get { return this._FORMAPAGOSAT; } set { this._FORMAPAGOSAT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMOPERACION
      /// </summary>
      [DataNames("NUMOPERACION")]
      [SugarColumn(ColumnName = "numoperacion", ColumnDescription = "Numero de operacion", Length = 30, )]
      public string NUMOPERACION {get { return this._NUMOPERACION; } set { this._NUMOPERACION = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ID_CFDI
      /// </summary>
      [DataNames("ID_CFDI")]
      [SugarColumn(ColumnName = "id_cfdi", ColumnDescription = "id_cfdi", Length = 50)]
      public string ID_CFDI {get { return this._ID_CFDI; } set { this._ID_CFDI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ESTADO_CFDI
      /// </summary>
      [DataNames("ESTADO_CFDI")]
      [SugarColumn(ColumnName = "estado_cfdi", ColumnDescription = "estado_cfdi", Length = 1)]
      public string ESTADO_CFDI {get { return this._ESTADO_CFDI; } set { this._ESTADO_CFDI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ARCHIVO_CFDI
      /// </summary>
      [DataNames("ARCHIVO_CFDI")]
      [SugarColumn(ColumnName = "archivo_cfdi", ColumnDescription = "archivo_cfdi")]
      public string ARCHIVO_CFDI {get { return this._ARCHIVO_CFDI; } set { this._ARCHIVO_CFDI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer UUIDCFDI
      /// </summary>
      [DataNames("UUIDCFDI")]
      [SugarColumn(ColumnName = "uuidcfdi", ColumnDescription = "Uuid timbre fiscal", Length = 36)]
      public string UUIDCFDI {get { return this._UUIDCFDI; } set { this._UUIDCFDI = value; this.OnPropertyChanged(); }}
   }
}
