﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///PARMOVS02
   ///</summary>
   [SugarTable("parmovs02")]
   public partial class PARMOVS02
   {
      public PARMOVS02(){
      }

      private int _NUM_REGP;
      private int _NUM_MOV;
      private string _CVE_CONCEP;
      private string _REFERENCIA;
      private double? _MONTO_IVA;
      private string _FACT;
      private double _MONTO_DOC;
      private double? _MONTO_EXT;
      private int _ORDEN;
      private string _X_OBSER;
      private int? _NUMCARGO;
      private int? _NUMCPTOPADRE;
      private double? _TIPOCAMBIOSAE;
      private double? _TIPOCAMBIOBANCO;
      private string _DOCTO;
      private int? _NO_PARTIDASAE;
      private int? _MONEDADOC;
      private string _PART_CLPV;
      private string _PART_CLPV_RFC;
      private string _PART_ANOMBREDE;
      private double? _IVA_MC;
      private string _PART_CTA_CONTAB_ASOC;
      private int? _PART_CONCEPSAE;
      private int? _PART_ANTICIPO;
      private string _PART_CTA_BANCO_ASOC;
      private string _PART_CVE_BANCO_ASOC;
      private double? _IMPSALDOANT;

      /// <summary>
      /// obtener o establecer NUM_REGP
      /// </summary>
      [DataNames("NUM_REGP")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "num_regp", ColumnDescription = "num_regp", IsNullable = false)]
      public int NUM_REGP {get { return this._NUM_REGP; } set { this._NUM_REGP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_MOV
      /// </summary>
      [DataNames("NUM_MOV")]
      [SugarColumn(ColumnName = "num_mov", ColumnDescription = "num_mov", IsNullable = false)]
      public int NUM_MOV {get { return this._NUM_MOV; } set { this._NUM_MOV = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CVE_CONCEP
      /// </summary>
      [DataNames("CVE_CONCEP")]
      [SugarColumn(ColumnName = "cve_concep", ColumnDescription = "cve_concep", Length = 6)]
      public string CVE_CONCEP {get { return this._CVE_CONCEP; } set { this._CVE_CONCEP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer REFERENCIA
      /// </summary>
      [DataNames("REFERENCIA")]
      [SugarColumn(ColumnName = "referencia", ColumnDescription = "referencia", Length = 20)]
      public string REFERENCIA {get { return this._REFERENCIA; } set { this._REFERENCIA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTO_IVA
      /// </summary>
      [DataNames("MONTO_IVA")]
      [SugarColumn(ColumnName = "monto_iva", ColumnDescription = "monto_iva", DefaultValue = 0)]
      public double? MONTO_IVA {get { return this._MONTO_IVA; } set { this._MONTO_IVA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FACT
      /// </summary>
      [DataNames("FACT")]
      [SugarColumn(ColumnName = "fact", ColumnDescription = "fact", Length = 20)]
      public string FACT {get { return this._FACT; } set { this._FACT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTO_DOC
      /// </summary>
      [DataNames("MONTO_DOC")]
      [SugarColumn(ColumnName = "monto_doc", ColumnDescription = "monto_doc", IsNullable = false)]
      public double MONTO_DOC {get { return this._MONTO_DOC; } set { this._MONTO_DOC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTO_EXT
      /// </summary>
      [DataNames("MONTO_EXT")]
      [SugarColumn(ColumnName = "monto_ext", ColumnDescription = "monto_ext", DefaultValue = 0)]
      public double? MONTO_EXT {get { return this._MONTO_EXT; } set { this._MONTO_EXT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ORDEN
      /// </summary>
      [DataNames("ORDEN")]
      [SugarColumn(ColumnName = "orden", ColumnDescription = "orden", IsNullable = false)]
      public int ORDEN {get { return this._ORDEN; } set { this._ORDEN = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer X_OBSER
      /// </summary>
      [DataNames("X_OBSER")]
      [SugarColumn(ColumnName = "x_obser", ColumnDescription = "x_obser", Length = 255)]
      public string X_OBSER {get { return this._X_OBSER; } set { this._X_OBSER = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMCARGO
      /// </summary>
      [DataNames("NUMCARGO")]
      [SugarColumn(ColumnName = "numcargo", ColumnDescription = "numcargo", DefaultValue = )]
      public int? NUMCARGO {get { return this._NUMCARGO; } set { this._NUMCARGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMCPTOPADRE
      /// </summary>
      [DataNames("NUMCPTOPADRE")]
      [SugarColumn(ColumnName = "numcptopadre", ColumnDescription = "numcptopadre", DefaultValue = )]
      public int? NUMCPTOPADRE {get { return this._NUMCPTOPADRE; } set { this._NUMCPTOPADRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOCAMBIOSAE
      /// </summary>
      [DataNames("TIPOCAMBIOSAE")]
      [SugarColumn(ColumnName = "tipocambiosae", ColumnDescription = "tipocambiosae", DefaultValue = )]
      public double? TIPOCAMBIOSAE {get { return this._TIPOCAMBIOSAE; } set { this._TIPOCAMBIOSAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOCAMBIOBANCO
      /// </summary>
      [DataNames("TIPOCAMBIOBANCO")]
      [SugarColumn(ColumnName = "tipocambiobanco", ColumnDescription = "tipocambiobanco", DefaultValue = )]
      public double? TIPOCAMBIOBANCO {get { return this._TIPOCAMBIOBANCO; } set { this._TIPOCAMBIOBANCO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DOCTO
      /// </summary>
      [DataNames("DOCTO")]
      [SugarColumn(ColumnName = "docto", ColumnDescription = "docto", Length = 20)]
      public string DOCTO {get { return this._DOCTO; } set { this._DOCTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NO_PARTIDASAE
      /// </summary>
      [DataNames("NO_PARTIDASAE")]
      [SugarColumn(ColumnName = "no_partidasae", ColumnDescription = "no_partidasae", DefaultValue = 0)]
      public int? NO_PARTIDASAE {get { return this._NO_PARTIDASAE; } set { this._NO_PARTIDASAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONEDADOC
      /// </summary>
      [DataNames("MONEDADOC")]
      [SugarColumn(ColumnName = "monedadoc", ColumnDescription = "monedadoc", DefaultValue = 1)]
      public int? MONEDADOC {get { return this._MONEDADOC; } set { this._MONEDADOC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PART_CLPV
      /// </summary>
      [DataNames("PART_CLPV")]
      [SugarColumn(ColumnName = "part_clpv", ColumnDescription = "part_clpv", Length = 10)]
      public string PART_CLPV {get { return this._PART_CLPV; } set { this._PART_CLPV = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PART_CLPV_RFC
      /// </summary>
      [DataNames("PART_CLPV_RFC")]
      [SugarColumn(ColumnName = "part_clpv_rfc", ColumnDescription = "part_clpv_rfc", Length = 13)]
      public string PART_CLPV_RFC {get { return this._PART_CLPV_RFC; } set { this._PART_CLPV_RFC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PART_ANOMBREDE
      /// </summary>
      [DataNames("PART_ANOMBREDE")]
      [SugarColumn(ColumnName = "part_anombrede", ColumnDescription = "part_anombrede", Length = 120)]
      public string PART_ANOMBREDE {get { return this._PART_ANOMBREDE; } set { this._PART_ANOMBREDE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IVA_MC
      /// </summary>
      [DataNames("IVA_MC")]
      [SugarColumn(ColumnName = "iva_mc", ColumnDescription = "iva_mc", DefaultValue = 0)]
      public double? IVA_MC {get { return this._IVA_MC; } set { this._IVA_MC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PART_CTA_CONTAB_ASOC
      /// </summary>
      [DataNames("PART_CTA_CONTAB_ASOC")]
      [SugarColumn(ColumnName = "part_cta_contab_asoc", ColumnDescription = "part_cta_contab_asoc", Length = 40)]
      public string PART_CTA_CONTAB_ASOC {get { return this._PART_CTA_CONTAB_ASOC; } set { this._PART_CTA_CONTAB_ASOC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PART_CONCEPSAE
      /// </summary>
      [DataNames("PART_CONCEPSAE")]
      [SugarColumn(ColumnName = "part_concepsae", ColumnDescription = "part_concepsae", DefaultValue = 0)]
      public int? PART_CONCEPSAE {get { return this._PART_CONCEPSAE; } set { this._PART_CONCEPSAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PART_ANTICIPO
      /// </summary>
      [DataNames("PART_ANTICIPO")]
      [SugarColumn(ColumnName = "part_anticipo", ColumnDescription = "part_anticipo", DefaultValue = 0)]
      public int? PART_ANTICIPO {get { return this._PART_ANTICIPO; } set { this._PART_ANTICIPO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PART_CTA_BANCO_ASOC
      /// </summary>
      [DataNames("PART_CTA_BANCO_ASOC")]
      [SugarColumn(ColumnName = "part_cta_banco_asoc", ColumnDescription = "part_cta_banco_asoc", Length = 50)]
      public string PART_CTA_BANCO_ASOC {get { return this._PART_CTA_BANCO_ASOC; } set { this._PART_CTA_BANCO_ASOC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PART_CVE_BANCO_ASOC
      /// </summary>
      [DataNames("PART_CVE_BANCO_ASOC")]
      [SugarColumn(ColumnName = "part_cve_banco_asoc", ColumnDescription = "part_cve_banco_asoc", Length = 6)]
      public string PART_CVE_BANCO_ASOC {get { return this._PART_CVE_BANCO_ASOC; } set { this._PART_CVE_BANCO_ASOC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IMPSALDOANT
      /// </summary>
      [DataNames("IMPSALDOANT")]
      [SugarColumn(ColumnName = "impsaldoant", ColumnDescription = "Importe de saldo anterior")]
      public double? IMPSALDOANT {get { return this._IMPSALDOANT; } set { this._IMPSALDOANT = value; this.OnPropertyChanged(); }}
   }
}
