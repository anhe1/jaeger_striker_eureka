﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///COMD
   ///</summary>
   [SugarTable("comd")]
   public partial class COMD
   {
      public COMD(){
      }

      private string _CVE_CONCEP;
      private string _TIPO;
      private string _CONCEP;
      private int? _CONCEPSAE;
      private string _CTA_CONTAB;
      private int? _ESTADO;
      private double? _IVA;
      private string _CLASIFICACION;
      private string _PLANTILLA;

      /// <summary>
      /// obtener o establecer CVE_CONCEP
      /// </summary>
      [DataNames("CVE_CONCEP")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "cve_concep", ColumnDescription = "cve_concep", IsNullable = false, Length = 6)]
      public string CVE_CONCEP {get { return this._CVE_CONCEP; } set { this._CVE_CONCEP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPO
      /// </summary>
      [DataNames("TIPO")]
      [SugarColumn(ColumnName = "tipo", ColumnDescription = "tipo", Length = 5, DefaultValue = "CARGO")]
      public string TIPO {get { return this._TIPO; } set { this._TIPO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CONCEP
      /// </summary>
      [DataNames("CONCEP")]
      [SugarColumn(ColumnName = "concep", ColumnDescription = "concep", Length = 30)]
      public string CONCEP {get { return this._CONCEP; } set { this._CONCEP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CONCEPSAE
      /// </summary>
      [DataNames("CONCEPSAE")]
      [SugarColumn(ColumnName = "concepsae", ColumnDescription = "concepsae", DefaultValue = 0)]
      public int? CONCEPSAE {get { return this._CONCEPSAE; } set { this._CONCEPSAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTA_CONTAB
      /// </summary>
      [DataNames("CTA_CONTAB")]
      [SugarColumn(ColumnName = "cta_contab", ColumnDescription = "cta_contab", Length = 40)]
      public string CTA_CONTAB {get { return this._CTA_CONTAB; } set { this._CTA_CONTAB = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ESTADO
      /// </summary>
      [DataNames("ESTADO")]
      [SugarColumn(ColumnName = "estado", ColumnDescription = "estado", DefaultValue = 1)]
      public int? ESTADO {get { return this._ESTADO; } set { this._ESTADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IVA
      /// </summary>
      [DataNames("IVA")]
      [SugarColumn(ColumnName = "iva", ColumnDescription = "iva", DefaultValue = 0)]
      public double? IVA {get { return this._IVA; } set { this._IVA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CLASIFICACION
      /// </summary>
      [DataNames("CLASIFICACION")]
      [SugarColumn(ColumnName = "clasificacion", ColumnDescription = "clasificacion", Length = 3)]
      public string CLASIFICACION {get { return this._CLASIFICACION; } set { this._CLASIFICACION = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PLANTILLA
      /// </summary>
      [DataNames("PLANTILLA")]
      [SugarColumn(ColumnName = "plantilla", ColumnDescription = "plantilla")]
      public string PLANTILLA {get { return this._PLANTILLA; } set { this._PLANTILLA = value; this.OnPropertyChanged(); }}
   }
}
