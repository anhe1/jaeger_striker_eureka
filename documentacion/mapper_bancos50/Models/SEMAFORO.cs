﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///SEMAFORO
   ///</summary>
   [SugarTable("semaforo")]
   public partial class SEMAFORO
   {
      public SEMAFORO(){
      }

      private int _HISTANCE;

      /// <summary>
      /// obtener o establecer HISTANCE
      /// </summary>
      [DataNames("HISTANCE")] 
      [SugarColumn(ColumnName = "histance", ColumnDescription = "histance")]
      public int HISTANCE {get { return this._HISTANCE; } set { this._HISTANCE = value; this.OnPropertyChanged(); }}
   }
}
