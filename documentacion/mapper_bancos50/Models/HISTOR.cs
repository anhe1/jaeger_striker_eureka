﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///HISTOR
   ///</summary>
   [SugarTable("histor")]
   public partial class HISTOR
   {
      public HISTOR(){
      }

      private int _MONEDA;
      private DateTime _FECHA;
      private double _TIPOCAMBIO;

      /// <summary>
      /// obtener o establecer MONEDA
      /// </summary>
      [DataNames("MONEDA")]
      [SugarColumn(ColumnName = "moneda", ColumnDescription = "moneda", IsNullable = false)]
      public int MONEDA {get { return this._MONEDA; } set { this._MONEDA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHA
      /// </summary>
      [DataNames("FECHA")]
      [SugarColumn(ColumnName = "fecha", ColumnDescription = "fecha", IsNullable = false)]
      public DateTime FECHA {get { return this._FECHA; } set { this._FECHA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOCAMBIO
      /// </summary>
      [DataNames("TIPOCAMBIO")]
      [SugarColumn(ColumnName = "tipocambio", ColumnDescription = "tipocambio", IsNullable = false)]
      public double TIPOCAMBIO {get { return this._TIPOCAMBIO; } set { this._TIPOCAMBIO = value; this.OnPropertyChanged(); }}
   }
}
