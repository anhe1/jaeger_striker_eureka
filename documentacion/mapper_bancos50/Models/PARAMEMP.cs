﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///PARAMEMP
   ///</summary>
   [SugarTable("paramemp")]
   public partial class PARAMEMP
   {
      public PARAMEMP(){
      }

      private int _IDEMP;
      private string _NOMBRE;
      private string _CVL_ALTA;
      private string _COD_POS;
      private string _PASSW;
      private DateTime? _FVIGENCIA;
      private string _DIREC;
      private string _POBL;
      private string _RFC_EMP;
      private string _BAN_INST;
      private string _EMP_COI;
      private string _COI_LINEA;
      private string _ALT_COI;
      private string _NUM_DEC;
      private string _INT_SAE;
      private string _INT_COI;
      private string _SAE_CXP;
      private string _SAE_CXC;
      private string _EMP_SAE;
      private string _ABO_CXP;
      private string _ANT_CXP;
      private string _ABO_CXC;
      private string _ANT_CXC;
      private string _ACT_TRANS;
      private string _CPF_CXC;
      private string _MON_BASE;
      private string _LIM_SGIRO;
      private string _POL_FTO;
      private string _MOV_FTO;
      private string _CTA_IMPSTO;
      private string _SUBDIR;
      private double? _IMPUESTO;
      private string _WIN_CASCADA;
      private string _SHOW_MONEDAS;
      private string _TC_CXP;
      private string _TC_CXC;
      private string _SAEESMULTIMON;
      private string _DESG_CTASCOI;
      private string _CAPRFC_IVA;
      private string _COI_SOLO_CHEQ;
      private string _S_EMP_COI;
      private string _S_DAT_COI;
      private string _NVER_COI;
      private string _S_EMP_SAE;
      private string _S_DAT_SAE;
      private string _NVER_SAE;
      private string _CED_EMP;
      private string _CTA_IVA_PAGADO;
      private string _CTA_IVA_COBRADO;
      private string _CTA_IVA_X_PAGAR;
      private string _CTA_IVA_X_COBRAR;
      private string _RECLASIFICAR_IVA;
      private string _RUTA_INS_SAE;
      private string _RUTA_INS_COI;
      private int? _EMPRESA_SAE;
      private int? _EMPRESA_COI;
      private string _VER_SAE;
      private string _VER_COI;
      private string _DIRTRAB;
      private int? _PART_CONT_ELEC;
      private string _UUIDCLPV;
      private string _UUIDIVA;
      private string _UUIDBANCO;
      private string _UUIDTODAS;
      private string _RECPAGOLINEA;
      private string _PASS_CORREO;
      private string _SERVIDOR_CORREO;
      private int? _PUERTO;
      private string _USUARIO_CORREO;
      private int? _REQUIERE_AUT;
      private int? _CONEXION_SEGURA;
      private int? _PROVEEDOR;
      private string _MOSTRARTABLERO;

      /// <summary>
      /// obtener o establecer IDEMP
      /// </summary>
      [DataNames("IDEMP")]
      [SugarColumn(ColumnName = "idemp", ColumnDescription = "idemp", IsNullable = false)]
      public int IDEMP {get { return this._IDEMP; } set { this._IDEMP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOMBRE
      /// </summary>
      [DataNames("NOMBRE")]
      [SugarColumn(ColumnName = "nombre", ColumnDescription = "nombre", Length = 120)]
      public string NOMBRE {get { return this._NOMBRE; } set { this._NOMBRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CVL_ALTA
      /// </summary>
      [DataNames("CVL_ALTA")]
      [SugarColumn(ColumnName = "cvl_alta", ColumnDescription = "cvl_alta", Length = 7)]
      public string CVL_ALTA {get { return this._CVL_ALTA; } set { this._CVL_ALTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer COD_POS
      /// </summary>
      [DataNames("COD_POS")]
      [SugarColumn(ColumnName = "cod_pos", ColumnDescription = "cod_pos", Length = 5)]
      public string COD_POS {get { return this._COD_POS; } set { this._COD_POS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PASSW
      /// </summary>
      [DataNames("PASSW")]
      [SugarColumn(ColumnName = "passw", ColumnDescription = "passw", Length = 6)]
      public string PASSW {get { return this._PASSW; } set { this._PASSW = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FVIGENCIA
      /// </summary>
      [DataNames("FVIGENCIA")]
      [SugarColumn(ColumnName = "fvigencia", ColumnDescription = "fvigencia")]
      public DateTime? FVIGENCIA {get { return this._FVIGENCIA; } set { this._FVIGENCIA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DIREC
      /// </summary>
      [DataNames("DIREC")]
      [SugarColumn(ColumnName = "direc", ColumnDescription = "direc", Length = 60)]
      public string DIREC {get { return this._DIREC; } set { this._DIREC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer POBL
      /// </summary>
      [DataNames("POBL")]
      [SugarColumn(ColumnName = "pobl", ColumnDescription = "pobl", Length = 30)]
      public string POBL {get { return this._POBL; } set { this._POBL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFC_EMP
      /// </summary>
      [DataNames("RFC_EMP")]
      [SugarColumn(ColumnName = "rfc_emp", ColumnDescription = "rfc_emp", Length = 15)]
      public string RFC_EMP {get { return this._RFC_EMP; } set { this._RFC_EMP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BAN_INST
      /// </summary>
      [DataNames("BAN_INST")]
      [SugarColumn(ColumnName = "ban_inst", ColumnDescription = "ban_inst", Length = 1)]
      public string BAN_INST {get { return this._BAN_INST; } set { this._BAN_INST = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer EMP_COI
      /// </summary>
      [DataNames("EMP_COI")]
      [SugarColumn(ColumnName = "emp_coi", ColumnDescription = "emp_coi", Length = 1)]
      public string EMP_COI {get { return this._EMP_COI; } set { this._EMP_COI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer COI_LINEA
      /// </summary>
      [DataNames("COI_LINEA")]
      [SugarColumn(ColumnName = "coi_linea", ColumnDescription = "coi_linea", Length = 1, DefaultValue = "S")]
      public string COI_LINEA {get { return this._COI_LINEA; } set { this._COI_LINEA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ALT_COI
      /// </summary>
      [DataNames("ALT_COI")]
      [SugarColumn(ColumnName = "alt_coi", ColumnDescription = "alt_coi", Length = 1, DefaultValue = "N")]
      public string ALT_COI {get { return this._ALT_COI; } set { this._ALT_COI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_DEC
      /// </summary>
      [DataNames("NUM_DEC")]
      [SugarColumn(ColumnName = "num_dec", ColumnDescription = "num_dec", Length = 1, DefaultValue = "2")]
      public string NUM_DEC {get { return this._NUM_DEC; } set { this._NUM_DEC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer INT_SAE
      /// </summary>
      [DataNames("INT_SAE")]
      [SugarColumn(ColumnName = "int_sae", ColumnDescription = "int_sae", Length = 1, DefaultValue = "N")]
      public string INT_SAE {get { return this._INT_SAE; } set { this._INT_SAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer INT_COI
      /// </summary>
      [DataNames("INT_COI")]
      [SugarColumn(ColumnName = "int_coi", ColumnDescription = "int_coi", Length = 1, DefaultValue = "N")]
      public string INT_COI {get { return this._INT_COI; } set { this._INT_COI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SAE_CXP
      /// </summary>
      [DataNames("SAE_CXP")]
      [SugarColumn(ColumnName = "sae_cxp", ColumnDescription = "sae_cxp", Length = 1, DefaultValue = "S")]
      public string SAE_CXP {get { return this._SAE_CXP; } set { this._SAE_CXP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SAE_CXC
      /// </summary>
      [DataNames("SAE_CXC")]
      [SugarColumn(ColumnName = "sae_cxc", ColumnDescription = "sae_cxc", Length = 1, DefaultValue = "S")]
      public string SAE_CXC {get { return this._SAE_CXC; } set { this._SAE_CXC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer EMP_SAE
      /// </summary>
      [DataNames("EMP_SAE")]
      [SugarColumn(ColumnName = "emp_sae", ColumnDescription = "emp_sae", Length = 1)]
      public string EMP_SAE {get { return this._EMP_SAE; } set { this._EMP_SAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABO_CXP
      /// </summary>
      [DataNames("ABO_CXP")]
      [SugarColumn(ColumnName = "abo_cxp", ColumnDescription = "abo_cxp", Length = 1)]
      public string ABO_CXP {get { return this._ABO_CXP; } set { this._ABO_CXP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ANT_CXP
      /// </summary>
      [DataNames("ANT_CXP")]
      [SugarColumn(ColumnName = "ant_cxp", ColumnDescription = "ant_cxp", Length = 1)]
      public string ANT_CXP {get { return this._ANT_CXP; } set { this._ANT_CXP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABO_CXC
      /// </summary>
      [DataNames("ABO_CXC")]
      [SugarColumn(ColumnName = "abo_cxc", ColumnDescription = "abo_cxc", Length = 1)]
      public string ABO_CXC {get { return this._ABO_CXC; } set { this._ABO_CXC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ANT_CXC
      /// </summary>
      [DataNames("ANT_CXC")]
      [SugarColumn(ColumnName = "ant_cxc", ColumnDescription = "ant_cxc", Length = 1)]
      public string ANT_CXC {get { return this._ANT_CXC; } set { this._ANT_CXC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ACT_TRANS
      /// </summary>
      [DataNames("ACT_TRANS")]
      [SugarColumn(ColumnName = "act_trans", ColumnDescription = "act_trans", Length = 1, DefaultValue = "N")]
      public string ACT_TRANS {get { return this._ACT_TRANS; } set { this._ACT_TRANS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CPF_CXC
      /// </summary>
      [DataNames("CPF_CXC")]
      [SugarColumn(ColumnName = "cpf_cxc", ColumnDescription = "cpf_cxc", Length = 1)]
      public string CPF_CXC {get { return this._CPF_CXC; } set { this._CPF_CXC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MON_BASE
      /// </summary>
      [DataNames("MON_BASE")]
      [SugarColumn(ColumnName = "mon_base", ColumnDescription = "mon_base", Length = 1)]
      public string MON_BASE {get { return this._MON_BASE; } set { this._MON_BASE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer LIM_SGIRO
      /// </summary>
      [DataNames("LIM_SGIRO")]
      [SugarColumn(ColumnName = "lim_sgiro", ColumnDescription = "lim_sgiro", Length = 1, DefaultValue = "N")]
      public string LIM_SGIRO {get { return this._LIM_SGIRO; } set { this._LIM_SGIRO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer POL_FTO
      /// </summary>
      [DataNames("POL_FTO")]
      [SugarColumn(ColumnName = "pol_fto", ColumnDescription = "pol_fto", Length = 13)]
      public string POL_FTO {get { return this._POL_FTO; } set { this._POL_FTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOV_FTO
      /// </summary>
      [DataNames("MOV_FTO")]
      [SugarColumn(ColumnName = "mov_fto", ColumnDescription = "mov_fto", Length = 13)]
      public string MOV_FTO {get { return this._MOV_FTO; } set { this._MOV_FTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTA_IMPSTO
      /// </summary>
      [DataNames("CTA_IMPSTO")]
      [SugarColumn(ColumnName = "cta_impsto", ColumnDescription = "cta_impsto", Length = 21)]
      public string CTA_IMPSTO {get { return this._CTA_IMPSTO; } set { this._CTA_IMPSTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SUBDIR
      /// </summary>
      [DataNames("SUBDIR")]
      [SugarColumn(ColumnName = "subdir", ColumnDescription = "subdir", Length = 80)]
      public string SUBDIR {get { return this._SUBDIR; } set { this._SUBDIR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IMPUESTO
      /// </summary>
      [DataNames("IMPUESTO")]
      [SugarColumn(ColumnName = "impuesto", ColumnDescription = "impuesto", DefaultValue = 0)]
      public double? IMPUESTO {get { return this._IMPUESTO; } set { this._IMPUESTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer WIN_CASCADA
      /// </summary>
      [DataNames("WIN_CASCADA")]
      [SugarColumn(ColumnName = "win_cascada", ColumnDescription = "win_cascada", Length = 1)]
      public string WIN_CASCADA {get { return this._WIN_CASCADA; } set { this._WIN_CASCADA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SHOW_MONEDAS
      /// </summary>
      [DataNames("SHOW_MONEDAS")]
      [SugarColumn(ColumnName = "show_monedas", ColumnDescription = "show_monedas", Length = 1)]
      public string SHOW_MONEDAS {get { return this._SHOW_MONEDAS; } set { this._SHOW_MONEDAS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TC_CXP
      /// </summary>
      [DataNames("TC_CXP")]
      [SugarColumn(ColumnName = "tc_cxp", ColumnDescription = "tc_cxp", Length = 1)]
      public string TC_CXP {get { return this._TC_CXP; } set { this._TC_CXP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TC_CXC
      /// </summary>
      [DataNames("TC_CXC")]
      [SugarColumn(ColumnName = "tc_cxc", ColumnDescription = "tc_cxc", Length = 1)]
      public string TC_CXC {get { return this._TC_CXC; } set { this._TC_CXC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SAEESMULTIMON
      /// </summary>
      [DataNames("SAEESMULTIMON")]
      [SugarColumn(ColumnName = "saeesmultimon", ColumnDescription = "saeesmultimon", Length = 1)]
      public string SAEESMULTIMON {get { return this._SAEESMULTIMON; } set { this._SAEESMULTIMON = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DESG_CTASCOI
      /// </summary>
      [DataNames("DESG_CTASCOI")]
      [SugarColumn(ColumnName = "desg_ctascoi", ColumnDescription = "desg_ctascoi", Length = 1)]
      public string DESG_CTASCOI {get { return this._DESG_CTASCOI; } set { this._DESG_CTASCOI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CAPRFC_IVA
      /// </summary>
      [DataNames("CAPRFC_IVA")]
      [SugarColumn(ColumnName = "caprfc_iva", ColumnDescription = "caprfc_iva", Length = 1)]
      public string CAPRFC_IVA {get { return this._CAPRFC_IVA; } set { this._CAPRFC_IVA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer COI_SOLO_CHEQ
      /// </summary>
      [DataNames("COI_SOLO_CHEQ")]
      [SugarColumn(ColumnName = "coi_solo_cheq", ColumnDescription = "coi_solo_cheq", Length = 1)]
      public string COI_SOLO_CHEQ {get { return this._COI_SOLO_CHEQ; } set { this._COI_SOLO_CHEQ = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer S_EMP_COI
      /// </summary>
      [DataNames("S_EMP_COI")]
      [SugarColumn(ColumnName = "s_emp_coi", ColumnDescription = "s_emp_coi", Length = 80)]
      public string S_EMP_COI {get { return this._S_EMP_COI; } set { this._S_EMP_COI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer S_DAT_COI
      /// </summary>
      [DataNames("S_DAT_COI")]
      [SugarColumn(ColumnName = "s_dat_coi", ColumnDescription = "s_dat_coi", Length = 80)]
      public string S_DAT_COI {get { return this._S_DAT_COI; } set { this._S_DAT_COI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NVER_COI
      /// </summary>
      [DataNames("NVER_COI")]
      [SugarColumn(ColumnName = "nver_coi", ColumnDescription = "nver_coi", Length = 1)]
      public string NVER_COI {get { return this._NVER_COI; } set { this._NVER_COI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer S_EMP_SAE
      /// </summary>
      [DataNames("S_EMP_SAE")]
      [SugarColumn(ColumnName = "s_emp_sae", ColumnDescription = "s_emp_sae", Length = 80)]
      public string S_EMP_SAE {get { return this._S_EMP_SAE; } set { this._S_EMP_SAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer S_DAT_SAE
      /// </summary>
      [DataNames("S_DAT_SAE")]
      [SugarColumn(ColumnName = "s_dat_sae", ColumnDescription = "s_dat_sae", Length = 80)]
      public string S_DAT_SAE {get { return this._S_DAT_SAE; } set { this._S_DAT_SAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NVER_SAE
      /// </summary>
      [DataNames("NVER_SAE")]
      [SugarColumn(ColumnName = "nver_sae", ColumnDescription = "nver_sae", Length = 1)]
      public string NVER_SAE {get { return this._NVER_SAE; } set { this._NVER_SAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CED_EMP
      /// </summary>
      [DataNames("CED_EMP")]
      [SugarColumn(ColumnName = "ced_emp", ColumnDescription = "ced_emp", Length = 30)]
      public string CED_EMP {get { return this._CED_EMP; } set { this._CED_EMP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTA_IVA_PAGADO
      /// </summary>
      [DataNames("CTA_IVA_PAGADO")]
      [SugarColumn(ColumnName = "cta_iva_pagado", ColumnDescription = "cta_iva_pagado", Length = 28)]
      public string CTA_IVA_PAGADO {get { return this._CTA_IVA_PAGADO; } set { this._CTA_IVA_PAGADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTA_IVA_COBRADO
      /// </summary>
      [DataNames("CTA_IVA_COBRADO")]
      [SugarColumn(ColumnName = "cta_iva_cobrado", ColumnDescription = "cta_iva_cobrado", Length = 28)]
      public string CTA_IVA_COBRADO {get { return this._CTA_IVA_COBRADO; } set { this._CTA_IVA_COBRADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTA_IVA_X_PAGAR
      /// </summary>
      [DataNames("CTA_IVA_X_PAGAR")]
      [SugarColumn(ColumnName = "cta_iva_x_pagar", ColumnDescription = "cta_iva_x_pagar", Length = 28)]
      public string CTA_IVA_X_PAGAR {get { return this._CTA_IVA_X_PAGAR; } set { this._CTA_IVA_X_PAGAR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTA_IVA_X_COBRAR
      /// </summary>
      [DataNames("CTA_IVA_X_COBRAR")]
      [SugarColumn(ColumnName = "cta_iva_x_cobrar", ColumnDescription = "cta_iva_x_cobrar", Length = 28)]
      public string CTA_IVA_X_COBRAR {get { return this._CTA_IVA_X_COBRAR; } set { this._CTA_IVA_X_COBRAR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RECLASIFICAR_IVA
      /// </summary>
      [DataNames("RECLASIFICAR_IVA")]
      [SugarColumn(ColumnName = "reclasificar_iva", ColumnDescription = "reclasificar_iva", Length = 1)]
      public string RECLASIFICAR_IVA {get { return this._RECLASIFICAR_IVA; } set { this._RECLASIFICAR_IVA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RUTA_INS_SAE
      /// </summary>
      [DataNames("RUTA_INS_SAE")]
      [SugarColumn(ColumnName = "ruta_ins_sae", ColumnDescription = "ruta_ins_sae", Length = 256)]
      public string RUTA_INS_SAE {get { return this._RUTA_INS_SAE; } set { this._RUTA_INS_SAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RUTA_INS_COI
      /// </summary>
      [DataNames("RUTA_INS_COI")]
      [SugarColumn(ColumnName = "ruta_ins_coi", ColumnDescription = "ruta_ins_coi", Length = 256)]
      public string RUTA_INS_COI {get { return this._RUTA_INS_COI; } set { this._RUTA_INS_COI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer EMPRESA_SAE
      /// </summary>
      [DataNames("EMPRESA_SAE")]
      [SugarColumn(ColumnName = "empresa_sae", ColumnDescription = "empresa_sae")]
      public int? EMPRESA_SAE {get { return this._EMPRESA_SAE; } set { this._EMPRESA_SAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer EMPRESA_COI
      /// </summary>
      [DataNames("EMPRESA_COI")]
      [SugarColumn(ColumnName = "empresa_coi", ColumnDescription = "empresa_coi")]
      public int? EMPRESA_COI {get { return this._EMPRESA_COI; } set { this._EMPRESA_COI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer VER_SAE
      /// </summary>
      [DataNames("VER_SAE")]
      [SugarColumn(ColumnName = "ver_sae", ColumnDescription = "ver_sae", Length = 3)]
      public string VER_SAE {get { return this._VER_SAE; } set { this._VER_SAE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer VER_COI
      /// </summary>
      [DataNames("VER_COI")]
      [SugarColumn(ColumnName = "ver_coi", ColumnDescription = "ver_coi", Length = 3)]
      public string VER_COI {get { return this._VER_COI; } set { this._VER_COI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DIRTRAB
      /// </summary>
      [DataNames("DIRTRAB")]
      [SugarColumn(ColumnName = "dirtrab", ColumnDescription = "dirtrab", Length = 255)]
      public string DIRTRAB {get { return this._DIRTRAB; } set { this._DIRTRAB = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PART_CONT_ELEC
      /// </summary>
      [DataNames("PART_CONT_ELEC")]
      [SugarColumn(ColumnName = "part_cont_elec", ColumnDescription = "part_cont_elec", DefaultValue = 0)]
      public int? PART_CONT_ELEC {get { return this._PART_CONT_ELEC; } set { this._PART_CONT_ELEC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer UUIDCLPV
      /// </summary>
      [DataNames("UUIDCLPV")]
      [SugarColumn(ColumnName = "uuidclpv", ColumnDescription = "uuidclpv", Length = 1, DefaultValue = "N")]
      public string UUIDCLPV {get { return this._UUIDCLPV; } set { this._UUIDCLPV = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer UUIDIVA
      /// </summary>
      [DataNames("UUIDIVA")]
      [SugarColumn(ColumnName = "uuidiva", ColumnDescription = "uuidiva", Length = 1, DefaultValue = "N")]
      public string UUIDIVA {get { return this._UUIDIVA; } set { this._UUIDIVA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer UUIDBANCO
      /// </summary>
      [DataNames("UUIDBANCO")]
      [SugarColumn(ColumnName = "uuidbanco", ColumnDescription = "uuidbanco", Length = 1, DefaultValue = "N")]
      public string UUIDBANCO {get { return this._UUIDBANCO; } set { this._UUIDBANCO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer UUIDTODAS
      /// </summary>
      [DataNames("UUIDTODAS")]
      [SugarColumn(ColumnName = "uuidtodas", ColumnDescription = "uuidtodas", Length = 1, DefaultValue = "N")]
      public string UUIDTODAS {get { return this._UUIDTODAS; } set { this._UUIDTODAS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RECPAGOLINEA
      /// </summary>
      [DataNames("RECPAGOLINEA")]
      [SugarColumn(ColumnName = "recpagolinea", ColumnDescription = "recpagolinea", Length = 1)]
      public string RECPAGOLINEA {get { return this._RECPAGOLINEA; } set { this._RECPAGOLINEA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PASS_CORREO
      /// </summary>
      [DataNames("PASS_CORREO")]
      [SugarColumn(ColumnName = "pass_correo", ColumnDescription = "pass_correo", Length = 100)]
      public string PASS_CORREO {get { return this._PASS_CORREO; } set { this._PASS_CORREO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SERVIDOR_CORREO
      /// </summary>
      [DataNames("SERVIDOR_CORREO")]
      [SugarColumn(ColumnName = "servidor_correo", ColumnDescription = "servidor_correo", Length = 100)]
      public string SERVIDOR_CORREO {get { return this._SERVIDOR_CORREO; } set { this._SERVIDOR_CORREO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PUERTO
      /// </summary>
      [DataNames("PUERTO")]
      [SugarColumn(ColumnName = "puerto", ColumnDescription = "puerto")]
      public int? PUERTO {get { return this._PUERTO; } set { this._PUERTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer USUARIO_CORREO
      /// </summary>
      [DataNames("USUARIO_CORREO")]
      [SugarColumn(ColumnName = "usuario_correo", ColumnDescription = "usuario_correo", Length = 100)]
      public string USUARIO_CORREO {get { return this._USUARIO_CORREO; } set { this._USUARIO_CORREO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer REQUIERE_AUT
      /// </summary>
      [DataNames("REQUIERE_AUT")]
      [SugarColumn(ColumnName = "requiere_aut", ColumnDescription = "requiere_aut")]
      public int? REQUIERE_AUT {get { return this._REQUIERE_AUT; } set { this._REQUIERE_AUT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CONEXION_SEGURA
      /// </summary>
      [DataNames("CONEXION_SEGURA")]
      [SugarColumn(ColumnName = "conexion_segura", ColumnDescription = "conexion_segura")]
      public int? CONEXION_SEGURA {get { return this._CONEXION_SEGURA; } set { this._CONEXION_SEGURA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PROVEEDOR
      /// </summary>
      [DataNames("PROVEEDOR")]
      [SugarColumn(ColumnName = "proveedor", ColumnDescription = "proveedor")]
      public int? PROVEEDOR {get { return this._PROVEEDOR; } set { this._PROVEEDOR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MOSTRARTABLERO
      /// </summary>
      [DataNames("MOSTRARTABLERO")]
      [SugarColumn(ColumnName = "mostrartablero", ColumnDescription = "mostrartablero", Length = 1)]
      public string MOSTRARTABLERO {get { return this._MOSTRARTABLERO; } set { this._MOSTRARTABLERO = value; this.OnPropertyChanged(); }}
   }
}
