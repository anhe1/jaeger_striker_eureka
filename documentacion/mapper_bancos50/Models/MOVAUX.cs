﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///MOVAUX
   ///</summary>
   [SugarTable("movaux")]
   public partial class MOVAUX
   {
      public MOVAUX(){
      }

      private int _NUM_REG;
      private int? _NUM_CTA;
      private string _REFERENCIA1;
      private string _REFERENCIA2;
      private DateTime _FECHA;
      private double? _ABONO;
      private double? _CARGO;
      private string _RFC;
      private int? _REVISADO;

      /// <summary>
      /// obtener o establecer NUM_REG
      /// </summary>
      [DataNames("NUM_REG")]
      [SugarColumn(ColumnName = "num_reg", ColumnDescription = "num_reg", IsNullable = false)]
      public int NUM_REG {get { return this._NUM_REG; } set { this._NUM_REG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_CTA
      /// </summary>
      [DataNames("NUM_CTA")]
      [SugarColumn(ColumnName = "num_cta", ColumnDescription = "num_cta")]
      public int? NUM_CTA {get { return this._NUM_CTA; } set { this._NUM_CTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer REFERENCIA1
      /// </summary>
      [DataNames("REFERENCIA1")]
      [SugarColumn(ColumnName = "referencia1", ColumnDescription = "referencia1", Length = 256)]
      public string REFERENCIA1 {get { return this._REFERENCIA1; } set { this._REFERENCIA1 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer REFERENCIA2
      /// </summary>
      [DataNames("REFERENCIA2")]
      [SugarColumn(ColumnName = "referencia2", ColumnDescription = "referencia2", Length = 256)]
      public string REFERENCIA2 {get { return this._REFERENCIA2; } set { this._REFERENCIA2 = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHA
      /// </summary>
      [DataNames("FECHA")]
      [SugarColumn(ColumnName = "fecha", ColumnDescription = "fecha", IsNullable = false)]
      public DateTime FECHA {get { return this._FECHA; } set { this._FECHA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ABONO
      /// </summary>
      [DataNames("ABONO")]
      [SugarColumn(ColumnName = "abono", ColumnDescription = "abono", DefaultValue = 0)]
      public double? ABONO {get { return this._ABONO; } set { this._ABONO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CARGO
      /// </summary>
      [DataNames("CARGO")]
      [SugarColumn(ColumnName = "cargo", ColumnDescription = "cargo", DefaultValue = 0)]
      public double? CARGO {get { return this._CARGO; } set { this._CARGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFC
      /// </summary>
      [DataNames("RFC")]
      [SugarColumn(ColumnName = "rfc", ColumnDescription = "rfc", Length = 256)]
      public string RFC {get { return this._RFC; } set { this._RFC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer REVISADO
      /// </summary>
      [DataNames("REVISADO")]
      [SugarColumn(ColumnName = "revisado", ColumnDescription = "revisado", DefaultValue = 0)]
      public int? REVISADO {get { return this._REVISADO; } set { this._REVISADO = value; this.OnPropertyChanged(); }}
   
}
