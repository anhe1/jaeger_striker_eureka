﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///CONTROL
   ///</summary>
   [SugarTable("control")]
   public partial class CONTROL
   {
      public CONTROL(){
      }

      private int? _CTMONED;
      private int? _CTCTAS;
      private int? _CTCOMD;
      private int? _CTBENEFICIARIOS;
      private int? _CTFORMPAG;
      private int? _CTCTECONC;
      private int? _CTTRANSFERENCIA;
      private int? _VER_BASDAT;
      private int? _NUM_EMP;

      /// <summary>
      /// obtener o establecer CTMONED
      /// </summary>
      [DataNames("CTMONED")]
      [SugarColumn(ColumnName = "ctmoned", ColumnDescription = "ctmoned", DefaultValue = 0)]
      public int? CTMONED {get { return this._CTMONED; } set { this._CTMONED = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTCTAS
      /// </summary>
      [DataNames("CTCTAS")]
      [SugarColumn(ColumnName = "ctctas", ColumnDescription = "ctctas", DefaultValue = 0)]
      public int? CTCTAS {get { return this._CTCTAS; } set { this._CTCTAS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTCOMD
      /// </summary>
      [DataNames("CTCOMD")]
      [SugarColumn(ColumnName = "ctcomd", ColumnDescription = "ctcomd", DefaultValue = 0)]
      public int? CTCOMD {get { return this._CTCOMD; } set { this._CTCOMD = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTBENEFICIARIOS
      /// </summary>
      [DataNames("CTBENEFICIARIOS")]
      [SugarColumn(ColumnName = "ctbeneficiarios", ColumnDescription = "ctbeneficiarios", DefaultValue = 0)]
      public int? CTBENEFICIARIOS {get { return this._CTBENEFICIARIOS; } set { this._CTBENEFICIARIOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTFORMPAG
      /// </summary>
      [DataNames("CTFORMPAG")]
      [SugarColumn(ColumnName = "ctformpag", ColumnDescription = "ctformpag", DefaultValue = 0)]
      public int? CTFORMPAG {get { return this._CTFORMPAG; } set { this._CTFORMPAG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTCTECONC
      /// </summary>
      [DataNames("CTCTECONC")]
      [SugarColumn(ColumnName = "ctcteconc", ColumnDescription = "ctcteconc", DefaultValue = 0)]
      public int? CTCTECONC {get { return this._CTCTECONC; } set { this._CTCTECONC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTTRANSFERENCIA
      /// </summary>
      [DataNames("CTTRANSFERENCIA")]
      [SugarColumn(ColumnName = "cttransferencia", ColumnDescription = "cttransferencia", DefaultValue = 0)]
      public int? CTTRANSFERENCIA {get { return this._CTTRANSFERENCIA; } set { this._CTTRANSFERENCIA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer VER_BASDAT
      /// </summary>
      [DataNames("VER_BASDAT")]
      [SugarColumn(ColumnName = "ver_basdat", ColumnDescription = "ver_basdat", DefaultValue = 0)]
      public int? VER_BASDAT {get { return this._VER_BASDAT; } set { this._VER_BASDAT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_EMP
      /// </summary>
      [DataNames("NUM_EMP")]
      [SugarColumn(ColumnName = "num_emp", ColumnDescription = "num_emp", DefaultValue = 0)]
      public int? NUM_EMP {get { return this._NUM_EMP; } set { this._NUM_EMP = value; this.OnPropertyChanged(); }}
   }
}
