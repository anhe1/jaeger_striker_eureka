﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("poldinamica")]
    public partial class POLDINAMICA
    {
           public POLDINAMICA(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("TITULO")] 
           [SugarColumn(ColumnName="titulo")]ColumnDescription 
        public string TITULO {get { return this._TITULO; } set { this._TITULO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("RUTAARCHIVO")] 
           [SugarColumn(ColumnName="rutaarchivo")]ColumnDescription 
        public string RUTAARCHIVO {get { return this._RUTAARCHIVO; } set { this._RUTAARCHIVO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPO")] 
           [SugarColumn(ColumnName="tipo")]ColumnDescription 
        public string TIPO {get { return this._TIPO; } set { this._TIPO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CLAS_USR")] 
           [SugarColumn(ColumnName="clas_usr")]ColumnDescription 
        public string CLAS_USR {get { return this._CLAS_USR; } set { this._CLAS_USR = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL736")] 
           [SugarColumn(ColumnName="trial736")]ColumnDescription 
        public string TRIAL736 {get { return this._TRIAL736; } set { this._TRIAL736 = value; this.OnPropertyChanged(); }}
    }
}
