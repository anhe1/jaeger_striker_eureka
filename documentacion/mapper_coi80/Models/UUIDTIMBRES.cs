﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("uuidtimbres")]
    public partial class UUIDTIMBRES
    {
           public UUIDTIMBRES(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUMREG")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="numreg")]ColumnDescription 
        public int NUMREG {get { return this._NUMREG; } set { this._NUMREG = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("UUIDTIMBRE")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="uuidtimbre")]ColumnDescription 
        public string UUIDTIMBRE {get { return this._UUIDTIMBRE; } set { this._UUIDTIMBRE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MONTO")] 
           [SugarColumn(ColumnName="monto")]ColumnDescription 
        public double? MONTO {get { return this._MONTO; } set { this._MONTO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("SERIE")] 
           [SugarColumn(ColumnName="serie")]ColumnDescription 
        public string SERIE {get { return this._SERIE; } set { this._SERIE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FOLIO")] 
           [SugarColumn(ColumnName="folio")]ColumnDescription 
        public string FOLIO {get { return this._FOLIO; } set { this._FOLIO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RFCEMISOR")] 
           [SugarColumn(ColumnName="rfcemisor")]ColumnDescription 
        public string RFCEMISOR {get { return this._RFCEMISOR; } set { this._RFCEMISOR = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RFCRECEPTOR")] 
           [SugarColumn(ColumnName="rfcreceptor")]ColumnDescription 
        public string RFCRECEPTOR {get { return this._RFCRECEPTOR; } set { this._RFCRECEPTOR = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ORDEN")] 
           [SugarColumn(ColumnName="orden")]ColumnDescription 
        public int? ORDEN {get { return this._ORDEN; } set { this._ORDEN = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FECHA")] 
           [SugarColumn(ColumnName="fecha")]ColumnDescription 
        public DateTime? FECHA {get { return this._FECHA; } set { this._FECHA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPOCOMPROBANTE")] 
           [SugarColumn(ColumnName="tipocomprobante")]ColumnDescription 
        public int? TIPOCOMPROBANTE {get { return this._TIPOCOMPROBANTE; } set { this._TIPOCOMPROBANTE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPOCAMBIO")] 
           [SugarColumn(ColumnName="tipocambio")]ColumnDescription 
        public double? TIPOCAMBIO {get { return this._TIPOCAMBIO; } set { this._TIPOCAMBIO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("VERSIONCFDI")] 
           [SugarColumn(ColumnName="versioncfdi")]ColumnDescription 
        public string VERSIONCFDI {get { return this._VERSIONCFDI; } set { this._VERSIONCFDI = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MONEDA")] 
           [SugarColumn(ColumnName="moneda")]ColumnDescription 
        public string MONEDA {get { return this._MONEDA; } set { this._MONEDA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL739")] 
           [SugarColumn(ColumnName="trial739")]ColumnDescription 
        public string TRIAL739 {get { return this._TRIAL739; } set { this._TRIAL739 = value; this.OnPropertyChanged(); }}
    }
}
