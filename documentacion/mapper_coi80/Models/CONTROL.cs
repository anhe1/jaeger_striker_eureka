﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///CONTROL
   ///</summary>
   [SugarTable("control")]
   public partial class CONTROL
   {
      public CONTROL(){
      }

      private int? _CTCONCEPTOS;
      private int? _CTDEPTOS;
      private int? _CTMONEDAS;
      private int? _CTRANGOS;
      private int? _CTTIPACTIV;
      private int? _CTREGPOL;
      private int? _VERBASDAT;
      private int? _CCOSTOS;
      private int? _CGRUPOS;
      private int? _CTCONCIETU;
      private int? _CTCONCIVAT;
      private int? _CTCONCIVAA;
      private int? _CTINFADIPAR;
      private int? _CTUUIDCOMP;
      private int? _CTIDOTROSIMP;
      private int? _CTIDOTROSIMPING;

      /// <summary>
      /// obtener o establecer CTCONCEPTOS
      /// </summary>
      [DataNames("CTCONCEPTOS")] 
      [SugarColumn(ColumnName = "ctconceptos", ColumnDescription = "Numero de control para conceptos")]
      public int? CTCONCEPTOS {get { return this._CTCONCEPTOS; } set { this._CTCONCEPTOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTDEPTOS
      /// </summary>
      [DataNames("CTDEPTOS")] 
      [SugarColumn(ColumnName = "ctdeptos", ColumnDescription = "Numero de control para deptos")]
      public int? CTDEPTOS {get { return this._CTDEPTOS; } set { this._CTDEPTOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTMONEDAS
      /// </summary>
      [DataNames("CTMONEDAS")] 
      [SugarColumn(ColumnName = "ctmonedas", ColumnDescription = "Numero de control para monedas")]
      public int? CTMONEDAS {get { return this._CTMONEDAS; } set { this._CTMONEDAS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTRANGOS
      /// </summary>
      [DataNames("CTRANGOS")] 
      [SugarColumn(ColumnName = "ctrangos", ColumnDescription = "Numero de control para rangos")]
      public int? CTRANGOS {get { return this._CTRANGOS; } set { this._CTRANGOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTTIPACTIV
      /// </summary>
      [DataNames("CTTIPACTIV")] 
      [SugarColumn(ColumnName = "cttipactiv", ColumnDescription = "Numero de control para activos")]
      public int? CTTIPACTIV {get { return this._CTTIPACTIV; } set { this._CTTIPACTIV = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTREGPOL
      /// </summary>
      [DataNames("CTREGPOL")] 
      [SugarColumn(ColumnName = "ctregpol", ColumnDescription = "Numero de control para polizas")]
      public int? CTREGPOL {get { return this._CTREGPOL; } set { this._CTREGPOL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer VERBASDAT
      /// </summary>
      [DataNames("VERBASDAT")] 
      [SugarColumn(ColumnName = "verbasdat", ColumnDescription = "Numero de control para la version del la DB")]
      public int? VERBASDAT {get { return this._VERBASDAT; } set { this._VERBASDAT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CCOSTOS
      /// </summary>
      [DataNames("CCOSTOS")] 
      [SugarColumn(ColumnName = "ccostos", ColumnDescription = "Numero de control para centro de costos")]
      public int? CCOSTOS {get { return this._CCOSTOS; } set { this._CCOSTOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CGRUPOS
      /// </summary>
      [DataNames("CGRUPOS")] 
      [SugarColumn(ColumnName = "cgrupos", ColumnDescription = "Numero de control para grupos")]
      public int? CGRUPOS {get { return this._CGRUPOS; } set { this._CGRUPOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTCONCIETU
      /// </summary>
      [DataNames("CTCONCIETU")] 
      [SugarColumn(ColumnName = "ctconcietu", ColumnDescription = "Numero de control para conceptos IETU")]
      public int? CTCONCIETU {get { return this._CTCONCIETU; } set { this._CTCONCIETU = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTCONCIVAT
      /// </summary>
      [DataNames("CTCONCIVAT")] 
      [SugarColumn(ColumnName = "ctconcivat", ColumnDescription = "Numero de control para conceptos IVA trasladado")]
      public int? CTCONCIVAT {get { return this._CTCONCIVAT; } set { this._CTCONCIVAT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTCONCIVAA
      /// </summary>
      [DataNames("CTCONCIVAA")] 
      [SugarColumn(ColumnName = "ctconcivaa", ColumnDescription = "Numero de control para conceptos IVA acreditable")]
      public int? CTCONCIVAA {get { return this._CTCONCIVAA; } set { this._CTCONCIVAA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTINFADIPAR
      /// </summary>
      [DataNames("CTINFADIPAR")] 
      [SugarColumn(ColumnName = "ctinfadipar", ColumnDescription = "Numero de control para informacion de pago")]
      public int? CTINFADIPAR {get { return this._CTINFADIPAR; } set { this._CTINFADIPAR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTUUIDCOMP
      /// </summary>
      [DataNames("CTUUIDCOMP")] 
      [SugarColumn(ColumnName = "ctuuidcomp", ColumnDescription = "Numero de control para informacion de comprobantes")]
      public int? CTUUIDCOMP {get { return this._CTUUIDCOMP; } set { this._CTUUIDCOMP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTIDOTROSIMP
      /// </summary>
      [DataNames("CTIDOTROSIMP")] 
      [SugarColumn(ColumnName = "ctidotrosimp", ColumnDescription = "Numero de control para operaciones con terceros")]
      public int? CTIDOTROSIMP {get { return this._CTIDOTROSIMP; } set { this._CTIDOTROSIMP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTIDOTROSIMPING
      /// </summary>
      [DataNames("CTIDOTROSIMPING")] 
      [SugarColumn(ColumnName = "ctidotrosimping", ColumnDescription = "Numero de control para ingresos cobrados")]
      public int? CTIDOTROSIMPING {get { return this._CTIDOTROSIMPING; } set { this._CTIDOTROSIMPING = value; this.OnPropertyChanged(); }}
   }
}
