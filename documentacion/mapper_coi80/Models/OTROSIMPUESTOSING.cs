﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("otrosimpuestosing")]
    public partial class OTROSIMPUESTOSING
    {
           public OTROSIMPUESTOSING(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("IDOPEIET")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="idopeiet")]ColumnDescription 
        public int IDOPEIET {get { return this._IDOPEIET; } set { this._IDOPEIET = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUMREG")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="numreg")]ColumnDescription 
        public int NUMREG {get { return this._NUMREG; } set { this._NUMREG = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NOMBRE")] 
           [SugarColumn(ColumnName="nombre")]ColumnDescription 
        public string NOMBRE {get { return this._NOMBRE; } set { this._NOMBRE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TASA")] 
           [SugarColumn(ColumnName="tasa")]ColumnDescription 
        public double? TASA {get { return this._TASA; } set { this._TASA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MONTO")] 
           [SugarColumn(ColumnName="monto")]ColumnDescription 
        public double? MONTO {get { return this._MONTO; } set { this._MONTO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("TIPO")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="tipo")]ColumnDescription 
        public int TIPO {get { return this._TIPO; } set { this._TIPO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("BASE")] 
           [SugarColumn(ColumnName="base")]ColumnDescription 
        public double? BASE {get { return this._BASE; } set { this._BASE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("TIPOIMPUESTO")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="tipoimpuesto")]ColumnDescription 
        public int TIPOIMPUESTO {get { return this._TIPOIMPUESTO; } set { this._TIPOIMPUESTO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("APLICAIC")] 
           [SugarColumn(ColumnName="aplicaic")]ColumnDescription 
        public int? APLICAIC {get { return this._APLICAIC; } set { this._APLICAIC = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("TIPOFACTOR")] 
           [SugarColumn(ColumnName="tipofactor")]ColumnDescription 
        public string TIPOFACTOR {get { return this._TIPOFACTOR; } set { this._TIPOFACTOR = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL732")] 
           [SugarColumn(ColumnName="trial732")]ColumnDescription 
        public string TRIAL732 {get { return this._TRIAL732; } set { this._TRIAL732 = value; this.OnPropertyChanged(); }}
    }
}
