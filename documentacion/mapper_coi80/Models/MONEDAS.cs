﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("monedas")]
    public partial class MONEDAS
    {
           public MONEDAS(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("MONEDA")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="moneda")]ColumnDescription 
        public int MONEDA {get { return this._MONEDA; } set { this._MONEDA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NOMBRE")] 
           [SugarColumn(ColumnName="nombre")]ColumnDescription 
        public string NOMBRE {get { return this._NOMBRE; } set { this._NOMBRE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PREFIJO")] 
           [SugarColumn(ColumnName="prefijo")]ColumnDescription 
        public string PREFIJO {get { return this._PREFIJO; } set { this._PREFIJO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FECHA")] 
           [SugarColumn(ColumnName="fecha")]ColumnDescription 
        public DateTime? FECHA {get { return this._FECHA; } set { this._FECHA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPOCAMBIO")] 
           [SugarColumn(ColumnName="tipocambio")]ColumnDescription 
        public double? TIPOCAMBIO {get { return this._TIPOCAMBIO; } set { this._TIPOCAMBIO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MONTO")] 
           [SugarColumn(ColumnName="monto")]ColumnDescription 
        public double? MONTO {get { return this._MONTO; } set { this._MONTO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DESPAUTO")] 
           [SugarColumn(ColumnName="despauto")]ColumnDescription 
        public string DESPAUTO {get { return this._DESPAUTO; } set { this._DESPAUTO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CLASSAT")] 
           [SugarColumn(ColumnName="classat")]ColumnDescription 
        public string CLASSAT {get { return this._CLASSAT; } set { this._CLASSAT = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL732")] 
           [SugarColumn(ColumnName="trial732")]ColumnDescription 
        public string TRIAL732 {get { return this._TRIAL732; } set { this._TRIAL732 = value; this.OnPropertyChanged(); }}
    }
}
