﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///DOCTOSFISCALES
   ///</summary>
   [SugarTable("doctosfiscales")]
   public partial class DOCTOSFISCALES
   {
      public DOCTOSFISCALES(){
      }

      private string _UUIDTIMBRECFDI;
      private string _RUTAARCHIVO;
      private double _TOTAL;
      private double _SUBTOTAL;
      private string _RFCEMISOR;
      private string _RFCRECEPTOR;
      private DateTime _FECHAEMISION;
      private string _SERIE;
      private string _FOLIO;
      private string _VERSIONCFDI;
      private string _LUGAREXPEDICION;
      private string _MONEDA;
      private string _NUMCTAPAGO;
      private double? _TIPOCAMBIO;
      private double? _DESCUENTO;
      private string _FORMAPAGO;
      private string _METODOPAGO;
      private double? _TOTALIMPUESTOSTRASLADADOS;
      private double? _TOTALIMPUESTOSRETENIDOS;
      private string _NOMBREEMISOR;
      private string _NOMBRERECEPTOR;
      private string _REGIMENFISCAL;
      private string _USOCFDI;
      private string _SELLOCFDI;
      private string _NOCERTIFICADO;
      private string _NOCERTIFICADOTIMBRE;
      private string _CONDICIONESDEPAGO;
      private string _TIPODECOMPROBANTE;
      private string _XML;

      /// <summary>
      /// obtener o establecer UUIDTIMBRECFDI
      /// </summary>
      [DataNames("UUIDTIMBRECFDI")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "uuidtimbrecfdi", ColumnDescription = "UUID del nodo TimbreFiscalDigital del CFDI", Length = 36)]
      public string UUIDTIMBRECFDI {get { return this._UUIDTIMBRECFDI; } set { this._UUIDTIMBRECFDI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RUTAARCHIVO
      /// </summary>
      [DataNames("RUTAARCHIVO")]
      [SugarColumn(ColumnName = "rutaarchivo", ColumnDescription = "Ruta del archivo", Length = 512)]
      public string RUTAARCHIVO {get { return this._RUTAARCHIVO; } set { this._RUTAARCHIVO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TOTAL
      /// </summary>
      [DataNames("TOTAL")]
      [SugarColumn(ColumnName = "total", ColumnDescription = "Total", IsNullable = false)]
      public double TOTAL {get { return this._TOTAL; } set { this._TOTAL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SUBTOTAL
      /// </summary>
      [DataNames("SUBTOTAL")]
      [SugarColumn(ColumnName = "subtotal", ColumnDescription = "Subtotal", IsNullable = false)]
      public double SUBTOTAL {get { return this._SUBTOTAL; } set { this._SUBTOTAL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFCEMISOR
      /// </summary>
      [DataNames("RFCEMISOR")]
      [SugarColumn(ColumnName = "rfcemisor", ColumnDescription = "RFC del emisor", IsNullable = false, Length = 36)]
      public string RFCEMISOR {get { return this._RFCEMISOR; } set { this._RFCEMISOR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFCRECEPTOR
      /// </summary>
      [DataNames("RFCRECEPTOR")]
      [SugarColumn(ColumnName = "rfcreceptor", ColumnDescription = "RFC del receptor", IsNullable = false, Length = 36)]
      public string RFCRECEPTOR {get { return this._RFCRECEPTOR; } set { this._RFCRECEPTOR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHAEMISION
      /// </summary>
      [DataNames("FECHAEMISION")]
      [SugarColumn(ColumnName = "fechaemision", ColumnDescription = "Fecha de emision del documento", IsNullable = false)]
      public DateTime FECHAEMISION {get { return this._FECHAEMISION; } set { this._FECHAEMISION = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SERIE
      /// </summary>
      [DataNames("SERIE")]
      [SugarColumn(ColumnName = "serie", ColumnDescription = "Serie del documento", Length = 100)]
      public string SERIE {get { return this._SERIE; } set { this._SERIE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FOLIO
      /// </summary>
      [DataNames("FOLIO")]
      [SugarColumn(ColumnName = "folio", ColumnDescription = "Folio del documento", Length = 100)]
      public string FOLIO {get { return this._FOLIO; } set { this._FOLIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer VERSIONCFDI
      /// </summary>
      [DataNames("VERSIONCFDI")]
      [SugarColumn(ColumnName = "versioncfdi", ColumnDescription = "Version del documento", IsNullable = false, Length = 6)]
      public string VERSIONCFDI {get { return this._VERSIONCFDI; } set { this._VERSIONCFDI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer LUGAREXPEDICION
      /// </summary>
      [DataNames("LUGAREXPEDICION")]
      [SugarColumn(ColumnName = "lugarexpedicion", ColumnDescription = "Lugar de expedicion", IsNullable = false, Length = 50)]
      public string LUGAREXPEDICION {get { return this._LUGAREXPEDICION; } set { this._LUGAREXPEDICION = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONEDA
      /// </summary>
      [DataNames("MONEDA")]
      [SugarColumn(ColumnName = "moneda", ColumnDescription = "Moneda", IsNullable = false, Length = 3)]
      public string MONEDA {get { return this._MONEDA; } set { this._MONEDA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMCTAPAGO
      /// </summary>
      [DataNames("NUMCTAPAGO")]
      [SugarColumn(ColumnName = "numctapago", ColumnDescription = "Numero de cuenta del pago", Length = 50)]
      public string NUMCTAPAGO {get { return this._NUMCTAPAGO; } set { this._NUMCTAPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOCAMBIO
      /// </summary>
      [DataNames("TIPOCAMBIO")]
      [SugarColumn(ColumnName = "tipocambio", ColumnDescription = "Tipo de cambio")]
      public double? TIPOCAMBIO {get { return this._TIPOCAMBIO; } set { this._TIPOCAMBIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DESCUENTO
      /// </summary>
      [DataNames("DESCUENTO")]
      [SugarColumn(ColumnName = "descuento", ColumnDescription = "Descuento")]
      public double? DESCUENTO {get { return this._DESCUENTO; } set { this._DESCUENTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FORMAPAGO
      /// </summary>
      [DataNames("FORMAPAGO")]
      [SugarColumn(ColumnName = "formapago", ColumnDescription = "Forma de pago", Length = 50)]
      public string FORMAPAGO {get { return this._FORMAPAGO; } set { this._FORMAPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer METODOPAGO
      /// </summary>
      [DataNames("METODOPAGO")]
      [SugarColumn(ColumnName = "metodopago", ColumnDescription = "Metodo de pago", Length = 3)]
      public string METODOPAGO {get { return this._METODOPAGO; } set { this._METODOPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TOTALIMPUESTOSTRASLADADOS
      /// </summary>
      [DataNames("TOTALIMPUESTOSTRASLADADOS")]
      [SugarColumn(ColumnName = "totalimpuestostrasladados", ColumnDescription = "Total de traslados")]
      public double? TOTALIMPUESTOSTRASLADADOS {get { return this._TOTALIMPUESTOSTRASLADADOS; } set { this._TOTALIMPUESTOSTRASLADADOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TOTALIMPUESTOSRETENIDOS
      /// </summary>
      [DataNames("TOTALIMPUESTOSRETENIDOS")]
      [SugarColumn(ColumnName = "totalimpuestosretenidos", ColumnDescription = "Total de retenidos")]
      public double? TOTALIMPUESTOSRETENIDOS {get { return this._TOTALIMPUESTOSRETENIDOS; } set { this._TOTALIMPUESTOSRETENIDOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOMBREEMISOR
      /// </summary>
      [DataNames("NOMBREEMISOR")]
      [SugarColumn(ColumnName = "nombreemisor", ColumnDescription = "Nombre del emisor", Length = 50)]
      public string NOMBREEMISOR {get { return this._NOMBREEMISOR; } set { this._NOMBREEMISOR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOMBRERECEPTOR
      /// </summary>
      [DataNames("NOMBRERECEPTOR")]
      [SugarColumn(ColumnName = "nombrereceptor", ColumnDescription = "Nombre del receptor", Length = 50)]
      public string NOMBRERECEPTOR {get { return this._NOMBRERECEPTOR; } set { this._NOMBRERECEPTOR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer REGIMENFISCAL
      /// </summary>
      [DataNames("REGIMENFISCAL")]
      [SugarColumn(ColumnName = "regimenfiscal", ColumnDescription = "Regimen fiscal", IsNullable = false, Length = 30)]
      public string REGIMENFISCAL {get { return this._REGIMENFISCAL; } set { this._REGIMENFISCAL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer USOCFDI
      /// </summary>
      [DataNames("USOCFDI")]
      [SugarColumn(ColumnName = "usocfdi", ColumnDescription = "UsoCDFI del nodo cfdi:Receptor", Length = 6)]
      public string USOCFDI {get { return this._USOCFDI; } set { this._USOCFDI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SELLOCFDI
      /// </summary>
      [DataNames("SELLOCFDI")]
      [SugarColumn(ColumnName = "sellocfdi", ColumnDescription = "Sello timbrado", IsNullable = false, Length = 100)]
      public string SELLOCFDI {get { return this._SELLOCFDI; } set { this._SELLOCFDI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOCERTIFICADO
      /// </summary>
      [DataNames("NOCERTIFICADO")]
      [SugarColumn(ColumnName = "nocertificado", ColumnDescription = "NoCertificado del nodo cfdi:Comprobante", IsNullable = false, Length = 100)]
      public string NOCERTIFICADO {get { return this._NOCERTIFICADO; } set { this._NOCERTIFICADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOCERTIFICADOTIMBRE
      /// </summary>
      [DataNames("NOCERTIFICADOTIMBRE")]
      [SugarColumn(ColumnName = "nocertificadotimbre", ColumnDescription = "NoCertificadoSATdel nodo tfd:TimbreFiscalDigital", IsNullable = false, Length = 100)]
      public string NOCERTIFICADOTIMBRE {get { return this._NOCERTIFICADOTIMBRE; } set { this._NOCERTIFICADOTIMBRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CONDICIONESDEPAGO
      /// </summary>
      [DataNames("CONDICIONESDEPAGO")]
      [SugarColumn(ColumnName = "condicionesdepago", ColumnDescription = "Condiciones de pago", Length = 50)]
      public string CONDICIONESDEPAGO {get { return this._CONDICIONESDEPAGO; } set { this._CONDICIONESDEPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SELLO
      /// </summary>
      [DataNames("SELLO")]
      [DataNames(ColumnName = "sello", ColumnDescription = "Sello del documento", IsNullable = false, Length = 100)]
      public string SELLO {get { return this._SELLO; } set { this._SELLO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPODECOMPROBANTE
      /// </summary>
      [DataNames("TIPODECOMPROBANTE")]
      [SugarColumn(ColumnName = "tipodecomprobante", ColumnDescription = "Tipo del comprobante", IsNullable = false, Length = 50)]
      public string TIPODECOMPROBANTE {get { return this._TIPODECOMPROBANTE; } set { this._TIPODECOMPROBANTE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer XML
      /// </summary>
      [DataNames("XML")]
      [SugarColumn(ColumnName = "xml", ColumnDescription = "XML CFDI")]
      public string XML {get { return this._XML; } set { this._XML = value; this.OnPropertyChanged(); }}
   }
}
