﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///CUENTAS20
   ///</summary>
   [SugarTable("cuentas20")]
   public partial class CUENTAS20
   {
      public CUENTAS20(){
      }

      private string _NUM_CTA;
      private string _STATUS;
      private string _TIPO;
      private string _NOMBRE;
      private string _DEPTSINO;
      private int? _BANDMULTI;
      private string _BANDAJT;
      private string _CTA_PAPA;
      private string _CTA_RAIZ;
      private int? _NIVEL;
      private string _CTA_COMP;
      private int? _NATURALEZA;
      private string _RFC;
      private string _CODAGRUP;
      private int? _CAPTURACHEQUE;
      private int? _CAPTURAUUID;
      private int? _BANCO;
      private string _CTABANCARIA;
      private string _CAPCHEQTIPOMOV;
      private int? _NOINCLUIRXML;
      private string _IDFISCAL;
      private int? _ESFLUJODEEFECTIVO;
      private string _BANCOEXTRANJERO;
      private string _RFCFLUJO;

      /// <summary>
      /// obtener o establecer NUM_CTA
      /// </summary>
      [DataNames("NUM_CTA")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "num_cta", ColumnDescription = "Numero de cuenta contable", IsNullable = false, Length = 21)]
      public string NUM_CTA {get { return this._NUM_CTA; } set { this._NUM_CTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer STATUS
      /// </summary>
      [DataNames("STATUS")]
      [SugarColumn(ColumnName = "status", ColumnDescription = "Estado", Length = 1)]
      public string STATUS {get { return this._STATUS; } set { this._STATUS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPO
      /// </summary>
      [DataNames("TIPO")]
      [SugarColumn(ColumnName = "tipo", ColumnDescription = "Tipo", Length = 1)]
      public string TIPO {get { return this._TIPO; } set { this._TIPO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOMBRE
      /// </summary>
      [DataNames("NOMBRE")]
      [SugarColumn(ColumnName = "nombre", ColumnDescription = "Nombre", Length = 40)]
      public string NOMBRE {get { return this._NOMBRE; } set { this._NOMBRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DEPTSINO
      /// </summary>
      [DataNames("DEPTSINO")]
      [SugarColumn(ColumnName = "deptsino", ColumnDescription = "Es departamental o no", Length = 1)]
      public string DEPTSINO {get { return this._DEPTSINO; } set { this._DEPTSINO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BANDMULTI
      /// </summary>
      [DataNames("BANDMULTI")]
      [SugarColumn(ColumnName = "bandmulti", ColumnDescription = "Maneja multimoneda")]
      public int? BANDMULTI {get { return this._BANDMULTI; } set { this._BANDMULTI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BANDAJT
      /// </summary>
      [DataNames("BANDAJT")]
      [SugarColumn(ColumnName = "bandajt", ColumnDescription = "Indica si afecta poliza de ajuste cambiario", Length = 1)]
      public string BANDAJT {get { return this._BANDAJT; } set { this._BANDAJT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTA_PAPA
      /// </summary>
      [DataNames("CTA_PAPA")]
      [SugarColumn(ColumnName = "cta_papa", ColumnDescription = "Cuenta papa", Length = 21)]
      public string CTA_PAPA {get { return this._CTA_PAPA; } set { this._CTA_PAPA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTA_RAIZ
      /// </summary>
      [DataNames("CTA_RAIZ")]
      [SugarColumn(ColumnName = "cta_raiz", ColumnDescription = "Cuenta raiz", Length = 21)]
      public string CTA_RAIZ {get { return this._CTA_RAIZ; } set { this._CTA_RAIZ = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NIVEL
      /// </summary>
      [DataNames("NIVEL")]
      [SugarColumn(ColumnName = "nivel", ColumnDescription = "Nivel")]
      public int? NIVEL {get { return this._NIVEL; } set { this._NIVEL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTA_COMP
      /// </summary>
      [DataNames("CTA_COMP")]
      [SugarColumn(ColumnName = "cta_comp", ColumnDescription = "Cuenta contrapartida", Length = 21)]
      public string CTA_COMP {get { return this._CTA_COMP; } set { this._CTA_COMP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NATURALEZA
      /// </summary>
      [DataNames("NATURALEZA")]
      [SugarColumn(ColumnName = "naturaleza", ColumnDescription = "Naturaleza")]
      public int? NATURALEZA {get { return this._NATURALEZA; } set { this._NATURALEZA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFC
      /// </summary>
      [DataNames("RFC")]
      [SugarColumn(ColumnName = "rfc", ColumnDescription = "RFC", Length = 30)]
      public string RFC {get { return this._RFC; } set { this._RFC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CODAGRUP
      /// </summary>
      [DataNames("CODAGRUP")]
      [SugarColumn(ColumnName = "codagrup", ColumnDescription = "Codigo de agrupacion", Length = 10)]
      public string CODAGRUP {get { return this._CODAGRUP; } set { this._CODAGRUP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CAPTURACHEQUE
      /// </summary>
      [DataNames("CAPTURACHEQUE")]
      [SugarColumn(ColumnName = "capturacheque", ColumnDescription = "Indica si se captura formas de pago")]
      public int? CAPTURACHEQUE {get { return this._CAPTURACHEQUE; } set { this._CAPTURACHEQUE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CAPTURAUUID
      /// </summary>
      [DataNames("CAPTURAUUID")]
      [SugarColumn(ColumnName = "capturauuid", ColumnDescription = "Indica si se captura comprobantes")]
      public int? CAPTURAUUID {get { return this._CAPTURAUUID; } set { this._CAPTURAUUID = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BANCO
      /// </summary>
      [DataNames("BANCO")]
      [SugarColumn(ColumnName = "banco", ColumnDescription = "Banco capturado en la forma de pago")]
      public int? BANCO {get { return this._BANCO; } set { this._BANCO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTABANCARIA
      /// </summary>
      [DataNames("CTABANCARIA")]
      [SugarColumn(ColumnName = "ctabancaria", ColumnDescription = "Cuenta bancaria capturada en la forma de pago", Length = 50)]
      public string CTABANCARIA {get { return this._CTABANCARIA; } set { this._CTABANCARIA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CAPCHEQTIPOMOV
      /// </summary>
      [DataNames("CAPCHEQTIPOMOV")]
      [SugarColumn(ColumnName = "capcheqtipomov", ColumnDescription = "Tipo de movimiento", Length = 1)]
      public string CAPCHEQTIPOMOV {get { return this._CAPCHEQTIPOMOV; } set { this._CAPCHEQTIPOMOV = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NOINCLUIRXML
      /// </summary>
      [DataNames("NOINCLUIRXML")]
      [SugarColumn(ColumnName = "noincluirxml", ColumnDescription = "Indica que no se incluira al general el xml de contabilidad")]
      public int? NOINCLUIRXML {get { return this._NOINCLUIRXML; } set { this._NOINCLUIRXML = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IDFISCAL
      /// </summary>
      [DataNames("IDFISCAL")]
      [SugarColumn(ColumnName = "idfiscal", ColumnDescription = "Identificador Fiscal", Length = 40)]
      public string IDFISCAL {get { return this._IDFISCAL; } set { this._IDFISCAL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ESFLUJODEEFECTIVO
      /// </summary>
      [DataNames("ESFLUJODEEFECTIVO")]
      [SugarColumn(ColumnName = "esflujodeefectivo", ColumnDescription = "Indica si es flujo de efectivo")]
      public int? ESFLUJODEEFECTIVO {get { return this._ESFLUJODEEFECTIVO; } set { this._ESFLUJODEEFECTIVO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BANCOEXTRANJERO
      /// </summary>
      [DataNames("BANCOEXTRANJERO")]
      [SugarColumn(ColumnName = "bancoextranjero", ColumnDescription = "Banco extranjero", Length = 225)]
      public string BANCOEXTRANJERO {get { return this._BANCOEXTRANJERO; } set { this._BANCOEXTRANJERO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RFCFLUJO
      /// </summary>
      [DataNames("RFCFLUJO")]
      [SugarColumn(ColumnName = "rfcflujo", ColumnDescription = "RFCEmisor Cuenta ordenante o RFCEmisor cuenta beneficiario", Length = 15)]
      public string RFCFLUJO {get { return this._RFCFLUJO; } set { this._RFCFLUJO = value; this.OnPropertyChanged(); }}
   }
}
