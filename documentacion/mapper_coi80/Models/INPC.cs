﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("inpc")]
    public partial class INPC
    {
           public INPC(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("EJERCICIO")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="ejercicio")]ColumnDescription 
        public int EJERCICIO {get { return this._EJERCICIO; } set { this._EJERCICIO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MES1")] 
           [SugarColumn(ColumnName="mes1")]ColumnDescription 
        public double? MES1 {get { return this._MES1; } set { this._MES1 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MES2")] 
           [SugarColumn(ColumnName="mes2")]ColumnDescription 
        public double? MES2 {get { return this._MES2; } set { this._MES2 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MES3")] 
           [SugarColumn(ColumnName="mes3")]ColumnDescription 
        public double? MES3 {get { return this._MES3; } set { this._MES3 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MES4")] 
           [SugarColumn(ColumnName="mes4")]ColumnDescription 
        public double? MES4 {get { return this._MES4; } set { this._MES4 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MES5")] 
           [SugarColumn(ColumnName="mes5")]ColumnDescription 
        public double? MES5 {get { return this._MES5; } set { this._MES5 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MES6")] 
           [SugarColumn(ColumnName="mes6")]ColumnDescription 
        public double? MES6 {get { return this._MES6; } set { this._MES6 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MES7")] 
           [SugarColumn(ColumnName="mes7")]ColumnDescription 
        public double? MES7 {get { return this._MES7; } set { this._MES7 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MES8")] 
           [SugarColumn(ColumnName="mes8")]ColumnDescription 
        public double? MES8 {get { return this._MES8; } set { this._MES8 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MES9")] 
           [SugarColumn(ColumnName="mes9")]ColumnDescription 
        public double? MES9 {get { return this._MES9; } set { this._MES9 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MES10")] 
           [SugarColumn(ColumnName="mes10")]ColumnDescription 
        public double? MES10 {get { return this._MES10; } set { this._MES10 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MES11")] 
           [SugarColumn(ColumnName="mes11")]ColumnDescription 
        public double? MES11 {get { return this._MES11; } set { this._MES11 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MES12")] 
           [SugarColumn(ColumnName="mes12")]ColumnDescription 
        public double? MES12 {get { return this._MES12; } set { this._MES12 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL732")] 
           [SugarColumn(ColumnName="trial732")]ColumnDescription 
        public string TRIAL732 {get { return this._TRIAL732; } set { this._TRIAL732 = value; this.OnPropertyChanged(); }}
    }
}
