﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///ADMPER
   ///</summary>
   [SugarTable("admper")]
   public partial class ADMPER
   {
      public ADMPER(){
      }

      private int _PERIODO;
      private int _EJERCICIO;
      private int? _CERRADO;
      private int? _ORDENADO;
      private int? _REQTRASP;
      private int? _CTPOLIZAS;
      
      /// <summary>
      /// obtener o establecer PERIODO
      /// </summary>
      [DataNames("PERIODO")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "periodo", ColumnDescription = "Periodo")] 
      public int PERIODO {get { return this._PERIODO; } set { this._PERIODO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer EJERCICIO
      /// </summary>
      [DataNames("EJERCICIO")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "ejercicio", ColumnDescription = "Ejercicio")] 
      public int EJERCICIO {get { return this._EJERCICIO; } set { this._EJERCICIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CERRADO
      /// </summary>
      [DataNames("CERRADO")] 
      [SugarColumn(ColumnName = "cerrado", ColumnDescription = "Ejercicio cerrado")] 
      public int? CERRADO {get { return this._CERRADO; } set { this._CERRADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ORDENADO
      /// </summary>
      [DataNames("ORDENADO")] 
      [SugarColumn(ColumnName = "ordenado", ColumnDescription = "Ordenado")] 
      public int? ORDENADO {get { return this._ORDENADO; } set { this._ORDENADO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer REQTRASP
      /// </summary>
      [DataNames("REQTRASP")] 
      [SugarColumn(ColumnName = "reqtrasp", ColumnDescription = "Indica si requiere traspaso")] 
      public int? REQTRASP {get { return this._REQTRASP; } set { this._REQTRASP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CTPOLIZAS
      /// </summary>
      [DataNames("CTPOLIZAS")] 
      [SugarColumn(ColumnName = "ctpolizas", ColumnDescription = "Cuenta de polizas")] 
      public int? CTPOLIZAS {get { return this._CTPOLIZAS; } set { this._CTPOLIZAS = value; this.OnPropertyChanged(); }}
   }
}
