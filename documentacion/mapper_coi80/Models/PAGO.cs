﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("pago")]
    public partial class PAGO
    {
           public PAGO(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("IDPAGO")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="idpago")]ColumnDescription 
        public int IDPAGO {get { return this._IDPAGO; } set { this._IDPAGO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("UUIDTIMBREPAGO")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="uuidtimbrepago")]ColumnDescription 
        public string UUIDTIMBREPAGO {get { return this._UUIDTIMBREPAGO; } set { this._UUIDTIMBREPAGO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FECHAPAGO")] 
           [SugarColumn(ColumnName="fechapago")]ColumnDescription 
        public DateTime? FECHAPAGO {get { return this._FECHAPAGO; } set { this._FECHAPAGO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FORMAPAGOP")] 
           [SugarColumn(ColumnName="formapagop")]ColumnDescription 
        public string FORMAPAGOP {get { return this._FORMAPAGOP; } set { this._FORMAPAGOP = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MONEDAP")] 
           [SugarColumn(ColumnName="monedap")]ColumnDescription 
        public string MONEDAP {get { return this._MONEDAP; } set { this._MONEDAP = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPOCAMBIOP")] 
           [SugarColumn(ColumnName="tipocambiop")]ColumnDescription 
        public double? TIPOCAMBIOP {get { return this._TIPOCAMBIOP; } set { this._TIPOCAMBIOP = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IMPORTE")] 
           [SugarColumn(ColumnName="importe")]ColumnDescription 
        public double? IMPORTE {get { return this._IMPORTE; } set { this._IMPORTE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NUMOPERACION")] 
           [SugarColumn(ColumnName="numoperacion")]ColumnDescription 
        public string NUMOPERACION {get { return this._NUMOPERACION; } set { this._NUMOPERACION = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RFCEMISORCTAORD")] 
           [SugarColumn(ColumnName="rfcemisorctaord")]ColumnDescription 
        public string RFCEMISORCTAORD {get { return this._RFCEMISORCTAORD; } set { this._RFCEMISORCTAORD = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NOMBREBANCOORDEXT")] 
           [SugarColumn(ColumnName="nombrebancoordext")]ColumnDescription 
        public string NOMBREBANCOORDEXT {get { return this._NOMBREBANCOORDEXT; } set { this._NOMBREBANCOORDEXT = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTAORDENANTE")] 
           [SugarColumn(ColumnName="ctaordenante")]ColumnDescription 
        public string CTAORDENANTE {get { return this._CTAORDENANTE; } set { this._CTAORDENANTE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RFCEMISORCTABEN")] 
           [SugarColumn(ColumnName="rfcemisorctaben")]ColumnDescription 
        public string RFCEMISORCTABEN {get { return this._RFCEMISORCTABEN; } set { this._RFCEMISORCTABEN = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTABENEFICIARIO")] 
           [SugarColumn(ColumnName="ctabeneficiario")]ColumnDescription 
        public string CTABENEFICIARIO {get { return this._CTABENEFICIARIO; } set { this._CTABENEFICIARIO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPOCADPAGO")] 
           [SugarColumn(ColumnName="tipocadpago")]ColumnDescription 
        public string TIPOCADPAGO {get { return this._TIPOCADPAGO; } set { this._TIPOCADPAGO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CERTPAGO")] 
           [SugarColumn(ColumnName="certpago")]ColumnDescription 
        public string CERTPAGO {get { return this._CERTPAGO; } set { this._CERTPAGO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CADPAGO")] 
           [SugarColumn(ColumnName="cadpago")]ColumnDescription 
        public string CADPAGO {get { return this._CADPAGO; } set { this._CADPAGO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("SELLOPAGO")] 
           [SugarColumn(ColumnName="sellopago")]ColumnDescription 
        public string SELLOPAGO {get { return this._SELLOPAGO; } set { this._SELLOPAGO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("VERSION")] 
           [SugarColumn(ColumnName="version")]ColumnDescription 
        public string VERSION {get { return this._VERSION; } set { this._VERSION = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL732")] 
           [SugarColumn(ColumnName="trial732")]ColumnDescription 
        public string TRIAL732 {get { return this._TRIAL732; } set { this._TRIAL732 = value; this.OnPropertyChanged(); }}
    }
}
