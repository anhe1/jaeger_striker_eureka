﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("saldos20")]
    public partial class SALDOS20
    {
           public SALDOS20(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_CTA")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="num_cta")]ColumnDescription 
        public string NUM_CTA {get { return this._NUM_CTA; } set { this._NUM_CTA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("EJERCICIO")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="ejercicio")]ColumnDescription 
        public int EJERCICIO {get { return this._EJERCICIO; } set { this._EJERCICIO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("INICIAL")] 
           [SugarColumn(ColumnName="inicial")]ColumnDescription 
        public double? INICIAL {get { return this._INICIAL; } set { this._INICIAL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("INICIALEX")] 
           [SugarColumn(ColumnName="inicialex")]ColumnDescription 
        public double? INICIALEX {get { return this._INICIALEX; } set { this._INICIALEX = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGO01")] 
           [SugarColumn(ColumnName="cargo01")]ColumnDescription 
        public double? CARGO01 {get { return this._CARGO01; } set { this._CARGO01 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONO01")] 
           [SugarColumn(ColumnName="abono01")]ColumnDescription 
        public double? ABONO01 {get { return this._ABONO01; } set { this._ABONO01 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGOEX01")] 
           [SugarColumn(ColumnName="cargoex01")]ColumnDescription 
        public double? CARGOEX01 {get { return this._CARGOEX01; } set { this._CARGOEX01 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONOEX01")] 
           [SugarColumn(ColumnName="abonoex01")]ColumnDescription 
        public double? ABONOEX01 {get { return this._ABONOEX01; } set { this._ABONOEX01 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGO02")] 
           [SugarColumn(ColumnName="cargo02")]ColumnDescription 
        public double? CARGO02 {get { return this._CARGO02; } set { this._CARGO02 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONO02")] 
           [SugarColumn(ColumnName="abono02")]ColumnDescription 
        public double? ABONO02 {get { return this._ABONO02; } set { this._ABONO02 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGOEX02")] 
           [SugarColumn(ColumnName="cargoex02")]ColumnDescription 
        public double? CARGOEX02 {get { return this._CARGOEX02; } set { this._CARGOEX02 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONOEX02")] 
           [SugarColumn(ColumnName="abonoex02")]ColumnDescription 
        public double? ABONOEX02 {get { return this._ABONOEX02; } set { this._ABONOEX02 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGO03")] 
           [SugarColumn(ColumnName="cargo03")]ColumnDescription 
        public double? CARGO03 {get { return this._CARGO03; } set { this._CARGO03 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONO03")] 
           [SugarColumn(ColumnName="abono03")]ColumnDescription 
        public double? ABONO03 {get { return this._ABONO03; } set { this._ABONO03 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGOEX03")] 
           [SugarColumn(ColumnName="cargoex03")]ColumnDescription 
        public double? CARGOEX03 {get { return this._CARGOEX03; } set { this._CARGOEX03 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONOEX03")] 
           [SugarColumn(ColumnName="abonoex03")]ColumnDescription 
        public double? ABONOEX03 {get { return this._ABONOEX03; } set { this._ABONOEX03 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGO04")] 
           [SugarColumn(ColumnName="cargo04")]ColumnDescription 
        public double? CARGO04 {get { return this._CARGO04; } set { this._CARGO04 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONO04")] 
           [SugarColumn(ColumnName="abono04")]ColumnDescription 
        public double? ABONO04 {get { return this._ABONO04; } set { this._ABONO04 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGOEX04")] 
           [SugarColumn(ColumnName="cargoex04")]ColumnDescription 
        public double? CARGOEX04 {get { return this._CARGOEX04; } set { this._CARGOEX04 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONOEX04")] 
           [SugarColumn(ColumnName="abonoex04")]ColumnDescription 
        public double? ABONOEX04 {get { return this._ABONOEX04; } set { this._ABONOEX04 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGO05")] 
           [SugarColumn(ColumnName="cargo05")]ColumnDescription 
        public double? CARGO05 {get { return this._CARGO05; } set { this._CARGO05 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONO05")] 
           [SugarColumn(ColumnName="abono05")]ColumnDescription 
        public double? ABONO05 {get { return this._ABONO05; } set { this._ABONO05 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGOEX05")] 
           [SugarColumn(ColumnName="cargoex05")]ColumnDescription 
        public double? CARGOEX05 {get { return this._CARGOEX05; } set { this._CARGOEX05 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONOEX05")] 
           [SugarColumn(ColumnName="abonoex05")]ColumnDescription 
        public double? ABONOEX05 {get { return this._ABONOEX05; } set { this._ABONOEX05 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGO06")] 
           [SugarColumn(ColumnName="cargo06")]ColumnDescription 
        public double? CARGO06 {get { return this._CARGO06; } set { this._CARGO06 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONO06")] 
           [SugarColumn(ColumnName="abono06")]ColumnDescription 
        public double? ABONO06 {get { return this._ABONO06; } set { this._ABONO06 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGOEX06")] 
           [SugarColumn(ColumnName="cargoex06")]ColumnDescription 
        public double? CARGOEX06 {get { return this._CARGOEX06; } set { this._CARGOEX06 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONOEX06")] 
           [SugarColumn(ColumnName="abonoex06")]ColumnDescription 
        public double? ABONOEX06 {get { return this._ABONOEX06; } set { this._ABONOEX06 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGO07")] 
           [SugarColumn(ColumnName="cargo07")]ColumnDescription 
        public double? CARGO07 {get { return this._CARGO07; } set { this._CARGO07 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONO07")] 
           [SugarColumn(ColumnName="abono07")]ColumnDescription 
        public double? ABONO07 {get { return this._ABONO07; } set { this._ABONO07 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGOEX07")] 
           [SugarColumn(ColumnName="cargoex07")]ColumnDescription 
        public double? CARGOEX07 {get { return this._CARGOEX07; } set { this._CARGOEX07 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONOEX07")] 
           [SugarColumn(ColumnName="abonoex07")]ColumnDescription 
        public double? ABONOEX07 {get { return this._ABONOEX07; } set { this._ABONOEX07 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGO08")] 
           [SugarColumn(ColumnName="cargo08")]ColumnDescription 
        public double? CARGO08 {get { return this._CARGO08; } set { this._CARGO08 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONO08")] 
           [SugarColumn(ColumnName="abono08")]ColumnDescription 
        public double? ABONO08 {get { return this._ABONO08; } set { this._ABONO08 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGOEX08")] 
           [SugarColumn(ColumnName="cargoex08")]ColumnDescription 
        public double? CARGOEX08 {get { return this._CARGOEX08; } set { this._CARGOEX08 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONOEX08")] 
           [SugarColumn(ColumnName="abonoex08")]ColumnDescription 
        public double? ABONOEX08 {get { return this._ABONOEX08; } set { this._ABONOEX08 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGO09")] 
           [SugarColumn(ColumnName="cargo09")]ColumnDescription 
        public double? CARGO09 {get { return this._CARGO09; } set { this._CARGO09 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONO09")] 
           [SugarColumn(ColumnName="abono09")]ColumnDescription 
        public double? ABONO09 {get { return this._ABONO09; } set { this._ABONO09 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGOEX09")] 
           [SugarColumn(ColumnName="cargoex09")]ColumnDescription 
        public double? CARGOEX09 {get { return this._CARGOEX09; } set { this._CARGOEX09 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONOEX09")] 
           [SugarColumn(ColumnName="abonoex09")]ColumnDescription 
        public double? ABONOEX09 {get { return this._ABONOEX09; } set { this._ABONOEX09 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGO10")] 
           [SugarColumn(ColumnName="cargo10")]ColumnDescription 
        public double? CARGO10 {get { return this._CARGO10; } set { this._CARGO10 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONO10")] 
           [SugarColumn(ColumnName="abono10")]ColumnDescription 
        public double? ABONO10 {get { return this._ABONO10; } set { this._ABONO10 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGOEX10")] 
           [SugarColumn(ColumnName="cargoex10")]ColumnDescription 
        public double? CARGOEX10 {get { return this._CARGOEX10; } set { this._CARGOEX10 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONOEX10")] 
           [SugarColumn(ColumnName="abonoex10")]ColumnDescription 
        public double? ABONOEX10 {get { return this._ABONOEX10; } set { this._ABONOEX10 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGO11")] 
           [SugarColumn(ColumnName="cargo11")]ColumnDescription 
        public double? CARGO11 {get { return this._CARGO11; } set { this._CARGO11 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONO11")] 
           [SugarColumn(ColumnName="abono11")]ColumnDescription 
        public double? ABONO11 {get { return this._ABONO11; } set { this._ABONO11 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGOEX11")] 
           [SugarColumn(ColumnName="cargoex11")]ColumnDescription 
        public double? CARGOEX11 {get { return this._CARGOEX11; } set { this._CARGOEX11 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONOEX11")] 
           [SugarColumn(ColumnName="abonoex11")]ColumnDescription 
        public double? ABONOEX11 {get { return this._ABONOEX11; } set { this._ABONOEX11 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGO12")] 
           [SugarColumn(ColumnName="cargo12")]ColumnDescription 
        public double? CARGO12 {get { return this._CARGO12; } set { this._CARGO12 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONO12")] 
           [SugarColumn(ColumnName="abono12")]ColumnDescription 
        public double? ABONO12 {get { return this._ABONO12; } set { this._ABONO12 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGOEX12")] 
           [SugarColumn(ColumnName="cargoex12")]ColumnDescription 
        public double? CARGOEX12 {get { return this._CARGOEX12; } set { this._CARGOEX12 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONOEX12")] 
           [SugarColumn(ColumnName="abonoex12")]ColumnDescription 
        public double? ABONOEX12 {get { return this._ABONOEX12; } set { this._ABONOEX12 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGO13")] 
           [SugarColumn(ColumnName="cargo13")]ColumnDescription 
        public double? CARGO13 {get { return this._CARGO13; } set { this._CARGO13 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONO13")] 
           [SugarColumn(ColumnName="abono13")]ColumnDescription 
        public double? ABONO13 {get { return this._ABONO13; } set { this._ABONO13 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGOEX13")] 
           [SugarColumn(ColumnName="cargoex13")]ColumnDescription 
        public double? CARGOEX13 {get { return this._CARGOEX13; } set { this._CARGOEX13 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONOEX13")] 
           [SugarColumn(ColumnName="abonoex13")]ColumnDescription 
        public double? ABONOEX13 {get { return this._ABONOEX13; } set { this._ABONOEX13 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGO14")] 
           [SugarColumn(ColumnName="cargo14")]ColumnDescription 
        public double? CARGO14 {get { return this._CARGO14; } set { this._CARGO14 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONO14")] 
           [SugarColumn(ColumnName="abono14")]ColumnDescription 
        public double? ABONO14 {get { return this._ABONO14; } set { this._ABONO14 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGOEX14")] 
           [SugarColumn(ColumnName="cargoex14")]ColumnDescription 
        public double? CARGOEX14 {get { return this._CARGOEX14; } set { this._CARGOEX14 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONOEX14")] 
           [SugarColumn(ColumnName="abonoex14")]ColumnDescription 
        public double? ABONOEX14 {get { return this._ABONOEX14; } set { this._ABONOEX14 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MOV01")] 
           [SugarColumn(ColumnName="mov01")]ColumnDescription 
        public int? MOV01 {get { return this._MOV01; } set { this._MOV01 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MOV02")] 
           [SugarColumn(ColumnName="mov02")]ColumnDescription 
        public int? MOV02 {get { return this._MOV02; } set { this._MOV02 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MOV03")] 
           [SugarColumn(ColumnName="mov03")]ColumnDescription 
        public int? MOV03 {get { return this._MOV03; } set { this._MOV03 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MOV04")] 
           [SugarColumn(ColumnName="mov04")]ColumnDescription 
        public int? MOV04 {get { return this._MOV04; } set { this._MOV04 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MOV05")] 
           [SugarColumn(ColumnName="mov05")]ColumnDescription 
        public int? MOV05 {get { return this._MOV05; } set { this._MOV05 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MOV06")] 
           [SugarColumn(ColumnName="mov06")]ColumnDescription 
        public int? MOV06 {get { return this._MOV06; } set { this._MOV06 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MOV07")] 
           [SugarColumn(ColumnName="mov07")]ColumnDescription 
        public int? MOV07 {get { return this._MOV07; } set { this._MOV07 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MOV08")] 
           [SugarColumn(ColumnName="mov08")]ColumnDescription 
        public int? MOV08 {get { return this._MOV08; } set { this._MOV08 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MOV09")] 
           [SugarColumn(ColumnName="mov09")]ColumnDescription 
        public int? MOV09 {get { return this._MOV09; } set { this._MOV09 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MOV10")] 
           [SugarColumn(ColumnName="mov10")]ColumnDescription 
        public int? MOV10 {get { return this._MOV10; } set { this._MOV10 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MOV11")] 
           [SugarColumn(ColumnName="mov11")]ColumnDescription 
        public int? MOV11 {get { return this._MOV11; } set { this._MOV11 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MOV12")] 
           [SugarColumn(ColumnName="mov12")]ColumnDescription 
        public int? MOV12 {get { return this._MOV12; } set { this._MOV12 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MOV13")] 
           [SugarColumn(ColumnName="mov13")]ColumnDescription 
        public int? MOV13 {get { return this._MOV13; } set { this._MOV13 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MOV14")] 
           [SugarColumn(ColumnName="mov14")]ColumnDescription 
        public int? MOV14 {get { return this._MOV14; } set { this._MOV14 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL736")] 
           [SugarColumn(ColumnName="trial736")]ColumnDescription 
        public string TRIAL736 {get { return this._TRIAL736; } set { this._TRIAL736 = value; this.OnPropertyChanged(); }}
    }
}
