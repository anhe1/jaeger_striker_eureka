﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("plantillaxrfc")]
    public partial class PLANTILLAXRFC
    {
           public PLANTILLAXRFC(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("RFC")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="rfc")]ColumnDescription 
        public string RFC {get { return this._RFC; } set { this._RFC = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("CLASPLANTILLA")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="clasplantilla")]ColumnDescription 
        public string CLASPLANTILLA {get { return this._CLASPLANTILLA; } set { this._CLASPLANTILLA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NOMBREPLANTILLA")] 
           [SugarColumn(ColumnName="nombreplantilla")]ColumnDescription 
        public string NOMBREPLANTILLA {get { return this._NOMBREPLANTILLA; } set { this._NOMBREPLANTILLA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NOMBREPLANTILLA2")] 
           [SugarColumn(ColumnName="nombreplantilla2")]ColumnDescription 
        public string NOMBREPLANTILLA2 {get { return this._NOMBREPLANTILLA2; } set { this._NOMBREPLANTILLA2 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL732")] 
           [SugarColumn(ColumnName="trial732")]ColumnDescription 
        public string TRIAL732 {get { return this._TRIAL732; } set { this._TRIAL732 = value; this.OnPropertyChanged(); }}
    }
}
