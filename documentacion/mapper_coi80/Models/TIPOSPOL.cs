﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("tipospol")]
    public partial class TIPOSPOL
    {
           public TIPOSPOL(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("TIPO")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="tipo")]ColumnDescription 
        public string TIPO {get { return this._TIPO; } set { this._TIPO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DESCRIP")] 
           [SugarColumn(ColumnName="descrip")]ColumnDescription 
        public string DESCRIP {get { return this._DESCRIP; } set { this._DESCRIP = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CLASSAT")] 
           [SugarColumn(ColumnName="classat")]ColumnDescription 
        public int? CLASSAT {get { return this._CLASSAT; } set { this._CLASSAT = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL739")] 
           [SugarColumn(ColumnName="trial739")]ColumnDescription 
        public string TRIAL739 {get { return this._TRIAL739; } set { this._TRIAL739 = value; this.OnPropertyChanged(); }}
    }
}
