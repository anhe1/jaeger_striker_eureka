﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("regpol")]
    public partial class REGPOL
    {
           public REGPOL(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("TRANSACCIO")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="transaccio")]ColumnDescription 
        public int TRANSACCIO {get { return this._TRANSACCIO; } set { this._TRANSACCIO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPOPOL")] 
           [SugarColumn(ColumnName="tipopol")]ColumnDescription 
        public string TIPOPOL {get { return this._TIPOPOL; } set { this._TIPOPOL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NUMPOL")] 
           [SugarColumn(ColumnName="numpol")]ColumnDescription 
        public string NUMPOL {get { return this._NUMPOL; } set { this._NUMPOL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FECHAPOL")] 
           [SugarColumn(ColumnName="fechapol")]ColumnDescription 
        public DateTime? FECHAPOL {get { return this._FECHAPOL; } set { this._FECHAPOL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FECHAOPR")] 
           [SugarColumn(ColumnName="fechaopr")]ColumnDescription 
        public DateTime? FECHAOPR {get { return this._FECHAOPR; } set { this._FECHAOPR = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("SISTEMA")] 
           [SugarColumn(ColumnName="sistema")]ColumnDescription 
        public string SISTEMA {get { return this._SISTEMA; } set { this._SISTEMA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NUMUSR")] 
           [SugarColumn(ColumnName="numusr")]ColumnDescription 
        public int? NUMUSR {get { return this._NUMUSR; } set { this._NUMUSR = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("OPERACION")] 
           [SugarColumn(ColumnName="operacion")]ColumnDescription 
        public int? OPERACION {get { return this._OPERACION; } set { this._OPERACION = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("STATUS")] 
           [SugarColumn(ColumnName="status")]ColumnDescription 
        public int? STATUS {get { return this._STATUS; } set { this._STATUS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPPOLINTR")] 
           [SugarColumn(ColumnName="tippolintr")]ColumnDescription 
        public string TIPPOLINTR {get { return this._TIPPOLINTR; } set { this._TIPPOLINTR = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("POLMODELO")] 
           [SugarColumn(ColumnName="polmodelo")]ColumnDescription 
        public string POLMODELO {get { return this._POLMODELO; } set { this._POLMODELO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("REGCONCLI")] 
           [SugarColumn(ColumnName="regconcli")]ColumnDescription 
        public int? REGCONCLI {get { return this._REGCONCLI; } set { this._REGCONCLI = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("STATUSCLI")] 
           [SugarColumn(ColumnName="statuscli")]ColumnDescription 
        public string STATUSCLI {get { return this._STATUSCLI; } set { this._STATUSCLI = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL736")] 
           [SugarColumn(ColumnName="trial736")]ColumnDescription 
        public string TRIAL736 {get { return this._TRIAL736; } set { this._TRIAL736 = value; this.OnPropertyChanged(); }}
    }
}
