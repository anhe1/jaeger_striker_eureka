﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///DEPTOS
   ///</summary>
   [SugarTable("deptos")]
   public partial class DEPTOS
   {
      public DEPTOS(){
      }

      private int _DEPTO;
      private string _DESCRIP;
         
      /// <summary>
      /// obtener o establecer DEPTO
      /// </summary>
      [DataNames("DEPTO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "depto", ColumnDescription = "Depto", IsNullable = false)] 
      public int DEPTO {get { return this._DEPTO; } set { this._DEPTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DESCRIP
      /// </summary>
      [DataNames("DESCRIP")]
      [SugarColumn(ColumnName = "descrip", ColumnDescription = "Descripcion", Length = 40)] 
      public string DESCRIP {get { return this._DESCRIP; } set { this._DESCRIP = value; this.OnPropertyChanged(); }}
   }
}
