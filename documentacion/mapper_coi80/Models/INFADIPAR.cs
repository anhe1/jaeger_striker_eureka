﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///INFADIPAR
   ///</summary>
   [SugarTable("infadipar")]
   public partial class INFADIPAR
   {
      public INFADIPAR(){
      }

      /// <summary>
      /// obtener o establecer TRIAL Default_New: Nullable_New:False
      /// </summary>
      [DataNames("NUMREG")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "numreg", ColumnDescription = "Numero de registro", )] 
      public int NUMREG {get { return this._NUMREG; } set { this._NUMREG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("FRMPAGO")] 
      [SugarColumn(ColumnName = "frmpago", ColumnDescription = "Forma de pago")] 
      public string FRMPAGO {get { return this._FRMPAGO; } set { this._FRMPAGO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("NUMCHEQUE")] 
      [SugarColumn(ColumnName = "numcheque", ColumnDescription = "Numero de cheque")] 
      public string NUMCHEQUE {get { return this._NUMCHEQUE; } set { this._NUMCHEQUE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New: Nullable_New:False
      /// </summary>
      [DataNames("BANCO")] 
      [SugarColumn(ColumnName = "banco", ColumnDescription = "Clavel del banco", IsNullable = false)] 
      public int BANCO {get { return this._BANCO; } set { this._BANCO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CTAORIG")] 
      [SugarColumn(ColumnName = "ctaorig", ColumnDescription = "Cuenta origen")] 
      public string CTAORIG {get { return this._CTAORIG; } set { this._CTAORIG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New: Nullable_New:False
      /// </summary>
      [DataNames("FECHA")] 
      [SugarColumn(ColumnName = "fecha", ColumnDescription = "Fecha", IsNullable = false)] 
      public DateTime FECHA {get { return this._FECHA; } set { this._FECHA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("MONTO")] 
      [SugarColumn(ColumnName = "monto", ColumnDescription = "Monto")] 
      public double? MONTO {get { return this._MONTO; } set { this._MONTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("BENEF")] 
      [SugarColumn(ColumnName = "benef", ColumnDescription = "Beneficiario")] 
      public string BENEF {get { return this._BENEF; } set { this._BENEF = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("RFC")] 
      [SugarColumn(ColumnName = "rfc", ColumnDescription = "RFC")] 
      public string RFC {get { return this._RFC; } set { this._RFC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New: Nullable_New:False
      /// </summary>
      [DataNames("BANCODEST")] 
      [SugarColumn(ColumnName = "bancodest", ColumnDescription = "Banco destino")] 
      public int BANCODEST {get { return this._BANCODEST; } set { this._BANCODEST = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("CTADEST")] 
      [SugarColumn(ColumnName = "ctadest", ColumnDescription = "Cuenta destino", IsNullable = false)] 
      public string CTADEST {get { return this._CTADEST; } set { this._CTADEST = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("BANCOORIGEXT")] 
      [SugarColumn(ColumnName = "bancoorigext", ColumnDescription = "Banco origen extranjero")] 
      public string BANCOORIGEXT {get { return this._BANCOORIGEXT; } set { this._BANCOORIGEXT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("BANCODESTEXT")] 
      [SugarColumn(ColumnName = "bancodestext", ColumnDescription = "Banco destino extranjero")] 
      public string BANCODESTEXT {get { return this._BANCODESTEXT; } set { this._BANCODESTEXT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
      /// </summary>
      [DataNames("IDFISCAL")] 
      [SugarColumn(ColumnName = "idfiscal", ColumnDescription = "Identificador Fiscal")] 
      public string IDFISCAL {get { return this._IDFISCAL; } set { this._IDFISCAL = value; this.OnPropertyChanged(); }}
   }
}
