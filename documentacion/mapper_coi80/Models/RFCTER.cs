﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("rfcter")]
    public partial class RFCTER
    {
           public RFCTER(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("RFCIDFISC")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="rfcidfisc")]ColumnDescription 
        public string RFCIDFISC {get { return this._RFCIDFISC; } set { this._RFCIDFISC = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPO")] 
           [SugarColumn(ColumnName="tipo")]ColumnDescription 
        public int? TIPO {get { return this._TIPO; } set { this._TIPO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NOMBRE")] 
           [SugarColumn(ColumnName="nombre")]ColumnDescription 
        public string NOMBRE {get { return this._NOMBRE; } set { this._NOMBRE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PAIS")] 
           [SugarColumn(ColumnName="pais")]ColumnDescription 
        public string PAIS {get { return this._PAIS; } set { this._PAIS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NACIONAL")] 
           [SugarColumn(ColumnName="nacional")]ColumnDescription 
        public string NACIONAL {get { return this._NACIONAL; } set { this._NACIONAL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPOOP")] 
           [SugarColumn(ColumnName="tipoop")]ColumnDescription 
        public int? TIPOOP {get { return this._TIPOOP; } set { this._TIPOOP = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CLAVESAE")] 
           [SugarColumn(ColumnName="clavesae")]ColumnDescription 
        public string CLAVESAE {get { return this._CLAVESAE; } set { this._CLAVESAE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CUENTAGAS")] 
           [SugarColumn(ColumnName="cuentagas")]ColumnDescription 
        public string CUENTAGAS {get { return this._CUENTAGAS; } set { this._CUENTAGAS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ESMONTO")] 
           [SugarColumn(ColumnName="esmonto")]ColumnDescription 
        public string ESMONTO {get { return this._ESMONTO; } set { this._ESMONTO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MONPORC")] 
           [SugarColumn(ColumnName="monporc")]ColumnDescription 
        public double? MONPORC {get { return this._MONPORC; } set { this._MONPORC = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVADEFAULT")] 
           [SugarColumn(ColumnName="ivadefault")]ColumnDescription 
        public double? IVADEFAULT {get { return this._IVADEFAULT; } set { this._IVADEFAULT = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PORCENTAJEIVA")] 
           [SugarColumn(ColumnName="porcentajeiva")]ColumnDescription 
        public double? PORCENTAJEIVA {get { return this._PORCENTAJEIVA; } set { this._PORCENTAJEIVA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PORCENTAJEISR")] 
           [SugarColumn(ColumnName="porcentajeisr")]ColumnDescription 
        public double? PORCENTAJEISR {get { return this._PORCENTAJEISR; } set { this._PORCENTAJEISR = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("INCLUYEIVA")] 
           [SugarColumn(ColumnName="incluyeiva")]ColumnDescription 
        public int? INCLUYEIVA {get { return this._INCLUYEIVA; } set { this._INCLUYEIVA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ESPROV")] 
           [SugarColumn(ColumnName="esprov")]ColumnDescription 
        public string ESPROV {get { return this._ESPROV; } set { this._ESPROV = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTABANCARIA")] 
           [SugarColumn(ColumnName="ctabancaria")]ColumnDescription 
        public string CTABANCARIA {get { return this._CTABANCARIA; } set { this._CTABANCARIA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("BANCO")] 
           [SugarColumn(ColumnName="banco")]ColumnDescription 
        public int? BANCO {get { return this._BANCO; } set { this._BANCO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FRMPAGO")] 
           [SugarColumn(ColumnName="frmpago")]ColumnDescription 
        public string FRMPAGO {get { return this._FRMPAGO; } set { this._FRMPAGO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("BANCOEXTRANJERO")] 
           [SugarColumn(ColumnName="bancoextranjero")]ColumnDescription 
        public string BANCOEXTRANJERO {get { return this._BANCOEXTRANJERO; } set { this._BANCOEXTRANJERO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ESIDFISCAL")] 
           [SugarColumn(ColumnName="esidfiscal")]ColumnDescription 
        public int? ESIDFISCAL {get { return this._ESIDFISCAL; } set { this._ESIDFISCAL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:'0' Nullable_New:True
        /// </summary>
        [DataNames("PLANTILLAPOLDIN")] 
           [SugarColumn(ColumnName="plantillapoldin")]ColumnDescription 
        public string PLANTILLAPOLDIN {get { return this._PLANTILLAPOLDIN; } set { this._PLANTILLAPOLDIN = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL736")] 
           [SugarColumn(ColumnName="trial736")]ColumnDescription 
        public string TRIAL736 {get { return this._TRIAL736; } set { this._TRIAL736 = value; this.OnPropertyChanged(); }}
    }
}
