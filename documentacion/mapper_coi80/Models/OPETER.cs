﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("opeter")]
    public partial class OPETER
    {
           public OPETER(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("TIPOPOL")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="tipopol")]ColumnDescription 
        public string TIPOPOL {get { return this._TIPOPOL; } set { this._TIPOPOL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUMPOL")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="numpol")]ColumnDescription 
        public string NUMPOL {get { return this._NUMPOL; } set { this._NUMPOL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("FECHAPOL")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="fechapol")]ColumnDescription 
        public DateTime FECHAPOL {get { return this._FECHAPOL; } set { this._FECHAPOL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUMPART")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="numpart")]ColumnDescription 
        public int NUMPART {get { return this._NUMPART; } set { this._NUMPART = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NUMCTA")] 
           [SugarColumn(ColumnName="numcta")]ColumnDescription 
        public string NUMCTA {get { return this._NUMCTA; } set { this._NUMCTA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RFCPROVE")] 
           [SugarColumn(ColumnName="rfcprove")]ColumnDescription 
        public string RFCPROVE {get { return this._RFCPROVE; } set { this._RFCPROVE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPOPE")] 
           [SugarColumn(ColumnName="tipope")]ColumnDescription 
        public int? TIPOPE {get { return this._TIPOPE; } set { this._TIPOPE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MONCONIVA")] 
           [SugarColumn(ColumnName="monconiva")]ColumnDescription 
        public double? MONCONIVA {get { return this._MONCONIVA; } set { this._MONCONIVA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MONDEDISR")] 
           [SugarColumn(ColumnName="mondedisr")]ColumnDescription 
        public double? MONDEDISR {get { return this._MONDEDISR; } set { this._MONDEDISR = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ACTOS15")] 
           [SugarColumn(ColumnName="actos15")]ColumnDescription 
        public double? ACTOS15 {get { return this._ACTOS15; } set { this._ACTOS15 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVAOP15")] 
           [SugarColumn(ColumnName="ivaop15")]ColumnDescription 
        public double? IVAOP15 {get { return this._IVAOP15; } set { this._IVAOP15 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ACTOS10")] 
           [SugarColumn(ColumnName="actos10")]ColumnDescription 
        public double? ACTOS10 {get { return this._ACTOS10; } set { this._ACTOS10 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVAOP10")] 
           [SugarColumn(ColumnName="ivaop10")]ColumnDescription 
        public double? IVAOP10 {get { return this._IVAOP10; } set { this._IVAOP10 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ACTOSCERO")] 
           [SugarColumn(ColumnName="actoscero")]ColumnDescription 
        public double? ACTOSCERO {get { return this._ACTOSCERO; } set { this._ACTOSCERO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ACTOSEXENT")] 
           [SugarColumn(ColumnName="actosexent")]ColumnDescription 
        public double? ACTOSEXENT {get { return this._ACTOSEXENT; } set { this._ACTOSEXENT = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVARETENID")] 
           [SugarColumn(ColumnName="ivaretenid")]ColumnDescription 
        public double? IVARETENID {get { return this._IVARETENID; } set { this._IVARETENID = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVATRASLAD")] 
           [SugarColumn(ColumnName="ivatraslad")]ColumnDescription 
        public double? IVATRASLAD {get { return this._IVATRASLAD; } set { this._IVATRASLAD = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVADEVOLU")] 
           [SugarColumn(ColumnName="ivadevolu")]ColumnDescription 
        public double? IVADEVOLU {get { return this._IVADEVOLU; } set { this._IVADEVOLU = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PERCAUSAC")] 
           [SugarColumn(ColumnName="percausac")]ColumnDescription 
        public DateTime? PERCAUSAC {get { return this._PERCAUSAC; } set { this._PERCAUSAC = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVANOAC15")] 
           [SugarColumn(ColumnName="ivanoac15")]ColumnDescription 
        public double? IVANOAC15 {get { return this._IVANOAC15; } set { this._IVANOAC15 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVANOAC10")] 
           [SugarColumn(ColumnName="ivanoac10")]ColumnDescription 
        public double? IVANOAC10 {get { return this._IVANOAC10; } set { this._IVANOAC10 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ESIMPORTA")] 
           [SugarColumn(ColumnName="esimporta")]ColumnDescription 
        public string ESIMPORTA {get { return this._ESIMPORTA; } set { this._ESIMPORTA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("OTRASRET")] 
           [SugarColumn(ColumnName="otrasret")]ColumnDescription 
        public double? OTRASRET {get { return this._OTRASRET; } set { this._OTRASRET = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ESDEVOL")] 
           [SugarColumn(ColumnName="esdevol")]ColumnDescription 
        public int? ESDEVOL {get { return this._ESDEVOL; } set { this._ESDEVOL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ISRRETENID")] 
           [SugarColumn(ColumnName="isrretenid")]ColumnDescription 
        public double? ISRRETENID {get { return this._ISRRETENID; } set { this._ISRRETENID = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVAGENERAL")] 
           [SugarColumn(ColumnName="ivageneral")]ColumnDescription 
        public int? IVAGENERAL {get { return this._IVAGENERAL; } set { this._IVAGENERAL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVAFRONTERIZO")] 
           [SugarColumn(ColumnName="ivafronterizo")]ColumnDescription 
        public int? IVAFRONTERIZO {get { return this._IVAFRONTERIZO; } set { this._IVAFRONTERIZO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IDCONCEPIVAA")] 
           [SugarColumn(ColumnName="idconcepivaa")]ColumnDescription 
        public int? IDCONCEPIVAA {get { return this._IDCONCEPIVAA; } set { this._IDCONCEPIVAA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DESDECFDI")] 
           [SugarColumn(ColumnName="desdecfdi")]ColumnDescription 
        public int? DESDECFDI {get { return this._DESDECFDI; } set { this._DESDECFDI = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IDOPTER")] 
           [SugarColumn(ColumnName="idopter")]ColumnDescription 
        public int? IDOPTER {get { return this._IDOPTER; } set { this._IDOPTER = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL732")] 
           [SugarColumn(ColumnName="trial732")]ColumnDescription 
        public string TRIAL732 {get { return this._TRIAL732; } set { this._TRIAL732 = value; this.OnPropertyChanged(); }}
    }
}
