﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("polizas20")]
    public partial class POLIZAS20
    {
           public POLIZAS20(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("TIPO_POLI")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="tipo_poli")]ColumnDescription 
        public string TIPO_POLI {get { return this._TIPO_POLI; } set { this._TIPO_POLI = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_POLIZ")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="num_poliz")]ColumnDescription 
        public string NUM_POLIZ {get { return this._NUM_POLIZ; } set { this._NUM_POLIZ = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("PERIODO")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="periodo")]ColumnDescription 
        public int PERIODO {get { return this._PERIODO; } set { this._PERIODO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("EJERCICIO")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="ejercicio")]ColumnDescription 
        public int EJERCICIO {get { return this._EJERCICIO; } set { this._EJERCICIO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FECHA_POL")] 
           [SugarColumn(ColumnName="fecha_pol")]ColumnDescription 
        public DateTime? FECHA_POL {get { return this._FECHA_POL; } set { this._FECHA_POL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CONCEP_PO")] 
           [SugarColumn(ColumnName="concep_po")]ColumnDescription 
        public string CONCEP_PO {get { return this._CONCEP_PO; } set { this._CONCEP_PO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NUM_PART")] 
           [SugarColumn(ColumnName="num_part")]ColumnDescription 
        public int? NUM_PART {get { return this._NUM_PART; } set { this._NUM_PART = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("LOGAUDITA")] 
           [SugarColumn(ColumnName="logaudita")]ColumnDescription 
        public string LOGAUDITA {get { return this._LOGAUDITA; } set { this._LOGAUDITA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CONTABILIZ")] 
           [SugarColumn(ColumnName="contabiliz")]ColumnDescription 
        public string CONTABILIZ {get { return this._CONTABILIZ; } set { this._CONTABILIZ = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NUMPARCUA")] 
           [SugarColumn(ColumnName="numparcua")]ColumnDescription 
        public int? NUMPARCUA {get { return this._NUMPARCUA; } set { this._NUMPARCUA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIENEDOCUMENTOS")] 
           [SugarColumn(ColumnName="tienedocumentos")]ColumnDescription 
        public int? TIENEDOCUMENTOS {get { return this._TIENEDOCUMENTOS; } set { this._TIENEDOCUMENTOS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PROCCONTAB")] 
           [SugarColumn(ColumnName="proccontab")]ColumnDescription 
        public int? PROCCONTAB {get { return this._PROCCONTAB; } set { this._PROCCONTAB = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ORIGEN")] 
           [SugarColumn(ColumnName="origen")]ColumnDescription 
        public string ORIGEN {get { return this._ORIGEN; } set { this._ORIGEN = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("UUID")] 
           [SugarColumn(ColumnName="uuid")]ColumnDescription 
        public string UUID {get { return this._UUID; } set { this._UUID = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ESPOLIZAPRIVADA")] 
           [SugarColumn(ColumnName="espolizaprivada")]ColumnDescription 
        public int? ESPOLIZAPRIVADA {get { return this._ESPOLIZAPRIVADA; } set { this._ESPOLIZAPRIVADA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("UUIDOP")] 
           [SugarColumn(ColumnName="uuidop")]ColumnDescription 
        public string UUIDOP {get { return this._UUIDOP; } set { this._UUIDOP = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL736")] 
           [SugarColumn(ColumnName="trial736")]ColumnDescription 
        public string TRIAL736 {get { return this._TRIAL736; } set { this._TRIAL736 = value; this.OnPropertyChanged(); }}
    }
}
