﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///AUXILIAR20
   ///</summary>
   [SugarTable("auxiliar20")]
   public partial class AUXILIAR20
   {
      public AUXILIAR20(){
      }

      private string _TIPO_POLI;
      private string _NUM_POLIZ;
      private double _NUM_PART;
      private int _PERIODO;
      private int _EJERCICIO;
      private string _NUM_CTA;
      private DateTime? _FECHA_POL;
      private string _CONCEP_PO;
      private string _DEBE_HABER;
      private double? _MONTOMOV;
      private int? _NUMDEPTO;
      private double? _TIPCAMBIO;
      private int? _CONTRAPAR;
      private int? _ORDEN;
      private int? _CCOSTOS;
      private int? _CGRUPOS;
      private int? _IDINFADIPAR;
      private int? _IDUUID;
      
      /// <summary>
      /// obtener o establecer TIPO_POLI
      /// </summary>
      [DataNames("TIPO_POLI")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "tipo_poli", ColumnDescription = "Tipo", IsNullable = false, Length = 2)] 
      public string TIPO_POLI {get { return this._TIPO_POLI; } set { this._TIPO_POLI = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_POLIZ
      /// </summary>
      [DataNames("NUM_POLIZ")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "num_poliz", ColumnDescription = "Numero", IsNullable = false, Length = 5)] 
      public string NUM_POLIZ {get { return this._NUM_POLIZ; } set { this._NUM_POLIZ = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_PART
      /// </summary>
      [DataNames("NUM_PART")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "num_part", ColumnDescription = "Numero de la partida", IsNullable = false)] 
      public double NUM_PART {get { return this._NUM_PART; } set { this._NUM_PART = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PERIODO
      /// </summary>
      [DataNames("PERIODO")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "periodo", ColumnDescription = "Periodo", IsNullable = false)] 
      public int PERIODO {get { return this._PERIODO; } set { this._PERIODO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer EJERCICIO
      /// </summary>
      [DataNames("EJERCICIO")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "ejercicio", ColumnDescription = "Ejercicio", IsNullable = false)] 
      public int EJERCICIO {get { return this._EJERCICIO; } set { this._EJERCICIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUM_CTA
      /// </summary>
      [DataNames("NUM_CTA")] 
      [SugarColumn(ColumnName = "num_cta", ColumnDescription = "Numero de la cuenta", Length = 21)] 
      public string NUM_CTA {get { return this._NUM_CTA; } set { this._NUM_CTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHA_POL
      /// </summary>
      [DataNames("FECHA_POL")] 
      [SugarColumn(ColumnName = "fecha_pol", ColumnDescription = "Fecha de registro")] 
      public DateTime? FECHA_POL {get { return this._FECHA_POL; } set { this._FECHA_POL = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CONCEP_PO
      /// </summary>
      [DataNames("CONCEP_PO")] 
      [SugarColumn(ColumnName = "concep_po", ColumnDescription = "Concepto", Length = 120)] 
      public string CONCEP_PO {get { return this._CONCEP_PO; } set { this._CONCEP_PO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DEBE_HABER
      /// </summary>
      [DataNames("DEBE_HABER")] 
      [SugarColumn(ColumnName = "debe_haber", ColumnDescription = "Abono o cargo", Length = 1)] 
      public string DEBE_HABER {get { return this._DEBE_HABER; } set { this._DEBE_HABER = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTOMOV
      /// </summary>
      [DataNames("MONTOMOV")] 
      [SugarColumn(ColumnName = "montomov", ColumnDescription = "Monto movimiento")] 
      public double? MONTOMOV {get { return this._MONTOMOV; } set { this._MONTOMOV = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMDEPTO
      /// </summary>
      [DataNames("NUMDEPTO")] 
      [SugarColumn(ColumnName = "numdepto", ColumnDescription = "Numero depto")] 
      public int? NUMDEPTO {get { return this._NUMDEPTO; } set { this._NUMDEPTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPCAMBIO
      /// </summary>
      [DataNames("TIPCAMBIO")] 
      [SugarColumn(ColumnName = "tipcambio", ColumnDescription = "Tipo cambio")] 
      public double? TIPCAMBIO {get { return this._TIPCAMBIO; } set { this._TIPCAMBIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CONTRAPAR
      /// </summary>
      [DataNames("CONTRAPAR")] 
      [SugarColumn(ColumnName = "contrapar", ColumnDescription = "Contrapartida")] 
      public int? CONTRAPAR {get { return this._CONTRAPAR; } set { this._CONTRAPAR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer ORDEN
      /// </summary>
      [DataNames("ORDEN")] 
      [SugarColumn(ColumnName = "orden", ColumnDescription = "Orden de la partida")] 
      public int? ORDEN {get { return this._ORDEN; } set { this._ORDEN = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CCOSTOS
      /// </summary>
      [DataNames("CCOSTOS")] 
      [SugarColumn(ColumnName = "ccostos", ColumnDescription = "Centro de costos")] 
      public int? CCOSTOS {get { return this._CCOSTOS; } set { this._CCOSTOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CGRUPOS
      /// </summary>
      [DataNames("CGRUPOS")] 
      [SugarColumn(ColumnName = "cgrupos", ColumnDescription = "Grupos")] 
      public int? CGRUPOS {get { return this._CGRUPOS; } set { this._CGRUPOS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IDINFADIPAR
      /// </summary>
      [DataNames("IDINFADIPAR")] 
      [SugarColumn(ColumnName = "idinfadipar", ColumnDescription = "Numero de registro de forma de pago")] 
      public int? IDINFADIPAR {get { return this._IDINFADIPAR; } set { this._IDINFADIPAR = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer IDUUID
      /// </summary>
      [DataNames("IDUUID")] 
      [SugarColumn(ColumnName = "iduuid", ColumnDescription = "Numero de registro de comprobantes")] 
      public int? IDUUID {get { return this._IDUUID; } set { this._IDUUID = value; this.OnPropertyChanged(); }}
   }
}
