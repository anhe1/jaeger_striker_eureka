﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///CTARUB
   ///</summary>
   [SugarTable("ctarub")]
   public partial class CTARUB
   {
      public CTARUB(){
      }

      private int _RUBRO;
      private string _CUENTA;
      private string _SEGPAPA;

      /// <summary>
      /// obtener o establecer RUBRO
      /// </summary>
      [DataNames("RUBRO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "rubro", IsNullable = false, ColumnDescription = "Rubro")]
      public int RUBRO {get { return this._RUBRO; } set { this._RUBRO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer CUENTA
      /// </summary>
      [DataNames("CUENTA")]
      [SugarColumn(IsPrimaryKey=true,ColumnName = "cuenta", ColumnDescription = "Cuenta", IsNullable = false, Length = 21)]
      public string CUENTA {get { return this._CUENTA; } set { this._CUENTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer SEGPAPA
      /// </summary>
      [DataNames("SEGPAPA")]
      [SugarColumn(ColumnName = "segpapa", ColumnDescription = "Cuenta papa", Length = 21)]
      public string SEGPAPA {get { return this._SEGPAPA; } set { this._SEGPAPA = value; this.OnPropertyChanged(); }}
   }
}