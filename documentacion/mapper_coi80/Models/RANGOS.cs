﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("rangos")]
    public partial class RANGOS
    {
           public RANGOS(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("RANGO")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="rango")]ColumnDescription 
        public int RANGO {get { return this._RANGO; } set { this._RANGO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DESCRIP")] 
           [SugarColumn(ColumnName="descrip")]ColumnDescription 
        public string DESCRIP {get { return this._DESCRIP; } set { this._DESCRIP = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL736")] 
           [SugarColumn(ColumnName="trial736")]ColumnDescription 
        public string TRIAL736 {get { return this._TRIAL736; } set { this._TRIAL736 = value; this.OnPropertyChanged(); }}
    }
}
