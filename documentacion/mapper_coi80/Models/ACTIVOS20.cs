﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///ACTIVOS20
   ///</summary>
   [SugarTable("activos20")]
   public partial class ACTIVOS20
   {
      public ACTIVOS20(){
      }

      private string _CLAVE;
      private int? _TIPO;
      private string _DESCRIP;
      private string _LOCALIZ;
      private double? _MONTORIG;
      private double? _MONTORIGEX;
      private DateTime? _FECHAADQ;
      private double? _MAXDED;
      private double? _VALMER;
      private double? _DEPACU;
      private double? _VIDAUT;
      private string _METDEP;
      private double? _TASDEP;
      private double? _TASDEPFIS;
      private string _STATUS;
      private DateTime? _FECHAELIM;
      private int? _NUMDEPTO;
      private DateTime? _FECHAULTRE;
      private string _NUMSERIE;
      private string _BANDEDINM;
      private DateTime? _FECINIDEP;
      private DateTime? _FECINIDEPF;
      private double? _DEPACUFISC;
      private string _OBSERVACIO;
      private double? _TIPOCAMBIO;
      private string _POLIZASEG;
      private string _COMPASEGU;
      private string _AGENTESEG;
      private string _TELSINIES;
      private string _TIPOCOBER;
      private double? _MONTOASEG;
      private double? _PRIMATOTA;
      private double? _DEDUCIBLE;
      private DateTime? _FECVIGENC;
      private DateTime? _FECPROXMA;
      private DateTime? _FECULTMAN;
      private int? _PERIODOMA;
      private double? _COSTOMANT;
      private string _RESPACTFIJO;

      /// <summary>
      /// obtener o establecer CLAVE
      /// </summary>
      [DataNames("CLAVE")] 
      [SugarColumn(IsPrimaryKey = true, ColumnName = "clave", ColumnDescription = "Clave", Length = 16)] 
      public string CLAVE {get { return this._CLAVE; } set { this._CLAVE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPO
      /// </summary>
      [DataNames("TIPO")] 
      [SugarColumn(ColumnName = "tipo", ColumnDescription = "Tipo")] 
      public int? TIPO {get { return this._TIPO; } set { this._TIPO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DESCRIP
      /// </summary>
      [DataNames("DESCRIP")] 
      [SugarColumn(ColumnName = "descrip", ColumnDescription = "Descripcion", IsNullable = false, Length = 30)] 
      public string DESCRIP {get { return this._DESCRIP; } set { this._DESCRIP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer LOCALIZ
      /// </summary>
      [DataNames("LOCALIZ")] 
      [SugarColumn(ColumnName = "localiz", ColumnDescription = "Localozacion", Length = 9)] 
      public string LOCALIZ {get { return this._LOCALIZ; } set { this._LOCALIZ = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTORIG
      /// </summary>
      [DataNames("MONTORIG")] 
      [SugarColumn(ColumnName = "montorig", ColumnDescription = "Monto original")] 
      public double? MONTORIG {get { return this._MONTORIG; } set { this._MONTORIG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTORIGEX
      /// </summary>
      [DataNames("MONTORIGEX")] 
      [SugarColumn(ColumnName = "montorigex", ColumnDescription = "Monto original extranjero")] 
      public double? MONTORIGEX {get { return this._MONTORIGEX; } set { this._MONTORIGEX = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHAADQ
      /// </summary>
      [DataNames("FECHAADQ")] 
      [SugarColumn(ColumnName = "fechaadq", ColumnDescription = "Fecha de adquisicion")] 
      public DateTime? FECHAADQ {get { return this._FECHAADQ; } set { this._FECHAADQ = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MAXDED
      /// </summary>
      [DataNames("MAXDED")] 
      [SugarColumn(ColumnName = "maxded", ColumnDescription = "Maximo deducible")] 
      public double? MAXDED {get { return this._MAXDED; } set { this._MAXDED = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer VALMER
      /// </summary>
      [DataNames("VALMER")] 
      [SugarColumn(ColumnName = "valmer", ColumnDescription = "Valor del mercado")] 
      public double? VALMER {get { return this._VALMER; } set { this._VALMER = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DEPACU
      /// </summary>
      [DataNames("DEPACU")] 
      [SugarColumn(ColumnName = "depacu", ColumnDescription = "Depreciacion acumulada")] 
      public double? DEPACU {get { return this._DEPACU; } set { this._DEPACU = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer VIDAUT
      /// </summary>
      [DataNames("VIDAUT")] 
      [SugarColumn(ColumnName = "vidaut", ColumnDescription = "Vida util")] 
      public double? VIDAUT {get { return this._VIDAUT; } set { this._VIDAUT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer METDEP
      /// </summary>
      [DataNames("METDEP")] 
      [SugarColumn(ColumnName = "metdep", ColumnDescription = "Metodo de depreciacion", Length = 1)] 
      public string METDEP {get { return this._METDEP; } set { this._METDEP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TASDEP
      /// </summary>
      [DataNames("TASDEP")] 
      [SugarColumn(ColumnName = "tasdep", ColumnDescription = "Tasa de depreciacion")] 
      public double? TASDEP {get { return this._TASDEP; } set { this._TASDEP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TASDEPFIS
      /// </summary>
      [DataNames("TASDEPFIS")] 
      [SugarColumn(ColumnName = "tasdepfis", ColumnDescription = "Tasa de depreciacion fiscal")] 
      public double? TASDEPFIS {get { return this._TASDEPFIS; } set { this._TASDEPFIS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer STATUS
      /// </summary>
      [DataNames("STATUS")] 
      [SugarColumn(ColumnName = "status", ColumnDescription = "Estatus", Length = 1)] 
      public string STATUS {get { return this._STATUS; } set { this._STATUS = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHAELIM
      /// </summary>
      [DataNames("FECHAELIM")] 
      [SugarColumn(ColumnName = "fechaelim", ColumnDescription = "Fecha eliminacion")] 
      public DateTime? FECHAELIM {get { return this._FECHAELIM; } set { this._FECHAELIM = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMDEPTO
      /// </summary>
      [DataNames("NUMDEPTO")] 
      [SugarColumn(ColumnName = "numdepto", ColumnDescription = "Numero de depto")] 
      public int? NUMDEPTO {get { return this._NUMDEPTO; } set { this._NUMDEPTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECHAULTRE
      /// </summary>
      [DataNames("FECHAULTRE")] 
      [SugarColumn(ColumnName = "fechaultre", ColumnDescription = "Fecha ultima revaluacion")] 
      public DateTime? FECHAULTRE {get { return this._FECHAULTRE; } set { this._FECHAULTRE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer NUMSERIE
      /// </summary>
      [DataNames("NUMSERIE")] 
      [SugarColumn(ColumnName = "numserie", ColumnDescription = "Numero de serie", Length = 24)] 
      public string NUMSERIE {get { return this._NUMSERIE; } set { this._NUMSERIE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer BANDEDINM
      /// </summary>
      [DataNames("BANDEDINM")] 
      [SugarColumn(ColumnName = "bandedinm", ColumnDescription = "Deduccion inmediata", Length = 1)] 
      public string BANDEDINM {get { return this._BANDEDINM; } set { this._BANDEDINM = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECINIDEP
      /// </summary>
      [DataNames("FECINIDEP")] 
      [SugarColumn(ColumnName = "fecinidep", ColumnDescription = "Fecha inicio depreciacion")] 
      public DateTime? FECINIDEP {get { return this._FECINIDEP; } set { this._FECINIDEP = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECINIDEPF
      /// </summary>
      [DataNames("FECINIDEPF")] 
      [SugarColumn(ColumnName = "fecinidepf", ColumnDescription = "Fecha depreciacion fiscal")] 
      public DateTime? FECINIDEPF {get { return this._FECINIDEPF; } set { this._FECINIDEPF = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DEPACUFISC
      /// </summary>
      [DataNames("DEPACUFISC")] 
      [SugarColumn(ColumnName = "depacufisc", ColumnDescription = "Depreciacion acumulada fiscal")] 
      public double? DEPACUFISC {get { return this._DEPACUFISC; } set { this._DEPACUFISC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer OBSERVACIO
      /// </summary>
      [DataNames("OBSERVACIO")] 
      [SugarColumn(ColumnName = "observacio", ColumnDescription = "Observaciones", Length = 250)] 
      public string OBSERVACIO {get { return this._OBSERVACIO; } set { this._OBSERVACIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOCAMBIO
      /// </summary>
      [DataNames("TIPOCAMBIO")] 
      [SugarColumn(ColumnName = "tipocambio", ColumnDescription = "Tipo de cambio")] 
      public double? TIPOCAMBIO {get { return this._TIPOCAMBIO; } set { this._TIPOCAMBIO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer POLIZASEG
      /// </summary>
      [DataNames("POLIZASEG")] 
      [SugarColumn(ColumnName = "polizaseg", ColumnDescription = "Poliza de seguro", Length = 20)] 
      public string POLIZASEG {get { return this._POLIZASEG; } set { this._POLIZASEG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer COMPASEGU
      /// </summary>
      [DataNames("COMPASEGU")] 
      [SugarColumn(ColumnName = "compasegu", ColumnDescription = "Compania asegurada", Length = 40)] 
      public string COMPASEGU {get { return this._COMPASEGU; } set { this._COMPASEGU = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer AGENTESEG
      /// </summary>
      [DataNames("AGENTESEG")] 
      [SugarColumn(ColumnName = "agenteseg", ColumnDescription = "Agente de seguro", Length = 40)] 
      public string AGENTESEG {get { return this._AGENTESEG; } set { this._AGENTESEG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TELSINIES
      /// </summary>
      [DataNames("TELSINIES")] 
      [SugarColumn(ColumnName = "telsinies", ColumnDescription = "Telefono sieniestro", Length = 15)] 
      public string TELSINIES {get { return this._TELSINIES; } set { this._TELSINIES = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer TIPOCOBER
      /// </summary>
      [DataNames("TIPOCOBER")] 
      [SugarColumn(ColumnName = "tipocober", ColumnDescription = "Tipo cobertura", Length = 20)] 
      public string TIPOCOBER {get { return this._TIPOCOBER; } set { this._TIPOCOBER = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer MONTOASEG
      /// </summary>
      [DataNames("MONTOASEG")] 
      [SugarColumn(ColumnName = "montoaseg", ColumnDescription = "Monto asegurado")] 
      public double? MONTOASEG {get { return this._MONTOASEG; } set { this._MONTOASEG = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PRIMATOTA
      /// </summary>
      [DataNames("PRIMATOTA")] 
      [SugarColumn(ColumnName = "primatota", ColumnDescription = "Prima total")] 
      public double? PRIMATOTA {get { return this._PRIMATOTA; } set { this._PRIMATOTA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DEDUCIBLE
      /// </summary>
      [DataNames("DEDUCIBLE")] 
      [SugarColumn(ColumnName = "deducible", ColumnDescription = "Deducible")] 
      public double? DEDUCIBLE {get { return this._DEDUCIBLE; } set { this._DEDUCIBLE = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECVIGENC
      /// </summary>
      [DataNames("FECVIGENC")] 
      [SugarColumn(ColumnName = "fecvigenc", ColumnDescription = "Fecha vigencia")] 
      public DateTime? FECVIGENC {get { return this._FECVIGENC; } set { this._FECVIGENC = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECPROXMA
      /// </summary>
      [DataNames("FECPROXMA")] 
      [SugarColumn(ColumnName = "fecproxma", ColumnDescription = "Fecha proxima mantenimiento")] 
      public DateTime? FECPROXMA {get { return this._FECPROXMA; } set { this._FECPROXMA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer FECULTMAN
      /// </summary>
      [DataNames("FECULTMAN")] 
      [SugarColumn(ColumnName = "fecultman", ColumnDescription = "Fecha ultimo mantenimiento")] 
      public DateTime? FECULTMAN {get { return this._FECULTMAN; } set { this._FECULTMAN = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer PERIODOMA
      /// </summary>
      [DataNames("PERIODOMA")] 
      [SugarColumn(ColumnName = "periodoma", ColumnDescription = "Periodo mantenimiento")] 
      public int? PERIODOMA {get { return this._PERIODOMA; } set { this._PERIODOMA = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer COSTOMANT
      /// </summary>
      [DataNames("COSTOMANT")] 
      [SugarColumn(ColumnName = "costomant", ColumnDescription = "Costo mantenimiento")] 
      public double? COSTOMANT {get { return this._COSTOMANT; } set { this._COSTOMANT = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer RESPACTFIJO
      /// </summary>
      [DataNames("RESPACTFIJO")] 
      [SugarColumn(ColumnName = "respactfijo", ColumnDescription = "Responsable del activo fijo", Length = 60)] 
      public string RESPACTFIJO {get { return this._RESPACTFIJO; } set { this._RESPACTFIJO = value; this.OnPropertyChanged(); }}
   }
}
