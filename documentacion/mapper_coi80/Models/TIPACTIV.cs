﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("tipactiv")]
    public partial class TIPACTIV
    {
           public TIPACTIV(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("CLAVE")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="clave")]ColumnDescription 
        public int CLAVE {get { return this._CLAVE; } set { this._CLAVE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DESCRIP")] 
           [SugarColumn(ColumnName="descrip")]ColumnDescription 
        public string DESCRIP {get { return this._DESCRIP; } set { this._DESCRIP = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DEDNORMAL")] 
           [SugarColumn(ColumnName="dednormal")]ColumnDescription 
        public double? DEDNORMAL {get { return this._DEDNORMAL; } set { this._DEDNORMAL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DEDIMED")] 
           [SugarColumn(ColumnName="dedimed")]ColumnDescription 
        public double? DEDIMED {get { return this._DEDIMED; } set { this._DEDIMED = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MAXDED")] 
           [SugarColumn(ColumnName="maxded")]ColumnDescription 
        public double? MAXDED {get { return this._MAXDED; } set { this._MAXDED = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("METODODEP")] 
           [SugarColumn(ColumnName="metododep")]ColumnDescription 
        public string METODODEP {get { return this._METODODEP; } set { this._METODODEP = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DEPPROY")] 
           [SugarColumn(ColumnName="depproy")]ColumnDescription 
        public double? DEPPROY {get { return this._DEPPROY; } set { this._DEPPROY = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTAAACTV")] 
           [SugarColumn(ColumnName="ctaaactv")]ColumnDescription 
        public string CTAAACTV {get { return this._CTAAACTV; } set { this._CTAAACTV = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTADEPRE")] 
           [SugarColumn(ColumnName="ctadepre")]ColumnDescription 
        public string CTADEPRE {get { return this._CTADEPRE; } set { this._CTADEPRE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTAGASTOS")] 
           [SugarColumn(ColumnName="ctagastos")]ColumnDescription 
        public string CTAGASTOS {get { return this._CTAGASTOS; } set { this._CTAGASTOS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTAPERGAN")] 
           [SugarColumn(ColumnName="ctapergan")]ColumnDescription 
        public string CTAPERGAN {get { return this._CTAPERGAN; } set { this._CTAPERGAN = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("QUEMONEDA")] 
           [SugarColumn(ColumnName="quemoneda")]ColumnDescription 
        public int? QUEMONEDA {get { return this._QUEMONEDA; } set { this._QUEMONEDA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL739")] 
           [SugarColumn(ColumnName="trial739")]ColumnDescription 
        public string TRIAL739 {get { return this._TRIAL739; } set { this._TRIAL739 = value; this.OnPropertyChanged(); }}
    }
}
