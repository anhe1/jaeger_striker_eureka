﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
   ///<summary>
   ///CONCEPTOfla
   ///</summary>
   [SugarTable("concepto")]
   public partial class CONCEPTO
   {
      public CONCEPTO(){
      }

      private int _CONCEPTO;
      private string _DESCRIP;

      /// <summary>
      /// obtener o establecer CONCEPTO
      /// </summary>
      [DataNames("CONCEPTO")]
      [SugarColumn(IsPrimaryKey = true, ColumnName = "concepto", ColumnDescription = "Conepto", IsNullable = false)] 
      public int CONCEPTO {get { return this._CONCEPTO; } set { this._CONCEPTO = value; this.OnPropertyChanged(); }}
      /// <summary>
      /// obtener o establecer DESCRIP
      /// </summary>
      [DataNames("DESCRIP")]
      [SugarColumn(ColumnName = "descrip", ColumnDescription = "Descripcion", Length = 120)] 
      public string DESCRIP {get { return this._DESCRIP; } set { this._DESCRIP = value; this.OnPropertyChanged(); }}
   }
}
