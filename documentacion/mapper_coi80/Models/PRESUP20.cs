﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("presup20")]
    public partial class PRESUP20
    {
           public PRESUP20(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("EJERCICIO")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="ejercicio")]ColumnDescription 
        public int EJERCICIO {get { return this._EJERCICIO; } set { this._EJERCICIO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_CTA")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="num_cta")]ColumnDescription 
        public string NUM_CTA {get { return this._NUM_CTA; } set { this._NUM_CTA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PRESUP01")] 
           [SugarColumn(ColumnName="presup01")]ColumnDescription 
        public double? PRESUP01 {get { return this._PRESUP01; } set { this._PRESUP01 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PRESUP02")] 
           [SugarColumn(ColumnName="presup02")]ColumnDescription 
        public double? PRESUP02 {get { return this._PRESUP02; } set { this._PRESUP02 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PRESUP03")] 
           [SugarColumn(ColumnName="presup03")]ColumnDescription 
        public double? PRESUP03 {get { return this._PRESUP03; } set { this._PRESUP03 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PRESUP04")] 
           [SugarColumn(ColumnName="presup04")]ColumnDescription 
        public double? PRESUP04 {get { return this._PRESUP04; } set { this._PRESUP04 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PRESUP05")] 
           [SugarColumn(ColumnName="presup05")]ColumnDescription 
        public double? PRESUP05 {get { return this._PRESUP05; } set { this._PRESUP05 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PRESUP06")] 
           [SugarColumn(ColumnName="presup06")]ColumnDescription 
        public double? PRESUP06 {get { return this._PRESUP06; } set { this._PRESUP06 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PRESUP07")] 
           [SugarColumn(ColumnName="presup07")]ColumnDescription 
        public double? PRESUP07 {get { return this._PRESUP07; } set { this._PRESUP07 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PRESUP08")] 
           [SugarColumn(ColumnName="presup08")]ColumnDescription 
        public double? PRESUP08 {get { return this._PRESUP08; } set { this._PRESUP08 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PRESUP09")] 
           [SugarColumn(ColumnName="presup09")]ColumnDescription 
        public double? PRESUP09 {get { return this._PRESUP09; } set { this._PRESUP09 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PRESUP10")] 
           [SugarColumn(ColumnName="presup10")]ColumnDescription 
        public double? PRESUP10 {get { return this._PRESUP10; } set { this._PRESUP10 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PRESUP11")] 
           [SugarColumn(ColumnName="presup11")]ColumnDescription 
        public double? PRESUP11 {get { return this._PRESUP11; } set { this._PRESUP11 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PRESUP12")] 
           [SugarColumn(ColumnName="presup12")]ColumnDescription 
        public double? PRESUP12 {get { return this._PRESUP12; } set { this._PRESUP12 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PRESUP13")] 
           [SugarColumn(ColumnName="presup13")]ColumnDescription 
        public double? PRESUP13 {get { return this._PRESUP13; } set { this._PRESUP13 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PRESUP14")] 
           [SugarColumn(ColumnName="presup14")]ColumnDescription 
        public double? PRESUP14 {get { return this._PRESUP14; } set { this._PRESUP14 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL736")] 
           [SugarColumn(ColumnName="trial736")]ColumnDescription 
        public string TRIAL736 {get { return this._TRIAL736; } set { this._TRIAL736 = value; this.OnPropertyChanged(); }}
    }
}
