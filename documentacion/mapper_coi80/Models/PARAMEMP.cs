﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Models
{
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("paramemp")]
    public partial class PARAMEMP
    {
           public PARAMEMP(){


           }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("IDEMP")] 
           [SugarColumn(IsPrimaryKey=true,ColumnName="idemp")]ColumnDescription 
        public int IDEMP {get { return this._IDEMP; } set { this._IDEMP = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DIREC")] 
           [SugarColumn(ColumnName="direc")]ColumnDescription 
        public string DIREC {get { return this._DIREC; } set { this._DIREC = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("POBL")] 
           [SugarColumn(ColumnName="pobl")]ColumnDescription 
        public string POBL {get { return this._POBL; } set { this._POBL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COD_POS")] 
           [SugarColumn(ColumnName="cod_pos")]ColumnDescription 
        public string COD_POS {get { return this._COD_POS; } set { this._COD_POS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RFC_EMP")] 
           [SugarColumn(ColumnName="rfc_emp")]ColumnDescription 
        public string RFC_EMP {get { return this._RFC_EMP; } set { this._RFC_EMP = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CED_EMP")] 
           [SugarColumn(ColumnName="ced_emp")]ColumnDescription 
        public string CED_EMP {get { return this._CED_EMP; } set { this._CED_EMP = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DIGCTA1")] 
           [SugarColumn(ColumnName="digcta1")]ColumnDescription 
        public int? DIGCTA1 {get { return this._DIGCTA1; } set { this._DIGCTA1 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DIGCTA2")] 
           [SugarColumn(ColumnName="digcta2")]ColumnDescription 
        public int? DIGCTA2 {get { return this._DIGCTA2; } set { this._DIGCTA2 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DIGCTA3")] 
           [SugarColumn(ColumnName="digcta3")]ColumnDescription 
        public int? DIGCTA3 {get { return this._DIGCTA3; } set { this._DIGCTA3 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DIGCTA4")] 
           [SugarColumn(ColumnName="digcta4")]ColumnDescription 
        public int? DIGCTA4 {get { return this._DIGCTA4; } set { this._DIGCTA4 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DIGCTA5")] 
           [SugarColumn(ColumnName="digcta5")]ColumnDescription 
        public int? DIGCTA5 {get { return this._DIGCTA5; } set { this._DIGCTA5 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DIGCTA6")] 
           [SugarColumn(ColumnName="digcta6")]ColumnDescription 
        public int? DIGCTA6 {get { return this._DIGCTA6; } set { this._DIGCTA6 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DIGCTA7")] 
           [SugarColumn(ColumnName="digcta7")]ColumnDescription 
        public int? DIGCTA7 {get { return this._DIGCTA7; } set { this._DIGCTA7 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DIGCTA8")] 
           [SugarColumn(ColumnName="digcta8")]ColumnDescription 
        public int? DIGCTA8 {get { return this._DIGCTA8; } set { this._DIGCTA8 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DIGCTA9")] 
           [SugarColumn(ColumnName="digcta9")]ColumnDescription 
        public int? DIGCTA9 {get { return this._DIGCTA9; } set { this._DIGCTA9 = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NIVELACTU")] 
           [SugarColumn(ColumnName="nivelactu")]ColumnDescription 
        public int? NIVELACTU {get { return this._NIVELACTU; } set { this._NIVELACTU = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("GUION_SINO")] 
           [SugarColumn(ColumnName="guion_sino")]ColumnDescription 
        public string GUION_SINO {get { return this._GUION_SINO; } set { this._GUION_SINO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("REDO_P")] 
           [SugarColumn(ColumnName="redo_p")]ColumnDescription 
        public string REDO_P {get { return this._REDO_P; } set { this._REDO_P = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CAPSALINI")] 
           [SugarColumn(ColumnName="capsalini")]ColumnDescription 
        public string CAPSALINI {get { return this._CAPSALINI; } set { this._CAPSALINI = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ASIGSECPOL")] 
           [SugarColumn(ColumnName="asigsecpol")]ColumnDescription 
        public string ASIGSECPOL {get { return this._ASIGSECPOL; } set { this._ASIGSECPOL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CONTABLINEA")] 
           [SugarColumn(ColumnName="contablinea")]ColumnDescription 
        public string CONTABLINEA {get { return this._CONTABLINEA; } set { this._CONTABLINEA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MESCIERRE")] 
           [SugarColumn(ColumnName="mescierre")]ColumnDescription 
        public int? MESCIERRE {get { return this._MESCIERRE; } set { this._MESCIERRE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("SUBDIR")] 
           [SugarColumn(ColumnName="subdir")]ColumnDescription 
        public string SUBDIR {get { return this._SUBDIR; } set { this._SUBDIR = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CAPCTASPOL")] 
           [SugarColumn(ColumnName="capctaspol")]ColumnDescription 
        public string CAPCTASPOL {get { return this._CAPCTASPOL; } set { this._CAPCTASPOL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DEPTO_SINO")] 
           [SugarColumn(ColumnName="depto_sino")]ColumnDescription 
        public string DEPTO_SINO {get { return this._DEPTO_SINO; } set { this._DEPTO_SINO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRASPASO")] 
           [SugarColumn(ColumnName="traspaso")]ColumnDescription 
        public string TRASPASO {get { return this._TRASPASO; } set { this._TRASPASO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTMONEXT")] 
           [SugarColumn(ColumnName="ctmonext")]ColumnDescription 
        public string CTMONEXT {get { return this._CTMONEXT; } set { this._CTMONEXT = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CSTIPOCMB")] 
           [SugarColumn(ColumnName="cstipocmb")]ColumnDescription 
        public string CSTIPOCMB {get { return this._CSTIPOCMB; } set { this._CSTIPOCMB = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CPTIPOCMB")] 
           [SugarColumn(ColumnName="cptipocmb")]ColumnDescription 
        public string CPTIPOCMB {get { return this._CPTIPOCMB; } set { this._CPTIPOCMB = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CQUEMONEXT")] 
           [SugarColumn(ColumnName="cquemonext")]ColumnDescription 
        public string CQUEMONEXT {get { return this._CQUEMONEXT; } set { this._CQUEMONEXT = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CQUEMONCTB")] 
           [SugarColumn(ColumnName="cquemonctb")]ColumnDescription 
        public string CQUEMONCTB {get { return this._CQUEMONCTB; } set { this._CQUEMONCTB = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTAIVA")] 
           [SugarColumn(ColumnName="ctaiva")]ColumnDescription 
        public string CTAIVA {get { return this._CTAIVA; } set { this._CTAIVA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTABANCO")] 
           [SugarColumn(ColumnName="ctabanco")]ColumnDescription 
        public string CTABANCO {get { return this._CTABANCO; } set { this._CTABANCO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CONMONTPOL")] 
           [SugarColumn(ColumnName="conmontpol")]ColumnDescription 
        public int? CONMONTPOL {get { return this._CONMONTPOL; } set { this._CONMONTPOL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DRIVERCOI")] 
           [SugarColumn(ColumnName="drivercoi")]ColumnDescription 
        public int? DRIVERCOI {get { return this._DRIVERCOI; } set { this._DRIVERCOI = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ALIAS")] 
           [SugarColumn(ColumnName="alias")]ColumnDescription 
        public string ALIAS {get { return this._ALIAS; } set { this._ALIAS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FOLIOUNICO")] 
           [SugarColumn(ColumnName="foliounico")]ColumnDescription 
        public int? FOLIOUNICO {get { return this._FOLIOUNICO; } set { this._FOLIOUNICO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CAPTURADOS")] 
           [SugarColumn(ColumnName="capturados")]ColumnDescription 
        public string CAPTURADOS {get { return this._CAPTURADOS; } set { this._CAPTURADOS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("POLDESCUAD")] 
           [SugarColumn(ColumnName="poldescuad")]ColumnDescription 
        public string POLDESCUAD {get { return this._POLDESCUAD; } set { this._POLDESCUAD = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DIRTRAB")] 
           [SugarColumn(ColumnName="dirtrab")]ColumnDescription 
        public string DIRTRAB {get { return this._DIRTRAB; } set { this._DIRTRAB = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVADEF")] 
           [SugarColumn(ColumnName="ivadef")]ColumnDescription 
        public string IVADEF {get { return this._IVADEF; } set { this._IVADEF = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVAENOPT")] 
           [SugarColumn(ColumnName="ivaenopt")]ColumnDescription 
        public int? IVAENOPT {get { return this._IVAENOPT; } set { this._IVAENOPT = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVAENINGR")] 
           [SugarColumn(ColumnName="ivaeningr")]ColumnDescription 
        public int? IVAENINGR {get { return this._IVAENINGR; } set { this._IVAENINGR = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("BUFFCUECUA")] 
           [SugarColumn(ColumnName="buffcuecua")]ColumnDescription 
        public string BUFFCUECUA {get { return this._BUFFCUECUA; } set { this._BUFFCUECUA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPOPERSONA")] 
           [SugarColumn(ColumnName="tipopersona")]ColumnDescription 
        public string TIPOPERSONA {get { return this._TIPOPERSONA; } set { this._TIPOPERSONA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NACIONALIDAD")] 
           [SugarColumn(ColumnName="nacionalidad")]ColumnDescription 
        public string NACIONALIDAD {get { return this._NACIONALIDAD; } set { this._NACIONALIDAD = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPOEMPRESA")] 
           [SugarColumn(ColumnName="tipoempresa")]ColumnDescription 
        public string TIPOEMPRESA {get { return this._TIPOEMPRESA; } set { this._TIPOEMPRESA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FCONSTITUCION")] 
           [SugarColumn(ColumnName="fconstitucion")]ColumnDescription 
        public string FCONSTITUCION {get { return this._FCONSTITUCION; } set { this._FCONSTITUCION = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("REGIMEN")] 
           [SugarColumn(ColumnName="regimen")]ColumnDescription 
        public string REGIMEN {get { return this._REGIMEN; } set { this._REGIMEN = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NOMBREREPRE")] 
           [SugarColumn(ColumnName="nombrerepre")]ColumnDescription 
        public string NOMBREREPRE {get { return this._NOMBREREPRE; } set { this._NOMBREREPRE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RFCREPRE")] 
           [SugarColumn(ColumnName="rfcrepre")]ColumnDescription 
        public string RFCREPRE {get { return this._RFCREPRE; } set { this._RFCREPRE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CURPREPRE")] 
           [SugarColumn(ColumnName="curprepre")]ColumnDescription 
        public string CURPREPRE {get { return this._CURPREPRE; } set { this._CURPREPRE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PODERNOT")] 
           [SugarColumn(ColumnName="podernot")]ColumnDescription 
        public string PODERNOT {get { return this._PODERNOT; } set { this._PODERNOT = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRASPASOSALDOS")] 
           [SugarColumn(ColumnName="traspasosaldos")]ColumnDescription 
        public int? TRASPASOSALDOS {get { return this._TRASPASOSALDOS; } set { this._TRASPASOSALDOS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NOMBRE")] 
           [SugarColumn(ColumnName="nombre")]ColumnDescription 
        public string NOMBRE {get { return this._NOMBRE; } set { this._NOMBRE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CCOSTOS")] 
           [SugarColumn(ColumnName="ccostos")]ColumnDescription 
        public int? CCOSTOS {get { return this._CCOSTOS; } set { this._CCOSTOS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CGRUPOS")] 
           [SugarColumn(ColumnName="cgrupos")]ColumnDescription 
        public int? CGRUPOS {get { return this._CGRUPOS; } set { this._CGRUPOS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ETQCCOSTOS")] 
           [SugarColumn(ColumnName="etqccostos")]ColumnDescription 
        public string ETQCCOSTOS {get { return this._ETQCCOSTOS; } set { this._ETQCCOSTOS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ETQCGRUPOS")] 
           [SugarColumn(ColumnName="etqcgrupos")]ColumnDescription 
        public string ETQCGRUPOS {get { return this._ETQCGRUPOS; } set { this._ETQCGRUPOS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVADEFIETU")] 
           [SugarColumn(ColumnName="ivadefietu")]ColumnDescription 
        public string IVADEFIETU {get { return this._IVADEFIETU; } set { this._IVADEFIETU = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPODECAPTURADIOT")] 
           [SugarColumn(ColumnName="tipodecapturadiot")]ColumnDescription 
        public int? TIPODECAPTURADIOT {get { return this._TIPODECAPTURADIOT; } set { this._TIPODECAPTURADIOT = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPODECAPTURAIETU")] 
           [SugarColumn(ColumnName="tipodecapturaietu")]ColumnDescription 
        public int? TIPODECAPTURAIETU {get { return this._TIPODECAPTURAIETU; } set { this._TIPODECAPTURAIETU = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CUENTAIVAACREDITABLE")] 
           [SugarColumn(ColumnName="cuentaivaacreditable")]ColumnDescription 
        public string CUENTAIVAACREDITABLE {get { return this._CUENTAIVAACREDITABLE; } set { this._CUENTAIVAACREDITABLE = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CUENTAIVATRASLADADO")] 
           [SugarColumn(ColumnName="cuentaivatrasladado")]ColumnDescription 
        public string CUENTAIVATRASLADADO {get { return this._CUENTAIVATRASLADADO; } set { this._CUENTAIVATRASLADADO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPOPOLIZA")] 
           [SugarColumn(ColumnName="tipopoliza")]ColumnDescription 
        public string TIPOPOLIZA {get { return this._TIPOPOLIZA; } set { this._TIPOPOLIZA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPOPOLIZATER")] 
           [SugarColumn(ColumnName="tipopolizater")]ColumnDescription 
        public string TIPOPOLIZATER {get { return this._TIPOPOLIZATER; } set { this._TIPOPOLIZATER = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("GIRO")] 
           [SugarColumn(ColumnName="giro")]ColumnDescription 
        public string GIRO {get { return this._GIRO; } set { this._GIRO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DEV_COMPRAS")] 
           [SugarColumn(ColumnName="dev_compras")]ColumnDescription 
        public string DEV_COMPRAS {get { return this._DEV_COMPRAS; } set { this._DEV_COMPRAS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DEV_VENTAS")] 
           [SugarColumn(ColumnName="dev_ventas")]ColumnDescription 
        public string DEV_VENTAS {get { return this._DEV_VENTAS; } set { this._DEV_VENTAS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("GASTO_ACTIVO")] 
           [SugarColumn(ColumnName="gasto_activo")]ColumnDescription 
        public string GASTO_ACTIVO {get { return this._GASTO_ACTIVO; } set { this._GASTO_ACTIVO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IEPS_POR_ACREDITAR")] 
           [SugarColumn(ColumnName="ieps_por_acreditar")]ColumnDescription 
        public string IEPS_POR_ACREDITAR {get { return this._IEPS_POR_ACREDITAR; } set { this._IEPS_POR_ACREDITAR = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IEPS_POR_TRASLADAR")] 
           [SugarColumn(ColumnName="ieps_por_trasladar")]ColumnDescription 
        public string IEPS_POR_TRASLADAR {get { return this._IEPS_POR_TRASLADAR; } set { this._IEPS_POR_TRASLADAR = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IMP_LOCAL_RETENIDO")] 
           [SugarColumn(ColumnName="imp_local_retenido")]ColumnDescription 
        public string IMP_LOCAL_RETENIDO {get { return this._IMP_LOCAL_RETENIDO; } set { this._IMP_LOCAL_RETENIDO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IMP_LOCAL_TRASLADADO")] 
           [SugarColumn(ColumnName="imp_local_trasladado")]ColumnDescription 
        public string IMP_LOCAL_TRASLADADO {get { return this._IMP_LOCAL_TRASLADADO; } set { this._IMP_LOCAL_TRASLADADO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ISR_RETENIDO")] 
           [SugarColumn(ColumnName="isr_retenido")]ColumnDescription 
        public string ISR_RETENIDO {get { return this._ISR_RETENIDO; } set { this._ISR_RETENIDO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVA_ACREDITAR")] 
           [SugarColumn(ColumnName="iva_acreditar")]ColumnDescription 
        public string IVA_ACREDITAR {get { return this._IVA_ACREDITAR; } set { this._IVA_ACREDITAR = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVA_POR_TRASLADAR")] 
           [SugarColumn(ColumnName="iva_por_trasladar")]ColumnDescription 
        public string IVA_POR_TRASLADAR {get { return this._IVA_POR_TRASLADAR; } set { this._IVA_POR_TRASLADAR = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IVA_RETENIDO")] 
           [SugarColumn(ColumnName="iva_retenido")]ColumnDescription 
        public string IVA_RETENIDO {get { return this._IVA_RETENIDO; } set { this._IVA_RETENIDO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("VENTAS")] 
           [SugarColumn(ColumnName="ventas")]ColumnDescription 
        public string VENTAS {get { return this._VENTAS; } set { this._VENTAS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RCERTIFICADO")] 
           [SugarColumn(ColumnName="rcertificado")]ColumnDescription 
        public string RCERTIFICADO {get { return this._RCERTIFICADO; } set { this._RCERTIFICADO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RLLAVEPRIVADA")] 
           [SugarColumn(ColumnName="rllaveprivada")]ColumnDescription 
        public string RLLAVEPRIVADA {get { return this._RLLAVEPRIVADA; } set { this._RLLAVEPRIVADA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CONTRASENIALLAVEPRIVADA")] 
           [SugarColumn(ColumnName="contraseniallaveprivada")]ColumnDescription 
        public string CONTRASENIALLAVEPRIVADA {get { return this._CONTRASENIALLAVEPRIVADA; } set { this._CONTRASENIALLAVEPRIVADA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PROVEEDORTIMBRADO")] 
           [SugarColumn(ColumnName="proveedortimbrado")]ColumnDescription 
        public string PROVEEDORTIMBRADO {get { return this._PROVEEDORTIMBRADO; } set { this._PROVEEDORTIMBRADO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("USUARIOTIMBRADO")] 
           [SugarColumn(ColumnName="usuariotimbrado")]ColumnDescription 
        public string USUARIOTIMBRADO {get { return this._USUARIOTIMBRADO; } set { this._USUARIOTIMBRADO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CONTRASENIATIMBRADO")] 
           [SugarColumn(ColumnName="contraseniatimbrado")]ColumnDescription 
        public string CONTRASENIATIMBRADO {get { return this._CONTRASENIATIMBRADO; } set { this._CONTRASENIATIMBRADO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PROVEEDORCANCELACION")] 
           [SugarColumn(ColumnName="proveedorcancelacion")]ColumnDescription 
        public string PROVEEDORCANCELACION {get { return this._PROVEEDORCANCELACION; } set { this._PROVEEDORCANCELACION = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("USUARIOCANCELACION")] 
           [SugarColumn(ColumnName="usuariocancelacion")]ColumnDescription 
        public string USUARIOCANCELACION {get { return this._USUARIOCANCELACION; } set { this._USUARIOCANCELACION = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CONTRASENIACANCELACION")] 
           [SugarColumn(ColumnName="contraseniacancelacion")]ColumnDescription 
        public string CONTRASENIACANCELACION {get { return this._CONTRASENIACANCELACION; } set { this._CONTRASENIACANCELACION = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ESPACIOASPEL")] 
           [SugarColumn(ColumnName="espacioaspel")]ColumnDescription 
        public string ESPACIOASPEL {get { return this._ESPACIOASPEL; } set { this._ESPACIOASPEL = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("USUARIOESPACIO")] 
           [SugarColumn(ColumnName="usuarioespacio")]ColumnDescription 
        public string USUARIOESPACIO {get { return this._USUARIOESPACIO; } set { this._USUARIOESPACIO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CONTRASENIAESPACIO")] 
           [SugarColumn(ColumnName="contraseniaespacio")]ColumnDescription 
        public string CONTRASENIAESPACIO {get { return this._CONTRASENIAESPACIO; } set { this._CONTRASENIAESPACIO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("IDCATEGORIAESPACIO")] 
           [SugarColumn(ColumnName="idcategoriaespacio")]ColumnDescription 
        public int? IDCATEGORIAESPACIO {get { return this._IDCATEGORIAESPACIO; } set { this._IDCATEGORIAESPACIO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NOMBRECATEGORIAESPACIO")] 
           [SugarColumn(ColumnName="nombrecategoriaespacio")]ColumnDescription 
        public string NOMBRECATEGORIAESPACIO {get { return this._NOMBRECATEGORIAESPACIO; } set { this._NOMBRECATEGORIAESPACIO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ASIGCATEGORIA")] 
           [SugarColumn(ColumnName="asigcategoria")]ColumnDescription 
        public int? ASIGCATEGORIA {get { return this._ASIGCATEGORIA; } set { this._ASIGCATEGORIA = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("GDIOTAPARTIRCFDI")] 
           [SugarColumn(ColumnName="gdiotapartircfdi")]ColumnDescription 
        public int? GDIOTAPARTIRCFDI {get { return this._GDIOTAPARTIRCFDI; } set { this._GDIOTAPARTIRCFDI = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("GUARDAROPEAUTO")] 
           [SugarColumn(ColumnName="guardaropeauto")]ColumnDescription 
        public int? GUARDAROPEAUTO {get { return this._GUARDAROPEAUTO; } set { this._GUARDAROPEAUTO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CFDIENTODASLASPARTIDAS")] 
           [SugarColumn(ColumnName="cfdientodaslaspartidas")]ColumnDescription 
        public int? CFDIENTODASLASPARTIDAS {get { return this._CFDIENTODASLASPARTIDAS; } set { this._CFDIENTODASLASPARTIDAS = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("LOGO")] 
           [SugarColumn(ColumnName="logo")]ColumnDescription 
        public byte[] LOGO {get { return this._LOGO; } set { this._LOGO = value; this.OnPropertyChanged(); }}
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TRIAL732")] 
           [SugarColumn(ColumnName="trial732")]ColumnDescription 
        public string TRIAL732 {get { return this._TRIAL732; } set { this._TRIAL732 = value; this.OnPropertyChanged(); }}
    }
}
