﻿using System.ComponentModel;

namespace Jaeger.Amazon.S3.ValueObjects {
    public enum StorageClassTypeEnum {
        [Description("STANDARD")]
        Standar,
        [Description("REDUCED_REDUNDANCY")]
        ReducedRedundancy
    }
}
