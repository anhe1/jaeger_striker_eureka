﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;

namespace Jaeger.Amazon.Entities {
    public class PropertiesConvert : ExpandableObjectConverter {
        [DebuggerNonUserCode]
        public PropertiesConvert() {
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType) {
            bool canConvertFrom;
            canConvertFrom = ((object)sourceType != (object)typeof(string) ? base.CanConvertFrom(context, sourceType) : true);
            return canConvertFrom;
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) {
            bool canConvertTo = false;
            return canConvertTo;
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value) {
            object convertFrom = null;
            return convertFrom;
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType) {
            object convertTo = null;
            return convertTo;
        }
    }
}