﻿/// develop:
/// purpose: configuracion para el servicio S3 de Amazon (Simple Storage Service)
using System;
using System.ComponentModel;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Jaeger.Amazon.Entities;

namespace Jaeger.Amazon.S3 {
    [JsonObject]
    [TypeConverter(typeof(PropertiesConvert))]
    public class Settings : PropertyChangeImplementation, IDataErrorInfo {
        private string regionField;
        private string bucketNameField;
        private string accessKeyIdField;
        private string secretAccessKeyField;
        private string folderField;
        private bool allowToStoreField;
        private bool sendAutomaticallyField;

        public Settings() {
            this.regionField = string.Empty;
            this.bucketNameField = string.Empty;
            this.accessKeyIdField = string.Empty;
            this.secretAccessKeyField = string.Empty;
            this.folderField = string.Empty;
            this.allowToStoreField = false;
            this.sendAutomaticallyField = false;
        }

        public Settings(string json) {
            try {
                Settings conf = JsonConvert.DeserializeObject<Settings>(json);
                if (conf != null) {
                    this.regionField = conf.Region;
                    this.bucketNameField = conf.BucketName;
                    this.accessKeyIdField = conf.AccessKeyId;
                    this.secretAccessKeyField = conf.SecretAccessKey;
                    this.folderField = conf.Folder;
                    this.allowToStoreField = conf.AllowToStore;
                    this.sendAutomaticallyField = conf.SendAutomatically;
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        [Category("Amazon")]
        [Description("Identificador único que asociado con una clave de acceso secreta.")]
        [DisplayName("AccessKeyId")]
        [JsonProperty("accessKeyId")]
        [PasswordPropertyText(true)]
        [XmlAttribute("accessKeyId")]
        public string AccessKeyId {
            get {
                return this.accessKeyIdField;
            }
            set {
                this.accessKeyIdField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("Amazon")]
        [Description("")]
        [DisplayName("Permitir guardar")]
        [JsonProperty("allowToStore")]
        [XmlAttribute("allowToStore")]
        public bool AllowToStore {
            get {
                return this.allowToStoreField;
            }
            set {
                this.allowToStoreField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("Amazon")]
        [Description("Contenedor de S3")]
        [DisplayName("BucketName")]
        [JsonProperty("bucketName")]
        [PasswordPropertyText(true)]
        [XmlAttribute("bucketname")]
        public string BucketName {
            get {
                return this.bucketNameField;
            }
            set {
                this.bucketNameField = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        [XmlIgnore]
        public string Error {
            get {
                return string.Empty;
            }
        }

        [Category("Amazon")]
        [Description("")]
        [DisplayName("Folder")]
        [JsonProperty("folder")]
        [XmlAttribute("folder")]
        public string Folder {
            get {
                return this.folderField;
            }
            set {
                this.folderField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [XmlIgnore]
        public string this[string columnName] {
            get {
                return string.Empty;
            }
        }

        [Category("Amazon")]
        [Description("")]
        [DisplayName("Region")]
        [JsonProperty("region")]
        [XmlAttribute("region")]
        public string Region {
            get {
                return this.regionField;
            }
            set {
                this.regionField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("Amazon")]
        [Description("")]
        [DisplayName("SecretAccessKey")]
        [JsonProperty("secretAccessKey")]
        [PasswordPropertyText(true)]
        [XmlAttribute("secretAccessKey")]
        public string SecretAccessKey {
            get {
                return this.secretAccessKeyField;
            }
            set {
                this.secretAccessKeyField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("Amazon")]
        [Description("Enviar archivos despúes de validar")]
        [DisplayName("Respaldar")]
        [JsonProperty("sendAutomatically")]
        [XmlAttribute("sendAutomatically")]
        public bool SendAutomatically {
            get {
                return this.sendAutomaticallyField;
            }
            set {
                this.sendAutomaticallyField = value;
                this.OnPropertyChanged();
            }
        }

        public string ToJson(Formatting objFormat = 0) {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static Settings Json(string json) {
            try {
                return JsonConvert.DeserializeObject<Settings>(json);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
    }
}