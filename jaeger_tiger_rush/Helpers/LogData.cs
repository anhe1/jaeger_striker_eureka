﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Threading.Tasks;
using SprightlySoftAWS;
using Jaeger.Amazon.S3;

namespace Jaeger.UI.Helpers {
    public class LogData {
        private string fileName = @"C:\Jaeger\Jaeger.Log\jaeger_tiher_rush.log";

        public string FormatLogData(string RequestURL, string RequestMethod, Dictionary<string, string> RequestHeaders, int ResponseStatusCode, string ResponseStatusDescription, Dictionary<string, string> ResponseHeaders, string ResponseString, int ErrorNumber, string ErrorDescription) {
            string ReturnString = "";

            if (ErrorNumber != 0) {
                ReturnString += "SprightlySoft ErrorDescription: " + ErrorDescription + Environment.NewLine;
                ReturnString += "SprightlySoft ErrorNumber: " + ErrorNumber + Environment.NewLine;
                ReturnString += Environment.NewLine;
            }

            if (ResponseStatusCode != 0) {
                ReturnString += "Request URL: " + RequestURL + Environment.NewLine;
                ReturnString += "Request Method: " + RequestMethod + Environment.NewLine;

                foreach (KeyValuePair<string, string> MyHeader in RequestHeaders) {
                    ReturnString += "Request Header: " + MyHeader.Key + ":" + MyHeader.Value + Environment.NewLine;
                }

                ReturnString += Environment.NewLine;
                ReturnString += "Response Status Code: " + ResponseStatusCode + Environment.NewLine;
                ReturnString += "Response Status Description: " + ResponseStatusDescription + Environment.NewLine;

                foreach (KeyValuePair<string, string> MyHeader in ResponseHeaders) {
                    ReturnString += "Response Header: " + MyHeader.Key + ":" + MyHeader.Value + Environment.NewLine;
                }

                if (ResponseString != "") {
                    ReturnString += Environment.NewLine;

                    try {
                        var XmlDoc = new XmlDocument();
                        XmlDoc.LoadXml(ResponseString);

                        ReturnString += "Response XML: " + Environment.NewLine + ResponseString + Environment.NewLine;

                        XmlNode XmlNode;
                        XmlNode = XmlDoc.SelectSingleNode("/Error/Message");

                        if (XmlNode != null) {
                            ReturnString += Environment.NewLine;
                            ReturnString += "Amazon Error Message: " + XmlNode.InnerText + Environment.NewLine;
                        }
                    }
                    catch (Exception e) {
                        ReturnString += "Response String: " + Environment.NewLine + ResponseString + Environment.NewLine;
                    }

                }
            }
            this.LogWrite(ReturnString);
            return ReturnString;
        }

        public void LogWrite(string oError) {
            try {
                if (!File.Exists(fileName)) {
                    File.Create(fileName).Close();
                }
                StreamWriter streamWriter = File.AppendText(fileName);
                object[] type = new object[] { oError, "|", DateTime.Now };
                streamWriter.WriteLine(string.Concat(type));
                streamWriter.Close();
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }
    }

    public interface ISimpleStorageService {

    }

    public class SimpleStorageServiceSprightly : LogData, ISimpleStorageService {

        REST MyREST = new REST();

        public SimpleStorageServiceSprightly() {
            this.Configuracion = new Settings();
        }

        public Settings Configuracion { get; set; }

        public string Message { get; set; }

        public string Log { get; set; }

        public int StatusCode { get; set; }

        public bool ObjectExist(string bucketName, string keyName, string accessKeyId, string secretAccessKey) {
            this.Message = string.Empty;
            this.Log = string.Empty;
            this.StatusCode = 0;

            string RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", bucketName, keyName, "");

            string RequestMethod = "HEAD";

            var ExtraRequestHeaders = new Dictionary<string, string> {
                { "x-amz-date", DateTime.UtcNow.ToString("r") }
            };

            string AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, accessKeyId, secretAccessKey);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            bool RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            this.StatusCode = MyREST.ResponseStatusCode;

            if (MyREST.ResponseStatusCode == 200) {
                this.Message = "The object exists, you have permission to it.";
            }
            else if (MyREST.ResponseStatusCode == 404) {
                this.Message = "The object does not exists.";
            }
            else if (MyREST.ResponseStatusCode == 403) {
                this.Message = "Permission error.  Someone else owns the object or your access keys are invalid.";
            }
            else {
                this.Log = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);
            }

            return false;
        }

        public void List(string TextBoxAWSAccessKeyId, string TextBoxAWSSecretAccessKey) {
            //create an instance of the Operation class
            var MyREST = new REST();

            //Build the URL to call. Do not specify a bucket name or key name when listing all buckets
            string RequestURL = RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", "", "", "");

            string RequestMethod = "GET";

            //add a date header
            Dictionary<string, string> ExtraRequestHeaders = new Dictionary<string, string> {
                { "x-amz-date", DateTime.UtcNow.ToString("r") }
            };

            //generate the authorization header value
            string AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId, TextBoxAWSSecretAccessKey);
            //add the authorization header
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            //call GetOperation and do not specify a bucket name or key name
            bool RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            XmlDocument XmlDoc;
            XmlNamespaceManager MyXmlNamespaceManager;
            XmlNode XmlNode;
            XmlNodeList XmlContentsNodeList;
            string ResponseMessage = "";

            if (RetBool == true) {
                //load the response XML from Amazon into an XmlDocument
                XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(MyREST.ResponseString);

                //specify the namespace for XML path expressions
                MyXmlNamespaceManager = new XmlNamespaceManager(XmlDoc.NameTable);
                MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                //extract all values from the response XML
                XmlNode = XmlDoc.SelectSingleNode("/amz:ListAllMyBucketsResult/amz:Owner/amz:ID", MyXmlNamespaceManager);
                ResponseMessage = ResponseMessage + "Owner ID = " + XmlNode.InnerText + Environment.NewLine;

                XmlNode = XmlDoc.SelectSingleNode("/amz:ListAllMyBucketsResult/amz:Owner/amz:DisplayName", MyXmlNamespaceManager);
                ResponseMessage = ResponseMessage + "Owner DisplayName = " + XmlNode.InnerText + Environment.NewLine;
                ResponseMessage = ResponseMessage + Environment.NewLine;

                XmlContentsNodeList = XmlDoc.SelectNodes("/amz:ListAllMyBucketsResult/amz:Buckets/amz:Bucket", MyXmlNamespaceManager);

                foreach (XmlNode XmlContentsNode in XmlContentsNodeList) {
                    XmlNode = XmlContentsNode.SelectSingleNode("./amz:Name", MyXmlNamespaceManager);
                    ResponseMessage = ResponseMessage + "Bucket Name = " + XmlNode.InnerText;

                    XmlNode = XmlContentsNode.SelectSingleNode("./amz:CreationDate", MyXmlNamespaceManager);
                    ResponseMessage = ResponseMessage + "     CreationDate = " + Convert.ToDateTime(XmlNode.InnerText) + Environment.NewLine;
                }

                //display values from the XML in the DialogOutput form
                //DialogOutput MyDialogOutput = new DialogOutput();
                //MyDialogOutput.Text = "Success";
                //MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                //MyDialogOutput.ShowDialog(this);

            }
            else {
                //build an error message
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                //display the error message
                //DialogOutput MyDialogOutput = new DialogOutput();
                //MyDialogOutput.Text = "Error";
                //MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                //MyDialogOutput.ShowDialog(this);
            }
        }
    }
}

// <auto-generated />
//
// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using CodeBeautify;
//
//    var welcome3 = Welcome3.FromJson(jsonString);

namespace CodeBeautify {
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Welcome3 {
        [JsonProperty("ListAllMyBucketsResult")]
        public ListAllMyBucketsResult ListAllMyBucketsResult { get; set; }
    }

    public partial class ListAllMyBucketsResult {
        [JsonProperty("Owner")]
        public Owner Owner { get; set; }

        [JsonProperty("Buckets")]
        public Buckets Buckets { get; set; }

        [JsonProperty("_xmlns")]
        public Uri Xmlns { get; set; }
    }

    public partial class Buckets {
        [JsonProperty("Bucket")]
        public Bucket[] Bucket { get; set; }
    }

    public partial class Bucket {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("CreationDate")]
        public DateTimeOffset CreationDate { get; set; }
    }

    public partial class Owner {
        [JsonProperty("ID")]
        public string Id { get; set; }

        [JsonProperty("DisplayName")]
        public string DisplayName { get; set; }
    }

    public partial class Welcome3 {
        public static Welcome3 FromJson(string json) => JsonConvert.DeserializeObject<Welcome3>(json, CodeBeautify.Converter.Settings);
    }

    public static class Serialize {
        public static string ToJson(this Welcome3 self) => JsonConvert.SerializeObject(self, CodeBeautify.Converter.Settings);
    }

    internal static class Converter {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
