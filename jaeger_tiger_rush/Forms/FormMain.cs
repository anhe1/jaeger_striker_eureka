﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Jaeger.UI.Forms {
    public partial class FormMain : Form {
        public static string FileName = @"C:\Jaeger\Jaeger.Log\jaeger_tiger_rush.log";

        public FormMain() {
            InitializeComponent();
        }

        private void FormMain_Load(Object sender, EventArgs e) {
            //when the form loads, set the text box values from the saved settings
            Properties.Settings MySettings = new Properties.Settings();
            TextBoxAWSAccessKeyId.Text = MySettings.AWSAccessKeyId;
            TextBoxAWSSecretAccessKey.Text = MySettings.AWSSecretAccessKey;
            TabControl1.SelectedIndex = MySettings.TabControlSelectedIndex;
            TextBoxBucketBucketName.Text = MySettings.BucketBucketName;
            TextBoxObjectBucketName.Text = MySettings.ObjectBucketName;
            TextBoxObjectKeyName.Text = MySettings.ObjectKeyName;
            TextBoxUploadFileName.Text = MySettings.UploadFileName;
            TextBoxUploadBucketName.Text = MySettings.UploadBucketName;
            TextBoxUploadKeyName.Text = MySettings.UploadKeyName;
            TextBoxDownloadBucketName.Text = MySettings.DownloadBucketName;
            TextBoxDownloadKeyName.Text = MySettings.DownloadKeyName;
            TextBoxDownloadFileName.Text = MySettings.DownloadFileName;
            TextBoxHashFileName.Text = MySettings.HashFileName;
            TextBoxUploadContentType.Text = MySettings.UploadContentType;
            CheckBoxUploadMakePublic.Checked = MySettings.UploadMakePublic;
            TextBoxVerBucketName.Text = MySettings.TextBoxVerBucketName;
            TextBoxVerKeyName.Text = MySettings.TextBoxVerKeyName;
            TextBoxVerVersionID.Text = MySettings.TextBoxVerVersionID;
            ComboBoxStorageClass.Text = ComboBoxStorageClass.Items[0].ToString();
        }

        protected override void OnFormClosing(FormClosingEventArgs e) {
            //when the form closes, save the text box values
            Properties.Settings MySettings = new Properties.Settings();
            MySettings.AWSAccessKeyId = TextBoxAWSAccessKeyId.Text;
            MySettings.AWSSecretAccessKey = TextBoxAWSSecretAccessKey.Text;
            MySettings.TabControlSelectedIndex = TabControl1.SelectedIndex;
            MySettings.BucketBucketName = TextBoxBucketBucketName.Text;
            MySettings.ObjectBucketName = TextBoxObjectBucketName.Text;
            MySettings.ObjectKeyName = TextBoxObjectKeyName.Text;
            MySettings.UploadFileName = TextBoxUploadFileName.Text;
            MySettings.UploadBucketName = TextBoxUploadBucketName.Text;
            MySettings.UploadKeyName = TextBoxUploadKeyName.Text;
            MySettings.DownloadBucketName = TextBoxDownloadBucketName.Text;
            MySettings.DownloadKeyName = TextBoxDownloadKeyName.Text;
            MySettings.DownloadFileName = TextBoxDownloadFileName.Text;
            MySettings.HashFileName = TextBoxHashFileName.Text;
            MySettings.UploadContentType = TextBoxUploadContentType.Text;
            MySettings.UploadMakePublic = CheckBoxUploadMakePublic.Checked;
            MySettings.TextBoxVerBucketName = TextBoxVerBucketName.Text;
            MySettings.TextBoxVerKeyName = TextBoxVerKeyName.Text;
            MySettings.TextBoxVerVersionID = TextBoxVerVersionID.Text;
            MySettings.Save();
        }

        private String FormatLogData(String RequestURL, String RequestMethod, Dictionary<String, String> RequestHeaders, int ResponseStatusCode, String ResponseStatusDescription, Dictionary<String, String> ResponseHeaders, String ResponseString, int ErrorNumber, String ErrorDescription) {
            String ReturnString = "";

            if (ErrorNumber != 0) {
                ReturnString += "SprightlySoft ErrorDescription: " + ErrorDescription + Environment.NewLine;
                ReturnString += "SprightlySoft ErrorNumber: " + ErrorNumber + Environment.NewLine;
                ReturnString += Environment.NewLine;
            }

            if (ResponseStatusCode != 0) {
                ReturnString += "Request URL: " + RequestURL + Environment.NewLine;
                ReturnString += "Request Method: " + RequestMethod + Environment.NewLine;

                foreach (KeyValuePair<String, String> MyHeader in RequestHeaders) {
                    ReturnString += "Request Header: " + MyHeader.Key + ":" + MyHeader.Value + Environment.NewLine;
                }

                ReturnString += Environment.NewLine;
                ReturnString += "Response Status Code: " + ResponseStatusCode + Environment.NewLine;
                ReturnString += "Response Status Description: " + ResponseStatusDescription + Environment.NewLine;

                foreach (KeyValuePair<String, String> MyHeader in ResponseHeaders) {
                    ReturnString += "Response Header: " + MyHeader.Key + ":" + MyHeader.Value + Environment.NewLine;
                }

                if (ResponseString != "") {
                    ReturnString += Environment.NewLine;

                    try {
                        System.Xml.XmlDocument XmlDoc = new System.Xml.XmlDocument();
                        XmlDoc.LoadXml(ResponseString);

                        ReturnString += "Response XML: " + Environment.NewLine + ResponseString + Environment.NewLine;

                        System.Xml.XmlNode XmlNode;
                        XmlNode = XmlDoc.SelectSingleNode("/Error/Message");

                        if (XmlNode != null) {
                            ReturnString += Environment.NewLine;
                            ReturnString += "Amazon Error Message: " + XmlNode.InnerText + Environment.NewLine;
                        }
                    }
                    catch (Exception e) {
                        ReturnString += "Response String: " + Environment.NewLine + ResponseString + Environment.NewLine;
                    }

                }
            }
            FormMain.LogWrite(ReturnString);
            return ReturnString;
        }

        private void ButtonListBuckets_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //GET Service: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTServiceGET.html

            //create an instance of the Operation class
            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            //Build the URL to call. Do not specify a bucket name or key name when listing all buckets
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", "", "", "");

            String RequestMethod;
            RequestMethod = "GET";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            //add a date header
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            //generate the authorization header value
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            //add the authorization header
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            //call GetOperation and do not specify a bucket name or key name
            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            //print the log data to the Output window
            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            System.Xml.XmlDocument XmlDoc;
            System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
            System.Xml.XmlNode XmlNode;
            System.Xml.XmlNodeList XmlContentsNodeList;
            String ResponseMessage = "";

            if (RetBool == true) {
                //load the response XML from Amazon into an XmlDocument
                XmlDoc = new System.Xml.XmlDocument();
                XmlDoc.LoadXml(MyREST.ResponseString);

                //specify the namespace for XML path expressions
                MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                //extract all values from the response XML
                XmlNode = XmlDoc.SelectSingleNode("/amz:ListAllMyBucketsResult/amz:Owner/amz:ID", MyXmlNamespaceManager);
                ResponseMessage = ResponseMessage + "Owner ID = " + XmlNode.InnerText + Environment.NewLine;

                XmlNode = XmlDoc.SelectSingleNode("/amz:ListAllMyBucketsResult/amz:Owner/amz:DisplayName", MyXmlNamespaceManager);
                ResponseMessage = ResponseMessage + "Owner DisplayName = " + XmlNode.InnerText + Environment.NewLine;
                ResponseMessage = ResponseMessage + Environment.NewLine;

                XmlContentsNodeList = XmlDoc.SelectNodes("/amz:ListAllMyBucketsResult/amz:Buckets/amz:Bucket", MyXmlNamespaceManager);

                foreach (System.Xml.XmlNode XmlContentsNode in XmlContentsNodeList) {
                    XmlNode = XmlContentsNode.SelectSingleNode("./amz:Name", MyXmlNamespaceManager);
                    ResponseMessage = ResponseMessage + "Bucket Name = " + XmlNode.InnerText;

                    XmlNode = XmlContentsNode.SelectSingleNode("./amz:CreationDate", MyXmlNamespaceManager);
                    ResponseMessage = ResponseMessage + "     CreationDate = " + Convert.ToDateTime(XmlNode.InnerText) + Environment.NewLine;
                }

                //display values from the XML in the DialogOutput form
                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Success";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);

            }
            else {
                //build an error message
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                //display the error message
                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonListBucketAll_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //The following sample uses the ListBucket class to retrieve all objects at once.  The class calls get bucket for each page of results.
            //For each object in the result a ListBucket.BucketItemObject is created and added to the BucketItemsArrayList property of the class.  

            SprightlySoftAWS.S3.ListBucket MyListBucket = new SprightlySoftAWS.S3.ListBucket();

            Boolean RetBool;
            RetBool = MyListBucket.ListBucket(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "/", "", TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyListBucket.LogData);
            System.Diagnostics.Debug.Print("");

            String ResponseMessage = "";

            if (RetBool == true) {
                foreach (SprightlySoftAWS.S3.ListBucket.BucketItemObject MyBucketItemObject in MyListBucket.BucketItemsArrayList) {
                    if (MyBucketItemObject.IsFolder == true) {
                        ResponseMessage = ResponseMessage + "FolderName = " + MyBucketItemObject.KeyName + Environment.NewLine;
                    }
                    else {
                        ResponseMessage = ResponseMessage + "KeyName = " + MyBucketItemObject.KeyName + "   LastModified = " + MyBucketItemObject.LastModified + "   ETag = " + MyBucketItemObject.ETag + "   Size = " + MyBucketItemObject.Size + "   OwnerID = " + MyBucketItemObject.OwnerID + "   OwnerName = " + MyBucketItemObject.OwnerName + "   StorageClass = " + MyBucketItemObject.StorageClass + Environment.NewLine;
                    }
                }

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Success";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }
            else {
                ResponseMessage = FormatLogData(MyListBucket.RequestURL, MyListBucket.RequestMethod, MyListBucket.RequestHeaders, MyListBucket.ResponseStatusCode, MyListBucket.ResponseStatusDescription, MyListBucket.ResponseHeaders, MyListBucket.ResponseStringFormatted, MyListBucket.ErrorNumber, MyListBucket.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonListBucketPages_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //This example lists a bucket one page at a time.  If there are more than 1000 objects in a result the IsTruncated value will be true
            //and the same function will be called again with the Marker value in the query string set to the NextMarker value from the XML.

            //GET Bucket (List Objects): http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTBucketGET.html

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String Delimiter = "/";  //use a forward slash as the delimiter if your bucket uses folders and you want to list a single folder.
            String Prefix = "";
            String Marker = "";
            int MaxKeys = 1000;  //1000 is the maximum number of objects that can be returned in a page
            Boolean IsTruncated = false;

            String QueryString;
            Boolean RetBool;
            System.Xml.XmlDocument XmlDoc;
            System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
            System.Xml.XmlNode XmlNode;
            System.Xml.XmlNodeList XmlContentsNodeList;
            System.Xml.XmlNodeList XmlCommonPrefixesNodeList;
            String ResponseMessage = "";

            String RequestURL;
            String RequestMethod = "GET";
            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            String AuthorizationValue;

            //put the following code in a loop as we may be listing many pages
            do {
                QueryString = "?delimiter=" + System.Uri.EscapeDataString(Delimiter) + "&prefix=" + System.Uri.EscapeDataString(Prefix) + "&marker=" + System.Uri.EscapeDataString(Marker) + "&max-keys=" + MaxKeys;

                RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", QueryString);

                ExtraRequestHeaders = new Dictionary<String, String>();
                ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));
                AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
                ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

                RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyREST.LogData);
                System.Diagnostics.Debug.Print("");

                if (RetBool == true) {
                    XmlDoc = new System.Xml.XmlDocument();
                    XmlDoc.LoadXml(MyREST.ResponseString);

                    MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                    MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                    XmlNode = XmlDoc.SelectSingleNode("/amz:ListBucketResult", MyXmlNamespaceManager);
                    if (XmlNode == null) {
                        MessageBox.Show("The ListBucketResult node does not exist in the response XML.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
                    else {
                        //the call was successful, list all values in the XML
                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListBucketResult/amz:Name", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Name = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListBucketResult/amz:Prefix", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Prefix = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListBucketResult/amz:Marker", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Marker = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListBucketResult/amz:MaxKeys", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "MaxKeys = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListBucketResult/amz:Delimiter", MyXmlNamespaceManager);
                        if (XmlNode != null) {
                            ResponseMessage = ResponseMessage + "Delimiter = " + XmlNode.InnerText + Environment.NewLine;
                        }

                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListBucketResult/amz:IsTruncated", MyXmlNamespaceManager);
                        IsTruncated = Convert.ToBoolean(XmlNode.InnerText);
                        ResponseMessage = ResponseMessage + "IsTruncated = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListBucketResult/amz:NextMarker", MyXmlNamespaceManager);
                        if (XmlNode != null) {
                            //set the Marker to the NextMarker value
                            Marker = XmlNode.InnerText;
                            ResponseMessage = ResponseMessage + "NextMarker = " + XmlNode.InnerText + Environment.NewLine;
                        }


                        XmlContentsNodeList = XmlDoc.SelectNodes("/amz:ListBucketResult/amz:Contents", MyXmlNamespaceManager);

                        foreach (System.Xml.XmlNode XmlContentsNode in XmlContentsNodeList) {
                            XmlNode = XmlContentsNode.SelectSingleNode("./amz:Key", MyXmlNamespaceManager);
                            ResponseMessage = ResponseMessage + "KeyName = " + XmlNode.InnerText;

                            //if the delimiter is empty the NextMarker XML element will not appear.  You will need to keep track of the Marker yourself.
                            if (Delimiter == "") {
                                Marker = XmlNode.InnerText;
                            }

                            XmlNode = XmlContentsNode.SelectSingleNode("./amz:LastModified", MyXmlNamespaceManager);
                            ResponseMessage = ResponseMessage + "     LastModified = " + Convert.ToDateTime(XmlNode.InnerText);

                            XmlNode = XmlContentsNode.SelectSingleNode("./amz:ETag", MyXmlNamespaceManager);
                            ResponseMessage = ResponseMessage + "     ETag = " + XmlNode.InnerText;

                            XmlNode = XmlContentsNode.SelectSingleNode("./amz:Size", MyXmlNamespaceManager);
                            ResponseMessage = ResponseMessage + "     Size = " + XmlNode.InnerText;

                            XmlNode = XmlContentsNode.SelectSingleNode("./amz:Owner/amz:ID", MyXmlNamespaceManager);
                            if (XmlNode != null) {
                                ResponseMessage = ResponseMessage + "     Owner ID = " + XmlNode.InnerText;
                            }

                            XmlNode = XmlContentsNode.SelectSingleNode("./amz:Owner/amz:DisplayName", MyXmlNamespaceManager);
                            if (XmlNode != null) {
                                ResponseMessage = ResponseMessage + "     Owner DisplayName = " + XmlNode.InnerText;
                            }

                            XmlNode = XmlContentsNode.SelectSingleNode("./amz:StorageClass", MyXmlNamespaceManager);
                            ResponseMessage = ResponseMessage + "     StorageClass = " + XmlNode.InnerText;

                            ResponseMessage = ResponseMessage + Environment.NewLine;
                        }


                        XmlCommonPrefixesNodeList = XmlDoc.SelectNodes("/amz:ListBucketResult/amz:CommonPrefixes", MyXmlNamespaceManager);

                        foreach (System.Xml.XmlNode XmlPrefixesNode in XmlCommonPrefixesNodeList) {
                            XmlNode = XmlPrefixesNode.SelectSingleNode("./amz:Prefix", MyXmlNamespaceManager);
                            ResponseMessage = ResponseMessage + "Prefix = " + XmlNode.InnerText + Environment.NewLine;
                        }

                        ResponseMessage = ResponseMessage + Environment.NewLine;
                    }

                }
                else {
                    ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);

                    break;
                }

                if (IsTruncated == false) {
                    //if there are no more pages to get, show the output and exit
                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Success";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                    break;
                }

            } while (IsTruncated == true);

            (sender as Button).Enabled = true;
        }

        private void ButtonCreateBucket_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //Working with Amazon S3 Buckets: http://docs.amazonwebservices.com/AmazonS3/latest/UsingBucket.html
            //PUT Bucket: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTBucketPUT.html

            String NewBucketName;
            NewBucketName = TextBoxBucketBucketName.Text;

            //This is a simple implementation of creating a bucket.  A more complete implementation would do the following:
            //  validate the bucket name
            //  check if a bucket with the same name already exists

            if (NewBucketName != "") {

                SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

                String RequestURL;
                RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", NewBucketName, "", "");

                String RequestMethod = "GET";

                Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
                ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                String AuthorizationValue;
                AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
                ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

                String PutXML;
                PutXML = "";  //US region

                //uncomment the following line to create the bucket in Europe
                //PutXML = "<CreateBucketConfiguration><LocationConstraint>EU</LocationConstraint></CreateBucketConfiguration>"  'EU region

                //uncomment the following line to create the bucket in Northern California
                //PutXML = "<CreateBucketConfiguration><LocationConstraint>us-west-1</LocationConstraint></CreateBucketConfiguration>"  'US-West region 

                //uncomment the following line to create the bucket in Asia Pacific (Singapore)
                //PutXML = "<CreateBucketConfiguration><LocationConstraint>ap-southeast-1</LocationConstraint></CreateBucketConfiguration>"  'US-West region 

                Boolean RetBool;
                RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, PutXML);

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyREST.LogData);
                System.Diagnostics.Debug.Print("");

                if (RetBool == true) {
                    MessageBox.Show("The bucket was created.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    String ResponseMessage;
                    ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }

            }

            (sender as Button).Enabled = true;
        }

        private void ButtonDeleteBucket_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //DELETE Bucket: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTBucketDELETE.html

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "");

            String RequestMethod = "DELETE";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            if (RetBool == true) {
                MessageBox.Show("The bucket was deleted.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else {
                String ResponseMessage;
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonBucketExists_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //do a HEAD operation on the bucket to see if it exists

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "");

            String RequestMethod = "HEAD";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            if (MyREST.ResponseStatusCode == 200) {
                MessageBox.Show("The bucket exists, you have permission to it.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (MyREST.ResponseStatusCode == 404) {
                MessageBox.Show("The bucket does not exists.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (MyREST.ResponseStatusCode == 403) {
                MessageBox.Show("Permission error.  Someone else owns the bucket or your access keys are invalid.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else {
                String ResponseMessage;
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonGetBucketLocation_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //GET Bucket location: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTBucketLocationGET.html

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?location");

            String RequestMethod = "GET";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            System.Xml.XmlDocument XmlDoc;
            System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
            System.Xml.XmlNode XmlNode;

            if (RetBool == true) {
                XmlDoc = new System.Xml.XmlDocument();
                XmlDoc.LoadXml(MyREST.ResponseString);

                MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                XmlNode = XmlDoc.SelectSingleNode("/amz:LocationConstraint", MyXmlNamespaceManager);

                if (XmlNode == null) {
                    MessageBox.Show("The LocationConstraint node does not exist in the response XML.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else {
                    if (XmlNode.InnerText == "") {
                        MessageBox.Show("Location is US.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else {
                        MessageBox.Show("Location is " + XmlNode.InnerText + ".", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            else {
                String ResponseMessage;
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonGetBucketLogging_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //Server Access Logging: http://docs.amazonwebservices.com/AmazonS3/latest/ServerLogs.html
            //GET Bucket logging: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTBucketGETlogging.html

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?logging");

            String RequestMethod = "GET";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            System.Xml.XmlDocument XmlDoc;
            System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
            System.Xml.XmlNode XmlNodeTargetBucket;
            System.Xml.XmlNode TargetPrefix;

            if (RetBool == true) {
                XmlDoc = new System.Xml.XmlDocument();
                XmlDoc.LoadXml(MyREST.ResponseString);

                MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                XmlNodeTargetBucket = XmlDoc.SelectSingleNode("/amz:BucketLoggingStatus/amz:LoggingEnabled/amz:TargetBucket", MyXmlNamespaceManager);
                TargetPrefix = XmlDoc.SelectSingleNode("/amz:BucketLoggingStatus/amz:LoggingEnabled/amz:TargetPrefix", MyXmlNamespaceManager);

                if (XmlNodeTargetBucket == null) {
                    MessageBox.Show("Logging is not enabled.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    MessageBox.Show("Logging is enabled.   TargetBucket=" + XmlNodeTargetBucket.InnerText + "   TargetPrefix=" + TargetPrefix.InnerText, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else {
                String ResponseMessage;
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonSetBucketLogging_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //Server Access Logging Configuration API: http://docs.amazonwebservices.com/AmazonS3/latest/LoggingAPI.html
            //PUT Bucket logging: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTBucketPUTlogging.html

            //ask the user where the log files should be saved
            String TargetBucket;
            TargetBucket = Microsoft.VisualBasic.Interaction.InputBox("To enable logging enter the bucket name where log files will be saved.  To disable logging leave the value below empty.", "Target Bucket", "", this.Location.X + 100, this.Location.Y + 150);

            String PutXML = "";
            Boolean RetBool;

            String RequestURL;
            String RequestMethod = "PUT";
            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            String AuthorizationValue;

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?logging");

            ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            if (TargetBucket == "") {
                //to diable logging send an XML with no LoggingEnabled element
                PutXML = PutXML + "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + Environment.NewLine;
                PutXML = PutXML + "<BucketLoggingStatus xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\"/>";

                RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, PutXML);

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyREST.LogData);
                System.Diagnostics.Debug.Print("");

                if (RetBool == true) {
                    MessageBox.Show("Logging has been disabled.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    String ResponseMessage;
                    ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
            }
            else {
                //This is a simple implementation of enabling logging.  A more complete implementation would do the following:
                //  validate the that TargetBucket exists
                //  get the ACLs of the target bucket.  The LogDelivery group requires WRITE and READ_ACP access to the target bucket
                //  add permission to the target bucket for the LogDelivery group if required

                PutXML = "";
                PutXML = PutXML + "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                PutXML = PutXML + "<BucketLoggingStatus xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">";
                PutXML = PutXML + "<LoggingEnabled>";
                PutXML = PutXML + "<TargetBucket>" + TargetBucket + "</TargetBucket>";
                PutXML = PutXML + "<TargetPrefix>" + TextBoxBucketBucketName.Text + "_log-</TargetPrefix>";
                //uncomment the following lines if you would like the AllUsers group to have read access to new log files
                //PutXML = PutXML + "<TargetGrants>";
                //PutXML = PutXML + "<Grant>";
                //PutXML = PutXML + "<Grantee xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"Group\">";
                //PutXML = PutXML + "<URI>http://acs.amazonaws.com/groups/global/AllUsers</URI>";
                //PutXML = PutXML + "</Grantee>";
                //PutXML = PutXML + "<Permission>READ</Permission>";
                //PutXML = PutXML + "</Grant>";
                //PutXML = PutXML + "</TargetGrants>";
                PutXML = PutXML + "</LoggingEnabled>";
                PutXML = PutXML + "</BucketLoggingStatus>";

                RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, PutXML);

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyREST.LogData);
                System.Diagnostics.Debug.Print("");

                if (RetBool == true) {
                    MessageBox.Show("Logging has been enabled.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    String ResponseMessage;
                    ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonGetBucketACLs_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //Buckets and Access Control: http://docs.amazonwebservices.com/AmazonS3/latest/BucketAccess.html
            //GET Bucket acl: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTBucketGETacl.html

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?acl");

            String RequestMethod = "GET";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            System.Xml.XmlDocument XmlDoc;
            System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
            System.Xml.XmlNode XmlNode;
            System.Xml.XmlNodeList XmlContentsNodeList;
            String ResponseMessage = "";

            if (RetBool == true) {
                XmlDoc = new System.Xml.XmlDocument();
                XmlDoc.LoadXml(MyREST.ResponseString);

                MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                XmlContentsNodeList = XmlDoc.SelectNodes("/amz:AccessControlPolicy/amz:AccessControlList/amz:Grant", MyXmlNamespaceManager);

                foreach (System.Xml.XmlNode XmlContentsNode in XmlContentsNodeList) {
                    XmlNode = XmlContentsNode.SelectSingleNode("./amz:Grantee", MyXmlNamespaceManager);

                    if (XmlNode.Attributes["xsi:type"].Value == "CanonicalUser") {
                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Grantee/amz:ID", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "CanonicalUser.ID = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Grantee/amz:DisplayName", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "CanonicalUser.DisplayName = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Permission", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Permission = " + XmlNode.InnerText + Environment.NewLine;
                    }
                    else if (XmlNode.Attributes["xsi:type"].Value == "AmazonCustomerByEmail") {
                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Grantee/amz:EmailAddress", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "AmazonCustomerByEmail.EmailAddress = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Permission", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Permission = " + XmlNode.InnerText + Environment.NewLine;
                    }
                    else if (XmlNode.Attributes["xsi:type"].Value == "Group") {
                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Grantee/amz:URI", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Group.URI = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Permission", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Permission = " + XmlNode.InnerText + Environment.NewLine;
                    }

                    ResponseMessage = ResponseMessage + Environment.NewLine;
                }

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Success";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }
            else {
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonSetBucketACLs_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            System.Windows.Forms.DialogResult MyDialogResult;
            MyDialogResult = MessageBox.Show("Would you like everyone to have the permission to list the objects in this bucket?", "Set ACLs", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            //Get the owner ID.  It will be used when constructing the Access Control List
            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?acl");

            String RequestMethod = "GET";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Xml.XmlDocument XmlDoc;
            System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
            System.Xml.XmlNode XmlNode;
            String ResponseMessage;

            if (RetBool == true) {
                XmlDoc = new System.Xml.XmlDocument();
                XmlDoc.LoadXml(MyREST.ResponseString);

                MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                XmlNode = XmlDoc.SelectSingleNode("/amz:AccessControlPolicy/amz:Owner/amz:ID", MyXmlNamespaceManager);

                //construct an Access Control List.  Grant the bucket owner FULL_CONTROL and the AllUsers group READ access.  Read access allows listing the bucket
                //Access Control Lists: http://docs.amazonwebservices.com/AmazonS3/latest/S3_ACLs.html

                String PutXML = "";
                PutXML = PutXML + "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                PutXML = PutXML + "<AccessControlPolicy xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">";
                PutXML = PutXML + "<Owner>";
                PutXML = PutXML + "<ID>" + XmlNode.InnerText + "</ID>";
                PutXML = PutXML + "</Owner>";
                PutXML = PutXML + "<AccessControlList>";
                PutXML = PutXML + "<Grant>";
                PutXML = PutXML + "<Grantee xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"CanonicalUser\">";
                PutXML = PutXML + "<ID>" + XmlNode.InnerText + "</ID>";
                PutXML = PutXML + "</Grantee>";
                PutXML = PutXML + "<Permission>FULL_CONTROL</Permission>";
                PutXML = PutXML + "</Grant>";
                //if the user selected yes, add read permission for the AllUsers group
                if (MyDialogResult == System.Windows.Forms.DialogResult.Yes) {
                    PutXML = PutXML + "<Grant>";
                    PutXML = PutXML + "<Grantee xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"Group\">";
                    PutXML = PutXML + "<URI>http://acs.amazonaws.com/groups/global/AllUsers</URI>";
                    PutXML = PutXML + "</Grantee>";
                    PutXML = PutXML + "<Permission>READ</Permission>";
                    PutXML = PutXML + "</Grant>";
                }
                PutXML = PutXML + "</AccessControlList>";
                PutXML = PutXML + "</AccessControlPolicy>";


                RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?acl");

                RequestMethod = "PUT";

                ExtraRequestHeaders = new Dictionary<String, String>();
                ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
                ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

                RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, PutXML);

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyREST.LogData);
                System.Diagnostics.Debug.Print("");

                if (RetBool == true) {
                    MessageBox.Show("ACLs were set on the bucket.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
            }
            else {
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonGetBucketRequestPayment_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //GET Bucket requestPayment: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTrequestPaymentGET.html

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?requestPayment");

            String RequestMethod = "GET";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            System.Xml.XmlDocument XmlDoc;
            System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
            System.Xml.XmlNode XmlNode;

            if (RetBool == true) {
                XmlDoc = new System.Xml.XmlDocument();
                XmlDoc.LoadXml(MyREST.ResponseString);

                MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                XmlNode = XmlDoc.SelectSingleNode("/amz:RequestPaymentConfiguration/amz:Payer", MyXmlNamespaceManager);

                if (XmlNode.InnerText == "BucketOwner")
                    MessageBox.Show("BucketOwner pays.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else {
                    MessageBox.Show("Requester pays.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else {
                String ResponseMessage;
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonSetBucketRequestPayment_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //Requester Pays Buckets: http://docs.amazonwebservices.com/AmazonS3/latest/dev/RequesterPaysBuckets.html
            //PUT Bucket requestPayment: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTrequestPaymentPUT.html

            //get who the payer should be
            System.Windows.Forms.DialogResult MyDialogResult;
            MyDialogResult = MessageBox.Show("Would you like the requester pay for requests?  Click Yes for Requester pays.  Click No for BucketOwner pays.", "Request Payer", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            String Payer;
            if (MyDialogResult == System.Windows.Forms.DialogResult.Yes) {
                Payer = "Requester";
            }
            else {
                Payer = "BucketOwner";
            }

            //Construct the XML to send
            String PutXML = "";
            PutXML = PutXML + "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + Environment.NewLine;
            PutXML = PutXML + "<RequestPaymentConfiguration xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">";
            PutXML = PutXML + "<Payer>" + Payer + "</Payer>";
            PutXML = PutXML + "</RequestPaymentConfiguration>";

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?requestPayment");

            String RequestMethod = "PUT";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, PutXML);

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            if (RetBool == true) {
                MessageBox.Show("RequestPayment has been set to " + Payer + ".", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else {
                String ResponseMessage;
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonGetBucketVersioning_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //GET Bucket versioning: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTBucketGETversioningStatus.html

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?versioning");

            String RequestMethod = "GET";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            System.Xml.XmlDocument XmlDoc;
            System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
            System.Xml.XmlNode XmlNode;

            if (RetBool == true) {
                XmlDoc = new System.Xml.XmlDocument();
                XmlDoc.LoadXml(MyREST.ResponseString);

                MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                XmlNode = XmlDoc.SelectSingleNode("/amz:VersioningConfiguration/amz:Status", MyXmlNamespaceManager);

                if (XmlNode == null) {
                    MessageBox.Show("Versioning is not enabled.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    if (XmlNode.InnerText == "Enabled") {
                        MessageBox.Show("Versioning is enabled.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else {
                        MessageBox.Show("Versioning is suspended.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            else {
                String ResponseMessage;
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonSetBucketVersioning_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //Enabling a Bucket's Versioning State: http://docs.amazonwebservices.com/AmazonS3/latest/dev/EnablingSuspendingandReturningtheVersioningState.html
            //PUT Bucket versioning: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTBucketPUTVersioningStatus.html

            System.Windows.Forms.DialogResult MyDialogResult;
            MyDialogResult = MessageBox.Show("Are you sure you would like to set versioning.  Once versioning is enabled on the bucket, it can never be set back to unversioned.", "Set Versioning", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (MyDialogResult == System.Windows.Forms.DialogResult.Yes) {
                MyDialogResult = MessageBox.Show("Would you like the enable versioning?  Click Yes to enable versioning.  Click No to suspend versioning.", "Set Versioning", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

                String VerStatus;
                if (MyDialogResult == System.Windows.Forms.DialogResult.Yes) {
                    VerStatus = "Enabled";
                }
                else {
                    VerStatus = "Suspended";
                }

                //Construct the XML to send
                String PutXML = "";
                PutXML = PutXML + "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + Environment.NewLine;
                PutXML = PutXML + "<VersioningConfiguration xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">";
                PutXML = PutXML + "<Status>" + VerStatus + "</Status>";
                PutXML = PutXML + "</VersioningConfiguration>";

                SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

                String RequestURL;
                RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?versioning");

                String RequestMethod = "PUT";

                Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
                ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                String AuthorizationValue;
                AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
                ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

                Boolean RetBool;
                RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, PutXML);

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyREST.LogData);
                System.Diagnostics.Debug.Print("");

                if (RetBool == true) {
                    MessageBox.Show("Versioning has been set to " + VerStatus + ".", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    String ResponseMessage;
                    ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonListBucketVersions_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //Listing the Objects in a Versioning-Enabled Bucket: http://docs.amazonwebservices.com/AmazonS3/latest/dev/ListingtheObjectsinaVersioningEnabledBucket.html
            //GET Bucket Object versions: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTBucketGETVersion.html

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String Delimiter = "/";  //use a forward slash as the delimiter if your bucket uses folders and you want to list a single folder.
            String KeyMarker = "";
            String VersionIDMarker = "";
            int MaxKeys = 1000;  //1000 is the maximum number of objects that can be returned in a page
            Boolean IsTruncated = false;

            String QueryString;
            Boolean RetBool;
            System.Xml.XmlDocument XmlDoc;
            System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
            System.Xml.XmlNode XmlNode;
            System.Xml.XmlNode XmlListVersionsResultNode;
            System.Xml.XmlNodeList XmlCommonPrefixesNodeList;
            String ResponseMessage = "";

            String RequestURL;
            String RequestMethod = "GET";
            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            String AuthorizationValue;

            //put the following code in a loop as we may be listing many pages
            do {
                QueryString = "?versions&delimiter=" + System.Uri.EscapeDataString(Delimiter) + "&key-marker=" + System.Uri.EscapeDataString(KeyMarker) + "&max-keys=" + MaxKeys;
                if (VersionIDMarker != "") {
                    QueryString = QueryString + "&version-id-marker=" + System.Uri.EscapeDataString(VersionIDMarker);
                }

                RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", QueryString);

                ExtraRequestHeaders = new Dictionary<String, String>();
                ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));
                AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
                ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

                RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyREST.LogData);
                System.Diagnostics.Debug.Print("");

                if (RetBool == true) {
                    XmlDoc = new System.Xml.XmlDocument();
                    XmlDoc.LoadXml(MyREST.ResponseString);

                    MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                    MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                    XmlNode = XmlDoc.SelectSingleNode("/amz:ListVersionsResult", MyXmlNamespaceManager);
                    if (XmlNode == null) {
                        MessageBox.Show("The ListVersionsResult node does not exist in the response XML.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
                    else {
                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListVersionsResult/amz:Name", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Name = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListVersionsResult/amz:Prefix", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Prefix = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListVersionsResult/amz:KeyMarker", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "KeyMarker = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListVersionsResult/amz:VersionIdMarker", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "VersionIdMarker = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListVersionsResult/amz:NextKeyMarker", MyXmlNamespaceManager);
                        if (XmlNode == null) {
                            KeyMarker = "";
                        }
                        else {
                            //set the KeyMarker to the NextKeyMarker value.  This will be used in the query string in the next call.
                            KeyMarker = XmlNode.InnerText;
                            ResponseMessage = ResponseMessage + "NextKeyMarker = " + XmlNode.InnerText + Environment.NewLine;
                        }

                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListVersionsResult/amz:NextVersionIdMarker", MyXmlNamespaceManager);
                        if (XmlNode == null) {
                            VersionIDMarker = "";
                        }
                        else {
                            //set the VersionIDMarker to the NextVersionIdMarker value.  This will be used in the query string in the next call.
                            VersionIDMarker = XmlNode.InnerText;
                            ResponseMessage = ResponseMessage + "NextVersionIdMarker = " + XmlNode.InnerText + Environment.NewLine;
                        }

                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListVersionsResult/amz:MaxKeys", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "MaxKeys = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListVersionsResult/amz:Delimiter", MyXmlNamespaceManager);
                        if (XmlNode != null) {
                            ResponseMessage = ResponseMessage + "Delimiter = " + XmlNode.InnerText + Environment.NewLine;
                        }

                        XmlNode = XmlDoc.SelectSingleNode("/amz:ListVersionsResult/amz:IsTruncated", MyXmlNamespaceManager);
                        IsTruncated = Convert.ToBoolean(XmlNode.InnerText);
                        ResponseMessage = ResponseMessage + "IsTruncated = " + XmlNode.InnerText + Environment.NewLine;


                        XmlListVersionsResultNode = XmlDoc.SelectSingleNode("/amz:ListVersionsResult", MyXmlNamespaceManager);

                        foreach (System.Xml.XmlElement ChildNodeList in XmlListVersionsResultNode.ChildNodes) {
                            if (ChildNodeList.Name == "DeleteMarker") {
                                ResponseMessage = ResponseMessage + "DeleteMarker";

                                XmlNode = ChildNodeList.SelectSingleNode("./amz:Key", MyXmlNamespaceManager);
                                ResponseMessage = ResponseMessage + "     Key = " + XmlNode.InnerText;

                                XmlNode = ChildNodeList.SelectSingleNode("./amz:VersionId", MyXmlNamespaceManager);
                                ResponseMessage = ResponseMessage + "     VersionId = " + XmlNode.InnerText;

                                XmlNode = ChildNodeList.SelectSingleNode("./amz:IsLatest", MyXmlNamespaceManager);
                                ResponseMessage = ResponseMessage + "     IsLatest = " + XmlNode.InnerText;

                                XmlNode = ChildNodeList.SelectSingleNode("./amz:LastModified", MyXmlNamespaceManager);
                                ResponseMessage = ResponseMessage + "     LastModified = " + XmlNode.InnerText;

                                XmlNode = ChildNodeList.SelectSingleNode("./amz:Owner/amz:ID", MyXmlNamespaceManager);
                                if (XmlNode != null) {
                                    ResponseMessage = ResponseMessage + "     Owner ID = " + XmlNode.InnerText;
                                }

                                ResponseMessage = ResponseMessage + Environment.NewLine;
                            }
                            else if (ChildNodeList.Name == "Version") {
                                ResponseMessage = ResponseMessage + "Version";

                                XmlNode = ChildNodeList.SelectSingleNode("./amz:Key", MyXmlNamespaceManager);
                                ResponseMessage = ResponseMessage + "     Key = " + XmlNode.InnerText;

                                XmlNode = ChildNodeList.SelectSingleNode("./amz:VersionId", MyXmlNamespaceManager);
                                ResponseMessage = ResponseMessage + "     VersionId = " + XmlNode.InnerText;

                                XmlNode = ChildNodeList.SelectSingleNode("./amz:IsLatest", MyXmlNamespaceManager);
                                ResponseMessage = ResponseMessage + "     IsLatest = " + XmlNode.InnerText;

                                XmlNode = ChildNodeList.SelectSingleNode("./amz:LastModified", MyXmlNamespaceManager);
                                ResponseMessage = ResponseMessage + "     LastModified = " + XmlNode.InnerText;

                                XmlNode = ChildNodeList.SelectSingleNode("./amz:ETag", MyXmlNamespaceManager);
                                ResponseMessage = ResponseMessage + "     ETag = " + XmlNode.InnerText;

                                XmlNode = ChildNodeList.SelectSingleNode("./amz:Size", MyXmlNamespaceManager);
                                ResponseMessage = ResponseMessage + "     Size = " + XmlNode.InnerText;

                                XmlNode = ChildNodeList.SelectSingleNode("./amz:Owner/amz:ID", MyXmlNamespaceManager);
                                if (XmlNode != null) {
                                    ResponseMessage = ResponseMessage + "     Owner ID = " + XmlNode.InnerText;
                                }

                                XmlNode = ChildNodeList.SelectSingleNode("./amz:StorageClass", MyXmlNamespaceManager);
                                ResponseMessage = ResponseMessage + "     StorageClass = " + XmlNode.InnerText;

                                ResponseMessage = ResponseMessage + Environment.NewLine;
                            }
                        }

                        XmlCommonPrefixesNodeList = XmlDoc.SelectNodes("/amz:ListVersionsResult/amz:CommonPrefixes", MyXmlNamespaceManager);
                        foreach (System.Xml.XmlNode XmlPrefixesNode in XmlCommonPrefixesNodeList) {
                            XmlNode = XmlPrefixesNode.SelectSingleNode("./amz:Prefix", MyXmlNamespaceManager);
                            ResponseMessage = ResponseMessage + "Prefix = " + XmlNode.InnerText + Environment.NewLine;
                        }

                        ResponseMessage = ResponseMessage + Environment.NewLine;
                    }
                }
                else {
                    ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);

                    break;
                }

                if (IsTruncated == false) {
                    //if there are no more pages to get, show the output and exit
                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Success";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                    break;
                }

            } while (IsTruncated == true);

            (sender as Button).Enabled = true;
        }

        private void ButtonGetBucketPolicies_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //Using Bucket Policies: http://docs.amazonwebservices.com/AmazonS3/latest/dev/UsingBucketPolicies.html
            //Returning the Bucket Policies on a Bucket: http://docs.amazonwebservices.com/AmazonS3/latest/dev/returnbucketpolicyonbucket.html

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?policy");

            String RequestMethod = "GET";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            if (RetBool == true) {
                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Success";
                MyDialogOutput.TextBoxOutput.Text = MyREST.ResponseString;
                MyDialogOutput.ShowDialog(this);
            }
            else {
                String ResponseMessage = "";
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonSetBucketPolicies_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //Setting Bucket Policies on a Bucket: http://docs.amazonwebservices.com/AmazonS3/latest/dev/setpolicyonbucket.html

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();
            String PutString = "";

            //The following example policy grants access to read files from the IP address of 168.144.123.123.
            PutString = PutString + "{" + Environment.NewLine;
            PutString = PutString + "  \"Version\":\"2008-10-17\"," + Environment.NewLine;
            PutString = PutString + "  \"Statement\":[{" + Environment.NewLine;
            PutString = PutString + "	\"Sid\":\"IPAllow\"," + Environment.NewLine;
            PutString = PutString + "        \"Effect\":\"Allow\"," + Environment.NewLine;
            PutString = PutString + "	  \"Principal\": {" + Environment.NewLine;
            PutString = PutString + "            \"AWS\": \"*\"" + Environment.NewLine;
            PutString = PutString + "      }," + Environment.NewLine;
            PutString = PutString + "      \"Action\":\"s3:GetObject\"," + Environment.NewLine;
            PutString = PutString + "      \"Resource\":\"arn:aws:s3:::" + TextBoxBucketBucketName.Text + "/*\"," + Environment.NewLine;
            PutString = PutString + "      \"Condition\":{" + Environment.NewLine;
            PutString = PutString + "        \"IpAddress\":{" + Environment.NewLine;
            PutString = PutString + "        \"aws:SourceIp\":\"168.144.123.123\"" + Environment.NewLine;
            PutString = PutString + "        }" + Environment.NewLine;
            PutString = PutString + "      }" + Environment.NewLine;
            PutString = PutString + "    }" + Environment.NewLine;
            PutString = PutString + "  ]" + Environment.NewLine;
            PutString = PutString + "}" + Environment.NewLine;

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?policy");

            String RequestMethod = "PUT";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, PutString);

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            if (RetBool == true) {
                MessageBox.Show("The buckey policy has been set.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else {
                String ResponseMessage;
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonDeleteBucketPolicies_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //Deleting Bucket Policies on a Bucket: http://docs.amazonwebservices.com/AmazonS3/latest/dev/deletebucketpolicy.html

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?policy");

            String RequestMethod = "DELETE";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            if (RetBool == true) {
                MessageBox.Show("The policy was deleted.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else {
                String ResponseMessage;
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonGetBucketNotification_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //GET Bucket notification: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTBucketGETnotification.html

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?notification");

            String RequestMethod = "GET";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            System.Xml.XmlDocument XmlDoc;
            System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
            System.Xml.XmlNode XmlNodeTopic;
            System.Xml.XmlNode XmlNodeEvent;

            if (RetBool == true) {
                XmlDoc = new System.Xml.XmlDocument();
                XmlDoc.LoadXml(MyREST.ResponseString);

                MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                XmlNodeTopic = XmlDoc.SelectSingleNode("/amz:NotificationConfiguration/amz:TopicConfiguration/amz:Topic", MyXmlNamespaceManager);
                XmlNodeEvent = XmlDoc.SelectSingleNode("/amz:NotificationConfiguration/amz:TopicConfiguration/amz:Event", MyXmlNamespaceManager);

                if (XmlNodeTopic == null) {
                    MessageBox.Show("Notification is not enabled.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    MessageBox.Show("Notification is enabled.   Topic=" + XmlNodeTopic.InnerText + "   Event=" + XmlNodeEvent.InnerText, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else {
                String ResponseMessage;
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonSetBucketNotification_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //Setting Up Notification of Bucket Events: http://docs.amazonwebservices.com/AmazonS3/latest/dev/NotificationHowTo.html
            //PUT Bucket notification: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTBucketPUTnotification.html

            String NotificationTopic;
            NotificationTopic = Microsoft.VisualBasic.Interaction.InputBox("Enter the Amazon Simple Notification Service (SNS) topic the notification will be sent to if Amazon S3 looses all copies of a Reduced Redundancy Storage (RRS) object.  To disable notification leave the value below empty.", "Amazon Simple Notification Service Topic", "", this.Location.X + 100, this.Location.Y + 150);

            String PutXML = "";
            Boolean RetBool;

            String RequestURL;
            String RequestMethod = "PUT";
            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            String AuthorizationValue;

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            if (NotificationTopic == "") {
                //to diable notification send an XML with an empty NotificationConfiguration element
                PutXML = PutXML + "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + Environment.NewLine;
                PutXML = PutXML + "<NotificationConfiguration xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\"/>";

                RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?notification");

                ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
                ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

                RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, PutXML);

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyREST.LogData);
                System.Diagnostics.Debug.Print("");

                if (RetBool == true) {
                    MessageBox.Show("Notification has been disabled.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    String ResponseMessage;
                    ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
            }
            else {
                PutXML = "";
                PutXML = PutXML + "<NotificationConfiguration>";
                PutXML = PutXML + "<TopicConfiguration>";
                PutXML = PutXML + "<Topic>" + NotificationTopic + "</Topic>";
                PutXML = PutXML + "<Event>s3:ReducedRedundancyLostObject</Event>";
                PutXML = PutXML + "</TopicConfiguration>";
                PutXML = PutXML + "</NotificationConfiguration>";

                RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxBucketBucketName.Text, "", "?notification");

                ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
                ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

                RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, PutXML);

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyREST.LogData);
                System.Diagnostics.Debug.Print("");

                if (RetBool == true) {
                    MessageBox.Show("Notification has been enabled.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    String ResponseMessage;
                    ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonGetObjectProperties_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //HEAD Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectHEAD.html

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxObjectBucketName.Text, TextBoxObjectKeyName.Text, "");

            String RequestMethod = "HEAD";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            String ResponseMessage = "";

            if (RetBool == true) {
                foreach (KeyValuePair<String, String> MyKeyValuePair in MyREST.ResponseHeaders) {
                    if (MyKeyValuePair.Key.StartsWith("x-amz-") == true) {
                        if (MyKeyValuePair.Key != "x-amz-id-2" && MyKeyValuePair.Key != "x-amz-request-id")  //Ignore the Amazon ID response headers.  They are included in every response and they could be used to troubleshoot issues on Amazon's end.
                        {
                            ResponseMessage = ResponseMessage + "Amazon header = " + MyKeyValuePair.Key + ":" + MyKeyValuePair.Value + Environment.NewLine;
                        }
                    }
                }

                if (MyREST.ResponseHeaders.ContainsKey("Cache-Control") == true) {
                    ResponseMessage = ResponseMessage + "Cache-Control = " + MyREST.ResponseHeaders["Cache-Control"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("Content-Disposition") == true) {
                    ResponseMessage = ResponseMessage + "Content-Disposition = " + MyREST.ResponseHeaders["Content-Disposition"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("Content-Encoding") == true) {
                    ResponseMessage = ResponseMessage + "Content-Encoding = " + MyREST.ResponseHeaders["Content-Encoding"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("Content-Language") == true) {
                    ResponseMessage = ResponseMessage + "Content-Language = " + MyREST.ResponseHeaders["Content-Language"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("Content-Type") == true) {
                    ResponseMessage = ResponseMessage + "Content-Type = " + MyREST.ResponseHeaders["Content-Type"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("Expires") == true) {
                    ResponseMessage = ResponseMessage + "Expires = " + MyREST.ResponseHeaders["Expires"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("Content-Length") == true) {
                    ResponseMessage = ResponseMessage + "Content-Length = " + MyREST.ResponseHeaders["Content-Length"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("Last-Modified") == true) {
                    ResponseMessage = ResponseMessage + "Last-Modified = " + MyREST.ResponseHeaders["Last-Modified"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("ETag") == true) {
                    ResponseMessage = ResponseMessage + "ETag = " + MyREST.ResponseHeaders["ETag"] + Environment.NewLine;
                }

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Success";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }
            else {
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonDeleteObject_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //The following code deletes an object in a bucket.  If versioning on the bucket is enabled,
            //the call will replace the object with a Delete Marker.

            //DELETE Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectDELETE.html

            System.Windows.Forms.DialogResult MyDialogResult;
            MyDialogResult = MessageBox.Show("Are you sure you would like to delete the object " + TextBoxObjectKeyName.Text + "?", "Delete Object", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (MyDialogResult == System.Windows.Forms.DialogResult.Yes) {
                SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

                String RequestURL;
                RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxObjectBucketName.Text, TextBoxObjectKeyName.Text, "");

                String RequestMethod = "DELETE";

                Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
                ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                String AuthorizationValue;
                AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
                ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

                Boolean RetBool;
                RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyREST.LogData);
                System.Diagnostics.Debug.Print("");

                if (RetBool == true) {
                    MessageBox.Show("The object was deleted.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    String ResponseMessage;
                    ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonObjectExists_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //do a HEAD operation on the object to see if it exists

            //HEAD Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectHEAD.html

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxObjectBucketName.Text, TextBoxObjectKeyName.Text, "");

            String RequestMethod = "HEAD";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            if (MyREST.ResponseStatusCode == 200) {
                MessageBox.Show("The object exists, you have permission to it.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (MyREST.ResponseStatusCode == 404) {
                MessageBox.Show("The object does not exists.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (MyREST.ResponseStatusCode == 403) {
                MessageBox.Show("Permission error.  Someone else owns the object or your access keys are invalid.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else {
                String ResponseMessage;
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonGetObjectACLs_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //GET Object acl: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectGETacl.html

            System.Xml.XmlDocument XmlDoc;
            System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
            System.Xml.XmlNode XmlNode;
            System.Xml.XmlNodeList XmlContentsNodeList;
            String ResponseMessage = "";

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxObjectBucketName.Text, TextBoxObjectKeyName.Text, "?acl");

            String RequestMethod = "GET";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            if (RetBool == true) {
                XmlDoc = new System.Xml.XmlDocument();
                XmlDoc.LoadXml(MyREST.ResponseString);

                MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                XmlContentsNodeList = XmlDoc.SelectNodes("/amz:AccessControlPolicy/amz:AccessControlList/amz:Grant", MyXmlNamespaceManager);

                foreach (System.Xml.XmlNode XmlContentsNode in XmlContentsNodeList) {
                    XmlNode = XmlContentsNode.SelectSingleNode("./amz:Grantee", MyXmlNamespaceManager);

                    if (XmlNode.Attributes["xsi:type"].Value == "CanonicalUser") {
                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Grantee/amz:ID", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "CanonicalUser.ID = " + XmlNode.InnerText + Environment.NewLine;
                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Grantee/amz:DisplayName", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "CanonicalUser.DisplayName = " + XmlNode.InnerText + Environment.NewLine;
                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Permission", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Permission = " + XmlNode.InnerText + Environment.NewLine;
                        ResponseMessage = ResponseMessage + Environment.NewLine;
                    }
                    else if (XmlNode.Attributes["xsi:type"].Value == "AmazonCustomerByEmail") {
                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Grantee/amz:EmailAddress", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "AmazonCustomerByEmail.EmailAddress = " + XmlNode.InnerText + Environment.NewLine;
                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Permission", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Permission = " + XmlNode.InnerText + Environment.NewLine;
                        ResponseMessage = ResponseMessage + Environment.NewLine;
                    }
                    else if (XmlNode.Attributes["xsi:type"].Value == "Group") {
                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Grantee/amz:URI", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Group.URI = " + XmlNode.InnerText + Environment.NewLine;
                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Permission", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Permission = " + XmlNode.InnerText + Environment.NewLine;
                        ResponseMessage = ResponseMessage + Environment.NewLine;
                    }
                }

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Success";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }
            else {
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonSetObjectACLs_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //PUT Object acl: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectPUTacl.html

            System.Windows.Forms.DialogResult MyDialogResult;
            MyDialogResult = MessageBox.Show("Would you like everyone to have the permission to download this object?", "Set ACLs", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxObjectBucketName.Text, TextBoxObjectKeyName.Text, "?acl");

            String RequestMethod = "GET";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            //get the owner of the object.  It will be used when constructing the Access Control List.
            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Xml.XmlDocument XmlDoc;
            System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
            System.Xml.XmlNode XmlNode;

            if (RetBool == true) {
                XmlDoc = new System.Xml.XmlDocument();
                XmlDoc.LoadXml(MyREST.ResponseString);

                MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                XmlNode = XmlDoc.SelectSingleNode("/amz:AccessControlPolicy/amz:Owner/amz:ID", MyXmlNamespaceManager);

                //construct an Access Control List.  Grant the AllUsers group READ permission.
                //Access Control Lists: http://docs.amazonwebservices.com/AmazonS3/latest/S3_ACLs.html

                String PutXML = "";
                PutXML = PutXML + "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                PutXML = PutXML + "<AccessControlPolicy xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">";
                PutXML = PutXML + "<Owner>";
                PutXML = PutXML + "<ID>" + XmlNode.InnerText + "</ID>";
                PutXML = PutXML + "</Owner>";
                PutXML = PutXML + "<AccessControlList>";
                PutXML = PutXML + "<Grant>";
                PutXML = PutXML + "<Grantee xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"CanonicalUser\">";
                PutXML = PutXML + "<ID>" + XmlNode.InnerText + "</ID>";
                PutXML = PutXML + "</Grantee>";
                PutXML = PutXML + "<Permission>FULL_CONTROL</Permission>";
                PutXML = PutXML + "</Grant>";
                if (MyDialogResult == System.Windows.Forms.DialogResult.Yes) {
                    PutXML = PutXML + "<Grant>";
                    PutXML = PutXML + "<Grantee xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"Group\">";
                    PutXML = PutXML + "<URI>http://acs.amazonaws.com/groups/global/AllUsers</URI>";
                    PutXML = PutXML + "</Grantee>";
                    PutXML = PutXML + "<Permission>READ</Permission>";
                    PutXML = PutXML + "</Grant>";
                }
                PutXML = PutXML + "</AccessControlList>";
                PutXML = PutXML + "</AccessControlPolicy>";


                RequestMethod = "PUT";

                RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxObjectBucketName.Text, TextBoxObjectKeyName.Text, "?acl");

                ExtraRequestHeaders = new Dictionary<String, String>();
                ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));
                AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
                ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

                RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, PutXML);

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyREST.LogData);
                System.Diagnostics.Debug.Print("");

                if (RetBool == true) {
                    MessageBox.Show("ACLs were set on the object.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    String ResponseMessage;
                    ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
            }
            else {
                String ResponseMessage;
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonCopyObject_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //Copying Amazon S3 Objects: http://docs.amazonwebservices.com/AmazonS3/latest/UsingCopyingObjects.html
            //PUT Object (Copy): http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectCOPY.html

            //This is a simple implementation of copying an object.  A more complete implementation would do the following:
            //  check if the destination file exists and prompt the user if they would like to overwrite it
            //  get the ACLs from the source file and apply them to the new file

            String NewKeyName;
            NewKeyName = TextBoxObjectKeyName.Text + "(1)";
            NewKeyName = Microsoft.VisualBasic.Interaction.InputBox("Enter the new key name.", "Copy Object", NewKeyName, this.Location.X + 100, this.Location.Y + 150);

            if (NewKeyName != "") {
                Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
                ExtraRequestHeaders.Add("x-amz-copy-source", "/" + TextBoxObjectBucketName.Text + "/" + Uri.EscapeDataString(TextBoxObjectKeyName.Text));
                ExtraRequestHeaders.Add("x-amz-metadata-directive", "COPY");  //use this request header to copy the metadata and extra headers

                SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

                String RequestURL;
                RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxObjectBucketName.Text, NewKeyName, "");

                String RequestMethod = "PUT";

                ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                String AuthorizationValue;
                AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
                ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

                Boolean RetBool;
                RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyREST.LogData);
                System.Diagnostics.Debug.Print("");

                System.Xml.XmlDocument XmlDoc;
                System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
                System.Xml.XmlNode XmlNode;
                String ResponseMessage = "";

                if (RetBool == true) {
                    XmlDoc = new System.Xml.XmlDocument();
                    XmlDoc.LoadXml(MyREST.ResponseString);

                    MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                    MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                    XmlNode = XmlDoc.SelectSingleNode("/amz:CopyObjectResult/amz:LastModified", MyXmlNamespaceManager);
                    ResponseMessage = ResponseMessage + "LastModified = " + XmlNode.InnerText + Environment.NewLine;

                    XmlNode = XmlDoc.SelectSingleNode("/amz:CopyObjectResult/amz:ETag", MyXmlNamespaceManager);
                    ResponseMessage = ResponseMessage + "ETag = " + XmlNode.InnerText + Environment.NewLine;

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Success";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
                else {
                    ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonSetObjectMetadata_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            String MetadataValue;
            MetadataValue = Microsoft.VisualBasic.Interaction.InputBox("Enter the value for a metadata key named x-amz-meta-mymetadata.", "Set Metadata", "", this.Location.X + 100, this.Location.Y + 150);

            if (MetadataValue != "") {
                //Metadata: http://docs.amazonwebservices.com/AmazonS3/latest/UsingMetadata.html
                //PUT Object (Copy): http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectCOPY.html
                //To update the metadata you basically copy the object and supply new metadata.  The source key name and destination key name will be the same.
                //A more complete implementation would get the ACLs before copying the object and set the ACLs after copying the object.

                Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
                ExtraRequestHeaders.Add("x-amz-copy-source", "/" + TextBoxObjectBucketName.Text + "/" + Uri.EscapeDataString(TextBoxObjectKeyName.Text));
                ExtraRequestHeaders.Add("x-amz-metadata-directive", "REPLACE");  //use this directive to replace the metadata and custom HTTP headers

                //add your metadata headers here.
                ExtraRequestHeaders.Add("x-amz-meta-mymetadata", MetadataValue);
                //ExtraRequestHeaders.Add("Content-Type", "text/plain")  //you can add custom HTTP headers such as Content-Type here.

                SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

                String RequestURL;
                RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxObjectBucketName.Text, TextBoxObjectKeyName.Text, "");

                String RequestMethod = "PUT";

                ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));
                String AuthorizationValue;
                AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
                ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

                Boolean RetBool;
                RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyREST.LogData);
                System.Diagnostics.Debug.Print("");

                System.Xml.XmlDocument XmlDoc;
                System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
                System.Xml.XmlNode XmlNode;
                String ResponseMessage;

                if (RetBool == true) {
                    XmlDoc = new System.Xml.XmlDocument();
                    XmlDoc.LoadXml(MyREST.ResponseString);

                    MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                    MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                    ResponseMessage = "The metadata has been updated.";

                    XmlNode = XmlDoc.SelectSingleNode("/amz:CopyObjectResult/amz:LastModified", MyXmlNamespaceManager);
                    System.Diagnostics.Debug.Print("LastModified=" + XmlNode.InnerText);

                    XmlNode = XmlDoc.SelectSingleNode("/amz:CopyObjectResult/amz:ETag", MyXmlNamespaceManager);
                    System.Diagnostics.Debug.Print("ETag=" + XmlNode.InnerText);

                    MessageBox.Show(ResponseMessage, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonGenerateURL_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //You would generate a URL if you want to share a file but the AllUsers group does not have read permission to the object.
            //The following exampe creates a URL that is valid for 1 day.

            //Query String Authentication: http://docs.amazonwebservices.com/AmazonS3/latest/S3_QSAuth.html

            SprightlySoftAWS.S3.Helper MyS3Helper = new SprightlySoftAWS.S3.Helper();

            String ExpiresURL;
            ExpiresURL = MyS3Helper.BuildExpiresURL(false, "s3.amazonaws.com", TextBoxObjectBucketName.Text, TextBoxObjectKeyName.Text, "", DateTime.Now.AddDays(1), TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);

            DialogOutput MyDialogOutput = new DialogOutput();
            MyDialogOutput.Text = "Success";
            MyDialogOutput.TextBoxOutput.Text = ExpiresURL;
            MyDialogOutput.ShowDialog(this);

            (sender as Button).Enabled = true;
        }

        private void ButtonUploadBrowse_Click(object sender, EventArgs e) {
            System.Windows.Forms.OpenFileDialog OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            OpenFileDialog.Multiselect = false;
            OpenFileDialog.FileName = "";
            OpenFileDialog.ShowDialog();

            if (OpenFileDialog.FileName != "") {
                if (System.IO.File.Exists(OpenFileDialog.FileName) == true) {
                    TextBoxUploadFileName.Text = OpenFileDialog.FileName;
                    TextBoxUploadKeyName.Text = System.IO.Path.GetFileName(OpenFileDialog.FileName);

                    //set the content type from the file's extension
                    //the SprightlySoftS3 Helper class contains a function that returns a dictionary of extensions and associated content types
                    SprightlySoftAWS.S3.Helper MyS3Helper = new SprightlySoftAWS.S3.Helper();
                    Dictionary<String, String> ContentTypesDictionary;
                    ContentTypesDictionary = MyS3Helper.GetContentTypesDictionary();

                    String MyExtension;
                    MyExtension = System.IO.Path.GetExtension(OpenFileDialog.FileName);
                    MyExtension = MyExtension.ToLower();

                    if (ContentTypesDictionary.ContainsKey(MyExtension) == true) {
                        TextBoxUploadContentType.Text = ContentTypesDictionary[MyExtension];
                    }
                    else {
                        TextBoxUploadContentType.Text = "";
                    }
                }
            }
        }

        private void ButtonUploadFile_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            if (System.IO.File.Exists(TextBoxUploadFileName.Text) == true) {
                //PUT Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectPUT.html

                Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();

                if (TextBoxUploadContentType.Text != "") {
                    ExtraRequestHeaders.Add("Content-Type", TextBoxUploadContentType.Text);
                }

                if (CheckBoxUploadMakePublic.Checked == true) {
                    //add a x-amz-acl header with the value of public-read to make the uploaded file public
                    //REST Access Control Policy: http://docs.amazonwebservices.com/AmazonS3/2006-03-01/dev/RESTAccessPolicy.html
                    ExtraRequestHeaders.Add("x-amz-acl", "public-read");
                }

                if (ComboBoxStorageClass.Text == "REDUCED_REDUNDANCY") {
                    //add a x-amz-storage-class header with the value of REDUCED_REDUNDANCY to make the uploaded file reduced redundancy
                    ExtraRequestHeaders.Add("x-amz-storage-class", "REDUCED_REDUNDANCY");
                }

                //add a Content-MD5 header to ensure data is not corrupted over the network
                //Amazon will return an error if the MD5 they calulate does not match the MD5 you send
                SprightlySoftAWS.S3.CalculateHash MyCalculateHash = new SprightlySoftAWS.S3.CalculateHash();

                String MyMD5;
                MyMD5 = MyCalculateHash.CalculateMD5FromFile(TextBoxUploadFileName.Text);
                ExtraRequestHeaders.Add("Content-MD5", MyMD5);


                SprightlySoftAWS.S3.Upload MyUpload = new SprightlySoftAWS.S3.Upload();

                String RequestURL;
                RequestURL = MyUpload.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxUploadBucketName.Text, TextBoxUploadKeyName.Text, "");

                String RequestMethod = "PUT";

                ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                String AuthorizationValue;
                AuthorizationValue = MyUpload.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
                ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

                Boolean RetBool;
                RetBool = MyUpload.UploadFile(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxUploadFileName.Text);

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyUpload.LogData);
                System.Diagnostics.Debug.Print("");

                if (RetBool == true) {
                    MessageBox.Show("Upload complete.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    String ResponseMessage;
                    ResponseMessage = FormatLogData(MyUpload.RequestURL, MyUpload.RequestMethod, MyUpload.RequestHeaders, MyUpload.ResponseStatusCode, MyUpload.ResponseStatusDescription, MyUpload.ResponseHeaders, MyUpload.ResponseStringFormatted, MyUpload.ErrorNumber, MyUpload.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
            }
            else {
                MessageBox.Show("The local file does not exist.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonDownloadFile_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(TextBoxDownloadFileName.Text)) == true) {
                //GET Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectGET.html

                SprightlySoftAWS.S3.Download MyDownload = new SprightlySoftAWS.S3.Download();

                String RequestURL;
                RequestURL = MyDownload.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxDownloadBucketName.Text, TextBoxDownloadKeyName.Text, "");

                String RequestMethod = "GET";

                Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
                ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                String AuthorizationValue;
                AuthorizationValue = MyDownload.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
                ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

                Boolean RetBool;
                RetBool = MyDownload.DownloadFile(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxDownloadFileName.Text, false);

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyDownload.LogData);
                System.Diagnostics.Debug.Print("");

                if (RetBool == true) {
                    MessageBox.Show("Download complete.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    String ResponseMessage;
                    ResponseMessage = FormatLogData(MyDownload.RequestURL, MyDownload.RequestMethod, MyDownload.RequestHeaders, MyDownload.ResponseStatusCode, MyDownload.ResponseStatusDescription, MyDownload.ResponseHeaders, MyDownload.ResponseStringFormatted, MyDownload.ErrorNumber, MyDownload.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
            }
            else {
                MessageBox.Show("The local file path does not exist.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonHashBrowse_Click(object sender, EventArgs e) {
            System.Windows.Forms.OpenFileDialog OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            OpenFileDialog.Multiselect = false;
            OpenFileDialog.FileName = "";
            OpenFileDialog.ShowDialog();

            if (OpenFileDialog.FileName != "") {
                if (System.IO.File.Exists(OpenFileDialog.FileName) == true) {
                    TextBoxHashFileName.Text = OpenFileDialog.FileName;
                }
            }
        }

        private void ButtonCalculateETag_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            SprightlySoftAWS.S3.CalculateHash MyCalculateHash = new SprightlySoftAWS.S3.CalculateHash();

            String ResponseValue;
            ResponseValue = MyCalculateHash.CalculateETagFromFile(TextBoxHashFileName.Text);

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyCalculateHash.LogData);
            System.Diagnostics.Debug.Print("");

            if (MyCalculateHash.ErrorNumber == 0) {
                MessageBox.Show("ETag = " + ResponseValue, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else {
                MessageBox.Show(MyCalculateHash.ErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonCalculateMD5_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            SprightlySoftAWS.S3.CalculateHash MyCalculateHash = new SprightlySoftAWS.S3.CalculateHash();

            String ResponseValue;
            ResponseValue = MyCalculateHash.CalculateMD5FromFile(TextBoxHashFileName.Text);

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyCalculateHash.LogData);
            System.Diagnostics.Debug.Print("");

            if (MyCalculateHash.ErrorNumber == 0) {
                MessageBox.Show("MD5 = " + ResponseValue, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else {
                MessageBox.Show(MyCalculateHash.ErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonVerGetProperties_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //You can get the properties for any version of an object.  Do a HEAD call on the object
            //and pass the versionID in the query string.

            //HEAD Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectHEAD.html

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String QueryString;
            QueryString = "?versionId=" + TextBoxVerVersionID.Text;

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxVerBucketName.Text, TextBoxVerKeyName.Text, QueryString);

            String RequestMethod = "HEAD";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            String ResponseMessage = "";

            if (RetBool == true) {
                foreach (KeyValuePair<String, String> MyKeyValuePair in MyREST.ResponseHeaders) {
                    if (MyKeyValuePair.Key.StartsWith("x-amz-") == true) {
                        if (MyKeyValuePair.Key != "x-amz-id-2" && MyKeyValuePair.Key != "x-amz-request-id")  //Ignore the Amazon ID response headers.  They are included in every response and they could be used to troubleshoot issues on Amazon's end.
                        {
                            ResponseMessage = ResponseMessage + "Amazon header = " + MyKeyValuePair.Key + ":" + MyKeyValuePair.Value + Environment.NewLine;
                        }
                    }
                }

                if (MyREST.ResponseHeaders.ContainsKey("Cache-Control") == true) {
                    ResponseMessage = ResponseMessage + "Cache-Control = " + MyREST.ResponseHeaders["Cache-Control"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("Content-Disposition") == true) {
                    ResponseMessage = ResponseMessage + "Content-Disposition = " + MyREST.ResponseHeaders["Content-Disposition"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("Content-Encoding") == true) {
                    ResponseMessage = ResponseMessage + "Content-Encoding = " + MyREST.ResponseHeaders["Content-Encoding"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("Content-Language") == true) {
                    ResponseMessage = ResponseMessage + "Content-Language = " + MyREST.ResponseHeaders["Content-Language"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("Content-Type") == true) {
                    ResponseMessage = ResponseMessage + "Content-Type = " + MyREST.ResponseHeaders["Content-Type"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("Expires") == true) {
                    ResponseMessage = ResponseMessage + "Expires = " + MyREST.ResponseHeaders["Expires"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("Content-Length") == true) {
                    ResponseMessage = ResponseMessage + "Content-Length = " + MyREST.ResponseHeaders["Content-Length"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("Last-Modified") == true) {
                    ResponseMessage = ResponseMessage + "Last-Modified = " + MyREST.ResponseHeaders["Last-Modified"] + Environment.NewLine;
                }

                if (MyREST.ResponseHeaders.ContainsKey("ETag") == true) {
                    ResponseMessage = ResponseMessage + "ETag = " + MyREST.ResponseHeaders["ETag"] + Environment.NewLine;
                }

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Success";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }
            else {
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonVerDeleteVersion_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //You can delete any version of an object by specifying the versionId in the query string.
            //When you specify a versionId the version is permanently deleted.  You can also delete a 
            //Delete Marker with this call.

            //DELETE Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectDELETE.html

            System.Windows.Forms.DialogResult MyDialogResult;
            MyDialogResult = MessageBox.Show("Are you sure you would like to delete the version " + TextBoxVerKeyName.Text + "?", "Delete Object", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (MyDialogResult == System.Windows.Forms.DialogResult.Yes) {
                SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

                String QueryString;
                QueryString = "?versionId=" + TextBoxVerVersionID.Text;

                String RequestURL;
                RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxVerBucketName.Text, TextBoxVerKeyName.Text, QueryString);

                String RequestMethod = "DELETE";

                Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
                ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                String AuthorizationValue;
                AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
                ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

                Boolean RetBool;
                RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyREST.LogData);
                System.Diagnostics.Debug.Print("");

                if (RetBool == true) {
                    MessageBox.Show("The version was deleted.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    String ResponseMessage;
                    ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonVerGetACLs_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //You can get the ACL of any version of an object by specifying the versionId in the query string.

            //GET Object acl: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectGETacl.html

            System.Xml.XmlDocument XmlDoc;
            System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
            System.Xml.XmlNode XmlNode;
            System.Xml.XmlNodeList XmlContentsNodeList;

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String QueryString;
            QueryString = "?acl&versionId=" + TextBoxVerVersionID.Text;

            String RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxVerBucketName.Text, TextBoxVerKeyName.Text, QueryString);

            String RequestMethod = "GET";

            Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            System.Diagnostics.Debug.Print("");
            System.Diagnostics.Debug.Print(MyREST.LogData);
            System.Diagnostics.Debug.Print("");

            if (RetBool == true) {
                String ResponseMessage = "";

                XmlDoc = new System.Xml.XmlDocument();
                XmlDoc.LoadXml(MyREST.ResponseString);

                MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                XmlContentsNodeList = XmlDoc.SelectNodes("/amz:AccessControlPolicy/amz:AccessControlList/amz:Grant", MyXmlNamespaceManager);

                foreach (System.Xml.XmlNode XmlContentsNode in XmlContentsNodeList) {
                    XmlNode = XmlContentsNode.SelectSingleNode("./amz:Grantee", MyXmlNamespaceManager);

                    if (XmlNode.Attributes["xsi:type"].Value == "CanonicalUser") {
                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Grantee/amz:ID", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "CanonicalUser.ID = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Grantee/amz:DisplayName", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "CanonicalUser.DisplayName = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Permission", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Permission = " + XmlNode.InnerText + Environment.NewLine;
                    }
                    else if (XmlNode.Attributes["xsi:type"].Value == "AmazonCustomerByEmail") {
                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Grantee/amz:EmailAddress", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "AmazonCustomerByEmail.EmailAddress = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Permission", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Permission = " + XmlNode.InnerText + Environment.NewLine;
                    }
                    else if (XmlNode.Attributes["xsi:type"].Value == "Group") {
                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Grantee/amz:URI", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Group.URI = " + XmlNode.InnerText + Environment.NewLine;

                        XmlNode = XmlContentsNode.SelectSingleNode("./amz:Permission", MyXmlNamespaceManager);
                        ResponseMessage = ResponseMessage + "Permission = " + XmlNode.InnerText + Environment.NewLine;
                    }

                    ResponseMessage = ResponseMessage + Environment.NewLine;
                }

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Success";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }
            else {
                String ResponseMessage;
                ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                DialogOutput MyDialogOutput = new DialogOutput();
                MyDialogOutput.Text = "Error";
                MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                MyDialogOutput.ShowDialog(this);
            }

            (sender as Button).Enabled = true;
        }

        private void ButtonVerGenerateURL_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            SprightlySoftAWS.S3.Helper MyS3Helper = new SprightlySoftAWS.S3.Helper();

            String QueryString;
            QueryString = "?versionId=" + TextBoxVerVersionID.Text;

            String ExpiresURL;
            ExpiresURL = MyS3Helper.BuildExpiresURL(false, "s3.amazonaws.com", TextBoxVerBucketName.Text, TextBoxVerKeyName.Text, QueryString, DateTime.Now.AddDays(1), TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);

            DialogOutput MyDialogOutput = new DialogOutput();
            MyDialogOutput.Text = "Success";
            MyDialogOutput.TextBoxOutput.Text = ExpiresURL;
            MyDialogOutput.ShowDialog(this);

            (sender as Button).Enabled = true;
        }

        private void ButtonVerCopyVersion_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;

            //PUT Object (Copy): http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectCOPY.html

            String NewKeyName;
            NewKeyName = TextBoxVerKeyName.Text;
            NewKeyName = Microsoft.VisualBasic.Interaction.InputBox("Enter the new key name.", "Copy Version", NewKeyName, this.Location.X + 100, this.Location.Y + 150);

            if (NewKeyName != "") {
                String QueryString;
                QueryString = "?versionId=" + TextBoxVerVersionID.Text;

                Dictionary<String, String> ExtraRequestHeaders = new Dictionary<String, String>();
                ExtraRequestHeaders.Add("x-amz-copy-source", "/" + TextBoxVerBucketName.Text + "/" + TextBoxVerKeyName.Text + QueryString);
                ExtraRequestHeaders.Add("x-amz-metadata-directive", "COPY");  //use this request header to copy the metadata and extra headers

                //uncomment the following line if you would like to set the copied file as reduced redundancy
                //ExtraRequestHeaders.Add("x-amz-storage-class", "REDUCED_REDUNDANCY");

                SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

                String RequestURL;
                RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", TextBoxVerBucketName.Text, NewKeyName, "");

                String RequestMethod = "PUT";

                ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                String AuthorizationValue;
                AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, TextBoxAWSAccessKeyId.Text, TextBoxAWSSecretAccessKey.Text);
                ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

                Boolean RetBool;
                RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

                System.Diagnostics.Debug.Print("");
                System.Diagnostics.Debug.Print(MyREST.LogData);
                System.Diagnostics.Debug.Print("");

                System.Xml.XmlDocument XmlDoc;
                System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
                System.Xml.XmlNode XmlNode;
                String ResponseMessage = "";

                if (RetBool == true) {
                    XmlDoc = new System.Xml.XmlDocument();
                    XmlDoc.LoadXml(MyREST.ResponseString);

                    MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(XmlDoc.NameTable);
                    MyXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                    XmlNode = XmlDoc.SelectSingleNode("/amz:CopyObjectResult/amz:LastModified", MyXmlNamespaceManager);
                    ResponseMessage = ResponseMessage + "LastModified = " + XmlNode.InnerText + Environment.NewLine;

                    XmlNode = XmlDoc.SelectSingleNode("/amz:CopyObjectResult/amz:ETag", MyXmlNamespaceManager);
                    ResponseMessage = ResponseMessage + "ETag = " + XmlNode.InnerText + Environment.NewLine;

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Success";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
                else {
                    ResponseMessage = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);

                    DialogOutput MyDialogOutput = new DialogOutput();
                    MyDialogOutput.Text = "Error";
                    MyDialogOutput.TextBoxOutput.Text = ResponseMessage;
                    MyDialogOutput.ShowDialog(this);
                }
            }

            (sender as Button).Enabled = true;
        }

        public static void LogWrite(string oError) {
            try {
                if (!File.Exists(FormMain.FileName)) {
                    File.Create(FormMain.FileName).Close();
                }
                StreamWriter streamWriter = File.AppendText(FormMain.FileName);
                object[] type = new object[] { oError, "|", DateTime.Now };
                streamWriter.WriteLine(string.Concat(type));
                streamWriter.Close();
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }
    }
}
