﻿namespace Jaeger.UI.Forms
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.Label19 = new System.Windows.Forms.Label();
            this.TextBoxDownloadKeyName = new System.Windows.Forms.TextBox();
            this.ButtonListBucketVersions = new System.Windows.Forms.Button();
            this.ButtonGetBucketVersioning = new System.Windows.Forms.Button();
            this.TabPage2 = new System.Windows.Forms.TabPage();
            this.ButtonSetBucketNotification = new System.Windows.Forms.Button();
            this.ButtonGetBucketNotification = new System.Windows.Forms.Button();
            this.ButtonDeleteBucketPolicies = new System.Windows.Forms.Button();
            this.ButtonSetBucketPolicies = new System.Windows.Forms.Button();
            this.ButtonGetBucketPolicies = new System.Windows.Forms.Button();
            this.ButtonSetBucketVersioning = new System.Windows.Forms.Button();
            this.ButtonSetBucketLogging = new System.Windows.Forms.Button();
            this.ButtonListBucketAll = new System.Windows.Forms.Button();
            this.Label15 = new System.Windows.Forms.Label();
            this.TextBoxBucketBucketName = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.ButtonBucketExists = new System.Windows.Forms.Button();
            this.ButtonSetBucketACLs = new System.Windows.Forms.Button();
            this.ButtonCreateBucket = new System.Windows.Forms.Button();
            this.ButtonGetBucketLogging = new System.Windows.Forms.Button();
            this.ButtonGetBucketACLs = new System.Windows.Forms.Button();
            this.ButtonListBucketPages = new System.Windows.Forms.Button();
            this.ButtonDeleteBucket = new System.Windows.Forms.Button();
            this.ButtonSetBucketRequestPayment = new System.Windows.Forms.Button();
            this.ButtonGetBucketRequestPayment = new System.Windows.Forms.Button();
            this.ButtonGetBucketLocation = new System.Windows.Forms.Button();
            this.TextBoxDownloadFileName = new System.Windows.Forms.TextBox();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.TextBoxAWSAccessKeyId = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.TextBoxAWSSecretAccessKey = new System.Windows.Forms.TextBox();
            this.TextBoxDownloadBucketName = new System.Windows.Forms.TextBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label14 = new System.Windows.Forms.Label();
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.TabPage1 = new System.Windows.Forms.TabPage();
            this.ButtonListBuckets = new System.Windows.Forms.Button();
            this.TabPage3 = new System.Windows.Forms.TabPage();
            this.Label16 = new System.Windows.Forms.Label();
            this.ButtonGenerateURL = new System.Windows.Forms.Button();
            this.Label5 = new System.Windows.Forms.Label();
            this.ButtonSetObjectMetadata = new System.Windows.Forms.Button();
            this.ButtonGetObjectACLs = new System.Windows.Forms.Button();
            this.TextBoxObjectKeyName = new System.Windows.Forms.TextBox();
            this.ButtonDeleteObject = new System.Windows.Forms.Button();
            this.Label6 = new System.Windows.Forms.Label();
            this.ButtonSetObjectACLs = new System.Windows.Forms.Button();
            this.TextBoxObjectBucketName = new System.Windows.Forms.TextBox();
            this.ButtonGetObjectProperties = new System.Windows.Forms.Button();
            this.ButtonCopyObject = new System.Windows.Forms.Button();
            this.ButtonObjectExists = new System.Windows.Forms.Button();
            this.TabPage5 = new System.Windows.Forms.TabPage();
            this.ComboBoxStorageClass = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label22 = new System.Windows.Forms.Label();
            this.Label21 = new System.Windows.Forms.Label();
            this.CheckBoxUploadMakePublic = new System.Windows.Forms.CheckBox();
            this.TextBoxUploadContentType = new System.Windows.Forms.TextBox();
            this.Label20 = new System.Windows.Forms.Label();
            this.Label18 = new System.Windows.Forms.Label();
            this.ButtonUploadBrowse = new System.Windows.Forms.Button();
            this.TextBoxUploadKeyName = new System.Windows.Forms.TextBox();
            this.TextBoxUploadBucketName = new System.Windows.Forms.TextBox();
            this.TextBoxUploadFileName = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.ButtonUploadFile = new System.Windows.Forms.Button();
            this.TabPage6 = new System.Windows.Forms.TabPage();
            this.Label11 = new System.Windows.Forms.Label();
            this.ButtonDownloadFile = new System.Windows.Forms.Button();
            this.TabPage4 = new System.Windows.Forms.TabPage();
            this.Label17 = new System.Windows.Forms.Label();
            this.ButtonCalculateMD5 = new System.Windows.Forms.Button();
            this.ButtonHashBrowse = new System.Windows.Forms.Button();
            this.TextBoxHashFileName = new System.Windows.Forms.TextBox();
            this.Label13 = new System.Windows.Forms.Label();
            this.ButtonCalculateETag = new System.Windows.Forms.Button();
            this.TabPage7 = new System.Windows.Forms.TabPage();
            this.ButtonVerCopyVersion = new System.Windows.Forms.Button();
            this.ButtonVerGenerateURL = new System.Windows.Forms.Button();
            this.ButtonVerGetACLs = new System.Windows.Forms.Button();
            this.ButtonVerDeleteVersion = new System.Windows.Forms.Button();
            this.ButtonVerGetProperties = new System.Windows.Forms.Button();
            this.Label26 = new System.Windows.Forms.Label();
            this.TextBoxVerVersionID = new System.Windows.Forms.TextBox();
            this.Label23 = new System.Windows.Forms.Label();
            this.Label24 = new System.Windows.Forms.Label();
            this.TextBoxVerKeyName = new System.Windows.Forms.TextBox();
            this.Label25 = new System.Windows.Forms.Label();
            this.TextBoxVerBucketName = new System.Windows.Forms.TextBox();
            this.TabPage2.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.TabControl1.SuspendLayout();
            this.TabPage1.SuspendLayout();
            this.TabPage3.SuspendLayout();
            this.TabPage5.SuspendLayout();
            this.TabPage6.SuspendLayout();
            this.TabPage4.SuspendLayout();
            this.TabPage7.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Location = new System.Drawing.Point(14, 14);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(200, 13);
            this.Label19.TabIndex = 42;
            this.Label19.Text = "Use the following form to download a file.";
            // 
            // TextBoxDownloadKeyName
            // 
            this.TextBoxDownloadKeyName.Location = new System.Drawing.Point(104, 70);
            this.TextBoxDownloadKeyName.Name = "TextBoxDownloadKeyName";
            this.TextBoxDownloadKeyName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxDownloadKeyName.TabIndex = 2;
            // 
            // ButtonListBucketVersions
            // 
            this.ButtonListBucketVersions.Location = new System.Drawing.Point(350, 139);
            this.ButtonListBucketVersions.Name = "ButtonListBucketVersions";
            this.ButtonListBucketVersions.Size = new System.Drawing.Size(96, 23);
            this.ButtonListBucketVersions.TabIndex = 49;
            this.ButtonListBucketVersions.Text = "List Versions";
            this.ButtonListBucketVersions.UseVisualStyleBackColor = true;
            this.ButtonListBucketVersions.Click += new System.EventHandler(this.ButtonListBucketVersions_Click);
            // 
            // ButtonGetBucketVersioning
            // 
            this.ButtonGetBucketVersioning.Location = new System.Drawing.Point(350, 81);
            this.ButtonGetBucketVersioning.Name = "ButtonGetBucketVersioning";
            this.ButtonGetBucketVersioning.Size = new System.Drawing.Size(96, 23);
            this.ButtonGetBucketVersioning.TabIndex = 48;
            this.ButtonGetBucketVersioning.Text = "Get Versioning";
            this.ButtonGetBucketVersioning.UseVisualStyleBackColor = true;
            this.ButtonGetBucketVersioning.Click += new System.EventHandler(this.ButtonGetBucketVersioning_Click);
            // 
            // TabPage2
            // 
            this.TabPage2.Controls.Add(this.ButtonSetBucketNotification);
            this.TabPage2.Controls.Add(this.ButtonGetBucketNotification);
            this.TabPage2.Controls.Add(this.ButtonDeleteBucketPolicies);
            this.TabPage2.Controls.Add(this.ButtonSetBucketPolicies);
            this.TabPage2.Controls.Add(this.ButtonGetBucketPolicies);
            this.TabPage2.Controls.Add(this.ButtonListBucketVersions);
            this.TabPage2.Controls.Add(this.ButtonGetBucketVersioning);
            this.TabPage2.Controls.Add(this.ButtonSetBucketVersioning);
            this.TabPage2.Controls.Add(this.ButtonSetBucketLogging);
            this.TabPage2.Controls.Add(this.ButtonListBucketAll);
            this.TabPage2.Controls.Add(this.Label15);
            this.TabPage2.Controls.Add(this.TextBoxBucketBucketName);
            this.TabPage2.Controls.Add(this.Label4);
            this.TabPage2.Controls.Add(this.ButtonBucketExists);
            this.TabPage2.Controls.Add(this.ButtonSetBucketACLs);
            this.TabPage2.Controls.Add(this.ButtonCreateBucket);
            this.TabPage2.Controls.Add(this.ButtonGetBucketLogging);
            this.TabPage2.Controls.Add(this.ButtonGetBucketACLs);
            this.TabPage2.Controls.Add(this.ButtonListBucketPages);
            this.TabPage2.Controls.Add(this.ButtonDeleteBucket);
            this.TabPage2.Controls.Add(this.ButtonSetBucketRequestPayment);
            this.TabPage2.Controls.Add(this.ButtonGetBucketRequestPayment);
            this.TabPage2.Controls.Add(this.ButtonGetBucketLocation);
            this.TabPage2.Location = new System.Drawing.Point(4, 22);
            this.TabPage2.Name = "TabPage2";
            this.TabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage2.Size = new System.Drawing.Size(575, 247);
            this.TabPage2.TabIndex = 1;
            this.TabPage2.Text = "Bucket Operations";
            this.TabPage2.UseVisualStyleBackColor = true;
            // 
            // ButtonSetBucketNotification
            // 
            this.ButtonSetBucketNotification.Location = new System.Drawing.Point(462, 168);
            this.ButtonSetBucketNotification.Name = "ButtonSetBucketNotification";
            this.ButtonSetBucketNotification.Size = new System.Drawing.Size(96, 23);
            this.ButtonSetBucketNotification.TabIndex = 59;
            this.ButtonSetBucketNotification.Text = "Set Notification";
            this.ButtonSetBucketNotification.UseVisualStyleBackColor = true;
            this.ButtonSetBucketNotification.Click += new System.EventHandler(this.ButtonSetBucketNotification_Click);
            // 
            // ButtonGetBucketNotification
            // 
            this.ButtonGetBucketNotification.Location = new System.Drawing.Point(462, 139);
            this.ButtonGetBucketNotification.Name = "ButtonGetBucketNotification";
            this.ButtonGetBucketNotification.Size = new System.Drawing.Size(96, 23);
            this.ButtonGetBucketNotification.TabIndex = 58;
            this.ButtonGetBucketNotification.Text = "Get Notification";
            this.ButtonGetBucketNotification.UseVisualStyleBackColor = true;
            this.ButtonGetBucketNotification.Click += new System.EventHandler(this.ButtonGetBucketNotification_Click);
            // 
            // ButtonDeleteBucketPolicies
            // 
            this.ButtonDeleteBucketPolicies.Location = new System.Drawing.Point(462, 110);
            this.ButtonDeleteBucketPolicies.Name = "ButtonDeleteBucketPolicies";
            this.ButtonDeleteBucketPolicies.Size = new System.Drawing.Size(96, 23);
            this.ButtonDeleteBucketPolicies.TabIndex = 57;
            this.ButtonDeleteBucketPolicies.Text = "Delete Policies";
            this.ButtonDeleteBucketPolicies.UseVisualStyleBackColor = true;
            this.ButtonDeleteBucketPolicies.Click += new System.EventHandler(this.ButtonDeleteBucketPolicies_Click);
            // 
            // ButtonSetBucketPolicies
            // 
            this.ButtonSetBucketPolicies.Location = new System.Drawing.Point(461, 81);
            this.ButtonSetBucketPolicies.Name = "ButtonSetBucketPolicies";
            this.ButtonSetBucketPolicies.Size = new System.Drawing.Size(96, 23);
            this.ButtonSetBucketPolicies.TabIndex = 56;
            this.ButtonSetBucketPolicies.Text = "Set Policies";
            this.ButtonSetBucketPolicies.UseVisualStyleBackColor = true;
            this.ButtonSetBucketPolicies.Click += new System.EventHandler(this.ButtonSetBucketPolicies_Click);
            // 
            // ButtonGetBucketPolicies
            // 
            this.ButtonGetBucketPolicies.Location = new System.Drawing.Point(349, 168);
            this.ButtonGetBucketPolicies.Name = "ButtonGetBucketPolicies";
            this.ButtonGetBucketPolicies.Size = new System.Drawing.Size(96, 23);
            this.ButtonGetBucketPolicies.TabIndex = 55;
            this.ButtonGetBucketPolicies.Text = "Get Policies";
            this.ButtonGetBucketPolicies.UseVisualStyleBackColor = true;
            this.ButtonGetBucketPolicies.Click += new System.EventHandler(this.ButtonGetBucketPolicies_Click);
            // 
            // ButtonSetBucketVersioning
            // 
            this.ButtonSetBucketVersioning.Location = new System.Drawing.Point(350, 110);
            this.ButtonSetBucketVersioning.Name = "ButtonSetBucketVersioning";
            this.ButtonSetBucketVersioning.Size = new System.Drawing.Size(96, 23);
            this.ButtonSetBucketVersioning.TabIndex = 47;
            this.ButtonSetBucketVersioning.Text = "Set Versioning";
            this.ButtonSetBucketVersioning.UseVisualStyleBackColor = true;
            this.ButtonSetBucketVersioning.Click += new System.EventHandler(this.ButtonSetBucketVersioning_Click);
            // 
            // ButtonSetBucketLogging
            // 
            this.ButtonSetBucketLogging.Location = new System.Drawing.Point(128, 168);
            this.ButtonSetBucketLogging.Name = "ButtonSetBucketLogging";
            this.ButtonSetBucketLogging.Size = new System.Drawing.Size(96, 23);
            this.ButtonSetBucketLogging.TabIndex = 9;
            this.ButtonSetBucketLogging.Text = "Set Logging";
            this.ButtonSetBucketLogging.UseVisualStyleBackColor = true;
            this.ButtonSetBucketLogging.Click += new System.EventHandler(this.ButtonSetBucketLogging_Click);
            // 
            // ButtonListBucketAll
            // 
            this.ButtonListBucketAll.Location = new System.Drawing.Point(17, 81);
            this.ButtonListBucketAll.Name = "ButtonListBucketAll";
            this.ButtonListBucketAll.Size = new System.Drawing.Size(96, 23);
            this.ButtonListBucketAll.TabIndex = 3;
            this.ButtonListBucketAll.Text = "List All";
            this.ButtonListBucketAll.UseVisualStyleBackColor = true;
            this.ButtonListBucketAll.Click += new System.EventHandler(this.ButtonListBucketAll_Click);
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Location = new System.Drawing.Point(14, 14);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(287, 13);
            this.Label15.TabIndex = 34;
            this.Label15.Text = "Use the following buttons to preform bucket related actions.";
            // 
            // TextBoxBucketBucketName
            // 
            this.TextBoxBucketBucketName.Location = new System.Drawing.Point(92, 44);
            this.TextBoxBucketBucketName.Name = "TextBoxBucketBucketName";
            this.TextBoxBucketBucketName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxBucketBucketName.TabIndex = 1;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(14, 47);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(72, 13);
            this.Label4.TabIndex = 32;
            this.Label4.Text = "Bucket Name";
            // 
            // ButtonBucketExists
            // 
            this.ButtonBucketExists.Location = new System.Drawing.Point(128, 81);
            this.ButtonBucketExists.Name = "ButtonBucketExists";
            this.ButtonBucketExists.Size = new System.Drawing.Size(96, 23);
            this.ButtonBucketExists.TabIndex = 6;
            this.ButtonBucketExists.Text = "Bucket Exists";
            this.ButtonBucketExists.UseVisualStyleBackColor = true;
            this.ButtonBucketExists.Click += new System.EventHandler(this.ButtonBucketExists_Click);
            // 
            // ButtonSetBucketACLs
            // 
            this.ButtonSetBucketACLs.Location = new System.Drawing.Point(239, 110);
            this.ButtonSetBucketACLs.Name = "ButtonSetBucketACLs";
            this.ButtonSetBucketACLs.Size = new System.Drawing.Size(96, 23);
            this.ButtonSetBucketACLs.TabIndex = 11;
            this.ButtonSetBucketACLs.Text = "Set ACLs";
            this.ButtonSetBucketACLs.UseVisualStyleBackColor = true;
            this.ButtonSetBucketACLs.Click += new System.EventHandler(this.ButtonSetBucketACLs_Click);
            // 
            // ButtonCreateBucket
            // 
            this.ButtonCreateBucket.Location = new System.Drawing.Point(17, 139);
            this.ButtonCreateBucket.Name = "ButtonCreateBucket";
            this.ButtonCreateBucket.Size = new System.Drawing.Size(96, 23);
            this.ButtonCreateBucket.TabIndex = 4;
            this.ButtonCreateBucket.Text = "Create Bucket";
            this.ButtonCreateBucket.UseVisualStyleBackColor = true;
            this.ButtonCreateBucket.Click += new System.EventHandler(this.ButtonCreateBucket_Click);
            // 
            // ButtonGetBucketLogging
            // 
            this.ButtonGetBucketLogging.Location = new System.Drawing.Point(128, 139);
            this.ButtonGetBucketLogging.Name = "ButtonGetBucketLogging";
            this.ButtonGetBucketLogging.Size = new System.Drawing.Size(96, 23);
            this.ButtonGetBucketLogging.TabIndex = 8;
            this.ButtonGetBucketLogging.Text = "Get Logging";
            this.ButtonGetBucketLogging.UseVisualStyleBackColor = true;
            this.ButtonGetBucketLogging.Click += new System.EventHandler(this.ButtonGetBucketLogging_Click);
            // 
            // ButtonGetBucketACLs
            // 
            this.ButtonGetBucketACLs.Location = new System.Drawing.Point(239, 81);
            this.ButtonGetBucketACLs.Name = "ButtonGetBucketACLs";
            this.ButtonGetBucketACLs.Size = new System.Drawing.Size(96, 23);
            this.ButtonGetBucketACLs.TabIndex = 10;
            this.ButtonGetBucketACLs.Text = "Get ACLs";
            this.ButtonGetBucketACLs.UseVisualStyleBackColor = true;
            this.ButtonGetBucketACLs.Click += new System.EventHandler(this.ButtonGetBucketACLs_Click);
            // 
            // ButtonListBucketPages
            // 
            this.ButtonListBucketPages.Location = new System.Drawing.Point(17, 110);
            this.ButtonListBucketPages.Name = "ButtonListBucketPages";
            this.ButtonListBucketPages.Size = new System.Drawing.Size(96, 23);
            this.ButtonListBucketPages.TabIndex = 2;
            this.ButtonListBucketPages.Text = "List By Pages";
            this.ButtonListBucketPages.UseVisualStyleBackColor = true;
            this.ButtonListBucketPages.Click += new System.EventHandler(this.ButtonListBucketPages_Click);
            // 
            // ButtonDeleteBucket
            // 
            this.ButtonDeleteBucket.Location = new System.Drawing.Point(17, 168);
            this.ButtonDeleteBucket.Name = "ButtonDeleteBucket";
            this.ButtonDeleteBucket.Size = new System.Drawing.Size(96, 23);
            this.ButtonDeleteBucket.TabIndex = 5;
            this.ButtonDeleteBucket.Text = "Delete Bucket";
            this.ButtonDeleteBucket.UseVisualStyleBackColor = true;
            this.ButtonDeleteBucket.Click += new System.EventHandler(this.ButtonDeleteBucket_Click);
            // 
            // ButtonSetBucketRequestPayment
            // 
            this.ButtonSetBucketRequestPayment.Location = new System.Drawing.Point(239, 168);
            this.ButtonSetBucketRequestPayment.Name = "ButtonSetBucketRequestPayment";
            this.ButtonSetBucketRequestPayment.Size = new System.Drawing.Size(96, 23);
            this.ButtonSetBucketRequestPayment.TabIndex = 13;
            this.ButtonSetBucketRequestPayment.Text = "Set Payment";
            this.ButtonSetBucketRequestPayment.UseVisualStyleBackColor = true;
            this.ButtonSetBucketRequestPayment.Click += new System.EventHandler(this.ButtonSetBucketRequestPayment_Click);
            // 
            // ButtonGetBucketRequestPayment
            // 
            this.ButtonGetBucketRequestPayment.Location = new System.Drawing.Point(239, 139);
            this.ButtonGetBucketRequestPayment.Name = "ButtonGetBucketRequestPayment";
            this.ButtonGetBucketRequestPayment.Size = new System.Drawing.Size(96, 23);
            this.ButtonGetBucketRequestPayment.TabIndex = 12;
            this.ButtonGetBucketRequestPayment.Text = "Get Payment";
            this.ButtonGetBucketRequestPayment.UseVisualStyleBackColor = true;
            this.ButtonGetBucketRequestPayment.Click += new System.EventHandler(this.ButtonGetBucketRequestPayment_Click);
            // 
            // ButtonGetBucketLocation
            // 
            this.ButtonGetBucketLocation.Location = new System.Drawing.Point(128, 110);
            this.ButtonGetBucketLocation.Name = "ButtonGetBucketLocation";
            this.ButtonGetBucketLocation.Size = new System.Drawing.Size(96, 23);
            this.ButtonGetBucketLocation.TabIndex = 7;
            this.ButtonGetBucketLocation.Text = "Get Location";
            this.ButtonGetBucketLocation.UseVisualStyleBackColor = true;
            this.ButtonGetBucketLocation.Click += new System.EventHandler(this.ButtonGetBucketLocation_Click);
            // 
            // TextBoxDownloadFileName
            // 
            this.TextBoxDownloadFileName.Location = new System.Drawing.Point(104, 96);
            this.TextBoxDownloadFileName.Name = "TextBoxDownloadFileName";
            this.TextBoxDownloadFileName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxDownloadFileName.TabIndex = 3;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.TextBoxAWSAccessKeyId);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.TextBoxAWSSecretAccessKey);
            this.GroupBox1.Location = new System.Drawing.Point(7, 12);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(578, 87);
            this.GroupBox1.TabIndex = 25;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Global Settings (Enter your values below)";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(5, 51);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(157, 13);
            this.Label3.TabIndex = 22;
            this.Label3.Text = "Amazon AWSSecretAccessKey";
            // 
            // TextBoxAWSAccessKeyId
            // 
            this.TextBoxAWSAccessKeyId.Location = new System.Drawing.Point(169, 22);
            this.TextBoxAWSAccessKeyId.Name = "TextBoxAWSAccessKeyId";
            this.TextBoxAWSAccessKeyId.Size = new System.Drawing.Size(287, 20);
            this.TextBoxAWSAccessKeyId.TabIndex = 16;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(5, 25);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(135, 13);
            this.Label2.TabIndex = 21;
            this.Label2.Text = "Amazon AWSAccessKeyId";
            // 
            // TextBoxAWSSecretAccessKey
            // 
            this.TextBoxAWSSecretAccessKey.Location = new System.Drawing.Point(169, 48);
            this.TextBoxAWSSecretAccessKey.Name = "TextBoxAWSSecretAccessKey";
            this.TextBoxAWSSecretAccessKey.Size = new System.Drawing.Size(287, 20);
            this.TextBoxAWSSecretAccessKey.TabIndex = 18;
            // 
            // TextBoxDownloadBucketName
            // 
            this.TextBoxDownloadBucketName.Location = new System.Drawing.Point(104, 44);
            this.TextBoxDownloadBucketName.Name = "TextBoxDownloadBucketName";
            this.TextBoxDownloadBucketName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxDownloadBucketName.TabIndex = 1;
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(15, 99);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(83, 13);
            this.Label12.TabIndex = 38;
            this.Label12.Text = "Local File Name";
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(14, 47);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(72, 13);
            this.Label10.TabIndex = 36;
            this.Label10.Text = "Bucket Name";
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(14, 14);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(309, 13);
            this.Label14.TabIndex = 6;
            this.Label14.Text = "Use the button below to list all buckets in your Amazon account.";
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.TabPage1);
            this.TabControl1.Controls.Add(this.TabPage2);
            this.TabControl1.Controls.Add(this.TabPage3);
            this.TabControl1.Controls.Add(this.TabPage5);
            this.TabControl1.Controls.Add(this.TabPage6);
            this.TabControl1.Controls.Add(this.TabPage4);
            this.TabControl1.Controls.Add(this.TabPage7);
            this.TabControl1.Location = new System.Drawing.Point(7, 105);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(583, 273);
            this.TabControl1.TabIndex = 24;
            // 
            // TabPage1
            // 
            this.TabPage1.Controls.Add(this.Label14);
            this.TabPage1.Controls.Add(this.ButtonListBuckets);
            this.TabPage1.Location = new System.Drawing.Point(4, 22);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage1.Size = new System.Drawing.Size(575, 247);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.Text = "Service Operations";
            this.TabPage1.UseVisualStyleBackColor = true;
            // 
            // ButtonListBuckets
            // 
            this.ButtonListBuckets.Location = new System.Drawing.Point(19, 45);
            this.ButtonListBuckets.Name = "ButtonListBuckets";
            this.ButtonListBuckets.Size = new System.Drawing.Size(96, 23);
            this.ButtonListBuckets.TabIndex = 1;
            this.ButtonListBuckets.Text = "List Buckets";
            this.ButtonListBuckets.UseVisualStyleBackColor = true;
            this.ButtonListBuckets.Click += new System.EventHandler(this.ButtonListBuckets_Click);
            // 
            // TabPage3
            // 
            this.TabPage3.Controls.Add(this.Label16);
            this.TabPage3.Controls.Add(this.ButtonGenerateURL);
            this.TabPage3.Controls.Add(this.Label5);
            this.TabPage3.Controls.Add(this.ButtonSetObjectMetadata);
            this.TabPage3.Controls.Add(this.ButtonGetObjectACLs);
            this.TabPage3.Controls.Add(this.TextBoxObjectKeyName);
            this.TabPage3.Controls.Add(this.ButtonDeleteObject);
            this.TabPage3.Controls.Add(this.Label6);
            this.TabPage3.Controls.Add(this.ButtonSetObjectACLs);
            this.TabPage3.Controls.Add(this.TextBoxObjectBucketName);
            this.TabPage3.Controls.Add(this.ButtonGetObjectProperties);
            this.TabPage3.Controls.Add(this.ButtonCopyObject);
            this.TabPage3.Controls.Add(this.ButtonObjectExists);
            this.TabPage3.Location = new System.Drawing.Point(4, 22);
            this.TabPage3.Name = "TabPage3";
            this.TabPage3.Size = new System.Drawing.Size(575, 247);
            this.TabPage3.TabIndex = 2;
            this.TabPage3.Text = "Object Operations";
            this.TabPage3.UseVisualStyleBackColor = true;
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Location = new System.Drawing.Point(14, 14);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(283, 13);
            this.Label16.TabIndex = 39;
            this.Label16.Text = "Use the following buttons to preform object related actions.";
            // 
            // ButtonGenerateURL
            // 
            this.ButtonGenerateURL.Location = new System.Drawing.Point(243, 136);
            this.ButtonGenerateURL.Name = "ButtonGenerateURL";
            this.ButtonGenerateURL.Size = new System.Drawing.Size(96, 23);
            this.ButtonGenerateURL.TabIndex = 10;
            this.ButtonGenerateURL.Text = "Generate URL";
            this.ButtonGenerateURL.UseVisualStyleBackColor = true;
            this.ButtonGenerateURL.Click += new System.EventHandler(this.ButtonGenerateURL_Click);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(14, 47);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(72, 13);
            this.Label5.TabIndex = 0;
            this.Label5.Text = "Bucket Name";
            // 
            // ButtonSetObjectMetadata
            // 
            this.ButtonSetObjectMetadata.Location = new System.Drawing.Point(243, 107);
            this.ButtonSetObjectMetadata.Name = "ButtonSetObjectMetadata";
            this.ButtonSetObjectMetadata.Size = new System.Drawing.Size(96, 23);
            this.ButtonSetObjectMetadata.TabIndex = 9;
            this.ButtonSetObjectMetadata.Text = "Set Metadata";
            this.ButtonSetObjectMetadata.UseVisualStyleBackColor = true;
            this.ButtonSetObjectMetadata.Click += new System.EventHandler(this.ButtonSetObjectMetadata_Click);
            // 
            // ButtonGetObjectACLs
            // 
            this.ButtonGetObjectACLs.Location = new System.Drawing.Point(130, 107);
            this.ButtonGetObjectACLs.Name = "ButtonGetObjectACLs";
            this.ButtonGetObjectACLs.Size = new System.Drawing.Size(96, 23);
            this.ButtonGetObjectACLs.TabIndex = 6;
            this.ButtonGetObjectACLs.Text = "Get ACLs";
            this.ButtonGetObjectACLs.UseVisualStyleBackColor = true;
            this.ButtonGetObjectACLs.Click += new System.EventHandler(this.ButtonGetObjectACLs_Click);
            // 
            // TextBoxObjectKeyName
            // 
            this.TextBoxObjectKeyName.Location = new System.Drawing.Point(92, 70);
            this.TextBoxObjectKeyName.Name = "TextBoxObjectKeyName";
            this.TextBoxObjectKeyName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxObjectKeyName.TabIndex = 2;
            // 
            // ButtonDeleteObject
            // 
            this.ButtonDeleteObject.Location = new System.Drawing.Point(17, 136);
            this.ButtonDeleteObject.Name = "ButtonDeleteObject";
            this.ButtonDeleteObject.Size = new System.Drawing.Size(96, 23);
            this.ButtonDeleteObject.TabIndex = 4;
            this.ButtonDeleteObject.Text = "Delete Object";
            this.ButtonDeleteObject.UseVisualStyleBackColor = true;
            this.ButtonDeleteObject.Click += new System.EventHandler(this.ButtonDeleteObject_Click);
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(16, 73);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(56, 13);
            this.Label6.TabIndex = 2;
            this.Label6.Text = "Key Name";
            // 
            // ButtonSetObjectACLs
            // 
            this.ButtonSetObjectACLs.Location = new System.Drawing.Point(130, 136);
            this.ButtonSetObjectACLs.Name = "ButtonSetObjectACLs";
            this.ButtonSetObjectACLs.Size = new System.Drawing.Size(96, 23);
            this.ButtonSetObjectACLs.TabIndex = 7;
            this.ButtonSetObjectACLs.Text = "Set ACLs";
            this.ButtonSetObjectACLs.UseVisualStyleBackColor = true;
            this.ButtonSetObjectACLs.Click += new System.EventHandler(this.ButtonSetObjectACLs_Click);
            // 
            // TextBoxObjectBucketName
            // 
            this.TextBoxObjectBucketName.Location = new System.Drawing.Point(92, 44);
            this.TextBoxObjectBucketName.Name = "TextBoxObjectBucketName";
            this.TextBoxObjectBucketName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxObjectBucketName.TabIndex = 1;
            // 
            // ButtonGetObjectProperties
            // 
            this.ButtonGetObjectProperties.Location = new System.Drawing.Point(17, 107);
            this.ButtonGetObjectProperties.Name = "ButtonGetObjectProperties";
            this.ButtonGetObjectProperties.Size = new System.Drawing.Size(96, 23);
            this.ButtonGetObjectProperties.TabIndex = 3;
            this.ButtonGetObjectProperties.Text = "Get Properties";
            this.ButtonGetObjectProperties.UseVisualStyleBackColor = true;
            this.ButtonGetObjectProperties.Click += new System.EventHandler(this.ButtonGetObjectProperties_Click);
            // 
            // ButtonCopyObject
            // 
            this.ButtonCopyObject.Location = new System.Drawing.Point(130, 165);
            this.ButtonCopyObject.Name = "ButtonCopyObject";
            this.ButtonCopyObject.Size = new System.Drawing.Size(96, 23);
            this.ButtonCopyObject.TabIndex = 8;
            this.ButtonCopyObject.Text = "Copy Object";
            this.ButtonCopyObject.UseVisualStyleBackColor = true;
            this.ButtonCopyObject.Click += new System.EventHandler(this.ButtonCopyObject_Click);
            // 
            // ButtonObjectExists
            // 
            this.ButtonObjectExists.Location = new System.Drawing.Point(17, 165);
            this.ButtonObjectExists.Name = "ButtonObjectExists";
            this.ButtonObjectExists.Size = new System.Drawing.Size(96, 23);
            this.ButtonObjectExists.TabIndex = 5;
            this.ButtonObjectExists.Text = "Object Exists";
            this.ButtonObjectExists.UseVisualStyleBackColor = true;
            this.ButtonObjectExists.Click += new System.EventHandler(this.ButtonObjectExists_Click);
            // 
            // TabPage5
            // 
            this.TabPage5.Controls.Add(this.ComboBoxStorageClass);
            this.TabPage5.Controls.Add(this.Label1);
            this.TabPage5.Controls.Add(this.Label22);
            this.TabPage5.Controls.Add(this.Label21);
            this.TabPage5.Controls.Add(this.CheckBoxUploadMakePublic);
            this.TabPage5.Controls.Add(this.TextBoxUploadContentType);
            this.TabPage5.Controls.Add(this.Label20);
            this.TabPage5.Controls.Add(this.Label18);
            this.TabPage5.Controls.Add(this.ButtonUploadBrowse);
            this.TabPage5.Controls.Add(this.TextBoxUploadKeyName);
            this.TabPage5.Controls.Add(this.TextBoxUploadBucketName);
            this.TabPage5.Controls.Add(this.TextBoxUploadFileName);
            this.TabPage5.Controls.Add(this.Label9);
            this.TabPage5.Controls.Add(this.Label8);
            this.TabPage5.Controls.Add(this.Label7);
            this.TabPage5.Controls.Add(this.ButtonUploadFile);
            this.TabPage5.Location = new System.Drawing.Point(4, 22);
            this.TabPage5.Name = "TabPage5";
            this.TabPage5.Size = new System.Drawing.Size(575, 247);
            this.TabPage5.TabIndex = 4;
            this.TabPage5.Text = "Upload";
            this.TabPage5.UseVisualStyleBackColor = true;
            // 
            // ComboBoxStorageClass
            // 
            this.ComboBoxStorageClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxStorageClass.FormattingEnabled = true;
            this.ComboBoxStorageClass.Items.AddRange(new object[] {
            "STANDARD",
            "REDUCED_REDUNDANCY"});
            this.ComboBoxStorageClass.Location = new System.Drawing.Point(103, 176);
            this.ComboBoxStorageClass.Name = "ComboBoxStorageClass";
            this.ComboBoxStorageClass.Size = new System.Drawing.Size(163, 21);
            this.ComboBoxStorageClass.TabIndex = 52;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(14, 179);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(75, 13);
            this.Label1.TabIndex = 51;
            this.Label1.Text = "Storage Class ";
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Location = new System.Drawing.Point(359, 125);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(52, 13);
            this.Label22.TabIndex = 47;
            this.Label22.Text = "(Optional)";
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Location = new System.Drawing.Point(14, 152);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(66, 13);
            this.Label21.TabIndex = 46;
            this.Label21.Text = "Make Public";
            // 
            // CheckBoxUploadMakePublic
            // 
            this.CheckBoxUploadMakePublic.AutoSize = true;
            this.CheckBoxUploadMakePublic.Location = new System.Drawing.Point(103, 153);
            this.CheckBoxUploadMakePublic.Name = "CheckBoxUploadMakePublic";
            this.CheckBoxUploadMakePublic.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxUploadMakePublic.TabIndex = 45;
            this.CheckBoxUploadMakePublic.UseVisualStyleBackColor = true;
            // 
            // TextBoxUploadContentType
            // 
            this.TextBoxUploadContentType.Location = new System.Drawing.Point(103, 122);
            this.TextBoxUploadContentType.Name = "TextBoxUploadContentType";
            this.TextBoxUploadContentType.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadContentType.TabIndex = 44;
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Location = new System.Drawing.Point(14, 125);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(71, 13);
            this.Label20.TabIndex = 43;
            this.Label20.Text = "Content-Type";
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Location = new System.Drawing.Point(14, 14);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(186, 13);
            this.Label18.TabIndex = 42;
            this.Label18.Text = "Use the following form to upload a file.";
            // 
            // ButtonUploadBrowse
            // 
            this.ButtonUploadBrowse.Location = new System.Drawing.Point(359, 42);
            this.ButtonUploadBrowse.Name = "ButtonUploadBrowse";
            this.ButtonUploadBrowse.Size = new System.Drawing.Size(75, 23);
            this.ButtonUploadBrowse.TabIndex = 2;
            this.ButtonUploadBrowse.Text = "Browse";
            this.ButtonUploadBrowse.UseVisualStyleBackColor = true;
            this.ButtonUploadBrowse.Click += new System.EventHandler(this.ButtonUploadBrowse_Click);
            // 
            // TextBoxUploadKeyName
            // 
            this.TextBoxUploadKeyName.Location = new System.Drawing.Point(103, 96);
            this.TextBoxUploadKeyName.Name = "TextBoxUploadKeyName";
            this.TextBoxUploadKeyName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadKeyName.TabIndex = 4;
            // 
            // TextBoxUploadBucketName
            // 
            this.TextBoxUploadBucketName.Location = new System.Drawing.Point(103, 70);
            this.TextBoxUploadBucketName.Name = "TextBoxUploadBucketName";
            this.TextBoxUploadBucketName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadBucketName.TabIndex = 3;
            // 
            // TextBoxUploadFileName
            // 
            this.TextBoxUploadFileName.Location = new System.Drawing.Point(103, 44);
            this.TextBoxUploadFileName.Name = "TextBoxUploadFileName";
            this.TextBoxUploadFileName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadFileName.TabIndex = 1;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(14, 99);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(56, 13);
            this.Label9.TabIndex = 37;
            this.Label9.Text = "Key Name";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(14, 73);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(72, 13);
            this.Label8.TabIndex = 36;
            this.Label8.Text = "Bucket Name";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(14, 47);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(83, 13);
            this.Label7.TabIndex = 35;
            this.Label7.Text = "Local File Name";
            // 
            // ButtonUploadFile
            // 
            this.ButtonUploadFile.Location = new System.Drawing.Point(103, 210);
            this.ButtonUploadFile.Name = "ButtonUploadFile";
            this.ButtonUploadFile.Size = new System.Drawing.Size(96, 23);
            this.ButtonUploadFile.TabIndex = 46;
            this.ButtonUploadFile.Text = "Upload File";
            this.ButtonUploadFile.UseVisualStyleBackColor = true;
            this.ButtonUploadFile.Click += new System.EventHandler(this.ButtonUploadFile_Click);
            // 
            // TabPage6
            // 
            this.TabPage6.Controls.Add(this.Label19);
            this.TabPage6.Controls.Add(this.TextBoxDownloadFileName);
            this.TabPage6.Controls.Add(this.TextBoxDownloadKeyName);
            this.TabPage6.Controls.Add(this.TextBoxDownloadBucketName);
            this.TabPage6.Controls.Add(this.Label12);
            this.TabPage6.Controls.Add(this.Label11);
            this.TabPage6.Controls.Add(this.Label10);
            this.TabPage6.Controls.Add(this.ButtonDownloadFile);
            this.TabPage6.Location = new System.Drawing.Point(4, 22);
            this.TabPage6.Name = "TabPage6";
            this.TabPage6.Size = new System.Drawing.Size(575, 247);
            this.TabPage6.TabIndex = 5;
            this.TabPage6.Text = "Download";
            this.TabPage6.UseVisualStyleBackColor = true;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(14, 73);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(56, 13);
            this.Label11.TabIndex = 37;
            this.Label11.Text = "Key Name";
            // 
            // ButtonDownloadFile
            // 
            this.ButtonDownloadFile.Location = new System.Drawing.Point(104, 131);
            this.ButtonDownloadFile.Name = "ButtonDownloadFile";
            this.ButtonDownloadFile.Size = new System.Drawing.Size(97, 23);
            this.ButtonDownloadFile.TabIndex = 4;
            this.ButtonDownloadFile.Text = "Download File";
            this.ButtonDownloadFile.UseVisualStyleBackColor = true;
            this.ButtonDownloadFile.Click += new System.EventHandler(this.ButtonDownloadFile_Click);
            // 
            // TabPage4
            // 
            this.TabPage4.Controls.Add(this.Label17);
            this.TabPage4.Controls.Add(this.ButtonCalculateMD5);
            this.TabPage4.Controls.Add(this.ButtonHashBrowse);
            this.TabPage4.Controls.Add(this.TextBoxHashFileName);
            this.TabPage4.Controls.Add(this.Label13);
            this.TabPage4.Controls.Add(this.ButtonCalculateETag);
            this.TabPage4.Location = new System.Drawing.Point(4, 22);
            this.TabPage4.Name = "TabPage4";
            this.TabPage4.Size = new System.Drawing.Size(575, 247);
            this.TabPage4.TabIndex = 3;
            this.TabPage4.Text = "Hash";
            this.TabPage4.UseVisualStyleBackColor = true;
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Location = new System.Drawing.Point(14, 14);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(268, 13);
            this.Label17.TabIndex = 41;
            this.Label17.Text = "Use the following buttons to calculate the hash of a file.";
            // 
            // ButtonCalculateMD5
            // 
            this.ButtonCalculateMD5.Location = new System.Drawing.Point(103, 109);
            this.ButtonCalculateMD5.Name = "ButtonCalculateMD5";
            this.ButtonCalculateMD5.Size = new System.Drawing.Size(96, 23);
            this.ButtonCalculateMD5.TabIndex = 4;
            this.ButtonCalculateMD5.Text = "Calculate MD5";
            this.ButtonCalculateMD5.UseVisualStyleBackColor = true;
            this.ButtonCalculateMD5.Click += new System.EventHandler(this.ButtonCalculateMD5_Click);
            // 
            // ButtonHashBrowse
            // 
            this.ButtonHashBrowse.Location = new System.Drawing.Point(359, 42);
            this.ButtonHashBrowse.Name = "ButtonHashBrowse";
            this.ButtonHashBrowse.Size = new System.Drawing.Size(75, 23);
            this.ButtonHashBrowse.TabIndex = 2;
            this.ButtonHashBrowse.Text = "Browse";
            this.ButtonHashBrowse.UseVisualStyleBackColor = true;
            this.ButtonHashBrowse.Click += new System.EventHandler(this.ButtonHashBrowse_Click);
            // 
            // TextBoxHashFileName
            // 
            this.TextBoxHashFileName.Location = new System.Drawing.Point(103, 44);
            this.TextBoxHashFileName.Name = "TextBoxHashFileName";
            this.TextBoxHashFileName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxHashFileName.TabIndex = 1;
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(14, 47);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(83, 13);
            this.Label13.TabIndex = 37;
            this.Label13.Text = "Local File Name";
            // 
            // ButtonCalculateETag
            // 
            this.ButtonCalculateETag.Location = new System.Drawing.Point(103, 80);
            this.ButtonCalculateETag.Name = "ButtonCalculateETag";
            this.ButtonCalculateETag.Size = new System.Drawing.Size(96, 23);
            this.ButtonCalculateETag.TabIndex = 3;
            this.ButtonCalculateETag.Text = "Calculate ETag";
            this.ButtonCalculateETag.UseVisualStyleBackColor = true;
            this.ButtonCalculateETag.Click += new System.EventHandler(this.ButtonCalculateETag_Click);
            // 
            // TabPage7
            // 
            this.TabPage7.Controls.Add(this.ButtonVerCopyVersion);
            this.TabPage7.Controls.Add(this.ButtonVerGenerateURL);
            this.TabPage7.Controls.Add(this.ButtonVerGetACLs);
            this.TabPage7.Controls.Add(this.ButtonVerDeleteVersion);
            this.TabPage7.Controls.Add(this.ButtonVerGetProperties);
            this.TabPage7.Controls.Add(this.Label26);
            this.TabPage7.Controls.Add(this.TextBoxVerVersionID);
            this.TabPage7.Controls.Add(this.Label23);
            this.TabPage7.Controls.Add(this.Label24);
            this.TabPage7.Controls.Add(this.TextBoxVerKeyName);
            this.TabPage7.Controls.Add(this.Label25);
            this.TabPage7.Controls.Add(this.TextBoxVerBucketName);
            this.TabPage7.Location = new System.Drawing.Point(4, 22);
            this.TabPage7.Name = "TabPage7";
            this.TabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage7.Size = new System.Drawing.Size(575, 247);
            this.TabPage7.TabIndex = 6;
            this.TabPage7.Text = "Object Versioning";
            this.TabPage7.UseVisualStyleBackColor = true;
            // 
            // ButtonVerCopyVersion
            // 
            this.ButtonVerCopyVersion.Location = new System.Drawing.Point(246, 134);
            this.ButtonVerCopyVersion.Name = "ButtonVerCopyVersion";
            this.ButtonVerCopyVersion.Size = new System.Drawing.Size(96, 23);
            this.ButtonVerCopyVersion.TabIndex = 51;
            this.ButtonVerCopyVersion.Text = "Copy Version";
            this.ButtonVerCopyVersion.UseVisualStyleBackColor = true;
            this.ButtonVerCopyVersion.Click += new System.EventHandler(this.ButtonVerCopyVersion_Click);
            // 
            // ButtonVerGenerateURL
            // 
            this.ButtonVerGenerateURL.Location = new System.Drawing.Point(132, 163);
            this.ButtonVerGenerateURL.Name = "ButtonVerGenerateURL";
            this.ButtonVerGenerateURL.Size = new System.Drawing.Size(96, 23);
            this.ButtonVerGenerateURL.TabIndex = 50;
            this.ButtonVerGenerateURL.Text = "Generate URL";
            this.ButtonVerGenerateURL.UseVisualStyleBackColor = true;
            this.ButtonVerGenerateURL.Click += new System.EventHandler(this.ButtonVerGenerateURL_Click);
            // 
            // ButtonVerGetACLs
            // 
            this.ButtonVerGetACLs.Location = new System.Drawing.Point(132, 134);
            this.ButtonVerGetACLs.Name = "ButtonVerGetACLs";
            this.ButtonVerGetACLs.Size = new System.Drawing.Size(96, 23);
            this.ButtonVerGetACLs.TabIndex = 49;
            this.ButtonVerGetACLs.Text = "Get ACLs";
            this.ButtonVerGetACLs.UseVisualStyleBackColor = true;
            this.ButtonVerGetACLs.Click += new System.EventHandler(this.ButtonVerGetACLs_Click);
            // 
            // ButtonVerDeleteVersion
            // 
            this.ButtonVerDeleteVersion.Location = new System.Drawing.Point(19, 163);
            this.ButtonVerDeleteVersion.Name = "ButtonVerDeleteVersion";
            this.ButtonVerDeleteVersion.Size = new System.Drawing.Size(96, 23);
            this.ButtonVerDeleteVersion.TabIndex = 48;
            this.ButtonVerDeleteVersion.Text = "Delete Version";
            this.ButtonVerDeleteVersion.UseVisualStyleBackColor = true;
            this.ButtonVerDeleteVersion.Click += new System.EventHandler(this.ButtonVerDeleteVersion_Click);
            // 
            // ButtonVerGetProperties
            // 
            this.ButtonVerGetProperties.Location = new System.Drawing.Point(19, 134);
            this.ButtonVerGetProperties.Name = "ButtonVerGetProperties";
            this.ButtonVerGetProperties.Size = new System.Drawing.Size(96, 23);
            this.ButtonVerGetProperties.TabIndex = 47;
            this.ButtonVerGetProperties.Text = "Get Properties";
            this.ButtonVerGetProperties.UseVisualStyleBackColor = true;
            this.ButtonVerGetProperties.Click += new System.EventHandler(this.ButtonVerGetProperties_Click);
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Location = new System.Drawing.Point(16, 99);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(56, 13);
            this.Label26.TabIndex = 46;
            this.Label26.Text = "Version ID";
            // 
            // TextBoxVerVersionID
            // 
            this.TextBoxVerVersionID.Location = new System.Drawing.Point(92, 96);
            this.TextBoxVerVersionID.Name = "TextBoxVerVersionID";
            this.TextBoxVerVersionID.Size = new System.Drawing.Size(250, 20);
            this.TextBoxVerVersionID.TabIndex = 45;
            // 
            // Label23
            // 
            this.Label23.AutoSize = true;
            this.Label23.Location = new System.Drawing.Point(14, 14);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(350, 13);
            this.Label23.TabIndex = 44;
            this.Label23.Text = "Use the following buttons to preform version related actions on an object.";
            // 
            // Label24
            // 
            this.Label24.AutoSize = true;
            this.Label24.Location = new System.Drawing.Point(14, 47);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(72, 13);
            this.Label24.TabIndex = 40;
            this.Label24.Text = "Bucket Name";
            // 
            // TextBoxVerKeyName
            // 
            this.TextBoxVerKeyName.Location = new System.Drawing.Point(92, 70);
            this.TextBoxVerKeyName.Name = "TextBoxVerKeyName";
            this.TextBoxVerKeyName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxVerKeyName.TabIndex = 43;
            // 
            // Label25
            // 
            this.Label25.AutoSize = true;
            this.Label25.Location = new System.Drawing.Point(16, 73);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(56, 13);
            this.Label25.TabIndex = 42;
            this.Label25.Text = "Key Name";
            // 
            // TextBoxVerBucketName
            // 
            this.TextBoxVerBucketName.Location = new System.Drawing.Point(92, 44);
            this.TextBoxVerBucketName.Name = "TextBoxVerBucketName";
            this.TextBoxVerBucketName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxVerBucketName.TabIndex = 41;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 382);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.TabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.Text = "SprightlySoft S3 All Operations for C#";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.TabPage2.ResumeLayout(false);
            this.TabPage2.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.TabControl1.ResumeLayout(false);
            this.TabPage1.ResumeLayout(false);
            this.TabPage1.PerformLayout();
            this.TabPage3.ResumeLayout(false);
            this.TabPage3.PerformLayout();
            this.TabPage5.ResumeLayout(false);
            this.TabPage5.PerformLayout();
            this.TabPage6.ResumeLayout(false);
            this.TabPage6.PerformLayout();
            this.TabPage4.ResumeLayout(false);
            this.TabPage4.PerformLayout();
            this.TabPage7.ResumeLayout(false);
            this.TabPage7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.TextBox TextBoxDownloadKeyName;
        internal System.Windows.Forms.Button ButtonListBucketVersions;
        internal System.Windows.Forms.Button ButtonGetBucketVersioning;
        internal System.Windows.Forms.TabPage TabPage2;
        internal System.Windows.Forms.Button ButtonSetBucketVersioning;
        internal System.Windows.Forms.Button ButtonSetBucketLogging;
        internal System.Windows.Forms.Button ButtonListBucketAll;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.TextBox TextBoxBucketBucketName;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Button ButtonBucketExists;
        internal System.Windows.Forms.Button ButtonSetBucketACLs;
        internal System.Windows.Forms.Button ButtonCreateBucket;
        internal System.Windows.Forms.Button ButtonGetBucketLogging;
        internal System.Windows.Forms.Button ButtonGetBucketACLs;
        internal System.Windows.Forms.Button ButtonListBucketPages;
        internal System.Windows.Forms.Button ButtonDeleteBucket;
        internal System.Windows.Forms.Button ButtonSetBucketRequestPayment;
        internal System.Windows.Forms.Button ButtonGetBucketRequestPayment;
        internal System.Windows.Forms.Button ButtonGetBucketLocation;
        internal System.Windows.Forms.TextBox TextBoxDownloadFileName;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox TextBoxAWSAccessKeyId;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox TextBoxAWSSecretAccessKey;
        internal System.Windows.Forms.TextBox TextBoxDownloadBucketName;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.TabControl TabControl1;
        internal System.Windows.Forms.TabPage TabPage1;
        internal System.Windows.Forms.Button ButtonListBuckets;
        internal System.Windows.Forms.TabPage TabPage3;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.Button ButtonGenerateURL;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Button ButtonSetObjectMetadata;
        internal System.Windows.Forms.Button ButtonGetObjectACLs;
        internal System.Windows.Forms.TextBox TextBoxObjectKeyName;
        internal System.Windows.Forms.Button ButtonDeleteObject;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Button ButtonSetObjectACLs;
        internal System.Windows.Forms.TextBox TextBoxObjectBucketName;
        internal System.Windows.Forms.Button ButtonGetObjectProperties;
        internal System.Windows.Forms.Button ButtonCopyObject;
        internal System.Windows.Forms.Button ButtonObjectExists;
        internal System.Windows.Forms.TabPage TabPage5;
        internal System.Windows.Forms.Label Label22;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.CheckBox CheckBoxUploadMakePublic;
        internal System.Windows.Forms.TextBox TextBoxUploadContentType;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.Button ButtonUploadBrowse;
        internal System.Windows.Forms.TextBox TextBoxUploadKeyName;
        internal System.Windows.Forms.TextBox TextBoxUploadBucketName;
        internal System.Windows.Forms.TextBox TextBoxUploadFileName;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Button ButtonUploadFile;
        internal System.Windows.Forms.TabPage TabPage6;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Button ButtonDownloadFile;
        internal System.Windows.Forms.TabPage TabPage4;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.Button ButtonCalculateMD5;
        internal System.Windows.Forms.Button ButtonHashBrowse;
        internal System.Windows.Forms.TextBox TextBoxHashFileName;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Button ButtonCalculateETag;
        internal System.Windows.Forms.TabPage TabPage7;
        internal System.Windows.Forms.Button ButtonVerCopyVersion;
        internal System.Windows.Forms.Button ButtonVerGenerateURL;
        internal System.Windows.Forms.Button ButtonVerGetACLs;
        internal System.Windows.Forms.Button ButtonVerDeleteVersion;
        internal System.Windows.Forms.Button ButtonVerGetProperties;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.TextBox TextBoxVerVersionID;
        internal System.Windows.Forms.Label Label23;
        internal System.Windows.Forms.Label Label24;
        internal System.Windows.Forms.TextBox TextBoxVerKeyName;
        internal System.Windows.Forms.Label Label25;
        internal System.Windows.Forms.TextBox TextBoxVerBucketName;
        internal System.Windows.Forms.ComboBox ComboBoxStorageClass;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button ButtonSetBucketNotification;
        internal System.Windows.Forms.Button ButtonGetBucketNotification;
        internal System.Windows.Forms.Button ButtonDeleteBucketPolicies;
        internal System.Windows.Forms.Button ButtonSetBucketPolicies;
        internal System.Windows.Forms.Button ButtonGetBucketPolicies;
    }
}

