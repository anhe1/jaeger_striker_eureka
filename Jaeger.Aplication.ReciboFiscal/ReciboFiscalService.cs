﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Base;
using Jaeger.DataAccess.ReciboFiscal.Repositories;
using Jaeger.Domain.ReciboFiscal.Contracts;
using Jaeger.Domain.ReciboFiscal.Entities;

namespace Jaeger.Aplication.ReciboFiscal {
    public class ReciboFiscalService : IReciboFiscalService {
        protected ISqlReciboFiscalRepository reciboFiscalRepository;

        public ReciboFiscalService() {
            this.reciboFiscalRepository = new SqlSugarReciboFiscalRepository(ConfigService.Synapsis.RDS.Edita);
        }

        public BindingList<ReciboFiscalModel> GetRecibosFiscales(int mes, int anio, int dia, string rfc) {
            return new BindingList<ReciboFiscalModel>(this.reciboFiscalRepository.GetRecibosFiscales(mes, anio, dia, rfc).ToList());
        }

        public bool ExportPrePoliza(List<Partida> partidas, string fileName) {
            if (partidas.Count > 0) {
                var sb = new System.Text.StringBuilder();
                foreach (var item in partidas) {
                    sb.Append(item.ToString() + "\r\n");
                }
                try {
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileName)) {
                        file.WriteLine(sb.ToString()); 
                    }
                } catch (System.Exception) {
                    return false;
                }
                return true;
            }
            return false;
        }
    }
}
