﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.ReciboFiscal.Entities;

namespace Jaeger.Aplication.ReciboFiscal {
    public interface IReciboFiscalService {
        /// <summary>
        /// listado de recibos fiscales por mes año y la verifcacion del emisor del RFC, solo funciona con la serie RF
        /// </summary>
        /// <param name="mes">mes</param>
        /// <param name="anio">año</param>
        /// <param name="rfc">rfc</param>
        BindingList<ReciboFiscalModel> GetRecibosFiscales(int mes, int anio, int dia, string rfc);
        bool ExportPrePoliza(List<Partida> partidas, string fileName);
    }
}
