﻿using System;
using Newtonsoft.Json;

namespace Jaeger.Util.Entities
{
    public class BackupComprobante
    {
        public BackupComprobante()
        {
        }

        [JsonProperty("subTipo")]
        public string SubTipoText { get; set; }

        [JsonProperty("keyname")]
        public string KeyName { get; set; }

        [JsonProperty("uuid")]
        public string IdComprobante { get; set; }

        [JsonProperty("fecha")]
        public DateTime Fecha { get; set; }

        [JsonProperty("nombre")]
        public string Nombre { get; set; }

        [JsonProperty("rfc")]
        public string RFC { get; set; }

        [JsonProperty("ejercicio")]
        public int Ejercicio { get; set; }

        [JsonProperty("periodo")]
        public int Periodo { get; set; }

        [JsonProperty("xml")]
        public string UrlXml { get; set; }

        [JsonProperty("pdf")]
        public string UrlPdf { get; set; }

        [JsonProperty("accuse")]
        public string UrlAccuse { get; set; }

        [JsonProperty("accusePDF")]
        public string UrlPdfAcuse { get; set; }

        [JsonProperty("cat")]
        public string Categoria { get; set; }

        [JsonProperty("nomina")]
        public string Nomina { get; set; }
    }
}
