﻿using System;
using System.IO;

namespace Jaeger.Util.Helpers {
    static public class HelperLogError {
        public static string FileName;

        /// <summary>
        /// Constructor
        /// </summary>
        static HelperLogError() {
            HelperLogError.FileName = "C:\\Jaeger\\Jaeger.Log\\sqlJaeger.log";
        }

        static public bool LogDelete() {
            try {
                File.Delete(HelperLogError.FileName);
                return true;
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static void LogWrite(string mensaje) {
            try {
                if (!File.Exists(HelperLogError.FileName)) {
                    File.Create(HelperLogError.FileName).Close();
                }
                var streamWriter = File.AppendText(HelperLogError.FileName);
                object[] type = new object[] { mensaje, ",", DateTime.Now.ToString("s") };
                streamWriter.WriteLine(string.Concat(type));
                streamWriter.Close();
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }
    }
}
