﻿using System;

namespace Jaeger.Util.Helpers
{
    public class DateFunctions
    {
        public static void FirstAndLastDayOfMonth(DateTime date, ref string first, ref string last)
        {
            DateTime dateTime;
            DateTime dateTime1 = new DateTime(date.Year, date.Month, 1, 0, 0, 0);
            first = dateTime1.ToString("yyyyMMdd 00:00:00");
            dateTime = (dateTime1.Month != 12 ? new DateTime(dateTime1.Year, dateTime1.Month + 1, 1) : new DateTime(dateTime1.Year + 1, 1, 1));
            DateTime dateTime2 = dateTime.AddDays(-1);
            last = string.Concat(dateTime2.ToString("yyyyMMdd"), " 23:59:59");
        }

        public static string GetTiempo(TimeSpan timeSpan)
        {
            int hours;
            string str = "";
            if (timeSpan.Days > 0)
            {
                str = string.Concat(str, timeSpan.Days, " Días ");
            }
            if (timeSpan.Hours <= 0)
            {
                str = string.Concat(str, "00:");
            }
            else
            {
                hours = timeSpan.Hours;
                str = string.Concat(str, hours.ToString("00"), ":");
            }
            if (timeSpan.Minutes <= 0)
            {
                str = string.Concat(str, "00:");
            }
            else
            {
                hours = timeSpan.Minutes;
                str = string.Concat(str, hours.ToString("00"), ":");
            }
            if (timeSpan.Seconds <= 0)
            {
                str = string.Concat(str, "00:");
            }
            else
            {
                hours = timeSpan.Seconds;
                str = string.Concat(str, hours.ToString("00"));
            }
            return string.Concat("(", str, ")");
        }
    }
}
