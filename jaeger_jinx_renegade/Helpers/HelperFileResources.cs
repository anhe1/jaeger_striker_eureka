﻿using System.IO;
using System.Reflection;

namespace Jaeger.Util.Helpers
{
    public class HelperFileResources
    {
        public static string GetResources(string strResources, string strFolder = "Jaeger.Resources", string startDirectory = "")
        {
            string strDirectory = Path.Combine("C:\\", strFolder);
            if (!(startDirectory == ""))
            {
                strDirectory = startDirectory;
            }

            if (!Directory.Exists(strDirectory))
            {
                Directory.CreateDirectory(strDirectory);
            }

            using (Stream oStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Concat("Jaeger", strResources)))
            {
                FileStream oFileStream = new FileStream(Path.Combine(strDirectory, strResources), FileMode.Create, FileAccess.Write);
                oStream.CopyTo(oFileStream);
                oFileStream.Close();
            }
            return strDirectory;
        }

        public static string GetResources(string strResources)
        {
            string result = string.Empty;
            using (Stream oStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Concat("Jaeger.", strResources)))
            {
                result = (new StreamReader(oStream)).ReadToEnd();
            }
            return result;
        }
    }
}
