﻿using System;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.C2K.Domain;
using Jaeger.DataAccess.Abstractions;
using Jaeger.C2K.Contracts;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.C2K.Repositories {
    public class SqlFbTComprobanteRepository : RepositoryMaster<TComprobanteModel>, ISqlTComprobanteRepository {
        public SqlFbTComprobanteRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public TComprobanteModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<TComprobanteModel> GetList() {
            var sqlCommand = new FbCommand() {
                CommandText = @"select TComprobante.UUID from TComprobante;"
            };
            return this.GetMapper(sqlCommand);
        }

        public int Insert(TComprobanteModel item) {
            throw new NotImplementedException();
        }

        public int Update(TComprobanteModel item) {
            throw new NotImplementedException();
        }
    }
}
