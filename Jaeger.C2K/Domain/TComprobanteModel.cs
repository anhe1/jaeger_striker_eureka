﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.C2K.Domain {
    public class TComprobanteModel {
        [DataNames("UUID")]
        public string IdDocumento { get; set; }
    }
}
