﻿using Jaeger.C2K.Domain;
using Jaeger.Domain.Contracts;

namespace Jaeger.C2K.Contracts {
    public interface ISqlTComprobanteRepository : IGenericRepository<TComprobanteModel> {
    }
}
