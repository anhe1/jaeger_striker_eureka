﻿using System;
using System.Collections.Generic;
using System.Linq;
using RestSharp;
using Jaeger.ValidaSAT.Entities;

namespace Jaeger.ValidaSAT.Service {

    public class ConsultaRFCMasivaService : ConsultaBaseService {
        private string urlService1 = "https://agsc.siat.sat.gob.mx/PTSC/ValidaRFC/UploadFileServletCPNOM";
        private string urlService2 = "https://agsc.siat.sat.gob.mx/PTSC/ValidaRFC/ServletProcesaArchivoMasivo";

        public ConsultaRFCMasivaService() : base() {
            this.urlService1 = "https://agsc.siat.sat.gob.mx/PTSC/ValidaRFC/UploadFileServletCPNOM";
            this.urlService2 = "https://agsc.siat.sat.gob.mx/PTSC/ValidaRFC/ServletProcesaArchivoMasivo";
        }

        private static List<ContribuyenteResponse> GetData(string contenido, string registros_enviados) {
            var _responses = new List<ContribuyenteResponse>();
            try {
                var registros = contenido.Split(new char[] { '\r' });
                for (int i = 0; i < registros.Length; i++) {
                    if (registros[i] != null && !string.IsNullOrEmpty(registros[i])) {
                        var columnas = registros[i].Split(new char[] { '|' });
                        if (columnas.Count() > 3) {
                            var validacionResponse = new ContribuyenteResponse() {
                                Registro = Convert.ToInt32(columnas[0].ToString()),
                                RFC = columnas[1].ToString(),
                                NombreRazonSocial = columnas[2].ToString(),
                                CodigoPostal = columnas[3].ToString(),
                                Resultado = columnas[4].ToString()
                            };
                            _responses.Add(validacionResponse);
                        } else {
                            var validacionResponse = new ContribuyenteResponse() {
                                Registro = Convert.ToInt32(columnas[0].ToString()),
                                RFC = columnas[1].ToString(),
                                Resultado = columnas[columnas.Length - 1].ToString()
                            };
                            _responses.Add(validacionResponse);
                        }
                    }
                }
            } catch (Exception ex) {
                LogErrorService.LogWrite(string.Concat("[ConsultaRFCMasivaService] Error: ", ex.Message), string.Concat(new string[] { ex.StackTrace, Environment.NewLine, registros_enviados, Environment.NewLine, contenido }));
            }
            return _responses;
        }

        public virtual List<ContribuyenteResponse> Procesar(List<ContribuyenteRequest> lstRFC) {
            var _responses = new List<ContribuyenteResponse>();
            try {
                // El archivo a seleccionar deberá cumplir con las siguientes características:
                //1.- Se debe utilizar como separador el pipe "|".
                //2.- Las columnas no deben de tener nombre/ título, en el primer renglón.
                //3.- En la primera columna, enumerar los registros proporcionados.
                //4.- En la segunda columna, proporcionar las claves de RFC a consultar, evite que el RFC tenga espacios en blanco.
                string _layoutTxt = "";
                foreach (ContribuyenteRequest validacionRequest in lstRFC) {
                    _layoutTxt = string.Concat(_layoutTxt, validacionRequest.ToString(), Environment.NewLine);
                }

                string _response = this.GetRequest(this.urlService1, this.urlService1, Method.POST, _layoutTxt, false);
                _response = this.GetResponse(this.urlService2, this.urlService2, Method.GET, "", false);
                // procesar resultado
                if (_response == null || string.IsNullOrEmpty(_response)) {
                    LogErrorService.LogWrite("[Registros Enviados]", _layoutTxt);
                } else {
                    _responses = GetData(_response, _layoutTxt);
                }
            } catch (Exception ex) {
                LogErrorService.LogWrite(string.Concat("[ConsultaRFCMasivaService] Error: ", ex.Message), ex.StackTrace);
            }
            return _responses;
        }
    }
}
