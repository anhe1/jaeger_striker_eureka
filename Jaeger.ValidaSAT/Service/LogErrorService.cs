﻿using System;
using System.IO;

namespace Jaeger.ValidaSAT.Service {

    public class LogErrorService {
        public static string FileName;

        /// <summary>
        /// Constructor
        /// </summary>
        static LogErrorService() {
            FileName = @"C:\Jaeger\Jaeger.Log\jaeger_validasat.log";
        }

        static public bool LogDelete() {
            try {
                File.Delete(FileName);
                return true;
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static void LogWrite(string mensaje, string stackTrace = "") {
            try {
                if (!File.Exists(FileName)) {
                    File.Create(FileName).Close();
                }
                var streamWriter = File.AppendText(FileName);
                object[] type = new object[] { mensaje, "|", DateTime.Now.ToString("s"), "|", stackTrace };
                streamWriter.WriteLine(string.Concat(type));
                streamWriter.Close();
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }
    }
}
