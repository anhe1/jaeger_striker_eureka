﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using RestSharp;
using Jaeger.ValidaSAT.Contracts;

namespace Jaeger.ValidaSAT.Service {
    public class ConsultaBaseService : IConsultaBaseService {
        public ConsultaBaseService() { }

        public virtual void InitCookies(bool initCookies) {
            if (initCookies) {
                ManagerService.Sesion = new CookieContainer();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            }
        }

        public virtual RestRequest CreateRequest(RestClient restClient) {
            var restRequest = new RestRequest();
            restRequest.AddParameter("Cookie", restClient.CookieContainer, ParameterType.Cookie);
            restRequest.AddParameter("Accept-Language", "es-MX,es;q=0.8,en-US;q=0.5,en;q=0.3", ParameterType.HttpHeader);
            restRequest.AddParameter("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8", ParameterType.HttpHeader);
            return restRequest;
        }

        public virtual RestClient CreateRestClient(Uri uri) {
            var restClient = new RestClient() {
                BaseUrl = uri,
                FollowRedirects = false,
                CookieContainer = ManagerService.Sesion
            };
            return restClient;
        }

        public string GetRequest(string _url, string previousURL, Method method, string parameter = "", bool initCookies = false) {
            string _response;
            try {
                InitCookies(initCookies);
                var uri = new Uri(_url);
                var restClient = CreateRestClient(uri);
                var restRequest = CreateRequest(restClient);

                if (!string.IsNullOrEmpty(previousURL)) {
                    restRequest.AddParameter("Referer", previousURL, ParameterType.HttpHeader);
                }
                if (method != Method.POST) {
                    restRequest.Method = method;
                } else {
                    restRequest.Method = Method.POST;
                    restRequest.AddParameter("Host", uri.Host, ParameterType.HttpHeader);
                    restRequest.AddParameter("Content-Type", "multipart/form-data", ParameterType.HttpHeader);
                    restRequest.AddParameter("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", ParameterType.HttpHeader);
                    restRequest.AddParameter("Content-Length", parameter.Length, ParameterType.HttpHeader);
                    byte[] bytes = Encoding.UTF8.GetBytes(parameter);
                    restRequest.AddFile("uploadFile", bytes, "layout.txt", "text/plain");
                }

                restClient.Encoding = Encoding.UTF8;
                IRestResponse restResponse = restClient.Execute(restRequest);
                _response = restResponse.StatusCode != HttpStatusCode.Found || restResponse.StatusCode != HttpStatusCode.Found ? restResponse.Content : new List<Parameter>(restResponse.Headers).Find((x) => x.Name == "Location").Value.ToString();
            } catch (Exception ex) {
                LogErrorService.LogWrite(string.Concat("[Consulta Pagina] Error: ", ex.Message), ex.StackTrace);
                _response = string.Empty;
            }
            return _response;
        }

        public virtual string GetResponse(string _url, string previousURL, Method method, string parameter = "", bool initCookies = false) {
            string _response;
            try {
                InitCookies(initCookies);
                var uri = new Uri(_url);
                var restClient = CreateRestClient(uri);
                var restRequest = CreateRequest(restClient);

                if (!string.IsNullOrEmpty(previousURL)) {
                    restRequest.AddParameter("Referer", previousURL, ParameterType.HttpHeader);
                }

                if (method != Method.POST) {
                    restRequest.Method = method;
                } else {
                    restRequest.Method = Method.POST;
                    restRequest.AddParameter("Host", uri.Host, ParameterType.HttpHeader);
                    restRequest.AddParameter("Content-Type", "application/x-www-form-urlencoded", ParameterType.HttpHeader);
                    restRequest.AddParameter("UserAgent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", ParameterType.HttpHeader);
                    restRequest.AddParameter("Content-Length", parameter.Length, ParameterType.HttpHeader);
                    restRequest.AddParameter("application/json", parameter, ParameterType.RequestBody);
                }

                restClient.Encoding = Encoding.UTF8;
                IRestResponse restResponse = restClient.Execute(restRequest);
                _response = restResponse.StatusCode != HttpStatusCode.Found || restResponse.StatusCode != HttpStatusCode.Found ? Encoding.UTF8.GetString(restResponse.RawBytes) : new List<Parameter>(restResponse.Headers).Find((x) => x.Name == "Location").Value.ToString();
            } catch (Exception ex) {
                LogErrorService.LogWrite(string.Concat("[Consulta Pagina] Error: ", ex.Message), ex.StackTrace);
                _response = "";
            }
            return _response;
        }
    }
}
