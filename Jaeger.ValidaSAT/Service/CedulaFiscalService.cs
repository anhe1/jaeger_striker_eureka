﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using RestSharp;
using Jaeger.ValidaSAT.Contracts;
using Jaeger.ValidaSAT.Entities;

namespace Jaeger.ValidaSAT.Service {
    public class CedulaFiscalService : ConsultaBaseService, ICedulaFiscalService {
        #region declaraciones
        private readonly string[] caracteres = new string[] { "&Aacute;", "Á", "&Agrave;", "À", "&Acirc;", "Â", "&Auml;", "Ä", "&Atilde;", "Ã", "&Aring;", "Å", "&aacute;", "á", "&agrave;", "à", "&acirc;", "â", "&auml;", "ä", "&atilde;", "ã", "&aring;", "å", "&Eacute;", "É", "&Egrave;", "È", "&Ecirc;", "Ê", "&Euml;", "Ë", "&eacute;", "é", "&egrave;", "è", "&ecirc;", "ê", "&euml;", "ë", "&Iacute;", "Í", "&Igrave;", "Ì", "&Icirc;", "Î", "&Iuml;", "Ï", "&iacute;", "í", "&igrave;", "ì", "&icirc;", "î", "&iuml;", "ï", "&Oacute;", "Ó", "&Ograve;", "Ò", "&Ocirc;", "Ô", "&Ouml;", "Ö", "&Otilde;", "Õ", "&oacute;", "ó", "&ograve;", "ò", "&ocirc;", "ô", "&ouml;", "ö", "&otilde;", "õ", "&Uacute;", "Ú", "&Ugrave;", "Ù", "&Ucirc;", "Û", "&Uuml;", "Ü", "&uacute;", "ú", "&ugrave;", "ù", "&ucirc;", "û", "&uuml;", "ü", "&Yacute;", "Ý", "&yacute;", "ý", "&yuml;", "ÿ", "&ntilde;", "ñ", "&Ntilde;", "Ñ", "&Ccedil;", "Ç", "&ccedil;", "ç", "&iexcl;", "¡", "&iquest;", "¿", "&acute;", "´", "&middot;", "·", "&cedil;", "¸", "&laquo;", "«", "&raquo;", "»", "&uml;", "¨", "&AElig;", "Æ", "&aelig;", "æ", "&szlig;", "ß", "&micro;", "µ", "&ETH;", "Ð", "&eth;", "ð", "&THORN;", "Þ", "&thorn;", "þ", "&cent;", "¢", "&pound;", "£", "&curren;", "¤", "&yen;", "¥", "&euro;", "€", "&#36;", "$", "&sup1;", "¹", "&sup2;", "²", "&sup3;", "³", "&times;", "×", "&divide;", "÷", "&plusmn;", "±", "&frac14;", "¼", "&frac12;", "½", "&frac34;", "¾", "&Oslash;", "Ø", "&oslash;", "ø", "&not;", "¬", "&lt;", "<", "&gt;", ">", "&amp;", "&", "&nbsp;", " ", "&quot;", "\"", "&ordm;", "º", "&ordf;", "ª", "&copy;", "©", "&reg;", "®", "&deg;", "°", "&brvbar;", "¦", "&sect;", "§", "&para;", "¶", "&macr;", "¯" };
        private readonly string UrlBase = "https://siat.sat.gob.mx/app/qr/faces/pages/mobile/validadorqr.jsf?";
        #endregion

        public CedulaFiscalService() {

        }

        public ResponseCedula GetByURL(string urlCedula) {
            var response = new ResponseCedula();

            try {
                if (!urlCedula.Contains(this.UrlBase)) {
                    response.Message = "La URL enviada no es de alguna cédula de identificación fiscal";
                } else {
                    string request = this.GetResponse(urlCedula, "", RestSharp.Method.POST);
                    if (string.IsNullOrEmpty(request)) {
                        response.Message = "No se pudo consultar la URL enviada";
                    } else if (request.Contains("no se le ha emitido su Cédula de identificación fiscal")) {
                        response.Message = request;
                    } else {
                        request = this.ReemplazarCaracteresEspeciales(request);
                        string[] strArrays = request.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                        string str2 = strArrays[0].ToString();
                        int num = str2.IndexOf("RFC:");
                        string rfc = str2.Remove(0, num + 4);
                        int num1 = rfc.IndexOf(",");
                        rfc = rfc.Remove(num1).TrimEnd(new char[0]).TrimStart(new char[0]);
                        if (rfc.Length.Equals(12) ? false : !rfc.Length.Equals(13)) {
                            response.Message = "No se pudo obtener el RFC en cédula de identificación fiscal";
                        } else {
                            response.IsValida = true;
                            var cedulaFiscal = new CedulaFiscal(rfc);
                            if (!rfc.Length.Equals(13)) {
                                cedulaFiscal.TipoPersona = CedulaFiscal.TipoPersonaEnum.Moral;
                                cedulaFiscal.Moral = this.GetDataPersonaMoral(rfc, strArrays);
                            } else {
                                cedulaFiscal.TipoPersona = CedulaFiscal.TipoPersonaEnum.Fisica;
                                cedulaFiscal.Fisica = this.GetDataPersonaFisica(rfc, strArrays);
                            }
                            response.CedulaFiscal = cedulaFiscal;
                        }
                    }
                }
                response.Message = response.Message;
            } catch (Exception exception) {
                response.Message = exception.Message.ToString();
            }
            return response;
        }

        public ResponseCedula GetByRFC(string rfc1, string idConstancia) {
            var urlCedula = string.Format(this.UrlBase + "D1=10&D2=1&D3={0}_{1}", idConstancia, rfc1);
            return this.GetByURL(urlCedula);
        }

        public ResponseCedula GetRequest(IRequestCedula request) {
            var response = new ResponseCedula();
            
            return response;
        }

        public override string GetResponse(string _url, string previousURL, Method method, string parameter = "", bool initCookies = false) {
            var response = new StringBuilder();
            var html = base.GetResponse(_url, previousURL, method, parameter, initCookies);
            html = Regex.Replace(html, "<(.|\\n)*?>", " ");
            int num = html.IndexOf("RFC:");
            html = html.Remove(0, num - 3);
            html = html.Replace("                           ", "\r\n");
            html = html.Replace("     ", "\r\n");
            html = html.Replace("    ", "\r\n");
            html = html.Replace("   ", " ");

            var content2 = html.Split(new char[] { '\r', });

            foreach (var item in content2) {
                if (item.Length > 0) {
                    if (!item.Contains("PrimeFaces")) {
                        if (item.Length > 1)
                            response.Append(item + "\r");
                    }
                }
            }
            return response.ToString();
        }

        private IPersonaFisica GetDataPersonaFisica(string rfc, string[] strArrays) {
            var perFisica = new Persona(rfc);
            var regimenFiscal = new RegimenFiscal();

            for (int i = 0; i < (int)strArrays.Length; i++) {
                string str = strArrays[i];
                string str1 = str;
                string str2 = "";
                int num = str1.IndexOf(":");
                if (num > 0) {
                    str2 = str1.Remove(0, num + 1).TrimEnd(new char[0]).TrimStart(new char[0]).Replace("  ", " ");
                }
                if (str.Contains("CURP:")) {
                    perFisica.CURP = str2;
                } else if (str.Contains("Nombre:")) {
                    perFisica.Nombre = str2;
                } else if (str.Contains("Apellido Paterno:")) {
                    perFisica.PrimerApellido = str2;
                } else if (str.Contains("Apellido Materno:")) {
                    perFisica.SegundoApellido = str2;
                } else if (str.Contains("Fecha Nacimiento:")) {
                    perFisica.FechaNacimiento = ReaderDateTime(str2);
                } else if (str.Contains("Fecha de Inicio de operaciones:")) {
                    perFisica.FechaInicio = ReaderDateTime(str2);
                } else if (str.Contains("Situación del contribuyente:")) {
                    perFisica.Situacion = str2;
                } else if (str.Contains("Fecha del último cambio de situación:")) {
                    perFisica.FechaUltimoCambio = ReaderDateTime(str2);
                } else if (str.Contains("Entidad Federativa:")) {
                    perFisica.EntidadFederativa = str2;
                } else if (str.Contains("Municipio o delegación:")) {
                    perFisica.MunicipioDelegacion = str2;
                } else if (str.Contains("Colonia:")) {
                    perFisica.Colonia = str2;
                } else if (str.Contains("Tipo de vialidad:")) {
                    perFisica.TipoVialidad = str2;
                } else if (str.Contains("Nombre de la vialidad:")) {
                    perFisica.NombreVialidad = str2;
                } else if (str.Contains("Número exterior:")) {
                    perFisica.NumExterior = str2;
                } else if (str.Contains("Número interior:")) {
                    perFisica.NumInterior = str2;
                } else if (str.Contains("CP:")) {
                    perFisica.CodigoPostal = str2;
                } else if (str.Contains("Correo electrónico:")) {
                    perFisica.Correo = str2;
                } else if (str.Contains("AL:")) {
                    perFisica.Al = str2;
                } else if (str.Contains("Régimen:")) {
                    regimenFiscal = new RegimenFiscal();
                    var regimenSATs = RegimenSAT.GetList();
                    var regimenSAT = regimenSATs.FirstOrDefault<RegimenSAT>((RegimenSAT x) => str2.ToUpper().Contains(x.Descripcion.ToUpper()));
                    if (regimenSAT != null) {
                        regimenFiscal.Clave = regimenSAT.Clave;
                        regimenFiscal.Descripcion = regimenSAT.Descripcion;
                    }
                } else if (str.Contains("Fecha de alta:")) {
                    regimenFiscal.FechaAlta = ReaderDateTime(str2);
                    perFisica.Regimenes.Add(regimenFiscal);
                }
            }
            return perFisica;
        }

        public IPersonaMoral GetDataPersonaMoral(string rfc, string[] array) {
            var perMoral = new Persona(rfc);
            var regimenFiscal = new RegimenFiscal();
            string[] filas = array;
            for (int i = 0; i < (int)filas.Length; i++) {
                string str = filas[i];
                string str1 = str;
                string str2 = "";
                int num = str1.IndexOf(":");
                if (num > 0) {
                    str2 = str1.Remove(0, num + 1).TrimEnd(new char[0]).TrimStart(new char[0]).Replace("  ", " ");
                }
                if (str.Contains("Denominación o Razón Social:")) {
                    perMoral.Nombre = str2;
                } else if (str.Contains("Régimen de capital:")) {
                    perMoral.RegimenCapital = str2;
                } else if (str.Contains("Fecha de constitución:")) {
                    perMoral.FechaConstitucion = ReaderDateTime(str2);
                } else if (str.Contains("Fecha de Inicio de operaciones:")) {
                    perMoral.FechaInicio = ReaderDateTime(str2);
                } else if (str.Contains("Situación del contribuyente:")) {
                    perMoral.Situacion = str2;
                } else if (str.Contains("Fecha del último cambio de situación:")) {
                    perMoral.FechaUltimoCambio = ReaderDateTime(str2);
                } else if (str.Contains("Entidad Federativa:")) {
                    perMoral.EntidadFederativa = str2;
                } else if (str.Contains("Municipio o delegación:")) {
                    perMoral.MunicipioDelegacion = str2;
                } else if (str.Contains("Colonia:")) {
                    perMoral.Colonia = str2;
                } else if (str.Contains("Tipo de vialidad:")) {
                    perMoral.TipoVialidad = str2;
                } else if (str.Contains("Nombre de la vialidad:")) {
                    perMoral.NombreVialidad = str2;
                } else if (str.Contains("Número exterior:")) {
                    perMoral.NumExterior = str2;
                } else if (str.Contains("Número interior:")) {
                    perMoral.NumInterior = str2;
                } else if (str.Contains("CP:")) {
                    perMoral.CodigoPostal = str2;
                } else if (str.Contains("Correo electrónico:")) {
                    perMoral.Correo = str2;
                } else if (str.Contains("AL:")) {
                    perMoral.Al = str2;
                } else if (str.Contains("Régimen:")) {
                    var regimenSATs = RegimenSAT.GetList();
                    var regimenSAT = regimenSATs.FirstOrDefault<RegimenSAT>((RegimenSAT x) => str2.ToUpper().Contains(x.Descripcion.ToUpper()));
                    if (regimenSAT != null) {
                        regimenFiscal.Clave = regimenSAT.Clave;
                        regimenFiscal.Descripcion = regimenSAT.Descripcion;
                    }
                } else if (str.Contains("Fecha de alta:")) {
                    regimenFiscal.FechaAlta = ReaderDateTime(str2);
                    perMoral.RegimenFiscal = regimenFiscal;
                }
            }
            return perMoral;
        }

        private string ReemplazarCaracteresEspeciales(string origen) {
            for (int i = 0; i < (int)this.caracteres.Length; i += 2) {
                if (origen.Contains(this.caracteres[i])) {
                    origen = origen.Replace(this.caracteres[i], this.caracteres[i + 1]);
                }
            }
            return origen;
        }

        private static DateTime ReaderDateTime(object valueObject) {
            if (valueObject != DBNull.Value) {
                if (valueObject != null) {
                    if (valueObject.ToString().Trim() != string.Empty) {
                        return DateTime.Parse(valueObject.ToString());
                    }
                }
            }
            return DateTime.MinValue;
        }
    }
}
