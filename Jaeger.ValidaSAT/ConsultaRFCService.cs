﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Jaeger.ValidaSAT.Contracts;
using Jaeger.ValidaSAT.Entities;
using Jaeger.ValidaSAT.Service;

namespace Jaeger.ValidaSAT {
    public class ConsultaRFCService : ConsultaRFCMasivaService, IConsultaRFCService {
        private readonly string Mensaje_SinRegistros = "No cuenta con registros a procesar";
        private readonly string Mensaje_RegistroExedido = "Sobrepasa el número de registros permitidos";
        private readonly string Mensaje_SinResultados = "Se accedió al servicio de validación masiva del SAT pero no se obtuvieron resultados, favor de intentarlo nuevamente.";

        public ConsultaRFCService() : base() {
            this.Initialize();
        }

        public virtual void Initialize() {
            try {
                this.CacheClean();
            } catch (Exception ex) {
                LogErrorService.LogWrite(string.Concat("[Inicializa] Error: ", ex.Message), ex.StackTrace);
            }
        }

        private void CacheClean() {
            try {
                Util.Services.CookieReader.InternetSetOption(IntPtr.Zero, 42, IntPtr.Zero, 0);
                ManagerService.Sesion = new CookieContainer();
            } catch (Exception ex) {
                LogErrorService.LogWrite(string.Concat("[Limpiar Cache] Error: ", ex.Message), ex.StackTrace);
            }
        }

        public IValidacionResponse Request(ValidacionRequest resquest) {
            var d = new List<ContribuyenteRequest>();
            int d11 = 1;
            foreach (var item in resquest.Registros) {
                item.Registro = d11;
                d.Add(item);
                d11++;
            }
            var d1 = Request(d);
            resquest.Registros = d1.Registros;
            resquest.Mensaje = d1.Mensaje;
            return resquest;
        }

        public IValidacionResponse Request(List<ContribuyenteRequest> lstRFC) {
            IValidacionResponse validacion = new ValidacionResponse();
            try {
                if (lstRFC == null || lstRFC.Count <= 0) {
                    validacion.Mensaje = this.Mensaje_SinRegistros;
                } else if (lstRFC.Count > 5000) {
                    validacion.Mensaje = this.Mensaje_RegistroExedido;
                } else {
                    var validacionResponses = this.Procesar(lstRFC);
                    if (validacionResponses == null || validacionResponses.Count <= 0) {
                        validacion.Mensaje = this.Mensaje_SinResultados;
                    } else {
                        validacion.Registros = validacionResponses;
                        validacion.Mensaje = "Validación terminada con exito";
                        for (int i = 0; i < validacion.Registros.Count; i++) {
                            var d = lstRFC.Where(it => it.Registro == validacion.Registros[i].Registro && it.RFC == validacion.Registros[i].RFC).FirstOrDefault();
                            if (d != null) {
                                validacion.Registros[i].NombreRazonSocial = d.NombreRazonSocial;
                                validacion.Registros[i].CodigoPostal = d.CodigoPostal;
                                validacion.Registros[i].Tag = d.Tag;
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                LogErrorService.LogWrite(string.Concat("[Inicia Validación] Error: ", ex.Message), ex.StackTrace);
            }
            return validacion;
        }
    }
}
