﻿using Jaeger.ValidaSAT.Entities;

namespace Jaeger.ValidaSAT.Contracts {
    public interface IConsultaRFCService {
        void Initialize();

        IValidacionResponse Request(ValidacionRequest resquest);
    }
}