﻿using static Jaeger.ValidaSAT.Entities.CedulaFiscal;

namespace Jaeger.ValidaSAT.Contracts {
    public interface ICedulaFiscal {
        /// <summary>
        /// obenter o establecer el tipo de persona asociado a la cedula fiscal
        /// </summary>
        TipoPersonaEnum TipoPersona { get; set; }

        /// <summary>
        /// obtener o establrcer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente sin guiones o espacios
        /// </summary>
        string RFC { get; set; }

        IPersonaFisica Fisica { get; set; }

        IPersonaMoral Moral { get; set; }
    }
}
