﻿using Jaeger.ValidaSAT.Entities;

namespace Jaeger.ValidaSAT.Contracts {
    public interface IConsultaRetencionService {
        ComprobanteRetencionResponse Consulta(ComprobanteRetencionRequest request);
    }
}