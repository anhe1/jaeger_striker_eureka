﻿using Jaeger.ValidaSAT.Entities;

namespace Jaeger.ValidaSAT.Contracts {
    public interface IResponseCedula {
        /// <summary>
        /// obtener o establecer es el objeto es valido
        /// </summary>
        bool IsValida { get; set; }

        CedulaFiscal CedulaFiscal { get; set; }

        string Message { get; set; }
    }
}
