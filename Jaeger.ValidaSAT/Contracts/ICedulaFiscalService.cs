﻿using Jaeger.ValidaSAT.Entities;

namespace Jaeger.ValidaSAT.Contracts {
    public interface ICedulaFiscalService {
        ResponseCedula GetByURL(string urlCedula);

        ResponseCedula GetByRFC(string rfc1, string idConstancia);

        ResponseCedula GetRequest(IRequestCedula request);
    }
}
