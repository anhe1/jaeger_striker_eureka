﻿namespace Jaeger.ValidaSAT.Contracts {
    public interface IContribuyenteRequest {
        /// <summary>
        /// obtener o establecer numero consecutivo
        /// </summary>
        int Registro { get; set; }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// minLength = 12  maxLength = 13
        /// pattern = "[A-Z&amp;Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]"
        /// </summary>
        string RFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre o razon social del contribuyente
        /// </summary>
        string NombreRazonSocial { get; set; }

        /// <summary>
        /// obtener o establecer codigo postal del domicilio fiscal del contribuyente
        /// </summary>
        string CodigoPostal { get; set; }

        string ToString();
    }
}