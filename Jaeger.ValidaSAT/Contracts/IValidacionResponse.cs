﻿using System.Collections.Generic;

namespace Jaeger.ValidaSAT.Entities {
    public interface IValidacionResponse {
        string Mensaje { get; set; }
        List<ContribuyenteResponse> Registros { get; set; }
    }
}