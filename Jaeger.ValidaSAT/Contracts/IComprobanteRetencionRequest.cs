﻿namespace Jaeger.ValidaSAT.Contracts {
    public interface IComprobanteRetencionRequest {
        /// <summary>
        /// obtener o establecer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente emisor del comprobante.
        /// </summary>
        string EmisorRFC { get; set; }
        /// <summary>
        /// obtener o establecer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente receptor del comprobante.
        /// </summary>
        string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        string IdDocumento { get; set; }

        /// <summary>
        /// obtener cadena de URL para el servicio
        /// </summary>
        string GetElementoImpreso();
    }
}