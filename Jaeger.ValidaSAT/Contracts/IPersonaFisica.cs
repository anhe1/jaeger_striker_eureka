﻿using System;
using System.Collections.Generic;
using Jaeger.ValidaSAT.Entities;

namespace Jaeger.ValidaSAT.Contracts {
    public interface IPersonaFisica : IDatosUbicacion {
        /// <summary>
        /// obtener o establrcer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente sin guiones o espacios
        /// </summary>
        string RFC { get; set; }

        #region datos de identificacion
        string CURP { get; set; }

        string Nombre { get; set; }

        string PrimerApellido { get; set; }

        string SegundoApellido { get; set; }

        /// <summary>
        /// obtener o establecer fecha de nacimineto
        /// </summary>
        DateTime? FechaNacimiento { get; set; }

        /// <summary>
        /// obtener o establecer fecha de inicio de operaciones
        /// </summary>
        DateTime? FechaInicio { get; set; }

        /// <summary>
        /// obtener o establecer situacion del contribuyente
        /// </summary>
        string Situacion { get; set; }

        /// <summary>
        /// obtener o establecer fecha del ultimo cambio de situacion
        /// </summary>
        DateTime? FechaUltimoCambio { get; set; }
        #endregion

        #region caracteriasticas fiscales
        List<RegimenFiscal> Regimenes { get; set; }
        #endregion
    }

    public interface IPersona : IPersonaFisica, IPersonaMoral, IDatosUbicacion {

    }
}
