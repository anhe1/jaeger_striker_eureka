﻿using System.ComponentModel;

namespace Jaeger.ValidaSAT.ValueObjects {
    public enum EstadoComprobanteEnum {
        [Description("Sin Validar")]
        Sin_Validar = 1,
        [Description("Vigente")]
        Vigente = 2,
        [Description("Cancelado")]
        Cancelado = 3
    }
}
