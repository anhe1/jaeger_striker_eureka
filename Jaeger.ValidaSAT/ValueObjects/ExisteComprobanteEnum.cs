﻿using System.ComponentModel;

namespace Jaeger.ValidaSAT.ValueObjects {

    public enum ExisteComprobanteEnum {
        [Description("Sin validar")]
        Sin_Validar = 1,
        [Description("Existe")]
        Existe = 2,
        [Description("No Existe")]
        No_existe = 3
    }
}
