﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Jaeger.ValidaSAT.Contracts;
using Jaeger.ValidaSAT.Entities;

namespace Jaeger.ValidaSAT.Service {
    public class ConsultaRetencionService : IConsultaRetencionService {
        private readonly string UrlService = "https://prodretencionverificacion.clouda.sat.gob.mx/Home/ConsultaRetencion{0}";

        public ConsultaRetencionService() {

        }

        private string Consulta(string url) {
            string end;
            try {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                Stream responseStream = ((HttpWebResponse)httpWebRequest.GetResponse()).GetResponseStream();
                Encoding encoding = Encoding.GetEncoding("utf-8");
                end = (new StreamReader(responseStream, encoding)).ReadToEnd();
            } catch (WebException ex) {
                LogErrorService.LogWrite(string.Concat("Error: ", ex.Message), ex.StackTrace);
                throw ex;
            } catch (Exception ex) {
                LogErrorService.LogWrite(string.Concat("Error: ", ex.Message), ex.StackTrace);
                throw ex;
            }
            return end;
        }

        public ComprobanteRetencionResponse Consulta(ComprobanteRetencionRequest request) {
            var _response = new ComprobanteRetencionResponse(request) {
                IsError = false
            };

            try {
                string _urlService = string.Format(this.UrlService, request.GetElementoImpreso());
                var _tablaResultado = this.Consulta(_urlService);
                // si encontramos id noresultados
                if (_tablaResultado.Contains("noresultados")) {
                    _response.Mensaje = Properties.Resources.msg_NoResultados;
                    _response.Existe = false;
                    return _response;
                }
                // procesar la tabla
                var htmlDataTable = HtmlToDatatableService.GetDataTable(_tablaResultado, "tbl_resultado");
                if (htmlDataTable == null || htmlDataTable.Rows.Count <= 0) {
                    _response.StatusComprobante = "Sin_Validar";
                    _response.Existe = false; 
                    _response.StatusCancelacion = string.Empty;
                    _response.ProcesoCancelacion = string.Empty;
                    _response.Mensaje = string.Empty;
                    return _response;
                } else {
                    _response.Existe = true; 
                    if (htmlDataTable.Rows[0]["Estado CFDI Retención"].ToString().ToLower() != "vigente") {
                        string str1 = htmlDataTable.Rows[0]["Fecha de Cancelación"].ToString();
                        _response.StatusComprobante = "Cancelado"; 
                        _response.FechaCancelacion = new DateTime?(DateTime.Parse(str1));
                    } else {
                        _response.StatusComprobante = "Vigente";
                    }
                    return _response;
                }
            } catch (Exception ex) {
                _response.IsError = true;
                _response.Existe = false; 
                _response.StatusComprobante = "Sin Validar";
                _response.Mensaje = string.Concat("Falla en la conexión al servicio de SAT :", Environment.NewLine, ex.ToString());
            }
            return _response;
        }
    }
}
