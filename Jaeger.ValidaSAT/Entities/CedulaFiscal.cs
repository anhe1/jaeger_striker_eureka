﻿using Jaeger.ValidaSAT.Contracts;

namespace Jaeger.ValidaSAT.Entities {
    public class CedulaFiscal : ICedulaFiscal {
        public enum TipoPersonaEnum {
            Fisica = 1,
            Moral = 2
        }

        public CedulaFiscal() { }

        public CedulaFiscal(string rfc) {
            this.RFC = rfc;
        }

        /// <summary>
        /// obenter o establecer el tipo de persona asociado a la cedula fiscal
        /// </summary>
        public TipoPersonaEnum TipoPersona { get; set; }

        /// <summary>
        /// obtener o establrcer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente sin guiones o espacios
        /// </summary>
        public string RFC { get; set; }

        public IPersonaFisica Fisica { get; 
             set; }

        public IPersonaMoral Moral { get; 
            set; }

        public object Persona {
            get { if (this.TipoPersona == TipoPersonaEnum.Moral)
                    return Moral as IPersonaMoral;
                return Fisica as IPersonaFisica;
            }
        }

        public void Cargar(object persona) {
            if (persona.GetType() == typeof(IPersonaFisica)) {
                this.Fisica = (IPersonaFisica)persona;
            } else if(persona.GetType() == typeof(IPersonaMoral)) {
                this.Moral = (IPersonaMoral)persona;
            }
        }
    }
}
