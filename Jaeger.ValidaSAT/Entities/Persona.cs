﻿using System;
using System.Collections.Generic;
using Jaeger.ValidaSAT.Contracts;

namespace Jaeger.ValidaSAT.Entities {
    public class Persona : DatosUbicacion, IPersona, IPersonaFisica, IPersonaMoral, IDatosUbicacion {
        public Persona() {
            this.Regimenes = new List<RegimenFiscal>();
            this.RegimenFiscal = new RegimenFiscal();
        }
        public Persona(string rfc) { this.RFC = rfc;
            this.Regimenes = new List<RegimenFiscal>();
            this.RegimenFiscal = new RegimenFiscal();
        }

        public string RFC { get; set; }
        public string CURP { get; set; }
        public string Nombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public DateTime? FechaInicio { get; set; }
        public string Situacion { get; set; }
        public DateTime? FechaUltimoCambio { get; set; }
        public List<RegimenFiscal> Regimenes { get; set; }
        public string RegimenCapital { get; set; }
        public DateTime? FechaConstitucion { get; set; }
        public RegimenFiscal RegimenFiscal { get; set; }
    }
}
