﻿namespace Jaeger.ValidaSAT.Entities {
    public class ContribuyenteResponse : ContribuyenteRequest {
        private string _RespuestaValidacion;

        public ContribuyenteResponse() : base() {
        }

        public ContribuyenteResponse(ContribuyenteRequest request) : base() {
            this.RFC = request.RFC;
            this.Registro = request.Registro;
            this.NombreRazonSocial = request.NombreRazonSocial;
            this.CodigoPostal = request.CodigoPostal;
            this.Tag = request.Tag;
        }

        public string Resultado {
            get { return _RespuestaValidacion; }
            set { _RespuestaValidacion = value; }
        }

        public bool IsValid {
            get { if (!string.IsNullOrEmpty(this.Resultado)) {
                    return this.Resultado.Contains("lido, y susceptible de recibir facturas");
                }
                return false;
            }
        }
    }
}
