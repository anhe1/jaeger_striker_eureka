﻿using Jaeger.ValidaSAT.Contracts;
using System.Web.Script.Serialization;
using System.Xml.Serialization;

namespace Jaeger.ValidaSAT.Entities {
    public class ContribuyenteRequest : IContribuyenteRequest {
        public ContribuyenteRequest() {
        }

        [ScriptIgnore]
        [XmlIgnore]
        private int _Registro;

        [ScriptIgnore]
        [XmlIgnore]
        private string _RFC;

        [ScriptIgnore]
        [XmlIgnore]
        private string _NombreRazonSocial;

        [ScriptIgnore]
        [XmlIgnore]
        private string _CodigoPostal;

        [ScriptIgnore]
        [XmlIgnore]
        private object _Tag;

        /// <summary>
        /// obtener o establecer numero consecutivo
        /// </summary>
        public int Registro {
            get { return this._Registro; }
            set { this._Registro = value; }
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// minLength = 12  maxLength = 13
        /// pattern = "[A-Z&amp;Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]"
        /// </summary>
        public string RFC {
            get {
                if (!string.IsNullOrEmpty(this._RFC))
                    return this._RFC.TrimStart(new char[0]).TrimEnd(new char[0]).ToUpper();
                return null;
            }
            set {
                this._RFC = value;
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del contribuyente
        /// </summary>
        public string NombreRazonSocial {
            get {
                return this._NombreRazonSocial;
            }
            set {
                this._NombreRazonSocial = value;
            }
        }

        /// <summary>
        /// obtener o establecer codigo postal del domicilio fiscal del contribuyente
        /// </summary>
        public string CodigoPostal {
            get { return this._CodigoPostal; }
            set { this._CodigoPostal = value; }
        }

        public object Tag {
            get { return this._Tag; }
            set { this._Tag = value; }
        }

        public override string ToString() {
            return string.Concat(new string[] { this.Registro.ToString(), "|", this.RFC, "|", this.NombreRazonSocial, "|", this.CodigoPostal });
        }
    }
}
