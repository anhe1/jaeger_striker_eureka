﻿using Jaeger.ValidaSAT.Contracts;

namespace Jaeger.ValidaSAT.Entities {
    public class ResponseCedula : IResponseCedula {
        public ResponseCedula() {
            this.IsValida = false;
            this.Message = string.Empty;
        }

        /// <summary>
        /// obtener o establecer es el objeto es valido
        /// </summary>
        public bool IsValida { get; set; }

        public CedulaFiscal CedulaFiscal { get; set; }

        public string Message { get; set; }
    }
}
