﻿namespace Jaeger.ValidaSAT.Entities {
    public class RequestCedula : Contracts.IRequestCedula {
        public RequestCedula() { }

        /// <summary>
        /// obtener o establrcer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente sin guiones o espacios
        /// </summary>
        public string RFC { get; set; }

        public string URL { get; set; }

        public string Message { get; set; }
    }
}
