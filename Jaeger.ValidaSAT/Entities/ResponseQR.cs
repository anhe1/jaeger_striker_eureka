﻿namespace Jaeger.ValidaSAT.Entities {
    public class ResponseQR {
        public ResponseQR() {
        }

        /// <summary>
        /// obtener o establecer es el objeto es valido
        /// </summary>
        public bool IsValida { get; set; }

        public string Message { get; set; }
    }
}
