﻿using Jaeger.ValidaSAT.Contracts;

namespace Jaeger.ValidaSAT.Entities {
    public class ComprobanteRetencionRequest : IComprobanteRetencionRequest {
        #region declaraciones
        private string _EmisorRFC;
        private string _ReceptorRFC;
        private string _IdDocumento;
        #endregion

        public ComprobanteRetencionRequest() {

        }

        public ComprobanteRetencionRequest(string emisorRFC, string receptorRFC, string idDocumento) {
            this.EmisorRFC = emisorRFC;
            this.ReceptorRFC = receptorRFC;
            this.IdDocumento = idDocumento;
        }

        /// <summary>
        /// obtener o establecer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente emisor del comprobante.
        /// </summary>
        public string EmisorRFC {
            get { return _EmisorRFC; }
            set { _EmisorRFC = value; }
        }

        /// <summary>
        /// obtener o establecer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente receptor del comprobante.
        /// </summary>
        public string ReceptorRFC {
            get { return _ReceptorRFC; }
            set { _ReceptorRFC = value; }
        }

        /// <summary>
        /// obtener o establer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        public string IdDocumento {
            get { return _IdDocumento; }
            set { _IdDocumento = value; }
        }

        /// <summary>
        /// obtener cadena de URL para el servicio
        /// </summary>
        public string GetElementoImpreso() {
            return string.IsNullOrEmpty(this.IdDocumento) ? "" : string.Format("?folio={0}&rfcEmisor={1}&rfcReceptor={2}", this.IdDocumento.Trim(), this.EmisorRFC.Replace("&", "&amp;").Trim(), this.ReceptorRFC.Replace("&", "&amp;").Trim());
        }
    }
}
