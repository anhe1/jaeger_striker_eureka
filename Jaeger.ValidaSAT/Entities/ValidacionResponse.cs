﻿using System.Collections.Generic;

namespace Jaeger.ValidaSAT.Entities {
    public class ValidacionResponse : IValidacionResponse {
        #region declaraciones
        private string _Mensaje;
        private List<ContribuyenteResponse> _ContribuyenteResponseList;
        #endregion

        public ValidacionResponse() {
            this.Registros = new List<ContribuyenteResponse>();
        }

        /// <summary>
        /// obtener o establecer mensaje de respuesta del servicio
        /// </summary>
        public string Mensaje {
            get { return _Mensaje; }
            set { _Mensaje = value; }
        }

        /// <summary>
        /// obtener o establecer los registros de la validación
        /// </summary>
        public List<ContribuyenteResponse> Registros {
            get { return this._ContribuyenteResponseList; }
            set { this._ContribuyenteResponseList = value; }
        }
    }
}
