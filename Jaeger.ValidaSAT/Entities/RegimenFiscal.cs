﻿using System;

namespace Jaeger.ValidaSAT.Entities {
    public class RegimenFiscal {
        private DateTime? _FechaAlta;
        public RegimenFiscal() {
        }

        public RegimenFiscal(string clave, string descripcion, DateTime? fechaAlta) {
            Clave = clave;
            Descripcion = descripcion;
            FechaAlta = fechaAlta;
        }

        public string Clave { get; set; }

        public DateTime? FechaAlta {
            get {
                if (this._FechaAlta >= new DateTime(1800, 1, 1))
                    return this._FechaAlta;
                return null;
            }
            set {
                this._FechaAlta = value;
            }
        }

        public string Descripcion { get; set; }
    }
}
