﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.StrikerEureka.Log.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.StrikerEureka.Log.Contracts {
    public interface ISqlCFDResponseValidacionRepository : IGenericRepository<CFDResponseValidacionModel> {
        void CrearRepositorio();

        IEnumerable<CFDResponseValidacionModel> GetList(string rfc, string comprobado = "", int cantidad = 100);

        CFDResponseValidacionModel Save(CFDResponseValidacionModel item);
    }
}
