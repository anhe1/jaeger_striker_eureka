﻿using System;

namespace Jaeger.Domain.StrikerEureka.Log.Contracts {
    public interface ICFDResponseValidacionModel {
        /// <summary>
        /// obtener ó establecer el indice de la tabla.
        /// </summary>
        int Id {
            get; set;
        }


        int IdComprobante {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio
        /// </summary>
        int IdDirectorio {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la version del comprobante
        /// </summary>
        string Version {
            get; set;
        }

        /// <summary>
        /// obtener o establecer tipo de archivo
        /// </summary>
        int MetaData {
            get; set;
        }

        string TipoComprobante {
            get; set;
        }

        string EstadoSAT {
            get; set;
        }

        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        string IdDocumento {
            get;set;
        }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        string Folio {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        string Serie {
            get; set;
        }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>

        string EmisorRFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el nombre, denominación o razón social del contribuyente emisor del comprobante.
        /// </summary>
        string Emisor {
            get; set;
        }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        string ReceptorRFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el nombre, denominación o razón social del contribuyente receptor del comprobante.
        /// </summary>
        string Receptor {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        DateTime? FechaEmision {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la fecha de certificacion del comprobante
        /// </summary>
        DateTime? FechaCertificacion {
            get; set;
        }

        DateTime? FechaValidacion {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el número de serie del certificado de sello digital que ampara al comprobante, de acuerdo con el acuse correspondiente a 20 posiciones otorgado por el sistema del SAT.
        /// </summary>
        string NoCertificado {
            get; set;
        }

        string NoCertificadoProvCertif {
            get; set;
        }

        string RFCProvCertif {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el código postal del lugar de expedición del comprobante (domicilio de la matriz o de la sucursal).
        /// </summary>
        string LugarExpedicion {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.
        /// </summary>
        string MetodoPago {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.
        /// </summary>
        string FormaPago {
            get; set;
        }

        /// <summary>
        /// obtener o establcer la clave del uso que dará a esta factura el receptor del CFDI.
        /// </summary>
        string UsoCFDI {
            get; set;
        }

        decimal SubTotal {
            get; set;
        }

        decimal Descuento {
            get; set;
        }

        decimal Total {
            get; set;
        }

        string JValidacion {
            get; set;
        }

        bool Registrado {
            get; set;
        }

        string Situacion {
            get; set;
        }

        string Resultado {
            get; set;
        }

        string Comprobado {
            get; set;
        }

        /// <summary>
        /// obtener keyname
        /// </summary>
        string KeyName {
            get;
        }

        /// <summary>
        /// obtener ruta completa del archivo XML
        /// </summary>
        string XML {
            get; set;
        }

        /// <summary>
        /// obtener ruta completa del archivo PDF
        /// </summary>
        string PDF {
            get; set;
        }
    }
}
