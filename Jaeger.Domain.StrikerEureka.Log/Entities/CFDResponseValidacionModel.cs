﻿using System;
using SqlSugar;
using Jaeger.Domain.StrikerEureka.Log.Contracts;

namespace Jaeger.Domain.StrikerEureka.Log.Entities {
    public class CFDResponseValidacionModel : ICFDResponseValidacionModel {
        private string idDocumento;
        private DateTime? fechaEmision;
        private DateTime? fechaCertificacion;
        private DateTime? fechaValidacion;

        public CFDResponseValidacionModel() {
        }

        /// <summary>
        /// obtener ó establecer el indice de la tabla.
        /// </summary>
        [SugarColumn(ColumnDescription = "indice de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int Id {
            get; set;
        }

        [SugarColumn(ColumnDescription = "id del comprobante", IsNullable = true)]
        public int IdComprobante {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio
        /// </summary>
        [SugarColumn(ColumnDescription = "id del directorio", IsNullable = true)]
        public int IdDirectorio {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la version del comprobante
        /// </summary>
        [SugarColumn(ColumnDescription = "version del comprobante fiscal", IsNullable = true, Length = 3)]
        public string Version {
            get; set;
        }

        /// <summary>
        /// obtener o establecer tipo de archivo
        /// </summary>
        public int MetaData {
            get; set;
        }

        [SugarColumn(ColumnDescription = "para expresar el efecto del comprobante fiscal para el contribuyente emisor. Ingreso/egreso/traslado", Length = 10, IsNullable = true)]
        public string TipoComprobante {
            get; set;
        }

        [SugarColumn(ColumnDescription = "estado del comprobante (correcto,error!,cancelado,vigente)", Length = 25, IsNullable = true)]
        public string EstadoSAT {
            get; set;
        }

        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        [SugarColumn(ColumnDescription = "36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122", IsNullable = true, Length = 36)]
        public string IdDocumento {
            get {
                return this.idDocumento;
            }
            set {
                if (value == null)
                    this.idDocumento = DateTime.Now.ToString();
                else
                    this.idDocumento = value.ToUpper();
            }
        }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        [SugarColumn(ColumnDescription = "atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.", Length = 21)]
        public string Folio {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        [SugarColumn(ColumnDescription = "serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres.", IsNullable = true, Length = 25)]
        public string Serie {
            get; set;
        }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        [SugarColumn(ColumnDescription = "rfc del emisor del comprobante", Length = 16, IsNullable = true)]
        public string EmisorRFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el nombre, denominación o razón social del contribuyente emisor del comprobante.
        /// </summary>
        [SugarColumn(ColumnDescription = "identidad fiscal del receptor o emisor del comprobante fiscal", Length = 255, IsNullable = true)]
        public string Emisor {
            get; set;
        }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        [SugarColumn(ColumnDescription = "registro federal del contribuyentes del receptor del comprobante", Length = 16)]
        public string ReceptorRFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el nombre, denominación o razón social del contribuyente receptor del comprobante.
        /// </summary>
        [SugarColumn(ColumnDescription = "nombre del receptor del comprobante", Length = 255)]
        public string Receptor {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        [SugarColumn(ColumnDescription = "fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.")]
        public DateTime? FechaEmision {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEmision > firstGoodDate)
                    return this.fechaEmision;
                return null;
            }
            set {
                this.fechaEmision = value;
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de certificacion del comprobante
        /// </summary>
        [SugarColumn(ColumnDescription = "fecha de certificacion", IsNullable = true)]
        public DateTime? FechaCertificacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCertificacion > firstGoodDate)
                    return this.fechaCertificacion;
                return null;
            }
            set {
                this.fechaCertificacion = value;
            }
        }

        [SugarColumn(ColumnDescription = "fecha de certificacion", IsNullable = true)]
        public DateTime? FechaValidacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaValidacion > firstGoodDate)
                    return this.fechaValidacion;
                return null;
            }
            set {
                this.fechaValidacion = value;
            }
        }

        /// <summary>
        /// obtener o establecer el número de serie del certificado de sello digital que ampara al comprobante, de acuerdo con el acuse correspondiente a 20 posiciones otorgado por el sistema del SAT.
        /// </summary>
        [SugarColumn(ColumnDescription = "numero de certificado del emisor del comprobante", IsNullable = true)]
        public string NoCertificado {
            get; set;
        }

        [SugarColumn(ColumnDescription = "representa el rfc del pac que certifica el comprobante", IsNullable = true)]
        public string NoCertificadoProvCertif {
            get; set;
        }

        [SugarColumn(ColumnDescription = "representa el rfc del pac que certifica el comprobante", IsNullable = true)]
        public string RFCProvCertif {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el código postal del lugar de expedición del comprobante (domicilio de la matriz o de la sucursal).
        /// </summary>
        [SugarColumn(ColumnDescription = "indicar llugar de expedición del comprobante fiscal", Length = 50, IsNullable = true)]
        public string LugarExpedicion {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.
        /// </summary>
        [SugarColumn(ColumnDescription = "Atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.", Length = 3)]
        public string MetodoPago {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.
        /// </summary>
        [SugarColumn(ColumnDescription = "obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.", Length = 2)]
        public string FormaPago {
            get; set;
        }

        /// <summary>
        /// obtener o establcer la clave del uso que dará a esta factura el receptor del CFDI.
        /// </summary>
        [SugarColumn(ColumnDescription = "atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.", Length = 3)]
        public string UsoCFDI {
            get; set;
        }

        [SugarColumn(ColumnDescription = "representar la suma de los importes antes de descuentos e impuestos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal SubTotal {
            get; set;
        }

        [SugarColumn(ColumnDescription = "para representar el importe de los descuentos aplicables antes de impuestos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal Descuento {
            get; set;
        }

        [SugarColumn(ColumnDescription = "representa la suma del subtotal, menos los descuentos aplicables, mas los impuestos, menos los impuestos retenidos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal Total {
            get; set;
        }

        [SugarColumn(ColumnDescription = "validación del comprobante fiscal", ColumnDataType = "Text")]
        public string JValidacion {
            get; set;
        }

        public bool Registrado {
            get; set;
        }

        [SugarColumn(ColumnDescription = "resultado de la descarga del comprobante", Length = 25)]
        public string Situacion {
            get; set;
        }

        [SugarColumn(ColumnDescription = "resultado de la descarga del comprobante", Length = 25)]
        public string Resultado {
            get; set;
        }

        [SugarColumn(ColumnDescription = "comprobacion", Length = 25)]
        public string Comprobado {
            get; set;
        }

        /// <summary>
        /// obtener keyname
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string KeyName {
            get {
                string[] emisorRfc;
                if (this.FechaCertificacion != null) {
                    emisorRfc = new string[] { "CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.IdDocumento, "-", this.FechaCertificacion.Value.ToString("yyyyMMddHHmmss") };
                } else if (this.FechaEmision != null) {
                    emisorRfc = new string[] { "CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.IdDocumento, "-", this.FechaEmision.Value.ToString("yyyyMMddHHmmss") };
                } else {
                    emisorRfc = new string[] { "CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.IdDocumento, "-00000000000000" };
                }
                return string.Concat(emisorRfc);
            }
        }

        /// <summary>
        /// obtener ruta completa del archivo XML
        /// </summary>
        [SugarColumn(ColumnDescription = "XML Completo en formato JSON (Al guardar en S3 se borra)", ColumnDataType = "TEXT", IsNullable = true)]
        public string XML {
            get; set;
        }

        /// <summary>
        /// obtener ruta completa del archivo PDF
        /// </summary>
        [SugarColumn(ColumnDescription = "XML Completo en formato JSON (Al guardar en S3 se borra)", ColumnDataType = "TEXT", IsNullable = true)]
        public string PDF {
            get; set;
        }
    }
}
