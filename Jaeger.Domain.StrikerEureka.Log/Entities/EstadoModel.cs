﻿namespace Jaeger.Domain.StrikerEureka.Log.Entities {
    public class EstadoModel : Base.Abstractions.BaseSingleModel {
        public EstadoModel() {
        }

        public EstadoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
