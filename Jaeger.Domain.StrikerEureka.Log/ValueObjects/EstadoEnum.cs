﻿using System.ComponentModel;

namespace Jaeger.Domain.StrikerEureka.Log.ValueObjects {
    public enum EstadoEnum {
        [Description("Todo")]
        Todo = 0,
        [Description("No Verificado")]
        NoVerificado = 1,
        [Description("Verificado")]
        Verificado
    }
}
