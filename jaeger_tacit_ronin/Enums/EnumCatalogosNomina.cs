using System.ComponentModel;

namespace Jaeger.Enums
{
    public enum EnumCatalogosNomina
    {
        [Description("Bancos")]
        Bancos,
        [Description("Codigos postales.")]
        CodigoPostal,
        [Description("Estados")]
        Estados,
        [Description("Tipo de origen recurso.")]
        OrigenRecurso, 
        [Description("Tipos de periodicidad del pago.")]
        PeriodicidadPago,
        [Description("Régimen fiscal.")]
        RegimenFiscal,
        [Description("Tipos de Deducciones.")]
        TipoDeduccion,
        [Description("Tipos de Hora Extra.")]
        TipoHoras,
        [Description("Tipo de Incapacidad.")]
        TipoIncapacidad,
        [Description("Tipos de jornada laboral.")]
        TipoJornada,
        [Description("Tipos de nómina.")]
        TipoNomina,
        [Description("Otro tipo de pago.")]
        TipoOtroPago,
        [Description("Tipos de percepciones.")]
        TipoPercepcion,
        [Description("Tipos de régimen de contratación.")]
        TipoRegimen,
        [Description("Clases en que deben inscribirse lo patrones.")]
        RiesgoPuesto
    }
}