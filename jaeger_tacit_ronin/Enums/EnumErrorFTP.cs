﻿using System;

namespace Jaeger.Enums
{
    public enum EnumErrorFTP
    {
        NotError,
        NotNetworkAvailable,
        NotFtpAvailable,
        FileNotFound,
        Unknown
    }
}
