using System.ComponentModel;

namespace Jaeger.Enums
{
    public enum EnumCatalogosPagos
    {
        [Description("Formas de Pago")]
        FormaPago,
        [Description("Metodo de Pago")]
        MetodoPago,
        [Description("Monedas")]
        Moneda,
        [Description("Tasa O Cuota")]
        TasaOCuota,
        [Description("Tipo de Cadena de Pago")]
        TipoCadenaPago,
        [Description("Tipo de Factor")]
        TipoFactor
    }
}