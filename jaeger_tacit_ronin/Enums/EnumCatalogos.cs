﻿using System.ComponentModel;

namespace Jaeger.Enums
{
    public enum EnumCatalogosCFDI
    {
        [Description("Catálogo de aduanas (tomado del anexo 22, apéndice I de la RGCE 2017).")]
        Aduanas,
        [Description("Formas de Pago")]
        FormaPago,
        [Description("Metodo de Pago")]
        MetodoPago,
        [Description("Monedas")]
        Moneda,
        [Description("Codigos postales")]
        CodigoPostal,
        [Description("Bancos")]
        Bancos,
        [Description("Certificados (CSD)")]
        Certificados,
        [Description("Productos y Servicios")]
        ProdServ,
        [Description("Tipos de relación entre CFDI")]
        RelacionCFDI,
        [Description("Uso de comprobantes")]
        UsoCFDI,
        [Description("Unidades de medida para los conceptos en el CFDI.")]
        Unidades,
        [Description("Catálogo de impuestos.")]
        ImpuestosCFDI,
        [Description("Catálogo de números de pedimento operados por aduana y ejercicio.")]
        NumPedimentoAduana,
        [Description("Países")]
        Paises,
        [Description("Patentes aduanales")]
        PatenteAduanal,
        [Description("Régimen Fiscal")]
        RegimenFiscal,
        [Description("Tasas o cuotas de impuestos.")]
        TasaOCuota,
        [Description("Tipos de comprobante.")]
        TipoDeComprobante,
        [Description("Tipo Factor")]
        TipoFactor,
    }
}
