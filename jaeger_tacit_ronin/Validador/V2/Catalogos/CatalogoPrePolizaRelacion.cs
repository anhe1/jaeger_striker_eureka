﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.Helpers;

namespace Jaeger.Validador.V2.Catalogos
{
    public class CatalogoPrePolizaRelacion : HelperCatalogo
    {
        private List<string> tiposRelacionField;

        public CatalogoPrePolizaRelacion()
        {
            this.tiposRelacionField = new List<string>();
        }

        public List<string> TiposRelacion
        {
            get
            {
                return this.tiposRelacionField;
            }
            set
            {
                this.tiposRelacionField = value;
                this.OnPropertyChanged();
            }
        }

        public CatalogoPrePolizaRelacion Load()
        {
            this.tiposRelacionField = new List<string>();
            this.tiposRelacionField.Add("01 Sustitución por Cancelación");
            this.tiposRelacionField.Add("02 Sustitución por Error");
            this.tiposRelacionField.Add("03 Cancela documento previo");
            return this;
        }
    }
}
