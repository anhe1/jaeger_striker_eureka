﻿using System;
using System.Text.RegularExpressions;

namespace Jaeger.Validador.V2
{
    public class HelperCfdi
    {
        public HelperCfdi()
        {
        }

        public static bool IsValidXml(string xmlContent)
        {
            MatchCollection c = (new Regex("cfdi:Comprobante", RegexOptions.Compiled)).Matches(xmlContent);
            if (0 < c.Count)
            {
                foreach (Match item in c)
                {
                    string str = item.ToString();
                    if (str.ToLower().Contains("cfdi:comprobante"))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static string VersionCfdi(string rutaXml)
        {
            return HelperCfdi.LeerAtributoXml(rutaXml, "Comprobante", "version", "");
        }

        public static string LeerAtributoXml(string strXml, string strNodo, string strAtributo, string valorDefault)
        {
            string str;

            try
            {
                string str1 = null;
                string str2 = strXml;
                int num = 0;
                int num1 = 0;
                int num2 = 0;
                string str3 = string.Concat(strAtributo, "=\"");
                if (!string.IsNullOrEmpty(strNodo))
                {
                    num2 = str2.IndexOf(string.Concat(":", strNodo));
                }
                if (num2 < 0)
                {
                    str = "";
                }
                else
                {
                    num = checked(str2.IndexOf(str3, num2) + str3.Length);
                    num1 = str2.IndexOf("\"", num);
                    str1 = str2.Substring(num, checked(num1 - num));
                    GC.Collect();
                    str = (num != checked(str3.Length - 1) ? str1 : "");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                str = "";
            }
            return str;
        }
    }
}
