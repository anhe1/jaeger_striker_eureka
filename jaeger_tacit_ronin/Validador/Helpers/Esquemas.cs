﻿using System;
using System.Xml;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Xml.Schema;

namespace Jaeger.Validador.Helpers
{
    public class Esquemas
    {
        private readonly XmlReaderSettings xmlReaderSetting32;
        private readonly XmlReaderSettings xmlReaderSetting33;
        private readonly XmlReaderSettings xmlReaderSetting40;

        public Esquemas()
        {
            xmlReaderSetting32 = new XmlReaderSettings();
            xmlReaderSetting33 = new XmlReaderSettings();
            xmlReaderSetting40 = new XmlReaderSettings();
        }

        public void Init()
        {
            this.xmlReaderSetting32.Schemas.Add("http://www.sat.gob.mx/cfd/3", (new GetXsd("cfdv32.xsd")).XmlReader);
            this.xmlReaderSetting33.Schemas.Add("http://www.sat.gob.mx/cfd/3", (new GetXsd("cfdv33.xsd")).XmlReader);
            this.xmlReaderSetting40.Schemas.Add("http://www.sat.gob.mx/cfd/4", (new GetXsd("cfdv40.xsd")).XmlReader);
        }

        public bool Schema32(CFDI.V32.Comprobante objeto)
        {
            bool flag = false;
            try
            {
                this.xmlReaderSetting32.Schemas.Compile();
                using (XmlReader xmlReader = XmlReader.Create(new StringReader(objeto.OriginalXmlString), this.xmlReaderSetting32))
                {
                    while (xmlReader.Read())
                    {
                    }
                }
                flag = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return flag;
        }

        public bool Schema33(CFDI.V33.Comprobante objeto)
        {
            bool flag = false;
            try
            {
                this.xmlReaderSetting33.Schemas.Compile();
                using (XmlReader xmlReader = XmlReader.Create(new StringReader(objeto.OriginalXmlString), this.xmlReaderSetting32))
                {
                    while (xmlReader.Read())
                    {
                    }
                }
                flag = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return flag;
        }

        public bool Schema40(CFDI.V40.Comprobante objeto) {
            bool flag = false;
            try {
                this.xmlReaderSetting33.Schemas.Compile();
                using (XmlReader xmlReader = XmlReader.Create(new StringReader(objeto.OriginalXmlString), this.xmlReaderSetting40)) {
                    while (xmlReader.Read()) {
                    }
                }
                flag = true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return flag;
        }

        //public bool Valid(object objeto)
        //{
        //    bool valid = false;
        //    return valid;
        //}

        public bool ValidarEsquema(CFDI.V32.Comprobante oValidateComprobante)
        {
            
            bool flag = false;
            try
            {
                XmlReaderSettings xmlReaderSetting = new XmlReaderSettings();
                Jaeger.Validador.Helpers.Esquemas validateSchema = this;
                xmlReaderSetting.ValidationEventHandler += new ValidationEventHandler(validateSchema.ValidationEventHandler);
                xmlReaderSetting.ValidationType = ValidationType.Schema;
                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/cfd/3"))
                {
                    if (oValidateComprobante.version != "3.0")
                    {
                        xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/cfd/3", (new GetXsd("cfdv3.xsd")).XmlReader);
                    }
                    else
                    {
                        xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/cfd/3", (new GetXsd("cfdv32.xsd")).XmlReader);
                    }
                    //this.LiErrores.Add("El atributo schemaLocation no existe. Se agrego para la validación.");
                }
                if ((oValidateComprobante.Complemento == null || oValidateComprobante.Complemento.Any == null ? false : true))
                {
                    List<XmlElement> any = oValidateComprobante.Complemento.Any;
                    int count = checked(any.Count - 1);
                    for (int i = 0; i <= count; i = checked(i + 1))
                    {
                        string lower = any[i].Name.ToLower();
                        string str = lower;
                        if (lower != null)
                        {
                            string str1 = str;
                            if (str1 == "detallista:detallista")
                            {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/detallista"))
                                {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/detallista", (new GetXsd("detallista.xsd")).XmlReader);
                                }
                            }
                            else if (str1 == "divisas:divisas")
                            {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/divisas"))
                                {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/divisas", (new GetXsd("Divisas.xsd")).XmlReader);
                                }
                            }
                            else if (str1 == "implocal:impuestoslocales")
                            {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/implocal"))
                                {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/implocal", (new GetXsd("implocal.xsd")).XmlReader);
                                }
                            }
                            else if (str1 == "ecc:estadodecuentacombustible")
                            {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/ecc"))
                                {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/ecc", (new GetXsd("ecc.xsd")).XmlReader);
                                }
                            }
                            else if (str1 == "ecb:estadodecuentabancario")
                            {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/ecb"))
                                {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/ecb", (new GetXsd("ecb.xsd")).XmlReader);
                                }
                            }
                            else if (str1 == "tfd:timbrefiscaldigital")
                            {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/TimbreFiscalDigital"))
                                {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/TimbreFiscalDigital", (new GetXsd("TimbreFiscalDigital.xsd")).XmlReader);
                                }
                            }
                            else if (str1 == "donat:donatarias")
                            {
                                if (oValidateComprobante.version != "3.0")
                                {
                                    if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/donat"))
                                    {
                                        xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/donat", (new GetXsd("donat11.xsd")).XmlReader);
                                    }
                                }
                                else if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/donat"))
                                {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/donat", (new GetXsd("donat.xsd")).XmlReader);
                                }
                            }
                            else if (str1 == "leyendasfisc:leyendasfiscales")
                            {
                                if (oValidateComprobante.version != "3.2" || xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/leyendasFiscales"))
                                {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/leyendasFiscales", (new GetXsd("leyendasFisc.xsd")).XmlReader);
                                }
                            }
                            else if (str1 == "pfic:pfintegrantecoordinado")
                            {
                                if (oValidateComprobante.version != "3.2" || xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/pfic"))
                                {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/pfic", (new GetXsd("pfic.xsd")).XmlReader);
                                }
                            }
                            else if (str1 == "tpe:turistapasajeroextranjero")
                            {
                                if (oValidateComprobante.version != "3.2" || xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/TuristaPasajeroExtranjero"))
                                {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/TuristaPasajeroExtranjero", (new GetXsd("TuristaPasajeroExtranjero.xsd")).XmlReader);
                                }
                            }
                            else if (str1 == "nomina:nomina")
                            {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/nomina"))
                                {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/nomina", (new GetXsd("nomina11.xsd")).XmlReader);
                                }
                            }
                            else if (str1 == "aerolineas:aerolineas")
                            {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/aerolineas"))
                                {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/aerolineas", (new GetXsd("aerolineas.xsd")).XmlReader);
                                }
                            }
                            else if (str1 == "consumodecombustibles:consumodecombustibles")
                            {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/consumodecombustibles"))
                                {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/consumodecombustibles", (new GetXsd("consumodecombustibles.xsd")).XmlReader);
                                }
                            }
                            else if (str1 == "valesdedespensa:valesdedespensa")
                            {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/valesdedespensa"))
                                {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/valesdedespensa", (new GetXsd("valesdedespensa.xsd")).XmlReader);
                                }
                            }
                            else if (str1 == "notariospublicos:notariospublicos")
                            {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/notariospublicos"))
                                {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/notariospublicos", (new GetXsd("notariospublicos.xsd")).XmlReader);
                                }
                            }
                        }
                    }
                }
                xmlReaderSetting.Schemas.Compile();
                Jaeger.Validador.Helpers.Esquemas validateSchema1 = this;
                xmlReaderSetting.ValidationEventHandler += new ValidationEventHandler(validateSchema1.ValidationEventHandler);
                using (XmlReader xmlReader = XmlReader.Create(new StringReader(oValidateComprobante.OriginalXmlString), xmlReaderSetting))
                {
                    while (xmlReader.Read())
                    {
                    }
                }
                flag = true;
            }
            catch (Exception ex)
            {
                //this.LiErrores.Add(string.Concat("|", ex.Message));
                Console.WriteLine(ex.Message);
            }
            return flag;
        }

        //private void ValidationCallBack(object sender, ValidationEventArgs e)
        //{
        //    if (e.Severity == XmlSeverityType.Warning)
        //    {
        //        //this.LiErrores.Add(string.Concat("Aviso: ", e.Message, "IEC1001"));
        //    }
        //    if (e.Severity == XmlSeverityType.Error)
        //    {
        //        //this.LiErrores.Add(string.Concat("Error de Validacion contra XSD: ", e.Message, "IEC1000"));
        //    }
        //}

        private void ValidationEventHandler(object sender, ValidationEventArgs validationEventArgs)
        {
            switch (validationEventArgs.Severity)
            {
                case XmlSeverityType.Error:
                {
                    Console.WriteLine(validationEventArgs.Message);
                    //this.LiErrores.Add(string.Concat("Error: ", validationEventArgs.Message));
                    break;
                }
                case XmlSeverityType.Warning:
                {
                    Console.WriteLine(validationEventArgs.Message);
                    //this.LiErrores.Add(string.Concat("Warning: ", validationEventArgs.Message));
                    break;
                }
            }
        }
    }
}
