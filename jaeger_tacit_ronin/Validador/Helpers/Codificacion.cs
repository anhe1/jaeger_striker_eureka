﻿using Jaeger.Edita.V2.Validador.Entities;
using System;
using System.IO;
using System.Text;

namespace Jaeger.Validador.Helpers
{
    public class Codificacion
    {
        public Codificacion()
        {
        }

        public static Encoding DetectEncoding(string ruta)
        {
            Encoding utf8;
            try
            {
                Encoding @default = null;
                FileStream fileStream = new FileStream(ruta, FileMode.Open, FileAccess.Read, FileShare.Read);
                if (!fileStream.CanSeek)
                {
                    @default = Encoding.Default;
                }
                else
                {
                    byte[] numArray = new byte[4];
                    fileStream.Read(numArray, 0, 4);
                    @default = ((numArray[0] != 239 || numArray[1] != 187 || numArray[2] != 191) && (numArray[0] != 255 || numArray[1] != 254) && (numArray[0] != 254 || numArray[1] != 255) && (numArray[0] != 0 || numArray[1] != 0 || numArray[2] != 254 || numArray[3] != 255) ? Encoding.Default : Encoding.UTF8);
                    fileStream.Seek((long)0, SeekOrigin.Begin);
                }
                byte[] numArray1 = new byte[4096];
                while (fileStream.Read(numArray1, 0, 4096) != 0)
                {
                    @default.GetString(numArray1);
                }
                Console.WriteLine();
                fileStream.Close();
                utf8 = @default;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                utf8 = Encoding.UTF8;
            }
            return utf8;
        }

        public static ValidationProperty ValidateUTF8Encode(byte[] byteComprobant)
        {
            long num;
            ValidationProperty validateUtf8Encode;
            ValidationProperty validationProperty = new ValidationProperty()
            {
                Name = Resources.ValidadorResource.Propiedad_Codificacion
            };
            ValidationProperty oResult = validationProperty;
            Encoding encoding = Codificacion.DetectBomBytes(byteComprobant);
            MemoryStream memoryStream = new MemoryStream(byteComprobant);
            memoryStream.Seek((long)0, SeekOrigin.Begin);
            StreamReader streamReader = new StreamReader(memoryStream);
            string end = streamReader.ReadToEnd();
            Encoding currentEncoding = streamReader.CurrentEncoding;
            if (encoding == Encoding.UTF8)
            {
                oResult.Value = Resources.ValidadorResource.CFDI_Codificacion_Valida;
                oResult.Valid = true;
                validateUtf8Encode = oResult;
            }
            else if (currentEncoding != Encoding.UTF8)
            {
                oResult.Value = string.Concat(Resources.ValidadorResource.CFDI_Codificacion_Valida, " ", encoding.EncodingName, encoding.BodyName);
                oResult.Valid = true;
                validateUtf8Encode = oResult;
            }
            else if (!Codificacion.IsUtf8ValidCharacters(end, out num))
            {
                oResult.Value = string.Format(Resources.ValidadorResource.CFDI_Codificacion_No_Valida, encoding.EncodingName, num);
                oResult.Valid = false;
                validateUtf8Encode = oResult;
            }
            else
            {
                oResult.Value = Resources.ValidadorResource.CFDI_Codificacion_Valida;
                oResult.Valid = true;
                validateUtf8Encode = oResult;
            }
            return validateUtf8Encode;
        }

        public static Encoding DetectBomBytes(byte[] bomBytes)
        {
            if (bomBytes == null)
            {
                throw new ArgumentNullException("Must provide a valid BOM byte array!", "BOMBytes");
            }

            if (bomBytes.Length < 2)
            {
                return null;
            }

            if (bomBytes[0] == 255 && bomBytes[1] == 254 && ((bomBytes.Length) < 4 || bomBytes[2] != 0 || bomBytes[3] != 0))
            {
                return Encoding.Unicode;
            }

            if (bomBytes[0] == 254 && bomBytes[1] == 255)
            {
                return Encoding.BigEndianUnicode;
            }

            if (bomBytes.Length < 3)
            {
                return null;
            }

            if (bomBytes[0] == 239 && bomBytes[1] == 187 && bomBytes[2] == 191)
            {
                return Encoding.UTF8;
            }

            if (bomBytes[0] == 43 && bomBytes[1] == 47 && bomBytes[2] == 118)
            {
                return Encoding.UTF7;
            }

            if (bomBytes.Length < 4)
            {
                return null;
            }

            if (bomBytes[0] == 255 && bomBytes[1] == 254 && bomBytes[2] == 0 && bomBytes[3] == 0)
            {
                return Encoding.UTF32;
            }

            if (bomBytes[0] != 0 || bomBytes[1] != 0 || bomBytes[2] != 254 || bomBytes[3] != 255)
            {
                return Encoding.ASCII;
            }

            return Encoding.GetEncoding(12001);
        }

        public static bool IsUtf8ValidCharacters(string inString, out long indexInvalid)
        {
            bool isUtf8ValidCharacters;
            indexInvalid = -1;
            if (inString != null)
            {
                int num = 0;
                do
                {
                    char chr = inString[num];
                    if (((chr >= 'ý' || chr <= '\u001F') && chr != '\t' && chr != '\n' && chr != '\r' ? false : true))
                    {
                        num = checked(num + 1);
                    }
                    else
                    {
                        indexInvalid = num;
                        isUtf8ValidCharacters = false;
                        return isUtf8ValidCharacters;
                    }
                }
                while (num < inString.Length);
                isUtf8ValidCharacters = true;
            }
            else
            {
                isUtf8ValidCharacters = true;
            }
            return isUtf8ValidCharacters;
        }
    }
}