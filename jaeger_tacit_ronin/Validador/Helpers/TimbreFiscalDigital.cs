﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Security.Cryptography.X509Certificates;
using Jaeger.Enums;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Services;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.Validador.Helpers
{
    public class TimbreFiscalDigital
    {

        public const string SelloSAT = "Sello SAT";

        public bool IsTimbreValido(string xml, string ruta)
        {
            XmlNamespaceManager xmlNamespaceManagers;
            XmlParserContext xmlParserContext;
            XmlReader xmlReader;
            XmlSchemaSet xmlSchemaSet = new XmlSchemaSet();
            XmlReaderSettings xmlReaderSetting = new XmlReaderSettings();
            try
            {
                try
                {
                    xmlSchemaSet.Add("http://www.sat.gob.mx/TimbreFiscalDigital", XmlReader.Create(new StreamReader(ruta)));
                    xmlReaderSetting.Schemas = xmlSchemaSet;
                    xmlReaderSetting.ValidationType = ValidationType.Schema;
                    xmlNamespaceManagers = new XmlNamespaceManager(new NameTable());
                    xmlNamespaceManagers.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                    xmlParserContext = new XmlParserContext(null, xmlNamespaceManagers, null, XmlSpace.None);
                    xmlReaderSetting.ConformanceLevel = ConformanceLevel.Fragment;
                    xmlReader = XmlReader.Create(new StringReader(xml), xmlReaderSetting, xmlParserContext);
                    while (xmlReader.Read())
                    {
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Concat("No se pudo validar el Timbre con las especificaciones del SAT: ", ex.Message.ToString()));
                }
            }
            finally
            {
                xmlReaderSetting = null;
                xmlReader = null;
                xmlSchemaSet = null;
                xmlNamespaceManagers = null;
                xmlParserContext = null;
            }
            return true;
        }

        /// <summary>
        /// validar complemento TimbreFiscalDigital v1.0, para cfdi v3.2
        /// </summary>
        public static ValidationProperty Validar(CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital objeto, ref CertificadosCatalogo catalogo)
        {
            ValidationProperty respuesta = new ValidationProperty() { Name = TimbreFiscalDigital.SelloSAT, Valid = true, Value = Resources.ValidadorResource.Propiedad_Valido };

            if (!(objeto == null))
            {
                Certificate certificado = catalogo.Search(objeto.noCertificadoSAT);
                string cadenaOriginal = objeto.CadenaOriginal;
                X509Certificate2 oX509Certificate2 = new X509Certificate2(Convert.FromBase64String(certificado.CerB64));
                if (!CryptoService.ValidaSello(objeto.selloSAT, cadenaOriginal, oX509Certificate2, "SHA251"))
                {
                    respuesta.Valid = false;
                    respuesta.Value = "No válido";
                }
            }
            else
            {
                respuesta.Value = Resources.ValidadorResource.TFD_Not_Found;
                respuesta.Valid = false;
            }
            return respuesta;
        }

        /// <summary>
        /// validar complemento TimbreFiscalDigital v1.1, para cfdi v3.3
        /// </summary>
        public static ValidationProperty Validar(CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital objeto, ref CertificadosCatalogo catalogo)
        {
            ValidationProperty respuesta = new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_Sello_SAT, Valid = true, Value = Resources.ValidadorResource.Propiedad_Valido };

            if (!(objeto == null))
            {
                Certificate certificado = catalogo.Search(objeto.NoCertificadoSAT);
                if (certificado.CerB64 != string.Empty)
                {
                    string cadenaOriginal = objeto.CadenaOriginal;
                    X509Certificate2 oX509Certificate2 = new X509Certificate2(Convert.FromBase64String(certificado.CerB64));
                    if (!CryptoService.ValidaSello(objeto.SelloSAT, cadenaOriginal, oX509Certificate2, "SHA256"))
                    {
                        respuesta.Valid = false;
                        respuesta.Value = Resources.ValidadorResource.Propiedad_No_Valido;
                    }
                }
                else
                {
                    respuesta.Value = "No se puede validar por falta de certificado";
                }
            }
            else
            {
                respuesta.Value = Resources.ValidadorResource.TFD_Not_Found;
                respuesta.Valid = false;
            }
            return respuesta;
        }

        /// <summary>
        /// validar estructura del complemento TimbreFiscalDigital
        /// </summary>
        public static List<ValidationProperty> ValidarEstructuraNamespaceTFD(Jaeger.CFDI.V32.Comprobante objeto)
        {
            List<ValidationProperty> lista = new List<ValidationProperty>();
            XmlElement xmlElement = objeto.Complemento.Any.ToList<XmlElement>().SingleOrDefault<XmlElement>((XmlElement p) => p.LocalName.ToLower().Contains("timbrefiscaldigital"));
            if (xmlElement.NamespaceURI != "http://www.sat.gob.mx/TimbreFiscalDigital" || xmlElement.Prefix != "tfd")
            {
                if (xmlElement.NamespaceURI == "http://www.sat.gob.mx/TimbreFiscalDigital")
                {
                    lista.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Namespace", Value = "Correcto", Valid = true });
                }
                else
                {
                    lista.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Namespace", Value = "No válido", Valid = false });
                }

                if (xmlElement.Prefix == "tfd")
                {
                    lista.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Prefijo", Value = "Correcto", Valid = true });
                }
                else
                {
                    lista.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Prefijo", Value = "Sin prefijo tfd del namespace", Valid = false });
                }
            }
            else
            {
                lista.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Estructura del complemento Timbre Fiscal", Valid = true, Value = "Correcto" });
            }
            return lista;
        }

        /// <summary>
        /// validar estructura del complemento TimbreFiscalDigital
        /// </summary>
        public static List<ValidationProperty> ValidarEstructuraNamespaceTFD(Jaeger.CFDI.V33.Comprobante objeto)
        {
            List<ValidationProperty> lista = new List<ValidationProperty>();
            XmlElement xmlElement = objeto.Complemento.Any.ToList<XmlElement>().SingleOrDefault<XmlElement>((XmlElement p) => p.LocalName.ToLower().Contains("timbrefiscaldigital"));
            if (xmlElement.NamespaceURI != "http://www.sat.gob.mx/TimbreFiscalDigital" || xmlElement.Prefix != "tfd")
            {
                if (xmlElement.NamespaceURI == "http://www.sat.gob.mx/TimbreFiscalDigital")
                {
                    lista.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Namespace", Value = "Correcto", Valid = true });
                }
                else
                {
                    lista.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Namespace", Value = "No válido", Valid = false });
                }

                if (xmlElement.Prefix == "tfd")
                {
                    lista.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Prefijo", Value = "Correcto", Valid = true });
                }
                else
                {
                    lista.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Prefijo", Value = "Sin prefijo tfd del namespace", Valid = false });
                }
            }
            else
            {
                lista.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Estructura del complemento Timbre Fiscal", Valid = true, Value = "Correcto" });
            }
            return lista;
        }

        public static List<ValidationProperty> ValidarEstructuraNamespaceTFD(CFDI.V40.Comprobante objeto) {
            List<ValidationProperty> lista = new List<ValidationProperty>();
            XmlElement xmlElement = objeto.Complemento.Any.ToList<XmlElement>().SingleOrDefault<XmlElement>((XmlElement p) => p.LocalName.ToLower().Contains("timbrefiscaldigital"));
            if (xmlElement.NamespaceURI != "http://www.sat.gob.mx/TimbreFiscalDigital" || xmlElement.Prefix != "tfd") {
                if (xmlElement.NamespaceURI == "http://www.sat.gob.mx/TimbreFiscalDigital") {
                    lista.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Namespace", Value = "Correcto", Valid = true });
                } else {
                    lista.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Namespace", Value = "No válido", Valid = false });
                }

                if (xmlElement.Prefix == "tfd") {
                    lista.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Prefijo", Value = "Correcto", Valid = true });
                } else {
                    lista.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Prefijo", Value = "Sin prefijo tfd del namespace", Valid = false });
                }
            } else {
                lista.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Estructura del complemento Timbre Fiscal", Valid = true, Value = "Correcto" });
            }
            return lista;
        }

        #region por sqlite

        //public static ValidationProperty Validar(CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital objeto, ref Jaeger.Validador.V3.Catalogos.Comun.SQLiteCatalogoCertificados catalogo)
        //{
        //    ValidationProperty respuesta = new ValidationProperty() { Name = TimbreFiscalDigital.SelloSAT, Valid = true, Value = Resources.ValidadorResource.Propiedad_Valido };

        //    if (!(objeto == null))
        //    {
        //        Certificate certificado = catalogo.Search(objeto.noCertificadoSAT);
        //        string cadenaOriginal = objeto.CadenaOriginal;
        //        X509Certificate2 oX509Certificate2 = new X509Certificate2(Convert.FromBase64String(certificado.CerB64));
        //        if (!CryptoService.ValidaSello(objeto.selloSAT, cadenaOriginal, oX509Certificate2, "SHA251"))
        //        {
        //            respuesta.Valid = false;
        //            respuesta.Value = "No válido";
        //        }
        //    }
        //    else
        //    {
        //        respuesta.Value = Resources.ValidadorResource.TFD_Not_Found;
        //        respuesta.Valid = false;
        //    }
        //    return respuesta;
        //}

        //public static ValidationProperty Validar(CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital objeto, ref Jaeger.Validador.V3.Catalogos.Comun.SQLiteCatalogoCertificados catalogo)
        //{
        //    ValidationProperty respuesta = new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_Sello_SAT, Valid = true, Value = Resources.ValidadorResource.Propiedad_Valido };

        //    if (!(objeto == null))
        //    {
        //        Certificate certificado = catalogo.Search(objeto.NoCertificadoSAT);
        //        if (certificado.CerB64 != string.Empty)
        //        {
        //            string cadenaOriginal = objeto.CadenaOriginal;
        //            X509Certificate2 oX509Certificate2 = new X509Certificate2(Convert.FromBase64String(certificado.CerB64));
        //            if (!CryptoService.ValidaSello(objeto.SelloSAT, cadenaOriginal, oX509Certificate2, "SHA256"))
        //            {
        //                respuesta.Valid = false;
        //                respuesta.Value = Resources.ValidadorResource.Propiedad_No_Valido;
        //            }
        //        }
        //        else
        //        {
        //            respuesta.Value = "No se puede validar por falta de certificado";
        //        }
        //    }
        //    else
        //    {
        //        respuesta.Value = Resources.ValidadorResource.TFD_Not_Found;
        //        respuesta.Valid = false;
        //    }
        //    return respuesta;
        //}

        #endregion
    }
}
