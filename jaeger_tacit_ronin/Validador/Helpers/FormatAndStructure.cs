﻿using System;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Enums;

namespace Jaeger.Validador
{
    public class FormatAndStructure
    {
        public FormatAndStructure()
        {
        }

        /// <summary>
        /// validacion de estructura para la versión 3.2
        /// </summary>
        /// <param name="byteComprobant"></param>
        /// <param name="comprobanteBasicInfo"></param>
        /// <returns></returns>
        public static ValidationProperty ValidarFormaYEstructura(byte[] byteComprobant, CFDI.V32.Comprobante comprobanteBasicInfo)
        {
            ValidationProperty oResult = new ValidationProperty();
            string str = string.Concat("Los comprobantes emitidos en el 2011 deben corresponder a una versión ", (comprobanteBasicInfo.version.StartsWith("3") ? "3.0" : "2.0"), ".");
            string str1 = string.Concat("Los comprobantes emitidos despues del 30 de Junio del 2012 deben corresponder a una versión ", (comprobanteBasicInfo.version.StartsWith("3") ? "3.2" : "2.2"), ".");
            DateTime universalTime = DateTime.Parse("2012-07-01T00:00:00.0000000Z").ToUniversalTime();
            DateTime dateTime = DateTime.Parse("2012-01-01T00:00:00.0000000Z").ToUniversalTime();
            oResult.Type = EnumPropertyType.Information;
            oResult.Name = "Emisión";


            if ((comprobanteBasicInfo.version == "3.0" || comprobanteBasicInfo.version == "2.0") && comprobanteBasicInfo.fecha.ToUniversalTime() >= universalTime)
            {
                oResult.Value = string.Concat("La fecha de emisión corresponde con la versión del comprobante. ", str1);
                oResult.Valid = true;
            }
            else if ((comprobanteBasicInfo.version == "3.2" || comprobanteBasicInfo.version == "2.2") && comprobanteBasicInfo.fecha < dateTime)
            {
                string[] strArrays = new string[] { "La versión del comprobante es correcta a su fecha de generación" };
                oResult.Value = string.Concat(strArrays);
                oResult.Valid = true;
            }
            else
            {
                oResult.Value = string.Concat("La fecha de emisión corresponde con la versión del comprobante. ", str);
                oResult.Valid = true;
            }
            return oResult;
        }

        public static ValidationProperty ValidarFormaYEstructura(byte[] byteComprobant, CFDI.V33.Comprobante comprobanteBasicInfo)
        {
            ValidationProperty resultado = new ValidationProperty();
            string str = string.Concat("Los comprobantes emitidos en el 2011 deben corresponder a una versión ", (comprobanteBasicInfo.Version.StartsWith("3") ? "3.0" : "2.0"), ".");
            string str1 = string.Concat("Los comprobantes emitidos despues del 1 de Julio del 2017 deben corresponder a una versión ", (comprobanteBasicInfo.Version.StartsWith("3") ? "3.3" : "3.2"), ".");
            DateTime universalTime = DateTime.Parse("2012-07-01T00:00:00.0000000Z").ToUniversalTime();
            DateTime dateTime = DateTime.Parse("2012-01-01T00:00:00.0000000Z").ToUniversalTime();
            DateTime emisioncfdi33 = DateTime.Parse("2012-01-01T00:00:00.0000000Z").ToUniversalTime();
            resultado.Type = EnumPropertyType.Information;
            resultado.Name = "Emisión";

            if ((comprobanteBasicInfo.Version == "3.3") || comprobanteBasicInfo.Fecha.ToUniversalTime() >= emisioncfdi33)
            {
                resultado.Value = String.Concat("La fecha de emisión corresponde con la versión del comprobante. ", str1);
                resultado.Valid = true;
            }
            else if ((comprobanteBasicInfo.Version == "3.0" || comprobanteBasicInfo.Version == "2.0") && comprobanteBasicInfo.Fecha.ToUniversalTime() >= universalTime) 
            {
                resultado.Value = String.Concat("La fecha de emisión corresponde con la versión del comprobante. ", str1);
                resultado.Valid = true;
            }
            else if ((comprobanteBasicInfo.Version == "3.2" || comprobanteBasicInfo.Version == "2.2") && comprobanteBasicInfo.Fecha < dateTime) 
            {
                resultado.Value = String.Concat("La fecha de emisión corresponde con la versión del comprobante. ", str);
                resultado.Valid = true;
            }
            else
            {
                resultado.Value = String.Concat("La versión del comprobante es correcta a su fecha de generación");
                resultado.Valid = true;
            }
            return resultado;
        }

        public static ValidationProperty ValidarFormaYEstructura(byte[] byteComprobant, CFDI.V40.Comprobante comprobanteBasicInfo) {
            ValidationProperty resultado = new ValidationProperty();
            string str = string.Concat("Los comprobantes emitidos en el 2011 deben corresponder a una versión ", (comprobanteBasicInfo.Version.StartsWith("4") ? "4.0" : "2.0"), ".");
            string str1 = string.Concat("Los comprobantes emitidos despues del 1 de Enero del 2022 deben corresponder a una versión ", (comprobanteBasicInfo.Version.StartsWith("4") ? "4.0" : "3.2"), ".");
            DateTime universalTime = DateTime.Parse("2022-01-01T00:00:00.0000000Z").ToUniversalTime();
            DateTime dateTime = DateTime.Parse("2022-01-01T00:00:00.0000000Z").ToUniversalTime();
            DateTime emisioncfdi33 = DateTime.Parse("2022-01-01T00:00:00.0000000Z").ToUniversalTime();
            resultado.Type = EnumPropertyType.Information;
            resultado.Name = "Emisión";

            if ((comprobanteBasicInfo.Version == "4.0") || comprobanteBasicInfo.Fecha.ToUniversalTime() >= emisioncfdi33) {
                resultado.Value = String.Concat("La fecha de emisión corresponde con la versión del comprobante. ", str1);
                resultado.Valid = true;
            } else if ((comprobanteBasicInfo.Version == "3.0" || comprobanteBasicInfo.Version == "2.0") && comprobanteBasicInfo.Fecha.ToUniversalTime() >= universalTime) {
                resultado.Value = String.Concat("La fecha de emisión corresponde con la versión del comprobante. ", str1);
                resultado.Valid = true;
            } else if ((comprobanteBasicInfo.Version == "3.2" || comprobanteBasicInfo.Version == "2.2") && comprobanteBasicInfo.Fecha < dateTime) {
                resultado.Value = String.Concat("La fecha de emisión corresponde con la versión del comprobante. ", str);
                resultado.Valid = true;
            } else {
                resultado.Value = String.Concat("La versión del comprobante es correcta a su fecha de generación");
                resultado.Valid = true;
            }
            return resultado;
        }
    }
}