﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Xml;

namespace Jaeger.Validador
{
    public class GetXsd
    {
        public string NameSpace
        {
            [DebuggerNonUserCode]
            get;
            [DebuggerNonUserCode]
            set;
        }

        public XmlReader XmlReader
        {
            [DebuggerNonUserCode]
            get;
            [DebuggerNonUserCode]
            set;
        }

        public GetXsd(string resourceName)
        {
            Stream manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Concat("Jaeger.xsd.", resourceName));
            if (manifestResourceStream != null)
            {
                this.XmlReader = XmlReader.Create(manifestResourceStream);
                while (this.XmlReader.Read())
                {
                    if ((!this.XmlReader.IsStartElement() || this.XmlReader.Name != "xs:schema"))
                    {
                        this.NameSpace = this.XmlReader["targetNamespace"];
                        break;
                    }
                }
            }
        }
    }
}