// develop: anhe1 251020182327
// purpose: clase para validacion de comprobante fiscal v3.2
using Jaeger.Catalogos.Entities;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Enums;
using Jaeger.Helpers;
using Jaeger.Interface;
using Jaeger.Services;
using Jaeger.Validador.Helpers;
using System;
using System.Collections.Generic;

namespace Jaeger.Validador.V3 {
    public class Comprobantev32 : IValidadorComprobantes {

        private Configuracion configuracionField;

        public Configuracion Configuracion {
            get {
                return this.configuracionField;
            }
            set {
                this.configuracionField = value;
            }
        }

        /// <summary>
        /// validacion de los esquemas del CFDI
        /// </summary>
        public ValidationProperty Esquemas(object objeto) {
            ValidationProperty r = new ValidationProperty { Type = EnumPropertyType.Information, Name = "Esquemas:", Valid = true };
            Jaeger.CFDI.V32.Comprobante o32 = (Jaeger.CFDI.V32.Comprobante)objeto;
            if (o32 != null) {
                Esquemas esquema = new Esquemas();
                r.Valid = esquema.ValidarEsquema(o32);
                if (r.Valid) {
                    r.Value = "Esquema V�lido";
                    return r;
                }
                r.Value = "Esquema sospechoso";
                return r;
            }
            return null;
        }

        /// <summary>
        /// validacion de la estructura del comprobante
        /// </summary>
        public ValidationProperty Estructura(object objeto, byte[] b) {
            Jaeger.CFDI.V32.Comprobante o32 = (CFDI.V32.Comprobante)objeto;
            if (o32 != null) {
                return FormatAndStructure.ValidarFormaYEstructura(b, o32);
            }
            return null;
        }

        /// <summary>
        /// validar sello del emisor del comprobante CFDI v3.3
        /// </summary>
        public List<ValidationProperty> SelloCFDI(object objeto) {
            List<ValidationProperty> lista = new List<ValidationProperty>();
            Jaeger.CFDI.V32.Comprobante o33 = (Jaeger.CFDI.V32.Comprobante)objeto;

            if (o33 != null) {
                ValidationProperty respuesta = new ValidationProperty { Type = EnumPropertyType.Information, Name = "Sello Emisor" };
                CryptoCertificate c = new CryptoCertificate();
                c.CargarCertificadoDeB64(o33.certificado);
                if (CryptoService.ValidaSelloCfdi(o33)) {
                    respuesta.Valid = true;
                    respuesta.Value = "V�lido";
                    lista.Add(respuesta);
                    
                    if (o33.fecha >= DateTime.Parse(c.ValidoDesde) & o33.fecha <= DateTime.Parse(c.ValidoHasta)) {
                        ValidationProperty w = new ValidationProperty { Type = EnumPropertyType.Information, Name = "Sello Emisor" };
                        w.Name = "CFD(I) con CSD";
                        w.Value = string.Concat("Vigente (al crearse) ", c.ValidoDesde, " al ", c.ValidoHasta);
                        lista.Add(w);
                    }
                    return lista;
                } else {
                    if (o33.fecha >= DateTime.Parse(c.ValidoDesde) & o33.fecha <= DateTime.Parse(c.ValidoHasta)) {
                        ValidationProperty w = new ValidationProperty { Type = EnumPropertyType.Information, Name = "Sello Emisor" };
                        w.Name = "CFD(I) con CSD";
                        w.Value = string.Concat("Vigente (al crearse) ", c.ValidoDesde, " al ", c.ValidoHasta);
                        lista.Add(w);
                    }
                    return lista;
                }
            } else {
                ValidationProperty respuesta = new ValidationProperty { Type = EnumPropertyType.Information, Name = "Sello Emisor" };
                respuesta.Name = "CFD(I) con CSD ";
                respuesta.Value = "No V�lido";
                respuesta.Valid = false;
                lista.Add(respuesta);
                return lista;
            }
            return null;
        }

        /// <summary>
        /// validar sello digital SAT
        /// </summary>
        public ValidationProperty SelloSAT(object objeto) {
            Jaeger.CFDI.V32.Comprobante o33 = (Jaeger.CFDI.V32.Comprobante)objeto;
            if (o33 != null) {
                ValidationProperty r = new ValidationProperty() { Type = EnumPropertyType.Attention, Name = "Sello SAT", Value = "No existe complemento del timbre fiscal", Valid = false };
                if (!(o33.Complemento == null)) {
                    if (!(o33.Complemento.TimbreFiscalDigital == null)) {
                        // buscamos certificado en el catalogo con el numero de serie valido
                        Certificate c = ValidadorComprobantes.CatalogoCertificados.Search(o33.Complemento.TimbreFiscalDigital.noCertificadoSAT);
                        if (c == null) {
                            // sino encontramos el certificado en el catalogo lo buscamos en el FTP del SAT
                            c = Jaeger.Helpers.DescargaCertificadoSAT.Buscar(o33.Complemento.TimbreFiscalDigital.noCertificadoSAT);
                        }
                        // si tenemos el certificado, entonces validados el sello 
                        if (c != null) {
                            return TimbreFiscalDigital.Validar(o33.Complemento.TimbreFiscalDigital, ref ValidadorComprobantes.CatalogoCertificados);
                        }
                    }
                }
                return r;
            }
            return null;
        }

        public ValidationProperty MetodoPago(object objeto) {
            Jaeger.CFDI.V32.Comprobante o33 = (Jaeger.CFDI.V32.Comprobante)objeto;
            return new ValidationProperty { Type = EnumPropertyType.Attention, Name = Resources.ValidadorResource.Propiedad_Metodo_Pago, Value = o33.metodoDePago, Valid = true };
        }

        public ValidationProperty FormaPago(object objeto) {
            Jaeger.CFDI.V32.Comprobante o33 = (Jaeger.CFDI.V32.Comprobante)objeto;
            return new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_Forma_Pago, Valid = true, Value = o33.formaDePago };
        }

        /// <summary>
        /// validacion del estado del comprobante con webservice del SAT
        /// </summary>
        public ValidationProperty EstadoSAT(object objeto) {
            Jaeger.CFDI.V32.Comprobante o33 = (Jaeger.CFDI.V32.Comprobante)objeto;
            if (o33 != null) {
                ValidationProperty r = new ValidationProperty { Type = EnumPropertyType.Information, Name = "Estado SAT", Valid = true };
                if (!(o33.Complemento == null)) {
                    if (!(o33.Complemento.TimbreFiscalDigital == null)) {
                        Jaeger.SAT.Entities.SatQueryResult q = HelperServiceQuerySAT.Query(o33.Emisor.rfc, o33.Receptor.rfc, o33.total, o33.Complemento.TimbreFiscalDigital.UUID);
                        if (q.Clave == "E") {
                            r.Code = "E";
                            r.Value = "No disponible";
                            r.Valid = false;
                        } else {
                            r.Code = "R";
                            r.Value = q.Status;
                            r.Valid = true;
                        }
                        return r;
                    }
                }
                r.Code = "E";
                r.Value = "No existe complemento de timbra fiscal";
                r.Valid = false;
                return r;
            }
            return null;
        }

        public ValidationProperty LugarExpedicion(object objeto) {
            Jaeger.CFDI.V32.Comprobante o33 = (Jaeger.CFDI.V32.Comprobante)objeto;
            return new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_Expedicion, Valid = true, Value = o33.LugarExpedicion };
        }

        /// <summary>
        /// validacion de la clave de uso del CFDI
        /// </summary>
        public ValidationProperty UsodeCFDI(object objeto) {
            return new ValidationProperty { Type = EnumPropertyType.Information, Name = "Uso de CFDI", Value = "No aplica para la versi�n del Comprobante", Valid = true };
        }

        /// <summary>
        /// validacion de las claves de productos y servicios, con el catalogo SAT
        /// </summary>
        public List<ValidationProperty> ProductosServicios(object objeto) {
            List<ValidationProperty> r = new List<ValidationProperty>();
            r.Add(new ValidationProperty { Type = EnumPropertyType.Information, Name = "Uso de CFDI", Value = "No aplica para la versi�n del Comprobante", Valid = true });
            return r;
        }

        public List<ValidationProperty> Articulo69B(object objeto) {
            List<ValidationProperty> response = new List<ValidationProperty>();
            response.Add(new ValidationProperty {
                Type = EnumPropertyType.Information,
                Name = "Art�culo 69B Situaci�n: ",
                Value = "No aplica para la versi�n del comprobante",
                Valid = true
            });
            return response;
        }
    }
}