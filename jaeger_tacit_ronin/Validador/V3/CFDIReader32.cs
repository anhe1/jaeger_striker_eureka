using System;
using System.Collections;
using System.IO;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Entities.Basico;
using System.Xml;
using System.Collections.Generic;
using Jaeger.Helpers;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.Validador.V3
{
    public class CFDIReader32 : Interface.IHelperCFDReader
    {
        private RelacionCFDICatalogo catalogoRelacionCFDIField;

        public RelacionCFDICatalogo CatalogoRelacionCFDI
        {
            get
            {
                return this.catalogoRelacionCFDIField;
            }
            set
            {
                this.catalogoRelacionCFDIField = value;
            }
        }

        public Documento Reader(FileInfo fileXml)
        {
            Documento objeto = new Documento();
            XmlReader reader = XmlReader.Create(fileXml.FullName);
            List<string> lista = new List<string>();
            bool impuestos = false;
            try
            {
                while (reader.Read())
                {
                    try
                    {
                        if (reader.IsStartElement())
                        {
                            string name = reader.Name;
                            if (!(name == null))
                            {
                                if (name == "cfdi:Comprobante")
                                {
                                    // comprobar version del comprobante
                                    if (reader["version"] == "3.2")
                                    {
                                        objeto.Version = EnumCfdVersion.V32;
                                    }
                                    else
                                    {
                                        objeto.Version = EnumCfdVersion.None;
                                    }
                                    // tiene folio?
                                    objeto.Folio = CFDReaderCommon.ReaderString(reader["folio"]);
                                    objeto.TipoComprobante = CFDReaderCommon.TipoComprobante(CFDReaderCommon.ReaderString(reader["tipoDeComprobante"]));
                                    objeto.Serie = CFDReaderCommon.ReaderString(reader["serie"]);
                                    objeto.Fecha = CFDReaderCommon.ReaderDateTime(reader["fecha"]);
                                    objeto.SubTotal = Convert.ToDecimal(reader["subTotal"]);
                                    objeto.Descuento = Convert.ToDecimal(reader["descuento"]);
                                    objeto.Total = Convert.ToDecimal(reader["total"]);
                                    objeto.MetodoPago = CFDReaderCommon.ReaderString(reader["metodoDePago"]);
                                    objeto.FormaPago = CFDReaderCommon.ReaderString(reader["formaDePago"]);
                                    objeto.LugarExpedicion = CFDReaderCommon.ReaderString(reader["LugarExpedicion"]);
                                    objeto.NoCertificado = CFDReaderCommon.ReaderString(reader["noCertificado"]);
                                    //objeto.NumCtaPago = CFDReaderCommon.ReaderString(reader["NumCtaPago"]);
                                }
                                else if (name == "cfdi:Emisor")
                                {
                                    objeto.EmisorRFC = reader["rfc"].ToString();
                                    objeto.Emisor = CFDReaderCommon.ReaderString(reader["nombre"]);
                                }
                                else if (name == "cfdi:Receptor")
                                {
                                    objeto.ReceptorRFC = reader["rfc"].ToString();
                                    objeto.Receptor = CFDReaderCommon.ReaderString(reader["nombre"]);
                                }
                                else if (name == "cfdi:Traslado")
                                {

                                }
                                else if (name == "cfdi:Retenido")
                                {

                                }
                                else if (name == "tfd:TimbreFiscalDigital")
                                {
                                    objeto.IdDocumento = CFDReaderCommon.ReaderString(reader["UUID"]);
                                    objeto.FechaCertificacion = CFDReaderCommon.ReaderDateTime(reader["FechaTimbrado"]);
                                    objeto.NoCertificadoProvCertif = CFDReaderCommon.ReaderString(reader["noCertificadoSAT"]);
                                    lista.Add("TimbreFiscalDigital");
                                }
                                else if (name == "cfdi:Concepto")
                                {
                                    Concepto concepto = new Concepto();
                                    concepto.NoIdentificacion = reader["noIdentificacion"];
                                    concepto.Cantidad = Convert.ToDecimal(reader["cantidad"]);
                                    concepto.Unidad = reader["unidad"];
                                    concepto.Descripcion = reader["descripcion"];
                                    concepto.ValorUnitario = Convert.ToDecimal(reader["valorUnitario"]);
                                    concepto.Importe = Convert.ToDecimal(reader["importe"]);
                                    objeto.Conceptos.Add(concepto);
                                }
                                else if (name == "cfdi:Impuestos")
                                {
                                    if (impuestos == false)
                                    {
                                        //this.ReaderCfdi32Impuestos(ref objeto, reader.ReadOuterXml());
                                        impuestos = true;
                                    }
                                }
                                else if (name == "cfdi:Complemento")
                                {

                                }
                                else if (name == "Nomina")
                                {
                                    lista.Add("Nomina");
                                }
                                else if (name == "nomina12:Nomina")
                                {
                                    lista.Add("Nomina12");
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(string.Concat("ReaderCFDI32: ", e.Message));
                        // sino pudo ser leido entonces utilizamos el otro metodo
                        return this.ReaderSerializer(fileXml);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Concat("ReaderCFDI32: ", e.Message));
                return this.ReaderSerializer(fileXml);
            }
            objeto.Complementos = string.Join(",", lista);
            return objeto;
        }

        public Documento ReaderSerializer(FileInfo fileXml)
        {
            CFDI.V32.Comprobante objeto = CFDI.V32.Comprobante.Load(fileXml.FullName);
            if (objeto != null)
            {
                Documento newItem = new Documento();
                newItem.TipoComprobante = CFDReaderCommon.TipoComprobante(Enum.GetName(typeof(CFDI.V32.ComprobanteTipoDeComprobante), objeto.tipoDeComprobante));
                newItem.Fecha = objeto.fecha;
                newItem.Folio = objeto.folio;
                newItem.Serie = objeto.serie;
                newItem.Emisor = objeto.Emisor.nombre;
                newItem.EmisorRFC = objeto.Emisor.rfc;
                newItem.Receptor = objeto.Receptor.nombre;
                newItem.ReceptorRFC = objeto.Receptor.rfc;
                newItem.LugarExpedicion = objeto.LugarExpedicion;
                newItem.MetodoPago = objeto.metodoDePago;
                newItem.FormaPago = objeto.formaDePago;
                newItem.Version = EnumCfdVersion.V32;
                newItem.NoCertificado = objeto.noCertificado;
                //newItem.NumCtaPago = objeto.NumCtaPago;
                newItem.SubTotal = Convert.ToDecimal(objeto.total);
                newItem.Total = newItem.SubTotal;

                newItem.Descuento = 0;
                if (objeto.descuentoSpecified)
                {
                    newItem.Descuento = Convert.ToDecimal(objeto.descuento);
                }

                // informacion del timbre fiscal
                if (!(objeto.Complemento.TimbreFiscalDigital == null))
                {
                    newItem.NoCertificadoProvCertif = objeto.Complemento.TimbreFiscalDigital.noCertificadoSAT;
                    newItem.FechaCertificacion = objeto.Complemento.TimbreFiscalDigital.FechaTimbrado;
                    newItem.IdDocumento = objeto.Complemento.TimbreFiscalDigital.UUID;
                }

                // conceptos del comprobante
                newItem.Conceptos = new System.ComponentModel.BindingList<Concepto>(HelperConvertidor.ComprobanteToBasic(objeto.Conceptos));

                // complementos
                if (!(objeto.Complemento.Any == null))
                {
                    List<string> lista = new List<string>();
                    foreach (XmlElement item in objeto.Complemento.Any)
                    {
                        lista.Add(item.LocalName);
                    }
                    newItem.Complementos = string.Join(",", lista.ToArray());
                }

                objeto = null;
                return newItem;
            }
            objeto = null;
            return null;
        }

        public void ReaderCfdi32Impuestos(ref Documento objeto, string inXml)
        {
            XmlDocument xmlD = new XmlDocument();
            xmlD.LoadXml(inXml);
            IEnumerator enumerator = null;
            try
            {
                enumerator = xmlD.SelectNodes("//*").GetEnumerator();
                while (enumerator.MoveNext())
                {
                    XmlElement current = (XmlElement)(enumerator.Current);
                    try
                    {
                        if (current.Name == "cfdi:Traslado")
                        {
                            if (current.GetAttribute("impuesto").ToLower() == "iva")
                            {
                                objeto.TotalTrasladoIVA = objeto.TotalTrasladoIVA + Convert.ToDecimal(current.GetAttribute("importe"));
                            }
                            else if (current.GetAttribute("impuesto").ToLower() == "ieps")
                            {
                                objeto.TotalTrasladoIEPS = objeto.TotalTrasladoIEPS + Convert.ToDecimal(current.GetAttribute("importe"));
                            }
                        }
                        else if (current.Name == "cfdi:Retencion")
                        {
                            if (current.GetAttribute("impuesto").ToLower() == "iva")
                            {
                                objeto.TotalRetencionIVA = objeto.TotalRetencionIVA + Convert.ToDecimal(current.GetAttribute("importe"));
                            }
                            else if (current.GetAttribute("impuesto").ToLower() == "isr")
                            {
                                objeto.TotalRetencionISR = objeto.TotalRetencionISR + Convert.ToDecimal(current.GetAttribute("importe"));
                            }
                            else if (current.GetAttribute("impuesto").ToLower() == "ieps")
                            {
                                objeto.TotalRetencionIEPS = objeto.TotalRetencionIEPS + Convert.ToDecimal(current.GetAttribute("importe"));
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(string.Concat("ReaderCFDI32Impuestos: ", e.Message));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Concat("ReaderCFDI32Impuestos: ", e.Message));
            }
        }
    }
}