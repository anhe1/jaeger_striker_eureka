﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Xml;
using Jaeger.Catalogos.Repositories;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Entities.Basico;
using Jaeger.Helpers;

namespace Jaeger.Validador.V3 {
    public class CFDIReader40 : Interface.IHelperCFDReader {
        private RelacionCFDICatalogo catalogoRelacionCFDIField;
        private readonly HelperConvertidor40 convertidor;

        public CFDIReader40() {
            this.convertidor = new HelperConvertidor40();
        }

        public RelacionCFDICatalogo CatalogoRelacionCFDI {
            get {
                return this.catalogoRelacionCFDIField;
            }
            set {
                this.catalogoRelacionCFDIField = value;
            }
        }

        public Documento Reader(FileInfo fileXml) {
            Documento objeto = new Documento();
            XmlReader reader = XmlReader.Create(fileXml.FullName);
            List<string> lista = new List<string>();

            try {
                while (reader.Read()) {
                    try {
                        if (reader.IsStartElement()) {
                            string name = reader.Name;
                            if (!(name == null)) {
                                if (name == "cfdi:Comprobante") {
                                    if (reader["Version"] == "4.0") {
                                        objeto.Version = EnumCfdVersion.V40;
                                    } else {
                                        objeto.Version = EnumCfdVersion.None;
                                    }
                                    objeto.Folio = reader["Folio"];
                                    objeto.TipoComprobante = CFDReaderCommon.TipoComprobante(CFDReaderCommon.ReaderString(reader["TipoDeComprobante"]));
                                    objeto.Serie = reader["Serie"];
                                    objeto.Fecha = CFDReaderCommon.ReaderDateTime(reader["Fecha"]);
                                    objeto.SubTotal = Convert.ToDecimal(reader["SubTotal"]);
                                    objeto.Descuento = Convert.ToDecimal(reader["Descuento"]);
                                    objeto.Total = Convert.ToDecimal(reader["Total"]);
                                    objeto.MetodoPago = CFDReaderCommon.ReaderString(reader["MetodoPago"]);
                                    objeto.FormaPago = CFDReaderCommon.ReaderString(reader["FormaPago"]);
                                    objeto.LugarExpedicion = CFDReaderCommon.ReaderString(reader["LugarExpedicion"]);
                                    objeto.NoCertificado = CFDReaderCommon.ReaderString(reader["NoCertificado"]);
                                    //objeto.NumCtaPago = CFDReaderCommon.ReaderString(reader["NumCtaPago"]);
                                } else if (name == "cfdi:Emisor") {
                                    objeto.EmisorRFC = reader["Rfc"];
                                    objeto.Emisor = reader["Nombre"];
                                    objeto.EmisorRegimen = reader["RegimenFiscal"];
                                } else if (name == "cfdi:Receptor") {
                                    objeto.ReceptorRFC = reader["Rfc"];
                                    objeto.Receptor = CFDReaderCommon.ReaderString(reader["Nombre"]);
                                    objeto.UsoCFDI = CFDReaderCommon.ReaderString(reader["UsoCFDI"]);
                                    objeto.ReceptorRegimen = CFDReaderCommon.ReaderString(reader["RegimenFiscalReceptor"]);
                                    objeto.DomicilioFiscal = CFDReaderCommon.ReaderString(reader["DomicilioFiscalReceptor"]);
                                } else if (name == "cfdi:Concepto") {
                                    Concepto concepto = new Concepto();
                                    concepto.NoIdentificacion = reader["NoIdentificacion"];
                                    concepto.ClaveProdServ = reader["ClaveProdServ"];
                                    concepto.Cantidad = Convert.ToDecimal(reader["Cantidad"]);
                                    concepto.ClaveUnidad = reader["ClaveUnidad"];
                                    concepto.Unidad = reader["Unidad"];
                                    concepto.Descripcion = reader["Descripcion"];
                                    concepto.ValorUnitario = Convert.ToDecimal(reader["ValorUnitario"]);
                                    concepto.Importe = Convert.ToDecimal(reader["Importe"]);
                                    concepto.Descuento = Convert.ToDecimal(reader["Descuento"]);
                                    objeto.Conceptos.Add(concepto);
                                } else if (name == "cfdi:Traslado") {
                                } else if (name == "cfdi:Retenido") {
                                } else if (name == "cfdi:Impuestos") {
                                    this.ReaderCfdi33Impuestos(ref objeto, reader.ReadOuterXml());
                                } else if (name == "tfd:TimbreFiscalDigital") {
                                    objeto.IdDocumento = reader["UUID"];
                                    objeto.FechaCertificacion = CFDReaderCommon.ReaderDateTime(reader["FechaTimbrado"]);
                                    objeto.NoCertificadoProvCertif = reader["NoCertificadoSAT"];
                                    objeto.RFCProvCertif = reader["RfcProvCertif"];
                                    //objeto.Leyenda = CFDReaderCommon.ReaderString(reader["Leyenda"]);
                                    lista.Add("TimbreFiscalDigital");
                                } else if (name == "pago20:Pago") {
                                    // creamos el objeto
                                    if (objeto.ComplementoPago == null) {
                                        objeto.ComplementoPago = new Jaeger.Edita.V2.CFDI.Entities.ComplementoPagos();
                                        objeto.ComplementoPago.Pago = new BindingList<Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoPagosPago>();
                                    }
                                    Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoPagosPago pago1 = new Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoPagosPago();
                                    pago1.FechaPago = CFDReaderCommon.ReaderDateTime(reader["FechaPago"]);
                                    pago1.FormaDePagoP = new Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoFormaPago { Clave = reader["FormaDePagoP"] };
                                    pago1.MonedaP = reader["MonedaP"];
                                    pago1.Monto = Convert.ToDecimal(reader["Monto"]);
                                    pago1.RfcEmisorCtaOrd = reader["RfcEmisorCtaOrd"];
                                    pago1.RfcEmisorCtaBen = reader["RfcEmisorCtaBen"];
                                    pago1.CtaBeneficiario = reader["CtaBeneficiario"];
                                    pago1.CtaOrdenante = reader["CtaOrdenante"];
                                    pago1.NumOperacion = reader["NumOperacion"];
                                    pago1.NomBancoOrdExt = reader["NomBancoOrdExt"];
                                    pago1.TipoCadPago = reader["TipoCadPago"];
                                    pago1.CertPago = reader["CertPago"];
                                    pago1.CadPago = reader["CadPago"];
                                    pago1.SelloPago = reader["SelloPago"];
                                    objeto.ComplementoPago.Version = reader["Version"];
                                    objeto.ComplementoPago.Pago.Add(pago1);
                                    lista.Add("Recepción de Pagos");
                                } else if (name == "pago10:DoctoRelacionado") {
                                    if (objeto.ComplementoPago == null) {
                                        objeto.ComplementoPago = new Jaeger.Edita.V2.CFDI.Entities.ComplementoPagos();
                                    }

                                    Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoPagoDoctoRelacionado doctoRel = new Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoPagoDoctoRelacionado();
                                    doctoRel.AutoCalcular = false;
                                    doctoRel.IdDocumento = reader["IdDocumento"];
                                    doctoRel.Serie = reader["Serie"];
                                    doctoRel.Folio = reader["Folio"];
                                    doctoRel.Moneda = reader["MonedaDR"];
                                    doctoRel.MetodoPago = reader["MetodoDePagoDR"];
                                    doctoRel.ImpSaldoAnt = Convert.ToDecimal(reader["ImpSaldoAnt"]);
                                    doctoRel.ImpPagado = Convert.ToDecimal(reader["ImpPagado"]);
                                    doctoRel.ImpSaldoInsoluto = Convert.ToDecimal(reader["ImpSaldoInsoluto"]);
                                    doctoRel.TipoCambio = Convert.ToDecimal(reader["TipoCambioDR"]);
                                    doctoRel.NumParcialidad = Convert.ToInt32(reader["NumParcialidad"]);
                                    if (objeto.ComplementoPago.Pago[objeto.ComplementoPago.Pago.Count - 1].DoctoRelacionado == null) {
                                        objeto.ComplementoPago.Pago[objeto.ComplementoPago.Pago.Count - 1].DoctoRelacionado = new BindingList<Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoPagoDoctoRelacionado>();
                                    }
                                    objeto.ComplementoPago.Pago[objeto.ComplementoPago.Pago.Count - 1].DoctoRelacionado.Add(doctoRel);
                                } else if (name == "cfdi:CfdiRelacionados") {
                                    if (objeto.CFDIRelacionado == null) {
                                        objeto.CFDIRelacionado = new Jaeger.Edita.V2.CFDI.Entities.ComprobanteCfdiRelacionados();
                                    }

                                    objeto.CFDIRelacionado.TipoRelacion = new Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoDoctoRelacionadoRelacion();
                                    objeto.CFDIRelacionado.TipoRelacion.Clave = reader["TipoRelacion"];
                                    //objeto.CFDIRelacionado.TipoRelacion.Descripcion = this.catalogoRelacionCFDIField.Search(objeto.CFDIRelacionado.TipoRelacion.Clave).Descripcion;
                                } else if (name == "cfdi:CfdiRelacionado") {
                                    if (objeto.CFDIRelacionado == null) {
                                        objeto.CFDIRelacionado = new Jaeger.Edita.V2.CFDI.Entities.ComprobanteCfdiRelacionados();
                                    }

                                    if (objeto.CFDIRelacionado.CfdiRelacionado == null) {
                                        objeto.CFDIRelacionado.CfdiRelacionado = new BindingList<Jaeger.Edita.V2.CFDI.Entities.ComprobanteCfdiRelacionadosCfdiRelacionado>();
                                    }

                                    objeto.CFDIRelacionado.CfdiRelacionado.Add(new Jaeger.Edita.V2.CFDI.Entities.ComprobanteCfdiRelacionadosCfdiRelacionado { IdDocumento = CFDReaderCommon.ReaderString(reader["UUID"]).ToUpper() });
                                } else if (name == "nomina12:Nomina") {
                                    lista.Add("Nomina12");
                                } else if (name == "implocal:ImpuestosLocales") {
                                    lista.Add("ImpuestosLocales");
                                } else if (name == "valesdedespensa:ValesDeDespensa") {
                                    lista.Add("ValesDeDespensa");
                                } else if (name == "aerolineas:Aerolineas") {
                                    lista.Add("Aerolineas");
                                }
                            }
                        }
                    } catch (Exception e) {
                        Console.WriteLine(string.Concat("ReaderCFDI33: ", e.Message));
                        return this.ReaderSerializer(fileXml);
                    }
                }
            } catch (Exception e) {
                Console.WriteLine(string.Concat("ReaderCFDI32: ", e.Message));
                return this.ReaderSerializer(fileXml);
            }

            objeto.Complementos = string.Join(",", lista);
            return objeto;
        }

        public Documento ReaderSerializer(FileInfo archivo) {
            List<XmlElement>.Enumerator enumerator = new List<XmlElement>.Enumerator();
            var objeto = Jaeger.CFDI.V40.Comprobante.Load(archivo.FullName);
            Documento objetoValidFile = new Documento();
            List<string> listas = new List<string>();

            if (objeto != null) {
                objetoValidFile.Descuento = Convert.ToDecimal(objeto.Descuento);
                objetoValidFile.EstadoSAT = "No Disponible";
                objetoValidFile.Emisor = objeto.Emisor.Nombre;
                objetoValidFile.EmisorRFC = objeto.Emisor.Rfc;
                objetoValidFile.Fecha = objeto.Fecha;
                objetoValidFile.Folio = objeto.Folio;
                objetoValidFile.Serie = objeto.Serie;
                objetoValidFile.LugarExpedicion = objeto.LugarExpedicion;
                objetoValidFile.MetodoPago = objeto.MetodoPago;
                objetoValidFile.FormaPago = objeto.FormaPago;
                objetoValidFile.NoCertificado = objeto.NoCertificado;
                objetoValidFile.Receptor = objeto.Receptor.Nombre;
                objetoValidFile.ReceptorRFC = objeto.Receptor.Rfc;
                objetoValidFile.UsoCFDI = objeto.Receptor.UsoCFDI;
                objetoValidFile.SubTotal = Convert.ToDecimal(objeto.SubTotal);
                objetoValidFile.Total = Convert.ToDecimal(objeto.Total);
                objetoValidFile.TipoComprobante = CFDReaderCommon.TipoComprobante(objeto.TipoDeComprobante);

                // CFDIs relacionados
                if (objeto.CfdiRelacionados != null) {
                    objetoValidFile.CFDIRelacionado = this.convertidor.ComprobanteCfdiRelacionados(objeto.CfdiRelacionados);
                    try {
                        objetoValidFile.CFDIRelacionado.TipoRelacion.Descripcion = this.CatalogoRelacionCFDI.Search(objetoValidFile.CFDIRelacionado.TipoRelacion.Clave).Descripcion;
                    } catch (Exception) {
                        objetoValidFile.CFDIRelacionado.TipoRelacion.Descripcion = "";
                    }
                }

                // nombres de los complementos
                if (objeto.Complemento.Any.Count > 0) {
                    try {
                        enumerator = objeto.Complemento.Any.GetEnumerator();
                        while (enumerator.MoveNext()) {
                            listas.Add(enumerator.Current.LocalName);
                        }
                    } finally {
                        ((IDisposable)enumerator).Dispose();
                    }
                }

                // informacion del timbre fiscal
                if (objeto.Complemento.TimbreFiscalDigital != null) {
                    objetoValidFile.NoCertificadoProvCertif = objeto.Complemento.TimbreFiscalDigital.NoCertificadoSAT;
                    objetoValidFile.IdDocumento = objeto.Complemento.TimbreFiscalDigital.UUID;
                    objetoValidFile.FechaCertificacion = objeto.Complemento.TimbreFiscalDigital.FechaTimbrado;
                    objetoValidFile.RFCProvCertif = objeto.Complemento.TimbreFiscalDigital.RfcProvCertif;
                }

                // conceptos del comprobante 
                //objetoValidFile.Conceptos = new BindingList<Concepto>(HelperConvertidor.ComprobanteToBasic(objeto.Conceptos));

                // complemento pagos10 
                if (objeto.Complemento.Pagos != null) {
                    objetoValidFile.ComplementoPago = this.convertidor.Create(objeto.Complemento.Pagos);
                }

                objetoValidFile.Complementos = string.Join(",", listas.ToArray());
            } else {
                return null;
            }
            return objetoValidFile;
        }

        public void ReaderCfdi33Impuestos(ref Documento objeto, string inXml) {
            if ((inXml.Contains("TotalImpuestosTrasladados") || inXml.Contains("TotalImpuestosRetenidos") ? true : false)) {
                XmlDocument xmlD = new XmlDocument();
                IEnumerator enumerator = null;
                xmlD.LoadXml(inXml);
                try {
                    enumerator = xmlD.SelectNodes("//*").GetEnumerator();
                    while (enumerator.MoveNext()) {
                        XmlElement current = (XmlElement)enumerator.Current;
                        try {
                            if (current.Name != "cfdi:Traslado") {
                                if (current.Name == "cfdi:Retencion") {
                                    if (current.GetAttribute("Impuesto").ToLower() == "002") {
                                        objeto.TotalRetencionIVA = objeto.TotalRetencionIVA + Convert.ToDecimal(current.GetAttribute("Importe"));
                                    } else if (current.GetAttribute("Impuesto").ToLower() == "001") {
                                        objeto.TotalRetencionISR = objeto.TotalRetencionISR + Convert.ToDecimal(current.GetAttribute("Importe"));
                                    } else if (current.GetAttribute("Impuesto").ToLower() == "003") {
                                        objeto.TotalRetencionIEPS = objeto.TotalRetencionIEPS + Convert.ToDecimal(current.GetAttribute("Importe"));
                                    }
                                }
                            } else if (current.GetAttribute("Impuesto").ToLower() == "002") {
                                objeto.TotalTrasladoIVA = objeto.TotalTrasladoIVA + Convert.ToDecimal(current.GetAttribute("Importe"));
                            } else if (current.GetAttribute("Impuesto").ToLower() == "001") {
                                objeto.TotalTrasladoIEPS = objeto.TotalTrasladoIEPS + Convert.ToDecimal(current.GetAttribute("Importe"));
                            }
                        } catch (Exception ex) {
                            Console.WriteLine(string.Concat("ReaderCFDI33Impuestos: ", ex.Message));
                        }
                    }
                } catch (Exception ex) {
                    Console.WriteLine(string.Concat("ReaderCFDI33Impuestos: ", ex.Message));
                }
            }
        }
    }
}