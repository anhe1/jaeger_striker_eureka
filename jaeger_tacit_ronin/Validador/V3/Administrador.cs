﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Ionic.Zip;
using Jaeger.Edita.V2.CFDI.Enums;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Enums;
using Jaeger.Entities.Validador;
using Jaeger.Validador.V3.Entities;

namespace Jaeger.Validador.V3 {
    public class Administrador : ValidadorComprobantes {

        public partial class AdministradorBase {
            public AdministradorBase() {
                this.Version = "3.0";
                this.Titulo = "Backup";
                this.Fecha = DateTime.Now;
                this.Documentos = new BindingList<Documento>();
            }

            [JsonProperty("ver")]
            public string Version { get; set; }

            [JsonProperty("fecha")]
            public DateTime Fecha { get; set; }

            [JsonProperty("titulo")]
            public string Titulo { get; set; }

            [JsonProperty("items")]
            public BindingList<Documento> Documentos { get; set; }

            [JsonProperty("errores")]
            public BindingList<Documento> DocumentosError { get; set; }

            public string ToJson(Formatting formatting = 0) {
                // configuracion json para la serializacion
                var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy" };
                return JsonConvert.SerializeObject(this, formatting, conf);
            }
        }

        //private AdministradorBase admin;
        private BindingList<Documento> documentos;
        private BindingList<Documento> documentosError;

        /// <summary>
        /// constructor
        /// </summary>
        public Administrador(Configuracion configuracion) : base(configuracion) {
            this.documentos = new BindingList<Documento>();
            this.documentosError = new BindingList<Documento>();
        }

        #region propiedades

        public BindingList<Documento> Documentos {
            get {
                return this.documentos;
            }
            set {
                this.documentos = value;
            }
        }

        public BindingList<Documento> DocumentosError {
            get {
                return this.documentosError;
            }
            set {
                this.documentosError = value;
            }
        }

        public bool AutoBackup { get; set; }

        #endregion

        public static Documento Identifica(FileInfo fileXml) {
            if (fileXml.Exists) {
                MetaDocument version = CFDReaderCommon.GetVersion(fileXml);
                if (version != null) {
                    Interface.IHelperCFDReader reader;
                    Documento c = new Documento();
                    if (version.Version == EnumCfdVersion.V32 & version.MetaType == EnumMetadataType.CFDI) {
                        reader = new CFDIReader32();
                        c = reader.Reader(fileXml);
                    } else if (version.Version == EnumCfdVersion.V33 & version.MetaType == EnumMetadataType.CFDI) {
                        reader = new CFDIReader33();
                        //reader.CatalogoRelacionCFDI = ValidadorComprobantes.CatalogoRelacionCFDI;
                        c = reader.Reader(fileXml);
                    } else if (version.Version == EnumCfdVersion.V40 & version.MetaType == EnumMetadataType.CFDI) {
                        reader = new CFDIReader40();
                        c = reader.Reader(fileXml);
                    } else {
                        c = null;
                    }

                    if (c != null) {
                        return c;
                    }
                }

            }
            return null;
        }

        public static Documento Agregar(FileInfo archivo, ref BindingList<Documento> lista) {
            Documento results = Administrador.Identifica(archivo);
            if (results != null) {
                // guardar el archivo en base 64
                results.ArchivoXML = CFDReaderCommon.Load(new FileInfo(Path.ChangeExtension(archivo.FullName, "xml")));
                if (File.Exists(Path.ChangeExtension(archivo.FullName, "pdf"))) {
                    results.ArchivoPDF = CFDReaderCommon.Load(new FileInfo(Path.ChangeExtension(archivo.FullName, "pdf")));
                } else {

                }
            } else {
                // aqui listamos los comprobantes que no fueron identificados

                Console.WriteLine("Error!");
            }

            // buscamos un duplicado
            Documento duplicado = null;
            try {
                duplicado = lista.First<Documento>((Documento p) => p.IdDocumento == results.IdDocumento);
            } catch (Exception ex) {
                Console.WriteLine(string.Concat(": " + ex.Message));
            }

            // comprobacion en el caso de que encontremos un acuse de cancelacion
            if (duplicado != null) {
                if (duplicado.MetaData == EnumMetadataType.CFDI & results.MetaData == EnumMetadataType.Acuse) {
                    duplicado.AcusePDF = results.ArchivoPDF;
                    duplicado.AcuseXML = results.AcuseXML;
                    duplicado.Acuse = results.Acuse;
                    duplicado.Registrado = false;
                    results = duplicado;
                    lista.Remove(duplicado);
                    duplicado = null;
                } else if (duplicado.MetaData == EnumMetadataType.Acuse & results.MetaData == EnumMetadataType.CFDI) {
                    results.AcuseXML = duplicado.ArchivoXML;
                    results.AcusePDF = duplicado.ArchivoPDF;
                    results.Acuse = duplicado.Acuse;
                    results.Registrado = false;
                    lista.Remove(duplicado);
                    duplicado = null;
                } else {
                    //results.Errors.Add(new Property { Type = EnumPropertyType.Error, Code = "", Name = "Error", Value = "Este archivo esta duplicado. " + version.Error });
                }

                if (duplicado == null & results.MetaData != Enums.EnumMetadataType.Other) {
                    lista.Add(results);
                } else {
                    return results;
                }
            }

            return results;
        }

        public Documento Agregar(FileInfo item) {
            Documento result = Administrador.Agregar(item, ref this.documentos);
            if (result != null) {
                this.documentos.Add(result);
            } else {
                Documento con = new Documento();
                con.ArchivoXML = CFDReaderCommon.Load(new FileInfo(item.FullName));
                this.DocumentosError.Add(con);
            }
            return result;
        }

        /// <summary>
        /// remover un objeto de la coleccion
        /// </summary>
        public bool Remover(int index) {
            try {
                this.Documentos.RemoveAt(index);
                return true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        /// <summary>
        /// ejecutar busqueda de archivos
        /// </summary>
        /// <param name="carpeta"></param>
        /// <param name="incluirSubCarpetas">si debe incluir subcarpetas</param>
        /// <param name="progreso"></param>
        /// <returns></returns>
        public async Task ExecuteSearch(string carpeta, bool incluirSubCarpetas, IProgress<Progreso> progreso) {
            Progreso reporte = new Progreso();
            int errores = 0;
            this.documentos.Clear();
            this.documentosError.Clear();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            await Task.Run(() => {
                try {
                    progreso.Report(new Progreso { Caption = "Obteniendo lista de archivos ..." });
                    List<string> archivos = Directory.GetFiles(carpeta, "*.xml", SearchOption.AllDirectories).ToList<string>();
                    foreach (string item in archivos) {
                        Documento results = Administrador.Agregar(new FileInfo(item), ref this.documentos);
                        if (results != null) {
                            this.documentos.Add(results);
                            reporte.Completado = (this.documentos.Count * 100) / archivos.Count;
                            reporte.Contador = this.documentos.Count;
                            reporte.Caption = string.Format("Proceso: {0} de {1} (Error {3}) | {2} % ", this.documentos.Count, archivos.Count, reporte.Completado, errores);
                        } else {
                            errores += 1;
                            reporte.Caption = string.Format("Ignorando archivo: {0}", item);
                            Documento con = new Documento();
                            con.ArchivoXML = CFDReaderCommon.Load(new FileInfo(item));
                            this.DocumentosError.Add(con);
                        }
                        progreso.Report(reporte);
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            });
            stopwatch.Stop();
            reporte.Caption = string.Format("Se identificaron {0} comprobantes, Tiempo: {1:00}:{2:00}:{3:00}", this.documentos.Count, stopwatch.Elapsed.Hours, stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds);
            progreso.Report(reporte);
            return;
        }

        //public void ExecuteValidacionI(int index)
        //{
        //    ValidadorComprobantes.Validar(this.Documentos[index], this.validador.Configuracion);
        //}

        //public void ExecuteValidacion(IProgress<Progreso> progreso)
        //{
        //    if (this.validador.Configuracion.ModoParalelo)
        //    {
        //        this.validador.RunValidarParallelAsync(Documentos, progreso);
        //    }
        //    else
        //    {
        //        this.validador.RunValidarAsync(this.Documentos, progreso);
        //    }
        //}

        /// <summary>
        /// busqueda del archivo PDF analizando el contenido 
        /// </summary>
        public static string BuscarPDF(FileInfo archivo, string idDocumento) {
            foreach (string item in Directory.GetFiles(archivo.DirectoryName, "*.pdf", SearchOption.AllDirectories)) {
                if (Jaeger.Helpers.HelperPdfExtractor.GetUUID(item).ToUpper() == idDocumento) {
                    return item;
                }
            }
            return string.Empty;
        }

        public string Buscar(string archivo, string idDocumento) {
            return Administrador.BuscarPDF(new FileInfo(archivo), idDocumento);
        }

        public async Task<string> ExecuteOpenZIP(string archivo, string ruta) {
            string output = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(output);
            if (Directory.Exists(output) == false) {

            }
            ruta = output;
            await Task.Run(() => {
                try {
                    var options = new ReadOptions { StatusMessageWriter = System.Console.Out };
                    using (ZipFile zip = ZipFile.Read(archivo, options)) {
                        zip.ExtractAll(ruta, ExtractExistingFileAction.OverwriteSilently);
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            });
            return output;
        }

        public async Task<string> ExecuteCreateZIP(string input) {
            string errorzip = "";

            await Task.Run(() => {
                try {
                    using (ZipFile zip = new ZipFile()) {
                        foreach (Documento item in this.Documentos) {
                            // agregamos archivo XML
                            if (item.ArchivoXML != null) {
                                ZipEntry e = zip.AddEntry(item.KeyName + ".xml", Jaeger.Helpers.HelperFile.FromBase64(item.ArchivoXML.B64));
                                e.Comment = "";
                            }

                            // agregamos representacion impresa PDF
                            if (item.ArchivoPDF != null) {
                                ZipEntry e = zip.AddEntry(item.KeyName + ".pdf", Jaeger.Helpers.HelperFile.FromBase64(item.ArchivoPDF.B64));
                                e.Comment = "Representacion impresa PDF";
                            }

                            if (item.AcuseXML != null) {
                                ZipEntry e = zip.AddEntry(item.KeyName + "-Acuse.xml", Jaeger.Helpers.HelperFile.ReadFileByte(item.AcuseXML.FullFileName));
                                e.Comment = "Acuse de cancelacion XML";
                            }

                            if (item.AcusePDF != null) {
                                ZipEntry e = zip.AddEntry(item.KeyName + "-Acuse.pdf", Jaeger.Helpers.HelperFile.ReadFileByte(item.AcuseXML.FullFileName));
                                e.Comment = "Acuse de cancelacion representación impresa PDF";
                            }
                        }
                        zip.Comment = "Este archivo ZIP fue creado por la aplicación jaeger_tacit_ronin.";
                        zip.Save(input);
                    }
                } catch (Exception ex) {
                    errorzip = "HelperDocumentos:ExecuteCreateZIP: " + ex.Message;
                    Console.WriteLine(errorzip);
                }
            });

            return input;
        }

        public async Task<string> ExecuteCopyTo(string destination, IProgress<Progreso> progreso) {
            await Task.Run(() => {
                foreach (Documento item in this.Documentos) {
                    if (item.ArchivoXML.FileInfo.Exists)
                        File.Copy(item.ArchivoXML.FullFileName, Path.Combine(destination, item.KeyName + ".xml"));

                    if (item.ArchivoPDF.FileInfo.Exists)
                        File.Copy(item.ArchivoPDF.FullFileName, Path.Combine(destination, item.KeyName + ".pdf"));
                    progreso.Report(new Progreso { Caption = "Copiand archivos ... " });
                }
            });

            progreso.Report(new Progreso { Caption = "Se copiaron los archivos a " + destination });
            return destination;
        }

        public async Task<string> ExecuteLogTo(string archivo, IProgress<Progreso> progreso) {
            FileInfo origen = new FileInfo(archivo);
            if (origen.Exists) {
                if (origen.Extension == ".zip") {
                    string output = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                    Directory.CreateDirectory(output);
                    try {
                        var options = new ReadOptions { StatusMessageWriter = System.Console.Out };
                        using (ZipFile zip = ZipFile.Read(archivo, options)) {
                            zip.ExtractAll(output, ExtractExistingFileAction.OverwriteSilently);
                        }
                        archivo = Path.Combine(output, Path.ChangeExtension(origen.Name, ".json"));
                    } catch (Exception ex) {
                        Console.WriteLine(ex.Message);
                        progreso.Report(new Progreso { Caption = ex.Message });
                        return "";
                    }
                }
                origen = new FileInfo(archivo);
                if (origen.Exists) {
                    this.Load(archivo);
                }

            }
            return archivo;
        }

        public bool Renombrar(int index) {
            bool si = false;
            // renombrar xml
            if (this.Documentos[index].ArchivoXML.FileInfo.Exists) {
                try {
                    FileSystem.Rename(this.Documentos[index].ArchivoXML.FullFileName, GenDestName(this.Documentos[index].ArchivoXML.FullFileName, this.Documentos[index].KeyName));
                    this.Documentos[index].ArchivoXML.FullFileName = GenDestName(this.Documentos[index].ArchivoXML.FullFileName, this.Documentos[index].KeyName);
                    si = true;
                } catch (Exception ex) {
                    Console.WriteLine("HelperDocumentos:GetDestName: " + ex.Message);
                }
            }
            // renombrar pdf
            if (this.Documentos[index].ArchivoPDF.FileInfo.Exists) {
                if ((si)) {
                    try {
                        FileSystem.Rename(this.Documentos[index].ArchivoPDF.FullFileName, GenDestName(this.Documentos[index].ArchivoPDF.FullFileName, this.Documentos[index].KeyName));
                        this.Documentos[index].ArchivoPDF.FullFileName = GenDestName(this.Documentos[index].ArchivoPDF.FullFileName, this.Documentos[index].KeyName);
                    } catch (Exception ex) {
                        Console.WriteLine("HelperDocumentos:Renombrar: " + ex.Message);
                    }
                }
            }
            return false;
        }

        public bool Load(string filename) {
            AdministradorBase admin = new AdministradorBase();
            try {

                // configuracion json para la serializacion
                var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy", };
                admin = JsonConvert.DeserializeObject<AdministradorBase>(Jaeger.Util.Helpers.HelperFiles.ReadFileText(filename), conf);
            } catch (Exception ex) {
                Console.WriteLine("Log: " + ex.Message);
                throw;
            }

            if (admin != null) {
                this.Documentos = admin.Documentos;
                this.documentosError = admin.DocumentosError;
                return true;
            }
            return false;
        }

        /// <summary>
        /// almacenar información
        /// </summary>
        public bool Save() {
            AdministradorBase admin = new AdministradorBase();
            admin.Documentos = this.Documentos;
            admin.DocumentosError = this.DocumentosError;

            string nombre = string.Format(@"Validador-{0}", admin.Fecha.ToString("ddMMyyhhmmss"));
            try {
                using (ZipFile zip = new ZipFile()) {
                    byte[] bytes = Encoding.Default.GetBytes(admin.ToJson());
                    string myString = Encoding.UTF8.GetString(bytes);
                    ZipEntry e = zip.AddEntry(nombre + ".json", myString);
                    zip.Comment = "Este archivo ZIP fue creado por la aplicación jaeger_tacit_ronin.";
                    zip.Save(@"C:\Jaeger\Jaeger.Log\" + nombre + ".zip");
                }
                return true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        private string GenDestName(string source, string prepend) {
            try {
                string dir = Path.GetDirectoryName(source);
                string sourceExtn = Path.GetExtension(source);
                string targFname = string.Concat(prepend, sourceExtn);
                string target = Path.Combine(dir, targFname);

                return target;
            } catch (Exception ex) {
                Console.WriteLine("HelperDocumentos:GetDestName: " + ex.Message);
                return source;
            }
        }

    }
}