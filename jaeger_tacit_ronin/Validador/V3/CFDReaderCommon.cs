using System.Text;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Entities.Validador;
using Jaeger.Enums;
using Jaeger.Helpers;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Validador.V2;
using Jaeger.Edita.Helpers;

namespace Jaeger.Validador.V3 {
    public static class CFDReaderCommon {
        public static MetaDocument GetVersion(FileInfo archivo) {
            MetaDocument output = new MetaDocument();
            XmlDocument xmlDoc = new XmlDocument();
            string stringXml = HelperFile.ReadFileText(archivo.FullName);

            if (IsValidXml(stringXml)) {
                try {
                    xmlDoc.LoadXml(stringXml);
                } catch (Exception) {
                    xmlDoc.LoadXml(ExtraeCFDI(stringXml));
                }
            }

            try {
                //primero removemos todas las adendas que pudiera tener
                XElement xElement = XElement.Parse(xmlDoc.InnerXml.ToString());
                XElement xmlAddenda = xElement.Elements().Where(p => p.Name.LocalName == "Addenda").FirstOrDefault();
                if (!(xmlAddenda == null)) {
                    xmlAddenda.Remove();
                }
                xmlDoc.LoadXml(xElement.ToString());
                output = CFDReaderCommon.GetVersion(xmlDoc);
            } catch (Exception ex) {
                Console.WriteLine("HelperCFDIReader:GetVersion: " + ex.Message);
                output.Error = ex.Message;
            } finally {
                xmlDoc = null;
            }
            return output;
        }

        public static MetaDocument GetVersion(XmlDocument objetoXml) {
            XmlNodeList elementsByTagName;
            string strVersion = string.Empty;
            MetaDocument resultado = new MetaDocument(EnumCfdVersion.None, EnumMetadataType.Other);

            try {
                try {
                    elementsByTagName = objetoXml.GetElementsByTagName("Comprobante");
                    if (elementsByTagName.Count != 1) {
                        elementsByTagName = objetoXml.GetElementsByTagName("cfdi:Comprobante");
                        if (elementsByTagName.Count != 1) {
                            elementsByTagName = objetoXml.GetElementsByTagName("retenciones:Retenciones");
                            resultado.MetaType = EnumMetadataType.Retenciones;
                            if (elementsByTagName.Count != 1) {
                                elementsByTagName = objetoXml.GetElementsByTagName("CancelaCFDResult");
                                if (elementsByTagName.Count != 1) {
                                    elementsByTagName = objetoXml.GetElementsByTagName("Acuse");
                                    if (elementsByTagName.Count != 1) {
                                    } else {
                                        resultado.Version = EnumCfdVersion.None;
                                        resultado.MetaType = EnumMetadataType.Acuse;
                                    }
                                } else {
                                    resultado.Version = EnumCfdVersion.None;
                                    resultado.MetaType = EnumMetadataType.Acuse;
                                }
                            } else {
                                strVersion = elementsByTagName[0].Attributes["Version"].Value.ToString();
                                resultado.Version = EnumCfdVersion.V10;
                            }
                        } else {
                            resultado.MetaType = EnumMetadataType.CFDI;
                            try {
                                strVersion = elementsByTagName[0].Attributes["Version"].Value.ToString();
                                if (strVersion == "3.3")
                                    resultado.Version = EnumCfdVersion.V33;
                                if (strVersion == "4.0")
                                    resultado.Version = EnumCfdVersion.V40;
                            } catch (Exception ex) {
                                Console.WriteLine("HelperCFDIReader:GetVersion: " + ex.Message);
                                try {
                                    strVersion = elementsByTagName[0].Attributes["version"].Value.ToString();
                                    resultado.Version = EnumCfdVersion.V32;
                                } catch (Exception) {
                                }
                            }
                        }
                    } else {
                        // sino podemos verifica la version entonces
                        if (HelperCfdi.IsValidXml(objetoXml.OuterXml)) {
                            HelperCfdi.VersionCfdi(objetoXml.OuterXml);
                        } else {
                            if (CFDReaderCommon.Primate(objetoXml) == "3.2") {
                                strVersion = "3.2";
                                resultado.MetaType = EnumMetadataType.CFDI;
                                resultado.Version = EnumCfdVersion.V32;
                            } else if (CFDReaderCommon.Primate(objetoXml) == "3.3") {
                                strVersion = "3.3";
                                resultado.MetaType = EnumMetadataType.CFDI;
                                resultado.Version = EnumCfdVersion.V33;
                            } else if (CFDReaderCommon.Primate(objetoXml) == "4.0") {
                                strVersion = "4.0";
                                resultado.MetaType = EnumMetadataType.CFDI;
                                resultado.Version = EnumCfdVersion.V33;
                            } else {
                                resultado.MetaType = EnumMetadataType.Other;
                            }
                        }
                    }
                } catch (Exception ex) {
                    Console.WriteLine("HelperCFDIReader:GetVersion: " + ex.Message);
                }
            } finally {
                objetoXml = null;
                elementsByTagName = null;
            }
            resultado.VerText = strVersion;
            return resultado;
        }

        public static string Primate(XmlDocument objetoXml) {
            XmlDocument x = new XmlDocument() {
                InnerXml = objetoXml.InnerXml
            };

            try {
                DataSet data = new DataSet();
                data.ReadXml(new XmlNodeReader(x));
                return data.Tables["Comprobante"].Rows[0]["version"].ToString();
            } catch (Exception) {
                return string.Empty;
            }
        }

        public static string SearchUuidText(string stringXml) {
            string idDocument = "";
            string findGuid = stringXml;
            MatchCollection guids = Regex.Matches(findGuid, "(\\{){0,1}[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}(\\}){0,1}");
            int count = checked(guids.Count - 1);
            for (int i = 0; i <= count; i = checked(i + 1)) {
                idDocument = guids[i].Value;
            }
            if (idDocument == findGuid) {
                return "";
            }
            return idDocument;
        }

        public static bool IsValidXml(string stringXml) {
            MatchCollection c = (new Regex("cfdi:Comprobante", RegexOptions.Compiled)).Matches(stringXml);
            if (0 < c.Count) {
                foreach (Match item in c) {
                    string str = item.ToString();
                    if (str.ToLower().Contains("cfdi:comprobante")) {
                        return true;
                    }
                }
            }
            return false;
        }

        public static string TipoComprobante(string clave) {
            if (clave.ToUpper().StartsWith("I")) {
                return "Ingreso";
            } else if (clave.ToUpper().StartsWith("E")) {
                return "Egreso";
            } else if (clave.ToUpper().StartsWith("N")) {
                return "Nomina";
            } else if (clave.ToUpper().StartsWith("T")) {
                return "Traslado";
            } else if (clave.ToUpper().StartsWith("P")) {
                return "Pagos";
            }
            return clave;
        }

        public static string ReaderString(object strValue) {
            if (strValue != DBNull.Value) {
                if (strValue != null) {
                    if (strValue.ToString().Trim() != string.Empty) {
                        return strValue.ToString();
                    }
                }
            }
            return "";
        }

        public static DateTime ReaderDateTime(object dtValue) {
            if (dtValue != DBNull.Value) {
                if (dtValue != null) {
                    if (dtValue.ToString().Trim() != string.Empty) {
                        return DateTime.Parse(dtValue.ToString());
                    }
                }
            }
            return DateTime.MinValue;
        }

        public static DocumentInfo Insert(FileInfo archivo) {
            if (!archivo.Exists) {
                return null;
            } else {
                Jaeger.Edita.Helpers.FileContentTypes fContent = new Jaeger.Edita.Helpers.FileContentTypes();
                DocumentInfo item = new DocumentInfo() {
                    Completed = false,
                    ContentType = fContent.FileContentType(archivo.FullName),
                    FileName = archivo.FullName,
                    FileExt = archivo.Extension
                };

                if (!archivo.Extension.ToLower().Contains("xml")) {
                    item.Base64 = HelperFile.ReadFileB64(archivo.FullName);
                } else {
                    item.Base64 = CFDReaderCommon.FileToBase64(archivo);
                }

                try {
                    item.Size = archivo.Length;
                } catch (Exception ex) {
                    Console.WriteLine("HelperCFDReaderCommon:Insert: " + ex.Message);
                }
                return item;
            }
        }

        public static DocumentoAnexo Load(FileInfo archivo) {
            if (!archivo.Exists) {
                return null;
            } else {
                FileContentTypes fContent = new FileContentTypes();
                DocumentoAnexo item = new DocumentoAnexo() {
                    Completed = false,
                    ContentFile = fContent.FileContentType(archivo.FullName),
                    FullFileName = archivo.FullName,
                };

                if (!archivo.Extension.ToLower().Contains("xml")) {
                    item.B64 = HelperFile.ReadFileB64(archivo.FullName);
                } else {
                    item.B64 = CFDReaderCommon.FileToBase64(archivo);
                }

                return item;
            }
        }

        private static string FileToBase64(FileInfo archivo) {
            UTF8Encoding objUtf8WithoutBom = new UTF8Encoding(false);
            try {
                using (FileStream fileStream = new FileStream(archivo.FullName, FileMode.Open, FileAccess.Read)) {
                    using (StreamReader streamReader = new StreamReader(fileStream, objUtf8WithoutBom)) {
                        return Convert.ToBase64String(Encoding.UTF8.GetBytes(streamReader.ReadToEnd()));
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine("HelperCFDReaderCommon:FileToB64: " + ex.Message);
                return "";
            }
        }

        private static string ExtraeCFDI(string contenidoXml) {
            int posicion1 = contenidoXml.IndexOf("<cfdi:Comprobante");
            int posicion2 = contenidoXml.IndexOf("</cfdi:Comprobante>");
            return contenidoXml.Substring(posicion1, contenidoXml.Length - posicion1);
        }
    }
}