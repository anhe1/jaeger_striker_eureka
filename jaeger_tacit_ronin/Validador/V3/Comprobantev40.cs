// develop: anhe1 251020182327
// purpose: clase para validacion de comprobante fiscal v3.3
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Jaeger.Interface;
using Jaeger.Enums;
using Jaeger.Helpers;
using Jaeger.SAT.Entities;
using Jaeger.Services;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Validador.Helpers;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Validador.V3
{
    public class Comprobantev40 : IValidadorComprobantes
    {
        public Configuracion Configuracion { get; set; }

        /// <summary>
        /// validacion de los esquemas del CFDI
        /// </summary>
        public ValidationProperty Esquemas(object objeto)
        {
            var r = new ValidationProperty { Type = EnumPropertyType.Information, Name = Resources.ValidadorResource.Propiedad_Esquemas, Valid = true };
            var _cfdi40 = (CFDI.V40.Comprobante)objeto;
            if (_cfdi40 != null)
            {
                var esquema = new Esquemas();
                r.Valid = esquema.Schema40(_cfdi40);
                if (r.Valid)
                {
                    r.Value = Resources.ValidadorResource.CFDI_Esquema_Valido;
                    return r;
                }
                r.Value = Resources.ValidadorResource.CFDI_Esquema_No_Valido;
                return r;
            }
            return null;
        }

        /// <summary>
        /// validacion de la estructura del comprobante
        /// </summary>
        public ValidationProperty Estructura(object objeto, byte[] b)
        {
            var _cfdi40 = (CFDI.V40.Comprobante)objeto;
            if (_cfdi40 != null)
            {
                return FormatAndStructure.ValidarFormaYEstructura(b, _cfdi40);
            }
            return null;
        }

        /// <summary>
        /// validar sello del emisor del comprobante CFDI v3.3
        /// </summary>
        public List<ValidationProperty> SelloCFDI(object objeto)
        {
            List<ValidationProperty> lista = new List<ValidationProperty>();
            var _cfdi40 = (Jaeger.CFDI.V40.Comprobante)objeto;

            if (_cfdi40 != null)
            {
                ValidationProperty respuesta = new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_Sello_Emisor, Valid = true, Value = Resources.ValidadorResource.Propiedad_Valido };
                if (CryptoService.ValidaSelloCfdi(_cfdi40))
                {
                    lista.Add(respuesta);
                    CryptoCertificate c = new CryptoCertificate();
                    c.CargarCertificadoDeB64(_cfdi40.Certificado);
                    if (_cfdi40.Fecha >= DateTime.Parse(c.ValidoDesde) & _cfdi40.Fecha <= DateTime.Parse(c.ValidoHasta))
                    {
                        ValidationProperty w = new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_CFDI_CSD, Valid = true };
                        w.Value = string.Format(Resources.ValidadorResource.CFDI_CSD_Vigencia, c.ValidoDesde, c.ValidoHasta);
                        lista.Add(w);
                    }
                    return lista;
                }
                else
                {
                    ValidationProperty w = new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_CFDI_CSD, Valid = false, Value = Resources.ValidadorResource.Propiedad_No_Valido };
                    lista.Add(w);
                }
            }
            else
            {
                ValidationProperty respuesta = new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_Sello_Emisor };
                respuesta.Name = Resources.ValidadorResource.Propiedad_CFDI_CSD;
                respuesta.Value = Resources.ValidadorResource.Propiedad_No_Valido;
                respuesta.Valid = false;
                lista.Add(respuesta);
                return lista;
            }
            return lista;
        }

        /// <summary>
        /// validar sello digital SAT
        /// </summary>
        public ValidationProperty SelloSAT(object objeto) {
            var _cfdi40 = (CFDI.V40.Comprobante)objeto;
            if (_cfdi40 != null) {
                ValidationProperty r = new ValidationProperty() { Type = EnumPropertyType.Attention, Name = Resources.ValidadorResource.Propiedad_Sello_SAT, Value = Resources.ValidadorResource.TFD_Not_Found, Valid = false };
                if (!(_cfdi40.Complemento == null)) {
                    if (!(_cfdi40.Complemento.TimbreFiscalDigital == null)) {
                        // buscamos certificado en el catalogo con el numero de serie valido
                        Certificate c = ValidadorComprobantes.CatalogoCertificados.Search(_cfdi40.Complemento.TimbreFiscalDigital.NoCertificadoSAT);
                        if (c == null) {
                            // sino encontramos el certificado en el catalogo lo buscamos en el FTP del SAT
                            c = DescargaCertificadoSAT.Buscar(_cfdi40.Complemento.TimbreFiscalDigital.NoCertificadoSAT);
                            ValidadorComprobantes.CatalogoCertificados.Add(c);
                            ValidadorComprobantes.CatalogoCertificados.Save();
                        }
                        // si tenemos el certificado, entonces validados el sello 
                        if (c != null) {
                            return TimbreFiscalDigital.Validar(_cfdi40.Complemento.TimbreFiscalDigital, ref ValidadorComprobantes.CatalogoCertificados);
                        }
                    }
                }
                return r;
            }
            return null;
        }

        /// <summary>
        /// validación de la clave del metodo de pago
        /// </summary>
        public ValidationProperty MetodoPago(object objeto)
        {
            var _cfdi40 = (CFDI.V40.Comprobante)objeto;
            if (_cfdi40 != null)
            {
                var r = new ValidationProperty() { Type = EnumPropertyType.Attention, Name = Resources.ValidadorResource.Propiedad_Metodo_Pago, Value = Resources.ValidadorResource.CFDI_No_Aplica, Valid = false };
                if (_cfdi40.TipoDeComprobante != "P")
                {
                    // debemos comprobar que exista el complemento 
                    if (!(_cfdi40.Complemento == null))
                    {
                        if (!(_cfdi40.Complemento.TimbreFiscalDigital == null))
                        {
                            // en esta fecha aplica la clave para el tipo de metodo de pago
                            if (_cfdi40.Complemento.TimbreFiscalDigital.FechaTimbrado >= new DateTime(2022, 1, 1))
                            {
                                if (_cfdi40.MetodoPagoSpecified)
                                {
                                    if (_cfdi40.MetodoPago == "PUE" | _cfdi40.MetodoPago == "PPD" | _cfdi40.MetodoPago == "PIP")
                                    {
                                        r.Value = _cfdi40.MetodoPago;
                                        r.Valid = true;
                                        return r;
                                    }
                                }
                                r.Valid = false;
                                r.Value = Resources.ValidadorResource.Metodo_Pago_Not_Found;
                            }
                            r.Value = Resources.ValidadorResource.Metodo_Pago_No_Aplica_Fecha;
                            r.Valid = true;
                            return r;
                        }
                    }
                    //r.Value = "Falta complemento TimbreFiscalDigital";
                    r.Value = Resources.ValidadorResource.TFD_Not_Found;
                }
                return r;
            }
            return null;
        }

        /// <summary>
        /// Validaro clave de forma de pago SAT
        /// </summary>
        public ValidationProperty FormaPago(object objeto)
        {
            var _cfdi40 = (CFDI.V40.Comprobante)objeto;
            if (_cfdi40 != null)
            {
                if (_cfdi40.TipoDeComprobante != "P")
                {
                    if (_cfdi40.FormaPago != null) {
                        ValidationProperty r = new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_Forma_Pago, Valid = true, Value = Resources.ValidadorResource.Propiedad_Valido };
                        ClaveFormaPago clave = ValidadorComprobantes.CatalogoFormaPago.Search(Regex.Replace(_cfdi40.FormaPago, "[^\\d]", ""));
                        if (!(clave == null))
                        {
                            r.Value = string.Concat("Correcto (", clave.Clave, " ", clave.Descripcion, ")");
                            return r;
                        }
                    }
                    else {
                        ValidationProperty r = new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_Forma_Pago, Value = "El atributo condicional (opcional) para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, no esta disponible en este comprobante.", Valid = false };
                        return r;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// validacion del estado del comprobante con webservice del SAT
        /// </summary>
        public ValidationProperty EstadoSAT(object objeto)
        {
            var _cfdi40 = (CFDI.V40.Comprobante)objeto;
            if (_cfdi40 != null)
            {
                ValidationProperty r = new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_Estado_SAT, Valid = true, Value = Resources.ValidadorResource.Propiedad_Valido };
                if (!(_cfdi40.Complemento == null))
                {
                    if (!(_cfdi40.Complemento.TimbreFiscalDigital == null))
                    {
                        SatQueryResult q = HelperServiceQuerySAT.Query(_cfdi40.Emisor.Rfc, _cfdi40.Receptor.Rfc, _cfdi40.Total, _cfdi40.Complemento.TimbreFiscalDigital.UUID);
                        if (q.Clave == "E")
                        {
                            r.Code = "E";
                            r.Value = "No disponible";
                            r.Valid = false;
                            r.Tag1 = null;
                        }
                        else
                        {
                            r.Code = "R";
                            r.Value = q.Status;
                            r.Valid = true;
                            if (q.Status == null)
                                r.Value = "No Disponible";

                            if (!string.IsNullOrEmpty(q.EFOS)) {
                                r.Tag1 = q.EFOS;
                            }
                        }
                        return r;
                    }
                }
                r.Code = "E";
                r.Value = Resources.ValidadorResource.TFD_Not_Found;
                r.Valid = false;
                return r;
            }
            return null;
        }

        /// <summary>
        /// validacion del lugar de expedición del comprobante 
        /// </summary>
        public ValidationProperty LugarExpedicion(object objeto)
        {
            ValidationProperty r = new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_Expedicion, Valid = true, Value = Resources.ValidadorResource.Propiedad_Valido };
            var _cfdi40 = (Jaeger.CFDI.V40.Comprobante)objeto;
            if (_cfdi40 != null)
            {
                if (!(string.IsNullOrEmpty(_cfdi40.LugarExpedicion)))
                {
                    ClaveCodigoPostal codigo = ValidadorComprobantes.CatalogoCodigoPostal.Search(_cfdi40.LugarExpedicion);
                    if (codigo != null)
                    {
                        r.Value = string.Concat("Correcto (", _cfdi40.LugarExpedicion, ": ", codigo.Estado, ")");
                    }
                    else
                    {
                        r.Value = string.Concat("No válido (", _cfdi40.LugarExpedicion, ": ", codigo.Estado, ")");
                        r.Valid = false;
                    }
                    return r;
                }
                r.Value = Resources.ValidadorResource.Propiedad_No_Disponible;
                r.Valid = false;
                return r;
            }
            return null;
        }

        /// <summary>
        /// validacion de la clave de uso del CFDI
        /// </summary>
        public ValidationProperty UsodeCFDI(object objeto)
        {
            ValidationProperty r = new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_Uso_CFDI, Valid = true, Value = Resources.ValidadorResource.Propiedad_Valido };
            var _cfdi40 = (Jaeger.CFDI.V40.Comprobante)objeto;
            if (_cfdi40 != null)
            {
                try
                {
                    ClaveUsoCFDI clave = ValidadorComprobantes.CatalogoUsoCFDI.Search(_cfdi40.Receptor.UsoCFDI);
                    if (!(clave == null))
                    {
                        r.Value = string.Format(Resources.ValidadorResource.Clave_Uso_CFDI_Valido, clave.Clave, clave.Descripcion);
                    }
                    else
                    {
                        r.Valid = false;
                        r.Value = string.Format(Resources.ValidadorResource.Clave_Uso_CFDI_No_Valido, _cfdi40.Receptor.UsoCFDI);
                    }
                }
                catch (Exception ex)
                {
                    r.Valid = false;
                    r.Value = ex.Message;
                }
                
                return r;
            }
            return null;
        }

        /// <summary>
        /// validacion de las claves de productos y servicios, con el catalogo SAT
        /// </summary>
        public List<ValidationProperty> ProductosServicios(object objeto)
        {
            List<ValidationProperty> r = new List<ValidationProperty>();
            var _cfdi40 = (CFDI.V40.Comprobante)objeto;
            if (_cfdi40 != null)
            {
                if (_cfdi40.Conceptos != null)
                {
                    foreach (Jaeger.CFDI.V40.ComprobanteConcepto item in _cfdi40.Conceptos)
                    {
                        ClaveProdServ clave = ValidadorComprobantes.CatalogoProdServicios.Search(item.ClaveProdServ);
                        if (clave != null)
                        {
                            ClaveUnidad unidad = ValidadorComprobantes.CatalogoUnidades.Search(item.ClaveUnidad);
                            if (unidad == null)
                            {
                                r.Add(new ValidationProperty { 
                                    Type = EnumPropertyType.Attention, Name = Resources.ValidadorResource.Propiedad_Clave_Unidad, Value = string.Format(Resources.ValidadorResource.Clave_Unidad_No_Existe, item.ClaveUnidad) });
                            }
                        }
                        else
                        {
                            r.Add(new ValidationProperty {
                                Type = EnumPropertyType.Attention,
                                Name = Resources.ValidadorResource.Propiedad_Clave_ProdServ,
                                Value = string.Format(Resources.ValidadorResource.Clave_ProdServ_No_Existe, item.ClaveProdServ)
                            });
                        }
                    }

                    if (r.Count > 0)
                    {
                        r.Add(new ValidationProperty {
                            Type = EnumPropertyType.Attention,
                            Name = Resources.ValidadorResource.Propiedad_Clave_ProdServ,
                            Value = Resources.ValidadorResource.Clave_ProdServ_No_Valido,
                            Valid = false
                        });
                    }
                    else
                    {
                        r.Add(new ValidationProperty {
                            Type = EnumPropertyType.Information,
                            Name = Resources.ValidadorResource.Propiedad_Clave_ProdServ,
                            Value = "Claves Correctas.",
                            Valid = true
                        });
                    }
                }
                else
                {
                    r.Add(new ValidationProperty {
                        Type = EnumPropertyType.Attention,
                        Name = Resources.ValidadorResource.Propiedad_Clave_ProdServ,
                        Value = "No existe nodo de conceptos."
                    });
                }
                return r;
            }
            return null;
        }

        /// <summary>
        /// comprobar rfc del emisor del comprobante en el catalog 69B de operaciones simuladas
        /// </summary>
        public List<ValidationProperty> Articulo69B(object objeto)
        {
            List<ValidationProperty> response = new List<ValidationProperty>();
            var _cfdi40 = (CFDI.V40.Comprobante)objeto;
            var prueba = ValidadorComprobantes.articulo69B.Search(_cfdi40.Emisor.Rfc);
            if (prueba != null)
            {
                response.Add(new ValidationProperty
                {
                    Type = EnumPropertyType.Information,
                    Name = "Artículo 69B Situación: ",
                    Value = string.Format("{0} \r\n Oficio: {1}", prueba.Situacion, prueba.Presunto.NoOficioGlobal) ,
                    Valid = false
                });
            }
            else
            {
                response.Add(new ValidationProperty
                {
                    Type = EnumPropertyType.Information,
                    Name = "Artículo 69B Situación: ",
                    Value = "Sin reporte",
                    Valid = true
                });
            }
            
            return response;
        }
    }
}