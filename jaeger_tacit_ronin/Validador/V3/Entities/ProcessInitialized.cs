using System;

namespace Jaeger.Validador.V3.Entities
{
    public class ProcessInitialized : EventArgs
    {
        public ProcessInitialized()
        {
        }

        public string Caption { get; set; }
    }
}