using System;

namespace Jaeger.Validador.V3.Entities
{
    public class ProcessCompleted : EventArgs
    {
        public ProcessCompleted()
        {
        }

        public string Caption { get; set; }
    }
}