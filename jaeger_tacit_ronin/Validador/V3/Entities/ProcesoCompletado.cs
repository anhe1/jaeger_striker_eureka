using System;

namespace Jaeger.Validador.V3.Entities
{
    public class ProcesoCompletado : EventArgs
    {
        public ProcesoCompletado()
        {
            this.Caption = "Completado";
        }

        public ProcesoCompletado(string caption)
        {
            this.Caption = caption;
        }

        public string Caption { get; set; }
    }
}