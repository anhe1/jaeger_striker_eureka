using System;

namespace Jaeger.Validador.V3.Entities
{
    public class ProcessChanged : EventArgs
    {
        public ProcessChanged()
        {
        }

        public string Caption { get; set; }
    }
}