﻿using System;

namespace Jaeger.Validador.V3.Entities
{
    public class Progreso : EventArgs
    {
        public Progreso()
        {
            this.Terminado = false;
        }

        public Progreso(string caption)
        {
            this.Caption = caption;
            this.Terminado = false;
        }

        public Progreso(int completado, int contador, string caption, bool terminado = false)
        {
            this.Completado = completado;
            this.Contador = contador;
            this.Caption = caption;
            this.Terminado = terminado;
        }

        /// <summary>
        /// obtener o establecer el % completado
        /// </summary>
        public int Completado { get; set; }

        /// <summary>
        /// obtener o establecer el contador de items
        /// </summary>
        public int Contador { get; set; }

        /// <summary>
        /// obtener o establecer texto
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// obtener o establecer si el proceso esta terminado
        /// </summary>
        public bool Terminado { get; set; }
    }
}
