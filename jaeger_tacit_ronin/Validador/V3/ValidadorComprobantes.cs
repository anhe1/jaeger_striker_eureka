﻿/// develop: anhe 251020182340
/// purpose: clase para validacion de comprobantes fiscales
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Diagnostics;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Interface;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Enums;
using Jaeger.Validador.Helpers;
using Jaeger.Validador.V3.Entities;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.Validador.V3 {
    public class ValidadorComprobantes {
        private string version = "3.0";
        private Edita.V2.Validador.Entities.Configuracion configuracion;

        public static FormaPagoCatalogo CatalogoFormaPago;
        public static CodigoPostalCatalogo CatalogoCodigoPostal;
        public static UsoCFDICatalogo CatalogoUsoCFDI;
        public static ProdServsCatalogo CatalogoProdServicios;
        public static UnidadesCatalogo CatalogoUnidades;
        public static CertificadosCatalogo CatalogoCertificados;
        public static RelacionCFDICatalogo CatalogoRelacionCFDI;
        public static Articulo69BCatalogo articulo69B;

        /// <summary>
        /// constructor
        /// </summary>
        public ValidadorComprobantes(Edita.V2.Validador.Entities.Configuracion objeto) {
            this.configuracion = objeto;
            // catalogos a usar
            CatalogoFormaPago = new FormaPagoCatalogo();
            CatalogoFormaPago.Load();
            CatalogoCodigoPostal = new CodigoPostalCatalogo();
            CatalogoCodigoPostal.Load();
            CatalogoProdServicios = new ProdServsCatalogo();
            CatalogoProdServicios.Load();
            CatalogoUnidades = new UnidadesCatalogo();
            CatalogoUnidades.Load();
            CatalogoUsoCFDI = new UsoCFDICatalogo();
            CatalogoUsoCFDI.Load();
            CatalogoCertificados = new CertificadosCatalogo();
            CatalogoCertificados.Load();
            CatalogoRelacionCFDI = new RelacionCFDICatalogo();
            CatalogoRelacionCFDI.Load();
            articulo69B = new Articulo69BCatalogo();
            articulo69B.Load();
        }

        #region propiedades

        public Jaeger.Edita.V2.Validador.Entities.Configuracion Configuracion {
            get {
                return this.configuracion;
            }
            set {
                this.configuracion = value;
            }
        }

        public string Version {
            get {
                return this.version;
            }
            set {
                this.version = value;
            }
        }

        #endregion

        /// <summary>
        /// validacion de comprobantes fiscales, de manera paralera asincronica.
        /// </summary>
        public async Task RunValidarParallelAsync(BindingList<Documento> objetos, IProgress<Progreso> progress) {

            Progreso reporte = new Progreso();
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            int contador = 0;
            stopwatch.Start();

            await Task.Run(() => {
                Parallel.ForEach<Documento>(objetos, (site) => {
                    Documento results = Validar(site, this.configuracion);
                    reporte.Completado = (contador * 100) / objetos.Count;
                    reporte.Contador = contador;
                    reporte.Caption = string.Format("Validando {0} de {1} | {2} % Completado. Tiempo transcurrido: {3:00}:{4:00}:{5:00}", contador, objetos.Count, reporte.Completado, stopwatch.Elapsed.Hours, stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds);
                    contador = contador + 1;
                    progress.Report(reporte);
                });
            });

            stopwatch.Stop();
            reporte.Caption = string.Format("Se validaron: {0} comprobantes | Duración: {1:00}:{2:00}:{3:00}", contador, stopwatch.Elapsed.Hours, stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds);
            reporte.Terminado = true;
            progress.Report(reporte);
            return;
        }

        public async Task RunValidarAsync(BindingList<Documento> objetos, IProgress<Progreso> progress) {
            Progreso reporte = new Progreso();
            Stopwatch stopwatch = new Stopwatch();
            int contador = 0;
            stopwatch.Start();

            await Task.Run(() => {
                foreach (Documento item in objetos) {
                    Documento results = Validar(item, this.configuracion);
                    reporte.Completado = (contador * 100) / objetos.Count;
                    reporte.Contador = contador;
                    reporte.Caption = string.Format("Validando {0} de {1} | {2} % Completado. Tiempo transcurrido: {3}", contador, objetos.Count, reporte.Completado, stopwatch.Elapsed);
                    contador = contador + 1;
                    progress.Report(reporte);
                }
            });

            stopwatch.Stop();
            reporte.Caption = string.Format("Se validaron: {0} comprobantes | Duración: {1:00}:{2:00}:{3:00}", contador, stopwatch.Elapsed.Hours, stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds);
            reporte.Terminado = true;
            progress.Report(reporte);
            return;
        }

        public Documento RunValidar(Documento objeto, IProgress<Progreso> progress) {
            Progreso reporte = new Progreso();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Stop();
            objeto = Validar(objeto, this.configuracion);
            reporte.Caption = string.Format("Se validaron: {0} comprobantes | Duración: {1:00}:{2:00}:{3:00}", 1, stopwatch.Elapsed.Hours, stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds);
            reporte.Terminado = true;
            progress.Report(reporte);
            return objeto;
        }

        /// <summary>
        /// validar comprobante
        /// </summary>
        private static Documento Validar(Documento objeto, Jaeger.Edita.V2.Validador.Entities.Configuracion conf) {
            var _fecha = DateTime.Now;
            ValidateResponse respuesta = new ValidateResponse();
            Byte[] bytearray = Jaeger.Util.Helpers.HelperFiles.FromBase64(objeto.ArchivoXML.B64);
            object comprobante = new object();

            IValidadorComprobantes iValidador;
            if (objeto.MetaData == EnumMetadataType.CFDI & objeto.Version == EnumCfdVersion.V32) {
                comprobante = CFDI.V32.Comprobante.Deserialize(System.Text.Encoding.UTF8.GetString(bytearray));
                iValidador = new Comprobantev32();
            } else if (objeto.MetaData == EnumMetadataType.CFDI & objeto.Version == EnumCfdVersion.V33) {
                comprobante = CFDI.V33.Comprobante.Deserialize(System.Text.Encoding.UTF8.GetString(bytearray));
                iValidador = new Comprobantev33();
                iValidador.Configuracion = conf;
            } else if (objeto.MetaData == EnumMetadataType.CFDI & objeto.Version == EnumCfdVersion.V40) {
                comprobante = CFDI.V40.Comprobante.Deserialize(System.Text.Encoding.UTF8.GetString(bytearray));
                iValidador = new Comprobantev40();
                iValidador.Configuracion = conf;
            } else {
                iValidador = null;
            }

            // datos generales del comprobante
            respuesta.Provider = Enum.GetName(typeof(Domain.Empresa.Entities.ServiceProvider.EnumServicePAC), Domain.Empresa.Entities.ServiceProvider.EnumServicePAC.Interno);
            respuesta.Version = "3.0";
            respuesta.Uuid = objeto.IdDocumento;
            respuesta.Effect = objeto.TipoComprobante;
            respuesta.Folio = objeto.Folio;
            respuesta.Serie = objeto.Serie;
            respuesta.DateTime = (DateTime)objeto.Fecha;
            respuesta.IssueName = objeto.Emisor;
            respuesta.IssueRfcId = objeto.EmisorRFC;
            respuesta.CustomerName = objeto.Receptor;
            respuesta.CustomerRfcId = objeto.ReceptorRFC;
            respuesta.Total = objeto.Total;
            respuesta.Validations.Add(new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_Tipo_Comprobante, Value = Enum.GetName(typeof(EnumMetadataType), objeto.MetaData) });
            respuesta.Validations.Add(new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_CFDI_Version, Value = objeto.VersionText });
            respuesta.Validations.Add(new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_NoCertificado_SAT, Value = objeto.NoCertificadoProvCertif });
            respuesta.Validations.Add(new ValidationProperty { Name = Resources.ValidadorResource.Propiedad_NoCertificado, Value = objeto.NoCertificado });

            // validaciones
            respuesta.FechaValidacion = DateTime.Now;
            respuesta.Validations.Add(iValidador.Esquemas(comprobante));
            respuesta.Validations.Add(iValidador.Estructura(comprobante, bytearray));
            respuesta.Validations.AddRange(iValidador.SelloCFDI(comprobante));

            ValidationProperty estadosat = iValidador.EstadoSAT(comprobante);
            objeto.EstadoSAT = estadosat.Value;
            respuesta.Validations.Add(estadosat);

            ValidationProperty sellosat = iValidador.SelloSAT(comprobante);
            respuesta.Validations.Add(sellosat);

            respuesta.Validations.Add(iValidador.MetodoPago(comprobante));
            respuesta.Validations.Add(iValidador.FormaPago(comprobante));
            // validar el estado del comprobante con el webservice SAT
            respuesta.Validations.Add(iValidador.LugarExpedicion(comprobante));
            respuesta.Validations.Add(Codificacion.ValidateUTF8Encode(bytearray));

            if (conf.ClaveUsoCFDI)
                respuesta.Validations.Add(iValidador.UsodeCFDI(comprobante));

            if (conf.ClaveProductoServicio)
                respuesta.Validations.AddRange(iValidador.ProductosServicios(comprobante));

            if (conf.Articulo69B) {
                if (!string.IsNullOrEmpty(estadosat.Tag1)) {
                    if (estadosat.Tag1 == "200" | estadosat.Tag1 == "201") {
                        respuesta.Validations.Add(new ValidationProperty {
                            Type = EnumPropertyType.Information,
                            Name = "Artículo 69B Situación: ",
                            Value = "Sin Reporte",
                            Valid = true
                        });
                    }
                } else {
                    respuesta.Validations.AddRange(iValidador.Articulo69B(comprobante));
                }
            }

            objeto.Validacion = respuesta;

            if (conf.EstadoSAT) {
                //// comprobar resultados de las validacions
                if (sellosat.Value != null) {
                    if (sellosat.Valid & (estadosat.Value.Contains("Vig") | estadosat.Value.Contains("Can")))
                        respuesta.IsValid = EnumValidateResult.Valido;
                    else if (sellosat.Valid & sellosat.Value.Contains("No"))
                        respuesta.IsValid = EnumValidateResult.NoValido;
                    else if (sellosat.Valid == false & sellosat.Value.Contains("Vig")) {
                        respuesta.Validations.Add(new ValidationProperty() {
                            Type = EnumPropertyType.Information,
                            Name = "Comprobante: ",
                            Value = Resources.ValidadorResource.Tipo_Comprobante_No_Soportado
                        });
                        respuesta.IsValid = EnumValidateResult.Valido;
                    } else
                        respuesta.IsValid = EnumValidateResult.NoValido;
                } else {
                    if (sellosat.Valid & sellosat.Value.Contains("No"))
                        respuesta.IsValid = EnumValidateResult.NoValido;
                    else
                        respuesta.IsValid = EnumValidateResult.Valido;
                }
            } else {
                if (sellosat.Value != null) {
                    if (sellosat.Valid)
                        respuesta.IsValid = EnumValidateResult.Valido;
                    else
                        respuesta.IsValid = EnumValidateResult.NoValido;
                } else {
                    respuesta.Validations.Add(new ValidationProperty() {
                        Type = EnumPropertyType.Information,
                        Name = "Comprobante: ",
                        Value = Resources.ValidadorResource.Tipo_Comprobante_No_Soportado
                    });
                    respuesta.IsValid = EnumValidateResult.NoValido;
                }
            }

            return objeto;
        }

    }
}
