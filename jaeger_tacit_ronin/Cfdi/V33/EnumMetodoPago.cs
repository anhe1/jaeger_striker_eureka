namespace Jaeger.CFDI.V33
{
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.sat.gob.mx/sitio_internet/cfd/catalogos")]
    public enum EnumMetodoPago
    {
        /// <comentarios/>
        PUE,
    
        /// <comentarios/>
        PIP,
    
        /// <comentarios/>
        PPD,
    }
}