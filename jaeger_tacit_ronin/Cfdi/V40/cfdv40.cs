﻿using System;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Xml.Xsl;
using System.Xml.XPath;
using Jaeger.CFDI.Complemento.TimbreFiscal.V11;
using Jaeger.CFDI.Complemento.Aerolineas.V10;
using Jaeger.CFDI.Complemento.ImpuestosLocales.V10;
using Jaeger.CFDI.Complemento.LeyendasFiscales.V10;
using Jaeger.CFDI.Complemento.ValesDeDespensa.V10;
using Jaeger.Helpers;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.CFDI.Complemento.Pagos.V20;

namespace Jaeger.CFDI.V40 {
    // 
    // Este código fuente fue generado automáticamente por xsd, Versión=4.8.3928.0.
    // 
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.sat.gob.mx/cfd/4", IsNullable = false)]
    public partial class Comprobante {
        [XmlAttribute("schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string XsiSchemaLocation;

        private ComprobanteInformacionGlobal informacionGlobalField;

        private ComprobanteCfdiRelacionados[] cfdiRelacionadosField;

        private ComprobanteEmisor emisorField;

        private ComprobanteReceptor receptorField;

        private ComprobanteConcepto[] conceptosField;

        private ComprobanteImpuestos impuestosField;

        private ComprobanteComplemento complementoField;

        private ComprobanteAddenda addendaField;

        private string versionField;

        private string serieField;

        private string folioField;

        private System.DateTime fechaField;

        private string selloField;

        private string formaPagoField;

        private bool formaPagoFieldSpecified;

        private string noCertificadoField;

        private string certificadoField;

        private string condicionesDePagoField;

        private decimal subTotalField;

        private decimal descuentoField;

        private bool descuentoFieldSpecified;

        private string monedaField;

        private decimal tipoCambioField;

        private bool tipoCambioFieldSpecified;

        private decimal totalField;

        private string tipoDeComprobanteField;

        private string exportacionField;

        private string metodoPagoField;

        private bool metodoPagoFieldSpecified;

        private string lugarExpedicionField;

        private string confirmacionField;

        public Comprobante() {
            this.versionField = "4.0";
        }

        /// <summary>
        /// Nodo condicional para precisar la información relacionada con el comprobante global.
        /// </summary>
        public ComprobanteInformacionGlobal InformacionGlobal {
            get {
                return this.informacionGlobalField;
            }
            set {
                this.informacionGlobalField = value;
            }
        }

        /// <remarks/>
        [XmlElementAttribute("CfdiRelacionados")]
        public ComprobanteCfdiRelacionados[] CfdiRelacionados {
            get {
                return this.cfdiRelacionadosField;
            }
            set {
                this.cfdiRelacionadosField = value;
            }
        }

        /// <remarks/>
        public ComprobanteEmisor Emisor {
            get {
                return this.emisorField;
            }
            set {
                this.emisorField = value;
            }
        }

        /// <remarks/>
        public ComprobanteReceptor Receptor {
            get {
                return this.receptorField;
            }
            set {
                this.receptorField = value;
            }
        }

        /// <remarks/>
        [XmlArrayItemAttribute("Concepto", IsNullable = false)]
        public ComprobanteConcepto[] Conceptos {
            get {
                return this.conceptosField;
            }
            set {
                this.conceptosField = value;
            }
        }

        /// <remarks/>
        public ComprobanteImpuestos Impuestos {
            get {
                return this.impuestosField;
            }
            set {
                this.impuestosField = value;
            }
        }

        /// <remarks/>
        public ComprobanteComplemento Complemento {
            get {
                return this.complementoField;
            }
            set {
                this.complementoField = value;
            }
        }

        /// <remarks/>
        public ComprobanteAddenda Addenda {
            get {
                return this.addendaField;
            }
            set {
                this.addendaField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Serie {
            get {
                return this.serieField;
            }
            set {
                this.serieField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Folio {
            get {
                return this.folioField;
            }
            set {
                this.folioField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public System.DateTime Fecha {
            get {
                return this.fechaField;
            }
            set {
                this.fechaField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Sello {
            get {
                return this.selloField;
            }
            set {
                this.selloField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string FormaPago {
            get {
                return this.formaPagoField;
            }
            set {
                this.formaPagoField = value;
            }
        }

        /// <remarks/>
        [XmlIgnoreAttribute()]
        public bool FormaPagoSpecified {
            get {
                return this.formaPagoFieldSpecified;
            }
            set {
                this.formaPagoFieldSpecified = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string NoCertificado {
            get {
                return this.noCertificadoField;
            }
            set {
                this.noCertificadoField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Certificado {
            get {
                return this.certificadoField;
            }
            set {
                this.certificadoField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string CondicionesDePago {
            get {
                return this.condicionesDePagoField;
            }
            set {
                this.condicionesDePagoField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal SubTotal {
            get {
                return this.subTotalField;
            }
            set {
                this.subTotalField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal Descuento {
            get {
                return this.descuentoField;
            }
            set {
                this.descuentoField = value;
            }
        }

        /// <remarks/>
        [XmlIgnoreAttribute()]
        public bool DescuentoSpecified {
            get {
                return this.descuentoFieldSpecified;
            }
            set {
                this.descuentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Moneda {
            get {
                return this.monedaField;
            }
            set {
                this.monedaField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal TipoCambio {
            get {
                return this.tipoCambioField;
            }
            set {
                this.tipoCambioField = value;
            }
        }

        /// <remarks/>
        [XmlIgnoreAttribute()]
        public bool TipoCambioSpecified {
            get {
                return this.tipoCambioFieldSpecified;
            }
            set {
                this.tipoCambioFieldSpecified = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal Total {
            get {
                return this.totalField;
            }
            set {
                this.totalField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string TipoDeComprobante {
            get {
                return this.tipoDeComprobanteField;
            }
            set {
                this.tipoDeComprobanteField = value;
            }
        }

        /// <summary>
        /// Atributo requerido para expresar si el comprobante ampara una operación de exportación.
        /// </summary>
        [XmlAttributeAttribute()]
        public string Exportacion {
            get {
                return this.exportacionField;
            }
            set {
                this.exportacionField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string MetodoPago {
            get {
                return this.metodoPagoField;
            }
            set {
                this.metodoPagoField = value;
            }
        }

        /// <remarks/>
        [XmlIgnoreAttribute()]
        public bool MetodoPagoSpecified {
            get {
                return this.metodoPagoFieldSpecified;
            }
            set {
                this.metodoPagoFieldSpecified = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string LugarExpedicion {
            get {
                return this.lugarExpedicionField;
            }
            set {
                this.lugarExpedicionField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Confirmacion {
            get {
                return this.confirmacionField;
            }
            set {
                this.confirmacionField = value;
            }
        }

        #region serializacion

        private static XmlSerializer objSerializer;

        private long idField;
        [XmlIgnore]
        public long Id {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
            }
        }

        [XmlIgnore]
        public Jaeger.Edita.V2.CFDI.Enums.EnumCfdiSubType Type { get; set; }

        private ValidateResponse objValidation;
        [XmlIgnore]
        public ValidateResponse Validation {
            get {
                return this.objValidation;
            }
            set {
                this.objValidation = value;
            }
        }

        [XmlIgnore]
        public string OriginalXmlString { get; set; }

        private static XmlSerializer Serializer {
            get {
                if (Comprobante.objSerializer == null) {
                    Comprobante.objSerializer = (new XmlSerializerFactory()).CreateSerializer(typeof(Comprobante));
                }
                return Comprobante.objSerializer;
            }
        }

        /// <summary>
        /// Deserialize
        /// </summary>
        public static Comprobante Deserialize(string input) {
            Comprobante objComprobante;
            using (StringReader stringReader = new StringReader(input)) {
                Comprobante objComprobante1 = (Comprobante)Comprobante.Serializer.Deserialize(XmlReader.Create(stringReader));
                objComprobante1.OriginalXmlString = input;
                objComprobante = objComprobante1;
            }
            return objComprobante;
        }

        public static Comprobante Load(string fileName) {
            Comprobante comprobante;
            UTF8Encoding objUtf8WithoutBom = new UTF8Encoding(false);
            try {
                using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read)) {
                    using (StreamReader streamReader = new StreamReader(fileStream, objUtf8WithoutBom)) {
                        comprobante = Comprobante.Deserialize(streamReader.ReadToEnd());
                    }
                }
                return comprobante;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        /// <summary>
        /// cargar
        /// </summary>
        public static Comprobante LoadXml(string xmlString) {
            try {
                return Comprobante.Deserialize(xmlString);
            } catch (Exception ex) {
                Console.WriteLine(string.Concat("V33_LoadXML: ", ex.Message));
                return null;
            }
        }

        /// <summary>
        /// cargar xml de un arreglo de bytes
        /// </summary>
        public static Comprobante LoadBytes(byte[] xmlBytes) {
            Comprobante outComprobante;
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Comprobante));
                XmlTextReader xmlTextReader = new XmlTextReader(new MemoryStream(xmlBytes));
                Comprobante t1 = (Comprobante)xmlSerializer.Deserialize(xmlTextReader);
                xmlTextReader.Close();
                outComprobante = t1;
            } catch (Exception ex) {
                Console.WriteLine("CFDIv33: " + ex.Message);
                return null;
            }
            return outComprobante;
        }

        public virtual void Save(string fileName) {
            using (StreamWriter streamWriter = new StreamWriter(fileName, false, new UTF8Encoding(false))) {
                streamWriter.WriteLine(this.Serialize());
            }
        }

        public virtual string Serialize() {
            string end;
            List<XmlElement>.Enumerator enumerator = new List<XmlElement>.Enumerator();
            XmlWriterSettings xmlWriterSetting = new XmlWriterSettings() {
                Encoding = Encoding.UTF8,
                Indent = true,
                CloseOutput = false,
                OmitXmlDeclaration = false
            };
            using (MemoryStream memoryStream = new MemoryStream()) {
                XmlWriter xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSetting);
                XmlSerializerNamespaces xmlSerializerNamespace = new XmlSerializerNamespaces();
                xmlSerializerNamespace.Add("cfdi", "http://www.sat.gob.mx/cfd/3");
                xmlSerializerNamespace.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                this.XsiSchemaLocation = "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd";
                if (this.Complemento != null) {
                    try {
                        enumerator = this.Complemento.Any.GetEnumerator();
                        while (enumerator.MoveNext()) {
                            string name = enumerator.Current.Name;
                            string str = name;
                            if (name != null) {
                                if (str == "nomina12:Nomina") {
                                    xmlSerializerNamespace.Add("nomina12", "http://www.sat.gob.mx/nomina12");
                                    this.XsiSchemaLocation = string.Concat(this.XsiSchemaLocation, " http://www.sat.gob.mx/nomina http://www.sat.gob.mx/sitio_internet/cfd/nomina/nomina12.xsd");
                                } else if (str == "pago10:Pagos") {
                                    xmlSerializerNamespace.Add("pagos10", "http://www.sat.gob.mx/Pagos");
                                    this.XsiSchemaLocation = "http://www.sat.gob.mx/Pagos http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd";
                                } else if (str == "implocal:ImpuestosLocales") {
                                    xmlSerializerNamespace.Add("implocal", "http://www.sat.gob.mx/implocal");
                                    this.XsiSchemaLocation = string.Concat(this.XsiSchemaLocation, " http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd");
                                }
                            }
                        }
                    } finally {
                        ((IDisposable)enumerator).Dispose();
                    }
                }
                this.Addenda = null;
                Comprobante.Serializer.Serialize(xmlWriter, this, xmlSerializerNamespace);
                memoryStream.Seek((long)0, SeekOrigin.Begin);
                using (StreamReader streamReader = new StreamReader(memoryStream, Encoding.UTF8)) {
                    end = streamReader.ReadToEnd();
                }
            }
            return end;
        }

        [XmlIgnore]
        public bool SelloValido {
            get {
                X509Certificate2 x509Certificate2 = new X509Certificate2();
                x509Certificate2.Import(Convert.FromBase64String(this.certificadoField));
                RSACryptoServiceProvider key = (RSACryptoServiceProvider)x509Certificate2.PublicKey.Key;
                bool selloValido = key.VerifyData(Encoding.UTF8.GetBytes(this.CadenaOriginal), CryptoConfig.MapNameToOID("SHA256"), Convert.FromBase64String(this.Sello));
                return selloValido;
            }
        }

        [XmlIgnore]
        public string CadenaOriginal {
            get {
                try {
                    string outOriginalString;
                    using (Stream manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Jaeger.xslt.cadenaoriginal_4_0.xslt")) {
                        if (manifestResourceStream == null) {
                            outOriginalString = null;
                        } else {
                            XmlDocument xmlDocument = new XmlDocument();
                            xmlDocument.Load(manifestResourceStream);
                            using (StringWriter stringWriter = new StringWriter()) {
                                using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter)) {
                                    XslCompiledTransform xslCompiledTransform = new XslCompiledTransform();
                                    xslCompiledTransform.Load(xmlDocument, null, new Resolver());
                                    xslCompiledTransform.Transform(new XPathDocument(new StringReader(this.Serialize())), null, xmlTextWriter);
                                    outOriginalString = stringWriter.ToString().Replace("&amp;", "&");
                                }
                            }
                        }
                    }
                    return outOriginalString;
                } catch (Exception e) {
                    Console.WriteLine(e.Message);
                    throw;
                }

            }
        }

        [XmlIgnore]
        public string CadenaOriginalOff {
            get {
                string outOriginalString;
                using (Stream manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Jaeger.xslt.cadenaoriginal_4_0_offline.xslt")) {
                    if (manifestResourceStream == null) {
                        outOriginalString = null;
                    } else {
                        XmlDocument xmlDocument = new XmlDocument();
                        xmlDocument.Load(manifestResourceStream);
                        using (StringWriter stringWriter = new StringWriter()) {
                            using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter)) {
                                XslCompiledTransform xslCompiledTransform = new XslCompiledTransform();
                                xslCompiledTransform.Load(xmlDocument, null, new Resolver());
                                xslCompiledTransform.Transform(new XPathDocument(new StringReader(this.Serialize())), null, xmlTextWriter);
                                outOriginalString = stringWriter.ToString().Replace("&amp;", "&");
                            }
                        }
                    }
                }
                return outOriginalString;
            }
        }

        #endregion
    }

    /// <summary>
    /// Nodo condicional para precisar la información relacionada con el comprobante global.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteInformacionGlobal {

        private string periodicidadField;

        private string mesesField;

        private short añoField;

        /// <summary>
        /// Atributo requerido para expresar el período al que corresponde la información del comprobante global.
        /// </summary>
        [XmlAttributeAttribute()]
        public string Periodicidad {
            get {
                return this.periodicidadField;
            }
            set {
                this.periodicidadField = value;
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el mes o los meses al que corresponde la información del comprobante global.
        /// </summary>
        [XmlAttributeAttribute()]
        public string Meses {
            get {
                return this.mesesField;
            }
            set {
                this.mesesField = value;
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el año al que corresponde la información del comprobante global.
        /// </summary>
        [XmlAttributeAttribute()]
        public short Año {
            get {
                return this.añoField;
            }
            set {
                this.añoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteCfdiRelacionados {

        private ComprobanteCfdiRelacionadosCfdiRelacionado[] cfdiRelacionadoField;

        private string tipoRelacionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CfdiRelacionado")]
        public ComprobanteCfdiRelacionadosCfdiRelacionado[] CfdiRelacionado {
            get {
                return this.cfdiRelacionadoField;
            }
            set {
                this.cfdiRelacionadoField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string TipoRelacion {
            get {
                return this.tipoRelacionField;
            }
            set {
                this.tipoRelacionField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteCfdiRelacionadosCfdiRelacionado {

        private string uUIDField;

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string UUID {
            get {
                return this.uUIDField;
            }
            set {
                this.uUIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteEmisor {

        private string rfcField;

        private string nombreField;

        private string regimenFiscalField;

        private string facAtrAdquirenteField;

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Rfc {
            get {
                return this.rfcField;
            }
            set {
                this.rfcField = value;
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el nombre, denominación o razón social del contribuyente inscrito en el RFC, del emisor del comprobante.
        /// 
        /// </summary>
        [XmlAttributeAttribute()]
        public string Nombre {
            get {
                return this.nombreField;
            }
            set {
                this.nombreField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string RegimenFiscal {
            get {
                return this.regimenFiscalField;
            }
            set {
                this.regimenFiscalField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string FacAtrAdquirente {
            get {
                return this.facAtrAdquirenteField;
            }
            set {
                this.facAtrAdquirenteField = value;
            }
        }
    }

    /// <summary>
    /// Nodo requerido para precisar la información del contribuyente receptor del comprobante.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteReceptor {

        private string rfcField;

        private string nombreField;

        private string domicilioFiscalReceptorField;

        private string residenciaFiscalField;

        private bool residenciaFiscalFieldSpecified;

        private string numRegIdTribField;

        private string regimenFiscalReceptorField;

        private string usoCFDIField;

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Rfc {
            get {
                return this.rfcField;
            }
            set {
                this.rfcField = value;
            }
        }

        /// <summary>
        /// Se debe registrar el nombre, primer apellido, segundo apellido, según corresponda denominación o razón social registrados en el RFC del contribuyente receptor del comprobante.
        /// En el caso de personas morales se debe adicionar el régimen societario.
        /// </summary>
        [XmlAttributeAttribute()]
        public string Nombre {
            get {
                return this.nombreField;
            }
            set {
                this.nombreField = value;
            }
        }

        /// <summary>
        /// Se debe registrar el código postal del domicilio fiscal del receptor del comprobante. El código postal, en caso de que sea diferente de los RFC genéricos, debe estar asociado a 
        /// la clave de RFC registrado en el campo Rfc de este Nodo.
        /// </summary>
        [XmlAttributeAttribute()]
        public string DomicilioFiscalReceptor {
            get {
                return this.domicilioFiscalReceptorField;
            }
            set {
                this.domicilioFiscalReceptorField = value;
            }
        }

        /// <summary>
        /// Atributo condicional para registrar la clave del país de residencia para efectos fiscales del receptor del comprobante, cuando se trate de un extranjero, y que es conforme con 
        /// la especificación ISO 3166-1 alpha-3. Es requerido cuando se incluya el complemento de comercio exterior o se registre el atributo NumRegIdTrib.
        /// </summary>
        [XmlAttributeAttribute()]
        public string ResidenciaFiscal {
            get {
                return this.residenciaFiscalField;
            }
            set {
                this.residenciaFiscalField = value;
            }
        }

        /// <remarks/>
        [XmlIgnoreAttribute()]
        public bool ResidenciaFiscalSpecified {
            get {
                return this.residenciaFiscalFieldSpecified;
            }
            set {
                this.residenciaFiscalFieldSpecified = value;
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el número de registro de identidad fiscal del receptor cuando sea residente en el extranjero. Es requerido cuando se incluya el complemento de 
        /// comercio exterior.
        /// </summary>
        [XmlAttributeAttribute()]
        public string NumRegIdTrib {
            get {
                return this.numRegIdTribField;
            }
            set {
                this.numRegIdTribField = value;
            }
        }

        /// <summary>
        /// Atributo requerido para incorporar la clave del régimen fiscal del contribuyente receptor al que aplicará el efecto fiscal de este comprobante.
        /// </summary>
        [XmlAttributeAttribute()]
        public string RegimenFiscalReceptor {
            get {
                return this.regimenFiscalReceptorField;
            }
            set {
                this.regimenFiscalReceptorField = value;
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.
        /// </summary>
        [XmlAttributeAttribute()]
        public string UsoCFDI {
            get {
                return this.usoCFDIField;
            }
            set {
                this.usoCFDIField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteConcepto {

        private ComprobanteConceptoImpuestos impuestosField;

        private ComprobanteConceptoACuentaTerceros aCuentaTercerosField;

        private ComprobanteConceptoInformacionAduanera[] informacionAduaneraField;

        private ComprobanteConceptoCuentaPredial[] cuentaPredialField;

        private ComprobanteConceptoComplementoConcepto complementoConceptoField;

        private ComprobanteConceptoParte[] parteField;

        private string claveProdServField;

        private string noIdentificacionField;

        private decimal cantidadField;

        private string claveUnidadField;

        private string unidadField;

        private string descripcionField;

        private decimal valorUnitarioField;

        private decimal importeField;

        private decimal descuentoField;

        private bool descuentoFieldSpecified;

        private string objetoImpField;

        /// <remarks/>
        public ComprobanteConceptoImpuestos Impuestos {
            get {
                return this.impuestosField;
            }
            set {
                this.impuestosField = value;
            }
        }

        /// <remarks/>
        public ComprobanteConceptoACuentaTerceros ACuentaTerceros {
            get {
                return this.aCuentaTercerosField;
            }
            set {
                this.aCuentaTercerosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("InformacionAduanera")]
        public ComprobanteConceptoInformacionAduanera[] InformacionAduanera {
            get {
                return this.informacionAduaneraField;
            }
            set {
                this.informacionAduaneraField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CuentaPredial")]
        public ComprobanteConceptoCuentaPredial[] CuentaPredial {
            get {
                return this.cuentaPredialField;
            }
            set {
                this.cuentaPredialField = value;
            }
        }

        /// <remarks/>
        public ComprobanteConceptoComplementoConcepto ComplementoConcepto {
            get {
                return this.complementoConceptoField;
            }
            set {
                this.complementoConceptoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Parte")]
        public ComprobanteConceptoParte[] Parte {
            get {
                return this.parteField;
            }
            set {
                this.parteField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string ClaveProdServ {
            get {
                return this.claveProdServField;
            }
            set {
                this.claveProdServField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string NoIdentificacion {
            get {
                return this.noIdentificacionField;
            }
            set {
                this.noIdentificacionField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal Cantidad {
            get {
                return this.cantidadField;
            }
            set {
                this.cantidadField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string ClaveUnidad {
            get {
                return this.claveUnidadField;
            }
            set {
                this.claveUnidadField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Unidad {
            get {
                return this.unidadField;
            }
            set {
                this.unidadField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Descripcion {
            get {
                return this.descripcionField;
            }
            set {
                this.descripcionField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal ValorUnitario {
            get {
                return this.valorUnitarioField;
            }
            set {
                this.valorUnitarioField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal Importe {
            get {
                return this.importeField;
            }
            set {
                this.importeField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal Descuento {
            get {
                return this.descuentoField;
            }
            set {
                this.descuentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DescuentoSpecified {
            get {
                return this.descuentoFieldSpecified;
            }
            set {
                this.descuentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string ObjetoImp {
            get {
                return this.objetoImpField;
            }
            set {
                this.objetoImpField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteConceptoImpuestos {

        private ComprobanteConceptoImpuestosTraslado[] trasladosField;

        private ComprobanteConceptoImpuestosRetencion[] retencionesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Traslado", IsNullable = false)]
        public ComprobanteConceptoImpuestosTraslado[] Traslados {
            get {
                return this.trasladosField;
            }
            set {
                this.trasladosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Retencion", IsNullable = false)]
        public ComprobanteConceptoImpuestosRetencion[] Retenciones {
            get {
                return this.retencionesField;
            }
            set {
                this.retencionesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteConceptoImpuestosTraslado {

        private decimal baseField;

        private string impuestoField;

        private string tipoFactorField;

        private decimal tasaOCuotaField;

        private bool tasaOCuotaFieldSpecified;

        private decimal importeField;

        private bool importeFieldSpecified;

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal Base {
            get {
                return this.baseField;
            }
            set {
                this.baseField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Impuesto {
            get {
                return this.impuestoField;
            }
            set {
                this.impuestoField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string TipoFactor {
            get {
                return this.tipoFactorField;
            }
            set {
                this.tipoFactorField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal TasaOCuota {
            get {
                return this.tasaOCuotaField;
            }
            set {
                this.tasaOCuotaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TasaOCuotaSpecified {
            get {
                return this.tasaOCuotaFieldSpecified;
            }
            set {
                this.tasaOCuotaFieldSpecified = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal Importe {
            get {
                return this.importeField;
            }
            set {
                this.importeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ImporteSpecified {
            get {
                return this.importeFieldSpecified;
            }
            set {
                this.importeFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteConceptoImpuestosRetencion {

        private decimal baseField;

        private string impuestoField;

        private string tipoFactorField;

        private decimal tasaOCuotaField;

        private decimal importeField;

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal Base {
            get {
                return this.baseField;
            }
            set {
                this.baseField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Impuesto {
            get {
                return this.impuestoField;
            }
            set {
                this.impuestoField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string TipoFactor {
            get {
                return this.tipoFactorField;
            }
            set {
                this.tipoFactorField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal TasaOCuota {
            get {
                return this.tasaOCuotaField;
            }
            set {
                this.tasaOCuotaField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal Importe {
            get {
                return this.importeField;
            }
            set {
                this.importeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteConceptoACuentaTerceros {

        private string rfcACuentaTercerosField;

        private string nombreACuentaTercerosField;

        private string regimenFiscalACuentaTercerosField;

        private string domicilioFiscalACuentaTercerosField;

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string RfcACuentaTerceros {
            get {
                return this.rfcACuentaTercerosField;
            }
            set {
                this.rfcACuentaTercerosField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string NombreACuentaTerceros {
            get {
                return this.nombreACuentaTercerosField;
            }
            set {
                this.nombreACuentaTercerosField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string RegimenFiscalACuentaTerceros {
            get {
                return this.regimenFiscalACuentaTercerosField;
            }
            set {
                this.regimenFiscalACuentaTercerosField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string DomicilioFiscalACuentaTerceros {
            get {
                return this.domicilioFiscalACuentaTercerosField;
            }
            set {
                this.domicilioFiscalACuentaTercerosField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteConceptoInformacionAduanera {

        private string numeroPedimentoField;

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string NumeroPedimento {
            get {
                return this.numeroPedimentoField;
            }
            set {
                this.numeroPedimentoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteConceptoCuentaPredial {

        private string numeroField;

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Numero {
            get {
                return this.numeroField;
            }
            set {
                this.numeroField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteConceptoComplementoConcepto {

        private System.Xml.XmlElement[] anyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAnyElementAttribute()]
        public System.Xml.XmlElement[] Any {
            get {
                return this.anyField;
            }
            set {
                this.anyField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteConceptoParte {

        private ComprobanteConceptoParteInformacionAduanera[] informacionAduaneraField;

        private string claveProdServField;

        private string noIdentificacionField;

        private decimal cantidadField;

        private string unidadField;

        private string descripcionField;

        private decimal valorUnitarioField;

        private bool valorUnitarioFieldSpecified;

        private decimal importeField;

        private bool importeFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("InformacionAduanera")]
        public ComprobanteConceptoParteInformacionAduanera[] InformacionAduanera {
            get {
                return this.informacionAduaneraField;
            }
            set {
                this.informacionAduaneraField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string ClaveProdServ {
            get {
                return this.claveProdServField;
            }
            set {
                this.claveProdServField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string NoIdentificacion {
            get {
                return this.noIdentificacionField;
            }
            set {
                this.noIdentificacionField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal Cantidad {
            get {
                return this.cantidadField;
            }
            set {
                this.cantidadField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Unidad {
            get {
                return this.unidadField;
            }
            set {
                this.unidadField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Descripcion {
            get {
                return this.descripcionField;
            }
            set {
                this.descripcionField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal ValorUnitario {
            get {
                return this.valorUnitarioField;
            }
            set {
                this.valorUnitarioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ValorUnitarioSpecified {
            get {
                return this.valorUnitarioFieldSpecified;
            }
            set {
                this.valorUnitarioFieldSpecified = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal Importe {
            get {
                return this.importeField;
            }
            set {
                this.importeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ImporteSpecified {
            get {
                return this.importeFieldSpecified;
            }
            set {
                this.importeFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteConceptoParteInformacionAduanera {

        private string numeroPedimentoField;

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string NumeroPedimento {
            get {
                return this.numeroPedimentoField;
            }
            set {
                this.numeroPedimentoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteImpuestos {

        private ComprobanteImpuestosRetencion[] retencionesField;

        private ComprobanteImpuestosTraslado[] trasladosField;

        private decimal totalImpuestosRetenidosField;

        private bool totalImpuestosRetenidosFieldSpecified;

        private decimal totalImpuestosTrasladadosField;

        private bool totalImpuestosTrasladadosFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Retencion", IsNullable = false)]
        public ComprobanteImpuestosRetencion[] Retenciones {
            get {
                return this.retencionesField;
            }
            set {
                this.retencionesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Traslado", IsNullable = false)]
        public ComprobanteImpuestosTraslado[] Traslados {
            get {
                return this.trasladosField;
            }
            set {
                this.trasladosField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal TotalImpuestosRetenidos {
            get {
                return this.totalImpuestosRetenidosField;
            }
            set {
                this.totalImpuestosRetenidosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalImpuestosRetenidosSpecified {
            get {
                return this.totalImpuestosRetenidosFieldSpecified;
            }
            set {
                this.totalImpuestosRetenidosFieldSpecified = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal TotalImpuestosTrasladados {
            get {
                return this.totalImpuestosTrasladadosField;
            }
            set {
                this.totalImpuestosTrasladadosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalImpuestosTrasladadosSpecified {
            get {
                return this.totalImpuestosTrasladadosFieldSpecified;
            }
            set {
                this.totalImpuestosTrasladadosFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteImpuestosRetencion {

        private string impuestoField;

        private decimal importeField;

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Impuesto {
            get {
                return this.impuestoField;
            }
            set {
                this.impuestoField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal Importe {
            get {
                return this.importeField;
            }
            set {
                this.importeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteImpuestosTraslado {

        private decimal baseField;

        private string impuestoField;

        private string tipoFactorField;

        private decimal tasaOCuotaField;

        private bool tasaOCuotaFieldSpecified;

        private decimal importeField;

        private bool importeFieldSpecified;

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal Base {
            get {
                return this.baseField;
            }
            set {
                this.baseField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string Impuesto {
            get {
                return this.impuestoField;
            }
            set {
                this.impuestoField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string TipoFactor {
            get {
                return this.tipoFactorField;
            }
            set {
                this.tipoFactorField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal TasaOCuota {
            get {
                return this.tasaOCuotaField;
            }
            set {
                this.tasaOCuotaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TasaOCuotaSpecified {
            get {
                return this.tasaOCuotaFieldSpecified;
            }
            set {
                this.tasaOCuotaFieldSpecified = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public decimal Importe {
            get {
                return this.importeField;
            }
            set {
                this.importeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ImporteSpecified {
            get {
                return this.importeFieldSpecified;
            }
            set {
                this.importeFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteComplemento {

        private List<XmlElement> anyField;

        /// <comentarios/>
        [XmlAnyElementAttribute()]
        public List<XmlElement> Any {
            get {
                return this.anyField;
            }
            set {
                this.anyField = value;
            }
        }

        [XmlIgnore]
        public TimbreFiscalDigital TimbreFiscalDigital {
            get {
                TimbreFiscalDigital timbreFiscalDigital;
                List<XmlElement>.Enumerator enumerator = this.anyField.GetEnumerator();
                try {
                    while (enumerator.MoveNext()) {
                        XmlElement current = enumerator.Current;
                        string name = current.Name;
                        if (name == null || current.Name != "tfd:TimbreFiscalDigital") {
                            continue;
                        }
                        timbreFiscalDigital = TimbreFiscalDigital.Deserialize(current.OuterXml);
                        return timbreFiscalDigital;
                    }
                    return null;
                } catch (Exception) {
                    timbreFiscalDigital = null;
                } finally {
                    ((IDisposable)enumerator).Dispose();
                }
                return timbreFiscalDigital;
            }
            set {
                XmlDocument objXml = new XmlDocument();
                objXml.LoadXml(value.Serialize());
                if (this.anyField == null) {
                    this.anyField = new List<XmlElement>();
                }
                this.anyField.Add(objXml.DocumentElement);
            }
        }

        [XmlIgnore]
        public Jaeger.CFDI.Complemento.Nomina.V12.Nomina Nomina12 {
            get {
                Jaeger.CFDI.Complemento.Nomina.V12.Nomina nomina12;
                List<XmlElement>.Enumerator enumerator = this.anyField.GetEnumerator();
                try {
                    while (enumerator.MoveNext()) {
                        XmlElement current = enumerator.Current;
                        string name = current.Name;
                        if (name == null || name != "nomina12:Nomina") {
                            continue;
                        }
                        nomina12 = Jaeger.CFDI.Complemento.Nomina.V12.Nomina.Deserialize(current.OuterXml);
                        return nomina12;
                    }
                    return null;
                } catch (Exception) {
                    nomina12 = null;
                } finally {
                    ((IDisposable)(object)enumerator).Dispose();
                }
                return nomina12;
            }
            set {
                XmlDocument objXml = new XmlDocument();
                objXml.LoadXml(value.Serialize());
                if (this.anyField == null) {
                    this.anyField = new List<XmlElement>();
                }
                this.anyField.Add(objXml.DocumentElement);
            }
        }

        [XmlIgnore]
        public Pagos Pagos {
            get {
                Pagos pagos10;
                List<XmlElement>.Enumerator enumerator = this.anyField.GetEnumerator();
                try {
                    while (enumerator.MoveNext()) {
                        XmlElement current = enumerator.Current;
                        string name = current.Name;
                        if (name == null || name != "pago20:Pagos") {
                            continue;
                        }
                        pagos10 = Pagos.Deserialize(current.OuterXml);
                        return pagos10;
                    }
                    return null;
                } catch (Exception) {
                    pagos10 = null;
                } finally {
                    ((IDisposable)(object)enumerator).Dispose();
                }
                return pagos10;
            }
            set {
                XmlDocument objXml = new XmlDocument();
                objXml.LoadXml(value.Serialize());
                if (this.anyField == null) {
                    this.anyField = new List<XmlElement>();
                }
                this.anyField.Add(objXml.DocumentElement);
            }
        }

        [XmlIgnore]
        public LeyendasFiscales LeyendasFiscales {
            get {
                LeyendasFiscales leyendasFiscales;
                leyendasFiscales = Jaeger.Helpers.HelperXmlSerializer.XmlDeserializarStringXml<LeyendasFiscales>(this.Buscar("leyendasFisc:LeyendasFiscales"));
                return leyendasFiscales;
            }
        }

        [XmlIgnore]
        public ValesDeDespensa ValesDeDespensa {
            get {
                ValesDeDespensa valesDeDespensa;
                valesDeDespensa = Jaeger.Helpers.HelperXmlSerializer.XmlDeserializarStringXml<ValesDeDespensa>(this.Buscar("valesdedespensa:ValesDeDespensa"));
                return valesDeDespensa;
            }
        }

        [XmlIgnore]
        public ImpuestosLocales ImpuestosLocales {
            get {
                ImpuestosLocales impuestosLocales;
                impuestosLocales = Jaeger.Helpers.HelperXmlSerializer.XmlDeserializarStringXml<ImpuestosLocales>(this.Buscar("implocal:ImpuestosLocales"));
                return impuestosLocales;
            }
        }

        [XmlIgnore]
        public Aerolineas Aerolineas {
            get {
                Aerolineas aerolineas;
                aerolineas = Jaeger.Helpers.HelperXmlSerializer.XmlDeserializarStringXml<Aerolineas>(this.Buscar("aerolineas:Aerolineas"));
                return aerolineas;
            }
        }

        [XmlIgnore]
        public Complemento.EstadoCuentaCombustible.V12.EstadoDeCuentaCombustible ECC12 {
            get {
                Complemento.EstadoCuentaCombustible.V12.EstadoDeCuentaCombustible cc;
                cc = Jaeger.Helpers.HelperXmlSerializer.XmlDeserializarStringXml<Complemento.EstadoCuentaCombustible.V12.EstadoDeCuentaCombustible>(Buscar(""));
                return cc;
            }
        }

        private string Buscar(string nombre) {
            List<XmlElement>.Enumerator enumerator = this.anyField.GetEnumerator();
            try {
                while (enumerator.MoveNext()) {
                    XmlElement current = enumerator.Current;
                    string name = current.Name;
                    if (name == null || name != nombre) {
                        continue;
                    }
                    return current.OuterXml;
                }
                return string.Empty;
            } catch (Exception) {
                return string.Empty;
            } finally {
                ((IDisposable)(object)enumerator).Dispose();
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/4")]
    public partial class ComprobanteAddenda {

        private System.Xml.XmlElement[] anyField;
        /// <remarks/>
        [System.Xml.Serialization.XmlAnyElementAttribute()]
        public System.Xml.XmlElement[] Any {
            get {
                return this.anyField;
            }
            set {
                this.anyField = value;
            }
        }
    }
}
