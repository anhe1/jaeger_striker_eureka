﻿/// <remarks/>
using System;
using System.IO;
using System.Xml.Serialization;

namespace Jaeger.CFDI
{
    [XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false, ElementName = "SPEI_Tercero")]
    public partial class SpeiTercero
    {
        private SpeiTerceroBeneficiario beneficiarioField;

        private SpeiTerceroOrdenante ordenanteField;

        private System.DateTime fechaOperacionField;

        private System.DateTime horaField;

        private ushort claveSPEIField;

        private string selloField;

        private ulong numeroCertificadoField;

        private string cadenaCDAField;

        /// <remarks/>
        public SpeiTerceroBeneficiario Beneficiario
        {
            get
            {
                return this.beneficiarioField;
            }
            set
            {
                this.beneficiarioField = value;
            }
        }

        /// <remarks/>
        public SpeiTerceroOrdenante Ordenante
        {
            get
            {
                return this.ordenanteField;
            }
            set
            {
                this.ordenanteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime FechaOperacion
        {
            get
            {
                return this.fechaOperacionField;
            }
            set
            {
                this.fechaOperacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "time")]
        public System.DateTime Hora
        {
            get
            {
                return this.horaField;
            }
            set
            {
                this.horaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort ClaveSPEI
        {
            get
            {
                return this.claveSPEIField;
            }
            set
            {
                this.claveSPEIField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string sello
        {
            get
            {
                return this.selloField;
            }
            set
            {
                this.selloField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong numeroCertificado
        {
            get
            {
                return this.numeroCertificadoField;
            }
            set
            {
                this.numeroCertificadoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string cadenaCDA
        {
            get
            {
                return this.cadenaCDAField;
            }
            set
            {
                this.cadenaCDAField = value;
            }
        }

        public static SpeiTercero LoadX(string input)
        {
            StreamReader oStreamReader = new StreamReader(input);
            try
            {
                return Jaeger.Helpers.HelperSerialize.DeserializeObject<SpeiTercero>(oStreamReader.ReadToEnd());
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                oStreamReader.Close();
            }
        }
    }
}

namespace Jaeger.CFDI
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SpeiTerceroBeneficiario
    {
        private string bancoReceptorField;

        private string nombreField;

        private byte tipoCuentaField;

        private string cuentaField;

        private string rFCField;

        private string conceptoField;

        private decimal iVAField;

        private byte montoPagoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string BancoReceptor
        {
            get
            {
                return this.bancoReceptorField;
            }
            set
            {
                this.bancoReceptorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte TipoCuenta
        {
            get
            {
                return this.tipoCuentaField;
            }
            set
            {
                this.tipoCuentaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Cuenta
        {
            get
            {
                return this.cuentaField;
            }
            set
            {
                this.cuentaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RFC
        {
            get
            {
                return this.rFCField;
            }
            set
            {
                this.rFCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Concepto
        {
            get
            {
                return this.conceptoField;
            }
            set
            {
                this.conceptoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal IVA
        {
            get
            {
                return this.iVAField;
            }
            set
            {
                this.iVAField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte MontoPago
        {
            get
            {
                return this.montoPagoField;
            }
            set
            {
                this.montoPagoField = value;
            }
        }
    }
}

namespace Jaeger.CFDI
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SpeiTerceroOrdenante
    {
        private string bancoEmisorField;

        private string nombreField;

        private byte tipoCuentaField;

        private string cuentaField;

        private string rFCField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string BancoEmisor
        {
            get
            {
                return this.bancoEmisorField;
            }
            set
            {
                this.bancoEmisorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte TipoCuenta
        {
            get
            {
                return this.tipoCuentaField;
            }
            set
            {
                this.tipoCuentaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Cuenta
        {
            get
            {
                return this.cuentaField;
            }
            set
            {
                this.cuentaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RFC
        {
            get
            {
                return this.rFCField;
            }
            set
            {
                this.rFCField = value;
            }
        }
    }
}

