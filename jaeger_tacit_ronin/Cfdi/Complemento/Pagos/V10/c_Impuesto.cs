namespace Jaeger.Sat.Cfdi.Complemento.Pagos.V10
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.sat.gob.mx/sitio_internet/cfd/catalogos")]
    public enum c_Impuesto
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("001")]
        Item001,
    
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("002")]
        Item002,
    
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("003")]
        Item003,
    }
}