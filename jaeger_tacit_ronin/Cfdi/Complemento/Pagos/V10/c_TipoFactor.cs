/// <remarks/>
namespace Jaeger.Sat.Cfdi.Complemento.Pagos.V10
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.sat.gob.mx/sitio_internet/cfd/catalogos")]
    public enum c_TipoFactor
    {
        /// <remarks/>
        Tasa,
    
        /// <remarks/>
        Cuota,
    
        /// <remarks/>
        Exento,
    }
}