﻿/// develop: 13042018000
/// purpose: clase para acceder a un archivo Excel
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Data;
using OfficeOpenXml;
using System.Collections;
using System.Data.OleDb;

namespace Jaeger.Helpers
{
    public class HelperExcelReader
    {
        public static DataTable GetDataTableFromExcel(string rutaExcel, string hoja, bool hasHeader = true)
        {
            FileInfo existingFile = new FileInfo(rutaExcel);
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                DataTable tbl = new DataTable();
                int startRow = 1;

                foreach (var firstRowCell in worksheet.Cells[1, 1, 1, worksheet.Dimension.End.Column])
                {
                    if (hasHeader)
                    {
                        tbl.Columns.Add(firstRowCell.Text);
                        startRow = 2;
                    }
                    else
                    {
                        tbl.Columns.Add(String.Format("Column {0}", firstRowCell.Start.Column));
                        startRow = 1;
                    }
                }
                
                for (int rowNum = startRow; rowNum <= worksheet.Dimension.End.Row; rowNum++)
                {
                    ExcelRange wsRow = worksheet.Cells[rowNum, 1, rowNum, worksheet.Dimension.End.Column];
                    DataRow row = tbl.Rows.Add();
                    foreach (var cell in wsRow)
                    {
                        row[cell.Start.Column - 1] = cell.Text;
                    }
                }
                return tbl;
            }
        }

        public static DataTable GetExcelATabla(string archivo, string hoja)
        {
            IEnumerator enumerator = null;
            string str = "";
            string str1 = "";
            DataTable dataTable = new DataTable();
            DataTable oleDbSchemaTable = new DataTable();
            try
            {
                using (OleDbConnection oleDbConnection = new OleDbConnection(string.Concat("Provider=Microsoft.ACE.OLEDB.12.0;Data Source='", archivo, "';Extended Properties=Excel 12.0;")))
                {
                    oleDbConnection.Open();
                    Guid tables = OleDbSchemaGuid.Tables;
                    object[] objArray = new object[] { null, null, null, "TABLE" };
                    oleDbSchemaTable = oleDbConnection.GetOleDbSchemaTable(tables, objArray);
                    str1 = oleDbSchemaTable.Rows[0]["TABLE_NAME"].ToString();
                    try
                    {
                        enumerator = oleDbSchemaTable.Rows.GetEnumerator();
                        while (enumerator.MoveNext())
                        {
                            DataRow current = (DataRow)enumerator.Current;
                            if (current["TABLE_NAME"].ToString() == string.Concat(hoja, "$"))
                            {
                                str1 = string.Concat(hoja, "$");
                            }
                        }
                    }
                    finally
                    {
                        if (enumerator is IDisposable)
                        {
                            (enumerator as IDisposable).Dispose();
                        }
                    }
                    str = string.Concat("SELECT * FROM [", str1, "]");
                    using (OleDbCommand oleDbCommand = new OleDbCommand(str, oleDbConnection))
                    {
                        dataTable.Load(oleDbCommand.ExecuteReader());
                    }
                }
                return dataTable;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public static List<string> GetWorkSheets(string archivo)
        {
            FileInfo existingFile = new FileInfo(archivo);
            List<string> hojas = new List<string>();
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                foreach (ExcelWorksheet item in package.Workbook.Worksheets)
                {
                    hojas.Add(item.Name);
                }
            }
            return hojas;
        }
    }
}
