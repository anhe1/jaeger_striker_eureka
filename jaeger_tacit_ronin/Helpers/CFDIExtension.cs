﻿/// develop: 290420171106
/// purpose: convertir diferentes objetos del comprobante fiscal a los de sistema.
using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Jaeger.CFDI.Entities;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Edita.V2.Directorio.Enums;
using Jaeger.Enums;
using Jaeger.Catalogos.Repositories;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Helpers
{
    public class CFDIExtension
    {
        readonly FormaPagoCatalogo formaPago33;
        readonly UsoCFDICatalogo usoCFDI;
        readonly RelacionCFDICatalogo relacionCFDI;

        /// <summary>
        /// constructor
        /// </summary>
        public CFDIExtension()
        {
            this.formaPago33 = new FormaPagoCatalogo();
            this.formaPago33.Load();
            this.usoCFDI = new UsoCFDICatalogo();
            this.usoCFDI.Load();
            this.relacionCFDI = new RelacionCFDICatalogo();
            this.relacionCFDI.Load();
        }

        #region comprobante fiscal v33

        /// <summary>
        /// convertir V33.ComprobanteEmisor a Contribuyente
        /// </summary>
        public ViewModelContribuyente Emisor(CFDI.V33.ComprobanteEmisor objeto, EnumRelationType relacion)
        {
            ViewModelContribuyente emisor = new ViewModelContribuyente()
            {
                Relacion = Enum.GetName(typeof(EnumRelationType), relacion),
                RFC = objeto.Rfc,
                Nombre = objeto.Nombre
            };
            return emisor;
        }

        /// <summary>
        /// convertir Cfd.Contribuyente a V33.ComprobanteEmisor
        /// </summary>
        public CFDI.V33.ComprobanteEmisor Emisor(ViewModelContribuyente objeto)
        {
            CFDI.V33.ComprobanteEmisor emisor = new CFDI.V33.ComprobanteEmisor()
            {
                Nombre = objeto.Nombre,
                Rfc = objeto.RFC,
                RegimenFiscal = objeto.RegimenFiscal
            };
            return emisor;
        }

        /// <summary>
        /// convertir Cfd.Contribyente a V33.ComprobanteReceptor
        /// </summary>
        public CFDI.V33.ComprobanteReceptor Receptor(ViewModelContribuyente objeto, string usoCfdi)
        {
            CFDI.V33.ComprobanteReceptor receptor = new CFDI.V33.ComprobanteReceptor()
            {
                Nombre = objeto.Nombre,
                Rfc = objeto.RFC,
                NumRegIdTrib = objeto.NumRegIdTrib,
                UsoCFDI = usoCfdi,
                ResidenciaFiscalSpecified = false
            };

            if (!(objeto.RegimenFiscal == null))
            {
                if (objeto.RegimenFiscal.Trim() != "")
                {
                    receptor.ResidenciaFiscalSpecified = true;
                    receptor.ResidenciaFiscal = objeto.ResidenciaFiscal;
                }
            }

            return receptor;
        }

        /// <summary>
        /// convertir V33.ComprobanteReceptor a Contribuyente
        /// </summary>
        public ViewModelContribuyente Receptor(CFDI.V33.ComprobanteReceptor objeto, EnumRelationType relacion)
        {
            ViewModelContribuyente receptor = new ViewModelContribuyente()
            {
                Relacion = Enum.GetName(typeof(EnumRelationType), relacion),
                RFC = objeto.Rfc,
                Nombre = objeto.Nombre
            };
            return receptor;
        }

        /// <summary>
        /// convertir V33.ComprobanteEmisor a Contribuyente
        /// </summary>
        public ViewModelContribuyente Emisor(CFDI.V33.ComprobanteEmisor objeto)
        {
            ViewModelContribuyente newItem = new ViewModelContribuyente();
            newItem.RFC = objeto.Rfc;
            newItem.Nombre = objeto.Nombre;
            return newItem;
        }

        /// <summary>
        /// convertir V33.ComprobanteReceptor a Contribuyente
        /// </summary>
        public ViewModelContribuyente Receptor(CFDI.V33.ComprobanteReceptor objeto)
        {
            ViewModelContribuyente newItem = new ViewModelContribuyente();
            newItem.RFC = objeto.Rfc;
            newItem.Nombre = objeto.Nombre;
            return newItem;
        }

        /// <summary>
        /// convertir objeto V33.Comprobante a Cfd.Comprobante falta probar
        /// </summary>
        public ViewModelComprobante Create(CFDI.V33.Comprobante objeto)
        {
            ViewModelComprobante item = new ViewModelComprobante();

            // datos generales
            item.TipoComprobanteText = objeto.TipoDeComprobante;

            item.Version = objeto.Version;
            item.Folio = objeto.Folio;
            item.Serie = objeto.Serie;
            item.SubTipo = objeto.Type;
            item.Receptor = this.Receptor(objeto.Receptor);
            item.Emisor = this.Emisor(objeto.Emisor);
            item.Conceptos = this.Conceptos(objeto.Conceptos);
            item.SubTotal = objeto.SubTotal;
            item.Total = objeto.Total;
            item.NoCertificado = objeto.NoCertificado;
            item.FechaEmision = objeto.Fecha;

            item.Moneda.Clave = objeto.Moneda;
            item.LugarExpedicion = objeto.LugarExpedicion;

            ClaveUsoCFDI uso = this.usoCFDI.Search(objeto.Receptor.UsoCFDI);
            if (uso != null)
            {
                item.UsoCfdi = uso;
            }
            else
            {
                item.UsoCfdi.Clave = objeto.Receptor.UsoCFDI;
            }

            // condiciones de pago
            item.CondicionPago = "";
            if (!(objeto.CondicionesDePago == null))
            {
                item.CondicionPago = objeto.CondicionesDePago;
            }

            // descuento
            item.Descuento = 0;
            if (objeto.DescuentoSpecified)
            {
                item.Descuento = objeto.Descuento;
            }

            // tipo de cambio
            if (objeto.TipoCambioSpecified)
            {
                item.TipoCambio = objeto.TipoCambio.ToString();
            }

            // forma de pago
            item.FormaPago.Clave = "";
            if (!(objeto.FormaPago == null))
            {
                if (objeto.FormaPagoSpecified)
                {
                    ClaveFormaPago clave = this.formaPago33.Search(Regex.Replace(objeto.FormaPago, "[^\\d]", ""));
                    if (clave == null)
                    {
                        item.FormaPago.Clave = objeto.FormaPago;
                    }
                    else
                    {
                        item.FormaPago = clave;
                    }
                }
            }

            // metodo de pago
            item.MetodoPago.Clave = "";
            if (!(objeto.MetodoPago == null))
            {
                if (objeto.MetodoPagoSpecified)
                {
                    item.MetodoPago.Clave = objeto.MetodoPago;
                    if (objeto.MetodoPago.ToUpper().Trim() == "PUE")
                    {
                        item.MetodoPago.Descripcion = "Pago en una sola exhibición";
                    }
                    else if (objeto.MetodoPago.ToUpper().Trim() == "PPD")
                    {
                        item.MetodoPago.Descripcion = "Pago en parcialidades o diferido";
                    }
                }
            }

            // comprobantes relacionados
            if (!(objeto.CfdiRelacionados == null))
            {
                ComprobanteCfdiRelacionados comDoctos = this.ComprobanteCfdiRelacionados(objeto.CfdiRelacionados);
                if (!(comDoctos == null))
                {
                    ClaveTipoRelacionCFDI clave = this.relacionCFDI.Search(comDoctos.TipoRelacion.Clave);
                    if (clave != null)
                    {
                        comDoctos.TipoRelacion.Descripcion = clave.Descripcion;
                    }

                    item.CfdiRelacionados = comDoctos;
                }
            }


            // impuestos
            if (!(objeto.Impuestos == null))
            {
                if (objeto.Impuestos.TotalImpuestosRetenidosSpecified)
                {
                    if (!(objeto.Impuestos.Retenciones == null))
                    {
                        foreach (CFDI.V33.ComprobanteImpuestosRetencion imp in objeto.Impuestos.Retenciones)
                        {
                            if (imp.Impuesto == "001") // retencion ISR
                            {
                                item.RetencionIsr = item.RetencionIsr + imp.Importe;
                            }
                            else if (imp.Impuesto == "002") // retencion IVA
                            {
                                item.RetencionIva = item.RetencionIva + imp.Importe;
                            }
                            else if (imp.Impuesto == "003") // retencion IEPS
                            {
                                item.RetencionIeps = item.RetencionIeps + imp.Importe;
                            }
                        }
                    }
                }

                if (objeto.Impuestos.TotalImpuestosTrasladadosSpecified)
                {
                    if (!(objeto.Impuestos.Traslados == null))
                    {
                        foreach (CFDI.V33.ComprobanteImpuestosTraslado imp in objeto.Impuestos.Traslados)
                        {
                            if (imp.Impuesto == "002") // traslado IVA
                            {
                                item.TrasladoIva = item.TrasladoIva + imp.Importe;
                            }
                            else if (imp.Impuesto == "003") // traslado IEPS
                            {
                                item.TrasladoIeps = item.TrasladoIeps + imp.Importe;
                            }
                        }
                    }
                }
            }

            // validacion
            if (!(objeto.Validation == null))
            {
                item.Validacion = objeto.Validation;
                //item.FechaVal = objeto.Validation.FechaValidacion;
                item.Result = item.Validacion.IsValidText;
                item.Estado = item.Validacion.ProofStatus;
                item.FechaEstado = item.FechaVal;
            }

            // complementos
            if (!(objeto.Complemento == null))
            {
                // complemento timbre fiscal
                if (!(objeto.Complemento.TimbreFiscalDigital == null))
                {
                    item.TimbreFiscal = this.TimbreFiscal(objeto.Complemento.TimbreFiscalDigital);
                }
                else
                {
                    item.TimbreFiscal = null;
                }

                // complemento nomina 1.2
                if (!(objeto.Complemento.Nomina12 == null))
                {
                    item.Nomina = this.Create(objeto.Complemento.Nomina12);
                    if (item.Nomina != null)
                    {
                        item.Nomina.Emisor.RFC = objeto.Emisor.Rfc;
                        item.Nomina.Receptor.RFC = objeto.Receptor.Rfc;
                        item.Nomina.Descuento = item.Descuento;
                        item.Receptor.ClaveUsoCFDI = objeto.Receptor.UsoCFDI;

                        if (item.TimbreFiscal != null)
                        {
                            item.Nomina.IdDocumento = item.TimbreFiscal.UUID;
                        }
                    }
                }

                // complemento de pagos
                if (!(objeto.Complemento.Pagos == null))
                {
                    ComplementoPagos c2 = this.Create(objeto.Complemento.Pagos);
                    if (!(c2 == null))
                    {
                        item.Complementos = new Complementos();
                        item.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.Pagos10, Data = c2.Json() });
                        item.ComplementoPagos = c2;
                    }
                }

                // complemento vales de despensa
                if (!(objeto.Complemento.ValesDeDespensa == null))
                {
                    ComplementoValesDeDespensa c3 = this.Create(objeto.Complemento.ValesDeDespensa);
                    if (c3 != null)
                    {
                        if (item.Complementos == null)
                        {
                            item.Complementos = new Complementos();
                        }
                        item.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.ValesDeDespensa, Data = c3.Json() });
                    }

                }

                // complemento leyendas fiscales
                if (!(objeto.Complemento.LeyendasFiscales == null))
                {
                    ComplementoLeyendasFiscales c4 = this.Create(objeto.Complemento.LeyendasFiscales);
                    if (c4 != null)
                    {
                        if (item.Complementos == null)
                        {
                            item.Complementos = new Complementos();
                        }
                        item.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.LeyendasFiscales, Data = c4.Json() });
                    }
                }

                // complemento Impuestos Locales
                if (!(objeto.Complemento.ImpuestosLocales == null))
                {
                    ComplementoImpuestosLocales c5 = this.Create(objeto.Complemento.ImpuestosLocales);
                    if (c5 != null)
                    {
                        if (item.Complementos == null)
                        {
                            item.Complementos = new Complementos();
                        }
                        item.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.ImpuestosLocales, Data = c5.Json() });
                    }
                }

                // complemento aerolineas
                if (!(objeto.Complemento.Aerolineas == null))
                {
                    ComplementoAerolineas c6 = this.Create(objeto.Complemento.Aerolineas);
                    if (c6 != null)
                    {
                        if (item.Complementos == null)
                        {
                            item.Complementos = new Complementos();
                        }
                        item.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.ImpuestosLocales, Data = c6.Json() });
                    }
                }
            }

            return item;
        }

        /// <summary>
        /// convertir objeto cfd.Comprobante a V33.Comprobante
        /// </summary>
        public CFDI.V33.Comprobante Create(ViewModelComprobante objeto)
        {
            CFDI.V33.Comprobante newItem = new CFDI.V33.Comprobante();
            // crear datos generales del comprobante
            newItem.TipoDeComprobante = Enum.GetName(typeof(EnumCfdiType), objeto.TipoComprobante).ToString().Substring(0, 1);
            newItem.Fecha = Convert.ToDateTime(objeto.FechaEmision.ToString("yyyy-MM-dd\\THH:mm:ss"));
            newItem.Moneda = objeto.Moneda.Clave;
            newItem.TipoCambio = Convert.ToDecimal(objeto.TipoCambio);
            newItem.TipoCambioSpecified = Convert.ToDecimal(objeto.TipoCambio) > 0;
            newItem.MetodoPago = objeto.MetodoPago.Clave;
            newItem.MetodoPagoSpecified = true;
            newItem.FormaPago = objeto.FormaPago.Clave;
            newItem.FormaPagoSpecified = true;
            newItem.LugarExpedicion = objeto.LugarExpedicion;

            newItem.CondicionesDePago = null;
            if (objeto.CondicionPago != null)
            {
                if (objeto.CondicionPago.Trim() != "")
                {
                    newItem.CondicionesDePago = objeto.CondicionPago;
                }
            }

            newItem.Folio = null;
            if (objeto.Folio.Trim() != "")
            {
                newItem.Folio = objeto.Folio;
            }

            newItem.Serie = null;
            if (objeto.Serie.Trim() != "")
            {
                newItem.Serie = objeto.Serie;
            }



            // comprobantes relacionados
            if (objeto.CfdiRelacionados != null)
            {
                if (objeto.CfdiRelacionados.CfdiRelacionado.Count > 0)
                {
                    newItem.CfdiRelacionados = this.ComprobanteCfdiRelacionados(objeto.CfdiRelacionados);
                }
            }

            // complementos
            if (objeto.Complementos != null)
            {
                if (objeto.Complementos.Objeto.Count > 0)
                {
                    CFDI.V33.ComprobanteComplemento itemComplemento = new CFDI.V33.ComprobanteComplemento();
                    foreach (Complemento item in objeto.Complementos.Objeto)
                    {
                        if (item.Nombre == EnumCfdiComplementos.Pagos10)
                        {
                            Jaeger.Edita.V2.CFDI.Entities.ComplementoPagos itemPagos10 = Jaeger.Edita.V2.CFDI.Entities.ComplementoPagos.Json(item.Data);
                            if (!(itemPagos10 == null))
                            {
                                //newItem.Complemento = new CFDI.V33.ComprobanteComplemento();
                                itemComplemento.Pagos = this.Create(itemPagos10);
                            }
                        }
                    }
                    newItem.Complemento = itemComplemento;
                }
            }

            // adendas
            if (objeto.Addendas != null)
            {
                if (objeto.Addendas.Objeto.Count > 0)
                {
                    CFDI.V33.ComprobanteAddenda itemAddenda = new CFDI.V33.ComprobanteAddenda();
                    foreach (Jaeger.CFDI.Entities.Addenda.Addenda item in objeto.Addendas.Objeto)
                    {
                        if (item.Nombre == EnumCfdiAddendas.AfasaMiscelanea)
                        {
                            itemAddenda.Miscelanea = Jaeger.CFDI.Entities.Addenda.AddendaMiscelanea.Json(item.Data);
                        }
                        else if (item.Nombre == EnumCfdiAddendas.AfasaReciboFiscal)
                        {
                            itemAddenda.ReciboFiscal = Jaeger.CFDI.Entities.Addenda.AddendaReciboFiscal.Json(item.Data);
                        }
                        else if (item.Nombre == EnumCfdiAddendas.CartaPorte)
                        {
                            itemAddenda.CartaPorte = Jaeger.CFDI.Entities.Addenda.AddendaCartaPorte.Json(item.Data);
                        }
                    }
                    newItem.Addenda = itemAddenda;
                }
            }

            // crear emisor y receptor del comprobante
            newItem.Emisor = this.Emisor(objeto.Emisor);
            newItem.Receptor = this.Receptor(objeto.Receptor, objeto.UsoCfdi.Clave);

            // crear conceptos
            newItem.Conceptos = this.Conceptos(objeto.Conceptos);

            // impuestos del comprobante
            if (objeto.TipoComprobante != EnumCfdiType.Pagos && objeto.TipoComprobante != EnumCfdiType.Traslado)
            {
                newItem.Impuestos = this.Impuestos(objeto);
            }

            // totales del comprobante
            if (objeto.SubTotal <= 0)
            {
                newItem.SubTotal = 0;
            }
            else
            {
                newItem.SubTotal = Math.Round(newItem.Conceptos.Sum(f => f.Importe), objeto.PrecisionDecimal);
            }

            newItem.Descuento = Math.Round(newItem.Conceptos.Sum(p => p.Descuento), objeto.PrecisionDecimal);
            if (objeto.Total <= 0)
            {
                newItem.Total = 0;
            }
            else
            {
                newItem.Total = Math.Round((newItem.SubTotal - newItem.Descuento) + (newItem.Impuestos.TotalImpuestosTrasladados) - (newItem.Impuestos.TotalImpuestosRetenidos), objeto.PrecisionDecimal);
            }



            // cuando el tipo de comprobante es pagos
            if (objeto.TipoComprobante == EnumCfdiType.Pagos)
            {
                newItem.DescuentoSpecified = false;
                newItem.TipoCambioSpecified = false;
                newItem.MetodoPagoSpecified = false;
                newItem.FormaPagoSpecified = false;
                newItem.CondicionesDePago = null;
                newItem.Conceptos[0].Cantidad = 1;
                newItem.Conceptos[0].ValorUnitario = 0;
                newItem.Conceptos[0].Importe = 0;
            }

            // cuando el tipo de comprobante es traslado
            if (objeto.TipoComprobante == EnumCfdiType.Traslado)
            {
                newItem.DescuentoSpecified = false;
                newItem.MetodoPagoSpecified = false;
                newItem.FormaPagoSpecified = false;
                newItem.CondicionesDePago = null;
            }
            return newItem;
        }

        /// <summary>
        /// convertir Cfd.Conceptos a V33.ComprobanteConcepto
        /// </summary>
        public CFDI.V33.ComprobanteConcepto[] Conceptos(BindingList<Jaeger.Edita.V2.CFDI.Entities.ViewModelComprobanteConcepto> objetos)
        {
            if (!(objetos == null))
            {
                List<CFDI.V33.ComprobanteConcepto> newItem = new List<CFDI.V33.ComprobanteConcepto>();
                foreach (Jaeger.Edita.V2.CFDI.Entities.ViewModelComprobanteConcepto item in objetos)
                {
                    if (item.IsActive == true)
                    {
                        newItem.Add(this.Concepto(item));
                    }
                }
                return newItem.ToArray();
            }
            return null;
        }

        /// <summary>
        /// Crear Cfd.Concepto a V33.ComprobanteConcepto
        /// </summary>
        public CFDI.V33.ComprobanteConcepto Concepto(Jaeger.Edita.V2.CFDI.Entities.ViewModelComprobanteConcepto objeto)
        {
            CFDI.V33.ComprobanteConcepto item = new CFDI.V33.ComprobanteConcepto();
            item.Cantidad = decimal.Round(objeto.Cantidad, 2, MidpointRounding.AwayFromZero);
            item.ClaveProdServ = objeto.ClaveProdServ;
            item.ClaveUnidad = objeto.ClaveUnidad;
            item.Descripcion = objeto.Descripcion;
            item.ValorUnitario = decimal.Round(objeto.ValorUnitario, 2, MidpointRounding.AwayFromZero);
            item.Importe = decimal.Round(objeto.Importe, 2, MidpointRounding.AwayFromZero);
            item.Descuento = decimal.Round(objeto.Descuento, 2, MidpointRounding.AwayFromZero);

            /// si existe descuento
            if (objeto.Descuento > 0)
            {
                item.DescuentoSpecified = true;
            }
            else
            {
                item.DescuentoSpecified = false;
            }

            /// opcional unidad
            if (!(objeto.Unidad == null))
            {
                if (objeto.Unidad.Trim() != "")
                {
                    item.Unidad = objeto.Unidad;
                }
            }

            /// opcional no de identificacion
            if (!(objeto.NoIdentificacion == null))
            {
                if (objeto.NoIdentificacion == null)
                {
                    if (objeto.NoIdentificacion.Trim() != "")
                    {
                        item.NoIdentificacion = objeto.NoIdentificacion;
                    }
                }
            }

            if (!(objeto.CtaPredial == null))
            {
                if (objeto.CtaPredial.Trim().Length > 0)
                {
                    CFDI.V33.ComprobanteConceptoCuentaPredial cuentaPredial = new CFDI.V33.ComprobanteConceptoCuentaPredial()
                    {
                        Numero = objeto.CtaPredial
                    };
                    item.CuentaPredial = cuentaPredial;
                }
            }

            // informacion aduanera
            if (!(objeto.InformacionAduanera == null))
            {
                item.InformacionAduanera = this.ConceptoInformacionAduanera(objeto.InformacionAduanera);
            }

            /// partes del concepto
            if (objeto.Parte.Count > 0)
            {
                item.Parte = this.ConceptoParte(objeto.Parte);
            }

            /// impuestos del concepto
            if (objeto.Impuestos.Count > 0)
            {
                item.Impuestos = this.ConceptoImpuestos(objeto.Impuestos);
            }

            return item;
        }

        public CFDI.V33.ComprobanteConceptoInformacionAduanera[] ConceptoInformacionAduanera(BindingList<ComprobanteInformacionAduanera> objetos)
        {
            if (!(objetos == null))
            {
                List<CFDI.V33.ComprobanteConceptoInformacionAduanera> newItems = new List<CFDI.V33.ComprobanteConceptoInformacionAduanera>();
                foreach (ComprobanteInformacionAduanera item in objetos)
                {
                    CFDI.V33.ComprobanteConceptoInformacionAduanera newItem = new CFDI.V33.ComprobanteConceptoInformacionAduanera();
                    newItem.NumeroPedimento = item.NumeroPedimento;
                    newItems.Add(newItem);
                }
                return newItems.ToArray();
            }
            return null;
        }

        public CFDI.V33.ComprobanteConceptoParteInformacionAduanera[] ConceptoParteInformacionAduanera(BindingList<ComprobanteInformacionAduanera> objetos)
        {
            if (!(objetos == null))
            {
                List<CFDI.V33.ComprobanteConceptoParteInformacionAduanera> newItems = new List<CFDI.V33.ComprobanteConceptoParteInformacionAduanera>();
                foreach (ComprobanteInformacionAduanera item in objetos)
                {
                    CFDI.V33.ComprobanteConceptoParteInformacionAduanera newItem = new CFDI.V33.ComprobanteConceptoParteInformacionAduanera();
                    newItem.NumeroPedimento = item.NumeroPedimento;
                    newItems.Add(newItem);
                }
                return newItems.ToArray();
            }
            return null;
        }

        /// <summary>
        /// crear Cfd.ConceptoParte V33.ComprobanteConceptoParte
        /// </summary>
        public CFDI.V33.ComprobanteConceptoParte[] ConceptoParte(BindingList<ConceptoParte> partes)
        {
            List<CFDI.V33.ComprobanteConceptoParte> item = new List<CFDI.V33.ComprobanteConceptoParte>();

            foreach (ConceptoParte parte in partes)
            {
                CFDI.V33.ComprobanteConceptoParte newItem = new CFDI.V33.ComprobanteConceptoParte();
                newItem.Cantidad = parte.Cantidad;
                newItem.ClaveProdServ = parte.ClaveProdServ;
                newItem.NoIdentificacion = parte.NoIdentificacion;
                newItem.Unidad = parte.Unidad;
                newItem.ValorUnitario = parte.ValorUnitario;
                newItem.ValorUnitarioSpecified = parte.ValorUnitario > 0;
                newItem.Descripcion = parte.Descripcion;
                newItem.Importe = parte.Importe;
                newItem.ImporteSpecified = parte.Importe > 0;
                if (!(parte.InformacionAduanera == null))
                {
                    newItem.InformacionAduanera = this.ConceptoParteInformacionAduanera(parte.InformacionAduanera);
                }
                item.Add(newItem);
            }
            return item.ToArray();
        }

        /// <summary>
        /// convertir lista Cfd.ConceptoImpuesto a V33.ComprobanteConceptoImpuestos
        /// </summary>
        public CFDI.V33.ComprobanteConceptoImpuestos ConceptoImpuestos(BindingList<ComprobanteConceptoImpuesto> objetos)
        {
            CFDI.V33.ComprobanteConceptoImpuestos impuestos = new CFDI.V33.ComprobanteConceptoImpuestos();
            List<CFDI.V33.ComprobanteConceptoImpuestosTraslado> listaTraslados = new List<CFDI.V33.ComprobanteConceptoImpuestosTraslado>();
            List<CFDI.V33.ComprobanteConceptoImpuestosRetencion> listaRetenciones = new List<CFDI.V33.ComprobanteConceptoImpuestosRetencion>();

            foreach (ComprobanteConceptoImpuesto item in objetos)
            {
                if (item.Tipo == EnumTipoImpuesto.Traslado)
                {
                    CFDI.V33.ComprobanteConceptoImpuestosTraslado newTraslado = new CFDI.V33.ComprobanteConceptoImpuestosTraslado();
                    newTraslado.Base = Math.Round(item.Base, 6);
                    newTraslado.Impuesto = string.Format("{0:000}", (int)item.Impuesto);
                    newTraslado.TasaOCuota = decimal.Round(item.TasaOCuota + new decimal(0.0000001), 6);//HelperComun.FormatoNumero(item.TasaOCuota, 6);
                    newTraslado.Importe = HelperComun.FormatoNumero(item.Importe, 2);
                    newTraslado.ImporteSpecified = item.Importe > 0;
                    newTraslado.TasaOCuotaSpecified = item.TasaOCuota > 0;
                    newTraslado.TipoFactor = Enum.GetName(typeof(EnumFactor), item.TipoFactor);
                    listaTraslados.Add(newTraslado);
                }
                else if (item.Tipo == EnumTipoImpuesto.Retencion)
                {
                    CFDI.V33.ComprobanteConceptoImpuestosRetencion newRetencion = new CFDI.V33.ComprobanteConceptoImpuestosRetencion();
                    newRetencion.Base = item.Base;
                    newRetencion.Importe = HelperComun.FormatoNumero(item.Importe, 2);
                    newRetencion.Impuesto = string.Format("{0:000}", (int)item.Impuesto);
                    newRetencion.TasaOCuota = decimal.Round(item.TasaOCuota + new decimal(0.0000001), 6); //HelperComun.FormatoNumero(item.TasaOCuota, 6);
                    newRetencion.TipoFactor = Enum.GetName(typeof(EnumFactor), item.TipoFactor);
                    listaRetenciones.Add(newRetencion);
                }
            }

            if (listaTraslados.Count > 0)
            {
                impuestos.Traslados = listaTraslados.ToArray();
            }

            if (listaRetenciones.Count > 0)
            {
                impuestos.Retenciones = listaRetenciones.ToArray();
            }
            return impuestos;
        }

        /// <summary>
        /// calcular los impuestos del objeto Cfd.Comprobante a V33.ComprobanteImpuestos
        /// </summary>
        public CFDI.V33.ComprobanteImpuestos Impuestos(ViewModelComprobante objeto)
        {
            CFDI.V33.ComprobanteImpuestos impuestos = new CFDI.V33.ComprobanteImpuestos();
            List<CFDI.V33.ComprobanteImpuestosTraslado> listaTraslados = new List<CFDI.V33.ComprobanteImpuestosTraslado>();
            List<CFDI.V33.ComprobanteImpuestosRetencion> listaRetenciones = new List<CFDI.V33.ComprobanteImpuestosRetencion>();

            if (!(objeto.Conceptos == null))
            {
                foreach (Jaeger.Edita.V2.CFDI.Entities.ViewModelComprobanteConcepto item in objeto.Conceptos)
                {
                    foreach (ComprobanteConceptoImpuesto impuesto in item.Impuestos)
                    {
                        int numeral = (int)impuesto.Impuesto;
                        if (impuesto.Tipo == EnumTipoImpuesto.Traslado)
                        {
                            CFDI.V33.ComprobanteImpuestosTraslado newTraslado = new CFDI.V33.ComprobanteImpuestosTraslado();
                            newTraslado.Impuesto = string.Format("{0:000}", numeral);
                            newTraslado.Importe = HelperComun.FormatoNumero(impuesto.Importe, 2);
                            newTraslado.TasaOCuota = decimal.Round(impuesto.TasaOCuota + new decimal(0.0000001), 6); //HelperComun.FormatoNumero(impuesto.TasaOCuota, 6);
                            newTraslado.TipoFactor = Enum.GetName(typeof(EnumFactor), impuesto.TipoFactor);
                            CFDI.V33.ComprobanteImpuestosTraslado buscar = listaTraslados.Find((CFDI.V33.ComprobanteImpuestosTraslado x) => x.Impuesto == newTraslado.Impuesto & x.TasaOCuota == newTraslado.TasaOCuota & x.TipoFactor == newTraslado.TipoFactor);
                            if (buscar == null)
                            {
                                listaTraslados.Add(newTraslado);
                            }
                            else
                            {
                                newTraslado.Importe = buscar.Importe + newTraslado.Importe;
                                listaTraslados.Remove(buscar);
                                listaTraslados.Add(newTraslado);
                            }
                        }
                        else if (impuesto.Tipo == EnumTipoImpuesto.Retencion)
                        {
                            CFDI.V33.ComprobanteImpuestosRetencion newRetencion = new CFDI.V33.ComprobanteImpuestosRetencion();
                            newRetencion.Impuesto = string.Format("{0:000}", numeral);
                            newRetencion.Importe = HelperComun.FormatoNumero(impuesto.Importe, 2);
                            CFDI.V33.ComprobanteImpuestosRetencion busca = new CFDI.V33.ComprobanteImpuestosRetencion();
                            if (busca != null)
                            {
                                newRetencion.Importe = decimal.Add(newRetencion.Importe, busca.Importe);
                                listaRetenciones.Remove(busca);
                            }
                            listaRetenciones.Add(newRetencion);
                        }
                    }
                }
            }

            if (listaTraslados.Count <= 0)
            {
                impuestos.TotalImpuestosTrasladadosSpecified = false;
            }
            else
            {
                impuestos.TotalImpuestosTrasladadosSpecified = true;
                impuestos.Traslados = listaTraslados.ToArray();
                impuestos.TotalImpuestosTrasladados = HelperComun.FormatoNumero(listaTraslados.Sum<CFDI.V33.ComprobanteImpuestosTraslado>((CFDI.V33.ComprobanteImpuestosTraslado p) => p.Importe), 2);
            }

            if (listaRetenciones.Count <= 0)
            {
                impuestos.TotalImpuestosRetenidosSpecified = false;
            }
            else
            {
                impuestos.TotalImpuestosRetenidosSpecified = true;
                impuestos.Retenciones = listaRetenciones.ToArray();
                impuestos.TotalImpuestosRetenidos = HelperComun.FormatoNumero(listaRetenciones.Sum<CFDI.V33.ComprobanteImpuestosRetencion>((CFDI.V33.ComprobanteImpuestosRetencion p) => p.Importe), 2);
            }
            return impuestos;
        }

        /// <summary>
        /// convertir objeto Cfd.ComprobantesRelacionados a V33.ComprobanteCfdiRelacionados
        /// </summary>
        public CFDI.V33.ComprobanteCfdiRelacionados ComprobanteCfdiRelacionados(ComprobanteCfdiRelacionados objeto)
        {
            CFDI.V33.ComprobanteCfdiRelacionados newItem = new CFDI.V33.ComprobanteCfdiRelacionados();
            List<CFDI.V33.ComprobanteCfdiRelacionadosCfdiRelacionado> lista = new List<CFDI.V33.ComprobanteCfdiRelacionadosCfdiRelacionado>();
            foreach (ComprobanteCfdiRelacionadosCfdiRelacionado item in objeto.CfdiRelacionado)
            {
                lista.Add(new CFDI.V33.ComprobanteCfdiRelacionadosCfdiRelacionado
                {
                    UUID = item.IdDocumento
                });
            }

            newItem.TipoRelacion = objeto.TipoRelacion.Clave;
            newItem.CfdiRelacionado = lista.ToArray();

            return newItem;
        }

        /// <summary>
        /// convertir array V33.ComprobanteConcepto a lista Cfd.Concepto
        /// </summary>
        public BindingList<ViewModelComprobanteConcepto> Conceptos(CFDI.V33.ComprobanteConcepto[] objetos)
        {
            BindingList<ViewModelComprobanteConcepto> newItems = new BindingList<ViewModelComprobanteConcepto>();
            foreach (CFDI.V33.ComprobanteConcepto objeto in objetos)
            {
                ViewModelComprobanteConcepto newItem = new ViewModelComprobanteConcepto();
                newItem.Cantidad = objeto.Cantidad;
                newItem.Descripcion = objeto.Descripcion;
                newItem.Importe = objeto.Importe;
                newItem.ValorUnitario = objeto.ValorUnitario;
                newItem.NoIdentificacion = objeto.NoIdentificacion;
                newItem.Unidad = objeto.Unidad;
                newItem.ClaveProdServ = objeto.ClaveProdServ;
                newItem.ClaveUnidad = objeto.ClaveUnidad;

                // impuestos
                if (!(objeto.Impuestos == null))
                {
                    //impuestos trasladados
                    if (!(objeto.Impuestos.Traslados == null))
                    {
                        foreach (CFDI.V33.ComprobanteConceptoImpuestosTraslado t in objeto.Impuestos.Traslados)
                        {
                            ComprobanteConceptoImpuesto itemT = new ComprobanteConceptoImpuesto();
                            itemT.Tipo = EnumTipoImpuesto.Traslado;

                            if (t.Impuesto == "002") // traslado IVA-002
                            {
                                itemT.Impuesto = EnumImpuesto.IVA;
                            }
                            else if (t.Impuesto == "003") //traslado IEPS
                            {
                                itemT.Impuesto = EnumImpuesto.IEPS;
                            }

                            if (t.TipoFactor.ToLower().Contains("tasa"))
                            {
                                itemT.TipoFactor = EnumFactor.Tasa;
                            }
                            else if (t.TipoFactor.ToLower().Contains("cuota"))
                            {
                                itemT.TipoFactor = EnumFactor.Cuota;
                            }

                            if (t.TasaOCuotaSpecified)
                            {
                                itemT.TasaOCuota = t.TasaOCuota;
                            }

                            if (t.ImporteSpecified)
                            {
                                itemT.Importe = t.Importe;
                            }

                            itemT.Base = t.Base;
                            newItem.Impuestos.Add(itemT);
                        }
                    }

                    // impuestos retenidos
                    if (!(objeto.Impuestos.Retenciones == null))
                    {
                        foreach (CFDI.V33.ComprobanteConceptoImpuestosRetencion r in objeto.Impuestos.Retenciones)
                        {
                            ComprobanteConceptoImpuesto itemR = new ComprobanteConceptoImpuesto();
                            itemR.Base = r.Base;
                            itemR.Importe = r.Importe;
                            itemR.Tipo = EnumTipoImpuesto.Retencion;
                            if (r.Impuesto == "002")
                            {
                                itemR.Impuesto = EnumImpuesto.IVA;
                            }
                            else if (r.Impuesto == "003")
                            {
                                itemR.Impuesto = EnumImpuesto.IEPS;
                            }
                            else if (r.Impuesto == "001")
                            {
                                itemR.Impuesto = EnumImpuesto.ISR;
                            }

                            if (r.TipoFactor.ToLower().Contains("tasa"))
                            {
                                itemR.TipoFactor = EnumFactor.Tasa;
                            }
                            else if (r.TipoFactor.ToLower().Contains("cuota"))
                            {
                                itemR.TipoFactor = EnumFactor.Cuota;
                            }
                            else if (r.TipoFactor.ToLower().Contains("exento"))
                            {
                                itemR.TipoFactor = EnumFactor.Exento;
                            }
                            newItem.Impuestos.Add(itemR);
                        }
                    }
                }

                if (!(objeto.CuentaPredial == null))
                {
                    if (!(objeto.CuentaPredial.Numero == null))
                    {
                        newItem.CtaPredial = objeto.CuentaPredial.Numero;
                    }
                }

                if (objeto.DescuentoSpecified)
                {
                    newItem.Descuento = objeto.Descuento;
                }
                else
                {
                    newItem.Descuento = 0;
                }

                // objeto concepto parte
                if (!(objeto.Parte == null))
                {
                    foreach (CFDI.V33.ComprobanteConceptoParte subItem in objeto.Parte)
                    {
                        newItem.Parte.Add(ConceptoParte(subItem));
                    }
                }

                // objeto informacion aduanera
                if (!(objeto.InformacionAduanera == null))
                {
                    newItem.InformacionAduanera = this.ConceptoInformacionAduanera(objeto.InformacionAduanera);
                }

                // agregamos el nuevo objeto a la lista
                newItems.Add(newItem);
            }
            return newItems;
        }

        /// <summary>
        /// convertir V33.ComprobanteConceptoParte a Cfd.ConceptoParte
        /// </summary>
        public ConceptoParte ConceptoParte(CFDI.V33.ComprobanteConceptoParte objeto)
        {
            ConceptoParte newItem = new ConceptoParte();
            newItem.Cantidad = objeto.Cantidad;
            newItem.ClaveProdServ = objeto.ClaveProdServ;
            newItem.Descripcion = objeto.Descripcion;
            newItem.NoIdentificacion = objeto.NoIdentificacion;
            newItem.Unidad = objeto.Unidad;

            if (objeto.ValorUnitarioSpecified)
            {
                newItem.ValorUnitario = objeto.ValorUnitario;
            }

            if (objeto.ImporteSpecified)
            {
                newItem.Importe = objeto.Importe;
            }

            if (!(objeto.InformacionAduanera == null))
            {
                newItem.InformacionAduanera = this.ConceptoParteInformacionAduanera(objeto.InformacionAduanera);
            }
            return newItem;
        }

        /// <summary>
        /// convertir un array ComprobanteConceptoInformacionAduanera a lista de ComprobanteInformacionAduanera comun
        /// </summary>
        public BindingList<ComprobanteInformacionAduanera> ConceptoInformacionAduanera(CFDI.V33.ComprobanteConceptoInformacionAduanera[] objetos)
        {
            if (!(objetos == null))
            {
                BindingList<ComprobanteInformacionAduanera> newItems = new BindingList<ComprobanteInformacionAduanera>();
                foreach (CFDI.V33.ComprobanteConceptoInformacionAduanera item in objetos)
                {
                    ComprobanteInformacionAduanera newItem = new ComprobanteInformacionAduanera();
                    newItem.NumeroPedimento = item.NumeroPedimento;
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        /// <summary>
        /// convertir un array ComprobanteConceptoParteInformacionAduanera a lista de ComprobanteInformacionAduanera comun
        /// </summary>
        public BindingList<ComprobanteInformacionAduanera> ConceptoParteInformacionAduanera(CFDI.V33.ComprobanteConceptoParteInformacionAduanera[] objetos)
        {
            if (!(objetos == null))
            {
                BindingList<ComprobanteInformacionAduanera> newItems = new BindingList<ComprobanteInformacionAduanera>();
                foreach (CFDI.V33.ComprobanteConceptoParteInformacionAduanera item in objetos)
                {
                    ComprobanteInformacionAduanera newItem = new ComprobanteInformacionAduanera();
                    newItem.NumeroPedimento = item.NumeroPedimento;
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        public ComprobanteCfdiRelacionados ComprobanteCfdiRelacionados(CFDI.V33.ComprobanteCfdiRelacionados objeto)
        {
            if (!(objeto == null))
            {
                ComprobanteCfdiRelacionados newItem = new ComprobanteCfdiRelacionados();
                newItem.TipoRelacion.Clave = objeto.TipoRelacion;
                foreach (CFDI.V33.ComprobanteCfdiRelacionadosCfdiRelacionado item in objeto.CfdiRelacionado)
                {
                    ComprobanteCfdiRelacionadosCfdiRelacionado newItem2 = new ComprobanteCfdiRelacionadosCfdiRelacionado();
                    newItem2.IdDocumento = item.UUID;
                    newItem.CfdiRelacionado.Add(newItem2);
                }
                return newItem;
            }
            return null;
        }

        #endregion

        #region complemento timbre fiscal version 11

        public ComplementoTimbreFiscal TimbreFiscal(CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital objeto)
        {
            ComplementoTimbreFiscal newItem = new ComplementoTimbreFiscal();
            newItem.UUID = objeto.UUID.ToUpper();
            newItem.Version = objeto.Version;
            newItem.FechaTimbrado = objeto.FechaTimbrado;
            newItem.Leyenda = objeto.Leyenda;
            newItem.NoCertificadoSAT = objeto.NoCertificadoSAT;
            newItem.RFCProvCertif = objeto.RfcProvCertif;
            newItem.SelloCFD = objeto.SelloCFD;
            newItem.SelloSAT = objeto.SelloSAT;
            return newItem;
        }

        #endregion

        #region complemento Nomina11

        /// <summary>
        /// convertir complemento nomina a objeto nomina comun v11
        /// </summary>
        public ComplementoNomina Create(Jaeger.CFDI.Complemento.Nomina.V11.Nomina objeto)
        {
            // en esta version del complemento no existe nodo emisor ni receptor
            ComplementoNomina newItem = new ComplementoNomina();
            // general
            newItem.FechaFinalPago = objeto.FechaFinalPago;
            newItem.FechaInicialPago = objeto.FechaFinalPago;
            newItem.FechaPago = objeto.FechaPago;
            newItem.TipoNomina = "";
            newItem.Version = objeto.Version;
            newItem.NumDiasPagados = objeto.NumDiasPagados;
            newItem.Emisor = new ComplementoNominaEmisor();
            newItem.Emisor.RegistroPatronal = objeto.RegistroPatronal;

            newItem.Receptor = new ComplementoNominaReceptor();
            newItem.Receptor.Antiguedad = objeto.Antiguedad.ToString();
            newItem.Receptor.Banco = objeto.Banco.ToString();
            newItem.Receptor.CuentaBancaria = objeto.CLABE;
            newItem.Receptor.CURP = objeto.CURP;
            newItem.Receptor.Departamento = objeto.Departamento;
            newItem.Receptor.Num = int.Parse(objeto.NumEmpleado);
            newItem.Receptor.NumSeguridadSocial = objeto.NumSeguridadSocial;
            newItem.Receptor.PeriodicidadPago = objeto.PeriodicidadPago;
            newItem.Receptor.Puesto = objeto.Puesto;
            newItem.Receptor.RiesgoPuesto = objeto.RiesgoPuesto.ToString();
            newItem.Receptor.SalarioBaseCotApor = objeto.SalarioBaseCotApor;
            newItem.Receptor.SalarioDiarioIntegrado = objeto.SalarioDiarioIntegrado;
            newItem.Receptor.TipoContrato = objeto.TipoContrato;
            newItem.Receptor.TipoJornada = objeto.TipoJornada;
            newItem.Receptor.TipoRegimen = objeto.TipoRegimen.ToString();
            newItem.Receptor.FechaInicioRelLaboral = objeto.FechaInicioRelLaboral;
            newItem.Percepciones = this.Nomina11(objeto.Percepciones);
            newItem.Deducciones = this.Nomina11(objeto.Deducciones);
            newItem.Incapacidades = this.Nomina11(objeto.Incapacidades);

            return newItem;
        }

        public ComplementoNominaPercepciones Nomina11(Jaeger.CFDI.Complemento.Nomina.V11.NominaPercepciones objeto)
        {
            ComplementoNominaPercepciones newPercepciones = new ComplementoNominaPercepciones();
            newPercepciones.Percepcion = new BindingList<ComplementoNominaPercepcion>();
            foreach (Jaeger.CFDI.Complemento.Nomina.V11.NominaPercepcionesPercepcion item in objeto.Percepcion)
            {
                ComplementoNominaPercepcion newPercepcion = new ComplementoNominaPercepcion();
                newPercepcion.TipoPercepcion = item.TipoPercepcion.ToString();
                newPercepcion.Clave = item.Clave;
                newPercepcion.Concepto = item.Concepto;
                newPercepcion.ImporteExento = item.ImporteExento;
                newPercepcion.ImporteGravado = item.ImporteGravado;
                newPercepciones.Percepcion.Add(newPercepcion);
            }
            newPercepciones.TotalExento = objeto.TotalExento;
            newPercepciones.TotalGravado = objeto.TotalGravado;
            return newPercepciones;
        }

        public ComplementoNominaDeducciones Nomina11(Jaeger.CFDI.Complemento.Nomina.V11.NominaDeducciones objeto)
        {
            ComplementoNominaDeducciones newItem = new ComplementoNominaDeducciones();
            newItem.Deduccion = new BindingList<ComplementoNominaDeduccion>();
            Jaeger.CFDI.Complemento.Nomina.V11.NominaDeduccionesDeduccion[] deduccion = objeto.Deduccion;
            for (int i = 0; i < deduccion.Length; i = i + 1)
            {
                Jaeger.CFDI.Complemento.Nomina.V11.NominaDeduccionesDeduccion item = deduccion[i];
                ComplementoNominaDeduccion newSubItem = new ComplementoNominaDeduccion()
                {
                    Clave = item.Clave,
                    Concepto = item.Concepto,
                    Importe = item.ImporteExento,
                    TipoDeduccion = item.TipoDeduccion.ToString()
                };
                newItem.Deduccion.Add(newSubItem);
            }

            newItem.TotalExento = objeto.TotalExento;
            newItem.TotalGravado = objeto.TotalGravado;
            return newItem;
        }

        public ComplementoNominaPercepcionHorasExtra Nomina11(Jaeger.CFDI.Complemento.Nomina.V11.NominaHorasExtra objeto)
        {
            ComplementoNominaPercepcionHorasExtra horasExtra = new ComplementoNominaPercepcionHorasExtra();
            horasExtra.Dias = objeto.Dias;
            horasExtra.HorasExtra = objeto.HorasExtra;
            horasExtra.ImportePagado = objeto.ImportePagado;
            horasExtra.TipoHoras = objeto.TipoHoras.ToString();
            return horasExtra;
        }

        public BindingList<ComplementoNominaIncapacidad> Nomina11(Jaeger.CFDI.Complemento.Nomina.V11.NominaIncapacidad[] objetos)
        {
            if (objetos != null)
            {
                BindingList<ComplementoNominaIncapacidad> incapaciades = new BindingList<ComplementoNominaIncapacidad>();
                foreach (Jaeger.CFDI.Complemento.Nomina.V11.NominaIncapacidad item in objetos)
                {
                    ComplementoNominaIncapacidad incapacidad = new ComplementoNominaIncapacidad();
                    incapacidad.DiasIncapacidad = Convert.ToInt32(item.DiasIncapacidad);
                    incapacidad.ImporteMonetario = item.Descuento;
                    incapacidad.TipoIncapacidad = item.TipoIncapacidad.ToString();
                    incapaciades.Add(incapacidad);
                }
                return incapaciades;
            }
            return null;
        }

        #endregion

        #region complemento Nomina12

        public ComplementoNomina Create(Jaeger.CFDI.Complemento.Nomina.V12.Nomina objeto)
        {
            ComplementoNomina newItem = new ComplementoNomina();
            // general
            newItem.FechaFinalPago = objeto.FechaFinalPago;
            newItem.FechaInicialPago = objeto.FechaFinalPago;
            newItem.FechaPago = objeto.FechaPago;
            newItem.TipoNomina = objeto.TipoNomina;
            newItem.Version = objeto.Version;
            newItem.NumDiasPagados = objeto.NumDiasPagados;
            // emisor
            newItem.Emisor = this.Nomina12(objeto.Emisor);
            // receptor
            newItem.Receptor = this.Nomina12(objeto.Receptor);
            // percepciones
            newItem.Percepciones = this.Nomina12(objeto.Percepciones);
            // deducciones
            newItem.Deducciones = this.Nomina12(objeto.Deducciones);
            // otros pafos
            newItem.OtrosPagos = this.Nomina12(objeto.OtrosPagos);
            // incapacidades
            newItem.Incapacidades = this.Nomina12(objeto.Incapacidades);
            newItem.TotalDeducciones = objeto.TotalDeducciones;
            newItem.TotalOtrosPagos = objeto.TotalOtrosPagos;
            newItem.TotalPercepciones = objeto.TotalPercepciones;

            return newItem;
        }

        public ComplementoNominaPercepciones Nomina12(Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepciones objeto)
        {
            ComplementoNominaPercepciones newItem = new ComplementoNominaPercepciones();
            newItem.Percepcion = new BindingList<ComplementoNominaPercepcion>();
            foreach (Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcion item in objeto.Percepcion)
            {
                ComplementoNominaPercepcion newPercepcion = new ComplementoNominaPercepcion();
                newPercepcion.Clave = item.Clave;
                newPercepcion.Concepto = item.Concepto;
                newPercepcion.TipoPercepcion = item.TipoPercepcion;
                newPercepcion.ImporteExento = item.ImporteExento;
                newPercepcion.ImporteGravado = item.ImporteGravado;

                if (item.AccionesOTitulos != null)
                {
                    newPercepcion.AccionesOTitulos = new ComplementoNominaPercepcionAccionesOTitulos();
                    newPercepcion.AccionesOTitulos.PrecioAlOtorgarse = item.AccionesOTitulos.PrecioAlOtorgarse;
                    newPercepcion.AccionesOTitulos.ValorMercado = item.AccionesOTitulos.ValorMercado;
                }

                if (item.HorasExtra != null)
                {
                    newPercepcion.HorasExtra = new BindingList<ComplementoNominaPercepcionHorasExtra>();
                    foreach (Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra itemHorasExtra in item.HorasExtra)
                    {
                        ComplementoNominaPercepcionHorasExtra newHorasExtra = new ComplementoNominaPercepcionHorasExtra();
                        newHorasExtra.Dias = itemHorasExtra.Dias;
                        newHorasExtra.HorasExtra = itemHorasExtra.HorasExtra;
                        newHorasExtra.ImportePagado = itemHorasExtra.ImportePagado;
                        newHorasExtra.TipoHoras = itemHorasExtra.TipoHoras;
                        newPercepcion.HorasExtra.Add(newHorasExtra);
                    }
                }
                newItem.Percepcion.Add(newPercepcion);
            }
            if (objeto.TotalJubilacionPensionRetiroSpecified)
            {
                newItem.TotalSeparacionIndemnizacion = objeto.TotalSeparacionIndemnizacion;
            }
            if (objeto.TotalSeparacionIndemnizacionSpecified)
            {
                newItem.TotalJubilacionPensionRetiro = objeto.TotalJubilacionPensionRetiro;
            }
            newItem.TotalSueldos = objeto.TotalSueldos;
            newItem.TotalExento = objeto.TotalExento;
            newItem.TotalGravado = objeto.TotalGravado;
            return newItem;
        }

        public ComplementoNominaDeducciones Nomina12(Jaeger.CFDI.Complemento.Nomina.V12.NominaDeducciones objeto)
        {
            ComplementoNominaDeducciones newItem = new ComplementoNominaDeducciones();
            newItem.Deduccion = new BindingList<ComplementoNominaDeduccion>();
            foreach (Jaeger.CFDI.Complemento.Nomina.V12.NominaDeduccionesDeduccion item in objeto.Deduccion)
            {
                ComplementoNominaDeduccion newSubItem = new ComplementoNominaDeduccion();
                newSubItem.Clave = item.Clave;
                newSubItem.TipoDeduccion = item.TipoDeduccion;
                newSubItem.Concepto = item.Concepto;
                newSubItem.Importe = item.Importe;
                newItem.Deduccion.Add(newSubItem);
            }
            newItem.TotalImpuestosRetenidos = objeto.TotalImpuestosRetenidos;
            newItem.TotalOtrasDeducciones = objeto.TotalOtrasDeducciones;
            return newItem;
        }

        public BindingList<ComplementoNominaOtroPago> Nomina12(Jaeger.CFDI.Complemento.Nomina.V12.NominaOtroPago[] objetos)
        {
            if (objetos != null)
            {
                BindingList<ComplementoNominaOtroPago> newItems = new BindingList<ComplementoNominaOtroPago>();
                foreach (Jaeger.CFDI.Complemento.Nomina.V12.NominaOtroPago item in objetos)
                {
                    ComplementoNominaOtroPago newItem = new ComplementoNominaOtroPago();
                    newItem.Clave = item.Clave;
                    newItem.Concepto = item.Concepto;
                    newItem.Importe = item.Importe;
                    newItem.TipoOtroPago = item.TipoOtroPago;
                    if (item.SubsidioAlEmpleo != null)
                    {
                        if (item.SubsidioAlEmpleo.SubsidioCausado != null)
                        {
                            newItem.SubsidioAlEmpleo = this.Nomina12(item.SubsidioAlEmpleo);
                        }
                    }

                    if (item.CompensacionSaldosAFavor != null)
                    {
                        newItem.CompensacionSaldosAFavor = new ComplementoNominaOtroPagoCompensacionSaldosAFavor();
                        newItem.CompensacionSaldosAFavor.Anio = item.CompensacionSaldosAFavor.Año;
                        newItem.CompensacionSaldosAFavor.RemanenteSalFav = item.CompensacionSaldosAFavor.RemanenteSalFav;
                        newItem.CompensacionSaldosAFavor.SaldoAFavor = item.CompensacionSaldosAFavor.SaldoAFavor;
                    }
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        public ComplementoNominaOtroPagoSubsidioAlEmpleo Nomina12(Jaeger.CFDI.Complemento.Nomina.V12.NominaOtroPagoSubsidioAlEmpleo objeto)
        {
            if (objeto != null)
            {
                ComplementoNominaOtroPagoSubsidioAlEmpleo nuevo = new ComplementoNominaOtroPagoSubsidioAlEmpleo();
                nuevo.SubsidioCausado = objeto.SubsidioCausado;
                return nuevo;
            }
            return null;
        }

        public ComplementoNominaEmisor Nomina12(Jaeger.CFDI.Complemento.Nomina.V12.NominaEmisor objeto)
        {
            ComplementoNominaEmisor newItem = new ComplementoNominaEmisor();
            newItem.CURP = objeto.Curp;
            newItem.RegistroPatronal = objeto.RegistroPatronal;
            newItem.RFCPatronOrigen = objeto.RfcPatronOrigen;
            if (objeto.EntidadSNCF != null)
            {
                if (objeto.EntidadSNCF.MontoRecursoPropioSpecified)
                {
                    newItem.EntidadSncf = new ComplementoNominaEmisorEntidadSncf();
                    newItem.EntidadSncf.MontoRecursoPropio = objeto.EntidadSNCF.MontoRecursoPropio;
                    newItem.EntidadSncf.OrigenRecurso = objeto.EntidadSNCF.OrigenRecurso;
                }
            }

            return newItem;
        }

        public ComplementoNominaReceptor Nomina12(Jaeger.CFDI.Complemento.Nomina.V12.NominaReceptor objeto)
        {
            ComplementoNominaReceptor newItem = new ComplementoNominaReceptor();
            newItem.Antiguedad = objeto.Antigüedad;
            newItem.Banco = objeto.Banco;
            newItem.ClaveEntFed = objeto.ClaveEntFed;
            newItem.CuentaBancaria = objeto.CuentaBancaria;
            newItem.CURP = objeto.Curp;
            newItem.Departamento = objeto.Departamento;
            newItem.FechaInicioRelLaboral = objeto.FechaInicioRelLaboral;
            newItem.Num = int.Parse(objeto.NumEmpleado);
            newItem.NumSeguridadSocial = objeto.NumSeguridadSocial;
            newItem.PeriodicidadPago = objeto.PeriodicidadPago;
            newItem.Puesto = objeto.Puesto;
            newItem.RiesgoPuesto = objeto.RiesgoPuesto;
            newItem.SalarioBaseCotApor = objeto.SalarioBaseCotApor;
            newItem.SalarioDiarioIntegrado = objeto.SalarioDiarioIntegrado;
            newItem.TipoContrato = objeto.TipoContrato;
            newItem.TipoJornada = objeto.TipoJornada;
            newItem.TipoRegimen = objeto.TipoRegimen;

            newItem.Sindicalizado = null;
            if (objeto.Sindicalizado != null)
            {
                if (objeto.SindicalizadoSpecified)
                {
                    newItem.Sindicalizado = objeto.Sindicalizado.ToString();
                }
            }
            return newItem;
        }

        public BindingList<ComplementoNominaIncapacidad> Nomina12(Jaeger.CFDI.Complemento.Nomina.V12.NominaIncapacidad[] objetos)
        {
            if (objetos != null)
            {
                BindingList<ComplementoNominaIncapacidad> newItems = new BindingList<ComplementoNominaIncapacidad>();
                foreach (Jaeger.CFDI.Complemento.Nomina.V12.NominaIncapacidad item in objetos)
                {
                    ComplementoNominaIncapacidad newItem = new ComplementoNominaIncapacidad();
                    newItem.DiasIncapacidad = item.DiasIncapacidad;
                    newItem.ImporteMonetario = item.ImporteMonetario;
                    newItem.TipoIncapacidad = item.TipoIncapacidad;
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        /// <summary>
        /// crear complemento de nomina a partir de la clase comun ComplementoNomina
        /// </summary>
        public Jaeger.CFDI.Complemento.Nomina.V12.Nomina Create(ComplementoNomina objeto)
        {
            if (objeto != null)
            {
                Jaeger.CFDI.Complemento.Nomina.V12.Nomina newComplemento = new CFDI.Complemento.Nomina.V12.Nomina();
                newComplemento.Emisor = this.Nomina12(objeto.Emisor);
                newComplemento.Receptor = this.Nomina12(objeto.Receptor);
                newComplemento.FechaInicialPago = objeto.FechaInicialPago.Value;
                newComplemento.FechaFinalPago = objeto.FechaFinalPago.Value;
                newComplemento.FechaPago = objeto.FechaPago.Value;
                newComplemento.NumDiasPagados = objeto.NumDiasPagados;
                newComplemento.TipoNomina = objeto.TipoNomina;
                newComplemento.Percepciones = this.Nomina12(objeto.Percepciones);
                newComplemento.Deducciones = this.Nomina12(objeto.Deducciones);
                newComplemento.Incapacidades = this.Nomina12(objeto.Incapacidades);
                newComplemento.OtrosPagos = this.Nomina12(objeto.OtrosPagos);
                return newComplemento;
            }
            return null;
        }

        public Jaeger.CFDI.Complemento.Nomina.V12.NominaEmisor Nomina12(ComplementoNominaEmisor objeto)
        {
            Jaeger.CFDI.Complemento.Nomina.V12.NominaEmisor nuevoEmisor = new CFDI.Complemento.Nomina.V12.NominaEmisor();
            nuevoEmisor.Curp = objeto.CURP;
            nuevoEmisor.RegistroPatronal = objeto.RegistroPatronal;
            nuevoEmisor.RfcPatronOrigen = null;
            if (objeto.RFCPatronOrigen != null)
            {
                nuevoEmisor.RfcPatronOrigen = objeto.RFCPatronOrigen;
            }
            nuevoEmisor.EntidadSNCF = null;
            if (objeto.EntidadSncf != null)
            {
                nuevoEmisor.EntidadSNCF = new CFDI.Complemento.Nomina.V12.NominaEmisorEntidadSNCF();
                nuevoEmisor.EntidadSNCF.MontoRecursoPropio = objeto.EntidadSncf.MontoRecursoPropio;
                nuevoEmisor.EntidadSNCF.OrigenRecurso = objeto.EntidadSncf.OrigenRecurso;
                nuevoEmisor.EntidadSNCF.MontoRecursoPropioSpecified = true;
            }
            return nuevoEmisor;
        }

        /// <summary>
        /// falta terminar
        /// </summary>
        public Jaeger.CFDI.Complemento.Nomina.V12.NominaReceptor Nomina12(ComplementoNominaReceptor objeto)
        {
            Jaeger.CFDI.Complemento.Nomina.V12.NominaReceptor nuevoReceptor = new CFDI.Complemento.Nomina.V12.NominaReceptor();
            nuevoReceptor.Antigüedad = objeto.Antiguedad;
            nuevoReceptor.Banco = objeto.Banco;
            nuevoReceptor.BancoSpecified = true;
            nuevoReceptor.ClaveEntFed = objeto.ClaveEntFed;
            nuevoReceptor.CuentaBancaria = objeto.CuentaBancaria;
            nuevoReceptor.Curp = objeto.CURP;
            return nuevoReceptor;
        }

        public Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepciones Nomina12(ComplementoNominaPercepciones objetos)
        {
            if (objetos != null)
            {
                Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepciones nuevoPercepciones = new CFDI.Complemento.Nomina.V12.NominaPercepciones();
                List<Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcion> lista = new List<CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcion>();
                // lista de percepciones
                foreach (Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoNominaPercepcion item in objetos.Percepcion)
                {
                    Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcion nuevaPercepcion = new CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcion();
                    nuevaPercepcion.TipoPercepcion = item.TipoPercepcion;
                    nuevaPercepcion.Clave = item.Clave;
                    nuevaPercepcion.Concepto = item.Concepto;
                    nuevaPercepcion.ImporteExento = item.ImporteExento;
                    nuevaPercepcion.ImporteGravado = item.ImporteGravado;
                    nuevaPercepcion.AccionesOTitulos = this.Nomina12(item.AccionesOTitulos);
                    nuevaPercepcion.HorasExtra = this.Nomina12(item.HorasExtra);
                    lista.Add(nuevaPercepcion);
                }
                nuevoPercepciones.Percepcion = lista.ToArray();
                nuevoPercepciones.JubilacionPensionRetiro = this.Nomina12(objetos.JubilacionPensionRetiro);
                nuevoPercepciones.SeparacionIndemnizacion = this.Nomina12(objetos.SeparacionIndemnizacion);
                return nuevoPercepciones;
            }
            return null;
        }

        public Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionAccionesOTitulos Nomina12(ComplementoNominaPercepcionAccionesOTitulos objetos)
        {
            if (objetos != null)
            {
                Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionAccionesOTitulos nuevo = new CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionAccionesOTitulos();
                nuevo.PrecioAlOtorgarse = objetos.PrecioAlOtorgarse;
                nuevo.ValorMercado = objetos.ValorMercado;
                return nuevo;
            }
            return null;
        }

        private CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra[] Nomina12(BindingList<ComplementoNominaPercepcionHorasExtra> objetos)
        {
            if (objetos != null)
            {
                List<CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra> lista = new List<CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra>();
                foreach (ComplementoNominaPercepcionHorasExtra item in objetos)
                {
                    CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra nuevo = new CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra();
                    nuevo.Dias = item.Dias;
                    nuevo.HorasExtra = item.HorasExtra;
                    nuevo.ImportePagado = item.ImportePagado;
                    nuevo.TipoHoras = item.TipoHoras;
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        public Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesJubilacionPensionRetiro Nomina12(ComplementoNominaPercepcionesJubilacionPensionRetiro objetos)
        {
            if (objetos != null)
            {
                Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesJubilacionPensionRetiro nuevo = new CFDI.Complemento.Nomina.V12.NominaPercepcionesJubilacionPensionRetiro();
                nuevo.IngresoAcumulable = objetos.IngresoAcumulable;
                nuevo.IngresoNoAcumulable = objetos.IngresoNoAcumulable;
                nuevo.MontoDiario = objetos.MontoDiario;
                nuevo.TotalParcialidad = objetos.TotalParcialidad;
                nuevo.TotalUnaExhibicion = objetos.TotalUnaExhibicion;
            }
            return null;
        }

        private CFDI.Complemento.Nomina.V12.NominaPercepcionesSeparacionIndemnizacion Nomina12(ComplementoNominaPercepcionesSeparacionIndemnizacion objetos)
        {
            if (objetos != null)
            {
                CFDI.Complemento.Nomina.V12.NominaPercepcionesSeparacionIndemnizacion nuevo = new CFDI.Complemento.Nomina.V12.NominaPercepcionesSeparacionIndemnizacion();
                nuevo.IngresoAcumulable = objetos.IngresoAcumulable;
                nuevo.IngresoNoAcumulable = objetos.IngresoNoAcumulable;
                nuevo.NumAñosServicio = objetos.NumaniosServicio;
                nuevo.TotalPagado = objetos.TotalPagado;
                nuevo.UltimoSueldoMensOrd = objetos.UltimoSueldoMensOrd;
                return nuevo;
            }
            return null;
        }

        private CFDI.Complemento.Nomina.V12.NominaDeducciones Nomina12(ComplementoNominaDeducciones objetos)
        {
            if (objetos != null)
            {
                CFDI.Complemento.Nomina.V12.NominaDeducciones nuevo = new CFDI.Complemento.Nomina.V12.NominaDeducciones();
                List<CFDI.Complemento.Nomina.V12.NominaDeduccionesDeduccion> lista = new List<CFDI.Complemento.Nomina.V12.NominaDeduccionesDeduccion>();
                foreach (ComplementoNominaDeduccion item in objetos.Deduccion)
                {
                    CFDI.Complemento.Nomina.V12.NominaDeduccionesDeduccion nuevaDeduccion = new CFDI.Complemento.Nomina.V12.NominaDeduccionesDeduccion();
                    nuevaDeduccion.Clave = item.Clave;
                    nuevaDeduccion.Concepto = item.Concepto;
                    nuevaDeduccion.Importe = item.Importe;
                    nuevaDeduccion.TipoDeduccion = item.TipoDeduccion;
                    lista.Add(nuevaDeduccion);
                }
                nuevo.Deduccion = lista.ToArray();
                nuevo.TotalImpuestosRetenidos = objetos.TotalImpuestosRetenidos;
                nuevo.TotalOtrasDeducciones = objetos.TotalOtrasDeducciones;
                return nuevo;
            }
            return null;
        }

        private CFDI.Complemento.Nomina.V12.NominaIncapacidad[] Nomina12(BindingList<ComplementoNominaIncapacidad> objetos)
        {
            if (objetos != null)
            {
                List<CFDI.Complemento.Nomina.V12.NominaIncapacidad> lista = new List<CFDI.Complemento.Nomina.V12.NominaIncapacidad>();
                foreach (ComplementoNominaIncapacidad item in objetos)
                {
                    CFDI.Complemento.Nomina.V12.NominaIncapacidad nuevo = new CFDI.Complemento.Nomina.V12.NominaIncapacidad();
                    nuevo.DiasIncapacidad = item.DiasIncapacidad;
                    nuevo.ImporteMonetario = item.ImporteMonetario;
                    nuevo.TipoIncapacidad = item.TipoIncapacidad;
                    nuevo.ImporteMonetarioSpecified = item.ImporteMonetario > 0;
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        private CFDI.Complemento.Nomina.V12.NominaOtroPago[] Nomina12(BindingList<ComplementoNominaOtroPago> objetos)
        {
            if (objetos != null)
            {
                List<CFDI.Complemento.Nomina.V12.NominaOtroPago> lista = new List<CFDI.Complemento.Nomina.V12.NominaOtroPago>();
                foreach (ComplementoNominaOtroPago item in objetos)
                {
                    CFDI.Complemento.Nomina.V12.NominaOtroPago nuevo = new CFDI.Complemento.Nomina.V12.NominaOtroPago();
                    nuevo.Clave = item.Clave;
                    nuevo.Concepto = item.Concepto;
                    nuevo.Importe = item.Importe;
                    nuevo.TipoOtroPago = item.TipoOtroPago;
                    nuevo.CompensacionSaldosAFavor = this.Nomina12(item.CompensacionSaldosAFavor);
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        private CFDI.Complemento.Nomina.V12.NominaOtroPagoCompensacionSaldosAFavor Nomina12(ComplementoNominaOtroPagoCompensacionSaldosAFavor objeto)
        {
            if (objeto != null)
            {
                CFDI.Complemento.Nomina.V12.NominaOtroPagoCompensacionSaldosAFavor nuevo = new CFDI.Complemento.Nomina.V12.NominaOtroPagoCompensacionSaldosAFavor();
                nuevo.Año = objeto.Anio;
                nuevo.RemanenteSalFav = objeto.RemanenteSalFav;
                nuevo.SaldoAFavor = objeto.SaldoAFavor;
                return nuevo;
            }
            return null;
        }

        #endregion

        #region complemento pagos10

        /// <summary>
        /// convertir complemento pagos10 a la clase basica
        /// </summary>
        public Entities.Basico.DocumentoRelacionado ComplementoPagos10ToBasic(CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado objeto)
        {
            Entities.Basico.DocumentoRelacionado item = new Entities.Basico.DocumentoRelacionado();
            item.IdDocumento = objeto.IdDocumento;
            item.Folio = objeto.Folio;
            item.ImpPagado = Jaeger.Helpers.DbConvert.ConvertDouble(objeto.ImpPagado);
            item.ImpSaldoAnterior = Jaeger.Helpers.DbConvert.ConvertDouble(objeto.ImpSaldoAnt);
            item.ImpSaldoInsoluto = Jaeger.Helpers.DbConvert.ConvertDouble(objeto.ImpSaldoInsoluto);
            item.MetodoDePago = objeto.MetodoDePagoDR;
            item.Moneda = objeto.MonedaDR;
            item.NumParcialidad = Jaeger.Helpers.DbConvert.ConvertInt32(objeto.NumParcialidad);
            item.Serie = objeto.Serie;
            item.TipoCambio = objeto.TipoCambioDR;
            return item;
        }

        public List<Entities.Basico.DocumentoRelacionado> ComplementoPago10ToBasic(CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado[] doctos)
        {
            List<Entities.Basico.DocumentoRelacionado> newList = new List<Entities.Basico.DocumentoRelacionado>();
            if (doctos == null)
            {
                return newList;
            }

            foreach (CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado item10 in doctos)
            {
                newList.Add(new Entities.Basico.DocumentoRelacionado
                {
                    IdDocumento = item10.IdDocumento,
                    Folio = item10.Folio,
                    ImpPagado = Jaeger.Helpers.DbConvert.ConvertDouble(item10.ImpPagado),
                    ImpSaldoAnterior = Jaeger.Helpers.DbConvert.ConvertDouble(item10.ImpSaldoAnt),
                    ImpSaldoInsoluto = Jaeger.Helpers.DbConvert.ConvertDouble(item10.ImpSaldoInsoluto),
                    MetodoDePago = item10.MetodoDePagoDR,
                    Moneda = item10.MonedaDR,
                    NumParcialidad = Jaeger.Helpers.DbConvert.ConvertInt32(item10.NumParcialidad),
                    Serie = item10.Serie,
                    TipoCambio = item10.TipoCambioDR
                });
            }
            return newList;
        }

        /// <summary>
        /// convertir complemento de pagos version 10 a ComplementoPagos10 comun
        /// </summary>
        public ComplementoPagos Create(Jaeger.CFDI.Complemento.Pagos.V10.Pagos objeto)
        {
            Jaeger.Edita.V2.CFDI.Entities.ComplementoPagos newItem = new ComplementoPagos();
            newItem.Version = objeto.Version;
            newItem.Pago = this.Pagos10(objeto.Pago);
            return newItem;
        }

        /// <summary>
        /// convertir un array de Pagos10.PagosPago[] al objeto comun
        /// </summary>
        public BindingList<ComplementoPagosPago> Pagos10(Jaeger.CFDI.Complemento.Pagos.V10.PagosPago[] objetos)
        {
            BindingList<Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoPagosPago> newItems = new BindingList<ComplementoPagosPago>();
            foreach (Jaeger.CFDI.Complemento.Pagos.V10.PagosPago item in objetos)
            {
                Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoPagosPago newItem = new ComplementoPagosPago();
                newItem.CadPago = item.CadPago;
                newItem.CtaBeneficiario = item.CtaBeneficiario;
                newItem.CtaOrdenante = item.CtaOrdenante;
                newItem.FechaPago = item.FechaPago;
                newItem.FormaDePagoP = new ComplementoPagoFormaPago { Clave = item.FormaDePagoP };
                newItem.MonedaP = item.MonedaP;
                newItem.Monto = item.Monto;
                newItem.NomBancoOrdExt = item.NomBancoOrdExt;
                newItem.NumOperacion = item.NumOperacion;
                newItem.RfcEmisorCtaBen = item.RfcEmisorCtaBen;
                newItem.RfcEmisorCtaOrd = item.RfcEmisorCtaOrd;

                newItem.TipoCadPago = null;
                if (item.TipoCadPagoSpecified)
                {
                    newItem.TipoCadPago = item.TipoCadPago;
                }

                if (item.TipoCambioPSpecified)
                {
                    newItem.TipoCambioP = item.TipoCambioP;
                }

                newItem.CertPago = null;
                if (item.CertPago != null)
                {
                    newItem.CertPago = Convert.ToBase64String(item.CertPago);
                }

                newItem.SelloPago = null;
                if (item.SelloPago != null)
                {
                    newItem.SelloPago = Convert.ToBase64String(item.SelloPago);
                }

                newItem.DoctoRelacionado = this.Pagos10(item.DoctoRelacionado);
                newItem.Impuestos = this.Pagos10(item.Impuestos);
                newItems.Add(newItem);
            }
            return newItems;
        }

        public BindingList<ComplementoPagoDoctoRelacionado> Pagos10(Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado[] objetos)
        {
            if (objetos != null)
            {
                BindingList<Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoPagoDoctoRelacionado> newItems = new BindingList<ComplementoPagoDoctoRelacionado>();
                foreach (Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado item in objetos)
                {
                    Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoPagoDoctoRelacionado newItem = new ComplementoPagoDoctoRelacionado();
                    newItem.Folio = item.Folio;
                    newItem.IdDocumento = item.IdDocumento;
                    newItem.MetodoPago = item.MetodoDePagoDR;
                    newItem.Moneda = item.MonedaDR;
                    newItem.NumParcialidad = Convert.ToInt32(item.NumParcialidad);
                    newItem.Serie = item.Serie;
                    if (item.ImpPagadoSpecified)
                    {
                        newItem.ImpPagado = item.ImpPagado;
                    }
                    if (item.ImpSaldoAntSpecified)
                    {
                        newItem.ImpSaldoAnt = item.ImpSaldoAnt;
                    }
                    if (item.ImpSaldoInsolutoSpecified)
                    {
                        newItem.ImpSaldoInsoluto = item.ImpSaldoInsoluto;
                    }
                    if (item.TipoCambioDRSpecified)
                    {
                        newItem.TipoCambio = item.TipoCambioDR;
                    }
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        public BindingList<ComplementoPagoImpuestos> Pagos10(Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestos[] objetos)
        {
            if (!(objetos == null))
            {
                BindingList<Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoImpuestos> newItems = new BindingList<ComplementoPagoImpuestos>();
                foreach (Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestos item in objetos)
                {
                    Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoImpuestos newItem = new ComplementoPagoImpuestos();
                    newItem.Retenciones = this.Pagos10(item.Retenciones);
                    newItem.Traslados = this.Pagos10(item.Traslados);
                    if (item.TotalImpuestosRetenidosSpecified)
                    {
                        newItem.TotalImpuestosRetenidos = item.TotalImpuestosRetenidos;
                    }
                    if (item.TotalImpuestosTrasladadosSpecified)
                    {
                        newItem.TotalImpuestosTrasladados = item.TotalImpuestosTrasladados;
                    }
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        public BindingList<ComplementoPagoImpuestosRetencion> Pagos10(Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestosRetencion[] objetos)
        {
            if (objetos != null)
            {
                BindingList<Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoImpuestosRetencion> newItems = new BindingList<ComplementoPagoImpuestosRetencion>();
                foreach (Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestosRetencion item in objetos)
                {
                    Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoImpuestosRetencion newItem = new ComplementoPagoImpuestosRetencion();
                    newItem.Impuesto = item.Impuesto;
                    newItem.Importe = item.Importe;
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        public BindingList<ComplementoPagoImpuestosTraslado> Pagos10(Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestosTraslado[] objetos)
        {
            if (objetos != null)
            {
                BindingList<Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoImpuestosTraslado> newItems = new BindingList<ComplementoPagoImpuestosTraslado>();
                foreach (Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestosTraslado item in objetos)
                {
                    Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoImpuestosTraslado newItem = new ComplementoPagoImpuestosTraslado();
                    newItem.Importe = item.Importe;
                    newItem.Impuesto = item.Impuesto;
                    newItem.TasaOCuota = item.TasaOCuota;
                    newItem.TipoFactor = item.TipoFactor;
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        /// <summary>
        /// crear complemento pagos10
        /// </summary>
        public Jaeger.CFDI.Complemento.Pagos.V10.Pagos Create(ComplementoPagos objeto)
        {
            if (!(objeto == null))
            {
                Jaeger.CFDI.Complemento.Pagos.V10.Pagos newItem = new CFDI.Complemento.Pagos.V10.Pagos();
                newItem.Version = objeto.Version;
                newItem.Pago = this.Pagos10(objeto.Pago);
                return newItem;
            }
            return null;
        }

        public Jaeger.CFDI.Complemento.Pagos.V10.PagosPago[] Pagos10(BindingList<ComplementoPagosPago> objetos)
        {
            if (!(objetos == null))
            {
                List<Jaeger.CFDI.Complemento.Pagos.V10.PagosPago> newItems = new List<CFDI.Complemento.Pagos.V10.PagosPago>();
                foreach (ComplementoPagosPago item in objetos)
                {
                    Jaeger.CFDI.Complemento.Pagos.V10.PagosPago newItem = new CFDI.Complemento.Pagos.V10.PagosPago();
                    newItem.CadPago = item.CadPago;

                    newItem.CtaBeneficiario = item.CtaBeneficiario;
                    newItem.CtaOrdenante = item.CtaOrdenante;
                    newItem.FechaPago = DateTime.Parse(item.FechaPago.ToString());
                    newItem.FormaDePagoP = item.FormaDePagoP.Clave;
                    newItem.Impuestos = this.Pagos10(item.Impuestos);
                    newItem.MonedaP = item.MonedaP;
                    newItem.Monto = item.Monto;
                    newItem.NomBancoOrdExt = item.NomBancoOrdExt;
                    newItem.NumOperacion = item.NumOperacion;

                    newItem.RfcEmisorCtaBen = null;
                    if (item.RfcEmisorCtaBen != null)
                    {
                        if (item.RfcEmisorCtaBen.Trim().Length > 0)
                        {
                            newItem.RfcEmisorCtaBen = item.RfcEmisorCtaBen;
                        }
                    }

                    newItem.RfcEmisorCtaOrd = null;
                    if (item.RfcEmisorCtaOrd != null)
                    {
                        if (item.RfcEmisorCtaOrd.Trim().Length > 0)
                        {
                            newItem.RfcEmisorCtaOrd = item.RfcEmisorCtaOrd;
                        }
                    }

                    newItem.TipoCadPago = item.TipoCadPago;
                    newItem.TipoCadPagoSpecified = !String.IsNullOrEmpty(item.TipoCadPago);
                    newItem.TipoCambioP = item.TipoCambioP;
                    newItem.TipoCambioPSpecified = item.TipoCambioP > 0;
                    newItem.DoctoRelacionado = this.Pagos10(item.DoctoRelacionado);

                    item.SelloPago = null;
                    if (item.SelloPago != null)
                    {
                        newItem.SelloPago = Convert.FromBase64String(item.SelloPago);
                    }

                    newItem.CertPago = null;
                    if (item.CertPago != null)
                    {
                        newItem.CertPago = Convert.FromBase64String(item.CertPago);
                    }

                    newItems.Add(newItem);
                }
                return newItems.ToArray();
            }
            return null;
        }

        public Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado[] Pagos10(BindingList<ComplementoPagoDoctoRelacionado> objetos)
        {
            if (!(objetos == null))
            {
                List<Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado> newItems = new List<CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado>();
                foreach (Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoPagoDoctoRelacionado item in objetos)
                {
                    Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado newItem = new CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado();
                    newItem.Folio = item.Folio;
                    newItem.IdDocumento = item.IdDocumento;
                    newItem.ImpPagado = item.ImpPagado;
                    newItem.ImpPagadoSpecified = item.ImpPagado > 0;
                    newItem.ImpSaldoAnt = item.ImpSaldoAnt;
                    newItem.ImpSaldoAntSpecified = item.ImpSaldoAnt > 0;
                    newItem.MetodoDePagoDR = item.MetodoPago;
                    newItem.MonedaDR = item.Moneda;
                    newItem.NumParcialidad = item.NumParcialidad.ToString();
                    newItem.Serie = item.Serie;
                    newItem.TipoCambioDR = item.TipoCambio;
                    newItem.TipoCambioDRSpecified = item.TipoCambio > 0;

                    newItem.ImpSaldoInsoluto = item.ImpSaldoInsoluto;
                    newItem.ImpSaldoInsolutoSpecified = item.ImpSaldoInsoluto > 0;

                    if (newItem.MetodoDePagoDR == "PPD")
                    {
                        newItem.ImpSaldoInsolutoSpecified = true;
                    }

                    newItems.Add(newItem);
                }
                return newItems.ToArray();
            }
            return null;
        }

        public Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestos[] Pagos10(BindingList<ComplementoPagoImpuestos> objetos)
        {
            if (!(objetos == null))
            {
                List<Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestos> newItems = new List<CFDI.Complemento.Pagos.V10.PagosPagoImpuestos>();
                foreach (Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoImpuestos item in objetos)
                {
                    Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestos newItem = new CFDI.Complemento.Pagos.V10.PagosPagoImpuestos();
                    newItem.Retenciones = this.Pagos10(item.Retenciones);
                    newItem.TotalImpuestosRetenidos = item.TotalImpuestosRetenidos;
                    newItem.Traslados = this.Pagos10(item.Traslados);
                    newItem.TotalImpuestosTrasladados = item.TotalImpuestosTrasladados;
                    if (newItem.Retenciones == null)
                    {
                        newItem.TotalImpuestosRetenidosSpecified = false;
                    }
                    else
                    {
                        newItem.TotalImpuestosRetenidosSpecified = true;
                    }

                    if (newItem.Traslados == null)
                    {
                        newItem.TotalImpuestosTrasladadosSpecified = false;
                    }
                    else
                    {
                        newItem.TotalImpuestosTrasladadosSpecified = true;
                    }
                }
                return newItems.ToArray();
            }
            return null;
        }

        public Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestosRetencion[] Pagos10(BindingList<ComplementoPagoImpuestosRetencion> objetos)
        {
            if (!(objetos == null))
            {
                List<Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestosRetencion> newItems = new List<CFDI.Complemento.Pagos.V10.PagosPagoImpuestosRetencion>();
                foreach (Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoImpuestosRetencion item in objetos)
                {
                    Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestosRetencion newItem = new CFDI.Complemento.Pagos.V10.PagosPagoImpuestosRetencion();
                    newItem.Importe = item.Importe;
                    newItem.Impuesto = item.Impuesto;
                    newItems.Add(newItem);
                }
                return newItems.ToArray();
            }
            return null;
        }

        public Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestosTraslado[] Pagos10(BindingList<ComplementoPagoImpuestosTraslado> objetos)
        {
            if (!(objetos == null))
            {
                List<Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestosTraslado> newItems = new List<CFDI.Complemento.Pagos.V10.PagosPagoImpuestosTraslado>();
                foreach (Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoImpuestosTraslado item in objetos)
                {
                    Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestosTraslado newItem = new CFDI.Complemento.Pagos.V10.PagosPagoImpuestosTraslado();
                    newItem.Importe = item.Importe;
                    newItem.Impuesto = item.Impuesto;
                    newItem.TasaOCuota = item.TasaOCuota;
                    newItem.TipoFactor = item.TipoFactor;
                    newItems.Add(newItem);
                }
                return newItems.ToArray();
            }
            return null;
        }

        #endregion

        #region complemento LeyendasFiscales

        public ComplementoLeyendasFiscales Create(Jaeger.CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscales objeto)
        {
            if (objeto != null)
            {
                ComplementoLeyendasFiscales nuevo = new ComplementoLeyendasFiscales();
                nuevo.Version = objeto.version;
                nuevo.Leyenda = this.LeyendasFiscales(objeto.Leyenda);
            }
            return null;
        }

        private BindingList<ComplementoLeyendasFiscalesLeyenda> LeyendasFiscales(CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscalesLeyenda[] objetos)
        {
            if (objetos != null)
            {
                BindingList<ComplementoLeyendasFiscalesLeyenda> lista = new BindingList<ComplementoLeyendasFiscalesLeyenda>();
                foreach (CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscalesLeyenda item in objetos)
                {
                    ComplementoLeyendasFiscalesLeyenda nuevo = new ComplementoLeyendasFiscalesLeyenda();
                    nuevo.DisposicionFiscal = item.disposicionFiscal;
                    nuevo.Norma = item.norma;
                    nuevo.TextoLeyenda = item.textoLeyenda;
                    lista.Add(nuevo);
                }
                return lista;
            }
            return null;
        }

        public Jaeger.CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscales Create(ComplementoLeyendasFiscales objeto)
        {
            Jaeger.CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscales nuevo = new CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscales();
            nuevo.version = objeto.Version;
            nuevo.Leyenda = this.LeyendasFiscales(objeto.Leyenda);
            return nuevo;
        }

        private CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscalesLeyenda[] LeyendasFiscales(BindingList<ComplementoLeyendasFiscalesLeyenda> objetos)
        {
            if (objetos != null)
            {
                List<CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscalesLeyenda> lista = new List<CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscalesLeyenda>();
                foreach (ComplementoLeyendasFiscalesLeyenda item in objetos)
                {
                    CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscalesLeyenda nuevo = new CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscalesLeyenda();
                    nuevo.disposicionFiscal = item.DisposicionFiscal;
                    nuevo.norma = item.Norma;
                    nuevo.textoLeyenda = item.TextoLeyenda;
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        #endregion

        #region complemento vales de despensa v10

        public ComplementoValesDeDespensa Create(CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensa objeto)
        {
            if (objeto != null)
            {
                ComplementoValesDeDespensa nuevo = new ComplementoValesDeDespensa();
                nuevo.Version = objeto.version;
                nuevo.RegistroPatronal = objeto.registroPatronal;
                nuevo.TipoOperacion = objeto.tipoOperacion;
                nuevo.Total = objeto.total;
                nuevo.Conceptos = this.ValesDeDespensa10(objeto.Conceptos);
            }
            return null;
        }

        private BindingList<ComplementoValesDeDespensaConcepto> ValesDeDespensa10(CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensaConcepto[] objetos)
        {
            if (objetos != null)
            {
                BindingList<ComplementoValesDeDespensaConcepto> lista = new BindingList<ComplementoValesDeDespensaConcepto>();
                foreach (CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensaConcepto item in objetos)
                {
                    ComplementoValesDeDespensaConcepto nuevo = new ComplementoValesDeDespensaConcepto();
                    nuevo.CURP = item.curp;
                    nuevo.Fecha = item.fecha;
                    nuevo.Identificador = item.identificador;
                    nuevo.Importe = item.importe;
                    nuevo.Nombre = item.nombre;
                    nuevo.NumSeguridadSocial = item.numSeguridadSocial;
                    nuevo.RFC = item.rfc;
                    lista.Add(nuevo);
                }
                return lista;
            }
            return null;
        }

        public CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensa Create(ComplementoValesDeDespensa objeto)
        {
            if (objeto != null)
            {
                CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensa nuevo = new CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensa();
                nuevo.version = objeto.Version;
                nuevo.registroPatronal = objeto.RegistroPatronal;
                nuevo.tipoOperacion = objeto.TipoOperacion;
                nuevo.total = objeto.Total;
                nuevo.Conceptos = this.ValesDeDespensa10(objeto.Conceptos);
            }
            return null;
        }

        private CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensaConcepto[] ValesDeDespensa10(BindingList<ComplementoValesDeDespensaConcepto> objetos)
        {
            if (objetos != null)
            {
                List<CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensaConcepto> lista = new List<CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensaConcepto>();
                foreach (ComplementoValesDeDespensaConcepto item in objetos)
                {
                    CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensaConcepto nuevo = new CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensaConcepto();
                    nuevo.rfc = item.RFC;
                    nuevo.curp = item.CURP;
                    nuevo.fecha = item.Fecha;
                    nuevo.identificador = item.Identificador;
                    nuevo.importe = item.Importe;
                    nuevo.nombre = item.Nombre;
                    nuevo.numSeguridadSocial = item.NumSeguridadSocial;
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        #endregion

        #region complemento impuestos locales v10

        public ComplementoImpuestosLocales Create(CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocales objeto)
        {
            if (objeto != null)
            {
                ComplementoImpuestosLocales nuevo = new ComplementoImpuestosLocales();
                nuevo.Version = objeto.version;
                nuevo.TotaldeRetenciones = objeto.TotaldeRetenciones;
                nuevo.TotaldeTraslados = objeto.TotaldeTraslados;
                nuevo.RetencionesLocales = this.ImpuestosLocales10(objeto.RetencionesLocales);
                nuevo.TrasladosLocales = this.ImpuestosLocales10(objeto.TrasladosLocales);
                return nuevo;
            }
            return null;
        }

        private BindingList<ComplementoImpuestosLocalesTrasladosLocales> ImpuestosLocales10(CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesTrasladosLocales[] objetos)
        {
            if (objetos != null)
            {
                BindingList<ComplementoImpuestosLocalesTrasladosLocales> lista = new BindingList<ComplementoImpuestosLocalesTrasladosLocales>();
                foreach (CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesTrasladosLocales item in objetos)
                {
                    ComplementoImpuestosLocalesTrasladosLocales nuevo = new ComplementoImpuestosLocalesTrasladosLocales();
                    nuevo.ImpLocTrasladado = item.ImpLocTrasladado;
                    nuevo.Importe = item.Importe;
                    nuevo.TasadeTraslado = item.TasadeTraslado;
                    lista.Add(nuevo);
                }
                return lista;
            }
            return null;
        }

        private BindingList<ComplementoImpuestosLocalesRetencionesLocales> ImpuestosLocales10(CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesRetencionesLocales[] objetos)
        {
            if (objetos != null)
            {
                BindingList<ComplementoImpuestosLocalesRetencionesLocales> lista = new BindingList<ComplementoImpuestosLocalesRetencionesLocales>();
                foreach (CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesRetencionesLocales item in objetos)
                {
                    ComplementoImpuestosLocalesRetencionesLocales nuevo = new ComplementoImpuestosLocalesRetencionesLocales();
                    nuevo.ImpLocRetenido = item.ImpLocRetenido;
                    nuevo.Importe = item.Importe;
                    nuevo.TasadeRetencion = item.TasadeRetencion;
                    lista.Add(nuevo);
                }
                return lista;
            }
            return null;
        }

        public CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocales Create(ComplementoImpuestosLocales objeto)
        {
            CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocales nuevo = new CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocales();
            nuevo.version = objeto.Version;
            nuevo.TotaldeRetenciones = objeto.TotaldeRetenciones;
            nuevo.TotaldeTraslados = objeto.TotaldeTraslados;
            nuevo.RetencionesLocales = this.ImpuestosLocales10(objeto.RetencionesLocales);
            nuevo.TrasladosLocales = this.ImpuestosLocales10(objeto.TrasladosLocales);
            return nuevo;
        }

        private CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesRetencionesLocales[] ImpuestosLocales10(BindingList<ComplementoImpuestosLocalesRetencionesLocales> objetos)
        {
            if (objetos != null)
            {
                List<CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesRetencionesLocales> lista = new List<CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesRetencionesLocales>();
                foreach (ComplementoImpuestosLocalesRetencionesLocales item in objetos)
                {
                    CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesRetencionesLocales nuevo = new CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesRetencionesLocales();
                    nuevo.ImpLocRetenido = item.ImpLocRetenido;
                    nuevo.Importe = item.Importe;
                    nuevo.TasadeRetencion = item.TasadeRetencion;
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        private CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesTrasladosLocales[] ImpuestosLocales10(BindingList<ComplementoImpuestosLocalesTrasladosLocales> objetos)
        {
            if (objetos != null)
            {
                List<CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesTrasladosLocales> lista = new List<CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesTrasladosLocales>();
                foreach (ComplementoImpuestosLocalesTrasladosLocales item in objetos)
                {
                    CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesTrasladosLocales nuevo = new CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesTrasladosLocales();
                    nuevo.ImpLocTrasladado = item.ImpLocTrasladado;
                    nuevo.Importe = item.Importe;
                    nuevo.TasadeTraslado = item.TasadeTraslado;
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        #endregion

        #region complemento aerolineas v10

        public ComplementoAerolineas Create(CFDI.Complemento.Aerolineas.V10.Aerolineas objeto)
        {
            if (objeto != null)
            {
                ComplementoAerolineas nuevo = new ComplementoAerolineas();
                nuevo.Version = objeto.Version;
                nuevo.TUA = objeto.TUA;
                nuevo.OtrosCargos = this.Aerolineas10(objeto.OtrosCargos);
                return nuevo;
            }
            return null;
        }

        private ComplementoAerolineasOtrosCargos Aerolineas10(CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargos objeto)
        {
            if (objeto != null)
            {
                ComplementoAerolineasOtrosCargos nuevo = new ComplementoAerolineasOtrosCargos();
                nuevo.TotalCargos = objeto.TotalCargos;
                nuevo.Cargo = this.Aerolineas10(objeto.Cargo);
                return nuevo;
            }
            return null;
        }

        private BindingList<ComplementoAerolineasOtrosCargosCargo> Aerolineas10(CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargosCargo[] objetos)
        {
            if (objetos != null)
            {
                BindingList<ComplementoAerolineasOtrosCargosCargo> lista = new BindingList<ComplementoAerolineasOtrosCargosCargo>();
                foreach (CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargosCargo item in objetos)
                {
                    ComplementoAerolineasOtrosCargosCargo nuevo = new ComplementoAerolineasOtrosCargosCargo();
                    nuevo.CodigoCargo = item.CodigoCargo;
                    nuevo.Importe = item.Importe;
                }
                return lista;
            }
            return null;
        }

        public CFDI.Complemento.Aerolineas.V10.Aerolineas Create(ComplementoAerolineas objeto)
        {
            if (objeto != null)
            {
                CFDI.Complemento.Aerolineas.V10.Aerolineas nuevo = new CFDI.Complemento.Aerolineas.V10.Aerolineas();
                nuevo.Version = objeto.Version;
                nuevo.TUA = objeto.TUA;
                nuevo.OtrosCargos = this.Aerolineas10(objeto.OtrosCargos);
                return nuevo;
            }
            return null;
        }

        private CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargos Aerolineas10(ComplementoAerolineasOtrosCargos objeto)
        {
            if (objeto != null)
            {
                CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargos nuevo = new CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargos();
                nuevo.TotalCargos = objeto.TotalCargos;
                nuevo.Cargo = this.Aerolineas10(objeto.Cargo);
            }
            return null;
        }

        private CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargosCargo[] Aerolineas10(BindingList<ComplementoAerolineasOtrosCargosCargo> objetos)
        {
            if (objetos != null)
            {
                List<CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargosCargo> lista = new List<CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargosCargo>();
                foreach (ComplementoAerolineasOtrosCargosCargo item in objetos)
                {
                    CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargosCargo nuevo = new CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargosCargo();
                    nuevo.CodigoCargo = item.CodigoCargo;
                    nuevo.Importe = item.Importe;
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        #endregion

        #region complemento spei

        public ComplementoSpei Spei(Jaeger.CFDI.Complemento_SPEI objeto)
        {
            if (objeto != null)
            {
                ComplementoSpei newItem = new ComplementoSpei();
                newItem.SpeiTercero = Spei(objeto.SPEI_Tercero);
                return newItem;
            }
            return null;
        }

        public BindingList<ComplementoSpeiTercero> Spei(Jaeger.CFDI.Complemento_SPEISPEI_Tercero[] objetos)
        {
            if (objetos != null)
            {
                BindingList<ComplementoSpeiTercero> newItems = new BindingList<ComplementoSpeiTercero>();
                foreach (Jaeger.CFDI.Complemento_SPEISPEI_Tercero item in objetos)
                {
                    ComplementoSpeiTercero newItem = new ComplementoSpeiTercero();
                    newItem.Beneficiario = Spei(item.Beneficiario);
                    newItem.CadenaCda = item.cadenaCDA;
                    newItem.ClaveSpei = item.ClaveSPEI;
                    newItem.FechaOperacion = item.FechaOperacion;
                    newItem.Hora = item.Hora;
                    newItem.NumeroCertificado = item.numeroCertificado;
                    newItem.Ordenante = this.Spei(item.Ordenante);
                    newItem.Sello = item.sello;
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        public ComplementoSpeiTerceroBeneficiario Spei(Jaeger.CFDI.Complemento_SPEISPEI_TerceroBeneficiario objeto)
        {
            if (objeto != null)
            {
                ComplementoSpeiTerceroBeneficiario newItem = new ComplementoSpeiTerceroBeneficiario();
                newItem.BancoReceptor = objeto.BancoReceptor;
                newItem.Concepto = objeto.Concepto;
                newItem.Cuenta = objeto.Cuenta;
                newItem.IVA = objeto.IVA;
                newItem.MontoPago = objeto.MontoPago;
                newItem.Nombre = objeto.Nombre;
                newItem.RFC = objeto.RFC;
                newItem.TipoCuenta = objeto.TipoCuenta;
            }
            return null;
        }

        public ComplementoSpeiTerceroOrdenante Spei(Jaeger.CFDI.Complemento_SPEISPEI_TerceroOrdenante objeto)
        {
            if (objeto != null)
            {
                ComplementoSpeiTerceroOrdenante newItem = new ComplementoSpeiTerceroOrdenante();
                newItem.BancoEmisor = objeto.BancoEmisor;
                newItem.Cuenta = objeto.Cuenta;
                newItem.Nombre = objeto.Nombre;
                newItem.RFC = objeto.RFC;
                newItem.TipoCuenta = objeto.TipoCuenta;
                return newItem;
            }
            return null;
        }

        public static ComplementoPagosPago Create(Jaeger.CFDI.SpeiTercero objeto)
        {
            if (objeto != null)
            {
                ComplementoPagosPago newItem = new ComplementoPagosPago();
                newItem.FechaPago = new DateTime(objeto.FechaOperacion.Year, objeto.FechaOperacion.Month, objeto.FechaOperacion.Day, objeto.Hora.Hour, objeto.Hora.Minute, objeto.Hora.Second);
                newItem.FormaDePagoP = new ComplementoPagoFormaPago { Clave = "03" };
                newItem.MonedaP = "MXN";
                newItem.Monto = objeto.Beneficiario.MontoPago;
                newItem.RfcEmisorCtaOrd = objeto.Ordenante.RFC;
                newItem.NomBancoOrdExt = objeto.Ordenante.BancoEmisor;
                newItem.CtaOrdenante = objeto.Ordenante.Cuenta.ToString();
                newItem.RfcEmisorCtaBen = objeto.Beneficiario.RFC;
                newItem.CtaBeneficiario = objeto.Beneficiario.Cuenta.ToString();
                newItem.CertPago = objeto.numeroCertificado.ToString();
                newItem.CadPago = objeto.cadenaCDA;
                newItem.SelloPago = objeto.sello;
                return newItem;
            }
            return null;
        }

        #endregion

        #region acuse de cancelacion

        /// <summary>
        /// convertir acuse de cancelacion SAT a acuse comun
        /// </summary>
        public ViewModelAccuseCancelacion Accuse(Jaeger.Edita.V2.CFDI.Entities.Cancel.Acuse objeto)
        {
            if (objeto != null)
            {
                ViewModelAccuseCancelacion newItem = new ViewModelAccuseCancelacion();
                newItem.RfcEmisor = objeto.RfcEmisor;
                newItem.FechaCancelacion = objeto.Fecha;
                newItem.FechaSolicitud = objeto.Fecha;
                newItem.SelloDigital = Convert.ToBase64String(objeto.Signature.SignatureValue.Value);
                foreach (Jaeger.Edita.V2.CFDI.Entities.Cancel.AcuseFolios item in objeto.Folios)
                {
                    newItem.FolioFiscal = item.UUID;
                    newItem.Estado = item.EstatusUUID;
                }
                return newItem;
            }
            return null;
        }

        #endregion

        #region funciones

        public static string Domicilio(CFDI.V32.t_UbicacionFiscal oDomicilio)
        {
            string outString = "";
            if (oDomicilio != null)
            {
                if (!string.IsNullOrEmpty(oDomicilio.calle))
                {
                    outString = string.Concat(outString, "Calle : ", oDomicilio.calle);
                }
                if (!string.IsNullOrEmpty(oDomicilio.noExterior))
                {
                    outString = string.Concat(outString, " ", oDomicilio.noExterior);
                }
                if (!string.IsNullOrEmpty(oDomicilio.noInterior))
                {
                    outString = string.Concat(outString, " ", oDomicilio.noInterior);
                }
                if (!string.IsNullOrEmpty(oDomicilio.colonia))
                {
                    outString = string.Concat(outString, Environment.NewLine, "Colonia : ", oDomicilio.colonia);
                }
                if (!string.IsNullOrEmpty(oDomicilio.localidad))
                {
                    outString = string.Concat(outString, Environment.NewLine, "Localidad : ", oDomicilio.localidad);
                }
                if (!string.IsNullOrEmpty(oDomicilio.municipio))
                {
                    outString = string.Concat(outString, Environment.NewLine, "Del/Mun : ", oDomicilio.municipio);
                }
                if (!string.IsNullOrEmpty(oDomicilio.codigoPostal))
                {
                    outString = string.Concat(outString, ", CP : ", oDomicilio.codigoPostal);
                }
                if (!string.IsNullOrEmpty(oDomicilio.pais))
                {
                    outString = string.Concat(outString, Environment.NewLine, "Pais : ", oDomicilio.pais);
                }
                if (!string.IsNullOrEmpty(oDomicilio.estado))
                {
                    outString = string.Concat(outString, Environment.NewLine, "Entidad Federativa : ", oDomicilio.estado);
                }
            }
            return outString;
        }

        /// <summary>
        /// Devuelve una cadena de texto con direccion para version 3.2
        /// </summary>
        public static string Domicilio(CFDI.V32.t_Ubicacion oDomicilio)
        {
            string outString = "";
            if (oDomicilio != null)
            {
                if (!string.IsNullOrEmpty(oDomicilio.calle))
                {
                    outString = string.Concat(outString, "Calle : ", oDomicilio.calle);
                }
                if (!string.IsNullOrEmpty(oDomicilio.noExterior))
                {
                    outString = string.Concat(outString, " ", oDomicilio.noExterior);
                }
                if (!string.IsNullOrEmpty(oDomicilio.noInterior))
                {
                    outString = string.Concat(outString, " ", oDomicilio.noInterior);
                }
                if (!string.IsNullOrEmpty(oDomicilio.colonia))
                {
                    outString = string.Concat(outString, Environment.NewLine, "Colonia : ", oDomicilio.colonia);
                }
                if (!string.IsNullOrEmpty(oDomicilio.localidad))
                {
                    outString = string.Concat(outString, Environment.NewLine, "Localidad : ", oDomicilio.localidad);
                }
                if (!string.IsNullOrEmpty(oDomicilio.municipio))
                {
                    outString = string.Concat(outString, Environment.NewLine, "Del/Mun : ", oDomicilio.municipio);
                }
                if (!string.IsNullOrEmpty(oDomicilio.codigoPostal))
                {
                    outString = string.Concat(outString, ", ", oDomicilio.codigoPostal);
                }
                if (!string.IsNullOrEmpty(oDomicilio.pais))
                {
                    outString = string.Concat(outString, Environment.NewLine, "Pais : ", oDomicilio.pais);
                }
                if (!string.IsNullOrEmpty(oDomicilio.estado))
                {
                    outString = string.Concat(outString, Environment.NewLine, "Entidad Federativa : ", oDomicilio.estado);
                }
            }
            return outString;
        }

        public static string Domicilio(ViewModelDomicilio oDomicilio)
        {
            string outString = "";
            if (oDomicilio != null)
            {
                if (!string.IsNullOrEmpty(oDomicilio.Calle))
                {
                    outString = string.Concat(outString, "Calle : ", oDomicilio.Calle);
                }
                if (!string.IsNullOrEmpty(oDomicilio.NoExterior))
                {
                    outString = string.Concat(outString, " ", oDomicilio.NoExterior);
                }
                if (!string.IsNullOrEmpty(oDomicilio.NoInterior))
                {
                    outString = string.Concat(outString, " ", oDomicilio.NoInterior);
                }
                if (!string.IsNullOrEmpty(oDomicilio.Colonia))
                {
                    outString = string.Concat(outString, Environment.NewLine, "Colonia : ", oDomicilio.Colonia);
                }
                if (!string.IsNullOrEmpty(oDomicilio.Localidad))
                {
                    outString = string.Concat(outString, Environment.NewLine, "Localidad : ", oDomicilio.Localidad);
                }
                if (!string.IsNullOrEmpty(oDomicilio.Municipio))
                {
                    outString = string.Concat(outString, Environment.NewLine, "Del/Mun : ", oDomicilio.Municipio);
                }
                if (!string.IsNullOrEmpty(oDomicilio.CodigoPostal))
                {
                    outString = string.Concat(outString, ", ", oDomicilio.CodigoPostal);
                }
                if (!string.IsNullOrEmpty(oDomicilio.Pais))
                {
                    outString = string.Concat(outString, Environment.NewLine, "Pais : ", oDomicilio.Pais);
                }
                if (!string.IsNullOrEmpty(oDomicilio.Estado))
                {
                    outString = string.Concat(outString, Environment.NewLine, "Entidad Federativa : ", oDomicilio.Estado);
                }
            }
            return outString;
        }

        #endregion

    }
}