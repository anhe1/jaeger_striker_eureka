﻿// develop: 030620171043
// purpose:
using System;
using System.IO;
using Jaeger.Enums;

namespace Jaeger.Helpers
{
    public class HelperTacitLog
    {


        public static string FileName = Path.Combine("C:\\Jaeger\\jaeger_tacit_ronin.log");
        public HelperTacitLog()
            : base()
        {
        }

        public static void LogWrite(Jaeger.Entities.Property oError)
        {
            try
            {
                if ((!File.Exists(HelperTacitLog.FileName)))
                {
                    File.Create(HelperTacitLog.FileName).Close();
                }
                System.IO.StreamWriter streamWriter = File.AppendText(HelperTacitLog.FileName);
                streamWriter.WriteLine(string.Concat(DateTime.Now, ",", Enum.GetName(typeof(EnumPropertyType), oError.Type), ",", oError.Type, oError.Code, ",", oError.Name, ",", oError.Value));
                streamWriter.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public bool LogDelete()
        {
            try
            {
                File.Delete(HelperTacitLog.FileName);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

    }
}