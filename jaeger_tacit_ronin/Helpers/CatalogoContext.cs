﻿/// develop: anhe1 30062019
/// purpose: Context para catalogos SAT
/// rev.:opcion para recuperar desde los recursos incrustados
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reflection;
using Jaeger.Edita.Enums;
using Newtonsoft.Json;

namespace Jaeger.Helpers
{
    /// <summary>
    /// clase contexto para el manejo de catalogos diversos
    /// </summary>
    /// <typeparam name="T">The type of the T.</typeparam>
    public class CatalogoContext<T> where T : class, new()
    {
        /// <summary>
        /// clase de catálogo base para contener información 
        /// </summary>
        /// <typeparam name="T">objeto clase T</typeparam>
        public partial class CatalogoBase<T>
        {
            private DateTime? actualizacionField;
            private DateTime? inicioVigencia;
            private DateTime? finVigencia;

            /// <summary>
            /// obtener o establecer version del catalogo
            /// </summary>
            [JsonProperty("ver", Order = 1)]
            public string Version { get; set; }

            /// <summary>
            /// obtener o establecer titulo del catalogo
            /// </summary>
            [JsonProperty("titulo", Order = 2)]
            public string Titulo { get; set; }

            /// <summary>
            /// obtener o establecer numero de revision del catalogo
            /// </summary>
            [JsonProperty("rev", Order = 3)]
            public string Revision { get; set; }

            /// <summary>
            /// obtener o establecer fecha de inicio de vigencia del catalogo
            /// </summary>
            [JsonProperty("vigi", Order = 5)]
            public DateTime? VigenciaIni
            {
                get
                {
                    DateTime firstGoodDate = new DateTime(1900, 1, 1);
                    if(this.inicioVigencia >= firstGoodDate)
                        return this.inicioVigencia;
                    return null;
                }
                set
                {
                    this.inicioVigencia = value;
                }
            }

            /// <summary>
            /// obtener o establecer fecha de fin de vigencia
            /// </summary>
            [JsonProperty("vigf", Order = 6)]
            public DateTime? VigenciaFin
            {
                get
                {
                    DateTime firstGoodDate = new DateTime(1900, 1, 1);
                    if (this.finVigencia >= firstGoodDate)
                        return this.finVigencia;
                    return null;
                }
                set
                {
                    this.finVigencia = value;
                }
            }

            /// <summary>
            /// obtener o establecer fecha de actualización del catalogo
            /// </summary>
            [JsonProperty("act", Order = 4)]
            public DateTime? Actualizacion
            {
                get
                {
                    DateTime firstGoodDate = new DateTime(1900, 1, 1);
                    if (this.actualizacionField >= firstGoodDate)
                        return this.actualizacionField;
                    return null;
                }
                set
                {
                    this.actualizacionField = value;
                }
            }

            [JsonProperty("items", Order = 7)]
            public List<T> Items { get; set; }

            /// <summary>
            /// constructor
            /// </summary>
            public CatalogoBase()
            {
                this.Version = "1.0";
                this.Titulo = "Catálogo";
                this.Revision = "0";
                this.Actualizacion = DateTime.Now;
                this.Items = new List<T>();
            }

            public string ToJson(Formatting formatting = 0)
            {
                // configuracion json para la serializacion
                var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy" };
                return JsonConvert.SerializeObject(this, formatting, conf);
            }
        }

        public enum EnumCatalogFormatting
        {
            Json
        }

        #region declaraciones

        private bool recuperarField = true;
        private CatalogoBase<T> catalogo;

        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public CatalogoContext()
        {
            this.FileName = "miCatalogo.json";
            this.StartPath = EnumPaths.Catalogos;
            this.SaveFormat = EnumCatalogFormatting.Json;
            this.catalogo = new CatalogoBase<T>();
        }

        #region propiedades

        /// <summary>
        /// obtener o establecer el nombre del archivo
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// obtener o establecer el formato del catalogo
        /// </summary>
        public EnumCatalogFormatting SaveFormat { get; set; }

        /// <summary>
        /// obtener o establecer la version del catalogo
        /// </summary>
        public string Version
        {
            get
            {
                return this.catalogo.Version;
            }
            set
            {
                this.catalogo.Version = value;
            }
        }

        /// <summary>
        /// obtener o establecer titulo del catalogo
        /// </summary>
        public string Title
        {
            get
            {
                return this.catalogo.Titulo;
            }
            set
            {
                this.catalogo.Titulo = value;
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de revision
        /// </summary>
        public string Revision
        {
            get
            {
                return this.catalogo.Revision;
            }
            set
            {
                this.catalogo.Revision = value;
            }
        }

        /// <summary>
        /// obtener o establecer ruta de inicial donde se encuentra el catalogo
        /// </summary>
        public EnumPaths StartPath { get; set; }

        /// <summary>
        /// obtener o establecer si el catalogo debe ser recuperado desde los recursos de la libreria
        /// </summary>
        public bool Recuperar
        {
            get
            {
                return this.recuperarField;
            }
            set
            {
                this.recuperarField = value;
            }
        }

        /// <summary>
        /// obtener o establecer la lista de objetos
        /// </summary>
        public List<T> Items
        {
            get 
            {
                return this.catalogo.Items;
            }
            set
            {
                this.catalogo.Items = value;
            }
        }

        #endregion

        #region metodos publicos

        public void Add(T newItem)
        {
            try
            {
                if (Items == null)
                {
                    this.Items = new List<T>();
                }
                T e = this.Items.FirstOrDefault<T>((t) => t == newItem);
                if (e == null)
                    this.Items.Add(newItem);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// eliminar un objeto de la coleccion por la referencia de un objeto
        /// </summary>
        public bool Delete(T deleteItem)
        {
            try
            {
                this.Items.Remove(deleteItem);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// eliminar un objeto de la coleccion por referencia del indice
        /// </summary>
        public bool Delete(int index)
        {
            try
            {
                this.Items.RemoveAt(index);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// cargar la informacion de un catalogo
        /// </summary>
        public void Load()
        {
            string localName = this.ResolverName(this.FileName, this.FileName, true);
            if (File.Exists(localName))
            {
                StreamReader oStreamReader = new StreamReader(localName);
                string valor = oStreamReader.ReadToEnd();
                oStreamReader.Close();
                if (valor.Length > 0)
                {
                    var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy" };
                    this.catalogo = JsonConvert.DeserializeObject<CatalogoBase<T>>(valor, conf);
                    this.Items = this.catalogo.Items;
                }
                if (this.Items == null)
                    this.Items = new List<T>();
            }
            else
            {
                this.Items = new List<T>();
            }
        }
        
        /// <summary>
        /// guardar los cambios del catalogo
        /// </summary>
        public bool Save()
        {
            if (this.SaveFormat == EnumCatalogFormatting.Json)
            {
                Jaeger.Util.Helpers.HelperFiles.WriteFileText(this.ResolverName(this.FileName), this.catalogo.ToJson());
            }
            return false;
        }

        /// <summary>
        /// restaurar el catalogo desde el proyecto
        /// </summary>
        public bool Restore()
        {
            return false;
        }

        #endregion

        #region metodos privados

        public bool GetResource(string nameResource, string fileName)
        {
            // sino existe la carpeta la creamos
            if (!(Directory.Exists(Path.GetDirectoryName(fileName))))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(fileName));
            }

            using (Stream oStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Concat("Jaeger.", "Catalogos.", nameResource)))
            {
                var oFileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                oStream.CopyTo(oFileStream);
                oFileStream.Close();
            }

            return File.Exists(fileName);
        }

        private string ResolverName(string fileName)
        {
            return JaegerManagerPaths.JaegerPath(this.StartPath, fileName);
        }

        private string ResolverName(string fileName, string fileDefault, bool resource = true)
        {
            string localName = fileName;
            if (File.Exists(fileName) == false)
            {
                if (resource)
                {
                    if (File.Exists(this.ResolverName(fileDefault)) == false)
                    {
                        if (this.Recuperar == true)
                        {
                            if (this.GetResource(fileDefault, this.ResolverName(fileDefault)))
                            {
                                localName = this.ResolverName(fileDefault);
                            }
                        }
                        else
                        {
                            localName = this.ResolverName(fileDefault);
                            this.Save();
                        }
                    }
                    else
                    {
                        localName = this.ResolverName(fileDefault);
                    }
                }
            }
            return localName;
        }

        #endregion

    }
}
