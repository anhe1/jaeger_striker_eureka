﻿using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos;
using Jaeger.Catalogos.Repositories;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Edita.V2.Directorio.Enums;
using Jaeger.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Jaeger.CFDI.Entities;

namespace Jaeger.Helpers {
    public class HelperConvertidor40 {
        private readonly FormaPagoCatalogo formaPago33;
        private readonly UsoCFDICatalogo usoCFDI;
        private readonly RelacionCFDICatalogo relacionCFDI;

        public HelperConvertidor40() {
            this.formaPago33 = new FormaPagoCatalogo();
            this.formaPago33.Load();
            this.usoCFDI = new UsoCFDICatalogo();
            this.usoCFDI.Load();
            this.relacionCFDI = new RelacionCFDICatalogo();
            this.relacionCFDI.Load();
        }

        #region comprobante fiscal v40

        /// <summary>
        /// convertir V33.ComprobanteEmisor a Contribuyente
        /// </summary>
        public Contribuyente Emisor(CFDI.V40.ComprobanteEmisor objeto, EnumRelationType relacion) {
            Contribuyente objEmisor = new Contribuyente();
            objEmisor.Relacion = Enum.GetName(typeof(EnumRelationType), relacion);
            objEmisor.RFC = objeto.Rfc;
            objEmisor.Nombre = objeto.Nombre;
            return objEmisor;
        }

        /// <summary>
        /// convertir Cfd.Contribuyente a V33.ComprobanteEmisor
        /// </summary>
        public CFDI.V40.ComprobanteEmisor Emisor(ViewModelContribuyente objeto) {
            var item = new CFDI.V40.ComprobanteEmisor();
            item.Nombre = objeto.Nombre;
            item.Rfc = objeto.RFC;
            item.RegimenFiscal = objeto.RegimenFiscal;
            return item;
        }

        /// <summary>
        /// convertir Cfd.Contribyente a V33.ComprobanteReceptor
        /// </summary>
        public CFDI.V40.ComprobanteReceptor Receptor(ViewModelContribuyente objeto, string usoCfdi) {
            var nuevoReceptor = new CFDI.V40.ComprobanteReceptor();
            nuevoReceptor.Nombre = objeto.Nombre;
            nuevoReceptor.Rfc = objeto.RFC;
            nuevoReceptor.NumRegIdTrib = objeto.NumRegIdTrib;
            nuevoReceptor.UsoCFDI = usoCfdi;
            nuevoReceptor.RegimenFiscalReceptor = objeto.RegimenFiscal;
            nuevoReceptor.ResidenciaFiscalSpecified = false;
            if (!(objeto.RegimenFiscal == null)) {
                if (objeto.RegimenFiscal.Trim() != "") {
                    nuevoReceptor.ResidenciaFiscalSpecified = true;
                    nuevoReceptor.ResidenciaFiscal = objeto.ResidenciaFiscal;
                }
            }
            return nuevoReceptor;
        }

        /// <summary>
        /// convertir V33.ComprobanteReceptor a Contribuyente
        /// </summary>
        public Contribuyente Receptor(CFDI.V40.ComprobanteReceptor objeto, EnumRelationType relacion) {
            Contribuyente nuevoReceptor = new Contribuyente();
            nuevoReceptor.Relacion = Enum.GetName(typeof(EnumRelationType), relacion);
            nuevoReceptor.RFC = objeto.Rfc;
            nuevoReceptor.Nombre = objeto.Nombre;
            nuevoReceptor.RegimenFiscal = objeto.RegimenFiscalReceptor;
            return nuevoReceptor;
        }

        /// <summary>
        /// convertir objeto V33.Comprobante a Cfd.Comprobante falta probar
        /// </summary>
        public Comprobante Create(CFDI.V40.Comprobante objeto) {
            Comprobante item = new Comprobante();

            // datos generales
            item.TipoComprobanteText = objeto.TipoDeComprobante;

            item.Version = objeto.Version;
            item.Folio = objeto.Folio;
            item.Serie = objeto.Serie;
            item.SubTipo = objeto.Type;
            item.Receptor = this.Receptor(objeto.Receptor);
            item.Emisor = this.Emisor(objeto.Emisor);
            item.Conceptos = this.Conceptos(objeto.Conceptos);
            item.SubTotal = objeto.SubTotal;
            item.Total = objeto.Total;
            item.NoCertificado = objeto.NoCertificado;
            item.FechaEmision = objeto.Fecha;
            
            item.Moneda.Clave = objeto.Moneda;
            item.LugarExpedicion = objeto.LugarExpedicion;

            ClaveUsoCFDI uso = this.usoCFDI.Search(objeto.Receptor.UsoCFDI);
            if (uso != null) {
                item.UsoCfdi = uso;
            } else {
                item.UsoCfdi.Clave = objeto.Receptor.UsoCFDI;
            }

            // condiciones de pago
            item.CondicionPago = "";
            if (!(objeto.CondicionesDePago == null)) {
                item.CondicionPago = objeto.CondicionesDePago;
            }

            // descuento
            item.Descuento = 0;
            if (objeto.DescuentoSpecified) {
                item.Descuento = objeto.Descuento;
            }

            // tipo de cambio
            if (objeto.TipoCambioSpecified) {
                item.TipoCambio = objeto.TipoCambio.ToString();
            }

            // forma de pago
            item.FormaPago.Clave = "";
            if (!(objeto.FormaPago == null)) {
                if (objeto.FormaPagoSpecified) {
                    ClaveFormaPago clave = this.formaPago33.Search(Regex.Replace(objeto.FormaPago, "[^\\d]", ""));
                    if (clave == null) {
                        item.FormaPago.Clave = objeto.FormaPago;
                    } else {
                        item.FormaPago = clave;
                    }
                }
            }

            // metodo de pago
            item.MetodoPago.Clave = "";
            if (!(objeto.MetodoPago == null)) {
                if (objeto.MetodoPagoSpecified) {
                    item.MetodoPago.Clave = objeto.MetodoPago;
                    if (objeto.MetodoPago.ToUpper().Trim() == "PUE") {
                        item.MetodoPago.Descripcion = "Pago en una sola exhibición";
                    } else if (objeto.MetodoPago.ToUpper().Trim() == "PPD") {
                        item.MetodoPago.Descripcion = "Pago en parcialidades o diferido";
                    }
                }
            }

            // comprobantes relacionados
            if (!(objeto.CfdiRelacionados == null)) {
                ComprobanteCfdiRelacionados comDoctos = this.ComprobanteCfdiRelacionados(objeto.CfdiRelacionados);
                if (!(comDoctos == null)) {
                    ClaveTipoRelacionCFDI clave = this.relacionCFDI.Search(comDoctos.TipoRelacion.Clave);
                    if (clave != null) {
                        comDoctos.TipoRelacion.Descripcion = clave.Descripcion;
                    }

                    item.CfdiRelacionados = comDoctos;
                }
            }


            // impuestos
            if (!(objeto.Impuestos == null)) {
                if (objeto.Impuestos.TotalImpuestosRetenidosSpecified) {
                    if (!(objeto.Impuestos.Retenciones == null)) {
                        foreach (CFDI.V40.ComprobanteImpuestosRetencion imp in objeto.Impuestos.Retenciones) {
                            if (imp.Impuesto == "001") // retencion ISR
                            {
                                item.RetencionIsr = item.RetencionIsr + imp.Importe;
                            } else if (imp.Impuesto == "002") // retencion IVA
                              {
                                item.RetencionIva = item.RetencionIva + imp.Importe;
                            } else if (imp.Impuesto == "003") // retencion IEPS
                              {
                                item.RetencionIeps = item.RetencionIeps + imp.Importe;
                            }
                        }
                    }
                }

                if (objeto.Impuestos.TotalImpuestosTrasladadosSpecified) {
                    if (!(objeto.Impuestos.Traslados == null)) {
                        foreach (CFDI.V40.ComprobanteImpuestosTraslado imp in objeto.Impuestos.Traslados) {
                            if (imp.Impuesto == "002") // traslado IVA
                            {
                                item.TrasladoIva = item.TrasladoIva + imp.Importe;
                            } else if (imp.Impuesto == "003") // traslado IEPS
                              {
                                item.TrasladoIeps = item.TrasladoIeps + imp.Importe;
                            }
                        }
                    }
                }
            }

            // validacion
            if (!(objeto.Validation == null)) {
                item.Validacion = objeto.Validation;
                //item.FechaVal = objeto.Validation.FechaValidacion;
                item.Result = item.Validacion.IsValidText;
                item.Estado = item.Validacion.ProofStatus;
                item.FechaEstado = item.FechaVal;
            }

            // complementos
            if (!(objeto.Complemento == null)) {
                // complemento timbre fiscal
                if (!(objeto.Complemento.TimbreFiscalDigital == null)) {
                    item.TimbreFiscal = this.TimbreFiscal(objeto.Complemento.TimbreFiscalDigital);
                } else {
                    item.TimbreFiscal = null;
                }

                // complemento nomina 1.2
                //if (!(objeto.Complemento.Nomina12 == null)) {
                //    item.Nomina = this.Create(objeto.Complemento.Nomina12);
                //    if (item.Nomina != null) {
                //        item.Nomina.Emisor.RFC = objeto.Emisor.Rfc;
                //        item.Nomina.Receptor.RFC = objeto.Receptor.Rfc;
                //        item.Nomina.Descuento = item.Descuento;
                //        item.Receptor.ClaveUsoCFDI = objeto.Receptor.UsoCFDI;

                //        if (item.TimbreFiscal != null) {
                //            item.Nomina.IdDocumento = item.TimbreFiscal.UUID;
                //        }
                //    }
                //}

                // complemento de pagos
                if (!(objeto.Complemento.Pagos == null)) {
                    ComplementoPagos c2 = this.Create(objeto.Complemento.Pagos);
                    if (!(c2 == null)) {
                        item.Complementos = new Complementos();
                        item.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.Pagos10, Data = c2.Json() });
                        item.ComplementoPagos = c2;
                    }
                }

                // complemento vales de despensa
                //if (!(objeto.Complemento.ValesDeDespensa == null)) {
                //    ComplementoValesDeDespensa c3 = this.Create(objeto.Complemento.ValesDeDespensa);
                //    if (c3 != null) {
                //        if (item.Complementos == null) {
                //            item.Complementos = new Complementos();
                //        }
                //        item.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.ValesDeDespensa, Data = c3.Json() });
                //    }

                //}

                // complemento leyendas fiscales
                //if (!(objeto.Complemento.LeyendasFiscales == null)) {
                //    ComplementoLeyendasFiscales c4 = this.Create(objeto.Complemento.LeyendasFiscales);
                //    if (c4 != null) {
                //        if (item.Complementos == null) {
                //            item.Complementos = new Complementos();
                //        }
                //        item.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.LeyendasFiscales, Data = c4.Json() });
                //    }
                //}

                // complemento Impuestos Locales
                //if (!(objeto.Complemento.ImpuestosLocales == null)) {
                //    ComplementoImpuestosLocales c5 = this.Create(objeto.Complemento.ImpuestosLocales);
                //    if (c5 != null) {
                //        if (item.Complementos == null) {
                //            item.Complementos = new Complementos();
                //        }
                //        item.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.ImpuestosLocales, Data = c5.Json() });
                //    }
                //}

                // complemento aerolineas
                //if (!(objeto.Complemento.Aerolineas == null)) {
                //    ComplementoAerolineas c6 = this.Create(objeto.Complemento.Aerolineas);
                //    if (c6 != null) {
                //        if (item.Complementos == null) {
                //            item.Complementos = new Complementos();
                //        }
                //        item.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.ImpuestosLocales, Data = c6.Json() });
                //    }
                //}
            }

            return item;
        }
      
        /// <summary>
        /// convertir V33.ComprobanteEmisor a Contribuyente
        /// </summary>
        public ViewModelContribuyente Emisor(CFDI.V40.ComprobanteEmisor objeto) {
            var newItem = new ViewModelContribuyente();
            newItem.RFC = objeto.Rfc;
            newItem.Nombre = objeto.Nombre;
            return newItem;
        }

        /// <summary>
        /// convertir V33.ComprobanteReceptor a Contribuyente
        /// </summary>
        public ViewModelContribuyente Receptor(CFDI.V40.ComprobanteReceptor objeto) {
            var newItem = new ViewModelContribuyente();
            newItem.RFC = objeto.Rfc;
            newItem.Nombre = objeto.Nombre;
            return newItem;
        }

        /// <summary>
        /// convertir array V33.ComprobanteConcepto a lista Cfd.Concepto
        /// </summary>
        public BindingList<ViewModelComprobanteConcepto> Conceptos(CFDI.V40.ComprobanteConcepto[] objetos) {
            BindingList<ViewModelComprobanteConcepto> newItems = new BindingList<ViewModelComprobanteConcepto>();
            foreach (CFDI.V40.ComprobanteConcepto objeto in objetos) {
                ViewModelComprobanteConcepto newItem = new ViewModelComprobanteConcepto();
                newItem.Cantidad = objeto.Cantidad;
                newItem.Descripcion = objeto.Descripcion;
                newItem.Importe = objeto.Importe;
                newItem.ValorUnitario = objeto.ValorUnitario;
                newItem.NoIdentificacion = objeto.NoIdentificacion;
                newItem.Unidad = objeto.Unidad;
                newItem.ClaveProdServ = objeto.ClaveProdServ;
                newItem.ClaveUnidad = objeto.ClaveUnidad;

                // impuestos
                if (!(objeto.Impuestos == null)) {
                    //impuestos trasladados
                    if (!(objeto.Impuestos.Traslados == null)) {
                        foreach (CFDI.V40.ComprobanteConceptoImpuestosTraslado t in objeto.Impuestos.Traslados) {
                            ComprobanteConceptoImpuesto itemT = new ComprobanteConceptoImpuesto();
                            itemT.Tipo = EnumTipoImpuesto.Traslado;

                            if (t.Impuesto == "002") // traslado IVA-002
                            {
                                itemT.Impuesto = EnumImpuesto.IVA;
                            } else if (t.Impuesto == "003") //traslado IEPS
                              {
                                itemT.Impuesto = EnumImpuesto.IEPS;
                            }

                            if (t.TipoFactor.ToLower().Contains("tasa")) {
                                itemT.TipoFactor = EnumFactor.Tasa;
                            } else if (t.TipoFactor.ToLower().Contains("cuota")) {
                                itemT.TipoFactor = EnumFactor.Cuota;
                            }

                            if (t.TasaOCuotaSpecified) {
                                itemT.TasaOCuota = t.TasaOCuota;
                            }

                            if (t.ImporteSpecified) {
                                itemT.Importe = t.Importe;
                            }

                            itemT.Base = t.Base;
                            newItem.Impuestos.Add(itemT);
                        }
                    }

                    // impuestos retenidos
                    if (!(objeto.Impuestos.Retenciones == null)) {
                        foreach (CFDI.V40.ComprobanteConceptoImpuestosRetencion r in objeto.Impuestos.Retenciones) {
                            ComprobanteConceptoImpuesto itemR = new ComprobanteConceptoImpuesto();
                            itemR.Base = r.Base;
                            itemR.Importe = r.Importe;
                            itemR.Tipo = EnumTipoImpuesto.Retencion;
                            if (r.Impuesto == "002") {
                                itemR.Impuesto = EnumImpuesto.IVA;
                            } else if (r.Impuesto == "003") {
                                itemR.Impuesto = EnumImpuesto.IEPS;
                            } else if (r.Impuesto == "001") {
                                itemR.Impuesto = EnumImpuesto.ISR;
                            }

                            if (r.TipoFactor.ToLower().Contains("tasa")) {
                                itemR.TipoFactor = EnumFactor.Tasa;
                            } else if (r.TipoFactor.ToLower().Contains("cuota")) {
                                itemR.TipoFactor = EnumFactor.Cuota;
                            } else if (r.TipoFactor.ToLower().Contains("exento")) {
                                itemR.TipoFactor = EnumFactor.Exento;
                            }
                            newItem.Impuestos.Add(itemR);
                        }
                    }
                }

                if (!(objeto.CuentaPredial == null)) {
                    //if (!(objeto.CuentaPredial.Numero == null)) {
                    //    newItem.CtaPredial = objeto.CuentaPredial.Numero;
                    //}
                }

                if (objeto.DescuentoSpecified) {
                    newItem.Descuento = objeto.Descuento;
                } else {
                    newItem.Descuento = 0;
                }

                // objeto concepto parte
                if (!(objeto.Parte == null)) {
                    foreach (CFDI.V40.ComprobanteConceptoParte subItem in objeto.Parte) {
                        newItem.Parte.Add(ConceptoParte(subItem));
                    }
                }

                // objeto informacion aduanera
                if (!(objeto.InformacionAduanera == null)) {
                    newItem.InformacionAduanera = this.ConceptoInformacionAduanera(objeto.InformacionAduanera);
                }

                // agregamos el nuevo objeto a la lista
                newItems.Add(newItem);
            }
            return newItems;
        }

        /// <summary>
        /// convertir V33.ComprobanteConceptoParte a Cfd.ConceptoParte
        /// </summary>
        public ConceptoParte ConceptoParte(CFDI.V40.ComprobanteConceptoParte objeto) {
            ConceptoParte newItem = new ConceptoParte();
            newItem.Cantidad = objeto.Cantidad;
            newItem.ClaveProdServ = objeto.ClaveProdServ;
            newItem.Descripcion = objeto.Descripcion;
            newItem.NoIdentificacion = objeto.NoIdentificacion;
            newItem.Unidad = objeto.Unidad;

            if (objeto.ValorUnitarioSpecified) {
                newItem.ValorUnitario = objeto.ValorUnitario;
            }

            if (objeto.ImporteSpecified) {
                newItem.Importe = objeto.Importe;
            }

            if (!(objeto.InformacionAduanera == null)) {
                newItem.InformacionAduanera = this.ConceptoParteInformacionAduanera(objeto.InformacionAduanera);
            }
            return newItem;
        }

        /// <summary>
        /// convertir un array ComprobanteConceptoInformacionAduanera a lista de ComprobanteInformacionAduanera comun
        /// </summary>
        public BindingList<ComprobanteInformacionAduanera> ConceptoInformacionAduanera(CFDI.V40.ComprobanteConceptoInformacionAduanera[] objetos) {
            if (!(objetos == null)) {
                BindingList<ComprobanteInformacionAduanera> newItems = new BindingList<ComprobanteInformacionAduanera>();
                foreach (CFDI.V40.ComprobanteConceptoInformacionAduanera item in objetos) {
                    ComprobanteInformacionAduanera newItem = new ComprobanteInformacionAduanera();
                    newItem.NumeroPedimento = item.NumeroPedimento;
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        /// <summary>
        /// convertir un array ComprobanteConceptoParteInformacionAduanera a lista de ComprobanteInformacionAduanera comun
        /// </summary>
        public BindingList<ComprobanteInformacionAduanera> ConceptoParteInformacionAduanera(CFDI.V40.ComprobanteConceptoParteInformacionAduanera[] objetos) {
            if (!(objetos == null)) {
                BindingList<ComprobanteInformacionAduanera> newItems = new BindingList<ComprobanteInformacionAduanera>();
                foreach (CFDI.V40.ComprobanteConceptoParteInformacionAduanera item in objetos) {
                    ComprobanteInformacionAduanera newItem = new ComprobanteInformacionAduanera();
                    newItem.NumeroPedimento = item.NumeroPedimento;
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        public ComprobanteCfdiRelacionados ComprobanteCfdiRelacionados(CFDI.V40.ComprobanteCfdiRelacionados[] objeto) {
            if (!(objeto == null)) {
                foreach (var item in objeto) {
                    var newItem = new ComprobanteCfdiRelacionados();
                    newItem.TipoRelacion.Clave = item.TipoRelacion;
                    
                    foreach (CFDI.V40.ComprobanteCfdiRelacionadosCfdiRelacionado item1 in item.CfdiRelacionado) {
                        var newItem2 = new ComprobanteCfdiRelacionadosCfdiRelacionado();
                        newItem2.IdDocumento = item1.UUID;
                        newItem.CfdiRelacionado.Add(newItem2);
                    }
                    return newItem;
                }
            }
            return null;
        }

        /// <summary>
        /// convertir CFDI v33 a Comprobante Basico
        /// </summary>
        public static Entities.Basico.Comprobante ComprobanteToBasic(CFDI.V40.Comprobante oComprobante) {
            Entities.Basico.Comprobante cfdiv32Info = new Entities.Basico.Comprobante();
            RegimenesFiscalesCatalogo catalogoRegimen = new RegimenesFiscalesCatalogo();
            UsoCFDICatalogo catUsoCfdi = new UsoCFDICatalogo();
            FormaPagoCatalogo catFormaPago = new FormaPagoCatalogo();
            MonedaCatalogo catMoneda = new MonedaCatalogo();
            MetodoPagoCatalogo catMetodo33 = new MetodoPagoCatalogo();
            ClaveRegimenFiscal item = new ClaveRegimenFiscal();
            ClaveFormaPago itemFormaPago = new ClaveFormaPago();
            ClaveMoneda itemMoneda = new ClaveMoneda();
            ClaveUsoCFDI itemUsoCfdi = new ClaveUsoCFDI();

            catalogoRegimen.Load();
            catUsoCfdi.Load();
            catFormaPago.Load();
            catMoneda.Load();
            catMetodo33.Load();

            try {
                itemFormaPago = catFormaPago.Search(oComprobante.FormaPago);
            } finally {
                if (itemFormaPago == null) {
                    itemFormaPago = new ClaveFormaPago { Clave = "", Descripcion = "" };
                }
            }

            try {
                itemMoneda = catMoneda.Search(oComprobante.Moneda.ToString());
            } catch (Exception) {

            }

            try {
                ClaveMetodoPago itemMetodoPago = catMetodo33.Search(oComprobante.MetodoPago);
                cfdiv32Info.MetodoDePago = String.Concat(itemMetodoPago.Clave, " - ", itemMetodoPago.Descripcion);
            } catch (Exception) {
                cfdiv32Info.MetodoDePago = oComprobante.MetodoPago;
            }

            // regimen fiscal
            item = catalogoRegimen.Search(oComprobante.Emisor.RegimenFiscal.ToString());
            if (item == null) {
                item = new ClaveRegimenFiscal { Clave = "", Descripcion = "" };
            }

            // uso de cfdi
            itemUsoCfdi = catUsoCfdi.Search(oComprobante.Receptor.UsoCFDI.ToString());
            if (itemUsoCfdi == null) {
                itemUsoCfdi.Clave = "";
                itemUsoCfdi.Descripcion = "";
            }

            if (oComprobante.TipoDeComprobante.ToUpper().Trim() == "I") {
                cfdiv32Info.TipoDeComprobante = "I - Ingreso";
            } else if (oComprobante.TipoDeComprobante.ToUpper().Trim() == "E") {
                cfdiv32Info.TipoDeComprobante = "E - Egreso";
            } else if (oComprobante.TipoDeComprobante.ToUpper().Trim() == "T") {
                cfdiv32Info.TipoDeComprobante = "T - Traslado";
            } else if (oComprobante.TipoDeComprobante.ToUpper().Trim() == "N") {
                cfdiv32Info.TipoDeComprobante = "N - Nomina";
            } else if (oComprobante.TipoDeComprobante.ToUpper().Trim() == "P") {
                cfdiv32Info.TipoDeComprobante = "P - Pago";
            }

            cfdiv32Info.Version = oComprobante.Version;
            cfdiv32Info.LugarDeExpedicion = oComprobante.LugarExpedicion;
            cfdiv32Info.FormaDePago = String.Concat(itemFormaPago.Clave, " - ", itemFormaPago.Descripcion);
            cfdiv32Info.CondicionesDePago = oComprobante.CondicionesDePago;
            cfdiv32Info.Fecha = oComprobante.Fecha.ToString("yyyy-MM-ddTHH:mm:ss");
            cfdiv32Info.Serie = oComprobante.Serie;
            cfdiv32Info.Folio = oComprobante.Folio;
            cfdiv32Info.SelloCfdi = oComprobante.Sello;
            cfdiv32Info.SubTotal = oComprobante.SubTotal;
            cfdiv32Info.Total = oComprobante.Total;
            cfdiv32Info.Descuento = oComprobante.Descuento;
            cfdiv32Info.Moneda = String.Concat(itemMoneda.Clave, " ", itemMoneda.Descripcion);
            cfdiv32Info.TipoDeCambio = oComprobante.TipoCambio.ToString();
            cfdiv32Info.EmisorNoCertificado = oComprobante.NoCertificado;
            cfdiv32Info.Emisor = oComprobante.Emisor.Nombre.ToUpper();
            cfdiv32Info.EmisorRfc = oComprobante.Emisor.Rfc;
            cfdiv32Info.EmisorRegimenFiscal = String.Concat(item.Clave, " ", item.Descripcion);
            cfdiv32Info.ReceptorRfc = oComprobante.Receptor.Rfc;

            if (!(oComprobante.CfdiRelacionados == null)) {
                //cfdiv32Info.TipoRelacion = oComprobante.CfdiRelacionados.TipoRelacion;
                //cfdiv32Info.CfdiRelacionado = String.Join(",", oComprobante.CfdiRelacionados.CfdiRelacionado.Select(Function(x) x.UUID).ToArray());
            }

            if (!(oComprobante.Receptor.Nombre == null)) {
                cfdiv32Info.Receptor = oComprobante.Receptor.Nombre.ToUpper();
            }

            if (!(oComprobante.Impuestos == null)) {
                cfdiv32Info.TotalImpuestosRetenidos = oComprobante.Impuestos.TotalImpuestosRetenidos;
                cfdiv32Info.TotalImpuestosTrasladados = oComprobante.Impuestos.TotalImpuestosTrasladados;

                // tomamos los montos de los traslados
                if (!(oComprobante.Impuestos.Traslados == null)) {
                    foreach (CFDI.V40.ComprobanteImpuestosTraslado itemTraslados in oComprobante.Impuestos.Traslados) {
                        if (itemTraslados.Impuesto == "002") {
                            cfdiv32Info.IvaTraslado = cfdiv32Info.IvaTraslado + itemTraslados.Importe;
                        } else if (itemTraslados.Impuesto == "003") {
                            cfdiv32Info.IepsTraslado = cfdiv32Info.IepsTraslado + itemTraslados.Importe;
                        }
                    }
                }

                // tomamos los montos de las retenciones
                if (!(oComprobante.Impuestos.Retenciones == null)) {
                    foreach (CFDI.V40.ComprobanteImpuestosRetencion itemRetencion in oComprobante.Impuestos.Retenciones) {
                        if (itemRetencion.Impuesto == "002") {
                            cfdiv32Info.IvaRetencion = cfdiv32Info.IvaRetencion + itemRetencion.Importe;
                        } else if (itemRetencion.Impuesto == "001") {
                            cfdiv32Info.IsrRetencion = cfdiv32Info.IsrRetencion + itemRetencion.Importe;
                        } else if (itemRetencion.Impuesto == "003") {
                            cfdiv32Info.IepsRetencion = cfdiv32Info.IepsRetencion + itemRetencion.Importe;
                        }
                    }
                }
            }

            cfdiv32Info.UsoDeCfdi = String.Concat(itemUsoCfdi.Clave, " - ", itemUsoCfdi.Descripcion);
            cfdiv32Info.ClaveDeConfirmacion = oComprobante.Confirmacion;
            //cfdiv32Info.TotalEnLetra = HelperNumberToString.ConvertNumberToString(oComprobante.Total, "peso", false, "", "Son ", "/100", HelperNumberToString.VbeIntStyle.VbTitle);
            cfdiv32Info.TotalEnLetra = HelperNumeroALetras.NumeroALetras(Jaeger.Helpers.DbConvert.ConvertDouble(oComprobante.Total), 1);
            if (!(oComprobante.Complemento == null)) {
                if (!(oComprobante.Complemento.TimbreFiscalDigital == null)) {
                    cfdiv32Info.Uuid = oComprobante.Complemento.TimbreFiscalDigital.UUID;
                    cfdiv32Info.FechaCertificacion = oComprobante.Complemento.TimbreFiscalDigital.FechaTimbrado;
                    cfdiv32Info.NoCertificadoSat = oComprobante.Complemento.TimbreFiscalDigital.NoCertificadoSAT;
                    cfdiv32Info.SelloSat = oComprobante.Complemento.TimbreFiscalDigital.SelloSAT;
                    cfdiv32Info.RfcProvCertif = oComprobante.Complemento.TimbreFiscalDigital.RfcProvCertif;
                    cfdiv32Info.CadenaOriginalSat = oComprobante.Complemento.TimbreFiscalDigital.CadenaOriginal;
                }
            }
            cfdiv32Info.Cbb = HelperQRCodes.CopyImageToByteArray(HelperQRCodes.GetCodigoQR("https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?", cfdiv32Info.EmisorRfc, cfdiv32Info.ReceptorRfc, oComprobante.Total.ToString(), cfdiv32Info.Uuid, cfdiv32Info.SelloSat));

            return cfdiv32Info;
        }

        public static List<Entities.Basico.Concepto> ComprobanteToBasic(CFDI.V40.ComprobanteConcepto[] objetos) {
            var nuevaLista = new List<Entities.Basico.Concepto>();
            foreach (CFDI.V40.ComprobanteConcepto item in objetos) {
                var _concepto = new Entities.Basico.Concepto {
                    Cantidad = item.Cantidad,
                    ClaveProdServ = item.ClaveProdServ,
                    ClaveUnidad = item.ClaveUnidad,
                    Descripcion = item.Descripcion,
                    Descuento = (item.DescuentoSpecified ? item.Descuento : 0),
                    NoIdentificacion = item.NoIdentificacion,
                    Unidad = item.Unidad
                };
                nuevaLista.Add(_concepto);
            }
            return nuevaLista;
        }

        public static Entities.Basico.Concepto ComprobanteToBasic(CFDI.V40.ComprobanteConcepto objeto) //aqi
        {
            Entities.Basico.Concepto item = new Entities.Basico.Concepto();
            item.Cantidad = objeto.Cantidad;
            item.Descripcion = objeto.Descripcion;
            item.Importe = objeto.Importe;
            item.Unidad = objeto.Unidad;
            item.ValorUnitario = objeto.ValorUnitario;
            item.ClaveProdServ = objeto.ClaveProdServ;
            item.Descuento = objeto.Descuento;
            item.ClaveUnidad = objeto.ClaveUnidad;
            item.NoIdentificacion = objeto.NoIdentificacion;

            // cuenta predial
            if (!(objeto.CuentaPredial == null)) {
               // item.NumeroCuentaPredial = objeto.CuentaPredial.Numero;
            }

            // informacion aduanera
            if (!(objeto.InformacionAduanera == null)) {
                if (objeto.InformacionAduanera.Length > 0) {
                    List<string> lista = new List<string>();
                    foreach (CFDI.V40.ComprobanteConceptoInformacionAduanera info in objeto.InformacionAduanera) {
                        lista.Add(info.NumeroPedimento);
                        item.InfoAduNumeroPedimento = info.NumeroPedimento;
                    }
                    item.InfoAduNumeroPedimento = string.Join(",", lista.ToArray());
                }
            }
            return item;
        }

        #endregion

        #region complemento pagos20

        /// <summary>
        /// convertir complemento pagos10 a la clase basica
        /// </summary>
        public Entities.Basico.DocumentoRelacionado ComplementoPagos10ToBasic(CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado objeto) {
            Entities.Basico.DocumentoRelacionado item = new Entities.Basico.DocumentoRelacionado();
            item.IdDocumento = objeto.IdDocumento;
            item.Folio = objeto.Folio;
            item.ImpPagado = Jaeger.Helpers.DbConvert.ConvertDouble(objeto.ImpPagado);
            item.ImpSaldoAnterior = Jaeger.Helpers.DbConvert.ConvertDouble(objeto.ImpSaldoAnt);
            item.ImpSaldoInsoluto = Jaeger.Helpers.DbConvert.ConvertDouble(objeto.ImpSaldoInsoluto);
            item.MetodoDePago = objeto.MetodoDePagoDR;
            item.Moneda = objeto.MonedaDR;
            item.NumParcialidad = Jaeger.Helpers.DbConvert.ConvertInt32(objeto.NumParcialidad);
            item.Serie = objeto.Serie;
            item.TipoCambio = objeto.TipoCambioDR;
            return item;
        }

        public List<Entities.Basico.DocumentoRelacionado> ComplementoPago10ToBasic(CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado[] doctos) {
            List<Entities.Basico.DocumentoRelacionado> newList = new List<Entities.Basico.DocumentoRelacionado>();
            if (doctos == null) {
                return newList;
            }

            foreach (CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado item10 in doctos) {
                newList.Add(new Entities.Basico.DocumentoRelacionado {
                    IdDocumento = item10.IdDocumento,
                    Folio = item10.Folio,
                    ImpPagado = Jaeger.Helpers.DbConvert.ConvertDouble(item10.ImpPagado),
                    ImpSaldoAnterior = Jaeger.Helpers.DbConvert.ConvertDouble(item10.ImpSaldoAnt),
                    ImpSaldoInsoluto = Jaeger.Helpers.DbConvert.ConvertDouble(item10.ImpSaldoInsoluto),
                    MetodoDePago = item10.MetodoDePagoDR,
                    Moneda = item10.MonedaDR,
                    NumParcialidad = Jaeger.Helpers.DbConvert.ConvertInt32(item10.NumParcialidad),
                    Serie = item10.Serie,
                    TipoCambio = item10.TipoCambioDR
                });
            }
            return newList;
        }

        /// <summary>
        /// convertir complemento de pagos version 10 a ComplementoPagos10 comun
        /// </summary>
        public ComplementoPagos Create(Jaeger.CFDI.Complemento.Pagos.V20.Pagos objeto) {
            var newItem = new ComplementoPagos();
            newItem.Version = objeto.Version;
            newItem.Pago = this.Pagos10(objeto.Pago);
            return newItem;
        }

        /// <summary>
        /// convertir un array de Pagos10.PagosPago[] al objeto comun
        /// </summary>
        public BindingList<ComplementoPagosPago> Pagos10(Jaeger.CFDI.Complemento.Pagos.V20.PagosPago[] objetos) {
            var newItems = new BindingList<ComplementoPagosPago>();
            foreach (CFDI.Complemento.Pagos.V20.PagosPago item in objetos) {
                var newItem = new ComplementoPagosPago();
                newItem.CadPago = item.CadPago;
                newItem.CtaBeneficiario = item.CtaBeneficiario;
                newItem.CtaOrdenante = item.CtaOrdenante;
                newItem.FechaPago = item.FechaPago;
                newItem.FormaDePagoP = new ComplementoPagoFormaPago { Clave = item.FormaDePagoP };
                newItem.MonedaP = item.MonedaP;
                newItem.Monto = item.Monto;
                newItem.NomBancoOrdExt = item.NomBancoOrdExt;
                newItem.NumOperacion = item.NumOperacion;
                newItem.RfcEmisorCtaBen = item.RfcEmisorCtaBen;
                newItem.RfcEmisorCtaOrd = item.RfcEmisorCtaOrd;

                newItem.TipoCadPago = null;
                if (item.TipoCadPagoSpecified) {
                    newItem.TipoCadPago = item.TipoCadPago;
                }

                if (item.TipoCambioPSpecified) {
                    newItem.TipoCambioP = item.TipoCambioP;
                }

                newItem.CertPago = null;
                if (item.CertPago != null) {
                    newItem.CertPago = Convert.ToBase64String(item.CertPago);
                }

                newItem.SelloPago = null;
                if (item.SelloPago != null) {
                    newItem.SelloPago = Convert.ToBase64String(item.SelloPago);
                }

                newItem.DoctoRelacionado = this.Pagos10(item.DoctoRelacionado);
                newItem.Impuestos = this.Pagos10(item.ImpuestosP);
                newItems.Add(newItem);
            }
            return newItems;
        }

        public BindingList<ComplementoPagoDoctoRelacionado> Pagos10(CFDI.Complemento.Pagos.V20.PagosPagoDoctoRelacionado[] objetos) {
            if (objetos != null) {
                var newItems = new BindingList<ComplementoPagoDoctoRelacionado>();
                foreach (CFDI.Complemento.Pagos.V20.PagosPagoDoctoRelacionado item in objetos) {
                    var newItem = new ComplementoPagoDoctoRelacionado();
                    newItem.Folio = item.Folio;
                    newItem.IdDocumento = item.IdDocumento;
                    //newItem.MetodoPago = item.MetodoDePagoDR;
                    newItem.Moneda = item.MonedaDR;
                    newItem.NumParcialidad = Convert.ToInt32(item.NumParcialidad);
                    newItem.Serie = item.Serie;
                    newItem.ImpPagado = item.ImpPagado;
                    newItem.ImpSaldoAnt = item.ImpSaldoAnt;
                    newItem.ImpSaldoInsoluto = item.ImpSaldoInsoluto;
                    
                    if (item.EquivalenciaDRSpecified) {
                        newItem.TipoCambio = item.EquivalenciaDR;
                    }
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        public BindingList<ComplementoPagoImpuestos> Pagos10(CFDI.Complemento.Pagos.V20.PagosPagoImpuestosP objetos) {
            if (!(objetos == null)) {
                var newItems = new ComplementoPagoImpuestos();
                var _items = new BindingList<ComplementoPagoImpuestos>();
                if (objetos.RetencionesP != null) {
                    newItems.Retenciones = this.Pagos10(objetos.RetencionesP);
                }

                if (objetos.TrasladosP != null) {
                    newItems.Traslados = this.Pagos10(objetos.TrasladosP);
                }

                if (newItems.Retenciones != null)
                    _items.Add(newItems);
                if (newItems.Traslados != null)
                    _items.Add(newItems);

                return _items;
            }
            return null;
        }

        public BindingList<ComplementoPagoImpuestosRetencion> Pagos10(CFDI.Complemento.Pagos.V20.PagosPagoImpuestosPRetencionP[] objetos) {
            if (objetos != null) {
                var newItems = new BindingList<ComplementoPagoImpuestosRetencion>();
                foreach (var item in objetos) {
                    var newItem = new ComplementoPagoImpuestosRetencion {
                        Impuesto = item.ImpuestoP,
                        Importe = item.ImporteP
                    };
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        public BindingList<ComplementoPagoImpuestosTraslado> Pagos10(Jaeger.CFDI.Complemento.Pagos.V20.PagosPagoImpuestosPTrasladoP[] objetos) {
            if (objetos != null) {
                var newItems = new BindingList<ComplementoPagoImpuestosTraslado>();
                foreach (var item in objetos) {
                    var newItem = new ComplementoPagoImpuestosTraslado();
                    if (item.ImportePSpecified)
                        newItem.Importe = item.ImporteP;
                    newItem.Impuesto = item.ImpuestoP;
                    if (item.TasaOCuotaPSpecified)
                        newItem.TasaOCuota = item.TasaOCuotaP;
                    newItem.TipoFactor = item.TipoFactorP;
                    newItem.Base = item.BaseP;
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }
        #endregion

        #region complemento timbre fiscal version 11

        public ComplementoTimbreFiscal TimbreFiscal(CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital objeto) {
            ComplementoTimbreFiscal newItem = new ComplementoTimbreFiscal {
                UUID = objeto.UUID,
                Version = objeto.Version,
                FechaTimbrado = objeto.FechaTimbrado,
                Leyenda = objeto.Leyenda,
                NoCertificadoSAT = objeto.NoCertificadoSAT,
                RFCProvCertif = objeto.RfcProvCertif,
                SelloCFD = objeto.SelloCFD,
                SelloSAT = objeto.SelloSAT
            };
            return newItem;
        }

        #endregion
    }
}
