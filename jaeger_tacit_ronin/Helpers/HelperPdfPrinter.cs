﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.IO;
using System.Drawing.Printing;
using HtmlAgilityPack;
using Pechkin;
using Pechkin.Synchronized;
using Jaeger.CFDI.V33;

namespace Jaeger.Helpers
{
    public class HelperPdfPrinter
    {
        private readonly HelperConvertidor convertidor = new HelperConvertidor();
        private readonly string templeteDefault ="factura33-generic.html";
        private string claveField;
        private string archivoLogoField;
        private string templeteField;
        private string carpetaTemporalField;
        private string carpetaMediosField;
        private string carpetaComprobantesField;

        public HelperPdfPrinter()
        {
            this.templeteField = "factura33-generic.html";
        }

        #region propiedades
        
        /// <summary>
        /// clave del dominio de la empresa
        /// </summary>
        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
            }
        }

        /// <summary>
        /// obtiene o establece la ruta del archivo del logo a utilizar
        /// </summary>
        public string Logo
        {
            get
            {
                return this.archivoLogoField;
            }
            set
            {
                this.archivoLogoField = value;
            }
        }

        /// <summary>
        /// carpeta para los archivos temporales
        /// </summary>
        public string CarpetaTemporal
        {
            get
            {
                return this.carpetaTemporalField;
            }
            set
            {
                this.carpetaTemporalField = value;
            }
        }

        /// <summary>
        /// carpeta para de imagenes
        /// </summary>
        public string CarpetaMedios
        {
            get
            {
                return this.carpetaMediosField;
            }
            set
            {
                this.carpetaMediosField = value;
            }
        }

        /// <summary>
        /// carpeta de comprobantes
        /// </summary>
        public string CarpetaComprobantes
        {
            get
            {
                return this.carpetaComprobantesField;
            }
            set
            {
                this.carpetaComprobantesField = value;
            }
        }

        /// <summary>
        /// obtiene o establece el templete html a usar
        /// </summary>
        public string Templete
        {
            get
            {
                return this.templeteField;
            }
            set
            {
                this.templeteField = value;
            }
        }

        #endregion

        public string HtmlToPdf(Jaeger.CFDI.Entities.Comprobante objeto, string keyName)
        {
            // obtener templete
            string htmlContent = HelperFileResource.GetResources("Reportes." + this.templeteDefault);

            // reemplazar datos generales
            htmlContent = htmlContent.Replace("{{EMISOR_RAZONSOCIAL}}", objeto.Emisor.Nombre);
            htmlContent = htmlContent.Replace("{{EMISOR_RFC}}", objeto.Emisor.RFC);
            htmlContent = htmlContent.Replace("{{EMISOR_REGIMEN}}", objeto.Emisor.RegimenFiscal);

            htmlContent = htmlContent.Replace("{{RECEPTOR_RAZONSOCIAL}}", objeto.Receptor.Nombre);
            htmlContent = htmlContent.Replace("{{RECEPTOR_RFC}}", objeto.Receptor.RFC);
            htmlContent = htmlContent.Replace("{{RECEPTOR_CURP}}", objeto.Receptor.CURP);
            htmlContent = htmlContent.Replace("{{RECEPTOR_USO_CFDI}}", objeto.Receptor.ClaveUsoCFDI);

            string salida = System.IO.Path.Combine(this.carpetaComprobantesField, keyName + ".pdf");

            return salida;
        }

        public string HtmlToPdf(Comprobante objeto, string keyName)
        {
            string htmlContent = HelperFileResource.GetResources("Reportes." + this.templeteDefault); //HelperFile.ReadFileText(templete);
            Entities.Basico.Comprobante basicComprobante = HelperConvertidor.ComprobanteToBasic(objeto);

            // reemplazar datos generales
            htmlContent = htmlContent.Replace("{{EMISOR_RAZONSOCIAL}}", objeto.Emisor.Nombre);
            htmlContent = htmlContent.Replace("{{EMISOR_RFC}}", basicComprobante.EmisorRfc);
            htmlContent = htmlContent.Replace("{{EMISOR_REGIMEN}}", basicComprobante.EmisorRegimenFiscal);

            htmlContent = htmlContent.Replace("{{RECEPTOR_RAZONSOCIAL}}", basicComprobante.Receptor);
            htmlContent = htmlContent.Replace("{{RECEPTOR_RFC}}", basicComprobante.ReceptorRfc);
            htmlContent = htmlContent.Replace("{{RECEPTOR_CURP}}", basicComprobante.ReceptorCurp);
            htmlContent = htmlContent.Replace("{{RECEPTOR_USO_CFDI}}", basicComprobante.UsoDeCfdi);

            // tipo de comprobante
            htmlContent = htmlContent.Replace("{{COMPROBANTE_TIPODECOMPROBANTE}}", basicComprobante.TipoDeComprobante);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_SERIE}}", basicComprobante.Serie);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_FOLIO}}", basicComprobante.Folio);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_NUMERODECERTIFICADO}}", basicComprobante.EmisorNoCertificado);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_FECHA}}", basicComprobante.Fecha);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_LUGAREXPEDICION}}", basicComprobante.LugarDeExpedicion);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_METODOPAGO}}", basicComprobante.MetodoDePago);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_FORMADEPAGO}}", basicComprobante.FormaDePago);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_CONDICIONES}}", basicComprobante.CondicionesDePago);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_MONEDA}}", basicComprobante.Moneda);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_CONFIRMACION}}", basicComprobante.ClaveDeConfirmacion);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_IVA}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(basicComprobante.Iva)));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_ISR}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(basicComprobante.Isr)));

            htmlContent = htmlContent.Replace("{{COMPROBANTE_IVARETENCION}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(basicComprobante.IvaRetencion)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_ISRRETENCION}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(basicComprobante.IsrRetencion)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_IEPSRETENCION}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(basicComprobante.IepsRetencion)).Replace("$", ""));

            htmlContent = htmlContent.Replace("{{COMPROBANTE_IVATRASLADO}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(basicComprobante.IvaTraslado)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_IEPSTRASLADO}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(basicComprobante.IepsTraslado)).Replace("$", ""));

            htmlContent = htmlContent.Replace("{{COMPROBANTE_DESCUENTOS}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(basicComprobante.Descuento)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_TOTALDESCUENTO}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(basicComprobante.Descuento)).Replace("$", ""));

            htmlContent = htmlContent.Replace("{{COMPROBANTE_SUBTOTAL}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(basicComprobante.SubTotal)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_TOTAL}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(basicComprobante.Total)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_CANTIDADCONLETRA}}", basicComprobante.TotalEnLetra);
            double largo = basicComprobante.CadenaOriginalSat.Length / 5;
            int c = Jaeger.Helpers.DbConvert.ConvertInt32(Math.Ceiling(largo));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_CADENAORIGINALTFD}}", String.Join(" ", this.SplitIntoColumns(basicComprobante.CadenaOriginalSat, c + 1, "")));
            largo = basicComprobante.SelloCfdi.Length / 5;
            c = Jaeger.Helpers.DbConvert.ConvertInt32(Math.Ceiling(largo));
            htmlContent = htmlContent.Replace("{{TIMBRE_SELLOCFD}}", String.Join(" ", this.SplitIntoColumns(basicComprobante.SelloCfdi, c + 1, "")));
            largo = basicComprobante.SelloSat.Length / 5;
            c = Jaeger.Helpers.DbConvert.ConvertInt32(Math.Ceiling(largo));
            htmlContent = htmlContent.Replace("{{TIMBRE_SELLOSAT}}", String.Join(" ", this.SplitIntoColumns(basicComprobante.SelloSat, c + 1, "")));
            htmlContent = htmlContent.Replace("{{TIMBRE_UUID}}", basicComprobante.Uuid);
            htmlContent = htmlContent.Replace("{{TIMBRE_CERTISAT}}", basicComprobante.NoCertificadoSat);
            htmlContent = htmlContent.Replace("{{TIMBRE_FECHA}}", basicComprobante.FechaCertificacion.ToString());
            htmlContent = htmlContent.Replace("{{TIMBRE_PROVEEDOR}}", basicComprobante.RfcProvCertif);
            htmlContent = htmlContent.Replace("{{TIMBRE_QRCODE}}", string.Concat("<img align=\"middle\" src=\"data:image/png;base64, ", basicComprobante.CbbB64, "\"/>"));

            // tipo de cambio
            if (basicComprobante.TipoDeCambio != "0")
            {
                htmlContent = htmlContent.Replace("{{COMPROBANTE_TIPODECAMBIO}}", basicComprobante.TipoDeCambio);
            }
            else
            {
                htmlContent = htmlContent.Replace("{{COMPROBANTE_TIPODECAMBIO}}", "");
            }

            // obetener el logo 
            string pathLogo = System.IO.Path.Combine(this.carpetaMediosField, string.Format("logo-{0}.png", this.claveField));
            if (File.Exists(this.Logo))
            {
                pathLogo = this.Logo;
            }

            htmlContent = (!File.Exists(pathLogo) ? htmlContent.Replace("{{COMPROBANTE_LOGO}}", "") : htmlContent.Replace("{{COMPROBANTE_LOGO}}", string.Concat("<img id=\"empresa-logo\" align=\"middle\" src=\"data:image/png;base64, ", HelperFile.ReadFileB64(pathLogo), "\"/>")));
            htmlContent = htmlContent.Replace("{{CFDIRELACIONADOS_TIPORELACION}}", basicComprobante.TipoRelacion);

            // documentos relacionados
            htmlContent = htmlContent.Replace("{{CFDIRELACIONADOS_TIPORELACION}}", basicComprobante.TipoRelacion);
            if (objeto.CfdiRelacionados == null)
            {
                htmlContent = htmlContent.Replace("{{CFDIRELACIONADOS_UIDSLIST}}", "");
            }
            else
            {
                basicComprobante.CfdiRelacionado = string.Join("<br/>", (
                    from x in objeto.CfdiRelacionados.CfdiRelacionado
                    select x.UUID).ToArray<string>());
                htmlContent = htmlContent.Replace("{{CFDIRELACIONADOS_UIDSLIST}}", basicComprobante.CfdiRelacionado);
            }

            // conceptos
            List<Entities.Basico.Concepto> listaConceptos = HelperConvertidor.ComprobanteToBasic(objeto.Conceptos);
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            // conceptos del comprobante
            HtmlNode htmlNodeconceptos = doc.GetElementbyId("conceptos");
            foreach (Entities.Basico.Concepto bConcepto in listaConceptos)
            {
                HtmlNode temporal = htmlNodeconceptos.Clone();
                temporal.InnerHtml = this.Concepto(bConcepto, temporal.InnerHtml);
                htmlNodeconceptos.ParentNode.ChildNodes.Add(temporal);
                temporal = null;
            }
            // removemos el elemento con Id Conceptos, este solo es la base del html
            htmlNodeconceptos.Remove();

            // confirmacion
            HtmlNode htmlNodeConfirmacion = doc.GetElementbyId("confirmacion");
            if (objeto.Confirmacion == null)
            {
                htmlNodeConfirmacion.Remove();
            }

            // tipo de relacion de CFDI's relacionados
            
            if (objeto.CfdiRelacionados == null)
            {
                HtmlNode htmlNodeTipoRelacion = doc.GetElementbyId("tiporelacion");
                htmlNodeTipoRelacion.Remove();
                HtmlNode htmlNodeCfdiRelacionado = doc.GetElementbyId("cfdirelacionado");
                htmlNodeCfdiRelacionado.Remove();
            }

            // metodo de pago
            HtmlNode htmlNodemetodopago = doc.GetElementbyId("metodopago");
            if (objeto.MetodoPago == null)
            {
                htmlNodemetodopago.Remove();
            }

            // condiciones de pago
            HtmlNode htmlNodeconceptos1 = doc.GetElementbyId("condiciones");
            if (objeto.CondicionesDePago == null)
            {
                htmlNodeconceptos1.Remove();
            }

            // forma de pago
            HtmlNode htmlNodeFormaPago = doc.GetElementbyId("formapago");
            if (objeto.FormaPago == null)
            {
                htmlNodeFormaPago.Remove();
            }

            // tipo de cambio
            HtmlNode htmlNodeTipoCambio = doc.GetElementbyId("tipocambio");
            if (objeto.TipoCambio == null)
            {
                htmlNodeTipoCambio.Remove();
            }

            // bloque de complementos pagos
            if (htmlContent.Contains("bloque-pagos10"))
            {
                HtmlNode nodeComplementoPagos10 = doc.GetElementbyId("bloque-pagos10");
                if (!(objeto.Complemento.Pagos == null))
                {
                    doc = ComplementoPagos10(objeto.Complemento.Pagos, doc);
                }
                else
                {
                    nodeComplementoPagos10.Remove();
                    HtmlNode nodeCfdiRelacionado = doc.GetElementbyId("bloque-pagos10documentos");
                    nodeCfdiRelacionado.Remove();
                }
            }

            // adendas
            if (htmlContent.Contains("bloque-addenda"))
            {
                HtmlNode nodeAddenda = doc.GetElementbyId("bloque-addenda");
                if (!(objeto.Addenda == null))
                {
                    if (!(objeto.Addenda.Miscelanea == null))
                    {
                        //nodeAddenda.InnerHtml = AddendaAfasaMiscelanea(objeto.Addenda.Miscelanea, nodeAddenda.InnerHtml)
                    }
                    else if (!(objeto.Addenda.ReciboFiscal == null))
                    {
                        //doc = AddedaAfasaReciboFiscal(objeto.Addenda.ReciboFiscal, doc)
                    }
                    else if (!(objeto.Addenda.CartaPorte == null))
                    {
                        //doc = AddendaTrasporteCartaPorte(objeto.Addenda.CartaPorte, doc);
                    }
                }
                else
                {
                    nodeAddenda.Remove();
                }
            }

            //HtmlNode htmlNodeTrasladoIeps = doc.GetElementbyId("trasladoieps");
            //if (basicComprobante.IepsTraslado <= 0)
            //{
            //    //htmlNodeTrasladoIEPS.Remove();
            //}

            // convertir el archivo html temporal en formato PDF
            string archivotemporal = System.IO.Path.Combine(this.carpetaTemporalField, basicComprobante.Uuid + ".html");
            doc.Save(archivotemporal, System.Text.Encoding.UTF8);
            GlobalConfig conf = new GlobalConfig();
            conf.SetMargins(0, 0, 0, 0);
            conf.SetColorMode(true);
            conf.SetPaperOrientation(false);
            conf.SetDocumentTitle("Comprobante Fiscal");
            conf.SetPaperSize(PaperKind.Letter);
            conf.SetOutputDpi(200);

            IPechkin objetop = new SynchronizedPechkin(conf);
            ObjectConfig objeto2 = new ObjectConfig();
            objeto2.SetAllowLocalContent(true);
            objeto2.SetPrintBackground(true);
            objeto2.SetPageUri(archivotemporal);
            objeto2.SetIntelligentShrinking(false);
            objeto2.SetPrintBackground(true);

            // guardamos las conversión del archivo html a PDF en la carpeta de comprobantes
            Byte[] pdfBuffer = objetop.Convert(objeto2);
            string salida = System.IO.Path.Combine(this.carpetaComprobantesField, keyName + ".pdf");
            HelperFile.WriteFileByte(pdfBuffer, salida);

            if (File.Exists(salida))
            {
                return salida;
            }
            else
            {
                return string.Empty;
            }
        }

        public string Concepto(Entities.Basico.Concepto bConcepto, string stringHtml)
        {
            stringHtml = stringHtml.Replace("{{CONCEPTO_CANTIDAD}}", Jaeger.Helpers.DbConvert.ConvertString(bConcepto.Cantidad));
            stringHtml = stringHtml.Replace("{{CONCEPTO_UNIDAD}}", bConcepto.Unidad);
            stringHtml = stringHtml.Replace("{{CONCEPTO_CLVUNIDAD}}", bConcepto.ClaveUnidad);
            stringHtml = stringHtml.Replace("{{CONCEPTO_CLVPRODSRV}}", bConcepto.ClaveProdServ);
            stringHtml = stringHtml.Replace("{{CONCEPTO_DESCRIPCION}}", bConcepto.Descripcion);
            stringHtml = stringHtml.Replace("{{CONCEPTO_VALORUNITARIO}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(bConcepto.ValorUnitario)).Replace("$", ""));
            stringHtml = stringHtml.Replace("{{CONCEPTO_DESCUENTO}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(bConcepto.Descuento)).Replace("$", ""));
            stringHtml = stringHtml.Replace("{{CONCEPTO_IMPORTE}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(bConcepto.Importe)).Replace("$", ""));
            if (bConcepto.NumeroCuentaPredial != "")
            {
                //stringHtml = stringHtml.Replace("{{CONCEPTO_CTAPREDIAL}}", "CUENTA PREDIAL: " + bConcepto.NumeroCuentaPredial.ToString());
            }
            else
            {
                stringHtml = stringHtml.Replace("{{CONCEPTO_CTAPREDIAL}}", "");
            }
            return stringHtml;
        }

        public HtmlDocument ComplementoPagos10(CFDI.Complemento.Pagos.V10.Pagos objPagos, HtmlDocument bloqueComplemento)
        {
            //List<Entities.Basico.ComplementoPagos10> co = new List<Entities.Basico.ComplementoPagos10>();
            //Entities.Basico.ComplementoPagos10 objPagos = co[0];

            HtmlNode nodeComplementoPagos10 = bloqueComplemento.GetElementbyId("bloque-pagos10");
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_FECHA}}", objPagos.Pago[0].FechaPago.ToShortDateString());
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_EMISORCTAORDENANTE}}", objPagos.Pago[0].RfcEmisorCtaOrd);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_CTAORDENANTE}}", objPagos.Pago[0].CtaOrdenante);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_EMISORBENEFICIARIO}}", objPagos.Pago[0].RfcEmisorCtaBen);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_CTABENEFICIARIO}}", objPagos.Pago[0].CtaBeneficiario);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_FORMAPAGO}}", objPagos.Pago[0].FormaDePagoP);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_MONEDAP}}", objPagos.Pago[0].MonedaP);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_TIPODECAMBIOP}}", objPagos.Pago[0].TipoCambioP.ToString());
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_MONTOP}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(objPagos.Pago[0].Monto)).Replace("$", ""));
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_NUMOPERACIONP}}", objPagos.Pago[0].NumOperacion);

            if (!(objPagos == null))
            {
                List<Entities.Basico.DocumentoRelacionado> dd = this.convertidor.ComplementoPago10ToBasic(objPagos.Pago[0].DoctoRelacionado);
                HtmlNode nodeCfdiRelacionado = bloqueComplemento.GetElementbyId("pagos10_cfdirelacionado");
                foreach (Entities.Basico.DocumentoRelacionado ddr in dd)
                {
                    HtmlNode temporal = nodeCfdiRelacionado.Clone();
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_IDDOCUMENTO}}", ddr.IdDocumento);
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_SERIE}}", ddr.Serie);
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_FOLIO}}", ddr.Folio);
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_MONEDA}}", ddr.Moneda);
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_TIPODECAMBIOPP}}", ddr.TipoCambio.ToString());
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_PARCIALIDAD}}", ddr.NumParcialidad.ToString());
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_SALDOANTERIOR}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(ddr.ImpSaldoAnterior)).Replace("$", ""));
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_PAGADO}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(ddr.ImpPagado)).Replace("$", ""));
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_SALDOINSOLUTO}}", this.FormatoMoneda(Jaeger.Helpers.DbConvert.ConvertDouble(ddr.ImpSaldoInsoluto)).Replace("$", ""));
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_METODODEPAGO}}", ddr.MetodoDePago);
                    nodeCfdiRelacionado.ParentNode.ChildNodes.Add(temporal);
                    temporal = null;
                }
                nodeCfdiRelacionado.Remove();
            }
            return bloqueComplemento;
        }

        /// <summary>
        /// formato moneda con decimales definidos
        /// </summary>
        public decimal FormatoNumero(double numero, int decimales)
        {
            string str = "".PadRight(decimales, '#');
            string str1 = string.Format(string.Concat("{0:0.", str, "}"), numero);
            return Convert.ToDecimal(str1);
        }

        /// <summary>
        /// formato moneda con decimales definidos
        /// </summary>
        public decimal FormatoNumero(decimal numero, int decimales)
        {
            string str = "".PadRight(decimales, '#');
            string str1 = string.Format(string.Concat("{0:0.", str, "}"), numero);
            return Convert.ToDecimal(str1);
        }

        /// <summary>
        /// formato de moneda
        /// </summary>
        public string FormatoMoneda(double p)
        {
            return string.Format(CultureInfo.CreateSpecificCulture("es-MX"), "{0:C2}", new object[] { p });
        }

        /// <summary>
        /// convierte una cadena en un arreglo de tamaño definido
        /// </summary>
        public string[] SplitIntoColumns(string inputString, int columns, string splitString)
        {
            string[] output = new string[checked(checked(columns - 1) + 1)];
            int colLength = checked((int)Math.Round((double)(inputString.Length) / (double)columns));
            int lastPos = 0;
            int num = columns;
            for (int colCount = 1; colCount <= num; colCount = checked(colCount + 1))
            {
                int pos = -1;
                if (checked(lastPos + colLength) < inputString.Length)
                {
                    pos = inputString.IndexOf(splitString, checked(lastPos + colLength));
                }
                if (pos == -1)
                {
                    pos = inputString.Length;
                }
                output[checked(colCount - 1)] = inputString.Substring(checked(lastPos), checked(pos - lastPos));
                lastPos = pos;
            }
            return output;
        }
    }
}
