﻿/// develop: anhe 04072019
/// purpose: catalogo de carpetas recientesusing Jaeger.Entities;
using System;
using Jaeger.Entities;

namespace Jaeger.Helpers
{
    public class CarpetasRecientes : CatalogoContext<CarpetaItem>
    {
        public CarpetasRecientes()
        {
            this.Title = "Carpetas Recientes";
            this.FileName = "CarpetasRecientes.json";
            this.StartPath = Jaeger.Edita.Enums.EnumPaths.Temporal;
            this.Recuperar = false;
        }

        public void Add(CarpetaItem newItem)
        {
            try
            {
                CarpetaItem e = this.Items.Find((CarpetaItem b) => b.Carpeta == newItem.Carpeta);
                if (e == null)
                    this.Items.Add(newItem);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
