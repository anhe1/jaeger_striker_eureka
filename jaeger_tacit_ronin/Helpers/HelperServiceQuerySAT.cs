﻿// develop: 061020160052
// purpose:
// * Mensajes de Respuesta
// Los mensajes de respuesta que arroja el servicio de consulta de CFDI´s incluyen la descripción del resultado de la operación que corresponden a la siguiente clasificación:
// Mensajes de Rechazo.
// N 601: La expresión impresa proporcionada no es válida.
// Este código de respuesta se presentará cuando la petición de validación no se haya respetado en el formato definido.
// N 602: Comprobante no encontrado.
// Este código de respuesta se presentará cuando el UUID del comprobante no se encuentre en la Base de Datos del SAT.
// Mensajes de Aceptación.
// S Comprobante obtenido satisfactoriamente.
// rev.: 201220171253: agregamos replace para escapar "&"
// rev.: 201801130147: agregamos try al generar url para la consulta, para regresar un error si no es posible generar la url
// rev.: 050120222031: de momento para el error 601 para comprobantes emitidos del 2022 lo dejaromos como validados
using System;
using System.Linq;
using System.Threading.Tasks;
using Jaeger.SAT.Entities;
using Jaeger.com.SAT.QueryCFDI;
using System.Net;

namespace Jaeger.Helpers {
    public static class HelperServiceQuerySAT {
        /// <summary>
        /// El Servicio de consulta de CFDI´s se diseñó para permitir la validación accediendo a un servicio publicado en la página del SAT desde Internet, el servicio pretende proveer una alternativa de consulta que requiera verificar el estado de un comprobante en las Bases de Datos del SAT. 
        /// </summary>
        public static SatQueryResult Query(string emisorRFC, string receptorRFC, decimal total, string uuid) {
            ServicePointManager.Expect100Continue = false;
            if (ServicePointManager.SecurityProtocol == (SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls))
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            ConsultaCFDIServiceClient serviceQuery = new ConsultaCFDIServiceClient();
            Acuse serviceResponse = new Acuse();
            SatQueryResult queryResult = new SatQueryResult();
            string datos;

            queryResult.Key = "E";
            queryResult.Status = "Error";

            try {
                datos = String.Format("?re={0}&rr={1}&tt={2}&id={3}", emisorRFC.Replace("&", "&amp;"), receptorRFC.Replace("&", "&amp;"), total, uuid).Trim();
            } catch (Exception ex) {
                queryResult.Clave = ex.Message;
                return queryResult;
            }

            try {
                serviceQuery.Open();
            } catch (Exception ex) {
                queryResult.Clave = ex.Message;
                Console.WriteLine(ex.Message);
                return queryResult;
            }

            try {
                serviceResponse = serviceQuery.Consulta(datos);
            } catch (Exception ex) {
                queryResult.Clave = ex.Message;
                Console.WriteLine(ex.Message);
                return queryResult;
            }

            queryResult.Status = serviceResponse.Estado;
            queryResult.Clave = serviceResponse.CodigoEstatus;
            queryResult.Key = serviceResponse.CodigoEstatus.ToString().Substring(0, 1);
            queryResult.EFOS = serviceResponse.ValidacionEFOS;

            if (serviceResponse.CodigoEstatus.Contains("601")) {
                queryResult.Status = "Vigente";
                queryResult.Clave = "200";
                queryResult.Key = "S";
            }

            if (serviceResponse.Estado != null) {
                if (serviceResponse.Estado.Contains("Error")) {
                    Console.WriteLine("");
                }
            }
            serviceQuery.Close();
            return queryResult;
        }

        public static async Task<SatQueryResult> QueryAsync(string emisorRFC, string receptorRFC, decimal total, string uuid) {
            Jaeger.SAT.Entities.SatQueryResult estado = null;
            await Task.Run(() => {
                estado = HelperServiceQuerySAT.Query(emisorRFC, receptorRFC, total, uuid);
            });
            return estado;
        }
    }
}
