﻿using System;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Jaeger.Helpers
{
    public class HelperCookieReader
    {
        private HelperCookieReader()
        {
        }

        public static int InternetCookieHttponly
        {
            get
            {
                return 8192;
            }
        }

        public static string GetCookie(string url)
        {
            string getCookie;
            int num = 512;
            StringBuilder stringBuilder = new StringBuilder(num);
            if (!HelperCookieReader.InternetGetCookieEx(url, null, stringBuilder, ref num, 8192, IntPtr.Zero))
            {
                if (num < 0)
                {
                    getCookie = null;
                    return getCookie;
                }
                else
                {
                    stringBuilder = new StringBuilder(num);
                    if (!HelperCookieReader.InternetGetCookieEx(url, null, stringBuilder, ref num, 8192, IntPtr.Zero))
                    {
                        getCookie = null;
                        return getCookie;
                    }
                }
            }
            getCookie = stringBuilder.ToString();
            return getCookie;
        }

        [DllImport("wininet.dll", CharSet = CharSet.None, ExactSpelling = false, SetLastError = true)]
        private static extern bool InternetGetCookieEx(string url, string cookieName, StringBuilder cookieData, ref int size, int flags, IntPtr pReserved);
    }
}
