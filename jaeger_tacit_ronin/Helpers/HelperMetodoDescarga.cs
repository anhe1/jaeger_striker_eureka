﻿// develop: anhe 171020182257
// purpose: crear descargas asincronica 
using System;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Net;
using System.IO;
using Jaeger.Entities;

namespace Jaeger.Helpers
{
    public partial class ProgressReportModel
    {
        public ProgressReportModel()
        {
            PercentageComplete = 0;
            SitesDownloaded = new BindingList<InfoArchivoDescargado>();
        }
        public int PercentageComplete { get; set; }
        public BindingList<InfoArchivoDescargado> SitesDownloaded { get; set; }
        
    }
}

namespace Jaeger.Helpers
{
    public static class HelperMetodoDescarga
    {
        public static async Task<BindingList<InfoArchivoDescargado>> RunDownloadParallelAsyncV2(IProgress<ProgressReportModel> progress, BindingList<InfoArchivoDescargado> websites, string urlWeb, string ruta)
        {
            BindingList<InfoArchivoDescargado> output = new BindingList<InfoArchivoDescargado>();
            ProgressReportModel report = new ProgressReportModel();
            await Task.Run(() =>
            {
                Parallel.ForEach<InfoArchivoDescargado>(websites, (site) =>
                {
                    InfoArchivoDescargado results = DownloadWebsite(site, urlWeb, ruta);
                    output.Add(results);
                    report.SitesDownloaded = output;
                    report.PercentageComplete = (output.Count * 100) / websites.Count;
                    progress.Report(report);
                });
            });
            return output;
        }

        public static InfoArchivoDescargado DownloadWebsite(InfoArchivoDescargado objeto, string urlweb, string ruta)
        {
            // crear la ruta de descarga
            string fileName = Path.Combine(ruta, objeto.Year, objeto.Month, objeto.Day, objeto.KeyName);
            Directory.CreateDirectory(Path.GetDirectoryName(fileName));
            if (Directory.Exists(Path.GetDirectoryName(fileName)) == false)
            {
                fileName = Jaeger.Helpers.JaegerManagerPaths.JaegerPath(Jaeger.Edita.Enums.EnumPaths.Downloads, objeto.KeyName);
            }
            if (Jaeger.Helpers.HelperValidacion.ValidaUrl(objeto.UrlXml))
            {
                if (HelperMetodoDescarga.DownloadFile(objeto.UrlXml, fileName + ".xml", urlweb))
                {
                    objeto.Result = "Completed";
                }
            }

            if (Jaeger.Helpers.HelperValidacion.ValidaUrl(objeto.UrlPdf))
            {
                HelperMetodoDescarga.DownloadFile(objeto.UrlPdf, fileName + ".pdf", urlweb);
            }

            if (Jaeger.Helpers.HelperValidacion.ValidaUrl(objeto.UrlAccuse))
            {
                HelperMetodoDescarga.DownloadFile(objeto.UrlAccuse, fileName + "-acuse.pdf", urlweb);
            }
            return objeto;
        }

        public static bool DownloadFile(string address, string fileName, string urlweb)
        {
            bool blnResult = false;
            try
            {
                string cookie = Jaeger.Helpers.HelperCookieReader.GetCookie(urlweb);
                WebClient webClient = new WebClient();
                if (!File.Exists(fileName))
                {
                    webClient.Headers.Add(HttpRequestHeader.Cookie, cookie);
                    Stream fileStream = new FileStream(fileName, FileMode.Create);
                    fileStream.Write(webClient.DownloadData(address), 0, webClient.DownloadData(address).Length);
                    fileStream.Flush();
                    fileStream.Close();
                    blnResult = true;
                }
                else
                {
                    blnResult = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                blnResult = false;
            }
            return blnResult;
        }
    }
}
