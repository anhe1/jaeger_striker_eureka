﻿using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Net.NetworkInformation;
using System.Threading;
using Jaeger.Edita.V2.Empresa.Entities;
using Jaeger.Enums;
using Jaeger.SAT.Entities;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Helpers
{
    public class DescargaCertificadoSAT
    {
        private FileTransferProtocol objSettings = new FileTransferProtocol();

        public DescargaCertificadoSAT()
            : base() {
        }

        private DescargaCertificadoSAT(FileTransferProtocol objeto)
            : base() {
            objSettings = objeto;
        }

        public FileTransferProtocol Settings {
            get {
                return this.objSettings;
            }
            set {
                this.objSettings = value;
            }
        }

        private static bool ValidateSerialToDownload(string serial) {
            // proposito: validar serial de certificado
            if ((string.IsNullOrEmpty(serial)))
                return false;
            if ((!serial.StartsWith("0")))
                return false;
            if ((serial.Replace("0", "").Length == 0))
                return false;
            if ((serial.Length < 20))
                return false;
            return true;
        }

        public static EnumErrorFTP FileExistInFtp(string pathFtp, string pathFtpFile) {
            EnumErrorFTP EnumErrorFTP;
            try {
                if ((!NetworkInterface.GetIsNetworkAvailable()))
                    EnumErrorFTP = EnumErrorFTP.NotNetworkAvailable;
                else if ((DescargaCertificadoSAT.IsFtpAvaible(pathFtpFile, "SIZE")))
                    return EnumErrorFTP.NotError;
                else
                    EnumErrorFTP = DescargaCertificadoSAT.IsFtpAvaible(pathFtp, "LIST") ? EnumErrorFTP.FileNotFound : EnumErrorFTP.NotFtpAvailable;
            } catch {
                EnumErrorFTP = EnumErrorFTP.Unknown;
            }
            return EnumErrorFTP;
        }

        private static bool IsFtpAvaible(string pathFtpFile, string typeValidation) {
            bool flag;
            FtpWebResponse response = null;
            try {
                try {
                    System.Net.FtpWebRequest ftpWebRequest = (FtpWebRequest)WebRequest.Create(pathFtpFile);
                    ftpWebRequest.Method = typeValidation;
                    DescargaCertificadoSAT.SetProxyInformation(ftpWebRequest);
                    response = (FtpWebResponse)ftpWebRequest.GetResponse();
                } catch (WebException webException1) {
                    WebException webException = webException1;
                    response = (FtpWebResponse)webException.Response;
                    if ((response.StatusCode != FtpStatusCode.ActionNotTakenFileUnavailable))
                        throw webException;
                    flag = false;
                    return flag;
                }
                return true;
            } finally {
                if ((response != null))
                    response.Close();
            }
            return flag;
        }

        private static void SetProxyInformation(FtpWebRequest request) {
            ConfigurationProxy oProxyConfiguration = ConfigurationProxy.GetProxyConfiguration();
            if ((oProxyConfiguration != null))
                request.Proxy = new WebProxy(oProxyConfiguration.Server, int.Parse(System.Convert.ToString(oProxyConfiguration.Port))) {
                    Credentials = new NetworkCredential(oProxyConfiguration.User, oProxyConfiguration.Password)
                };
        }

        public static string GetPathHttpsSat(string oPathHttp, string oSerial) {
            string sParte1 = oSerial.Substring(0, 6);
            string sParte2 = oSerial.Substring(6, 6);
            string sParte3 = oSerial.Substring(12, 2);
            string sParte4 = oSerial.Substring(14, 2);
            string sParte5 = oSerial.Substring(16, 2);
            string strUrlReturn = "https://rdc.sat.gob.mx/rccf/{0}/{1}/{2}/{3}/{4}/{5}.cer";
            return string.Format(strUrlReturn, sParte1, sParte2, sParte3, sParte4, sParte5, oSerial);
        }

        public static string GetPathFtpsat1(string oSerial) {
            string sParte1 = oSerial.Substring(0, 6);
            string sParte2 = oSerial.Substring(6, 6);
            string sParte3 = oSerial.Substring(12, 2);
            string sParte4 = oSerial.Substring(14, 2);
            string sParte5 = oSerial.Substring(16, 2);
            string sPathFtp = "ftp://ftp2.sat.gob.mx/certificados/FEA/{0}/{1}/{2}/{3}/{4}/{5}.cer";
            return string.Format(sPathFtp, sParte1, sParte2, sParte3, sParte4, sParte5, oSerial);
        }

        public static string GetPathFtpsat(string oPathFtpSat, string oSerial) {
            string patternFtp = @"(\d{6})(\d{6})(\d{2})(\d{2})(\d{2})(\d{2})";
            string evalueFtp = "$1/$2/$3/$4/$5/$&.cer";
            string result = Regex.Replace(oSerial, patternFtp, evalueFtp);
            return string.Concat(oPathFtpSat, result);
        }

        public static void DownloadFTP(string oFtp, string nameFile, string oUser = "", string oPsw = "") {
            // Create a request
            string uri = oFtp;
            FtpWebRequest ftp = (FtpWebRequest)FtpWebRequest.Create(uri);
            // Set the credentials
            ftp.Credentials = new NetworkCredential(oUser, oPsw);
            // Turn off KeepAlive (will close connection on completion)
            ftp.KeepAlive = false;
            // we want a binary
            ftp.UseBinary = true;
            // Define the action required (in this case, download a file)
            ftp.Method = WebRequestMethods.Ftp.DownloadFile;

            // If we were using a method that uploads data e.g. UploadFile
            // we would open the ftp.GetRequestStream here an send the data

            // Get the response to the Ftp request and the associated stream
            using (FtpWebResponse response = (FtpWebResponse)ftp.GetResponse()) {
                using (Stream responseStream = response.GetResponseStream()) {
                    // loop to read & write to file
                    using (FileStream fs = new FileStream(nameFile, System.IO.FileMode.Create)) {
                        byte[] buffer = new byte[2048];
                        int read = 0;
                        do {
                            read = responseStream.Read(buffer, 0, buffer.Length);
                            fs.Write(buffer, 0, read);
                        }
                        while (!(read == 0)); // see Note(1)
                        responseStream.Close();
                        fs.Flush();
                        fs.Close();
                    }
                    responseStream.Close();
                }
                response.Close();
            }
        }

        public static Certificate Buscar(string oSerial) {
            if ((!DescargaCertificadoSAT.ValidateSerialToDownload(oSerial))) {
            }
            byte[] fileByZip = null;
            string str = System.Convert.ToString("ftp://ftp2.sat.gob.mx/Certificados/FEA/");
            string pathFtpSat = DescargaCertificadoSAT.GetPathFtpsat(str, oSerial);
            string pathHttpSat = DescargaCertificadoSAT.GetPathHttpsSat("falta el dato", oSerial);
            int num = 0;

            do {
                fileByZip = DescargaCertificadoSAT.GetFileByFtp(pathFtpSat);
                num = num + 1;
                if ((fileByZip != null))
                    continue;
                else {
                    fileByZip = HelperFile.DownloadFile(pathHttpSat);
                    if ((fileByZip != null))
                        continue;
                }

                Thread.Sleep(200 * num);
            }
            while (fileByZip == null && num < 3);

            if ((fileByZip == null)) {
                var oCertificate1 = new Certificate();
                oCertificate1.Serial = "Error";
                switch (DescargaCertificadoSAT.FileExistInFtp(str, pathFtpSat)) {
                    case EnumErrorFTP.NotNetworkAvailable: {
                            oCertificate1.CerB64 = "No se encontró un enlace de Internet activo.";
                            break;
                        }

                    case EnumErrorFTP.NotFtpAvailable: {
                            oCertificate1.CerB64 = "La ruta del FTP no es correcta o no esta disponible.";
                            break;
                        }

                    case EnumErrorFTP.FileNotFound: {
                            oCertificate1.CerB64 = "EL certificado no fue encontrado en el FTP del SAT.";
                            break;
                        }
                }
                // Throw New Exception("Error al intentar descargar el certificado del FTP del SAT, verifique su configuración de red.")
                // Common.LogError.LogWrite(New [Property] With {.Code = -1, .Name = "SearchCertificate", .Value = "Error al intentar descargar el certificado del FTP del SAT, verifique su configuración de red."})
                return oCertificate1;
            }
            var oCertificate = Helper2Certificado.GetInfo(fileByZip);
            return oCertificate;
        }

        private static byte[] GetFileByFtp(string pathFtpFile) {
            byte[] array = null;
            Console.WriteLine(pathFtpFile);
            try {
                FtpWebRequest networkCredential = (FtpWebRequest)WebRequest.Create(pathFtpFile);
                string value = "";
                string str = "";
                networkCredential.Method = "RETR";
                networkCredential.Credentials = new NetworkCredential(value, str);
                networkCredential.Timeout = 600000;
                // SearchOutsideCertificate.SetProxyInformation(networkCredential)
                Stream responseStream = networkCredential.GetResponse().GetResponseStream();
                using (MemoryStream memoryStream = new MemoryStream()) {
                    responseStream.CopyTo(memoryStream);
                    array = memoryStream.ToArray();
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return array;
        }
    }
}
