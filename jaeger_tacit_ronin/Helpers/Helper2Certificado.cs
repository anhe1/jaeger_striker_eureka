﻿using System;
using System.Security.Cryptography.X509Certificates;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Helpers
{
    public static class Helper2Certificado
    {
        public static Certificate GetInfo(byte[] array)
        {
            X509Certificate2 x509 = new X509Certificate2(array);
            Certificate response = new Certificate()
            {
                BeginDateExpiration = x509.NotBefore,
                EndDateExpiration = x509.NotAfter,
                Serial = Helper2Certificado.serialASCIItoHex(x509.SerialNumber)
            };
            return response;
        }

        public static string serialASCIItoHex(string serialAscci)
        {
            string str = "";
            string str1 = "";
            for (int i = 0; i <= serialAscci.Length - 1; i += 2)
            {
                str1 = serialAscci.Substring(i, 2);
                str = string.Concat(str, char.ConvertFromUtf32(Convert.ToInt32(str1, 16)));
            }
            return str;
        }
    }
}
