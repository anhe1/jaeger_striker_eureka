﻿/// develop: 290420171106
/// purpose: convertir diferentes objetos del comprobante fiscal a los de sistema.
using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Jaeger.CFDI.Entities;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Edita.V2.Directorio.Enums;
using Jaeger.Enums;
using Jaeger.Catalogos.Repositories;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Helpers {
    public class CFDIExtension40 {
        readonly FormaPagoCatalogo formaPago33;
        readonly UsoCFDICatalogo usoCFDI;
        readonly RelacionCFDICatalogo relacionCFDI;

        /// <summary>
        /// constructor
        /// </summary>
        public CFDIExtension40() {
            this.formaPago33 = new FormaPagoCatalogo();
            this.formaPago33.Load();
            this.usoCFDI = new UsoCFDICatalogo();
            this.usoCFDI.Load();
            this.relacionCFDI = new RelacionCFDICatalogo();
            this.relacionCFDI.Load();
        }

        #region comprobante fiscal V40

        /// <summary>
        /// convertir V40.ComprobanteEmisor a Contribuyente
        /// </summary>
        public ViewModelContribuyente Emisor(CFDI.V40.ComprobanteEmisor objeto, EnumRelationType relacion) {
            var emisor = new ViewModelContribuyente() {
                Relacion = Enum.GetName(typeof(EnumRelationType), relacion),
                RFC = objeto.Rfc,
                Nombre = objeto.Nombre
            };
            return emisor;
        }

        /// <summary>
        /// convertir Cfd.Contribuyente a V40.ComprobanteEmisor
        /// </summary>
        public CFDI.V40.ComprobanteEmisor Emisor(ViewModelContribuyente objeto) {
            var emisor = new CFDI.V40.ComprobanteEmisor() {
                Nombre = objeto.Nombre,
                Rfc = objeto.RFC,
                RegimenFiscal = objeto.RegimenFiscal
            };
            return emisor;
        }

        /// <summary>
        /// convertir Cfd.Contribyente a V40.ComprobanteReceptor
        /// </summary>
        public CFDI.V40.ComprobanteReceptor Receptor(ViewModelContribuyente objeto, string usoCfdi) {
            var receptor = new CFDI.V40.ComprobanteReceptor() {
                Nombre = objeto.Nombre,
                Rfc = objeto.RFC,
                NumRegIdTrib = objeto.NumRegIdTrib,
                UsoCFDI = usoCfdi,
                ResidenciaFiscalSpecified = false
            };

            if (!(objeto.RegimenFiscal == null)) {
                if (objeto.RegimenFiscal.Trim() != "") {
                    receptor.ResidenciaFiscalSpecified = true;
                    receptor.ResidenciaFiscal = objeto.ResidenciaFiscal;
                }
            }

            return receptor;
        }

        /// <summary>
        /// convertir V40.ComprobanteReceptor a Contribuyente
        /// </summary>
        public ViewModelContribuyente Receptor(CFDI.V40.ComprobanteReceptor objeto, EnumRelationType relacion) {
            var receptor = new ViewModelContribuyente() {
                Relacion = Enum.GetName(typeof(EnumRelationType), relacion),
                RFC = objeto.Rfc,
                Nombre = objeto.Nombre,
                RegimenFiscal = objeto.RegimenFiscalReceptor,
                DomicilioFiscal = objeto.DomicilioFiscalReceptor
            };
            return receptor;
        }

        /// <summary>
        /// convertir V40.ComprobanteEmisor a Contribuyente
        /// </summary>
        public ViewModelContribuyente Emisor(CFDI.V40.ComprobanteEmisor objeto) {
            ViewModelContribuyente newItem = new ViewModelContribuyente();
            newItem.RFC = objeto.Rfc;
            newItem.Nombre = objeto.Nombre;
            return newItem;
        }

        /// <summary>
        /// convertir V40.ComprobanteReceptor a Contribuyente
        /// </summary>
        public ViewModelContribuyente Receptor(CFDI.V40.ComprobanteReceptor objeto) {
            ViewModelContribuyente newItem = new ViewModelContribuyente();
            newItem.RFC = objeto.Rfc;
            newItem.Nombre = objeto.Nombre;
            newItem.DomicilioFiscal = objeto.DomicilioFiscalReceptor;
            newItem.RegimenFiscal = objeto.RegimenFiscalReceptor;
            return newItem;
        }

        /// <summary>
        /// convertir objeto V40.Comprobante a Cfd.Comprobante falta probar
        /// </summary>
        public ViewModelComprobante Create(CFDI.V40.Comprobante objeto) {
            var _comprobante = new ViewModelComprobante();

            // datos generales
            _comprobante.TipoComprobanteText = objeto.TipoDeComprobante;

            _comprobante.Version = objeto.Version;
            _comprobante.Folio = objeto.Folio;
            _comprobante.Serie = objeto.Serie;
            _comprobante.SubTipo = objeto.Type;
            _comprobante.Receptor = this.Receptor(objeto.Receptor);
            _comprobante.Emisor = this.Emisor(objeto.Emisor);
            _comprobante.Conceptos = this.Conceptos(objeto.Conceptos);
            _comprobante.SubTotal = objeto.SubTotal;
            _comprobante.Total = objeto.Total;
            _comprobante.NoCertificado = objeto.NoCertificado;
            _comprobante.FechaEmision = objeto.Fecha;

            _comprobante.Moneda.Clave = objeto.Moneda;
            _comprobante.LugarExpedicion = objeto.LugarExpedicion;

            if (objeto.Receptor.DomicilioFiscalReceptor != null)
                _comprobante.DomicilioFiscal = objeto.Receptor.DomicilioFiscalReceptor;

            if (objeto.Receptor.ResidenciaFiscal != null)
                _comprobante.ResidenciaFiscal = objeto.Receptor.ResidenciaFiscal;

            if (objeto.Receptor.NumRegIdTrib != null)
                _comprobante.Receptor.NumRegIdTrib = objeto.Receptor.NumRegIdTrib;

            var uso = this.usoCFDI.Search(objeto.Receptor.UsoCFDI);
            if (uso != null) {
                _comprobante.UsoCfdi = uso;
            } else {
                _comprobante.UsoCfdi.Clave = objeto.Receptor.UsoCFDI;
            }

            // condiciones de pago
            _comprobante.CondicionPago = "";
            if (!(objeto.CondicionesDePago == null)) {
                _comprobante.CondicionPago = objeto.CondicionesDePago;
            }

            // descuento
            _comprobante.Descuento = 0;
            if (objeto.DescuentoSpecified) {
                _comprobante.Descuento = objeto.Descuento;
            }

            // tipo de cambio
            if (objeto.TipoCambioSpecified) {
                _comprobante.TipoCambio = objeto.TipoCambio.ToString();
            }

            // forma de pago
            _comprobante.FormaPago.Clave = "";
            if (!(objeto.FormaPago == null)) {
                if (objeto.FormaPagoSpecified) {
                    ClaveFormaPago clave = this.formaPago33.Search(Regex.Replace(objeto.FormaPago, "[^\\d]", ""));
                    if (clave == null) {
                        _comprobante.FormaPago.Clave = objeto.FormaPago;
                    } else {
                        _comprobante.FormaPago = clave;
                    }
                }
            }

            // metodo de pago
            _comprobante.MetodoPago.Clave = "";
            if (!(objeto.MetodoPago == null)) {
                if (objeto.MetodoPagoSpecified) {
                    _comprobante.MetodoPago.Clave = objeto.MetodoPago;
                    if (objeto.MetodoPago.ToUpper().Trim() == "PUE") {
                        _comprobante.MetodoPago.Descripcion = "Pago en una sola exhibición";
                    } else if (objeto.MetodoPago.ToUpper().Trim() == "PPD") {
                        _comprobante.MetodoPago.Descripcion = "Pago en parcialidades o diferido";
                    }
                }
            }

            // comprobantes relacionados
            if (!(objeto.CfdiRelacionados == null)) {
                ComprobanteCfdiRelacionados comDoctos = this.ComprobanteCfdiRelacionados(objeto.CfdiRelacionados);
                if (!(comDoctos == null)) {
                    ClaveTipoRelacionCFDI clave = this.relacionCFDI.Search(comDoctos.TipoRelacion.Clave);
                    if (clave != null) {
                        comDoctos.TipoRelacion.Descripcion = clave.Descripcion;
                    }

                    _comprobante.CfdiRelacionados = comDoctos;
                }
            }


            // impuestos
            if (!(objeto.Impuestos == null)) {
                if (objeto.Impuestos.TotalImpuestosRetenidosSpecified) {
                    if (!(objeto.Impuestos.Retenciones == null)) {
                        foreach (CFDI.V40.ComprobanteImpuestosRetencion imp in objeto.Impuestos.Retenciones) {
                            if (imp.Impuesto == "001") // retencion ISR
                            {
                                _comprobante.RetencionIsr = _comprobante.RetencionIsr + imp.Importe;
                            } else if (imp.Impuesto == "002") // retencion IVA
                              {
                                _comprobante.RetencionIva = _comprobante.RetencionIva + imp.Importe;
                            } else if (imp.Impuesto == "003") // retencion IEPS
                              {
                                _comprobante.RetencionIeps = _comprobante.RetencionIeps + imp.Importe;
                            }
                        }
                    }
                }

                if (objeto.Impuestos.TotalImpuestosTrasladadosSpecified) {
                    if (!(objeto.Impuestos.Traslados == null)) {
                        foreach (CFDI.V40.ComprobanteImpuestosTraslado imp in objeto.Impuestos.Traslados) {
                            if (imp.Impuesto == "002") // traslado IVA
                            {
                                _comprobante.TrasladoIva = _comprobante.TrasladoIva + imp.Importe;
                            } else if (imp.Impuesto == "003") // traslado IEPS
                              {
                                _comprobante.TrasladoIeps = _comprobante.TrasladoIeps + imp.Importe;
                            }
                        }
                    }
                }
            }

            // validacion
            if (!(objeto.Validation == null)) {
                _comprobante.Validacion = objeto.Validation;
                //item.FechaVal = objeto.Validation.FechaValidacion;
                _comprobante.Result = _comprobante.Validacion.IsValidText;
                _comprobante.Estado = _comprobante.Validacion.ProofStatus;
                _comprobante.FechaEstado = _comprobante.FechaVal;
            }

            // complementos
            if (!(objeto.Complemento == null)) {
                // complemento timbre fiscal
                if (!(objeto.Complemento.TimbreFiscalDigital == null)) {
                    _comprobante.TimbreFiscal = this.TimbreFiscal(objeto.Complemento.TimbreFiscalDigital);
                } else {
                    _comprobante.TimbreFiscal = null;
                }

                // complemento nomina 1.2
                if (!(objeto.Complemento.Nomina12 == null)) {
                    _comprobante.Nomina = this.Create(objeto.Complemento.Nomina12);
                    if (_comprobante.Nomina != null) {
                        _comprobante.Nomina.Emisor.RFC = objeto.Emisor.Rfc;
                        _comprobante.Nomina.Receptor.RFC = objeto.Receptor.Rfc;
                        _comprobante.Nomina.Descuento = _comprobante.Descuento;
                        _comprobante.Receptor.ClaveUsoCFDI = objeto.Receptor.UsoCFDI;

                        if (_comprobante.TimbreFiscal != null) {
                            _comprobante.Nomina.IdDocumento = _comprobante.TimbreFiscal.UUID;
                        }
                    }
                }

                // complemento de pagos
                if (!(objeto.Complemento.Pagos == null)) {
                    ComplementoPagos c2 = this.Create(objeto.Complemento.Pagos);
                    if (!(c2 == null)) {
                        _comprobante.Complementos = new Complementos();
                        _comprobante.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.Pagos10, Data = c2.Json() });
                        _comprobante.ComplementoPagos = c2;
                    }
                }

                // complemento vales de despensa
                if (!(objeto.Complemento.ValesDeDespensa == null)) {
                    ComplementoValesDeDespensa c3 = this.Create(objeto.Complemento.ValesDeDespensa);
                    if (c3 != null) {
                        if (_comprobante.Complementos == null) {
                            _comprobante.Complementos = new Complementos();
                        }
                        _comprobante.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.ValesDeDespensa, Data = c3.Json() });
                    }

                }

                // complemento leyendas fiscales
                if (!(objeto.Complemento.LeyendasFiscales == null)) {
                    ComplementoLeyendasFiscales c4 = this.Create(objeto.Complemento.LeyendasFiscales);
                    if (c4 != null) {
                        if (_comprobante.Complementos == null) {
                            _comprobante.Complementos = new Complementos();
                        }
                        _comprobante.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.LeyendasFiscales, Data = c4.Json() });
                    }
                }

                // complemento Impuestos Locales
                if (!(objeto.Complemento.ImpuestosLocales == null)) {
                    ComplementoImpuestosLocales c5 = this.Create(objeto.Complemento.ImpuestosLocales);
                    if (c5 != null) {
                        if (_comprobante.Complementos == null) {
                            _comprobante.Complementos = new Complementos();
                        }
                        _comprobante.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.ImpuestosLocales, Data = c5.Json() });
                    }
                }

                // complemento aerolineas
                if (!(objeto.Complemento.Aerolineas == null)) {
                    ComplementoAerolineas c6 = this.Create(objeto.Complemento.Aerolineas);
                    if (c6 != null) {
                        if (_comprobante.Complementos == null) {
                            _comprobante.Complementos = new Complementos();
                        }
                        _comprobante.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.ImpuestosLocales, Data = c6.Json() });
                    }
                }
            }

            return _comprobante;
        }

        /// <summary>
        /// convertir array V40.ComprobanteConcepto a lista Cfd.Concepto
        /// </summary>
        public BindingList<ViewModelComprobanteConcepto> Conceptos(CFDI.V40.ComprobanteConcepto[] conceptos) {
            var _conceptos = new BindingList<ViewModelComprobanteConcepto>();
            foreach (CFDI.V40.ComprobanteConcepto concepto in conceptos) {
                var _concepto = new ViewModelComprobanteConcepto {
                    Cantidad = concepto.Cantidad,
                    Descripcion = concepto.Descripcion,
                    Importe = concepto.Importe,
                    ValorUnitario = concepto.ValorUnitario,
                    NoIdentificacion = concepto.NoIdentificacion,
                    Unidad = concepto.Unidad,
                    ClaveProdServ = concepto.ClaveProdServ,
                    ClaveUnidad = concepto.ClaveUnidad
                };

                if (concepto.ACuentaTerceros != null) {
                    _concepto.ACuentaTerceros = new ComprobanteConceptoACuentaTerceros {
                        DomicilioFiscalACuentaTerceros = concepto.ACuentaTerceros.DomicilioFiscalACuentaTerceros,
                        NombreACuentaTerceros = concepto.ACuentaTerceros.NombreACuentaTerceros,
                        RegimenFiscalACuentaTerceros = concepto.ACuentaTerceros.RegimenFiscalACuentaTerceros,
                        RfcACuentaTerceros = concepto.ACuentaTerceros.RfcACuentaTerceros
                    };
                }

                // impuestos
                if (!(concepto.Impuestos == null)) {
                    //impuestos trasladados
                    if (!(concepto.Impuestos.Traslados == null)) {
                        foreach (CFDI.V40.ComprobanteConceptoImpuestosTraslado t in concepto.Impuestos.Traslados) {
                            ComprobanteConceptoImpuesto itemT = new ComprobanteConceptoImpuesto();
                            itemT.Tipo = EnumTipoImpuesto.Traslado;

                            if (t.Impuesto == "002") // traslado IVA-002
                            {
                                itemT.Impuesto = EnumImpuesto.IVA;
                            } else if (t.Impuesto == "003") //traslado IEPS
                              {
                                itemT.Impuesto = EnumImpuesto.IEPS;
                            }

                            if (t.TipoFactor.ToLower().Contains("tasa")) {
                                itemT.TipoFactor = EnumFactor.Tasa;
                            } else if (t.TipoFactor.ToLower().Contains("cuota")) {
                                itemT.TipoFactor = EnumFactor.Cuota;
                            }

                            if (t.TasaOCuotaSpecified) {
                                itemT.TasaOCuota = t.TasaOCuota;
                            }

                            if (t.ImporteSpecified) {
                                itemT.Importe = t.Importe;
                            }

                            itemT.Base = t.Base;
                            _concepto.Impuestos.Add(itemT);
                        }
                    }

                    // impuestos retenidos
                    if (!(concepto.Impuestos.Retenciones == null)) {
                        foreach (CFDI.V40.ComprobanteConceptoImpuestosRetencion r in concepto.Impuestos.Retenciones) {
                            ComprobanteConceptoImpuesto itemR = new ComprobanteConceptoImpuesto();
                            itemR.Base = r.Base;
                            itemR.Importe = r.Importe;
                            itemR.Tipo = EnumTipoImpuesto.Retencion;
                            if (r.Impuesto == "002") {
                                itemR.Impuesto = EnumImpuesto.IVA;
                            } else if (r.Impuesto == "003") {
                                itemR.Impuesto = EnumImpuesto.IEPS;
                            } else if (r.Impuesto == "001") {
                                itemR.Impuesto = EnumImpuesto.ISR;
                            }

                            if (r.TipoFactor.ToLower().Contains("tasa")) {
                                itemR.TipoFactor = EnumFactor.Tasa;
                            } else if (r.TipoFactor.ToLower().Contains("cuota")) {
                                itemR.TipoFactor = EnumFactor.Cuota;
                            } else if (r.TipoFactor.ToLower().Contains("exento")) {
                                itemR.TipoFactor = EnumFactor.Exento;
                            }
                            _concepto.Impuestos.Add(itemR);
                        }
                    }
                }

                if (!(concepto.CuentaPredial == null)) {
                    foreach (var item in concepto.CuentaPredial) {
                        _concepto.CtaPredial = item.Numero;
                    }
                }

                if (concepto.DescuentoSpecified) {
                    _concepto.Descuento = concepto.Descuento;
                } else {
                    _concepto.Descuento = 0;
                }

                // objeto concepto parte
                if (!(concepto.Parte == null)) {
                    foreach (CFDI.V40.ComprobanteConceptoParte subItem in concepto.Parte) {
                        _concepto.Parte.Add(ConceptoParte(subItem));
                    }
                }

                // objeto informacion aduanera
                if (!(concepto.InformacionAduanera == null)) {
                    _concepto.InformacionAduanera = this.ConceptoInformacionAduanera(concepto.InformacionAduanera);
                }

                // agregamos el nuevo objeto a la lista
                _conceptos.Add(_concepto);
            }
            return _conceptos;
        }

        /// <summary>
        /// convertir V40.ComprobanteConceptoParte a Cfd.ConceptoParte
        /// </summary>
        public ConceptoParte ConceptoParte(CFDI.V40.ComprobanteConceptoParte objeto) {
            ConceptoParte newItem = new ConceptoParte {
                Cantidad = objeto.Cantidad,
                ClaveProdServ = objeto.ClaveProdServ,
                Descripcion = objeto.Descripcion,
                NoIdentificacion = objeto.NoIdentificacion,
                Unidad = objeto.Unidad
            };

            if (objeto.ValorUnitarioSpecified) {
                newItem.ValorUnitario = objeto.ValorUnitario;
            }

            if (objeto.ImporteSpecified) {
                newItem.Importe = objeto.Importe;
            }

            if (!(objeto.InformacionAduanera == null)) {
                newItem.InformacionAduanera = this.ConceptoParteInformacionAduanera(objeto.InformacionAduanera);
            }
            return newItem;
        }

        /// <summary>
        /// convertir un array ComprobanteConceptoInformacionAduanera a lista de ComprobanteInformacionAduanera comun
        /// </summary>
        public BindingList<ComprobanteInformacionAduanera> ConceptoInformacionAduanera(CFDI.V40.ComprobanteConceptoInformacionAduanera[] objetos) {
            if (!(objetos == null)) {
                BindingList<ComprobanteInformacionAduanera> newItems = new BindingList<ComprobanteInformacionAduanera>();
                foreach (CFDI.V40.ComprobanteConceptoInformacionAduanera item in objetos) {
                    ComprobanteInformacionAduanera newItem = new ComprobanteInformacionAduanera();
                    newItem.NumeroPedimento = item.NumeroPedimento;
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        /// <summary>
        /// convertir un array ComprobanteConceptoParteInformacionAduanera a lista de ComprobanteInformacionAduanera comun
        /// </summary>
        public BindingList<ComprobanteInformacionAduanera> ConceptoParteInformacionAduanera(CFDI.V40.ComprobanteConceptoParteInformacionAduanera[] objetos) {
            if (!(objetos == null)) {
                BindingList<ComprobanteInformacionAduanera> newItems = new BindingList<ComprobanteInformacionAduanera>();
                foreach (CFDI.V40.ComprobanteConceptoParteInformacionAduanera item in objetos) {
                    ComprobanteInformacionAduanera newItem = new ComprobanteInformacionAduanera();
                    newItem.NumeroPedimento = item.NumeroPedimento;
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        public ComprobanteCfdiRelacionados ComprobanteCfdiRelacionados(CFDI.V40.ComprobanteCfdiRelacionados[] objeto) {
            if (!(objeto == null)) {
                foreach (var item in objeto) {
                    var newItem = new ComprobanteCfdiRelacionados();
                    newItem.TipoRelacion.Clave = item.TipoRelacion;
                    foreach (var item1 in newItem.CfdiRelacionado) {
                        newItem.CfdiRelacionado.Add(new ComprobanteCfdiRelacionadosCfdiRelacionado {
                            IdDocumento = item1.IdDocumento,
                            Moneda = item1.Moneda,
                            FormaPago = item1.FormaPago,
                            MetodoPago = item1.MetodoPago,
                            RFC = item1.RFC,
                            Folio = item1.Folio,
                            Serie = item1.Serie,
                            Nombre = item1.Nombre,
                            ImporteAplicado = item1.ImporteAplicado
                        });
                    }
                    return newItem;
                }
            }
            return null;
        }

        #endregion

        #region complemento timbre fiscal version 11

        public ComplementoTimbreFiscal TimbreFiscal(CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital objeto) {
            ComplementoTimbreFiscal newItem = new ComplementoTimbreFiscal();
            newItem.UUID = objeto.UUID.ToUpper();
            newItem.Version = objeto.Version;
            newItem.FechaTimbrado = objeto.FechaTimbrado;
            newItem.Leyenda = objeto.Leyenda;
            newItem.NoCertificadoSAT = objeto.NoCertificadoSAT;
            newItem.RFCProvCertif = objeto.RfcProvCertif;
            newItem.SelloCFD = objeto.SelloCFD;
            newItem.SelloSAT = objeto.SelloSAT;
            return newItem;
        }

        #endregion

        #region complemento Nomina12

        public ComplementoNomina Create(Jaeger.CFDI.Complemento.Nomina.V12.Nomina objeto) {
            ComplementoNomina newItem = new ComplementoNomina();
            // general
            newItem.FechaFinalPago = objeto.FechaFinalPago;
            newItem.FechaInicialPago = objeto.FechaFinalPago;
            newItem.FechaPago = objeto.FechaPago;
            newItem.TipoNomina = objeto.TipoNomina;
            newItem.Version = objeto.Version;
            newItem.NumDiasPagados = objeto.NumDiasPagados;
            // emisor
            newItem.Emisor = this.Nomina12(objeto.Emisor);
            // receptor
            newItem.Receptor = this.Nomina12(objeto.Receptor);
            // percepciones
            newItem.Percepciones = this.Nomina12(objeto.Percepciones);
            // deducciones
            newItem.Deducciones = this.Nomina12(objeto.Deducciones);
            // otros pafos
            newItem.OtrosPagos = this.Nomina12(objeto.OtrosPagos);
            // incapacidades
            newItem.Incapacidades = this.Nomina12(objeto.Incapacidades);
            newItem.TotalDeducciones = objeto.TotalDeducciones;
            newItem.TotalOtrosPagos = objeto.TotalOtrosPagos;
            newItem.TotalPercepciones = objeto.TotalPercepciones;

            return newItem;
        }

        public ComplementoNominaPercepciones Nomina12(Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepciones objeto) {
            ComplementoNominaPercepciones newItem = new ComplementoNominaPercepciones();
            newItem.Percepcion = new BindingList<ComplementoNominaPercepcion>();
            foreach (Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcion item in objeto.Percepcion) {
                ComplementoNominaPercepcion newPercepcion = new ComplementoNominaPercepcion();
                newPercepcion.Clave = item.Clave;
                newPercepcion.Concepto = item.Concepto;
                newPercepcion.TipoPercepcion = item.TipoPercepcion;
                newPercepcion.ImporteExento = item.ImporteExento;
                newPercepcion.ImporteGravado = item.ImporteGravado;

                if (item.AccionesOTitulos != null) {
                    newPercepcion.AccionesOTitulos = new ComplementoNominaPercepcionAccionesOTitulos();
                    newPercepcion.AccionesOTitulos.PrecioAlOtorgarse = item.AccionesOTitulos.PrecioAlOtorgarse;
                    newPercepcion.AccionesOTitulos.ValorMercado = item.AccionesOTitulos.ValorMercado;
                }

                if (item.HorasExtra != null) {
                    newPercepcion.HorasExtra = new BindingList<ComplementoNominaPercepcionHorasExtra>();
                    foreach (Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra itemHorasExtra in item.HorasExtra) {
                        ComplementoNominaPercepcionHorasExtra newHorasExtra = new ComplementoNominaPercepcionHorasExtra();
                        newHorasExtra.Dias = itemHorasExtra.Dias;
                        newHorasExtra.HorasExtra = itemHorasExtra.HorasExtra;
                        newHorasExtra.ImportePagado = itemHorasExtra.ImportePagado;
                        newHorasExtra.TipoHoras = itemHorasExtra.TipoHoras;
                        newPercepcion.HorasExtra.Add(newHorasExtra);
                    }
                }
                newItem.Percepcion.Add(newPercepcion);
            }
            if (objeto.TotalJubilacionPensionRetiroSpecified) {
                newItem.TotalSeparacionIndemnizacion = objeto.TotalSeparacionIndemnizacion;
            }
            if (objeto.TotalSeparacionIndemnizacionSpecified) {
                newItem.TotalJubilacionPensionRetiro = objeto.TotalJubilacionPensionRetiro;
            }
            newItem.TotalSueldos = objeto.TotalSueldos;
            newItem.TotalExento = objeto.TotalExento;
            newItem.TotalGravado = objeto.TotalGravado;
            return newItem;
        }

        public ComplementoNominaDeducciones Nomina12(Jaeger.CFDI.Complemento.Nomina.V12.NominaDeducciones objeto) {
            ComplementoNominaDeducciones newItem = new ComplementoNominaDeducciones();
            newItem.Deduccion = new BindingList<ComplementoNominaDeduccion>();
            foreach (Jaeger.CFDI.Complemento.Nomina.V12.NominaDeduccionesDeduccion item in objeto.Deduccion) {
                ComplementoNominaDeduccion newSubItem = new ComplementoNominaDeduccion();
                newSubItem.Clave = item.Clave;
                newSubItem.TipoDeduccion = item.TipoDeduccion;
                newSubItem.Concepto = item.Concepto;
                newSubItem.Importe = item.Importe;
                newItem.Deduccion.Add(newSubItem);
            }
            newItem.TotalImpuestosRetenidos = objeto.TotalImpuestosRetenidos;
            newItem.TotalOtrasDeducciones = objeto.TotalOtrasDeducciones;
            return newItem;
        }

        public BindingList<ComplementoNominaOtroPago> Nomina12(Jaeger.CFDI.Complemento.Nomina.V12.NominaOtroPago[] objetos) {
            if (objetos != null) {
                BindingList<ComplementoNominaOtroPago> newItems = new BindingList<ComplementoNominaOtroPago>();
                foreach (Jaeger.CFDI.Complemento.Nomina.V12.NominaOtroPago item in objetos) {
                    ComplementoNominaOtroPago newItem = new ComplementoNominaOtroPago();
                    newItem.Clave = item.Clave;
                    newItem.Concepto = item.Concepto;
                    newItem.Importe = item.Importe;
                    newItem.TipoOtroPago = item.TipoOtroPago;
                    if (item.SubsidioAlEmpleo != null) {
                        if (item.SubsidioAlEmpleo.SubsidioCausado != null) {
                            newItem.SubsidioAlEmpleo = this.Nomina12(item.SubsidioAlEmpleo);
                        }
                    }

                    if (item.CompensacionSaldosAFavor != null) {
                        newItem.CompensacionSaldosAFavor = new ComplementoNominaOtroPagoCompensacionSaldosAFavor();
                        newItem.CompensacionSaldosAFavor.Anio = item.CompensacionSaldosAFavor.Año;
                        newItem.CompensacionSaldosAFavor.RemanenteSalFav = item.CompensacionSaldosAFavor.RemanenteSalFav;
                        newItem.CompensacionSaldosAFavor.SaldoAFavor = item.CompensacionSaldosAFavor.SaldoAFavor;
                    }
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        public ComplementoNominaOtroPagoSubsidioAlEmpleo Nomina12(Jaeger.CFDI.Complemento.Nomina.V12.NominaOtroPagoSubsidioAlEmpleo objeto) {
            if (objeto != null) {
                ComplementoNominaOtroPagoSubsidioAlEmpleo nuevo = new ComplementoNominaOtroPagoSubsidioAlEmpleo();
                nuevo.SubsidioCausado = objeto.SubsidioCausado;
                return nuevo;
            }
            return null;
        }

        public ComplementoNominaEmisor Nomina12(Jaeger.CFDI.Complemento.Nomina.V12.NominaEmisor objeto) {
            ComplementoNominaEmisor newItem = new ComplementoNominaEmisor();
            newItem.CURP = objeto.Curp;
            newItem.RegistroPatronal = objeto.RegistroPatronal;
            newItem.RFCPatronOrigen = objeto.RfcPatronOrigen;
            if (objeto.EntidadSNCF != null) {
                if (objeto.EntidadSNCF.MontoRecursoPropioSpecified) {
                    newItem.EntidadSncf = new ComplementoNominaEmisorEntidadSncf();
                    newItem.EntidadSncf.MontoRecursoPropio = objeto.EntidadSNCF.MontoRecursoPropio;
                    newItem.EntidadSncf.OrigenRecurso = objeto.EntidadSNCF.OrigenRecurso;
                }
            }

            return newItem;
        }

        public ComplementoNominaReceptor Nomina12(Jaeger.CFDI.Complemento.Nomina.V12.NominaReceptor objeto) {
            ComplementoNominaReceptor newItem = new ComplementoNominaReceptor();
            newItem.Antiguedad = objeto.Antigüedad;
            newItem.Banco = objeto.Banco;
            newItem.ClaveEntFed = objeto.ClaveEntFed;
            newItem.CuentaBancaria = objeto.CuentaBancaria;
            newItem.CURP = objeto.Curp;
            newItem.Departamento = objeto.Departamento;
            newItem.FechaInicioRelLaboral = objeto.FechaInicioRelLaboral;
            newItem.Num = int.Parse(objeto.NumEmpleado);
            newItem.NumSeguridadSocial = objeto.NumSeguridadSocial;
            newItem.PeriodicidadPago = objeto.PeriodicidadPago;
            newItem.Puesto = objeto.Puesto;
            newItem.RiesgoPuesto = objeto.RiesgoPuesto;
            newItem.SalarioBaseCotApor = objeto.SalarioBaseCotApor;
            newItem.SalarioDiarioIntegrado = objeto.SalarioDiarioIntegrado;
            newItem.TipoContrato = objeto.TipoContrato;
            newItem.TipoJornada = objeto.TipoJornada;
            newItem.TipoRegimen = objeto.TipoRegimen;

            newItem.Sindicalizado = null;
            if (objeto.Sindicalizado != null) {
                if (objeto.SindicalizadoSpecified) {
                    newItem.Sindicalizado = objeto.Sindicalizado.ToString();
                }
            }
            return newItem;
        }

        public BindingList<ComplementoNominaIncapacidad> Nomina12(Jaeger.CFDI.Complemento.Nomina.V12.NominaIncapacidad[] objetos) {
            if (objetos != null) {
                BindingList<ComplementoNominaIncapacidad> newItems = new BindingList<ComplementoNominaIncapacidad>();
                foreach (Jaeger.CFDI.Complemento.Nomina.V12.NominaIncapacidad item in objetos) {
                    ComplementoNominaIncapacidad newItem = new ComplementoNominaIncapacidad();
                    newItem.DiasIncapacidad = item.DiasIncapacidad;
                    newItem.ImporteMonetario = item.ImporteMonetario;
                    newItem.TipoIncapacidad = item.TipoIncapacidad;
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        /// <summary>
        /// crear complemento de nomina a partir de la clase comun ComplementoNomina
        /// </summary>
        public Jaeger.CFDI.Complemento.Nomina.V12.Nomina Create(ComplementoNomina objeto) {
            if (objeto != null) {
                Jaeger.CFDI.Complemento.Nomina.V12.Nomina newComplemento = new CFDI.Complemento.Nomina.V12.Nomina();
                newComplemento.Emisor = this.Nomina12(objeto.Emisor);
                newComplemento.Receptor = this.Nomina12(objeto.Receptor);
                newComplemento.FechaInicialPago = objeto.FechaInicialPago.Value;
                newComplemento.FechaFinalPago = objeto.FechaFinalPago.Value;
                newComplemento.FechaPago = objeto.FechaPago.Value;
                newComplemento.NumDiasPagados = objeto.NumDiasPagados;
                newComplemento.TipoNomina = objeto.TipoNomina;
                newComplemento.Percepciones = this.Nomina12(objeto.Percepciones);
                newComplemento.Deducciones = this.Nomina12(objeto.Deducciones);
                newComplemento.Incapacidades = this.Nomina12(objeto.Incapacidades);
                newComplemento.OtrosPagos = this.Nomina12(objeto.OtrosPagos);
                return newComplemento;
            }
            return null;
        }

        public Jaeger.CFDI.Complemento.Nomina.V12.NominaEmisor Nomina12(ComplementoNominaEmisor objeto) {
            Jaeger.CFDI.Complemento.Nomina.V12.NominaEmisor nuevoEmisor = new CFDI.Complemento.Nomina.V12.NominaEmisor();
            nuevoEmisor.Curp = objeto.CURP;
            nuevoEmisor.RegistroPatronal = objeto.RegistroPatronal;
            nuevoEmisor.RfcPatronOrigen = null;
            if (objeto.RFCPatronOrigen != null) {
                nuevoEmisor.RfcPatronOrigen = objeto.RFCPatronOrigen;
            }
            nuevoEmisor.EntidadSNCF = null;
            if (objeto.EntidadSncf != null) {
                nuevoEmisor.EntidadSNCF = new CFDI.Complemento.Nomina.V12.NominaEmisorEntidadSNCF();
                nuevoEmisor.EntidadSNCF.MontoRecursoPropio = objeto.EntidadSncf.MontoRecursoPropio;
                nuevoEmisor.EntidadSNCF.OrigenRecurso = objeto.EntidadSncf.OrigenRecurso;
                nuevoEmisor.EntidadSNCF.MontoRecursoPropioSpecified = true;
            }
            return nuevoEmisor;
        }

        /// <summary>
        /// falta terminar
        /// </summary>
        public Jaeger.CFDI.Complemento.Nomina.V12.NominaReceptor Nomina12(ComplementoNominaReceptor objeto) {
            Jaeger.CFDI.Complemento.Nomina.V12.NominaReceptor nuevoReceptor = new CFDI.Complemento.Nomina.V12.NominaReceptor();
            nuevoReceptor.Antigüedad = objeto.Antiguedad;
            nuevoReceptor.Banco = objeto.Banco;
            nuevoReceptor.BancoSpecified = true;
            nuevoReceptor.ClaveEntFed = objeto.ClaveEntFed;
            nuevoReceptor.CuentaBancaria = objeto.CuentaBancaria;
            nuevoReceptor.Curp = objeto.CURP;
            return nuevoReceptor;
        }

        public Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepciones Nomina12(ComplementoNominaPercepciones objetos) {
            if (objetos != null) {
                Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepciones nuevoPercepciones = new CFDI.Complemento.Nomina.V12.NominaPercepciones();
                List<Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcion> lista = new List<CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcion>();
                // lista de percepciones
                foreach (Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoNominaPercepcion item in objetos.Percepcion) {
                    Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcion nuevaPercepcion = new CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcion();
                    nuevaPercepcion.TipoPercepcion = item.TipoPercepcion;
                    nuevaPercepcion.Clave = item.Clave;
                    nuevaPercepcion.Concepto = item.Concepto;
                    nuevaPercepcion.ImporteExento = item.ImporteExento;
                    nuevaPercepcion.ImporteGravado = item.ImporteGravado;
                    nuevaPercepcion.AccionesOTitulos = this.Nomina12(item.AccionesOTitulos);
                    nuevaPercepcion.HorasExtra = this.Nomina12(item.HorasExtra);
                    lista.Add(nuevaPercepcion);
                }
                nuevoPercepciones.Percepcion = lista.ToArray();
                nuevoPercepciones.JubilacionPensionRetiro = this.Nomina12(objetos.JubilacionPensionRetiro);
                nuevoPercepciones.SeparacionIndemnizacion = this.Nomina12(objetos.SeparacionIndemnizacion);
                return nuevoPercepciones;
            }
            return null;
        }

        public Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionAccionesOTitulos Nomina12(ComplementoNominaPercepcionAccionesOTitulos objetos) {
            if (objetos != null) {
                Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionAccionesOTitulos nuevo = new CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionAccionesOTitulos();
                nuevo.PrecioAlOtorgarse = objetos.PrecioAlOtorgarse;
                nuevo.ValorMercado = objetos.ValorMercado;
                return nuevo;
            }
            return null;
        }

        private CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra[] Nomina12(BindingList<ComplementoNominaPercepcionHorasExtra> objetos) {
            if (objetos != null) {
                List<CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra> lista = new List<CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra>();
                foreach (ComplementoNominaPercepcionHorasExtra item in objetos) {
                    CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra nuevo = new CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra();
                    nuevo.Dias = item.Dias;
                    nuevo.HorasExtra = item.HorasExtra;
                    nuevo.ImportePagado = item.ImportePagado;
                    nuevo.TipoHoras = item.TipoHoras;
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        public Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesJubilacionPensionRetiro Nomina12(ComplementoNominaPercepcionesJubilacionPensionRetiro objetos) {
            if (objetos != null) {
                Jaeger.CFDI.Complemento.Nomina.V12.NominaPercepcionesJubilacionPensionRetiro nuevo = new CFDI.Complemento.Nomina.V12.NominaPercepcionesJubilacionPensionRetiro();
                nuevo.IngresoAcumulable = objetos.IngresoAcumulable;
                nuevo.IngresoNoAcumulable = objetos.IngresoNoAcumulable;
                nuevo.MontoDiario = objetos.MontoDiario;
                nuevo.TotalParcialidad = objetos.TotalParcialidad;
                nuevo.TotalUnaExhibicion = objetos.TotalUnaExhibicion;
            }
            return null;
        }

        private CFDI.Complemento.Nomina.V12.NominaPercepcionesSeparacionIndemnizacion Nomina12(ComplementoNominaPercepcionesSeparacionIndemnizacion objetos) {
            if (objetos != null) {
                CFDI.Complemento.Nomina.V12.NominaPercepcionesSeparacionIndemnizacion nuevo = new CFDI.Complemento.Nomina.V12.NominaPercepcionesSeparacionIndemnizacion();
                nuevo.IngresoAcumulable = objetos.IngresoAcumulable;
                nuevo.IngresoNoAcumulable = objetos.IngresoNoAcumulable;
                nuevo.NumAñosServicio = objetos.NumaniosServicio;
                nuevo.TotalPagado = objetos.TotalPagado;
                nuevo.UltimoSueldoMensOrd = objetos.UltimoSueldoMensOrd;
                return nuevo;
            }
            return null;
        }

        private CFDI.Complemento.Nomina.V12.NominaDeducciones Nomina12(ComplementoNominaDeducciones objetos) {
            if (objetos != null) {
                CFDI.Complemento.Nomina.V12.NominaDeducciones nuevo = new CFDI.Complemento.Nomina.V12.NominaDeducciones();
                List<CFDI.Complemento.Nomina.V12.NominaDeduccionesDeduccion> lista = new List<CFDI.Complemento.Nomina.V12.NominaDeduccionesDeduccion>();
                foreach (ComplementoNominaDeduccion item in objetos.Deduccion) {
                    CFDI.Complemento.Nomina.V12.NominaDeduccionesDeduccion nuevaDeduccion = new CFDI.Complemento.Nomina.V12.NominaDeduccionesDeduccion();
                    nuevaDeduccion.Clave = item.Clave;
                    nuevaDeduccion.Concepto = item.Concepto;
                    nuevaDeduccion.Importe = item.Importe;
                    nuevaDeduccion.TipoDeduccion = item.TipoDeduccion;
                    lista.Add(nuevaDeduccion);
                }
                nuevo.Deduccion = lista.ToArray();
                nuevo.TotalImpuestosRetenidos = objetos.TotalImpuestosRetenidos;
                nuevo.TotalOtrasDeducciones = objetos.TotalOtrasDeducciones;
                return nuevo;
            }
            return null;
        }

        private CFDI.Complemento.Nomina.V12.NominaIncapacidad[] Nomina12(BindingList<ComplementoNominaIncapacidad> objetos) {
            if (objetos != null) {
                List<CFDI.Complemento.Nomina.V12.NominaIncapacidad> lista = new List<CFDI.Complemento.Nomina.V12.NominaIncapacidad>();
                foreach (ComplementoNominaIncapacidad item in objetos) {
                    CFDI.Complemento.Nomina.V12.NominaIncapacidad nuevo = new CFDI.Complemento.Nomina.V12.NominaIncapacidad();
                    nuevo.DiasIncapacidad = item.DiasIncapacidad;
                    nuevo.ImporteMonetario = item.ImporteMonetario;
                    nuevo.TipoIncapacidad = item.TipoIncapacidad;
                    nuevo.ImporteMonetarioSpecified = item.ImporteMonetario > 0;
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        private CFDI.Complemento.Nomina.V12.NominaOtroPago[] Nomina12(BindingList<ComplementoNominaOtroPago> objetos) {
            if (objetos != null) {
                List<CFDI.Complemento.Nomina.V12.NominaOtroPago> lista = new List<CFDI.Complemento.Nomina.V12.NominaOtroPago>();
                foreach (ComplementoNominaOtroPago item in objetos) {
                    CFDI.Complemento.Nomina.V12.NominaOtroPago nuevo = new CFDI.Complemento.Nomina.V12.NominaOtroPago();
                    nuevo.Clave = item.Clave;
                    nuevo.Concepto = item.Concepto;
                    nuevo.Importe = item.Importe;
                    nuevo.TipoOtroPago = item.TipoOtroPago;
                    nuevo.CompensacionSaldosAFavor = this.Nomina12(item.CompensacionSaldosAFavor);
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        private CFDI.Complemento.Nomina.V12.NominaOtroPagoCompensacionSaldosAFavor Nomina12(ComplementoNominaOtroPagoCompensacionSaldosAFavor objeto) {
            if (objeto != null) {
                CFDI.Complemento.Nomina.V12.NominaOtroPagoCompensacionSaldosAFavor nuevo = new CFDI.Complemento.Nomina.V12.NominaOtroPagoCompensacionSaldosAFavor();
                nuevo.Año = objeto.Anio;
                nuevo.RemanenteSalFav = objeto.RemanenteSalFav;
                nuevo.SaldoAFavor = objeto.SaldoAFavor;
                return nuevo;
            }
            return null;
        }

        #endregion

        #region complemento pagos10

        /// <summary>
        /// convertir complemento pagos10 a la clase basica
        /// </summary>
        public Entities.Basico.DocumentoRelacionado ComplementoPagos10ToBasic(CFDI.Complemento.Pagos.V20.PagosPagoDoctoRelacionado objeto) {
            Entities.Basico.DocumentoRelacionado item = new Entities.Basico.DocumentoRelacionado();
            item.IdDocumento = objeto.IdDocumento;
            item.Folio = objeto.Folio;
            item.ImpPagado = Jaeger.Helpers.DbConvert.ConvertDouble(objeto.ImpPagado);
            item.ImpSaldoAnterior = Jaeger.Helpers.DbConvert.ConvertDouble(objeto.ImpSaldoAnt);
            item.ImpSaldoInsoluto = Jaeger.Helpers.DbConvert.ConvertDouble(objeto.ImpSaldoInsoluto);
            item.MetodoDePago = ""; //objeto.MetodoDePagoDR;
            item.Moneda = objeto.MonedaDR;
            item.NumParcialidad = DbConvert.ConvertInt32(objeto.NumParcialidad);
            item.Serie = objeto.Serie;
            if (objeto.EquivalenciaDRSpecified)
                item.TipoCambio = objeto.EquivalenciaDR;
            return item;
        }

        public List<Entities.Basico.DocumentoRelacionado> ComplementoPago10ToBasic(CFDI.Complemento.Pagos.V20.PagosPagoDoctoRelacionado[] doctos) {
            List<Entities.Basico.DocumentoRelacionado> newList = new List<Entities.Basico.DocumentoRelacionado>();
            if (doctos == null) {
                return newList;
            }

            foreach (CFDI.Complemento.Pagos.V20.PagosPagoDoctoRelacionado item10 in doctos) {
                newList.Add(new Entities.Basico.DocumentoRelacionado {
                    IdDocumento = item10.IdDocumento,
                    Folio = item10.Folio,
                    ImpPagado = Jaeger.Helpers.DbConvert.ConvertDouble(item10.ImpPagado),
                    ImpSaldoAnterior = Jaeger.Helpers.DbConvert.ConvertDouble(item10.ImpSaldoAnt),
                    ImpSaldoInsoluto = Jaeger.Helpers.DbConvert.ConvertDouble(item10.ImpSaldoInsoluto),
                    MetodoDePago = "", // item10.MetodoDePagoDR,
                    Moneda = item10.MonedaDR,
                    NumParcialidad = Jaeger.Helpers.DbConvert.ConvertInt32(item10.NumParcialidad),
                    Serie = item10.Serie,
                    TipoCambio = (item10.EquivalenciaDRSpecified ? item10.EquivalenciaDR : new decimal(0.0))
                });
            }
            return newList;
        }

        /// <summary>
        /// convertir complemento de pagos version 10 a ComplementoPagos10 comun
        /// </summary>
        public ComplementoPagos Create(Jaeger.CFDI.Complemento.Pagos.V20.Pagos objeto) {
            Jaeger.Edita.V2.CFDI.Entities.ComplementoPagos newItem = new ComplementoPagos();
            newItem.Version = objeto.Version;
            newItem.Pago = this.Pagos10(objeto.Pago);
            return newItem;
        }

        /// <summary>
        /// convertir un array de Pagos10.PagosPago[] al objeto comun
        /// </summary>
        public BindingList<ComplementoPagosPago> Pagos10(Jaeger.CFDI.Complemento.Pagos.V20.PagosPago[] objetos) {
            var newItems = new BindingList<ComplementoPagosPago>();
            foreach (Jaeger.CFDI.Complemento.Pagos.V20.PagosPago item in objetos) {
                var newItem = new ComplementoPagosPago();
                newItem.CadPago = item.CadPago;
                newItem.CtaBeneficiario = item.CtaBeneficiario;
                newItem.CtaOrdenante = item.CtaOrdenante;
                newItem.FechaPago = item.FechaPago;
                newItem.FormaDePagoP = new ComplementoPagoFormaPago { Clave = item.FormaDePagoP };
                newItem.MonedaP = item.MonedaP;
                newItem.Monto = item.Monto;
                newItem.NomBancoOrdExt = item.NomBancoOrdExt;
                newItem.NumOperacion = item.NumOperacion;
                newItem.RfcEmisorCtaBen = item.RfcEmisorCtaBen;
                newItem.RfcEmisorCtaOrd = item.RfcEmisorCtaOrd;

                newItem.TipoCadPago = null;
                if (item.TipoCadPagoSpecified) {
                    newItem.TipoCadPago = item.TipoCadPago;
                }

                if (item.TipoCambioPSpecified) {
                    newItem.TipoCambioP = item.TipoCambioP;
                }

                newItem.CertPago = null;
                if (item.CertPago != null) {
                    newItem.CertPago = Convert.ToBase64String(item.CertPago);
                }

                newItem.SelloPago = null;
                if (item.SelloPago != null) {
                    newItem.SelloPago = Convert.ToBase64String(item.SelloPago);
                }

                newItem.DoctoRelacionado = this.Pagos10(item.DoctoRelacionado);
                //newItem.Impuestos = this.Pagos10(item.Impuestos);
                newItems.Add(newItem);
            }
            return newItems;
        }

        public BindingList<ComplementoPagoDoctoRelacionado> Pagos10(Jaeger.CFDI.Complemento.Pagos.V20.PagosPagoDoctoRelacionado[] objetos) {
            if (objetos != null) {
                var newItems = new BindingList<ComplementoPagoDoctoRelacionado>();
                foreach (CFDI.Complemento.Pagos.V20.PagosPagoDoctoRelacionado item in objetos) {
                    var newItem = new ComplementoPagoDoctoRelacionado();
                    newItem.Folio = item.Folio;
                    newItem.IdDocumento = item.IdDocumento;
                    newItem.MetodoPago = ""; // item.MetodoDePagoDR;
                    newItem.Moneda = item.MonedaDR;
                    newItem.NumParcialidad = Convert.ToInt32(item.NumParcialidad);
                    newItem.Serie = item.Serie;
                    newItem.ImpPagado = item.ImpPagado;
                    newItem.ImpSaldoAnt = item.ImpSaldoAnt;
                    newItem.ImpSaldoInsoluto = item.ImpSaldoInsoluto;
                    if (item.EquivalenciaDRSpecified) {
                        newItem.TipoCambio = item.EquivalenciaDR;
                    }
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        //public BindingList<ComplementoPagoImpuestos> Pagos10(Jaeger.CFDI.Complemento.Pagos.V20.PagosPagoImpuestos[] objetos) {
        //    if (!(objetos == null)) {
        //        BindingList<Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoImpuestos> newItems = new BindingList<ComplementoPagoImpuestos>();
        //        foreach (Jaeger.CFDI.Complemento.Pagos.V20.PagosPagoImpuestos item in objetos) {
        //            Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoImpuestos newItem = new ComplementoPagoImpuestos();
        //            newItem.Retenciones = this.Pagos10(item.Retenciones);
        //            newItem.Traslados = this.Pagos10(item.Traslados);
        //            if (item.TotalImpuestosRetenidosSpecified) {
        //                newItem.TotalImpuestosRetenidos = item.TotalImpuestosRetenidos;
        //            }
        //            if (item.TotalImpuestosTrasladadosSpecified) {
        //                newItem.TotalImpuestosTrasladados = item.TotalImpuestosTrasladados;
        //            }
        //            newItems.Add(newItem);
        //        }
        //        return newItems;
        //    }
        //    return null;
        //}

        //public BindingList<ComplementoPagoImpuestosRetencion> Pagos10(Jaeger.CFDI.Complemento.Pagos.V20.PagosPagoImpuestosRetencion[] objetos) {
        //    if (objetos != null) {
        //        BindingList<Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoImpuestosRetencion> newItems = new BindingList<ComplementoPagoImpuestosRetencion>();
        //        foreach (Jaeger.CFDI.Complemento.Pagos.V20.PagosPagoImpuestosRetencion item in objetos) {
        //            Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoImpuestosRetencion newItem = new ComplementoPagoImpuestosRetencion();
        //            newItem.Impuesto = item.Impuesto;
        //            newItem.Importe = item.Importe;
        //            newItems.Add(newItem);
        //        }
        //        return newItems;
        //    }
        //    return null;
        //}

        //public BindingList<ComplementoPagoImpuestosTraslado> Pagos10(Jaeger.CFDI.Complemento.Pagos.V20.PagosPagoImpuestosTraslado[] objetos) {
        //    if (objetos != null) {
        //        BindingList<Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoImpuestosTraslado> newItems = new BindingList<ComplementoPagoImpuestosTraslado>();
        //        foreach (Jaeger.CFDI.Complemento.Pagos.V20.PagosPagoImpuestosTraslado item in objetos) {
        //            Jaeger.Edita.V2.CFDI.Entities.ComplementoPagoImpuestosTraslado newItem = new ComplementoPagoImpuestosTraslado();
        //            newItem.Importe = item.Importe;
        //            newItem.Impuesto = item.Impuesto;
        //            newItem.TasaOCuota = item.TasaOCuota;
        //            newItem.TipoFactor = item.TipoFactor;
        //            newItems.Add(newItem);
        //        }
        //        return newItems;
        //    }
        //    return null;
        //}
        #endregion

        #region complemento LeyendasFiscales

        public ComplementoLeyendasFiscales Create(Jaeger.CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscales objeto) {
            if (objeto != null) {
                ComplementoLeyendasFiscales nuevo = new ComplementoLeyendasFiscales();
                nuevo.Version = objeto.version;
                nuevo.Leyenda = this.LeyendasFiscales(objeto.Leyenda);
            }
            return null;
        }

        private BindingList<ComplementoLeyendasFiscalesLeyenda> LeyendasFiscales(CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscalesLeyenda[] objetos) {
            if (objetos != null) {
                BindingList<ComplementoLeyendasFiscalesLeyenda> lista = new BindingList<ComplementoLeyendasFiscalesLeyenda>();
                foreach (CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscalesLeyenda item in objetos) {
                    ComplementoLeyendasFiscalesLeyenda nuevo = new ComplementoLeyendasFiscalesLeyenda();
                    nuevo.DisposicionFiscal = item.disposicionFiscal;
                    nuevo.Norma = item.norma;
                    nuevo.TextoLeyenda = item.textoLeyenda;
                    lista.Add(nuevo);
                }
                return lista;
            }
            return null;
        }

        public Jaeger.CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscales Create(ComplementoLeyendasFiscales objeto) {
            Jaeger.CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscales nuevo = new CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscales();
            nuevo.version = objeto.Version;
            nuevo.Leyenda = this.LeyendasFiscales(objeto.Leyenda);
            return nuevo;
        }

        private CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscalesLeyenda[] LeyendasFiscales(BindingList<ComplementoLeyendasFiscalesLeyenda> objetos) {
            if (objetos != null) {
                List<CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscalesLeyenda> lista = new List<CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscalesLeyenda>();
                foreach (ComplementoLeyendasFiscalesLeyenda item in objetos) {
                    CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscalesLeyenda nuevo = new CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscalesLeyenda();
                    nuevo.disposicionFiscal = item.DisposicionFiscal;
                    nuevo.norma = item.Norma;
                    nuevo.textoLeyenda = item.TextoLeyenda;
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        #endregion

        #region complemento vales de despensa v10

        public ComplementoValesDeDespensa Create(CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensa objeto) {
            if (objeto != null) {
                ComplementoValesDeDespensa nuevo = new ComplementoValesDeDespensa();
                nuevo.Version = objeto.version;
                nuevo.RegistroPatronal = objeto.registroPatronal;
                nuevo.TipoOperacion = objeto.tipoOperacion;
                nuevo.Total = objeto.total;
                nuevo.Conceptos = this.ValesDeDespensa10(objeto.Conceptos);
            }
            return null;
        }

        private BindingList<ComplementoValesDeDespensaConcepto> ValesDeDespensa10(CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensaConcepto[] objetos) {
            if (objetos != null) {
                BindingList<ComplementoValesDeDespensaConcepto> lista = new BindingList<ComplementoValesDeDespensaConcepto>();
                foreach (CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensaConcepto item in objetos) {
                    ComplementoValesDeDespensaConcepto nuevo = new ComplementoValesDeDespensaConcepto();
                    nuevo.CURP = item.curp;
                    nuevo.Fecha = item.fecha;
                    nuevo.Identificador = item.identificador;
                    nuevo.Importe = item.importe;
                    nuevo.Nombre = item.nombre;
                    nuevo.NumSeguridadSocial = item.numSeguridadSocial;
                    nuevo.RFC = item.rfc;
                    lista.Add(nuevo);
                }
                return lista;
            }
            return null;
        }

        public CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensa Create(ComplementoValesDeDespensa objeto) {
            if (objeto != null) {
                CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensa nuevo = new CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensa();
                nuevo.version = objeto.Version;
                nuevo.registroPatronal = objeto.RegistroPatronal;
                nuevo.tipoOperacion = objeto.TipoOperacion;
                nuevo.total = objeto.Total;
                nuevo.Conceptos = this.ValesDeDespensa10(objeto.Conceptos);
            }
            return null;
        }

        private CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensaConcepto[] ValesDeDespensa10(BindingList<ComplementoValesDeDespensaConcepto> objetos) {
            if (objetos != null) {
                List<CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensaConcepto> lista = new List<CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensaConcepto>();
                foreach (ComplementoValesDeDespensaConcepto item in objetos) {
                    CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensaConcepto nuevo = new CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensaConcepto();
                    nuevo.rfc = item.RFC;
                    nuevo.curp = item.CURP;
                    nuevo.fecha = item.Fecha;
                    nuevo.identificador = item.Identificador;
                    nuevo.importe = item.Importe;
                    nuevo.nombre = item.Nombre;
                    nuevo.numSeguridadSocial = item.NumSeguridadSocial;
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        #endregion

        #region complemento impuestos locales v10

        public ComplementoImpuestosLocales Create(CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocales objeto) {
            if (objeto != null) {
                ComplementoImpuestosLocales nuevo = new ComplementoImpuestosLocales();
                nuevo.Version = objeto.version;
                nuevo.TotaldeRetenciones = objeto.TotaldeRetenciones;
                nuevo.TotaldeTraslados = objeto.TotaldeTraslados;
                nuevo.RetencionesLocales = this.ImpuestosLocales10(objeto.RetencionesLocales);
                nuevo.TrasladosLocales = this.ImpuestosLocales10(objeto.TrasladosLocales);
                return nuevo;
            }
            return null;
        }

        private BindingList<ComplementoImpuestosLocalesTrasladosLocales> ImpuestosLocales10(CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesTrasladosLocales[] objetos) {
            if (objetos != null) {
                BindingList<ComplementoImpuestosLocalesTrasladosLocales> lista = new BindingList<ComplementoImpuestosLocalesTrasladosLocales>();
                foreach (CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesTrasladosLocales item in objetos) {
                    ComplementoImpuestosLocalesTrasladosLocales nuevo = new ComplementoImpuestosLocalesTrasladosLocales();
                    nuevo.ImpLocTrasladado = item.ImpLocTrasladado;
                    nuevo.Importe = item.Importe;
                    nuevo.TasadeTraslado = item.TasadeTraslado;
                    lista.Add(nuevo);
                }
                return lista;
            }
            return null;
        }

        private BindingList<ComplementoImpuestosLocalesRetencionesLocales> ImpuestosLocales10(CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesRetencionesLocales[] objetos) {
            if (objetos != null) {
                BindingList<ComplementoImpuestosLocalesRetencionesLocales> lista = new BindingList<ComplementoImpuestosLocalesRetencionesLocales>();
                foreach (CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesRetencionesLocales item in objetos) {
                    ComplementoImpuestosLocalesRetencionesLocales nuevo = new ComplementoImpuestosLocalesRetencionesLocales();
                    nuevo.ImpLocRetenido = item.ImpLocRetenido;
                    nuevo.Importe = item.Importe;
                    nuevo.TasadeRetencion = item.TasadeRetencion;
                    lista.Add(nuevo);
                }
                return lista;
            }
            return null;
        }

        public CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocales Create(ComplementoImpuestosLocales objeto) {
            CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocales nuevo = new CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocales();
            nuevo.version = objeto.Version;
            nuevo.TotaldeRetenciones = objeto.TotaldeRetenciones;
            nuevo.TotaldeTraslados = objeto.TotaldeTraslados;
            nuevo.RetencionesLocales = this.ImpuestosLocales10(objeto.RetencionesLocales);
            nuevo.TrasladosLocales = this.ImpuestosLocales10(objeto.TrasladosLocales);
            return nuevo;
        }

        private CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesRetencionesLocales[] ImpuestosLocales10(BindingList<ComplementoImpuestosLocalesRetencionesLocales> objetos) {
            if (objetos != null) {
                List<CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesRetencionesLocales> lista = new List<CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesRetencionesLocales>();
                foreach (ComplementoImpuestosLocalesRetencionesLocales item in objetos) {
                    CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesRetencionesLocales nuevo = new CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesRetencionesLocales();
                    nuevo.ImpLocRetenido = item.ImpLocRetenido;
                    nuevo.Importe = item.Importe;
                    nuevo.TasadeRetencion = item.TasadeRetencion;
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        private CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesTrasladosLocales[] ImpuestosLocales10(BindingList<ComplementoImpuestosLocalesTrasladosLocales> objetos) {
            if (objetos != null) {
                List<CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesTrasladosLocales> lista = new List<CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesTrasladosLocales>();
                foreach (ComplementoImpuestosLocalesTrasladosLocales item in objetos) {
                    CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesTrasladosLocales nuevo = new CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesTrasladosLocales();
                    nuevo.ImpLocTrasladado = item.ImpLocTrasladado;
                    nuevo.Importe = item.Importe;
                    nuevo.TasadeTraslado = item.TasadeTraslado;
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        #endregion

        #region complemento aerolineas v10

        public ComplementoAerolineas Create(CFDI.Complemento.Aerolineas.V10.Aerolineas objeto) {
            if (objeto != null) {
                ComplementoAerolineas nuevo = new ComplementoAerolineas();
                nuevo.Version = objeto.Version;
                nuevo.TUA = objeto.TUA;
                nuevo.OtrosCargos = this.Aerolineas10(objeto.OtrosCargos);
                return nuevo;
            }
            return null;
        }

        private ComplementoAerolineasOtrosCargos Aerolineas10(CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargos objeto) {
            if (objeto != null) {
                ComplementoAerolineasOtrosCargos nuevo = new ComplementoAerolineasOtrosCargos();
                nuevo.TotalCargos = objeto.TotalCargos;
                nuevo.Cargo = this.Aerolineas10(objeto.Cargo);
                return nuevo;
            }
            return null;
        }

        private BindingList<ComplementoAerolineasOtrosCargosCargo> Aerolineas10(CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargosCargo[] objetos) {
            if (objetos != null) {
                BindingList<ComplementoAerolineasOtrosCargosCargo> lista = new BindingList<ComplementoAerolineasOtrosCargosCargo>();
                foreach (CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargosCargo item in objetos) {
                    ComplementoAerolineasOtrosCargosCargo nuevo = new ComplementoAerolineasOtrosCargosCargo();
                    nuevo.CodigoCargo = item.CodigoCargo;
                    nuevo.Importe = item.Importe;
                }
                return lista;
            }
            return null;
        }

        public CFDI.Complemento.Aerolineas.V10.Aerolineas Create(ComplementoAerolineas objeto) {
            if (objeto != null) {
                CFDI.Complemento.Aerolineas.V10.Aerolineas nuevo = new CFDI.Complemento.Aerolineas.V10.Aerolineas();
                nuevo.Version = objeto.Version;
                nuevo.TUA = objeto.TUA;
                nuevo.OtrosCargos = this.Aerolineas10(objeto.OtrosCargos);
                return nuevo;
            }
            return null;
        }

        private CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargos Aerolineas10(ComplementoAerolineasOtrosCargos objeto) {
            if (objeto != null) {
                CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargos nuevo = new CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargos();
                nuevo.TotalCargos = objeto.TotalCargos;
                nuevo.Cargo = this.Aerolineas10(objeto.Cargo);
            }
            return null;
        }

        private CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargosCargo[] Aerolineas10(BindingList<ComplementoAerolineasOtrosCargosCargo> objetos) {
            if (objetos != null) {
                List<CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargosCargo> lista = new List<CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargosCargo>();
                foreach (ComplementoAerolineasOtrosCargosCargo item in objetos) {
                    CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargosCargo nuevo = new CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargosCargo();
                    nuevo.CodigoCargo = item.CodigoCargo;
                    nuevo.Importe = item.Importe;
                    lista.Add(nuevo);
                }
                return lista.ToArray();
            }
            return null;
        }

        #endregion

        #region complemento spei

        public ComplementoSpei Spei(Jaeger.CFDI.Complemento_SPEI objeto) {
            if (objeto != null) {
                ComplementoSpei newItem = new ComplementoSpei();
                newItem.SpeiTercero = Spei(objeto.SPEI_Tercero);
                return newItem;
            }
            return null;
        }

        public BindingList<ComplementoSpeiTercero> Spei(Jaeger.CFDI.Complemento_SPEISPEI_Tercero[] objetos) {
            if (objetos != null) {
                BindingList<ComplementoSpeiTercero> newItems = new BindingList<ComplementoSpeiTercero>();
                foreach (Jaeger.CFDI.Complemento_SPEISPEI_Tercero item in objetos) {
                    ComplementoSpeiTercero newItem = new ComplementoSpeiTercero();
                    newItem.Beneficiario = Spei(item.Beneficiario);
                    newItem.CadenaCda = item.cadenaCDA;
                    newItem.ClaveSpei = item.ClaveSPEI;
                    newItem.FechaOperacion = item.FechaOperacion;
                    newItem.Hora = item.Hora;
                    newItem.NumeroCertificado = item.numeroCertificado;
                    newItem.Ordenante = this.Spei(item.Ordenante);
                    newItem.Sello = item.sello;
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        public ComplementoSpeiTerceroBeneficiario Spei(Jaeger.CFDI.Complemento_SPEISPEI_TerceroBeneficiario objeto) {
            if (objeto != null) {
                ComplementoSpeiTerceroBeneficiario newItem = new ComplementoSpeiTerceroBeneficiario();
                newItem.BancoReceptor = objeto.BancoReceptor;
                newItem.Concepto = objeto.Concepto;
                newItem.Cuenta = objeto.Cuenta;
                newItem.IVA = objeto.IVA;
                newItem.MontoPago = objeto.MontoPago;
                newItem.Nombre = objeto.Nombre;
                newItem.RFC = objeto.RFC;
                newItem.TipoCuenta = objeto.TipoCuenta;
            }
            return null;
        }

        public ComplementoSpeiTerceroOrdenante Spei(Jaeger.CFDI.Complemento_SPEISPEI_TerceroOrdenante objeto) {
            if (objeto != null) {
                ComplementoSpeiTerceroOrdenante newItem = new ComplementoSpeiTerceroOrdenante();
                newItem.BancoEmisor = objeto.BancoEmisor;
                newItem.Cuenta = objeto.Cuenta;
                newItem.Nombre = objeto.Nombre;
                newItem.RFC = objeto.RFC;
                newItem.TipoCuenta = objeto.TipoCuenta;
                return newItem;
            }
            return null;
        }

        public static ComplementoPagosPago Create(Jaeger.CFDI.SpeiTercero objeto) {
            if (objeto != null) {
                ComplementoPagosPago newItem = new ComplementoPagosPago();
                newItem.FechaPago = new DateTime(objeto.FechaOperacion.Year, objeto.FechaOperacion.Month, objeto.FechaOperacion.Day, objeto.Hora.Hour, objeto.Hora.Minute, objeto.Hora.Second);
                newItem.FormaDePagoP = new ComplementoPagoFormaPago { Clave = "03" };
                newItem.MonedaP = "MXN";
                newItem.Monto = objeto.Beneficiario.MontoPago;
                newItem.RfcEmisorCtaOrd = objeto.Ordenante.RFC;
                newItem.NomBancoOrdExt = objeto.Ordenante.BancoEmisor;
                newItem.CtaOrdenante = objeto.Ordenante.Cuenta.ToString();
                newItem.RfcEmisorCtaBen = objeto.Beneficiario.RFC;
                newItem.CtaBeneficiario = objeto.Beneficiario.Cuenta.ToString();
                newItem.CertPago = objeto.numeroCertificado.ToString();
                newItem.CadPago = objeto.cadenaCDA;
                newItem.SelloPago = objeto.sello;
                return newItem;
            }
            return null;
        }

        #endregion

        #region acuse de cancelacion

        /// <summary>
        /// convertir acuse de cancelacion SAT a acuse comun
        /// </summary>
        public ViewModelAccuseCancelacion Accuse(Jaeger.Edita.V2.CFDI.Entities.Cancel.Acuse objeto) {
            if (objeto != null) {
                ViewModelAccuseCancelacion newItem = new ViewModelAccuseCancelacion();
                newItem.RfcEmisor = objeto.RfcEmisor;
                newItem.FechaCancelacion = objeto.Fecha;
                newItem.FechaSolicitud = objeto.Fecha;
                newItem.SelloDigital = Convert.ToBase64String(objeto.Signature.SignatureValue.Value);
                foreach (Jaeger.Edita.V2.CFDI.Entities.Cancel.AcuseFolios item in objeto.Folios) {
                    newItem.FolioFiscal = item.UUID;
                    newItem.Estado = item.EstatusUUID;
                }
                return newItem;
            }
            return null;
        }

        #endregion

        #region funciones

        public static string Domicilio(CFDI.V32.t_UbicacionFiscal oDomicilio) {
            string outString = "";
            if (oDomicilio != null) {
                if (!string.IsNullOrEmpty(oDomicilio.calle)) {
                    outString = string.Concat(outString, "Calle : ", oDomicilio.calle);
                }
                if (!string.IsNullOrEmpty(oDomicilio.noExterior)) {
                    outString = string.Concat(outString, " ", oDomicilio.noExterior);
                }
                if (!string.IsNullOrEmpty(oDomicilio.noInterior)) {
                    outString = string.Concat(outString, " ", oDomicilio.noInterior);
                }
                if (!string.IsNullOrEmpty(oDomicilio.colonia)) {
                    outString = string.Concat(outString, Environment.NewLine, "Colonia : ", oDomicilio.colonia);
                }
                if (!string.IsNullOrEmpty(oDomicilio.localidad)) {
                    outString = string.Concat(outString, Environment.NewLine, "Localidad : ", oDomicilio.localidad);
                }
                if (!string.IsNullOrEmpty(oDomicilio.municipio)) {
                    outString = string.Concat(outString, Environment.NewLine, "Del/Mun : ", oDomicilio.municipio);
                }
                if (!string.IsNullOrEmpty(oDomicilio.codigoPostal)) {
                    outString = string.Concat(outString, ", CP : ", oDomicilio.codigoPostal);
                }
                if (!string.IsNullOrEmpty(oDomicilio.pais)) {
                    outString = string.Concat(outString, Environment.NewLine, "Pais : ", oDomicilio.pais);
                }
                if (!string.IsNullOrEmpty(oDomicilio.estado)) {
                    outString = string.Concat(outString, Environment.NewLine, "Entidad Federativa : ", oDomicilio.estado);
                }
            }
            return outString;
        }

        /// <summary>
        /// Devuelve una cadena de texto con direccion para version 3.2
        /// </summary>
        public static string Domicilio(CFDI.V32.t_Ubicacion oDomicilio) {
            string outString = "";
            if (oDomicilio != null) {
                if (!string.IsNullOrEmpty(oDomicilio.calle)) {
                    outString = string.Concat(outString, "Calle : ", oDomicilio.calle);
                }
                if (!string.IsNullOrEmpty(oDomicilio.noExterior)) {
                    outString = string.Concat(outString, " ", oDomicilio.noExterior);
                }
                if (!string.IsNullOrEmpty(oDomicilio.noInterior)) {
                    outString = string.Concat(outString, " ", oDomicilio.noInterior);
                }
                if (!string.IsNullOrEmpty(oDomicilio.colonia)) {
                    outString = string.Concat(outString, Environment.NewLine, "Colonia : ", oDomicilio.colonia);
                }
                if (!string.IsNullOrEmpty(oDomicilio.localidad)) {
                    outString = string.Concat(outString, Environment.NewLine, "Localidad : ", oDomicilio.localidad);
                }
                if (!string.IsNullOrEmpty(oDomicilio.municipio)) {
                    outString = string.Concat(outString, Environment.NewLine, "Del/Mun : ", oDomicilio.municipio);
                }
                if (!string.IsNullOrEmpty(oDomicilio.codigoPostal)) {
                    outString = string.Concat(outString, ", ", oDomicilio.codigoPostal);
                }
                if (!string.IsNullOrEmpty(oDomicilio.pais)) {
                    outString = string.Concat(outString, Environment.NewLine, "Pais : ", oDomicilio.pais);
                }
                if (!string.IsNullOrEmpty(oDomicilio.estado)) {
                    outString = string.Concat(outString, Environment.NewLine, "Entidad Federativa : ", oDomicilio.estado);
                }
            }
            return outString;
        }

        public static string Domicilio(ViewModelDomicilio oDomicilio) {
            string outString = "";
            if (oDomicilio != null) {
                if (!string.IsNullOrEmpty(oDomicilio.Calle)) {
                    outString = string.Concat(outString, "Calle : ", oDomicilio.Calle);
                }
                if (!string.IsNullOrEmpty(oDomicilio.NoExterior)) {
                    outString = string.Concat(outString, " ", oDomicilio.NoExterior);
                }
                if (!string.IsNullOrEmpty(oDomicilio.NoInterior)) {
                    outString = string.Concat(outString, " ", oDomicilio.NoInterior);
                }
                if (!string.IsNullOrEmpty(oDomicilio.Colonia)) {
                    outString = string.Concat(outString, Environment.NewLine, "Colonia : ", oDomicilio.Colonia);
                }
                if (!string.IsNullOrEmpty(oDomicilio.Localidad)) {
                    outString = string.Concat(outString, Environment.NewLine, "Localidad : ", oDomicilio.Localidad);
                }
                if (!string.IsNullOrEmpty(oDomicilio.Municipio)) {
                    outString = string.Concat(outString, Environment.NewLine, "Del/Mun : ", oDomicilio.Municipio);
                }
                if (!string.IsNullOrEmpty(oDomicilio.CodigoPostal)) {
                    outString = string.Concat(outString, ", ", oDomicilio.CodigoPostal);
                }
                if (!string.IsNullOrEmpty(oDomicilio.Pais)) {
                    outString = string.Concat(outString, Environment.NewLine, "Pais : ", oDomicilio.Pais);
                }
                if (!string.IsNullOrEmpty(oDomicilio.Estado)) {
                    outString = string.Concat(outString, Environment.NewLine, "Entidad Federativa : ", oDomicilio.Estado);
                }
            }
            return outString;
        }

        #endregion
    }
}