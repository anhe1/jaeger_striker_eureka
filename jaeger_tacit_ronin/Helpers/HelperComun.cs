﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Jaeger.Helpers
{
    public class HelperComun
    {
        public static decimal FormatoNumero(double numero, int decimales)
        {
            string str = "".PadRight(decimales, '#');
            string str1 = string.Format(string.Concat("{0:0.", str, "}"), numero);
            return Convert.ToDecimal(str1);
        }

        public static decimal FormatoNumero(decimal numero, int decimales)
        {
            string str = "".PadRight(decimales, '#');
            string str1 = string.Format(string.Concat("{0:0.", str, "}"), numero);
            return Convert.ToDecimal(str1);
        }

        public static string CleanString(string inString)
        {
            string outString;
            try
            {
                outString = new Regex("[^ÁÉÍÓÚ a-zA-Z0-9 (), -:.\\[\\]]").Replace(inString, "");
                outString = new Regex("\\+").Replace(outString, " ");
            }
            catch (Exception)
            {
                outString = string.Empty;
            }
            return outString;
        }

        public static string CleanStringFileName(string inString)
        {
            string outString;
            try
            {
                outString = new Regex("[^a-zA-Z0-9 \\[\\]]").Replace(inString, "");
                outString = new Regex("\\+").Replace(outString, " ");
            }
            catch (Exception)
            {
                outString = string.Empty;
            }
            return outString;
        }

        public static string CleanStringForRename(string name)
        {
            string stringOut = name.Replace("/", "").Replace("\\", "").Replace("$", "").Replace("#", "").Replace("&", "");
            return stringOut;
        }

        public static string AgregarCeros(double numero, int decimales)
        {
            string str = "";
            string str1 = "".PadRight(decimales, '0');
            str = string.Format(string.Concat("{0:0.", str1, "}"), numero);
            return str;
        }
    }
}
