﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Jaeger.Entities.Reportes;

namespace Jaeger.Helpers
{
    public class HelperExcelExport
    {
        public static string RunSample(string nombreXlsx, string hojaNombre, EstadoCuentaComprobantes nuevo)
        {
            FileInfo newFile = new FileInfo(nombreXlsx);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var package = new ExcelPackage(newFile))
            {
                // agregar una nueva hoja
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(hojaNombre + "1");
                
                // encabezados para el archivo Excel
                int fila = 1;
                worksheet.Row(fila).Style.Font.Bold = true;
                worksheet.Cells[fila, 1].Value = "Tipo Comprobante";
                worksheet.Column(1).Width = 10;
                worksheet.Cells[fila, 2].Value = "Status";
                worksheet.Column(2).Width = 10;
                worksheet.Cells[fila, 3].Value = "Folio";
                worksheet.Column(3).Width = 10;
                worksheet.Cells[fila, 4].Value = "Serie";
                worksheet.Column(4).Width = 10;
                worksheet.Cells[fila, 5].Value = "Emisor (RFC)";
                worksheet.Column(5).Width = 17;
                worksheet.Cells[fila, 6].Value = "Emisor";
                worksheet.Column(6).Width = 60;
                worksheet.Cells[fila, 7].Value = "Receptor (RFC)";
                worksheet.Column(7).Width = 17;
                worksheet.Cells[fila, 8].Value = "Receptor";
                worksheet.Column(8).Width = 60;
                worksheet.Cells[fila, 9].Value = "UUID";
                worksheet.Column(9).Width = 39;
                worksheet.Cells[fila, 10].Value = "Fecha Emisión";
                worksheet.Column(10).Width = 11;
                worksheet.Cells[fila, 11].Value = "Fecha Timbrado";
                worksheet.Column(11).Width = 11;
                worksheet.Cells[fila, 12].Value = "Metodo Pago";
                worksheet.Column(12).Width = 12;
                worksheet.Cells[fila, 13].Value = "Forma Pago";
                worksheet.Column(13).Width = 12;
                worksheet.Cells[fila, 14].Value = "Parcialidad";
                worksheet.Column(14).Width = 10;
                worksheet.Cells[fila, 15].Value = "Total";
                worksheet.Column(15).Width = 12;
                worksheet.Cells[fila, 16].Value = "Pagado";
                worksheet.Column(16).Width = 12;
                worksheet.Cells[fila, 17].Value = "Saldo";
                worksheet.Column(17).Width = 12;
                worksheet.Cells[fila, 18].Value = "Estado";
                worksheet.Column(18).Width = 12;
                worksheet.Cells["A1:R1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["A1:R1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                fila = 2;

                foreach (Entities.Reportes.ComprobanteNormal item in nuevo.Items)
                {
                    worksheet.Cells[fila, 1].Value = item.TipoComprobanteText;
                    worksheet.Cells[fila, 2].Value = item.Status;
                    worksheet.Cells[fila, 3].Value = item.Folio;
                    worksheet.Cells[fila, 4].Value = item.Serie;
                    worksheet.Cells[fila, 5].Value = item.EmisorRFC;
                    worksheet.Cells[fila, 6].Value = item.Emisor;
                    worksheet.Cells[fila, 7].Value = item.ReceptorRFC;
                    worksheet.Cells[fila, 8].Value = item.Receptor;
                    worksheet.Cells[fila, 9].Value = item.UUID;
                    worksheet.Cells[fila,10].Value = item.FechaEmision;
                    worksheet.Cells[fila,10].Style.Numberformat.Format = "mm-dd-yy";
                    worksheet.Cells[fila,11].Value = item.FechaTimbrado;
                    worksheet.Cells[fila,11].Style.Numberformat.Format = "mm-dd-yy";
                    worksheet.Cells[fila,12].Value = item.ClaveMetodoPago;
                    worksheet.Cells[fila,13].Value = item.ClaveFormaPago;
                    worksheet.Cells[fila,14].Value = item.NumParcialidad;
                    worksheet.Cells[fila,15].Value = item.Total;
                    worksheet.Cells[fila,15].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[fila,16].Value = 0;//item.Acumulado;
                    worksheet.Cells[fila,16].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[fila,17].Value = item.Saldo;
                    worksheet.Cells[fila,17].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[fila,18].Value = item.Estado;

                    if (item.Estado == "Cancelado")
                    {
                        worksheet.Row(fila).Style.Font.Italic = true;
                    }

                    if (item.Saldo > 0)
                    {
                        worksheet.Cells[fila, 17].Style.Font.Color.SetColor(Color.Red);
                    }

                    fila = fila + 1;

                    if (item.Pagos.Count > 0)
                    {
                        foreach (Entities.Reportes.ComprobanteNormal item2 in item.Pagos)
                        {
                            worksheet.Cells[fila, 1].Value = item2.TipoComprobanteText;
                            worksheet.Cells[fila, 2].Value = item2.Status;
                            worksheet.Cells[fila, 3].Value = item2.Folio;
                            worksheet.Cells[fila, 4].Value = item2.Serie;
                            worksheet.Cells[fila, 5].Value = item2.EmisorRFC;
                            worksheet.Cells[fila, 6].Value = item2.Emisor;
                            worksheet.Cells[fila, 7].Value = item2.ReceptorRFC;
                            worksheet.Cells[fila, 8].Value = item2.Receptor;
                            worksheet.Cells[fila, 9].Value = item2.UUID;
                            worksheet.Cells[fila,10].Value = item2.FechaEmision;
                            worksheet.Cells[fila,10].Style.Numberformat.Format = "mm-dd-yy";
                            worksheet.Cells[fila,11].Value = item2.FechaTimbrado;
                            worksheet.Cells[fila,11].Style.Numberformat.Format = "mm-dd-yy";
                            worksheet.Cells[fila,12].Value = item2.ClaveMetodoPago;
                            worksheet.Cells[fila,13].Value = item2.ClaveFormaPago;
                            worksheet.Cells[fila,14].Value = item2.NumParcialidad;
                            worksheet.Cells[fila,15].Value = item2.Total;
                            worksheet.Cells[fila,15].Style.Numberformat.Format = "#,##0.00";
                            worksheet.Cells[fila, 16].Value = item.Acumulado;
                            worksheet.Cells[fila,16].Style.Numberformat.Format = "#,##0.00";
                            worksheet.Cells[fila,17].Value = item2.Saldo;
                            worksheet.Cells[fila,17].Style.Numberformat.Format = "#,##0.00";
                            worksheet.Cells[fila,18].Value = item2.Estado;

                            if (item2.TipoComprobanteText == "Egreso")
                            {
                                worksheet.Cells[fila, 16].Value = item2.Total;
                                worksheet.Cells[fila, 17].Value = 0;
                            }

                            if (item.Estado == "Cancelado")
                            {
                                worksheet.Row(fila).Style.Font.Italic = true;
                            }

                            fila = fila + 1;
                        }
                    }
                }

                // establecer algunas propiedades del documento
                package.Workbook.Properties.Title = "Reporte";
                package.Workbook.Properties.Author = "Tacit Ronin";
                package.Workbook.Properties.Comments = "Exportación de datos.";
                package.Workbook.Properties.Created = DateTime.Now;
                // propiedades extendidas
                package.Workbook.Properties.Company = "Jaeger Tacit Ronin";
                // establecer algunos valores de propiedad personalizados
                package.Workbook.Properties.SetCustomPropertyValue("Creado por", "Antonio Hernández R.");
                package.Workbook.Properties.SetCustomPropertyValue("AssemblyName", "Tacit Ronin");
                
                package.Save();
            }
            return string.Empty;
        }

        public static void RunEstadoCuenta(string xlsx, EstadoDeCuenta objetos)
        {
            FileInfo newFile = new FileInfo(xlsx);
            using (var package = new ExcelPackage(newFile))
            {
                // agregar una nueva hoja
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("hoja1");

                // encabezados para el archivo Excel
                int fila = 1;
                worksheet.Row(fila).Style.Font.Bold = true;
                worksheet.Cells[fila, 1].Value = "Tipo Comprobante";

                foreach (EstadoDeCuentaComprobantes item in objetos.Items)
                {
                    worksheet.Cells[fila, 1].Value = item.TipoComprobanteText;
                    fila = +1;
                }

                package.Save();
            }
        }
    }
}
