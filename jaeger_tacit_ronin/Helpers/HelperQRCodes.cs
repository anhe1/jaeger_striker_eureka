﻿using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;
using ThoughtWorks.QRCode.Codec;
using ThoughtWorks.QRCode.Codec.Data;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace Jaeger.Helpers
{
    public class HelperQRCodes
    {
        public HelperQRCodes()
        {
        }

        /// <summary>
        /// configuracion para version 3.3
        /// </summary>
        public static Image GetCodigoQR(string url, string emisor, string receptor, string total, string uuid, string sello)
        {
            Image image = null;
            int num = 2;
            string str = num.ToString();
            QRCodeEncoder qRCodeEncoder = new QRCodeEncoder()
            {
                QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE,
                QRCodeScale = num,
                QRCodeErrorCorrect = ThoughtWorks.QRCode.Codec.QRCodeEncoder.ERROR_CORRECTION.M
            };

            if (str == "Alto (25%)")
            {
                qRCodeEncoder.QRCodeErrorCorrect = ThoughtWorks.QRCode.Codec.QRCodeEncoder.ERROR_CORRECTION.Q;
            }
            qRCodeEncoder.QRCodeVersion = 0;

            try
            {
                string[] contenido = { url, "&id=", uuid, "&re=", emisor, "&rr=", receptor, "&tt=", total, "&fe=", sello.Substring(sello.Length - 8, 8) };
                image = qRCodeEncoder.Encode(string.Concat(contenido), Encoding.UTF8);
            }
            catch (Exception)
            {
                throw;
            }
            return image;
        }

        public static Image GetCodigoQR(string emisor, string receptor, string total, string uuid)
        {
            Image image = null;
            int num = 2;
            string str = num.ToString();
            QRCodeEncoder qRCodeEncoder = new QRCodeEncoder()
            {
                QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE,
                QRCodeScale = num,
                QRCodeErrorCorrect = ThoughtWorks.QRCode.Codec.QRCodeEncoder.ERROR_CORRECTION.M
            };

            if (str == "Alto (25%)")
            {
                qRCodeEncoder.QRCodeErrorCorrect = ThoughtWorks.QRCode.Codec.QRCodeEncoder.ERROR_CORRECTION.Q;
            }
            qRCodeEncoder.QRCodeVersion = 0;

            try
            {
                string[] strArrays = new string[] { "?re=", emisor, "&rr=", receptor, "&tt=", total, "&id=", uuid };
                string str1 = string.Concat(strArrays);
                image = qRCodeEncoder.Encode(str1, Encoding.UTF8);
            }
            catch (Exception)
            {
                throw;
            }
            return image;
        }

        public static Image GetQRRemision(string emisor, string receptor, string total, string folio)
        {
            Image image = null;
            int num = 2;
            string str = num.ToString();
            QRCodeEncoder qRCodeEncoder = new QRCodeEncoder()
            {
                QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE,
                QRCodeScale = num,
                QRCodeErrorCorrect = ThoughtWorks.QRCode.Codec.QRCodeEncoder.ERROR_CORRECTION.Q
            };

            if (str == "Alto (25%)")
            {
                qRCodeEncoder.QRCodeErrorCorrect = ThoughtWorks.QRCode.Codec.QRCodeEncoder.ERROR_CORRECTION.Q;
            }
            qRCodeEncoder.QRCodeVersion = 0;

            try
            {
                string[] strArrays = new string[] { "?re=", emisor, "&rr=", receptor, "&tt=", total, "&folio=", folio };
                string str1 = string.Concat(strArrays);
                image = qRCodeEncoder.Encode(str1, Encoding.UTF8);
            }
            catch (Exception)
            {
                throw;
            }
            return image;
        }

        public static Image GetQRImage(string contenido)
        {
            Image image = null;
            QRCodeEncoder qRCodeEncoder = new QRCodeEncoder()
            {
                QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE,
                QRCodeScale = 2,
                QRCodeErrorCorrect = ThoughtWorks.QRCode.Codec.QRCodeEncoder.ERROR_CORRECTION.Q
            };
            qRCodeEncoder.QRCodeVersion = 0;

            try
            {
                image = qRCodeEncoder.Encode(contenido, Encoding.UTF8);
            }
            catch (Exception)
            {
                throw;
            }
            return image;
        }

        public static byte[] GetQRBytes(string[] contenido)
        {
            return HelperQRCodes.CopyImageToByteArray(HelperQRCodes.GetQRImage(string.Concat(contenido)));
        }

        public static string GetQRBase64(string[] contenido)
        {
            return System.Convert.ToBase64String(GetQRBytes(contenido));
        }

        public static string[] QrDecode(Image img)
        {
            string[] strArray;
            QRCodeDecoder qRCodeDecoder = new QRCodeDecoder();
            QRCodeBitmapImage qRCodeBitmapImage = new QRCodeBitmapImage(new Bitmap(img));
            string empty = string.Empty;
            string[] strArray1 = null;
            try
            {
                if (img.Width == img.Height)
                {
                    string str = qRCodeDecoder.decode(qRCodeBitmapImage);
                    if (str.Length > 0)
                    {
                        str = str.Replace('?', ' ');
                        str = str.TrimStart(new char[0]);
                        char[] chrArray = new char[] { '&' };
                        string[] strArray2 = str.Split(chrArray);
                        if (strArray2.Length == 4)
                        {
                            string str1 = strArray2[0];
                            chrArray = new char[] { '=' };
                            string str2 = str1.Split(chrArray)[1];
                            string str3 = strArray2[1];
                            chrArray = new char[] { '=' };
                            empty = str3.Split(chrArray)[1];
                            string str4 = strArray2[2];
                            chrArray = new char[] { '=' };
                            string str5 = str4.Split(chrArray)[1];
                            chrArray = new char[] { '0' };
                            str5.TrimStart(chrArray);
                            string str6 = strArray2[3];
                            chrArray = new char[] { '=' };
                            string upper = str6.Split(chrArray)[1].Trim().ToUpper();
                            strArray1 = new string[] { str2, empty, upper };
                        }
                    }
                }
                else
                {
                    strArray = null;
                    return strArray;
                }
            }
            catch
            {
            }
            strArray = strArray1;
            return strArray;
        }

        public static Byte[] CopyImageToByteArray(Image theImage)
        {
            try
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    theImage.Save(memoryStream, ImageFormat.Png);
                    return memoryStream.ToArray();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string[] Decodifica(Stream pdfStream)
        {
            string[] strArray;
            if (pdfStream != null)
            {
                strArray = HelperQRCodes.Decodificar(pdfStream);
            }
            else
            {
                strArray = null;
            }
            return strArray;
        }

        public static string[] Decodifica(string pdfFile)
        {
            string[] strArray;
            if (File.Exists(pdfFile))
            {
                Stream stream = null;
                string[] strArray1 = null;
                if (File.Exists(pdfFile))
                {
                    FileStream fileStream = File.OpenRead(pdfFile);
                    stream = fileStream;
                    Stream stream1 = fileStream;
                    try
                    {
                        strArray1 = HelperQRCodes.Decodificar(stream);
                    }
                    finally
                    {
                        if (stream1 != null)
                        {
                            stream1.Dispose();
                        }
                    }
                }
                strArray = strArray1;
            }
            else
            {
                strArray = null;
            }
            return strArray;
        }

        private static string[] Decodificar(Stream pdfstream)
        {
            string[] strArray = null;
            try
            {
                PdfReader pdfReader = new PdfReader(pdfstream);
                PdfObject pdfObject = null;
                PdfStream pdfStream = null;
                for (int i = 0; i <= pdfReader.XrefSize - 1; i++)
                {
                    pdfObject = pdfReader.GetPdfObject(i);
                    if ((pdfObject == null ? false : pdfObject.IsStream()))
                    {
                        pdfStream = (PdfStream)pdfObject;
                        PdfObject pdfObject1 = pdfStream.Get(PdfName.SUBTYPE);
                        if ((pdfObject1 == null ? false : pdfObject1.ToString() == PdfName.IMAGE.ToString()))
                        {
                            try
                            {
                                PdfImageObject pdfImageObject = new PdfImageObject((PRStream)pdfStream);
                                string[] strArray1 = HelperQRCodes.QrDecode(pdfImageObject.GetDrawingImage());
                                if (strArray1 != null)
                                {
                                    if (strArray1.Length > 0)
                                    {
                                        strArray = strArray1;
                                    }
                                }
                                if (strArray1 != null)
                                {
                                    if (strArray[0].Length > 0)
                                    {
                                        break;
                                    }
                                }
                            }
                            catch
                            {
                                string uuid = HelperQRCodes.GetUUID(pdfstream);
                                if (uuid.Length > 0)
                                {
                                    strArray = new string[] { "", "", uuid, "" };
                                }
                            }
                        }
                    }
                }
                pdfReader.Close();
            }
            catch
            {
            }
            return strArray;
        }

        public static string GetUUID(Stream pdfStream)
        {
            string str;
            StringBuilder stringBuilder = new StringBuilder();
            if (pdfStream == null)
            {
                str = "";
            }
            else
            {
                PdfReader pdfReader = new PdfReader(pdfStream);
                for (int i = 1; i <= pdfReader.NumberOfPages; i++)
                {
                    string textFromPage = PdfTextExtractor.GetTextFromPage(pdfReader, i, new SimpleTextExtractionStrategy());
                    textFromPage = Encoding.UTF8.GetString(Encoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(textFromPage)));
                    stringBuilder.Append(textFromPage);
                }
                pdfReader.Close();
                string str1 = Regex.Replace(stringBuilder.ToString(), "[\n\r\t]", "");
                Match match = Regex.Match(str1, "[a-f0-9A-F]{8}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9AF]{12}");
                str = (!match.Success || !(match.Value != "") ? "" : match.Value.ToString().ToUpper());
            }
            return str;
        }

        public static string GetUUID(string filePath)
        {
            string str;
            if (File.Exists(filePath))
            {
                StringBuilder stringBuilder = new StringBuilder();
                if (!File.Exists(filePath))
                {
                    str = "";
                }
                else
                {
                    PdfReader pdfReader = new PdfReader(filePath);
                    if (pdfReader == null)
                    {
                        str = "";
                    }
                    else
                    {
                        for (int i = 1; i <= pdfReader.NumberOfPages; i++)
                        {
                            string textFromPage = PdfTextExtractor.GetTextFromPage(pdfReader, i, new SimpleTextExtractionStrategy());
                            textFromPage = Encoding.UTF8.GetString(Encoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(textFromPage)));
                            stringBuilder.Append(textFromPage);
                        }
                        pdfReader.Close();
                        string str1 = Regex.Replace(stringBuilder.ToString(), "[\n\r\t]", "");
                        Match match = Regex.Match(str1, "[a-f0-9A-F]{8}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9AF]{12}");
                        str = (!match.Success || !(match.Value != "") ? "" : match.Value.ToString().ToUpper());
                    }
                }
            }
            else
            {
                str = "";
            }
            return str;
        }

    }
}
