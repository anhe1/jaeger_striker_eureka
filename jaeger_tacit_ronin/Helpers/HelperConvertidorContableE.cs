﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.Edita.Enums;
using Jaeger.Edita.SAT;
using Jaeger.ContaE.V13;

namespace Jaeger.Helpers
{
    public class HelperConvertidorContableE
    {
        public HelperConvertidorContableE()
        {
        }

        /// <summary>
        /// claves para el tipo de documentos
        /// </summary>
        public static List<ClaveContable> Claves()
        {
            List<ClaveContable> lista = new List<ClaveContable>();
            lista.Add(new ClaveContable { Clave = "CT", Descripcion = "Catálogo de Cuentas"});
            //Me.listaTipoClave.Add(New ContableClave With {.Clave = "PL", .Nombre = "Polizas del Período"})
            lista.Add(new ClaveContable { Clave = "BN", Descripcion = "Balanza Normal"});
            lista.Add(new ClaveContable { Clave = "BC", Descripcion = "Balanza Complementaria" });
            //Me.listaTipoClave.Add(New ContableClave With {.Clave = "XC", .Nombre = "Auxiliar de Cuentas"})
            //Me.listaTipoClave.Add(New ContableClave With {.Clave = "XF", .Nombre = "Auxiliar de Folio"})
            return lista;
        }

        public static List<ContableTipoEnvio> TiposDeEnvio()
        {
            List<ContableTipoEnvio> lista = new List<ContableTipoEnvio>();
            lista.Add(new ContableTipoEnvio { Clave = "N", Descripcion = "Normal"});
            lista.Add(new ContableTipoEnvio { Clave = "C", Descripcion = "Complementaria" });
            return lista;
        }

        public static List<ContableTipoSolicitud> TiposDeSolicitud()
        {
            List<ContableTipoSolicitud> lista = new List<ContableTipoSolicitud>();
            lista.Add(new ContableTipoSolicitud { Clave = "AF", Descripcion = "Acto de Fiscalización"});
            lista.Add(new ContableTipoSolicitud { Clave = "FC", Descripcion = "Fiscalización Compulsa"});
            lista.Add(new ContableTipoSolicitud { Clave = "DE", Descripcion = "Devolución"});
            lista.Add(new ContableTipoSolicitud { Clave = "CO", Descripcion = "Compensación" });
            return lista;
        }

        public Balanza Balanza(ContableLayout objeto)
        {
            Balanza balanzaV13 = new Balanza();
            balanzaV13.RFC = objeto.Rfc;
            balanzaV13.Anio = objeto.Ejercicio;
            balanzaV13.Mes = Enum.GetName(typeof(EnumMonthsOfYear), objeto.Periodo);
            balanzaV13.TipoEnvio = objeto.TipoEnvio.Clave;
            balanzaV13.FechaModBal = objeto.Fecha;
            balanzaV13.FechaModBalSpecified = (balanzaV13.TipoEnvio != "N" ? true : false);

            return balanzaV13;
        }
    }
}
