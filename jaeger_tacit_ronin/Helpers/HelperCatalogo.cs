﻿// develop: 2005201723449 
// purpose: Con esta clase realizamos las tareas comunes para los catálogos.
// rev.: 030620171228: agregamos funciones para la resolucion de las rutas.
using System;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Reflection;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using Jaeger.Enums;

namespace Jaeger.Helpers
{
    public class HelperCatalogo : HelperSerialize, INotifyPropertyChanged
	{
        private string versionField;
        private string revisionField;
        private DateTime? fechaInicioVigenciaField;
        private bool fechaInicioVigenciaFieldSpecified;
        private DateTime? fechaFinVigenciaField;
        private bool fechaFinVigenciaFieldSpecified;
        private string startPathField;

        public HelperCatalogo()
        {
            this.startPathField = "C:\\Jaeger\\Jaeger.Catalogos";
        }

        [XmlIgnore]
        [JsonIgnore]
        public string StartPath
        {
            get
            {
                return this.startPathField;
            }
            set
            {
                this.startPathField = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttributeAttribute("version")]
        [JsonProperty("ver")]
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttributeAttribute("revision")]
        [JsonProperty("revision")]
        public string Revision
        {
            get
            {
                return this.revisionField;
            }
            set
            {
                this.revisionField = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [XmlIgnore]
        [JsonIgnore]
        public bool InicioVigenciaSpecified
        {
            get
            {
                return this.fechaInicioVigenciaFieldSpecified;
            }
            set
            {
                this.fechaInicioVigenciaFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [XmlAttribute("iniVigencia")]
        [JsonIgnore]
        public string InicioVigenciaX
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaInicioVigenciaField >= firstGoodDate)
                {
                    return this.fechaInicioVigenciaField.Value.ToString("yyyy-MM-dd");
                }
                else
                {
                    return null;
                }
            }
            set
            {
                this.fechaInicioVigenciaField = Convert.ToDateTime(value);
                this.OnPropertyChanged();
            }
        }

        [Description("Fecha inicio de vigencia")]
        [DisplayName("Fecha inicio de vigencia")]
        [XmlIgnore]
        [JsonIgnore]
        public DateTime? InicioVigencia
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaInicioVigenciaField >= firstGoodDate)
                {
                    return this.fechaInicioVigenciaField;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                this.fechaInicioVigenciaField = value;
                this.fechaInicioVigenciaFieldSpecified = true;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [XmlIgnore]
        [JsonIgnore]
        public bool FinVigenciaSpecified
        {
            get
            {
                return this.fechaFinVigenciaFieldSpecified;
            }
            set
            {
                this.fechaFinVigenciaFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [XmlAttribute("finVigencia")]
        [JsonIgnore]
        public string FinVigenciaX
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaFinVigenciaField >= firstGoodDate)
                {
                    return this.fechaFinVigenciaField.Value.ToString("yyyy-MM-dd");
                }
                else
                {
                    return null;
                }
            }
            set
            {
                this.fechaFinVigenciaField = Convert.ToDateTime(value);
                this.OnPropertyChanged();
            }
        }

        [Description("Fecha fin de vigencia")]
        [DisplayName("Fecha fin de vigencia")]
        [XmlIgnore]
        [JsonIgnore]
        public DateTime? FinVigencia
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaFinVigenciaField >= firstGoodDate)
                {
                    return this.fechaFinVigenciaField;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                this.fechaFinVigenciaField = value;
                this.fechaFinVigenciaFieldSpecified = true;
                this.OnPropertyChanged();
            }
        }

        [XmlIgnore()]
        [JsonIgnore]
		string searchItem = string.Empty;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler propertyChangedEventHandler = this.PropertyChanged;
            if (propertyChangedEventHandler != null)
            {
                propertyChangedEventHandler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged<T>(Expression<Func<T>> propertyExpression)
        {
            this.OnPropertyChanged(((MemberExpression)propertyExpression.Body).Member.Name);
        }

		/// <summary>
		/// Cargar un objeto
		/// </summary>
		/// <typeparam name="T">tipo de objeto</typeparam>
		/// <param name="nameFile">nombre del archivo</param>
		/// <returns>retorna clase del objeto referenciado desde el archivo</returns>
		protected internal T LoadObject<T>(string nameFile)
		{
			object oCatalog = new object();

			if (File.Exists(nameFile) == false) 
            {
                HelperTacitLog.LogWrite(new Jaeger.Entities.Property
                {
                    Type = EnumPropertyType.Error,
                    Code = "-1",
                    Name = "ERROR",
                    Value = "No se encontro el archivo de catálogo"
                });
				return default(T);
			}

			StreamReader oStreamReader = new StreamReader(nameFile);
			try {
				oCatalog = HelperSerialize.DeserializeObject<T>(oStreamReader.ReadToEnd());
			} catch (Exception ex) {
                HelperTacitLog.LogWrite(new Jaeger.Entities.Property
                {
                    Type = EnumPropertyType.Error,
                    Code = "001",
                    Name = "Error",
                    Value = string.Concat("helper_catalog_load_object: ", ex.Message)
                });
			} finally {
				oStreamReader.Close();
			}
			return (T)oCatalog;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T">Tipo de objeto</typeparam>
		/// <param name="nameFile">nombre del archivo</param>
		/// <param name="objeto"></param>
		protected internal bool SaveObject<T>(string nameFile, object objeto)
		{
			bool bResult = false;
			System.Text.UTF8Encoding oUtf8Encoding = new System.Text.UTF8Encoding(false);
			StreamWriter oStreamWriter = new StreamWriter(nameFile, false, oUtf8Encoding);
			StringWriter oStringWriter = new StringWriter();
			string oXmlString = HelperSerialize.SerializeObject<T>((T)objeto);
			try {
				oStringWriter.Write(oXmlString);
				oStreamWriter.Write(oStringWriter);
				bResult = true;
			} catch (System.Exception exception) {
                HelperTacitLog.LogWrite(new Jaeger.Entities.Property
                {
                    Type = EnumPropertyType.Error,
                    Code = "001",
                    Name = "Error",
                    Value = string.Concat("helper_catalog_load_object: ", "No es posible crear el documento de catálogo. ", exception.Message)
                });
				bResult = false;
			}
			oStreamWriter.Close();
			oStringWriter = null;
			return bResult;
		}

		/// <summary>
		/// recuperar recurso
		/// </summary>
		/// <param name="nameResource"></param>
		/// <param name="fileName"></param>
		public bool GetResource(string nameResource, string fileName)
		{
			// sino existe la carpeta la creamos
			if (!(Directory.Exists(Path.GetDirectoryName(fileName)))) 
            {
				Directory.CreateDirectory(Path.GetDirectoryName(fileName));
			}

			using (Stream oStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Concat("Jaeger.","Catalogos.", nameResource))) 
            {
				FileStream oFileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
				oStream.CopyTo(oFileStream);
				oFileStream.Close();
			}

			return File.Exists(fileName);
		}

		/// <summary>
		/// solo para resolver el nombre y la ruta del archivo
		/// </summary>
		/// <param name="fileName">nombre del archivo</param>
		private string ResolverName(string fileName)
		{
			return Path.Combine(this.startPathField, fileName);
		}

		/// <summary>
		/// Resolver el nombre del archivo que se va a utilizar ya sea que por default o el que se indica
		/// </summary>
		/// <param name="fileName">nombre del archivo</param>
		/// <param name="fileDefault">nombre del archivo del recurso incrustado</param>
		/// <param name="resource">si debe tomarlo o no de los recursos</param>
		public string ResolverName(string fileName, string fileDefault, bool resource = true)
		{
			string localName = fileName;
			if (File.Exists(fileName) == false) 
            {
				if (resource) 
                {
					if (File.Exists(this.ResolverName(fileDefault)) == false) 
                    {
						if (this.GetResource(fileDefault, this.ResolverName(fileDefault))) 
                        {
							localName = this.ResolverName(fileDefault);
						}
					} else 
                    {
						localName = this.ResolverName(fileDefault);
					}
				}
			}
			return localName;
		}

	}
}
