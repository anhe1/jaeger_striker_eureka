﻿using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using Jaeger.Edita.V2.Nomina.Entities;
using Jaeger.Edita.V2.Nomina.Layout;
using OfficeOpenXml;
using System.Data;
using Jaeger.Domain.Services.Mapping;
using System.Text.RegularExpressions;

namespace Jaeger.Helpers
{
    public class LayoutExcelNomina12
    {
        private FileInfo fileInfo;
        private ExcelPackage excelPackage;

        public LayoutExcelNomina12()
        {
            this.excelPackage = new ExcelPackage();
        }

        public string ExcelFile
        {
            get
            {
                return this.fileInfo.FullName;
            }
            set
            {
                this.fileInfo = new FileInfo(value);
            }
        }

        public bool Load()
        {
            return false;
        }

        public List<Entities.Layout.ReciboNominaLayout> Reader()
        {
            DataTable datos = this.GetDataTableFromExcel("", true);
            DataNamesMapper<Entities.Layout.ReciboNominaLayout> mapper = new DataNamesMapper<Entities.Layout.ReciboNominaLayout>();
            return mapper.Map(datos).ToList();
        }

        public DataTable GetDataTableFromExcel(string hoja, bool hasHeader = true)
        {
            using (ExcelPackage package = new ExcelPackage(this.fileInfo))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                DataTable tbl = new DataTable();
                int startRow = 1;

                // nombres de los encabezados
                foreach (var firstRowCell in worksheet.Cells[1, 1, 1, worksheet.Dimension.End.Column])
                {
                    if (hasHeader)
                    {
                        tbl.Columns.Add(firstRowCell.Text);
                        startRow = 2;
                    }
                    else
                    {
                        tbl.Columns.Add(string.Format("Column {0}", firstRowCell.Start.Column));
                        startRow = 1;
                    }
                }

                for (int rowNum = startRow; rowNum <= worksheet.Dimension.End.Row; rowNum++)
                {
                    ExcelRange wsRow = worksheet.Cells[rowNum, 1, rowNum, worksheet.Dimension.End.Column];
                    DataRow row = tbl.Rows.Add();
                    foreach (var cell in wsRow)
                    {
                        row[cell.Start.Column - 1] = cell.Text;
                    }
                }
                return tbl;
            }
        }

        public BindingList<NominaLayOutEmpleado> LoadNomina(string hoja)
        {
            BindingList<NominaLayOutEmpleado> recibosField = new BindingList<NominaLayOutEmpleado>();
            DataTable dataTable = GetDataTableFromExcel1(hoja, true);
            if (!(dataTable == null))
            {
                if ((dataTable.Rows.Count > 0))
                {
                    foreach (DataRow fila in dataTable.Rows)
                    {
                        NominaLayOutEmpleado item = new NominaLayOutEmpleado();
                        foreach (DataColumn columna in fila.Table.Columns)
                        {
                            switch (columna.ColumnName.ToString().ToUpper())
                            {
                                case "REGISTROPATRONAL":
                                        item.RegistroPatronal = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "TIPONOMINA":
                                        item.TipoNomina = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "RFCRECEPTOR":
                                        item.ReceptorRFC = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "FECHAPAGO":
                                        item.FechaPago = DbConvert.ConvertDateTime(fila[columna.ColumnName]);
                                        break;
                                case "FECHAINICIALPAGO":
                                        item.FechaInicialPago = DbConvert.ConvertDateTime(fila[columna.ColumnName]);
                                        break;
                                case "FECHAFINALPAGO":
                                        item.FechaFinalPago = DbConvert.ConvertDateTime(fila[columna.ColumnName]);
                                        break;
                                case "NUMDIASPAGADOS":
                                        item.NumDiasPagados = DbConvert.ConvertInt32(fila[columna.ColumnName]);
                                        break;
                                case "LUGAREXPEDICION":
                                        item.LugarExpedicion = DbConvert.ConvertString(fila[columna.ColumnName]).Trim();
                                        break;
                                case "NOMBRERECEPTOR":
                                        item.NombreReceptor = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "CORREO":
                                        item.Correo = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "CURPRECEPTOR":
                                        item.ReceptorRFC = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "NUMSEGURIDADSOCIAL":
                                        item.NumSeguridadSocial = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "FECHAINICIORELLABORAL":
                                        item.FechaInicioRelLaboral = DbConvert.ConvertDateTime(fila[columna.ColumnName]);
                                        break;
                                case "ANTIGÜEDAD":
                                        item.Antiguedad = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "TIPOCONTRATO":
                                        item.TipoContrato = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "SINDICALIZADO":
                                        item.Sindicalizado = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "TIPOJORNADA":
                                        item.TipoJornada = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "TIPOREGIMEN":
                                        item.TipoRegimen = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "NUMEMPLEADO":
                                        item.NumEmpleado = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "DEPARTAMENTO":
                                        item.Departamento = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "PUESTO":
                                        item.Puesto = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "RIESGOPUESTO":
                                        item.RiesgoPuesto = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "PERIODICIDADPAGO":
                                        item.PeriodicidadPago = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "BANCO":
                                        item.ClaveBanco = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "CUENTABANCARIA":
                                        item.CuentaBancaria = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "SALARIOBASECOTAPOR":
                                        item.SalarioBaseCotApor = DbConvert.ConvertDecimal(fila[columna.ColumnName]);
                                        break;
                                case "SALARIODIARIOINTEGRADO":
                                        item.SalarioDiarioIntegrado = DbConvert.ConvertDecimal(fila[columna.ColumnName]);
                                        break;
                                case "CLAVEENTFED":
                                        item.ClaveEntFed = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "SUBSIDIOALEMPLEO":
                                        item.SubsidioAlEmpleo = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "SUBSIDIOCAUSADO":
                                        item.SubsidioAlEmpleo = DbConvert.ConvertString(fila[columna.ColumnName]);
                                        break;
                                case "DIASINCAPACIDAD":
                                        item.DiasIncapacidad = DbConvert.ConvertDecimal(fila[columna.ColumnName]);
                                        break;
                                case "TIPOINCAPACIDAD":
                                        item.DiasIncapacidad = DbConvert.ConvertDecimal(fila[columna.ColumnName]);
                                        break;
                                case "IMPORTEMONETARIO":
                                        item.ImporteMonetario = DbConvert.ConvertDecimal(fila[columna.ColumnName]);
                                        break;
                                default:
                                    {
                                        if (columna.ColumnName.ToUpper().Contains("ELEMENTO"))
                                        {
                                            var codigo = Regex.Replace(columna.ColumnName, @"[^\d]", "");
                                            NominaElemento item2 = new NominaElemento();
                                            string clave1 = string.Concat("clave", codigo);
                                            item2.Clave = DbConvert.ConvertString(fila[clave1]);
                                            item2.Tipo = DbConvert.ConvertString(fila[string.Concat("tipo", codigo)]);
                                            item2.Concepto = DbConvert.ConvertString(fila[string.Concat("CONCEPTO", codigo)]);
                                            item2.ImporteGravado = DbConvert.ConvertDecimal(fila[string.Concat("gravado", codigo)]);
                                            item2.ImporteExento = DbConvert.ConvertDecimal(fila[string.Concat("exento", codigo)]);
                                            //item2.ElementoText = DbConvert.ConvertString(fila[string.Concat("elemento", codigo)]);
                                            if (DbConvert.ConvertString(fila[string.Concat("elemento", codigo)]).Trim() == "1")
                                            {
                                                item2.Elemento = Jaeger.Edita.V2.Nomina.Enums.EnumNominaElemento.Percepcion;
                                            }
                                            else if (DbConvert.ConvertString(fila[string.Concat("elemento", codigo)]).Trim() == "2")
                                            {
                                                item2.Elemento = Jaeger.Edita.V2.Nomina.Enums.EnumNominaElemento.Deduccion;
                                            }
                                            else if (DbConvert.ConvertString(fila[string.Concat("elemento", codigo)]).Trim() == "3")
                                            {
                                                item2.Elemento = Jaeger.Edita.V2.Nomina.Enums.EnumNominaElemento.HorasExtra;
                                            }
                                            else
                                            {
                                                item2.Elemento = Jaeger.Edita.V2.Nomina.Enums.EnumNominaElemento.None;
                                            }

                                            if (item2.Elemento > 0)
                                                item.Items.Add(item2);
                                        }

                                        break;
                                    }
                            }
                        }
                        recibosField.Add(item);
                    }
                }
            }
            return recibosField;
        }

        private DataTable GetDataTableFromExcel1(string hoja, bool hasHeader = true)
        {
            FileInfo existingFile = new FileInfo(this.fileInfo.FullName);
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                DataTable tbl = new DataTable();
                int startRow = 1;

                foreach (var firstRowCell in worksheet.Cells[1, 1, 1, worksheet.Dimension.End.Column])
                {
                    if (hasHeader)
                    {
                        tbl.Columns.Add(firstRowCell.Text);
                        startRow = 2;
                    }
                    else
                    {
                        tbl.Columns.Add(string.Format("Column {0}", firstRowCell.Start.Column));
                        startRow = 1;
                    }
                }

                for (int rowNum = startRow; rowNum <= worksheet.Dimension.End.Row; rowNum++)
                {
                    ExcelRange wsRow = worksheet.Cells[rowNum, 1, rowNum, worksheet.Dimension.End.Column];
                    DataRow row = tbl.Rows.Add();
                    foreach (var cell in wsRow)
                    {
                        row[cell.Start.Column - 1] = cell.Text;
                    }
                }
                return tbl;
            }
        }
    }
}
