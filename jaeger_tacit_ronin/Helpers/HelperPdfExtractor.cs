﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace Jaeger.Helpers
{
    public class HelperPdfExtractor
    {
        public HelperPdfExtractor()
        {
        }

        public static string ReadPdfFile(string fileName)
        {
            PdfReader reader2 = new PdfReader(fileName);
            string stringReturn = string.Empty;
            
            for (int page = 1; page <= reader2.NumberOfPages; page = checked(page + 1))
            {
                ITextExtractionStrategy its = new SimpleTextExtractionStrategy();
                PdfReader reader = new PdfReader(fileName);
                string s = PdfTextExtractor.GetTextFromPage(reader, page, its);
                s = Encoding.UTF8.GetString(Encoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(s)));
                stringReturn = string.Concat(stringReturn, s);
                reader.Close();
            }

            MatchCollection guids = Regex.Matches(stringReturn, "(\\{){0,1}[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}(\\}){0,1}");
            int count = checked(guids.Count - 1);
            for (int i = 0; i <= count; i = checked(i + 1))
            {
                stringReturn = guids[i].Value;
            }

            return stringReturn;
        }

        public static string GetUUID(string fileName)
        {
            string uuidFound;
            StringBuilder stringBuilder = new StringBuilder();
            if (File.Exists(fileName))
            {
                PdfReader pdfReader = new PdfReader(fileName);
                if (pdfReader != null)
                {
                    for (int num = 1; num <= pdfReader.NumberOfPages; num = checked(num + 1))
                    {
                        string textFromPage = PdfTextExtractor.GetTextFromPage(pdfReader, num, new SimpleTextExtractionStrategy());
                        textFromPage = Encoding.UTF8.GetString(Encoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(textFromPage)));
                        stringBuilder.Append(textFromPage);
                    }
                    pdfReader.Close();
                    string str1 = Regex.Replace(stringBuilder.ToString(), "[\r\n\t]", "");
                    Match match = Regex.Match(str1, "[a-f0-9A-F]{8}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9AF]{12}", RegexOptions.IgnoreCase);
                    uuidFound = (!match.Success || !(match.Value != "") ? "" : match.Value.ToString().ToUpper());
                }
                else
                {
                    uuidFound = string.Empty;
                }
            }
            else
            {
                uuidFound = string.Empty;
            }
            return uuidFound;
        }
    }
}
