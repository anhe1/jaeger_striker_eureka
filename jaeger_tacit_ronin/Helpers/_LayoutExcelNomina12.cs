﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;

namespace Jaeger.Helpers
{
    public class LayoutExcelNomina12
    {
        private FileInfo fileInfo;
        private ExcelPackage package;
        private ExcelWorksheets excelWorksheets;

        public class ExcelInfo
        {
            public int Index { get; set; }
            public string Name { get; set; }

            public ExcelInfo(int index, string name)
            {
                this.Index = index;
                this.Name = name;
            }
        }

        public LayoutExcelNomina12()
        {
            this.Sheets = new List<ExcelInfo>();
        }

        #region propiedadess

        public string ExcelFile
        {
            get
            {
                return this.fileInfo.FullName;
            }
            set
            {
                this.fileInfo = new FileInfo(value);
            }
        }

        public List<ExcelInfo> Sheets { get; set; }

        #endregion

        public bool Load()
        {
            this.package = new ExcelPackage(this.fileInfo);

            foreach (var item in package.Workbook.Worksheets)
            {
                Console.WriteLine(item.Index + " " + item.Name);
                this.Sheets.Add(new ExcelInfo(item.Index, item.Name));
            }

            return false;
        }

        public bool Reader()
        {
            ExcelWorksheet currentSheet = this.package.Workbook.Worksheets[1] as ExcelWorksheet;
            List<Entities.Layout.ReciboNominaLayout> lista = new List<Entities.Layout.ReciboNominaLayout>();
            int filas = 0;
            int columnas = 0;
            if (currentSheet != null)
            {
                for (int row = 2; row < currentSheet.Dimension.Rows; row++)
                {
                    if (currentSheet.Cells[row, 1].Value != null)
                    {
                        Entities.Layout.ReciboNominaLayout recibo = new Entities.Layout.ReciboNominaLayout();
                        filas++;
                        columnas = 1;
                        for (int col = 1; col < currentSheet.Dimension.Columns; col++)
                        {
                            if (currentSheet.Cells[1, col].Value != null)
                            {
                                if (currentSheet.Cells[1, col].Value.ToString().Trim().Length > 0)
                                {
                                    string switch_on = currentSheet.Cells[1, col].Value.ToString();
                                    object currentValue = currentSheet.Cells[row, col].Value;
                                    switch (switch_on)
                                    {
                                        case "RegistroPatronal":
                                            recibo.RegistroPatronal = currentValue.ToString();
                                            break;
                                        case "TipoNomina":
                                            recibo.TipoNomina = currentValue.ToString();
                                            break;
                                        case "FechaPago":
                                            recibo.FechaPago = DateTime.Parse(currentValue.ToString());
                                            break;
                                        case "FechaInicialPago":
                                            recibo.FechaInicialPago = DateTime.Parse(currentValue.ToString());
                                            break;
                                        case "FechaFinalPago":
                                            recibo.FechaFinalPago = DateTime.Parse(currentValue.ToString());
                                            break;
                                        case "NumDiasPagados":
                                            recibo.NumDiasPagados = int.Parse(currentValue.ToString());
                                            break;
                                        case "LugarExpedicion":
                                            recibo.LugarExpedicion = currentValue.ToString();
                                            break;
                                        case "RfcReceptor":
                                            
                                            break;
                                        case "NombreReceptor":
                                            break;
                                        case "Correo":
                                            break;
                                        case "CurpReceptor":
                                            break;
                                        case "NumSeguridadSocial":
                                            break;
                                        case "FechaInicioRelLaboral":
                                            break;
                                        case "Antigüedad":
                                            break;
                                        case "TipoContrato":
                                            break;
                                        case "TipoJornada":
                                            break;
                                        case "TipoRegimen":
                                            break;
                                        case "NumEmpleado":
                                            break;
                                        case "Departamento":
                                            break;
                                        case "Puesto":
                                            break;
                                        case "RiesgoPuesto":
                                            break;
                                        case "PeriodicidadPago":
                                            break;
                                        case "Banco":
                                            break;
                                        case "CuentaBancaria":
                                            break;
                                        case "SalarioBaseCotApor":
                                            break;
                                        case "SalarioDiarioIntegrado":
                                            break;
                                            //ClaveEntFed SubsidioCausado DiasIncapacidad TipoIncapacidad ImporteMonetario    TipoRelacion UUIDRelacionado elemento1 tipo1   clave1 concepto1   gravado1 exento1 elemento2 tipo2   clave2 concepto2   gravado2 exento2 elemento3 tipo3   clave3 concepto3   gravado3 exento3 elemento4 tipo4   clave4 concepto4   gravado4 exento4 elemento5 tipo5   clave5 concepto5   gravado5 exento5 elemento6 tipo6   clave6 concepto6   gravado6 exento6 elemento7 tipo7   clave7 concepto7   gravado7 exento7 elemento8 tipo8   clave8 concepto8   gravado8 exento8 elemento9 tipo9   clave9 concepto9   gravado9 exento9 elemento10 tipo10  clave10 concepto10  gravado10 exento10    elemento11 tipo11  clave11 concepto11  gravado11 exento11    elemento12 tipo12  clave12 concepto12  gravado12 exento12    elemento13 tipo13  clave13 concepto13  gravado13 exento13    elemento14 tipo14  clave14 concepto14  gravado14 exento14    elemento15 tipo15  clave15 concepto15  gravado15 exento15    elemento16 tipo16  clave16 concepto16  gravado16 exento16    elemento17 tipo17  clave17 concepto17  gravado17 exento17

                                    }
                                    if (currentSheet.Cells[1, col].ToString().Contains("element"))
                                        Console.WriteLine(currentSheet.Cells[row, col].Value);
                                    columnas++;
                                }
                            }
                        }
                        lista.Add(recibo);
                    }
                }
                Console.WriteLine(string.Format("{0} {1} {2}", filas, columnas, lista.Count));
                return true;
            }

            return false;
        }

        public DataTable GetDataTableFromExcel(string hoja, bool hasHeader = true)
        {
            
            using (ExcelPackage package = new ExcelPackage(this.fileInfo))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                DataTable tbl = new DataTable();
                int startRow = 1;

                foreach (var firstRowCell in worksheet.Cells[1, 1, 1, worksheet.Dimension.End.Column])
                {
                    if (hasHeader)
                    {
                        tbl.Columns.Add(firstRowCell.Text);
                        startRow = 2;
                    }
                    else
                    {
                        tbl.Columns.Add(String.Format("Column {0}", firstRowCell.Start.Column));
                        startRow = 1;
                    }
                }

                for (int rowNum = startRow; rowNum <= worksheet.Dimension.End.Row; rowNum++)
                {
                    ExcelRange wsRow = worksheet.Cells[rowNum, 1, rowNum, worksheet.Dimension.End.Column];
                    DataRow row = tbl.Rows.Add();
                    foreach (var cell in wsRow)
                    {
                        try
                        {

                        row[cell.Start.Column - 1] = cell.Text;
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
                return tbl;
            }
        }
    }
}
