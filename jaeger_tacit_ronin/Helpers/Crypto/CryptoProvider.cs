namespace Jaeger.Helpers.Crypto
{
    /// <summary>
    /// Proveedores del Servicio de criptografía.
    /// </summary>
    public enum CryptoProvider
    {
        DES,
        TripleDES,
        RC2,
        Rijndael
    }
}