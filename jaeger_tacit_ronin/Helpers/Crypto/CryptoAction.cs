namespace Jaeger.Helpers.Crypto
{
    /// <summary>
    /// Encripción / Desencripción.
    /// </summary>
    public enum CryptoAction
    {
        Encrypt,
        Desencrypt
    }
}