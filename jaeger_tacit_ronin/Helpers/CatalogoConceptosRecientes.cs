﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Jaeger.Edita.V2.CFDI.Entities;
// d.Items.Add(new ViewModelComprobanteConcepto { ClaveUnidad = "H87", ClaveProdServ = "01010101", Descripcion = "No existe en el catálogo" });
namespace Jaeger.Helpers
{
    [JsonObject]
    public class CatalogoConceptosRecientes : CatalogoContext<ViewModelComprobanteConcepto>
    {
        public static string NombreArchivo = "CatalogoProductoServiciosRecientes.json";

        public CatalogoConceptosRecientes()
        {
            this.Version = "2.0";
            this.StartPath = Jaeger.Edita.Enums.EnumPaths.Temporal;
            this.Title = "Catalogo de conceptos recientes de CFDI";
            this.FileName = "CatalogoProductoServiciosRecientes.json";
        }

        /// <summary>
        /// buscar un producto por la clave
        /// </summary>
        public ViewModelComprobanteConcepto Search(string findId)
        {
            ViewModelComprobanteConcepto objeto = new ViewModelComprobanteConcepto();
            objeto = this.Items.FirstOrDefault<ViewModelComprobanteConcepto>((ViewModelComprobanteConcepto p) => p.ClaveProdServ == findId);
            return objeto;
        }
    }
}
