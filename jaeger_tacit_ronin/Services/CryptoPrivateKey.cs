﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace Jaeger.Services
{
    public class CryptoPrivateKey
    {
        
        private string mensajeErrorField;

        private int codigoErrorField;

        private string tipoCertificadoField;

        private string moduloField;

        private RSACryptoServiceProvider llaveRsaField;

        private byte[] bufferLlaveField;

        public int CodigoDeError
        {
            get
            {
                return this.codigoErrorField;
            }
        }

        public RSACryptoServiceProvider LlavePrivadaRsa
        {
            get
            {
                return this.llaveRsaField;
            }
        }

        public string MensajeDeError
        {
            get
            {
                return this.mensajeErrorField;
            }
        }

        public string Modulus
        {
            get
            {
                return this.moduloField;
            }
        }

        public string TipoCertificado
        {
            get
            {
                return this.tipoCertificadoField;
            }
        }

        public CryptoPrivateKey()
        {
            this.Inicializar();
        }

        public CryptoPrivateKey(string rutaLlave, string contrasenia)
        {
            this.Inicializar();
            this.CargarLlave(rutaLlave, contrasenia);
        }

        public void CargarLlave(string rutaLlave, string contraseniaLlave)
        {
            if (rutaLlave == "")
            {
                this.mensajeErrorField = "La ruta de la llave privada no puede ser vacía.";
                this.codigoErrorField = 121;
            }
            else if (contraseniaLlave == "")
            {
                this.mensajeErrorField = "La contraseña de la llave privada no puede ser vacía.";
                this.codigoErrorField = 122;
            }
            else if (File.Exists(rutaLlave))
            {
                try
                {
                    byte[] numArray = File.ReadAllBytes(rutaLlave);
                    if (numArray != null)
                    {
                        this.CargarLlave1(numArray, contraseniaLlave);
                    }
                    else
                    {
                        this.mensajeErrorField = "Archivo de llave vacío.";
                        this.codigoErrorField = 121;
                        return;
                    }
                }
                catch (Exception exception)
                {
                    this.mensajeErrorField = string.Concat("Error al leer el archivo de llave. ", exception.Message);
                    this.codigoErrorField = 521;
                    return;
                }
            }
            else
            {
                this.mensajeErrorField = "Ruta de archivo de llave privada inválida o no existe.";
                this.codigoErrorField = 221;
            }
        }

        public void CargarLlave1(byte[] keyblob, string contraseniaLlave)
        {
            if (keyblob == null)
            {
                this.mensajeErrorField = "El arreglo de bytes de la llave privada no puede ser nulo.";
                this.codigoErrorField = 123;
            }
            else if (!(contraseniaLlave == ""))
            {
                RSACryptoServiceProvider rSACryptoServiceProvider = null;
                rSACryptoServiceProvider = this.DecodeX509PublicKey(keyblob);
                if (rSACryptoServiceProvider == null)
                {
                    rSACryptoServiceProvider = this.DecodeRsaPrivateKey(keyblob);
                    if (rSACryptoServiceProvider == null)
                    {
                        rSACryptoServiceProvider = this.DecodePrivateKeyInfo(keyblob);
                        if (rSACryptoServiceProvider == null)
                        {
                            SecureString secureString = this.ToSecureString(contraseniaLlave);
                            if (secureString != null)
                            {
                                rSACryptoServiceProvider = this.DecodeEncryptedPrivateKeyInfo(keyblob, secureString);
                                if (rSACryptoServiceProvider != null)
                                {
                                    this.llaveRsaField = rSACryptoServiceProvider;
                                    this.tipoCertificadoField = "Llave privada PKCS #8 encriptada";
                                }
                                else if (this.mensajeErrorField == "")
                                {
                                    this.mensajeErrorField = "Llave privada incorrecta o dañada.";
                                    this.codigoErrorField = 321;
                                }
                            }
                            else
                            {
                                this.mensajeErrorField = "La contraseña de la llave no puede ser vacía.";
                                this.codigoErrorField = 122;
                            }
                        }
                        else
                        {
                            this.llaveRsaField = rSACryptoServiceProvider;
                            this.tipoCertificadoField = "Llave privada PKCS #8 sin encriptación";
                        }
                    }
                    else
                    {
                        this.llaveRsaField = rSACryptoServiceProvider;
                        this.tipoCertificadoField = "Llave privada RSA";
                    }
                }
                else
                {
                    this.llaveRsaField = rSACryptoServiceProvider;
                    this.tipoCertificadoField = "Llave pública X509";
                }
            }
            else
            {
                this.mensajeErrorField = "La contraseña de la llave privada no puede ser vacía.";
                this.codigoErrorField = 122;
            }
        }

        public void CargarLlaveDeB64(string llavePrivadaB64, string contraseniaLlave)
        {
            if (llavePrivadaB64 == "")
            {
                this.mensajeErrorField = "La cadena de la llave privada en base 64 no puede ser vacía.";
                this.codigoErrorField = 124;
            }
            else if (!(contraseniaLlave == ""))
            {
                try
                {
                    byte[] numArray = Convert.FromBase64String(llavePrivadaB64);
                    if (numArray != null)
                    {
                        this.CargarLlave1(numArray, contraseniaLlave);
                    }
                    else
                    {
                        this.mensajeErrorField = "Archivo de llave vacío.";
                        this.codigoErrorField = 124;
                        return;
                    }
                }
                catch (Exception exception)
                {
                    this.mensajeErrorField = string.Concat("Error al leer el archivo de llave. ", exception.Message);
                    this.codigoErrorField = 521;
                    return;
                }
            }
            else
            {
                this.mensajeErrorField = "La contraseña de la llave privada no puede ser vacía.";
                this.codigoErrorField = 122;
            }
        }

        private bool CompareBytearrays(byte[] a, byte[] b)
        {
            bool flag;
            if (a.Length == b.Length)
            {
                int num = 0;
                byte[] numArray = a;
                int num1 = 0;
                while (num1 < numArray.Length)
                {
                    if (numArray[num1] == b[num])
                    {
                        num++;
                        num1++;
                    }
                    else
                    {
                        flag = false;
                        return flag;
                    }
                }
                flag = true;
            }
            else
            {
                flag = false;
            }
            return flag;
        }

        private RSACryptoServiceProvider DecodeEncryptedPrivateKeyInfo(byte[] encpkcs8, SecureString pass)
        {
            int num;
            int num1;
            RSACryptoServiceProvider rSACryptoServiceProvider;
            if (pass != null)
            {
                byte[] numArray = new byte[] { 6, 9, 42, 134, 72, 134, 247, 13, 1, 5, 13 };
                byte[] numArray1 = new byte[] { 6, 9, 42, 134, 72, 134, 247, 13, 1, 5, 12 };
                byte[] numArray2 = new byte[] { 6, 8, 42, 134, 72, 134, 247, 13, 3, 7 };
                byte[] numArray3 = new byte[10];
                //byte[] numArray4 = new byte[11];
                bool flag = false;
                MemoryStream memoryStream = new MemoryStream(encpkcs8);
                //int length = (int)memoryStream.Length;
                BinaryReader binaryReader = new BinaryReader(memoryStream);
                byte num2 = 0;
                ushort num3 = 0;
                try
                {
                    try
                    {
                        num3 = binaryReader.ReadUInt16();
                        if (num3 == 33072)
                        {
                            binaryReader.ReadByte();
                        }
                        else if (num3 != 33328)
                        {
                            rSACryptoServiceProvider = null;
                            return rSACryptoServiceProvider;
                        }
                        else
                        {
                            binaryReader.ReadInt16();
                        }
                        num3 = binaryReader.ReadUInt16();
                        if (num3 == 33072)
                        {
                            binaryReader.ReadByte();
                        }
                        else if (num3 == 33328)
                        {
                            binaryReader.ReadInt16();
                        }
                        if (this.CompareBytearrays(binaryReader.ReadBytes(11), numArray))
                        {
                            num3 = binaryReader.ReadUInt16();
                            if (num3 == 33072)
                            {
                                binaryReader.ReadByte();
                            }
                            else if (num3 == 33328)
                            {
                                binaryReader.ReadInt16();
                            }
                            num3 = binaryReader.ReadUInt16();
                            if (num3 == 33072)
                            {
                                binaryReader.ReadByte();
                            }
                            else if (num3 == 33328)
                            {
                                binaryReader.ReadInt16();
                            }
                            if (this.CompareBytearrays(binaryReader.ReadBytes(11), numArray1))
                            {
                                num3 = binaryReader.ReadUInt16();
                                if (num3 == 33072)
                                {
                                    binaryReader.ReadByte();
                                }
                                else if (num3 == 33328)
                                {
                                    binaryReader.ReadInt16();
                                }
                                num2 = binaryReader.ReadByte();
                                if (num2 == 4)
                                {
                                    byte[] numArray5 = binaryReader.ReadBytes((int)binaryReader.ReadByte());
                                    num2 = binaryReader.ReadByte();
                                    if (num2 == 2)
                                    {
                                        int num4 = binaryReader.ReadByte();
                                        if (num4 == 1)
                                        {
                                            num1 = binaryReader.ReadByte();
                                        }
                                        else if (num4 != 2)
                                        {
                                            rSACryptoServiceProvider = null;
                                            return rSACryptoServiceProvider;
                                        }
                                        else
                                        {
                                            num1 = 256 * binaryReader.ReadByte() + binaryReader.ReadByte();
                                        }
                                        num3 = binaryReader.ReadUInt16();
                                        if (num3 == 33072)
                                        {
                                            binaryReader.ReadByte();
                                        }
                                        else if (num3 == 33328)
                                        {
                                            binaryReader.ReadInt16();
                                        }
                                        if (num3 == 4400)
                                        {
                                            numArray3 = binaryReader.ReadBytes(7);
                                        }
                                        else if (this.CompareBytearrays(binaryReader.ReadBytes(10), numArray2))
                                        {
                                            flag = true;
                                        }
                                        else
                                        {
                                            this.mensajeErrorField = "No es una llave privada OIDdes-EDE3-CBC";
                                            rSACryptoServiceProvider = null;
                                            return rSACryptoServiceProvider;
                                        }
                                        num2 = binaryReader.ReadByte();
                                        if (num2 == 4)
                                        {
                                            byte[] numArray6 = binaryReader.ReadBytes((int)binaryReader.ReadByte());
                                            num2 = binaryReader.ReadByte();
                                            if (num2 == 4)
                                            {
                                                num2 = binaryReader.ReadByte();
                                                if (num2 == 129)
                                                {
                                                    num = binaryReader.ReadByte();
                                                }
                                                else if (num2 != 130)
                                                {
                                                    num = num2;
                                                }
                                                else
                                                {
                                                    num = 256 * binaryReader.ReadByte() + binaryReader.ReadByte();
                                                }
                                                byte[] numArray7 = binaryReader.ReadBytes(num);
                                                byte[] numArray8 = this.DecryptPbdk2(numArray7, numArray5, numArray6, pass, num1, flag);
                                                if (numArray8 != null)
                                                {
                                                    rSACryptoServiceProvider = this.DecodePrivateKeyInfo(numArray8);
                                                }
                                                else
                                                {
                                                    this.mensajeErrorField = "Contraseña de llave privada incorrecta.";
                                                    this.codigoErrorField = 323;
                                                    rSACryptoServiceProvider = null;
                                                }
                                            }
                                            else
                                            {
                                                rSACryptoServiceProvider = null;
                                            }
                                        }
                                        else
                                        {
                                            rSACryptoServiceProvider = null;
                                        }
                                    }
                                    else
                                    {
                                        rSACryptoServiceProvider = null;
                                    }
                                }
                                else
                                {
                                    rSACryptoServiceProvider = null;
                                }
                            }
                            else
                            {
                                rSACryptoServiceProvider = null;
                            }
                        }
                        else
                        {
                            rSACryptoServiceProvider = null;
                        }
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception.Message);
                        rSACryptoServiceProvider = null;
                    }
                }
                finally
                {
                    binaryReader.Close();
                }
            }
            else
            {
                rSACryptoServiceProvider = null;
            }
            return rSACryptoServiceProvider;
        }

        private byte[] DecodeOpenSslPrivateKey(string instr, SecureString pass)
        {
            byte[] numArray;
            byte[] numArray1;
            string str = instr.Trim();
            if ((!str.StartsWith("-----BEGIN RSA PRIVATE KEY-----") ? false : str.EndsWith("-----END RSA PRIVATE KEY-----")))
            {
                StringBuilder stringBuilder = new StringBuilder(str);
                stringBuilder.Replace("-----BEGIN RSA PRIVATE KEY-----", "");
                stringBuilder.Replace("-----END RSA PRIVATE KEY-----", "");
                string str1 = stringBuilder.ToString().Trim();
                try
                {
                    numArray = Convert.FromBase64String(str1);
                    numArray1 = numArray;
                    return numArray1;
                }
                catch (FormatException formatException)
                {
                    Console.WriteLine(formatException.Message);
                }
                StringReader stringReader = new StringReader(str1);
                if (stringReader.ReadLine().StartsWith("Proc-Type: 4,ENCRYPTED"))
                {
                    string str2 = stringReader.ReadLine();
                    if (str2.StartsWith("DEK-Info: DES-EDE3-CBC,"))
                    {
                        string str3 = str2.Substring(str2.IndexOf(",") + 1).Trim();
                        byte[] num = new byte[str3.Length / 2];
                        for (int i = 0; i < num.Length; i++)
                        {
                            num[i] = Convert.ToByte(str3.Substring(i * 2, 2), 16);
                        }
                        if (stringReader.ReadLine() == "")
                        {
                            string end = stringReader.ReadToEnd();
                            try
                            {
                                numArray = Convert.FromBase64String(end);
                            }
                            catch (FormatException formatException1)
                            {
                                Console.WriteLine(formatException1.Message);
                                numArray1 = null;
                                return numArray1;
                            }
                            byte[] openSSL3deskey = this.GetOpenSsl3deskey(num, pass, 1, 2);
                            if (openSSL3deskey != null)
                            {
                                byte[] numArray2 = this.DecryptKey(numArray, openSSL3deskey, num);
                                if (numArray2 == null)
                                {
                                    numArray1 = null;
                                }
                                else
                                {
                                    numArray1 = numArray2;
                                }
                            }
                            else
                            {
                                numArray1 = null;
                            }
                        }
                        else
                        {
                            numArray1 = null;
                        }
                    }
                    else
                    {
                        numArray1 = null;
                    }
                }
                else
                {
                    numArray1 = null;
                }
            }
            else
            {
                numArray1 = null;
            }
            return numArray1;
        }

        private byte[] DecodeOpenSslPublicKey(string instr)
        {
            byte[] numArray;
            byte[] numArray1;
            string str = instr.Trim();
            if ((!str.StartsWith("-----BEGIN PUBLIC KEY-----") ? false : str.EndsWith("-----END PUBLIC KEY-----")))
            {
                StringBuilder stringBuilder = new StringBuilder(str);
                stringBuilder.Replace("-----BEGIN PUBLIC KEY-----", "");
                stringBuilder.Replace("-----END PUBLIC KEY-----", "");
                string str1 = stringBuilder.ToString().Trim();
                try
                {
                    numArray = Convert.FromBase64String(str1);
                }
                catch (FormatException formatException)
                {
                    Console.WriteLine(formatException.Message);
                    numArray1 = null;
                    return numArray1;
                }
                numArray1 = numArray;
            }
            else
            {
                numArray1 = null;
            }
            return numArray1;
        }

        private void DecodePemKey(string pemstr, string password)
        {
            RSACryptoServiceProvider rSACryptoServiceProvider;
            string xmlString;
            SecureString secureString = this.ToSecureString(password);
            if (!(!pemstr.StartsWith("-----BEGIN PUBLIC KEY-----") ? true : !pemstr.EndsWith("-----END PUBLIC KEY-----")))
            {
                byte[] numArray = this.DecodeOpenSslPublicKey(pemstr);
                if (numArray != null)
                {
                    rSACryptoServiceProvider = this.DecodeX509PublicKey(numArray);
                    rSACryptoServiceProvider.ToXmlString(false);
                }
            }
            else if (!(!pemstr.StartsWith("-----BEGIN RSA PRIVATE KEY-----") ? true : !pemstr.EndsWith("-----END RSA PRIVATE KEY-----")))
            {
                byte[] numArray1 = this.DecodeOpenSslPrivateKey(pemstr, secureString);
                if (numArray1 != null)
                {
                    rSACryptoServiceProvider = this.DecodeRsaPrivateKey(numArray1);
                    xmlString = rSACryptoServiceProvider.ToXmlString(true);
                }
            }
            else if (!(!pemstr.StartsWith("-----BEGIN PRIVATE KEY-----") ? true : !pemstr.EndsWith("-----END PRIVATE KEY-----")))
            {
                byte[] numArray2 = this.DecodePkcs8PrivateKey(pemstr);
                if (numArray2 != null)
                {
                    rSACryptoServiceProvider = this.DecodePrivateKeyInfo(numArray2);
                    if (rSACryptoServiceProvider != null)
                    {
                        xmlString = rSACryptoServiceProvider.ToXmlString(true);
                    }
                }
            }
            else if ((!pemstr.StartsWith("-----BEGIN ENCRYPTED PRIVATE KEY-----") ? false : pemstr.EndsWith("-----END ENCRYPTED PRIVATE KEY-----")))
            {
                byte[] numArray3 = this.DecodePkcs8EncPrivateKey(pemstr);
                if (numArray3 != null)
                {
                    rSACryptoServiceProvider = this.DecodeEncryptedPrivateKeyInfo(numArray3, secureString);
                    if (rSACryptoServiceProvider != null)
                    {
                        xmlString = rSACryptoServiceProvider.ToXmlString(true);
                    }
                }
            }
        }

        private byte[] DecodePkcs8EncPrivateKey(string instr)
        {
            byte[] numArray;
            byte[] numArray1;
            string str = instr.Trim();
            if ((!str.StartsWith("-----BEGIN ENCRYPTED PRIVATE KEY-----") ? false : str.EndsWith("-----END ENCRYPTED PRIVATE KEY-----")))
            {
                StringBuilder stringBuilder = new StringBuilder(str);
                stringBuilder.Replace("-----BEGIN ENCRYPTED PRIVATE KEY-----", "");
                stringBuilder.Replace("-----END ENCRYPTED PRIVATE KEY-----", "");
                string str1 = stringBuilder.ToString().Trim();
                try
                {
                    numArray = Convert.FromBase64String(str1);
                }
                catch (FormatException formatException)
                {
                    Console.WriteLine(formatException.Message);
                    numArray1 = null;
                    return numArray1;
                }
                numArray1 = numArray;
            }
            else
            {
                numArray1 = null;
            }
            return numArray1;
        }

        private byte[] DecodePkcs8PrivateKey(string instr)
        {
            byte[] numArray;
            byte[] numArray1;
            string str = instr.Trim();
            if ((!str.StartsWith("-----BEGIN PRIVATE KEY-----") ? false : str.EndsWith("-----END PRIVATE KEY-----")))
            {
                StringBuilder stringBuilder = new StringBuilder(str);
                stringBuilder.Replace("-----BEGIN PRIVATE KEY-----", "");
                stringBuilder.Replace("-----END PRIVATE KEY-----", "");
                string str1 = stringBuilder.ToString().Trim();
                try
                {
                    numArray = Convert.FromBase64String(str1);
                }
                catch (FormatException formatException)
                {
                    Console.WriteLine(formatException.Message);
                    numArray1 = null;
                    return numArray1;
                }
                numArray1 = numArray;
            }
            else
            {
                numArray1 = null;
            }
            return numArray1;
        }

        private RSACryptoServiceProvider DecodePrivateKeyInfo(byte[] pkcs8)
        {
            RSACryptoServiceProvider rSACryptoServiceProvider;
            byte[] numArray = new byte[] { 48, 13, 6, 9, 42, 134, 72, 134, 247, 13, 1, 1, 1, 5, 0 };
            //byte[] numArray1 = new byte[15];
            MemoryStream memoryStream = new MemoryStream(pkcs8);
            int length = (int)memoryStream.Length;
            BinaryReader binaryReader = new BinaryReader(memoryStream);
            byte num = 0;
            ushort num1 = 0;
            try
            {
                try
                {
                    num1 = binaryReader.ReadUInt16();
                    if (num1 == 33072)
                    {
                        binaryReader.ReadByte();
                    }
                    else if (num1 != 33328)
                    {
                        rSACryptoServiceProvider = null;
                        return rSACryptoServiceProvider;
                    }
                    else
                    {
                        binaryReader.ReadInt16();
                    }
                    num = binaryReader.ReadByte();
                    if (num == 2)
                    {
                        num1 = binaryReader.ReadUInt16();
                        if (num1 != 1)
                        {
                            rSACryptoServiceProvider = null;
                        }
                        else if (this.CompareBytearrays(binaryReader.ReadBytes(15), numArray))
                        {
                            num = binaryReader.ReadByte();
                            if (num == 4)
                            {
                                num = binaryReader.ReadByte();
                                if (num == 129)
                                {
                                    binaryReader.ReadByte();
                                }
                                else if (num == 130)
                                {
                                    binaryReader.ReadUInt16();
                                }
                                byte[] numArray2 = binaryReader.ReadBytes((int)((long)length - memoryStream.Position));
                                rSACryptoServiceProvider = this.DecodeRsaPrivateKey(numArray2);
                            }
                            else
                            {
                                rSACryptoServiceProvider = null;
                            }
                        }
                        else
                        {
                            rSACryptoServiceProvider = null;
                        }
                    }
                    else
                    {
                        rSACryptoServiceProvider = null;
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message);
                    rSACryptoServiceProvider = null;
                }
            }
            finally
            {
                binaryReader.Close();
            }
            return rSACryptoServiceProvider;
        }

        private RSACryptoServiceProvider DecodeRsaPrivateKey(byte[] privkey)
        {
            byte[] numArray;
            byte[] numArray1;
            byte[] numArray2;
            byte[] numArray3;
            byte[] numArray4;
            byte[] numArray5;
            int num;
            RSACryptoServiceProvider rSACryptoServiceProvider;
            this.bufferLlaveField = privkey;
            BinaryReader binaryReader = new BinaryReader(new MemoryStream(privkey));
            ushort num1 = 0;
            int integerSize = 0;
            try
            {
                try
                {
                    num1 = binaryReader.ReadUInt16();
                    if (num1 == 33072)
                    {
                        binaryReader.ReadByte();
                    }
                    else if (num1 != 33328)
                    {
                        rSACryptoServiceProvider = null;
                        return rSACryptoServiceProvider;
                    }
                    else
                    {
                        binaryReader.ReadInt16();
                    }
                    num1 = binaryReader.ReadUInt16();
                    if (num1 != 258)
                    {
                        rSACryptoServiceProvider = null;
                    }
                    else if (binaryReader.ReadByte() == 0)
                    {
                        integerSize = this.GetIntegerSize(binaryReader);
                        byte[] numArray6 = binaryReader.ReadBytes(integerSize);
                        integerSize = this.GetIntegerSize(binaryReader);
                        byte[] numArray7 = binaryReader.ReadBytes(integerSize);
                        integerSize = this.GetIntegerSize(binaryReader);
                        if (integerSize % 8 == 0)
                        {
                            numArray = binaryReader.ReadBytes(integerSize);
                        }
                        else
                        {
                            num = (integerSize / 8 + 1) * 8;
                            numArray = new byte[num];
                            binaryReader.ReadBytes(integerSize).CopyTo(numArray, num - integerSize);
                        }
                        integerSize = this.GetIntegerSize(binaryReader);
                        if (integerSize % 8 == 0)
                        {
                            numArray1 = binaryReader.ReadBytes(integerSize);
                        }
                        else
                        {
                            num = (integerSize / 8 + 1) * 8;
                            numArray1 = new byte[num];
                            binaryReader.ReadBytes(integerSize).CopyTo(numArray1, num - integerSize);
                        }
                        integerSize = this.GetIntegerSize(binaryReader);
                        if (integerSize % 8 == 0)
                        {
                            numArray2 = binaryReader.ReadBytes(integerSize);
                        }
                        else
                        {
                            num = (integerSize / 8 + 1) * 8;
                            numArray2 = new byte[num];
                            binaryReader.ReadBytes(integerSize).CopyTo(numArray2, num - integerSize);
                        }
                        integerSize = this.GetIntegerSize(binaryReader);
                        if (integerSize % 8 == 0)
                        {
                            numArray3 = binaryReader.ReadBytes(integerSize);
                        }
                        else
                        {
                            num = (integerSize / 8 + 1) * 8;
                            numArray3 = new byte[num];
                            binaryReader.ReadBytes(integerSize).CopyTo(numArray3, num - integerSize);
                        }
                        integerSize = this.GetIntegerSize(binaryReader);
                        if (integerSize % 8 == 0)
                        {
                            numArray4 = binaryReader.ReadBytes(integerSize);
                        }
                        else
                        {
                            num = (integerSize / 8 + 1) * 8;
                            numArray4 = new byte[num];
                            binaryReader.ReadBytes(integerSize).CopyTo(numArray4, num - integerSize);
                        }
                        integerSize = this.GetIntegerSize(binaryReader);
                        if (integerSize % 8 == 0)
                        {
                            numArray5 = binaryReader.ReadBytes(integerSize);
                        }
                        else
                        {
                            num = (integerSize / 8 + 1) * 8;
                            numArray5 = new byte[num];
                            binaryReader.ReadBytes(integerSize).CopyTo(numArray5, num - integerSize);
                        }
                        RSACryptoServiceProvider rSACryptoServiceProvider1 = new RSACryptoServiceProvider();
                        RSAParameters rSAParameter = new RSAParameters();
                        this.moduloField = Encoding.UTF8.GetString(numArray6);
                        rSAParameter.Modulus = numArray6;
                        rSAParameter.Exponent = numArray7;
                        rSAParameter.D = numArray;
                        rSAParameter.P = numArray1;
                        rSAParameter.Q = numArray2;
                        rSAParameter.DP = numArray3;
                        rSAParameter.DQ = numArray4;
                        rSAParameter.InverseQ = numArray5;
                        rSACryptoServiceProvider1.ImportParameters(rSAParameter);
                        rSACryptoServiceProvider = rSACryptoServiceProvider1;
                    }
                    else
                    {
                        rSACryptoServiceProvider = null;
                    }
                }
                catch (Exception exception)
                {
                    this.mensajeErrorField = string.Concat("Error al desencriptar la llave. ", exception.Message);
                    this.codigoErrorField = 322;
                    rSACryptoServiceProvider = null;
                }
            }
            finally
            {
                binaryReader.Close();
            }
            return rSACryptoServiceProvider;
        }

        private RSACryptoServiceProvider DecodeX509PublicKey(byte[] x509key)
        {
            RSACryptoServiceProvider rSACryptoServiceProvider;
            byte[] numArray = new byte[] { 48, 13, 6, 9, 42, 134, 72, 134, 247, 13, 1, 1, 1, 5, 0 };
            //byte[] numArray1 = new byte[15];
            BinaryReader binaryReader = new BinaryReader(new MemoryStream(x509key));
            ushort num = 0;
            try
            {
                try
                {
                    num = binaryReader.ReadUInt16();
                    if (num == 33072)
                    {
                        binaryReader.ReadByte();
                    }
                    else if (num != 33328)
                    {
                        rSACryptoServiceProvider = null;
                        return rSACryptoServiceProvider;
                    }
                    else
                    {
                        binaryReader.ReadInt16();
                    }
                    if (this.CompareBytearrays(binaryReader.ReadBytes(15), numArray))
                    {
                        num = binaryReader.ReadUInt16();
                        if (num == 33027)
                        {
                            binaryReader.ReadByte();
                        }
                        else if (num != 33283)
                        {
                            rSACryptoServiceProvider = null;
                            return rSACryptoServiceProvider;
                        }
                        else
                        {
                            binaryReader.ReadInt16();
                        }
                        if (binaryReader.ReadByte() == 0)
                        {
                            num = binaryReader.ReadUInt16();
                            if (num == 33072)
                            {
                                binaryReader.ReadByte();
                            }
                            else if (num != 33328)
                            {
                                rSACryptoServiceProvider = null;
                                return rSACryptoServiceProvider;
                            }
                            else
                            {
                                binaryReader.ReadInt16();
                            }
                            num = binaryReader.ReadUInt16();
                            byte num1 = 0;
                            byte num2 = 0;
                            if (num == 33026)
                            {
                                num1 = binaryReader.ReadByte();
                            }
                            else if (num != 33282)
                            {
                                rSACryptoServiceProvider = null;
                                return rSACryptoServiceProvider;
                            }
                            else
                            {
                                num2 = binaryReader.ReadByte();
                                num1 = binaryReader.ReadByte();
                            }
                            byte[] numArray2 = new byte[] { num1, num2, 0, 0 };
                            int num3 = BitConverter.ToInt32(numArray2, 0);
                            byte num4 = binaryReader.ReadByte();
                            binaryReader.BaseStream.Seek((long)-1, SeekOrigin.Current);
                            if (num4 == 0)
                            {
                                binaryReader.ReadByte();
                                num3--;
                            }
                            byte[] numArray3 = binaryReader.ReadBytes(num3);
                            if (binaryReader.ReadByte() == 2)
                            {
                                byte[] numArray4 = binaryReader.ReadBytes((int)binaryReader.ReadByte());
                                this.ShowBytes("\nExponent", numArray4);
                                this.ShowBytes("\nModulus", numArray3);
                                RSACryptoServiceProvider rSACryptoServiceProvider1 = new RSACryptoServiceProvider();
                                RSAParameters rSAParameter = new RSAParameters()
                                {
                                    Modulus = numArray3,
                                    Exponent = numArray4
                                };
                                rSACryptoServiceProvider1.ImportParameters(rSAParameter);
                                rSACryptoServiceProvider = rSACryptoServiceProvider1;
                            }
                            else
                            {
                                rSACryptoServiceProvider = null;
                            }
                        }
                        else
                        {
                            rSACryptoServiceProvider = null;
                        }
                    }
                    else
                    {
                        rSACryptoServiceProvider = null;
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message);
                    rSACryptoServiceProvider = null;
                }
            }
            finally
            {
                binaryReader.Close();
            }
            return rSACryptoServiceProvider;
        }

        private byte[] DecryptKey(byte[] cipherData, byte[] desKey, byte[] IV)
        {
            byte[] array;
            MemoryStream memoryStream = new MemoryStream();
            TripleDES v = TripleDES.Create();
            v.Key = desKey;
            v.IV = IV;
            try
            {
                CryptoStream cryptoStream = new CryptoStream(memoryStream, v.CreateDecryptor(), CryptoStreamMode.Write);
                cryptoStream.Write(cipherData, 0, cipherData.Length);
                cryptoStream.Close();
            }
            catch (Exception exception)
            {
                this.mensajeErrorField = string.Concat("Error al desencriptar la llave. ", exception.Message);
                this.codigoErrorField = 322;
                array = null;
                return array;
            }
            array = memoryStream.ToArray();
            return array;
        }

        private byte[] DecryptPbdk2(byte[] edata, byte[] salt, byte[] IV, SecureString secpswd, int iterations, bool es3DES)
        {
            byte[] array;
            CryptoStream cryptoStream = null;
            IntPtr zero = IntPtr.Zero;
            byte[] numArray = new byte[secpswd.Length];
            zero = Marshal.SecureStringToGlobalAllocAnsi(secpswd);
            Marshal.Copy(zero, numArray, 0, numArray.Length);
            Marshal.ZeroFreeGlobalAllocAnsi(zero);
            try
            {
                Rfc2898DeriveBytes rfc2898DeriveByte = new Rfc2898DeriveBytes(numArray, salt, iterations);
                MemoryStream memoryStream = new MemoryStream();
                if (!es3DES)
                {
                    DES bytes = DES.Create();
                    bytes.Key = rfc2898DeriveByte.GetBytes(8);
                    bytes.IV = IV;
                    cryptoStream = new CryptoStream(memoryStream, bytes.CreateDecryptor(), CryptoStreamMode.Write);
                }
                else
                {
                    TripleDES v = TripleDES.Create();
                    DES.Create();
                    v.Key = rfc2898DeriveByte.GetBytes(24);
                    v.IV = IV;
                    cryptoStream = new CryptoStream(memoryStream, v.CreateDecryptor(), CryptoStreamMode.Write);
                }
                cryptoStream.Write(edata, 0, edata.Length);
                cryptoStream.Flush();
                cryptoStream.Close();
                array = memoryStream.ToArray();
            }
            catch (Exception exception)
            {
                this.mensajeErrorField = string.Concat("Error al desencriptar la llave. ", exception.Message);
                this.codigoErrorField = 322;
                array = null;
            }
            return array;
        }

        public void ExportarPem(string rutaArchivoPem)
        {
            if (rutaArchivoPem == "")
            {
                this.mensajeErrorField = "La ruta del archivo PEM no puede ser vacía.";
                this.codigoErrorField = 0;
            }
            else if (this.bufferLlaveField == null)
            {
                this.mensajeErrorField = "No ha cargado una llave privada.";
                this.codigoErrorField = 101;
            }
            else if (Directory.Exists(Path.GetDirectoryName(rutaArchivoPem)))
            {
                rutaArchivoPem = string.Concat(Path.GetDirectoryName(rutaArchivoPem), "\\", Path.GetFileNameWithoutExtension(rutaArchivoPem));
                rutaArchivoPem = string.Concat(rutaArchivoPem, ".pem");
                try
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.AppendLine("-----BEGIN RSA PRIVATE KEY-----");
                    string base64String = Convert.ToBase64String(this.bufferLlaveField);
                    for (int i = 0; i < base64String.Length - 1; i += 64)
                    {
                        if (i + 64 >= base64String.Length - 1)
                        {
                            stringBuilder.AppendLine(base64String.Substring(i, base64String.Length - i));
                        }
                        else
                        {
                            stringBuilder.AppendLine(base64String.Substring(i, 64));
                        }
                    }
                    stringBuilder.AppendLine("-----END RSA PRIVATE KEY-----");
                    File.WriteAllText(rutaArchivoPem, stringBuilder.ToString(), new UTF8Encoding(false));
                    if (!File.Exists(rutaArchivoPem))
                    {
                        this.mensajeErrorField = "Error al crear el archivo. Verifique permisos de escritura en el directorio.";
                        this.codigoErrorField = 505;
                    }
                }
                catch (Exception exception)
                {
                    this.mensajeErrorField = string.Concat("Error interno al exportar certificado a PFX: ", exception.Message);
                    this.codigoErrorField = 506;
                }
            }
            else
            {
                this.mensajeErrorField = "El directorio especificado no es válido o no existe.";
                this.codigoErrorField = 201;
            }
        }

        public string ExportarXml()
        {
            string xmlString;
            if (this.llaveRsaField != null)
            {
                xmlString = this.llaveRsaField.ToXmlString(true);
            }
            else
            {
                this.mensajeErrorField = "No ha cargado una llave privada.";
                this.codigoErrorField = 103;
                xmlString = null;
            }
            return xmlString;
        }

        ~CryptoPrivateKey()
        {
            this.mensajeErrorField = null;
            this.codigoErrorField = 0;
            this.tipoCertificadoField = null;
            this.moduloField = null;
            this.bufferLlaveField = null;
            if (this.llaveRsaField != null)
            {
                this.llaveRsaField = null;
            }
            GC.Collect();
        }

        private int GetIntegerSize(BinaryReader binr)
        {
            int num;
            byte num1 = 0;
            byte num2 = 0;
            byte num3 = 0;
            int num4 = 0;
            num1 = binr.ReadByte();
            if (num1 == 2)
            {
                num1 = binr.ReadByte();
                if (num1 == 129)
                {
                    num4 = binr.ReadByte();
                }
                else if (num1 != 130)
                {
                    num4 = num1;
                }
                else
                {
                    num3 = binr.ReadByte();
                    num2 = binr.ReadByte();
                    byte[] numArray = new byte[] { num2, num3, 0, 0 };
                    num4 = BitConverter.ToInt32(numArray, 0);
                }
                while (binr.ReadByte() == 0)
                {
                    num4--;
                }
                binr.BaseStream.Seek((long)-1, SeekOrigin.Current);
                num = num4;
            }
            else
            {
                num = 0;
            }
            return num;
        }

        private byte[] GetOpenSsl3deskey(byte[] salt, SecureString secpswd, int count, int miter)
        {
            IntPtr zero = IntPtr.Zero;
            int num = 16;
            byte[] numArray = new byte[num * miter];
            byte[] numArray1 = new byte[secpswd.Length];
            zero = Marshal.SecureStringToGlobalAllocAnsi(secpswd);
            Marshal.Copy(zero, numArray1, 0, numArray1.Length);
            Marshal.ZeroFreeGlobalAllocAnsi(zero);
            byte[] numArray2 = new byte[numArray1.Length + salt.Length];
            Array.Copy(numArray1, numArray2, numArray1.Length);
            Array.Copy(salt, 0, numArray2, numArray1.Length, salt.Length);
            MD5 mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
            byte[] numArray3 = null;
            byte[] numArray4 = new byte[num + numArray2.Length];
            for (int i = 0; i < miter; i++)
            {
                if (i != 0)
                {
                    Array.Copy(numArray3, numArray4, numArray3.Length);
                    Array.Copy(numArray2, 0, numArray4, numArray3.Length, numArray2.Length);
                    numArray3 = numArray4;
                }
                else
                {
                    numArray3 = numArray2;
                }
                for (int j = 0; j < count; j++)
                {
                    numArray3 = mD5CryptoServiceProvider.ComputeHash(numArray3);
                }
                Array.Copy(numArray3, 0, numArray, i * num, numArray3.Length);
            }
            byte[] numArray5 = new byte[24];
            Array.Copy(numArray, numArray5, numArray5.Length);
            Array.Clear(numArray1, 0, numArray1.Length);
            Array.Clear(numArray2, 0, numArray2.Length);
            Array.Clear(numArray3, 0, numArray3.Length);
            Array.Clear(numArray4, 0, numArray4.Length);
            Array.Clear(numArray, 0, numArray.Length);
            return numArray5;
        }

        private void Inicializar()
        {
            this.mensajeErrorField = "";
            this.codigoErrorField = 0;
            this.tipoCertificadoField = "";
            this.moduloField = "";
            this.llaveRsaField = null;
        }

        private string SellarCadenaCodificacion(string cadenaOriginal, string codSha)
        {
            this.mensajeErrorField = "";
            this.codigoErrorField = 0;
            byte[] numArray;
            string base64String;
            if (this.llaveRsaField == null)
            {
                this.mensajeErrorField = "No ha cargado una llave privada.";
                this.codigoErrorField = 103;
                base64String = null;
            }
            else if (!(cadenaOriginal == ""))
            {
                RSACryptoServiceProvider rSACryptoServiceProvider = this.llaveRsaField;
                try
                {
                    byte[] bytes = (new UTF8Encoding()).GetBytes(cadenaOriginal);
                    try
                    {
                        try
                        {
                            numArray = rSACryptoServiceProvider.SignData(bytes, CryptoConfig.MapNameToOID(codSha));
                        }
                        catch (CryptographicException cryptographicException)
                        {
                            this.mensajeErrorField = string.Concat("Error interno al sellar la cadena original: ", cryptographicException.Message);
                            this.codigoErrorField = 522;
                            numArray = null;
                        }
                    }
                    finally
                    {
                        rSACryptoServiceProvider.PersistKeyInCsp = false;
                    }
                }
                finally
                {
                    if (rSACryptoServiceProvider != null)
                    {
                        ((IDisposable)rSACryptoServiceProvider).Dispose();
                    }
                }
                base64String = Convert.ToBase64String(numArray);
            }
            else
            {
                this.mensajeErrorField = "La cadena original no puede ser vacía.";
                this.codigoErrorField = 125;
                base64String = null;
            }
            return base64String;
        }

        public string SellarCadenaSha1(string cadenaOriginal)
        {
            return this.SellarCadenaCodificacion(cadenaOriginal, "SHA1");
        }

        public string SellarCadenaSha256(string cadenaOriginal)
        {
            return this.SellarCadenaCodificacion(cadenaOriginal, "SHA256");
        }

        public string SellarCadenaSha512(string cadenaOriginal)
        {
            return this.SellarCadenaCodificacion(cadenaOriginal, "SHA512");
        }

        private void ShowBytes(string info, byte[] data)
        {
            for (int i = 1; i <= data.Length; i++)
            {
                Console.Write("{0:X2}  ", data[i - 1]);
                if (i % 16 == 0)
                {
                }
                Console.WriteLine();
            }
            Console.WriteLine("\n\n");
        }

        private void ShowRsaProps(RSACryptoServiceProvider rsa)
        {
            CspKeyContainerInfo cspKeyContainerInfo = rsa.CspKeyContainerInfo;
        }

        private SecureString ToSecureString(string source)
        {
            SecureString secureString;
            if (!string.IsNullOrEmpty(source))
            {
                SecureString secureString1 = new SecureString();
                char[] charArray = source.ToCharArray();
                for (int i = 0; i < charArray.Length; i++)
                {
                    secureString1.AppendChar(charArray[i]);
                }
                secureString = secureString1;
            }
            else
            {
                secureString = null;
            }
            return secureString;
        }
    }
}