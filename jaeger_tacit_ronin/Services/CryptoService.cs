﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Jaeger.Services
{
    public class CryptoService
    {
        [DebuggerNonUserCode]
        public CryptoService()
        {
        }

        public static bool ValidaSello(string selloBs64, string cadenaOriginal, X509Certificate2 x509, string mapNameToOid)
        {
            bool flag = false;
            try
            {
                byte[] array = Convert.FromBase64String(selloBs64);
                byte[] bytes = Encoding.UTF8.GetBytes(cadenaOriginal);
                List<byte> nums = new List<byte>(array);
                while ((double)nums.Count < (double)x509.PublicKey.Key.KeySize / 8)
                {
                    nums.Insert(0, 0);
                }
                array = nums.ToArray();
                RSACryptoServiceProvider key = (RSACryptoServiceProvider)x509.PublicKey.Key;
                flag = key.VerifyData(bytes, CryptoConfig.MapNameToOID(mapNameToOid), array);
            }
            catch (Exception exception)
            {
                Console.WriteLine(string.Concat("validaSello: ", exception.Message));
            }
            return flag;
        }

        public static bool ValidaSelloCfdi(CFDI.V32.Comprobante oComprobante)
        {
            bool flag = false;
            try
            {
                byte[] numArray = Convert.FromBase64String(oComprobante.sello);
                X509Certificate2 oX509Certificate2 = new X509Certificate2(Convert.FromBase64String(oComprobante.certificado));
                string empty = string.Empty;
                empty = oComprobante.OriginalXmlString;
                byte[] bytes = Encoding.UTF8.GetBytes(empty);
                RSACryptoServiceProvider key = (RSACryptoServiceProvider)oX509Certificate2.PublicKey.Key;
                flag = key.VerifyData(bytes, CryptoConfig.MapNameToOID("SHA1"), numArray);
            }
            catch (Exception exception)
            {
                Console.WriteLine(string.Concat("validaSelloCFDI: ", exception.Message));
            }
            return flag;
        }

        public static bool ValidaSelloCfdi(CFDI.V33.Comprobante oComprobante)
        {
            bool flag = false;
            try
            {
                byte[] numArray = Convert.FromBase64String(oComprobante.Sello);
                X509Certificate2 oX509Certificate2 = new X509Certificate2(Convert.FromBase64String(oComprobante.Certificado));
                string empty = string.Empty;
                empty = oComprobante.CadenaOriginalOff;
                byte[] bytes = Encoding.UTF8.GetBytes(empty);
                RSACryptoServiceProvider key = (RSACryptoServiceProvider)oX509Certificate2.PublicKey.Key;
                flag = key.VerifyData(bytes, CryptoConfig.MapNameToOID("SHA256"), numArray);
                if (!flag)
                {
                    empty = oComprobante.CadenaOriginal;
                    byte[] bytes1 = Encoding.UTF8.GetBytes(empty);
                    RSACryptoServiceProvider key1 = (RSACryptoServiceProvider)oX509Certificate2.PublicKey.Key;
                    flag = key1.VerifyData(bytes1, CryptoConfig.MapNameToOID("SHA256"), numArray);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Concat("validaSelloCFDI: ", ex.Message));
            }
            return flag;
        }

        public static bool ValidaSelloCfdi(CFDI.V40.Comprobante oComprobante) {
            bool flag = false;
            try {
                byte[] numArray = Convert.FromBase64String(oComprobante.Sello);
                X509Certificate2 oX509Certificate2 = new X509Certificate2(Convert.FromBase64String(oComprobante.Certificado));
                string empty = string.Empty;
                empty = oComprobante.CadenaOriginalOff;
                byte[] bytes = Encoding.UTF8.GetBytes(empty);
                RSACryptoServiceProvider key = (RSACryptoServiceProvider)oX509Certificate2.PublicKey.Key;
                flag = key.VerifyData(bytes, CryptoConfig.MapNameToOID("SHA256"), numArray);
                if (!flag) {
                    empty = oComprobante.CadenaOriginalOff;
                    byte[] bytes1 = Encoding.UTF8.GetBytes(empty);
                    RSACryptoServiceProvider key1 = (RSACryptoServiceProvider)oX509Certificate2.PublicKey.Key;
                    flag = key1.VerifyData(bytes1, CryptoConfig.MapNameToOID("SHA256"), numArray);
                }
            } catch (Exception ex) {
                Console.WriteLine(string.Concat("validaSelloCFDI: ", ex.Message));
            }
            return flag;
        }
    }
}