﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace Jaeger.Services
{
    public class Crypto2Certificate
    {
        private new Dictionary<int, string> errores= new Dictionary<int,string>();
        private string mensajeErrorField;
        private int codigoErrorField;
        private static CryptoPrivateKey llaveField;
        private X509Certificate2 certificadoX509Field;
        private string tipoCertificadoField;
        private string versionField;
        private int longitudClaveField;
        private static string moduloField;
        private static string strXmlField;
        private bool esFielField;
        private string emisorField;
        private string asuntoField;
        private string validoDesdeField;
        private string validoHastaField;
        private string noSerieField;
        private string rfcField;
        private string razonSocialField;
        private string autoridadEmisoraField;

        public string AutoridadEmisora
        {
            get
            {
                return this.autoridadEmisoraField;
            }
        }

        public string CadenaAsunto
        {
            get
            {
                return this.asuntoField;
            }
        }

        public string CadenaEmisor
        {
            get
            {
                return this.emisorField;
            }
        }

        public X509Certificate2 CertificadoOriginal
        {
            get
            {
                return this.certificadoX509Field;
            }
        }

        public int CodigoDeError
        {
            get
            {
                return this.codigoErrorField;
            }
        }

        public bool EsFiel
        {
            get
            {
                return this.esFielField;
            }
        }

        private CryptoPrivateKey CryptoPrivateKey
        {
            get
            {
                return Crypto2Certificate.llaveField;
            }
        }

        public int LongitudDeClavePublica
        {
            get
            {
                return this.longitudClaveField;
            }
        }

        public string MensajeDeError
        {
            get
            {
                return this.mensajeErrorField;
            }
        }

        private string Modulus
        {
            get
            {
                return Crypto2Certificate.moduloField;
            }
        }

        public string NoSerie
        {
            get
            {
                return this.noSerieField;
            }
        }

        public string RazonSocial
        {
            get
            {
                return this.razonSocialField;
            }
        }

        public string Rfc
        {
            get
            {
                return this.rfcField;
            }
        }

        public string TipoCertificado
        {
            get
            {
                return this.tipoCertificadoField;
            }
        }

        public string ValidoDesde
        {
            get
            {
                return this.validoDesdeField;
            }
        }

        public string ValidoHasta
        {
            get
            {
                return this.validoHastaField;
            }
        }

        public string Version
        {
            get
            {
                return this.versionField;
            }
        }

        public Crypto2Certificate()
        {
            this.Inicializar();
        }

        public Crypto2Certificate(string rutaCertificado)
        {
            this.Inicializar();
            this.CargarCertificado(rutaCertificado);
        }

        public void CargarCertificado(string rutaCertificado)
        {
            if (rutaCertificado == "")
            {
                this.mensajeErrorField = "La ruta del certificado no puede ser vacía.";
                this.codigoErrorField = 111;
            }
            else if (!File.Exists(rutaCertificado))
            {
                this.mensajeErrorField = "Ruta de archivo de certificado inválida o no existe.";
                this.codigoErrorField = 211;
            }
            else
            {
                this.certificadoX509Field = this.GetFilecert(rutaCertificado);
                if (this.certificadoX509Field != null)
                {
                    this.CargarPropiedades();
                }
                else
                {
                    this.mensajeErrorField = "Archivo de certificado X509 no válido.";
                    this.codigoErrorField = 311;
                }
            }
        }

        public void CargarCertificado1(byte[] byteCertificado)
        {
            try
            {
                this.certificadoX509Field = new X509Certificate2(byteCertificado);
            }
            catch (Exception exception)
            {
                this.mensajeErrorField = string.Concat("Buffer del certificado incorrecto. ", exception.Message);
                this.codigoErrorField = 312;
                return;
            }
            this.CargarPropiedades();
        }

        public void CargarCertificadoDeB64(string certificadoB64)
        {
            if (!(certificadoB64 == ""))
            {
                try
                {
                    this.certificadoX509Field = new X509Certificate2(Convert.FromBase64String(certificadoB64));
                }
                catch (Exception exception)
                {
                    this.mensajeErrorField = string.Concat("Certificado en formato base 64 no válido. ", exception.Message);
                    this.codigoErrorField = 313;
                    return;
                }
                this.CargarPropiedades();
            }
            else
            {
                this.mensajeErrorField = "La cadena del certificado en base 64 no puede ser vacía.";
                this.codigoErrorField = 112;
            }
        }

        public void CargarLlavePrivada(CryptoPrivateKey claseLlavePrivada)
        {
            if (this.certificadoX509Field != null)
            {
                Crypto2Certificate.llaveField = claseLlavePrivada;
                try
                {
                    this.certificadoX509Field.PrivateKey = claseLlavePrivada.LlavePrivadaRsa;
                }
                catch (Exception exception)
                {
                    this.mensajeErrorField = string.Concat("Error de correspondencia entre certificado y llave privada. ", exception.Message);
                    this.codigoErrorField = 314;
                    return;
                }
                if (Crypto2Certificate.moduloField != Crypto2Certificate.llaveField.Modulus)
                {
                    this.mensajeErrorField = "Error de correspondencia entre certificado y llave privada.";
                    this.codigoErrorField = 314;
                }
            }
            else
            {
                this.mensajeErrorField = "No ha cargado un certificado.";
                this.codigoErrorField = 101;
            }
        }

        private void CargarPropiedades()
        {
            if (this.certificadoX509Field != null)
            {
                try
                {
                    Crypto2Certificate.strXmlField = this.CertToXml(this.certificadoX509Field);
                    this.versionField = this.certificadoX509Field.Version.ToString();
                    this.asuntoField = this.certificadoX509Field.Subject;
                    this.emisorField = this.certificadoX509Field.Issuer;
                    DateTime notAfter = this.certificadoX509Field.NotAfter;
                    this.validoHastaField = notAfter.ToString("s");
                    notAfter = this.certificadoX509Field.NotBefore;
                    this.validoDesdeField = notAfter.ToString("s");
                    Regex regex = new Regex("[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9A]");
                    int num = this.certificadoX509Field.Subject.IndexOf("OID.2.5.4.45=");
                    if (num < 0)
                    {
                        num = 0;
                    }
                    Match match = regex.Match(this.certificadoX509Field.Subject, num);
                    this.rfcField = match.Value.ToString();
                    this.esFielField = this.certificadoX509Field.Subject.IndexOf("OU=") < 0;
                    if (!this.esFielField)
                    {
                        this.tipoCertificadoField = "SELLO";
                    }
                    else
                    {
                        this.tipoCertificadoField = "FIEL";
                    }
                    this.longitudClaveField = this.certificadoX509Field.PublicKey.Key.KeySize;
                    this.noSerieField = this.SerialAsciItoHex(this.certificadoX509Field.SerialNumber);
                    int num1 = this.certificadoX509Field.Subject.IndexOf(" OID.2.5.4.41=") + 14;
                    if (num1 == 13)
                    {
                        num1 = this.certificadoX509Field.Subject.IndexOf("CN=") + 3;
                    }
                    int length = this.certificadoX509Field.Subject.IndexOf(", ", num1);
                    if (length == -1)
                    {
                        length = this.certificadoX509Field.Subject.Length;
                    }
                    this.razonSocialField = this.certificadoX509Field.Subject.Substring(num1, length - num1).Trim();
                    num1 = this.certificadoX509Field.Issuer.IndexOf(" O=") + 3;
                    if (num1 == 2)
                    {
                        num1 = this.certificadoX509Field.Issuer.IndexOf("CN=") + 3;
                    }
                    length = this.certificadoX509Field.Issuer.IndexOf(", ", num1);
                    if (length == -1)
                    {
                        length = this.certificadoX509Field.Issuer.Length;
                    }
                    this.autoridadEmisoraField = this.certificadoX509Field.Issuer.Substring(num1, length - num1).Trim();
                }
                catch (Exception exception)
                {
                    this.mensajeErrorField = string.Concat("Error al leer las propiedades del certificado. ", exception.Message);
                    this.codigoErrorField = 514;
                }
            }
            else
            {
                this.mensajeErrorField = "No ha cargado un certificado.";
                this.codigoErrorField = 101;
            }
        }

        private string CertToXml(X509Certificate2 cert)
        {
            string xmlString;
            BinaryReader binaryReader = new BinaryReader(new MemoryStream(cert.GetPublicKey()));
            ushort num = 0;
            try
            {
                try
                {
                    num = binaryReader.ReadUInt16();
                    if (num == 33072)
                    {
                        binaryReader.ReadByte();
                    }
                    else if (num != 33328)
                    {
                        xmlString = null;
                        return xmlString;
                    }
                    else
                    {
                        binaryReader.ReadInt16();
                    }
                    num = binaryReader.ReadUInt16();
                    byte num1 = 0;
                    byte num2 = 0;
                    if (num == 33026)
                    {
                        num1 = binaryReader.ReadByte();
                    }
                    else if (num != 33282)
                    {
                        xmlString = null;
                        return xmlString;
                    }
                    else
                    {
                        num2 = binaryReader.ReadByte();
                        num1 = binaryReader.ReadByte();
                    }
                    byte[] numArray = new byte[] { num1, num2, 0, 0 };
                    int num3 = BitConverter.ToInt32(numArray, 0);
                    if (binaryReader.PeekChar() == 0)
                    {
                        binaryReader.ReadByte();
                        num3--;
                    }
                    byte[] numArray1 = binaryReader.ReadBytes(num3);
                    if (binaryReader.ReadByte() == 2)
                    {
                        byte[] numArray2 = binaryReader.ReadBytes((int)binaryReader.ReadByte());
                        if (binaryReader.PeekChar() == -1)
                        {
                            RSACryptoServiceProvider rSACryptoServiceProvider = new RSACryptoServiceProvider();
                            RSAParameters rSAParameter = new RSAParameters()
                            {
                                Modulus = numArray1
                            };
                            Crypto2Certificate.moduloField = Encoding.UTF8.GetString(numArray1);
                            rSAParameter.Exponent = numArray2;
                            rSACryptoServiceProvider.ImportParameters(rSAParameter);
                            xmlString = rSACryptoServiceProvider.ToXmlString(false);
                        }
                        else
                        {
                            xmlString = null;
                        }
                    }
                    else
                    {
                        xmlString = null;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    xmlString = null;
                }
            }
            finally
            {
                binaryReader.Close();
            }
            return xmlString;
        }

        public string ExportarB64()
        {
            string base64String;
            if (this.certificadoX509Field != null)
            {
                base64String = Convert.ToBase64String(this.certificadoX509Field.RawData);
            }
            else
            {
                this.mensajeErrorField = "No ha cargado un certificado.";
                this.codigoErrorField = 101;
                base64String = null;
            }
            return base64String;
        }

        public void ExportarPem(string rutaArchivoPem)
        {
            if (rutaArchivoPem == "")
            {
                this.mensajeErrorField = "La ruta del archivo PEM no puede ser vacía.";
                this.codigoErrorField = 0;
            }
            else if (this.certificadoX509Field == null)
            {
                this.mensajeErrorField = "No ha cargado un certificado.";
                this.codigoErrorField = 101;
            }
            else if (Directory.Exists(Path.GetDirectoryName(rutaArchivoPem)))
            {
                rutaArchivoPem = string.Concat(Path.GetDirectoryName(rutaArchivoPem), "\\", Path.GetFileNameWithoutExtension(rutaArchivoPem));
                rutaArchivoPem = string.Concat(rutaArchivoPem, ".pem");
                try
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.AppendLine("-----BEGIN CERTIFICATE-----");
                    string base64String = Convert.ToBase64String(this.certificadoX509Field.Export(X509ContentType.Cert));
                    for (int i = 0; i < base64String.Length - 1; i += 64)
                    {
                        if (i + 64 >= base64String.Length - 1)
                        {
                            stringBuilder.AppendLine(base64String.Substring(i, base64String.Length - i));
                        }
                        else
                        {
                            stringBuilder.AppendLine(base64String.Substring(i, 64));
                        }
                    }
                    stringBuilder.AppendLine("-----END CERTIFICATE-----");
                    File.WriteAllText(rutaArchivoPem, stringBuilder.ToString(), new UTF8Encoding(false));
                    if (!File.Exists(rutaArchivoPem))
                    {
                        this.mensajeErrorField = "Error al crear el archivo. Verifique permisos de escritura en el directorio.";
                        this.codigoErrorField = 505;
                    }
                }
                catch (Exception exception)
                {
                    this.mensajeErrorField = string.Concat("Error interno al exportar certificado a PFX: ", exception.Message);
                    this.codigoErrorField = 506;
                }
            }
            else
            {
                this.mensajeErrorField = "El directorio especificado no es válido o no existe.";
                this.codigoErrorField = 201;
            }
        }

        public void ExportarPfx(string rutaArchivoPfx, string nuevaContrasenia)
        {
            byte[] numArray;
            if (rutaArchivoPfx == "")
            {
                this.mensajeErrorField = "La ruta del archivo PFX no puede ser vacía.";
                this.codigoErrorField = 113;
            }
            else if (this.certificadoX509Field == null)
            {
                this.mensajeErrorField = "No ha cargado un certificado.";
                this.codigoErrorField = 101;
            }
            else if (Crypto2Certificate.llaveField == null)
            {
                this.mensajeErrorField = "No ha cargado una llave privada al certificado.";
                this.codigoErrorField = 102;
            }
            else if (Directory.Exists(Path.GetDirectoryName(rutaArchivoPfx)))
            {
                rutaArchivoPfx = string.Concat(Path.GetDirectoryName(rutaArchivoPfx), "\\", Path.GetFileNameWithoutExtension(rutaArchivoPfx));
                rutaArchivoPfx = string.Concat(rutaArchivoPfx, ".pfx");
                try
                {
                    numArray = (!(nuevaContrasenia != "") ? this.certificadoX509Field.Export(X509ContentType.Pfx) : this.certificadoX509Field.Export(X509ContentType.Pfx, this.ToSecureString(nuevaContrasenia)));
                    File.WriteAllBytes(rutaArchivoPfx, numArray);
                    if (!File.Exists(rutaArchivoPfx))
                    {
                        this.mensajeErrorField = "Error al crear el archivo. Verifique permisos de escritura en el directorio.";
                        this.codigoErrorField = 505;
                    }
                }
                catch (Exception exception)
                {
                    this.mensajeErrorField = string.Concat("Error interno al exportar certificado a PFX: ", exception.Message);
                    this.codigoErrorField = 506;
                }
            }
            else
            {
                this.mensajeErrorField = "El directorio especificado no es válido o no existe.";
                this.codigoErrorField = 201;
            }
        }

        public string ExportarXml()
        {
            string str;
            if (this.certificadoX509Field != null)
            {
                str = Crypto2Certificate.strXmlField;
            }
            else
            {
                this.mensajeErrorField = "No ha cargado un certificado.";
                this.codigoErrorField = 101;
                str = null;
            }
            return str;
        }

        ~Crypto2Certificate()
        {
            this.certificadoX509Field = null;
            Crypto2Certificate.llaveField = null;
            GC.Collect();
        }

        private X509Certificate2 GetFilecert(string certfileName)
        {
            X509Certificate2 x509Certificate2;
            X509Certificate2 x509Certificate21 = new X509Certificate2();
            try
            {
                x509Certificate21.Import(certfileName);
                x509Certificate2 = x509Certificate21;
            }
            catch (CryptographicException cryptographicException1)
            {
                Console.WriteLine(cryptographicException1.Message);
                StreamReader streamReader = File.OpenText(certfileName);
                string end = streamReader.ReadToEnd();
                streamReader.Close();
                StringBuilder stringBuilder = new StringBuilder(end);
                stringBuilder.Replace("-----BEGIN CERTIFICATE-----", "");
                stringBuilder.Replace("-----END CERTIFICATE-----", "");
                try
                {
                    x509Certificate21 = new X509Certificate2(Convert.FromBase64String(stringBuilder.ToString()));
                    x509Certificate2 = x509Certificate21;
                }
                catch (FormatException formatException)
                {
                    Console.WriteLine(formatException.Message);
                    x509Certificate2 = null;
                }
                catch (CryptographicException cryptographicException)
                {
                    Console.WriteLine(cryptographicException.Message);
                    x509Certificate2 = null;
                }
            }
            return x509Certificate2;
        }

        private void Inicializar()
        {
            this.mensajeErrorField = "";
            this.codigoErrorField = 0;
            this.tipoCertificadoField = "";
            Crypto2Certificate.moduloField = "";
            this.validoDesdeField = "";
            this.versionField = "";
            this.emisorField = "";
            this.asuntoField = "";
            this.validoDesdeField = "";
            this.validoHastaField = "";
            this.noSerieField = "";
            this.rfcField = "";
            this.razonSocialField = "";
            this.autoridadEmisoraField = "";
            this.esFielField = false;
        }

        public void SellarArchivoXml(string rutaXml)
        {
            if (this.certificadoX509Field == null)
            {
                this.mensajeErrorField = "No ha cargado un certificado.";
                this.codigoErrorField = 101;
            }
            else if (Crypto2Certificate.llaveField == null)
            {
                this.mensajeErrorField = "No ha cargado una llave privada al certificado.";
                this.codigoErrorField = 102;
            }
            else if (rutaXml == "")
            {
                this.mensajeErrorField = "La ruta del XML no puede ser vacía.";
                this.codigoErrorField = 115;
            }
            else if (File.Exists(rutaXml))
            {
                try
                {
                    XmlDocument xmlDocument = new XmlDocument()
                    {
                        PreserveWhitespace = false
                    };
                    xmlDocument.Load(rutaXml);
                    SignedXml signedXml = new SignedXml(xmlDocument)
                    {
                        SigningKey = this.certificadoX509Field.PrivateKey
                    };
                    Reference reference = new Reference()
                    {
                        Uri = ""
                    };
                    reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
                    signedXml.AddReference(reference);
                    KeyInfoX509Data keyInfoX509Datum = new KeyInfoX509Data(this.certificadoX509Field);
                    keyInfoX509Datum.AddIssuerSerial(this.certificadoX509Field.Issuer, this.certificadoX509Field.SerialNumber.ToString());
                    signedXml.KeyInfo.AddClause(keyInfoX509Datum);
                    signedXml.ComputeSignature();
                    XmlElement xml = signedXml.GetXml();
                    xmlDocument.DocumentElement.AppendChild(xmlDocument.ImportNode(xml, true));
                    if (xmlDocument.FirstChild is XmlDeclaration)
                    {
                        xmlDocument.RemoveChild(xmlDocument.FirstChild);
                    }
                    XmlTextWriter xmlTextWriter = new XmlTextWriter(rutaXml, new UTF8Encoding(false));
                    try
                    {
                        xmlDocument.WriteTo(xmlTextWriter);
                    }
                    finally
                    {
                        if (xmlTextWriter != null)
                        {
                            ((IDisposable)xmlTextWriter).Dispose();
                        }
                    }
                }
                catch (Exception exception)
                {
                    this.mensajeErrorField = string.Concat("Error al sellar la cadena XML. ", exception.Message);
                    this.codigoErrorField = 512;
                    return;
                }
            }
            else
            {
                this.mensajeErrorField = "La ruta especificada no es válida o no existe.";
                this.codigoErrorField = 202;
            }
        }

        private string SellarCadenaCodificacion(string cadenaOriginal, string codSha)
        {
            byte[] numArray;
            string base64String;
            if (this.certificadoX509Field == null)
            {
                this.mensajeErrorField = "No ha cargado un certificado.";
                this.codigoErrorField = 101;
                base64String = null;
            }
            else if (this.certificadoX509Field.PrivateKey != null)
            {
                RSACryptoServiceProvider privateKey = (RSACryptoServiceProvider)this.certificadoX509Field.PrivateKey;
                try
                {
                    byte[] bytes = (new UTF8Encoding()).GetBytes(cadenaOriginal);
                    try
                    {
                        try
                        {
                            numArray = privateKey.SignData(bytes, CryptoConfig.MapNameToOID(codSha));
                        }
                        catch (CryptographicException cryptographicException)
                        {
                            this.mensajeErrorField = string.Concat("Error interno al sellar la cadena. ", cryptographicException.Message);
                            this.codigoErrorField = 513;
                            base64String = null;
                            return base64String;
                        }
                    }
                    finally
                    {
                        privateKey.PersistKeyInCsp = false;
                    }
                }
                finally
                {
                    if (privateKey != null)
                    {
                        ((IDisposable)privateKey).Dispose();
                    }
                }
                base64String = Convert.ToBase64String(numArray);
            }
            else
            {
                this.mensajeErrorField = "No ha cargado una llave privada al certificado.";
                this.codigoErrorField = 102;
                base64String = null;
            }
            return base64String;
        }

        public string SellarCadenaSha1(string cadenaOriginal)
        {
            return this.SellarCadenaCodificacion(cadenaOriginal, "SHA1");
        }

        public string SellarCadenaSha256(string cadenaOriginal)
        {
            return this.SellarCadenaCodificacion(cadenaOriginal, "SHA256");
        }

        public string SellarCadenaSha512(string cadenaOriginal)
        {
            return this.SellarCadenaCodificacion(cadenaOriginal, "SHA512");
        }

        public string SellarCadenaXml(string cadenaXml)
        {
            string innerXml;
            if (this.certificadoX509Field == null)
            {
                this.mensajeErrorField = "No ha cargado un certificado.";
                this.codigoErrorField = 101;
                innerXml = null;
            }
            else if (!(cadenaXml.Trim() == ""))
            {
                try
                {
                    XmlDocument xmlDocument = new XmlDocument()
                    {
                        PreserveWhitespace = false
                    };
                    xmlDocument.LoadXml(cadenaXml);
                    SignedXml signedXml = new SignedXml(xmlDocument)
                    {
                        SigningKey = this.certificadoX509Field.PrivateKey
                    };
                    Reference reference = new Reference()
                    {
                        Uri = ""
                    };
                    reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
                    signedXml.AddReference(reference);
                    KeyInfoX509Data keyInfoX509Datum = new KeyInfoX509Data(this.certificadoX509Field, X509IncludeOption.EndCertOnly);
                    keyInfoX509Datum.AddIssuerSerial(this.certificadoX509Field.Issuer, this.certificadoX509Field.SerialNumber.ToString());
                    signedXml.KeyInfo.AddClause(keyInfoX509Datum);
                    signedXml.ComputeSignature();
                    XmlElement xml = signedXml.GetXml();
                    xmlDocument.DocumentElement.AppendChild(xmlDocument.ImportNode(xml, true));
                    if (xmlDocument.FirstChild is XmlDeclaration)
                    {
                        xmlDocument.RemoveChild(xmlDocument.FirstChild);
                    }
                    innerXml = xmlDocument.InnerXml;
                }
                catch (Exception exception)
                {
                    this.mensajeErrorField = string.Concat("Error al sellar la cadena XML. ", exception.Message);
                    this.codigoErrorField = 512;
                    innerXml = null;
                }
            }
            else
            {
                this.mensajeErrorField = "Cadena de XML vacía.";
                this.codigoErrorField = 114;
                innerXml = "";
            }
            return innerXml;
        }

        private string SerialAsciItoHex(string serialAscci)
        {
            string str = "";
            string str1 = "";
            for (int i = 0; i <= serialAscci.Length - 1; i += 2)
            {
                str1 = serialAscci.Substring(i, 2);
                str = string.Concat(str, char.ConvertFromUtf32(Convert.ToInt32(str1, 16)));
            }
            return str;
        }

        private void ShowBytes(string info, byte[] data)
        {
            for (int i = 1; i <= data.Length; i++)
            {
                Console.Write("{0:X2}  ", data[i - 1]);
                if (i % 16 == 0)
                {
                }
            }
        }

        private SecureString ToSecureString(string source)
        {
            SecureString secureString;
            if (!string.IsNullOrEmpty(source))
            {
                SecureString secureString1 = new SecureString();
                char[] charArray = source.ToCharArray();
                for (int i = 0; i < charArray.Length; i++)
                {
                    secureString1.AppendChar(charArray[i]);
                }
                secureString = secureString1;
            }
            else
            {
                secureString = null;
            }
            return secureString;
        }

        public bool VerificarCadenaCertificados(bool certificadoRealSat)
        {
            bool flag;
            if (this.certificadoX509Field != null)
            {
                flag = this.VerificarCadenaCertificados2(this.certificadoX509Field, certificadoRealSat);
            }
            else
            {
                this.mensajeErrorField = "No ha cargado un certificado.";
                this.codigoErrorField = 101;
                flag = false;
            }
            return flag;
        }

        public bool VerificarCadenaCertificados1(string rutaCertificado, bool certificadoRealSat)
        {
            bool flag;
            if (File.Exists(rutaCertificado))
            {
                try
                {
                    flag = this.VerificarCadenaCertificados2(new X509Certificate2(rutaCertificado), certificadoRealSat);
                }
                catch (Exception exception)
                {
                    this.mensajeErrorField = string.Concat("Certificado inválido o incorrecto. ", exception.Message);
                    this.codigoErrorField = 511;
                    flag = false;
                }
            }
            else
            {
                this.mensajeErrorField = "La ruta especificada no es válida o no existe.";
                this.codigoErrorField = 202;
                flag = false;
            }
            return flag;
        }

        private bool VerificarCadenaCertificados2(X509Certificate2 cert, bool certificadoRealSat)
        {
            bool flag;
            string str = "";
            X509Certificate2Collection x509Certificate2Collection = new X509Certificate2Collection();
            X509Chain x509Chain = new X509Chain();
            if (!certificadoRealSat)
            {
                x509Certificate2Collection.Add(new X509Certificate2(Convert.FromBase64String("MIIElzCCA3+gAwIBAgIUMDAwMDAwMDAwMDAwMDAwMDAwMDgwDQYJKoZIhvcNAQEFBQAwgawxCzAJBgNVBAYTAk1YMQ0wCwYDVQQIDARELkYuMRkwFwYDVQQHDBBDaXVkYWQgZGUgTWV4aWNvMRgwFgYDVQQKDA9CYW5jbyBkZSBNZXhpY28xDTALBgNVBAsMBERTT1AxDDAKBgNVBAMMA0FSQzElMCMGCSqGSIb3DQEJARYWYXJjQGllcy5iYW54aWNvLm9yZy5teDEVMBMGCSqGSIb3DQEJAgwGdmFyaW9zMB4XDTEwMDYxMTE5NDEwNloXDTE3MDYxMTE5NDEwNlowggFcMRowGAYDVQQDDBFBLkMuIDIgZGUgcHJ1ZWJhczEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMSkwJwYJKoZIhvcNAQkBFhphc2lzbmV0QHBydWViYXMuc2F0LmdvYi5teDEmMCQGA1UECQwdQXYuIEhpZGFsZ28gNzcsIENvbC4gR3VlcnJlcm8xDjAMBgNVBBEMBTA2MzAwMQswCQYDVQQGEwJNWDEZMBcGA1UECAwQRGlzdHJpdG8gRmVkZXJhbDESMBAGA1UEBwwJQ295b2Fjw6FuMTQwMgYJKoZIhvcNAQkCDCVSZXNwb25zYWJsZTogQXJhY2VsaSBHYW5kYXJhIEJhdXRpc3RhMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2QEstvgBIPadUjfPd7EB8EfNDXBwyc6sLGPyBZI3k+F5z1ktN3oLM5xA8WUqn8BXf5Bq8KH12Uq96zNBOdqVjeuyDsjIOrYHDNcLKnj1KBhBOpeoi+F2x5Dk8T5I4gA6bfugZ3zihIq34gCbCILfVG6odI25r3XNxTL2u+WCXkxh3ZJ6Lp5rinDoFCcXqbdMntRehgoCNxa4Lc9xm4k33hpqGkEDI7+pxGCGtlXQ5SJpetENGBsZasUhI95wGpFeA1Enh0/NMXkSCH3FIg6UJ0q9XrQOZCFXctaQpE/3CkZBPCksbbE2BMpr3GMRAG3kNcZvBqHiaoU8jLZwGzrvtwIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQC2Io1QqQkG95Kw8B/HJoe8sbB6sQGbu8wKgUbgnjHyj/Fc7wKF/F5g8HVN4NHDC0EWSpdWTopLsFC2kCjKAtozoy1tGnGR/XBEvMBVp8TfyUgw9r+teWCkLiurP1sbVwDC9rwGXJSjEOhCySiz/1qvvTF2gyPEXP1h+IXNigeXw975TmVaWubBz23O8J0CpboTLDmY+sj/toX1w7Ollx7r0u6pK9H0F3UC7yvV964V18L+btt3pB49+q3/cbuGvDa5qf6ZBbjlWrQgzjlp0pphAax7i4qfVw/C8NT9J4Ui3gGvvlUK8+U73oZdnYxQGd9xOoovyyqHkPQVeUB/nx8U")));
                x509Certificate2Collection.Add(new X509Certificate2(Convert.FromBase64String("MIIDzjCCAragAwIBAgIUMDAwMDAwMDAwMDAwMDAwMDkwMDAwDQYJKoZIhvcNAQEFBQAwfzEYMBYGA1UECgwPQmFuY28gZGUgTWV4aWNvMQswCQYDVQQGEwJNWDElMCMGA1UEAwwcQWdlbmNpYSBSZWdpc3RyYWRvcmEgQ2VudHJhbDEvMC0GA1UECwwmSW5mcmFlc3RydWN0dXJhIEV4dGVuZGlkYSBkZSBTZWd1cmlkYWQwHhcNMDgxMDAzMjEyMzIxWhcNMTYxMDAzMjEyMzIxWjB/MRgwFgYDVQQKDA9CYW5jbyBkZSBNZXhpY28xCzAJBgNVBAYTAk1YMSUwIwYDVQQDDBxBZ2VuY2lhIFJlZ2lzdHJhZG9yYSBDZW50cmFsMS8wLQYDVQQLDCZJbmZyYWVzdHJ1Y3R1cmEgRXh0ZW5kaWRhIGRlIFNlZ3VyaWRhZDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANoRotECuCS5we7MM7adu1sP4WaE3aLC9Qn5v0q+zTGK2vwCoADqpMKGw2w2O0rw7Yx85b1mfdlWV9kUuaj/WOs75zzz0/qCuThPf87ys6Slg8As4ZcaHOY8REeArH5AgTPptamf6dmw/+80r+FYLsUa5U/qiEIdN7tvle2RO0VDcl/TcRb/mxrRqy7dLYCNnM8v2ZsPhQA3le7A1ME4nx9BeoB9RSXrdJ3O9I6jjYsxsSEHHiCyv/segjp204a6slHl2TukIQTxhQHcvg6xQH10VMQBeHLqdkJN5qCP0hLijN4Wf1k3vVdRFHhRyKFREN7dHCmuIN4IUY7Sin5gng0CAwEAAaNCMEAwHQYDVR0RBBYwFIESaWVzQGJhbnhpY28ub3JnLm14MA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgEGMA0GCSqGSIb3DQEBBQUAA4IBAQCdG75ZhgDN3n9uGCD2ochE0PkqpXM+9b3b3JB+LqbRa/meqjjlJpCkCw942qp1Jfbj+E78+R4PYwLYiiiNCMZ+X6xsa8KoUkeD1ELfr9HEY6FugLqQ1nzbO0EhyhMTLDRNbRkCv99nIJ4kcklyp5cxatrKzatzBNzJ+ZbTxCtDDrCYMVluTCiG9GAq8Adfd2tE65LkihFBKqmC4CwiYugQP/MN4IKh1jjCAYPwDiZwaqWzkjgqW0UxIyK9xLybf5+FPiAAdlEV5Qyb2SbxguB1v1qHhzQqQ6NJBsQtNfM0nBFHDp3Pk+uTzAQ27mfYSuWX6p+MTwD7XBKRmSIOVa7r")));
                x509Certificate2Collection.Add(new X509Certificate2(Convert.FromBase64String("MIIEXTCCA0WgAwIBAgIUMDAwMDAwMDAwMDAwMDAwMDAwMTAwDQYJKoZIhvcNAQEFBQAwgawxCzAJBgNVBAYTAk1YMQ0wCwYDVQQIDARELkYuMRkwFwYDVQQHDBBDaXVkYWQgZGUgTWV4aWNvMRgwFgYDVQQKDA9CYW5jbyBkZSBNZXhpY28xDTALBgNVBAsMBERTT1AxDDAKBgNVBAMMA0FSQzElMCMGCSqGSIb3DQEJARYWYXJjQGllcy5iYW54aWNvLm9yZy5teDEVMBMGCSqGSIb3DQEJAgwGdmFyaW9zMB4XDTEwMDIxNTIwMjExM1oXDTE4MDIxNTIwMjExM1owgawxCzAJBgNVBAYTAk1YMQ0wCwYDVQQIDARELkYuMRkwFwYDVQQHDBBDaXVkYWQgZGUgTWV4aWNvMRgwFgYDVQQKDA9CYW5jbyBkZSBNZXhpY28xDTALBgNVBAsMBERTT1AxDDAKBgNVBAMMA0FSQzElMCMGCSqGSIb3DQEJARYWYXJjQGllcy5iYW54aWNvLm9yZy5teDEVMBMGCSqGSIb3DQEJAgwGdmFyaW9zMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwB/i+Fp0QAp/MM8Hu4QSVZUtVeDn7yVCKkVUzEsgVaCDNAomnUcYsJMYsjFULn35MdBl15ey0l/SidoaYyCHvEYeDhPKJDosvtxXLkh0Tj1/Vz/IWOWVp14CO4RZ4nxZ7xTXugIcAiMnnwAfyA06xFvBamz3zf1TJGmO3PeS45QYNGD8MOBPeL/46LAOVuRIRkUEv/p2dGjr6yvIj68rN2G/GBu8gayakAW8Xd/NU06UXLJlA0RfNFIdW3C8cD1zUFqFP4iPFu/3zoZxUEWEYV6W/CocUrdYIClZOJQYSZ/XvIj9ESdIbdx1u09wj1toQiVuaOENPAD50U9cT06z+QIDAQABo3UwczAdBgNVHQ4EFgQUCcRAIkhrjUkvbDKAJ2b+gBndEF0wDwYDVR0TBAgwBgEB/wIBATALBgNVHQ8EBAMCAeYwEQYJYIZIAYb4QgEBBAQDAgEGMCEGA1UdEQQaMBiBFmFyY0BpZXMuYmFueGljby5vcmcubXgwDQYJKoZIhvcNAQEFBQADggEBAKokUoEZqOCPZlxgb3DVgMomvEs8bR7Il3BHOcbdwwah66BMlmCu42jlVsCVGLyJ+fbherYKM39Kt39ePnugJNTHyQAMqn+NMWu9xvB0+TZtH8144xP6CqVaHgAPkEQYTTIxvzGtJykPh3v8v9RMvWy9iduXLWG7s07obrZLrblMgw4wGj7hNsiZ5KJPAw3eXC6g/ZIZh4IE1P2IGABcloSy4EW15dQPDezRCzlgvnvjN7yscJrsiWKPHMSGXcOtpELcgDkAGIvgOD0NXcE955azbjmh1MekV0TZNpLWUJDeMEg84WvUBoWe/hnKOQ7OBqtB1EFfyMbEl0t5pVQ69g8=")));
            }
            else
            {
                x509Certificate2Collection.Add(new X509Certificate2(Convert.FromBase64String("MIIFuDCCBKCgAwIBAgIUMDAwMDAwMDAwMDAwMDAwMDEwMjIwDQYJKoZIhvcNAQEEBQAwggF4MQswCQYDVQQGEwJNWDEVMBMGA1UECBMMTWV4aWNvLCBELkYuMRkwFwYDVQQHExBDaXVkYWQgZGUgTWV4aWNvMRgwFgYDVQQKEw9CYW5jbyBkZSBNZXhpY28xIDAeBgNVBAsTF0dlcmVuY2lhIGRlIEluZm9ybWF0aWNhMRQwEgYDVQQDEwtBUkMgQmFueGljbzEVMBMGA1UELRMMQk1FODIxMTMwU1hBMSgwJgYDVQQMEx9BZG1pbmlzdHJhZG9yIENlbnRyYWwgZGUgbGEgSUVTMREwDwYGdYhdjzUREwUwNjA1OTEpMCcGBnWIXY81ExYdQXYuIDUgZGUgTWF5byAjMiwgQ29sLiBDZW50cm8xHjAcBgZ1iF2PNRcTEkFCQ0RFRjEyMzQ1Njc4SDAwMDEeMBwGBnWIXY81HRMSQUJDRDk4MDcwNlpZWFdWVTU0MSYwJAYJKoZIhvcNAQkBFhdjY29yb25hZEBiYW54aWNvLm9yZy5teDAeFw0wNDEwMjcwNTAwMDBaFw0wODEwMjcwNjAwMDBaMIIBijEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEmMCQGA1UEEBMdQXYuIEhpZGFsZ28gNzcsIENvbC4gR3VlcnJlcm8xDjAMBgNVBBETBTA2MzAwMRIwEAYDVQQHDAlDb3lvYWPDoW4xGTAXBgNVBAgTEERpc3RyaXRvIEZlZGVyYWwxCzAJBgNVBAYTAk1YMRUwEwYDVQQtEwxTQVQ5NzA3MDFOTjMxITAfBgkqhkiG9w0BCQEWEmFzaXNuZXRAc2F0LmdvYi5teDE1MDMGCSqGSIb3DQEJAhMmUmVzcG9uc2FibGU6IENlc2FyIEx1aXMgUGVyYWxlcyBUZWxsZXowggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDkULY6icoRfNXtvQdPAGlTC2IwXcblxeBWxqeI0JZLZwbx117eJGV2SG9ej/4QxSsWKxF/C1X8HJVMvpj8P/Zty79I9poAPjzknq0k/1vTj4iwhOtOx08bEilXOJFYyjZQEkW1txB+v5mIVHPEGEmhZ5J/lOQP7v3r/jwwy1eB/yclMKqHuNipKPg7xQKp2nY84T/0plk5OHKHUJsQEwnlMBk00OuBKk1lrNyuCXnuCqKs7AboK15DvMTLdzS9n8SzN31N5fO5z4LCpRyROISZhJxjyHwfKeaPTnkqW2y6S0wWoa6qMJyUeqx6v9ZyrJl21bSc/OblpWirKBahDUHbAgMBAAGjJDAiMBIGA1UdEwEB/wQIMAYBAf8CAQAwDAYDVR0PBAUDAwfmADANBgkqhkiG9w0BAQQFAAOCAQEAWDLnTC+EObvXn7hzJmWrl9L3/AQBK6l3sJjPEqZSyWUQCW9z7Hv0r6ChKA4znj9merq/+SHbC04YiK5E0qLm9SgE0XgEdETjBNH4nSVQ7h5/m0EIxANPnYFj5YhSpPMIwFMvSGYjGxAUIANdlTELhusZck5XH1O3NT3ugiczKks9EI4aUGBVzJfHzz+bj+Js84LP0vCvW6vfH9o6WozY41HV0B0MIbPeFGQpMmntwsWhHjh9mTHxHb3ggvUlUvJ5z2aafS6msF6BsR58uI8bdiufFBc+gD+5aebjau7rGPVqCy3vJ0oTn8MiQRuBMUosqIisXzQfOzu+EGiEcYxuRQ==")));
                x509Certificate2Collection.Add(new X509Certificate2(Convert.FromBase64String("MIIFHjCCBAagAwIBAgIUMDAwMDAwMDAwMDAwMDAwMDEwNDMwDQYJKoZIhvcNAQEFBQAwggE2MTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExHzAdBgkqhkiG9w0BCQEWEGFjb2RzQHNhdC5nb2IubXgxJjAkBgNVBAkMHUF2LiBIaWRhbGdvIDc3LCBDb2wuIEd1ZXJyZXJvMQ4wDAYDVQQRDAUwNjMwMDELMAkGA1UEBhMCTVgxGTAXBgNVBAgMEERpc3RyaXRvIEZlZGVyYWwxEzARBgNVBAcMCkN1YXVodGVtb2MxMzAxBgkqhkiG9w0BCQIMJFJlc3BvbnNhYmxlOiBGZXJuYW5kbyBNYXJ0w61uZXogQ29zczAeFw0wODEwMTYxODI5NDBaFw0xNjEwMTQxODI5NDBaMIIBNjE4MDYGA1UEAwwvQS5DLiBkZWwgU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExLzAtBgNVBAoMJlNlcnZpY2lvIGRlIEFkbWluaXN0cmFjacOzbiBUcmlidXRhcmlhMR8wHQYJKoZIhvcNAQkBFhBhY29kc0BzYXQuZ29iLm14MSYwJAYDVQQJDB1Bdi4gSGlkYWxnbyA3NywgQ29sLiBHdWVycmVybzEOMAwGA1UEEQwFMDYzMDAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRMwEQYDVQQHDApDdWF1aHRlbW9jMTMwMQYJKoZIhvcNAQkCDCRSZXNwb25zYWJsZTogRmVybmFuZG8gTWFydMOtbmV6IENvc3MwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDlkFD9MrqF4NDx2DRfON6QvYCxaoPYFsLIMHuRzc2FlYI4ZDYlq+OA341rfgP2UqAUgC/MXJ2dXPHm/Egkg170X0Pp2Sm8IJuKSqM9oOI+rUDtqh8iVDouvQGIkSaiQ0hMrt8btQdjMPruSwUf5t20UgsYPP9IH4QereGNGFDvjvAOqFA44t2DNQS6Bec0Tldi6s7j+gIcItXGxNbP30NrBnR+7ZmkgaQ1VJnjh2HdyvRbiOuIicK4WCl7co3OX85hNirckAG/2B4OOY5e9+1BkOF4BA8f2dTOmhb/pTqRoMhbDvqpbIqU5OgbxZmi5tpvElRVPshSKLVqNe51R6LTAgMBAAGjIDAeMA8GA1UdEwEB/wQFMAMBAf8wCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBBQUAA4IBAQDI4fg+F5xPIaXUkCfkfEhjJmxnhAf52PCqHw9NuzdMYpE0P+qO5RvfMvsS1RBMMv3v4ASQx4NeJUQia+3cCc9E69kSwVhJfY9UOAtOIFQ4W1eUBJ+WIEzbChWtL8ADi5lhsE73gmapGuxVqye+4e/HNLdTv3MzhmqS69DkbdySzRnoPMCrspxX4EU8nsFD/HnhdgNu8J5b5HV9JckM2OC3BMTPp3BhKBAlADHLSgmttxZhMoK7nW+gus1px3B2yLmorf2GUOC3kQrrrLeNBEoZgSPmkVjeL6Z+/qfctvd1LzAla4VZpXH3uQw7a2EHM18k9fczppiB4O1/ShgIiHVp")));
                x509Certificate2Collection.Add(new X509Certificate2(Convert.FromBase64String("MIIG7TCCBXmgAwIBAgIUMDAwMDAwMDAwMDAwMDAwMDEwNjYwDQYJKoZIhvcNAQEFBQAwgbcxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRMwEQYDVQQHDApDdWF1aHRlbW9jMRgwFgYDVQQKDA9CYW5jbyBkZSBNZXhpY28xJTAjBgNVBAMMHEFnZW5jaWEgUmVnaXN0cmFkb3JhIENlbnRyYWwxNzA1BgkqhkiG9w0BCQIMKFJlc3BvbnNhYmxlIEpvc2UgQW50b25pbyBIZXJuYW5kZXogQXl1c28wHhcNMTExMjE2MjAxNTE3WhcNMTkxMjE2MjAxNTE3WjCCAZUxODA2BgNVBAMML0EuQy4gZGVsIFNlcnZpY2lvIGRlIEFkbWluaXN0cmFjacOzbiBUcmlidXRhcmlhMS8wLQYDVQQKDCZTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTE4MDYGA1UECwwvQWRtaW5pc3RyYWNpw7NuIGRlIFNlZ3VyaWRhZCBkZSBsYSBJbmZvcm1hY2nDs24xITAfBgkqhkiG9w0BCQEWEmFzaXNuZXRAc2F0LmdvYi5teDEmMCQGA1UECQwdQXYuIEhpZGFsZ28gNzcsIENvbC4gR3VlcnJlcm8xDjAMBgNVBBEMBTA2MzAwMQswCQYDVQQGEwJNWDEZMBcGA1UECAwQRGlzdHJpdG8gRmVkZXJhbDEUMBIGA1UEBwwLQ3VhdWh0w6ltb2MxFTATBgNVBC0TDFNBVDk3MDcwMU5OMzE+MDwGCSqGSIb3DQEJAgwvUmVzcG9uc2FibGU6IENlY2lsaWEgR3VpbGxlcm1pbmEgR2FyY8OtYSBHdWVycmEwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCrxjRjL3QpVcZyxgasnh6ZKtCDCI+u+5tW0B5oVYsF2aAzWg/YkmkNAq/HONj+O6gByjpVQ6E9VWMh/Y62BLh4JwTO7B+fuTTX4X52Tg5v8nw+cKz6buZ8MbJfPDdyqrsKi8gikw2PqGnYC3xiXWg2Ox331xf9eCQXM+cilYqoxI1L2cUvBdwnrDj02mUJKwfkfMPRW/hmqo/9Kud4d71lU/qyWVnHi1JvrvGrmmn33DMr2lE/Lw9xJTUUUb4wrnyWkIgcg5/m9275nLLuuKOus4gXFzHCDkknl0fXxmRGVINR08fBembKcDEkogVbPJL+8INWvDZ1HVRj2F8wsS1zAgMBAAGjggGyMIIBrjAdBgNVHQ4EFgQUSYHlcY2SpLvH01i9NNL5vbrgIa8wgfcGA1UdIwSB7zCB7IAUVVOboMPjBn7RVkCDoX9+919EWXehgb2kgbowgbcxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRMwEQYDVQQHDApDdWF1aHRlbW9jMRgwFgYDVQQKDA9CYW5jbyBkZSBNZXhpY28xJTAjBgNVBAMMHEFnZW5jaWEgUmVnaXN0cmFkb3JhIENlbnRyYWwxNzA1BgkqhkiG9w0BCQIMKFJlc3BvbnNhYmxlIEpvc2UgQW50b25pbyBIZXJuYW5kZXogQXl1c2+CFDAwMDAwMDAwMDAwMDAwMDAwMDAyMA8GA1UdEwQIMAYBAf8CAQAwCwYDVR0PBAQDAgH+MCoGA1UdHwQjMCEwH6AdoBuGGWh0dHA6Ly93d3cuc2F0LmdvYi5teC9DUkwwNgYIKwYBBQUHAQEEKjAoMCYGCCsGAQUFBzABhhpodHRwOi8vd3d3LnNhdC5nb2IubXgvb2NzcDARBglghkgBhvhCAQEEBAMCAQYwDQYJKoZIhvcNAQEFBQADggFdAH8hUMoHazSaLyAy+xr/AyrCV6wyS4yhr/XFmXRI6SJ55s8DKDC9lT7ut20OTkPabIV5F4XAXDET+nHEXQxY6IVafv0GThELa3C8jZmkB4UWDDrvMIMDZdKl82+IrXpRLQN9tqNp7yLoG0OTz8LDN0Ev5gK65vIt3ANG6O42XgbC/KySY5+ssmzCo/Y9XTyz2KZsyw2VUV0UsxsBRlnfB3oetax8Q/ir4LPaARCIRZpwU95vdS7THIGN46PCvm5Ri3/pNsg0ijSUaVNPS+5RWi54Qgh25LJXLw/lr8zN2FhzpbqwVyPk4rla0VXGADEIMbK7W/vx7PyqP4YvMAHbzV/eYFiTN4mB8gYWHszkeLXUL7u1UlE21grXh2ZvEuLG9BgdvsoQeqkA4ul0mY494SdULi9LMOP1z3ZaA9SmDzPi9roUS+td31mtIRcNLh4RGynuTYtrePa3bs2kjw==")));
                x509Certificate2Collection.Add(new X509Certificate2(Convert.FromBase64String("MIIHHTCCBamgAwIBAgIUMDAwMDAwMDAwMDAwMDAwMDEwNzAwDQYJKoZIhvcNAQEFBQAwgbcxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRMwEQYDVQQHDApDdWF1aHRlbW9jMRgwFgYDVQQKDA9CYW5jbyBkZSBNZXhpY28xJTAjBgNVBAMMHEFnZW5jaWEgUmVnaXN0cmFkb3JhIENlbnRyYWwxNzA1BgkqhkiG9w0BCQIMKFJlc3BvbnNhYmxlIEpvc2UgQW50b25pbyBIZXJuYW5kZXogQXl1c28wHhcNMTMwNDI5MTY0MTU2WhcNMjEwNDI5MTY0MTU2WjCCAYoxODA2BgNVBAMML0EuQy4gZGVsIFNlcnZpY2lvIGRlIEFkbWluaXN0cmFjacOzbiBUcmlidXRhcmlhMS8wLQYDVQQKDCZTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTE4MDYGA1UECwwvQWRtaW5pc3RyYWNpw7NuIGRlIFNlZ3VyaWRhZCBkZSBsYSBJbmZvcm1hY2nDs24xHzAdBgkqhkiG9w0BCQEWEGFjb2RzQHNhdC5nb2IubXgxJjAkBgNVBAkMHUF2LiBIaWRhbGdvIDc3LCBDb2wuIEd1ZXJyZXJvMQ4wDAYDVQQRDAUwNjMwMDELMAkGA1UEBhMCTVgxGTAXBgNVBAgMEERpc3RyaXRvIEZlZGVyYWwxFDASBgNVBAcMC0N1YXVodMOpbW9jMRUwEwYDVQQtEwxTQVQ5NzA3MDFOTjMxNTAzBgkqhkiG9w0BCQIMJlJlc3BvbnNhYmxlOiBDbGF1ZGlhIENvdmFycnViaWFzIE9jaG9hMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4Vwy/gl8pY/dyJJLPa6U3f0rqGyHtb6eG1AvI/R6nB4qXuGrcXB9lGpJ21aBSD1RyvEN/cS5GvDQUM+Gzkv1+og3TZthFs/FfInW/GuqFexStJXMd/NsypgOdBJOJxj68WrbWwyhT9yl271bx8GPipuuB3dA4c0rSip51btH2fIBRFWeDA1cDudawIhgy3Z90qFF1P/rWNno7+LJ35LzB7+SZg5kPE4RFs8a4NdWs9TI2Eei/JhAS6rz3g5BDIkJEGLdYnfF67hJr2RO1BQz/Yl4aEOAyEafKSEgkzBJqT6NeZR43VKPMTyRHHbaybVYCIVQkHzJKKk58aZPiYa8NwIDAQABo4IB7TCCAekwHQYDVR0OBBYEFLwp6I5rvjE2Z4XGN6+cAvQh0kzMMIH3BgNVHSMEge8wgeyAFFVTm6DD4wZ+0VZAg6F/fvdfRFl3oYG9pIG6MIG3MQswCQYDVQQGEwJNWDEZMBcGA1UECAwQRGlzdHJpdG8gRmVkZXJhbDETMBEGA1UEBwwKQ3VhdWh0ZW1vYzEYMBYGA1UECgwPQmFuY28gZGUgTWV4aWNvMSUwIwYDVQQDDBxBZ2VuY2lhIFJlZ2lzdHJhZG9yYSBDZW50cmFsMTcwNQYJKoZIhvcNAQkCDChSZXNwb25zYWJsZSBKb3NlIEFudG9uaW8gSGVybmFuZGV6IEF5dXNvghQwMDAwMDAwMDAwMDAwMDAwMDAwMjAPBgNVHRMECDAGAQH/AgEAMAsGA1UdDwQEAwIB/jA0BgNVHSUELTArBggrBgEFBQcDAgYIKwYBBQUHAwgGCWCGSAGG+EIEAQYKKwYBBAGCNwoDAzA7BggrBgEFBQcBAQQvMC0wKwYIKwYBBQUHMAGGH2h0dHBzOi8vY2ZkaS5zYXQuZ29iLm14L2Vkb2ZpZWwwKgYDVR0fBCMwITAfoB2gG4YZaHR0cDovL3d3dy5zYXQuZ29iLm14L2NybDARBglghkgBhvhCAQEEBAMCAQYwDQYJKoZIhvcNAQEFBQADggFdAERVuw31vCU+8hGGhcg705M+jdfnJMcf456xSG/ysoF9AJ1Vt5EZPtt4kgEgC6I9wJQmdP/9MO8j9OZJRuXgvIWiE6AFuxqFQWCMLSAntXYe9iMdjbGRZZWRi1Jjvjs3u5wKYSLty5OIOM72k52FkSvrZAEQzJ95oCRFnQO5ArUfbqkX7eqG7E70ouXVc62YD6bsxnyxsfXYWEt+m6kcRZQK0mrtykcyW50CaRdVKREeruhgK4rzsbqGu+8I/xOBhJ03zSqsfqLGOA1WGH4ZK2oguaIauNCVQOwmSrDAbB5DUg07ibhr4Br2qCtbfBv0UaiHy2ug66f8z+c8pzGS1RdixmUQ4exVTewrchVSZUchyeEaBN/mOyLWuiJ1MEDUV1bPglZUHP2NY4fyOtfhtSyvpjHEHBv4rpjG7KgsCI1o2G8SwbBMXjO3AuAVgzgpFWzXNnzQrtr9Rlq5aw==")));
                x509Certificate2Collection.Add(new X509Certificate2(Convert.FromBase64String("MIIFgTCCBGmgAwIBAgIVMDAwMDAwMDAwMDAwMDAwMDAwMDAwMA0GCSqGSIb3DQEBBAUAMIIBeDELMAkGA1UEBhMCTVgxFTATBgNVBAgTDE1leGljbywgRC5GLjEZMBcGA1UEBxMQQ2l1ZGFkIGRlIE1leGljbzEYMBYGA1UEChMPQmFuY28gZGUgTWV4aWNvMSAwHgYDVQQLExdHZXJlbmNpYSBkZSBJbmZvcm1hdGljYTEUMBIGA1UEAxMLQVJDIEJhbnhpY28xFTATBgNVBC0TDEJNRTgyMTEzMFNYQTEoMCYGA1UEDBMfQWRtaW5pc3RyYWRvciBDZW50cmFsIGRlIGxhIElFUzERMA8GBnWIXY81ERMFMDYwNTkxKTAnBgZ1iF2PNRMWHUF2LiA1IGRlIE1heW8gIzIsIENvbC4gQ2VudHJvMR4wHAYGdYhdjzUXExJBQkNERUYxMjM0NTY3OEgwMDAxHjAcBgZ1iF2PNR0TEkFCQ0Q5ODA3MDZaWVhXVlU1NDEmMCQGCSqGSIb3DQEJARYXY2Nvcm9uYWRAYmFueGljby5vcmcubXgwHhcNMDEwNjIwMTYwNzU3WhcNMTEwNjIwMTYwNzU3WjCCAXgxCzAJBgNVBAYTAk1YMRUwEwYDVQQIEwxNZXhpY28sIEQuRi4xGTAXBgNVBAcTEENpdWRhZCBkZSBNZXhpY28xGDAWBgNVBAoTD0JhbmNvIGRlIE1leGljbzEgMB4GA1UECxMXR2VyZW5jaWEgZGUgSW5mb3JtYXRpY2ExFDASBgNVBAMTC0FSQyBCYW54aWNvMRUwEwYDVQQtEwxCTUU4MjExMzBTWEExKDAmBgNVBAwTH0FkbWluaXN0cmFkb3IgQ2VudHJhbCBkZSBsYSBJRVMxETAPBgZ1iF2PNRETBTA2MDU5MSkwJwYGdYhdjzUTFh1Bdi4gNSBkZSBNYXlvICMyLCBDb2wuIENlbnRybzEeMBwGBnWIXY81FxMSQUJDREVGMTIzNDU2NzhIMDAwMR4wHAYGdYhdjzUdExJBQkNEOTgwNzA2WllYV1ZVNTQxJjAkBgkqhkiG9w0BCQEWF2Njb3JvbmFkQGJhbnhpY28ub3JnLm14MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwyh3yGuhKOxGk6lQsG3F0aV+ZvqWCRoBrcp7EJX8jb0kzLcYFHq46l00m+1BFlkGmnpoFtkNHiswi3JRyuPjM1ZDdDrol/L+Hq/M5wzFuMJntsb3Sa5XN+8IyK09FKZBR84qECXkjQIkxdvlkVMW6JYydpB7M3vIPz9gBzzbF/75qiuOK36j4MukTMguWu2XOHPgCqlZCbbMJCzFWiO48u9nxJGM+Q23aJ6xFynDnU4u9D+m0+AYdzSTwhEMXB2OCJB05aGYbAQdhEDoRMiMQozyYt8fsqYrsn5ZUAo4/FvNxeKlH6ozg3pHRCjSm3mfVFiimPNFZGLFODDkc7P1AwIDAQABMA0GCSqGSIb3DQEBBAUAA4IBAQBWxfAi4kZb7Ha9oIGn5O9Sg3bpugOlEarGpqtmrTl5DWrlt8pMLVFmI4x0JAiYKdOjswE2Le9BEIwogAHfFN4m3B44JOxvsZZ6FlE+bRsIwI2EZend+rJdx2l0CEbtAub4OuppiM13Jfsb2beB7kz2Y7onD4iXR6twESR95VhjOZSwJ0JLNXjzMHy6dYHH5VrBJvCRK0UEX/ILEcRMez6dd7+7PWeCT7pixP4yaLCRP3lAAYVt+dD9ESAuOGXuqswvBj09rno+rJSHISbv1Wa6otOO52WHSHm4Q2zc7foUSkDh1dQi+HovzDF51tJ42duSpjiFtdGqDCGsF+PaKXSQ")));
                x509Certificate2Collection.Add(new X509Certificate2(Convert.FromBase64String("MIIDzjCCAragAwIBAgIUMDAwMDAwMDAwMDAwMDAwMDAwMDEwDQYJKoZIhvcNAQEFBQAwfzEYMBYGA1UECgwPQmFuY28gZGUgTWV4aWNvMQswCQYDVQQGEwJNWDElMCMGA1UEAwwcQWdlbmNpYSBSZWdpc3RyYWRvcmEgQ2VudHJhbDEvMC0GA1UECwwmSW5mcmFlc3RydWN0dXJhIEV4dGVuZGlkYSBkZSBTZWd1cmlkYWQwHhcNMDgwMzI1MDAwNjE5WhcNMTYwMzI1MDAwNjE5WjB/MRgwFgYDVQQKDA9CYW5jbyBkZSBNZXhpY28xCzAJBgNVBAYTAk1YMSUwIwYDVQQDDBxBZ2VuY2lhIFJlZ2lzdHJhZG9yYSBDZW50cmFsMS8wLQYDVQQLDCZJbmZyYWVzdHJ1Y3R1cmEgRXh0ZW5kaWRhIGRlIFNlZ3VyaWRhZDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJYhIaw3/JnJqRjAEmInnDE9/GLXe2NZlUtmKpdw9DSQB+lUr1Lca38owy7GIUUTdlaDGOn4VnL8fn4mc9wlYvj98mzWmEAm/6mM5Sh19fy0MJCiRoQtehI+rV0oRYNW5n+myyit2CEdG2RAkLTfrbXeQUnL/CJZ47ghlrwzu/FCkioPwIr/oMgMvLxAuQuGiXx1eR7tfqW46WnbwDzvp7tL29dyN9907R247q64XKu1XRV2AO21I1LalFU93n9Q8aiT2FkuU3iSbzDf++w3xfuWTHKfsDjnaOrNb+3VJdV4Y9SpXJQp81cA7L2jyQlex/wbeASF2EH/Go557X4S4ucCAwEAAaNCMEAwHQYDVR0RBBYwFIESaWVzQGJhbnhpY28ub3JnLm14MA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgEGMA0GCSqGSIb3DQEBBQUAA4IBAQBN6OrTNvG7RTvZZMQ1cqqlTS+FMaMtpJku6MpJAkxD1dPl17Wf9zEL5wpcurUl9gIKJ/DZ6a3Li5qfQxGRlTgJnxiIvwE0DYpWjnjt9DY/kVIxTsxWiTy2kJayWPDFNxPYSXXvc9ElmuX6TrnUHdY5m0qXpCpjdDkk/DGYgjlZjJOncYZSAY9anMESZ+cxYTxSS2Oot8lTIRhLR8RUgovMP8zN1ptKEKE9D8k/ZGhT5U8k2blvKdKZMzN+3zEnQrUdWrOAHMDZaBIhPUz10ckY4p/wBdErEQodjSpwBPcoyZ24UVGonhcejJKLxH/Fgu5Ewhr8h0U/pW1yndLYiifi")));
                x509Certificate2Collection.Add(new X509Certificate2(Convert.FromBase64String("MIIFCDCCA5SgAwIBAgIUMDAwMDAwMDAwMDAwMDAwMDAwMDIwDQYJKoZIhvcNAQEFBQAwgbcxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRMwEQYDVQQHDApDdWF1aHRlbW9jMRgwFgYDVQQKDA9CYW5jbyBkZSBNZXhpY28xJTAjBgNVBAMMHEFnZW5jaWEgUmVnaXN0cmFkb3JhIENlbnRyYWwxNzA1BgkqhkiG9w0BCQIMKFJlc3BvbnNhYmxlIEpvc2UgQW50b25pbyBIZXJuYW5kZXogQXl1c28wHhcNMTAwNzIwMTgzMjUxWhcNMjYwNzIwMTgzMjUxWjCBtzELMAkGA1UEBhMCTVgxGTAXBgNVBAgMEERpc3RyaXRvIEZlZGVyYWwxEzARBgNVBAcMCkN1YXVodGVtb2MxGDAWBgNVBAoMD0JhbmNvIGRlIE1leGljbzElMCMGA1UEAwwcQWdlbmNpYSBSZWdpc3RyYWRvcmEgQ2VudHJhbDE3MDUGCSqGSIb3DQEJAgwoUmVzcG9uc2FibGUgSm9zZSBBbnRvbmlvIEhlcm5hbmRleiBBeXVzbzCCAX4wDQYJKoZIhvcNAQEBBQADggFrADCCAWYCggFdAJcZunyAhNIf31jKkd5KLMqMmzDvRZvsXdnHbOrjS7ereOg2G+xsjZ8Mu4uWIpEe77m9EmijfgdrQXhDEMcGb+Ga1qE/zx/P4NyuXAcNg2QQ9AEx0obRAFtMquxS86Of8y3OBBgPhGbonn714Eo72p9Gl74FR5wNb4GxlmM8xHymUXUsthosF8eU98UOVaPuoC284WpTI29uvFKVotd63L6LaGzSh/eh3iioI9Qxd0XCqWoEGgp0zzq1K2EPVgrzqCiUOUz2vfSMbXtNEo0BhA2qgp7LJ85MefmWWF5TRbaSpYDvAnjvSeTYET9Ta6hQKNczghPEFl0UIVGau2gaiebUf7K+YGY0Qkq0HTzOH7uX31yRDszOXUEGO8xLaLiJGBtjlG/UPddUEUTvytnFQD7I7EV1FfUBBMwtjPkyiDQZ/AouxdY3ojjX+KbKo3VQ+PMF9FBllWRVqrkoeQIDAQABo1IwUDAdBgNVHQ4EFgQUVVOboMPjBn7RVkCDoX9+919EWXcwDwYDVR0TBAgwBgEB/wIBATALBgNVHQ8EBAMCAeYwEQYJYIZIAYb4QgEBBAQDAgEGMA0GCSqGSIb3DQEBBQUAA4IBXQAhrVVcqJuhC55y/Gst0LhaPZ+Ikb6IHFEyuitaPbXxczO6KeJ/X2uRTuoXEaYEIz8PvDTpB3Qpx17sXaVJJEIrlRIi7DK3bjuNM90fo2Ua1vCEvVRyaKQFTtiCwWUymOrBtRiL0Q9HQyfD/0fQxjAf6uWWhsM5KsWtH3/kvYrAbveibPV4cNGdbhwOb5YNYFvx8GtjjQ+QncNB2Xpo3PGDkpIEkY4MGHm/vqOiHXreOYJvjNNhMfMtZ9mP5E9awOgoRCuZtwBv7FQdCK8vqi8oOT1U7FwMmJ6RJrSWDoplmgbzEWtyHhkyyV7SO0wnUXWrySf9JWtmK5XAzmDgFAPWRYX3PX64sI0pBjOGPBwkYwkzjYcc+ldbEzqBAsoO7PKc0q9NwwNhIl98rfiuXsE/JRBlMSOKiNgdcACFC6nTVG1TTq7OrtQEmhzRJLj7zPopU4xzwHvJgTYrFGM=")));
            }
            x509Chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
            x509Chain.ChainPolicy.RevocationMode = X509RevocationMode.NoCheck;
            x509Chain.ChainPolicy.VerificationFlags = X509VerificationFlags.IgnoreCtlNotTimeValid | X509VerificationFlags.IgnoreInvalidBasicConstraints | X509VerificationFlags.AllowUnknownCertificateAuthority | X509VerificationFlags.IgnoreWrongUsage | X509VerificationFlags.IgnoreEndRevocationUnknown | X509VerificationFlags.IgnoreCtlSignerRevocationUnknown | X509VerificationFlags.IgnoreCertificateAuthorityRevocationUnknown | X509VerificationFlags.IgnoreRootRevocationUnknown;
            x509Chain.ChainPolicy.RevocationMode = X509RevocationMode.NoCheck;
            x509Chain.ChainPolicy.RevocationFlag = X509RevocationFlag.ExcludeRoot;
            x509Chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 0, 30);
            x509Chain.ChainPolicy.ExtraStore.AddRange(x509Certificate2Collection);
            x509Chain.Build(cert);
            if (x509Chain.ChainStatus.Length == 0)
            {
                str = "";
            }
            else
            {
                X509ChainStatus[] chainStatus = x509Chain.ChainStatus;
                for (int i = 0; i < chainStatus.Length; i++)
                {
                    X509ChainStatus x509ChainStatu = chainStatus[i];
                    str = string.Concat(str, x509ChainStatu.Status.ToString(), " - ", x509ChainStatu.StatusInformation);
                }
            }
            str = str.Replace("UntrustedRoot - Se procesó correctamente una cadena de certificados, pero termina en un certificado de raíz no compatible con el proveedor de confianza.\r\n", "");
            if (!certificadoRealSat)
            {
                str = str.Replace("InvalidBasicConstraints - No se ha cumplido con una extensión de delimitación básica de certificado.\r\n", "");
            }
            x509Certificate2Collection.Clear();
            x509Certificate2Collection = null;
            x509Chain.Reset();
            x509Chain = null;
            GC.Collect();
            if (!(str == ""))
            {
                this.mensajeErrorField = string.Concat("Errores de verificación de cadena de certificados: ", str);
                this.codigoErrorField = 315;
                flag = false;
            }
            else
            {
                flag = true;
            }
            return flag;
        }

        private void WriteKeyBlob(string keyblobfile, byte[] keydata)
        {
            FileStream fileStream = null;
            if (!File.Exists(keyblobfile))
            {
                try
                {
                    try
                    {
                        fileStream = new FileStream(keyblobfile, FileMode.CreateNew);
                        fileStream.Write(keydata, 0, keydata.Length);
                    }
                    catch (Exception exception)
                    {
                        this.mensajeErrorField = string.Concat("Error al escribir en método WriteKeyBlob. ", exception.Message);
                    }
                }
                finally
                {
                    fileStream.Close();
                }
            }
        }

        private void CatalogoErrores()
        {
            this.errores = new Dictionary<int, string>();
            this.errores.Add(0, "La ruta del archivo PEM no puede ser vacía.");
            this.errores.Add(101, "No ha cargado un certificado.");
            this.errores.Add(102, "No ha cargado una llave privada al certificado.");
            this.errores.Add(111, "La ruta del certificado no puede ser vacía.");
            this.errores.Add(112, "La cadena del certificado en base 64 no puede ser vacía.");
            this.errores.Add(113, "La ruta del archivo PFX no puede ser vacía.");
            this.errores.Add(114, "Cadena de XML vacía.");
            this.errores.Add(115, "La ruta del XML no puede ser vacía.");
            this.errores.Add(201, "El directorio especificado no es válido o no existe.");
            this.errores.Add(202, "La ruta especificada no es válida o no existe.");
            this.errores.Add(211, "Ruta de archivo de certificado inválida o no existe.");
            this.errores.Add(311, "Archivo de certificado X509 no válido.");
            this.errores.Add(312, "Buffer del certificado incorrecto. ");
            this.errores.Add(313, "Certificado en formato base 64 no válido. ");
            this.errores.Add(314, "Error de correspondencia entre certificado y llave privada. ");
            this.errores.Add(505, "Error al crear el archivo. Verifique permisos de escritura en el directorio.");
            this.errores.Add(506, "Error interno al exportar certificado a PFX: ");
            this.errores.Add(511, "Certificado inválido o incorrecto. ");
            this.errores.Add(512, "Error al sellar la cadena XML. ");
            this.errores.Add(513, "Error interno al sellar la cadena. ");
            this.errores.Add(514, "Error al leer las propiedades del certificado. ");
        }
    }
}