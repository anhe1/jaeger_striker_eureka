﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------
// 
// Este código fuente fue generado automáticamente por xsd, Versión=4.6.1055.0.
// 

namespace Jaeger.Retenciones.Complemento.EnajenacionDeAcciones.V10
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/1/enajenaciondeacciones")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/1/enajenaciondeacciones", IsNullable = false)]
    public partial class EnajenaciondeAcciones
    {
        private string versionField;
    
        private string contratoIntermediacionField;
    
        private decimal gananciaField;
    
        private decimal perdidaField;
    
        public EnajenaciondeAcciones()
        {
            this.versionField = "1.0";
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ContratoIntermediacion
        {
            get
            {
                return this.contratoIntermediacionField;
            }
            set
            {
                this.contratoIntermediacionField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Ganancia
        {
            get
            {
                return this.gananciaField;
            }
            set
            {
                this.gananciaField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Perdida
        {
            get
            {
                return this.perdidaField;
            }
            set
            {
                this.perdidaField = value;
            }
        }
    }
}
