namespace Jaeger.Retenciones.Complemento.Intereses.V10
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/1/intereses")]
    public enum InteresesOperFinancDerivad
    {
        /// <remarks/>
        SI,
    
        /// <remarks/>
        NO,
    }
}