namespace Jaeger.Retenciones.Complemento.Dividendos.V10
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/1/dividendos")]
    public enum DividendosDividOUtilTipoSocDistrDiv
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("Sociedad Nacional")]
        SociedadNacional,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("Sociedad Extranjera")]
        SociedadExtranjera,
    }
}