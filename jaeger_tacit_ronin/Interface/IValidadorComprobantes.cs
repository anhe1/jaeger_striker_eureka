/// develop: anhe1 251020182320
/// purpose: interface para validación de comprobantes fiscales
using Jaeger.Edita.V2.Validador.Entities;
using System.Collections.Generic;

namespace Jaeger.Interface
{
    public interface IValidadorComprobantes
    {
        Configuracion Configuracion { get; set; }

        ValidationProperty Esquemas(object objeto);

        ValidationProperty Estructura(object objeto, byte[] b);

        List<ValidationProperty> SelloCFDI(object objeto);

        ValidationProperty SelloSAT(object objeto);

        ValidationProperty MetodoPago(object objeto);

        ValidationProperty FormaPago(object objeto);

        ValidationProperty EstadoSAT(object objeto);

        ValidationProperty LugarExpedicion(object objeto);

        ValidationProperty UsodeCFDI(object objeto);

        List<ValidationProperty> ProductosServicios(object objeto);

        List<ValidationProperty> Articulo69B(object objeto);
    }
}