﻿using Jaeger.CFDI.V33;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Edita.V2.CFDI.Entities.Cancel;
using Jaeger.Edita.V2.Validador.Entities;

namespace Jaeger.Interface
{
    public interface IHelperCertificacion
    {
        #region propiedades

        int Codigo { get; set; }
        string Mensaje { get; set; }
        string CerBase64 { get; set; }
        string KeyBase64 { get; set; }
        string PassKey { get; set; }
        IServiceProvider Settings { get; set; }
        
        #endregion

        Comprobante Timbrar(string input);
        CancelaCFDResponse Cancelar(string uuid);
        ValidateResponse Validar(string inputXml);
    }
}
