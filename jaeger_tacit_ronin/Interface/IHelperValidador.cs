using System;
using System.Threading.Tasks;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.SAT.Entities;

namespace Jaeger.Interface
{
    public interface IHelperValidador
    {
        //Configuracion Configuracion { get; set; }

        /// <summary>
        /// Comprobar estado del comprobante
        /// </summary>
        Task<SatQueryResult> ExecuteEstadoSAT(int index);

        bool ExecuteBackUp(int index);

        /// <summary>
        /// verificacion de comprobantes
        /// </summary>
        void ExecuteVerifica(int index);

        void ExecuteVerifica();

        int SearchRFC(ViewModelContribuyente objeto);

        int SearchNominaReceptorRFC(ComplementoNominaReceptor objeto);

        string BackupS3(DocumentoAnexo item, string keyname, int index, string folioFiscal);

        /// <summary>
        /// registrar comprobante en la base de datos
        /// </summary>
        bool ExecuteRegistra(int index);

        /// <summary>
        /// ejecutar registro del comprobante en la base de datos y backup en S3
        /// </summary>
        void ExecuteRegistra();
    }
}