﻿using System.Collections.Generic;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Helpers;
using SqlSugar;

namespace Jaeger.Interface
{
    public interface ISQLiteContext<T> where T : class, new()
    {
        SimpleClient<T> CurrentDb { get; }
        SqlSugarClient Db { get; }
        SQLiteContext<T>.SQLiteMessage Message { get; set; }
        DataBaseConfiguracion Settings { get; set; }

        SQLiteContext<T>.SQLiteMessage Create();
        bool CreateDB();
        string CreateGuid(string[] datos);
        bool Delete(int id);
        T GetById(int id);
        List<T> GetList();
        bool Insert(List<T> items);
        int Insert(T item);
        int Update(T objeto);
    }
}