﻿/// <summary>
/// develop: anhe 180720192216
/// purpose: interface para la implementacion de catalogos
/// </summary>

namespace Jaeger.Interface
{
    public interface ISQLiteCatalogo<T>
    {
        T Search(string clave);
    }
}
