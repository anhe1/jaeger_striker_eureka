﻿using System.IO;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.Interface
{
    public interface IHelperCFDReader
    {
        RelacionCFDICatalogo CatalogoRelacionCFDI { get; set; }

        Jaeger.Edita.V2.Validador.Entities.Documento Reader(FileInfo archivo);

        Jaeger.Edita.V2.Validador.Entities.Documento ReaderSerializer(FileInfo archivo);
    }
}
