﻿using System;
using System.Linq;

namespace Jaeger.Entities
{
    public class Certificado : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string version;
        private string autoridadEmisora;
        private string asunto;
        private string emisor;
        private bool esFiel;
        private string noSerie;
        private string razonSocial;
        private string rfc;
        private string tipoCertificado;
        private DateTime desde;
        private DateTime hasta;

        public string Version
        {
            get
            {
                return this.version;
            }
            set
            {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        public string AutoridadEmisora
        {
            get
            {
                return this.autoridadEmisora;
            }
            set
            {
                this.autoridadEmisora = value;
                this.OnPropertyChanged();
            }
        }

        public string Asunto
        {
            get
            {
                return this.asunto;
            }
            set
            {
                this.asunto = value;
                this.OnPropertyChanged();
            }
        }

        public string Emisor
        {
            get
            {
                return this.emisor;
            }
            set
            {
                this.emisor = value;
                this.OnPropertyChanged();
            }
        }

        public bool EsFiel
        {
            get
            {
                return this.esFiel;
            }
            set
            {
                this.esFiel = value;
                this.OnPropertyChanged();
            }
        }

        public string NoSerie
        {
            get
            {
                return this.noSerie;
            }
            set
            {
                this.noSerie = value;
                this.OnPropertyChanged();
            }
        }

        public string RazonSocial
        {
            get
            {
                return this.razonSocial;
            }
            set
            {
                this.razonSocial = value;
            }
        }

        public string RFC
        {
            get
            {
                return this.rfc;
            }
            set
            {
                this.rfc = value;
                this.OnPropertyChanged();
            }
        }

        public string TipoCertificado
        {
            get
            {
                return this.tipoCertificado;
            }
            set
            {
                this.tipoCertificado = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime Desde
        {
            get
            {
                return this.desde;
            }
            set
            {
                this.desde = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime Hasta
        {
            get
            {
                return this.hasta;
            }
            set
            {
                this.hasta = value;
                this.OnPropertyChanged();
            }
        }
    }
}
