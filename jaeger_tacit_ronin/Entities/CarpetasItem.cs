using Jaeger.Domain.Base.Abstractions;
using Newtonsoft.Json;

namespace Jaeger.Entities
{
    [JsonObject("item")]
    public partial class CarpetaItem : BasePropertyChangeImplementation
    {
        private string carpetaField;

        [JsonProperty("path")]
        public string Carpeta
        {
            get
            {
                return this.carpetaField;
            }
            set
            {
                this.carpetaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public string Accion
        {
            get
            {
                return "Quitar";
            }
        }
    }
}