﻿using System;
using System.Xml;
using Jaeger.CFDI.V33;
using Jaeger.Edita.V2.CFDI.Entities.Cancel;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Helpers;
using Jaeger.com.FC.Service;
using Jaeger.Interface;

namespace Jaeger.Certificacion
{
    public class HelperFiscoClic : Jaeger.Edita.V2.Empresa.Entities.SettingsBasicPac, IHelperCertificacion
    {
        // credenciales para pruebas
        private readonly string usuarioTesting = "AAA111111ZZZ";
        private readonly string passwordTesting = "TeStInGfIsCoClIc2012Ws";

        private int errorCodigoField;
        private string errorMensajeField;

        public HelperFiscoClic(Domain.Empresa.Contracts.IServiceProvider conf)
        {
            this.Settings = conf;
        }

        #region propiedades

        /// <summary>
        /// codigo de error
        /// </summary>
        public int Codigo
        {
            get
            {
                return this.errorCodigoField;
            }
            set
            {
                this.errorCodigoField = value;
            }
        }

        /// <summary>
        /// mensaje de error
        /// </summary>
        public string Mensaje
        {
            get
            {
                return this.errorMensajeField;
            }
            set
            {
                this.errorMensajeField = value;
            }
        }

        #endregion

        #region timbrar comprobante

        public Comprobante Timbrar(string input)
        {
            if (this.Settings.Production == false)
            {
                return this.Timbrado_Testing(input);
            }
            else
            {
                return this.Timprado_Produccion(input);
            }
        }

        public Comprobante Timbrado_Testing(string inputXml)
        {
            string stringResponse = string.Empty;
            cfdiServiceInterfaceClient interfaceCliente = new cfdiServiceInterfaceClient();
            
            try
            {
                // este servicio unicamente nos regresa el timbre fiscal
                stringResponse = interfaceCliente.timbraCFDIXMLTest(inputXml, this.usuarioTesting, this.passwordTesting);

                if (string.IsNullOrEmpty(stringResponse) == false)
                {
                    Jaeger.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital timbre = Jaeger.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital.Deserialize(stringResponse);
                    //Helper.HelperArchivos.WriteFileText(System.IO.Path.Combine(this.rutaTemporalesField, string.Concat(timbre.UUID, ".xml")), stringResponse);
                    Jaeger.Util.Helpers.HelperFiles.WriteFileText(JaegerManagerPaths.JaegerPath(Jaeger.Edita.Enums.EnumPaths.Temporal, string.Concat(timbre.UUID, ".xml")), stringResponse);
                    Comprobante comprobante = Comprobante.LoadXml(inputXml);
                    comprobante.Complemento = new ComprobanteComplemento();
                    comprobante.Complemento.TimbreFiscalDigital = timbre;
                    if (comprobante != null)
                    {
                        comprobante.Save(JaegerManagerPaths.JaegerPath(Jaeger.Edita.Enums.EnumPaths.Temporal, string.Concat(timbre.UUID, ".xml")));
                        this.errorCodigoField = 0;
                        this.errorMensajeField = null;
                        return comprobante;
                    }
                }
            }
            catch (Exception ex)
            {
                this.errorCodigoField = 1;
                this.errorMensajeField = ex.Message;
            }
            return null;
        }

        public Comprobante Timprado_Produccion(string inputXml)
        {
            string stringResponse = string.Empty;
            cfdiServiceInterfaceClient interfaceCliente = new cfdiServiceInterfaceClient();
            try
            {
                // este servicio unicamente nos regresa el timbre fiscal
                this.Settings.User = "IPR981125PN9";
                this.Settings.Pass = "aIm21dInGSMib59gaMmy";
                stringResponse = interfaceCliente.timbraCFDIXML(inputXml, this.Settings.User, this.Settings.Pass);

                if (string.IsNullOrEmpty(stringResponse) == false)
                {
                    Jaeger.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital timbre = Jaeger.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital.Deserialize(stringResponse);
                    Jaeger.Util.Helpers.HelperFiles.WriteFileText(JaegerManagerPaths.JaegerPath(Jaeger.Edita.Enums.EnumPaths.Comprobantes, string.Concat(timbre.UUID, ".xml")), stringResponse);
                    Comprobante comprobante = Comprobante.LoadXml(inputXml);
                    comprobante.Complemento = new ComprobanteComplemento();
                    comprobante.Complemento.TimbreFiscalDigital = timbre;
                    if (comprobante != null)
                    {
                        comprobante.Save(JaegerManagerPaths.JaegerPath(Jaeger.Edita.Enums.EnumPaths.Comprobantes, string.Concat(timbre.UUID, ".xml")));
                        this.errorCodigoField = 0;
                        this.errorMensajeField = null;
                        return comprobante;
                    }
                }
            }
            catch (Exception ex)
            {
                this.errorCodigoField = 1;
                this.errorMensajeField = ex.Message;
            }
            return null;
        }

        #endregion

        #region cancelar comprobante

        public CancelaCFDResponse Cancelar(string uuid)
        {
            if (this.Settings.Production == false)
            {
                return this.Cancelar_Testing(uuid);
            }
            else
            {
                return this.Cancelar_Produccion(uuid);
            }
        }
 
        private CancelaCFDResponse Cancelar_Testing(string uuid)
        {
            string stringResponse = string.Empty;
            bool boolResponse = false;

            cfdiServiceInterfaceClient interfaceCliente = new cfdiServiceInterfaceClient();
            try
            {
                boolResponse = interfaceCliente.cancelaCFDITest(uuid, this.Settings.RFC, this.usuarioTesting, this.passwordTesting);
                if (boolResponse == true)
                {
                    stringResponse = interfaceCliente.cancelaCFDIAcuseTest(uuid, this.Settings.RFC, this.usuarioTesting, this.passwordTesting);
                    Console.WriteLine(stringResponse);
                }
            }
            catch (Exception)
            {
                
            }
            return null;
        }

        private CancelaCFDResponse Cancelar_Produccion(string uuid)
        {
            string stringResponse = string.Empty;
            cfdiServiceInterfaceClient interfaceCliente = new cfdiServiceInterfaceClient();
            interfaceCliente = null;
            Console.WriteLine(uuid + stringResponse + interfaceCliente.ToString());
            return null;
        }

        #endregion

        #region validacion

        public ValidateResponse Validar(string inputXml) 
        {
            cfdiServiceInterfaceClient portClient = new cfdiServiceInterfaceClient();
            ValidateResponse validateResponse = new ValidateResponse();
            validateResponse.Provider = "";
            validateResponse.DateTime = DateTime.Now;
            string stringResponse = string.Empty;
            DateTime inicio = DateTime.Now;

            if (this.Settings.Production == false)
            {
                try
                {
                    stringResponse = portClient.validaComprobanteTest(inputXml, this.usuarioTesting, this.passwordTesting, true);
                }
                catch (Exception)
                {
                    
                }
            }
            else
            {
                try
                {
                    stringResponse = portClient.validaComprobante(inputXml, this.Settings.User, this.Settings.Pass, true);
                }
                catch (Exception)
                {
                    
                }
            }

            XmlDocument xmlResponse = new XmlDocument();
            try
            {
                xmlResponse.LoadXml(stringResponse);
                foreach (XmlNode item in xmlResponse["ResultadoValidacion"])
                {
                    if (item.Name == "nodo")
                    {
                        if (item.Attributes["tipo"].Value == "bandera")
                        {
                            validateResponse.Validations.Add(new ValidationProperty { Code = "-1", Name = item.Attributes["nombre"].Value, Value = (item.Attributes["valor"].Value == "1" ? "Válido" : "No válido") });
                        }
                        else if (item.Attributes["tipo"].Value == "datos")
                        {
                            validateResponse.Validations.Add(new ValidationProperty { Code = "-1", Name = item.Attributes["nombre"].Value, Value = item.Attributes["valor"].Value });
                        }
                    }
                    else if (item.Name == "banderaCorrecto")
                    {
                        validateResponse.Validations.Add(new ValidationProperty { Code = "-1", Name = "Comprobante", Value = (item.InnerText.ToString() != "" ? true : false).ToString() });
                    }
                    else if (item.Name == "validacionesRestantes")
                    {

                    }
                    else if (item.Name == "RFCEmisor")
                    {

                    }
                }
            }
            catch (Exception)
            {
                
            }
            DateTime fin = DateTime.Now;
            string totalMin = string.Empty;
            TimeSpan total = new TimeSpan(fin.Ticks - inicio.Ticks);
            totalMin = string.Concat(total.Hours.ToString("00"), ":", total.Minutes.ToString("00"), ":", total.Seconds.ToString("00"), ".", total.Milliseconds.ToString());
            validateResponse.TimeElapsed = totalMin;
            return validateResponse;
        }
        #endregion
    }
}
