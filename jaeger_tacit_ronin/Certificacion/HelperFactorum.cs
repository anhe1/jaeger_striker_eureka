﻿using System;
using Jaeger.CFDI.V33;
using Jaeger.Edita.V2.CFDI.Entities.Cancel;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Helpers;
using Jaeger.Interface;

namespace Jaeger.Certificacion
{
    public class HelperFactorum : Edita.V2.Empresa.Entities.SettingsBasicPac, IHelperCertificacion
    {
        // credenciales para pruebas
        private readonly string usuarioTesting = "prueba1@factorum.com.mx";
        private readonly string passwordTesting = "prueba2011";
        private readonly string rfcTesting = "LAN7008173R5";

        private int errorCodigoField;
        private string errorMensajeField;

        public HelperFactorum(Domain.Empresa.Contracts.IServiceProvider conf)
        {
            this.Settings = conf;
        }

        #region propiedades

        /// <summary>
        /// codigo de error
        /// </summary>
        public int Codigo
        {
            get
            {
                return this.errorCodigoField;
            }
            set
            {
                this.errorCodigoField = value;
            }
        }

        /// <summary>
        /// mensaje de error
        /// </summary>
        public string Mensaje
        {
            get
            {
                return this.errorMensajeField;
            }
            set
            {
                this.errorMensajeField = value;
            }
        }

        #endregion

        #region timbrar comprobante

        public Comprobante Timbrar(string input)
        {
            if (this.Settings.Production == false)
            {
                return this.Timbrado_Testing(input);
            }
            else
            {
                return this.Timprado_Produccion(input);
            }
        }

        private Comprobante Timbrado_Testing(string input)
        {
            com.FAC.Timbre.ReturnFactorumWS response = new com.FAC.Timbre.ReturnFactorumWS();
            com.FAC.Timbre.FactorumCFDiServiceSoapClient soapClient = new com.FAC.Timbre.FactorumCFDiServiceSoapClient();
            try
            {
                response = soapClient.FactorumGenYaSelladoTest(this.usuarioTesting, this.rfcTesting, this.passwordTesting, input);
                if (response != null)
                {
                    // por cualquier cosa guardamos primero el archivo de manera temporal
                    Jaeger.Util.Helpers.HelperFiles.WriteFileByte(response.ReturnFileXML, JaegerManagerPaths.JaegerPath(Jaeger.Edita.Enums.EnumPaths.Temporal, string.Concat(response.UUID, ".xml")));
                    Comprobante comprobante = Comprobante.LoadBytes(response.ReturnFileXML);
                    if (comprobante != null)
                    {
                        this.errorCodigoField = 0;
                        this.errorMensajeField = null;
                        return comprobante;
                    }
                }
            }
            catch (Exception ex)
            {
                this.errorCodigoField = 1;
                this.errorMensajeField = ex.Message;
            }
            return null;
        }

        private Comprobante Timprado_Produccion(string input)
        {
            Console.WriteLine(input);
            return null;
        }

        #endregion

        #region cancelar comprobante

        public CancelaCFDResponse Cancelar(string uuid)
        {
            if (this.Settings.Production == false)
            {
                return this.Cancelar_Testing(uuid);
            }
            else
            {
                return this.Cancelar_Produccion(uuid);
            }
        }
 
        private CancelaCFDResponse Cancelar_Testing(string uuid)
        {
            // TODO: Implement this method
            Console.WriteLine(uuid);
            throw new NotImplementedException();
        }

        private CancelaCFDResponse Cancelar_Produccion(string uuid)
        {
            Console.WriteLine(uuid);
            throw new NotImplementedException();
        }

        #endregion

        #region validacion

        public ValidateResponse Validar(string inputXml) 
        {
            com.FAC.Valida.WSValidaXMLSoapClient soapClient = new com.FAC.Valida.WSValidaXMLSoapClient();
            try
            {
                com.FAC.Valida.ReturnService response = soapClient.ValidaXMLparaWebService(this.usuarioTesting, this.rfcTesting, this.passwordTesting, System.Text.Encoding.UTF8.GetBytes(inputXml), true);
                if (response != null)
                {
                    ValidateResponse validateResponse = new ValidateResponse();
                    if (response.Codigo == "200")
                    {
                        validateResponse.IsValid = Enums.EnumValidateResult.Valido;
                        foreach (string item in response.Resultado)
                        {
                            validateResponse.Validations.Add(new ValidationProperty { Type = Enums.EnumPropertyType.Information, Name = "1111", Value = item });
                        }
                        return validateResponse;
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            return null;
        }
        #endregion
    }
}
