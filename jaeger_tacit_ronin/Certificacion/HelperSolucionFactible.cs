﻿// develop: 310120180127
// purpose: clase para certificacion y cancelacion con el proveedor SolucionFactible
// Cancelación:
//    200 - El proceso de cancelación se ha completado correctamente.
//    500 - Han ocurrido errores que no han permitido completar el proceso de cancelación.
//    601 - Error de autenticación,el nombre de usuario o contraseña son incorrectos.
//    602 - La cuenta de usuario se encuentra bloqueada.
//    603 - La contraseña de la cuenta ha expirado.
//    604 - Se ha superado el número máximo permitido de intentos fallidos de autenticación.
//    605 - El usuario se encuentra inactivo
//    611 - No se han proporcionado UUIDs a cancelar.
//    620 - Permiso denegado.
//    1701 - La llave privada y la llave pública del CSD no corresponden.
//    1702 - La llave privada de la contraseña es incorrecta.
//    1703 - La llave privada no cumple con la estructura esperada.
//    1704 - La llave Privada no es una llave RSA.
//    1710 - La estructura del certificado no cumple con la estructura X509 esperada.
//    1711 - El certificado no esá vigente todavía.
//    1712 - El certificado ha expirado.
//    1713 - La llave pública contenida en el certificado no es una llave RSA.
//    1803 - El dato no es un UUID válido.
//
// Códigos de status de cancelación de CFDI:
//    200 - El proceso de cancelación se ha completado correctamente.
//    500 - Han ocurrido errores que no han permitido completar el proceso de cancelación.
//
// Códigos de respuesta del SAT para la cancelación de CFDI:
//    201 - El folio se ha cancelado con éxito.
//    202 - El CFDI ya había sido cancelado previamente.
//    203 - UUID no corresponde al emisor.
//    204 - El CFDI no aplica para cancelación.
//    205 - El UUID no existe o no ha sido procesado por el SAT.
//    402 - El Contribuyente no se encuentra el la LCO o la validez de obligaciones se reporta como negativa.
// Nota: Sólo se puede tener certeza de que un CFDI fue cancelado cuando el statusUUID es 201 (Se ha cancelado el CFDI) o 202 (El CFDI ya había sido cancelado previamente).

using System;
using System.Text;
using System.Xml;
using Jaeger.CFDI.V33;
using Jaeger.Edita.V2.CFDI.Entities.Cancel;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Helpers;
using Jaeger.Interface;

namespace Jaeger.Certificacion
{
    public class HelperSolucionFactible : Jaeger.Edita.V2.Empresa.Entities.SettingsBasicPac, IHelperCertificacion
    {
        /// credenciales para pruebas
        private readonly string usuarioTesting = "testing@solucionfactible.com";
        private readonly string passwordTesting = "timbrado.SF.16672";

        private int errorCodigoField;
        private string errorMensajeField;
        /// <summary>
        /// constructor
        /// </summary>
        public HelperSolucionFactible(Domain.Empresa.Contracts.IServiceProvider conf) 
        {
            this.Settings = conf;
        }

        #region propiedades

        /// <summary>
        /// codigo de error
        /// </summary>
        public int Codigo
        {
            get
            {
                return this.errorCodigoField;
            }
            set
            {
                this.errorCodigoField = value;
            }
        }

        /// <summary>
        /// mensaje de error
        /// </summary>
        public string Mensaje
        {
            get
            {
                return this.errorMensajeField;
            }
            set
            {
                this.errorMensajeField = value;
            }
        }

        #endregion

        #region timbrar comprobante

        public Comprobante Timbrar(string inputXml)
        {
            
            if (this.Settings.Production == false)
            {
                return this.TimbradoTesting(inputXml);
            }
            else
            {
                return this.TimbradoProduccion(inputXml);
            }
        }

        public Comprobante TimbradoProduccion(string cfdv33b64)
        {
            // Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;
            string prodEndpoint = "TimbradoEndpoint_PRODUCCION";
            com.SF.Timbre.Produccion.TimbradoPortType portCliente = null;
            portCliente = new com.SF.Timbre.Produccion.TimbradoPortTypeClient(prodEndpoint);

            try
            {
                Byte[] bytes = Encoding.UTF8.GetBytes(cfdv33b64);
                com.SF.Timbre.Produccion.timbrarRequest request = new com.SF.Timbre.Produccion.timbrarRequest();
                request.cfdi = bytes;
                request.usuario = this.Settings.User;
                request.password = this.Settings.Pass;
                // se envian datos para timbrar
                com.SF.Timbre.Produccion.timbrarResponse response = portCliente.timbrar(request);

                if (response.@return.status == 200)
                {
                    com.SF.Timbre.Produccion.CFDICertificacion resultado = response.@return;
                    if (resultado.resultados != null)
                    {
                        if (resultado.resultados[0].cfdiTimbrado != null)
                        {
                            XmlDocument xmlResultante = new XmlDocument();
                            xmlResultante.LoadXml(System.Text.Encoding.UTF8.GetString(resultado.resultados[0].cfdiTimbrado));
                            //xmlResultante.Save(System.IO.Path.Combine(this.rutaComprobantesField, string.Concat(resultado.resultados[0].uuid, ".xml")));
                            xmlResultante.Save(JaegerManagerPaths.JaegerPath(Jaeger.Edita.Enums.EnumPaths.Comprobantes, string.Concat(resultado.resultados[0].uuid, ".xml")));
                            Comprobante resultComprobante = new CFDI.V33.Comprobante();
                            resultComprobante = CFDI.V33.Comprobante.LoadXml(System.Text.Encoding.UTF8.GetString(resultado.resultados[0].cfdiTimbrado));
                            return resultComprobante;
                        }
                        else
                        {
                            this.errorCodigoField = response.@return.status;
                            this.errorMensajeField = response.@return.resultados[0].mensaje;
                        }
                    }
                    else
                    {
                        this.errorCodigoField = response.@return.status;
                        this.errorMensajeField = response.@return.mensaje;
                    }
                }
            }
            catch (Exception ex)
            {
                this.errorCodigoField = 0;
                this.errorMensajeField = ex.Message;
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public Comprobante TimbradoTesting(string cfdv33b64)
        {
            string testEndpoint = "TimbradoEndpoint_TESTING";
            // Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;
            com.SF.Timbre.Testing.TimbradoPortType portCliente = null;
            portCliente = new com.SF.Timbre.Testing.TimbradoPortTypeClient(testEndpoint);

            try
            {
                Byte[] bytes = Encoding.UTF8.GetBytes(cfdv33b64);
                com.SF.Timbre.Testing.timbrarRequest request = new com.SF.Timbre.Testing.timbrarRequest();
                request.cfdi = bytes;
                request.usuario = "testing-cfdi33@ipr981125pn9.sf";
                request.password = "Gwc2gO3b9CVQw9IquTflIHjQ";
                // enviamos datos para timbrar
                com.SF.Timbre.Testing.timbrarResponse response = portCliente.timbrar(request);

                if (response.@return.status == 200)
                {
                    com.SF.Timbre.Testing.CFDICertificacion resultado = response.@return;
                    if (resultado.resultados != null)
                    {
                        if (resultado.resultados[0].cfdiTimbrado != null)
                        {
                            XmlDocument xmlResultante = new XmlDocument();
                            xmlResultante.LoadXml(System.Text.Encoding.UTF8.GetString(resultado.resultados[0].cfdiTimbrado));
                            //xmlResultante.Save(System.IO.Path.Combine(this.RutaTemporales, string.Concat(resultado.resultados[0].uuid, "_temporal.xml")));
                            xmlResultante.Save(JaegerManagerPaths.JaegerPath(Jaeger.Edita.Enums.EnumPaths.Temporal, string.Concat(resultado.resultados[0].uuid, "_temporal.xml")));
                            Comprobante resultComprobante = new CFDI.V33.Comprobante();
                            resultComprobante = CFDI.V33.Comprobante.LoadXml(System.Text.Encoding.UTF8.GetString(resultado.resultados[0].cfdiTimbrado));
                            return resultComprobante;
                        }
                        else
                        {
                            this.errorCodigoField = response.@return.status;
                            this.errorMensajeField = response.@return.resultados[0].mensaje;
                        }
                    }
                    else
                    {
                        this.errorCodigoField = response.@return.status;
                        this.errorMensajeField = response.@return.mensaje;
                    }
                }
            }
            catch (Exception ex)
            {
                this.errorCodigoField = 1;
                this.errorMensajeField = ex.Message;
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        #endregion

        #region cancelar comprobante

        public CancelaCFDResponse Cancelar(string uuid)
        {
            if (this.Settings.Production == false)
            {
                return this.CancelarTesting(uuid);
            }
            else
            {
                return this.CancelarProduccion(uuid);
            }
        }

        public CancelaCFDResponse CancelarProduccion(string uuid)
        {
            // Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;
            // El paquete o namespace en el que se encuentran las clases será el que se define al agregar la referencia al WebService,
            // en este ejemplo es: com.sf.ws.Timbrado
            com.SF.Cancelacion.Produccion.CancelacionPortTypeClient objCancelacion = null;
            CancelaCFDResponse objAcuse = new CancelaCFDResponse();
            objCancelacion = new com.SF.Cancelacion.Produccion.CancelacionPortTypeClient("Cancelacion_Produccion");

            try
            {
                com.SF.Cancelacion.Produccion.KeyValue[] objProperties = new com.SF.Cancelacion.Produccion.KeyValue[0];
                com.SF.Cancelacion.Produccion.StatusCancelacionResponse objResponse = new com.SF.Cancelacion.Produccion.StatusCancelacionResponse();
                Envelope objEnvelope = new Envelope();
                string stringXml;
                byte[] byteCer = System.Text.UTF8Encoding.UTF8.GetBytes(this.CerBase64);
                byte[] byteKey = System.Text.UTF8Encoding.UTF8.GetBytes(this.KeyBase64);

                objResponse = objCancelacion.cancelar(this.Settings.User,
                                                      this.Settings.Pass,
                                                      this.Settings.RFC,
                                                      new string[] { uuid },
                                                      byteCer,
                                                      byteKey,
                                                      this.KeyBase64,
                                                      objProperties);
                if (objResponse.status == 200)
                {
                    stringXml = Encoding.UTF8.GetString(objResponse.acuseSat);
                    //if (!System.IO.Directory.Exists(this.rutaAccusesField))
                    //{
                    //    System.IO.Directory.CreateDirectory(this.rutaAccusesField);
                    //}

                    //Helper.HelperArchivos.WriteFileByte(objResponse.acuseSat, System.IO.Path.Combine(this.rutaAccusesField, string.Concat("Acuse-", uuid, ".xml")));
                    Jaeger.Util.Helpers.HelperFiles.WriteFileByte(objResponse.acuseSat, JaegerManagerPaths.JaegerPath(Jaeger.Edita.Enums.EnumPaths.Accuse, string.Concat("Acuse-", uuid, ".xml")));
                    objEnvelope = Jaeger.Helpers.HelperXmlSerializer.XmlDeserializarStringXml<Jaeger.Edita.V2.CFDI.Entities.Cancel.Envelope>(stringXml);
                    objAcuse = objEnvelope.Body.CancelaCFDResponse;
                    return objAcuse;
                }
                else if (objResponse.status == 201)
                {
                    this.errorCodigoField = 201;
                    this.errorMensajeField = objResponse.mensaje;
                }
                else if (objResponse.status == 601)
                {
                    this.errorCodigoField = 601;
                    this.errorMensajeField = objResponse.mensaje;
                    return null;
                }
                else
                {
                    if (objResponse != null)
                    {
                        this.errorCodigoField = objResponse.status;
                        this.errorMensajeField = objResponse.mensaje;
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            this.errorCodigoField = 201;
            this.errorMensajeField = "Este procedimiento no se encuentra en modo productivo";
            return null;
        }

        public CancelaCFDResponse CancelarTesting(string uuid)
        {
            //Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;
            //El paquete o namespace en el que se encuentran las clases será el que se define al agregar la referencia al WebService,
            //en este ejemplo es: com.sf.ws.Timbrado
            com.SF.Cancelacion.Testing.CancelacionPortTypeClient objCancelacion = null;
            Jaeger.Edita.V2.CFDI.Entities.Cancel.CancelaCFDResponse objAcuse = new Jaeger.Edita.V2.CFDI.Entities.Cancel.CancelaCFDResponse();
            objCancelacion = new com.SF.Cancelacion.Testing.CancelacionPortTypeClient("Cancelacion_Testing");

            try
            {
                com.SF.Cancelacion.Testing.KeyValue[] objProperties = new com.SF.Cancelacion.Testing.KeyValue[0];
                com.SF.Cancelacion.Testing.StatusCancelacionResponse objResponse = new com.SF.Cancelacion.Testing.StatusCancelacionResponse();

                Jaeger.Edita.V2.CFDI.Entities.Cancel.Envelope objEnvelope = new Jaeger.Edita.V2.CFDI.Entities.Cancel.Envelope();
                string stringXml;
                byte[] byteCer = System.Text.UTF8Encoding.UTF8.GetBytes(this.CerBase64);
                byte[] byteKey = System.Text.UTF8Encoding.UTF8.GetBytes(this.KeyBase64);

                objResponse = objCancelacion.cancelar(this.usuarioTesting,
                                                      this.passwordTesting,
                                                      this.Settings.RFC,
                                                      new string[] { uuid },
                                                      byteCer,
                                                      byteKey,
                                                      this.KeyBase64,
                                                      objProperties);
                if (objResponse.status == 200)
                {
                    stringXml = Encoding.UTF8.GetString(objResponse.acuseSat);
                    //if (!System.IO.Directory.Exists(this.rutaAccusesField))
                    //{
                    //    System.IO.Directory.CreateDirectory(this.rutaAccusesField);
                    //}
                    //Helper.HelperArchivos.WriteFileByte(objResponse.acuseSat, System.IO.Path.Combine(this.rutaTemporalesField, string.Concat("Acuse-", uuid, "_testing.Xml")));
                    Jaeger.Util.Helpers.HelperFiles.WriteFileByte(objResponse.acuseSat, JaegerManagerPaths.JaegerPath(Jaeger.Edita.Enums.EnumPaths.Temporal, string.Concat("Acuse-", uuid, "_testing.Xml")));
                    objEnvelope = Jaeger.Helpers.HelperXmlSerializer.XmlDeserializarStringXml<Jaeger.Edita.V2.CFDI.Entities.Cancel.Envelope>(stringXml);
                    objAcuse = objEnvelope.Body.CancelaCFDResponse;
                }
                else if (objResponse.status == 201)
                {
                    this.errorCodigoField = 201;
                    this.errorMensajeField = objResponse.mensaje;
                }
                else if (objResponse.status == 601)
                {
                    this.errorCodigoField = 601;
                    this.errorMensajeField = objResponse.mensaje;
                    return null;
                }
                else
                {
                    this.errorCodigoField = objResponse.status;
                    this.errorMensajeField = objResponse.mensaje;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return objAcuse;
        }

        #endregion

        #region validar comprobante

        public ValidateResponse Validar(string inputXml)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        #endregion

        #region utilidades

        /// <summary>
        /// Devuelve la cantidad de folios (timbres) contratados, usados y disponibles para el usuario.
        /// </summary>
        /// <returns></returns>
        public Entities.TimbresDisponibles TimbresDisponible()
        {
            if (this.Settings.Production == false)
            {
                return this.TimbresDisponibles_Testing();
            }
            else
            {
                return this.TimbresDisponibles_Produccion();
            }
        }

        public Entities.TimbresDisponibles TimbresDisponibles_Testing()
        {
            com.SF.Utilerias.Testing.UtileriasPortType portClient = null;
            portClient = new com.SF.Utilerias.Testing.UtileriasPortTypeClient("UtileriasHttpSoap11Endpoint_Testing");
            com.SF.Utilerias.Testing.getTimbresRequest request = new com.SF.Utilerias.Testing.getTimbresRequest();
            request.usuario = this.usuarioTesting;
            request.password = this.passwordTesting;
            request.rfcEmisor = this.Settings.RFC;

            try
            {
                com.SF.Utilerias.Testing.getTimbresResponse response = portClient.getTimbres(request);
                if (response != null)
                {
                    if (response.@return.status == 200)
                    {
                        Entities.TimbresDisponibles a = new Entities.TimbresDisponibles();
                        a.Contratados = response.@return.contratados.ToString();
                        a.Disponibles = response.@return.disponibles.ToString();
                        a.Mensaje = response.@return.mensaje.ToString();
                        a.Status = response.@return.status.ToString();
                        a.Usados = response.@return.usados.ToString();
                        return a;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Mensaje = ex.Message;
            }
            return null;
        }

        public Entities.TimbresDisponibles TimbresDisponibles_Produccion()
        {
            com.SF.Utilerias.Produccion.UtileriasPortType portClient = null;
            portClient = new com.SF.Utilerias.Produccion.UtileriasPortTypeClient("UtileriasHttpsSoap11Endpoint1_Produccion");
            com.SF.Utilerias.Produccion.getTimbresRequest request = new com.SF.Utilerias.Produccion.getTimbresRequest();
            request.usuario = this.Settings.User;
            request.password = this.Settings.Pass;
            request.rfcEmisor = this.Settings.RFC;

            try
            {
                com.SF.Utilerias.Produccion.getTimbresResponse response = portClient.getTimbres(request);
                if (response != null)
                {
                    if (response.@return.status == 200)
                    {
                        Entities.TimbresDisponibles a = new Entities.TimbresDisponibles();
                        a.Contratados = response.@return.contratados.ToString();
                        a.Disponibles = response.@return.disponibles.ToString();
                        a.Mensaje = response.@return.mensaje.ToString();
                        a.Status = response.@return.status.ToString();
                        a.Usados = response.@return.usados.ToString();
                        return a;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return null;
        }

        public Entities.BuscarResponse Buscar_Produccion(string uuid)
        {
            com.SF.Utilerias.Produccion.UtileriasPortType portClient = null;
            portClient = new com.SF.Utilerias.Produccion.UtileriasPortTypeClient("UtileriasHttpsSoap11Endpoint1_Produccion");
            com.SF.Utilerias.Produccion.ParametrosBuscar parametros = new com.SF.Utilerias.Produccion.ParametrosBuscar();
            parametros.uuid = uuid;
            com.SF.Utilerias.Produccion.buscarRequest request = new com.SF.Utilerias.Produccion.buscarRequest();
            request.usuario = this.Settings.User;
            request.password = this.Settings.Pass;
            request.parametros = parametros;

            try
            {
                com.SF.Utilerias.Produccion.buscarResponse response = portClient.buscar(request);
                Entities.BuscarResponse respuesta = new Entities.BuscarResponse();
                if (response != null)
                {
                    if (response.@return != null)
                    {
                        //respuesta.Cancelado = response.@return.cfdis[0].cancelado;
                        respuesta.EmisorNombre = response.@return.cfdis[0].emisorNombre;
                        respuesta.EmisorRfc = response.@return.cfdis[0].emisorRFC;
                        respuesta.FechaCancelacion = response.@return.cfdis[0].fechaCancelacion;
                        respuesta.FechaEmision = response.@return.cfdis[0].fechaEmision;
                        respuesta.FechaTimbrado = response.@return.cfdis[0].fechaTimbrado;
                        respuesta.Folio = response.@return.cfdis[0].folio;
                        respuesta.ReceptorNombre = response.@return.cfdis[0].receptorNombre;
                        respuesta.ReceptorRfc = response.@return.cfdis[0].receptorRFC;
                        respuesta.SelloDigital = response.@return.cfdis[0].selloDigital;
                        respuesta.SelloSat = response.@return.cfdis[0].selloSAT;
                        respuesta.Serie = response.@return.cfdis[0].serie;
                        //respuesta.Total = new decimal(response.@return.cfdis[0].total);
                        respuesta.Uuid = response.@return.cfdis[0].uuid;
                        return respuesta;
                    }
                }
            }
            catch (Exception)
            {
            }

            return null;
        }

        public string GenerarManifiesto(Jaeger.Entities.Manifiesto manifiesto)
        {
            com.SF.Utilerias.Testing.Emisor emisor = new com.SF.Utilerias.Testing.Emisor();

            string result = "";
            
            
            emisor.rfc = manifiesto.RFC;
            emisor.razonSocial = manifiesto.RazonSocial;
            emisor.nombreComercial = manifiesto.NombreComercial;
            emisor.email = manifiesto.Correo;
            emisor.domicilioFiscal = new com.SF.Utilerias.Testing.DireccionFiscal();
            emisor.domicilioFiscal.calle = manifiesto.Domicilio.Calle;
            emisor.domicilioFiscal.ciudad = manifiesto.Domicilio.Ciudad;
            emisor.domicilioFiscal.codigoPostal = manifiesto.Domicilio.CodigoPostal;
            emisor.domicilioFiscal.colonia = manifiesto.Domicilio.Colonia;
            emisor.domicilioFiscal.estado = manifiesto.Domicilio.Estado;
            emisor.domicilioFiscal.noExt = manifiesto.Domicilio.NoExterior;
            emisor.domicilioFiscal.noInt = manifiesto.Domicilio.NoInterior;
            emisor.domicilioFiscal.pais = manifiesto.Domicilio.Pais;
            
            com.SF.Utilerias.Testing.UtileriasPortType portClient = null;
            portClient = new com.SF.Utilerias.Testing.UtileriasPortTypeClient("UtileriasHttpSoap11Endpoint_Testing");
            com.SF.Utilerias.Testing.generarManifiestoRequest request = new com.SF.Utilerias.Testing.generarManifiestoRequest();
            request.usuario = this.usuarioTesting;
            request.password = this.passwordTesting;
            request.emisor = emisor;

            try
            {
                com.SF.Utilerias.Testing.generarManifiestoResponse response = portClient.generarManifiesto(request);
                if (response != null)
                {
                    if (response.@return.status == 200)
                    {
                        result = response.@return.mensaje;
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }

            return result;
        }
        #endregion
    }
}
