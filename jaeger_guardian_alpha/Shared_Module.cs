﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace Jaeger.SoftAWS {
    internal static class Shared_Module {
        public static string BuildS3RequestURLShared(bool UseSSL, string RequestEndpoint, string BucketName, string KeyName, string QueryString) {
            BucketName = Strings.Trim(BucketName);
            string str = "";
            str = (!UseSSL ? string.Concat(str, "http://") : string.Concat(str, "https://"));
            bool isVirtualHostedBucket = Shared_Module.GetIsVirtualHostedBucket(BucketName);
            if (isVirtualHostedBucket && Operators.CompareString(BucketName, "", false) != 0) {
                str = string.Concat(str, BucketName, ".");
            }
            str = string.Concat(str, RequestEndpoint, "/");
            if (!isVirtualHostedBucket) {
                str = string.Concat(str, BucketName, "/");
            }
            str = string.Concat(str, Uri.EscapeDataString(KeyName));
            if (Operators.CompareString(QueryString, "", false) != 0) {
                if (Strings.InStr(QueryString, "?", CompareMethod.Binary) != 1) {
                    QueryString = string.Concat("?", QueryString);
                }
                str = string.Concat(str, QueryString);
            }
            return str;
        }

        private static byte[] ComposeSigningKey(string algorithm, string awsSecretAccessKey, string region, string MyDate, string service) {
            char[] charArray = string.Concat("AWS4", awsSecretAccessKey).ToCharArray();
            byte[] numArray = Shared_Module.ComputeHash(algorithm, Encoding.UTF8.GetBytes(charArray), Encoding.UTF8.GetBytes(MyDate));
            byte[] numArray1 = Shared_Module.ComputeHash(algorithm, numArray, Encoding.UTF8.GetBytes(region));
            byte[] numArray2 = Shared_Module.ComputeHash(algorithm, numArray1, Encoding.UTF8.GetBytes(service));
            return Shared_Module.ComputeHash(algorithm, numArray2, Encoding.UTF8.GetBytes("aws4_request"));
        }

        private static string compressSpaces(string data) {
            if (Information.IsNothing(data) || !data.Contains(" ")) {
                return data;
            }
            return (new Regex("\\s+")).Replace(data, " ");
        }

        private static byte[] ComputeHash(string algorithm, byte[] key, byte[] data) {
            KeyedHashAlgorithm keyedHashAlgorithm = KeyedHashAlgorithm.Create(algorithm);
            keyedHashAlgorithm.Key = key;
            return keyedHashAlgorithm.ComputeHash(data);
        }

        public static string DictionaryToString(Dictionary<string, string> MyDictionary) {
            Dictionary<string, string>.Enumerator enumerator = new Dictionary<string, string>.Enumerator();
            string str = "";
            if (MyDictionary != null) {
                try {
                    enumerator = MyDictionary.GetEnumerator();
                    while (enumerator.MoveNext()) {
                        KeyValuePair<string, string> current = enumerator.Current;
                        if (Operators.CompareString(str, "", false) != 0) {
                            str = string.Concat(str, ",");
                        }
                        str = string.Concat(str, current.Key, ":", current.Value);
                    }
                }
                finally {
                    ((IDisposable)enumerator).Dispose();
                }
            }
            return str;
        }

        public static string FormatXML(string InputXML) {
            string str;
            if (!InputXML.Contains("<")) {
                return InputXML;
            }
            try {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(InputXML);
                StringWriter stringWriter = new StringWriter();
                XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter) {
                    Formatting = Formatting.Indented
                };
                xmlDocument.WriteTo(xmlTextWriter);
                xmlTextWriter.Flush();
                str = stringWriter.ToString();
            }
            catch (Exception exception) {
                ProjectData.SetProjectError(exception);
                str = InputXML;
                ProjectData.ClearProjectError();
            }
            return str;
        }

        public static string GetActionTimestampSignatureValueShared(string RequestURL, string AWSSecretAccessKey, ref string StringUsedForSignature) {
            IEnumerator enumerator = null;
            string query = (new Uri(RequestURL)).Query;
            if (Strings.InStr(query, "?", CompareMethod.Binary) == 1) {
                query = Strings.Right(query, checked(Strings.Len(query) - 1));
            }
            NameValueCollection nameValueCollection = HttpUtility.ParseQueryString(query);
            string item = "";
            string str = "";
            try {
                enumerator = nameValueCollection.Keys.GetEnumerator();
                while (enumerator.MoveNext()) {
                    string str1 = Conversions.ToString(enumerator.Current);
                    if (Operators.CompareString(str1.ToLower(), "action", false) != 0) {
                        if (Operators.CompareString(str1.ToLower(), "timestamp", false) != 0) {
                            continue;
                        }
                        str = nameValueCollection[str1];
                    }
                    else {
                        item = nameValueCollection[str1];
                    }
                }
            }
            finally {
                if (enumerator is IDisposable) {
                    (enumerator as IDisposable).Dispose();
                }
            }
            StringUsedForSignature = string.Concat(item, str);
            HMACSHA1 hMACSHA1 = new HMACSHA1(Encoding.UTF8.GetBytes(AWSSecretAccessKey));
            string base64String = Convert.ToBase64String(hMACSHA1.ComputeHash(Encoding.UTF8.GetBytes(StringUsedForSignature)));
            return base64String;
        }

        public static string GetAWS3AuthorizationValueShared(string AWSAccessKeyId, string AWSSecretAccessKey, string AmzDate) {
            HMACSHA256 hMACSHA256 = new HMACSHA256(Encoding.UTF8.GetBytes(AWSSecretAccessKey));
            string base64String = Convert.ToBase64String(hMACSHA256.ComputeHash(Encoding.UTF8.GetBytes(AmzDate)));
            string str = string.Concat("AWS3-HTTPS AWSAccessKeyId=", Uri.EscapeDataString(AWSAccessKeyId), ",Algorithm=HmacSHA256,Signature=", base64String);
            return str;
        }

        public static string GetAWSSignatureVersion4ValueByPartsShared(Dictionary<string, string> RequestHeaders, string ServiceURL, string authenticationServiceName, string authenticationRegion, string RequestMethod, string PostDataHash, string AWSAccessKeyId, string AWSSecretAccessKey, ref string CanonicalString, ref string StringToSign) {
            Uri uri = new Uri(ServiceURL);
            string localPath = uri.LocalPath;
            string str = "HMACSHA256";
            if (!RequestHeaders.ContainsKey("x-amz-date")) {
                throw new Exception("x-amz-date must exist in RequestHeaders.");
            }
            if (!RequestHeaders["x-amz-date"].EndsWith("Z") | RequestHeaders["x-amz-date"].Length != 16) {
                throw new Exception("The value for x-amz-date in RequestHeaders is not valid.");
            }
            string item = RequestHeaders["x-amz-date"];
            string[] strArrays = new string[] { item.Substring(0, 4), "-", item.Substring(4, 2), "-", item.Substring(6, 2), "T", item.Substring(9, 2), ":", item.Substring(11, 2), ":", item.Substring(13, 2) };
            string str1 = string.Concat(strArrays);
            DateTime dateTime = DateTime.ParseExact(str1, "s", CultureInfo.InvariantCulture);
            string str2 = dateTime.ToString("yyyyMMdd", CultureInfo.InvariantCulture);
            string lower = authenticationServiceName.Trim().ToLower();
            if (!RequestHeaders.ContainsKey("Host")) {
                RequestHeaders.Add("Host", uri.Host);
            }
            object[] objArray = new object[] { str2, authenticationRegion, lower, "aws4_request" };
            string str3 = string.Format("{0}/{1}/{2}/{3}", objArray);
            List<string> headersForSigning = Shared_Module.GetHeadersForSigning(RequestHeaders);
            string canonicalRequest = Shared_Module.GetCanonicalRequest(headersForSigning, ServiceURL, localPath, RequestHeaders, PostDataHash, RequestMethod);
            CanonicalString = canonicalRequest.Replace("\n", "\\n");
            StringBuilder stringBuilder = new StringBuilder();
            objArray = new object[] { "AWS4", "HMAC-SHA256", item, str3 };
            stringBuilder.AppendFormat("{0}-{1}\n{2}\n{3}\n", objArray);
            HashAlgorithm hashAlgorithm = HashAlgorithm.Create("SHA-256");
            byte[] numArray = hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(canonicalRequest));
            stringBuilder.Append(Shared_Module.ToHex(numArray, true));
            StringToSign = stringBuilder.ToString().Replace("\n", "\\n");
            KeyedHashAlgorithm keyedHashAlgorithm = KeyedHashAlgorithm.Create(str);
            keyedHashAlgorithm.Key = Shared_Module.ComposeSigningKey(str, AWSSecretAccessKey, authenticationRegion, str2, lower);
            byte[] numArray1 = keyedHashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(stringBuilder.ToString()));
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.AppendFormat("{0}-{1} ", "AWS4", "HMAC-SHA256");
            stringBuilder1.AppendFormat("Credential={0}/{1}, ", AWSAccessKeyId, str3);
            stringBuilder1.AppendFormat("SignedHeaders={0}, ", Shared_Module.GetSignedHeaders(headersForSigning));
            stringBuilder1.AppendFormat("Signature={0}", Shared_Module.ToHex(numArray1, true));
            return stringBuilder1.ToString();
        }

        public static string GetAWSSignatureVersion4ValueShared(Dictionary<string, string> RequestHeaders, string serviceURL, string authenticationServiceName, string authenticationRegion, string RequestMethod, string PostDataHash, string AWSAccessKeyId, string AWSSecretAccessKey, ref string CanonicalString, ref string StringToSign) {
            return Shared_Module.GetAWSSignatureVersion4ValueByPartsShared(RequestHeaders, serviceURL, authenticationServiceName, authenticationRegion, RequestMethod, PostDataHash, AWSAccessKeyId, AWSSecretAccessKey, ref CanonicalString, ref StringToSign);
        }

        private static string GetCanonicalizedHeaders(List<string> headersToSign, Dictionary<string, string> allHeaders) {
            Dictionary<string, string>.Enumerator enumerator = new Dictionary<string, string>.Enumerator();
            SortedDictionary<string, string>.Enumerator enumerator1 = new SortedDictionary<string, string>.Enumerator();
            if (Information.IsNothing(headersToSign) || headersToSign.Count == 0) {
                return string.Empty;
            }
            int count = checked(headersToSign.Count - 1);
            for (int i = 0; i <= count; i = checked(i + 1)) {
                headersToSign[i] = headersToSign[i].ToLower();
            }
            SortedDictionary<string, string> strs = new SortedDictionary<string, string>();
            try {
                enumerator = allHeaders.GetEnumerator();
                while (enumerator.MoveNext()) {
                    KeyValuePair<string, string> current = enumerator.Current;
                    if (!headersToSign.Contains(current.Key.ToLower())) {
                        continue;
                    }
                    strs[current.Key] = current.Value;
                }
            }
            finally {
                ((IDisposable)enumerator).Dispose();
            }
            StringBuilder stringBuilder = new StringBuilder();
            try {
                enumerator1 = strs.GetEnumerator();
                while (enumerator1.MoveNext()) {
                    KeyValuePair<string, string> keyValuePair = enumerator1.Current;
                    stringBuilder.Append(keyValuePair.Key.ToLower());
                    stringBuilder.Append(":");
                    stringBuilder.Append(Shared_Module.compressSpaces(keyValuePair.Value));
                    stringBuilder.Append('\n');
                }
            }
            finally {
                ((IDisposable)enumerator1).Dispose();
            }
            return stringBuilder.ToString();
        }

        private static string GetCanonicalizedQueryString(Dictionary<string, string> parameters) {
            SortedDictionary<string, string>.Enumerator enumerator = new SortedDictionary<string, string>.Enumerator();
            SortedDictionary<string, string> strs = new SortedDictionary<string, string>(parameters, StringComparer.Ordinal);
            StringBuilder stringBuilder = new StringBuilder();
            try {
                enumerator = strs.GetEnumerator();
                while (enumerator.MoveNext()) {
                    KeyValuePair<string, string> current = enumerator.Current;
                    if (current.Value == null) {
                        continue;
                    }
                    string key = current.Key;
                    string value = current.Value;
                    stringBuilder.Append(Shared_Module.UrlEncode(key, false));
                    stringBuilder.Append("=");
                    value = HttpUtility.UrlDecode(value);
                    stringBuilder.Append(Shared_Module.UrlEncode(value, false));
                    stringBuilder.Append("&");
                }
            }
            finally {
                ((IDisposable)enumerator).Dispose();
            }
            string str = stringBuilder.ToString();
            if (string.IsNullOrEmpty(str)) {
                return string.Empty;
            }
            return str.Substring(0, checked(str.Length - 1));
        }

        private static string GetCanonicalizedResourcePath(string resourcePath) {
            if (Operators.CompareString(resourcePath, "", false) == 0 || Operators.CompareString(resourcePath, "/", false) == 0) {
                return "/";
            }
            string[] strArrays = new string[] { "/" };
            string[] strArrays1 = resourcePath.Split(strArrays, StringSplitOptions.RemoveEmptyEntries);
            StringBuilder stringBuilder = new StringBuilder();
            string[] strArrays2 = strArrays1;
            for (int i = 0; i < checked((int)strArrays2.Length); i = checked(i + 1)) {
                string str = strArrays2[i];
                stringBuilder.AppendFormat("/{0}", Shared_Module.UrlEncode(str, false));
            }
            if (resourcePath.EndsWith("/", StringComparison.Ordinal)) {
                stringBuilder.Append("/");
            }
            return stringBuilder.ToString();
        }

        private static string GetCanonicalRequest(List<string> headersToSign, string RequestURL, string KeyName, Dictionary<string, string> headers, string PostDataHash, string RequestMethod) {
            string str = string.Concat("/", KeyName);
            Dictionary<string, string> strs = Shared_Module.ParseQueryParameters(RequestURL);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("{0}\n", RequestMethod.ToUpper());
            stringBuilder.AppendFormat("{0}\n", Shared_Module.GetCanonicalizedResourcePath(str));
            stringBuilder.AppendFormat("{0}\n", Shared_Module.GetCanonicalizedQueryString(strs));
            stringBuilder.AppendFormat("{0}\n", Shared_Module.GetCanonicalizedHeaders(headersToSign, headers));
            stringBuilder.AppendFormat("{0}\n", Shared_Module.GetSignedHeaders(headersToSign));
            stringBuilder.Append(PostDataHash);
            return stringBuilder.ToString();
        }

        public static string GetCloudFrontAuthorizationValueShared(string AWSAccessKeyId, string AWSSecretAccessKey, string AmzDate) {
            HMACSHA1 hMACSHA1 = new HMACSHA1(Encoding.UTF8.GetBytes(AWSSecretAccessKey));
            string base64String = Convert.ToBase64String(hMACSHA1.ComputeHash(Encoding.UTF8.GetBytes(AmzDate)));
            return string.Concat("AWS ", AWSAccessKeyId, ":", base64String);
        }

        public static string GetDevPaySignatureValueShared(string RequestURL, string AWSSecretAccessKey, ref string StringUsedForSignature) {
            IEnumerator enumerator = null;
            StringUsedForSignature = "";
            string base64String = "";
            string query = (new Uri(RequestURL)).Query;
            if (Strings.InStr(query, "?", CompareMethod.Binary) == 1) {
                query = Strings.Right(query, checked(Strings.Len(query) - 1));
            }
            NameValueCollection nameValueCollection = HttpUtility.ParseQueryString(query);
            String_Comparer stringComparer = new String_Comparer(CompareInfo.GetCompareInfo(""), CompareOptions.StringSort);
            SortedList sortedLists = new SortedList(stringComparer);
            try {
                enumerator = nameValueCollection.Keys.GetEnumerator();
                while (enumerator.MoveNext()) {
                    string str = Conversions.ToString(enumerator.Current);
                    sortedLists.Add(str, nameValueCollection[str]);
                }
            }
            finally {
                if (enumerator is IDisposable) {
                    (enumerator as IDisposable).Dispose();
                }
            }
            IDictionaryEnumerator dictionaryEnumerator = sortedLists.GetEnumerator();
            while (dictionaryEnumerator.MoveNext()) {
                object objectValue = RuntimeHelpers.GetObjectValue(dictionaryEnumerator.Current);
                if (!Operators.ConditionalCompareObjectNotEqual(NewLateBinding.LateGet(objectValue, null, "Value", new object[0], null, null, null), "", false)) {
                    continue;
                }
                StringUsedForSignature = Conversions.ToString(Operators.AddObject(StringUsedForSignature, Operators.ConcatenateObject(NewLateBinding.LateGet(objectValue, null, "Key", new object[0], null, null, null), NewLateBinding.LateGet(objectValue, null, "Value", new object[0], null, null, null))));
            }
            HMACSHA1 hMACSHA1 = new HMACSHA1(Encoding.UTF8.GetBytes(AWSSecretAccessKey));
            base64String = Convert.ToBase64String(hMACSHA1.ComputeHash(Encoding.UTF8.GetBytes(StringUsedForSignature)));
            return base64String;
        }

        public static string GetErrorDescriptionFromErrorNumber(int ErrorNumber, string ExtraDescription) {
            string str = "";
            if (ErrorNumber == 1001) {
                str = "Invalid character in ExtraRequestHeader value.";
            }
            else if (ErrorNumber == 1002) {
                str = "Error when setting header.";
            }
            else if (ErrorNumber == 1003) {
                str = "Getting the response failed.";
            }
            else if (ErrorNumber == 1004) {
                str = "Opening the request stream failed.";
            }
            else if (ErrorNumber == 1005) {
                str = "Unexpected end of file.";
            }
            else if (ErrorNumber == 1006) {
                str = "Error getting the response stream.";
            }
            else if (ErrorNumber == 1007) {
                str = "Error opening the file.";
            }
            else if (ErrorNumber == 1008) {
                str = "The operation was aborted.";
            }
            else if (ErrorNumber == 1009) {
                str = "Error setting the byte range.";
            }
            else if (ErrorNumber == 1111) {
                str = "The range value is not valid.";
            }
            else if (ErrorNumber == 1112) {
                str = "Opening the local file failed.";
            }
            else if (ErrorNumber == 1113) {
                str = "Reading the source stream failed.";
            }
            else if (ErrorNumber == 1114) {
                str = "Writing to stream failed.";
            }
            else if (ErrorNumber == 1121) {
                str = "Reading the source file failed.";
            }
            else if (ErrorNumber == 1122) {
                str = "Writing to the upload stream failed.";
            }
            else if (ErrorNumber == 1123) {
                str = "Error opening local file.";
            }
            else if (ErrorNumber == 1131) {
                str = "The Method parameter is not valid.   Only GET, COPY, DELETE, and HEAD are allowed.";
            }
            else if (ErrorNumber == 1141) {
                str = "The Method parameter is not valid.   Only PUT is allowed.";
            }
            else if (ErrorNumber == 1142) {
                str = "Sending the request failed.";
            }
            else if (ErrorNumber == 1151) {
                str = "Error during encryption.";
            }
            else if (ErrorNumber == 1161) {
                str = "Error during decryption.";
            }
            else if (ErrorNumber == 1171) {
                str = "The ListBucketResult node does not exist in the response XML.";
            }
            else if (ErrorNumber == 1172) {
                str = "The operation was aborted.";
            }
            if (Operators.CompareString(ExtraDescription, "", false) != 0) {
                str = string.Concat(str, "   ", ExtraDescription);
            }
            return str;
        }

        private static List<string> GetHeadersForSigning(Dictionary<string, string> RequestHeaders) {
            Dictionary<string, string>.Enumerator enumerator = new Dictionary<string, string>.Enumerator();
            List<string> strs = new List<string>();
            try {
                enumerator = RequestHeaders.GetEnumerator();
                while (enumerator.MoveNext()) {
                    strs.Add(enumerator.Current.Key);
                }
            }
            finally {
                ((IDisposable)enumerator).Dispose();
            }
            strs.Sort(StringComparer.OrdinalIgnoreCase);
            return strs;
        }

        public static bool GetIsVirtualHostedBucket(string BucketName) {
            bool flag;
            if (Operators.CompareString(Strings.LCase(BucketName), BucketName, false) != 0) {
                flag = false;
            }
            else if (Strings.InStr(BucketName, "_", CompareMethod.Binary) <= 0) {
                flag = (Strings.Len(BucketName) <= 63 ? true : false);
            }
            else {
                flag = false;
            }
            return flag;
        }

        public static string GetMechanicalTurkSignatureValueShared(string RequestURL, string AWSSecretAccessKey, ref string StringUsedForSignature) {
            IEnumerator enumerator = null;
            string base64String = "";
            string query = (new Uri(RequestURL)).Query;
            if (Strings.InStr(query, "?", CompareMethod.Binary) == 1) {
                query = Strings.Right(query, checked(Strings.Len(query) - 1));
            }
            NameValueCollection nameValueCollection = HttpUtility.ParseQueryString(query);
            string item = "";
            string str = "";
            string item1 = "";
            try {
                enumerator = nameValueCollection.Keys.GetEnumerator();
                while (enumerator.MoveNext()) {
                    string str1 = Conversions.ToString(enumerator.Current);
                    if (Operators.CompareString(str1.ToLower(), "service", false) == 0) {
                        item = nameValueCollection[str1];
                    }
                    else if (Operators.CompareString(str1.ToLower(), "operation", false) != 0) {
                        if (Operators.CompareString(str1.ToLower(), "timestamp", false) != 0) {
                            continue;
                        }
                        item1 = nameValueCollection[str1];
                    }
                    else {
                        str = nameValueCollection[str1];
                    }
                }
            }
            finally {
                if (enumerator is IDisposable) {
                    (enumerator as IDisposable).Dispose();
                }
            }
            StringUsedForSignature = string.Concat(item, str, item1);
            HMACSHA1 hMACSHA1 = new HMACSHA1(Encoding.UTF8.GetBytes(AWSSecretAccessKey));
            base64String = Convert.ToBase64String(hMACSHA1.ComputeHash(Encoding.UTF8.GetBytes(StringUsedForSignature)));
            return base64String;
        }

        private static string GetParametersAsString(string QueryString) {
            IEnumerator enumerator = null;
            String_Comparer stringComparer = new String_Comparer(CompareInfo.GetCompareInfo(""), CompareOptions.StringSort);
            SortedList sortedLists = new SortedList(stringComparer);
            NameValueCollection nameValueCollection = HttpUtility.ParseQueryString(QueryString);
            try {
                enumerator = nameValueCollection.Keys.GetEnumerator();
                while (enumerator.MoveNext()) {
                    string str = Conversions.ToString(enumerator.Current);
                    sortedLists.Add(str, nameValueCollection[str]);
                }
            }
            finally {
                if (enumerator is IDisposable) {
                    (enumerator as IDisposable).Dispose();
                }
            }
            string str1 = "";
            IDictionaryEnumerator dictionaryEnumerator = sortedLists.GetEnumerator();
            while (dictionaryEnumerator.MoveNext()) {
                object objectValue = RuntimeHelpers.GetObjectValue(dictionaryEnumerator.Current);
                str1 = Conversions.ToString(Operators.AddObject(str1, Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(NewLateBinding.LateGet(objectValue, null, "Key", new object[0], null, null, null), "="), Shared_Module.UrlEncode(Conversions.ToString(NewLateBinding.LateGet(objectValue, null, "Value", new object[0], null, null, null)), false)), "&")));
            }
            if (Operators.CompareString(str1, "", false) != 0) {
                str1 = Strings.Left(str1, checked(Strings.Len(str1) - 1));
            }
            return str1;
        }

        public static string GetS3AuthorizationValueByPartsShared(string BucketName, string KeyName, string QueryString, string RequestMethod, Dictionary<string, string> ExtraRequestHeaders, string AWSAccessKeyId, string AWSSecretAccessKey, ref string StringThatWasSigned) {
            Dictionary<string, string>.Enumerator enumerator = new Dictionary<string, string>.Enumerator();
            Dictionary<string, string>.Enumerator enumerator1 = new Dictionary<string, string>.Enumerator();
            IEnumerator enumerator2 = null;
            IEnumerator enumerator3 = null;
            string value = "";
            string str = "";
            try {
                enumerator = ExtraRequestHeaders.GetEnumerator();
                while (enumerator.MoveNext()) {
                    KeyValuePair<string, string> current = enumerator.Current;
                    if (Operators.CompareString(Strings.LCase(current.Key), "content-md5", false) != 0) {
                        if (Operators.CompareString(Strings.LCase(current.Key), "content-type", false) != 0) {
                            continue;
                        }
                        str = current.Value;
                    }
                    else {
                        value = current.Value;
                    }
                }
            }
            finally {
                ((IDisposable)enumerator).Dispose();
            }
            StringThatWasSigned = "";
            StringThatWasSigned = string.Concat(StringThatWasSigned, RequestMethod.ToUpper(), "\n");
            StringThatWasSigned = string.Concat(StringThatWasSigned, value, "\n");
            StringThatWasSigned = string.Concat(StringThatWasSigned, str, "\n");
            StringThatWasSigned = string.Concat(StringThatWasSigned, "\n");
            NameValueCollection nameValueCollection = new NameValueCollection();
            try {
                enumerator1 = ExtraRequestHeaders.GetEnumerator();
                while (enumerator1.MoveNext()) {
                    KeyValuePair<string, string> keyValuePair = enumerator1.Current;
                    if (Strings.InStr(Strings.LCase(keyValuePair.Key), "x-amz-", CompareMethod.Binary) != 1) {
                        continue;
                    }
                    nameValueCollection.Add(keyValuePair.Key, keyValuePair.Value);
                }
            }
            finally {
                ((IDisposable)enumerator1).Dispose();
            }
            String_Comparer stringComparer = new String_Comparer(CompareInfo.GetCompareInfo(""), CompareOptions.StringSort);
            SortedList sortedLists = new SortedList(stringComparer);
            try {
                enumerator2 = nameValueCollection.Keys.GetEnumerator();
                while (enumerator2.MoveNext()) {
                    string str1 = Conversions.ToString(enumerator2.Current);
                    sortedLists.Add(str1, nameValueCollection[str1]);
                }
            }
            finally {
                if (enumerator2 is IDisposable) {
                    (enumerator2 as IDisposable).Dispose();
                }
            }
            IDictionaryEnumerator dictionaryEnumerator = sortedLists.GetEnumerator();
            while (dictionaryEnumerator.MoveNext()) {
                object objectValue = RuntimeHelpers.GetObjectValue(dictionaryEnumerator.Current);
                string stringThatWasSigned = StringThatWasSigned;
                Type type = typeof(Strings);
                object[] objArray = new object[1];
                object obj = objectValue;
                objArray[0] = RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(obj, null, "Key", new object[0], null, null, null));
                object[] objArray1 = objArray;
                bool[] flagArray = new bool[] { true };
                object obj1 = NewLateBinding.LateGet(null, type, "LCase", objArray1, null, null, flagArray);
                if (flagArray[0]) {
                    object[] objectValue1 = new object[] { RuntimeHelpers.GetObjectValue(objArray1[0]) };
                    NewLateBinding.LateSetComplex(obj, null, "Key", objectValue1, null, null, true, false);
                }
                StringThatWasSigned = Conversions.ToString(Operators.AddObject(stringThatWasSigned, Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(obj1, ":"), NewLateBinding.LateGet(objectValue, null, "Value", new object[0], null, null, null)), '\n')));
            }
            if (Operators.CompareString(BucketName, "", false) != 0 & Shared_Module.GetIsVirtualHostedBucket(BucketName)) {
                StringThatWasSigned = string.Concat(StringThatWasSigned, "/", BucketName);
            }
            if (Operators.CompareString(KeyName, "", false) == 0) {
                KeyName = "/";
            }
            StringThatWasSigned = string.Concat(StringThatWasSigned, KeyName);
            if (Strings.InStr(QueryString, "?", CompareMethod.Binary) == 1) {
                QueryString = Strings.Right(QueryString, checked(Strings.Len(QueryString) - 1));
            }
            if (Operators.CompareString(QueryString, "", false) != 0) {
                SortedList sortedLists1 = new SortedList(stringComparer);
                NameValueCollection nameValueCollection1 = HttpUtility.ParseQueryString(QueryString);
                try {
                    enumerator3 = nameValueCollection1.Keys.GetEnumerator();
                    while (enumerator3.MoveNext()) {
                        string str2 = Conversions.ToString(enumerator3.Current);
                        if (!Information.IsNothing(str2)) {
                            if (!(Operators.CompareString(str2, "versionId", false) == 0 | Operators.CompareString(str2, "uploadId", false) == 0 | Operators.CompareString(str2, "partNumber", false) == 0)) {
                                continue;
                            }
                            sortedLists1.Add(str2, nameValueCollection1[str2]);
                        }
                        else {
                            sortedLists1.Add(nameValueCollection1[str2], "");
                        }
                    }
                }
                finally {
                    if (enumerator3 is IDisposable) {
                        (enumerator3 as IDisposable).Dispose();
                    }
                }
                string str3 = "";
                IDictionaryEnumerator dictionaryEnumerator1 = sortedLists1.GetEnumerator();
                while (dictionaryEnumerator1.MoveNext()) {
                    object objectValue2 = RuntimeHelpers.GetObjectValue(dictionaryEnumerator1.Current);
                    if (!Operators.ConditionalCompareObjectNotEqual(NewLateBinding.LateGet(objectValue2, null, "Key", new object[0], null, null, null), "", false)) {
                        continue;
                    }
                    str3 = (!Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(objectValue2, null, "Value", new object[0], null, null, null), "", false) ? Conversions.ToString(Operators.AddObject(str3, Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(NewLateBinding.LateGet(objectValue2, null, "Key", new object[0], null, null, null), "="), NewLateBinding.LateGet(objectValue2, null, "Value", new object[0], null, null, null)), "&"))) : Conversions.ToString(Operators.AddObject(str3, Operators.ConcatenateObject(NewLateBinding.LateGet(objectValue2, null, "Key", new object[0], null, null, null), "&"))));
                }
                if (Operators.CompareString(str3, "", false) != 0) {
                    str3 = string.Concat("?", Strings.Left(str3, checked(Strings.Len(str3) - 1)));
                }
                StringThatWasSigned = string.Concat(StringThatWasSigned, str3);
            }
            HMACSHA1 hMACSHA1 = new HMACSHA1(Encoding.UTF8.GetBytes(AWSSecretAccessKey));
            string base64String = Convert.ToBase64String(hMACSHA1.ComputeHash(Encoding.UTF8.GetBytes(StringThatWasSigned)));
            return string.Concat("AWS ", AWSAccessKeyId, ":", base64String);
        }

        public static string GetS3AuthorizationValueShared(string RequestURL, string RequestMethod, Dictionary<string, string> ExtraRequestHeaders, string AWSAccessKeyId, string AWSSecretAccessKey, ref string StringThatWasSigned) {
            Uri uri = new Uri(RequestURL);
            string str = "";
            string host = uri.Host;
            char[] chrArray = new char[] { '.' };
            if (checked((int)host.Split(chrArray).Length) > 3) {
                str = host;
                int num = 1;
                do {
                    str = str.Substring(0, str.LastIndexOf("."));
                    num = checked(num + 1);
                }
                while (num <= 3);
            }
            string absolutePath = uri.AbsolutePath;
            string query = uri.Query;
            return Shared_Module.GetS3AuthorizationValueByPartsShared(str, absolutePath, query, RequestMethod, ExtraRequestHeaders, AWSAccessKeyId, AWSSecretAccessKey, ref StringThatWasSigned);
        }

        public static string GetSignatureVersion2ValueShared(string RequestURL, string RequestMethod, string SendData, string AWSSecretAccessKey, ref string StringUsedForSignature) {
            IEnumerator enumerator = null;
            IEnumerator enumerator1 = null;
            IEnumerator<KeyValuePair<string, string>> enumerator2 = null;
            string lower = "";
            string base64String = "";
            Uri uri = new Uri(RequestURL);
            string str = uri.Host.ToLower();
            string query = uri.Query;
            if (Strings.InStr(query, "?", CompareMethod.Binary) == 1) {
                query = Strings.Right(query, checked(Strings.Len(query) - 1));
            }
            NameValueCollection nameValueCollection = HttpUtility.ParseQueryString(query);
            NameValueCollection nameValueCollection1 = HttpUtility.ParseQueryString(SendData);
            string absolutePath = uri.AbsolutePath;
            if (Operators.CompareString(absolutePath, "", false) == 0) {
                absolutePath = "/";
            }
            IDictionary<string, string> strs = new Dictionary<string, string>();
            try {
                enumerator = nameValueCollection.Keys.GetEnumerator();
                while (enumerator.MoveNext()) {
                    string str1 = Conversions.ToString(enumerator.Current);
                    if (Information.IsNothing(str1)) {
                        continue;
                    }
                    if (Operators.CompareString(str1.ToLower(), "signaturemethod", false) == 0) {
                        lower = nameValueCollection[str1].ToLower();
                    }
                    strs.Add(str1, nameValueCollection[str1]);
                }
            }
            finally {
                if (enumerator is IDisposable) {
                    (enumerator as IDisposable).Dispose();
                }
            }
            try {
                enumerator1 = nameValueCollection1.Keys.GetEnumerator();
                while (enumerator1.MoveNext()) {
                    string str2 = Conversions.ToString(enumerator1.Current);
                    if (Information.IsNothing(str2)) {
                        continue;
                    }
                    if (Operators.CompareString(str2.ToLower(), "signaturemethod", false) == 0) {
                        lower = nameValueCollection1[str2].ToLower();
                    }
                    strs.Add(str2, nameValueCollection[str2]);
                }
            }
            finally {
                if (enumerator1 is IDisposable) {
                    (enumerator1 as IDisposable).Dispose();
                }
            }
            IDictionary<string, string> strs1 = new SortedDictionary<string, string>(strs, StringComparer.Ordinal);
            StringUsedForSignature = "";
            StringUsedForSignature = string.Concat(StringUsedForSignature, RequestMethod.ToUpper(), "\n");
            StringUsedForSignature = string.Concat(StringUsedForSignature, str, "\n");
            StringUsedForSignature = string.Concat(StringUsedForSignature, absolutePath, "\n");
            //using (StringUsedForSignature = string.Concat(StringUsedForSignature, absolutePath, "\n")) {
            //    enumerator2 = strs1.GetEnumerator();
            //    while (enumerator2.MoveNext()) {
            //        KeyValuePair<string, string> current = enumerator2.Current;
            //        string[] stringUsedForSignature = new string[] { StringUsedForSignature, SharedModule.SDKUrlEncode(current.Key, Conversions.ToString(false)), "=", SharedModule.SDKUrlEncode(current.Value, Conversions.ToString(false)), "&" };
            //        StringUsedForSignature = string.Concat(stringUsedForSignature);
            //    }
            //}

            StringUsedForSignature = string.Concat(StringUsedForSignature, absolutePath, "\n");
            enumerator2 = strs1.GetEnumerator();
            while (enumerator2.MoveNext()) {
                KeyValuePair<string, string> current = enumerator2.Current;
                string[] stringUsedForSignature = new string[] { StringUsedForSignature, Shared_Module.SDKUrlEncode(current.Key, Conversions.ToString(false)), "=", Shared_Module.SDKUrlEncode(current.Value, Conversions.ToString(false)), "&" };
                StringUsedForSignature = string.Concat(stringUsedForSignature);
            }

            if (StringUsedForSignature.EndsWith("&")) {
                StringUsedForSignature = Strings.Left(StringUsedForSignature, checked(Strings.Len(StringUsedForSignature) - 1));
            }
            if (Operators.CompareString(lower, "HmacSHA1".ToLower(), false) != 0) {
                HMACSHA256 hMACSHA256 = new HMACSHA256(Encoding.UTF8.GetBytes(AWSSecretAccessKey));
                base64String = Convert.ToBase64String(hMACSHA256.ComputeHash(Encoding.UTF8.GetBytes(StringUsedForSignature)));
            }
            else {
                HMACSHA1 hMACSHA1 = new HMACSHA1(Encoding.UTF8.GetBytes(AWSSecretAccessKey));
                base64String = Convert.ToBase64String(hMACSHA1.ComputeHash(Encoding.UTF8.GetBytes(StringUsedForSignature)));
            }
            return base64String;
        }

        private static string GetSignedHeaders(List<string> headersToSign) {
            List<string>.Enumerator enumerator = new List<string>.Enumerator();
            StringBuilder stringBuilder = new StringBuilder();
            try {
                enumerator = headersToSign.GetEnumerator();
                while (enumerator.MoveNext()) {
                    string current = enumerator.Current;
                    if (stringBuilder.Length > 0) {
                        stringBuilder.Append(";");
                    }
                    stringBuilder.Append(current.ToLower());
                }
            }
            finally {
                ((IDisposable)enumerator).Dispose();
            }
            return stringBuilder.ToString();
        }

        private static Dictionary<string, string> ParseQueryParameters(string url) {
            string str;
            Dictionary<string, string> strs = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(url)) {
                int num = url.IndexOf("?");
                if (num >= 0) {
                    string str1 = url.Substring(checked(num + 1));
                    string[] strArrays = new string[] { "&" };
                    string[] strArrays1 = str1.Split(strArrays, StringSplitOptions.None);
                    for (int i = 0; i < checked((int)strArrays1.Length); i = checked(i + 1)) {
                        string str2 = strArrays1[i];
                        if (!string.IsNullOrEmpty(str2)) {
                            char[] chrArray = new char[] { '=' };
                            string[] strArrays2 = str2.Split(chrArray, 2);
                            string str3 = strArrays2[0];
                            str = (checked((int)strArrays2.Length) != 1 ? strArrays2[1] : string.Empty);
                            strs.Add(str3, str);
                        }
                    }
                }
            }
            return strs;
        }

        public static string ProxyToString(IWebProxy MyProxy, Uri MyUri) {
            string originalString = "";
            if (!Information.IsNothing(MyProxy)) {
                originalString = MyProxy.GetProxy(MyUri).OriginalString;
            }
            return originalString;
        }

        private static string SDKUrlEncode(string data, string path) {
            StringBuilder stringBuilder = new StringBuilder(checked(data.Length * 2));
            string str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";
            if (Conversions.ToBoolean(path)) {
                str = string.Concat(str, "/:");
            }
            byte[] bytes = Encoding.UTF8.GetBytes(data);
            for (int i = 0; i < checked((int)bytes.Length); i = checked(i + 1)) {
                byte num = bytes[i];
                if (str.IndexOf(Convert.ToChar(num)) < 0) {
                    stringBuilder.Append("%").Append(string.Format("{0:X2}", Convert.ToInt32(num)));
                }
                else {
                    stringBuilder.Append(Convert.ToChar(num));
                }
            }
            return stringBuilder.ToString();
        }

        private static string ToHex(byte[] data, bool lowercase) {
            StringBuilder stringBuilder = new StringBuilder();
            int length = checked(checked((int)data.Length) - 1);
            for (int i = 0; i <= length; i = checked(i + 1)) {
                stringBuilder.Append(data[i].ToString((lowercase ? "x2" : "X2")));
            }
            return stringBuilder.ToString();
        }

        private static string UrlEncode(string data, bool path) {
            StringBuilder stringBuilder = new StringBuilder(checked(data.Length * 2));
            string str = string.Concat("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~", (path ? "/:" : ""));
            byte[] bytes = Encoding.UTF8.GetBytes(data);
            for (int i = 0; i < checked((int)bytes.Length); i = checked(i + 1)) {
                char chr = Convert.ToChar(bytes[i]);
                if (str.IndexOf(chr) == -1) {
                    stringBuilder.Append("%").Append(string.Format("{0:X2}", Convert.ToInt32(chr)));
                }
                else {
                    stringBuilder.Append(chr);
                }
            }
            return stringBuilder.ToString();
        }
    }
}
