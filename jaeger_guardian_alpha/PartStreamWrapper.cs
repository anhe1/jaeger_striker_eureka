﻿using System;
using System.IO;

namespace Jaeger.SoftAWS {
    internal class PartStreamWrapper : Stream {
        private Stream wrappedStream;

        private long initialPosition;

        private long partSize;

        public override bool CanRead {
            get {
                return this.wrappedStream.CanRead;
            }
        }

        public override bool CanSeek {
            get {
                return this.wrappedStream.CanSeek;
            }
        }

        public override bool CanTimeout {
            get {
                return this.wrappedStream.CanTimeout;
            }
        }

        public override bool CanWrite {
            get {
                return this.wrappedStream.CanWrite;
            }
        }

        public override long Length {
            get {
                long length = checked(this.wrappedStream.Length - this.initialPosition);
                if (length > this.partSize) {
                    length = this.partSize;
                }
                return length;
            }
        }

        public override long Position {
            get {
                return checked(this.wrappedStream.Position - this.initialPosition);
            }
            set {
                this.wrappedStream.Position = checked(this.initialPosition + value);
            }
        }

        public override int ReadTimeout {
            get {
                return this.wrappedStream.ReadTimeout;
            }
            set {
                this.wrappedStream.ReadTimeout = value;
            }
        }

        private long RemainingPartSize {
            get {
                return checked(this.partSize - this.Position);
            }
        }

        public override int WriteTimeout {
            get {
                return this.wrappedStream.WriteTimeout;
            }
            set {
                this.wrappedStream.WriteTimeout = value;
            }
        }

        public PartStreamWrapper(Stream stream, long partSize) {
            this.wrappedStream = stream;
            this.initialPosition = stream.Position;
            long length = checked(stream.Length - stream.Position);
            if (partSize == (long)0 || length < partSize) {
                this.partSize = length;
            }
            else {
                this.partSize = partSize;
            }
        }

        public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state) {
            throw new NotImplementedException();
        }

        public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state) {
            throw new NotImplementedException();
        }

        public override void Close() {
            base.Close();
        }

        protected override void Dispose(bool disposing) {
            base.Dispose(disposing);
        }

        public override int EndRead(IAsyncResult asyncResult) {
            return base.EndRead(asyncResult);
        }

        public override void EndWrite(IAsyncResult asyncResult) {
            base.EndWrite(asyncResult);
        }

        public override void Flush() {
        }

        public override int Read(byte[] buffer, int offset, int count) {
            int num = checked((int)(((long)count < this.RemainingPartSize ? (long)count : this.RemainingPartSize)));
            if (num < 0) {
                return 0;
            }
            return this.wrappedStream.Read(buffer, offset, num);
        }

        public override long Seek(long offset, SeekOrigin origin) {
            long position = (long)0;
            switch (origin) {
                case SeekOrigin.Begin: {
                        position = checked(this.initialPosition + offset);
                        break;
                    }
                case SeekOrigin.Current: {
                        position = checked(this.wrappedStream.Position + offset);
                        break;
                    }
                case SeekOrigin.End: {
                        position = checked(checked(this.wrappedStream.Position + this.partSize) + offset);
                        break;
                    }
            }
            if (position < this.initialPosition) {
                position = this.initialPosition;
            }
            else if (position > checked(this.initialPosition + this.partSize)) {
                position = checked(this.initialPosition + this.partSize);
            }
            this.wrappedStream.Position = position;
            return this.Position;
        }

        public override void SetLength(long value) {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count) {
            throw new NotImplementedException();
        }

        public override void WriteByte(byte value) {
            throw new NotImplementedException();
        }
    }
}
