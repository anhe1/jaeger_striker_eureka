﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Globalization;

namespace Jaeger.SoftAWS {
    internal class String_Comparer : IComparer {
        private CompareInfo myComp;

        private CompareOptions myOptions;

        public String_Comparer(CompareInfo cmpi, CompareOptions options) {
            this.myOptions = CompareOptions.None;
            this.myComp = cmpi;
            this.myOptions = options;
        }

        public int Compare(object a, object b) {
            if (Operators.ConditionalCompareObjectEqual(a, b, false)) {
                return 0;
            }
            if (a == null) {
                return -1;
            }
            if (b == null) {
                return 1;
            }
            string str = Conversions.ToString(a);
            string str1 = Conversions.ToString(b);
            if (!(str != null & str1 != null)) {
                throw new ArgumentException("StringComparer.Compare   a and b must be strings.");
            }
            return this.myComp.Compare(str, str1, this.myOptions);
        }
    }
}
