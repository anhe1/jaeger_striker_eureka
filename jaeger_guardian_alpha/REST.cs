﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using Jaeger.SoftAWS.S3;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace Jaeger.SoftAWS {
    public class REST {
        private HttpWebRequest MyRequest;

        private int InternalState;

        private int InternalErrorNumber;

        private string InternalErrorDescription;

        private IWebProxy InternalProxy;

        private ICredentials InternalCredentials;

        private bool InternalIgnoreSSLCertificateErrors;

        private bool InternalAllowAutoRedirect;

        private int InternalTimeoutSeconds;

        private int InternalReadWriteTimeoutSeconds;

        private string InternalLogData;

        private int InternalResponseStatusCode;

        private string InternalResponseStatusDescription;

        private Dictionary<string, string> InternalResponseHeaders;

        private string InternalResponseString;

        private string InternalRequestURL;

        private string InternalRequestMethod;

        private Dictionary<string, string> InternalRequestHeaders;

        /// <summary>Gets/Sets a value that indicates whether the request should follow redirection responses.</summary>
        public bool AllowAutoRedirect {
            get {
                return this.InternalAllowAutoRedirect;
            }
            set {
                this.InternalAllowAutoRedirect = value;
            }
        }

        /// <summary>Gets/Sets a System.Net.ICredentials object that defines authentication information for the request.</summary>
        public ICredentials Credentials {
            get {
                return this.InternalCredentials;
            }
            set {
                this.InternalCredentials = value;
            }
        }

        /// <summary>A message that describes the last exception.</summary>
        public string ErrorDescription {
            get {
                return this.InternalErrorDescription;
            }
        }

        public int ErrorNumber {
            get {
                return this.InternalErrorNumber;
            }
        }

        public bool IgnoreSSLCertificateErrors {
            get {
                return this.InternalIgnoreSSLCertificateErrors;
            }
            set {
                this.InternalIgnoreSSLCertificateErrors = value;
            }
        }

        public string LogData {
            get {
                return this.InternalLogData;
            }
        }

        public IWebProxy Proxy {
            get {
                return this.InternalProxy;
            }
            set {
                this.InternalProxy = value;
            }
        }

        /// <summary>Gets/Sets the ReadWriteTimeout value.</summary>
        public int ReadWriteTimeoutSeconds {
            get {
                return this.InternalReadWriteTimeoutSeconds;
            }
            set {
                this.InternalReadWriteTimeoutSeconds = value;
            }
        }

        public Dictionary<string, string> RequestHeaders {
            get {
                return this.InternalRequestHeaders;
            }
        }

        /// <summary>Returns the HTTP method that was used to connect to Amazon. This is the method that was passed to the function. This value is read only and is provided for logging purposes.</summary>
        public string RequestMethod {
            get {
                return this.InternalRequestMethod;
            }
        }

        public string RequestURL {
            get {
                return this.InternalRequestURL;
            }
        }

        /// <summary>Returns a list of HTTP headers returned from Amazon.</summary>
        public Dictionary<string, string> ResponseHeaders {
            get {
                return this.InternalResponseHeaders;
            }
        }

        /// <summary>Returns the HTTP status code that was returned from Amazon.</summary>
        public int ResponseStatusCode {
            get {
                return this.InternalResponseStatusCode;
            }
        }

        public string ResponseStatusDescription {
            get {
                return this.InternalResponseStatusDescription;
            }
        }

        public string ResponseString {
            get {
                return this.InternalResponseString;
            }
        }

        /// <summary>Indents and formats the ResponseString if it is XML.</summary>
        public string ResponseStringFormatted {
            get {
                return Shared_Module.FormatXML(this.InternalResponseString);
            }
        }

        /// <summary>Gets the current state of the component.</summary>
        public int State {
            get {
                return this.InternalState;
            }
        }

        public int TimeoutSeconds {
            get {
                return this.InternalTimeoutSeconds;
            }
            set {
                this.InternalTimeoutSeconds = value;
            }
        }

        public string Version {
            get {
                AssemblyName name = Assembly.GetExecutingAssembly().GetName();
                string[] str = new string[] { Conversions.ToString(name.Version.Major), ".", Conversions.ToString(name.Version.Minor), ".", Conversions.ToString(name.Version.Build) };
                return string.Concat(str);
            }
        }

        public REST() {
            this.InternalState = 0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalProxy = null;
            this.InternalCredentials = null;
            this.InternalIgnoreSSLCertificateErrors = false;
            this.InternalAllowAutoRedirect = true;
            this.InternalTimeoutSeconds = 100;
            this.InternalReadWriteTimeoutSeconds = 300;
            this.InternalLogData = "";
            this.InternalResponseStatusCode = 0;
            this.InternalResponseStatusDescription = "";
            this.InternalResponseHeaders = null;
            this.InternalResponseString = "";
            this.InternalRequestURL = "";
            this.InternalRequestMethod = "";
            this.InternalRequestHeaders = null;
        }

        public void Abort() {
            if (this.MyRequest != null) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Aborting the request\r\n");
                this.MyRequest.Abort();
            }
        }

        public string BuildS3RequestURL(bool UseSSL, string RequestEndpoint, string BucketName, string KeyName, string QueryString) {
            return Shared_Module.BuildS3RequestURLShared(UseSSL, RequestEndpoint, BucketName, KeyName, QueryString);
        }

        public string GetAlexaSignatureValue(string RequestURL, string AWSSecretAccessKey) {
            string str = "";
            return Shared_Module.GetActionTimestampSignatureValueShared(RequestURL, AWSSecretAccessKey, ref str);
        }

        /// <summary>Gets the signature value you will pass in an Alexa Web Information Service or Alexa Top Sites request.</summary>
        /// <param name="RequestURL">The URL you will be making the request against. This includes the query string.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <param name="StringThatWasSigned">This parameter will contain the string that was hashed from the parameters you supplied.  It is useful if you are interested to see how the signature was calculated.</param>
        /// <returns>The Signature value you will use in the query string of the URL you request against.</returns>
        public string GetAlexaSignatureValue(string RequestURL, string AWSSecretAccessKey, ref string StringThatWasSigned) {
            return Shared_Module.GetActionTimestampSignatureValueShared(RequestURL, AWSSecretAccessKey, ref StringThatWasSigned);
        }

        /// <summary>Gets the X-Amzn-Authorization header value for a Amazon Route 53 request.</summary>
        /// <param name="AWSAccessKeyId">Your Amazon access key ID.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <param name="AmzDate">The value you will pass in the x-amz-date header.</param>
        /// <returns>The value you will pass in the X-Amzn-Authorization header.</returns>
        public string GetAWS3AuthorizationValue(string AWSAccessKeyId, string AWSSecretAccessKey, string AmzDate) {
            return Shared_Module.GetAWS3AuthorizationValueShared(AWSAccessKeyId, AWSSecretAccessKey, AmzDate);
        }

        public string GetAWSSignatureVersion4Value(Dictionary<string, string> RequestHeaders, string ServiceURL, string ServiceName, string Region, string RequestMethod, string PostDataHash, string AWSAccessKeyId, string AWSSecretAccessKey) {
            string str = "";
            string str1 = "";
            return Shared_Module.GetAWSSignatureVersion4ValueShared(RequestHeaders, ServiceURL, ServiceName, Region, RequestMethod, PostDataHash, AWSAccessKeyId, AWSSecretAccessKey, ref str, ref str1);
        }

        /// <summary>Gets the Signature Version 4 value which will be used in the Authorization header.</summary>
        /// <param name="RequestHeaders">Headers you will be sending in the request. One header must be x-amz-date.</param>
        /// <param name="ServiceURL">The URL you will be making the request against.</param>
        /// <param name="ServiceName">The service are using. For example glacier.</param>
        /// <param name="Region">The region you are using. For example us-east-1.</param>
        /// <param name="RequestMethod">The request method. This is usually GET, PUT, POST, or DELETE.</param>
        /// <param name="PostDataHash">The SHA-256 hash value of the data you will be sending.</param>
        /// <param name="AWSAccessKeyId">Your Amazon access key ID.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <param name="CanonicalString">This parameter will contain the canonical string that was generated. It is useful if you are interested to see how the signature was calculated.</param>
        /// <param name="StringToSign">This parameter will contain the string that was signed. It is useful if you are interested to see how the signature was calculated.</param>
        /// <returns>The Signature Version 4 value you will pass in the Authorization header.</returns>
        public string GetAWSSignatureVersion4Value(Dictionary<string, string> RequestHeaders, string ServiceURL, string ServiceName, string Region, string RequestMethod, string PostDataHash, string AWSAccessKeyId, string AWSSecretAccessKey, ref string CanonicalString, ref string StringToSign) {
            return Shared_Module.GetAWSSignatureVersion4ValueShared(RequestHeaders, ServiceURL, ServiceName, Region, RequestMethod, PostDataHash, AWSAccessKeyId, AWSSecretAccessKey, ref CanonicalString, ref StringToSign);
        }

        /// <summary>Gets the Authorization header value for a CloudFront request.</summary>
        /// <param name="AWSAccessKeyId">Your Amazon access key ID.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <param name="AmzDate">The value you will pass in the x-amz-date header.</param>
        /// <returns>The value you will pass in the Authorization header.</returns>
        public string GetCloudFrontAuthorizationValue(string AWSAccessKeyId, string AWSSecretAccessKey, string AmzDate) {
            return Shared_Module.GetCloudFrontAuthorizationValueShared(AWSAccessKeyId, AWSSecretAccessKey, AmzDate);
        }

        public string GetDevPaySignatureValue(string RequestURL, string AWSSecretAccessKey) {
            string str = "";
            return Shared_Module.GetDevPaySignatureValueShared(RequestURL, AWSSecretAccessKey, ref str);
        }

        /// <summary>Gets the signature value you will pass in a DevPay request.</summary>
        /// <param name="RequestURL">The URL you will be making the request against. This includes the query string.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <param name="StringThatWasSigned">This parameter will contain the string that was hashed from the parameters you supplied.  It is useful if you are interested to see how the signature was calculated.</param>
        /// <returns>The Signature value you will use in the query string of the URL you request against.</returns>
        public string GetDevPaySignatureValue(string RequestURL, string AWSSecretAccessKey, ref string StringThatWasSigned) {
            return Shared_Module.GetDevPaySignatureValueShared(RequestURL, AWSSecretAccessKey, ref StringThatWasSigned);
        }

        public string GetFulfillmentWebServiceSignatureValue(string RequestURL, string AWSSecretAccessKey) {
            string str = "";
            return Shared_Module.GetActionTimestampSignatureValueShared(RequestURL, AWSSecretAccessKey, ref str);
        }

        /// <summary>Gets the signature value you will pass in an Amazon Fulfillment Web Service request.</summary>
        /// <param name="RequestURL">The URL you will be making the request against. This includes the query string.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <param name="StringThatWasSigned">This parameter will contain the string that was hashed from the parameters you supplied.  It is useful if you are interested to see how the signature was calculated.</param>
        /// <returns>The Signature value you will use in the query string of the URL you request against.</returns>
        public string GetFulfillmentWebServiceSignatureValue(string RequestURL, string AWSSecretAccessKey, ref string StringThatWasSigned) {
            return Shared_Module.GetActionTimestampSignatureValueShared(RequestURL, AWSSecretAccessKey, ref StringThatWasSigned);
        }

        public string GetMechanicalTurkSignatureValue(string RequestURL, string AWSSecretAccessKey) {
            string str = "";
            return Shared_Module.GetMechanicalTurkSignatureValueShared(RequestURL, AWSSecretAccessKey, ref str);
        }

        /// <summary>Gets the signature value you will pass in a Mechanical Turk request.</summary>
        /// <param name="RequestURL">The URL you will be making the request against. This includes the query string.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <param name="StringThatWasSigned">This parameter will contain the string that was hashed from the parameters you supplied.  It is useful if you are interested to see how the signature was calculated.</param>
        /// <returns>The Signature value you will use in the query string of the URL you request against.</returns>
        public string GetMechanicalTurkSignatureValue(string RequestURL, string AWSSecretAccessKey, ref string StringThatWasSigned) {
            return Shared_Module.GetMechanicalTurkSignatureValueShared(RequestURL, AWSSecretAccessKey, ref StringThatWasSigned);
        }

        public string GetS3AuthorizationValue(string RequestURL, string RequestMethod, Dictionary<string, string> ExtraRequestHeaders, string AWSAccessKeyId, string AWSSecretAccessKey) {
            string str = "";
            return Shared_Module.GetS3AuthorizationValueShared(RequestURL, RequestMethod, ExtraRequestHeaders, AWSAccessKeyId, AWSSecretAccessKey, ref str);
        }

        /// <summary>Gets the Authorization header value for a S3 request.</summary>
        /// <param name="RequestURL">The URL you will be making the request against. This includes the query string.</param>
        /// <param name="RequestMethod">The request method.  This is usually GET, PUT, POST, COPY, or DELETE.</param>
        /// <param name="ExtraRequestHeaders">Extra headers you will be sending in the request.  One header must be x-amz-date.</param>
        /// <param name="AWSAccessKeyId">Your Amazon access key ID.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <param name="StringThatWasSigned">This parameter will contain the string that was hashed from the parameters you supplied.  It is useful if you are interested to see how the signature was calculated.</param>
        /// <returns>The value you will pass in the Authorization header.</returns>
        public string GetS3AuthorizationValue(string RequestURL, string RequestMethod, Dictionary<string, string> ExtraRequestHeaders, string AWSAccessKeyId, string AWSSecretAccessKey, ref string StringThatWasSigned) {
            return Shared_Module.GetS3AuthorizationValueShared(RequestURL, RequestMethod, ExtraRequestHeaders, AWSAccessKeyId, AWSSecretAccessKey, ref StringThatWasSigned);
        }

        public string GetS3AuthorizationValueByParts(string BucketName, string KeyName, string QueryString, string RequestMethod, Dictionary<string, string> ExtraRequestHeaders, string AWSAccessKeyId, string AWSSecretAccessKey) {
            string str = "";
            return Shared_Module.GetS3AuthorizationValueByPartsShared(BucketName, KeyName, QueryString, RequestMethod, ExtraRequestHeaders, AWSAccessKeyId, AWSSecretAccessKey, ref str);
        }

        /// <summary>Gets the Authorization header value for a S3 request by specifying the components of the request.</summary>
        /// <param name="BucketName">The bucket name you will make a request against.</param>
        /// <param name="KeyName">The key name you will make a request against.</param>
        /// <param name="QueryString">The query string you will include in the URL.</param>
        /// <param name="RequestMethod">The request method.  This is usually GET, PUT, POST, COPY, or DELETE.</param>
        /// <param name="ExtraRequestHeaders">Extra headers you will be sending in the request.  One header must be x-amz-date.</param>
        /// <param name="AWSAccessKeyId">Your Amazon access key ID.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <param name="StringThatWasSigned">This parameter will contain the string that was hashed from the parameters you supplied.  It is useful if you are interested to see how the signature was calculated.</param>
        /// <returns>The value you will pass in the Authorization header.</returns>
        public string GetS3AuthorizationValueByParts(string BucketName, string KeyName, string QueryString, string RequestMethod, Dictionary<string, string> ExtraRequestHeaders, string AWSAccessKeyId, string AWSSecretAccessKey, ref string StringThatWasSigned) {
            return Shared_Module.GetS3AuthorizationValueByPartsShared(BucketName, KeyName, QueryString, RequestMethod, ExtraRequestHeaders, AWSAccessKeyId, AWSSecretAccessKey, ref StringThatWasSigned);
        }

        /// <summary>Gets the signature value when the required SignatureVersion is 2.  This type of signature is used for EC2, SimpleDB, RDS, SQS, SNS and many other Amazon services.</summary>
        /// <param name="RequestURL">The URL you will be making the request against. This includes the query string.</param>
        /// <param name="RequestMethod">The request method.  This is usually GET, PUT, POST, COPY, or DELETE.</param>
        /// <param name="SendData">The data you will be sending in a POST request.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <returns>The Signature value you will use in the query string of the URL you request against.</returns>
        public string GetSignatureVersion2Value(string RequestURL, string RequestMethod, string SendData, string AWSSecretAccessKey) {
            string str = "";
            return Shared_Module.GetSignatureVersion2ValueShared(RequestURL, RequestMethod, SendData, AWSSecretAccessKey, ref str);
        }

        public string GetSignatureVersion2Value(string RequestURL, string RequestMethod, string SendData, string AWSSecretAccessKey, ref string StringThatWasSigned) {
            return Shared_Module.GetSignatureVersion2ValueShared(RequestURL, RequestMethod, SendData, AWSSecretAccessKey, ref StringThatWasSigned);
        }

        private bool MakeGetRequest(string RequestURL, string Method, Dictionary<string, string> ExtraRequestHeaders) {
            HttpWebResponse response;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            Dictionary<string, string>.Enumerator enumerator = new Dictionary<string, string>.Enumerator();
            IEnumerator enumerator1 = null;
            object[] objectValue;
            IEnumerator enumerator2 = null;
            IEnumerator enumerator3 = null;
            bool flag = true;
            this.InternalState = 1;
            string[] internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            REST.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            this.InternalRequestURL = RequestURL;
            this.InternalRequestMethod = Method;
            if (this.InternalIgnoreSSLCertificateErrors) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Setting ServerCertificateValidationCallback\r\n");
                ServicePointManager.ServerCertificateValidationCallback = null;
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(SharedModule.CertificateValidationCallBack);
            }
            this.MyRequest = (HttpWebRequest)WebRequest.Create(RequestURL);
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Method=", Method, "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            this.MyRequest.AllowAutoRedirect = this.InternalAllowAutoRedirect;
            this.MyRequest.Method = Method;
            this.MyRequest.KeepAlive = true;
            this.MyRequest.ServicePoint.ConnectionLimit = 20;
            this.MyRequest.Timeout = checked(this.InternalTimeoutSeconds * 1000);
            this.MyRequest.ReadWriteTimeout = checked(this.InternalReadWriteTimeoutSeconds * 1000);
            this.MyRequest.Credentials = CredentialCache.DefaultCredentials;
            if (this.InternalCredentials != null) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Setting credentials\r\n");
                this.MyRequest.Credentials = this.InternalCredentials;
            }
            if (ExtraRequestHeaders != null) {
                string str = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
                try {
                    enumerator = ExtraRequestHeaders.GetEnumerator();
                    while (enumerator.MoveNext()) {
                        KeyValuePair<string, string> current = enumerator.Current;
                        int num = Strings.Len(current.Value);
                        int num1 = 1;
                        while (num1 <= num) {
                            if (Strings.InStr(str, Strings.Mid(current.Value, num1, 1), CompareMethod.Binary) != 0) {
                                num1 = checked(num1 + 1);
                            }
                            else {
                                this.InternalErrorNumber = 1001;
                                int internalErrorNumber = this.InternalErrorNumber;
                                internalLogData = new string[] { "Key=", current.Key, "   Value=", current.Value, "   Invalid Character=", Strings.Mid(current.Value, num1, 1) };
                                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(internalErrorNumber, string.Concat(internalLogData));
                                flag = false;
                                goto Label0;
                            }
                        }
                        if (Operators.CompareString(Strings.LCase(current.Key), "content-type", false) == 0) {
                            this.MyRequest.ContentType = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "user-agent", false) == 0) {
                            this.MyRequest.UserAgent = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "referer", false) == 0) {
                            this.MyRequest.Referer = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "accept", false) == 0) {
                            this.MyRequest.Accept = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "if-modified-since", false) == 0) {
                            this.MyRequest.IfModifiedSince = Conversions.ToDate(current.Value);
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "host", false) != 0) {
                            try {
                                this.MyRequest.Headers[current.Key] = current.Value;
                            }
                            catch (Exception exception1) {
                                ProjectData.SetProjectError(exception1);
                                Exception exception = exception1;
                                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error adding request header   RequestHeader=", current.Key, ":", current.Value, "   Message=", exception.Message, "\r\n" };
                                this.InternalLogData = string.Concat(internalLogData);
                                this.InternalErrorNumber = 1002;
                                int internalErrorNumber1 = this.InternalErrorNumber;
                                internalLogData = new string[] { "Message=", exception.Message, "   Header Name=", current.Key, "   Header Value=", current.Value };
                                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(internalErrorNumber1, string.Concat(internalLogData));
                                flag = false;
                                ProjectData.ClearProjectError();
                                goto Label0;
                            }
                        }
                    }
                }
                finally {
                    ((IDisposable)enumerator).Dispose();
                }
            }
            if (Operators.CompareString(this.MyRequest.UserAgent, "", false) == 0) {
                this.MyRequest.UserAgent = string.Concat("SoftAWS/", this.Version);
            }
            if (!Information.IsNothing(this.InternalProxy)) {
                this.MyRequest.Proxy = this.InternalProxy;
                Uri proxy = this.InternalProxy.GetProxy(this.MyRequest.Address);
                if (this.MyRequest.Address != proxy) {
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Proxy server set   Proxy.Address=", proxy.AbsoluteUri, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                }
            }
            try {
                enumerator1 = this.MyRequest.Headers.GetEnumerator();
                while (enumerator1.MoveNext()) {
                    object obj = RuntimeHelpers.GetObjectValue(enumerator1.Current);
                    object obj1 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   RequestHeader="), obj), ":");
                    WebHeaderCollection headers = this.MyRequest.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj) };
                    this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj1, NewLateBinding.LateGet(headers, null, "Item", objectValue, null, null, null)), "\r\n"));
                    Dictionary<string, string> internalRequestHeaders = this.InternalRequestHeaders;
                    string str1 = Conversions.ToString(obj);
                    WebHeaderCollection webHeaderCollection = this.MyRequest.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj) };
                    internalRequestHeaders.Add(str1, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection, null, "Item", objectValue, null, null, null)));
                }
            }
            finally {
                if (enumerator1 is IDisposable) {
                    (enumerator1 as IDisposable).Dispose();
                }
            }
            try {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response\r\n");
                response = (HttpWebResponse)this.MyRequest.GetResponse();
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response was successful\r\n");
            }
            catch (WebException webException1) {
                ProjectData.SetProjectError(webException1);
                WebException webException = webException1;
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response failed   Message=", webException.Message, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                flag = false;
                this.InternalErrorNumber = 1003;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", webException.Message));
                HttpWebResponse httpWebResponse = (HttpWebResponse)webException.Response;
                if (!Information.IsNothing(httpWebResponse)) {
                    this.InternalResponseStatusCode = (int)httpWebResponse.StatusCode;
                    this.InternalResponseStatusDescription = httpWebResponse.StatusDescription;
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusCode=", Conversions.ToString((int)httpWebResponse.StatusCode), "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusDescription=", httpWebResponse.StatusDescription, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    try {
                        enumerator2 = httpWebResponse.Headers.GetEnumerator();
                        while (enumerator2.MoveNext()) {
                            object objectValue1 = RuntimeHelpers.GetObjectValue(enumerator2.Current);
                            object obj2 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   ResponseHeader="), objectValue1), ":");
                            WebHeaderCollection headers1 = httpWebResponse.Headers;
                            objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue1) };
                            this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj2, NewLateBinding.LateGet(headers1, null, "Item", objectValue, null, null, null)), "\r\n"));
                            Dictionary<string, string> internalResponseHeaders = this.InternalResponseHeaders;
                            string str2 = Conversions.ToString(objectValue1);
                            WebHeaderCollection webHeaderCollection1 = httpWebResponse.Headers;
                            objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue1) };
                            internalResponseHeaders.Add(str2, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection1, null, "Item", objectValue, null, null, null)));
                        }
                    }
                    finally {
                        if (enumerator2 is IDisposable) {
                            (enumerator2 as IDisposable).Dispose();
                        }
                    }
                    Stream responseStream = httpWebResponse.GetResponseStream();
                    try {
                        this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the error response stream\r\n");
                        string end = (new StreamReader(responseStream, true)).ReadToEnd();
                        this.InternalResponseString = end;
                        this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the error response stream was successful\r\n");
                        internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   StreamData=", end, "\r\n" };
                        this.InternalLogData = string.Concat(internalLogData);
                    }
                    catch (Exception exception3) {
                        ProjectData.SetProjectError(exception3);
                        Exception exception2 = exception3;
                        internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error getting the error response stream   Message=", exception2.Message, "\r\n" };
                        this.InternalLogData = string.Concat(internalLogData);
                        ProjectData.ClearProjectError();
                    }
                    httpWebResponse.Close();
                }
                this.MyRequest.Abort();
                ProjectData.ClearProjectError();
                goto Label0;
            }
            catch (Exception exception5) {
                ProjectData.SetProjectError(exception5);
                Exception exception4 = exception5;
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error getting the response   Message=", exception4.Message, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                flag = false;
                this.InternalErrorNumber = 1003;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception4.Message));
                this.MyRequest.Abort();
                ProjectData.ClearProjectError();
                goto Label0;
            }
            this.InternalResponseStatusCode = (int)response.StatusCode;
            this.InternalResponseStatusDescription = response.StatusDescription;
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusCode=", Conversions.ToString((int)response.StatusCode), "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusDescription=", response.StatusDescription, "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            try {
                enumerator3 = response.Headers.GetEnumerator();
                while (enumerator3.MoveNext()) {
                    object objectValue2 = RuntimeHelpers.GetObjectValue(enumerator3.Current);
                    object obj3 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   ResponseHeader="), objectValue2), ":");
                    WebHeaderCollection headers2 = response.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue2) };
                    this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj3, NewLateBinding.LateGet(headers2, null, "Item", objectValue, null, null, null)), "\r\n"));
                    Dictionary<string, string> strs = this.InternalResponseHeaders;
                    string str3 = Conversions.ToString(objectValue2);
                    WebHeaderCollection webHeaderCollection2 = response.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue2) };
                    strs.Add(str3, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection2, null, "Item", objectValue, null, null, null)));
                }
            }
            finally {
                if (enumerator3 is IDisposable) {
                    (enumerator3 as IDisposable).Dispose();
                }
            }
            Stream stream = response.GetResponseStream();
            try {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response stream\r\n");
                string end1 = (new StreamReader(stream, true)).ReadToEnd();
                this.InternalResponseString = end1;
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response stream was successful\r\n");
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   StreamData=", end1, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
            }
            catch (Exception exception7) {
                ProjectData.SetProjectError(exception7);
                Exception exception6 = exception7;
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error getting the response stream   Message=", exception6.Message, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                flag = false;
                this.InternalErrorNumber = 1006;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception6.Message));
                this.MyRequest.Abort();
                ProjectData.ClearProjectError();
            }
            response.Close();
            Label0:
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ErrorNumber=", Conversions.ToString(this.InternalErrorNumber), "   ErrorDescription=", this.InternalErrorDescription, "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            this.InternalState = 0;
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            return flag;
        }

        private bool MakePostRequest(string RequestURL, string Method, Dictionary<string, string> ExtraRequestHeaders, string SendData) {
            Stream requestStream = null;
            HttpWebResponse response = null;
            Dictionary<string, string>.Enumerator enumerator = new Dictionary<string, string>.Enumerator();
            IEnumerator enumerator1 = null;
            object[] objectValue;
            IEnumerator enumerator2 = null;
            IEnumerator enumerator3 = null;
            bool flag = true;
            bool flag1 = false;
            this.InternalState = 1;
            string[] internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            REST.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            this.InternalRequestURL = RequestURL;
            this.InternalRequestMethod = Method;
            ServicePointManager.ServerCertificateValidationCallback = null;
            if (this.InternalIgnoreSSLCertificateErrors) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Setting ServerCertificateValidationCallback\r\n");
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(SharedModule.CertificateValidationCallBack);
            }
            this.MyRequest = (HttpWebRequest)WebRequest.Create(RequestURL);
            this.MyRequest.AllowAutoRedirect = this.InternalAllowAutoRedirect;
            this.MyRequest.Method = Method;
            this.MyRequest.KeepAlive = true;
            this.MyRequest.ServicePoint.Expect100Continue = true;
            this.MyRequest.ServicePoint.ConnectionLimit = 20;
            this.MyRequest.Timeout = checked(this.InternalTimeoutSeconds * 1000);
            this.MyRequest.ReadWriteTimeout = checked(this.InternalReadWriteTimeoutSeconds * 1000);
            this.MyRequest.ContentLength = (long)Encoding.UTF8.GetByteCount(SendData);
            this.MyRequest.Credentials = CredentialCache.DefaultCredentials;
            if (this.InternalCredentials != null) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Setting credentials\r\n");
                this.MyRequest.Credentials = this.InternalCredentials;
            }
            if (ExtraRequestHeaders != null) {
                string str = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
                try {
                    enumerator = ExtraRequestHeaders.GetEnumerator();
                    while (enumerator.MoveNext()) {
                        KeyValuePair<string, string> current = enumerator.Current;
                        int num = Strings.Len(current.Value);
                        int num1 = 1;
                        while (num1 <= num) {
                            if (Strings.InStr(str, Strings.Mid(current.Value, num1, 1), CompareMethod.Binary) != 0) {
                                num1 = checked(num1 + 1);
                            }
                            else {
                                this.InternalErrorNumber = 1001;
                                int internalErrorNumber = this.InternalErrorNumber;
                                internalLogData = new string[] { "Key=", current.Key, "   Value=", current.Value, "   Invalid Character=", Strings.Mid(current.Value, num1, 1) };
                                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(internalErrorNumber, string.Concat(internalLogData));
                                flag = false;
                                goto Label0;
                            }
                        }
                        if (Operators.CompareString(Strings.LCase(current.Key), "content-type", false) == 0) {
                            this.MyRequest.ContentType = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "user-agent", false) == 0) {
                            this.MyRequest.UserAgent = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "referer", false) == 0) {
                            this.MyRequest.Referer = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "accept", false) == 0) {
                            this.MyRequest.Accept = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "if-modified-since", false) == 0) {
                            this.MyRequest.IfModifiedSince = Conversions.ToDate(current.Value);
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "host", false) != 0) {
                            try {
                                this.MyRequest.Headers[current.Key] = current.Value;
                            }
                            catch (Exception exception1) {
                                ProjectData.SetProjectError(exception1);
                                Exception exception = exception1;
                                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error adding request header   RequestHeader=", current.Key, ":", current.Value, "   Message=", exception.Message, "\r\n" };
                                this.InternalLogData = string.Concat(internalLogData);
                                this.InternalErrorNumber = 1002;
                                int internalErrorNumber1 = this.InternalErrorNumber;
                                internalLogData = new string[] { "Message=", exception.Message, "   Header Name=", current.Key, "   Header Value=", current.Value };
                                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(internalErrorNumber1, string.Concat(internalLogData));
                                flag = false;
                                ProjectData.ClearProjectError();
                                goto Label0;
                            }
                        }
                    }
                }
                finally {
                    ((IDisposable)enumerator).Dispose();
                }
            }
            if (Operators.CompareString(this.MyRequest.UserAgent, "", false) == 0) {
                this.MyRequest.UserAgent = string.Concat("SoftAWS/", this.Version);
            }
            if (!Information.IsNothing(this.InternalProxy)) {
                this.MyRequest.Proxy = this.InternalProxy;
                Uri proxy = this.InternalProxy.GetProxy(this.MyRequest.Address);
                if (this.MyRequest.Address != proxy) {
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Proxy server set   Proxy.Address=", proxy.AbsoluteUri, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                }
            }
            try {
                enumerator1 = this.MyRequest.Headers.GetEnumerator();
                while (enumerator1.MoveNext()) {
                    object obj = RuntimeHelpers.GetObjectValue(enumerator1.Current);
                    object obj1 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   RequestHeader="), obj), ":");
                    WebHeaderCollection headers = this.MyRequest.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj) };
                    this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj1, NewLateBinding.LateGet(headers, null, "Item", objectValue, null, null, null)), "\r\n"));
                    Dictionary<string, string> internalRequestHeaders = this.InternalRequestHeaders;
                    string str1 = Conversions.ToString(obj);
                    WebHeaderCollection webHeaderCollection = this.MyRequest.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj) };
                    internalRequestHeaders.Add(str1, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection, null, "Item", objectValue, null, null, null)));
                }
            }
            finally {
                if (enumerator1 is IDisposable) {
                    (enumerator1 as IDisposable).Dispose();
                }
            }
            try {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the request stream\r\n");
                requestStream = this.MyRequest.GetRequestStream();
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the request stream was successful\r\n");
            }
            catch (Exception exception3) {
                ProjectData.SetProjectError(exception3);
                Exception exception2 = exception3;
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the request stream failed   Message=", exception2.Message, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                this.InternalErrorNumber = 1004;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception2.Message));
                flag = false;
                ProjectData.ClearProjectError();
                goto Label0;
            }
            flag1 = true;
            byte[] bytes = Encoding.UTF8.GetBytes(SendData);
            try {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Sending the request\r\n");
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   SendData=", SendData, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                requestStream.Write(bytes, 0, checked((int)bytes.Length));
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Sending the request was successful\r\n");
            }
            catch (Exception exception5) {
                ProjectData.SetProjectError(exception5);
                Exception exception4 = exception5;
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Sending the request failed   Message=", exception4.Message, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                this.InternalErrorNumber = 1142;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception4.Message));
                flag = false;
                ProjectData.ClearProjectError();
            }
            Label0:
            if (requestStream != null) {
                try {
                    requestStream.Close();
                }
                catch (Exception exception6) {
                    ProjectData.SetProjectError(exception6);
                    ProjectData.ClearProjectError();
                }
                requestStream = null;
            }
            if (flag1) {
                try {
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response\r\n");
                    response = (HttpWebResponse)this.MyRequest.GetResponse();
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response was successful\r\n");
                }
                catch (WebException webException1) {
                    ProjectData.SetProjectError(webException1);
                    WebException webException = webException1;
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response failed   Message=", webException.Message, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    if (flag) {
                        flag = false;
                        this.InternalErrorNumber = 1003;
                        this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", webException.Message));
                        HttpWebResponse httpWebResponse = (HttpWebResponse)webException.Response;
                        if (!Information.IsNothing(httpWebResponse)) {
                            this.InternalResponseStatusCode = (int)httpWebResponse.StatusCode;
                            this.InternalResponseStatusDescription = httpWebResponse.StatusDescription;
                            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusCode=", Conversions.ToString((int)httpWebResponse.StatusCode), "\r\n" };
                            this.InternalLogData = string.Concat(internalLogData);
                            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusDescription=", httpWebResponse.StatusDescription, "\r\n" };
                            this.InternalLogData = string.Concat(internalLogData);
                            try {
                                enumerator2 = httpWebResponse.Headers.GetEnumerator();
                                while (enumerator2.MoveNext()) {
                                    object objectValue1 = RuntimeHelpers.GetObjectValue(enumerator2.Current);
                                    object obj2 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   ResponseHeader="), objectValue1), ":");
                                    WebHeaderCollection headers1 = httpWebResponse.Headers;
                                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue1) };
                                    this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj2, NewLateBinding.LateGet(headers1, null, "Item", objectValue, null, null, null)), "\r\n"));
                                    Dictionary<string, string> internalResponseHeaders = this.InternalResponseHeaders;
                                    string str2 = Conversions.ToString(objectValue1);
                                    WebHeaderCollection webHeaderCollection1 = httpWebResponse.Headers;
                                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue1) };
                                    internalResponseHeaders.Add(str2, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection1, null, "Item", objectValue, null, null, null)));
                                }
                            }
                            finally {
                                if (enumerator2 is IDisposable) {
                                    (enumerator2 as IDisposable).Dispose();
                                }
                            }
                            Stream responseStream = httpWebResponse.GetResponseStream();
                            try {
                                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the error response stream\r\n");
                                string end = (new StreamReader(responseStream, true)).ReadToEnd();
                                this.InternalResponseString = end;
                                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the error response stream was successful\r\n");
                                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   StreamData=", end, "\r\n" };
                                this.InternalLogData = string.Concat(internalLogData);
                            }
                            catch (Exception exception8) {
                                ProjectData.SetProjectError(exception8);
                                Exception exception7 = exception8;
                                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error getting the error response stream   Message=", exception7.Message, "\r\n" };
                                this.InternalLogData = string.Concat(internalLogData);
                                ProjectData.ClearProjectError();
                            }
                            httpWebResponse.Close();
                        }
                    }
                    this.MyRequest.Abort();
                    ProjectData.ClearProjectError();
                }
                catch (Exception exception10) {
                    ProjectData.SetProjectError(exception10);
                    Exception exception9 = exception10;
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error getting the response   Message=", exception9.Message, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    flag = false;
                    this.InternalErrorNumber = 1003;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception9.Message));
                    this.MyRequest.Abort();
                    ProjectData.ClearProjectError();
                }
            }
            if (response != null) {
                this.InternalResponseStatusCode = (int)response.StatusCode;
                this.InternalResponseStatusDescription = response.StatusDescription;
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusCode=", Conversions.ToString((int)response.StatusCode), "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusDescription=", response.StatusDescription, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                try {
                    enumerator3 = response.Headers.GetEnumerator();
                    while (enumerator3.MoveNext()) {
                        object objectValue2 = RuntimeHelpers.GetObjectValue(enumerator3.Current);
                        object obj3 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   ResponseHeader="), objectValue2), ":");
                        WebHeaderCollection headers2 = response.Headers;
                        objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue2) };
                        this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj3, NewLateBinding.LateGet(headers2, null, "Item", objectValue, null, null, null)), "\r\n"));
                        Dictionary<string, string> strs = this.InternalResponseHeaders;
                        string str3 = Conversions.ToString(objectValue2);
                        WebHeaderCollection webHeaderCollection2 = response.Headers;
                        objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue2) };
                        strs.Add(str3, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection2, null, "Item", objectValue, null, null, null)));
                    }
                }
                finally {
                    if (enumerator3 is IDisposable) {
                        (enumerator3 as IDisposable).Dispose();
                    }
                }
                Stream stream = response.GetResponseStream();
                try {
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response stream\r\n");
                    string end1 = (new StreamReader(stream, true)).ReadToEnd();
                    this.InternalResponseString = end1;
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response stream was successful\r\n");
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   StreamData=", end1, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                }
                catch (Exception exception12) {
                    ProjectData.SetProjectError(exception12);
                    Exception exception11 = exception12;
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error getting the response stream   Message=", exception11.Message, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    flag = false;
                    this.InternalErrorNumber = 1006;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception11.Message));
                    ProjectData.ClearProjectError();
                }
                response.Close();
            }
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ErrorNumber=", Conversions.ToString(this.InternalErrorNumber), "   ErrorDescription=", this.InternalErrorDescription, "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            this.InternalState = 0;
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            return flag;
        }

        /// <summary>Makes a REST requets.</summary>
        /// <param name="RequestURL">The URL to make the request against.</param>
        /// <param name="Method">The request method.  This is usually GET, PUT, POST, COPY, or DELETE.</param>
        /// <param name="ExtraRequestHeaders">Extra headers to send in the request.</param>
        /// <param name="SendData">The data to send when making a PUT or POST request.</param>
        /// <returns>Returns True if the request was successful.</returns>
        public bool MakeRequest(string RequestURL, string Method, Dictionary<string, string> ExtraRequestHeaders, string SendData = "") {
            bool flag;
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalResponseString = "";
            this.InternalLogData = "";
            this.InternalResponseStatusCode = 0;
            this.InternalResponseStatusDescription = "";
            this.InternalResponseHeaders = new Dictionary<string, string>();
            this.InternalRequestURL = "";
            this.InternalRequestMethod = "";
            this.InternalRequestHeaders = new Dictionary<string, string>();
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   RequestURL=", RequestURL, "   Method=", Method, "   ExtraRequestHeaders=", Shared_Module.DictionaryToString(ExtraRequestHeaders), "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            if (!(Operators.CompareString(Method.ToUpper(), "PUT", false) == 0 | Operators.CompareString(Method.ToUpper(), "POST", false) == 0)) {
                flag = this.MakeGetRequest(RequestURL, Method, ExtraRequestHeaders);
                name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", Conversions.ToString(flag), "\r\n" };
                this.InternalLogData = string.Concat(name);
                return flag;
            }
            flag = this.MakePostRequest(RequestURL, Method, ExtraRequestHeaders, SendData);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", Conversions.ToString(flag), "\r\n" };
            this.InternalLogData = string.Concat(name);
            return flag;
        }

        /// <summary>Fires when the value of the State property is changed.</summary>
        public event REST.StateChangedEventEventHandler StateChangedEvent;

        public delegate void StateChangedEventEventHandler();
    }
}
