﻿using Jaeger.SoftAWS;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;

public partial class Helper {
    public Helper() {
    }

    /// <summary>Decodes a Base64 string.</summary>
    /// <param name="Base64String">The string in Base64 format.</param>
    /// <returns>The decoded string.</returns>
    public string Base64ToString(string Base64String) {
        byte[] numArray = Convert.FromBase64String(Base64String);
        return Encoding.UTF8.GetString(numArray);
    }

    /// <summary>Creates an authentication URL used to retrieve an object that is not public.</summary>
    /// <param name="UseSSL">Sets if the URL will use http or https.</param>
    /// <param name="RequestEndpoint">Sets the Amazon S3 domain name in the URL.  This is usually s3.amazonaws.com.</param>
    /// <param name="BucketName">The bucket name where the object resides.</param>
    /// <param name="KeyName">The key name of the object to download.</param>
    /// <param name="QueryString">A query string to add to the URL.  Set to x-amz-request-payer=requester if you want to create an authentication URL for a requester pays bucket.  You can also use versionId=[YOUR_VERSION_ID] to generate a URL for a specific version.</param>
    /// <param name="Expires">The date the URL is valid until.</param>
    /// <param name="AWSAccessKeyId">The Amazon Access Key ID to use.</param>
    /// <param name="AWSSecretAccessKey">The Amazon Secret Access Key to use.</param>
    /// <returns>An authenticated URL.</returns>
    /// <remarks>See <see href="http://docs.amazonwebservices.com/AmazonS3/latest/S3_QSAuth.html">Amazon: Query String Authentication</see> and <see href="http://docs.amazonwebservices.com/AmazonS3/latest/dev/RESTAuthentication.html">Amazon: Query String Request Authentication Alternative</see>.</remarks>
    public string BuildExpiresURL(bool UseSSL, string RequestEndpoint, string BucketName, string KeyName, string QueryString, DateTime Expires, string AWSAccessKeyId, string AWSSecretAccessKey) {
        string[] bucketName;
        DateTime dateTime = new DateTime(1970, 1, 1);
        TimeSpan universalTime = Expires.ToUniversalTime() - dateTime;
        long num = checked((long)Math.Round(universalTime.TotalSeconds));
        string str = "";
        str = string.Concat(str, "GET\n");
        str = string.Concat(str, "\n");
        str = string.Concat(str, "\n");
        str = string.Concat(str, Conversions.ToString(num), "\n");
        string str1 = "";
        string str2 = "";
        if (Operators.CompareString(QueryString, "", false) != 0) {
            string queryString = QueryString;
            if (Strings.InStr(queryString, "?", CompareMethod.Binary) == 1) {
                queryString = Strings.Right(queryString, checked(Strings.Len(queryString) - 1));
            }
            string[] strArrays = Strings.Split(queryString, "&", -1, CompareMethod.Binary);
            int num1 = Information.LBound(strArrays, 1);
            int num2 = Information.UBound(strArrays, 1);
            for (int i = num1; i <= num2; i = checked(i + 1)) {
                if (Strings.InStr(strArrays[i], "=", CompareMethod.Binary) > 0) {
                    string[] strArrays1 = Strings.Split(strArrays[i], "=", -1, CompareMethod.Binary);
                    if (Strings.InStr(Strings.LCase(strArrays1[0]), "x-amz-", CompareMethod.Binary) != 1) {
                        str2 = string.Concat("&", str2, strArrays[i]);
                    }
                    else {
                        bucketName = new string[] { str, Strings.LCase(strArrays1[0]), ":", strArrays1[1], "\n" };
                        str = string.Concat(bucketName);
                    }
                    str1 = string.Concat("&", str1, strArrays[i]);
                }
            }
        }
        bucketName = new string[] { str, "/", BucketName, "/", Uri.EscapeDataString(KeyName) };
        str = string.Concat(bucketName);
        if (Operators.CompareString(str2, "", false) != 0) {
            str = string.Concat(str, "?", Strings.Right(str2, checked(Strings.Len(str2) - 1)));
        }
        HMACSHA1 hMACSHA1 = new HMACSHA1(Encoding.UTF8.GetBytes(AWSSecretAccessKey));
        string base64String = Convert.ToBase64String(hMACSHA1.ComputeHash(Encoding.UTF8.GetBytes(str)));
        string str3 = "";
        str3 = (!UseSSL ? string.Concat(str3, "http://") : string.Concat(str3, "https://"));
        if (Operators.CompareString(RequestEndpoint, "", false) == 0) {
            RequestEndpoint = "s3.amazonaws.com";
        }
        if (!Strings.LCase(RequestEndpoint).EndsWith(".amazonaws.com")) {
            str3 = string.Concat(str3, RequestEndpoint, "/");
        }
        else {
            bool isVirtualHostedBucket = Shared_Module.GetIsVirtualHostedBucket(BucketName);
            if (isVirtualHostedBucket) {
                str3 = string.Concat(str3, BucketName, ".");
            }
            str3 = string.Concat(str3, RequestEndpoint, "/");
            if (!isVirtualHostedBucket) {
                str3 = string.Concat(str3, BucketName, "/");
            }
        }
        str3 = string.Concat(str3, Uri.EscapeDataString(KeyName));
        bucketName = new string[] { str3, "?AWSAccessKeyId=", AWSAccessKeyId, "&Expires=", Conversions.ToString(num), "&Signature=", Uri.EscapeDataString(base64String) };
        str3 = string.Concat(bucketName);
        if (Operators.CompareString(str1, "", false) != 0) {
            str3 = string.Concat(str3, str1);
        }
        return str3;
    }

    /// <summary>Converts an ETag hash into a MD5 hash.</summary>
    /// <param name="ETag">A hash in ETag format.</param>
    /// <returns>The hash in MD5 format.</returns>
    /// <remarks>This function is useful it took a long time to calculate an ETag hash and you want to quickly convert it to a MD5 hash.</remarks>
    public string ConvertETagToMD5(string ETag) {
        byte[] numArray = new byte[checked(checked((int)Math.Round((double)Strings.Len(ETag) / 2 - 1)) + 1)];
        int num = checked(Strings.Len(ETag) - 1);
        for (int i = 0; i <= num; i = checked(i + 2)) {
            numArray[checked((int)Math.Round((double)i / 2))] = Convert.ToByte(ETag.Substring(i, 2), 16);
        }
        return Convert.ToBase64String(numArray);
    }

    public string ConvertMD5ToETag(string MD5) {
        byte[] numArray = Convert.FromBase64String(MD5);
        StringBuilder stringBuilder = new StringBuilder();
        byte[] numArray1 = numArray;
        for (int i = 0; i < checked((int)numArray1.Length); i = checked(i + 1)) {
            byte num = numArray1[i];
            stringBuilder.Append(num.ToString("x2"));
        }
        return stringBuilder.ToString();
    }

    [SecurityCritical]
    public string FormatByteSize(long MySize) {
        StringBuilder stringBuilder = new StringBuilder(256);
        Helper.StrFormatByteSize(MySize, stringBuilder, stringBuilder.Capacity);
        return stringBuilder.ToString();
    }

    public string FormatDateISO8601(DateTime InputDate) {
        return InputDate.ToString("yyyy-MM-dd\\THH:mm:ss.fff\\Z");
    }

    /// <summary>Returns a date in RFC 1123 format.</summary>
    /// <param name="InputDate">The date.</param>
    /// <returns>The date in RFC 1123 format.</returns>
    public string FormatDateRFC1123(DateTime InputDate) {
        return InputDate.ToString("r");
    }

    /// <summary>Gets the ActiveTimeBias registry value.</summary>
    /// <returns>The difference in minutes between Greenwich Mean Time and the local time.</returns>
    /// <remarks>This is useful to determine the current time zone.</remarks>
    public int GetActiveTimeBias() {
        RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Control\\TimeZoneInformation");
        int integer = Conversions.ToInteger(registryKey.GetValue("ActiveTimeBias"));
        registryKey.Close();
        return integer;
    }

    public Dictionary<string, string> GetContentTypesDictionary() {
        IEnumerator enumerator = null;
        ArrayList arrayLists = new ArrayList();
        arrayLists.Add(".323|text/h323");
        arrayLists.Add(".aaf|application/octet-stream");
        arrayLists.Add(".aca|application/octet-stream");
        arrayLists.Add(".accdb|application/msaccess");
        arrayLists.Add(".accde|application/msaccess");
        arrayLists.Add(".accdt|application/msaccess");
        arrayLists.Add(".acx|application/internet-property-stream");
        arrayLists.Add(".afm|application/octet-stream");
        arrayLists.Add(".ai|application/postscript");
        arrayLists.Add(".aif|audio/x-aiff");
        arrayLists.Add(".aifc|audio/aiff");
        arrayLists.Add(".aiff|audio/aiff");
        arrayLists.Add(".application|application/x-ms-application");
        arrayLists.Add(".art|image/x-jg");
        arrayLists.Add(".asd|application/octet-stream");
        arrayLists.Add(".asf|video/x-ms-asf");
        arrayLists.Add(".asi|application/octet-stream");
        arrayLists.Add(".asm|text/plain");
        arrayLists.Add(".asr|video/x-ms-asf");
        arrayLists.Add(".asx|video/x-ms-asf");
        arrayLists.Add(".atom|application/atom+xml");
        arrayLists.Add(".au|audio/basic");
        arrayLists.Add(".avi|video/x-msvideo");
        arrayLists.Add(".axs|application/olescript");
        arrayLists.Add(".bas|text/plain");
        arrayLists.Add(".bcpio|application/x-bcpio");
        arrayLists.Add(".bin|application/octet-stream");
        arrayLists.Add(".bmp|image/bmp");
        arrayLists.Add(".c|text/plain");
        arrayLists.Add(".cab|application/octet-stream");
        arrayLists.Add(".calx|application/vnd.ms-office.calx");
        arrayLists.Add(".cat|application/vnd.ms-pki.seccat");
        arrayLists.Add(".cdf|application/x-cdf");
        arrayLists.Add(".chm|application/octet-stream");
        arrayLists.Add(".class|application/x-java-applet");
        arrayLists.Add(".clp|application/x-msclip");
        arrayLists.Add(".cmx|image/x-cmx");
        arrayLists.Add(".cnf|text/plain");
        arrayLists.Add(".cod|image/cis-cod");
        arrayLists.Add(".cpio|application/x-cpio");
        arrayLists.Add(".cpp|text/plain");
        arrayLists.Add(".crd|application/x-mscardfile");
        arrayLists.Add(".crl|application/pkix-crl");
        arrayLists.Add(".crt|application/x-x509-ca-cert");
        arrayLists.Add(".csh|application/x-csh");
        arrayLists.Add(".css|text/css");
        arrayLists.Add(".csv|application/octet-stream");
        arrayLists.Add(".cur|application/octet-stream");
        arrayLists.Add(".dcr|application/x-director");
        arrayLists.Add(".deploy|application/octet-stream");
        arrayLists.Add(".der|application/x-x509-ca-cert");
        arrayLists.Add(".dib|image/bmp");
        arrayLists.Add(".dir|application/x-director");
        arrayLists.Add(".disco|text/xml");
        arrayLists.Add(".dll|application/x-msdownload");
        arrayLists.Add(".dll.config|text/xml");
        arrayLists.Add(".dlm|text/dlm");
        arrayLists.Add(".doc|application/msword");
        arrayLists.Add(".docm|application/vnd.ms-word.document.macroEnabled.12");
        arrayLists.Add(".docx|application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        arrayLists.Add(".dot|application/msword");
        arrayLists.Add(".dotm|application/vnd.ms-word.template.macroEnabled.12");
        arrayLists.Add(".dotx|application/vnd.openxmlformats-officedocument.wordprocessingml.template");
        arrayLists.Add(".dsp|application/octet-stream");
        arrayLists.Add(".dtd|text/xml");
        arrayLists.Add(".dvi|application/x-dvi");
        arrayLists.Add(".dwf|drawing/x-dwf");
        arrayLists.Add(".dwp|application/octet-stream");
        arrayLists.Add(".dxr|application/x-director");
        arrayLists.Add(".eml|message/rfc822");
        arrayLists.Add(".emz|application/octet-stream");
        arrayLists.Add(".eot|application/octet-stream");
        arrayLists.Add(".eps|application/postscript");
        arrayLists.Add(".etx|text/x-setext");
        arrayLists.Add(".evy|application/envoy");
        arrayLists.Add(".exe|application/octet-stream");
        arrayLists.Add(".exe.config|text/xml");
        arrayLists.Add(".fdf|application/vnd.fdf");
        arrayLists.Add(".fif|application/fractals");
        arrayLists.Add(".fla|application/octet-stream");
        arrayLists.Add(".flr|x-world/x-vrml");
        arrayLists.Add(".flv|video/x-flv");
        arrayLists.Add(".gif|image/gif");
        arrayLists.Add(".gtar|application/x-gtar");
        arrayLists.Add(".gz|application/x-gzip");
        arrayLists.Add(".h|text/plain");
        arrayLists.Add(".hdf|application/x-hdf");
        arrayLists.Add(".hdml|text/x-hdml");
        arrayLists.Add(".hhc|application/x-oleobject");
        arrayLists.Add(".hhk|application/octet-stream");
        arrayLists.Add(".hhp|application/octet-stream");
        arrayLists.Add(".hlp|application/winhlp");
        arrayLists.Add(".hqx|application/mac-binhex40");
        arrayLists.Add(".hta|application/hta");
        arrayLists.Add(".htc|text/x-component");
        arrayLists.Add(".htm|text/html");
        arrayLists.Add(".html|text/html");
        arrayLists.Add(".htt|text/webviewhtml");
        arrayLists.Add(".hxt|text/html");
        arrayLists.Add(".ico|image/x-icon");
        arrayLists.Add(".ics|application/octet-stream");
        arrayLists.Add(".ief|image/ief");
        arrayLists.Add(".iii|application/x-iphone");
        arrayLists.Add(".inf|application/octet-stream");
        arrayLists.Add(".ins|application/x-internet-signup");
        arrayLists.Add(".isp|application/x-internet-signup");
        arrayLists.Add(".IVF|video/x-ivf");
        arrayLists.Add(".jar|application/java-archive");
        arrayLists.Add(".java|application/octet-stream");
        arrayLists.Add(".jck|application/liquidmotion");
        arrayLists.Add(".jcz|application/liquidmotion");
        arrayLists.Add(".jfif|image/pjpeg");
        arrayLists.Add(".jpb|application/octet-stream");
        arrayLists.Add(".jpe|image/jpeg");
        arrayLists.Add(".jpeg|image/jpeg");
        arrayLists.Add(".jpg|image/jpeg");
        arrayLists.Add(".js|application/x-javascript");
        arrayLists.Add(".jsx|text/jscript");
        arrayLists.Add(".latex|application/x-latex");
        arrayLists.Add(".lit|application/x-ms-reader");
        arrayLists.Add(".lpk|application/octet-stream");
        arrayLists.Add(".lsf|video/x-la-asf");
        arrayLists.Add(".lsx|video/x-la-asf");
        arrayLists.Add(".lzh|application/octet-stream");
        arrayLists.Add(".m13|application/x-msmediaview");
        arrayLists.Add(".m14|application/x-msmediaview");
        arrayLists.Add(".m1v|video/mpeg");
        arrayLists.Add(".m3u|audio/x-mpegurl");
        arrayLists.Add(".man|application/x-troff-man");
        arrayLists.Add(".manifest|application/x-ms-manifest");
        arrayLists.Add(".map|text/plain");
        arrayLists.Add(".mdb|application/x-msaccess");
        arrayLists.Add(".mdp|application/octet-stream");
        arrayLists.Add(".me|application/x-troff-me");
        arrayLists.Add(".mht|message/rfc822");
        arrayLists.Add(".mhtml|message/rfc822");
        arrayLists.Add(".mid|audio/mid");
        arrayLists.Add(".midi|audio/mid");
        arrayLists.Add(".mix|application/octet-stream");
        arrayLists.Add(".mmf|application/x-smaf");
        arrayLists.Add(".mno|text/xml");
        arrayLists.Add(".mny|application/x-msmoney");
        arrayLists.Add(".mov|video/quicktime");
        arrayLists.Add(".movie|video/x-sgi-movie");
        arrayLists.Add(".mp2|video/mpeg");
        arrayLists.Add(".mp3|audio/mpeg");
        arrayLists.Add(".mp4|video/mp4");
        arrayLists.Add(".mpa|video/mpeg");
        arrayLists.Add(".mpe|video/mpeg");
        arrayLists.Add(".mpeg|video/mpeg");
        arrayLists.Add(".mpg|video/mpeg");
        arrayLists.Add(".mpp|application/vnd.ms-project");
        arrayLists.Add(".mpv2|video/mpeg");
        arrayLists.Add(".ms|application/x-troff-ms");
        arrayLists.Add(".msi|application/octet-stream");
        arrayLists.Add(".mso|application/octet-stream");
        arrayLists.Add(".mvb|application/x-msmediaview");
        arrayLists.Add(".mvc|application/x-miva-compiled");
        arrayLists.Add(".nc|application/x-netcdf");
        arrayLists.Add(".nsc|video/x-ms-asf");
        arrayLists.Add(".nws|message/rfc822");
        arrayLists.Add(".ocx|application/octet-stream");
        arrayLists.Add(".oda|application/oda");
        arrayLists.Add(".odc|text/x-ms-odc");
        arrayLists.Add(".ods|application/oleobject");
        arrayLists.Add(".one|application/onenote");
        arrayLists.Add(".onea|application/onenote");
        arrayLists.Add(".onetoc|application/onenote");
        arrayLists.Add(".onetoc2|application/onenote");
        arrayLists.Add(".onetmp|application/onenote");
        arrayLists.Add(".onepkg|application/onenote");
        arrayLists.Add(".p10|application/pkcs10");
        arrayLists.Add(".p12|application/x-pkcs12");
        arrayLists.Add(".p7b|application/x-pkcs7-certificates");
        arrayLists.Add(".p7c|application/pkcs7-mime");
        arrayLists.Add(".p7m|application/pkcs7-mime");
        arrayLists.Add(".p7r|application/x-pkcs7-certreqresp");
        arrayLists.Add(".p7s|application/pkcs7-signature");
        arrayLists.Add(".pbm|image/x-portable-bitmap");
        arrayLists.Add(".pcx|application/octet-stream");
        arrayLists.Add(".pcz|application/octet-stream");
        arrayLists.Add(".pdf|application/pdf");
        arrayLists.Add(".pfb|application/octet-stream");
        arrayLists.Add(".pfm|application/octet-stream");
        arrayLists.Add(".pfx|application/x-pkcs12");
        arrayLists.Add(".pgm|image/x-portable-graymap");
        arrayLists.Add(".pko|application/vnd.ms-pki.pko");
        arrayLists.Add(".pma|application/x-perfmon");
        arrayLists.Add(".pmc|application/x-perfmon");
        arrayLists.Add(".pml|application/x-perfmon");
        arrayLists.Add(".pmr|application/x-perfmon");
        arrayLists.Add(".pmw|application/x-perfmon");
        arrayLists.Add(".png|image/png");
        arrayLists.Add(".pnm|image/x-portable-anymap");
        arrayLists.Add(".pnz|image/png");
        arrayLists.Add(".pot|application/vnd.ms-powerpoint");
        arrayLists.Add(".potm|application/vnd.ms-powerpoint.template.macroEnabled.12");
        arrayLists.Add(".potx|application/vnd.openxmlformats-officedocument.presentationml.template");
        arrayLists.Add(".ppam|application/vnd.ms-powerpoint.addin.macroEnabled.12");
        arrayLists.Add(".ppm|image/x-portable-pixmap");
        arrayLists.Add(".pps|application/vnd.ms-powerpoint");
        arrayLists.Add(".ppsm|application/vnd.ms-powerpoint.slideshow.macroEnabled.12");
        arrayLists.Add(".ppsx|application/vnd.openxmlformats-officedocument.presentationml.slideshow");
        arrayLists.Add(".ppt|application/vnd.ms-powerpoint");
        arrayLists.Add(".pptm|application/vnd.ms-powerpoint.presentation.macroEnabled.12");
        arrayLists.Add(".pptx|application/vnd.openxmlformats-officedocument.presentationml.presentation");
        arrayLists.Add(".prf|application/pics-rules");
        arrayLists.Add(".prm|application/octet-stream");
        arrayLists.Add(".prx|application/octet-stream");
        arrayLists.Add(".ps|application/postscript");
        arrayLists.Add(".psd|application/octet-stream");
        arrayLists.Add(".psm|application/octet-stream");
        arrayLists.Add(".psp|application/octet-stream");
        arrayLists.Add(".pub|application/x-mspublisher");
        arrayLists.Add(".qt|video/quicktime");
        arrayLists.Add(".qtl|application/x-quicktimeplayer");
        arrayLists.Add(".qxd|application/octet-stream");
        arrayLists.Add(".ra|audio/x-pn-realaudio");
        arrayLists.Add(".ram|audio/x-pn-realaudio");
        arrayLists.Add(".rar|application/octet-stream");
        arrayLists.Add(".ras|image/x-cmu-raster");
        arrayLists.Add(".rf|image/vnd.rn-realflash");
        arrayLists.Add(".rgb|image/x-rgb");
        arrayLists.Add(".rm|application/vnd.rn-realmedia");
        arrayLists.Add(".rmi|audio/mid");
        arrayLists.Add(".roff|application/x-troff");
        arrayLists.Add(".rpm|audio/x-pn-realaudio-plugin");
        arrayLists.Add(".rtf|application/rtf");
        arrayLists.Add(".rtx|text/richtext");
        arrayLists.Add(".scd|application/x-msschedule");
        arrayLists.Add(".sct|text/scriptlet");
        arrayLists.Add(".sea|application/octet-stream");
        arrayLists.Add(".setpay|application/set-payment-initiation");
        arrayLists.Add(".setreg|application/set-registration-initiation");
        arrayLists.Add(".sgml|text/sgml");
        arrayLists.Add(".sh|application/x-sh");
        arrayLists.Add(".shar|application/x-shar");
        arrayLists.Add(".sit|application/x-stuffit");
        arrayLists.Add(".sldm|application/vnd.ms-powerpoint.slide.macroEnabled.12");
        arrayLists.Add(".sldx|application/vnd.openxmlformats-officedocument.presentationml.slide");
        arrayLists.Add(".smd|audio/x-smd");
        arrayLists.Add(".smi|application/octet-stream");
        arrayLists.Add(".smx|audio/x-smd");
        arrayLists.Add(".smz|audio/x-smd");
        arrayLists.Add(".snd|audio/basic");
        arrayLists.Add(".snp|application/octet-stream");
        arrayLists.Add(".spc|application/x-pkcs7-certificates");
        arrayLists.Add(".spl|application/futuresplash");
        arrayLists.Add(".src|application/x-wais-source");
        arrayLists.Add(".ssm|application/streamingmedia");
        arrayLists.Add(".sst|application/vnd.ms-pki.certstore");
        arrayLists.Add(".stl|application/vnd.ms-pki.stl");
        arrayLists.Add(".sv4cpio|application/x-sv4cpio");
        arrayLists.Add(".sv4crc|application/x-sv4crc");
        arrayLists.Add(".swf|application/x-shockwave-flash");
        arrayLists.Add(".t|application/x-troff");
        arrayLists.Add(".tar|application/x-tar");
        arrayLists.Add(".tcl|application/x-tcl");
        arrayLists.Add(".tex|application/x-tex");
        arrayLists.Add(".texi|application/x-texinfo");
        arrayLists.Add(".texinfo|application/x-texinfo");
        arrayLists.Add(".tgz|application/x-compressed");
        arrayLists.Add(".thmx|application/vnd.ms-officetheme");
        arrayLists.Add(".thn|application/octet-stream");
        arrayLists.Add(".tif|image/tiff");
        arrayLists.Add(".tiff|image/tiff");
        arrayLists.Add(".toc|application/octet-stream");
        arrayLists.Add(".tr|application/x-troff");
        arrayLists.Add(".trm|application/x-msterminal");
        arrayLists.Add(".tsv|text/tab-separated-values");
        arrayLists.Add(".ttf|application/octet-stream");
        arrayLists.Add(".txt|text/plain");
        arrayLists.Add(".u32|application/octet-stream");
        arrayLists.Add(".uls|text/iuls");
        arrayLists.Add(".ustar|application/x-ustar");
        arrayLists.Add(".vbs|text/vbscript");
        arrayLists.Add(".vcf|text/x-vcard");
        arrayLists.Add(".vcs|text/plain");
        arrayLists.Add(".vdx|application/vnd.ms-visio.viewer");
        arrayLists.Add(".vml|text/xml");
        arrayLists.Add(".vsd|application/vnd.visio");
        arrayLists.Add(".vss|application/vnd.visio");
        arrayLists.Add(".vst|application/vnd.visio");
        arrayLists.Add(".vsto|application/octet-stream");
        arrayLists.Add(".vsw|application/vnd.visio");
        arrayLists.Add(".vsx|application/vnd.visio");
        arrayLists.Add(".vtx|application/vnd.visio");
        arrayLists.Add(".wav|audio/wav");
        arrayLists.Add(".wax|audio/x-ms-wax");
        arrayLists.Add(".wbmp|image/vnd.wap.wbmp");
        arrayLists.Add(".wcm|application/vnd.ms-works");
        arrayLists.Add(".wdb|application/vnd.ms-works");
        arrayLists.Add(".wks|application/vnd.ms-works");
        arrayLists.Add(".wm|video/x-ms-wm");
        arrayLists.Add(".wma|audio/x-ms-wma");
        arrayLists.Add(".wmd|application/x-ms-wmd");
        arrayLists.Add(".wmf|application/x-msmetafile");
        arrayLists.Add(".wml|text/vnd.wap.wml");
        arrayLists.Add(".wmlc|application/vnd.wap.wmlc");
        arrayLists.Add(".wmls|text/vnd.wap.wmlscript");
        arrayLists.Add(".wmlsc|application/vnd.wap.wmlscriptc");
        arrayLists.Add(".wmp|video/x-ms-wmp");
        arrayLists.Add(".wmv|video/x-ms-wmv");
        arrayLists.Add(".wmx|video/x-ms-wmx");
        arrayLists.Add(".wmz|application/x-ms-wmz");
        arrayLists.Add(".wps|application/vnd.ms-works");
        arrayLists.Add(".wri|application/x-mswrite");
        arrayLists.Add(".wrl|x-world/x-vrml");
        arrayLists.Add(".wrz|x-world/x-vrml");
        arrayLists.Add(".wsdl|text/xml");
        arrayLists.Add(".wvx|video/x-ms-wvx");
        arrayLists.Add(".x|application/directx");
        arrayLists.Add(".xaf|x-world/x-vrml");
        arrayLists.Add(".xap|application/x-silverlight-app");
        arrayLists.Add(".xaml|application/xaml+xml");
        arrayLists.Add(".xbap|application/x-ms-xbap");
        arrayLists.Add(".xbm|image/x-xbitmap");
        arrayLists.Add(".xdr|text/plain");
        arrayLists.Add(".xla|application/vnd.ms-excel");
        arrayLists.Add(".xlam|application/vnd.ms-excel.addin.macroEnabled.12");
        arrayLists.Add(".xlc|application/vnd.ms-excel");
        arrayLists.Add(".xlm|application/vnd.ms-excel");
        arrayLists.Add(".xls|application/vnd.ms-excel");
        arrayLists.Add(".xlsb|application/vnd.ms-excel.sheet.binary.macroEnabled.12");
        arrayLists.Add(".xlsm|application/vnd.ms-excel.sheet.macroEnabled.12");
        arrayLists.Add(".xlsx|application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        arrayLists.Add(".xlt|application/vnd.ms-excel");
        arrayLists.Add(".xltm|application/vnd.ms-excel.template.macroEnabled.12");
        arrayLists.Add(".xltx|application/vnd.openxmlformats-officedocument.spreadsheetml.template");
        arrayLists.Add(".xlw|application/vnd.ms-excel");
        arrayLists.Add(".xml|text/xml");
        arrayLists.Add(".xof|x-world/x-vrml");
        arrayLists.Add(".xpm|image/x-xpixmap");
        arrayLists.Add(".xps|application/vnd.ms-xpsdocument");
        arrayLists.Add(".xsd|text/xml");
        arrayLists.Add(".xsf|text/xml");
        arrayLists.Add(".xsl|text/xml");
        arrayLists.Add(".xslt|text/xml");
        arrayLists.Add(".xsn|application/octet-stream");
        arrayLists.Add(".xtp|application/octet-stream");
        arrayLists.Add(".xwd|image/x-xwindowdump");
        arrayLists.Add(".z|application/x-compress");
        arrayLists.Add(".zip|application/x-zip-compressed");
        arrayLists.Sort();
        Dictionary<string, string> strs = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
        try {
            enumerator = arrayLists.GetEnumerator();
            while (enumerator.MoveNext()) {
                object objectValue = RuntimeHelpers.GetObjectValue(enumerator.Current);
                Array arrays = Strings.Split(Conversions.ToString(objectValue), "|", -1, CompareMethod.Binary);
                Dictionary<string, string> strs1 = strs;
                Type type = typeof(Strings);
                object[] objArray = new object[1];
                object[] objArray1 = new object[1];
                int num = 0;
                objArray1[0] = num;
                objArray[0] = RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(arrays, objArray1, null));
                object[] objArray2 = objArray;
                bool[] flagArray = new bool[] { true };
                object obj = NewLateBinding.LateGet(null, type, "LCase", objArray2, null, null, flagArray);
                if (flagArray[0]) {
                    object[] objectValue1 = new object[] { num, RuntimeHelpers.GetObjectValue(objArray2[0]) };
                    NewLateBinding.LateIndexSetComplex(arrays, objectValue1, null, true, false);
                }
                string str = Conversions.ToString(obj);
                object[] objArray3 = new object[] { 1 };
                strs1.Add(str, Conversions.ToString(NewLateBinding.LateIndexGet(arrays, objArray3, null)));
            }
        }
        finally {
            if (enumerator is IDisposable) {
                (enumerator as IDisposable).Dispose();
            }
        }
        return strs;
    }

    public string GetDisplayTypeForFile(string FileExtension) {
        string str = "";
        try {
            RegistryKey registryKey = Registry.ClassesRoot.OpenSubKey(FileExtension, false);
            if (!Information.IsNothing(registryKey)) {
                string str1 = Conversions.ToString(registryKey.GetValue(""));
                registryKey.Close();
                if (!Information.IsNothing(str1)) {
                    registryKey = Registry.ClassesRoot.OpenSubKey(str1, false);
                    str = Conversions.ToString(registryKey.GetValue(""));
                    registryKey.Close();
                }
            }
        }
        catch (Exception exception) {
            ProjectData.SetProjectError(exception);
            ProjectData.ClearProjectError();
        }
        if (Operators.CompareString(str, "", false) == 0) {
            if (Operators.CompareString(FileExtension, "", false) != 0) {
                FileExtension = Strings.Right(FileExtension, checked(Strings.Len(FileExtension) - 1));
                str = string.Concat(Strings.UCase(FileExtension), " File");
            }
            else {
                str = "File";
            }
        }
        return str;
    }

    /// <summary>Gets the system's description for a folder.</summary>
    /// <returns>The description for a folder.</returns>
    public string GetDisplayTypeForFolder() {
        string str = "";
        try {
            RegistryKey registryKey = Registry.ClassesRoot.OpenSubKey("Directory", false);
            str = Conversions.ToString(registryKey.GetValue(""));
            registryKey.Close();
        }
        catch (Exception exception) {
            ProjectData.SetProjectError(exception);
            ProjectData.ClearProjectError();
        }
        if (Operators.CompareString(str, "", false) == 0) {
            str = "File Folder";
        }
        return str;
    }

    [SecurityCritical]
    public Bitmap GetIconByExtension(string FileExtension, bool SmallIcon) {
        return FileIcon.GetFileIconPrivate(string.Concat("*", FileExtension), SmallIcon);
    }

    /// <summary>Gets the system icon for an item.</summary>
    /// <param name="ItemPath">The path to the item.  This can be the path to a file, folder, or drive.</param>
    /// <param name="SmallIcon">Set to True if you would like a 16 x 16 pixel icon.  Set to False if you would like a 32 x 32 pixle icon.</param>
    /// <returns>A bitmap of the icon for the item.</returns>
    /// <remarks>See <see href="http://msdn.microsoft.com/en-us/library/bb762179(VS.85).aspx">MSDN: SHGetFileInfo Function</see></remarks>
    [SecurityCritical]
    public Bitmap GetIconByPath(string ItemPath, bool SmallIcon) {
        return FileIcon.GetFileIconPrivate(ItemPath, SmallIcon);
    }

    [DllImport("shlwapi", CharSet = CharSet.Auto, ExactSpelling = false)]
    private static extern long StrFormatByteSize(long fileSize, StringBuilder buffer, int bufferSize);

    public string StringToBase64(string PlainString) {
        return Convert.ToBase64String(Encoding.UTF8.GetBytes(PlainString));
    }
}