﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading;

namespace Jaeger.SoftAWS.S3 {
    public class Download {
        private HttpWebRequest MyRequest;

        private bool KeepGoing;

        private int InternalState;

        private long InternalBytesTransfered;

        private long InternalBytesTotal;

        private float InternalLimitKBpsSpeed;

        private int InternalErrorNumber;

        private string InternalErrorDescription;

        private IWebProxy InternalProxy;

        private int InternalBufferLength;

        private int InternalTimeoutSeconds;

        private int InternalReadWriteTimeoutSeconds;

        private string InternalResponseString;

        private int InternalResponseStatusCode;

        private string InternalResponseStatusDescription;

        private Dictionary<string, string> InternalResponseHeaders;

        private string InternalLogData;

        private string InternalRequestURL;

        private string InternalRequestMethod;

        private Dictionary<string, string> InternalRequestHeaders;

        private FileSystemCaller MyFileSystemCaller;

        private bool InternalAllowWin32;

        private bool InternalIgnoreSSLCertificateErrors;

        /// <summary>Gets/Sets if Win32 API calls should be allowed for working with long paths.</summary>
        public bool AllowWin32 {
            get {
                return this.InternalAllowWin32;
            }
            set {
                this.InternalAllowWin32 = value;
                this.MyFileSystemCaller = new FileSystemCaller(this.InternalAllowWin32);
            }
        }

        /// <summary>The size of chunks that will be downloaded.</summary>
        public int BufferLength {
            get {
                return this.InternalBufferLength;
            }
            set {
                this.InternalBufferLength = value;
            }
        }

        public long BytesTotal {
            get {
                return this.InternalBytesTotal;
            }
        }

        /// <summary>The number of bytes that have been downloaded.</summary>
        public long BytesTransfered {
            get {
                return this.InternalBytesTransfered;
            }
        }

        /// <summary>A message that describes the last exception.</summary>
        public string ErrorDescription {
            get {
                return this.InternalErrorDescription;
            }
        }

        public int ErrorNumber {
            get {
                return this.InternalErrorNumber;
            }
        }

        public bool IgnoreSSLCertificateErrors {
            get {
                return this.InternalIgnoreSSLCertificateErrors;
            }
            set {
                this.InternalIgnoreSSLCertificateErrors = value;
            }
        }

        /// <summary>Gets/Sets the maximum download speed.</summary>
        public float LimitKBpsSpeed {
            get {
                return this.InternalLimitKBpsSpeed;
            }
            set {
                this.InternalLimitKBpsSpeed = value;
            }
        }

        /// <summary>Returns log information related to last method call.</summary>
        public string LogData {
            get {
                return this.InternalLogData;
            }
        }

        public IWebProxy Proxy {
            get {
                return this.InternalProxy;
            }
            set {
                this.InternalProxy = value;
            }
        }

        /// <summary>Gets/Sets the ReadWriteTimeout value.</summary>
        public int ReadWriteTimeoutSeconds {
            get {
                return this.InternalReadWriteTimeoutSeconds;
            }
            set {
                this.InternalReadWriteTimeoutSeconds = value;
            }
        }

        public Dictionary<string, string> RequestHeaders {
            get {
                return this.InternalRequestHeaders;
            }
        }

        /// <summary>Returns the HTTP method that was used to connect to Amazon. This is the method that was passed to the function. This value is read only and is provided for logging purposes.</summary>
        public string RequestMethod {
            get {
                return this.InternalRequestMethod;
            }
        }

        public string RequestURL {
            get {
                return this.InternalRequestURL;
            }
        }

        public Dictionary<string, string> ResponseHeaders {
            get {
                return this.InternalResponseHeaders;
            }
        }

        public int ResponseStatusCode {
            get {
                return this.InternalResponseStatusCode;
            }
        }

        /// <summary>Returns the HTTP status description that was returned from Amazon.</summary>
        public string ResponseStatusDescription {
            get {
                return this.InternalResponseStatusDescription;
            }
        }

        public string ResponseString {
            get {
                return this.InternalResponseString;
            }
        }

        /// <summary>Indents and formats the ResponseString if it is XML.</summary>
        public string ResponseStringFormatted {
            get {
                return Shared_Module.FormatXML(this.InternalResponseString);
            }
        }

        public int State {
            get {
                return this.InternalState;
            }
        }

        public int TimeoutSeconds {
            get {
                return this.InternalTimeoutSeconds;
            }
            set {
                this.InternalTimeoutSeconds = value;
            }
        }

        /// <summary>Gets the version number of the SprightlySoft S3 component.</summary>
        public string Version {
            get {
                AssemblyName name = Assembly.GetExecutingAssembly().GetName();
                string[] str = new string[] { Conversions.ToString(name.Version.Major), ".", Conversions.ToString(name.Version.Minor), ".", Conversions.ToString(name.Version.Build) };
                return string.Concat(str);
            }
        }

        public Download() {
            this.InternalState = 0;
            this.InternalBytesTransfered = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalLimitKBpsSpeed = 0f;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalProxy = null;
            this.InternalBufferLength = 16384;
            this.InternalTimeoutSeconds = 100;
            this.InternalReadWriteTimeoutSeconds = 300;
            this.InternalResponseString = "";
            this.InternalResponseStatusCode = 0;
            this.InternalResponseStatusDescription = "";
            this.InternalResponseHeaders = null;
            this.InternalLogData = "";
            this.InternalRequestURL = "";
            this.InternalRequestMethod = "";
            this.InternalRequestHeaders = null;
            this.InternalAllowWin32 = true;
            this.MyFileSystemCaller = new FileSystemCaller(this.InternalAllowWin32);
            this.InternalIgnoreSSLCertificateErrors = false;
        }

        public void Abort() {
            if (this.MyRequest != null) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Aborting the request\r\n");
                this.MyRequest.Abort();
                this.KeepGoing = false;
            }
        }

        /// <summary>Builds the RequestURL you will use to contact Amazon S3.</summary>
        /// <param name="UseSSL">Specifies whether the URL should use http or https.</param>
        /// <param name="RequestEndpoint">The domain name you will make a request against.  This is usually s3.amazonaws.com, s3-us-west-1.amazonaws.com, or s3-ap-southeast-1.amazonaws.com.</param>
        /// <param name="BucketName">The bucket name you will make a request against.</param>
        /// <param name="KeyName">The key name you will make a request against.</param>
        /// <param name="QueryString">The query string to include in the URL.</param>
        /// <returns>A URL.</returns>
        public string BuildS3RequestURL(bool UseSSL, string RequestEndpoint, string BucketName, string KeyName, string QueryString) {
            return Shared_Module.BuildS3RequestURLShared(UseSSL, RequestEndpoint, BucketName, KeyName, QueryString);
        }

        /// <summary>Gets an object from Amazon S3 and saves it as a file.</summary>
        /// <param name="RequestURL">The URL you will be making the request against. This includes the query string.</param>
        /// <param name="Method">The request method.  For a download from S3 this is usually GET.</param>
        /// <param name="ExtraRequestHeaders">A list of extra headers to send to Amazon.</param>
        /// <param name="LocalFileName">The full path where the file will be saved.</param>
        /// <param name="AppendToFile">Sets if an existing local file will be overwritten or appended to.</param>
        /// <returns>Returns True if the download was successful.</returns>
        [SecurityCritical]
        public bool DownloadFile(string RequestURL, string Method, Dictionary<string, string> ExtraRequestHeaders, string LocalFileName, bool AppendToFile) {
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesTransfered = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalResponseString = "";
            this.InternalLogData = "";
            this.InternalResponseStatusCode = 0;
            this.InternalResponseStatusDescription = "";
            this.InternalResponseHeaders = new Dictionary<string, string>();
            this.InternalRequestURL = "";
            this.InternalRequestMethod = "";
            this.InternalRequestHeaders = new Dictionary<string, string>();
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   RequestURL=", RequestURL, "   Method=", Method, "   ExtraRequestHeaders=", Shared_Module.DictionaryToString(ExtraRequestHeaders), "   LocalFileName=", LocalFileName, "   AppendToFile=", Conversions.ToString(AppendToFile), "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            bool flag = this.DownloadFileFunction(RequestURL, Method, ExtraRequestHeaders, LocalFileName, AppendToFile);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", Conversions.ToString(flag), "\r\n" };
            this.InternalLogData = string.Concat(name);
            return flag;
        }

        [SecurityCritical]
        private bool DownloadFileFunction(string RequestURL, string Method, Dictionary<string, string> ExtraRequestHeaders, string LocalFileName, bool AppendToFile) {
            int num;
            Stream stream = null;
            HttpWebResponse response = null;
            Stream responseStream = null;
            long num1;
            long num2 = 0L;
            long num3 = 0L;
            Dictionary<string, string>.Enumerator enumerator = new Dictionary<string, string>.Enumerator();
            IEnumerator enumerator1 = null;
            object[] objectValue;
            IEnumerator enumerator2 = null;
            IEnumerator enumerator3 = null;
            IEnumerator enumerator4 = null;
            IEnumerator enumerator5 = null;
            IEnumerator enumerator6 = null;
            IEnumerator enumerator7 = null;
            object[] key;
            bool flag = true;
            this.InternalState = 1;
            string[] internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            Download.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            this.InternalRequestURL = RequestURL;
            this.InternalRequestMethod = Method;
            ServicePointManager.ServerCertificateValidationCallback = null;
            if (this.InternalIgnoreSSLCertificateErrors) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Setting ServerCertificateValidationCallback\r\n");
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(SharedModule.CertificateValidationCallBack);
            }
            this.MyRequest = (HttpWebRequest)WebRequest.Create(RequestURL);
            this.MyRequest.ServicePoint.ConnectionLimit = 50;
            this.MyRequest.AllowWriteStreamBuffering = true;
            this.MyRequest.AllowAutoRedirect = true;
            this.MyRequest.Timeout = checked(this.InternalTimeoutSeconds * 1000);
            this.MyRequest.ReadWriteTimeout = checked(this.InternalReadWriteTimeoutSeconds * 1000);
            this.MyRequest.Method = Method;
            this.MyRequest.KeepAlive = true;
            this.MyRequest.Credentials = CredentialCache.DefaultCredentials;
            if (ExtraRequestHeaders != null) {
                string str = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
                try {
                    enumerator = ExtraRequestHeaders.GetEnumerator();
                    while (enumerator.MoveNext()) {
                        KeyValuePair<string, string> current = enumerator.Current;
                        int num4 = Strings.Len(current.Value);
                        int num5 = 1;
                        while (num5 <= num4) {
                            if (Strings.InStr(str, Strings.Mid(current.Value, num5, 1), CompareMethod.Binary) != 0) {
                                num5 = checked(num5 + 1);
                            }
                            else {
                                this.InternalErrorNumber = 1001;
                                int internalErrorNumber = this.InternalErrorNumber;
                                internalLogData = new string[] { "Key=", current.Key, "   Value=", current.Value, "   Invalid Character=", Strings.Mid(current.Value, num5, 1) };
                                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(internalErrorNumber, string.Concat(internalLogData));
                                flag = false;
                                goto Label1;
                            }
                        }
                        if (Operators.CompareString(Strings.LCase(current.Key), "content-type", false) == 0) {
                            this.MyRequest.ContentType = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "user-agent", false) == 0) {
                            this.MyRequest.UserAgent = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "referer", false) == 0) {
                            this.MyRequest.Referer = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "accept", false) == 0) {
                            this.MyRequest.Accept = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "if-modified-since", false) == 0) {
                            this.MyRequest.IfModifiedSince = Conversions.ToDate(current.Value);
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "host", false) != 0) {
                            if (Operators.CompareString(Strings.LCase(current.Key), "range", false) != 0) {
                                try {
                                    this.MyRequest.Headers[current.Key] = current.Value;
                                }
                                catch (Exception exception1) {
                                    ProjectData.SetProjectError(exception1);
                                    Exception exception = exception1;
                                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error adding request header   RequestHeader=", current.Key, ":", current.Value, "   Message=", exception.Message, "\r\n" };
                                    this.InternalLogData = string.Concat(internalLogData);
                                    this.InternalErrorNumber = 1002;
                                    int internalErrorNumber1 = this.InternalErrorNumber;
                                    internalLogData = new string[] { "Message=", exception.Message, "   Header Name=", current.Key, "   Header Value=", current.Value };
                                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(internalErrorNumber1, string.Concat(internalLogData));
                                    flag = false;
                                    ProjectData.ClearProjectError();
                                    goto Label1;
                                }
                            }
                            else {
                                string str1 = Strings.LCase(current.Value);
                                if (!(Strings.InStr(str1, "bytes=", CompareMethod.Binary) != 1 | Strings.InStr(str1, "-", CompareMethod.Binary) == 0)) {
                                    str1 = Strings.Replace(str1, "bytes=", "", 1, -1, CompareMethod.Binary);
                                    string[] strArrays = Strings.Split(str1, ",", -1, CompareMethod.Binary);
                                    for (int i = 0; i < checked((int)strArrays.Length); i = checked(i + 1)) {
                                        string str2 = Strings.Trim(strArrays[i]);
                                        string[] strArrays1 = Strings.Split(str2, "-", -1, CompareMethod.Binary);
                                        string str3 = Strings.Trim(strArrays1[0]);
                                        string str4 = Strings.Trim(strArrays1[1]);
                                        if (Operators.CompareString(str4, "", false) == 0) {
                                            if (Versioned.IsNumeric(str3)) {
                                                num1 = Conversions.ToLong(str3);
                                                if (num1 <= (long)2147483647) {
                                                    this.MyRequest.AddRange(checked((int)num1));
                                                }
                                                else {
                                                    MethodInfo method = typeof(WebHeaderCollection).GetMethod("AddWithoutValidate", BindingFlags.Instance | BindingFlags.NonPublic);
                                                    key = new object[] { current.Key, current.Value };
                                                    method.Invoke(this.MyRequest.Headers, key);
                                                }
                                            }
                                            else {
                                                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   The range value is not valid.      Header Name=", current.Key, "   Header Value=", current.Value, "\r\n" };
                                                this.InternalLogData = string.Concat(internalLogData);
                                                this.InternalErrorNumber = 1111;
                                                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Header Name=", current.Key, "   Header Value=", current.Value));
                                                flag = false;
                                                goto Label1;
                                            }
                                        }
                                        else if (!(!Versioned.IsNumeric(str3) | !Versioned.IsNumeric(str4))) {
                                            num1 = Conversions.ToLong(str3);
                                            long num6 = Conversions.ToLong(str4);
                                            if (!(num1 > (long)2147483647 | num6 > (long)2147483647)) {
                                                this.MyRequest.AddRange(checked((int)num1), checked((int)num6));
                                            }
                                            else {
                                                MethodInfo methodInfo = typeof(WebHeaderCollection).GetMethod("AddWithoutValidate", BindingFlags.Instance | BindingFlags.NonPublic);
                                                key = new object[] { current.Key, current.Value };
                                                methodInfo.Invoke(this.MyRequest.Headers, key);
                                            }
                                        }
                                        else {
                                            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   The range value is not valid.      Header Name=", current.Key, "   Header Value=", current.Value, "\r\n" };
                                            this.InternalLogData = string.Concat(internalLogData);
                                            this.InternalErrorNumber = 1111;
                                            this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Header Name=", current.Key, "   Header Value=", current.Value));
                                            flag = false;
                                            goto Label1;
                                        }
                                    }
                                }
                                else {
                                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   The range value is not valid.      Header Name=", current.Key, "   Header Value=", current.Value, "\r\n" };
                                    this.InternalLogData = string.Concat(internalLogData);
                                    this.InternalErrorNumber = 1111;
                                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Header Name=", current.Key, "   Header Value=", current.Value));
                                    flag = false;
                                    goto Label1;
                                }
                            }
                        }
                    }
                    goto Label0;
                }
                finally {
                    ((IDisposable)enumerator).Dispose();
                }
            }
            else {
                goto Label0;
            }
            Label1:
            if (stream != null) {
                if (flag) {
                    stream.Flush();
                }
                stream.Close();
            }
            if (responseStream != null) {
                responseStream.Close();
            }
            if (response != null) {
                response.Close();
            }
            this.MyRequest = null;
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ErrorNumber=", Conversions.ToString(this.InternalErrorNumber), "   ErrorDescription=", this.InternalErrorDescription, "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            this.InternalState = 0;
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            return flag;
            Label0:
            if (Operators.CompareString(this.MyRequest.UserAgent, "", false) == 0) {
                this.MyRequest.UserAgent = string.Concat("SoftAWS/", this.Version);
            }
            if (!Information.IsNothing(this.InternalProxy)) {
                this.MyRequest.Proxy = this.InternalProxy;
                Uri proxy = this.InternalProxy.GetProxy(this.MyRequest.Address);
                if (this.MyRequest.Address != proxy) {
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Proxy server set   Proxy.Address=", proxy.AbsoluteUri, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                }
            }
            try {
                enumerator1 = this.MyRequest.Headers.GetEnumerator();
                while (enumerator1.MoveNext()) {
                    object obj = RuntimeHelpers.GetObjectValue(enumerator1.Current);
                    object obj1 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   RequestHeader="), obj), ":");
                    WebHeaderCollection headers = this.MyRequest.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj) };
                    this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj1, NewLateBinding.LateGet(headers, null, "Item", objectValue, null, null, null)), "\r\n"));
                    Dictionary<string, string> internalRequestHeaders = this.InternalRequestHeaders;
                    string str5 = Conversions.ToString(obj);
                    WebHeaderCollection webHeaderCollection = this.MyRequest.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj) };
                    internalRequestHeaders.Add(str5, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection, null, "Item", objectValue, null, null, null)));
                }
            }
            finally {
                if (enumerator1 is IDisposable) {
                    (enumerator1 as IDisposable).Dispose();
                }
            }
            try {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response\r\n");
                response = (HttpWebResponse)this.MyRequest.GetResponse();
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response was successful\r\n");
            }
            catch (WebException webException1) {
                ProjectData.SetProjectError(webException1);
                WebException webException = webException1;
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response failed   Message=", webException.Message, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                flag = false;
                this.InternalErrorNumber = 1003;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", webException.Message));
                HttpWebResponse httpWebResponse = (HttpWebResponse)webException.Response;
                if (!Information.IsNothing(httpWebResponse)) {
                    this.InternalResponseStatusCode = (int)httpWebResponse.StatusCode;
                    this.InternalResponseStatusDescription = httpWebResponse.StatusDescription;
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusCode=", Conversions.ToString((int)httpWebResponse.StatusCode), "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusDescription=", httpWebResponse.StatusDescription, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    try {
                        enumerator2 = httpWebResponse.Headers.GetEnumerator();
                        while (enumerator2.MoveNext()) {
                            object objectValue1 = RuntimeHelpers.GetObjectValue(enumerator2.Current);
                            object obj2 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   ResponseHeader="), objectValue1), ":");
                            WebHeaderCollection headers1 = httpWebResponse.Headers;
                            objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue1) };
                            this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj2, NewLateBinding.LateGet(headers1, null, "Item", objectValue, null, null, null)), "\r\n"));
                            Dictionary<string, string> internalResponseHeaders = this.InternalResponseHeaders;
                            string str6 = Conversions.ToString(objectValue1);
                            WebHeaderCollection webHeaderCollection1 = httpWebResponse.Headers;
                            objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue1) };
                            internalResponseHeaders.Add(str6, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection1, null, "Item", objectValue, null, null, null)));
                        }
                    }
                    finally {
                        if (enumerator2 is IDisposable) {
                            (enumerator2 as IDisposable).Dispose();
                        }
                    }
                    Stream responseStream1 = httpWebResponse.GetResponseStream();
                    try {
                        this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the error response stream\r\n");
                        string end = (new StreamReader(responseStream1, true)).ReadToEnd();
                        this.InternalResponseString = end;
                        this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the error response stream was successful\r\n");
                        internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   StreamData=", end, "\r\n" };
                        this.InternalLogData = string.Concat(internalLogData);
                    }
                    catch (Exception exception3) {
                        ProjectData.SetProjectError(exception3);
                        Exception exception2 = exception3;
                        internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error getting the error response stream   Message=", exception2.Message, "\r\n" };
                        this.InternalLogData = string.Concat(internalLogData);
                        ProjectData.ClearProjectError();
                    }
                    httpWebResponse.Close();
                }
                this.MyRequest.Abort();
                ProjectData.ClearProjectError();
                goto Label1;
            }
            catch (Exception exception5) {
                ProjectData.SetProjectError(exception5);
                Exception exception4 = exception5;
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response failed   Message=", exception4.Message, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                this.InternalErrorNumber = 1003;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception4.Message));
                flag = false;
                this.MyRequest.Abort();
                ProjectData.ClearProjectError();
                goto Label1;
            }
            this.InternalResponseStatusCode = (int)response.StatusCode;
            this.InternalResponseStatusDescription = response.StatusDescription;
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusCode=", Conversions.ToString((int)response.StatusCode), "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusDescription=", response.StatusDescription, "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            try {
                enumerator3 = response.Headers.GetEnumerator();
                while (enumerator3.MoveNext()) {
                    object objectValue2 = RuntimeHelpers.GetObjectValue(enumerator3.Current);
                    object obj3 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   ResponseHeader="), objectValue2), ":");
                    WebHeaderCollection headers2 = response.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue2) };
                    this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj3, NewLateBinding.LateGet(headers2, null, "Item", objectValue, null, null, null)), "\r\n"));
                    Dictionary<string, string> strs = this.InternalResponseHeaders;
                    string str7 = Conversions.ToString(objectValue2);
                    WebHeaderCollection webHeaderCollection2 = response.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue2) };
                    strs.Add(str7, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection2, null, "Item", objectValue, null, null, null)));
                }
            }
            finally {
                if (enumerator3 is IDisposable) {
                    (enumerator3 as IDisposable).Dispose();
                }
            }
            this.InternalBytesTotal = response.ContentLength;
            responseStream = response.GetResponseStream();
            if (!AppendToFile) {
                try {
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the local file   FileMode=Create   LocalFileName=", LocalFileName, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    stream = this.MyFileSystemCaller.FileOpen(LocalFileName, FileMode.Create);
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the local file was successful\r\n");
                }
                catch (Exception exception7) {
                    ProjectData.SetProjectError(exception7);
                    Exception exception6 = exception7;
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the local file failed   Message=", exception6.Message, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    this.InternalErrorNumber = 1112;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception6.Message));
                    flag = false;
                    this.MyRequest.Abort();
                    ProjectData.ClearProjectError();
                    goto Label1;
                }
            }
            else {
                try {
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the local file   FileMode=Append   LocalFileName=", LocalFileName, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    stream = this.MyFileSystemCaller.FileOpen(LocalFileName, FileMode.Append);
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the local file was successful\r\n");
                }
                catch (Exception exception9) {
                    ProjectData.SetProjectError(exception9);
                    Exception exception8 = exception9;
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the local file failed   Message=", exception8.Message, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    this.InternalErrorNumber = 1112;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception8.Message));
                    flag = false;
                    this.MyRequest.Abort();
                    ProjectData.ClearProjectError();
                    goto Label1;
                }
            }
            byte[] numArray = new byte[checked(this.InternalBufferLength + 1)];
            ArrayList arrayLists = new ArrayList();
            long contentLength = response.ContentLength;
            long num7 = (long)0;
            DateTime now = DateTime.Now;
            ArrayList arrayLists1 = new ArrayList();
            ArrayList arrayLists2 = new ArrayList();
            DateTime dateTime = DateTime.Now;
            arrayLists1.Add(dateTime.Ticks);
            arrayLists2.Add(this.InternalBytesTransfered);
            Label2:
            while (contentLength > (long)0) {
                try {
                    num = responseStream.Read(numArray, 0, checked((int)numArray.Length));
                }
                catch (Exception exception11) {
                    ProjectData.SetProjectError(exception11);
                    Exception exception10 = exception11;
                    try {
                        enumerator4 = arrayLists.GetEnumerator();
                        while (enumerator4.MoveNext()) {
                            object objectValue3 = RuntimeHelpers.GetObjectValue(enumerator4.Current);
                            this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(this.InternalLogData, objectValue3), "\r\n"));
                        }
                    }
                    finally {
                        if (enumerator4 is IDisposable) {
                            (enumerator4 as IDisposable).Dispose();
                        }
                    }
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Reading the source stream failed   Message=", exception10.Message, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    this.InternalErrorNumber = 1113;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception10.Message));
                    flag = false;
                    this.MyRequest.Abort();
                    ProjectData.ClearProjectError();
                    goto Label1;
                }
                if (num <= 0) {
                    try {
                        enumerator6 = arrayLists.GetEnumerator();
                        while (enumerator6.MoveNext()) {
                            object objectValue4 = RuntimeHelpers.GetObjectValue(enumerator6.Current);
                            this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(this.InternalLogData, objectValue4), "\r\n"));
                        }
                    }
                    finally {
                        if (enumerator6 is IDisposable) {
                            (enumerator6 as IDisposable).Dispose();
                        }
                    }
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Nothing to write.  Unexpected end of file.\r\n");
                    this.InternalErrorNumber = 1005;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, "");
                    flag = false;
                    this.MyRequest.Abort();
                    goto Label1;
                }
                else {
                    try {
                        stream.Write(numArray, 0, num);
                    }
                    catch (Exception exception13) {
                        ProjectData.SetProjectError(exception13);
                        Exception exception12 = exception13;
                        try {
                            enumerator5 = arrayLists.GetEnumerator();
                            while (enumerator5.MoveNext()) {
                                object obj4 = RuntimeHelpers.GetObjectValue(enumerator5.Current);
                                this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(this.InternalLogData, obj4), "\r\n"));
                            }
                        }
                        finally {
                            if (enumerator5 is IDisposable) {
                                (enumerator5 as IDisposable).Dispose();
                            }
                        }
                        internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Writing to stream failed   Message=", exception12.Message, "\r\n" };
                        this.InternalLogData = string.Concat(internalLogData);
                        this.InternalErrorNumber = 1114;
                        this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception12.Message));
                        flag = false;
                        this.MyRequest.Abort();
                        ProjectData.ClearProjectError();
                        goto Label1;
                    }
                    num7 = checked(num7 + (long)num);
                    this.InternalBytesTransfered = num7;
                    contentLength = checked(contentLength - (long)num);
                    dateTime = DateTime.Now;
                    arrayLists1.Add(dateTime.Ticks);
                    arrayLists2.Add(this.InternalBytesTransfered);
                    Download.ProgressChangedEventEventHandler progressChangedEventEventHandler = this.ProgressChangedEvent;
                    if (progressChangedEventEventHandler != null) {
                        progressChangedEventEventHandler();
                    }
                    arrayLists.Add(string.Concat(Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising ProgressChangedEvent   BytesTransfered=", Conversions.ToString(this.InternalBytesTransfered)));
                    if (arrayLists.Count == 31) {
                        arrayLists[10] = "... The log data was truncated.  Only the first 10 and last 20 events were logged.  Monitor the ProgressChangedEvent for all values.";
                    }
                    else if (arrayLists.Count > 31) {
                        arrayLists.RemoveAt(11);
                    }
                    if (this.InternalLimitKBpsSpeed <= 0f) {
                        continue;
                    }
                    int count = checked(arrayLists1.Count - 1);
                    int num8 = 0;
                    while (num8 <= count) {
                        num3 = Conversions.ToLong(arrayLists1[num8]);
                        num2 = Conversions.ToLong(arrayLists2[num8]);
                        dateTime = DateTime.Now;
                        TimeSpan timeSpan = new TimeSpan(Conversions.ToLong(Operators.SubtractObject(dateTime.Ticks, arrayLists1[num8])));
                        if (timeSpan.TotalSeconds >= 60) {
                            num8 = checked(num8 + 1);
                        }
                        else {
                            int num9 = checked(num8 - 1);
                            for (int j = 0; j <= num9; j = checked(j + 1)) {
                                arrayLists1.RemoveAt(0);
                                arrayLists2.RemoveAt(0);
                            }
                            break;
                        }
                    }
                    long ticks = DateTime.Now.Ticks;
                    long num10 = Conversions.ToLong(arrayLists2[checked(arrayLists2.Count - 1)]);
                    TimeSpan timeSpan1 = new TimeSpan(checked(ticks - num3));
                    long num11 = checked(num10 - num2);
                    if ((float)((double)num11 / 1024 / timeSpan1.TotalSeconds) <= this.InternalLimitKBpsSpeed) {
                        continue;
                    }
                    long num12 = checked((long)Math.Round((double)(checked(num11 + (long)this.InternalBufferLength)) / 1024 / (double)this.InternalLimitKBpsSpeed * 10000000 + (double)num3));
                    dateTime = DateTime.Now;
                    float single = (float)((double)(checked(dateTime.Ticks - num3)) / 10000000);
                    dateTime = DateTime.Now;
                    float ticks1 = (float)((double)(checked(num12 - dateTime.Ticks)) / 10000000);
                    while (DateTime.Now.Ticks < num12) {
                        if (this.KeepGoing) {
                            Thread.Sleep(50);
                        }
                        else {
                            goto Label2;
                        }
                    }
                }
            }
            try {
                enumerator7 = arrayLists.GetEnumerator();
                while (enumerator7.MoveNext()) {
                    object objectValue5 = RuntimeHelpers.GetObjectValue(enumerator7.Current);
                    this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(this.InternalLogData, objectValue5), "\r\n"));
                }
                goto Label1;
            }
            finally {
                if (enumerator7 is IDisposable) {
                    (enumerator7 as IDisposable).Dispose();
                }
            }
        }

        [SecurityCritical]
        public bool DownloadFileToStream(string RequestURL, string Method, Dictionary<string, string> ExtraRequestHeaders, ref Stream ResponseStream) {
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesTransfered = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalResponseString = "";
            this.InternalLogData = "";
            this.InternalResponseStatusCode = 0;
            this.InternalResponseStatusDescription = "";
            this.InternalResponseHeaders = new Dictionary<string, string>();
            this.InternalRequestURL = "";
            this.InternalRequestMethod = "";
            this.InternalRequestHeaders = new Dictionary<string, string>();
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   RequestURL=", RequestURL, "   Method=", Method, "   ExtraRequestHeaders=", Shared_Module.DictionaryToString(ExtraRequestHeaders), "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            bool streamFunction = this.DownloadFileToStreamFunction(RequestURL, Method, ExtraRequestHeaders, ref ResponseStream);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", Conversions.ToString(streamFunction), "\r\n" };
            this.InternalLogData = string.Concat(name);
            return streamFunction;
        }

        [SecurityCritical]
        private bool DownloadFileToStreamFunction(string RequestURL, string Method, Dictionary<string, string> ExtraRequestHeaders, ref Stream ResponseStream) {
            HttpWebResponse response;
            long num;
            Dictionary<string, string>.Enumerator enumerator = new Dictionary<string, string>.Enumerator();
            IEnumerator enumerator1 = null;
            object[] objectValue;
            IEnumerator enumerator2 = null;
            IEnumerator enumerator3 = null;
            object[] key;
            bool flag = true;
            this.InternalState = 1;
            string[] internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            Download.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            this.InternalRequestURL = RequestURL;
            this.InternalRequestMethod = Method;
            ServicePointManager.ServerCertificateValidationCallback = null;
            if (this.InternalIgnoreSSLCertificateErrors) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Setting ServerCertificateValidationCallback\r\n");
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(SharedModule.CertificateValidationCallBack);
            }
            this.MyRequest = (HttpWebRequest)WebRequest.Create(RequestURL);
            this.MyRequest.ServicePoint.ConnectionLimit = 50;
            this.MyRequest.AllowWriteStreamBuffering = true;
            this.MyRequest.AllowAutoRedirect = true;
            this.MyRequest.Timeout = checked(this.InternalTimeoutSeconds * 1000);
            this.MyRequest.ReadWriteTimeout = checked(this.InternalReadWriteTimeoutSeconds * 1000);
            this.MyRequest.Method = Method;
            this.MyRequest.KeepAlive = true;
            this.MyRequest.Credentials = CredentialCache.DefaultCredentials;
            if (ExtraRequestHeaders != null) {
                string str = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
                try {
                    enumerator = ExtraRequestHeaders.GetEnumerator();
                    while (enumerator.MoveNext()) {
                        KeyValuePair<string, string> current = enumerator.Current;
                        int num1 = Strings.Len(current.Value);
                        int num2 = 1;
                        while (num2 <= num1) {
                            if (Strings.InStr(str, Strings.Mid(current.Value, num2, 1), CompareMethod.Binary) != 0) {
                                num2 = checked(num2 + 1);
                            }
                            else {
                                this.InternalErrorNumber = 1001;
                                int internalErrorNumber = this.InternalErrorNumber;
                                internalLogData = new string[] { "Key=", current.Key, "   Value=", current.Value, "   Invalid Character=", Strings.Mid(current.Value, num2, 1) };
                                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(internalErrorNumber, string.Concat(internalLogData));
                                flag = false;
                                goto Label1;
                            }
                        }
                        if (Operators.CompareString(Strings.LCase(current.Key), "content-type", false) == 0) {
                            this.MyRequest.ContentType = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "user-agent", false) == 0) {
                            this.MyRequest.UserAgent = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "referer", false) == 0) {
                            this.MyRequest.Referer = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "accept", false) == 0) {
                            this.MyRequest.Accept = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "if-modified-since", false) == 0) {
                            this.MyRequest.IfModifiedSince = Conversions.ToDate(current.Value);
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "host", false) != 0) {
                            if (Operators.CompareString(Strings.LCase(current.Key), "range", false) != 0) {
                                try {
                                    this.MyRequest.Headers[current.Key] = current.Value;
                                }
                                catch (Exception exception1) {
                                    ProjectData.SetProjectError(exception1);
                                    Exception exception = exception1;
                                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error adding request header   RequestHeader=", current.Key, ":", current.Value, "   Message=", exception.Message, "\r\n" };
                                    this.InternalLogData = string.Concat(internalLogData);
                                    this.InternalErrorNumber = 1002;
                                    int internalErrorNumber1 = this.InternalErrorNumber;
                                    internalLogData = new string[] { "Message=", exception.Message, "   Header Name=", current.Key, "   Header Value=", current.Value };
                                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(internalErrorNumber1, string.Concat(internalLogData));
                                    flag = false;
                                    ProjectData.ClearProjectError();
                                    goto Label1;
                                }
                            }
                            else {
                                string str1 = Strings.LCase(current.Value);
                                if (!(Strings.InStr(str1, "bytes=", CompareMethod.Binary) != 1 | Strings.InStr(str1, "-", CompareMethod.Binary) == 0)) {
                                    str1 = Strings.Replace(str1, "bytes=", "", 1, -1, CompareMethod.Binary);
                                    string[] strArrays = Strings.Split(str1, ",", -1, CompareMethod.Binary);
                                    for (int i = 0; i < checked((int)strArrays.Length); i = checked(i + 1)) {
                                        string str2 = Strings.Trim(strArrays[i]);
                                        string[] strArrays1 = Strings.Split(str2, "-", -1, CompareMethod.Binary);
                                        string str3 = Strings.Trim(strArrays1[0]);
                                        string str4 = Strings.Trim(strArrays1[1]);
                                        if (Operators.CompareString(str4, "", false) == 0) {
                                            if (Versioned.IsNumeric(str3)) {
                                                num = Conversions.ToLong(str3);
                                                if (num <= (long)2147483647) {
                                                    this.MyRequest.AddRange(checked((int)num));
                                                }
                                                else {
                                                    MethodInfo method = typeof(WebHeaderCollection).GetMethod("AddWithoutValidate", BindingFlags.Instance | BindingFlags.NonPublic);
                                                    key = new object[] { current.Key, current.Value };
                                                    method.Invoke(this.MyRequest.Headers, key);
                                                }
                                            }
                                            else {
                                                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   The range value is not valid.      Header Name=", current.Key, "   Header Value=", current.Value, "\r\n" };
                                                this.InternalLogData = string.Concat(internalLogData);
                                                this.InternalErrorNumber = 1111;
                                                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Header Name=", current.Key, "   Header Value=", current.Value));
                                                flag = false;
                                                goto Label1;
                                            }
                                        }
                                        else if (!(!Versioned.IsNumeric(str3) | !Versioned.IsNumeric(str4))) {
                                            num = Conversions.ToLong(str3);
                                            long num3 = Conversions.ToLong(str4);
                                            if (!(num > (long)2147483647 | num3 > (long)2147483647)) {
                                                this.MyRequest.AddRange(checked((int)num), checked((int)num3));
                                            }
                                            else {
                                                MethodInfo methodInfo = typeof(WebHeaderCollection).GetMethod("AddWithoutValidate", BindingFlags.Instance | BindingFlags.NonPublic);
                                                key = new object[] { current.Key, current.Value };
                                                methodInfo.Invoke(this.MyRequest.Headers, key);
                                            }
                                        }
                                        else {
                                            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   The range value is not valid.      Header Name=", current.Key, "   Header Value=", current.Value, "\r\n" };
                                            this.InternalLogData = string.Concat(internalLogData);
                                            this.InternalErrorNumber = 1111;
                                            this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Header Name=", current.Key, "   Header Value=", current.Value));
                                            flag = false;
                                            goto Label1;
                                        }
                                    }
                                }
                                else {
                                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   The range value is not valid.      Header Name=", current.Key, "   Header Value=", current.Value, "\r\n" };
                                    this.InternalLogData = string.Concat(internalLogData);
                                    this.InternalErrorNumber = 1111;
                                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Header Name=", current.Key, "   Header Value=", current.Value));
                                    flag = false;
                                    goto Label1;
                                }
                            }
                        }
                    }
                    goto Label0;
                }
                finally {
                    ((IDisposable)enumerator).Dispose();
                }
            }
            else {
                goto Label0;
            }
            Label1:
            this.MyRequest = null;
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ErrorNumber=", Conversions.ToString(this.InternalErrorNumber), "   ErrorDescription=", this.InternalErrorDescription, "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            this.InternalState = 0;
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            return flag;
            Label0:
            if (Operators.CompareString(this.MyRequest.UserAgent, "", false) == 0) {
                this.MyRequest.UserAgent = string.Concat("SoftAWS/", this.Version);
            }
            if (!Information.IsNothing(this.InternalProxy)) {
                this.MyRequest.Proxy = this.InternalProxy;
                Uri proxy = this.InternalProxy.GetProxy(this.MyRequest.Address);
                if (this.MyRequest.Address != proxy) {
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Proxy server set   Proxy.Address=", proxy.AbsoluteUri, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                }
            }
            try {
                enumerator1 = this.MyRequest.Headers.GetEnumerator();
                while (enumerator1.MoveNext()) {
                    object obj = RuntimeHelpers.GetObjectValue(enumerator1.Current);
                    object obj1 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   RequestHeader="), obj), ":");
                    WebHeaderCollection headers = this.MyRequest.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj) };
                    this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj1, NewLateBinding.LateGet(headers, null, "Item", objectValue, null, null, null)), "\r\n"));
                    Dictionary<string, string> internalRequestHeaders = this.InternalRequestHeaders;
                    string str5 = Conversions.ToString(obj);
                    WebHeaderCollection webHeaderCollection = this.MyRequest.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj) };
                    internalRequestHeaders.Add(str5, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection, null, "Item", objectValue, null, null, null)));
                }
            }
            finally {
                if (enumerator1 is IDisposable) {
                    (enumerator1 as IDisposable).Dispose();
                }
            }
            try {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response\r\n");
                response = (HttpWebResponse)this.MyRequest.GetResponse();
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response was successful\r\n");
            }
            catch (WebException webException1) {
                ProjectData.SetProjectError(webException1);
                WebException webException = webException1;
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response failed   Message=", webException.Message, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                flag = false;
                this.InternalErrorNumber = 1003;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", webException.Message));
                HttpWebResponse httpWebResponse = (HttpWebResponse)webException.Response;
                if (!Information.IsNothing(httpWebResponse)) {
                    this.InternalResponseStatusCode = (int)httpWebResponse.StatusCode;
                    this.InternalResponseStatusDescription = httpWebResponse.StatusDescription;
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusCode=", Conversions.ToString((int)httpWebResponse.StatusCode), "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusDescription=", httpWebResponse.StatusDescription, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    try {
                        enumerator2 = httpWebResponse.Headers.GetEnumerator();
                        while (enumerator2.MoveNext()) {
                            object objectValue1 = RuntimeHelpers.GetObjectValue(enumerator2.Current);
                            object obj2 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   ResponseHeader="), objectValue1), ":");
                            WebHeaderCollection headers1 = httpWebResponse.Headers;
                            objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue1) };
                            this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj2, NewLateBinding.LateGet(headers1, null, "Item", objectValue, null, null, null)), "\r\n"));
                            Dictionary<string, string> internalResponseHeaders = this.InternalResponseHeaders;
                            string str6 = Conversions.ToString(objectValue1);
                            WebHeaderCollection webHeaderCollection1 = httpWebResponse.Headers;
                            objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue1) };
                            internalResponseHeaders.Add(str6, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection1, null, "Item", objectValue, null, null, null)));
                        }
                    }
                    finally {
                        if (enumerator2 is IDisposable) {
                            (enumerator2 as IDisposable).Dispose();
                        }
                    }
                    Stream responseStream = httpWebResponse.GetResponseStream();
                    try {
                        this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the error response stream\r\n");
                        string end = (new StreamReader(responseStream, true)).ReadToEnd();
                        this.InternalResponseString = end;
                        this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the error response stream was successful\r\n");
                        internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   StreamData=", end, "\r\n" };
                        this.InternalLogData = string.Concat(internalLogData);
                    }
                    catch (Exception exception3) {
                        ProjectData.SetProjectError(exception3);
                        Exception exception2 = exception3;
                        internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error getting the error response stream   Message=", exception2.Message, "\r\n" };
                        this.InternalLogData = string.Concat(internalLogData);
                        ProjectData.ClearProjectError();
                    }
                    httpWebResponse.Close();
                }
                this.MyRequest.Abort();
                ProjectData.ClearProjectError();
                goto Label1;
            }
            catch (Exception exception5) {
                ProjectData.SetProjectError(exception5);
                Exception exception4 = exception5;
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response failed   Message=", exception4.Message, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                this.InternalErrorNumber = 1003;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception4.Message));
                flag = false;
                this.MyRequest.Abort();
                ProjectData.ClearProjectError();
                goto Label1;
            }
            this.InternalResponseStatusCode = (int)response.StatusCode;
            this.InternalResponseStatusDescription = response.StatusDescription;
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusCode=", Conversions.ToString((int)response.StatusCode), "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusDescription=", response.StatusDescription, "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            try {
                enumerator3 = response.Headers.GetEnumerator();
                while (enumerator3.MoveNext()) {
                    object objectValue2 = RuntimeHelpers.GetObjectValue(enumerator3.Current);
                    object obj3 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   ResponseHeader="), objectValue2), ":");
                    WebHeaderCollection headers2 = response.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue2) };
                    this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj3, NewLateBinding.LateGet(headers2, null, "Item", objectValue, null, null, null)), "\r\n"));
                    Dictionary<string, string> strs = this.InternalResponseHeaders;
                    string str7 = Conversions.ToString(objectValue2);
                    WebHeaderCollection webHeaderCollection2 = response.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue2) };
                    strs.Add(str7, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection2, null, "Item", objectValue, null, null, null)));
                }
            }
            finally {
                if (enumerator3 is IDisposable) {
                    (enumerator3 as IDisposable).Dispose();
                }
            }
            this.InternalBytesTotal = response.ContentLength;
            ResponseStream = response.GetResponseStream();
            goto Label1;
        }

        public string GetAWSSignatureVersion4Value(Dictionary<string, string> RequestHeaders, string ServiceURL, string ServiceName, string Region, string RequestMethod, string PostDataHash, string AWSAccessKeyId, string AWSSecretAccessKey) {
            string str = "";
            string str1 = "";
            return Shared_Module.GetAWSSignatureVersion4ValueShared(RequestHeaders, ServiceURL, ServiceName, Region, RequestMethod, PostDataHash, AWSAccessKeyId, AWSSecretAccessKey, ref str, ref str1);
        }

        /// <summary>Gets the Signature Version 4 value which will be used in the Authorization header.</summary>
        /// <param name="RequestHeaders">Headers you will be sending in the request. One header must be x-amz-date.</param>
        /// <param name="ServiceURL">The URL you will be making the request against.</param>
        /// <param name="ServiceName">The service are using. For example glacier.</param>
        /// <param name="Region">The region you are using. For example us-east-1.</param>
        /// <param name="RequestMethod">The request method. This is usually GET, PUT, POST, or DELETE.</param>
        /// <param name="PostDataHash">The SHA-256 hash value of the data you will be sending.</param>
        /// <param name="AWSAccessKeyId">Your Amazon access key ID.</param>
        /// <param name="CanonicalString">This parameter will contain the canonical string that was generated. It is useful if you are interested to see how the signature was calculated.</param>
        /// <param name="StringToSign">This parameter will contain the string that was signed. It is useful if you are interested to see how the signature was calculated.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <returns>The Signature Version 4 value you will pass in the Authorization header.</returns>
        public string GetAWSSignatureVersion4Value(Dictionary<string, string> RequestHeaders, string ServiceURL, string ServiceName, string Region, string RequestMethod, string PostDataHash, string AWSAccessKeyId, string AWSSecretAccessKey, ref string CanonicalString, ref string StringToSign) {
            return Shared_Module.GetAWSSignatureVersion4ValueShared(RequestHeaders, ServiceURL, ServiceName, Region, RequestMethod, PostDataHash, AWSAccessKeyId, AWSSecretAccessKey, ref CanonicalString, ref StringToSign);
        }

        /// <summary>Gets the Authorization header value for a S3 request.</summary>
        /// <param name="RequestURL">The URL you will be making the request against. This includes the query string.</param>
        /// <param name="RequestMethod">The request method.  This is usually GET, PUT, POST, COPY or DELETE.</param>
        /// <param name="ExtraRequestHeaders">Extra headers you will be sending in the request.  One header must be x-amz-date.</param>
        /// <param name="AWSAccessKeyId">Your Amazon access key ID.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <returns>The value you will pass in the Authorization header.</returns>
        public string GetS3AuthorizationValue(string RequestURL, string RequestMethod, Dictionary<string, string> ExtraRequestHeaders, string AWSAccessKeyId, string AWSSecretAccessKey) {
            string str = "";
            return Shared_Module.GetS3AuthorizationValueShared(RequestURL, RequestMethod, ExtraRequestHeaders, AWSAccessKeyId, AWSSecretAccessKey, ref str);
        }

        public string GetS3AuthorizationValue(string RequestURL, string RequestMethod, Dictionary<string, string> ExtraRequestHeaders, string AWSAccessKeyId, string AWSSecretAccessKey, ref string StringThatWasSigned) {
            return Shared_Module.GetS3AuthorizationValueShared(RequestURL, RequestMethod, ExtraRequestHeaders, AWSAccessKeyId, AWSSecretAccessKey, ref StringThatWasSigned);
        }

        /// <summary>Gets the Authorization header value for a S3 request by specifying the components of the request.</summary>
        /// <param name="BucketName">The bucket name you will make a request against.</param>
        /// <param name="KeyName">The key name you will make a request against.</param>
        /// <param name="QueryString">The query string you will include in the URL.</param>
        /// <param name="RequestMethod">The request method.  This is usually GET, PUT, POST, COPY or DELETE.</param>
        /// <param name="ExtraRequestHeaders">Extra headers you will be sending in the request.  One header must be x-amz-date.</param>
        /// <param name="AWSAccessKeyId">Your Amazon access key ID.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <returns>The value you will pass in the Authorization header.</returns>
        public string GetS3AuthorizationValueByParts(string BucketName, string KeyName, string QueryString, string RequestMethod, Dictionary<string, string> ExtraRequestHeaders, string AWSAccessKeyId, string AWSSecretAccessKey) {
            string str = "";
            return Shared_Module.GetS3AuthorizationValueByPartsShared(BucketName, KeyName, QueryString, RequestMethod, ExtraRequestHeaders, AWSAccessKeyId, AWSSecretAccessKey, ref str);
        }

        public string GetS3AuthorizationValueByParts(string BucketName, string KeyName, string QueryString, string RequestMethod, Dictionary<string, string> ExtraRequestHeaders, string AWSAccessKeyId, string AWSSecretAccessKey, ref string StringThatWasSigned) {
            return Shared_Module.GetS3AuthorizationValueByPartsShared(BucketName, KeyName, QueryString, RequestMethod, ExtraRequestHeaders, AWSAccessKeyId, AWSSecretAccessKey, ref StringThatWasSigned);
        }

        public event Download.ProgressChangedEventEventHandler ProgressChangedEvent;

        /// <summary>Fires when the value of the State property is changed.</summary>
        public event Download.StateChangedEventEventHandler StateChangedEvent;

        public delegate void ProgressChangedEventEventHandler();

        public delegate void StateChangedEventEventHandler();
    }
}
