﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.Net;
using System.Net.Security;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Jaeger.SoftAWS.S3 {
    internal static class SharedModule {
        public static void AddAuthorizationRequestHeader(ref HttpWebRequest OrigRequest, string BucketName, string AmazonAccessKeyID, string AmazonSecretAccessKey) {
            IEnumerator enumerator = null;
            IEnumerator enumerator1 = null;
            string str = "";
            str = string.Concat(str, OrigRequest.Method, "\n");
            str = string.Concat(str, OrigRequest.Headers[HttpRequestHeader.ContentMd5], "\n");
            str = string.Concat(str, OrigRequest.ContentType, "\n");
            str = string.Concat(str, "\n");
            NameValueCollection nameValueCollection = new NameValueCollection();
            try {
                enumerator = OrigRequest.Headers.GetEnumerator();
                while (enumerator.MoveNext()) {
                    string str1 = Conversions.ToString(enumerator.Current);
                    if (Strings.InStr(Strings.LCase(str1), "x-amz-", CompareMethod.Binary) != 1) {
                        continue;
                    }
                    string[] values = OrigRequest.Headers.GetValues(str1);
                    for (int i = 0; i < checked((int)values.Length); i = checked(i + 1)) {
                        nameValueCollection.Add(str1, values[i]);
                    }
                }
            }
            finally {
                if (enumerator is IDisposable) {
                    (enumerator as IDisposable).Dispose();
                }
            }
            String_Comparer stringComparer = new String_Comparer(CompareInfo.GetCompareInfo(""), CompareOptions.StringSort);
            SortedList sortedLists = new SortedList(stringComparer);
            try {
                enumerator1 = nameValueCollection.Keys.GetEnumerator();
                while (enumerator1.MoveNext()) {
                    string str2 = Conversions.ToString(enumerator1.Current);
                    sortedLists.Add(str2, nameValueCollection[str2]);
                }
            }
            finally {
                if (enumerator1 is IDisposable) {
                    (enumerator1 as IDisposable).Dispose();
                }
            }
            IDictionaryEnumerator dictionaryEnumerator = sortedLists.GetEnumerator();
            while (dictionaryEnumerator.MoveNext()) {
                object objectValue = RuntimeHelpers.GetObjectValue(dictionaryEnumerator.Current);
                string str3 = str;
                Type type = typeof(Strings);
                object[] objArray = new object[1];
                object obj = objectValue;
                objArray[0] = RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(obj, null, "Key", new object[0], null, null, null));
                object[] objArray1 = objArray;
                bool[] flagArray = new bool[] { true };
                object obj1 = NewLateBinding.LateGet(null, type, "LCase", objArray1, null, null, flagArray);
                if (flagArray[0]) {
                    object[] objectValue1 = new object[] { RuntimeHelpers.GetObjectValue(objArray1[0]) };
                    NewLateBinding.LateSetComplex(obj, null, "Key", objectValue1, null, null, true, false);
                }
                str = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(str3, obj1), ":"), NewLateBinding.LateGet(objectValue, null, "Value", new object[0], null, null, null)), '\n'));
            }
            if (Shared_Module.GetIsVirtualHostedBucket(BucketName) && Operators.CompareString(BucketName, "", false) != 0) {
                str = string.Concat(str, "/", BucketName);
            }
            str = string.Concat(str, OrigRequest.RequestUri.AbsolutePath);
            string query = OrigRequest.RequestUri.Query;
            if (Operators.CompareString(query, "", false) != 0) {
                string[] strArrays = Strings.Split(query, "&", -1, CompareMethod.Binary);
                string str4 = "";
                int num = Information.LBound(strArrays, 1);
                int num1 = Information.UBound(strArrays, 1);
                for (int j = num; j <= num1; j = checked(j + 1)) {
                    if (Operators.CompareString(strArrays[j], "", false) != 0 && Strings.InStr(strArrays[j], "=", CompareMethod.Binary) == 0 | Strings.InStr(strArrays[j], "versionId", CompareMethod.Binary) > 0) {
                        str4 = string.Concat(str4, strArrays[j], "&");
                    }
                }
                if (Operators.CompareString(str4, "", false) != 0) {
                    str4 = Strings.Left(str4, checked(Strings.Len(str4) - 1));
                }
                str = string.Concat(str, str4);
            }
            HMACSHA1 hMACSHA1 = new HMACSHA1(Encoding.UTF8.GetBytes(AmazonSecretAccessKey));
            string base64String = Convert.ToBase64String(hMACSHA1.ComputeHash(Encoding.UTF8.GetBytes(str)));
            OrigRequest.Headers["Authorization"] = string.Concat("AWS ", AmazonAccessKeyID, ":", base64String);
        }

        public static string BuildAmazomURL(string BucketName, string AmazonFileName, bool UseSSL, string DomainName, string QueryString) {
            BucketName = Strings.Trim(BucketName);
            string str = "";
            str = (!UseSSL ? string.Concat(str, "http://") : string.Concat(str, "https://"));
            bool isVirtualHostedBucket = Shared_Module.GetIsVirtualHostedBucket(BucketName);
            if (isVirtualHostedBucket && Operators.CompareString(BucketName, "", false) != 0) {
                str = string.Concat(str, BucketName, ".");
            }
            str = string.Concat(str, DomainName, "/");
            if (!isVirtualHostedBucket) {
                str = string.Concat(str, BucketName, "/");
            }
            str = string.Concat(str, Uri.EscapeDataString(AmazonFileName));
            if (Operators.CompareString(QueryString, "", false) != 0) {
                str = string.Concat(str, QueryString);
            }
            return str;
        }

        public static bool CertificateValidationCallBack(object sender, X509Certificate cert, X509Chain chain, System.Net.Security.SslPolicyErrors SslPolicyErrors) {
            return true;
        }
    }
}
