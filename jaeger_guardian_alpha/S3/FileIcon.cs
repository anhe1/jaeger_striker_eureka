﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Security;

public partial class Helper {
    internal static class FileIcon {
        private const int SHGFI_ICON = 256;

        private const int SHGFI_SMALLICON = 1;

        private const int SHGFI_LARGEICON = 0;

        private const int SHGFI_USEFILEATTRIBUTES = 16;

        static FileIcon() {
        }

        [SecurityCritical]
        public static Bitmap GetFileIconPrivate(string FileName, bool SmallIcon) {
            Bitmap bitmap = null;
            int num;
            FileIcon.SHFILEINFO sHFILEINFO = new FileIcon.SHFILEINFO() {
                szDisplayName = new string('\0', 260),
                szTypeName = new string('\0', 80)
            };
            num = (Strings.InStr(FileName, "*", CompareMethod.Binary) <= 0 ? 256 : 272);
            if (!SmallIcon) {
                num |= 0;
            }
            else {
                num |= 1;
            }
            try {
                FileIcon.SHGetFileInfo(ref FileName, 0, ref sHFILEINFO, Marshal.SizeOf(sHFILEINFO), num);
                bitmap = Icon.FromHandle(sHFILEINFO.hIcon).ToBitmap();
            }
            catch (Exception exception) {
                ProjectData.SetProjectError(exception);
                ProjectData.ClearProjectError();
            }
            return bitmap;
        }

        [DllImport("shell32.dll", CharSet = CharSet.Auto, ExactSpelling = false, SetLastError = true)]
        private static extern IntPtr SHGetFileInfo(ref string pszPath, int dwFileAttributes, ref FileIcon.SHFILEINFO psfi, int cbFileInfo, int uFlags);

        private struct SHFILEINFO {
            public IntPtr hIcon;

            public int iIcon;

            public int dwAttributes;

            public string szDisplayName;

            public string szTypeName;
        }
    }
}