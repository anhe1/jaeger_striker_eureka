﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Xml;

namespace Jaeger.SoftAWS.S3 {
    public class ListBucket {
        [AccessedThroughProperty("MyREST")]
        private REST _MyREST;

        private bool KeepGoing;

        private int InternalState;

        private int InternalErrorNumber;

        private string InternalErrorDescription;

        private IWebProxy InternalProxy;

        private string InternalLogData;

        private int InternalResponseStatusCode;

        private string InternalResponseStatusDescription;

        private Dictionary<string, string> InternalResponseHeaders;

        private string InternalResponseString;

        private string InternalRequestURL;

        private string InternalRequestMethod;

        private Dictionary<string, string> InternalRequestHeaders;

        private ArrayList InternalBucketItemsArrayList;

        /// <summary>An ArrayList of BucketItemObjects that represent the items in the bucket.</summary>
        public ArrayList BucketItemsArrayList {
            get {
                return this.InternalBucketItemsArrayList;
            }
        }

        public string ErrorDescription {
            get {
                return this.InternalErrorDescription;
            }
        }

        /// <summary>A number that describes the last exception.</summary>
        public int ErrorNumber {
            get {
                return this.InternalErrorNumber;
            }
        }

        public string LogData {
            get {
                return this.InternalLogData;
            }
        }

        public virtual REST MyREST {
            get {
                return this._MyREST;
            }
            [MethodImpl(MethodImplOptions.Synchronized)]
            set {
                this._MyREST = value;
            }
        }

        /// <summary>Gets/Sets a System.Net.IWebProxy object that defines proxy settings.</summary>
        public IWebProxy Proxy {
            get {
                return this.InternalProxy;
            }
            set {
                this.InternalProxy = value;
            }
        }

        public Dictionary<string, string> RequestHeaders {
            get {
                return this.InternalRequestHeaders;
            }
        }

        /// <summary>Returns the HTTP method that was used to connect to Amazon. This is the method that was passed to the function. This value is read only and is provided for logging purposes.</summary>
        public string RequestMethod {
            get {
                return this.InternalRequestMethod;
            }
        }

        public string RequestURL {
            get {
                return this.InternalRequestURL;
            }
        }

        /// <summary>Returns a list of HTTP headers returned from Amazon.</summary>
        public Dictionary<string, string> ResponseHeaders {
            get {
                return this.InternalResponseHeaders;
            }
        }

        /// <summary>Returns the HTTP status code that was returned from Amazon.</summary>
        public int ResponseStatusCode {
            get {
                return this.InternalResponseStatusCode;
            }
        }

        public string ResponseStatusDescription {
            get {
                return this.InternalResponseStatusDescription;
            }
        }

        public string ResponseString {
            get {
                return this.InternalResponseString;
            }
        }

        /// <summary>Indents and formats the ResponseString if it is XML.</summary>
        public string ResponseStringFormatted {
            get {
                return Shared_Module.FormatXML(this.InternalResponseString);
            }
        }

        public int State {
            get {
                return this.InternalState;
            }
        }

        /// <summary>Gets the version number of the SprightlySoft S3 component.</summary>
        public string Version {
            get {
                AssemblyName name = Assembly.GetExecutingAssembly().GetName();
                string[] str = new string[] { Conversions.ToString(name.Version.Major), ".", Conversions.ToString(name.Version.Minor), ".", Conversions.ToString(name.Version.Build) };
                return string.Concat(str);
            }
        }

        public ListBucket() {
            this.InternalState = 0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalProxy = null;
            this.InternalLogData = "";
            this.InternalResponseStatusCode = 0;
            this.InternalResponseStatusDescription = "";
            this.InternalResponseHeaders = null;
            this.InternalResponseString = "";
            this.InternalRequestURL = "";
            this.InternalRequestMethod = "";
            this.InternalRequestHeaders = null;
            this.InternalBucketItemsArrayList = new ArrayList();
        }

        public void Abort() {
            if (this.MyREST != null) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Aborting ListBucket\r\n");
                if (this.MyREST.State == 1) {
                    this.MyREST.Abort();
                }
                this.KeepGoing = false;
            }
        }

        public bool ListBucket1(bool UseSSL, string RequestEndpoint, string BucketName, string Delimiter, string Prefix, string AWSAccessKeyId, string AWSSecretAccessKey) {
            string str;
            string innerText;
            ListBucket.BucketItemObject bucketItemObject;
            string str1;
            string str2;
            bool flag;
            string[] name;
            IEnumerator enumerator = null;
            IEnumerator enumerator1 = null;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.MyREST = new REST();
            if (!Information.IsNothing(this.InternalProxy)) {
                this.MyREST.Proxy = this.InternalProxy;
            }
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalResponseString = "";
            this.InternalLogData = "";
            this.InternalResponseHeaders = new Dictionary<string, string>();
            this.InternalBucketItemsArrayList = new ArrayList();
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   UseSSL=", Conversions.ToString(UseSSL), "   RequestEndpoint=", RequestEndpoint, "   BucketName=", BucketName, "   Delimiter=", Delimiter, "   Prefix=", Prefix, "   AWSAccessKeyId=", AWSAccessKeyId, "   AWSSecretAccessKey=", AWSSecretAccessKey, "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            ListBucket.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            string innerText1 = "";
            int num = 1000;
            Helper helper = new Helper();
            bool flag1 = false;
            name = new string[] { "?delimiter=", Uri.EscapeDataString(Delimiter), "&prefix=", Uri.EscapeDataString(Prefix), "&marker=", Uri.EscapeDataString(innerText1), "&max-keys=", Conversions.ToString(num) };
            string str3 = string.Concat(name);
            string str4 = "GET";
            while (true) {
                if (this.KeepGoing) {
                    string str5 = this.MyREST.BuildS3RequestURL(UseSSL, RequestEndpoint, BucketName, "", str3);
                    Dictionary<string, string> strs = new Dictionary<string, string>()
                    {
                        { "x-amz-date", DateTime.UtcNow.ToString("r") }
                    };
                    string s3AuthorizationValue = this.MyREST.GetS3AuthorizationValue(str5, str4, strs, AWSAccessKeyId, AWSSecretAccessKey);
                    strs.Add("Authorization", s3AuthorizationValue);
                    name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Calling REST.MakeRequest   RequestURL=", str5, "   RequestMethod=", str4, "   ExtraRequestHeaders=", Shared_Module.DictionaryToString(strs), "\r\n" };
                    this.InternalLogData = string.Concat(name);
                    flag = this.MyREST.MakeRequest(str5, str4, strs, "");
                    this.InternalResponseStatusCode = this.MyREST.ResponseStatusCode;
                    this.InternalResponseStatusDescription = this.MyREST.ResponseStatusDescription;
                    this.InternalResponseHeaders = this.MyREST.ResponseHeaders;
                    this.InternalResponseString = this.MyREST.ResponseString;
                    this.InternalRequestURL = this.MyREST.RequestURL;
                    this.InternalRequestMethod = this.MyREST.RequestMethod;
                    this.InternalRequestHeaders = this.MyREST.RequestHeaders;
                    this.InternalErrorNumber = this.MyREST.ErrorNumber;
                    this.InternalErrorDescription = this.MyREST.ErrorDescription;
                    name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   GetOperation complete   ReturnValue=", Conversions.ToString(flag), "   LogData=\r\n\r\n", this.MyREST.LogData, "\r\n" };
                    this.InternalLogData = string.Concat(name);
                    if (flag) {
                        this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Processing ResponseString\r\n");
                        XmlDocument xmlDocument = new XmlDocument();
                        xmlDocument.LoadXml(this.MyREST.ResponseString);
                        XmlNamespaceManager xmlNamespaceManagers = new XmlNamespaceManager(xmlDocument.NameTable);
                        xmlNamespaceManagers.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");
                        XmlNode xmlNodes = xmlDocument.SelectSingleNode("/amz:ListBucketResult", xmlNamespaceManagers);
                        if (!Information.IsNothing(xmlNodes)) {
                            xmlNodes = xmlDocument.SelectSingleNode("/amz:ListBucketResult/amz:NextMarker", xmlNamespaceManagers);
                            if (!Information.IsNothing(xmlNodes)) {
                                innerText1 = xmlNodes.InnerText;
                            }
                            xmlNodes = xmlDocument.SelectSingleNode("/amz:ListBucketResult/amz:IsTruncated", xmlNamespaceManagers);
                            flag1 = Conversions.ToBoolean(xmlNodes.InnerText);
                            XmlNodeList xmlNodeLists = xmlDocument.SelectNodes("/amz:ListBucketResult/amz:Contents", xmlNamespaceManagers);
                            try {
                                enumerator = xmlNodeLists.GetEnumerator();
                                while (enumerator.MoveNext()) {
                                    XmlNode current = (XmlNode)enumerator.Current;
                                    xmlNodes = current.SelectSingleNode("./amz:Key", xmlNamespaceManagers);
                                    innerText = xmlNodes.InnerText;
                                    xmlNodes = current.SelectSingleNode("./amz:LastModified", xmlNamespaceManagers);
                                    DateTime date = Conversions.ToDate(xmlNodes.InnerText);
                                    xmlNodes = current.SelectSingleNode("./amz:ETag", xmlNamespaceManagers);
                                    string innerText2 = xmlNodes.InnerText;
                                    xmlNodes = current.SelectSingleNode("./amz:Size", xmlNamespaceManagers);
                                    long num1 = Conversions.ToLong(xmlNodes.InnerText);
                                    xmlNodes = current.SelectSingleNode("./amz:Owner/amz:ID", xmlNamespaceManagers);
                                    str1 = (!Information.IsNothing(xmlNodes) ? xmlNodes.InnerText : "");
                                    xmlNodes = current.SelectSingleNode("./amz:Owner/amz:DisplayName", xmlNamespaceManagers);
                                    str2 = (!Information.IsNothing(xmlNodes) ? xmlNodes.InnerText : "");
                                    xmlNodes = current.SelectSingleNode("./amz:StorageClass", xmlNamespaceManagers);
                                    string innerText3 = xmlNodes.InnerText;
                                    if (Operators.CompareString(innerText, Prefix, false) != 0) {
                                        str = innerText;
                                        if (Operators.CompareString(Prefix, "", false) != 0) {
                                            str = Strings.Right(str, checked(Strings.Len(str) - Strings.Len(Prefix)));
                                        }
                                        bucketItemObject = new ListBucket.BucketItemObject() {
                                            IsFolder = false,
                                            BucketName = BucketName,
                                            KeyName = innerText,
                                            DisplayName = str,
                                            Size = num1,
                                            LastModified = date,
                                            ETag = innerText2,
                                            OwnerID = str1,
                                            OwnerName = str2,
                                            StorageClass = innerText3
                                        };
                                        this.InternalBucketItemsArrayList.Add(bucketItemObject);
                                    }
                                    if (Operators.CompareString(Delimiter, "", false) != 0) {
                                        continue;
                                    }
                                    innerText1 = innerText;
                                }
                            }
                            finally {
                                if (enumerator is IDisposable) {
                                    (enumerator as IDisposable).Dispose();
                                }
                            }
                            XmlNodeList xmlNodeLists1 = xmlDocument.SelectNodes("/amz:ListBucketResult/amz:CommonPrefixes", xmlNamespaceManagers);
                            try {
                                enumerator1 = xmlNodeLists1.GetEnumerator();
                                while (enumerator1.MoveNext()) {
                                    xmlNodes = ((XmlNode)enumerator1.Current).SelectSingleNode("./amz:Prefix", xmlNamespaceManagers);
                                    innerText = xmlNodes.InnerText;
                                    str = innerText;
                                    if (Operators.CompareString(Prefix, "", false) != 0) {
                                        str = Strings.Right(str, checked(Strings.Len(str) - Strings.Len(Prefix)));
                                    }
                                    str = Strings.Left(str, checked(Strings.Len(str) - Strings.Len(Delimiter)));
                                    bucketItemObject = new ListBucket.BucketItemObject() {
                                        IsFolder = true,
                                        BucketName = BucketName,
                                        KeyName = innerText,
                                        DisplayName = str,
                                        Size = (long)-1,
                                        LastModified = DateTime.MinValue,
                                        ETag = "",
                                        OwnerID = "",
                                        OwnerName = "",
                                        StorageClass = ""
                                    };
                                    this.InternalBucketItemsArrayList.Add(bucketItemObject);
                                }
                            }
                            finally {
                                if (enumerator1 is IDisposable) {
                                    (enumerator1 as IDisposable).Dispose();
                                }
                            }
                            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising ProgressChangedEvent   BucketItemsArrayList.Count=", Conversions.ToString(this.InternalBucketItemsArrayList.Count), "\r\n" };
                            this.InternalLogData = string.Concat(name);
                            ListBucket.ProgressChangedEventEventHandler progressChangedEventEventHandler = this.ProgressChangedEvent;
                            if (progressChangedEventEventHandler != null) {
                                progressChangedEventEventHandler();
                            }
                            if (!flag1) {
                                break;
                            }
                            this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Another page exists   IsTruncated=True\r\n");
                            name = new string[] { "?delimiter=", Uri.EscapeDataString(Delimiter), "&prefix=", Uri.EscapeDataString(Prefix), "&marker=", Uri.EscapeDataString(innerText1), "&max-keys=", Conversions.ToString(num) };
                            str3 = string.Concat(name);
                        }
                        else {
                            this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   The ListBucketResult node does not exist in the response XML\r\n");
                            flag = false;
                            this.InternalErrorNumber = 1171;
                            this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, "");
                            break;
                        }
                    }
                    else if (this.KeepGoing) {
                        this.InternalResponseString = this.MyREST.ResponseString;
                        break;
                    }
                    else {
                        this.InternalErrorNumber = 1172;
                        this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, "");
                        this.InternalResponseString = "";
                        break;
                    }
                }
                else {
                    flag = false;
                    this.InternalErrorNumber = 1172;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, "");
                    break;
                }
            }
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ErrorNumber=", Conversions.ToString(this.InternalErrorNumber), "   ErrorDescription=", this.InternalErrorDescription, "\r\n" };
            this.InternalLogData = string.Concat(name);
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", Conversions.ToString(flag), "\r\n" };
            this.InternalLogData = string.Concat(name);
            return flag;
        }

        public event ListBucket.ProgressChangedEventEventHandler ProgressChangedEvent;

        /// <summary>Fires when the value of the State property is changed.</summary>
        public event ListBucket.StateChangedEventEventHandler StateChangedEvent;

        /// <summary>A class that represents an item in a bucket.</summary>
        public class BucketItemObject {
            private bool InternalIsFolder;

            private string InternalBucketName;

            private string InternalKeyName;

            private string InternalDisplayName;

            private long InternalSize;

            private DateTime InternalLastModified;

            private string InternalETag;

            private string InternalOwnerID;

            private string InternalOwnerName;

            private string InternalStorageClass;

            public string BucketName {
                get {
                    return this.InternalBucketName;
                }
                set {
                    this.InternalBucketName = value;
                }
            }

            public string DisplayName {
                get {
                    return this.InternalDisplayName;
                }
                set {
                    this.InternalDisplayName = value;
                }
            }

            /// <summary>The ETag of the item.  Folders do not have an ETag and their value is set to empty.</summary>
            public string ETag {
                get {
                    return this.InternalETag;
                }
                set {
                    this.InternalETag = value;
                }
            }

            /// <summary>Set to true if the key represents a folder.  IsFolder will always be false if the delimiter in ListBucket is empty.  IsFolder is determined by the ListBucketResult/CommonPrefixes node in the XML response from a Get Bucket call.</summary>
            public bool IsFolder {
                get {
                    return this.InternalIsFolder;
                }
                set {
                    this.InternalIsFolder = value;
                }
            }

            /// <summary>The key name of the item.</summary>
            public string KeyName {
                get {
                    return this.InternalKeyName;
                }
                set {
                    this.InternalKeyName = value;
                }
            }

            public DateTime LastModified {
                get {
                    return this.InternalLastModified;
                }
                set {
                    this.InternalLastModified = value;
                }
            }

            public string OwnerID {
                get {
                    return this.InternalOwnerID;
                }
                set {
                    this.InternalOwnerID = value;
                }
            }

            /// <summary>The OwnerName of the item.</summary>
            public string OwnerName {
                get {
                    return this.InternalOwnerName;
                }
                set {
                    this.InternalOwnerName = value;
                }
            }

            /// <summary>The size of the item in bytes.  Folders do not have a size and their value is set to -1.</summary>
            public long Size {
                get {
                    return this.InternalSize;
                }
                set {
                    this.InternalSize = value;
                }
            }

            public string StorageClass {
                get {
                    return this.InternalStorageClass;
                }
                set {
                    this.InternalStorageClass = value;
                }
            }

            public BucketItemObject() {
            }
        }

        public delegate void ProgressChangedEventEventHandler();

        public delegate void StateChangedEventEventHandler();
    }
}
