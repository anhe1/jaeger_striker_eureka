﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace Jaeger.SoftAWS.S3 {
    /// <summary>Provides methods to calculate the hash of a file.</summary>
    public class CalculateHash {
        private bool KeepGoing;

        private int InternalState;

        private long InternalBytesProcessed;

        private long InternalBytesTotal;

        private int InternalErrorNumber;

        private string InternalErrorDescription;

        private string InternalLogData;

        private FileSystemCaller MyFileSystemCaller;

        private bool InternalAllowWin32;

        private const long MB = 1048576L;

        /// <summary>Gets/Sets if Win32 API calls should be allowed for working with long paths.</summary>
        public bool AllowWin32 {
            get {
                return this.InternalAllowWin32;
            }
            set {
                this.InternalAllowWin32 = value;
                this.MyFileSystemCaller = new FileSystemCaller(this.InternalAllowWin32);
            }
        }

        public long BytesProcessed {
            get {
                return this.InternalBytesProcessed;
            }
        }

        /// <summary>Returns the number of bytes that should be processed.</summary>
        public long BytesTotal {
            get {
                return this.InternalBytesTotal;
            }
        }

        /// <summary>A message that describes the last exception.</summary>
        public string ErrorDescription {
            get {
                return this.InternalErrorDescription;
            }
        }

        public int ErrorNumber {
            get {
                return this.InternalErrorNumber;
            }
        }

        public string LogData {
            get {
                return this.InternalLogData;
            }
        }

        /// <summary>Gets the current state of the component.</summary>
        public int State {
            get {
                return this.InternalState;
            }
        }

        public string Version {
            get {
                AssemblyName name = Assembly.GetExecutingAssembly().GetName();
                string[] str = new string[] { Conversions.ToString(name.Version.Major), ".", Conversions.ToString(name.Version.Minor), ".", Conversions.ToString(name.Version.Build) };
                return string.Concat(str);
            }
        }

        public CalculateHash() {
            this.InternalState = 0;
            this.InternalBytesProcessed = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalLogData = "";
            this.InternalAllowWin32 = true;
            this.MyFileSystemCaller = new FileSystemCaller(this.InternalAllowWin32);
        }

        public void Abort() {
            this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Aborting the operation\r\n");
            this.KeepGoing = false;
        }

        [SecurityCritical]
        public string CalculateETagFromFile(string LocalFileName) {
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesProcessed = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalLogData = "";
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   LocalFileName=", LocalFileName, "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            string str = "";
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            CalculateHash.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            HashAlgorithm hashAlgorithm = MD5.Create();
            byte[] numArray = this.CalculateHashFromFileFunction(LocalFileName, (long)-1, (long)-1, hashAlgorithm);
            StringBuilder stringBuilder = new StringBuilder();
            byte[] numArray1 = numArray;
            for (int i = 0; i < checked((int)numArray1.Length); i = checked(i + 1)) {
                byte num = numArray1[i];
                stringBuilder.Append(num.ToString("x2"));
            }
            str = stringBuilder.ToString();
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", str, "\r\n" };
            this.InternalLogData = string.Concat(name);
            return str;
        }

        /// <summary>Calculates the ETag hash of a file.</summary>
        /// <param name="LocalFileName">The path to the local file.</param>
        /// <param name="ByteRangeStart">The byte index where you want to start hashing.</param>
        /// <param name="ByteRangeEnd">The byte index where you want to finish hashing.</param>
        /// <returns>The ETag hash value of the file.</returns>
        [SecurityCritical]
        public string CalculateETagFromFile(string LocalFileName, long ByteRangeStart, long ByteRangeEnd) {
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesProcessed = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalLogData = "";
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   LocalFileName=", LocalFileName, "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            string str = "";
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            CalculateHash.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            if (ByteRangeStart < (long)0) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error, ByteRangeStart is less than 0.\r\n");
                this.InternalErrorNumber = 1009;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("ByteRangeStart=", Conversions.ToString(ByteRangeStart)));
                str = "";
            }
            else if (ByteRangeStart < ByteRangeEnd) {
                HashAlgorithm hashAlgorithm = MD5.Create();
                byte[] numArray = this.CalculateHashFromFileFunction(LocalFileName, ByteRangeStart, ByteRangeEnd, hashAlgorithm);
                StringBuilder stringBuilder = new StringBuilder();
                byte[] numArray1 = numArray;
                for (int i = 0; i < checked((int)numArray1.Length); i = checked(i + 1)) {
                    byte num = numArray1[i];
                    stringBuilder.Append(num.ToString("x2"));
                }
                str = stringBuilder.ToString();
            }
            else {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error, ByteRangeEnd is less than ByteRangeStart.\r\n");
                this.InternalErrorNumber = 1009;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("ByteRangeStart=", Conversions.ToString(ByteRangeStart), "   ByteRangeEnd=", Conversions.ToString(ByteRangeEnd)));
                str = "";
            }
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", str, "\r\n" };
            this.InternalLogData = string.Concat(name);
            return str;
        }

        public string CalculateETagFromString(string SourceString) {
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesProcessed = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalLogData = "";
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   SourceString=", SourceString, "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            string str = "";
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            CalculateHash.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            this.InternalBytesTotal = (long)Strings.Len(SourceString);
            MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
            byte[] numArray = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(SourceString));
            StringBuilder stringBuilder = new StringBuilder();
            int length = checked(checked((int)numArray.Length) - 1);
            for (int i = 0; i <= length; i = checked(i + 1)) {
                stringBuilder.Append(numArray[i].ToString("x2"));
            }
            str = stringBuilder.ToString();
            this.InternalBytesProcessed = (long)Strings.Len(SourceString);
            this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising ProgressChangedEvent   BytesProcessed=", Conversions.ToString(this.InternalBytesProcessed));
            CalculateHash.ProgressChangedEventEventHandler progressChangedEventEventHandler = this.ProgressChangedEvent;
            if (progressChangedEventEventHandler != null) {
                progressChangedEventEventHandler();
            }
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", str, "\r\n" };
            this.InternalLogData = string.Concat(name);
            return str;
        }

        [SecurityCritical]
        private byte[] CalculateHashFromFileFunction(string LocalFileName, long ByteRangeStart, long ByteRangeEnd, HashAlgorithm MyHashAlgorithm) {
            int num = 0;
            Stream stream;
            string[] internalLogData;
            IEnumerator enumerator = null;
            IEnumerator enumerator1 = null;
            byte[] hash = new byte[0];
            ArrayList arrayLists = new ArrayList();
            try {
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the file   LocalFileName=", LocalFileName, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                stream = this.MyFileSystemCaller.FileOpenRead(LocalFileName);
            }
            catch (Exception exception1) {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error opening the file   Message=", exception.Message, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                this.InternalErrorNumber = 1007;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception.Message));
                ProjectData.ClearProjectError();
                return hash;
            }
            if (!(ByteRangeStart == (long)-1 & ByteRangeEnd == (long)-1)) {
                if (ByteRangeEnd > stream.Length) {
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error, ByteRangeEnd is greater than the length of the file.\r\n");
                    this.InternalErrorNumber = 1009;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("ByteRangeEnd=", Conversions.ToString(ByteRangeEnd), "   FileLength=", Conversions.ToString(stream.Length)));
                    return hash;
                }
                this.InternalBytesTotal = checked(checked(ByteRangeEnd - ByteRangeStart) + (long)1);
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   BytesTotal=", Conversions.ToString(this.InternalBytesTotal), "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Seeking to ", Conversions.ToString(ByteRangeStart), "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                stream.Seek(ByteRangeStart, SeekOrigin.Begin);
            }
            else {
                this.InternalBytesTotal = stream.Length;
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   BytesTotal=", Conversions.ToString(this.InternalBytesTotal), "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
            }
            long internalBytesTotal = this.InternalBytesTotal;
            byte[] numArray = new byte[262144];
            if (this.InternalBytesTotal == (long)0) {
                MyHashAlgorithm.TransformFinalBlock(numArray, 0, num);
            }
            while (internalBytesTotal > (long)0) {
                if (this.KeepGoing) {
                    numArray = new byte[262144];
                    num = (internalBytesTotal < (long)(checked((int)numArray.Length)) ? stream.Read(numArray, 0, checked((int)internalBytesTotal)) : stream.Read(numArray, 0, checked((int)numArray.Length)));
                    internalBytesTotal = checked(internalBytesTotal - (long)num);
                    if (internalBytesTotal != (long)0) {
                        MyHashAlgorithm.TransformBlock(numArray, 0, num, numArray, 0);
                    }
                    else {
                        MyHashAlgorithm.TransformFinalBlock(numArray, 0, num);
                    }
                    this.InternalBytesProcessed = checked(this.InternalBytesProcessed + (long)num);
                    CalculateHash.ProgressChangedEventEventHandler progressChangedEventEventHandler = this.ProgressChangedEvent;
                    if (progressChangedEventEventHandler != null) {
                        progressChangedEventEventHandler();
                    }
                    arrayLists.Add(string.Concat(Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising ProgressChangedEvent   BytesProcessed=", Conversions.ToString(this.InternalBytesProcessed)));
                    if (arrayLists.Count != 31) {
                        if (arrayLists.Count <= 31) {
                            continue;
                        }
                        arrayLists.RemoveAt(11);
                    }
                    else {
                        arrayLists[10] = "... The log data was truncated.  Only the first 10 and last 20 events were logged.  Monitor the ProgressChangedEvent for all values.";
                    }
                }
                else {
                    try {
                        enumerator = arrayLists.GetEnumerator();
                        while (enumerator.MoveNext()) {
                            object objectValue = RuntimeHelpers.GetObjectValue(enumerator.Current);
                            this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(this.InternalLogData, objectValue), "\r\n"));
                        }
                    }
                    finally {
                        if (enumerator is IDisposable) {
                            (enumerator as IDisposable).Dispose();
                        }
                    }
                    stream.Close();
                    hash = new byte[0];
                    this.InternalErrorNumber = 1008;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, "");
                    return hash;
                }
            }
            try {
                enumerator1 = arrayLists.GetEnumerator();
                while (enumerator1.MoveNext()) {
                    object obj = RuntimeHelpers.GetObjectValue(enumerator1.Current);
                    this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(this.InternalLogData, obj), "\r\n"));
                }
            }
            finally {
                if (enumerator1 is IDisposable) {
                    (enumerator1 as IDisposable).Dispose();
                }
            }
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Closing the file   LocalFileName=", LocalFileName, "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            stream.Close();
            hash = MyHashAlgorithm.Hash;
            return hash;
        }

        [SecurityCritical]
        public string CalculateMD5FromFile(string LocalFileName) {
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesProcessed = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalLogData = "";
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   LocalFileName=", LocalFileName, "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            string base64String = "";
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            CalculateHash.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            HashAlgorithm hashAlgorithm = MD5.Create();
            byte[] numArray = this.CalculateHashFromFileFunction(LocalFileName, (long)-1, (long)-1, hashAlgorithm);
            base64String = Convert.ToBase64String(numArray);
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", base64String, "\r\n" };
            this.InternalLogData = string.Concat(name);
            return base64String;
        }

        /// <summary>Calculates the MD5 hash of a part of a file.</summary>
        /// <param name="LocalFileName">The path to the local file.</param>
        /// <param name="ByteRangeStart">The byte index where you want to start hashing.</param>
        /// <param name="ByteRangeEnd">The byte index where you want to finish hashing.</param>
        /// <returns>The MD5 hash value of the file.</returns>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.security.cryptography.md5cryptoserviceprovider.aspx">http://msdn.microsoft.com/en-us/library/system.security.cryptography.md5cryptoserviceprovider.aspx</see>
        /// </remarks>
        [SecurityCritical]
        public string CalculateMD5FromFile(string LocalFileName, long ByteRangeStart, long ByteRangeEnd) {
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesProcessed = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalLogData = "";
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   LocalFileName=", LocalFileName, "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            string base64String = "";
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            CalculateHash.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            if (ByteRangeStart < (long)0) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error, ByteRangeStart is less than 0.\r\n");
                this.InternalErrorNumber = 1009;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("ByteRangeStart=", Conversions.ToString(ByteRangeStart)));
                base64String = "";
            }
            else if (ByteRangeStart <= ByteRangeEnd) {
                HashAlgorithm hashAlgorithm = MD5.Create();
                byte[] numArray = this.CalculateHashFromFileFunction(LocalFileName, ByteRangeStart, ByteRangeEnd, hashAlgorithm);
                base64String = Convert.ToBase64String(numArray);
            }
            else {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error, ByteRangeEnd is less than ByteRangeStart.\r\n");
                this.InternalErrorNumber = 1009;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("ByteRangeStart=", Conversions.ToString(ByteRangeStart), "   ByteRangeEnd=", Conversions.ToString(ByteRangeEnd)));
                base64String = "";
            }
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", base64String, "\r\n" };
            this.InternalLogData = string.Concat(name);
            return base64String;
        }

        /// <summary>Calculates the MD5 hash of a string.</summary>
        /// <param name="SourceString">The string to be hashed.</param>
        /// <returns>The MD5 hash value of the string.</returns>
        public string CalculateMD5FromString(string SourceString) {
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesProcessed = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalLogData = "";
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   SourceString=", SourceString, "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            string base64String = "";
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            CalculateHash.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            this.InternalBytesTotal = (long)Strings.Len(SourceString);
            MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
            byte[] numArray = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(SourceString));
            base64String = Convert.ToBase64String(numArray);
            this.InternalBytesProcessed = (long)Strings.Len(SourceString);
            this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising ProgressChangedEvent   BytesProcessed=", Conversions.ToString(this.InternalBytesProcessed));
            CalculateHash.ProgressChangedEventEventHandler progressChangedEventEventHandler = this.ProgressChangedEvent;
            if (progressChangedEventEventHandler != null) {
                progressChangedEventEventHandler();
            }
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", base64String, "\r\n" };
            this.InternalLogData = string.Concat(name);
            return base64String;
        }

        [SecurityCritical]
        public string CalculateMultipartETagFromFile(string LocalFileName, long MultipartPartSizeBytes) {
            byte[] numArray;
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesProcessed = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalLogData = "";
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   LocalFileName=", LocalFileName, "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            string str = "";
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            CalculateHash.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            byte[] numArray1 = new byte[0];
            long num = this.MyFileSystemCaller.FileInfoLength(LocalFileName);
            long multipartPartSizeBytes = (long)0;
            long multipartPartSizeBytes1 = (long)0;
            int num1 = 0;
            MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
            while (multipartPartSizeBytes1 < checked(num - (long)1)) {
                num1 = checked(num1 + 1);
                multipartPartSizeBytes = checked(checked(MultipartPartSizeBytes * (long)num1) - MultipartPartSizeBytes);
                multipartPartSizeBytes1 = checked(checked(MultipartPartSizeBytes * (long)num1) - (long)1);
                if (multipartPartSizeBytes1 >= num) {
                    multipartPartSizeBytes1 = checked(num - (long)1);
                }
                HashAlgorithm hashAlgorithm = MD5.Create();
                numArray = this.CalculateHashFromFileFunction(LocalFileName, multipartPartSizeBytes, multipartPartSizeBytes1, hashAlgorithm);
                numArray1 = this.CombineByteArray(numArray1, numArray);
            }
            numArray = mD5CryptoServiceProvider.ComputeHash(numArray1);
            StringBuilder stringBuilder = new StringBuilder();
            byte[] numArray2 = numArray;
            for (int i = 0; i < checked((int)numArray2.Length); i = checked(i + 1)) {
                byte num2 = numArray2[i];
                stringBuilder.Append(num2.ToString("x2"));
            }
            str = stringBuilder.ToString();
            str = string.Concat(str, "-", num1.ToString());
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", str, "\r\n" };
            this.InternalLogData = string.Concat(name);
            return str;
        }

        /// <summary>Calculates the SHA-256 hash of a file.</summary>
        /// <param name="LocalFileName">The path to the local file.</param>
        /// <returns>The SHA-256 hash value of the file.</returns>
        [SecurityCritical]
        public string CalculateSHA256FromFile(string LocalFileName) {
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesProcessed = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalLogData = "";
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   LocalFileName=", LocalFileName, "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            string str = "";
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            CalculateHash.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            HashAlgorithm hashAlgorithm = SHA256.Create();
            byte[] numArray = this.CalculateHashFromFileFunction(LocalFileName, (long)-1, (long)-1, hashAlgorithm);
            StringBuilder stringBuilder = new StringBuilder();
            byte[] numArray1 = numArray;
            for (int i = 0; i < checked((int)numArray1.Length); i = checked(i + 1)) {
                byte num = numArray1[i];
                stringBuilder.Append(num.ToString("x2"));
            }
            str = stringBuilder.ToString();
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", str, "\r\n" };
            this.InternalLogData = string.Concat(name);
            return str;
        }

        [SecurityCritical]
        public string CalculateSHA256FromFile(string LocalFileName, long ByteRangeStart, long ByteRangeEnd) {
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesProcessed = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalLogData = "";
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   LocalFileName=", LocalFileName, "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            string str = "";
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            CalculateHash.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            if (ByteRangeStart < (long)0) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error, ByteRangeStart is less than 0.\r\n");
                this.InternalErrorNumber = 1009;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("ByteRangeStart=", Conversions.ToString(ByteRangeStart)));
                str = "";
            }
            else if (ByteRangeStart <= ByteRangeEnd) {
                HashAlgorithm hashAlgorithm = SHA256.Create();
                byte[] numArray = this.CalculateHashFromFileFunction(LocalFileName, ByteRangeStart, ByteRangeEnd, hashAlgorithm);
                StringBuilder stringBuilder = new StringBuilder();
                byte[] numArray1 = numArray;
                for (int i = 0; i < checked((int)numArray1.Length); i = checked(i + 1)) {
                    byte num = numArray1[i];
                    stringBuilder.Append(num.ToString("x2"));
                }
                str = stringBuilder.ToString();
            }
            else {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error, ByteRangeEnd is less than ByteRangeStart.\r\n");
                this.InternalErrorNumber = 1009;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("ByteRangeStart=", Conversions.ToString(ByteRangeStart), "   ByteRangeEnd=", Conversions.ToString(ByteRangeEnd)));
                str = "";
            }
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", str, "\r\n" };
            this.InternalLogData = string.Concat(name);
            return str;
        }

        /// <summary>Calculates the SHA-256 hash of a string.</summary>
        /// <param name="SourceString">The string to be hashed.</param>
        /// <returns>The SHA-256 hash value of the string.</returns>
        public string CalculateSHA256FromString(string SourceString) {
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesProcessed = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalLogData = "";
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   SourceString=", SourceString, "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            string str = "";
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            CalculateHash.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            this.InternalBytesTotal = (long)Strings.Len(SourceString);
            HashAlgorithm hashAlgorithm = SHA256.Create();
            byte[] numArray = hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(SourceString));
            StringBuilder stringBuilder = new StringBuilder();
            int length = checked(checked((int)numArray.Length) - 1);
            for (int i = 0; i <= length; i = checked(i + 1)) {
                stringBuilder.Append(numArray[i].ToString("x2"));
            }
            str = stringBuilder.ToString();
            this.InternalBytesProcessed = (long)Strings.Len(SourceString);
            this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising ProgressChangedEvent   BytesProcessed=", Conversions.ToString(this.InternalBytesProcessed));
            CalculateHash.ProgressChangedEventEventHandler progressChangedEventEventHandler = this.ProgressChangedEvent;
            if (progressChangedEventEventHandler != null) {
                progressChangedEventEventHandler();
            }
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", str, "\r\n" };
            this.InternalLogData = string.Concat(name);
            return str;
        }

        [SecurityCritical]
        public string CalculateSHA256TreeHashFromFile(string LocalFileName) {
            FileStream fileStream;
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesProcessed = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalLogData = "";
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   LocalFileName=", LocalFileName, "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            string str = "";
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            CalculateHash.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            try {
                name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the file   LocalFileName=", LocalFileName, "\r\n" };
                this.InternalLogData = string.Concat(name);
                fileStream = this.MyFileSystemCaller.FileOpenRead(LocalFileName);
            }
            catch (Exception exception1) {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error opening the file   Message=", exception.Message, "\r\n" };
                this.InternalLogData = string.Concat(name);
                this.InternalErrorNumber = 1007;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception.Message));
                ProjectData.ClearProjectError();
                goto Label0;
            }
            this.InternalBytesTotal = fileStream.Length;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   BytesTotal=", Conversions.ToString(this.InternalBytesTotal), "\r\n" };
            this.InternalLogData = string.Concat(name);
            if (this.InternalBytesTotal != (long)0) {
                str = this.CalculateTreeHash(fileStream, (long)-1, (long)-1);
            }
            else {
                HashAlgorithm hashAlgorithm = SHA256.Create();
                byte[] numArray = hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(""));
                StringBuilder stringBuilder = new StringBuilder();
                int length = checked(checked((int)numArray.Length) - 1);
                for (int i = 0; i <= length; i = checked(i + 1)) {
                    stringBuilder.Append(numArray[i].ToString("x2"));
                }
                str = stringBuilder.ToString();
            }
            if (!this.KeepGoing) {
                str = "";
                this.InternalErrorNumber = 1008;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, "");
            }
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Closing the file   LocalFileName=", LocalFileName, "\r\n" };
            this.InternalLogData = string.Concat(name);
            fileStream.Close();
            Label0:
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", str, "\r\n" };
            this.InternalLogData = string.Concat(name);
            return str;
        }

        /// <summary>Calculates the SHA-256 tree hash of a file.</summary>
        /// <param name="LocalFileName">The path to the local file.</param>
        /// <param name="ByteRangeStart">The byte index where you want to start hashing.</param>
        /// <param name="ByteRangeEnd">The byte index where you want to finish hashing.</param>
        /// <returns>The SHA-256 tree hash value of the file.</returns>
        [SecurityCritical]
        public string CalculateSHA256TreeHashFromFile(string LocalFileName, long ByteRangeStart, long ByteRangeEnd) {
            FileStream fileStream;
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesProcessed = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalLogData = "";
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   LocalFileName=", LocalFileName, "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            string str = "";
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            CalculateHash.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            if (ByteRangeStart < (long)0) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error, ByteRangeStart is less than 0.\r\n");
                this.InternalErrorNumber = 1009;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("ByteRangeStart=", Conversions.ToString(ByteRangeStart)));
                str = "";
            }
            else if (ByteRangeStart <= ByteRangeEnd) {
                try {
                    name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the file   LocalFileName=", LocalFileName, "\r\n" };
                    this.InternalLogData = string.Concat(name);
                    fileStream = this.MyFileSystemCaller.FileOpenRead(LocalFileName);
                }
                catch (Exception exception1) {
                    ProjectData.SetProjectError(exception1);
                    Exception exception = exception1;
                    name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error opening the file   Message=", exception.Message, "\r\n" };
                    this.InternalLogData = string.Concat(name);
                    this.InternalErrorNumber = 1007;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception.Message));
                    ProjectData.ClearProjectError();
                    goto Label0;
                }
                this.InternalBytesTotal = fileStream.Length;
                name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   BytesTotal=", Conversions.ToString(this.InternalBytesTotal), "\r\n" };
                this.InternalLogData = string.Concat(name);
                if (this.InternalBytesTotal != (long)0) {
                    str = this.CalculateTreeHash(fileStream, ByteRangeStart, ByteRangeEnd);
                }
                else {
                    HashAlgorithm hashAlgorithm = SHA256.Create();
                    byte[] numArray = hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(""));
                    StringBuilder stringBuilder = new StringBuilder();
                    int length = checked(checked((int)numArray.Length) - 1);
                    for (int i = 0; i <= length; i = checked(i + 1)) {
                        stringBuilder.Append(numArray[i].ToString("x2"));
                    }
                    str = stringBuilder.ToString();
                }
                if (!this.KeepGoing) {
                    str = "";
                    this.InternalErrorNumber = 1008;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, "");
                }
                name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Closing the file   LocalFileName=", LocalFileName, "\r\n" };
                this.InternalLogData = string.Concat(name);
                fileStream.Close();
            }
            else {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error, ByteRangeEnd is less than ByteRangeStart.\r\n");
                this.InternalErrorNumber = 1009;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("ByteRangeStart=", Conversions.ToString(ByteRangeStart), "   ByteRangeEnd=", Conversions.ToString(ByteRangeEnd)));
                str = "";
            }
            Label0:
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", str, "\r\n" };
            this.InternalLogData = string.Concat(name);
            return str;
        }

        private string CalculateTreeHash(Stream stream, long ByteRangeStart, long ByteRangeEnd) {
            List<byte[]> numArrays = this.computePartHashs(stream, ByteRangeStart, ByteRangeEnd);
            if (!this.KeepGoing) {
                return "";
            }
            byte[] numArray = this.computeTreehHash(numArrays);
            return BitConverter.ToString(numArray).Replace("-", "").ToLower();
        }

        private byte[] CombineByteArray(byte[] first, byte[] second) {
            if (checked((int)first.Length) == 0) {
                return second;
            }
            byte[] numArray = new byte[checked(checked(checked(checked((int)first.Length) + checked((int)second.Length)) - 1) + 1)];
            Buffer.BlockCopy(first, 0, numArray, 0, checked((int)first.Length));
            Buffer.BlockCopy(second, 0, numArray, checked((int)first.Length), checked((int)second.Length));
            return numArray;
        }

        private List<byte[]> computePartHashs(Stream stream, long ByteRangeStart, long ByteRangeEnd) {
            long position = stream.Position;
            if (ByteRangeStart > (long)-1) {
                stream.Seek(ByteRangeStart, SeekOrigin.Begin);
            }
            SHA256 sHA256 = SHA256.Create();
            List<byte[]> numArrays = new List<byte[]>();
            while (stream.Position < stream.Length && (ByteRangeEnd <= (long)-1 || stream.Position <= ByteRangeEnd)) {
                if (!this.KeepGoing) {
                    return null;
                }
                PartStreamWrapper partStreamWrapper = new PartStreamWrapper(stream, (long)1048576);
                numArrays.Add(sHA256.ComputeHash(partStreamWrapper));
                this.InternalBytesProcessed = stream.Position;
                CalculateHash.ProgressChangedEventEventHandler progressChangedEventEventHandler = this.ProgressChangedEvent;
                if (progressChangedEventEventHandler == null) {
                    continue;
                }
                progressChangedEventEventHandler();
            }
            stream.Seek(position, SeekOrigin.Begin);
            return numArrays;
        }

        private byte[] computeTreehHash(List<byte[]> partHashsums) {
            SHA256 sHA256 = SHA256.Create();
            while (partHashsums.Count > 1) {
                List<byte[]> numArrays = new List<byte[]>();
                int num = checked((int)Math.Round(Math.Floor((double)partHashsums.Count / 2) - 1));
                for (int i = 0; i <= num; i = checked(i + 1)) {
                    byte[] item = partHashsums[checked(2 * i)];
                    byte[] numArray = partHashsums[checked(checked(2 * i) + 1)];
                    byte[] numArray1 = new byte[checked(checked(checked(checked((int)item.Length) + checked((int)numArray.Length)) - 1) + 1)];
                    Array.Copy(item, 0, numArray1, 0, checked((int)item.Length));
                    Array.Copy(numArray, 0, numArray1, checked((int)item.Length), checked((int)numArray.Length));
                    sHA256.ComputeHash(numArray1);
                    numArrays.Add(sHA256.ComputeHash(numArray1));
                }
                if (partHashsums.Count % 2 == 1) {
                    numArrays.Add(partHashsums[checked(partHashsums.Count - 1)]);
                }
                partHashsums = numArrays;
            }
            return partHashsums[0];
        }

        private int getHexVal(char hex) {
            int num;
            int num1 = Convert.ToInt32(hex);
            int num2 = num1;
            if (num1 < 58) {
                num = 48;
            }
            else {
                num = (num1 < 97 ? 55 : 87);
            }
            return checked(num2 - num);
        }

        private byte[] stringToByteArrayFastest(string hex) {
            if (hex.Length % 2 == 1) {
                throw new Exception("The binary key cannot have an odd number of digits");
            }
            byte[] hexVal = new byte[checked(checked((hex.Length >> 1) - 1) + 1)];
            int num = Convert.ToInt32(decimal.Subtract(Math.Floor(new decimal(hex.Length >> 1)), decimal.One));
            for (int i = 0; i <= num; i = checked(i + 1)) {
                hexVal[i] = checked((byte)(checked((this.getHexVal(hex[i << 1]) << 4) + this.getHexVal(hex[checked((i << 1) + 1)]))));
            }
            return hexVal;
        }

        /// <summary>Fires when the value of the BytesProcessed property is changed.</summary>
        public event CalculateHash.ProgressChangedEventEventHandler ProgressChangedEvent;

        public event CalculateHash.StateChangedEventEventHandler StateChangedEvent;

        public delegate void ProgressChangedEventEventHandler();

        public delegate void StateChangedEventEventHandler();
    }
}
