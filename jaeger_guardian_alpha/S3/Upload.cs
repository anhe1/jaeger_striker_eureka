﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading;

namespace Jaeger.SoftAWS.S3 {
    public class Upload {
        private HttpWebRequest MyRequest;

        private bool KeepGoing;

        private int InternalState;

        private long InternalBytesTransfered;

        private long InternalBytesTotal;

        private float InternalLimitKBpsSpeed;

        private int InternalErrorNumber;

        private string InternalErrorDescription;

        private IWebProxy InternalProxy;

        private int InternalBufferLength;

        private int InternalTimeoutSeconds;

        private int InternalReadWriteTimeoutSeconds;

        private string InternalResponseString;

        private int InternalResponseStatusCode;

        private string InternalResponseStatusDescription;

        private Dictionary<string, string> InternalResponseHeaders;

        private string InternalLogData;

        private string InternalRequestURL;

        private string InternalRequestMethod;

        private Dictionary<string, string> InternalRequestHeaders;

        private FileSystemCaller MyFileSystemCaller;

        private bool InternalAllowWin32;

        private bool InternalIgnoreSSLCertificateErrors;

        /// <summary>Gets/Sets if Win32 API calls should be allowed for working with long paths.</summary>
        public bool AllowWin32 {
            get {
                return this.InternalAllowWin32;
            }
            set {
                this.InternalAllowWin32 = value;
                this.MyFileSystemCaller = new FileSystemCaller(this.InternalAllowWin32);
            }
        }

        /// <summary>The size of chunks that will be uploaded.</summary>
        public int BufferLength {
            get {
                return this.InternalBufferLength;
            }
            set {
                this.InternalBufferLength = value;
            }
        }

        public long BytesTotal {
            get {
                return this.InternalBytesTotal;
            }
        }

        /// <summary>The number of bytes that have been downloaded.</summary>
        public long BytesTransfered {
            get {
                return this.InternalBytesTransfered;
            }
        }

        /// <summary>A message that describes the last exception.</summary>
        public string ErrorDescription {
            get {
                return this.InternalErrorDescription;
            }
        }

        public int ErrorNumber {
            get {
                return this.InternalErrorNumber;
            }
        }

        public bool IgnoreSSLCertificateErrors {
            get {
                return this.InternalIgnoreSSLCertificateErrors;
            }
            set {
                this.InternalIgnoreSSLCertificateErrors = value;
            }
        }

        /// <summary>Gets/Sets the maximum upload speed.</summary>
        public float LimitKBpsSpeed {
            get {
                return this.InternalLimitKBpsSpeed;
            }
            set {
                this.InternalLimitKBpsSpeed = value;
            }
        }

        /// <summary>Returns log information related to last method call.</summary>
        public string LogData {
            get {
                return this.InternalLogData;
            }
        }

        public IWebProxy Proxy {
            get {
                return this.InternalProxy;
            }
            set {
                this.InternalProxy = value;
            }
        }

        /// <summary>Gets/Sets the ReadWriteTimeout value.</summary>
        public int ReadWriteTimeoutSeconds {
            get {
                return this.InternalReadWriteTimeoutSeconds;
            }
            set {
                this.InternalReadWriteTimeoutSeconds = value;
            }
        }

        public Dictionary<string, string> RequestHeaders {
            get {
                return this.InternalRequestHeaders;
            }
        }

        /// <summary>Returns the HTTP method that was used to connect to Amazon. This is the method that was passed to the function. This value is read only and is provided for logging purposes.</summary>
        public string RequestMethod {
            get {
                return this.InternalRequestMethod;
            }
        }

        public string RequestURL {
            get {
                return this.InternalRequestURL;
            }
        }

        public Dictionary<string, string> ResponseHeaders {
            get {
                return this.InternalResponseHeaders;
            }
        }

        public int ResponseStatusCode {
            get {
                return this.InternalResponseStatusCode;
            }
        }

        /// <summary>Returns the HTTP status description that was returned from Amazon.</summary>
        public string ResponseStatusDescription {
            get {
                return this.InternalResponseStatusDescription;
            }
        }

        public string ResponseString {
            get {
                return this.InternalResponseString;
            }
        }

        /// <summary>Indents and formats the ResponseString if it is XML.</summary>
        public string ResponseStringFormatted {
            get {
                return Shared_Module.FormatXML(this.InternalResponseString);
            }
        }

        public int State {
            get {
                return this.InternalState;
            }
        }

        public int TimeoutSeconds {
            get {
                return this.InternalTimeoutSeconds;
            }
            set {
                this.InternalTimeoutSeconds = value;
            }
        }

        /// <summary>Gets the version number of the SprightlySoft S3 component.</summary>
        public string Version {
            get {
                AssemblyName name = Assembly.GetExecutingAssembly().GetName();
                string[] str = new string[] { Conversions.ToString(name.Version.Major), ".", Conversions.ToString(name.Version.Minor), ".", Conversions.ToString(name.Version.Build) };
                return string.Concat(str);
            }
        }

        public Upload() {
            this.InternalState = 0;
            this.InternalProxy = null;
            this.InternalBufferLength = 16384;
            this.InternalLimitKBpsSpeed = 0f;
            this.InternalTimeoutSeconds = checked((int)Math.Round(Math.Floor(2147483.647)));
            this.InternalReadWriteTimeoutSeconds = 300;
            this.InternalBytesTransfered = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalResponseString = "";
            this.InternalResponseStatusCode = 0;
            this.InternalResponseStatusDescription = "";
            this.InternalResponseHeaders = null;
            this.InternalLogData = "";
            this.InternalRequestURL = "";
            this.InternalRequestMethod = "";
            this.InternalRequestHeaders = null;
            this.InternalAllowWin32 = true;
            this.MyFileSystemCaller = new FileSystemCaller(this.InternalAllowWin32);
            this.InternalIgnoreSSLCertificateErrors = false;
        }

        /// <summary>Aborts the current method.</summary>
        public void Abort() {
            if (this.MyRequest != null) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Aborting the request\r\n");
                this.MyRequest.Abort();
                this.KeepGoing = false;
            }
        }

        public string BuildS3RequestURL(bool UseSSL, string RequestEndpoint, string BucketName, string KeyName, string QueryString) {
            return Shared_Module.BuildS3RequestURLShared(UseSSL, RequestEndpoint, BucketName, KeyName, QueryString);
        }

        /// <summary>Gets the Signature Version 4 value which will be used in the Authorization header.</summary>
        /// <param name="RequestHeaders">Headers you will be sending in the request. One header must be x-amz-date.</param>
        /// <param name="ServiceURL">The URL you will be making the request against.</param>
        /// <param name="ServiceName">The service are using. For example glacier.</param>
        /// <param name="Region">The region you are using. For example us-east-1.</param>
        /// <param name="RequestMethod">The request method. This is usually GET, PUT, POST, or DELETE.</param>
        /// <param name="PostDataHash">The SHA-256 hash value of the data you will be sending.</param>
        /// <param name="AWSAccessKeyId">Your Amazon access key ID.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <returns>The Signature Version 4 value you will pass in the Authorization header.</returns>
        public string GetAWSSignatureVersion4Value(Dictionary<string, string> RequestHeaders, string ServiceURL, string ServiceName, string Region, string RequestMethod, string PostDataHash, string AWSAccessKeyId, string AWSSecretAccessKey) {
            string str = "";
            string str1 = "";
            return Shared_Module.GetAWSSignatureVersion4ValueShared(RequestHeaders, ServiceURL, ServiceName, Region, RequestMethod, PostDataHash, AWSAccessKeyId, AWSSecretAccessKey, ref str, ref str1);
        }

        public string GetAWSSignatureVersion4Value(Dictionary<string, string> RequestHeaders, string ServiceURL, string ServiceName, string Region, string RequestMethod, string PostDataHash, string AWSAccessKeyId, string AWSSecretAccessKey, ref string CanonicalString, ref string StringToSign) {
            return Shared_Module.GetAWSSignatureVersion4ValueShared(RequestHeaders, ServiceURL, ServiceName, Region, RequestMethod, PostDataHash, AWSAccessKeyId, AWSSecretAccessKey, ref CanonicalString, ref StringToSign);
        }

        public string GetS3AuthorizationValue(string RequestURL, string RequestMethod, Dictionary<string, string> ExtraRequestHeaders, string AWSAccessKeyId, string AWSSecretAccessKey) {
            string str = "";
            return Shared_Module.GetS3AuthorizationValueShared(RequestURL, RequestMethod, ExtraRequestHeaders, AWSAccessKeyId, AWSSecretAccessKey, ref str);
        }

        /// <summary>Gets the Authorization header value for a S3 request.</summary>
        /// <param name="RequestURL">The URL you will be making the request against. This includes the query string.</param>
        /// <param name="RequestMethod">The request method.  This is usually GET, PUT, POST, COPY or DELETE.</param>
        /// <param name="ExtraRequestHeaders">Extra headers you will be sending in the request.  One header must be x-amz-date.</param>
        /// <param name="AWSAccessKeyId">Your Amazon access key ID.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <param name="StringThatWasSigned">This parameter will contain the string that was hashed from the parameters you supplied.  It is useful if you are interested to see how the signature was calculated.</param>
        /// <returns>The value you will pass in the Authorization header.</returns>
        public string GetS3AuthorizationValue(string RequestURL, string RequestMethod, Dictionary<string, string> ExtraRequestHeaders, string AWSAccessKeyId, string AWSSecretAccessKey, ref string StringThatWasSigned) {
            return Shared_Module.GetS3AuthorizationValueShared(RequestURL, RequestMethod, ExtraRequestHeaders, AWSAccessKeyId, AWSSecretAccessKey, ref StringThatWasSigned);
        }

        public string GetS3AuthorizationValueByParts(string BucketName, string KeyName, string QueryString, string RequestMethod, Dictionary<string, string> ExtraRequestHeaders, string AWSAccessKeyId, string AWSSecretAccessKey) {
            string str = "";
            return Shared_Module.GetS3AuthorizationValueByPartsShared(BucketName, KeyName, QueryString, RequestMethod, ExtraRequestHeaders, AWSAccessKeyId, AWSSecretAccessKey, ref str);
        }

        /// <summary>Gets the Authorization header value for a S3 request by specifying the components of the request.</summary>
        /// <param name="BucketName">The bucket name you will make a request against.</param>
        /// <param name="KeyName">The key name you will make a request against.</param>
        /// <param name="QueryString">The query string you will include in the URL.</param>
        /// <param name="RequestMethod">The request method.  This is usually GET, PUT, POST, COPY or DELETE.</param>
        /// <param name="ExtraRequestHeaders">Extra headers you will be sending in the request.  One header must be x-amz-date.</param>
        /// <param name="AWSAccessKeyId">Your Amazon access key ID.</param>
        /// <param name="AWSSecretAccessKey">Your Amazon secret access key.</param>
        /// <param name="StringThatWasSigned">This parameter will contain the string that was hashed from the parameters you supplied.  It is useful if you are interested to see how the signature was calculated.</param>
        /// <returns>The value you will pass in the Authorization header.</returns>
        public string GetS3AuthorizationValueByParts(string BucketName, string KeyName, string QueryString, string RequestMethod, Dictionary<string, string> ExtraRequestHeaders, string AWSAccessKeyId, string AWSSecretAccessKey, ref string StringThatWasSigned) {
            return Shared_Module.GetS3AuthorizationValueByPartsShared(BucketName, KeyName, QueryString, RequestMethod, ExtraRequestHeaders, AWSAccessKeyId, AWSSecretAccessKey, ref StringThatWasSigned);
        }

        /// <summary>Takes a file and puts it in Amazon S3.</summary>
        /// <param name="RequestURL">The URL you will be making the request against. This includes the query string.</param>
        /// <param name="Method">The request method.  For an upload to S3 this is usually PUT.</param>
        /// <param name="ExtraRequestHeaders">A list of extra headers to send to Amazon.</param>
        /// <param name="LocalFileName">The full path of the file to upload.</param>
        /// <returns>Returns True if the upload was successful.</returns>
        [SecurityCritical]
        public bool UploadFile(string RequestURL, string Method, Dictionary<string, string> ExtraRequestHeaders, string LocalFileName) {
            bool flag;
            Stream stream;
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesTransfered = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalResponseString = "";
            this.InternalLogData = "";
            this.InternalResponseStatusCode = 0;
            this.InternalResponseStatusDescription = "";
            this.InternalResponseHeaders = new Dictionary<string, string>();
            this.InternalRequestURL = "";
            this.InternalRequestMethod = "";
            this.InternalRequestHeaders = new Dictionary<string, string>();
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   RequestURL=", RequestURL, "   Method=", Method, "   ExtraRequestHeaders=", Shared_Module.DictionaryToString(ExtraRequestHeaders), "   LocalFileName=", LocalFileName, "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            Upload.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            try {
                name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening file to upload   LocalFileName=", LocalFileName, "\r\n" };
                this.InternalLogData = string.Concat(name);
                stream = this.MyFileSystemCaller.FileOpenRead(LocalFileName);
            }
            catch (Exception exception1) {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error opening local file   Message=", exception.Message, "\r\n" };
                this.InternalLogData = string.Concat(name);
                this.InternalErrorNumber = 1123;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception.Message));
                flag = false;
                ProjectData.ClearProjectError();
                goto Label0;
            }
            flag = this.UploadFileFunction(RequestURL, Method, ExtraRequestHeaders, ref stream, (long)-1, (long)-1);
            try {
                stream.Close();
            }
            catch (Exception exception2) {
                ProjectData.SetProjectError(exception2);
                ProjectData.ClearProjectError();
            }
            stream = null;
            Label0:
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", Conversions.ToString(flag), "\r\n" };
            this.InternalLogData = string.Concat(name);
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            return flag;
        }

        /// <summary>Takes a file and puts it in Amazon S3.</summary>
        /// <param name="RequestURL">The URL you will be making the request against. This includes the query string.</param>
        /// <param name="Method">The request method.  For an upload to S3 this is usually PUT.</param>
        /// <param name="ExtraRequestHeaders">A list of extra headers to send to Amazon.</param>
        /// <param name="LocalFileName">The full path of the file to upload.</param>
        /// <returns>Returns True if the upload was successful.</returns>
        [SecurityCritical]
        public bool UploadFile(string RequestURL, string Method, Dictionary<string, string> ExtraRequestHeaders, Stream stream1, string LocalFileName) {
            bool flag;
            Stream stream = stream1;
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesTransfered = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalResponseString = "";
            this.InternalLogData = "";
            this.InternalResponseStatusCode = 0;
            this.InternalResponseStatusDescription = "";
            this.InternalResponseHeaders = new Dictionary<string, string>();
            this.InternalRequestURL = "";
            this.InternalRequestMethod = "";
            this.InternalRequestHeaders = new Dictionary<string, string>();
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   RequestURL=", RequestURL, "   Method=", Method, "   ExtraRequestHeaders=", Shared_Module.DictionaryToString(ExtraRequestHeaders), "   LocalFileName=", LocalFileName, "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            Upload.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            try {
                name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening file to upload   LocalFileName=", LocalFileName, "\r\n" };
                this.InternalLogData = string.Concat(name);
                //stream = this.MyFileSystemCaller.FileOpenRead(LocalFileName);
            }
            catch (Exception exception1) {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error opening local file   Message=", exception.Message, "\r\n" };
                this.InternalLogData = string.Concat(name);
                this.InternalErrorNumber = 1123;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception.Message));
                flag = false;
                ProjectData.ClearProjectError();
                goto Label0;
            }
            flag = this.UploadFileFunction(RequestURL, Method, ExtraRequestHeaders, ref stream, (long)-1, (long)-1);
            //try {
            //    stream.Close();
            //}
            //catch (Exception exception2) {
            //    ProjectData.SetProjectError(exception2);
            //    ProjectData.ClearProjectError();
            //}
            //stream = null;
            Label0:
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", Conversions.ToString(flag), "\r\n" };
            this.InternalLogData = string.Concat(name);
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            return flag;
        }

        [SecurityCritical]
        public bool UploadFile(string RequestURL, string Method, Dictionary<string, string> ExtraRequestHeaders, string LocalFileName, long ByteRangeStart, long ByteRangeEnd) {
            bool flag;
            Stream stream;
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesTransfered = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalResponseString = "";
            this.InternalLogData = "";
            this.InternalResponseStatusCode = 0;
            this.InternalResponseStatusDescription = "";
            this.InternalResponseHeaders = new Dictionary<string, string>();
            this.InternalRequestURL = "";
            this.InternalRequestMethod = "";
            this.InternalRequestHeaders = new Dictionary<string, string>();
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   RequestURL=", RequestURL, "   Method=", Method, "   ExtraRequestHeaders=", Shared_Module.DictionaryToString(ExtraRequestHeaders), "   LocalFileName=", LocalFileName, "   ByteRangeStart=", Conversions.ToString(ByteRangeStart), "   ByteRangeEnd=", Conversions.ToString(ByteRangeEnd), "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            Upload.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            try {
                name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening file to upload   LocalFileName=", LocalFileName, "\r\n" };
                this.InternalLogData = string.Concat(name);
                stream = this.MyFileSystemCaller.FileOpenRead(LocalFileName);
            }
            catch (Exception exception1) {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error opening local file   Message=", exception.Message, "\r\n" };
                this.InternalLogData = string.Concat(name);
                this.InternalErrorNumber = 1123;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception.Message));
                flag = false;
                ProjectData.ClearProjectError();
                goto Label0;
            }
            flag = this.UploadFileFunction(RequestURL, Method, ExtraRequestHeaders, ref stream, ByteRangeStart, ByteRangeEnd);
            try {
                stream.Close();
            }
            catch (Exception exception2) {
                ProjectData.SetProjectError(exception2);
                ProjectData.ClearProjectError();
            }
            stream = null;
            Label0:
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", Conversions.ToString(flag), "\r\n" };
            this.InternalLogData = string.Concat(name);
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            return flag;
        }

        /// <summary>Takes a stream and uploads it to a file in Amazon S3.</summary>
        /// <param name="RequestURL">The URL you will be making the request against. This includes the query string.</param>
        /// <param name="Method">The request method.  For an upload to S3 this is usually PUT.</param>
        /// <param name="ExtraRequestHeaders">A list of extra headers to send to Amazon.</param>
        /// <param name="SourceStream">The stream to upload.</param>
        /// <returns>Returns True if the upload was successful.</returns>
        public bool UploadFile(string RequestURL, string Method, Dictionary<string, string> ExtraRequestHeaders, ref Stream SourceStream) {
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesTransfered = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalResponseString = "";
            this.InternalLogData = "";
            this.InternalResponseStatusCode = 0;
            this.InternalResponseStatusDescription = "";
            this.InternalResponseHeaders = new Dictionary<string, string>();
            this.InternalRequestURL = "";
            this.InternalRequestMethod = "";
            this.InternalRequestHeaders = new Dictionary<string, string>();
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   RequestURL=", RequestURL, "   Method=", Method, "   ExtraRequestHeaders=", Shared_Module.DictionaryToString(ExtraRequestHeaders), "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            Upload.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            bool flag = this.UploadFileFunction(RequestURL, Method, ExtraRequestHeaders, ref SourceStream, (long)-1, (long)-1);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", Conversions.ToString(flag), "\r\n" };
            this.InternalLogData = string.Concat(name);
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            return flag;
        }

        public bool UploadFile(string RequestURL, string Method, Dictionary<string, string> ExtraRequestHeaders, ref Stream SourceStream, long ByteRangeStart, long ByteRangeEnd) {
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesTransfered = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalResponseString = "";
            this.InternalLogData = "";
            this.InternalResponseStatusCode = 0;
            this.InternalResponseStatusDescription = "";
            this.InternalResponseHeaders = new Dictionary<string, string>();
            this.InternalRequestURL = "";
            this.InternalRequestMethod = "";
            this.InternalRequestHeaders = new Dictionary<string, string>();
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   RequestURL=", RequestURL, "   Method=", Method, "   ExtraRequestHeaders=", Shared_Module.DictionaryToString(ExtraRequestHeaders), "   ByteRangeStart=", Conversions.ToString(ByteRangeStart), "   ByteRangeEnd=", Conversions.ToString(ByteRangeEnd), "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            Upload.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            bool flag = this.UploadFileFunction(RequestURL, Method, ExtraRequestHeaders, ref SourceStream, ByteRangeStart, ByteRangeEnd);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", Conversions.ToString(flag), "\r\n" };
            this.InternalLogData = string.Concat(name);
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            return flag;
        }

        private bool UploadFileFunction(string RequestURL, string Method, Dictionary<string, string> ExtraRequestHeaders, ref Stream SourceStream, long ByteRangeStart, long ByteRangeEnd) {
            int num;
            Stream requestStream = null;
            HttpWebResponse response = null;
            long num1 = 0L;
            long num2 = 0L;
            string[] internalLogData;
            Dictionary<string, string>.Enumerator enumerator = new Dictionary<string, string>.Enumerator();
            IEnumerator enumerator1 = null;
            object[] objectValue;
            IEnumerator enumerator2 = null;
            IEnumerator enumerator3 = null;
            IEnumerator enumerator4 = null;
            IEnumerator enumerator5 = null;
            IEnumerator enumerator6 = null;
            IEnumerator enumerator7 = null;
            bool flag = true;
            bool flag1 = false;
            if (ByteRangeStart == (long)-1 & ByteRangeEnd == (long)-1) {
                this.InternalBytesTotal = SourceStream.Length;
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   BytesTotal=", Conversions.ToString(this.InternalBytesTotal), "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
            }
            else if (ByteRangeStart < (long)0) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error, ByteRangeStart is less than 0.\r\n");
                this.InternalErrorNumber = 1009;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("ByteRangeStart=", Conversions.ToString(ByteRangeStart)));
                flag = false;
                goto Label0;
            }
            else if (ByteRangeStart <= ByteRangeEnd) {
                if (ByteRangeEnd > SourceStream.Length) {
                    goto Label1;
                }
                this.InternalBytesTotal = checked(checked(ByteRangeEnd - ByteRangeStart) + (long)1);
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   BytesTotal=", Conversions.ToString(this.InternalBytesTotal), "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Seeking to ", Conversions.ToString(ByteRangeStart), "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                SourceStream.Seek(ByteRangeStart, SeekOrigin.Begin);
            }
            else {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error, ByteRangeEnd is less than ByteRangeStart.\r\n");
                this.InternalErrorNumber = 1009;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("ByteRangeStart=", Conversions.ToString(ByteRangeStart), "   ByteRangeEnd=", Conversions.ToString(ByteRangeEnd)));
                flag = false;
                goto Label0;
            }
            this.InternalRequestURL = RequestURL;
            this.InternalRequestMethod = Method;
            ServicePointManager.ServerCertificateValidationCallback = null;
            if (this.InternalIgnoreSSLCertificateErrors) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Setting ServerCertificateValidationCallback\r\n");
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(SharedModule.CertificateValidationCallBack);
            }
            this.MyRequest = (HttpWebRequest)WebRequest.Create(RequestURL);
            this.MyRequest.ServicePoint.ConnectionLimit = 50;
            this.MyRequest.AllowWriteStreamBuffering = false;
            this.MyRequest.AllowAutoRedirect = true;
            this.MyRequest.Timeout = checked(this.InternalTimeoutSeconds * 1000);
            this.MyRequest.ReadWriteTimeout = checked(this.InternalReadWriteTimeoutSeconds * 1000);
            this.MyRequest.Method = Method;
            this.MyRequest.ServicePoint.Expect100Continue = true;
            this.MyRequest.KeepAlive = false;
            this.MyRequest.ContentLength = this.InternalBytesTotal;
            this.MyRequest.Credentials = CredentialCache.DefaultCredentials;
            if (ExtraRequestHeaders != null) {
                string str = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
                try {
                    enumerator = ExtraRequestHeaders.GetEnumerator();
                    while (enumerator.MoveNext()) {
                        KeyValuePair<string, string> current = enumerator.Current;
                        int num3 = Strings.Len(current.Value);
                        int num4 = 1;
                        while (num4 <= num3) {
                            if (Strings.InStr(str, Strings.Mid(current.Value, num4, 1), CompareMethod.Binary) != 0) {
                                num4 = checked(num4 + 1);
                            }
                            else {
                                this.InternalErrorNumber = 1001;
                                int internalErrorNumber = this.InternalErrorNumber;
                                internalLogData = new string[] { "Key=", current.Key, "   Value=", current.Value, "   Invalid Character=", Strings.Mid(current.Value, num4, 1) };
                                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(internalErrorNumber, string.Concat(internalLogData));
                                flag = false;
                                goto Label0;
                            }
                        }
                        if (Operators.CompareString(Strings.LCase(current.Key), "content-type", false) == 0) {
                            this.MyRequest.ContentType = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "user-agent", false) == 0) {
                            this.MyRequest.UserAgent = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "referer", false) == 0) {
                            this.MyRequest.Referer = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "accept", false) == 0) {
                            this.MyRequest.Accept = current.Value;
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "if-modified-since", false) == 0) {
                            this.MyRequest.IfModifiedSince = Conversions.ToDate(current.Value);
                        }
                        else if (Operators.CompareString(Strings.LCase(current.Key), "host", false) != 0) {
                            try {
                                this.MyRequest.Headers[current.Key] = current.Value;
                            }
                            catch (Exception exception1) {
                                ProjectData.SetProjectError(exception1);
                                Exception exception = exception1;
                                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error adding request header   RequestHeader=", current.Key, ":", current.Value, "   Message=", exception.Message, "\r\n" };
                                this.InternalLogData = string.Concat(internalLogData);
                                this.InternalErrorNumber = 1002;
                                int internalErrorNumber1 = this.InternalErrorNumber;
                                internalLogData = new string[] { "Message=", exception.Message, "   Header Name=", current.Key, "   Header Value=", current.Value };
                                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(internalErrorNumber1, string.Concat(internalLogData));
                                flag = false;
                                ProjectData.ClearProjectError();
                                goto Label0;
                            }
                        }
                    }
                }
                finally {
                    ((IDisposable)enumerator).Dispose();
                }
            }
            if (Operators.CompareString(this.MyRequest.UserAgent, "", false) == 0) {
                this.MyRequest.UserAgent = string.Concat("SoftAWS/", this.Version);
            }
            if (!Information.IsNothing(this.InternalProxy)) {
                this.MyRequest.Proxy = this.InternalProxy;
                Uri proxy = this.InternalProxy.GetProxy(this.MyRequest.Address);
                if (this.MyRequest.Address != proxy) {
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Proxy server set   Proxy.Address=", proxy.AbsoluteUri, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                }
            }
            try {
                enumerator1 = this.MyRequest.Headers.GetEnumerator();
                while (enumerator1.MoveNext()) {
                    object obj = RuntimeHelpers.GetObjectValue(enumerator1.Current);
                    object obj1 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   RequestHeader="), obj), ":");
                    WebHeaderCollection headers = this.MyRequest.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj) };
                    this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj1, NewLateBinding.LateGet(headers, null, "Item", objectValue, null, null, null)), "\r\n"));
                    Dictionary<string, string> internalRequestHeaders = this.InternalRequestHeaders;
                    string str1 = Conversions.ToString(obj);
                    WebHeaderCollection webHeaderCollection = this.MyRequest.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj) };
                    internalRequestHeaders.Add(str1, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection, null, "Item", objectValue, null, null, null)));
                }
            }
            finally {
                if (enumerator1 is IDisposable) {
                    (enumerator1 as IDisposable).Dispose();
                }
            }
            try {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the request stream\r\n");
                requestStream = this.MyRequest.GetRequestStream();
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the request stream was successful\r\n");
            }
            catch (Exception exception3) {
                ProjectData.SetProjectError(exception3);
                Exception exception2 = exception3;
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the request stream failed   Message=", exception2.Message, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                this.InternalErrorNumber = 1004;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception2.Message));
                flag = false;
                ProjectData.ClearProjectError();
                goto Label0;
            }
            flag1 = true;
            if (!this.MyRequest.HaveResponse) {
                byte[] numArray = new byte[checked(this.InternalBufferLength + 1)];
                long internalBytesTotal = this.InternalBytesTotal;
                long num5 = (long)0;
                ArrayList arrayLists = new ArrayList();
                ArrayList arrayLists1 = new ArrayList();
                DateTime now = DateTime.Now;
                arrayLists.Add(now.Ticks);
                arrayLists1.Add(this.InternalBytesTransfered);
                ArrayList arrayLists2 = new ArrayList();
                Label2:
                while (internalBytesTotal > (long)0) {
                    try {
                        num = (internalBytesTotal < (long)(checked((int)numArray.Length)) ? SourceStream.Read(numArray, 0, checked((int)internalBytesTotal)) : SourceStream.Read(numArray, 0, checked((int)numArray.Length)));
                    }
                    catch (Exception exception5) {
                        ProjectData.SetProjectError(exception5);
                        Exception exception4 = exception5;
                        try {
                            enumerator2 = arrayLists2.GetEnumerator();
                            while (enumerator2.MoveNext()) {
                                object objectValue1 = RuntimeHelpers.GetObjectValue(enumerator2.Current);
                                this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(this.InternalLogData, objectValue1), "\r\n"));
                            }
                        }
                        finally {
                            if (enumerator2 is IDisposable) {
                                (enumerator2 as IDisposable).Dispose();
                            }
                        }
                        internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Reading the source file failed   Message=", exception4.Message, "\r\n" };
                        this.InternalLogData = string.Concat(internalLogData);
                        this.InternalErrorNumber = 1121;
                        this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception4.Message));
                        flag = false;
                        ProjectData.ClearProjectError();
                        goto Label0;
                    }
                    if (num <= 0) {
                        try {
                            enumerator4 = arrayLists2.GetEnumerator();
                            while (enumerator4.MoveNext()) {
                                object objectValue2 = RuntimeHelpers.GetObjectValue(enumerator4.Current);
                                this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(this.InternalLogData, objectValue2), "\r\n"));
                            }
                        }
                        finally {
                            if (enumerator4 is IDisposable) {
                                (enumerator4 as IDisposable).Dispose();
                            }
                        }
                        this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Nothing to write.  Unexpected end of file.\r\n");
                        this.InternalErrorNumber = 1005;
                        this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, "");
                        flag = false;
                        goto Label0;
                    }
                    else {
                        try {
                            requestStream.Write(numArray, 0, num);
                        }
                        catch (Exception exception7) {
                            ProjectData.SetProjectError(exception7);
                            Exception exception6 = exception7;
                            try {
                                enumerator3 = arrayLists2.GetEnumerator();
                                while (enumerator3.MoveNext()) {
                                    object obj2 = RuntimeHelpers.GetObjectValue(enumerator3.Current);
                                    this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(this.InternalLogData, obj2), "\r\n"));
                                }
                            }
                            finally {
                                if (enumerator3 is IDisposable) {
                                    (enumerator3 as IDisposable).Dispose();
                                }
                            }
                            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Writing to the upload stream failed   Message=", exception6.Message, "\r\n" };
                            this.InternalLogData = string.Concat(internalLogData);
                            this.InternalErrorNumber = 1122;
                            this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception6.Message));
                            flag = false;
                            ProjectData.ClearProjectError();
                            goto Label0;
                        }
                        num5 = checked(num5 + (long)num);
                        this.InternalBytesTransfered = num5;
                        internalBytesTotal = checked(internalBytesTotal - (long)num);
                        now = DateTime.Now;
                        arrayLists.Add(now.Ticks);
                        arrayLists1.Add(this.InternalBytesTransfered);
                        Upload.ProgressChangedEventEventHandler progressChangedEventEventHandler = this.ProgressChangedEvent;
                        if (progressChangedEventEventHandler != null) {
                            progressChangedEventEventHandler();
                        }
                        arrayLists2.Add(string.Concat(Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising ProgressChangedEvent   BytesTransfered=", Conversions.ToString(this.InternalBytesTransfered)));
                        if (arrayLists2.Count == 31) {
                            arrayLists2[10] = "... The log data was truncated.  Only the first 10 and last 20 events were logged.  Monitor the ProgressChangedEvent for all values.";
                        }
                        else if (arrayLists2.Count > 31) {
                            arrayLists2.RemoveAt(11);
                        }
                        int count = checked(arrayLists.Count - 1);
                        int num6 = 0;
                        while (num6 <= count) {
                            num2 = Conversions.ToLong(arrayLists[num6]);
                            num1 = Conversions.ToLong(arrayLists1[num6]);
                            now = DateTime.Now;
                            TimeSpan timeSpan = new TimeSpan(Conversions.ToLong(Operators.SubtractObject(now.Ticks, arrayLists[num6])));
                            if (timeSpan.TotalSeconds >= 60) {
                                num6 = checked(num6 + 1);
                            }
                            else {
                                int num7 = checked(num6 - 1);
                                for (int i = 0; i <= num7; i = checked(i + 1)) {
                                    arrayLists.RemoveAt(0);
                                    arrayLists1.RemoveAt(0);
                                }
                                break;
                            }
                        }
                        long ticks = DateTime.Now.Ticks;
                        long num8 = Conversions.ToLong(arrayLists1[checked(arrayLists1.Count - 1)]);
                        TimeSpan timeSpan1 = new TimeSpan(checked(ticks - num2));
                        long num9 = checked(num8 - num1);
                        float totalSeconds = (float)((double)num9 / 1024 / timeSpan1.TotalSeconds);
                        if (this.InternalLimitKBpsSpeed <= 0f || totalSeconds <= this.InternalLimitKBpsSpeed) {
                            continue;
                        }
                        long num10 = checked((long)Math.Round((double)(checked(num9 + (long)this.InternalBufferLength)) / 1024 / (double)this.InternalLimitKBpsSpeed * 10000000 + (double)num2));
                        now = DateTime.Now;
                        float single = (float)((double)(checked(now.Ticks - num2)) / 10000000);
                        now = DateTime.Now;
                        float ticks1 = (float)((double)(checked(num10 - now.Ticks)) / 10000000);
                        while (DateTime.Now.Ticks < num10) {
                            if (this.KeepGoing) {
                                Thread.Sleep(50);
                            }
                            else {
                                goto Label2;
                            }
                        }
                    }
                }
                try {
                    enumerator5 = arrayLists2.GetEnumerator();
                    while (enumerator5.MoveNext()) {
                        object objectValue3 = RuntimeHelpers.GetObjectValue(enumerator5.Current);
                        this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(this.InternalLogData, objectValue3), "\r\n"));
                    }
                }
                finally {
                    if (enumerator5 is IDisposable) {
                        (enumerator5 as IDisposable).Dispose();
                    }
                }
            }
            else {
                flag = false;
            }
            Label0:
            if (requestStream != null) {
                try {
                    requestStream.Close();
                }
                catch (Exception exception8) {
                    ProjectData.SetProjectError(exception8);
                    ProjectData.ClearProjectError();
                }
                requestStream = null;
            }
            if (flag1) {
                try {
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response\r\n");
                    response = (HttpWebResponse)this.MyRequest.GetResponse();
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response was successful\r\n");
                }
                catch (WebException webException1) {
                    ProjectData.SetProjectError(webException1);
                    WebException webException = webException1;
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response failed   Message=", webException.Message, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    this.InternalErrorNumber = 1003;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", webException.Message));
                    HttpWebResponse httpWebResponse = (HttpWebResponse)webException.Response;
                    if (!Information.IsNothing(httpWebResponse)) {
                        this.InternalResponseStatusCode = (int)httpWebResponse.StatusCode;
                        this.InternalResponseStatusDescription = httpWebResponse.StatusDescription;
                        internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusCode=", Conversions.ToString((int)httpWebResponse.StatusCode), "\r\n" };
                        this.InternalLogData = string.Concat(internalLogData);
                        internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusDescription=", httpWebResponse.StatusDescription, "\r\n" };
                        this.InternalLogData = string.Concat(internalLogData);
                        try {
                            enumerator6 = httpWebResponse.Headers.GetEnumerator();
                            while (enumerator6.MoveNext()) {
                                object obj3 = RuntimeHelpers.GetObjectValue(enumerator6.Current);
                                object obj4 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   ResponseHeader="), obj3), ":");
                                WebHeaderCollection headers1 = httpWebResponse.Headers;
                                objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj3) };
                                this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj4, NewLateBinding.LateGet(headers1, null, "Item", objectValue, null, null, null)), "\r\n"));
                                Dictionary<string, string> internalResponseHeaders = this.InternalResponseHeaders;
                                string str2 = Conversions.ToString(obj3);
                                WebHeaderCollection webHeaderCollection1 = httpWebResponse.Headers;
                                objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj3) };
                                internalResponseHeaders.Add(str2, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection1, null, "Item", objectValue, null, null, null)));
                            }
                        }
                        finally {
                            if (enumerator6 is IDisposable) {
                                (enumerator6 as IDisposable).Dispose();
                            }
                        }
                        Stream responseStream = httpWebResponse.GetResponseStream();
                        try {
                            this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the error response stream\r\n");
                            string end = (new StreamReader(responseStream, true)).ReadToEnd();
                            this.InternalResponseString = end;
                            this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the error response stream was successful\r\n");
                            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   StreamData=", end, "\r\n" };
                            this.InternalLogData = string.Concat(internalLogData);
                        }
                        catch (Exception exception10) {
                            ProjectData.SetProjectError(exception10);
                            Exception exception9 = exception10;
                            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error getting the error response stream   Message=", exception9.Message, "\r\n" };
                            this.InternalLogData = string.Concat(internalLogData);
                            ProjectData.ClearProjectError();
                        }
                        httpWebResponse.Close();
                    }
                    flag = false;
                    this.MyRequest.Abort();
                    ProjectData.ClearProjectError();
                }
                catch (Exception exception12) {
                    ProjectData.SetProjectError(exception12);
                    Exception exception11 = exception12;
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error getting the response   Message=", exception11.Message, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    flag = false;
                    this.InternalErrorNumber = 1003;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception11.Message));
                    this.MyRequest.Abort();
                    ProjectData.ClearProjectError();
                }
            }
            if (!Information.IsNothing(response)) {
                this.InternalResponseStatusCode = (int)response.StatusCode;
                this.InternalResponseStatusDescription = response.StatusDescription;
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusCode=", Conversions.ToString((int)response.StatusCode), "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusDescription=", response.StatusDescription, "\r\n" };
                this.InternalLogData = string.Concat(internalLogData);
                try {
                    enumerator7 = response.Headers.GetEnumerator();
                    while (enumerator7.MoveNext()) {
                        object objectValue4 = RuntimeHelpers.GetObjectValue(enumerator7.Current);
                        object obj5 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   ResponseHeader="), objectValue4), ":");
                        WebHeaderCollection headers2 = response.Headers;
                        objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue4) };
                        this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj5, NewLateBinding.LateGet(headers2, null, "Item", objectValue, null, null, null)), "\r\n"));
                        Dictionary<string, string> strs = this.InternalResponseHeaders;
                        string str3 = Conversions.ToString(objectValue4);
                        WebHeaderCollection webHeaderCollection2 = response.Headers;
                        objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue4) };
                        strs.Add(str3, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection2, null, "Item", objectValue, null, null, null)));
                    }
                }
                finally {
                    if (enumerator7 is IDisposable) {
                        (enumerator7 as IDisposable).Dispose();
                    }
                }
                Stream stream = response.GetResponseStream();
                try {
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response stream\r\n");
                    string end1 = (new StreamReader(stream, true)).ReadToEnd();
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response stream was successful\r\n");
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   StreamData=", end1, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                }
                catch (Exception exception14) {
                    ProjectData.SetProjectError(exception14);
                    Exception exception13 = exception14;
                    internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error getting the response stream   Message=", exception13.Message, "\r\n" };
                    this.InternalLogData = string.Concat(internalLogData);
                    flag = false;
                    this.InternalErrorNumber = 1006;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception13.Message));
                    ProjectData.ClearProjectError();
                }
                response.Close();
            }
            internalLogData = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ErrorNumber=", Conversions.ToString(this.InternalErrorNumber), "   ErrorDescription=", this.InternalErrorDescription, "\r\n" };
            this.InternalLogData = string.Concat(internalLogData);
            return flag;
            Label1:
            this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error, ByteRangeEnd is greater than the length of the file.\r\n");
            this.InternalErrorNumber = 1009;
            this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("ByteRangeEnd=", Conversions.ToString(ByteRangeEnd), "   FileLength=", Conversions.ToString(SourceStream.Length)));
            flag = false;
            goto Label0;
        }

        /// <summary>Takes a file and puts it in Amazon S3.</summary>
        /// <param name="RequestURL">The URL you will be making the request against. This includes the query string.</param>
        /// <param name="Method">The request method.  For an upload to S3 this is usually PUT.</param>
        /// <param name="ExtraRequestHeaders">A list of extra headers to send to Amazon.</param>
        /// <param name="FormCollection">A collection of form keys and values.</param>
        /// <param name="LocalFileNameArray">An array of file name paths of files to upload.</param>
        /// <returns>Returns True if the upload was successful.</returns>
        [SecurityCritical]
        public bool UploadMultipartForm(string RequestURL, string Method, Dictionary<string, string> ExtraRequestHeaders, NameValueCollection FormCollection, string[] LocalFileNameArray) {
            bool flag;
            string[] name;
            if (this.InternalState != 0) {
                name = new string[] { MethodBase.GetCurrentMethod().ReflectedType.Name, ".", MethodBase.GetCurrentMethod().Name, "   An operation is in progress.  Abort the current operation or create a new instance of the class.  State=", Conversions.ToString(this.State) };
                throw new Exception(string.Concat(name));
            }
            this.KeepGoing = true;
            this.InternalBytesTransfered = (long)0;
            this.InternalBytesTotal = (long)0;
            this.InternalErrorNumber = 0;
            this.InternalErrorDescription = "";
            this.InternalResponseString = "";
            this.InternalLogData = "";
            this.InternalResponseStatusCode = 0;
            this.InternalResponseStatusDescription = "";
            this.InternalResponseHeaders = new Dictionary<string, string>();
            this.InternalRequestURL = "";
            this.InternalRequestMethod = "";
            this.InternalRequestHeaders = new Dictionary<string, string>();
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " starting   RequestURL=", RequestURL, "   Method=", Method, "   ExtraRequestHeaders=", Shared_Module.DictionaryToString(ExtraRequestHeaders), "   LocalFileName=", string.Join(";", LocalFileNameArray), "\r\n" };
            this.InternalLogData = string.Concat(name);
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Version=", this.Version, "\r\n" };
            this.InternalLogData = string.Concat(name);
            this.InternalState = 1;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            Upload.StateChangedEventEventHandler stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            try {
                string[] localFileNameArray = LocalFileNameArray;
                for (int i = 0; i < checked((int)localFileNameArray.Length); i = checked(i + 1)) {
                    string str = localFileNameArray[i];
                    name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening file to upload   LocalFileName=", str, "\r\n" };
                    this.InternalLogData = string.Concat(name);
                    this.MyFileSystemCaller.FileOpenRead(str).Close();
                }
            }
            catch (Exception exception1) {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error opening local file   Message=", exception.Message, "\r\n" };
                this.InternalLogData = string.Concat(name);
                this.InternalErrorNumber = 1123;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception.Message));
                flag = false;
                ProjectData.ClearProjectError();
                goto Label0;
            }
            flag = this.UploadMultipartFormFunction(RequestURL, Method, ExtraRequestHeaders, FormCollection, LocalFileNameArray);
            Label0:
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ", MethodBase.GetCurrentMethod().Name, " complete   Response=", Conversions.ToString(flag), "\r\n" };
            this.InternalLogData = string.Concat(name);
            this.InternalState = 0;
            name = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising StateChangedEvent   State=", Conversions.ToString(this.InternalState), "\r\n" };
            this.InternalLogData = string.Concat(name);
            stateChangedEventEventHandler = this.StateChangedEvent;
            if (stateChangedEventEventHandler != null) {
                stateChangedEventEventHandler();
            }
            return flag;
        }

        [SecurityCritical]
        private bool UploadMultipartFormFunction(string RequestURL, string Method, Dictionary<string, string> ExtraRequestHeaders, NameValueCollection FormCollection, string[] LocalFileNameArray) {
            int num;
            Stream requestStream = null;
            HttpWebResponse response = null;
            long num1 = 0L;
            long num2 = 0L;
            IEnumerator enumerator = null;
            Dictionary<string, string>.Enumerator enumerator1 = new Dictionary<string, string>.Enumerator();
            IEnumerator enumerator2 = null;
            object[] objectValue;
            IEnumerator enumerator3 = null;
            IEnumerator enumerator4 = null;
            IEnumerator enumerator5 = null;
            IEnumerator enumerator6 = null;
            IEnumerator enumerator7 = null;
            IEnumerator enumerator8 = null;
            bool flag = true;
            bool flag1 = false;
            this.InternalRequestURL = RequestURL;
            this.InternalRequestMethod = Method;
            ServicePointManager.ServerCertificateValidationCallback = null;
            if (this.InternalIgnoreSSLCertificateErrors) {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Setting ServerCertificateValidationCallback\r\n");
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(SharedModule.CertificateValidationCallBack);
            }
            this.MyRequest = (HttpWebRequest)WebRequest.Create(RequestURL);
            Guid guid = Guid.NewGuid();
            string str = string.Concat("--", guid.ToString());
            this.MyRequest.ContentType = string.Concat("multipart/form-data; boundary=", str);
            Stream memoryStream = new MemoryStream();
            byte[] bytes = Encoding.ASCII.GetBytes(string.Concat(Environment.NewLine, "--", str, Environment.NewLine));
            string[] newLine = new string[] { Environment.NewLine, "--", str, Environment.NewLine, "Content-Disposition: form-data; name=\"{0}\";", Environment.NewLine, Environment.NewLine, "{1}" };
            string str1 = string.Concat(newLine);
            try {
                enumerator = FormCollection.Keys.GetEnumerator();
                while (enumerator.MoveNext()) {
                    string str2 = Conversions.ToString(enumerator.Current);
                    string str3 = string.Format(str1, str2, FormCollection[str2]);
                    byte[] numArray = Encoding.UTF8.GetBytes(str3);
                    memoryStream.Write(numArray, 0, checked((int)numArray.Length));
                }
            }
            finally {
                if (enumerator is IDisposable) {
                    (enumerator as IDisposable).Dispose();
                }
            }
            memoryStream.Write(bytes, 0, checked((int)bytes.Length));
            newLine = new string[] { "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"", Environment.NewLine, "Content-Type: application/octet-stream", Environment.NewLine, Environment.NewLine };
            string str4 = string.Concat(newLine);
            int length = checked(checked((int)LocalFileNameArray.Length) - 1);
            for (int i = 0; i <= length; i = checked(i + 1)) {
                string str5 = string.Format(str4, "content", this.MyFileSystemCaller.GetFileName(LocalFileNameArray[i]));
                byte[] bytes1 = Encoding.UTF8.GetBytes(str5);
                memoryStream.Write(bytes1, 0, checked((int)bytes1.Length));
                FileStream fileStream = this.MyFileSystemCaller.FileOpenRead(LocalFileNameArray[i]);
                byte[] numArray1 = new byte[1025];
                long num3 = (long)0;
                long length1 = fileStream.Length;
                while (length1 > (long)0) {
                    num3 = (long)fileStream.Read(numArray1, 0, checked((int)numArray1.Length));
                    length1 = checked(length1 - num3);
                    memoryStream.Write(numArray1, 0, checked((int)num3));
                }
                memoryStream.Write(bytes, 0, checked((int)bytes.Length));
                fileStream.Close();
            }
            this.InternalBytesTotal = memoryStream.Length;
            memoryStream.Position = (long)0;
            this.MyRequest.ServicePoint.ConnectionLimit = 50;
            this.MyRequest.AllowWriteStreamBuffering = false;
            this.MyRequest.AllowAutoRedirect = true;
            this.MyRequest.Timeout = checked(this.InternalTimeoutSeconds * 1000);
            this.MyRequest.ReadWriteTimeout = checked(this.InternalReadWriteTimeoutSeconds * 1000);
            this.MyRequest.Method = Method;
            this.MyRequest.ServicePoint.Expect100Continue = true;
            this.MyRequest.KeepAlive = false;
            this.MyRequest.ContentLength = this.InternalBytesTotal;
            this.MyRequest.Credentials = CredentialCache.DefaultCredentials;
            if (ExtraRequestHeaders != null) {
                string str6 = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
                try {
                    enumerator1 = ExtraRequestHeaders.GetEnumerator();
                    while (enumerator1.MoveNext()) {
                        KeyValuePair<string, string> current = enumerator1.Current;
                        int num4 = Strings.Len(current.Value);
                        int num5 = 1;
                        while (num5 <= num4) {
                            if (Strings.InStr(str6, Strings.Mid(current.Value, num5, 1), CompareMethod.Binary) != 0) {
                                num5 = checked(num5 + 1);
                            }
                            else {
                                this.InternalErrorNumber = 1001;
                                int internalErrorNumber = this.InternalErrorNumber;
                                newLine = new string[] { "Key=", current.Key, "   Value=", current.Value, "   Invalid Character=", Strings.Mid(current.Value, num5, 1) };
                                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(internalErrorNumber, string.Concat(newLine));
                                flag = false;
                                goto Label0;
                            }
                        }
                        if (Operators.CompareString(Strings.LCase(current.Key), "content-type", false) != 0) {
                            if (Operators.CompareString(Strings.LCase(current.Key), "user-agent", false) == 0) {
                                this.MyRequest.UserAgent = current.Value;
                            }
                            else if (Operators.CompareString(Strings.LCase(current.Key), "referer", false) == 0) {
                                this.MyRequest.Referer = current.Value;
                            }
                            else if (Operators.CompareString(Strings.LCase(current.Key), "accept", false) == 0) {
                                this.MyRequest.Accept = current.Value;
                            }
                            else if (Operators.CompareString(Strings.LCase(current.Key), "if-modified-since", false) == 0) {
                                this.MyRequest.IfModifiedSince = Conversions.ToDate(current.Value);
                            }
                            else if (Operators.CompareString(Strings.LCase(current.Key), "host", false) != 0) {
                                try {
                                    this.MyRequest.Headers[current.Key] = current.Value;
                                }
                                catch (Exception exception1) {
                                    ProjectData.SetProjectError(exception1);
                                    Exception exception = exception1;
                                    newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error adding request header   RequestHeader=", current.Key, ":", current.Value, "   Message=", exception.Message, "\r\n" };
                                    this.InternalLogData = string.Concat(newLine);
                                    this.InternalErrorNumber = 1002;
                                    int internalErrorNumber1 = this.InternalErrorNumber;
                                    newLine = new string[] { "Message=", exception.Message, "   Header Name=", current.Key, "   Header Value=", current.Value };
                                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(internalErrorNumber1, string.Concat(newLine));
                                    flag = false;
                                    ProjectData.ClearProjectError();
                                    goto Label0;
                                }
                            }
                        }
                    }
                }
                finally {
                    ((IDisposable)enumerator1).Dispose();
                }
            }
            if (Operators.CompareString(this.MyRequest.UserAgent, "", false) == 0) {
                this.MyRequest.UserAgent = string.Concat("SoftAWS/", this.Version);
            }
            if (!Information.IsNothing(this.InternalProxy)) {
                this.MyRequest.Proxy = this.InternalProxy;
                Uri proxy = this.InternalProxy.GetProxy(this.MyRequest.Address);
                if (this.MyRequest.Address != proxy) {
                    newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Proxy server set   Proxy.Address=", proxy.AbsoluteUri, "\r\n" };
                    this.InternalLogData = string.Concat(newLine);
                }
            }
            try {
                enumerator2 = this.MyRequest.Headers.GetEnumerator();
                while (enumerator2.MoveNext()) {
                    object obj = RuntimeHelpers.GetObjectValue(enumerator2.Current);
                    object obj1 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   RequestHeader="), obj), ":");
                    WebHeaderCollection headers = this.MyRequest.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj) };
                    this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj1, NewLateBinding.LateGet(headers, null, "Item", objectValue, null, null, null)), "\r\n"));
                    Dictionary<string, string> internalRequestHeaders = this.InternalRequestHeaders;
                    string str7 = Conversions.ToString(obj);
                    WebHeaderCollection webHeaderCollection = this.MyRequest.Headers;
                    objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj) };
                    internalRequestHeaders.Add(str7, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection, null, "Item", objectValue, null, null, null)));
                }
            }
            finally {
                if (enumerator2 is IDisposable) {
                    (enumerator2 as IDisposable).Dispose();
                }
            }
            try {
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the request stream\r\n");
                requestStream = this.MyRequest.GetRequestStream();
                this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the request stream was successful\r\n");
            }
            catch (Exception exception3) {
                ProjectData.SetProjectError(exception3);
                Exception exception2 = exception3;
                newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Opening the request stream failed   Message=", exception2.Message, "\r\n" };
                this.InternalLogData = string.Concat(newLine);
                this.InternalErrorNumber = 1004;
                this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception2.Message));
                flag = false;
                ProjectData.ClearProjectError();
                goto Label0;
            }
            flag1 = true;
            if (!this.MyRequest.HaveResponse) {
                byte[] numArray2 = new byte[checked(this.InternalBufferLength + 1)];
                long internalBytesTotal = this.InternalBytesTotal;
                long num6 = (long)0;
                ArrayList arrayLists = new ArrayList();
                ArrayList arrayLists1 = new ArrayList();
                DateTime now = DateTime.Now;
                arrayLists.Add(now.Ticks);
                arrayLists1.Add(this.InternalBytesTransfered);
                ArrayList arrayLists2 = new ArrayList();
                Label1:
                while (internalBytesTotal > (long)0) {
                    try {
                        num = (internalBytesTotal < (long)(checked((int)numArray2.Length)) ? memoryStream.Read(numArray2, 0, checked((int)internalBytesTotal)) : memoryStream.Read(numArray2, 0, checked((int)numArray2.Length)));
                    }
                    catch (Exception exception5) {
                        ProjectData.SetProjectError(exception5);
                        Exception exception4 = exception5;
                        try {
                            enumerator3 = arrayLists2.GetEnumerator();
                            while (enumerator3.MoveNext()) {
                                object objectValue1 = RuntimeHelpers.GetObjectValue(enumerator3.Current);
                                this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(this.InternalLogData, objectValue1), "\r\n"));
                            }
                        }
                        finally {
                            if (enumerator3 is IDisposable) {
                                (enumerator3 as IDisposable).Dispose();
                            }
                        }
                        newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Reading the source file failed   Message=", exception4.Message, "\r\n" };
                        this.InternalLogData = string.Concat(newLine);
                        this.InternalErrorNumber = 1121;
                        this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception4.Message));
                        flag = false;
                        ProjectData.ClearProjectError();
                        goto Label0;
                    }
                    if (num <= 0) {
                        try {
                            enumerator5 = arrayLists2.GetEnumerator();
                            while (enumerator5.MoveNext()) {
                                object objectValue2 = RuntimeHelpers.GetObjectValue(enumerator5.Current);
                                this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(this.InternalLogData, objectValue2), "\r\n"));
                            }
                        }
                        finally {
                            if (enumerator5 is IDisposable) {
                                (enumerator5 as IDisposable).Dispose();
                            }
                        }
                        this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Nothing to write.  Unexpected end of file.\r\n");
                        this.InternalErrorNumber = 1005;
                        this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, "");
                        flag = false;
                        goto Label0;
                    }
                    else {
                        try {
                            requestStream.Write(numArray2, 0, num);
                        }
                        catch (Exception exception7) {
                            ProjectData.SetProjectError(exception7);
                            Exception exception6 = exception7;
                            try {
                                enumerator4 = arrayLists2.GetEnumerator();
                                while (enumerator4.MoveNext()) {
                                    object obj2 = RuntimeHelpers.GetObjectValue(enumerator4.Current);
                                    this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(this.InternalLogData, obj2), "\r\n"));
                                }
                            }
                            finally {
                                if (enumerator4 is IDisposable) {
                                    (enumerator4 as IDisposable).Dispose();
                                }
                            }
                            newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Writing to the upload stream failed   Message=", exception6.Message, "\r\n" };
                            this.InternalLogData = string.Concat(newLine);
                            this.InternalErrorNumber = 1122;
                            this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception6.Message));
                            flag = false;
                            ProjectData.ClearProjectError();
                            goto Label0;
                        }
                        num6 = checked(num6 + (long)num);
                        this.InternalBytesTransfered = num6;
                        internalBytesTotal = checked(internalBytesTotal - (long)num);
                        now = DateTime.Now;
                        arrayLists.Add(now.Ticks);
                        arrayLists1.Add(this.InternalBytesTransfered);
                        Upload.ProgressChangedEventEventHandler progressChangedEventEventHandler = this.ProgressChangedEvent;
                        if (progressChangedEventEventHandler != null) {
                            progressChangedEventEventHandler();
                        }
                        arrayLists2.Add(string.Concat(Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Raising ProgressChangedEvent   BytesTransfered=", Conversions.ToString(this.InternalBytesTransfered)));
                        if (arrayLists2.Count == 31) {
                            arrayLists2[10] = "... The log data was truncated.  Only the first 10 and last 20 events were logged.  Monitor the ProgressChangedEvent for all values.";
                        }
                        else if (arrayLists2.Count > 31) {
                            arrayLists2.RemoveAt(11);
                        }
                        int count = checked(arrayLists.Count - 1);
                        int num7 = 0;
                        while (num7 <= count) {
                            num2 = Conversions.ToLong(arrayLists[num7]);
                            num1 = Conversions.ToLong(arrayLists1[num7]);
                            now = DateTime.Now;
                            TimeSpan timeSpan = new TimeSpan(Conversions.ToLong(Operators.SubtractObject(now.Ticks, arrayLists[num7])));
                            if (timeSpan.TotalSeconds >= 60) {
                                num7 = checked(num7 + 1);
                            }
                            else {
                                int num8 = checked(num7 - 1);
                                for (int j = 0; j <= num8; j = checked(j + 1)) {
                                    arrayLists.RemoveAt(0);
                                    arrayLists1.RemoveAt(0);
                                }
                                break;
                            }
                        }
                        long ticks = DateTime.Now.Ticks;
                        long num9 = Conversions.ToLong(arrayLists1[checked(arrayLists1.Count - 1)]);
                        TimeSpan timeSpan1 = new TimeSpan(checked(ticks - num2));
                        long num10 = checked(num9 - num1);
                        float totalSeconds = (float)((double)num10 / 1024 / timeSpan1.TotalSeconds);
                        if (this.InternalLimitKBpsSpeed <= 0f || totalSeconds <= this.InternalLimitKBpsSpeed) {
                            continue;
                        }
                        long num11 = checked((long)Math.Round((double)(checked(num10 + (long)this.InternalBufferLength)) / 1024 / (double)this.InternalLimitKBpsSpeed * 10000000 + (double)num2));
                        now = DateTime.Now;
                        float single = (float)((double)(checked(now.Ticks - num2)) / 10000000);
                        now = DateTime.Now;
                        float ticks1 = (float)((double)(checked(num11 - now.Ticks)) / 10000000);
                        while (DateTime.Now.Ticks < num11) {
                            if (this.KeepGoing) {
                                Thread.Sleep(50);
                            }
                            else {
                                goto Label1;
                            }
                        }
                    }
                }
                try {
                    enumerator6 = arrayLists2.GetEnumerator();
                    while (enumerator6.MoveNext()) {
                        object objectValue3 = RuntimeHelpers.GetObjectValue(enumerator6.Current);
                        this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(this.InternalLogData, objectValue3), "\r\n"));
                    }
                }
                finally {
                    if (enumerator6 is IDisposable) {
                        (enumerator6 as IDisposable).Dispose();
                    }
                }
            }
            else {
                flag = false;
            }
            Label0:
            if (requestStream != null) {
                try {
                    requestStream.Close();
                }
                catch (Exception exception8) {
                    ProjectData.SetProjectError(exception8);
                    ProjectData.ClearProjectError();
                }
                requestStream = null;
            }
            if (flag1) {
                try {
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response\r\n");
                    response = (HttpWebResponse)this.MyRequest.GetResponse();
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response was successful\r\n");
                }
                catch (WebException webException1) {
                    ProjectData.SetProjectError(webException1);
                    WebException webException = webException1;
                    newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response failed   Message=", webException.Message, "\r\n" };
                    this.InternalLogData = string.Concat(newLine);
                    this.InternalErrorNumber = 1003;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", webException.Message));
                    HttpWebResponse httpWebResponse = (HttpWebResponse)webException.Response;
                    if (!Information.IsNothing(httpWebResponse)) {
                        this.InternalResponseStatusCode = (int)httpWebResponse.StatusCode;
                        this.InternalResponseStatusDescription = httpWebResponse.StatusDescription;
                        newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusCode=", Conversions.ToString((int)httpWebResponse.StatusCode), "\r\n" };
                        this.InternalLogData = string.Concat(newLine);
                        newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusDescription=", httpWebResponse.StatusDescription, "\r\n" };
                        this.InternalLogData = string.Concat(newLine);
                        try {
                            enumerator7 = httpWebResponse.Headers.GetEnumerator();
                            while (enumerator7.MoveNext()) {
                                object obj3 = RuntimeHelpers.GetObjectValue(enumerator7.Current);
                                object obj4 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   ResponseHeader="), obj3), ":");
                                WebHeaderCollection headers1 = httpWebResponse.Headers;
                                objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj3) };
                                this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj4, NewLateBinding.LateGet(headers1, null, "Item", objectValue, null, null, null)), "\r\n"));
                                Dictionary<string, string> internalResponseHeaders = this.InternalResponseHeaders;
                                string str8 = Conversions.ToString(obj3);
                                WebHeaderCollection webHeaderCollection1 = httpWebResponse.Headers;
                                objectValue = new object[] { RuntimeHelpers.GetObjectValue(obj3) };
                                internalResponseHeaders.Add(str8, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection1, null, "Item", objectValue, null, null, null)));
                            }
                        }
                        finally {
                            if (enumerator7 is IDisposable) {
                                (enumerator7 as IDisposable).Dispose();
                            }
                        }
                        Stream responseStream = httpWebResponse.GetResponseStream();
                        try {
                            this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the error response stream\r\n");
                            string end = (new StreamReader(responseStream, true)).ReadToEnd();
                            this.InternalResponseString = end;
                            this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the error response stream was successful\r\n");
                            newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   StreamData=", end, "\r\n" };
                            this.InternalLogData = string.Concat(newLine);
                        }
                        catch (Exception exception10) {
                            ProjectData.SetProjectError(exception10);
                            Exception exception9 = exception10;
                            newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error getting the error response stream   Message=", exception9.Message, "\r\n" };
                            this.InternalLogData = string.Concat(newLine);
                            ProjectData.ClearProjectError();
                        }
                        httpWebResponse.Close();
                    }
                    flag = false;
                    this.MyRequest.Abort();
                    ProjectData.ClearProjectError();
                }
                catch (Exception exception12) {
                    ProjectData.SetProjectError(exception12);
                    Exception exception11 = exception12;
                    newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error getting the response   Message=", exception11.Message, "\r\n" };
                    this.InternalLogData = string.Concat(newLine);
                    flag = false;
                    this.InternalErrorNumber = 1003;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception11.Message));
                    this.MyRequest.Abort();
                    ProjectData.ClearProjectError();
                }
            }
            if (!Information.IsNothing(response)) {
                this.InternalResponseStatusCode = (int)response.StatusCode;
                this.InternalResponseStatusDescription = response.StatusDescription;
                newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusCode=", Conversions.ToString((int)response.StatusCode), "\r\n" };
                this.InternalLogData = string.Concat(newLine);
                newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ResponseStatusDescription=", response.StatusDescription, "\r\n" };
                this.InternalLogData = string.Concat(newLine);
                try {
                    enumerator8 = response.Headers.GetEnumerator();
                    while (enumerator8.MoveNext()) {
                        object objectValue4 = RuntimeHelpers.GetObjectValue(enumerator8.Current);
                        object obj5 = Operators.ConcatenateObject(Operators.ConcatenateObject(string.Concat(string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff")), "   ResponseHeader="), objectValue4), ":");
                        WebHeaderCollection headers2 = response.Headers;
                        objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue4) };
                        this.InternalLogData = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(obj5, NewLateBinding.LateGet(headers2, null, "Item", objectValue, null, null, null)), "\r\n"));
                        Dictionary<string, string> strs = this.InternalResponseHeaders;
                        string str9 = Conversions.ToString(objectValue4);
                        WebHeaderCollection webHeaderCollection2 = response.Headers;
                        objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue4) };
                        strs.Add(str9, Conversions.ToString(NewLateBinding.LateGet(webHeaderCollection2, null, "Item", objectValue, null, null, null)));
                    }
                }
                finally {
                    if (enumerator8 is IDisposable) {
                        (enumerator8 as IDisposable).Dispose();
                    }
                }
                Stream stream = response.GetResponseStream();
                try {
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response stream\r\n");
                    string end1 = (new StreamReader(stream, true)).ReadToEnd();
                    this.InternalLogData = string.Concat(this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Getting the response stream was successful\r\n");
                    newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   StreamData=", end1, "\r\n" };
                    this.InternalLogData = string.Concat(newLine);
                }
                catch (Exception exception14) {
                    ProjectData.SetProjectError(exception14);
                    Exception exception13 = exception14;
                    newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   Error getting the response stream   Message=", exception13.Message, "\r\n" };
                    this.InternalLogData = string.Concat(newLine);
                    flag = false;
                    this.InternalErrorNumber = 1006;
                    this.InternalErrorDescription = Shared_Module.GetErrorDescriptionFromErrorNumber(this.InternalErrorNumber, string.Concat("Message=", exception13.Message));
                    ProjectData.ClearProjectError();
                }
                response.Close();
            }
            newLine = new string[] { this.InternalLogData, Strings.Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss:ff"), "   ErrorNumber=", Conversions.ToString(this.InternalErrorNumber), "   ErrorDescription=", this.InternalErrorDescription, "\r\n" };
            this.InternalLogData = string.Concat(newLine);
            return flag;
        }

        public event Upload.ProgressChangedEventEventHandler ProgressChangedEvent;

        /// <summary>Fires when the value of the State property is changed.</summary>
        public event Upload.StateChangedEventEventHandler StateChangedEvent;

        public delegate void ProgressChangedEventEventHandler();

        public delegate void StateChangedEventEventHandler();
    }
}
