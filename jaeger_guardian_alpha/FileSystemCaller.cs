﻿using System.IO;
using System.Security;

namespace Jaeger.SoftAWS {
    public class FileSystemCaller {
        private bool UseWin32;

        private short MaxPath;

        public FileSystemCaller(bool AllowWin32) {
            this.UseWin32 = false;
            this.MaxPath = 248;
            this.UseWin32 = AllowWin32;
        }

        [SecurityCritical]
        public void FileDelete(string path) {
            if (path.Length < this.MaxPath || !this.UseWin32) {
                File.Delete(path);
            }
            else {
                File.Delete(path);
            }
        }

        [SecurityCritical]
        public bool FileExists(string path) {
            if (path.Length >= this.MaxPath && this.UseWin32) {
                return File.Exists(path);
            }
            return File.Exists(path);
        }

        [SecurityCritical]
        public long FileInfoLength(string path) {
            if (path.Length >= this.MaxPath && this.UseWin32) {
                return (new FileInfo(path)).Length;
            }
            return (new FileInfo(path)).Length;
        }

        [SecurityCritical]
        public FileStream FileOpen(string path, FileMode FileMode) {
            if (path.Length > this.MaxPath && this.UseWin32) {
                return File.Open(path, FileMode);
            }
            return File.Open(path, FileMode);
        }

        [SecurityCritical]
        public FileStream FileOpen(string path, FileMode FileMode, FileAccess FileAccess, FileShare FileShare) {
            if (path.Length < this.MaxPath || !this.UseWin32) {
                return File.Open(path, FileMode, FileAccess);
            }
            return File.Open(path, FileMode, FileAccess, FileShare);
        }

        [SecurityCritical]
        public FileStream FileOpenRead(string path) {
            if (path.Length >= this.MaxPath && this.UseWin32) {
                return File.OpenRead(path);
            }
            return File.OpenRead(path);
        }

        [SecurityCritical]
        public string GetFileName(string path) {
            if (path.Length >= this.MaxPath && this.UseWin32) {
                return Path.GetFileName(path);
            }
            return Path.GetFileName(path);
        }
    }
}
