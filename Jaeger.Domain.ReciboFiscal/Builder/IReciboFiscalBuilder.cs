﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.ReciboFiscal.Builder {
    public interface IReciboFiscalBuilder : IConditionalBuilder, IConditionalBuild {
        IReciboFiscalYearBuilder Year(int year);
    }

    public interface IReciboFiscalYearBuilder {
        IReciboFiscalMonthBuilder Month(int month);
    }

    public interface IReciboFiscalMonthBuilder {

    }

    public class ReciboFiscalBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IReciboFiscalBuilder, IReciboFiscalYearBuilder, IReciboFiscalMonthBuilder {
        public ReciboFiscalBuilder() : base() { }

        public IReciboFiscalYearBuilder Year(int month) {
            return this;
        }

        public IReciboFiscalMonthBuilder Month(int month) {
            return this;
        }
    }
}
