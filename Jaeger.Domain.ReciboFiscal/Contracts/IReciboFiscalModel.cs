﻿using System;
using Jaeger.Domain.ReciboFiscal.Entities;

namespace Jaeger.Domain.ReciboFiscal.Contracts {
    public interface IReciboFiscalModel {
        AddendaReciboFiscalBoleta Addenda {
            get;
            set;
        }
        string Consecutivo {
            get;
        }
        double CuotaAutomovil {
            get;
        }
        double CuotaGastosAdmon {
            get;
        }
        double CuotaIva {
            get;
        }
        double CuotaSeguroVida {
            get;
        }
        int Dia {
            get;
        }
        string Emisor {
            get;
            set;
        }
        string EmisorRFC {
            get;
            set;
        }
        string Estado {
            get;
            set;
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        DateTime? FechaCancela {
            get;
            set;
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// </summary>
        DateTime? FechaEmision {
            get;
            set;
        }

        /// <summary>
        /// fecha de certificación
        /// </summary>
        DateTime? FechaTimbre {
            get;
            set;
        }
        string Folio {
            get;
            set;
        }
        int Id {
            get;
            set;
        }

        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        string IdDocumento {
            get;
            set;
        }
        decimal Iva {
            get;
            set;
        }
        string JSon {
            set;
        }
        string Mensualidad {
            get;
        }
        int NoPlan {
            get;
        }
        string NoPlanText {
            get;
        }
        decimal Otros {
            get;
        }
        string Receptor {
            get;
            set;
        }
        string ReceptorRfc {
            get;
            set;
        }
        decimal SeguroAuto {
            get;
        }
        string Serie {
            get;
            set;
        }
        decimal SubTotal {
            get;
            set;
        }
        decimal Total {
            get;
            set;
        }
        decimal TotalPagado {
            get;
        }

        string Json();
        void Json(string strJson);
    }
}