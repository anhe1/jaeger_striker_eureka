﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.ReciboFiscal.Entities;

namespace Jaeger.Domain.ReciboFiscal.Contracts {
    public interface ISqlReciboFiscalRepository : IGenericRepository<ReciboFiscalModel> {
        /// <summary>
        /// listado de recibos fiscales por mes año y la verifcacion del emisor del RFC, solo funciona con la serie RF
        /// </summary>
        /// <param name="mes">mes</param>
        /// <param name="anio">año</param>
        /// <param name="rfc">rfc</param>
        IEnumerable<ReciboFiscalModel> GetRecibosFiscales(int mes, int anio, int dia, string rfc);
    }
}
