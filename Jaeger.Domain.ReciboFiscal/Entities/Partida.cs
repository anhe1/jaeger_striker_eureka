﻿using System;

namespace Jaeger.Domain.ReciboFiscal.Entities {
    public partial class Partida : ICloneable {
        public Partida() {
            this.Campo1 = "";
            this.Campo2 = "";
            this.Campo3 = "";
            this.Campo4 = "";
            this.Campo5 = "";
            this.Campo6 = "";
            this.Campo7 = "";
            this.Campo8 = "";
            this.Campo9 = "";
        }

        public string Campo1 { get; set; }
        public string Campo2 { get; set; }
        public string Campo3 { get; set; }
        public string Campo4 { get; set; }
        public string Campo5 { get; set; }
        public string Campo6 { get; set; }
        public string Campo7 { get; set; }
        public string Campo8 { get; set; }
        public string Campo9 { get; set; }

        public override string ToString() {
            return $"\"{this.Campo1}\",\"{this.Campo2}\",\"{this.Campo3}\",\"{this.Campo4}\",\"{this.Campo5}\",\"{this.Campo6}\",\"{this.Campo7}\",\"{this.Campo8}\",\"{this.Campo9}\"";
        }

        public object Clone() {
            return this.MemberwiseClone();
        }
    }
}
