﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.ReciboFiscal.Entities {
    public class Parametros : BasePropertyChangeImplementation {
        private string fieldField;
        private string labelField;
        private string typeField;
        private string valueField;

        public Parametros() {
        }

        [JsonProperty("field")]
        public string Field {
            get {
                return this.fieldField;
            }
            set {
                this.fieldField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("label")]
        public string Label {
            get {
                return this.labelField;
            }
            set {
                this.labelField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("type")]
        public string Type {
            get {
                return this.typeField;
            }
            set {
                this.typeField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("value")]
        public string Value {
            get {
                return this.valueField;
            }
            set {
                this.valueField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
