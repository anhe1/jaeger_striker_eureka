﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.ReciboFiscal.Entities {
    [JsonObject]
    public class AddendaAfasaBoletaPago : BasePropertyChangeImplementation {
        private string nameField;
        private List<Parametros> listaField;

        public AddendaAfasaBoletaPago() {
        }

        [JsonProperty("params")]
        public List<Parametros> Items {
            get {
                return this.listaField;
            }
            set {
                this.listaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("name")]
        public string Name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
                this.OnPropertyChanged("Name");
            }
        }


        public string Json() {
            return JsonConvert.SerializeObject(this);
        }

        public static AddendaAfasaBoletaPago Json(string jsonString) {
            AddendaAfasaBoletaPago json;
            try {
                json = JsonConvert.DeserializeObject<AddendaAfasaBoletaPago>(jsonString);
            } catch (Exception e) {
                Console.WriteLine("Addenda: " + e.Message);
                json = null;
            }
            return json;
        }
    }
}