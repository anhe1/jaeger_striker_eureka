﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Validador.Builder {
    public interface IValidadorGridViewBuilder : IGridViewBuilder, IGridViewColumnsBuild, IGridViewTempleteBuild {
        IValidadorTempleteGridViewBuilder Templetes();
    }

    public interface IValidadorColumnsGridViewBuilder : IGridViewBuilder, IGridViewColumnsBuild {
        #region columnas
        IValidadorColumnsGridViewBuilder Id();

        IValidadorColumnsGridViewBuilder TipoComprobante();

        IValidadorColumnsGridViewBuilder Version();

        IValidadorColumnsGridViewBuilder MetaData();

        IValidadorColumnsGridViewBuilder Folio();

        IValidadorColumnsGridViewBuilder Serie();

        IValidadorColumnsGridViewBuilder EmisorRFC();

        IValidadorColumnsGridViewBuilder EmisorNombre();

        IValidadorColumnsGridViewBuilder ReceptorRFC();

        IValidadorColumnsGridViewBuilder ReceptorNombre();

        IValidadorColumnsGridViewBuilder FechaEmision();

        IValidadorColumnsGridViewBuilder FechaTimbre();

        IValidadorColumnsGridViewBuilder IdDocumento();

        IValidadorColumnsGridViewBuilder NoCertificado();

        IValidadorColumnsGridViewBuilder RFCProvCertif();

        IValidadorColumnsGridViewBuilder Estado();

        IValidadorColumnsGridViewBuilder LugarExpedicion();

        IValidadorColumnsGridViewBuilder UsoCFDI();

        IValidadorColumnsGridViewBuilder MetodoPago();

        IValidadorColumnsGridViewBuilder FormaPago();

        IValidadorColumnsGridViewBuilder MetodoVsForma();

        IValidadorColumnsGridViewBuilder SubTotal();

        IValidadorColumnsGridViewBuilder Descuento();

        IValidadorColumnsGridViewBuilder RetencionISR();

        IValidadorColumnsGridViewBuilder RetencionIVA();

        IValidadorColumnsGridViewBuilder RetencionIEPS();


        IValidadorColumnsGridViewBuilder TrasladoIVA();

        IValidadorColumnsGridViewBuilder TrasladoIEPS();

        IValidadorColumnsGridViewBuilder Total();

        IValidadorColumnsGridViewBuilder PathXML();

        IValidadorColumnsGridViewBuilder PathPDF();

        IValidadorColumnsGridViewBuilder Situacion();

        IValidadorColumnsGridViewBuilder ClaveUnidad();

        IValidadorColumnsGridViewBuilder ClaveConceptos();

        IValidadorColumnsGridViewBuilder Complementos();

        IValidadorColumnsGridViewBuilder Resultado();

        IValidadorColumnsGridViewBuilder Registrado();
        #endregion
    }

    public interface IValidadorTempleteGridViewBuilder : IGridViewTempleteBuild, IValidadorColumnsGridViewBuilder {
        IValidadorTempleteGridViewBuilder Master();

        IConceptoTempleteGridViewBuilder Conceptos();
    }
}
