﻿using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Validador.Builder {
    public class ConceptoTempleteGridViewBuilder : GridViewBuilder, IGridViewBuilder, IGridViewColumnsBuild, IGridViewTempleteBuild, IConceptoTempleteGridViewBuilder, IConceptoTempletesGridViewBuilder,
         IConceptoColumnsGridViewBuilder, IConceptoParteTempletesGridViewBuilder {
        public ConceptoTempleteGridViewBuilder() : base() { }

        public IConceptoTempletesGridViewBuilder Master() {
            this.Cantidad().ClaveUnidad().Unidad().NoIdentificacion().ClaveProducto().Descripcion().ObjetoImp().ValorUnitario().Descuento().Importe();
            return this;
        }

        public IConceptoParteTempletesGridViewBuilder ConceptoParte() {
            this._Columns.Clear();
            this.Cantidad().Unidad().ClaveUnidad().NoIdentificacion().ClaveProducto().Descripcion().ValorUnitario().Importe();
            return this;
        }

        #region conceptos
        public IConceptoColumnsGridViewBuilder Cantidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Cantidad",
                FormatString = "{0:n}",
                HeaderText = "Cantidad",
                Name = "Cantidad",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 70
            });
            return this;
        }

        public IConceptoColumnsGridViewBuilder ClaveUnidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveUnidad",
                HeaderText = "Clv. Unidad",
                Name = "ClaveUnidad",
                Width = 75
            });
            return this;
        }

        public IConceptoColumnsGridViewBuilder Unidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Unidad",
                HeaderText = "Unidad",
                Name = "Unidad",
                Width = 65
            });
            return this;
        }

        public IConceptoColumnsGridViewBuilder NoIdentificacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "NoIdentificacion",
                HeaderText = "No. Ident.",
                Name = "NoIdentificacion",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 90
            });
            return this;
        }

        public IConceptoColumnsGridViewBuilder ObjetoImp() {
            this._Columns.Add(new GridViewMultiComboBoxColumn {
                DisplayMember = "Id",
                FieldName = "ObjetoImp",
                HeaderText = "Obj. Imp.",
                Name = "ObjetoImp",
                TextAlignment = ContentAlignment.MiddleCenter,
                ValueMember = "Id"
            });
            return this;
        }

        public IConceptoColumnsGridViewBuilder ClaveProducto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveProducto",
                HeaderText = "Clv. Prod.",
                Name = "ClaveProducto",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IConceptoColumnsGridViewBuilder Descripcion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Descripcion",
                HeaderText = "Concepto",
                Name = "Descripcion",
                Width = 285
            });
            return this;
        }

        public IConceptoColumnsGridViewBuilder ValorUnitario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "ValorUnitario",
                FormatString = "{0:N2}",
                HeaderText = "V. Unitario",
                Name = "ValorUnitario",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IConceptoColumnsGridViewBuilder Importe() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Importe",
                FormatString = "{0:N2}",
                HeaderText = "Importe",
                Name = "Importe",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IConceptoColumnsGridViewBuilder Descuento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Descuento",
                FormatString = "{0:N2}",
                HeaderText = "Descuento",
                Name = "Descuento",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }
        #endregion
    }
}
