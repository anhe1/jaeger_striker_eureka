﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Validador.Builder {
    /// <summary>
    /// builder para templete de conceptos
    /// </summary>
    public interface IConceptoTempleteGridViewBuilder : IGridViewBuilder, IGridViewColumnsBuild, IGridViewTempleteBuild, IConceptoParteTempletesGridViewBuilder {
        IConceptoTempletesGridViewBuilder Master();
    }

    public interface IConceptoTempletesGridViewBuilder : IGridViewBuilder, IConceptoColumnsGridViewBuilder {
        IConceptoParteTempletesGridViewBuilder ConceptoParte();
    }

    public interface IConceptoParteTempletesGridViewBuilder : IGridViewBuilder, IConceptoColumnsGridViewBuilder {
    }

    public interface IConceptoColumnsGridViewBuilder : IGridViewColumnsBuild {
        IConceptoColumnsGridViewBuilder Cantidad();
        IConceptoColumnsGridViewBuilder ClaveUnidad();
        IConceptoColumnsGridViewBuilder Unidad();
        IConceptoColumnsGridViewBuilder NoIdentificacion();
        IConceptoColumnsGridViewBuilder ObjetoImp();
        IConceptoColumnsGridViewBuilder ClaveProducto();
        IConceptoColumnsGridViewBuilder Descripcion();
        IConceptoColumnsGridViewBuilder ValorUnitario();
        IConceptoColumnsGridViewBuilder Importe();
        IConceptoColumnsGridViewBuilder Descuento();
    }
}
