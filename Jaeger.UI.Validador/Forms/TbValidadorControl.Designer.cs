﻿
namespace Jaeger.UI.Validador.Forms {
    partial class TbValidadorControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCommandColumn gridViewCommandColumn1 = new Telerik.WinControls.UI.GridViewCommandColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.RadCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.TCarpeta = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.ToolBarRowElement = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.lblCarpeta = new Telerik.WinControls.UI.CommandBarLabel();
            this.HostCarpeta = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Separador1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Carpetas = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.IncluirSubCarpetas = new Telerik.WinControls.UI.RadMenuItem();
            this.IncluirArchivoZIP = new Telerik.WinControls.UI.RadMenuItem();
            this.AbrirArchivoZIP = new Telerik.WinControls.UI.RadMenuItem();
            this.TDetener = new Telerik.WinControls.UI.CommandBarButton();
            this.Separador2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.TAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.TRemover = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.TRemoverTodo = new Telerik.WinControls.UI.RadMenuItem();
            this.TRemoverSeleccionado = new Telerik.WinControls.UI.RadMenuItem();
            this.TActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.TFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.TValidar = new Telerik.WinControls.UI.CommandBarButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.TValidacionSelect = new Telerik.WinControls.UI.RadMenuItem();
            this.TValidacionPDF = new Telerik.WinControls.UI.RadMenuItem();
            this.Herramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.TConfiguracion = new Telerik.WinControls.UI.RadMenuItem();
            this.TExportar = new Telerik.WinControls.UI.RadMenuItem();
            this.TGuardarEn = new Telerik.WinControls.UI.RadMenuItem();
            this.TExportarExcel = new Telerik.WinControls.UI.RadMenuItem();
            this.TExportarTemplete = new Telerik.WinControls.UI.RadMenuItem();
            this.TExportarPlantilla = new Telerik.WinControls.UI.RadMenuItem();
            this.TBackup = new Telerik.WinControls.UI.RadMenuItem();
            this.BakcupAutomatico = new Telerik.WinControls.UI.RadMenuItem();
            this.TBackupTodo = new Telerik.WinControls.UI.RadMenuItem();
            this.TBackupSeleccionado = new Telerik.WinControls.UI.RadMenuItem();
            this.TLogError = new Telerik.WinControls.UI.RadMenuItem();
            this.TCatalogo = new Telerik.WinControls.UI.RadMenuItem();
            this.TUpdate = new Telerik.WinControls.UI.RadMenuItem();
            this.TInformacion = new Telerik.WinControls.UI.RadMenuItem();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).BeginInit();
            this.RadCommandBar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TCarpeta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TCarpeta.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TCarpeta.EditorControl.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // RadCommandBar1
            // 
            this.RadCommandBar1.Controls.Add(this.TCarpeta);
            this.RadCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar1.Name = "RadCommandBar1";
            this.RadCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.ToolBarRowElement});
            this.RadCommandBar1.Size = new System.Drawing.Size(1213, 55);
            this.RadCommandBar1.TabIndex = 5;
            // 
            // TCarpeta
            // 
            this.TCarpeta.AutoSizeDropDownToBestFit = true;
            // 
            // TCarpeta.NestedRadGridView
            // 
            this.TCarpeta.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.TCarpeta.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TCarpeta.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.TCarpeta.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.TCarpeta.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.TCarpeta.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.TCarpeta.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Carpeta";
            gridViewTextBoxColumn1.HeaderText = "Carpeta";
            gridViewTextBoxColumn1.Name = "Carpeta";
            gridViewTextBoxColumn1.Width = 200;
            gridViewCommandColumn1.FieldName = "Accion";
            gridViewCommandColumn1.HeaderText = "Acción";
            gridViewCommandColumn1.Name = "Remover";
            gridViewCommandColumn1.Width = 85;
            this.TCarpeta.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewCommandColumn1});
            this.TCarpeta.EditorControl.MasterTemplate.EnableGrouping = false;
            this.TCarpeta.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.TCarpeta.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.TCarpeta.EditorControl.Name = "NestedRadGridView";
            this.TCarpeta.EditorControl.ReadOnly = true;
            this.TCarpeta.EditorControl.ShowGroupPanel = false;
            this.TCarpeta.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.TCarpeta.EditorControl.TabIndex = 0;
            this.TCarpeta.Location = new System.Drawing.Point(63, 5);
            this.TCarpeta.Name = "TCarpeta";
            this.TCarpeta.NullText = "Selecciona";
            this.TCarpeta.Size = new System.Drawing.Size(239, 20);
            this.TCarpeta.TabIndex = 6;
            this.TCarpeta.TabStop = false;
            // 
            // ToolBarRowElement
            // 
            this.ToolBarRowElement.MinSize = new System.Drawing.Size(25, 25);
            this.ToolBarRowElement.Name = "ToolBarRowElement";
            this.ToolBarRowElement.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            this.ToolBarRowElement.Text = "";
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "Emision de Comprobante";
            this.ToolBar.EnableFocusBorder = false;
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.lblCarpeta,
            this.HostCarpeta,
            this.Separador1,
            this.Carpetas,
            this.TDetener,
            this.Separador2,
            this.TAgregar,
            this.TRemover,
            this.TActualizar,
            this.TFiltro,
            this.TValidar,
            this.Imprimir,
            this.Herramientas,
            this.Cerrar});
            this.ToolBar.Name = "ToolBar";
            // 
            // 
            // 
            this.ToolBar.OverflowButton.Enabled = true;
            this.ToolBar.ShowHorizontalLine = false;
            this.ToolBar.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.ToolBar.GetChildAt(2))).Enabled = true;
            // 
            // lblCarpeta
            // 
            this.lblCarpeta.DisplayName = "Etiqueta: Carpeta";
            this.lblCarpeta.Name = "lblCarpeta";
            this.lblCarpeta.Text = "Carpeta:";
            // 
            // HostCarpeta
            // 
            this.HostCarpeta.DisplayName = "H.Carpeta";
            this.HostCarpeta.MaxSize = new System.Drawing.Size(0, 0);
            this.HostCarpeta.MinSize = new System.Drawing.Size(260, 0);
            this.HostCarpeta.Name = "HostCarpeta";
            this.HostCarpeta.Text = "commandBarHostItem1";
            // 
            // Separador1
            // 
            this.Separador1.DisplayName = "Separador 1";
            this.Separador1.Name = "Separador1";
            this.Separador1.VisibleInOverflowMenu = false;
            // 
            // Carpetas
            // 
            this.Carpetas.DefaultItem = null;
            this.Carpetas.DisplayName = "Carpetas";
            this.Carpetas.DrawText = false;
            this.Carpetas.Image = global::Jaeger.UI.Validador.Properties.Resources.browse_folder_16px;
            this.Carpetas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.IncluirSubCarpetas,
            this.IncluirArchivoZIP,
            this.AbrirArchivoZIP});
            this.Carpetas.Name = "Carpetas";
            this.Carpetas.Text = "Carpeta";
            // 
            // IncluirSubCarpetas
            // 
            this.IncluirSubCarpetas.CheckOnClick = true;
            this.IncluirSubCarpetas.Name = "IncluirSubCarpetas";
            this.IncluirSubCarpetas.Text = "Incluir sub carpetas";
            // 
            // IncluirArchivoZIP
            // 
            this.IncluirArchivoZIP.CheckOnClick = true;
            this.IncluirArchivoZIP.Name = "IncluirArchivoZIP";
            this.IncluirArchivoZIP.Text = "Incluir archivos ZIP";
            // 
            // AbrirArchivoZIP
            // 
            this.AbrirArchivoZIP.Name = "AbrirArchivoZIP";
            this.AbrirArchivoZIP.Text = "Abrir archivo ZIP";
            // 
            // TDetener
            // 
            this.TDetener.DisplayName = "Detener";
            this.TDetener.DrawText = false;
            this.TDetener.Image = global::Jaeger.UI.Validador.Properties.Resources.cancel_16px;
            this.TDetener.Name = "TDetener";
            this.TDetener.Text = "commandBarButton1";
            this.TDetener.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Separador2
            // 
            this.Separador2.DisplayName = "Separador 2";
            this.Separador2.Name = "Separador2";
            this.Separador2.VisibleInOverflowMenu = false;
            // 
            // TAgregar
            // 
            this.TAgregar.DisplayName = "Agregar";
            this.TAgregar.DrawText = true;
            this.TAgregar.Image = global::Jaeger.UI.Validador.Properties.Resources.add_16px;
            this.TAgregar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.TAgregar.Name = "TAgregar";
            this.TAgregar.Text = "Agregar";
            this.TAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TRemover
            // 
            this.TRemover.DisplayName = "Remover";
            this.TRemover.DrawText = true;
            this.TRemover.Image = global::Jaeger.UI.Validador.Properties.Resources.delete_16px;
            this.TRemover.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.TRemover.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.TRemoverTodo,
            this.TRemoverSeleccionado});
            this.TRemover.Name = "TRemover";
            this.TRemover.Text = "Remover";
            this.TRemover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TRemoverTodo
            // 
            this.TRemoverTodo.Name = "TRemoverTodo";
            this.TRemoverTodo.Text = "Todo";
            // 
            // TRemoverSeleccionado
            // 
            this.TRemoverSeleccionado.Name = "TRemoverSeleccionado";
            this.TRemoverSeleccionado.Text = "Seleccionado";
            // 
            // TActualizar
            // 
            this.TActualizar.DisplayName = "Actualizar";
            this.TActualizar.DrawText = true;
            this.TActualizar.Image = global::Jaeger.UI.Validador.Properties.Resources.refresh_16px;
            this.TActualizar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.TActualizar.Name = "TActualizar";
            this.TActualizar.Text = "Actualizar";
            this.TActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TFiltro
            // 
            this.TFiltro.DisplayName = "Filtro";
            this.TFiltro.DrawText = true;
            this.TFiltro.Image = global::Jaeger.UI.Validador.Properties.Resources.filter_16px;
            this.TFiltro.Name = "TFiltro";
            this.TFiltro.Text = "Filtro";
            this.TFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TValidar
            // 
            this.TValidar.DisplayName = "Validar";
            this.TValidar.DrawText = true;
            this.TValidar.Image = global::Jaeger.UI.Validador.Properties.Resources.protect_16px;
            this.TValidar.Name = "TValidar";
            this.TValidar.Text = "Validar";
            this.TValidar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Imprimir
            // 
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Image = global::Jaeger.UI.Validador.Properties.Resources.print_16px;
            this.Imprimir.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.TValidacionSelect,
            this.TValidacionPDF});
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TValidacionSelect
            // 
            this.TValidacionSelect.Name = "TValidacionSelect";
            this.TValidacionSelect.Text = "Seleccionado";
            // 
            // TValidacionPDF
            // 
            this.TValidacionPDF.Name = "TValidacionPDF";
            this.TValidacionPDF.Text = "Descargar PDF";
            // 
            // Herramientas
            // 
            this.Herramientas.DisplayName = "Herramientas";
            this.Herramientas.DrawText = true;
            this.Herramientas.Image = global::Jaeger.UI.Validador.Properties.Resources.toolbox_16px;
            this.Herramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.TConfiguracion,
            this.TExportar,
            this.TBackup,
            this.TLogError,
            this.TCatalogo});
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Text = "Herramientas";
            this.Herramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TConfiguracion
            // 
            this.TConfiguracion.Image = global::Jaeger.UI.Validador.Properties.Resources.settings_16;
            this.TConfiguracion.Name = "TConfiguracion";
            this.TConfiguracion.Text = "Configuración";
            // 
            // TExportar
            // 
            this.TExportar.Image = global::Jaeger.UI.Validador.Properties.Resources.xls_16px;
            this.TExportar.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.TGuardarEn,
            this.TExportarExcel,
            this.TExportarTemplete,
            this.TExportarPlantilla});
            this.TExportar.Name = "TExportar";
            this.TExportar.Text = "Exportar";
            // 
            // TGuardarEn
            // 
            this.TGuardarEn.Image = global::Jaeger.UI.Validador.Properties.Resources.save_16px;
            this.TGuardarEn.Name = "TGuardarEn";
            this.TGuardarEn.Text = "Guardar en ...";
            // 
            // TExportarExcel
            // 
            this.TExportarExcel.Image = global::Jaeger.UI.Validador.Properties.Resources.xls_16px;
            this.TExportarExcel.Name = "TExportarExcel";
            this.TExportarExcel.Text = "Excel";
            // 
            // TExportarTemplete
            // 
            this.TExportarTemplete.Name = "TExportarTemplete";
            this.TExportarTemplete.Text = "Por plantilla";
            // 
            // TExportarPlantilla
            // 
            this.TExportarPlantilla.Name = "TExportarPlantilla";
            this.TExportarPlantilla.Text = "Descargar plantilla";
            // 
            // TBackup
            // 
            this.TBackup.Image = global::Jaeger.UI.Validador.Properties.Resources.data_protection_16px;
            this.TBackup.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.BakcupAutomatico,
            this.TBackupTodo,
            this.TBackupSeleccionado});
            this.TBackup.Name = "TBackup";
            this.TBackup.Text = "Backup";
            // 
            // BakcupAutomatico
            // 
            this.BakcupAutomatico.Name = "BakcupAutomatico";
            this.BakcupAutomatico.Text = "Automático";
            // 
            // TBackupTodo
            // 
            this.TBackupTodo.Name = "TBackupTodo";
            this.TBackupTodo.Text = "Todos";
            // 
            // TBackupSeleccionado
            // 
            this.TBackupSeleccionado.Name = "TBackupSeleccionado";
            this.TBackupSeleccionado.Text = "Seleccionado";
            // 
            // TLogError
            // 
            this.TLogError.Image = global::Jaeger.UI.Validador.Properties.Resources.warning_shield_16px;
            this.TLogError.Name = "TLogError";
            this.TLogError.Text = "Log de errores";
            // 
            // TCatalogo
            // 
            this.TCatalogo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.TUpdate,
            this.TInformacion});
            this.TCatalogo.Name = "TCatalogo";
            this.TCatalogo.Text = "Catálogos";
            // 
            // TUpdate
            // 
            this.TUpdate.Image = global::Jaeger.UI.Validador.Properties.Resources.available_updates_16px;
            this.TUpdate.Name = "TUpdate";
            this.TUpdate.Text = "Buscar actualización";
            // 
            // TInformacion
            // 
            this.TInformacion.Name = "TInformacion";
            this.TInformacion.Text = "Información";
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Validador.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TbValidadorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.RadCommandBar1);
            this.MinimumSize = new System.Drawing.Size(0, 30);
            this.Name = "TbValidadorControl";
            this.Size = new System.Drawing.Size(1213, 30);
            this.Load += new System.EventHandler(this.TbValidadorControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).EndInit();
            this.RadCommandBar1.ResumeLayout(false);
            this.RadCommandBar1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TCarpeta.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TCarpeta.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TCarpeta)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar1;
        internal Telerik.WinControls.UI.CommandBarRowElement ToolBarRowElement;
        internal Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarLabel lblCarpeta;
        private Telerik.WinControls.UI.CommandBarHostItem HostCarpeta;
        private Telerik.WinControls.UI.CommandBarSeparator Separador1;
        protected internal Telerik.WinControls.UI.CommandBarButton TDetener;
        private Telerik.WinControls.UI.CommandBarSeparator Separador2;
        protected internal Telerik.WinControls.UI.CommandBarButton TAgregar;
        protected internal Telerik.WinControls.UI.CommandBarDropDownButton TRemover;
        protected internal Telerik.WinControls.UI.CommandBarButton TActualizar;
        protected internal Telerik.WinControls.UI.CommandBarToggleButton TFiltro;
        protected internal Telerik.WinControls.UI.CommandBarButton TValidar;
        protected internal Telerik.WinControls.UI.CommandBarDropDownButton Imprimir;
        protected internal Telerik.WinControls.UI.CommandBarButton Cerrar;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox TCarpeta;
        protected internal Telerik.WinControls.UI.CommandBarSplitButton Carpetas;
        protected internal Telerik.WinControls.UI.RadMenuItem TConfiguracion;
        protected internal Telerik.WinControls.UI.RadMenuItem TExportar;
        protected internal Telerik.WinControls.UI.RadMenuItem IncluirSubCarpetas;
        protected internal Telerik.WinControls.UI.RadMenuItem IncluirArchivoZIP;
        protected internal Telerik.WinControls.UI.RadMenuItem AbrirArchivoZIP;
        protected internal Telerik.WinControls.UI.RadMenuItem TRemoverTodo;
        protected internal Telerik.WinControls.UI.RadMenuItem BakcupAutomatico;
        protected internal Telerik.WinControls.UI.RadMenuItem TBackupTodo;
        protected internal Telerik.WinControls.UI.RadMenuItem TBackupSeleccionado;
        protected internal Telerik.WinControls.UI.RadMenuItem TRemoverSeleccionado;
        public Telerik.WinControls.UI.CommandBarDropDownButton Herramientas;
        protected internal Telerik.WinControls.UI.RadMenuItem TGuardarEn;
        protected internal Telerik.WinControls.UI.RadMenuItem TValidacionPDF;
        private Telerik.WinControls.UI.RadMenuItem TBackup;
        protected internal Telerik.WinControls.UI.RadMenuItem TLogError;
        protected internal Telerik.WinControls.UI.RadMenuItem TValidacionSelect;
        protected internal Telerik.WinControls.UI.RadMenuItem TExportarExcel;
        protected internal Telerik.WinControls.UI.RadMenuItem TExportarTemplete;
        protected internal Telerik.WinControls.UI.RadMenuItem TExportarPlantilla;
        protected internal Telerik.WinControls.UI.RadMenuItem TInformacion;
        protected internal Telerik.WinControls.UI.RadMenuItem TUpdate;
        protected internal Telerik.WinControls.UI.RadMenuItem TCatalogo;
    }
}
