﻿
namespace Jaeger.UI.Validador.Forms {
    partial class ComprobanteFiscalForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComprobanteFiscalForm));
            this.BValida = new Telerik.WinControls.UI.RadStatusStrip();
            this.lblProgreso = new Telerik.WinControls.UI.RadLabelElement();
            this.gridDocumentos = new Telerik.WinControls.UI.RadGridView();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.rotatingRingsWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.RotatingRingsWaitingBarIndicatorElement();
            this.gridConceptos = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridCFDIRelacionado = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridCFDIRelacionadoDocumento = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridRecepcionPago = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridRecepcionPagoD = new Telerik.WinControls.UI.GridViewTemplate();
            this.Iconos = new System.Windows.Forms.ImageList(this.components);
            this.ContextualMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.ContextMenuCopiar = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuRemover = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuValidar = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuEstadoSAT = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuArchivos = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextArchivoPDFBuscar = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextArchivoPDF = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextArchivoAcusePDF = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextArchivoAcuseXML = new Telerik.WinControls.UI.RadMenuItem();
            this.TValida = new Jaeger.UI.Validador.Forms.TbValidadorControl();
            ((System.ComponentModel.ISupportInitialize)(this.BValida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDocumentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDocumentos.MasterTemplate)).BeginInit();
            this.gridDocumentos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCFDIRelacionado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCFDIRelacionadoDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRecepcionPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRecepcionPagoD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // BValida
            // 
            this.BValida.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.lblProgreso});
            this.BValida.Location = new System.Drawing.Point(0, 424);
            this.BValida.Name = "BValida";
            this.BValida.Size = new System.Drawing.Size(1279, 26);
            this.BValida.SizingGrip = false;
            this.BValida.TabIndex = 2;
            // 
            // lblProgreso
            // 
            this.lblProgreso.Name = "lblProgreso";
            this.BValida.SetSpring(this.lblProgreso, false);
            this.lblProgreso.Text = "Listo!";
            this.lblProgreso.TextWrap = true;
            this.lblProgreso.UseCompatibleTextRendering = false;
            // 
            // gridDocumentos
            // 
            this.gridDocumentos.Controls.Add(this.Espera);
            this.gridDocumentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDocumentos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridDocumentos.MasterTemplate.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.gridConceptos,
            this.gridCFDIRelacionado,
            this.gridRecepcionPago});
            this.gridDocumentos.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.gridDocumentos.Name = "gridDocumentos";
            this.gridDocumentos.ShowGroupPanel = false;
            this.gridDocumentos.Size = new System.Drawing.Size(1279, 394);
            this.gridDocumentos.TabIndex = 5;
            this.gridDocumentos.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.GridData_CellFormatting);
            this.gridDocumentos.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridData_CellDoubleClick);
            this.gridDocumentos.CommandCellClick += new Telerik.WinControls.UI.CommandCellClickEventHandler(this.GridData_CommandCellClick);
            this.gridDocumentos.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.GridData_ContextMenuOpening);
            // 
            // Espera
            // 
            this.Espera.AssociatedControl = this.gridDocumentos;
            this.Espera.Location = new System.Drawing.Point(604, 163);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(70, 70);
            this.Espera.TabIndex = 2;
            this.Espera.Text = "Espera";
            this.Espera.WaitingIndicators.Add(this.rotatingRingsWaitingBarIndicatorElement1);
            this.Espera.WaitingStep = 7;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.RotatingRings;
            ((Telerik.WinControls.UI.RadWaitingBarElement)(this.Espera.GetChildAt(0))).WaitingStep = 7;
            ((Telerik.WinControls.UI.WaitingBarContentElement)(this.Espera.GetChildAt(0).GetChildAt(0))).WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.RotatingRings;
            ((Telerik.WinControls.UI.WaitingBarSeparatorElement)(this.Espera.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Dash = false;
            // 
            // rotatingRingsWaitingBarIndicatorElement1
            // 
            this.rotatingRingsWaitingBarIndicatorElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.rotatingRingsWaitingBarIndicatorElement1.Name = "rotatingRingsWaitingBarIndicatorElement1";
            this.rotatingRingsWaitingBarIndicatorElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.rotatingRingsWaitingBarIndicatorElement1.UseCompatibleTextRendering = false;
            // 
            // gridConceptos
            // 
            this.gridConceptos.Caption = "Conceptos";
            this.gridConceptos.ViewDefinition = tableViewDefinition1;
            // 
            // gridCFDIRelacionado
            // 
            this.gridCFDIRelacionado.Caption = "CFDI Relacionados";
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            this.gridCFDIRelacionado.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1});
            this.gridCFDIRelacionado.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.gridCFDIRelacionadoDocumento});
            this.gridCFDIRelacionado.ViewDefinition = tableViewDefinition3;
            // 
            // gridCFDIRelacionadoDocumento
            // 
            this.gridCFDIRelacionadoDocumento.Caption = "Documento Relacionado";
            gridViewTextBoxColumn2.FieldName = "IdDocumento";
            gridViewTextBoxColumn2.HeaderText = "IdDocumento";
            gridViewTextBoxColumn2.MaxLength = 36;
            gridViewTextBoxColumn2.Name = "IdDocumento";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 120;
            this.gridCFDIRelacionadoDocumento.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn2});
            this.gridCFDIRelacionadoDocumento.ViewDefinition = tableViewDefinition2;
            // 
            // gridRecepcionPago
            // 
            this.gridRecepcionPago.Caption = "Complemento: Pago";
            gridViewTextBoxColumn3.FieldName = "Version";
            gridViewTextBoxColumn3.HeaderText = "Ver.";
            gridViewTextBoxColumn3.Name = "Version";
            gridViewTextBoxColumn4.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn4.FieldName = "FechaPago";
            gridViewTextBoxColumn4.FormatString = "{0:d}";
            gridViewTextBoxColumn4.HeaderText = "Fec. Pago";
            gridViewTextBoxColumn4.Name = "FechaPago";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 75;
            gridViewTextBoxColumn5.FieldName = "FormaDePagoP";
            gridViewTextBoxColumn5.HeaderText = "Cve. Pago";
            gridViewTextBoxColumn5.Name = "FormaDePagoP";
            gridViewTextBoxColumn6.FieldName = "MonedaP";
            gridViewTextBoxColumn6.HeaderText = "Moneda";
            gridViewTextBoxColumn6.Name = "MonedaP";
            gridViewTextBoxColumn7.DataType = typeof(decimal);
            gridViewTextBoxColumn7.FieldName = "Monto";
            gridViewTextBoxColumn7.FormatString = "{0:N2}";
            gridViewTextBoxColumn7.HeaderText = "Monto";
            gridViewTextBoxColumn7.Name = "Monto";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn7.Width = 85;
            gridViewTextBoxColumn8.FieldName = "NumOperacion";
            gridViewTextBoxColumn8.HeaderText = "Núm. Operación";
            gridViewTextBoxColumn8.Name = "NumOperacion";
            gridViewTextBoxColumn8.Width = 100;
            gridViewTextBoxColumn9.FieldName = "RfcEmisorCtaOrd";
            gridViewTextBoxColumn9.HeaderText = "RFC Cta. Ordenante";
            gridViewTextBoxColumn9.Name = "RfcEmisorCtaOrd";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn9.Width = 95;
            gridViewTextBoxColumn10.FieldName = "NomBancoOrdExt";
            gridViewTextBoxColumn10.HeaderText = "NomBancoOrdExt";
            gridViewTextBoxColumn10.Name = "NomBancoOrdExt";
            gridViewTextBoxColumn10.Width = 200;
            gridViewTextBoxColumn11.FieldName = "CtaOrdenante";
            gridViewTextBoxColumn11.HeaderText = "CtaOrdenante";
            gridViewTextBoxColumn11.Name = "CtaOrdenante";
            gridViewTextBoxColumn12.FieldName = "RfcEmisorCtaBen";
            gridViewTextBoxColumn12.HeaderText = "RfcEmisorCtaBen";
            gridViewTextBoxColumn12.Name = "RfcEmisorCtaBen";
            gridViewTextBoxColumn12.Width = 95;
            gridViewTextBoxColumn13.FieldName = "CtaBeneficiario";
            gridViewTextBoxColumn13.HeaderText = "CtaBeneficiario";
            gridViewTextBoxColumn13.Name = "CtaBeneficiario";
            gridViewTextBoxColumn13.Width = 95;
            this.gridRecepcionPago.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13});
            this.gridRecepcionPago.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.gridRecepcionPagoD});
            this.gridRecepcionPago.ViewDefinition = tableViewDefinition5;
            // 
            // gridRecepcionPagoD
            // 
            gridViewTextBoxColumn14.FieldName = "IdDocumento";
            gridViewTextBoxColumn14.HeaderText = "IdDocumento (UUID)";
            gridViewTextBoxColumn14.Name = "IdDocumento";
            gridViewTextBoxColumn14.Width = 200;
            gridViewTextBoxColumn15.FieldName = "Serie";
            gridViewTextBoxColumn15.HeaderText = "Serie";
            gridViewTextBoxColumn15.Name = "Serie";
            gridViewTextBoxColumn16.FieldName = "Folio";
            gridViewTextBoxColumn16.HeaderText = "Folio";
            gridViewTextBoxColumn16.Name = "Folio";
            gridViewTextBoxColumn17.FieldName = "RFC";
            gridViewTextBoxColumn17.HeaderText = "RFC";
            gridViewTextBoxColumn17.IsVisible = false;
            gridViewTextBoxColumn17.Name = "RFC";
            gridViewTextBoxColumn17.Width = 85;
            gridViewTextBoxColumn18.FieldName = "Nombre";
            gridViewTextBoxColumn18.HeaderText = "Nombre o Razón Social";
            gridViewTextBoxColumn18.IsVisible = false;
            gridViewTextBoxColumn18.Name = "Nombre";
            gridViewTextBoxColumn18.Width = 200;
            gridViewTextBoxColumn19.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn19.FieldName = "FechaEmision";
            gridViewTextBoxColumn19.FormatString = "{0:d}";
            gridViewTextBoxColumn19.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn19.IsVisible = false;
            gridViewTextBoxColumn19.Name = "FechaEmision";
            gridViewTextBoxColumn19.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn19.Width = 75;
            gridViewTextBoxColumn20.FieldName = "FormaDePagoP";
            gridViewTextBoxColumn20.HeaderText = "Forma Pago";
            gridViewTextBoxColumn20.IsVisible = false;
            gridViewTextBoxColumn20.Name = "FormaDePagoP";
            gridViewTextBoxColumn21.FieldName = "MetodoPago";
            gridViewTextBoxColumn21.HeaderText = "Met. Pago";
            gridViewTextBoxColumn21.Name = "MetodoPago";
            gridViewTextBoxColumn22.FieldName = "MonedaDR";
            gridViewTextBoxColumn22.HeaderText = "Moneda";
            gridViewTextBoxColumn22.Name = "MonedaDR";
            gridViewTextBoxColumn23.DataType = typeof(decimal);
            gridViewTextBoxColumn23.FieldName = "EquivalenciaDR";
            gridViewTextBoxColumn23.FormatString = "{0:N2}";
            gridViewTextBoxColumn23.HeaderText = "T. Cambio";
            gridViewTextBoxColumn23.Name = "EquivalenciaDR";
            gridViewTextBoxColumn23.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn24.FieldName = "NumParcialidad";
            gridViewTextBoxColumn24.HeaderText = "N. Parcialidad";
            gridViewTextBoxColumn24.Name = "NumParcialidad";
            gridViewTextBoxColumn24.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn25.DataType = typeof(decimal);
            gridViewTextBoxColumn25.FieldName = "ImpSaldoAnt";
            gridViewTextBoxColumn25.FormatString = "{0:N2}";
            gridViewTextBoxColumn25.HeaderText = "S. Anterior";
            gridViewTextBoxColumn25.Name = "ImpSaldoAnt";
            gridViewTextBoxColumn25.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn25.Width = 75;
            gridViewTextBoxColumn26.DataType = typeof(decimal);
            gridViewTextBoxColumn26.FieldName = "ImpPagado";
            gridViewTextBoxColumn26.FormatString = "{0:N2}";
            gridViewTextBoxColumn26.HeaderText = "Imp. Pagado";
            gridViewTextBoxColumn26.Name = "ImpPagado";
            gridViewTextBoxColumn26.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn26.Width = 75;
            gridViewTextBoxColumn27.DataType = typeof(decimal);
            gridViewTextBoxColumn27.FieldName = "ImpSaldoInsoluto";
            gridViewTextBoxColumn27.FormatString = "{0:N2}";
            gridViewTextBoxColumn27.HeaderText = "S. Insoluto";
            gridViewTextBoxColumn27.Name = "ImpSaldoInsoluto";
            gridViewTextBoxColumn27.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn27.Width = 75;
            gridViewTextBoxColumn28.FieldName = "ObjetoImpDR";
            gridViewTextBoxColumn28.HeaderText = "Objeto Imp.";
            gridViewTextBoxColumn28.Name = "ObjetoImpDR";
            this.gridRecepcionPagoD.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28});
            this.gridRecepcionPagoD.ViewDefinition = tableViewDefinition4;
            // 
            // Iconos
            // 
            this.Iconos.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Iconos.ImageStream")));
            this.Iconos.TransparentColor = System.Drawing.Color.Transparent;
            this.Iconos.Images.SetKeyName(0, "XML");
            this.Iconos.Images.SetKeyName(1, "PDF");
            // 
            // ContextualMenu
            // 
            this.ContextualMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ContextMenuCopiar,
            this.ContextMenuRemover,
            this.ContextMenuValidar,
            this.ContextMenuEstadoSAT,
            this.ContextMenuArchivos});
            // 
            // ContextMenuCopiar
            // 
            this.ContextMenuCopiar.Image = global::Jaeger.UI.Validador.Properties.Resources.copy_16px;
            this.ContextMenuCopiar.Name = "ContextMenuCopiar";
            this.ContextMenuCopiar.Text = "Copiar";
            this.ContextMenuCopiar.UseCompatibleTextRendering = false;
            this.ContextMenuCopiar.Click += new System.EventHandler(this.ContextMenuCopiar_Click);
            // 
            // ContextMenuRemover
            // 
            this.ContextMenuRemover.Image = global::Jaeger.UI.Validador.Properties.Resources.delete_16px;
            this.ContextMenuRemover.Name = "ContextMenuRemover";
            this.ContextMenuRemover.Text = "Remover";
            // 
            // ContextMenuValidar
            // 
            this.ContextMenuValidar.Image = global::Jaeger.UI.Validador.Properties.Resources.protect_16px;
            this.ContextMenuValidar.Name = "ContextMenuValidar";
            this.ContextMenuValidar.Text = "Validar";
            this.ContextMenuValidar.UseCompatibleTextRendering = false;
            // 
            // ContextMenuEstadoSAT
            // 
            this.ContextMenuEstadoSAT.Name = "ContextMenuEstadoSAT";
            this.ContextMenuEstadoSAT.Text = "Estado SAT";
            this.ContextMenuEstadoSAT.UseCompatibleTextRendering = false;
            // 
            // ContextMenuArchivos
            // 
            this.ContextMenuArchivos.Image = global::Jaeger.UI.Validador.Properties.Resources.xml_file_16px;
            this.ContextMenuArchivos.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ContextArchivoPDFBuscar,
            this.ContextArchivoPDF,
            this.ContextArchivoAcusePDF,
            this.ContextArchivoAcuseXML});
            this.ContextMenuArchivos.Name = "ContextMenuArchivos";
            this.ContextMenuArchivos.Text = "Archivos";
            this.ContextMenuArchivos.UseCompatibleTextRendering = false;
            // 
            // ContextArchivoPDFBuscar
            // 
            this.ContextArchivoPDFBuscar.Name = "ContextArchivoPDFBuscar";
            this.ContextArchivoPDFBuscar.Text = "Buscar PDF";
            this.ContextArchivoPDFBuscar.UseCompatibleTextRendering = false;
            // 
            // ContextArchivoPDF
            // 
            this.ContextArchivoPDF.Name = "ContextArchivoPDF";
            this.ContextArchivoPDF.Text = "Adjuntar PDF";
            this.ContextArchivoPDF.UseCompatibleTextRendering = false;
            // 
            // ContextArchivoAcusePDF
            // 
            this.ContextArchivoAcusePDF.Name = "ContextArchivoAcusePDF";
            this.ContextArchivoAcusePDF.Text = "Adjuntar Acuse PDF";
            this.ContextArchivoAcusePDF.UseCompatibleTextRendering = false;
            // 
            // ContextArchivoAcuseXML
            // 
            this.ContextArchivoAcuseXML.Name = "ContextArchivoAcuseXML";
            this.ContextArchivoAcuseXML.Text = "Adjuntar Acuse XML";
            this.ContextArchivoAcuseXML.UseCompatibleTextRendering = false;
            // 
            // TValida
            // 
            this.TValida.Dock = System.Windows.Forms.DockStyle.Top;
            this.TValida.Location = new System.Drawing.Point(0, 0);
            this.TValida.MinimumSize = new System.Drawing.Size(0, 30);
            this.TValida.Name = "TValida";
            this.TValida.Size = new System.Drawing.Size(1279, 30);
            this.TValida.TabIndex = 0;
            // 
            // ComprobanteFiscalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1279, 450);
            this.Controls.Add(this.gridDocumentos);
            this.Controls.Add(this.BValida);
            this.Controls.Add(this.TValida);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ComprobanteFiscalForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Validador";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ComprobanteFiscalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BValida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDocumentos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDocumentos)).EndInit();
            this.gridDocumentos.ResumeLayout(false);
            this.gridDocumentos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCFDIRelacionado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCFDIRelacionadoDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRecepcionPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRecepcionPagoD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadStatusStrip BValida;
        private Telerik.WinControls.UI.RadLabelElement lblProgreso;
        private Telerik.WinControls.UI.RadGridView gridDocumentos;
        private System.Windows.Forms.ImageList Iconos;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuCopiar;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuValidar;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuEstadoSAT;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuArchivos;
        private Telerik.WinControls.UI.RadMenuItem ContextArchivoPDFBuscar;
        private Telerik.WinControls.UI.RadMenuItem ContextArchivoPDF;
        private Telerik.WinControls.UI.RadMenuItem ContextArchivoAcusePDF;
        private Telerik.WinControls.UI.RadMenuItem ContextArchivoAcuseXML;
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.RotatingRingsWaitingBarIndicatorElement rotatingRingsWaitingBarIndicatorElement1;
        protected internal Telerik.WinControls.UI.RadContextMenu ContextualMenu;
        protected internal TbValidadorControl TValida;
        private Telerik.WinControls.UI.GridViewTemplate gridConceptos;
        private Telerik.WinControls.UI.GridViewTemplate gridCFDIRelacionado;
        private Telerik.WinControls.UI.GridViewTemplate gridCFDIRelacionadoDocumento;
        private Telerik.WinControls.UI.GridViewTemplate gridRecepcionPago;
        private Telerik.WinControls.UI.GridViewTemplate gridRecepcionPagoD;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuRemover;
    }
}