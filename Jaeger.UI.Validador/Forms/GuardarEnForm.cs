﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Validador.Services;
using Jaeger.Aplication.Validador.Contracts;

namespace Jaeger.UI.Validador.Forms {
    public partial class GuardarEnForm : RadForm {
        public BindingList<IDocumentoFiscal> _DataSource;

        public GuardarEnForm(BindingList<IDocumentoFiscal> dataSource) {
            InitializeComponent();
            this._DataSource = new BindingList<IDocumentoFiscal>(dataSource);
        }

        private void DescargaForm_Load(object sender, EventArgs e) {

        }

        private void BtnExaminar_Click(object sender, EventArgs e) {
            var folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK) {
                this.GuardarEn.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void BtnGuardar_Click(object sender, EventArgs e) {
            if (!System.IO.Directory.Exists(this.GuardarEn.Text)) {
                return;
            }
            using (var espera = new Waiting2Form(this.Procesar)) {
                espera.Text = "Guardando archivos ...";
                espera.ShowDialog(this);
            }
        }

        private void BtnCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Procesar() {
            if (this._DataSource != null) {
                if (this._DataSource.Count > 0) {
                    for (int i = 0; i < this._DataSource.Count; i++) {
                        if (this.chkInluirValidacion.Checked) {
                            var val = ComprobanteValidacionPrinter.Json(this._DataSource[i].Validacion.Json());
                            if (val != null) {
                             //   Services.ComunService.ValidacionPDF(val, System.IO.Path.Combine(this.GuardarEn.Text, val.KeyName() + ".pdf"));
                            }
                        }
                        this._DataSource[i].MoveTo(this.GuardarEn.Text);
                    }
                }
            }
        }
    }
}
