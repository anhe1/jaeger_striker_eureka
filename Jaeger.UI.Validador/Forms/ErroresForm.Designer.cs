﻿
namespace Jaeger.UI.Validador.Forms {
    partial class ErroresForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErroresForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblInformacion = new Telerik.WinControls.UI.RadLabel();
            this.panel1 = new Telerik.WinControls.UI.RadPanel();
            this.btnEnviar = new Telerik.WinControls.UI.RadButton();
            this.btnCopiar = new Telerik.WinControls.UI.RadButton();
            this.lblArchivos = new Telerik.WinControls.UI.RadLabel();
            this.Cerrar = new Telerik.WinControls.UI.RadButton();
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.GridDataError = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInformacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnEnviar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCopiar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblArchivos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDataError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDataError.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(462, 50);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblInformacion
            // 
            this.lblInformacion.Location = new System.Drawing.Point(12, 18);
            this.lblInformacion.Name = "lblInformacion";
            this.lblInformacion.Size = new System.Drawing.Size(235, 18);
            this.lblInformacion.TabIndex = 1;
            this.lblInformacion.Text = "Lista de comprobantes con error en la lectura.";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnEnviar);
            this.panel1.Controls.Add(this.btnCopiar);
            this.panel1.Controls.Add(this.lblArchivos);
            this.panel1.Controls.Add(this.Cerrar);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 318);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(462, 42);
            this.panel1.TabIndex = 2;
            // 
            // btnEnviar
            // 
            this.btnEnviar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEnviar.Enabled = false;
            this.btnEnviar.Location = new System.Drawing.Point(214, 10);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(75, 23);
            this.btnEnviar.TabIndex = 4;
            this.btnEnviar.Text = "Enviar";
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // btnCopiar
            // 
            this.btnCopiar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopiar.Location = new System.Drawing.Point(295, 10);
            this.btnCopiar.Name = "btnCopiar";
            this.btnCopiar.Size = new System.Drawing.Size(75, 23);
            this.btnCopiar.TabIndex = 3;
            this.btnCopiar.Text = "Copiar";
            this.btnCopiar.Click += new System.EventHandler(this.btnCopiar_Click);
            // 
            // lblArchivos
            // 
            this.lblArchivos.Location = new System.Drawing.Point(12, 15);
            this.lblArchivos.Name = "lblArchivos";
            this.lblArchivos.Size = new System.Drawing.Size(67, 18);
            this.lblArchivos.TabIndex = 2;
            this.lblArchivos.Text = "Archivos {0}:";
            // 
            // Cerrar
            // 
            this.Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Cerrar.Location = new System.Drawing.Point(376, 10);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(75, 23);
            this.Cerrar.TabIndex = 0;
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.Click += new System.EventHandler(this.Cerrar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.GridDataError);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.HeaderText = "";
            this.groupBox1.Location = new System.Drawing.Point(0, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(462, 268);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // GridDataError
            // 
            this.GridDataError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridDataError.Location = new System.Drawing.Point(2, 18);
            // 
            // 
            // 
            this.GridDataError.MasterTemplate.AutoGenerateColumns = false;
            this.GridDataError.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.FieldName = "PathXML";
            gridViewTextBoxColumn1.HeaderText = "Ruta XML";
            gridViewTextBoxColumn1.Name = "PathXML";
            gridViewTextBoxColumn1.Width = 211;
            gridViewTextBoxColumn2.FieldName = "Resultado";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Resultado";
            gridViewTextBoxColumn2.Width = 247;
            this.GridDataError.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.GridDataError.MasterTemplate.ShowFilteringRow = false;
            this.GridDataError.MasterTemplate.ShowRowHeaderColumn = false;
            this.GridDataError.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridDataError.Name = "GridDataError";
            this.GridDataError.ReadOnly = true;
            this.GridDataError.ShowGroupPanel = false;
            this.GridDataError.ShowRowErrors = false;
            this.GridDataError.Size = new System.Drawing.Size(458, 248);
            this.GridDataError.TabIndex = 0;
            this.GridDataError.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridDataError_KeyDown);
            // 
            // ErroresForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 360);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblInformacion);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ErroresForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Errores";
            this.Load += new System.EventHandler(this.ErroresForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInformacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnEnviar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCopiar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblArchivos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridDataError.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDataError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadLabel lblInformacion;
        private Telerik.WinControls.UI.RadPanel panel1;
        private Telerik.WinControls.UI.RadButton Cerrar;
        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        public Telerik.WinControls.UI.RadGridView GridDataError;
        private Telerik.WinControls.UI.RadLabel lblArchivos;
        private Telerik.WinControls.UI.RadButton btnCopiar;
        private Telerik.WinControls.UI.RadButton btnEnviar;
    }
}