﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Validador.Services;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Base;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Aplication.Validador.Catalogos;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.UI.Validador.Builder;

namespace Jaeger.UI.Validador.Forms {
    public partial class ComprobanteFiscalForm : RadForm {
        #region declaraciones
        protected internal Aplication.Validador.Builder.Administrador _Administrador;
        protected internal IVerificarService validador;
        protected internal IAdministradorBackup _Backup;
        protected internal Aplication.Empresa.IConfiguracionService Empresa;
        protected internal IConfiguration _Configuration;
        private Progress<Progreso> progreso;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ComprobanteFiscalForm() {
            InitializeComponent();
        }

        private void ComprobanteFiscalForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.TValida.Enabled = false;

            using (IValidadorGridViewBuilder builder = new ValidadorGridViewBuilder()) {
                this.gridDocumentos.Columns.AddRange(builder.Templetes().Master().Build());
                this.gridConceptos.Columns.AddRange(builder.Templetes().Conceptos().Master().Build());
            }

            this.gridDocumentos.Standard();
            this.gridConceptos.Standard();
            this.gridCFDIRelacionado.Standard();
            this.gridCFDIRelacionadoDocumento.Standard();
            this.gridRecepcionPago.Standard();
            this.gridRecepcionPagoD.Standard();

            this.GridFormatosCondicionales();

            this.Empresa = new Aplication.Empresa.ConfiguracionService();
            var d0 = this.Empresa.GetParametros(Domain.Empresa.Enums.ConfigGroupEnum.Validador);
            this._Configuration = Aplication.Validador.Abstracts.VerificarBase.Conf().Add(d0).Build();
            this.TValida.Administrador = new Aplication.Validador.Builder.Administrador();
            this.TValida.Administrador.AddRFC(ConfigService.Synapsis.Empresa.RFC).AddConfiguration(this._Configuration);
            this._Administrador = this.TValida.Administrador;

            this.validador = new VerificarService();
            this.validador.WithRFC(ConfigService.Synapsis.Empresa.RFC);
            this.validador.AddConfiguration(this._Configuration);

            this.TValida.TAgregar.Click += this.TValida_Agregar_Click;
            this.TValida.TRemoverSeleccionado.Click += this.TValida_Remover_Click;
            this.TValida.TRemoverTodo.Click += this.TValida_Remover_Todo_Click;
            this.TValida.TActualizar.Click += TValida_Actualizar_Click;
            this.TValida.TValidacionSelect.Click += this.TValida_Imprimir_Click;
            this.TValida.TExportarExcel.Click += this.TValida_ExportarExcel_Click;
            this.TValida.TExportarTemplete.Click += this.TValida_ExportarTemplete_Click;
            this.TValida.TExportarPlantilla.Click += this.TValida_DescargarTemplete_Click;
            this.TValida.TLogError.Click += TValida_LogError_Click;
            this.TValida.TConfiguracion.Click += this.TValida_Configuracion_Click;
            this.TValida.TFiltro.Click += TValida_Filtro_Click;
            this.TValida.Cerrar.Click += this.TValida_Cerrar_Click;
            this.TValida.TValidar.Click += this.TValida_Validar_Click;
            this.TValida.TBackupTodo.Click += TValida_BackupTodo_Click;
            this.TValida.TValidacionPDF.Click += this.TValida_ValidacionPDF_Click;
            this.TValida.TGuardarEn.Click += TValida_GuardarEn_Click;
            this.TValida.TInformacion.Click += TValida_Informacion_Click;
            this.TValida.TUpdate.Click += TValida_Update_Click;
            this.ContextArchivoPDFBuscar.Click += this.TValida_BuscarPDF_Click;
            this.ContextArchivoPDF.Click += this.TValdia_AsignarPDF_Click;
            this.ContextMenuValidar.Click += this.ContextValidar_Click;
            this.ContextMenuRemover.Click += ContextMenuRemover_Click;

            this.gridDocumentos.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.GridData_RowSourceNeeded);
            this.gridConceptos.HierarchyDataProvider = new GridViewEventDataProvider(this.gridConceptos);
            this.gridCFDIRelacionado.HierarchyDataProvider = new GridViewEventDataProvider(this.gridCFDIRelacionado);
            this.gridCFDIRelacionadoDocumento.HierarchyDataProvider = new GridViewEventDataProvider(this.gridCFDIRelacionadoDocumento);
            this.gridRecepcionPago.HierarchyDataProvider = new GridViewEventDataProvider(this.gridRecepcionPago);
            this.gridRecepcionPagoD.HierarchyDataProvider = new GridViewEventDataProvider(this.gridRecepcionPagoD);
            this.TValida.Enabled = true;
        }

        private void TValida_Update_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Updates)) {
                espera.Text = "Buscando actualización";
                espera.ShowDialog(this);
            }
        }

        #region barra de herramientas
        private void TValida_Informacion_Click(object sender, EventArgs e) {
            RadMessageBox.Show(this, Articulo69B.Catalogo.Revision.TrimEnd(), Articulo69B.Catalogo.Title, MessageBoxButtons.OK, RadMessageIcon.Info);
        }

        private void TValida_LogError_Click(object sender, EventArgs e) {
            if (this._Administrador.DataError.Count > 0) {
                var conError = new ErroresForm();
                conError.GridDataError.DataSource = this._Administrador.DataError;
                conError.ShowDialog(this);
            } else {
                RadMessageBox.Show(this, "No existe información por mostrar", "Validador", MessageBoxButtons.OK, RadMessageIcon.Info);
            }
        }

        private void TValida_Agregar_Click(object sender, EventArgs e) {
            using (var _openFile = new OpenFileDialog() { Filter = "*.xml|*.XML" }) {
                if (_openFile.ShowDialog(this) == DialogResult.OK) {
                    if (!_openFile.FileName.ToLower().EndsWith(".pdf")) {
                        this.TValida.TAgregar.Enabled = false;
                        this._Administrador.Agregar(_openFile.FileName);
                        this.TValida.TAgregar.Enabled = true;
                        this.gridDocumentos.DataSource = this.TValida.Administrador.DataSource;
                        if (this._Administrador.DataError.Count > 0) {
                            var conError = new ErroresForm();
                            conError.GridDataError.DataSource = this._Administrador.DataError;
                            conError.ShowDialog(this);
                        }
                    } else {
                        RadMessageBox.Show(this, "Tipo de archivo no válido.", "Validador", MessageBoxButtons.OK, RadMessageIcon.Info);
                    }
                }
            }
        }

        private void TValida_Remover_Todo_Click(object sender, EventArgs e) {
            this._Administrador.DataSource.Clear();
        }

        private void TValida_Remover_Click(object sender, EventArgs e) {
            if (this.gridDocumentos.CurrentRow != null) {
                var seleccionado = this.gridDocumentos.CurrentRow.DataBoundItem as IDocumentoFiscal;
                if (seleccionado != null) {
                    this.TValida.Administrador.Remover(seleccionado);
                }
            }
        }

        private void TValida_Actualizar_Click(object sender, EventArgs e) {
            if (!Directory.Exists(this.TValida.TCarpeta.Text)) {
                RadMessageBox.Show(this, string.Format(Properties.Resources.validador_msg_RutaSinAcceso, this.TValida.TCarpeta.Text), "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            this.gridDocumentos.DataSource = null;
            this._Administrador.DataSource.Clear();
            using (var espera = new Waiting1Form(this.Cargar)) {
                espera.Text = "Leyendo archivos ...";
                espera.ShowDialog(this);
            }
            this.gridDocumentos.DataSource = this._Administrador.DataSource;
            this.gridDocumentos.Refresh();
            this.lblProgreso.Text = string.Format("Archivos: {0}", this._Administrador.DataSource.Count);
            if (this._Administrador.DataError.Count > 0) {
                var conError = new ErroresForm();
                conError.GridDataError.DataSource = this._Administrador.DataError;
                conError.ShowDialog(this);
            }
        }

        private void TValida_Imprimir_Click(object sender, EventArgs e) {
            if (this.gridDocumentos.CurrentRow != null) {
                var seleccionado = this.gridDocumentos.CurrentRow.DataBoundItem as IDocumentoFiscal;
                if (seleccionado != null) {
                    if (seleccionado.Validacion != null) {
                        var reporte = new ReporteForm(ComprobanteValidacionPrinter.Json(seleccionado.Validacion.Json()));
                        reporte.ShowDialog(this);
                    }
                }
            }
        }

        private void TValida_ValidacionPDF_Click(object sender, EventArgs e) {
            var folder = new FolderBrowserDialog() { Description = "Selecciona ruta de descarga" };
            if (folder.ShowDialog(this) != DialogResult.OK)
                return;

            if (Directory.Exists(folder.SelectedPath) == false) {
                MessageBox.Show(this, Properties.Resources.validador_msg_RutaNoValida, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            this.TValida.TValidacionPDF.Tag = folder.SelectedPath;

            using (var espera = new Waiting1Form(this.PDFValidacion)) {
                espera.Text = "Procesando ...";
                espera.ShowDialog(this);
            }
        }

        private void TValida_Validar_Click(object sender, EventArgs e) {
            if (this._Administrador.DataSource.Count <= 0) {
                RadMessageBox.Show(this, "No existen documentos por procesar.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }
            //using (var espera = new Waiting1Form(this.ProcesarTodo1)) {
            //    espera.Text = "Validando...";
            //    espera.ShowDialog(this);
            //};
            this.Espera.Visible = true;
            this.Espera.StartWaiting();
            this.TValida.Enabled = false;
            this.ProcesarTodo1();
            //this.gridDocumentos.DataSource = null;
            //this.gridDocumentos.DataSource = this.Administrador._DataSource;
            //this.gridDocumentos.Refresh();
        }

        private void TValida_Configuracion_Click(object sender, EventArgs e) {
            var configuracion = new ConfiguracionForm(this.validador.Configuracion);
            configuracion.Guardar_Click += TValida_Configuracion_Guardar_Click;
            configuracion.ShowDialog(this);
        }

        private void TValida_Configuracion_Guardar_Click(object sender, IConfiguration e) {
            var d1 = Aplication.Validador.Abstracts.VerificarBase.Conf().Build(e);
            this.Empresa.Save(d1);
            this.validador.Configuracion = e;
        }

        private void TValida_Filtro_Click(object sender, EventArgs e) {
            this.gridDocumentos.ActivateFilterRow(((CommandBarToggleButton)sender).ToggleState);
        }

        private void TValida_ExportarExcel_Click(object sender, EventArgs e) {
            var saveFile = new SaveFileDialog() { Filter = "*.xlsx|*xlsx", DefaultExt = "xlsx", AddExtension = true };
            if (saveFile.ShowDialog() == DialogResult.OK) {
                if (Util.Services.FileService.IsFileinUse(saveFile.FileName)) {
                    RadMessageBox.Show(this, "El archivo seleccionado actualmente se encuentra en uso.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                }
                this.TValida.Administrador.ExportarExcel(saveFile.FileName);
            }
        }

        private void TValida_ExportarTemplete_Click(object sender, EventArgs eventArgs) {
            var openFile = new OpenFileDialog() { Filter = "*.xlsx|*xlsx", DefaultExt = "xlsx", AddExtension = true, Title = "Selecciona un templete EXCEL" };
            if (openFile.ShowDialog() == DialogResult.OK) {
                if (Util.Services.FileService.IsFileinUse(openFile.FileName)) {
                    RadMessageBox.Show(this, "El archivo seleccionado actualmente se encuentra en uso.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                }
                var saveFile = new SaveFileDialog() {
                    Filter = "*.xlsx|*xlsx", DefaultExt = "xlsx", AddExtension = true, Title = "Nombre del archivo de exportación",
                    FileName = "Reporte_Validacion_" + DateTime.Now.ToString("ddMMyyyy_hhmmss")
                };
                if (saveFile.ShowDialog() == DialogResult.OK) {
                    if (Util.Services.FileService.IsFileinUse(saveFile.FileName)) {
                        RadMessageBox.Show(this, "El archivo seleccionado actualmente se encuentra en uso.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        return;
                    }
                    this.TValida.Administrador.ExportarExcel(saveFile.FileName, openFile.FileName);
                }
            }
        }

        private void TValida_DescargarTemplete_Click(object sender, EventArgs eventArgs) {
            var saveFile = new SaveFileDialog() { Filter = "*.xlsx|*xlsx", DefaultExt = "xlsx", AddExtension = true };
            if (saveFile.ShowDialog() == DialogResult.OK) {
                if (Util.Services.FileService.IsFileinUse(saveFile.FileName)) {
                    RadMessageBox.Show(this, "El archivo seleccionado actualmente se encuentra en uso.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                }
                this._Administrador.DescargarTemplete(saveFile.FileName);
            }
        }

        private void TValida_BuscarPDF_Click(object sender, EventArgs e) {
            var seleccionado = this.gridDocumentos.CurrentRow.DataBoundItem as IDocumentoFiscal;
            if (seleccionado != null) {
                var resultado = Aplication.Validador.Builder.Administrador.SearchPDF(new FileInfo(seleccionado.PathXML), seleccionado.IdDocumento);
                if (string.IsNullOrEmpty(resultado)) {
                    RadMessageBox.Show(this, "No se encontro", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                } else {
                    MessageBox.Show(this, resultado);
                    seleccionado.PathPDF = resultado;
                }
            }
        }

        private void TValdia_AsignarPDF_Click(object sender, EventArgs e) {
            var seleccionado = this.gridDocumentos.CurrentRow.DataBoundItem as IDocumentoFiscal;
            if (seleccionado != null) {
                var openFile = new OpenFileDialog() { Filter = "*.pdf|*.PDF" };
                if (openFile.ShowDialog(this) == DialogResult.OK) {
                    seleccionado.PathPDF = openFile.FileName;
                }
            }
        }

        private void TValida_BackupTodo_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.TBackupTodo)) {
                espera.Text = "Verificando ...";
                espera.ShowDialog(this);
            }
        }

        private void TValida_GuardarEn_Click(object sender, EventArgs e) {
            if (this._Administrador.DataSource.Count <= 0)
                return;
            var guardarEn = new GuardarEnForm(this._Administrador.DataSource);
            guardarEn.ShowDialog(this);
        }

        private void TValida_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region menu contextual
        private void ContextValidar_Click(object sender, EventArgs e) {
            var seleccionado = this.gridDocumentos.CurrentRow.DataBoundItem as IDocumentoFiscal;
            if (seleccionado != null) {
                using (var espera = new Waiting1Form(this.ProcesarSeleccionado)) {
                    espera.Text = "Validando...";
                    espera.ShowDialog(this);
                }
            }
        }

        private void ContextMenuCopiar_Click(object sender, EventArgs e) {
            Clipboard.SetDataObject(this.gridDocumentos.CurrentCell.Value.ToString());
        }

        private void ContextMenuRemover_Click(object sender, EventArgs e) {
            if (this.gridDocumentos.CurrentRow != null) {
                var seleccionado = this.gridDocumentos.CurrentRow.DataBoundItem as IDocumentoFiscal;
                if (seleccionado != null) {
                    this.TValida.Administrador.Remover(seleccionado);
                }
            }
        }
        #endregion

        #region metodos del grid
        private void GridData_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridFilterCellElement) {

            } else if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.gridDocumentos.CurrentRow.ViewInfo.ViewTemplate == this.gridDocumentos.MasterTemplate) {
                    e.ContextMenu = this.ContextualMenu.DropDown;
                } //else if (this.gridData.CurrentRow.ViewInfo.ViewTemplate == this.gridComplementoPagosDoctoRelacionado) {
                //    if ((int)this.gridData.CurrentRow.ViewInfo.CurrentRow.Cells["IdComprobante"].Value == 0)
                //        e.ContextMenu = this.mContextualComplementoP.DropDown;
                //}
            }
        }

        private void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            IDocumentoFiscal fila = e.ParentRow.DataBoundItem as IDocumentoFiscal;
            if (e.Template.Caption == this.gridConceptos.Caption) {
                if (fila.Conceptos != null) {
                    foreach (var item in fila.Conceptos) {
                        GridViewRowInfo row = e.Template.Rows.NewRow();
                        row.Cells["ClaveProducto"].Value = item.ClaveProdServ;
                        row.Cells["NoIdentificacion"].Value = item.NoIdentificacion;
                        row.Cells["Cantidad"].Value = item.Cantidad;
                        row.Cells["ClaveUnidad"].Value = item.ClaveUnidad;
                        row.Cells["Unidad"].Value = item.Unidad;
                        row.Cells["Descripcion"].Value = item.Descripcion;
                        row.Cells["ObjetoImp"].Value = item.ObjetoImp;
                        row.Cells["ValorUnitario"].Value = item.ValorUnitario;
                        row.Cells["Importe"].Value = item.Importe;
                        row.Cells["Descuento"].Value = item.Descuento;
                        e.SourceCollection.Add(row);
                    }
                }
            } else if (e.Template.Caption == this.gridCFDIRelacionado.Caption) {   // grid de CFDI Relacionados al comprobante (clave de tipo de relacion)
                if (fila.CFDIRelacionado != null) {
                    foreach (var item in fila.CFDIRelacionado) {
                        GridViewRowInfo row = e.Template.Rows.NewRow();
                        row.Tag = item.CFDIRelacionados;
                        row.Cells["Clave"].Value = item.TipoRelacion;
                        //row.Cells["Name"].Value = fila.CFDIRelacionado.TipoRelacion.Descripcion;
                        e.SourceCollection.Add(row);
                    }
                }
            } else if (e.Template.Caption == this.gridCFDIRelacionadoDocumento.Caption) {   // grid de documentos relacionados 
                GridViewHierarchyRowInfo otro = e.ParentRow.Parent as GridViewHierarchyRowInfo;
                IDocumentoFiscal t1 = otro.DataBoundItem as IDocumentoFiscal;
                if (t1 != null) {
                    if (e.ParentRow.Tag != null) {
                        var d0 = e.ParentRow.Tag as List<SAT.Reader.CFD.Interfaces.IDocumentoCFDIRelacionado>;
                        if (d0 != null) {
                            foreach (var item in d0) {
                                GridViewRowInfo row = e.Template.Rows.NewRow();
                                row.Cells["IdDocumento"].Value = item.UUID;
                                e.SourceCollection.Add(row);
                            }
                        }
                    }
                }
            } else if (e.Template.Caption == this.gridRecepcionPago.Caption) {   // grid de Complemento de Pagos
                if (fila.Pagos != null) {
                    foreach (var item in fila.Pagos) {
                        GridViewRowInfo row = e.Template.Rows.NewRow();
                        row.Cells["Version"].Value = item.Version;
                        row.Cells["FechaPago"].Value = item.FechaPagoP;
                        row.Cells["FormaDePagoP"].Value = item.FormaDePagoP;
                        row.Cells["MonedaP"].Value = item.MonedaP;
                        row.Cells["Monto"].Value = item.MontoP;
                        row.Cells["NumOperacion"].Value = item.NumOperacion;
                        row.Cells["RfcEmisorCtaOrd"].Value = item.RfcEmisorCtaOrd;
                        row.Cells["NomBancoOrdExt"].Value = item.NomBancoOrdExt;
                        row.Cells["CtaOrdenante"].Value = item.CtaOrdenante;
                        row.Cells["RfcEmisorCtaBen"].Value = item.RfcEmisorCtaBen;
                        row.Cells["CtaBeneficiario"].Value = item.CtaBeneficiario;
                        row.Tag = item.Relacionados;
                        e.SourceCollection.Add(row);
                    }
                }
            } else if (e.Template.Caption == this.gridRecepcionPagoD.Caption) {
                GridViewRowInfo rowView = e.ParentRow;
                if (rowView != null) {
                    var pagos = e.ParentRow.Tag as List<SAT.Reader.CFD.Interfaces.IDocumentoComplementoPagoCFDIRelacionado>;
                    if (pagos != null) {
                        foreach (var doctoRel in pagos) {
                            GridViewRowInfo row = e.Template.Rows.NewRow();
                            row.Cells["IdDocumento"].Value = doctoRel.IdDocumento;
                            row.Cells["Serie"].Value = doctoRel.Serie;
                            row.Cells["Folio"].Value = doctoRel.Folio;
                            row.Cells["RFC"].Value = doctoRel.RFC;
                            row.Cells["Nombre"].Value = doctoRel.Nombre;
                            row.Cells["FechaEmision"].Value = doctoRel.FechaEmision;
                            row.Cells["FormaDePagoP"].Value = doctoRel.FormaDePagoP;
                            row.Cells["MetodoPago"].Value = doctoRel.MetodoPago;
                            row.Cells["MonedaDR"].Value = doctoRel.MonedaDR;
                            row.Cells["EquivalenciaDR"].Value = doctoRel.EquivalenciaDR;
                            row.Cells["NumParcialidad"].Value = doctoRel.NumParcialidad;
                            row.Cells["ImpSaldoAnt"].Value = doctoRel.ImpSaldoAnt;
                            row.Cells["ImpPagado"].Value = doctoRel.ImpPagado;
                            row.Cells["ImpSaldoInsoluto"].Value = doctoRel.ImpSaldoInsoluto;
                            row.Cells["ObjetoImpDR"].Value = doctoRel.ObjetoImpDR;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            }
        }

        private void GridData_CellFormatting(object sender, CellFormattingEventArgs e) {
            if (e.Column.Name == "XML" || e.Column.Name == "PDF") {
                if (e.CellElement.Value != null) {
                    if (e.CellElement.Value.ToString() != "") {
                        if (e.Column.Name == "XML") {
                            e.CellElement.Image = Iconos.Images["XML"];
                            e.CellElement.DrawText = false;
                        } else if (e.Column.Name == "PDF") {
                            e.CellElement.Image = Iconos.Images["PDF"];
                            e.CellElement.DrawText = false;
                        }
                    }
                }
            } else {
                e.CellElement.Image = null;
                e.CellElement.DrawText = true;
                if (e.Column.Name != "Resultado") {
                    if (e.CellElement.Children.Count > 0)
                        e.CellElement.Children.Clear();
                }
            }
        }

        private void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (e.Column.Name == "XML" || e.Column.Name == "PDF") {
                var seleccionado = this.gridDocumentos.CurrentRow.DataBoundItem as IDocumentoFiscal;
                if (seleccionado != null) {
                    if (e.Column.Name == "XML") {

                    } else if (e.Column.Name == "PDF") {
                        if (!string.IsNullOrEmpty(seleccionado.PathPDF)) {
                            if (File.Exists(seleccionado.PathPDF)) {
                                var viewPDF = new ViewerPdfForm(seleccionado.PathPDF) { MdiParent = this.MdiParent };
                                viewPDF.Show();
                            }
                        }
                    }
                }
            }
        }

        private void GridData_CommandCellClick(object sender, GridViewCellEventArgs e) {
            var seleccionado = this.gridDocumentos.CurrentRow.DataBoundItem as IDocumentoFiscal;
            if (seleccionado != null) {
                using (var espera = new Waiting1Form(this.ProcesarSeleccionado)) {
                    espera.Text = "Validando...";
                    espera.ShowDialog(this);
                }
            }
        }
        #endregion

        #region metodos privados
        private void Cargar() {
            this.progreso = new Progress<Progreso>();
            this.progreso.ProgressChanged += Progreso_ProgressChanged;
            this.TValida.Administrador.Search(this.TValida.TCarpeta.Text, SearchOption.AllDirectories, this.progreso);
        }

        private void ProcesarSeleccionado() {
            var seleccionado = this.gridDocumentos.CurrentRow.DataBoundItem as IDocumentoFiscal;
            this.validador.Execute(seleccionado);
        }

        private async void ProcesarTodo1() {
            this.progreso = new Progress<Progreso>();
            this.progreso.ProgressChanged += Progreso_ProgressChanged;
            await this.validador.ProcesarParallelAsync(this._Administrador.DataSource.ToList(), this.progreso);
        }

        private void ProcesarTodo() {
            this.progreso = new Progress<Progreso>();
            this.progreso.ProgressChanged += Progreso_ProgressChanged;
            IProgress<Progreso> d1 = this.progreso;
            var reporte = new Progreso();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < this._Administrador.DataSource.Count; i++) {
                reporte.Caption = "Procesando: " + this.TValida.Administrador.DataSource[i].PathXML;
                d1.Report(reporte);
                this.validador.Execute(this._Administrador.DataSource[i]);
            }
            stopwatch.Stop();
            TimeSpan elapsed = stopwatch.Elapsed;
            reporte.Caption = string.Format("{0:00}:{1:00}:{2:00}", elapsed.Hours, elapsed.Minutes, elapsed.Seconds);
            d1.Report(reporte);
        }

        private void TBackupTodo() {
            this.gridDocumentos.BeginUpdate();
            this.progreso = new Progress<Progreso>();
            this.progreso.ProgressChanged += Progreso_ProgressChanged;
            this._Administrador.DataSource = this._Backup.Procesar(this._Administrador.DataSource, this.progreso);
            this.gridDocumentos.EndUpdate();
        }

        protected virtual void PDFValidacion() {
            foreach (var item in this._Administrador.DataSource) {
                if (item.Version == "3.3") {
                    var val = ComprobanteValidacionPrinter.Json(item.Validacion.Json());
                    if (val != null) {
                      //  Services.ComunService.ValidacionPDF(val, Path.Combine((string)this.Tag, val.KeyName() + ".pdf"));
                    }
                }
            }
        }

        private void GridFormatosCondicionales() {
            ExpressionFormattingObject emisorReceptor = new ExpressionFormattingObject("Receptor", string.Format("EmisorRFC NOT = '{0}' AND ReceptorRFC NOT = '{1}'", ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RFC), true) { RowBackColor = Color.LightYellow };
            ExpressionFormattingObject registro = new ExpressionFormattingObject("Registrado", "Registrado = true", true) { RowForeColor = Color.Green };
            this.gridDocumentos.Columns["EmisorRFC"].ConditionalFormattingObjectList.Add(emisorReceptor);
            this.gridDocumentos.Columns["Registrado"].ConditionalFormattingObjectList.Add(registro);
        }

        private void Updates() {
            var d = Actualizar.GetVersion();
            var d0 = new UpdateManager().WithVersion(d);
            if (d0.CheckForUpdate()) {
                this.lblProgreso.Text = "Actualización disponible " + d0.Data.Version.ToString();
                Actualizar.Procesar(d0.Data);
            } else {
                this.lblProgreso.Text = "No hay actualizaciones disponibles.";
            }
        }
        #endregion

        #region eventos
        private void Progreso_ProgressChanged(object sender, Progreso e) {
            this.lblProgreso.Text = e.Caption;
            if (e.Terminado) {
                this.TValida.Enabled = true;
                this.Espera.StopWaiting();
                this.Espera.Visible = false;
                this.gridDocumentos.DataSource = null;
                this.gridDocumentos.DataSource = this._Administrador.DataSource;
                if (this.validador.Configuracion.Registrar) {
                    this.TValida.TBackupTodo.PerformClick();
                }
            }
        }
        #endregion
    }
}
