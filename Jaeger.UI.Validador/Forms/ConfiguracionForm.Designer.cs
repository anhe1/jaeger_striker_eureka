﻿
namespace Jaeger.UI.Validador.Forms {
    partial class ConfiguracionForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfiguracionForm));
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.ObtenerEmisor = new Telerik.WinControls.UI.RadCheckBox();
            this.CheckSchema = new Telerik.WinControls.UI.RadCheckBox();
            this.CheckUTF8 = new Telerik.WinControls.UI.RadCheckBox();
            this.CheckUsoCFDI = new Telerik.WinControls.UI.RadCheckBox();
            this.CheckSelloCFDI = new Telerik.WinControls.UI.RadCheckBox();
            this.ValidarClaveMetodoPago = new Telerik.WinControls.UI.RadCheckBox();
            this.CheckSelloTFD = new Telerik.WinControls.UI.RadCheckBox();
            this.DownloadCertificado = new Telerik.WinControls.UI.RadCheckBox();
            this.ValidarClaveFormaPago = new Telerik.WinControls.UI.RadCheckBox();
            this.CheckEstadoSAT = new Telerik.WinControls.UI.RadCheckBox();
            this.RequiereFormaYMetodoPago = new Telerik.WinControls.UI.RadCheckBox();
            this.CheckAddendaSin = new Telerik.WinControls.UI.RadCheckBox();
            this.ValidaAritmetica = new Telerik.WinControls.UI.RadCheckBox();
            this.ValidarClaveConcepto = new Telerik.WinControls.UI.RadCheckBox();
            this.CheckRenameFile = new Telerik.WinControls.UI.RadCheckBox();
            this.ModoParalelo = new Telerik.WinControls.UI.RadCheckBox();
            this.AsociarPDF = new Telerik.WinControls.UI.RadCheckBox();
            this.groupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.SoloCFDIValido = new Telerik.WinControls.UI.RadCheckBox();
            this.RegistrarComprobante = new Telerik.WinControls.UI.RadCheckBox();
            this.RegistrarConceptos = new Telerik.WinControls.UI.RadCheckBox();
            this.groupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.CheckArticulo69B = new Telerik.WinControls.UI.RadCheckBox();
            this.groupBox4 = new Telerik.WinControls.UI.RadGroupBox();
            this.VerificarEmisorCP = new Telerik.WinControls.UI.RadCheckBox();
            this.LugarExpedicion32B = new Telerik.WinControls.UI.RadCheckBox();
            this.LugarExpedicion32 = new Telerik.WinControls.UI.RadTextBox();
            this.Guardar = new Telerik.WinControls.UI.RadButton();
            this.Cerrar = new Telerik.WinControls.UI.RadButton();
            this.label1 = new Telerik.WinControls.UI.RadLabel();
            this.CheckSoloEmisorReceptor = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ObtenerEmisor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckSchema)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckUTF8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckUsoCFDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckSelloCFDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidarClaveMetodoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckSelloTFD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DownloadCertificado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidarClaveFormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEstadoSAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequiereFormaYMetodoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckAddendaSin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidaAritmetica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidarClaveConcepto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckRenameFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ModoParalelo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AsociarPDF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox2)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SoloCFDIValido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegistrarComprobante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegistrarConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox3)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckArticulo69B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox4)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VerificarEmisorCP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LugarExpedicion32B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LugarExpedicion32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Guardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckSoloEmisorReceptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.ObtenerEmisor);
            this.groupBox1.Controls.Add(this.CheckSchema);
            this.groupBox1.Controls.Add(this.CheckUTF8);
            this.groupBox1.Controls.Add(this.CheckUsoCFDI);
            this.groupBox1.Controls.Add(this.CheckSelloCFDI);
            this.groupBox1.Controls.Add(this.ValidarClaveMetodoPago);
            this.groupBox1.Controls.Add(this.CheckSelloTFD);
            this.groupBox1.Controls.Add(this.DownloadCertificado);
            this.groupBox1.Controls.Add(this.ValidarClaveFormaPago);
            this.groupBox1.Controls.Add(this.CheckEstadoSAT);
            this.groupBox1.Controls.Add(this.RequiereFormaYMetodoPago);
            this.groupBox1.Controls.Add(this.CheckAddendaSin);
            this.groupBox1.Controls.Add(this.ValidaAritmetica);
            this.groupBox1.Controls.Add(this.ValidarClaveConcepto);
            this.groupBox1.HeaderText = "General";
            this.groupBox1.Location = new System.Drawing.Point(12, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(269, 422);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "General";
            // 
            // ObtenerEmisor
            // 
            this.ObtenerEmisor.Location = new System.Drawing.Point(10, 323);
            this.ObtenerEmisor.Name = "ObtenerEmisor";
            this.ObtenerEmisor.Size = new System.Drawing.Size(246, 46);
            this.ObtenerEmisor.TabIndex = 2;
            this.ObtenerEmisor.Text = "<html><p>Obtener nombre o denominación social desde </p><p>el certificado, cuando" +
    " no este disponible el </p><p>valor del atributo,</p></html>";
            // 
            // CheckSchema
            // 
            this.CheckSchema.Location = new System.Drawing.Point(10, 23);
            this.CheckSchema.Name = "CheckSchema";
            this.CheckSchema.Size = new System.Drawing.Size(96, 18);
            this.CheckSchema.TabIndex = 1;
            this.CheckSchema.Text = "Validación XSD";
            // 
            // CheckUTF8
            // 
            this.CheckUTF8.Location = new System.Drawing.Point(10, 47);
            this.CheckUTF8.Name = "CheckUTF8";
            this.CheckUTF8.Size = new System.Drawing.Size(152, 18);
            this.CheckUTF8.TabIndex = 1;
            this.CheckUTF8.Text = "Verificar codificación UTF8";
            // 
            // CheckUsoCFDI
            // 
            this.CheckUsoCFDI.Location = new System.Drawing.Point(10, 190);
            this.CheckUsoCFDI.Name = "CheckUsoCFDI";
            this.CheckUsoCFDI.Size = new System.Drawing.Size(168, 18);
            this.CheckUsoCFDI.TabIndex = 1;
            this.CheckUsoCFDI.Text = "Verificar clave de uso de CFDI";
            // 
            // CheckSelloCFDI
            // 
            this.CheckSelloCFDI.Location = new System.Drawing.Point(10, 71);
            this.CheckSelloCFDI.Name = "CheckSelloCFDI";
            this.CheckSelloCFDI.Size = new System.Drawing.Size(107, 18);
            this.CheckSelloCFDI.TabIndex = 1;
            this.CheckSelloCFDI.Text = "Validar sello CFDI";
            // 
            // ValidarClaveMetodoPago
            // 
            this.ValidarClaveMetodoPago.Location = new System.Drawing.Point(10, 261);
            this.ValidarClaveMetodoPago.Name = "ValidarClaveMetodoPago";
            this.ValidarClaveMetodoPago.Size = new System.Drawing.Size(142, 18);
            this.ValidarClaveMetodoPago.TabIndex = 1;
            this.ValidarClaveMetodoPago.Text = "Validar método de pago";
            // 
            // CheckSelloTFD
            // 
            this.CheckSelloTFD.Location = new System.Drawing.Point(10, 94);
            this.CheckSelloTFD.Name = "CheckSelloTFD";
            this.CheckSelloTFD.Size = new System.Drawing.Size(103, 18);
            this.CheckSelloTFD.TabIndex = 1;
            this.CheckSelloTFD.Text = "Validar sello SAT";
            // 
            // DownloadCertificado
            // 
            this.DownloadCertificado.Location = new System.Drawing.Point(10, 118);
            this.DownloadCertificado.Name = "DownloadCertificado";
            this.DownloadCertificado.Size = new System.Drawing.Size(237, 18);
            this.DownloadCertificado.TabIndex = 1;
            this.DownloadCertificado.Text = "Descargar certificado para validar Sello TFD";
            // 
            // ValidarClaveFormaPago
            // 
            this.ValidarClaveFormaPago.Location = new System.Drawing.Point(10, 238);
            this.ValidarClaveFormaPago.Name = "ValidarClaveFormaPago";
            this.ValidarClaveFormaPago.Size = new System.Drawing.Size(132, 18);
            this.ValidarClaveFormaPago.TabIndex = 1;
            this.ValidarClaveFormaPago.Text = "Validar forma de Pago";
            // 
            // CheckEstadoSAT
            // 
            this.CheckEstadoSAT.Location = new System.Drawing.Point(10, 142);
            this.CheckEstadoSAT.Name = "CheckEstadoSAT";
            this.CheckEstadoSAT.Size = new System.Drawing.Size(223, 18);
            this.CheckEstadoSAT.TabIndex = 1;
            this.CheckEstadoSAT.Text = "Verificar el estado del comprobante SAT.";
            // 
            // RequiereFormaYMetodoPago
            // 
            this.RequiereFormaYMetodoPago.Location = new System.Drawing.Point(10, 285);
            this.RequiereFormaYMetodoPago.Name = "RequiereFormaYMetodoPago";
            this.RequiereFormaYMetodoPago.Size = new System.Drawing.Size(249, 32);
            this.RequiereFormaYMetodoPago.TabIndex = 1;
            this.RequiereFormaYMetodoPago.Text = "<html><p>Requerir \"Forma de pago\" y \"Método de pago\" </p><p>para comprobantes de " +
    "ingreso y egreso.</p></html>";
            // 
            // CheckAddendaSin
            // 
            this.CheckAddendaSin.Location = new System.Drawing.Point(10, 166);
            this.CheckAddendaSin.Name = "CheckAddendaSin";
            this.CheckAddendaSin.Size = new System.Drawing.Size(184, 18);
            this.CheckAddendaSin.TabIndex = 1;
            this.CheckAddendaSin.Text = "Valida addendas sin Namespace.";
            // 
            // ValidaAritmetica
            // 
            this.ValidaAritmetica.Location = new System.Drawing.Point(10, 375);
            this.ValidaAritmetica.Name = "ValidaAritmetica";
            this.ValidaAritmetica.Size = new System.Drawing.Size(206, 32);
            this.ValidaAritmetica.TabIndex = 1;
            this.ValidaAritmetica.Text = "<html><p>Aritmética en porcentaje sobre el total </p><p>(configurable en opciones" +
    " avanzadas)</p></html>";
            // 
            // ValidarClaveConcepto
            // 
            this.ValidarClaveConcepto.Location = new System.Drawing.Point(10, 214);
            this.ValidarClaveConcepto.Name = "ValidarClaveConcepto";
            this.ValidarClaveConcepto.Size = new System.Drawing.Size(177, 18);
            this.ValidarClaveConcepto.TabIndex = 1;
            this.ValidarClaveConcepto.Text = "Verificar clave SAT de concepto";
            // 
            // CheckRenameFile
            // 
            this.CheckRenameFile.Location = new System.Drawing.Point(9, 65);
            this.CheckRenameFile.Name = "CheckRenameFile";
            this.CheckRenameFile.Size = new System.Drawing.Size(158, 18);
            this.CheckRenameFile.TabIndex = 1;
            this.CheckRenameFile.Text = "Renombrar archivos origen.";
            // 
            // ModoParalelo
            // 
            this.ModoParalelo.Location = new System.Drawing.Point(9, 19);
            this.ModoParalelo.Name = "ModoParalelo";
            this.ModoParalelo.Size = new System.Drawing.Size(94, 18);
            this.ModoParalelo.TabIndex = 1;
            this.ModoParalelo.Text = "Modo paralelo";
            // 
            // AsociarPDF
            // 
            this.AsociarPDF.Location = new System.Drawing.Point(9, 42);
            this.AsociarPDF.Name = "AsociarPDF";
            this.AsociarPDF.Size = new System.Drawing.Size(124, 18);
            this.AsociarPDF.TabIndex = 1;
            this.AsociarPDF.Text = "Buscar y asignar PDF";
            // 
            // groupBox2
            // 
            this.groupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox2.Controls.Add(this.CheckSoloEmisorReceptor);
            this.groupBox2.Controls.Add(this.SoloCFDIValido);
            this.groupBox2.Controls.Add(this.CheckRenameFile);
            this.groupBox2.Controls.Add(this.ModoParalelo);
            this.groupBox2.Controls.Add(this.RegistrarComprobante);
            this.groupBox2.Controls.Add(this.AsociarPDF);
            this.groupBox2.Controls.Add(this.RegistrarConceptos);
            this.groupBox2.HeaderText = "Opciones adicionales";
            this.groupBox2.Location = new System.Drawing.Point(287, 36);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(269, 200);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Opciones adicionales";
            // 
            // SoloCFDIValido
            // 
            this.SoloCFDIValido.Enabled = false;
            this.SoloCFDIValido.Location = new System.Drawing.Point(29, 134);
            this.SoloCFDIValido.Name = "SoloCFDIValido";
            this.SoloCFDIValido.Size = new System.Drawing.Size(200, 18);
            this.SoloCFDIValido.TabIndex = 2;
            this.SoloCFDIValido.Text = "Solo registrar comprobantes validos";
            // 
            // RegistrarComprobante
            // 
            this.RegistrarComprobante.Location = new System.Drawing.Point(9, 88);
            this.RegistrarComprobante.Name = "RegistrarComprobante";
            this.RegistrarComprobante.Size = new System.Drawing.Size(159, 18);
            this.RegistrarComprobante.TabIndex = 1;
            this.RegistrarComprobante.Text = "Registrar Comprobante (S3)";
            this.RegistrarComprobante.CheckStateChanged += new System.EventHandler(this.RegistrarComprobante_CheckStateChanged);
            // 
            // RegistrarConceptos
            // 
            this.RegistrarConceptos.Location = new System.Drawing.Point(29, 111);
            this.RegistrarConceptos.Name = "RegistrarConceptos";
            this.RegistrarConceptos.Size = new System.Drawing.Size(208, 18);
            this.RegistrarConceptos.TabIndex = 1;
            this.RegistrarConceptos.Text = "Registrar conceptos del comprobante";
            // 
            // groupBox3
            // 
            this.groupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox3.Controls.Add(this.CheckArticulo69B);
            this.groupBox3.HeaderText = "Artículo 69B";
            this.groupBox3.Location = new System.Drawing.Point(287, 236);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(269, 92);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Artículo 69B";
            // 
            // CheckArticulo69B
            // 
            this.CheckArticulo69B.Location = new System.Drawing.Point(7, 17);
            this.CheckArticulo69B.Name = "CheckArticulo69B";
            this.CheckArticulo69B.Size = new System.Drawing.Size(251, 61);
            this.CheckArticulo69B.TabIndex = 2;
            this.CheckArticulo69B.Text = resources.GetString("CheckArticulo69B.Text");
            // 
            // groupBox4
            // 
            this.groupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox4.Controls.Add(this.VerificarEmisorCP);
            this.groupBox4.Controls.Add(this.LugarExpedicion32B);
            this.groupBox4.Controls.Add(this.LugarExpedicion32);
            this.groupBox4.HeaderText = "Lugar de Expedición";
            this.groupBox4.Location = new System.Drawing.Point(287, 334);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(269, 124);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Lugar de Expedición";
            // 
            // VerificarEmisorCP
            // 
            this.VerificarEmisorCP.Location = new System.Drawing.Point(9, 19);
            this.VerificarEmisorCP.Name = "VerificarEmisorCP";
            this.VerificarEmisorCP.Size = new System.Drawing.Size(239, 32);
            this.VerificarEmisorCP.TabIndex = 3;
            this.VerificarEmisorCP.Text = "<html><p>Verificar Lugar de expedición (Codigo Postal) </p><p>a partir de la vers" +
    "ión 3.3</p></html>";
            // 
            // LugarExpedicion32B
            // 
            this.LugarExpedicion32B.Location = new System.Drawing.Point(9, 49);
            this.LugarExpedicion32B.Name = "LugarExpedicion32B";
            this.LugarExpedicion32B.Size = new System.Drawing.Size(214, 46);
            this.LugarExpedicion32B.TabIndex = 3;
            this.LugarExpedicion32B.Text = "<html><p>Expresión regular para la comprobación </p><p>del codigo postal ó lugar " +
    "de expedición,</p><p> para versión CFDI 3.2</p></html>";
            this.LugarExpedicion32B.CheckStateChanged += new System.EventHandler(this.LugarExpedicion32B_CheckStateChanged);
            // 
            // LugarExpedicion32
            // 
            this.LugarExpedicion32.Enabled = false;
            this.LugarExpedicion32.Location = new System.Drawing.Point(9, 99);
            this.LugarExpedicion32.MaxLength = 80;
            this.LugarExpedicion32.Name = "LugarExpedicion32";
            this.LugarExpedicion32.Size = new System.Drawing.Size(248, 20);
            this.LugarExpedicion32.TabIndex = 1;
            // 
            // Guardar
            // 
            this.Guardar.Location = new System.Drawing.Point(562, 36);
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(75, 23);
            this.Guardar.TabIndex = 3;
            this.Guardar.Text = "Guardar";
            this.Guardar.Click += new System.EventHandler(this.Guardar_Click1);
            // 
            // Cerrar
            // 
            this.Cerrar.Location = new System.Drawing.Point(562, 65);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(75, 23);
            this.Cerrar.TabIndex = 3;
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.Click += new System.EventHandler(this.Cerrar_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(15, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(402, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "Versiones que puede validar con esta herramienta: CFDI 3.2, CFDI 3.3 y CFDI 4.0";
            // 
            // CheckSoloEmisorReceptor
            // 
            this.CheckSoloEmisorReceptor.AutoSize = false;
            this.CheckSoloEmisorReceptor.Location = new System.Drawing.Point(9, 158);
            this.CheckSoloEmisorReceptor.Name = "CheckSoloEmisorReceptor";
            this.CheckSoloEmisorReceptor.Size = new System.Drawing.Size(239, 36);
            this.CheckSoloEmisorReceptor.TabIndex = 3;
            this.CheckSoloEmisorReceptor.Text = "Aceptar solo comprobantes relacionados al RFC registrado.";
            this.CheckSoloEmisorReceptor.TextWrap = true;
            // 
            // ConfiguracionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 461);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Cerrar);
            this.Controls.Add(this.Guardar);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfiguracionForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configuración";
            this.Load += new System.EventHandler(this.ConfiguracionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ObtenerEmisor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckSchema)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckUTF8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckUsoCFDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckSelloCFDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidarClaveMetodoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckSelloTFD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DownloadCertificado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidarClaveFormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEstadoSAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequiereFormaYMetodoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckAddendaSin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidaAritmetica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidarClaveConcepto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckRenameFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ModoParalelo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AsociarPDF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SoloCFDIValido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegistrarComprobante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegistrarConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox3)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckArticulo69B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox4)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VerificarEmisorCP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LugarExpedicion32B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LugarExpedicion32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Guardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckSoloEmisorReceptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        private Telerik.WinControls.UI.RadCheckBox CheckRenameFile;
        private Telerik.WinControls.UI.RadCheckBox CheckEstadoSAT;
        private Telerik.WinControls.UI.RadCheckBox ModoParalelo;
        private Telerik.WinControls.UI.RadCheckBox AsociarPDF;
        private Telerik.WinControls.UI.RadCheckBox CheckSchema;
        private Telerik.WinControls.UI.RadGroupBox groupBox2;
        private Telerik.WinControls.UI.RadCheckBox CheckUsoCFDI;
        private Telerik.WinControls.UI.RadCheckBox ValidarClaveConcepto;
        private Telerik.WinControls.UI.RadCheckBox ValidarClaveFormaPago;
        private Telerik.WinControls.UI.RadCheckBox DownloadCertificado;
        private Telerik.WinControls.UI.RadCheckBox ValidarClaveMetodoPago;
        private Telerik.WinControls.UI.RadGroupBox groupBox3;
        private Telerik.WinControls.UI.RadCheckBox CheckArticulo69B;
        private Telerik.WinControls.UI.RadGroupBox groupBox4;
        private Telerik.WinControls.UI.RadCheckBox LugarExpedicion32B;
        private Telerik.WinControls.UI.RadTextBox LugarExpedicion32;
        private Telerik.WinControls.UI.RadButton Guardar;
        private Telerik.WinControls.UI.RadButton Cerrar;
        private Telerik.WinControls.UI.RadCheckBox CheckUTF8;
        private Telerik.WinControls.UI.RadCheckBox CheckSelloCFDI;
        private Telerik.WinControls.UI.RadCheckBox ValidaAritmetica;
        private Telerik.WinControls.UI.RadCheckBox CheckAddendaSin;
        private Telerik.WinControls.UI.RadCheckBox RegistrarConceptos;
        private Telerik.WinControls.UI.RadLabel label1;
        private Telerik.WinControls.UI.RadCheckBox RequiereFormaYMetodoPago;
        private Telerik.WinControls.UI.RadCheckBox ObtenerEmisor;
        private Telerik.WinControls.UI.RadCheckBox CheckSelloTFD;
        private Telerik.WinControls.UI.RadCheckBox VerificarEmisorCP;
        private Telerik.WinControls.UI.RadCheckBox RegistrarComprobante;
        private Telerik.WinControls.UI.RadCheckBox SoloCFDIValido;
        private Telerik.WinControls.UI.RadCheckBox CheckSoloEmisorReceptor;
    }
}