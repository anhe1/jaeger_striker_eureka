﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.Aplication.Validador.Contracts;

namespace Jaeger.UI.Validador.Forms {
    public partial class ConfiguracionForm : RadForm {
        protected internal IConfiguration configuracion;
        public event EventHandler<IConfiguration> Guardar_Click;

        public void OnGuardarClick(IConfiguration e) {
            if (this.Guardar_Click != null)
                this.Guardar_Click(this, e);
        }

        public ConfiguracionForm(IConfiguration configuracion) {
            InitializeComponent();
            this.configuracion = configuracion;
        }

        private void ConfiguracionForm_Load(object sender, EventArgs e) {
            if (configuracion == null) {
                this.configuracion = new Configuration();
            }
            this.CreateBinding();
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Guardar_Click1(object sender, EventArgs e) {
            this.OnGuardarClick(this.configuracion);
        }

        private void CreateBinding() {
            this.CheckSchema.DataBindings.Clear();
            this.CheckSchema.DataBindings.Add("Checked", this.configuracion, "CheckSchema", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CheckUTF8.DataBindings.Clear();
            this.CheckUTF8.DataBindings.Add("Checked", this.configuracion, "CheckUTF8", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CheckSelloCFDI.DataBindings.Clear();
            this.CheckSelloCFDI.DataBindings.Add("Checked", this.configuracion, "CheckSelloCFDI", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CheckSelloTFD.DataBindings.Clear();
            this.CheckSelloTFD.DataBindings.Add("Checked", this.configuracion, "CheckSelloTFD", true, DataSourceUpdateMode.OnPropertyChanged);

            this.DownloadCertificado.DataBindings.Clear();
            this.DownloadCertificado.DataBindings.Add("Checked", this.configuracion, "DescargaCertificado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CheckEstadoSAT.DataBindings.Clear();
            this.CheckEstadoSAT.DataBindings.Add("Checked", this.configuracion, "EstadoSAT", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CheckAddendaSin.DataBindings.Clear();
            this.CheckAddendaSin.DataBindings.Add("Checked", this.configuracion, "RemoverAddenda", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CheckUsoCFDI.DataBindings.Clear();
            this.CheckUsoCFDI.DataBindings.Add("Checked", this.configuracion, "CheckUsoCFDI", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ValidarClaveConcepto.DataBindings.Clear();
            this.ValidarClaveConcepto.DataBindings.Add("Checked", this.configuracion, "ClavesConcepto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ValidarClaveFormaPago.DataBindings.Clear();
            this.ValidarClaveFormaPago.DataBindings.Add("Checked", this.configuracion, "FormaPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ValidarClaveMetodoPago.DataBindings.Clear();
            this.ValidarClaveMetodoPago.DataBindings.Add("Checked", this.configuracion, "MetodoPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RequiereFormaYMetodoPago.DataBindings.Clear();
            this.RequiereFormaYMetodoPago.DataBindings.Add("Checked", this.configuracion, "Check1", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ObtenerEmisor.DataBindings.Clear();
            this.ObtenerEmisor.DataBindings.Add("Checked", this.configuracion, "GetEmisor", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.ValidaAritmetica.DataBindings.Clear();
            this.ValidaAritmetica.Enabled = false;
            //this.ValidaAritmetica.DataBindings.Add("Checked", this.configuracion, "ValidaAritmetica", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ModoParalelo.DataBindings.Clear();
            this.ModoParalelo.DataBindings.Add("Checked", this.configuracion, "ModoParalelo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.AsociarPDF.DataBindings.Clear();
            this.AsociarPDF.DataBindings.Add("Checked", this.configuracion, "SearchPDF", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CheckRenameFile.DataBindings.Clear();
            this.CheckRenameFile.Enabled = false;
            this.CheckRenameFile.DataBindings.Add("Checked", this.configuracion, "RenameFiles", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RegistrarComprobante.DataBindings.Clear();
            this.RegistrarComprobante.DataBindings.Add("Checked", this.configuracion, "Registrar", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RegistrarConceptos.DataBindings.Clear();
            this.RegistrarConceptos.DataBindings.Add("Checked", this.configuracion, "RegistrarConceptos", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CheckArticulo69B.DataBindings.Clear();
            this.CheckArticulo69B.DataBindings.Add("Checked", this.configuracion, "Articulo69B", true, DataSourceUpdateMode.OnPropertyChanged);

            this.VerificarEmisorCP.DataBindings.Clear();
            this.VerificarEmisorCP.Enabled = false;
//            this.VerificarEmisorCP.DataBindings.Add("Checked", this.configuracion, "VerificarEmisorCP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.LugarExpedicion32B.DataBindings.Clear();
            this.LugarExpedicion32B.Enabled = false;
            //this.LugarExpedicion32B.DataBindings.Add("Checked", this.configuracion, "LugarExpedicion32B", true, DataSourceUpdateMode.OnPropertyChanged);

            this.LugarExpedicion32.DataBindings.Clear();
            this.LugarExpedicion32.Enabled = false;
            //this.LugarExpedicion32.DataBindings.Add("Text", this.configuracion, "LugarExpedicion32", true, DataSourceUpdateMode.OnPropertyChanged);

            this.SoloCFDIValido.DataBindings.Clear();
            this.SoloCFDIValido.DataBindings.Add("Checked", this.configuracion, "OnlyVerified", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CheckSoloEmisorReceptor.DataBindings.Clear();
            this.CheckSoloEmisorReceptor.DataBindings.Add("Checked", this.configuracion, "SoloEmisorReceptor", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void RegistrarComprobante_CheckStateChanged(object sender, EventArgs e) {
            if (RegistrarComprobante.Checked) {
                this.RegistrarConceptos.Enabled = true;
                this.SoloCFDIValido.Enabled = true;
            } else {
                this.RegistrarConceptos.Enabled = false;
                this.RegistrarConceptos.Checked = false;
                this.SoloCFDIValido.Enabled = false;
                this.SoloCFDIValido.Checked = false;
            }
        }

        private void LugarExpedicion32B_CheckStateChanged(object sender, EventArgs e) {
            if (LugarExpedicion32B.Checked) {
                this.LugarExpedicion32.Enabled = true;
            } else {
                this.LugarExpedicion32.Enabled = false;
            }
        }
    }
}
