﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Util.Services;
using Jaeger.Domain.Comprobante.Entities.Complemento;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Base.Services;

namespace Jaeger.UI.Validador.Forms {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private EmbeddedResources horizon = new EmbeddedResources("Jaeger.Domain.Comprobante");

        public ReporteForm(string rfc, string razonSocial) : base(rfc, razonSocial) {
        }

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(ComprobanteValidacionPrinter)) {
                this.CrearReporteValidacion();
            } else if (this.CurrentObject.GetType() == typeof(ComprobanteListadoPrinter)) {
                this.ComprobantesListadoReporte();
            } else if (this.CurrentObject.GetType() == typeof(ComprobanteFiscalPrinter)) {
                this.ComprobanteFiscalReporte();
            }
        }

        private void CrearReporteValidacion() {
            var current = (ComprobanteValidacionPrinter)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Comprobante.Reports.ComprobanteValidacion35Report.rdlc");
            this.SetDisplayName("Reporte Validación");
            var d = DbConvert.ConvertToDataTable<ComprobanteValidacionPrinter>(new List<ComprobanteValidacionPrinter>() { current });
            this.SetDataSource("Validacion", d);
            var d1 = DbConvert.ConvertToDataTable<PropertyValidate>(current.Resultados);
            this.SetDataSource("Resultados", d1);
            this.Finalizar();
        }

        private void ComprobantesListadoReporte() {
            var current = (ComprobanteListadoPrinter)this.CurrentObject;
            if (current.Vertical) {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Comprobante.Reports.ComprobantesListadoVReporte.rdlc");
            } else {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Comprobante.Reports.ComprobantesListadoHReporte.rdlc");
            }
            this.SetDisplayName("Reporte 1");
            var d = DbConvert.ConvertToDataTable<ComprobanteFiscalDetailSingleModel>(current.Conceptos);
            this.Procesar();
            this.SetDataSource("Comprobantes", d);
            this.Finalizar();
        }

        private void ComprobanteFiscalReporte() {
            var current = (ComprobanteFiscalPrinter)this.CurrentObject;
            this.SetDisplayName("Comprobante");

            if (current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Pagos) {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Comprobante.Reports.CFDIv0PagoReporte.rdlc");
                var p1 = DbConvert.ConvertToDataTable<ComplementoPagoDetailModel>(current.RecepcionPago);
                var docs = DbConvert.ConvertToDataTable<PagosPagoDoctoRelacionadoDetailModel>(current.RecepcionPago[0].DoctoRelacionados);
                this.SetDataSource("Pago", p1);
                this.SetDataSource("DoctoRelacionado", docs);
            } else if (current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Nomina) {

            } else if ((current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Ingreso | current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Egreso) && current.InformacionGlobal == null && current.CartaPorte == null) {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Comprobante.Reports.CFDIv0Reporte.rdlc");
            } else if (current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Ingreso && current.InformacionGlobal != null && current.Documento != "porte") {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Comprobante.Reports.CFDIv0GlobalReporte.rdlc");
                var g = DbConvert.ConvertToDataTable<ComprobanteInformacionGlobalDetailModel>(new List<ComprobanteInformacionGlobalDetailModel>() { current.InformacionGlobal });
                this.SetDataSource("InformacionGlobal", g);
            } else if (current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Ingreso && current.Documento == "porte") {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Comprobante.Reports.CFDIv0CartaPorteReporte.rdlc");
                var g = DbConvert.ConvertToDataTable<Domain.Comprobante.Entities.Complemento.CartaPorte.CartaPorteDetailModel>(new List<Domain.Comprobante.Entities.Complemento.CartaPorte.CartaPorteDetailModel>() { current.CartaPorte });
                this.SetDataSource("CartaPorte", g);
                this.SetDataSource("Ubicaciones", DbConvert.ConvertToDataTable(current.CartaPorte.Ubicaciones));
                this.SetDataSource("FigurasTransporte", DbConvert.ConvertToDataTable(current.CartaPorte.FiguraTransporte));
                this.SetDataSource("Mercancia", DbConvert.ConvertToDataTable<Domain.Comprobante.Entities.Complemento.CartaPorte.CartaPorteMercancias>(new List<Domain.Comprobante.Entities.Complemento.CartaPorte.CartaPorteMercancias> { current.CartaPorte.Mercancias }));
                this.SetDataSource("MercanciasMercancia", DbConvert.ConvertToDataTable(current.CartaPorte.Mercancias.Mercancia));

                this.SetParameter("ConfigVehicular", current.CartaPorte.Mercancias.Autotransporte.IdentificacionVehicular.ConfigVehicular);
                this.SetParameter("PlacaVM", current.CartaPorte.Mercancias.Autotransporte.IdentificacionVehicular.PlacaVM);
                this.SetParameter("AnioModeloVM", current.CartaPorte.Mercancias.Autotransporte.IdentificacionVehicular.AnioModeloVM.ToString());

                this.SetParameter("PermSCT", current.CartaPorte.Mercancias.Autotransporte.PermSCT);
                this.SetParameter("NumPermisoSCT", current.CartaPorte.Mercancias.Autotransporte.NumPermisoSCT);


                this.SetParameter("AseguraRespCivil", current.CartaPorte.Mercancias.Autotransporte.Seguros.AseguraRespCivil);
                this.SetParameter("PolizaRespCivil", current.CartaPorte.Mercancias.Autotransporte.Seguros.PolizaRespCivil);
                this.SetParameter("AseguraMedAmbiente", current.CartaPorte.Mercancias.Autotransporte.Seguros.AseguraMedAmbiente);
                this.SetParameter("PolizaMedAmbiente", current.CartaPorte.Mercancias.Autotransporte.Seguros.PolizaMedAmbiente);
                this.SetParameter("AseguraCarga", current.CartaPorte.Mercancias.Autotransporte.Seguros.AseguraCarga);
                this.SetParameter("PolizaCarga", current.CartaPorte.Mercancias.Autotransporte.Seguros.PolizaCarga);
            }

            var d1 = DbConvert.ConvertToDataTable<ComprobanteFiscalPrinter>(new List<ComprobanteFiscalPrinter>() { current });
            var d = DbConvert.ConvertToDataTable<ComprobanteConceptoDetailModel>(current.Conceptos);
            var d3 = DbConvert.ConvertToDataTable<ComplementoTimbreFiscal>(new List<ComplementoTimbreFiscal>() { current.TimbreFiscal });
            
            this.Procesar();
            this.SetDataSource("Comprobante", d1);
            this.SetDataSource("Conceptos", d);
            this.SetDataSource("TimbreFiscal", d3);

            if (current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Pagos) {

            }
            this.Finalizar();
        }
    }
}
