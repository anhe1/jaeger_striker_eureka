﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.Services;

namespace Jaeger.UI.Validador.Forms {
    /// <summary>
    /// barra de herramientas del formulario de validacion de comprobantes
    /// </summary>
    public partial class TbValidadorControl : UserControl {
        #region declaraciones
        protected internal ICarpetaRecienteService carpetaReciente;
        protected internal Aplication.Validador.Builder.Administrador Administrador;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public TbValidadorControl() {
            InitializeComponent();
        }

        private void TbValidadorControl_Load(object sender, EventArgs e) {
            this.HostCarpeta.HostedItem = this.TCarpeta.MultiColumnComboBoxElement;
            this.carpetaReciente = new CarpetaRecienteService().WithLocalFolder();
            this.carpetaReciente.Load();
            this.TCarpeta.DataSource = this.carpetaReciente.Items;
            this.TCarpeta.EditorControl.CommandCellClick += new CommandCellClickEventHandler(this.CboCarpetas_CommandCellClick);
            this.TCarpeta.EditorControl.ShowColumnHeaders = false;
            this.TCarpeta.EditorControl.ShowRowHeaderColumn = false;
            this.Carpetas.Click += Carpetas_Click;
        }

        #region barra de herramientas
        private void Carpetas_Click(object sender, EventArgs e) {
            var openfolder = new FolderBrowserDialog { Description = "Selecciona carpeta para búsqueda de comprobantes" };
            if (openfolder.ShowDialog(this) == DialogResult.OK) {
                this.carpetaReciente.Add(new CarpetaModel(openfolder.SelectedPath));
                this.carpetaReciente.Save();
                this.TCarpeta.DataSource = null;
                this.TCarpeta.DataSource = this.carpetaReciente.Items;
                this.TCarpeta.Text = openfolder.SelectedPath;
                if (System.IO.Directory.Exists(openfolder.SelectedPath)) {
                    this.TActualizar.PerformClick();
                }
            }
        }

        private void CboCarpetas_CommandCellClick(object sender, GridViewCellEventArgs e) {
            if (e.Column.Name == "Remover") {
                this.carpetaReciente.Delete(e.RowIndex);
                this.carpetaReciente.Save();
                this.TCarpeta.DataSource = null;
                this.TCarpeta.DataSource = this.carpetaReciente.Items;
            }
        }
        #endregion
    }
}
