﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.IO;
using System.Windows.Forms;
using Ionic.Zip;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Services;

namespace Jaeger.UI.Forms {
    public partial class LogOperationsControl : UserControl {

        public LogOperationsControl() {
            InitializeComponent();
        }

        private void LogOperationsControl_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.txtPath.Text = Properties.Settings.Default.PathLog;
            this.textBox3.Text = Properties.Settings.Default.BucketName;
        }

        private void ButtonUploadLog_Click(object sender, EventArgs e) {
            var openFolder = new FolderBrowserDialog() { Description = "Selecciona ruta" };
            if (this.txtPath.Text != string.Empty) {
                openFolder.SelectedPath = this.txtPath.Text;
            }
            if (openFolder.ShowDialog(this) != DialogResult.OK)
                return;

            if (Directory.Exists(openFolder.SelectedPath) == false) {
                MessageBox.Show(this, Properties.Resources.Message_Error_RutaNoValida, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            txtPath.Text = openFolder.SelectedPath;

            // Take a snapshot of the file system.  
            string startFolder = openFolder.SelectedPath;

            // Used in WriteLine to trim output lines.  
            int trimLength = startFolder.Length;

            // Take a snapshot of the file system.  
            var dir = new DirectoryInfo(startFolder);

            // This method assumes that the application has discovery permissions  
            // for all folders under the specified path.  
            IEnumerable<FileInfo> fileList = dir.GetFiles("*.zip", SearchOption.AllDirectories);
            txtFilesCount.Text = fileList.Count().ToString();
            numericPages.Value = 25;
            this.TextBoxOutput.Text = this.TextBoxOutput.Text + "Total archivos: " + fileList.Count().ToString() + Environment.NewLine;
        }

        private void numericPages_ValueChanged(object sender, EventArgs e) {
            this.label24.Text = "Estimate: " + Math.Ceiling((new decimal(int.Parse(txtFilesCount.Text)) / (int)numericPages.Value)).ToString();
        }

        private void ButtonCreateZIP_Click(object sender, EventArgs e) {
            if (this.txtFilesCount.Text == string.Empty) {
                this.ButtonUploadLog_Click(this, new EventArgs());
            }
            using (var espera = new WaitingForm(this.CrearZIP)) {
                espera.Text = "Creando...";
                espera.ShowDialog(this);
            }

            using (var espera = new WaitingForm(this.Up)) {
                espera.Text = "Cargando archivos ...";
                espera.ShowDialog(this);
            }
        }

        private void CrearZIP() {
            List<string> resulta = new List<string>();
            this.TextBoxOutput.Text = string.Empty;
            // Take a snapshot of the file system.  
            DirectoryInfo dir = new DirectoryInfo(this.txtPath.Text);
            IEnumerable<FileInfo> fileList = dir.GetFiles("*.zip", SearchOption.AllDirectories);
            var r = _30s.Chunk(fileList, (int)numericPages.Value);
            this.TextBoxOutput.Text = string.Empty;
            int numgrup = 0;
            foreach (var item in r) {
                numgrup++;
                var archivo = this.txtPath.Text + DateTime.Now.ToString("ddMMyyhhmmss") + "_" + numgrup.ToString("00") + ".zip";
                this.TextBoxOutput.Text = this.TextBoxOutput.Text + archivo + Environment.NewLine;
                using (var zip = new ZipFile()) {
                    foreach (var f in item) {
                        this.TextBoxOutput.Text = this.TextBoxOutput.Text + f.FullName + Environment.NewLine;
                        ZipEntry e = zip.AddEntry(f.Name, File.ReadAllBytes(f.FullName));
                    }
                    zip.Comment = "create by StrikerPhoenix.Beta";
                    zip.Save(archivo);
                    resulta.Add(archivo);
                }
            }
            this.TextBoxOutput.Text = this.TextBoxOutput.Text + "Total archivos: " + fileList.Count().ToString() + Environment.NewLine;
            this.TextBoxOutput.Text = this.TextBoxOutput.Text + "Total grupos: " + r.Count().ToString() + Environment.NewLine;
            this.ButtonCreateZIP.Tag = resulta;
        }

        private void Up() {
            List<string> resulta = this.ButtonCreateZIP.Tag as List<string>;
            if (resulta != null) {
                for (int i = 0; i < resulta.Count; i++) {
                    this.TextBoxOutput.Text = this.TextBoxOutput.Text + Helper.editaS3.Upload(resulta[i], Path.GetFileName(resulta[i]), "", this.textBox3.Text, Helper.Region, false) + Environment.NewLine;
                }
            }
        }

        private void ButtonUnzipSource_Click(object sender, EventArgs e) {
            var openFolder = new FolderBrowserDialog() { Description = "Selecciona la carpeta de origen" };

            if (openFolder.ShowDialog(this) != DialogResult.OK)
                return;

            this.TextBoxUnzipSourceFolder.Text = openFolder.SelectedPath;
        }

        private void ButtonUnzipTarget_Click(object sender, EventArgs e) {
            var openFolder = new FolderBrowserDialog() { Description = "Selecciona la carpeta destino" };

            if (openFolder.ShowDialog(this) != DialogResult.OK)
                return;
            this.TextBoxUnzipTargetFolder.Text = openFolder.SelectedPath;
        }

        private void ButtonUnZipStart_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.UnzipFolder)) {
                espera.Text = "Procesando...";
                espera.ShowDialog(this);
            }
        }

        private void UnzipFolder() {
            if (Directory.Exists(this.TextBoxUnzipTargetFolder.Text) == false) {
                Directory.CreateDirectory(this.TextBoxUnzipTargetFolder.Text);
            }

            DirectoryInfo dir = new DirectoryInfo(this.TextBoxUnzipSourceFolder.Text);
            IEnumerable<FileInfo> fileList = dir.GetFiles("*.zip", SearchOption.TopDirectoryOnly);
            foreach (var item in fileList) {
                var options = new ReadOptions { StatusMessageWriter = System.Console.Out };
                using (ZipFile zip = ZipFile.Read(item.FullName, options)) {
                    zip.ExtractAll(this.TextBoxUnzipTargetFolder.Text, ExtractExistingFileAction.OverwriteSilently);
                }
            }
        }

        private void AgruparLogs() {
            // Take a snapshot of the file system.  
            string startFolder = TextBoxUnzipSourceFolder.Text;//            @"D:\bitbucket\Log_Revisar";

            // Used in WriteLine to trim output lines.  
            int trimLength = startFolder.Length;

            var dir = new DirectoryInfo(startFolder);
            IEnumerable<FileInfo> fileList = dir.GetFiles("*.json", SearchOption.TopDirectoryOnly);

            // Create the query.  
            var queryGroupByExt =
                from file in fileList
                group file by file.Name.Substring(12, 4) into fileGroup
                orderby fileGroup.Key
                select fileGroup;

            // Iterate through the outer collection of groups.  
            foreach (var filegroup in queryGroupByExt) {
                Console.WriteLine(filegroup.Key == string.Empty ? "[none]" : filegroup.Key);
                var directorio = @"D:\bitbucket\Log_Revisar\" + filegroup.Key;
                Directory.CreateDirectory(directorio);
                foreach (var f in filegroup) {
                    Console.WriteLine("\t{0}", f.FullName.Substring(trimLength));
                    f.MoveTo(directorio + "\\" + f.Name);
                }
            }
        }
    }
}
