﻿namespace Jaeger.UI.Forms {
    partial class DownloadControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ButtonDownloadFile1 = new System.Windows.Forms.Button();
            this.ButtonDownloadBrowse1 = new System.Windows.Forms.Button();
            this.TextBoxDownloadFileName1 = new System.Windows.Forms.TextBox();
            this.TextBoxDownloadURL = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ButtonDownloadBrowse = new System.Windows.Forms.Button();
            this.Label19 = new System.Windows.Forms.Label();
            this.ButtonDownloadFile = new System.Windows.Forms.Button();
            this.TextBoxDownloadFileName = new System.Windows.Forms.TextBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.TextBoxDownloadKeyName = new System.Windows.Forms.TextBox();
            this.Label11 = new System.Windows.Forms.Label();
            this.TextBoxDownloadBucketName = new System.Windows.Forms.TextBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ButtonDownloadFile1);
            this.groupBox4.Controls.Add(this.ButtonDownloadBrowse1);
            this.groupBox4.Controls.Add(this.TextBoxDownloadFileName1);
            this.groupBox4.Controls.Add(this.TextBoxDownloadURL);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(0, 166);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(454, 107);
            this.groupBox4.TabIndex = 54;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Option 2";
            // 
            // ButtonDownloadFile1
            // 
            this.ButtonDownloadFile1.Location = new System.Drawing.Point(105, 71);
            this.ButtonDownloadFile1.Name = "ButtonDownloadFile1";
            this.ButtonDownloadFile1.Size = new System.Drawing.Size(97, 23);
            this.ButtonDownloadFile1.TabIndex = 61;
            this.ButtonDownloadFile1.Text = "Download File";
            this.ButtonDownloadFile1.UseVisualStyleBackColor = true;
            // 
            // ButtonDownloadBrowse1
            // 
            this.ButtonDownloadBrowse1.Location = new System.Drawing.Point(361, 43);
            this.ButtonDownloadBrowse1.Name = "ButtonDownloadBrowse1";
            this.ButtonDownloadBrowse1.Size = new System.Drawing.Size(75, 23);
            this.ButtonDownloadBrowse1.TabIndex = 60;
            this.ButtonDownloadBrowse1.Text = "Browse";
            this.ButtonDownloadBrowse1.UseVisualStyleBackColor = true;
            // 
            // TextBoxDownloadFileName1
            // 
            this.TextBoxDownloadFileName1.Location = new System.Drawing.Point(105, 45);
            this.TextBoxDownloadFileName1.Name = "TextBoxDownloadFileName1";
            this.TextBoxDownloadFileName1.Size = new System.Drawing.Size(250, 20);
            this.TextBoxDownloadFileName1.TabIndex = 57;
            // 
            // TextBoxDownloadURL
            // 
            this.TextBoxDownloadURL.Location = new System.Drawing.Point(105, 19);
            this.TextBoxDownloadURL.Name = "TextBoxDownloadURL";
            this.TextBoxDownloadURL.Size = new System.Drawing.Size(250, 20);
            this.TextBoxDownloadURL.TabIndex = 56;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 58;
            this.label5.Text = "URL:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 13);
            this.label13.TabIndex = 59;
            this.label13.Text = "Local File Name";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ButtonDownloadBrowse);
            this.groupBox3.Controls.Add(this.Label19);
            this.groupBox3.Controls.Add(this.ButtonDownloadFile);
            this.groupBox3.Controls.Add(this.TextBoxDownloadFileName);
            this.groupBox3.Controls.Add(this.Label10);
            this.groupBox3.Controls.Add(this.TextBoxDownloadKeyName);
            this.groupBox3.Controls.Add(this.Label11);
            this.groupBox3.Controls.Add(this.TextBoxDownloadBucketName);
            this.groupBox3.Controls.Add(this.Label12);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(454, 166);
            this.groupBox3.TabIndex = 53;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Option 1";
            // 
            // ButtonDownloadBrowse
            // 
            this.ButtonDownloadBrowse.Location = new System.Drawing.Point(361, 96);
            this.ButtonDownloadBrowse.Name = "ButtonDownloadBrowse";
            this.ButtonDownloadBrowse.Size = new System.Drawing.Size(75, 23);
            this.ButtonDownloadBrowse.TabIndex = 55;
            this.ButtonDownloadBrowse.Text = "Browse";
            this.ButtonDownloadBrowse.UseVisualStyleBackColor = true;
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Location = new System.Drawing.Point(15, 22);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(200, 13);
            this.Label19.TabIndex = 50;
            this.Label19.Text = "Use the following form to download a file.";
            // 
            // ButtonDownloadFile
            // 
            this.ButtonDownloadFile.Location = new System.Drawing.Point(105, 133);
            this.ButtonDownloadFile.Name = "ButtonDownloadFile";
            this.ButtonDownloadFile.Size = new System.Drawing.Size(97, 23);
            this.ButtonDownloadFile.TabIndex = 46;
            this.ButtonDownloadFile.Text = "Download File";
            this.ButtonDownloadFile.UseVisualStyleBackColor = true;
            // 
            // TextBoxDownloadFileName
            // 
            this.TextBoxDownloadFileName.Location = new System.Drawing.Point(105, 98);
            this.TextBoxDownloadFileName.Name = "TextBoxDownloadFileName";
            this.TextBoxDownloadFileName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxDownloadFileName.TabIndex = 45;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(15, 49);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(72, 13);
            this.Label10.TabIndex = 47;
            this.Label10.Text = "Bucket Name";
            // 
            // TextBoxDownloadKeyName
            // 
            this.TextBoxDownloadKeyName.Location = new System.Drawing.Point(105, 72);
            this.TextBoxDownloadKeyName.Name = "TextBoxDownloadKeyName";
            this.TextBoxDownloadKeyName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxDownloadKeyName.TabIndex = 44;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(15, 75);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(56, 13);
            this.Label11.TabIndex = 48;
            this.Label11.Text = "Key Name";
            // 
            // TextBoxDownloadBucketName
            // 
            this.TextBoxDownloadBucketName.Location = new System.Drawing.Point(105, 46);
            this.TextBoxDownloadBucketName.Name = "TextBoxDownloadBucketName";
            this.TextBoxDownloadBucketName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxDownloadBucketName.TabIndex = 43;
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(16, 101);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(83, 13);
            this.Label12.TabIndex = 49;
            this.Label12.Text = "Local File Name";
            // 
            // DownloadControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Name = "DownloadControl";
            this.Size = new System.Drawing.Size(454, 277);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        internal System.Windows.Forms.Button ButtonDownloadFile1;
        internal System.Windows.Forms.Button ButtonDownloadBrowse1;
        internal System.Windows.Forms.TextBox TextBoxDownloadFileName1;
        internal System.Windows.Forms.TextBox TextBoxDownloadURL;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox3;
        internal System.Windows.Forms.Button ButtonDownloadBrowse;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.Button ButtonDownloadFile;
        internal System.Windows.Forms.TextBox TextBoxDownloadFileName;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.TextBox TextBoxDownloadKeyName;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.TextBox TextBoxDownloadBucketName;
        internal System.Windows.Forms.Label Label12;
    }
}
