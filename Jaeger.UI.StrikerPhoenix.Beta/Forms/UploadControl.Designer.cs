﻿namespace Jaeger.UI.Forms {
    partial class UploadControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.TextBoxUploadBucketName = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.ComboBoxStorageClass = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Label22 = new System.Windows.Forms.Label();
            this.Label21 = new System.Windows.Forms.Label();
            this.CheckBoxUploadMakePublic = new System.Windows.Forms.CheckBox();
            this.TextBoxUploadContentType = new System.Windows.Forms.TextBox();
            this.Label20 = new System.Windows.Forms.Label();
            this.Label18 = new System.Windows.Forms.Label();
            this.ButtonUploadBrowse = new System.Windows.Forms.Button();
            this.TextBoxUploadKeyName = new System.Windows.Forms.TextBox();
            this.TextBoxUploadFileName = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.ButtonUploadFile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextBoxUploadBucketName
            // 
            this.TextBoxUploadBucketName.Location = new System.Drawing.Point(98, 65);
            this.TextBoxUploadBucketName.Name = "TextBoxUploadBucketName";
            this.TextBoxUploadBucketName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadBucketName.TabIndex = 85;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(9, 68);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(72, 13);
            this.Label8.TabIndex = 86;
            this.Label8.Text = "Bucket Name";
            // 
            // ComboBoxStorageClass
            // 
            this.ComboBoxStorageClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxStorageClass.FormattingEnabled = true;
            this.ComboBoxStorageClass.Items.AddRange(new object[] {
            "STANDARD",
            "REDUCED_REDUNDANCY"});
            this.ComboBoxStorageClass.Location = new System.Drawing.Point(98, 171);
            this.ComboBoxStorageClass.Name = "ComboBoxStorageClass";
            this.ComboBoxStorageClass.Size = new System.Drawing.Size(163, 21);
            this.ComboBoxStorageClass.TabIndex = 84;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 83;
            this.label4.Text = "Storage Class ";
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Location = new System.Drawing.Point(351, 120);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(52, 13);
            this.Label22.TabIndex = 82;
            this.Label22.Text = "(Optional)";
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Location = new System.Drawing.Point(9, 147);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(66, 13);
            this.Label21.TabIndex = 80;
            this.Label21.Text = "Make Public";
            // 
            // CheckBoxUploadMakePublic
            // 
            this.CheckBoxUploadMakePublic.AutoSize = true;
            this.CheckBoxUploadMakePublic.Location = new System.Drawing.Point(98, 148);
            this.CheckBoxUploadMakePublic.Name = "CheckBoxUploadMakePublic";
            this.CheckBoxUploadMakePublic.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxUploadMakePublic.TabIndex = 79;
            this.CheckBoxUploadMakePublic.UseVisualStyleBackColor = true;
            // 
            // TextBoxUploadContentType
            // 
            this.TextBoxUploadContentType.Location = new System.Drawing.Point(98, 117);
            this.TextBoxUploadContentType.Name = "TextBoxUploadContentType";
            this.TextBoxUploadContentType.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadContentType.TabIndex = 78;
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Location = new System.Drawing.Point(9, 120);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(71, 13);
            this.Label20.TabIndex = 77;
            this.Label20.Text = "Content-Type";
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Location = new System.Drawing.Point(9, 9);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(186, 13);
            this.Label18.TabIndex = 76;
            this.Label18.Text = "Use the following form to upload a file.";
            // 
            // ButtonUploadBrowse
            // 
            this.ButtonUploadBrowse.Location = new System.Drawing.Point(354, 37);
            this.ButtonUploadBrowse.Name = "ButtonUploadBrowse";
            this.ButtonUploadBrowse.Size = new System.Drawing.Size(75, 23);
            this.ButtonUploadBrowse.TabIndex = 72;
            this.ButtonUploadBrowse.Text = "Browse";
            this.ButtonUploadBrowse.UseVisualStyleBackColor = true;
            // 
            // TextBoxUploadKeyName
            // 
            this.TextBoxUploadKeyName.Location = new System.Drawing.Point(98, 91);
            this.TextBoxUploadKeyName.Name = "TextBoxUploadKeyName";
            this.TextBoxUploadKeyName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadKeyName.TabIndex = 73;
            // 
            // TextBoxUploadFileName
            // 
            this.TextBoxUploadFileName.Location = new System.Drawing.Point(98, 39);
            this.TextBoxUploadFileName.Name = "TextBoxUploadFileName";
            this.TextBoxUploadFileName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadFileName.TabIndex = 71;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(9, 94);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(56, 13);
            this.Label9.TabIndex = 75;
            this.Label9.Text = "Key Name";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(9, 42);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(83, 13);
            this.Label7.TabIndex = 74;
            this.Label7.Text = "Local File Name";
            // 
            // ButtonUploadFile
            // 
            this.ButtonUploadFile.Location = new System.Drawing.Point(98, 205);
            this.ButtonUploadFile.Name = "ButtonUploadFile";
            this.ButtonUploadFile.Size = new System.Drawing.Size(96, 23);
            this.ButtonUploadFile.TabIndex = 81;
            this.ButtonUploadFile.Text = "Upload File";
            this.ButtonUploadFile.UseVisualStyleBackColor = true;
            // 
            // UploadControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TextBoxUploadBucketName);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.ComboBoxStorageClass);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Label22);
            this.Controls.Add(this.Label21);
            this.Controls.Add(this.CheckBoxUploadMakePublic);
            this.Controls.Add(this.TextBoxUploadContentType);
            this.Controls.Add(this.Label20);
            this.Controls.Add(this.Label18);
            this.Controls.Add(this.ButtonUploadBrowse);
            this.Controls.Add(this.TextBoxUploadKeyName);
            this.Controls.Add(this.TextBoxUploadFileName);
            this.Controls.Add(this.Label9);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.ButtonUploadFile);
            this.Name = "UploadControl";
            this.Size = new System.Drawing.Size(438, 238);
            this.Load += new System.EventHandler(this.UploadControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox TextBoxUploadBucketName;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.ComboBox ComboBoxStorageClass;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label Label22;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.CheckBox CheckBoxUploadMakePublic;
        internal System.Windows.Forms.TextBox TextBoxUploadContentType;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.Button ButtonUploadBrowse;
        internal System.Windows.Forms.TextBox TextBoxUploadKeyName;
        internal System.Windows.Forms.TextBox TextBoxUploadFileName;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Button ButtonUploadFile;
    }
}
