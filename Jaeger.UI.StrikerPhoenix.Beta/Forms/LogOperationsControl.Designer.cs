﻿namespace Jaeger.UI.Forms {
    partial class LogOperationsControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.GroupUnzip = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.TextBoxUnzipTargetFolder = new System.Windows.Forms.TextBox();
            this.TextBoxUnzipSourceFolder = new System.Windows.Forms.TextBox();
            this.ButtonUnzipTarget = new System.Windows.Forms.Button();
            this.ButtonUnzipSource = new System.Windows.Forms.Button();
            this.ButtonUnZipStart = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.txtFilesCount = new System.Windows.Forms.TextBox();
            this.numericPages = new System.Windows.Forms.NumericUpDown();
            this.ButtonUploadLog = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.ButtonCreateZIP = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.TextBoxOutput = new System.Windows.Forms.TextBox();
            this.GroupUnzip.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericPages)).BeginInit();
            this.SuspendLayout();
            // 
            // GroupUnzip
            // 
            this.GroupUnzip.Controls.Add(this.label29);
            this.GroupUnzip.Controls.Add(this.label28);
            this.GroupUnzip.Controls.Add(this.TextBoxUnzipTargetFolder);
            this.GroupUnzip.Controls.Add(this.TextBoxUnzipSourceFolder);
            this.GroupUnzip.Controls.Add(this.ButtonUnzipTarget);
            this.GroupUnzip.Controls.Add(this.ButtonUnzipSource);
            this.GroupUnzip.Controls.Add(this.ButtonUnZipStart);
            this.GroupUnzip.Dock = System.Windows.Forms.DockStyle.Top;
            this.GroupUnzip.Location = new System.Drawing.Point(4, 109);
            this.GroupUnzip.Name = "GroupUnzip";
            this.GroupUnzip.Size = new System.Drawing.Size(448, 108);
            this.GroupUnzip.TabIndex = 74;
            this.GroupUnzip.TabStop = false;
            this.GroupUnzip.Text = "Unzip";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 51);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(73, 13);
            this.label29.TabIndex = 67;
            this.label29.Text = "Target Folder:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 22);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(76, 13);
            this.label28.TabIndex = 67;
            this.label28.Text = "Source Folder:";
            // 
            // TextBoxUnzipTargetFolder
            // 
            this.TextBoxUnzipTargetFolder.Location = new System.Drawing.Point(88, 47);
            this.TextBoxUnzipTargetFolder.Name = "TextBoxUnzipTargetFolder";
            this.TextBoxUnzipTargetFolder.Size = new System.Drawing.Size(247, 20);
            this.TextBoxUnzipTargetFolder.TabIndex = 68;
            // 
            // TextBoxUnzipSourceFolder
            // 
            this.TextBoxUnzipSourceFolder.Location = new System.Drawing.Point(88, 18);
            this.TextBoxUnzipSourceFolder.Name = "TextBoxUnzipSourceFolder";
            this.TextBoxUnzipSourceFolder.Size = new System.Drawing.Size(247, 20);
            this.TextBoxUnzipSourceFolder.TabIndex = 68;
            // 
            // ButtonUnzipTarget
            // 
            this.ButtonUnzipTarget.Location = new System.Drawing.Point(341, 46);
            this.ButtonUnzipTarget.Name = "ButtonUnzipTarget";
            this.ButtonUnzipTarget.Size = new System.Drawing.Size(96, 23);
            this.ButtonUnzipTarget.TabIndex = 69;
            this.ButtonUnzipTarget.Text = "Browser";
            this.ButtonUnzipTarget.UseVisualStyleBackColor = true;
            this.ButtonUnzipTarget.Click += new System.EventHandler(this.ButtonUnzipTarget_Click);
            // 
            // ButtonUnzipSource
            // 
            this.ButtonUnzipSource.Location = new System.Drawing.Point(341, 17);
            this.ButtonUnzipSource.Name = "ButtonUnzipSource";
            this.ButtonUnzipSource.Size = new System.Drawing.Size(96, 23);
            this.ButtonUnzipSource.TabIndex = 69;
            this.ButtonUnzipSource.Text = "Browser";
            this.ButtonUnzipSource.UseVisualStyleBackColor = true;
            this.ButtonUnzipSource.Click += new System.EventHandler(this.ButtonUnzipSource_Click);
            // 
            // ButtonUnZipStart
            // 
            this.ButtonUnZipStart.Location = new System.Drawing.Point(341, 75);
            this.ButtonUnZipStart.Name = "ButtonUnZipStart";
            this.ButtonUnZipStart.Size = new System.Drawing.Size(96, 23);
            this.ButtonUnZipStart.TabIndex = 66;
            this.ButtonUnZipStart.Text = "Start";
            this.ButtonUnZipStart.UseVisualStyleBackColor = true;
            this.ButtonUnZipStart.Click += new System.EventHandler(this.ButtonUnZipStart_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.txtPath);
            this.groupBox5.Controls.Add(this.textBox4);
            this.groupBox5.Controls.Add(this.textBox3);
            this.groupBox5.Controls.Add(this.txtFilesCount);
            this.groupBox5.Controls.Add(this.numericPages);
            this.groupBox5.Controls.Add(this.ButtonUploadLog);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.ButtonCreateZIP);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(4, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(448, 105);
            this.groupBox5.TabIndex = 73;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Create Logs";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 21);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "Folder Log:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(207, 78);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(36, 13);
            this.label27.TabIndex = 69;
            this.label27.Text = "Prefix:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(10, 78);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(75, 13);
            this.label26.TabIndex = 69;
            this.label26.Text = "Bucket Name:";
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(72, 17);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(263, 20);
            this.txtPath.TabIndex = 24;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(249, 74);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(86, 20);
            this.textBox4.TabIndex = 68;
            this.textBox4.Text = "Logs";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(88, 74);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(113, 20);
            this.textBox3.TabIndex = 68;
            this.textBox3.Text = "xaxx010101000";
            // 
            // txtFilesCount
            // 
            this.txtFilesCount.Location = new System.Drawing.Point(43, 45);
            this.txtFilesCount.Name = "txtFilesCount";
            this.txtFilesCount.ReadOnly = true;
            this.txtFilesCount.Size = new System.Drawing.Size(40, 20);
            this.txtFilesCount.TabIndex = 24;
            // 
            // numericPages
            // 
            this.numericPages.Location = new System.Drawing.Point(149, 45);
            this.numericPages.Name = "numericPages";
            this.numericPages.Size = new System.Drawing.Size(52, 20);
            this.numericPages.TabIndex = 67;
            this.numericPages.ValueChanged += new System.EventHandler(this.numericPages_ValueChanged);
            // 
            // ButtonUploadLog
            // 
            this.ButtonUploadLog.Location = new System.Drawing.Point(341, 16);
            this.ButtonUploadLog.Name = "ButtonUploadLog";
            this.ButtonUploadLog.Size = new System.Drawing.Size(96, 23);
            this.ButtonUploadLog.TabIndex = 66;
            this.ButtonUploadLog.Text = "Browser";
            this.ButtonUploadLog.UseVisualStyleBackColor = true;
            this.ButtonUploadLog.Click += new System.EventHandler(this.ButtonUploadLog_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 49);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(31, 13);
            this.label25.TabIndex = 23;
            this.label25.Text = "Files:";
            // 
            // ButtonCreateZIP
            // 
            this.ButtonCreateZIP.Location = new System.Drawing.Point(341, 44);
            this.ButtonCreateZIP.Name = "ButtonCreateZIP";
            this.ButtonCreateZIP.Size = new System.Drawing.Size(96, 23);
            this.ButtonCreateZIP.TabIndex = 66;
            this.ButtonCreateZIP.Text = "Start";
            this.ButtonCreateZIP.UseVisualStyleBackColor = true;
            this.ButtonCreateZIP.Click += new System.EventHandler(this.ButtonCreateZIP_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(207, 49);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(50, 13);
            this.label24.TabIndex = 23;
            this.label24.Text = "Estimate:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(89, 49);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 13);
            this.label23.TabIndex = 23;
            this.label23.Text = "By pages:";
            // 
            // TextBoxOutput
            // 
            this.TextBoxOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextBoxOutput.Location = new System.Drawing.Point(4, 217);
            this.TextBoxOutput.MaxLength = 1032767;
            this.TextBoxOutput.Multiline = true;
            this.TextBoxOutput.Name = "TextBoxOutput";
            this.TextBoxOutput.ReadOnly = true;
            this.TextBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TextBoxOutput.Size = new System.Drawing.Size(448, 107);
            this.TextBoxOutput.TabIndex = 72;
            this.TextBoxOutput.WordWrap = false;
            // 
            // LogOperationsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TextBoxOutput);
            this.Controls.Add(this.GroupUnzip);
            this.Controls.Add(this.groupBox5);
            this.Name = "LogOperationsControl";
            this.Padding = new System.Windows.Forms.Padding(4);
            this.Size = new System.Drawing.Size(456, 328);
            this.Load += new System.EventHandler(this.LogOperationsControl_Load);
            this.GroupUnzip.ResumeLayout(false);
            this.GroupUnzip.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericPages)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox GroupUnzip;
        internal System.Windows.Forms.Label label29;
        internal System.Windows.Forms.Label label28;
        internal System.Windows.Forms.TextBox TextBoxUnzipTargetFolder;
        internal System.Windows.Forms.TextBox TextBoxUnzipSourceFolder;
        internal System.Windows.Forms.Button ButtonUnzipTarget;
        internal System.Windows.Forms.Button ButtonUnzipSource;
        internal System.Windows.Forms.Button ButtonUnZipStart;
        private System.Windows.Forms.GroupBox groupBox5;
        internal System.Windows.Forms.Label label14;
        internal System.Windows.Forms.Label label27;
        internal System.Windows.Forms.Label label26;
        internal System.Windows.Forms.TextBox txtPath;
        internal System.Windows.Forms.TextBox textBox4;
        internal System.Windows.Forms.TextBox textBox3;
        internal System.Windows.Forms.TextBox txtFilesCount;
        private System.Windows.Forms.NumericUpDown numericPages;
        internal System.Windows.Forms.Button ButtonUploadLog;
        internal System.Windows.Forms.Label label25;
        internal System.Windows.Forms.Button ButtonCreateZIP;
        internal System.Windows.Forms.Label label24;
        internal System.Windows.Forms.Label label23;
        internal System.Windows.Forms.TextBox TextBoxOutput;
    }
}
