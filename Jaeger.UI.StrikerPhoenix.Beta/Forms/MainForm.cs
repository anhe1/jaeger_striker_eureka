﻿using Jaeger.UI.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace Jaeger.UI.Forms {
    public partial class MainForm : Form {
        

        public MainForm() {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e) {
            this.txtAWSAccessKeyId.Text = Properties.Settings.Default.AccessKeyId;
            this.txtAWSSecretAccessKey.Text = Properties.Settings.Default.SecretAccessKey;
            this.TextBoxRegion.Text = Properties.Settings.Default.Region;
        }

        private void ComboBoxInterface_SelectedIndexChanged(object sender, EventArgs e) {
            this.CreateService();
        }

        #region menus
        private void menu_Archivo_Salir_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region configuracion y servicio

        private void CreateService() {
            if (this.ComboBoxInterface.Text == this.ComboBoxInterface.Items[0].ToString()) {
                Helper.Region = this.TextBoxRegion.Text;
                Helper.editaS3 = this.GetSimpleStorageServiceAWS3();
            } else {
                //Helper.editaS3 = this.GetServiceSoftS3();
                MessageBox.Show(this, Properties.Resources.Message_Error_NoDisponible, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        //private SimpleStorageServiceSoftS3 GetServiceSoftS3() {
        //    var d = this.Config();
        //    return new SimpleStorageServiceSoftS3(d.AccessKeyId, d.SecretAccessKey, d.Region);
        //}

        private Amazon.S3.Helpers.SimpleStorageServiceAWS3 GetSimpleStorageServiceAWS3() {
            var d = this.Config();
            return new Amazon.S3.Helpers.SimpleStorageServiceAWS3(d.AccessKeyId, d.SecretAccessKey, d.Region);
        }

        private Amazon.S3.Settings Config() {
            return new Amazon.S3.Settings { AccessKeyId = this.txtAWSAccessKeyId.Text, SecretAccessKey = this.txtAWSSecretAccessKey.Text, Region = this.TextBoxRegion.Text };
        }

        #endregion

       
        private bool RemoteFileExists(string url) {
            try {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                response.Close();
                return (response.StatusCode == HttpStatusCode.OK);
            } catch {
                //Any exception will returns false.
                return false;
            }
        }
    }


    /// <summary>
    /// clase estatica para crear sublistas de una lista
    /// </summary>
    public static partial class _30s {
        public static List<List<T>> Chunk<T>(IEnumerable<T> data, int size) {
            return data
              .Select((x, i) => new { Index = i, Value = x })
              .GroupBy(x => x.Index / size)
              .Select(x => x.Select(v => v.Value).ToList())
              .ToList();
        }
    }
}
