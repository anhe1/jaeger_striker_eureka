﻿namespace Jaeger.UI.Forms {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.m_menuStrip = new System.Windows.Forms.MenuStrip();
            this.mArchivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Archivo_Salir = new System.Windows.Forms.ToolStripMenuItem();
            this.mVentana = new System.Windows.Forms.ToolStripMenuItem();
            this.mAcercaDe = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ComboBoxInterface = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TextBoxRegion = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.txtAWSAccessKeyId = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.txtAWSSecretAccessKey = new System.Windows.Forms.TextBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.uploadControl1 = new Jaeger.UI.Forms.UploadControl();
            this.downloadControl1 = new Jaeger.UI.Forms.DownloadControl();
            this.logOperationsControl1 = new Jaeger.UI.Forms.LogOperationsControl();
            this.m_menuStrip.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_menuStrip
            // 
            this.m_menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mArchivo,
            this.mVentana,
            this.mAcercaDe});
            this.m_menuStrip.Location = new System.Drawing.Point(0, 0);
            this.m_menuStrip.MdiWindowListItem = this.mVentana;
            this.m_menuStrip.Name = "m_menuStrip";
            this.m_menuStrip.Size = new System.Drawing.Size(716, 24);
            this.m_menuStrip.TabIndex = 0;
            this.m_menuStrip.Text = "Menú";
            // 
            // mArchivo
            // 
            this.mArchivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Archivo_Salir});
            this.mArchivo.Name = "mArchivo";
            this.mArchivo.Size = new System.Drawing.Size(48, 20);
            this.mArchivo.Text = "mFile";
            // 
            // menu_Archivo_Salir
            // 
            this.menu_Archivo_Salir.Name = "menu_Archivo_Salir";
            this.menu_Archivo_Salir.Size = new System.Drawing.Size(103, 22);
            this.menu_Archivo_Salir.Text = "b.Exit";
            this.menu_Archivo_Salir.Click += new System.EventHandler(this.menu_Archivo_Salir_Click);
            // 
            // mVentana
            // 
            this.mVentana.Name = "mVentana";
            this.mVentana.Size = new System.Drawing.Size(74, 20);
            this.mVentana.Text = "mWindow";
            // 
            // mAcercaDe
            // 
            this.mAcercaDe.Name = "mAcercaDe";
            this.mAcercaDe.Size = new System.Drawing.Size(63, 20);
            this.mAcercaDe.Text = "mAbout";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 398);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(716, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(35, 17);
            this.toolStripStatusLabel1.Text = "Listo.";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBox5);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.GroupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(466, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 374);
            this.panel1.TabIndex = 3;
            // 
            // textBox5
            // 
            this.textBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox5.Location = new System.Drawing.Point(0, 230);
            this.textBox5.MaxLength = 1032767;
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox5.Size = new System.Drawing.Size(250, 144);
            this.textBox5.TabIndex = 27;
            this.textBox5.WordWrap = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ComboBoxInterface);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 167);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(250, 63);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Options";
            // 
            // ComboBoxInterface
            // 
            this.ComboBoxInterface.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxInterface.FormattingEnabled = true;
            this.ComboBoxInterface.Items.AddRange(new object[] {
            "SimpleStorageServiceAWS3",
            "SoftAWS"});
            this.ComboBoxInterface.Location = new System.Drawing.Point(6, 35);
            this.ComboBoxInterface.Name = "ComboBoxInterface";
            this.ComboBoxInterface.Size = new System.Drawing.Size(230, 21);
            this.ComboBoxInterface.TabIndex = 3;
            this.ComboBoxInterface.SelectedIndexChanged += new System.EventHandler(this.ComboBoxInterface_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Interface";
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.label6);
            this.GroupBox1.Controls.Add(this.TextBoxRegion);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.txtAWSAccessKeyId);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.txtAWSSecretAccessKey);
            this.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.GroupBox1.Location = new System.Drawing.Point(0, 0);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(250, 167);
            this.GroupBox1.TabIndex = 26;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Global Settings (Enter your values below)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Region";
            // 
            // TextBoxRegion
            // 
            this.TextBoxRegion.Location = new System.Drawing.Point(6, 138);
            this.TextBoxRegion.Name = "TextBoxRegion";
            this.TextBoxRegion.Size = new System.Drawing.Size(230, 20);
            this.TextBoxRegion.TabIndex = 24;
            this.TextBoxRegion.Text = "us-east-1";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(3, 74);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(91, 13);
            this.Label3.TabIndex = 22;
            this.Label3.Text = "SecretAccessKey";
            // 
            // txtAWSAccessKeyId
            // 
            this.txtAWSAccessKeyId.Location = new System.Drawing.Point(6, 40);
            this.txtAWSAccessKeyId.Name = "txtAWSAccessKeyId";
            this.txtAWSAccessKeyId.Size = new System.Drawing.Size(230, 20);
            this.txtAWSAccessKeyId.TabIndex = 16;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(3, 24);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(69, 13);
            this.Label2.TabIndex = 21;
            this.Label2.Text = "AccessKeyId";
            // 
            // txtAWSSecretAccessKey
            // 
            this.txtAWSSecretAccessKey.Location = new System.Drawing.Point(6, 90);
            this.txtAWSSecretAccessKey.Name = "txtAWSSecretAccessKey";
            this.txtAWSSecretAccessKey.Size = new System.Drawing.Size(230, 20);
            this.txtAWSSecretAccessKey.TabIndex = 18;
            // 
            // tabControl
            // 
            this.tabControl.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Controls.Add(this.tabPage4);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 24);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(466, 374);
            this.tabControl.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.uploadControl1);
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(458, 348);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Upload";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.downloadControl1);
            this.tabPage2.Location = new System.Drawing.Point(4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(458, 348);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Download";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(458, 348);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Buckets";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.logOperationsControl1);
            this.tabPage4.Location = new System.Drawing.Point(4, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(458, 348);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Log Operations";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // uploadControl1
            // 
            this.uploadControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uploadControl1.Location = new System.Drawing.Point(3, 3);
            this.uploadControl1.Name = "uploadControl1";
            this.uploadControl1.Size = new System.Drawing.Size(452, 342);
            this.uploadControl1.TabIndex = 0;
            // 
            // downloadControl1
            // 
            this.downloadControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.downloadControl1.Location = new System.Drawing.Point(3, 3);
            this.downloadControl1.Name = "downloadControl1";
            this.downloadControl1.Size = new System.Drawing.Size(452, 342);
            this.downloadControl1.TabIndex = 0;
            // 
            // logOperationsControl1
            // 
            this.logOperationsControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logOperationsControl1.Location = new System.Drawing.Point(3, 3);
            this.logOperationsControl1.Name = "logOperationsControl1";
            this.logOperationsControl1.Padding = new System.Windows.Forms.Padding(4);
            this.logOperationsControl1.Size = new System.Drawing.Size(452, 342);
            this.logOperationsControl1.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 420);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.m_menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.m_menuStrip;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "mainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.m_menuStrip.ResumeLayout(false);
            this.m_menuStrip.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip m_menuStrip;
        private System.Windows.Forms.ToolStripMenuItem mArchivo;
        private System.Windows.Forms.ToolStripMenuItem menu_Archivo_Salir;
        private System.Windows.Forms.ToolStripMenuItem mVentana;
        private System.Windows.Forms.ToolStripMenuItem mAcercaDe;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox ComboBoxInterface;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.TextBox TextBoxRegion;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox txtAWSAccessKeyId;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox txtAWSSecretAccessKey;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private LogOperationsControl logOperationsControl1;
        private UploadControl uploadControl1;
        private DownloadControl downloadControl1;
    }
}