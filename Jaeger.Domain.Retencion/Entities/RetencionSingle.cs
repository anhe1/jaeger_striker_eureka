﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Retencion.Entities {
    [SugarTable("_cfdret")]
    public class RetencionSingle {
        private DateTime fechaEmision;
        private DateTime? fechaTimbrado;
        private DateTime? fechaEstado;
        private DateTime? fechaCancela;
        private DateTime? fechaEntrega;
        private DateTime? fechaUltimoPago;
        private DateTime? fechaValidacion;
        private DateTime? fechaModifica = null;
        private string rfcProveedorCertificacion;

        public RetencionSingle() {
            
        }

        /// <summary>
        /// obtener ó establecer el indice de la tabla.
        /// </summary>
        [DataNames("_cfdret_id")]
        [SugarColumn(ColumnName = "_cfdret_id", ColumnDescription = "indice de la tabla", Length = 11, IsIdentity = true, IsPrimaryKey = true)]
        public int Id { get; set; }

        /// <summary>
        /// obtener ó establecer si el registro es activo.
        /// </summary>
        [DataNames("_cfdret_a")]
        [SugarColumn(ColumnName = "_cfdret_a", ColumnDescription = "registro activo")]
        public bool Activo { get; set; }

        /// <summary>
        /// obtener ó establecer si el registro es activo.
        /// </summary>
        [DataNames("_cfdret_extr")]
        [SugarColumn(ColumnName = "_cfdret_extr", ColumnDescription = "indica si el receptor es extranjero", DefaultValue = "0")]
        public bool ReceptorExtranjero { get; set; }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        [DataNames("_cfdret_doc_id")]
        [SugarColumn(ColumnName = "_cfdret_doc_id")]
        public int SubTipoInt { get; set; }

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio
        /// </summary>
        [DataNames("_cfdret_drctr_id")]
        [SugarColumn(ColumnName = "_cfdret_drctr_id", ColumnDescription = "indice de relacion con el directorio", IsNullable = true)]
        public int IdDirectorio { get; set; }

        [DataNames("_cfdret_idaden")]
        [SugarColumn(ColumnName = "_cfdret_idaden", ColumnDescription = "ID de adenda para el modo edición", Length = 11, IsNullable = true)]
        public int IdAddenda { get; set; }

        [DataNames("_cfdret_idserie")]
        [SugarColumn(ColumnName = "_cfdret_idserie", ColumnDescription = "ID de serie para el modo edición", Length = 11, IsNullable = true)]
        public int IdSerie { get; set; }

        [DataNames("_cfdret_domie")]
        [SugarColumn(ColumnName = "_cfdret_domie", ColumnDescription = "indice de relacion del domicilio emisor", Length = 11, IsNullable = true)]
        public int EmisorIdDomicilio { get; set; }

        [DataNames("_cfdret_domir")]
        [SugarColumn(ColumnName = "_cfdret_domir", ColumnDescription = "indice de relacion del domicilio receptor", Length = 11, IsNullable = true)]
        public int ReceptorIdDomicilio { get; set; }

        /// <summary>
        /// obtener o establecer la precision decimal utilizada para el comprobante
        /// </summary>
        [DataNames("_cfdret_prec")]
        [SugarColumn(ColumnName = "_cfdret_prec", ColumnDescription = "precision decimal utilizada para el comprobante", Length = 2, IsNullable = true)]
        public int PrecisionDecimal { get; set; }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión del mes inicial del periodo de la retención e información de pagos
        /// </summary>
        [DataNames("_cfdret_ini")]
        [SugarColumn(ColumnName = "_cfdret_ini", ColumnDescription = "atributo requerido para la expresión del mes inicial del periodo de la retención e información de pagos", Length = 2, IsNullable = true)]
        public int MesInicial { get; set; }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión del mes final del periodo de la retención e información de pagos
        /// </summary>
        [DataNames("_cfdret_fin")]
        [SugarColumn(ColumnName = "_cfdret_fin", ColumnDescription = "atributo requerido para la expresión del mes final del periodo de la retención e información de pagos", Length = 2, IsNullable = true)]
        public int MesFinal { get; set; }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión del ejercicio fiscal (año)
        /// </summary>
        [DataNames("_cfdret_eje")]
        [SugarColumn(ColumnName = "_cfdret_eje", ColumnDescription = "atributo requerido para la expresión del ejercicio fiscal (año)", Length = 4, IsNullable = true)]
        public int Ejercicio { get; set; }

        /// <summary>
        /// Atributo requerido para expresar  el total del monto de la operación  que se relaciona en el comprobante
        /// </summary>
        [DataNames("_cfdret_opr")]
        [SugarColumn(ColumnName = "_cfdret_opr", ColumnDescription = "para expresar  el total del monto de la operación  que se relaciona en el comprobante", Length = 11, DecimalDigits = 4)]
        public decimal MontoTotalOperacion { get; set; }

        /// <summary>
        /// Atributo requerido para expresar el total del monto gravado de la operación  que se relaciona en el comprobante.
        /// </summary>
        [DataNames("_cfdret_grv")]
        [SugarColumn(ColumnName = "_cfdret_grv", ColumnDescription = "para expresar el total del monto gravado de la operación  que se relaciona en el comprobante.", Length = 11, DecimalDigits = 4)]
        public decimal MontoTotalGravado { get; set; }

        /// <summary>
        /// Atributo requerido para expresar el total del monto exento de la operación  que se relaciona en el comprobante.
        /// </summary>
        [DataNames("_cfdret_ext")]
        [SugarColumn(ColumnName = "_cfdret_ext", ColumnDescription = "para expresar el total del monto exento de la operación  que se relaciona en el comprobante.", Length = 11, DecimalDigits = 4)]
        public decimal MontoTotalExento { get; set; }

        /// <summary>
        /// Atributo requerido para expresar el monto total de las retenciones. Sumatoria de los montos de retención del nodo ImpRetenidos.
        /// </summary>
        [DataNames("_cfdret_tot")]
        [SugarColumn(ColumnName = "_cfdret_tot", ColumnDescription = "para expresar el monto total de las retenciones. Sumatoria de los montos de retención del nodo ImpRetenidos.", Length = 11, DecimalDigits = 4)]
        public decimal MontoTotalRetencion { get; set; }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// minInclusive value ="2014-01-01T00:00:00-06:00"
        /// pattern value = "-?([1-9][0-9]{3,}|0[0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9](\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))"
        /// </summary>
        [DataNames("_cfdret_fecems")]
        [SugarColumn(ColumnName = "_cfdret_fecems", ColumnDescription = "fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                DateTime firstGoodDate = new DateTime(2004, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaEmision = value;
                else
                    this.fechaEmision = firstGoodDate;
            }
        }

        /// <summary>
        /// fecha de certificación
        /// </summary>
        [DataNames("_cfdret_feccert")]
        [SugarColumn(ColumnName = "_cfdret_feccert", ColumnDescription = "fecha de certificacion", IsNullable = true)]
        public DateTime? FechaTimbre {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaTimbrado >= firstGoodDate)
                    return this.fechaTimbrado;
                else
                    return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaTimbrado = value;
            }
        }

        /// <summary>
        /// obtener o establecer la fecha del estado del comprobante (SAT)
        /// </summary>
        [DataNames("_cfdret_fecedo")]
        [SugarColumn(ColumnName = "_cfdret_fecedo", ColumnDescription = "fecha del estado del comprobante (SAT)", IsNullable = true)]
        public DateTime? FechaEstado {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEstado >= firstGoodDate)
                    return this.fechaEstado;
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaEstado = value;
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdret_feccnc")]
        [SugarColumn(ColumnName = "_cfdret_feccnc", ColumnDescription = "fecha de cancelacion del comprobante", IsNullable = true)]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancela >= firstGoodDate)
                    return this.fechaCancela;
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaCancela = value;
            }
        }

        /// <summary>
        /// fecha de entrega o recepcion del comprobante
        /// </summary>
        [DataNames("_cfdret_fecent")]
        [SugarColumn(ColumnName = "_cfdret_fecent", ColumnDescription = "fecha de entrega o recepcion del comprobante", IsNullable = true)]
        public DateTime? FechaEntrega {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEntrega >= firstGoodDate) {
                    return this.fechaEntrega;
                }
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaEntrega = value;
            }
        }

        /// <summary>
        /// fecha de ultimo pago o cobro del comprobante
        /// </summary>
        [DataNames("_cfdret_fecupc")]
        [SugarColumn(ColumnName = "_cfdret_fecupc", ColumnDescription = "ultima fecha de cobro o pago del comprobante", IsNullable = true)]
        public DateTime? FechaUltPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaUltimoPago >= firstGoodDate) {
                    return this.fechaUltimoPago;
                }
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaUltimoPago = value;
            }
        }

        /// <summary>
        /// fecha de validacion del comprobante
        /// </summary>
        [DataNames("_cfdret_fecval")]
        [SugarColumn(ColumnName = "_cfdret_fecval", ColumnDescription = "fecha de validacion del comprobante", IsNullable = true)]
        public DateTime? FechaVal {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaValidacion >= firstGoodDate)
                    return this.fechaValidacion;
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaValidacion = value;
            }
        }

        [DataNames("_cfdret_fn")]
        [SugarColumn(ColumnName = "_cfdret_fn", ColumnDescription = "fecha de creacion del registro")]
        public DateTime FechaNuevo { get; set; }

        [DataNames("_cfdret_fm")]
        [SugarColumn(ColumnName = "_cfdret_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica.Value;
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaModifica = value;
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido con valor prefijado que indica la versión del estándar bajo el que se encuentra expresada la retención y/o comprobante de información de pagos.
        /// </summary>
        [DataNames("_cfdret_ver")]
        [SugarColumn(ColumnName = "_cfdret_ver", ColumnDescription = "indica la version del estandar bajo el que se encuentra expresada la retencion y/o comprobante de informacion de pagos.", DefaultValue = "1.0", Length = 3)]
        public string Version { get; set; }

        /// <summary>
        /// obtener o establecer atributo requerido para expresar la clave de la retención e información de pagos de acuerdo al catálogo publicado en internet por el SAT.
        /// </summary>
        [DataNames("_cfdret_cve")]
        [SugarColumn(ColumnName = "_cfdret_cve", ColumnDescription = "expresar la clave de la retención e información de pagos de acuerdo al catálogo publicado en internet por el SAT.", IsNullable = true, Length = 3)]
        public string ClaveRetencion { get; set; }

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        [DataNames("_cfdret_estado")]
        [SugarColumn(ColumnName = "_cfdret_estado", ColumnDescription = "estado del comprobante del servicio del SAT", Length = 10, IsNullable = true)]
        public string Estado { get; set; }

        [DataNames("_cfdret_status")]
        [SugarColumn(ColumnName = "_cfdret_status", ColumnDescription = "status del comprobante", Length = 14, DefaultValue = "Pendiente", IsNullable = true)]
        public string Status { get; set; }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        [DataNames("_cfdret_rfce")]
        [SugarColumn(ColumnName = "_cfdret_rfce", ColumnDescription = "registro federal de contribuyentes del emisor del comprobante", Length = 14)]
        public string EmisorRFC { get; set; }

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        [DataNames("_cfdret_rfcr")]
        [SugarColumn(ColumnName = "_cfdret_rfcr", ColumnDescription = "registro federal de contribuyentes del receptor del comprobante", Length = 14)]
        public string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establecer el RFC del proveedor de certificación de comprobantes fiscales digitales que genera el timbre fiscal digital.
        /// </summary>
        [DataNames("_cfdret_pac")]
        [SugarColumn(ColumnName = "_cfdret_pac", ColumnDescription = "RFC del proveedor de certificación de comprobantes fiscales digitales que genera el timbre fiscal digital.", Length = 14, IsNullable = true)]
        public string RFCProvCertif {
            get {
                if (!(string.IsNullOrEmpty(rfcProveedorCertificacion)))
                    return this.rfcProveedorCertificacion;
                return null;
            }
            set {
                if (!(string.IsNullOrEmpty(value)))
                    this.rfcProveedorCertificacion = value;
            }
        }

        /// <summary>
        /// obtener o establecer atributo opcional para la Clave Única del Registro Poblacional del contribuyente emisor del documento de retención e información de pagos.
        /// </summary>
        [DataNames("_cfdret_curpe")]
        [SugarColumn(ColumnName = "_cfdret_curpe", ColumnDescription = "opcional para la Clave Única del Registro Poblacional del contribuyente emisor del documento de retención e información de pagos.", Length = 18, IsNullable = true)]
        public string EmisorCURP { get; set; }

        /// <summary>
        /// obtener o establecer atributo opcional para la Clave Única del Registro Poblacional del contribuyente emisor del documento de retención e información de pagos.
        /// </summary>
        [DataNames("_cfdret_curpr")]
        [SugarColumn(ColumnName = "_cfdret_curpr", ColumnDescription = "opcional para la Clave Única del Registro Poblacional del contribuyente emisor del documento de retención e información de pagos.", Length = 18, IsNullable = true)]
        public string ReceptorCURP { get; set; }

        /// <summary>
        /// opcional para expresar el número de registro de identificación fiscal del receptor del documento cuando sea residente en el extranjero
        /// </summary>
        [DataNames("_cfdret_regidr")]
        [SugarColumn(ColumnName = "_cfdret_regidr", ColumnDescription = "opcional para expresar el número de registro de identificación fiscal del receptor del documento cuando sea residente en el extranjero", Length = 20, IsNullable = true)]
        public string ReceptorNumRegIdTrib { get; set; }

        /// <summary>
        /// obtener o establecer numero de certificado del emisor del comprobante
        /// </summary>
        [DataNames("_cfdret_nocert")]
        [SugarColumn(ColumnName = "_cfdret_nocert", ColumnDescription = "para expresar el número de serie del certificado de sello digital con el que se selló digitalmente el documento de la retención e información de pagos.", Length = 20, IsNullable = true)]
        public string NoCertificado { get; set; }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        [DataNames("_cfdret_folio")]
        [SugarColumn(ColumnName = "_cfdret_folio", ColumnDescription = "atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.", Length = 21, IsNullable = true)]
        public string Folio { get; set; }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        [DataNames("_cfdret_serie")]
        [SugarColumn(ColumnName = "_cfdret_serie", ColumnDescription = "serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres.", Length = 25, IsNullable = true)]
        public string Serie { get; set; }

        /// <summary>
        /// obtener o establecer tipo de documento en modo texto
        /// </summary>
        [DataNames("_cfdret_doc")]
        [SugarColumn(ColumnName = "_cfdret_doc", ColumnDescription = "documento (factura, nota de credito,nota de cargo)", Length = 36, IsNullable = true)]
        public string Documento { get; set; }

        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        [DataNames("_cfdret_uuid")]
        [SugarColumn(ColumnName = "_cfdret_uuid", ColumnDescription = "36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122", Length = 36, IsNullable = true)]
        public string IdDocumento { get; set; }

        /// <summary>
        /// obtener o establecer atributo opcional que expresa la descripción de la retención e información de pagos en caso de que en el atributo CveRetenc se haya elegido el valor para 'otro tipo de retenciones'
        /// </summary>
        [DataNames("_cfdret_desc")]
        [SugarColumn(ColumnName = "_cfdret_desc", ColumnDescription = "opcional que expresa la descripción de la retención e información de pagos en caso de que en el atributo CveRetenc se haya elegido el valor para otro tipo de retenciones", Length = 100, IsNullable = true)]
        public string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        [DataNames("_cfdret_nome")]
        [SugarColumn(ColumnName = "_cfdret_nome", ColumnDescription = "para incorporar la clave en el Registro Federal de Contribuyentes correspondiente al contribuyente emisor del documento de retención e información de pagos, sin guiones o espacios.", Length = 300)]
        public string EmisorNombre { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        [DataNames("_cfdret_nomr")]
        [SugarColumn(ColumnName = "_cfdret_nomr", ColumnDescription = "para el nombre, denominación o razón social del contribuyente receptor del documento.", Length = 300)]
        public string ReceptorNombre { get; set; }

        [DataNames("_cfdret_compl")]
        [SugarColumn(ColumnName = "_cfdret_compl", ColumnDataType = "TEXT", ColumnDescription = "complemento", IsNullable = true)]
        public string JComplementos { get; set; }

        /// <summary>
        /// obtener o establecer el acuse de validacion del comprobante
        /// </summary>
        [DataNames("_cfdret_val")]
        [SugarColumn(ColumnName = "_cfdret_val", ColumnDataType = "TEXT", ColumnDescription = "validacion", IsNullable = true)]
        public string JValidacion { get; set; }

        /// <summary>
        /// obtener o establecer el contenido del archivo del acuse de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdret_acuse")]
        [SugarColumn(ColumnName = "_cfdret_acuse", ColumnDataType = "TEXT", ColumnDescription = "acuse de cancelacion", IsNullable = true)]
        public string JAccuse { get; set; }

        [DataNames("_cfdret_adenda")]
        [SugarColumn(ColumnName = "_cfdret_adenda", ColumnDataType = "TEXT", ColumnDescription = "addenda", IsNullable = true)]
        public string JAddenda { get; set; }

        [DataNames("_cfdret_impst")]
        [SugarColumn(ColumnName = "_cfdret_impuesto", ColumnDataType = "TEXT", ColumnDescription = "para expresar el total de los impuestos retenidos que se desprenden de los conceptos expresados en el documento de retenciones e información de pagos.(json)", IsNullable = true)]
        public string JImpuestosRetenidos { get; set; }

        /// <summary>
        /// obtener o establecer url del archivo XML del comprobante fiscal
        /// </summary>
        [DataNames("_cfdret_url_xml")]
        [SugarColumn(ColumnName = "_cfdret_url_xml", ColumnDataType = "TEXT", ColumnDescription = "url de descarga para el archivo xml", IsNullable = true)]
        public string FileXML { get; set; }

        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        [DataNames("_cfdret_url_pdf")]
        [SugarColumn(ColumnName = "_cfdret_url_pdf", ColumnDataType = "TEXT", ColumnDescription = "url de descarga para la representación impresa", IsNullable = true)]
        public string FilePDF { get; set; }

        /// <summary>
        /// obtener o establecer la url del archivo de acuse de cancelacion
        /// </summary>
        [DataNames("_cfdret_url_acu")]
        [SugarColumn(ColumnName = "_cfdret_url_acu", ColumnDataType = "TEXT", ColumnDescription = "url de descarga de la representacio impresa del acusde de cancelacion del comprobante", IsNullable = true)]
        public string FileAccuse { get; set; }

        /// <summary>
        /// obtener o establecer url de descarga del archivo xml del acuse de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdret_url_xmlacu")]
        [SugarColumn(ColumnName = "_cfdret_url_xmlacu", ColumnDataType = "TEXT", ColumnDescription = "url de descarga del archivo xml del acuse de cancelacion del comprobante", IsNullable = true)]
        public string FileAccuseXML { get; set; }

        /// <summary>
        /// obtener o establecer url de descarga del archivo pdf del acuse de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdret_url_pdfacu")]
        [SugarColumn(ColumnName = "_cfdret_url_pdfacu", ColumnDataType = "TEXT", ColumnDescription = "url de descarga del archivo pdf del acuse de cancelacion del comprobante", IsNullable = true)]
        public string FileAccusePDF { get; set; }

        /// <summary>
        /// obtener o establecer el contenido del archivo XML
        /// </summary>
        [DataNames("_cfdret_save")]
        [SugarColumn(ColumnName = "_cfdret_save", ColumnDataType = "TEXT", ColumnDescription = "XML Completo en formato JSON (Al guardar en S3 se borra)", IsNullable = true)]
        public string XML { get; set; }

        [DataNames("_cfdret_usr_n")]
        [SugarColumn(ColumnName = "_cfdret_usr_n", ColumnDescription = "clave de usuario que crea el registro", Length = 14, IsOnlyIgnoreUpdate = true)]
        public string Creo { get; set; }

        [DataNames("_cfdret_usr_m")]
        [SugarColumn(ColumnName = "_cfdret_usr_m", ColumnDescription = "clave del ultimo usuario que modifico el registro", Length = 14, IsOnlyIgnoreInsert = true, IsNullable = true)]
        public string Modifica { get; set; }

        [DataNames("_cfdret_sync")]
        [SugarColumn(ColumnName = "_cfdret_sync", ColumnDescription = "", DefaultValue = "0")]
        public bool Sincronizado { get; set; }
    }
}
