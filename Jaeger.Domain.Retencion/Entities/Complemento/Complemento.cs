﻿using System;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Retencion.ValueObjects;

namespace Jaeger.Domain.Retencion.Entities.Complemento {
    public class Complemento : BasePropertyChangeImplementation {
        private RetencionComplementoEnum tipoComplemento;
        private string dataField;

        [JsonIgnore]
        public RetencionComplementoEnum Nombre {
            get {
                return this.tipoComplemento;
            }
            set {
                this.tipoComplemento = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("nombre", Order = 1)]
        public string NombreText {
            get {
                return Enum.GetName(typeof(RetencionComplementoEnum), this.Nombre);
            }
            set {
                this.Nombre = (RetencionComplementoEnum)Enum.Parse(typeof(RetencionComplementoEnum), value);
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("data", Order = 2)]
        public string Data {
            get {
                return this.dataField;
            }
            set {
                this.dataField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
