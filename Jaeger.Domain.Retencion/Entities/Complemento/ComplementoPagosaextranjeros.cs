﻿using Jaeger.Domain.Base.Abstractions;
using Newtonsoft.Json;

namespace Jaeger.Domain.Retencion.Entities.Complemento {
    /// <summary>
    /// Complemento para expresar los pagos que se realizan a residentes en el extranjero
    /// </summary>
    [JsonObject("Pagosaextranjeros")]
    public class ComplementoPagosaextranjeros : BasePropertyChangeImplementation {
        #region declaraciones
        private PagosaextranjerosNoBeneficiario noBeneficiarioField;
        private PagosaextranjerosBeneficiario beneficiarioField;
        private string versionField;
        private string esBenefEfectDelCobroField;
        #endregion

        public ComplementoPagosaextranjeros() {
            this.versionField = "1.0";
            this.noBeneficiarioField = new PagosaextranjerosNoBeneficiario();
            this.beneficiarioField = new PagosaextranjerosBeneficiario();
        }

        /// <summary>
        /// Atributo requerido con valor prefijado que indica la versión del complemento de pagos realizados a residentes a residentes en el extranjero
        /// </summary>
        [JsonProperty("ver")]
        public string Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar si el beneficiario del pago es la misma persona que retiene
        /// </summary>
        [JsonProperty("esBenefEfectDelCobro")]
        public string EsBenefEfectDelCobro {
            get {
                return this.esBenefEfectDelCobroField;
            }
            set {
                this.esBenefEfectDelCobroField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public bool IsBenefEfectDelCobro {
            get { return this.EsBenefEfectDelCobro == "SI"; }
            set { if (value == true)
                    this.EsBenefEfectDelCobro = "SI";
                else this.EsBenefEfectDelCobro = "NO";
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo opcional para expresar la información del residente extranjero efectivo del cobro</xs:
        /// </summary>

        [JsonProperty("noBeneficiario")]
        public PagosaextranjerosNoBeneficiario NoBeneficiario {
            get {
                return this.noBeneficiarioField;
            }
            set {
                this.noBeneficiarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo opcional para precisar la información del representante para efectos fiscales en México</xs:documentation>
        /// </summary>
        [JsonProperty("beneficiario")]
        public PagosaextranjerosBeneficiario Beneficiario {
            get {
                return this.beneficiarioField;
            }
            set {
                this.beneficiarioField = value;
                this.OnPropertyChanged();
            }
        }

        public string Json() {
            return JsonConvert.SerializeObject(this, 0);
        }

        public static ComplementoPagosaextranjeros Json(string inputJson) {
            try {
                return JsonConvert.DeserializeObject<ComplementoPagosaextranjeros>(inputJson);
            }
            catch {
                return null;
            }
        }
    }

    /// <summary>
    /// Nodo opcional para expresar la información del residente extranjero efectivo del cobro
    /// </summary>
    [JsonObject("NoBeneficiario")]
    public partial class PagosaextranjerosNoBeneficiario : BasePropertyChangeImplementation {

        private string paisDeResidParaEfecFiscField;
        private string conceptoPagoField;
        private string descripcionConceptoField;

        /// <summary>
        /// Atributo requerido para expresar la clave del país de residencia del extranjero, conforme al catálogo de países publicado en el Anexo 10 de la RMF.
        /// </summary>
        [JsonProperty("paisDeResidParaEfecFisc")]
        public string PaisDeResidParaEfecFisc {
            get {
                return this.paisDeResidParaEfecFiscField;
            }
            set {
                this.paisDeResidParaEfecFiscField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar  el tipo contribuyente sujeto a la retención, conforme al catálogo.</xs:documentation>
        /// </summary>
        [JsonProperty("conceptoPago")]
        public string ConceptoPago {
            get {
                return this.conceptoPagoField;
            }
            set {
                this.conceptoPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la descripción de la definición del pago del residente en el extranjero</xs:documentation>
        /// </summary>
        [JsonProperty("descripcionConcepto")]
        public string DescripcionConcepto {
            get {
                return this.descripcionConceptoField;
            }
            set {
                this.descripcionConceptoField = value;
                this.OnPropertyChanged();
            }
        }
    }

    /// <summary>
    /// Nodo opcional para precisar la información del representante para efectos fiscales en México
    /// </summary>
    [JsonObject("Beneficiario")]
    public partial class PagosaextranjerosBeneficiario : BasePropertyChangeImplementation {

        private string rFCField;
        private string cURPField;
        private string nomDenRazSocBField;
        private string conceptoPagoField;
        private string descripcionConceptoField;

        /// <summary>
        /// Atributo requerido para expresar la clave del registro federal de contribuyentes del representante legal en México
        /// </summary>
        [JsonProperty("rfc")]
        public string RFC {
            get {
                return this.rFCField;
            }
            set {
                this.rFCField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión de la CURP del representante legal
        /// </summary>
        [JsonProperty("curp")]
        public string CURP {
            get {
                return this.cURPField;
            }
            set {
                this.cURPField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el nombre, denominación o razón social del contribuyente en México
        /// </summary>
        [JsonProperty("nomDenRazSocB")]
        public string NomDenRazSocB {
            get {
                return this.nomDenRazSocBField;
            }
            set {
                this.nomDenRazSocBField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el tipo de contribuyente sujeto a la retención, conforme al catálogo.
        /// </summary>
        [JsonProperty("conceptoPago")]
        public string ConceptoPago {
            get {
                return this.conceptoPagoField;
            }
            set {
                this.conceptoPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la descripción de la definición del pago del residente en el extranjero</xs:documentation>
        /// </summary>
        [JsonProperty("descripcionConcepto")]
        public string DescripcionConcepto {
            get {
                return this.descripcionConceptoField;
            }
            set {
                this.descripcionConceptoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
