﻿using System.ComponentModel;
using Newtonsoft.Json;

namespace Jaeger.Domain.Retencion.Entities.Complemento {
    public class Complementos {
        private BindingList<Complemento> objetoField;

        /// <summary>
        /// constructor
        /// </summary>
        public Complementos() {
            this.objetoField = new BindingList<Complemento>();
        }

        #region propiedades

        [JsonProperty("objeto")]
        public BindingList<Complemento> Objeto {
            get {
                return this.objetoField;
            }
            set {
                this.objetoField = value;
            }
        }

        #endregion

        #region metodos

        public string Json(Formatting objFormat = 0) {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static Complementos Json(string inputJson) {
            try {
                return JsonConvert.DeserializeObject<Complementos>(inputJson);
            }
            catch {
                return null;
            }
        }

        #endregion
    }
}
