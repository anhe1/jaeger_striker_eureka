﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Retencion.Contracts;
using System.ComponentModel;

namespace Jaeger.Domain.Retencion.Entities {
    /// <summary>
    /// Estándar de Documento Electrónico Retenciones e Información de Pagos.
    /// </summary>
    [SugarTable("_cfdret", "estandar de documento electronico retenciones e informacion de pagos.")]
    public class ComprobanteRetencionModel : BasePropertyChangeImplementation, IComprobanteRetencionModel {
        #region declaraciones
        private int index;
        private bool activo;
        private bool extranjero;
        private int idDirectorio;
        private int subTipoInt;
        private int idAddenda;
        private int idComplemento;
        private DateTime fechaEmision;
        private DateTime? fechaTimbrado;
        private DateTime? fechaEstado;
        private DateTime? fechaCancela;
        private DateTime? fechaEntrega;
        private DateTime? fechaUltimoPago;
        private DateTime? fechaValidacion;
        private DateTime? fechaModifica = null;
        private string rfcProveedorCertificacion;
        private int idSerie;
        private int emisorIdDomicilio;
        private int receptorIdDomicilio;
        private int precisionDecimal;
        private int mesInicial;
        private int mesFinal;
        private int ejercicio;
        private decimal montoTotalOperacion;
        private decimal montoTotalGravado;
        private decimal montoTotalExento;
        private decimal montoTotalRetencion;
        private DateTime fechaNuevo;
        private string version;
        private string claveRetencion;
        private string estado;
        private string status;
        private string emisorRFC;
        private string receptorRFC;
        private string emisorCURP;
        private string receptorCURP;
        private string receptorNumRegIdTrib;
        private string noCertificado;
        private string documento;
        private string serie;
        private string folio;
        private string idDocumento;
        private string emisorNombre;
        private string descripcion;
        private string receptorNombre;
        private string jValidacion;
        private string jAccuse;
        private string jAddenda;
        private string fileXML;
        private string filePDF;
        private string fileAccuseXML;
        private string fileAccuse;
        private string fileAccusePDF;
        private string xML;
        private string creo;
        private bool sincronizado;
        private string modifica;
        private Complemento.ComplementoPagosaextranjeros _Pagosaextranjeros;
        private string regimenFiscalE;
        private string domicilioFiscalR;
        private string lugarExpRetenc;
        private BindingList<RetencionImpuestosDetailModel> impuestosRetenidos;
        private decimal utilidadBimestralField;
        private decimal iSRCorrespondienteField;
        private RetencionesCfdiRetenRelacionados relacionados;
        #endregion

        public ComprobanteRetencionModel() {
            this.SubTipoInt = 1;
        }

        /// <summary>
        /// obtener ó establecer el indice de la tabla.
        /// </summary>
        [DataNames("_cfdret_id")]
        [SugarColumn(ColumnName = "_cfdret_id", ColumnDescription = "indice de la tabla", Length = 11, IsIdentity = true, IsPrimaryKey = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener ó establecer si el registro es activo.
        /// </summary>
        [DataNames("_cfdret_a")]
        [SugarColumn(ColumnName = "_cfdret_a", ColumnDescription = "registro activo")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener ó establecer si el registro es activo.
        /// </summary>
        [DataNames("_cfdret_extr")]
        [SugarColumn(ColumnName = "_cfdret_extr", ColumnDescription = "indica si el receptor es extranjero", DefaultValue = "0")]
        public bool ReceptorExtranjero {
            get {
                return this.extranjero;
            }
            set {
                this.extranjero = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        [DataNames("_cfdret_doc_id")]
        [SugarColumn(ColumnName = "_cfdret_doc_id")]
        public int SubTipoInt {
            get {
                return this.subTipoInt;
            }
            set {
                this.subTipoInt = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio
        /// </summary>
        [DataNames("_cfdret_drctr_id")]
        [SugarColumn(ColumnName = "_cfdret_drctr_id", ColumnDescription = "indice de relacion con el directorio", IsNullable = true)]
        public int IdDirectorio {
            get {
                return this.idDirectorio;
            }
            set {
                this.idDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdret_idaden")]
        [SugarColumn(ColumnName = "_cfdret_idaden", ColumnDescription = "ID de adenda para el modo edición", Length = 11, IsNullable = true)]
        public int IdAddenda {
            get {
                return this.idAddenda;
            }
            set {
                this.idAddenda = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdret_idcmp")]
        [SugarColumn(ColumnName = "_cfdret_idcmp", ColumnDescription = "id de complemento de comprobantes 8-Pagos a extranjeros", Length = 11, IsNullable = true)]
        public int IdComplemento {
            get {
                return this.idComplemento;
            }
            set {
                this.idComplemento = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdret_idserie")]
        [SugarColumn(ColumnName = "_cfdret_idserie", ColumnDescription = "ID de serie para el modo edición", Length = 11, IsNullable = true)]
        public int IdSerie {
            get { return this.idSerie; }
            set {
                this.idSerie = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdret_domie")]
        [SugarColumn(ColumnName = "_cfdret_domie", ColumnDescription = "indice de relacion del domicilio emisor", Length = 11, IsNullable = true)]
        public int EmisorIdDomicilio {
            get { return this.emisorIdDomicilio; }
            set {
                this.emisorIdDomicilio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdret_domir")]
        [SugarColumn(ColumnName = "_cfdret_domir", ColumnDescription = "indice de relacion del domicilio receptor", Length = 11, IsNullable = true)]
        public int ReceptorIdDomicilio {
            get { return this.receptorIdDomicilio; }
            set {
                this.receptorIdDomicilio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la precision decimal utilizada para el comprobante
        /// </summary>
        [DataNames("_cfdret_prec")]
        [SugarColumn(ColumnName = "_cfdret_prec", ColumnDescription = "precision decimal utilizada para el comprobante", Length = 2, IsNullable = true)]
        public int PrecisionDecimal {
            get { return this.precisionDecimal; }
            set {
                this.precisionDecimal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión del mes inicial del periodo de la retención e información de pagos
        /// </summary>
        [DataNames("_cfdret_ini")]
        [SugarColumn(ColumnName = "_cfdret_ini", ColumnDescription = "atributo requerido para la expresión del mes inicial del periodo de la retención e información de pagos", Length = 2, IsNullable = true)]
        public int MesInicial {
            get { return this.mesInicial; }
            set {
                this.mesInicial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión del mes final del periodo de la retención e información de pagos
        /// </summary>
        [DataNames("_cfdret_fin")]
        [SugarColumn(ColumnName = "_cfdret_fin", ColumnDescription = "atributo requerido para la expresión del mes final del periodo de la retención e información de pagos", Length = 2, IsNullable = true)]
        public int MesFinal {
            get { return this.mesFinal; }
            set {
                this.mesFinal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión del ejercicio fiscal (año)
        /// </summary>
        [DataNames("_cfdret_eje")]
        [SugarColumn(ColumnName = "_cfdret_eje", ColumnDescription = "atributo requerido para la expresión del ejercicio fiscal (año)", Length = 4, IsNullable = true)]
        public int Ejercicio {
            get { return this.ejercicio; }
            set {
                this.ejercicio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar  el total del monto de la operación  que se relaciona en el comprobante
        /// </summary>
        [DataNames("_cfdret_opr")]
        [SugarColumn(ColumnName = "_cfdret_opr", ColumnDescription = "para expresar  el total del monto de la operación  que se relaciona en el comprobante", Length = 11, DecimalDigits = 4)]
        public decimal MontoTotalOperacion {
            get { return this.montoTotalOperacion; }
            set {
                this.montoTotalOperacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el total del monto gravado de la operación  que se relaciona en el comprobante.
        /// </summary>
        [DataNames("_cfdret_grv")]
        [SugarColumn(ColumnName = "_cfdret_grv", ColumnDescription = "para expresar el total del monto gravado de la operación  que se relaciona en el comprobante.", Length = 11, DecimalDigits = 4)]
        public decimal MontoTotalGravado {
            get { return montoTotalGravado; }
            set {
                montoTotalGravado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el total del monto exento de la operación  que se relaciona en el comprobante.
        /// </summary>
        [DataNames("_cfdret_ext")]
        [SugarColumn(ColumnName = "_cfdret_ext", ColumnDescription = "para expresar el total del monto exento de la operación  que se relaciona en el comprobante.", Length = 11, DecimalDigits = 4)]
        public decimal MontoTotalExento {
            get {
                return montoTotalExento;
            }
            set {
                montoTotalExento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el monto de la utilidad bimestral.
        /// </summary>
        [DataNames("_cfdret_utlb")]
        [SugarColumn(IsIgnore = true, ColumnName = "_cfdret_utlb", ColumnDescription = "para expresar el total del monto exento de la operación  que se relaciona en el comprobante.", Length = 11, DecimalDigits = 4)]
        public decimal UtilidadBimestral {
            get {
                return this.utilidadBimestralField;
            }
            set {
                this.utilidadBimestralField = value;
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el monto del ISR correspondiente al bimestre.
        /// </summary>
        [DataNames("_cfdret_isrc")]
        [SugarColumn(IsIgnore = true, ColumnName = "_cfdret_isrc", ColumnDescription = "para expresar el total del monto exento de la operación  que se relaciona en el comprobante.", Length = 11, DecimalDigits = 4)]
        public decimal ISRCorrespondiente {
            get {
                return this.iSRCorrespondienteField;
            }
            set {
                this.iSRCorrespondienteField = value;
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el monto total de las retenciones. Sumatoria de los montos de retención del nodo ImpRetenidos.
        /// </summary>
        [DataNames("_cfdret_tot")]
        [SugarColumn(ColumnName = "_cfdret_tot", ColumnDescription = "para expresar el monto total de las retenciones. Sumatoria de los montos de retención del nodo ImpRetenidos.", Length = 11, DecimalDigits = 4)]
        public decimal MontoTotalRetencion {
            get {
                return montoTotalRetencion;
            }
            set {
                montoTotalRetencion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// minInclusive value ="2014-01-01T00:00:00-06:00"
        /// pattern value = "-?([1-9][0-9]{3,}|0[0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9](\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))"
        /// </summary>
        [DataNames("_cfdret_fecems")]
        [SugarColumn(ColumnName = "_cfdret_fecems", ColumnDescription = "fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de certificación
        /// </summary>
        [DataNames("_cfdret_feccert")]
        [SugarColumn(ColumnName = "_cfdret_feccert", ColumnDescription = "fecha de certificacion", IsNullable = true)]
        public DateTime? FechaTimbre {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaTimbrado >= firstGoodDate)
                    return this.fechaTimbrado;
                else
                    return null;
            }
            set {
                this.fechaTimbrado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha del estado del comprobante (SAT)
        /// </summary>
        [DataNames("_cfdret_fecedo")]
        [SugarColumn(ColumnName = "_cfdret_fecedo", ColumnDescription = "fecha del estado del comprobante (SAT)", IsNullable = true)]
        public DateTime? FechaEstado {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEstado >= firstGoodDate)
                    return this.fechaEstado;
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaEstado = value;
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdret_feccnc")]
        [SugarColumn(ColumnName = "_cfdret_feccnc", ColumnDescription = "fecha de cancelacion del comprobante", IsNullable = true)]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancela >= firstGoodDate)
                    return this.fechaCancela;
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaCancela = value;
            }
        }

        /// <summary>
        /// fecha de entrega o recepcion del comprobante
        /// </summary>
        [DataNames("_cfdret_fecent")]
        [SugarColumn(ColumnName = "_cfdret_fecent", ColumnDescription = "fecha de entrega o recepcion del comprobante", IsNullable = true)]
        public DateTime? FechaEntrega {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEntrega >= firstGoodDate) {
                    return this.fechaEntrega;
                }
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaEntrega = value;
            }
        }

        /// <summary>
        /// fecha de ultimo pago o cobro del comprobante
        /// </summary>
        [DataNames("_cfdret_fecupc")]
        [SugarColumn(ColumnName = "_cfdret_fecupc", ColumnDescription = "ultima fecha de cobro o pago del comprobante", IsNullable = true)]
        public DateTime? FechaUltPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaUltimoPago >= firstGoodDate) {
                    return this.fechaUltimoPago;
                }
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaUltimoPago = value;
            }
        }

        /// <summary>
        /// fecha de validacion del comprobante
        /// </summary>
        [DataNames("_cfdret_fecval")]
        [SugarColumn(ColumnName = "_cfdret_fecval", ColumnDescription = "fecha de validacion del comprobante", IsNullable = true)]
        public DateTime? FechaVal {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaValidacion >= firstGoodDate)
                    return this.fechaValidacion;
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaValidacion = value;
            }
        }

        [DataNames("_cfdret_fn")]
        [SugarColumn(ColumnName = "_cfdret_fn", ColumnDescription = "fecha de creacion del registro")]
        public DateTime FechaNuevo {
            get {
                return fechaNuevo;
            }
            set {
                fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdret_fm")]
        [SugarColumn(ColumnName = "_cfdret_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica.Value;
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaModifica = value;
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido con valor prefijado que indica la versión del estándar bajo el que se encuentra expresada la retención y/o comprobante de información de pagos.
        /// </summary>
        [DataNames("_cfdret_ver")]
        [SugarColumn(ColumnName = "_cfdret_ver", ColumnDescription = "indica la version del estandar bajo el que se encuentra expresada la retencion y/o comprobante de informacion de pagos.", DefaultValue = "1.0", Length = 3)]
        public string Version {
            get {
                return version;
            }
            set {
                version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para expresar la clave de la retención e información de pagos de acuerdo al catálogo publicado en internet por el SAT.
        /// </summary>
        [DataNames("_cfdret_cve")]
        [SugarColumn(ColumnName = "_cfdret_cve", ColumnDescription = "expresar la clave de la retención e información de pagos de acuerdo al catálogo publicado en internet por el SAT.", IsNullable = true, Length = 3)]
        public string ClaveRetencion {
            get {
                return claveRetencion;
            }
            set {
                claveRetencion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        [DataNames("_cfdret_estado")]
        [SugarColumn(ColumnName = "_cfdret_estado", ColumnDescription = "estado del comprobante del servicio del SAT", Length = 10, IsNullable = true)]
        public string Estado {
            get {
                return estado;
            }
            set {
                estado = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdret_status")]
        [SugarColumn(ColumnName = "_cfdret_status", ColumnDescription = "status del comprobante", Length = 14, DefaultValue = "Pendiente", IsNullable = true)]
        public string Status {
            get {
                return status;
            }
            set {
                status = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        [DataNames("_cfdret_rfce")]
        [SugarColumn(ColumnName = "_cfdret_rfce", ColumnDescription = "registro federal de contribuyentes del emisor del comprobante", Length = 14)]
        public string EmisorRFC {
            get {
                return emisorRFC;
            }
            set {
                emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para incorporar la clave del régimen del contribuyente emisor del comprobante que ampara retenciones e información de pagos.
        /// </summary>
        [DataNames("_cfdret_rgmfe")]
        [SugarColumn(IsIgnore = true, ColumnName = "_cfdret_rgmfe", ColumnDescription = "clave del regimen del contribuyente emisor del comprobante que ampara retenciones e informacion de pagos.", Length = 3)]
        public string RegimenFiscalE {
            get {
                return regimenFiscalE;
            }
            set {
                regimenFiscalE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        [DataNames("_cfdret_rfcr")]
        [SugarColumn(ColumnName = "_cfdret_rfcr", ColumnDescription = "registro federal de contribuyentes del receptor del comprobante", Length = 14)]
        public string ReceptorRFC {
            get {
                return receptorRFC;
            }
            set {
                receptorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del proveedor de certificación de comprobantes fiscales digitales que genera el timbre fiscal digital.
        /// </summary>
        [DataNames("_cfdret_pac")]
        [SugarColumn(ColumnName = "_cfdret_pac", ColumnDescription = "RFC del proveedor de certificación de comprobantes fiscales digitales que genera el timbre fiscal digital.", Length = 14, IsNullable = true)]
        public string RFCProvCertif {
            get {
                if (!(string.IsNullOrEmpty(rfcProveedorCertificacion)))
                    return this.rfcProveedorCertificacion;
                return null;
            }
            set {
                if (!(string.IsNullOrEmpty(value)))
                    this.rfcProveedorCertificacion = value;
            }
        }

        /// <summary>
        /// obtener o establecer atributo opcional para la Clave Única del Registro Poblacional del contribuyente emisor del documento de retención e información de pagos.
        /// </summary>
        [DataNames("_cfdret_curpe")]
        [SugarColumn(ColumnName = "_cfdret_curpe", ColumnDescription = "opcional para la Clave Única del Registro Poblacional del contribuyente emisor del documento de retención e información de pagos.", Length = 18, IsNullable = true)]
        public string EmisorCURP {
            get {
                return emisorCURP;
            }
            set {
                emisorCURP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo opcional para la Clave Única del Registro Poblacional del contribuyente emisor del documento de retención e información de pagos.
        /// </summary>
        [DataNames("_cfdret_curp")]
        [SugarColumn(ColumnName = "_cfdret_curpr", ColumnDescription = "opcional para la Clave Única del Registro Poblacional del contribuyente emisor del documento de retención e información de pagos.", Length = 18, IsNullable = true)]
        public string ReceptorCURP {
            get {
                return receptorCURP;
            }
            set {
                receptorCURP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// opcional para expresar el número de registro de identificación fiscal del receptor del documento cuando sea residente en el extranjero
        /// </summary>
        [DataNames("_cfdret_regidr")]
        [SugarColumn(ColumnName = "_cfdret_regidr", ColumnDescription = "opcional para expresar el número de registro de identificación fiscal del receptor del documento cuando sea residente en el extranjero", Length = 20, IsNullable = true)]
        public string ReceptorNumRegIdTrib {
            get {
                return receptorNumRegIdTrib;
            }
            set {
                receptorNumRegIdTrib = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el código postal del domicilio fiscal del receptor del comprobante que ampara retenciones e información de pagos.
        /// </summary>
        [DataNames("_cfdret_domfr")]
        [SugarColumn(IsIgnore = true, ColumnName = "_cfdret_domfr", ColumnDescription = "codigo postal del domicilio fiscal del receptor del comprobante que ampara retenciones e informacion de pagos.", Length = 5, IsNullable = true)]
        public string ReceptorDomicilioFiscal {
            get {
                return domicilioFiscalR;
            }
            set {
                domicilioFiscalR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para incorporar el código postal del lugar de expedición del comprobante que ampara retenciones e información de pagos.
        /// </summary>
        [DataNames("_cfdret_lgrexp")]
        [SugarColumn(IsIgnore = true, ColumnName = "_cfdret_lgrexp", ColumnDescription = "codigo postal del lugar de expedicion del comprobante que ampara retenciones e informacion de pagos.", Length = 5, IsNullable = true)]
        public string LugarExpRetenc {
            get {
                return lugarExpRetenc;
            }
            set {
                lugarExpRetenc = value;
            }
        }

        /// <summary>
        /// obtener o establecer numero de certificado del emisor del comprobante
        /// </summary>
        [DataNames("_cfdret_nocert")]
        [SugarColumn(ColumnName = "_cfdret_nocert", ColumnDescription = "para expresar el número de serie del certificado de sello digital con el que se selló digitalmente el documento de la retención e información de pagos.", Length = 20, IsNullable = true)]
        public string NoCertificado {
            get {
                return noCertificado;
            }
            set {
                noCertificado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        [DataNames("_cfdret_folio")]
        [SugarColumn(ColumnName = "_cfdret_folio", ColumnDescription = "atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.", Length = 21, IsNullable = true)]
        public string Folio {
            get {
                return folio;
            }
            set {
                folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        [DataNames("_cfdret_serie")]
        [SugarColumn(ColumnName = "_cfdret_serie", ColumnDescription = "serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres.", Length = 25, IsNullable = true)]
        public string Serie {
            get {
                return serie;
            }
            set {
                serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de documento en modo texto
        /// </summary>
        [DataNames("_cfdret_doc")]
        [SugarColumn(ColumnName = "_cfdret_doc", ColumnDescription = "documento (factura, nota de credito,nota de cargo)", Length = 36, IsNullable = true)]
        public string Documento {
            get {
                return documento;
            }
            set {
                documento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        [DataNames("_cfdret_uuid")]
        [SugarColumn(ColumnName = "_cfdret_uuid", ColumnDescription = "36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122", Length = 36, IsNullable = true)]
        public string IdDocumento {
            get {
                return idDocumento;
            }
            set {
                idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo opcional que expresa la descripción de la retención e información de pagos en caso de que en el atributo CveRetenc se haya elegido el valor para 'otro tipo de retenciones'
        /// </summary>
        [DataNames("_cfdret_desc")]
        [SugarColumn(ColumnName = "_cfdret_desc", ColumnDescription = "opcional que expresa la descripción de la retención e información de pagos en caso de que en el atributo CveRetenc se haya elegido el valor para otro tipo de retenciones", Length = 100, IsNullable = true)]
        public string Descripcion {
            get {
                return descripcion;
            }
            set {
                descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        [DataNames("_cfdret_nome")]
        [SugarColumn(ColumnName = "_cfdret_nome", ColumnDescription = "para incorporar la clave en el Registro Federal de Contribuyentes correspondiente al contribuyente emisor del documento de retención e información de pagos, sin guiones o espacios.", Length = 300)]
        public string EmisorNombre {
            get {
                return emisorNombre;
            }
            set {
                emisorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        [DataNames("_cfdret_nomr")]
        [SugarColumn(ColumnName = "_cfdret_nomr", ColumnDescription = "para el nombre, denominación o razón social del contribuyente receptor del documento.", Length = 300)]
        public string ReceptorNombre {
            get {
                return receptorNombre;
            }
            set {
                receptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdret_compl")]
        [SugarColumn(ColumnName = "_cfdret_compl", ColumnDataType = "TEXT", ColumnDescription = "complemento", IsNullable = true)]
        public string JComplementos {
            get {
                if (this.Pagosaextranjeros != null)
                    return this.Pagosaextranjeros.Json();
                return null;
            }
            set {
                if (!string.IsNullOrEmpty(value)) {
                    this.Pagosaextranjeros = Complemento.ComplementoPagosaextranjeros.Json(value);
                    this.OnPropertyChanged();
                }
            }
        }

        [SugarColumn(IsIgnore = true)]
        public Complemento.ComplementoPagosaextranjeros Pagosaextranjeros {
            get { return this._Pagosaextranjeros; }
            set {
                this._Pagosaextranjeros = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el acuse de validacion del comprobante
        /// </summary>
        [DataNames("_cfdret_val")]
        [SugarColumn(ColumnName = "_cfdret_val", ColumnDataType = "TEXT", ColumnDescription = "validacion", IsNullable = true)]
        public string JValidacion {
            get {
                return jValidacion;
            }
            set {
                jValidacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el contenido del archivo del acuse de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdret_acuse")]
        [SugarColumn(ColumnName = "_cfdret_acuse", ColumnDataType = "TEXT", ColumnDescription = "acuse de cancelacion", IsNullable = true)]
        public string JAccuse {
            get {
                return jAccuse;
            }
            set {
                jAccuse = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdret_adenda")]
        [SugarColumn(ColumnName = "_cfdret_adenda", ColumnDataType = "TEXT", ColumnDescription = "addenda", IsNullable = true)]
        public string JAddenda {
            get {
                return jAddenda;
            }
            set {
                jAddenda = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdret_impst")]
        [SugarColumn(ColumnName = "_cfdret_impuesto", ColumnDataType = "TEXT", ColumnDescription = "para expresar el total de los impuestos retenidos que se desprenden de los conceptos expresados en el documento de retenciones e información de pagos.(json)", IsNullable = true)]
        public string JImpuestosRetenidos {
            get {
                if (this.ImpuestosRetenidos != null) {
                    if (this.ImpuestosRetenidos.Count > 0) {
                        return Newtonsoft.Json.JsonConvert.SerializeObject(this.ImpuestosRetenidos, new Newtonsoft.Json.JsonSerializerSettings { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore });
                    }
                }
                return null;
            }
            set {
                if (!string.IsNullOrEmpty(value)) {
                    try {
                        this.ImpuestosRetenidos = Newtonsoft.Json.JsonConvert.DeserializeObject<BindingList<RetencionImpuestosDetailModel>>(value);
                    } catch (Exception ex) {
                        Console.WriteLine(ex.ToString());
                    }
                    this.OnPropertyChanged();
                }
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<RetencionImpuestosDetailModel> ImpuestosRetenidos {
            get {
                return this.impuestosRetenidos;
            }
            set {
                this.impuestosRetenidos = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public RetencionesCfdiRetenRelacionados Relacionados {
            get { return this.relacionados; }
            set {
                this.relacionados = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer url del archivo XML del comprobante fiscal
        /// </summary>
        [DataNames("_cfdret_url_xml")]
        [SugarColumn(ColumnName = "_cfdret_url_xml", ColumnDataType = "TEXT", ColumnDescription = "url de descarga para el archivo xml", IsNullable = true)]
        public string FileXML {
            get {
                return fileXML;
            }
            set {
                fileXML = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        [DataNames("_cfdret_url_pdf")]
        [SugarColumn(ColumnName = "_cfdret_url_pdf", ColumnDataType = "TEXT", ColumnDescription = "url de descarga para la representación impresa", IsNullable = true)]
        public string FilePDF {
            get {
                return filePDF;
            }
            set {
                filePDF = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la url del archivo de acuse de cancelacion
        /// </summary>
        [DataNames("_cfdret_url_acu")]
        [SugarColumn(ColumnName = "_cfdret_url_acu", ColumnDataType = "TEXT", ColumnDescription = "url de descarga de la representacio impresa del acusde de cancelacion del comprobante", IsNullable = true)]
        public string FileAccuse {
            get {
                return fileAccuse;
            }
            set {
                fileAccuse = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer url de descarga del archivo xml del acuse de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdret_url_xmlacu")]
        [SugarColumn(ColumnName = "_cfdret_url_xmlacu", ColumnDataType = "TEXT", ColumnDescription = "url de descarga del archivo xml del acuse de cancelacion del comprobante", IsNullable = true)]
        public string FileAccuseXML {
            get {
                return fileAccuseXML;
            }
            set {
                fileAccuseXML = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer url de descarga del archivo pdf del acuse de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdret_url_pdfacu")]
        [SugarColumn(ColumnName = "_cfdret_url_pdfacu", ColumnDataType = "TEXT", ColumnDescription = "url de descarga del archivo pdf del acuse de cancelacion del comprobante", IsNullable = true)]
        public string FileAccusePDF {
            get {
                return fileAccusePDF;
            }
            set {
                fileAccusePDF = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el contenido del archivo XML
        /// </summary>
        [DataNames("_cfdret_save")]
        [SugarColumn(ColumnName = "_cfdret_save", ColumnDataType = "TEXT", ColumnDescription = "XML Completo en formato JSON (Al guardar en S3 se borra)", IsNullable = true)]
        public string XML {
            get {
                return xML;
            }
            set {
                xML = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdret_usr_n")]
        [SugarColumn(ColumnName = "_cfdret_usr_n", ColumnDescription = "clave de usuario que crea el registro", Length = 14, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return creo;
            }
            set {
                creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdret_usr_m")]
        [SugarColumn(ColumnName = "_cfdret_usr_m", ColumnDescription = "clave del ultimo usuario que modifico el registro", Length = 14, IsOnlyIgnoreInsert = true, IsNullable = true)]
        public string Modifica {
            get {
                return modifica;
            }
            set {
                modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdret_sync")]
        [SugarColumn(ColumnName = "_cfdret_sync", ColumnDescription = "", DefaultValue = "0")]
        public bool Sincronizado {
            get {
                return sincronizado;
            }
            set {
                sincronizado = value;
                this.OnPropertyChanged();
            }
        }
    }
}
