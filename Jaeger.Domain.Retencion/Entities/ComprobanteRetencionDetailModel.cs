﻿using System;
using System.ComponentModel;
using System.Linq;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Retencion.Contracts;
using Jaeger.Domain.Retencion.ValueObjects;
using Jaeger.Domain.Services;
using SqlSugar;

namespace Jaeger.Domain.Retencion.Entities {
    /// <summary>
    /// Estándar de Documento Electrónico Retenciones e Información de Pagos.
    /// </summary>
    [SugarTable("_cfdret", "estandar de documento electronico retenciones e informacion de pagos.")]
    public class ComprobanteRetencionDetailModel : ComprobanteRetencionModel, IComprobanteRetencionDetailModel {

        #region declaraciones
        private Complemento.Complementos complementos;
        private decimal tipoCambio;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ComprobanteRetencionDetailModel() {
            this.Version = "1.0";
            this.Documento = "retencion";
            this.FechaEmision = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
            this.complementos = new Complemento.Complementos();
            this.PrecisionDecimal = 2;
            this.FechaNuevo = DateTime.Now;
            this.Activo = true;
            this.ImpuestosRetenidos = new BindingList<RetencionImpuestosDetailModel>();
            this.ImpuestosRetenidos.AddingNew += new AddingNewEventHandler(this.ImpuestosRetenidos_AddingNew);
            this.ImpuestosRetenidos.ListChanged += new ListChangedEventHandler(this.ImpuestosRetenidos_ListChanged);
        }

        #region propiedades

        /// <summary>
        /// fecha de validacion del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public DateTime? FechaValida {
            get {
                return this.FechaVal;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal TipoCambio {
            get {
                return this.tipoCambio;
            }
            set {
                this.tipoCambio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// complementos del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public Complemento.Complementos Complementos {
            get {
                return this.complementos;
            }
            set {
                this.complementos = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// complemento del timbre fiscal
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ComplementoTimbreFiscal TimbreFiscal {
            get {
                if (ValidacionService.UUID(this.IdDocumento))
                    return new ComplementoTimbreFiscal {
                        UUID = this.IdDocumento,
                        NoCertificadoSAT = this.NoCertificado,
                        FechaTimbrado = this.FechaTimbre,
                        RFCProvCertif = this.RFCProvCertif
                    };
                return null;
            }
            set {
                this.IdDocumento = value.UUID;
                this.RFCProvCertif = value.RFCProvCertif;
                this.NoCertificado = value.NoCertificadoSAT;
                this.FechaTimbre = value.FechaTimbrado;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region propiedades ignoradas

        private bool editable = false;
        /// <summary>
        /// obtiene si el comprobante es editable, si existe timbre fiscal entonces el comprobante no es editable
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public bool Editable {
            get {
                this.editable = !ValidacionService.UUID(this.IdDocumento);
                return this.editable;
            }
            set {
                this.editable = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CFDISubTypeEnum SubTipo {
            get {
                return (CFDISubTypeEnum)Enum.Parse(typeof(CFDISubTypeEnum), this.SubTipoInt.ToString());
            }
            set {
                this.SubTipoInt = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public bool XmlDisponible {
            get { return ValidacionService.URL(this.FileXML); }
        }

        [SugarColumn(IsIgnore = true)]
        public bool PdfDisponible {
            get { return ValidacionService.URL(this.FilePDF); }
        }
        #endregion

        #region metodos privados
        private void Conceptos_AddingNew(object sender, AddingNewEventArgs e) {
            //e.NewObject = new ComprobanteConceptoModel { SubId = this.Id };
        }

        private void ImpuestosRetenidos_AddingNew(object sender, AddingNewEventArgs e) {
            e.NewObject = new RetencionImpuestosDetailModel { IdComprobante = this.Id };
        }

        private void ImpuestosRetenidos_ListChanged(object sender, ListChangedEventArgs e) {
            this.MontoTotalOperacion = this.ImpuestosRetenidos.Where((RetencionImpuestosDetailModel p) => p.Activo == true).Sum((RetencionImpuestosDetailModel p) => p.Base);
            this.MontoTotalExento = this.ImpuestosRetenidos.Where((RetencionImpuestosDetailModel p) => p.Activo == true).Sum((RetencionImpuestosDetailModel p) => p.Exento);
            this.MontoTotalGravado = this.ImpuestosRetenidos.Where((RetencionImpuestosDetailModel p) => p.Activo == true).Sum((RetencionImpuestosDetailModel p) => p.Gravado);
            this.MontoTotalRetencion = this.ImpuestosRetenidos.Where((RetencionImpuestosDetailModel p) => p.Activo == true).Sum((RetencionImpuestosDetailModel p) => p.MontoRetenido);
        }
        #endregion

        #region metodos publicos
        public string KeyName() {
            string nombre = "CFDI-";
            if (this.TimbreFiscal != null) {
                nombre = string.Concat("CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.TimbreFiscal.UUID, "-", this.TimbreFiscal.FechaTimbrado.Value.ToString("yyyyMMddHHmmss"));
            }
            else {
                nombre = string.Concat("CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.Serie, "-", this.Folio, DateTime.Now.ToString("yyyyMMddHHmmss"));
            }
            return nombre;
        }

        /// <summary>
        /// establecer el contenido del comprobante con las opciones por default
        /// </summary>
        public void Default() {
            if (this.SubTipo == CFDISubTypeEnum.Emitido) {
            
            }
        }

        /// <summary>
        /// crear copia de la clase actual
        /// </summary>
        public void Clonar() {
            if (this.SubTipo == CFDISubTypeEnum.Emitido) {
                // datos generales
                this.Id = 0;
                this.Folio = null;
                this.TimbreFiscal = null;
                this.FechaEmision = DateTime.Now;
                this.FilePDF = null;
                this.FileXML = null;
                this.Estado = null;
                this.IdDocumento = null;
                this.FechaTimbre = null;
                this.Status = "Pendiente";
            }
        }
        #endregion
    }
}
