﻿namespace Jaeger.Domain.Retencion.Entities {
    /// <summary>
    /// Nodo opcional para precisar la información de los comprobantes relacionados.
    /// </summary>
    public partial class RetencionesCfdiRetenRelacionados : Base.Abstractions.BasePropertyChangeImplementation {
        private string tipoRelacionField;
        private string uUIDField;

        /// <summary>
        /// Atributo requerido para indicar la clave de la relación que existe entre éste que se está generando y el comprobante que ampara retenciones e información de pagos previos.
        /// </summary>
        public string TipoRelacion {
            get {
                return this.tipoRelacionField;
            }
            set {
                this.tipoRelacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el folio fiscal (UUID) de un comprobante que ampara retención e información de pagos, relacionado con el presente comprobante, ejemplo: Si éste sustituye a un comprobante cancelado.
        /// </summary>
        public string UUID {
            get {
                return this.uUIDField;
            }
            set {
                this.uUIDField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
