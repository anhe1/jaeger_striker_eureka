﻿namespace Jaeger.Domain.Retencion.Entities {
    /// <summary>
    /// Estándar de Documento Electrónico Retenciones e Información de Pagos.
    /// </summary>
    public class RetencionImpuestosDetailModel : RetencionImpuestosModel {

        public RetencionImpuestosDetailModel() : base() {
            
        }
    }
}
