﻿using SqlSugar;
using Newtonsoft.Json;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Retencion.Entities {
    /// <summary>
    /// Nodo opcional para expresar el total de los impuestos retenidos que se desprenden de los conceptos expresados en el documento de retenciones e información de pagos.
    /// </summary>
    [SugarTable("_cfdreti", "impuestos retenidos que se desprenden de los conceptos expresados en el documento de retenciones e información de pagos.")]
    public class RetencionImpuestosModel {

        public RetencionImpuestosModel() {
            this.Activo = true;
        }

        [JsonIgnore]
        [DataNames("_cfdreti_id")]
        [SugarColumn(ColumnName = "_cfdreti_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id { get; set; }

        [JsonIgnore]
        [DataNames("_cfdreti_a")]
        [SugarColumn(ColumnName = "_cfdreti_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo { get; set; }

        [JsonIgnore]
        [DataNames("_cfdreti_sbid")]
        [SugarColumn(ColumnName = "_cfdreti_sbid", ColumnDescription = "indice de relacion con la tabla de comprobante de retenciones")]
        public int IdComprobante { get; set; }

        /// <summary>
        /// Atributo opcional para señalar el tipo de impuesto retenido del periodo o ejercicio conforme al catálogo.
        /// </summary>
        [JsonProperty("impuesto")]
        [DataNames("_cfdreti_timp")]
        [SugarColumn(ColumnName = "_cfdreti_timp", ColumnDescription = "tipo de impuesto retenido del periodo o ejercicio conforme al catalogo.", Length = 2)]
        public string TipoImpuesto { get; set; }

        /// <summary>
        /// Atributo requerido para precisar si el monto de la retención es considerado pago definitivo o pago provisional
        /// </summary>
        [JsonProperty("tipo")]
        [DataNames("_cfdreti_tpg")]
        [SugarColumn(ColumnName = "_cfdreti_tpg", ColumnDescription = "para precisar si el monto de la retención es considerado pago definitivo o pago provisional", Length = 20)]
        public string TipoPago { get; set; }

        /// <summary>
        /// Atributo opcional para expresar la  base del impuesto, que puede ser la diferencia entre los ingresos percibidos y las deducciones autorizadas
        /// </summary>
        [JsonProperty("base")]
        [DataNames("_cfdreti_base")]
        [SugarColumn(ColumnName = "_cfdreti_base", ColumnDescription = "para expresar la  base del impuesto, que puede ser la diferencia entre los ingresos percibidos y las deducciones autorizadas", Length = 14, DecimalDigits = 4)]
        public decimal Base { get; set; }

        /// <summary>
        /// obtener o establecer para expresar el total del monto gravado de la operación  que se relaciona en el comprobante.
        /// </summary>
        [JsonProperty("gravado")]
        [DataNames("_cfdreti_grv")]
        [SugarColumn(ColumnName = "_cfdreti_grv", ColumnDescription = "para expresar el total del monto gravado de la operación  que se relaciona en el comprobante.", Length = 14, DecimalDigits = 4)]
        public decimal Gravado { get; set; }

        [JsonProperty("exento")]
        [DataNames("_cfdreti_exe")]
        [SugarColumn(ColumnName = "_cfdreti_exe", ColumnDescription = "para expresar el total del monto exento de la operación  que se relaciona en el comprobante.", Length = 14, DecimalDigits = 4)]
        public decimal Exento { get; set; }

        /// <summary>
        /// Atributo requerido para expresar el importe del impuesto retenido en el periodo o ejercicio
        /// </summary>
        [JsonProperty("monto")]
        [DataNames("_cfdreti_mnt")]
        [SugarColumn(ColumnName = "_cfdreti_mnt", ColumnDescription = "para expresar el monto total de las retenciones. Sumatoria de los montos de retención del nodo ImpRetenidos.", Length = 14, DecimalDigits = 4)]
        public decimal MontoRetenido { get; set; }
    }
}
