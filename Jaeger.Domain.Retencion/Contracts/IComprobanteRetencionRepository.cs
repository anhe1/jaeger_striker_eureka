﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Retencion.Entities;
using Jaeger.Domain.Retencion.ValueObjects;

namespace Jaeger.Domain.Retencion.Contracts {
    public interface IComprobanteRetencionRepository : IGenericRepository<ComprobanteRetencionModel> {

        ComprobanteRetencionDetailModel GetComprobante(int index);

        ComprobanteRetencionDetailModel Save(ComprobanteRetencionDetailModel item);

        IEnumerable<RetencionSingle> ComprobanteSingles(CFDISubTypeEnum subTipo, int year, int month);

        IEnumerable<RetencionSingle> GetSearch(int folio);

        /// <summary>
        /// actualizar url del archivo XML del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">direccion URL</param>
        /// <returns>verdadero si la acción se realiza con exito</returns>
        bool UpdateUrlXml(int index, string url);

        /// <summary>
        /// actualizar url del archivo XML del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">direccion URL</param>
        /// <returns>verdadero si la acción se realiza con exito</returns>
        bool UpdateUrlPdf(int index, string url);

        /// <summary>
        /// actualizar url del acuse de cancelacion del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">url</param>
        /// <returns>verdadero si la accion se realiza con exito</returns>
        bool UpdateUrlXmlAcuse(int index, string url);

        /// <summary>
        /// actualizar url del acuse de cancelacion del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">url</param>
        /// <returns>verdadero si la accion se realiza con exito</returns>
        bool UpdateUrlPdfAcuse(int index, string url);

        /// <summary>
        /// actualizar estado del comprobante
        /// </summary>
        /// <param name="idDocumento">folio fiscal del comprobante</param>
        /// <param name="estado">estado del comprobante</param>
        /// <param name="usuario">clave del usuario</param>
        /// <returns>true al actualizar base de datos</returns>
        bool UpdateEstado(string idDocumento, string estado);

        /// <summary>
        /// metodo para actualizar el status del comprobante fiscal
        /// </summary>
        /// <param name="indice">indice del comprobante</param>
        /// <param name="status">nuevo status</param>
        /// <param name="usuario">clave del usuario</param>
        bool Update(int indice, string status);

        bool Create();
    }
}
