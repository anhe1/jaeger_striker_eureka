﻿using System;

namespace Jaeger.Domain.Retencion.Contracts {
    interface IComprobanteRetencionModel {
        /// <summary>
        /// obtener ó establecer el indice de la tabla.
        /// </summary>
        int Id {
            get;set;
        }

        /// <summary>
        /// obtener ó establecer si el registro es activo.
        /// </summary>
        bool Activo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio
        /// </summary>
        int IdDirectorio {
            get; set;
        }

        /// <summary>
        /// obtener ó establecer si el registro es activo.
        /// </summary>
        bool ReceptorExtranjero {
            get; set;
        }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        int SubTipoInt {
            get; set;
        }

        int IdAddenda {
            get; set;
        }

        int IdSerie {
            get; set;
        }

        int IdComplemento {
            get; set;
        }

        int EmisorIdDomicilio {
            get; set;
        }

        int ReceptorIdDomicilio {
            get; set;
        }

        /// <summary>
        /// obtener o establecer atributo requerido con valor prefijado que indica la versión del estándar bajo el que se encuentra expresada la retención y/o comprobante de información de pagos.
        /// </summary>
        string Version {
            get; set;
        }

        /// <summary>
        /// obtener o establecer tipo de documento en modo texto
        /// </summary>
        string Documento {
            get; set;
        }

        string Status {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        string Estado {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        string Serie {
            get; set;
        }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        string Folio {
            get; set;
        }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        string EmisorRFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer atributo opcional para la Clave Única del Registro Poblacional del contribuyente emisor del documento de retención e información de pagos.
        /// </summary>
        string EmisorCURP {
            get; set;
        }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        string EmisorNombre {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        string ReceptorRFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer atributo opcional para la Clave Única del Registro Poblacional del contribuyente emisor del documento de retención e información de pagos.
        /// </summary>
        string ReceptorCURP {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        string ReceptorNombre {
            get; set;
        }

        /// <summary>
        /// opcional para expresar el número de registro de identificación fiscal del receptor del documento cuando sea residente en el extranjero
        /// </summary>
        string ReceptorNumRegIdTrib {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la precision decimal utilizada para el comprobante
        /// </summary>
        int PrecisionDecimal {
            get; set;
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión del mes inicial del periodo de la retención e información de pagos
        /// </summary>
        int MesInicial {
            get; set;
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión del mes final del periodo de la retención e información de pagos
        /// </summary>
        int MesFinal {
            get; set;
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión del ejercicio fiscal (año)
        /// </summary>
        int Ejercicio {
            get; set;
        }

        /// <summary>
        /// Atributo requerido para expresar  el total del monto de la operación  que se relaciona en el comprobante
        /// </summary>
        decimal MontoTotalOperacion {
            get; set;
        }

        /// <summary>
        /// Atributo requerido para expresar el total del monto gravado de la operación  que se relaciona en el comprobante.
        /// </summary>
        decimal MontoTotalGravado {
            get; set;
        }

        /// <summary>
        /// Atributo requerido para expresar el total del monto exento de la operación  que se relaciona en el comprobante.
        /// </summary>
        decimal MontoTotalExento {
            get; set;
        }

        /// <summary>
        /// Atributo requerido para expresar el monto total de las retenciones. Sumatoria de los montos de retención del nodo ImpRetenidos.
        /// </summary>
        decimal MontoTotalRetencion {
            get; set;
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// minInclusive value ="2014-01-01T00:00:00-06:00"
        /// pattern value = "-?([1-9][0-9]{3,}|0[0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9](\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))"
        /// </summary>
        DateTime FechaEmision {
            get; set;
        }

        /// <summary>
        /// fecha de certificación
        /// </summary>
        DateTime? FechaTimbre {
            get; set;
        }

        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        string IdDocumento {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la fecha del estado del comprobante (SAT)
        /// </summary>
        DateTime? FechaEstado {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        DateTime? FechaCancela {
            get; set;
        }

        /// <summary>
        /// fecha de entrega o recepcion del comprobante
        /// </summary>
        DateTime? FechaEntrega {
            get; set;
        }

        /// <summary>
        /// fecha de ultimo pago o cobro del comprobante
        /// </summary>
        DateTime? FechaUltPago {
            get; set;
        }

        /// <summary>
        /// fecha de validacion del comprobante
        /// </summary>
        DateTime? FechaVal {
            get; set;
        }

        /// <summary>
        /// obtener o establecer atributo requerido para expresar la clave de la retención e información de pagos de acuerdo al catálogo publicado en internet por el SAT.
        /// </summary>
        string ClaveRetencion {
            get; set;
        }

        /// <summary>
        /// obtener o establecer numero de certificado del emisor del comprobante
        /// </summary>
        string NoCertificado {
            get; set;
        }

        /// <summary>
        /// obtener o establecer atributo opcional que expresa la descripción de la retención e información de pagos en caso de que en el atributo CveRetenc se haya elegido el valor para 'otro tipo de retenciones'
        /// </summary>
        string Descripcion {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el RFC del proveedor de certificación de comprobantes fiscales digitales que genera el timbre fiscal digital.
        /// </summary>
        string RFCProvCertif {
            get; set;
        }

        /// <summary>
        /// obtener o establecer url del archivo XML del comprobante fiscal
        /// </summary>
        string FileXML {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        string FilePDF {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la url del archivo de acuse de cancelacion
        /// </summary>
        string FileAccuse {
            get; set;
        }

        /// <summary>
        /// obtener o establecer url de descarga del archivo xml del acuse de cancelacion del comprobante
        /// </summary>
        string FileAccuseXML {
            get; set;
        }

        /// <summary>
        /// obtener o establecer url de descarga del archivo pdf del acuse de cancelacion del comprobante
        /// </summary>
        string FileAccusePDF {
            get; set;
        }

        string JComplementos {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el acuse de validacion del comprobante
        /// </summary>
        string JValidacion {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el contenido del archivo del acuse de cancelacion del comprobante
        /// </summary>
        string JAccuse {
            get; set;
        }

        string JAddenda {
            get; set;
        }

        string JImpuestosRetenidos {
            get; set;
        }

        bool Sincronizado {
            get; set;
        }

        string Creo {
            get; set;
        }

        string Modifica {
            get; set;
        }

        DateTime FechaNuevo {
            get; set;
        }

        DateTime? FechaModifica {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el contenido del archivo XML
        /// </summary>
        string XML {
            get; set;
        }
    }
}