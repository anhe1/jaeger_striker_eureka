﻿using System.ComponentModel;

namespace Jaeger.Domain.Retencion.ValueObjects {
    /// <summary>
    /// representa el subtipo del comprobante fiscal
    /// </summary>
    public enum CFDISubTypeEnum {
        [Description("Todos")]
        None,
        [Description("Emitido")]
        Emitido,
        [Description("Recibido")]
        Recibido,
    }
}