﻿using System.ComponentModel;

namespace Jaeger.Domain.Retencion.ValueObjects {
    public enum RetencionComplementoEnum {
        [Description("Dividendos")]
        Dividendos = 2,
        [Description("Pago a Extranjeros")]
        Pagosaextranjeros = 8
    }
}
