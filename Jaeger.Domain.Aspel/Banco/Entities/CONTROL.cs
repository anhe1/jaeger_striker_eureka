﻿using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Banco.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("control")]
    public partial class CONTROL : BasePropertyChangeImplementation {
        private int? _CTMONED;
        private int? _CTCTAS;
        private int? _CTCOMD;
        private int? _CTBENEFICIARIOS;
        private int? _CTFORMPAG;
        private int? _CTCTECONC;
        private int? _CTTRANSFERENCIA;
        private int? _VER_BASDAT;
        private int? _NUM_EMP;

        public CONTROL() {


        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CTMONED")]
        [SugarColumn(ColumnName = "ctmoned", ColumnDescription = "")]
        public int? CTMONED {
            get {
                return this._CTMONED;
            }
            set {
                this._CTMONED = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CTCTAS")]
        [SugarColumn(ColumnName = "ctctas", ColumnDescription = "")]
        public int? CTCTAS {
            get {
                return this._CTCTAS;
            }
            set {
                this._CTCTAS = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CTCOMD")]
        [SugarColumn(ColumnName = "ctcomd", ColumnDescription = "")]
        public int? CTCOMD {
            get {
                return this._CTCOMD;
            }
            set {
                this._CTCOMD = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CTBENEFICIARIOS")]
        [SugarColumn(ColumnName = "ctbeneficiarios", ColumnDescription = "")]
        public int? CTBENEFICIARIOS {
            get {
                return this._CTBENEFICIARIOS;
            }
            set {
                this._CTBENEFICIARIOS = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CTFORMPAG")]
        [SugarColumn(ColumnName = "ctformpag", ColumnDescription = "")]
        public int? CTFORMPAG {
            get {
                return this._CTFORMPAG;
            }
            set {
                this._CTFORMPAG = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CTCTECONC")]
        [SugarColumn(ColumnName = "ctcteconc", ColumnDescription = "")]
        public int? CTCTECONC {
            get {
                return this._CTCTECONC;
            }
            set {
                this._CTCTECONC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CTTRANSFERENCIA")]
        [SugarColumn(ColumnName = "cttransferencia", ColumnDescription = "")]
        public int? CTTRANSFERENCIA {
            get {
                return this._CTTRANSFERENCIA;
            }
            set {
                this._CTTRANSFERENCIA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("VER_BASDAT")]
        [SugarColumn(ColumnName = "ver_basdat", ColumnDescription = "")]
        public int? VER_BASDAT {
            get {
                return this._VER_BASDAT;
            }
            set {
                this._VER_BASDAT = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("NUM_EMP")]
        [SugarColumn(ColumnName = "num_emp", ColumnDescription = "")]
        public int? NUM_EMP {
            get {
                return this._NUM_EMP;
            }
            set {
                this._NUM_EMP = value;
                this.OnPropertyChanged();
            }
        }
    }
}
