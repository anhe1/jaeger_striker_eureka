﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Banco.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("moneda")]
    public partial class MonedaModel : BasePropertyChangeImplementation {
        private int _NUM_REG;
        private string _MONEDA;
        private string _PREFIJO;
        private DateTime? _FECHA;
        private double? _TIPOCAMBIO;
        private string _LEY_SING;
        private string _LEY_PLUR;
        private string _TERMINA;
        private string _IDIOMA;
        private string _MONEDASAT;

        public MonedaModel() {

        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_REG")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "num_reg", ColumnDescription = "")]
        public int NUM_REG {
            get {
                return this._NUM_REG;
            }
            set {
                this._NUM_REG = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MONEDA")]
        [SugarColumn(ColumnName = "moneda", ColumnDescription = "")]
        public string MONEDA {
            get {
                return this._MONEDA;
            }
            set {
                this._MONEDA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PREFIJO")]
        [SugarColumn(ColumnName = "prefijo", ColumnDescription = "")]
        public string PREFIJO {
            get {
                return this._PREFIJO;
            }
            set {
                this._PREFIJO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:current_timestamp() Nullable_New:True
        /// </summary>
        [DataNames("FECHA")]
        [SugarColumn(ColumnName = "fecha", ColumnDescription = "")]
        public DateTime? FECHA {
            get {
                return this._FECHA;
            }
            set {
                this._FECHA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:1 Nullable_New:True
        /// </summary>
        [DataNames("TIPOCAMBIO")]
        [SugarColumn(ColumnName = "tipocambio", ColumnDescription = "")]
        public double? TIPOCAMBIO {
            get {
                return this._TIPOCAMBIO;
            }
            set {
                this._TIPOCAMBIO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("LEY_SING")]
        [SugarColumn(ColumnName = "ley_sing", ColumnDescription = "")]
        public string LEY_SING {
            get {
                return this._LEY_SING;
            }
            set {
                this._LEY_SING = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("LEY_PLUR")]
        [SugarColumn(ColumnName = "ley_plur", ColumnDescription = "")]
        public string LEY_PLUR {
            get {
                return this._LEY_PLUR;
            }
            set {
                this._LEY_PLUR = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TERMINA")]
        [SugarColumn(ColumnName = "termina", ColumnDescription = "")]
        public string TERMINA {
            get {
                return this._TERMINA;
            }
            set {
                this._TERMINA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:'E' Nullable_New:True
        /// </summary>
        [DataNames("IDIOMA")]
        [SugarColumn(ColumnName = "idioma", ColumnDescription = "")]
        public string IDIOMA {
            get {
                return this._IDIOMA;
            }
            set {
                this._IDIOMA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MONEDASAT")]
        [SugarColumn(ColumnName = "monedasat", ColumnDescription = "")]
        public string MONEDASAT {
            get {
                return this._MONEDASAT;
            }
            set {
                this._MONEDASAT = value;
                this.OnPropertyChanged();
            }
        }
    }
}
