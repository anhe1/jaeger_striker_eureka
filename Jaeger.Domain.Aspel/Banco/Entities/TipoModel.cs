﻿namespace Jaeger.Domain.Aspel.Banco.Entities {
    public class TipoModel : Domain.Base.Abstractions.BaseSingleModel {
        public TipoModel() {
        }

        public TipoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
