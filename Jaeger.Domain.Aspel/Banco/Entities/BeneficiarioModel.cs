﻿using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Banco.Entities {
    ///<summary>
    /// Tabla Aspel Bancos BENEF
    ///</summary>
    [SugarTable("benef")]
    public partial class BeneficiarioModel : BasePropertyChangeImplementation {
        private int _NUM_REG;
        private string _NOMBRE;
        private string _RFC;
        private string _CTA_CONTAB;
        private string _TIPO;
        private string _INF_GENERAL;
        private string _REFERENCIA;
        private string _BANCO;
        private string _SUCURSAL;
        private string _CUENTA;
        private string _CLABE;
        private string _CVE_BANCO;
        private int? _ESBANCOEXT;
        private string _BANCODESC;
        private string _RFCBANCO;

        public BeneficiarioModel() {

        }

        /// <summary>
        /// obtener o establecer numero de registro, indice de la tabla
        /// </summary>
        [DataNames("NUM_REG")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "num_reg")]
        public int NUM_REG {
            get {
                return this._NUM_REG;
            }
            set {
                this._NUM_REG = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre completo del beneficiario
        /// </summary>
        [DataNames("NOMBRE")]
        [SugarColumn(ColumnName = "nombre", ColumnDescription = "nombre completo del beneficiario", Length = 60)]
        public string NOMBRE {
            get {
                return this._NOMBRE;
            }
            set {
                this._NOMBRE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes correspondiente al beneficiario
        /// </summary>
        [DataNames("RFC")]
        [SugarColumn(ColumnName = "rfc", ColumnDescription = "registro federal de contribuyentes correspondiente al beneficiario", Length = 15)]
        public string RFC {
            get {
                return this._RFC;
            }
            set {
                this._RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTA_CONTAB")]
        [SugarColumn(ColumnName = "cta_contab")]
        public string CTA_CONTAB {
            get {
                return this._CTA_CONTAB;
            }
            set {
                this._CTA_CONTAB = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:'BENEFICIARIO' Nullable_New:True
        /// </summary>
        [DataNames("TIPO")]
        [SugarColumn(ColumnName = "tipo")]
        public string TIPO {
            get {
                return this._TIPO;
            }
            set {
                this._TIPO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("INF_GENERAL")]
        [SugarColumn(ColumnName = "inf_general")]
        public string INF_GENERAL {
            get {
                return this._INF_GENERAL;
            }
            set {
                this._INF_GENERAL = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("REFERENCIA")]
        [SugarColumn(ColumnName = "referencia")]
        public string REFERENCIA {
            get {
                return this._REFERENCIA;
            }
            set {
                this._REFERENCIA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("BANCO")]
        [SugarColumn(ColumnName = "banco")]
        public string BANCO {
            get {
                return this._BANCO;
            }
            set {
                this._BANCO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("SUCURSAL")]
        [SugarColumn(ColumnName = "sucursal")]
        public string SUCURSAL {
            get {
                return this._SUCURSAL;
            }
            set {
                this._SUCURSAL = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CUENTA")]
        [SugarColumn(ColumnName = "cuenta")]
        public string CUENTA {
            get {
                return this._CUENTA;
            }
            set {
                this._CUENTA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CLABE")]
        [SugarColumn(ColumnName = "clabe")]
        public string CLABE {
            get {
                return this._CLABE;
            }
            set {
                this._CLABE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CVE_BANCO")]
        [SugarColumn(ColumnName = "cve_banco")]
        public string CVE_BANCO {
            get {
                return this._CVE_BANCO;
            }
            set {
                this._CVE_BANCO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ESBANCOEXT")]
        [SugarColumn(ColumnName = "esbancoext")]
        public int? ESBANCOEXT {
            get {
                return this._ESBANCOEXT;
            }
            set {
                this._ESBANCOEXT = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("BANCODESC")]
        [SugarColumn(ColumnName = "bancodesc")]
        public string BANCODESC {
            get {
                return this._BANCODESC;
            }
            set {
                this._BANCODESC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RFCBANCO")]
        [SugarColumn(ColumnName = "rfcbanco")]
        public string RFCBANCO {
            get {
                return this._RFCBANCO;
            }
            set {
                this._RFCBANCO = value;
                this.OnPropertyChanged();
            }
        }
    }
}
