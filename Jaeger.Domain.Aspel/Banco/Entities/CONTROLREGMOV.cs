﻿using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Banco.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("controlregmov")]
    public partial class CONTROLREGMOV : BasePropertyChangeImplementation {
        private int _CUENTA;
        private int _MOVIMIENTOS;
        private int _PARTIDAS;
        private int _UUIDS;

        public CONTROLREGMOV() {


        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("CUENTA")]
        [SugarColumn(ColumnName = "cuenta", ColumnDescription = "")]
        public int CUENTA {
            get {
                return this._CUENTA;
            }
            set {
                this._CUENTA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("MOVIMIENTOS")]
        [SugarColumn(ColumnName = "movimientos", ColumnDescription = "")]
        public int MOVIMIENTOS {
            get {
                return this._MOVIMIENTOS;
            }
            set {
                this._MOVIMIENTOS = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("PARTIDAS")]
        [SugarColumn(ColumnName = "partidas", ColumnDescription = "")]
        public int PARTIDAS {
            get {
                return this._PARTIDAS;
            }
            set {
                this._PARTIDAS = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("UUIDS")]
        [SugarColumn(ColumnName = "uuids", ColumnDescription = "")]
        public int UUIDS {
            get {
                return this._UUIDS;
            }
            set {
                this._UUIDS = value;
                this.OnPropertyChanged();
            }
        }
    }
}
