﻿using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Banco.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("uuidtimbres")]
    public partial class UUIDTIMBRES : BasePropertyChangeImplementation {
        private int _NUM_REGU;
        private int _NUM_REGP;
        private string _UUIDTIMBRE;
        private double? _MONTO;
        private string _SERIE;
        private string _FOLIO;
        private string _RFCEMISOR;
        private string _RFCRECEPTOR;
        private int? _ORDEN;
        private string _FECHA;
        private int? _TIPOCOMPROBANTE;
        private string _MONEDADR;
        private double? _TIPOCAMBIODR;
        private string _METODODEPAGODR;
        private int? _NUMPARCIALIDAD;
        private double? _IMPPAGADO;
        private double? _IMPSALDOINSOLUTO;

        public UUIDTIMBRES() {


        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_REGU")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "num_regu", ColumnDescription = "")]
        public int NUM_REGU {
            get {
                return this._NUM_REGU;
            }
            set {
                this._NUM_REGU = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_REGP")]
        [SugarColumn(ColumnName = "num_regp", ColumnDescription = "")]
        public int NUM_REGP {
            get {
                return this._NUM_REGP;
            }
            set {
                this._NUM_REGP = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("UUIDTIMBRE")]
        [SugarColumn(ColumnName = "uuidtimbre", ColumnDescription = "")]
        public string UUIDTIMBRE {
            get {
                return this._UUIDTIMBRE;
            }
            set {
                this._UUIDTIMBRE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MONTO")]
        [SugarColumn(ColumnName = "monto", ColumnDescription = "")]
        public double? MONTO {
            get {
                return this._MONTO;
            }
            set {
                this._MONTO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("SERIE")]
        [SugarColumn(ColumnName = "serie", ColumnDescription = "")]
        public string SERIE {
            get {
                return this._SERIE;
            }
            set {
                this._SERIE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FOLIO")]
        [SugarColumn(ColumnName = "folio", ColumnDescription = "")]
        public string FOLIO {
            get {
                return this._FOLIO;
            }
            set {
                this._FOLIO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RFCEMISOR")]
        [SugarColumn(ColumnName = "rfcemisor", ColumnDescription = "")]
        public string RFCEMISOR {
            get {
                return this._RFCEMISOR;
            }
            set {
                this._RFCEMISOR = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RFCRECEPTOR")]
        [SugarColumn(ColumnName = "rfcreceptor", ColumnDescription = "")]
        public string RFCRECEPTOR {
            get {
                return this._RFCRECEPTOR;
            }
            set {
                this._RFCRECEPTOR = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ORDEN")]
        [SugarColumn(ColumnName = "orden", ColumnDescription = "")]
        public int? ORDEN {
            get {
                return this._ORDEN;
            }
            set {
                this._ORDEN = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FECHA")]
        [SugarColumn(ColumnName = "fecha", ColumnDescription = "")]
        public string FECHA {
            get {
                return this._FECHA;
            }
            set {
                this._FECHA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPOCOMPROBANTE")]
        [SugarColumn(ColumnName = "tipocomprobante", ColumnDescription = "")]
        public int? TIPOCOMPROBANTE {
            get {
                return this._TIPOCOMPROBANTE;
            }
            set {
                this._TIPOCOMPROBANTE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MONEDADR")]
        [SugarColumn(ColumnName = "monedadr", ColumnDescription = "")]
        public string MONEDADR {
            get {
                return this._MONEDADR;
            }
            set {
                this._MONEDADR = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPOCAMBIODR")]
        [SugarColumn(ColumnName = "tipocambiodr", ColumnDescription = "")]
        public double? TIPOCAMBIODR {
            get {
                return this._TIPOCAMBIODR;
            }
            set {
                this._TIPOCAMBIODR = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("METODODEPAGODR")]
        [SugarColumn(ColumnName = "metododepagodr", ColumnDescription = "")]
        public string METODODEPAGODR {
            get {
                return this._METODODEPAGODR;
            }
            set {
                this._METODODEPAGODR = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NUMPARCIALIDAD")]
        [SugarColumn(ColumnName = "numparcialidad", ColumnDescription = "")]
        public int? NUMPARCIALIDAD {
            get {
                return this._NUMPARCIALIDAD;
            }
            set {
                this._NUMPARCIALIDAD = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IMPPAGADO")]
        [SugarColumn(ColumnName = "imppagado", ColumnDescription = "")]
        public double? IMPPAGADO {
            get {
                return this._IMPPAGADO;
            }
            set {
                this._IMPPAGADO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IMPSALDOINSOLUTO")]
        [SugarColumn(ColumnName = "impsaldoinsoluto", ColumnDescription = "")]
        public double? IMPSALDOINSOLUTO {
            get {
                return this._IMPSALDOINSOLUTO;
            }
            set {
                this._IMPSALDOINSOLUTO = value;
                this.OnPropertyChanged();
            }
        }
    }
}
