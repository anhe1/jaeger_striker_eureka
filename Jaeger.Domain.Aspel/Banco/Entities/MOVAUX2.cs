﻿using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Banco.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("movaux2")]
    public partial class MOVAUX2 : BasePropertyChangeImplementation {
        private int _NUM_CTA;
        private int _NUM_MOV_AUX;
        private int _NUM_REG_MOV;

        public MOVAUX2() {


        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_CTA")]
        [SugarColumn(ColumnName = "num_cta", ColumnDescription = "")]
        public int NUM_CTA {
            get {
                return this._NUM_CTA;
            }
            set {
                this._NUM_CTA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_MOV_AUX")]
        [SugarColumn(ColumnName = "num_mov_aux", ColumnDescription = "")]
        public int NUM_MOV_AUX {
            get {
                return this._NUM_MOV_AUX;
            }
            set {
                this._NUM_MOV_AUX = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_REG_MOV")]
        [SugarColumn(ColumnName = "num_reg_mov", ColumnDescription = "")]
        public int NUM_REG_MOV {
            get {
                return this._NUM_REG_MOV;
            }
            set {
                this._NUM_REG_MOV = value;
                this.OnPropertyChanged();
            }
        }
    }
}
