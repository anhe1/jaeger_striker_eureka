﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Banco.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("movaux")]
    public partial class MOVAUX : BasePropertyChangeImplementation {
        private int _NUM_REG;
        private int? _NUM_CTA;
        private string _REFERENCIA1;
        private DateTime _FECHA;
        private string _REFERENCIA2;
        private double? _ABONO;
        private double? _CARGO;
        private string _RFC;
        private int? _REVISADO;

        public MOVAUX() {


        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_REG")]
        [SugarColumn(ColumnName = "num_reg", ColumnDescription = "")]
        public int NUM_REG {
            get {
                return this._NUM_REG;
            }
            set {
                this._NUM_REG = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NUM_CTA")]
        [SugarColumn(ColumnName = "num_cta", ColumnDescription = "")]
        public int? NUM_CTA {
            get {
                return this._NUM_CTA;
            }
            set {
                this._NUM_CTA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("REFERENCIA1")]
        [SugarColumn(ColumnName = "referencia1", ColumnDescription = "")]
        public string REFERENCIA1 {
            get {
                return this._REFERENCIA1;
            }
            set {
                this._REFERENCIA1 = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("REFERENCIA2")]
        [SugarColumn(ColumnName = "referencia2", ColumnDescription = "")]
        public string REFERENCIA2 {
            get {
                return this._REFERENCIA2;
            }
            set {
                this._REFERENCIA2 = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("FECHA")]
        [SugarColumn(ColumnName = "fecha", ColumnDescription = "")]
        public DateTime FECHA {
            get {
                return this._FECHA;
            }
            set {
                this._FECHA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ABONO")]
        [SugarColumn(ColumnName = "abono", ColumnDescription = "")]
        public double? ABONO {
            get {
                return this._ABONO;
            }
            set {
                this._ABONO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CARGO")]
        [SugarColumn(ColumnName = "cargo", ColumnDescription = "")]
        public double? CARGO {
            get {
                return this._CARGO;
            }
            set {
                this._CARGO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RFC")]
        [SugarColumn(ColumnName = "rfc", ColumnDescription = "")]
        public string RFC {
            get {
                return this._RFC;
            }
            set {
                this._RFC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("REVISADO")]
        [SugarColumn(ColumnName = "revisado", ColumnDescription = "")]
        public int? REVISADO {
            get {
                return this._REVISADO;
            }
            set {
                this._REVISADO = value;
                this.OnPropertyChanged();
            }
        }
    }
}
