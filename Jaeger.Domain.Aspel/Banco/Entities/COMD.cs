﻿using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Banco.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("comd")]
    public partial class COMD : BasePropertyChangeImplementation {
        private string _CVE_CONCEP;
        private string _TIPO;
        private string _CONCEP;
        private int? _CONCEPSAE;
        private string _CTA_CONTAB;
        private int? _ESTADO;
        private double? _IVA;
        private string _CLASIFICACION;
        private string _PLANTILLA;

        public COMD() {


        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("CVE_CONCEP")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "cve_concep", ColumnDescription = "")]
        public string CVE_CONCEP {
            get {
                return this._CVE_CONCEP;
            }
            set {
                this._CVE_CONCEP = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:'CARGO' Nullable_New:True
        /// </summary>
        [DataNames("TIPO")]
        [SugarColumn(ColumnName = "tipo", ColumnDescription = "")]
        public string TIPO {
            get {
                return this._TIPO;
            }
            set {
                this._TIPO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CONCEP")]
        [SugarColumn(ColumnName = "concep", ColumnDescription = "")]
        public string CONCEP {
            get {
                return this._CONCEP;
            }
            set {
                this._CONCEP = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CONCEPSAE")]
        [SugarColumn(ColumnName = "concepsae", ColumnDescription = "")]
        public int? CONCEPSAE {
            get {
                return this._CONCEPSAE;
            }
            set {
                this._CONCEPSAE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTA_CONTAB")]
        [SugarColumn(ColumnName = "cta_contab", ColumnDescription = "")]
        public string CTA_CONTAB {
            get {
                return this._CTA_CONTAB;
            }
            set {
                this._CTA_CONTAB = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:1 Nullable_New:True
        /// </summary>
        [DataNames("ESTADO")]
        [SugarColumn(ColumnName = "estado", ColumnDescription = "")]
        public int? ESTADO {
            get {
                return this._ESTADO;
            }
            set {
                this._ESTADO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("IVA")]
        [SugarColumn(ColumnName = "iva", ColumnDescription = "")]
        public double? IVA {
            get {
                return this._IVA;
            }
            set {
                this._IVA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CLASIFICACION")]
        [SugarColumn(ColumnName = "clasificacion", ColumnDescription = "")]
        public string CLASIFICACION {
            get {
                return this._CLASIFICACION;
            }
            set {
                this._CLASIFICACION = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PLANTILLA")]
        [SugarColumn(ColumnName = "plantilla", ColumnDescription = "")]
        public string PLANTILLA {
            get {
                return this._PLANTILLA;
            }
            set {
                this._PLANTILLA = value;
                this.OnPropertyChanged();
            }
        }
    }
}
