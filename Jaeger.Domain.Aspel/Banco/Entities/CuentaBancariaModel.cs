﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Banco.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("ctas")]
    public partial class CuentaBancariaModel : BasePropertyChangeImplementation {
        private int _NUM_REG;
        private string _NUM_CTA;
        private int? _STATUS;
        private string _BANCO;
        private string _SUCURSAL;
        private int? _MONEDA;
        private string _CTA_CONTAB;
        private int? _DIA_CORTE;
        private int? _SIG_CHEQUE;
        private DateTime _FECH_APER;
        private string _CHE_FTO;
        private string _BMP_BAN;
        private string _FUNCIONARIO;
        private string _TELEFONO;
        private string _CLABE;
        private string _PLAZA;
        private string _CVE_BANCO;
        private string _NOMBRE_CTA;
        private double? _SALDO_INI;
        private int? _ESBANCOEXT;
        private string _RFC;

        public CuentaBancariaModel() {

        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_REG")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "num_reg", ColumnDescription = "")]
        public int NUM_REG {
            get {
                return this._NUM_REG;
            }
            set {
                this._NUM_REG = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NUM_CTA")]
        [SugarColumn(ColumnName = "num_cta", ColumnDescription = "")]
        public string NUM_CTA {
            get {
                return this._NUM_CTA;
            }
            set {
                this._NUM_CTA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:1 Nullable_New:True
        /// </summary>
        [DataNames("STATUS")]
        [SugarColumn(ColumnName = "status", ColumnDescription = "")]
        public int? STATUS {
            get {
                return this._STATUS;
            }
            set {
                this._STATUS = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("BANCO")]
        [SugarColumn(ColumnName = "banco", ColumnDescription = "")]
        public string BANCO {
            get {
                return this._BANCO;
            }
            set {
                this._BANCO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("SUCURSAL")]
        [SugarColumn(ColumnName = "sucursal", ColumnDescription = "")]
        public string SUCURSAL {
            get {
                return this._SUCURSAL;
            }
            set {
                this._SUCURSAL = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:1 Nullable_New:True
        /// </summary>
        [DataNames("MONEDA")]
        [SugarColumn(ColumnName = "moneda", ColumnDescription = "")]
        public int? MONEDA {
            get {
                return this._MONEDA;
            }
            set {
                this._MONEDA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTA_CONTAB")]
        [SugarColumn(ColumnName = "cta_contab", ColumnDescription = "")]
        public string CTA_CONTAB {
            get {
                return this._CTA_CONTAB;
            }
            set {
                this._CTA_CONTAB = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:30 Nullable_New:True
        /// </summary>
        [DataNames("DIA_CORTE")]
        [SugarColumn(ColumnName = "dia_corte", ColumnDescription = "")]
        public int? DIA_CORTE {
            get {
                return this._DIA_CORTE;
            }
            set {
                this._DIA_CORTE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:1 Nullable_New:True
        /// </summary>
        [DataNames("SIG_CHEQUE")]
        [SugarColumn(ColumnName = "sig_cheque", ColumnDescription = "")]
        public int? SIG_CHEQUE {
            get {
                return this._SIG_CHEQUE;
            }
            set {
                this._SIG_CHEQUE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("FECH_APER")]
        [SugarColumn(ColumnName = "fech_aper", ColumnDescription = "")]
        public DateTime FECH_APER {
            get {
                return this._FECH_APER;
            }
            set {
                this._FECH_APER = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CHE_FTO")]
        [SugarColumn(ColumnName = "che_fto", ColumnDescription = "")]
        public string CHE_FTO {
            get {
                return this._CHE_FTO;
            }
            set {
                this._CHE_FTO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("BMP_BAN")]
        [SugarColumn(ColumnName = "bmp_ban", ColumnDescription = "")]
        public string BMP_BAN {
            get {
                return this._BMP_BAN;
            }
            set {
                this._BMP_BAN = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FUNCIONARIO")]
        [SugarColumn(ColumnName = "funcionario", ColumnDescription = "")]
        public string FUNCIONARIO {
            get {
                return this._FUNCIONARIO;
            }
            set {
                this._FUNCIONARIO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TELEFONO")]
        [SugarColumn(ColumnName = "telefono", ColumnDescription = "")]
        public string TELEFONO {
            get {
                return this._TELEFONO;
            }
            set {
                this._TELEFONO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CLABE")]
        [SugarColumn(ColumnName = "clabe", ColumnDescription = "")]
        public string CLABE {
            get {
                return this._CLABE;
            }
            set {
                this._CLABE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PLAZA")]
        [SugarColumn(ColumnName = "plaza", ColumnDescription = "")]
        public string PLAZA {
            get {
                return this._PLAZA;
            }
            set {
                this._PLAZA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CVE_BANCO")]
        [SugarColumn(ColumnName = "cve_banco", ColumnDescription = "")]
        public string CVE_BANCO {
            get {
                return this._CVE_BANCO;
            }
            set {
                this._CVE_BANCO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NOMBRE_CTA")]
        [SugarColumn(ColumnName = "nombre_cta", ColumnDescription = "")]
        public string NOMBRE_CTA {
            get {
                return this._NOMBRE_CTA;
            }
            set {
                this._NOMBRE_CTA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("SALDO_INI")]
        [SugarColumn(ColumnName = "saldo_ini", ColumnDescription = "")]
        public double? SALDO_INI {
            get {
                return this._SALDO_INI;
            }
            set {
                this._SALDO_INI = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("ESBANCOEXT")]
        [SugarColumn(ColumnName = "esbancoext", ColumnDescription = "")]
        public int? ESBANCOEXT {
            get {
                return this._ESBANCOEXT;
            }
            set {
                this._ESBANCOEXT = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:'0' Nullable_New:True
        /// </summary>
        [DataNames("RFC")]
        [SugarColumn(ColumnName = "rfc", ColumnDescription = "")]
        public string RFC {
            get {
                return this._RFC;
            }
            set {
                this._RFC = value;
                this.OnPropertyChanged();
            }
        }
    }
}
