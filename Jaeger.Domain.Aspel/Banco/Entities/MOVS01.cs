﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Banco.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("movs01")]
    public partial class MOVS01 : BasePropertyChangeImplementation {
        #region declaraciones
        private int _NUM_REG;
        private string _CVE_CONCEP;
        private int? _CON_PART;
        private int? _NUM_CHEQUE;
        private string referencia1;
        private string referencia2;
        private string _STATUS;
        private DateTime _FECHA;
        private DateTime _F_COBRO;
        private string _BAND_PRN;
        private string _BAND_CONT;
        private string _ACT_SAE;
        private string _NUM_POL;
        private int? _SAE_COI;
        private string _TIP_POL;
        private double _MONTO_TOT;
        private double? _MONTO_IVA_TOT;
        private double? _MONTO_EXT;
        private int? _MONEDA;
        private double? _T_CAMBIO;
        private int? _HORA;
        private string _CLPV;
        private int? _CTA_TRANSF;
        private DateTime? _FECHA_LIQ;
        private DateTime? _FECHA_POL;
        private int? _CVE_INST;
        private string _MONDIFSAE;
        private double? _TCAMBIOSAE;
        private string _RFC;
        private int? _CONC_SAE;
        private int? _TRANS_COI;
        private int? _INTSAENOI;
        private string _X_OBSER;
        private int _FACTOR;
        private int _FORMAPAGO;
        private string _ANOMBREDE;
        private string _ASOCIADO;
        private string _CTA_CONTAB_ASOC;
        private int? _REVISADO;
        private string _PRIORIDAD;
        private int? _DOC_ASOC;
        private int? _RESALTAR;
        private int? _ANTICIPO;
        private int? _IDTRANSF;
        private string _SUCURSAL;
        private string _SOLICIT;
        private int? _MULTI_CLPV;
        private string _CLABE;
        private string _CUENTA;
        private string _CVE_BANCO_ASOC;
        private int? _NUM_CONC_AUTO;
        private int? _COI_DEPTO;
        private int? _COI_CCOSTOS;
        private int? _COI_PROYECTO;
        private string _CUENTABAN_CLPV;
        private string _NOMBANCO;
        private string _RFCBANCOORD;
        private string _FORMAPAGOSAT;
        private string _NUMOPERACION;
        private string _ID_CFDI;
        private string _ESTADO_CFDI;
        private string _ARCHIVO_CFDI;
        private string _UUIDCFDI;
        #endregion

        public MOVS01() {

        }

        /// <summary>
        /// obtener o establecer indice de la tabla (Clave)
        /// </summary>
        [DataNames("NUM_REG")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "num_reg", ColumnDescription = "")]
        public int NUM_REG {
            get {
                return this._NUM_REG;
            }
            set {
                this._NUM_REG = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del concepto
        /// </summary>
        [DataNames("CVE_CONCEP")]
        [SugarColumn(ColumnName = "cve_concep", ColumnDescription = "clave del concepto")]
        public string CVE_CONCEP {
            get {
                return this._CVE_CONCEP;
            }
            set {
                this._CVE_CONCEP = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer contador de partidas asociadas
        /// </summary>
        [DataNames("CON_PART")]
        [SugarColumn(ColumnName = "con_part", ColumnDescription = "contador de partidas asociadas")]
        public int? CON_PART {
            get {
                return this._CON_PART;
            }
            set {
                this._CON_PART = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer el numero de cheque asociado a la operacion
        /// </summary>
        [DataNames("NUM_CHEQUE")]
        [SugarColumn(ColumnName = "num_cheque", ColumnDescription = "")]
        public int? NUM_CHEQUE {
            get {
                return this._NUM_CHEQUE;
            }
            set {
                this._NUM_CHEQUE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer referencia 1
        /// </summary>
        [DataNames("REF1")]
        [SugarColumn(ColumnName = "ref1", ColumnDescription = "referencia 1")]
        public string Referencia1 {
            get {
                return this.referencia1;
            }
            set {
                this.referencia1 = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer referencia 2
        /// </summary>
        [DataNames("REF2")]
        [SugarColumn(ColumnName = "ref2", ColumnDescription = "referencia 2")]
        public string Referencia2 {
            get {
                return this.referencia2;
            }
            set {
                this.referencia2 = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer Status
        /// </summary>
        [DataNames("STATUS")]
        [SugarColumn(ColumnName = "status", ColumnDescription = "")]
        public string STATUS {
            get {
                return this._STATUS;
            }
            set {
                this._STATUS = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("FECHA")]
        [SugarColumn(ColumnName = "fecha", ColumnDescription = "")]
        public DateTime FECHA {
            get {
                return this._FECHA;
            }
            set {
                this._FECHA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("F_COBRO")]
        [SugarColumn(ColumnName = "f_cobro", ColumnDescription = "")]
        public DateTime F_COBRO {
            get {
                return this._F_COBRO;
            }
            set {
                this._F_COBRO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:'N' Nullable_New:True
        /// </summary>
        [DataNames("BAND_PRN")]
        [SugarColumn(ColumnName = "band_prn", ColumnDescription = "")]
        public string BAND_PRN {
            get {
                return this._BAND_PRN;
            }
            set {
                this._BAND_PRN = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:'N' Nullable_New:True
        /// </summary>
        [DataNames("BAND_CONT")]
        [SugarColumn(ColumnName = "band_cont", ColumnDescription = "")]
        public string BAND_CONT {
            get {
                return this._BAND_CONT;
            }
            set {
                this._BAND_CONT = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:'N' Nullable_New:True
        /// </summary>
        [DataNames("ACT_SAE")]
        [SugarColumn(ColumnName = "act_sae", ColumnDescription = "")]
        public string ACT_SAE {
            get {
                return this._ACT_SAE;
            }
            set {
                this._ACT_SAE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NUM_POL")]
        [SugarColumn(ColumnName = "num_pol", ColumnDescription = "")]
        public string NUM_POL {
            get {
                return this._NUM_POL;
            }
            set {
                this._NUM_POL = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIP_POL")]
        [SugarColumn(ColumnName = "tip_pol", ColumnDescription = "")]
        public string TIP_POL {
            get {
                return this._TIP_POL;
            }
            set {
                this._TIP_POL = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("SAE_COI")]
        [SugarColumn(ColumnName = "sae_coi", ColumnDescription = "")]
        public int? SAE_COI {
            get {
                return this._SAE_COI;
            }
            set {
                this._SAE_COI = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("MONTO_TOT")]
        [SugarColumn(ColumnName = "monto_tot", ColumnDescription = "")]
        public double MONTO_TOT {
            get {
                return this._MONTO_TOT;
            }
            set {
                this._MONTO_TOT = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MONTO_IVA_TOT")]
        [SugarColumn(ColumnName = "monto_iva_tot", ColumnDescription = "")]
        public double? MONTO_IVA_TOT {
            get {
                return this._MONTO_IVA_TOT;
            }
            set {
                this._MONTO_IVA_TOT = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MONTO_EXT")]
        [SugarColumn(ColumnName = "monto_ext", ColumnDescription = "")]
        public double? MONTO_EXT {
            get {
                return this._MONTO_EXT;
            }
            set {
                this._MONTO_EXT = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:1 Nullable_New:True
        /// </summary>
        [DataNames("MONEDA")]
        [SugarColumn(ColumnName = "moneda", ColumnDescription = "")]
        public int? MONEDA {
            get {
                return this._MONEDA;
            }
            set {
                this._MONEDA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:1 Nullable_New:True
        /// </summary>
        [DataNames("T_CAMBIO")]
        [SugarColumn(ColumnName = "t_cambio", ColumnDescription = "")]
        public double? T_CAMBIO {
            get {
                return this._T_CAMBIO;
            }
            set {
                this._T_CAMBIO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("HORA")]
        [SugarColumn(ColumnName = "hora", ColumnDescription = "")]
        public int? HORA {
            get {
                return this._HORA;
            }
            set {
                this._HORA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la tabla de beneficiarios
        /// </summary>
        [DataNames("CLPV")]
        [SugarColumn(ColumnName = "clpv", ColumnDescription = "indice de la tabla de beneficiarios")]
        public string CLPV {
            get {
                return this._CLPV;
            }
            set {
                this._CLPV = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CTA_TRANSF")]
        [SugarColumn(ColumnName = "cta_transf", ColumnDescription = "")]
        public int? CTA_TRANSF {
            get {
                return this._CTA_TRANSF;
            }
            set {
                this._CTA_TRANSF = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FECHA_LIQ")]
        [SugarColumn(ColumnName = "fecha_liq", ColumnDescription = "")]
        public DateTime? FECHA_LIQ {
            get {
                return this._FECHA_LIQ;
            }
            set {
                this._FECHA_LIQ = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FECHA_POL")]
        [SugarColumn(ColumnName = "fecha_pol", ColumnDescription = "")]
        public DateTime? FECHA_POL {
            get {
                return this._FECHA_POL;
            }
            set {
                this._FECHA_POL = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CVE_INST")]
        [SugarColumn(ColumnName = "cve_inst", ColumnDescription = "")]
        public int? CVE_INST {
            get {
                return this._CVE_INST;
            }
            set {
                this._CVE_INST = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MONDIFSAE")]
        [SugarColumn(ColumnName = "mondifsae", ColumnDescription = "")]
        public string MONDIFSAE {
            get {
                return this._MONDIFSAE;
            }
            set {
                this._MONDIFSAE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:1 Nullable_New:True
        /// </summary>
        [DataNames("TCAMBIOSAE")]
        [SugarColumn(ColumnName = "tcambiosae", ColumnDescription = "")]
        public double? TCAMBIOSAE {
            get {
                return this._TCAMBIOSAE;
            }
            set {
                this._TCAMBIOSAE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RFC")]
        [SugarColumn(ColumnName = "rfc", ColumnDescription = "")]
        public string RFC {
            get {
                return this._RFC;
            }
            set {
                this._RFC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("CONC_SAE")]
        [SugarColumn(ColumnName = "conc_sae", ColumnDescription = "")]
        public int? CONC_SAE {
            get {
                return this._CONC_SAE;
            }
            set {
                this._CONC_SAE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("SOLICIT")]
        [SugarColumn(ColumnName = "solicit", ColumnDescription = "")]
        public string SOLICIT {
            get {
                return this._SOLICIT;
            }
            set {
                this._SOLICIT = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("TRANS_COI")]
        [SugarColumn(ColumnName = "trans_coi", ColumnDescription = "")]
        public int? TRANS_COI {
            get {
                return this._TRANS_COI;
            }
            set {
                this._TRANS_COI = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("INTSAENOI")]
        [SugarColumn(ColumnName = "intsaenoi", ColumnDescription = "")]
        public int? INTSAENOI {
            get {
                return this._INTSAENOI;
            }
            set {
                this._INTSAENOI = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("X_OBSER")]
        [SugarColumn(ColumnName = "x_obser", ColumnDescription = "")]
        public string X_OBSER {
            get {
                return this._X_OBSER;
            }
            set {
                this._X_OBSER = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("FACTOR")]
        [SugarColumn(ColumnName = "factor", ColumnDescription = "")]
        public int FACTOR {
            get {
                return this._FACTOR;
            }
            set {
                this._FACTOR = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("FORMAPAGO")]
        [SugarColumn(ColumnName = "formapago", ColumnDescription = "")]
        public int FormaPago {
            get {
                return this._FORMAPAGO;
            }
            set {
                this._FORMAPAGO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ANOMBREDE")]
        [SugarColumn(ColumnName = "anombrede", ColumnDescription = "")]
        public string ANOMBREDE {
            get {
                return this._ANOMBREDE;
            }
            set {
                this._ANOMBREDE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("ASOCIADO")]
        [SugarColumn(ColumnName = "asociado", ColumnDescription = "")]
        public string ASOCIADO {
            get {
                return this._ASOCIADO;
            }
            set {
                this._ASOCIADO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTA_CONTAB_ASOC")]
        [SugarColumn(ColumnName = "cta_contab_asoc", ColumnDescription = "")]
        public string CTA_CONTAB_ASOC {
            get {
                return this._CTA_CONTAB_ASOC;
            }
            set {
                this._CTA_CONTAB_ASOC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("REVISADO")]
        [SugarColumn(ColumnName = "revisado", ColumnDescription = "")]
        public int? REVISADO {
            get {
                return this._REVISADO;
            }
            set {
                this._REVISADO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:'NORMAL' Nullable_New:True
        /// </summary>
        [DataNames("PRIORIDAD")]
        [SugarColumn(ColumnName = "prioridad", ColumnDescription = "")]
        public string PRIORIDAD {
            get {
                return this._PRIORIDAD;
            }
            set {
                this._PRIORIDAD = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DOC_ASOC")]
        [SugarColumn(ColumnName = "doc_asoc", ColumnDescription = "")]
        public int? DOC_ASOC {
            get {
                return this._DOC_ASOC;
            }
            set {
                this._DOC_ASOC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RESALTAR")]
        [SugarColumn(ColumnName = "resaltar", ColumnDescription = "")]
        public int? RESALTAR {
            get {
                return this._RESALTAR;
            }
            set {
                this._RESALTAR = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ANTICIPO")]
        [SugarColumn(ColumnName = "anticipo", ColumnDescription = "")]
        public int? ANTICIPO {
            get {
                return this._ANTICIPO;
            }
            set {
                this._ANTICIPO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IDTRANSF")]
        [SugarColumn(ColumnName = "idtransf", ColumnDescription = "")]
        public int? IDTRANSF {
            get {
                return this._IDTRANSF;
            }
            set {
                this._IDTRANSF = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("SUCURSAL")]
        [SugarColumn(ColumnName = "sucursal", ColumnDescription = "")]
        public string SUCURSAL {
            get {
                return this._SUCURSAL;
            }
            set {
                this._SUCURSAL = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CUENTA")]
        [SugarColumn(ColumnName = "cuenta", ColumnDescription = "")]
        public string CUENTA {
            get {
                return this._CUENTA;
            }
            set {
                this._CUENTA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CLABE")]
        [SugarColumn(ColumnName = "clabe", ColumnDescription = "")]
        public string CLABE {
            get {
                return this._CLABE;
            }
            set {
                this._CLABE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("MULTI_CLPV")]
        [SugarColumn(ColumnName = "multi_clpv", ColumnDescription = "")]
        public int? MULTI_CLPV {
            get {
                return this._MULTI_CLPV;
            }
            set {
                this._MULTI_CLPV = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CVE_BANCO_ASOC")]
        [SugarColumn(ColumnName = "cve_banco_asoc", ColumnDescription = "")]
        public string CVE_BANCO_ASOC {
            get {
                return this._CVE_BANCO_ASOC;
            }
            set {
                this._CVE_BANCO_ASOC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NUM_CONC_AUTO")]
        [SugarColumn(ColumnName = "num_conc_auto", ColumnDescription = "")]
        public int? NUM_CONC_AUTO {
            get {
                return this._NUM_CONC_AUTO;
            }
            set {
                this._NUM_CONC_AUTO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COI_DEPTO")]
        [SugarColumn(ColumnName = "coi_depto", ColumnDescription = "")]
        public int? COI_DEPTO {
            get {
                return this._COI_DEPTO;
            }
            set {
                this._COI_DEPTO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COI_CCOSTOS")]
        [SugarColumn(ColumnName = "coi_ccostos", ColumnDescription = "")]
        public int? COI_CCOSTOS {
            get {
                return this._COI_CCOSTOS;
            }
            set {
                this._COI_CCOSTOS = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COI_PROYECTO")]
        [SugarColumn(ColumnName = "coi_proyecto", ColumnDescription = "")]
        public int? COI_PROYECTO {
            get {
                return this._COI_PROYECTO;
            }
            set {
                this._COI_PROYECTO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CUENTABAN_CLPV")]
        [SugarColumn(ColumnName = "cuentaban_clpv", ColumnDescription = "")]
        public string CUENTABAN_CLPV {
            get {
                return this._CUENTABAN_CLPV;
            }
            set {
                this._CUENTABAN_CLPV = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NOMBANCO")]
        [SugarColumn(ColumnName = "nombanco", ColumnDescription = "")]
        public string NOMBANCO {
            get {
                return this._NOMBANCO;
            }
            set {
                this._NOMBANCO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RFCBANCOORD")]
        [SugarColumn(ColumnName = "rfcbancoord", ColumnDescription = "")]
        public string RFCBANCOORD {
            get {
                return this._RFCBANCOORD;
            }
            set {
                this._RFCBANCOORD = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FORMAPAGOSAT")]
        [SugarColumn(ColumnName = "formapagosat", ColumnDescription = "")]
        public string FORMAPAGOSAT {
            get {
                return this._FORMAPAGOSAT;
            }
            set {
                this._FORMAPAGOSAT = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NUMOPERACION")]
        [SugarColumn(ColumnName = "numoperacion", ColumnDescription = "")]
        public string NUMOPERACION {
            get {
                return this._NUMOPERACION;
            }
            set {
                this._NUMOPERACION = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ID_CFDI")]
        [SugarColumn(ColumnName = "id_cfdi", ColumnDescription = "")]
        public string ID_CFDI {
            get {
                return this._ID_CFDI;
            }
            set {
                this._ID_CFDI = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ESTADO_CFDI")]
        [SugarColumn(ColumnName = "estado_cfdi", ColumnDescription = "")]
        public string ESTADO_CFDI {
            get {
                return this._ESTADO_CFDI;
            }
            set {
                this._ESTADO_CFDI = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ARCHIVO_CFDI")]
        [SugarColumn(ColumnName = "archivo_cfdi", ColumnDescription = "")]
        public string ARCHIVO_CFDI {
            get {
                return this._ARCHIVO_CFDI;
            }
            set {
                this._ARCHIVO_CFDI = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("UUIDCFDI")]
        [SugarColumn(ColumnName = "uuidcfdi", ColumnDescription = "")]
        public string UUIDCFDI {
            get {
                return this._UUIDCFDI;
            }
            set {
                this._UUIDCFDI = value;
                this.OnPropertyChanged();
            }
        }
    }
}
