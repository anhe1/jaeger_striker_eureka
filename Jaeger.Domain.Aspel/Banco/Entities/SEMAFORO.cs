﻿using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Banco.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("semaforo")]
    public partial class SEMAFORO : BasePropertyChangeImplementation {
        private int _HISTANCE;

        public SEMAFORO() {


        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("HISTANCE")]
        [SugarColumn(ColumnName = "histance", ColumnDescription = "")]
        public int HISTANCE {
            get {
                return this._HISTANCE;
            }
            set {
                this._HISTANCE = value;
                this.OnPropertyChanged();
            }
        }
    }
}
