﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Banco.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("histor")]
    public partial class HISTOR : BasePropertyChangeImplementation {
        private int _MONEDA;
        private DateTime _FECHA;
        private double _TIPOCAMBIO;

        public HISTOR() {


        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("MONEDA")]
        [SugarColumn(ColumnName = "moneda", ColumnDescription = "")]
        public int MONEDA {
            get {
                return this._MONEDA;
            }
            set {
                this._MONEDA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("FECHA")]
        [SugarColumn(ColumnName = "fecha", ColumnDescription = "")]
        public DateTime FECHA {
            get {
                return this._FECHA;
            }
            set {
                this._FECHA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("TIPOCAMBIO")]
        [SugarColumn(ColumnName = "tipocambio", ColumnDescription = "")]
        public double TIPOCAMBIO {
            get {
                return this._TIPOCAMBIO;
            }
            set {
                this._TIPOCAMBIO = value;
                this.OnPropertyChanged();
            }
        }
    }
}
