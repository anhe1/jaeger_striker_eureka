﻿using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Banco.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("formpago")]
    public partial class FormaPagoModel : BasePropertyChangeImplementation {
        private int _NUM_REG;
        private string _DESCRIPCION;
        private int? _TIPO;
        private string _FORMAPAGOSAT;

        public FormaPagoModel() {


        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_REG")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "num_reg", ColumnDescription = "")]
        public int NUM_REG {
            get {
                return this._NUM_REG;
            }
            set {
                this._NUM_REG = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DESCRIPCION")]
        [SugarColumn(ColumnName = "descripcion", ColumnDescription = "")]
        public string DESCRIPCION {
            get {
                return this._DESCRIPCION;
            }
            set {
                this._DESCRIPCION = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:-1 Nullable_New:True
        /// </summary>
        [DataNames("TIPO")]
        [SugarColumn(ColumnName = "tipo", ColumnDescription = "")]
        public int? TIPO {
            get {
                return this._TIPO;
            }
            set {
                this._TIPO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FORMAPAGOSAT")]
        [SugarColumn(ColumnName = "formapagosat", ColumnDescription = "")]
        public string FORMAPAGOSAT {
            get {
                return this._FORMAPAGOSAT;
            }
            set {
                this._FORMAPAGOSAT = value;
                this.OnPropertyChanged();
            }
        }
    }
}
