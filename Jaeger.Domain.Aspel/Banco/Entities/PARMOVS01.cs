﻿using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Banco.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("parmovs01")]
    public partial class PARMOVS01 : BasePropertyChangeImplementation {
        private int _NUM_REGP;
        private int _NUM_MOV;
        private string _CVE_CONCEP;
        private string _REFERENCIA;
        private double? _MONTO_IVA;
        private string _FACT;
        private double _MONTO_DOC;
        private double? _MONTO_EXT;
        private int _ORDEN;
        private string _X_OBSER;
        private int? _NUMCARGO;
        private int? _NUMCPTOPADRE;
        private double? _TIPOCAMBIOSAE;
        private double? _TIPOCAMBIOBANCO;
        private string _DOCTO;
        private int? _NO_PARTIDASAE;
        private int? _MONEDADOC;
        private string _PART_CLPV;
        private string _PART_CLPV_RFC;
        private string _PART_ANOMBREDE;
        private double? _IVA_MC;
        private string _PART_CTA_CONTAB_ASOC;
        private int? _PART_CONCEPSAE;
        private int? _PART_ANTICIPO;
        private string _PART_CTA_BANCO_ASOC;
        private string _PART_CVE_BANCO_ASOC;
        private double? _IMPSALDOANT;

        public PARMOVS01() {

        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_REGP")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "num_regp", ColumnDescription = "")]
        public int NUM_REGP {
            get {
                return this._NUM_REGP;
            }
            set {
                this._NUM_REGP = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_MOV")]
        [SugarColumn(ColumnName = "num_mov", ColumnDescription = "")]
        public int NUM_MOV {
            get {
                return this._NUM_MOV;
            }
            set {
                this._NUM_MOV = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CVE_CONCEP")]
        [SugarColumn(ColumnName = "cve_concep", ColumnDescription = "")]
        public string CVE_CONCEP {
            get {
                return this._CVE_CONCEP;
            }
            set {
                this._CVE_CONCEP = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("REFERENCIA")]
        [SugarColumn(ColumnName = "referencia", ColumnDescription = "")]
        public string REFERENCIA {
            get {
                return this._REFERENCIA;
            }
            set {
                this._REFERENCIA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MONTO_IVA")]
        [SugarColumn(ColumnName = "monto_iva", ColumnDescription = "")]
        public double? MONTO_IVA {
            get {
                return this._MONTO_IVA;
            }
            set {
                this._MONTO_IVA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("FACT")]
        [SugarColumn(ColumnName = "fact", ColumnDescription = "")]
        public string FACT {
            get {
                return this._FACT;
            }
            set {
                this._FACT = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("MONTO_DOC")]
        [SugarColumn(ColumnName = "monto_doc", ColumnDescription = "")]
        public double MONTO_DOC {
            get {
                return this._MONTO_DOC;
            }
            set {
                this._MONTO_DOC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("MONTO_EXT")]
        [SugarColumn(ColumnName = "monto_ext", ColumnDescription = "")]
        public double? MONTO_EXT {
            get {
                return this._MONTO_EXT;
            }
            set {
                this._MONTO_EXT = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("ORDEN")]
        [SugarColumn(ColumnName = "orden", ColumnDescription = "")]
        public int ORDEN {
            get {
                return this._ORDEN;
            }
            set {
                this._ORDEN = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("X_OBSER")]
        [SugarColumn(ColumnName = "x_obser", ColumnDescription = "")]
        public string X_OBSER {
            get {
                return this._X_OBSER;
            }
            set {
                this._X_OBSER = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("NUMCARGO")]
        [SugarColumn(ColumnName = "numcargo", ColumnDescription = "")]
        public int? NUMCARGO {
            get {
                return this._NUMCARGO;
            }
            set {
                this._NUMCARGO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("NUMCPTOPADRE")]
        [SugarColumn(ColumnName = "numcptopadre", ColumnDescription = "")]
        public int? NUMCPTOPADRE {
            get {
                return this._NUMCPTOPADRE;
            }
            set {
                this._NUMCPTOPADRE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("TIPOCAMBIOSAE")]
        [SugarColumn(ColumnName = "tipocambiosae", ColumnDescription = "")]
        public double? TIPOCAMBIOSAE {
            get {
                return this._TIPOCAMBIOSAE;
            }
            set {
                this._TIPOCAMBIOSAE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("TIPOCAMBIOBANCO")]
        [SugarColumn(ColumnName = "tipocambiobanco", ColumnDescription = "")]
        public double? TIPOCAMBIOBANCO {
            get {
                return this._TIPOCAMBIOBANCO;
            }
            set {
                this._TIPOCAMBIOBANCO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DOCTO")]
        [SugarColumn(ColumnName = "docto", ColumnDescription = "")]
        public string DOCTO {
            get {
                return this._DOCTO;
            }
            set {
                this._DOCTO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("NO_PARTIDASAE")]
        [SugarColumn(ColumnName = "no_partidasae", ColumnDescription = "")]
        public int? NO_PARTIDASAE {
            get {
                return this._NO_PARTIDASAE;
            }
            set {
                this._NO_PARTIDASAE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:1 Nullable_New:True
        /// </summary>
        [DataNames("MONEDADOC")]
        [SugarColumn(ColumnName = "monedadoc", ColumnDescription = "")]
        public int? MONEDADOC {
            get {
                return this._MONEDADOC;
            }
            set {
                this._MONEDADOC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PART_CLPV")]
        [SugarColumn(ColumnName = "part_clpv", ColumnDescription = "")]
        public string PART_CLPV {
            get {
                return this._PART_CLPV;
            }
            set {
                this._PART_CLPV = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PART_CLPV_RFC")]
        [SugarColumn(ColumnName = "part_clpv_rfc", ColumnDescription = "")]
        public string PART_CLPV_RFC {
            get {
                return this._PART_CLPV_RFC;
            }
            set {
                this._PART_CLPV_RFC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PART_ANOMBREDE")]
        [SugarColumn(ColumnName = "part_anombrede", ColumnDescription = "")]
        public string PART_ANOMBREDE {
            get {
                return this._PART_ANOMBREDE;
            }
            set {
                this._PART_ANOMBREDE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("IVA_MC")]
        [SugarColumn(ColumnName = "iva_mc", ColumnDescription = "")]
        public double? IVA_MC {
            get {
                return this._IVA_MC;
            }
            set {
                this._IVA_MC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PART_CTA_CONTAB_ASOC")]
        [SugarColumn(ColumnName = "part_cta_contab_asoc", ColumnDescription = "")]
        public string PART_CTA_CONTAB_ASOC {
            get {
                return this._PART_CTA_CONTAB_ASOC;
            }
            set {
                this._PART_CTA_CONTAB_ASOC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PART_CONCEPSAE")]
        [SugarColumn(ColumnName = "part_concepsae", ColumnDescription = "")]
        public int? PART_CONCEPSAE {
            get {
                return this._PART_CONCEPSAE;
            }
            set {
                this._PART_CONCEPSAE = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("PART_ANTICIPO")]
        [SugarColumn(ColumnName = "part_anticipo", ColumnDescription = "")]
        public int? PART_ANTICIPO {
            get {
                return this._PART_ANTICIPO;
            }
            set {
                this._PART_ANTICIPO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PART_CTA_BANCO_ASOC")]
        [SugarColumn(ColumnName = "part_cta_banco_asoc", ColumnDescription = "")]
        public string PART_CTA_BANCO_ASOC {
            get {
                return this._PART_CTA_BANCO_ASOC;
            }
            set {
                this._PART_CTA_BANCO_ASOC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PART_CVE_BANCO_ASOC")]
        [SugarColumn(ColumnName = "part_cve_banco_asoc", ColumnDescription = "")]
        public string PART_CVE_BANCO_ASOC {
            get {
                return this._PART_CVE_BANCO_ASOC;
            }
            set {
                this._PART_CVE_BANCO_ASOC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IMPSALDOANT")]
        [SugarColumn(ColumnName = "impsaldoant", ColumnDescription = "")]
        public double? IMPSALDOANT {
            get {
                return this._IMPSALDOANT;
            }
            set {
                this._IMPSALDOANT = value;
                this.OnPropertyChanged();
            }
        }
    }
}
