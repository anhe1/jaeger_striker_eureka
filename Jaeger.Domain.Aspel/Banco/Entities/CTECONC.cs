﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Banco.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("cteconc")]
    public partial class CTECONC : BasePropertyChangeImplementation {
        private int _NUM_REG;
        private int _NUM_CTA;
        private DateTime _FECHA_REG;
        private DateTime _FECHA_INICIO;
        private DateTime _FECHA_FIN;
        private double _TOT_CARGOS;
        private double _TOT_ABONOS;
        private double _SALDO;

        public CTECONC() {


        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_REG")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "num_reg", ColumnDescription = "")]
        public int NUM_REG {
            get {
                return this._NUM_REG;
            }
            set {
                this._NUM_REG = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_CTA")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "num_cta", ColumnDescription = "")]
        public int NUM_CTA {
            get {
                return this._NUM_CTA;
            }
            set {
                this._NUM_CTA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("FECHA_REG")]
        [SugarColumn(ColumnName = "fecha_reg", ColumnDescription = "")]
        public DateTime FECHA_REG {
            get {
                return this._FECHA_REG;
            }
            set {
                this._FECHA_REG = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("FECHA_INICIO")]
        [SugarColumn(ColumnName = "fecha_inicio", ColumnDescription = "")]
        public DateTime FECHA_INICIO {
            get {
                return this._FECHA_INICIO;
            }
            set {
                this._FECHA_INICIO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("FECHA_FIN")]
        [SugarColumn(ColumnName = "fecha_fin", ColumnDescription = "")]
        public DateTime FECHA_FIN {
            get {
                return this._FECHA_FIN;
            }
            set {
                this._FECHA_FIN = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("TOT_CARGOS")]
        [SugarColumn(ColumnName = "tot_cargos", ColumnDescription = "")]
        public double TOT_CARGOS {
            get {
                return this._TOT_CARGOS;
            }
            set {
                this._TOT_CARGOS = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("TOT_ABONOS")]
        [SugarColumn(ColumnName = "tot_abonos", ColumnDescription = "")]
        public double TOT_ABONOS {
            get {
                return this._TOT_ABONOS;
            }
            set {
                this._TOT_ABONOS = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("SALDO")]
        [SugarColumn(ColumnName = "saldo", ColumnDescription = "")]
        public double SALDO {
            get {
                return this._SALDO;
            }
            set {
                this._SALDO = value;
                this.OnPropertyChanged();
            }
        }
    }
}
