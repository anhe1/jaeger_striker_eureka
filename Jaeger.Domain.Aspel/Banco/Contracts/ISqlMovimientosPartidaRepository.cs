﻿using Jaeger.Domain.Aspel.Banco.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Aspel.Banco.Contracts {
    public interface ISqlMovimientosPartidaRepository : IGenericRepository<PARMOVS01> {
    }
}
