﻿using System.Collections.Generic;
using Jaeger.Domain.Aspel.Coi80.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Aspel.Coi80.Contracts {
    public interface ISqlPolizas20Repository : IGenericRepository<PolizaModel> {
        PolizaDetailModel GetById(int numpoliza, string tipo, int periodo, int ejercicio);
        IEnumerable<PolizaDetailModel> GetList(int periodo, int ejercicio);
    }
}
