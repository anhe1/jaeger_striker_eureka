﻿using Jaeger.Domain.Aspel.Coi80.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Aspel.Coi80.Contracts {
    public interface ISqlCtarubRepository : IGenericRepository<CTARUB> {
    }
}
