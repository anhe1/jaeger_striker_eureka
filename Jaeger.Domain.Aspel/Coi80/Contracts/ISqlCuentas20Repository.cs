using System.Linq;
using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Aspel.Coi80.Entities;

namespace Jaeger.Domain.Aspel.Coi80.Contracts {

    public interface ISqlCuentas20Repository : IGenericRepository<CuentaContableModel> {
        /// <summary>
        /// obtener listado de cuentas contables por ejercicio
        /// </summary>
        IEnumerable<CuentaContableModel> GetList(int ejercicio);
    }
}