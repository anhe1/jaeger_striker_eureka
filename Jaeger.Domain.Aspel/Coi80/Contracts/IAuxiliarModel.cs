﻿using System;

namespace Jaeger.Domain.Aspel.Coi80.Contracts {
    public interface IAuxiliarModel {
        int? CCOSTOS { get; set; }
        int? CGRUPOS { get; set; }
        string CONCEP_PO { get; set; }
        int? CONTRAPAR { get; set; }
        string DEBE_HABER { get; set; }
        int EJERCICIO { get; set; }
        DateTime? FECHA_POL { get; set; }
        int? IDINFADIPAR { get; set; }
        int? IDUUID { get; set; }
        double? MONTOMOV { get; set; }
        string NUM_CTA { get; set; }
        double NUM_PART { get; set; }
        string NUM_POLIZ { get; set; }
        int? NUMDEPTO { get; set; }
        int? ORDEN { get; set; }
        int PERIODO { get; set; }
        double? TIPCAMBIO { get; set; }
        string TIPO_POLI { get; set; }
    }
}