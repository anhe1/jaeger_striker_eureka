﻿using System.Collections.Generic;
using Jaeger.Domain.Aspel.Coi80.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Aspel.Coi80.Contracts {
    public interface ISqlAuxiliar20Repository : IGenericRepository<AuxiliarModel> {
        /// <summary>
        /// obtener listado de auxiliares de polizas
        /// </summary>
        /// <param name="numpoliza">numero de poliza, -1 para obtener todas</param>
        /// <param name="tipo">tipo de poliza, "*" para obtener todas</param>
        /// <param name="periodo">perido</param>
        /// <param name="ejercicio">ejercicio</param>
        IEnumerable<AuxiliarDetailModel> GetList(int numpoliza, string tipo, int periodo, int ejercicio);
    }
}
