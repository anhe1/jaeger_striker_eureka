﻿namespace Jaeger.Domain.Aspel.Coi80.Entities {
    public class AuxiliarDetailModel : AuxiliarModel {
        public AuxiliarDetailModel() {
        }

        public string NumeroCuenta {
            get {
                return long.Parse(this.NUM_CTA.Substring(1, this.NUM_CTA.Length - 11)).ToString("0000-000-000");
            }
            set {
                this.NUM_CTA = value.ToString();
            }
        }

        public double? Debe {
            get {
                if (this.DEBE_HABER == "D")
                    return this.MONTOMOV;
                return 0;
            }
            set {
                this.MONTOMOV = value;
                this.OnPropertyChanged();
            }
        }

        public double? Haber {
            get {
                if (this.DEBE_HABER == "H")
                    return this.MONTOMOV;
                return 0;
            }
            set {
                this.MONTOMOV = value;
                this.OnPropertyChanged();
            }
        }
    }
}
