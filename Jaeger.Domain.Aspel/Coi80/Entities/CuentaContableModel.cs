﻿using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Coi80.Entities {
    ///<summary>
    /// Cuenta contable
    ///</summary>
    [SugarTable("cuentas20")]
    public partial class CuentaContableModel : Domain.Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private string _NUM_CTA;
        private string _STATUS;
        private string _TIPO;
        private string _NOMBRE;
        private string _DEPTSINO;
        private int? _BANDMULTI;
        private string _BANDAJT;
        private string _CTA_PAPA;
        private string _CTA_RAIZ;
        private int? _NIVEL;
        private string _CTA_COMP;
        private int? _NATURALEZA;
        private string _RFC;
        private string _CODAGRUP;
        private int? _CAPTURACHEQUE;
        private int? _CAPTURAUUID;
        private int? _BANCO;
        private string _CTABANCARIA;
        private string _CAPCHEQTIPOMOV;
        private int? _NOINCLUIRXML;
        private string _IDFISCAL;
        private int? _ESFLUJODEEFECTIVO;
        private string _BANCOEXTRANJERO;
        private string _RFCFLUJO;
        #endregion

        public CuentaContableModel() {


        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("NUM_CTA")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "num_cta", ColumnDescription = "")]
        public string NUM_CTA {
            get {
                return this._NUM_CTA;
            }
            set {
                this._NUM_CTA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("STATUS")]
        [SugarColumn(ColumnName = "status", ColumnDescription = "")]
        public string STATUS {
            get {
                return this._STATUS;
            }
            set {
                this._STATUS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("TIPO")]
        [SugarColumn(ColumnName = "tipo", ColumnDescription = "")]
        public string TIPO {
            get {
                return this._TIPO;
            }
            set {
                this._TIPO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NOMBRE")]
        [SugarColumn(ColumnName = "nombre", ColumnDescription = "")]
        public string NOMBRE {
            get {
                return this._NOMBRE;
            }
            set {
                this._NOMBRE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DEPTSINO")]
        [SugarColumn(ColumnName = "deptsino", ColumnDescription = "")]
        public string DEPTSINO {
            get {
                return this._DEPTSINO;
            }
            set {
                this._DEPTSINO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("BANDMULTI")]
        [SugarColumn(ColumnName = "bandmulti", ColumnDescription = "")]
        public int? BANDMULTI {
            get {
                return this._BANDMULTI;
            }
            set {
                this._BANDMULTI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("BANDAJT")]
        [SugarColumn(ColumnName = "bandajt", ColumnDescription = "")]
        public string BANDAJT {
            get {
                return this._BANDAJT;
            }
            set {
                this._BANDAJT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTA_PAPA")]
        [SugarColumn(ColumnName = "cta_papa", ColumnDescription = "")]
        public string CTA_PAPA {
            get {
                return this._CTA_PAPA;
            }
            set {
                this._CTA_PAPA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTA_RAIZ")]
        [SugarColumn(ColumnName = "cta_raiz", ColumnDescription = "")]
        public string CTA_RAIZ {
            get {
                return this._CTA_RAIZ;
            }
            set {
                this._CTA_RAIZ = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NIVEL")]
        [SugarColumn(ColumnName = "nivel", ColumnDescription = "")]
        public int? NIVEL {
            get {
                return this._NIVEL;
            }
            set {
                this._NIVEL = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTA_COMP")]
        [SugarColumn(ColumnName = "cta_comp", ColumnDescription = "")]
        public string CTA_COMP {
            get {
                return this._CTA_COMP;
            }
            set {
                this._CTA_COMP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("NATURALEZA")]
        [SugarColumn(ColumnName = "naturaleza", ColumnDescription = "")]
        public int? NATURALEZA {
            get {
                return this._NATURALEZA;
            }
            set {
                this._NATURALEZA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RFC")]
        [SugarColumn(ColumnName = "rfc", ColumnDescription = "")]
        public string RFC {
            get {
                return this._RFC;
            }
            set {
                this._RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CODAGRUP")]
        [SugarColumn(ColumnName = "codagrup", ColumnDescription = "")]
        public string CODAGRUP {
            get {
                return this._CODAGRUP;
            }
            set {
                this._CODAGRUP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CAPTURACHEQUE")]
        [SugarColumn(ColumnName = "capturacheque", ColumnDescription = "")]
        public int? CAPTURACHEQUE {
            get {
                return this._CAPTURACHEQUE;
            }
            set {
                this._CAPTURACHEQUE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CAPTURAUUID")]
        [SugarColumn(ColumnName = "capturauuid", ColumnDescription = "")]
        public int? CAPTURAUUID {
            get {
                return this._CAPTURAUUID;
            }
            set {
                this._CAPTURAUUID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("BANCO")]
        [SugarColumn(ColumnName = "banco", ColumnDescription = "")]
        public int? BANCO {
            get {
                return this._BANCO;
            }
            set {
                this._BANCO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CTABANCARIA")]
        [SugarColumn(ColumnName = "ctabancaria", ColumnDescription = "")]
        public string CTABANCARIA {
            get {
                return this._CTABANCARIA;
            }
            set {
                this._CTABANCARIA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("CAPCHEQTIPOMOV")]
        [SugarColumn(ColumnName = "capcheqtipomov", ColumnDescription = "")]
        public string CAPCHEQTIPOMOV {
            get {
                return this._CAPCHEQTIPOMOV;
            }
            set {
                this._CAPCHEQTIPOMOV = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("NOINCLUIRXML")]
        [SugarColumn(ColumnName = "noincluirxml", ColumnDescription = "")]
        public int? NOINCLUIRXML {
            get {
                return this._NOINCLUIRXML;
            }
            set {
                this._NOINCLUIRXML = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("IDFISCAL")]
        [SugarColumn(ColumnName = "idfiscal", ColumnDescription = "")]
        public string IDFISCAL {
            get {
                return this._IDFISCAL;
            }
            set {
                this._IDFISCAL = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("ESFLUJODEEFECTIVO")]
        [SugarColumn(ColumnName = "esflujodeefectivo", ColumnDescription = "")]
        public int? ESFLUJODEEFECTIVO {
            get {
                return this._ESFLUJODEEFECTIVO;
            }
            set {
                this._ESFLUJODEEFECTIVO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("BANCOEXTRANJERO")]
        [SugarColumn(ColumnName = "bancoextranjero", ColumnDescription = "")]
        public string BANCOEXTRANJERO {
            get {
                return this._BANCOEXTRANJERO;
            }
            set {
                this._BANCOEXTRANJERO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("RFCFLUJO")]
        [SugarColumn(ColumnName = "rfcflujo", ColumnDescription = "")]
        public string RFCFLUJO {
            get {
                return this._RFCFLUJO;
            }
            set {
                this._RFCFLUJO = value;
                this.OnPropertyChanged();
            }
        }
    }
}
