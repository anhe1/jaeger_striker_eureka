﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Coi80.Entities {
    ///<summary>
    ///UUIDTIMBRES
    ///</summary>
    [SugarTable("uuidtimbres")]
    public partial class UUIDTIMBRES : Domain.Base.Abstractions.BasePropertyChangeImplementation {
        public UUIDTIMBRES() {
        }

        private int _NUMREG;
        private string _UUIDTIMBRE;
        private double? _MONTO;
        private string _SERIE;
        private string _FOLIO;
        private string _RFCEMISOR;
        private string _RFCRECEPTOR;
        private int? _ORDEN;
        private DateTime? _FECHA;
        private int? _TIPOCOMPROBANTE;
        private double? _TIPOCAMBIO;
        private string _VERSIONCFDI;
        private string _MONEDA;

        /// <summary>
        /// obtener o establecer Numero de registro (NUMREG)
        /// </summary>
        [DataNames("NUMREG")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "numreg", ColumnDescription = "Numero de registro", IsNullable = false)]
        public int NUMREG {
            get {
                return this._NUMREG;
            }
            set {
                this._NUMREG = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer UUID del comprobante (UUIDTIMBRE)
        /// Longitud: 36
        /// </summary>
        [DataNames("UUIDTIMBRE")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "uuidtimbre", ColumnDescription = "UUID del comprobante", IsNullable = false, Length = 36)]
        public string UUIDTIMBRE {
            get {
                return this._UUIDTIMBRE;
            }
            set {
                this._UUIDTIMBRE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer MONTO
        /// </summary>
        [DataNames("MONTO")]
        [SugarColumn(ColumnName = "monto", ColumnDescription = "Monto")]
        public double? MONTO {
            get {
                return this._MONTO;
            }
            set {
                this._MONTO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer SERIE
        /// Longitud: 100
        /// </summary>
        [DataNames("SERIE")]
        [SugarColumn(ColumnName = "serie", ColumnDescription = "Serie", Length = 100)]
        public string SERIE {
            get {
                return this._SERIE;
            }
            set {
                this._SERIE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer FOLIO
        /// Longitud: 100
        /// </summary>
        [DataNames("FOLIO")]
        [SugarColumn(ColumnName = "folio", ColumnDescription = "Folio", Length = 100)]
        public string FOLIO {
            get {
                return this._FOLIO;
            }
            set {
                this._FOLIO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC Emisor (RFCEMISOR)
        /// Longitud: 30
        /// </summary>
        [DataNames("RFCEMISOR")]
        [SugarColumn(ColumnName = "rfcemisor", ColumnDescription = "RFC emisor", Length = 30)]
        public string RFCEMISOR {
            get {
                return this._RFCEMISOR;
            }
            set {
                this._RFCEMISOR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC receptor (RFCRECEPTOR)
        /// Longitud: 30
        /// </summary>
        [DataNames("RFCRECEPTOR")]
        [SugarColumn(ColumnName = "rfcreceptor", ColumnDescription = "RFC receptor", Length = 30)]
        public string RFCRECEPTOR {
            get {
                return this._RFCRECEPTOR;
            }
            set {
                this._RFCRECEPTOR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer orden de la partida (ORDEN)
        /// </summary>
        [DataNames("ORDEN")]
        [SugarColumn(ColumnName = "orden", ColumnDescription = "Orden de la partida")]
        public int? ORDEN {
            get {
                return this._ORDEN;
            }
            set {
                this._ORDEN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer FECHA
        /// </summary>
        [DataNames("FECHA")]
        [SugarColumn(ColumnName = "fecha", ColumnDescription = "Fecha")]
        public DateTime? FECHA {
            get {
                return this._FECHA;
            }
            set {
                this._FECHA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Indica si es CFDi o Extranjero (TIPOCOMPROBANTE)
        /// </summary>
        [DataNames("TIPOCOMPROBANTE")]
        [SugarColumn(ColumnName = "tipocomprobante", ColumnDescription = "Indica si es CFDi o Extranjero")]
        public int? TIPOCOMPROBANTE {
            get {
                return this._TIPOCOMPROBANTE;
            }
            set {
                this._TIPOCOMPROBANTE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de cambio (TIPOCAMBIO)
        /// </summary>
        [DataNames("TIPOCAMBIO")]
        [SugarColumn(ColumnName = "tipocambio", ColumnDescription = "tipo de cambio")]
        public double? TIPOCAMBIO {
            get {
                return this._TIPOCAMBIO;
            }
            set {
                this._TIPOCAMBIO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer VERSIONCFDI
        /// Longitud: 6
        /// </summary>
        [DataNames("VERSIONCFDI")]
        [SugarColumn(ColumnName = "versioncfdi", ColumnDescription = "version del CFDI", Length = 6)]
        public string VERSIONCFDI {
            get {
                return this._VERSIONCFDI;
            }
            set {
                this._VERSIONCFDI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer MONEDA
        /// Longitud: 3
        /// </summary>
        [DataNames("MONEDA")]
        [SugarColumn(ColumnName = "moneda", ColumnDescription = "Tipo de moneda", Length = 3)]
        public string MONEDA {
            get {
                return this._MONEDA;
            }
            set {
                this._MONEDA = value;
                this.OnPropertyChanged();
            }
        }
    }
}
