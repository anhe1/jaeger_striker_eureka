﻿using System.ComponentModel;

namespace Jaeger.Domain.Aspel.Coi80.Entities {
    public class PolizaDetailModel : PolizaModel {
        private BindingList<AuxiliarDetailModel> auxiliares;

        public PolizaDetailModel() {
            this.auxiliares = new BindingList<AuxiliarDetailModel>();
        }

        public int Numero {
            get {
                return int.Parse(this.NUM_POLIZ);
            }
            set {
                this.NUM_POLIZ = value.ToString();
                this.OnPropertyChanged();
            }
        }

        public BindingList<AuxiliarDetailModel> Auxiliares {
            get {
                return this.auxiliares;
            }
            set {
                this.auxiliares = value;
                this.OnPropertyChanged();
            }
        }
    }
}
