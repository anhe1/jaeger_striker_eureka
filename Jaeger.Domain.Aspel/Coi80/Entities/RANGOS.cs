﻿using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Aspel.Coi80.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("rangos")]
    public partial class RANGOS : Domain.Base.Abstractions.BasePropertyChangeImplementation {
        private int _RANGO;
        private string _DESCRIP;

        public RANGOS() {


        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("RANGO")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "rango", ColumnDescription = "")]
        public int RANGO {
            get {
                return this._RANGO;
            }
            set {
                this._RANGO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("DESCRIP")]
        [SugarColumn(ColumnName = "descrip", ColumnDescription = "")]
        public string DESCRIP {
            get {
                return this._DESCRIP;
            }
            set {
                this._DESCRIP = value;
                this.OnPropertyChanged();
            }
        }
    }
}
