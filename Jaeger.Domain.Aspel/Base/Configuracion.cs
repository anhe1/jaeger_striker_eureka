﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Aspel.Base {
    public class Configuracion : BasePropertyChangeImplementation {
        private DataBaseConfiguracion bancos50;
        private DataBaseConfiguracion coi80;

        public Configuracion() {
            this.Empresa = new EmpresaGeneral();
            this.bancos50 = new DataBaseConfiguracion();
            this.coi80 = new DataBaseConfiguracion();
        }

        [DisplayName("Empresa")]
        [JsonProperty("Empresa")]
        [TypeConverter(typeof(PropertiesConvert))]
        public EmpresaGeneral Empresa { get; set; }

        [DisplayName("Bancos 50"), Description("Configuración de Base de Datos de Bancos ver. 5.0")]
        [JsonProperty("bancos50")]
        [TypeConverter(typeof(PropertiesConvert))]
        public DataBaseConfiguracion Bancos {
            get { return this.bancos50; }
            set { this.bancos50 = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("COI 80"), Description("Configuración de Base de Datos de COI ver. 8.0")]
        [JsonProperty("coi80")]
        [TypeConverter(typeof(PropertiesConvert))]
        public DataBaseConfiguracion Coi {
            get {
                return this.coi80;
            }
            set {
                this.coi80 = value;
                this.OnPropertyChanged();
            }
        }

        public override string ToString() {
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.SerializeObject(this, conf);
        }

        public static Configuracion Load(string contenido) {
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            var local = new Configuracion();
            try {
                local = JsonConvert.DeserializeObject<Configuracion>(HelperCsTripleDes.Decrypt(contenido, "", true), conf);
                if (local == null)
                    local = JsonConvert.DeserializeObject<Configuracion>(contenido, conf);
                return local;

            } catch (Exception) {
                return null;
            }
        }
    }
}
