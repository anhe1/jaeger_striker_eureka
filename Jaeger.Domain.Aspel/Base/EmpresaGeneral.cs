﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Aspel.Base {
    [JsonObject("general")]
    public class EmpresaGeneral : BasePropertyChangeImplementation {

        public EmpresaGeneral() {
            
        }

        /// <summary>
        /// obtener o establecer la clave o subdominio de la empresa
        /// </summary>
        [DisplayName("Clave")]
        public string Clave { get; set; }

        /// <summary>
        /// obtener o establecer nombre de la razon social
        /// </summary>
        [DisplayName("Nombre ó Razon Social")]
        [JsonProperty("razonSocial")]
        public string RazonSocial { get; set; }

        /// <summary>
        /// obtener o establecer Registro Federal de Causantes (RFC)
        /// </summary>
        [DisplayName("Registro Federal de Causantes (RFC)")]
        [JsonProperty("rfc")]
        public string RFC { get; set; }

        /// <summary>
        /// obtener o establecer regimen fiscal
        /// </summary>
        [DisplayName("Régimen Fiscal")]
        [JsonProperty("regimenFiscal")]
        public string RegimenFiscal { get; set; }

        /// <summary>
        /// obtener o establecer registro patronal
        /// </summary>
        [DisplayName("Registro Patronal")]
        [JsonProperty("registroPatronal")]
        public string RegistroPatronal { get; set; }

        /// <summary>
        /// obtener o establecer el domicilio fiscal
        /// </summary>
        [DisplayName("Domicilio Fiscal")]
        [JsonProperty("domicilioFiscal")]
        [TypeConverter(typeof(PropertiesConvert))]
        public DomicilioFiscal DomicilioFiscal { get; set; }

        public string Json() {
            //JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.SerializeObject(this);
        }

        public static EmpresaGeneral Json(string inputString) {
            JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.DeserializeObject<EmpresaGeneral>(inputString, conf);
        }
    }
}
