﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Aspel.Base {
    public class ServerLocal : BasePropertyChangeImplementation {
        private BindingList<Configuracion> empresas;

        public ServerLocal() {
            this.empresas = new BindingList<Configuracion>();
        }

        public BindingList<Configuracion> Empresas {
            get { return this.empresas; }
            set {
                this.empresas = value;
                this.OnPropertyChanged();
            }
        }

        public string Json() {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, Formatting = Formatting.Indented });
        }

        public static ServerLocal Json(string inputString) {
            if (!string.IsNullOrEmpty(inputString)) {
                try {
                    return JsonConvert.DeserializeObject<ServerLocal>(inputString, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
            return null;
        }
    }
}
