﻿namespace Jaeger.Domain.Aspel.Base {
    public class SynapsisLocal {
        private readonly string localFileName = @"C:\Jaeger\jaeger_echo_dark.json";

        public SynapsisLocal() {

        }

        public ServerLocal Load() {
            return ServerLocal.Json(System.IO.File.ReadAllText(localFileName));
        }

        public ServerLocal Load(string jsonInput) {
            return ServerLocal.Json(jsonInput);
        }

        public void Save(ServerLocal synapsis, string localFile) {
            System.IO.File.WriteAllText(localFile, synapsis.Json());
        }

        public void Save(ServerLocal synapsis) {
            System.IO.File.WriteAllText(this.localFileName, synapsis.Json());
        }
    }
}
