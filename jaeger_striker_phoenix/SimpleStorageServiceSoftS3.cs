﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Xml;
using System.Diagnostics;
using Jaeger.SoftAWS.S3;
using Jaeger.Amazon.S3.ValueObjects;
using Jaeger.SoftAWS;

namespace Jaeger.UI {

    public class SimpleStorageServiceSoftS3 : Amazon.S3.Helpers.FileContentType, Amazon.S3.Contracts.ISimpleStorageService {
        private readonly string fileNameLog = @"C:\Jaeger\Jaeger.Log\jaeger_echo_alpha.log";

        public SimpleStorageServiceSoftS3(Amazon.S3.Settings settings) : base() {
            this.AccessKeyId = settings.AccessKeyId;
            this.SecretAccessKey = settings.SecretAccessKey;
            this.Region = settings.Region;
        }

        public SimpleStorageServiceSoftS3(string AccessKeyId, string SecretAccessKey, string regionEndpoint) {
            this.AccessKeyId = AccessKeyId;
            this.SecretAccessKey = SecretAccessKey;
            this.Region = Region;
        }

        public string AccessKeyId { get; set; }

        public string SecretAccessKey { get; set; }

        public string Region { get; set; }

        public string Message { get; set; }

        /// <summary>
        /// obtener o establecer codigo de status
        /// </summary>
        public int StatusCode { get; set; }

        public bool Delete(string bucketName, string keyName) {
            //The following code deletes an object in a bucket.  If versioning on the bucket is enabled,
            //the call will replace the object with a Delete Marker.
            //DELETE Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectDELETE.html

            REST MyREST = new REST();

            string RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", bucketName, keyName, "");

            string RequestMethod = "DELETE";

            Dictionary<string, string> ExtraRequestHeaders = new Dictionary<string, string>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            string AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, this.AccessKeyId, this.SecretAccessKey);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            bool RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            Debug.Print("");
            Debug.Print(MyREST.LogData);
            Debug.Print("");

            if (RetBool == true) {
                this.Message = "The object was deleted.";
            }
            else {
                this.Message = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);
            }
            return RetBool;
        }

        public bool Download(string localFileName, string bucketName, string keyName) {
            //GET Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectGET.html

            Download MyDownload = new Download();

            string RequestURL;
            RequestURL = MyDownload.BuildS3RequestURL(true, "s3.amazonaws.com", bucketName, keyName, "");

            string RequestMethod = "GET";

            Dictionary<string, string> ExtraRequestHeaders = new Dictionary<string, string>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            string AuthorizationValue;
            AuthorizationValue = MyDownload.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, this.AccessKeyId, this.SecretAccessKey);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            bool RetBool;
            RetBool = MyDownload.DownloadFile(RequestURL, RequestMethod, ExtraRequestHeaders, localFileName, false);

            Debug.Print("");
            Debug.Print(MyDownload.LogData);
            Debug.Print("");

            if (RetBool == true) {
                this.Message = "Download complete.";
            }
            else {
                this.StatusCode = 403;
                this.Message = FormatLogData(MyDownload.RequestURL, MyDownload.RequestMethod, MyDownload.RequestHeaders, MyDownload.ResponseStatusCode, MyDownload.ResponseStatusDescription, MyDownload.ResponseHeaders, MyDownload.ResponseStringFormatted, MyDownload.ErrorNumber, MyDownload.ErrorDescription);
            }
            return RetBool;
        }

        public bool Download(string address, string localFileName) {
            bool downloadFile;
            try {
                WebClient webClient = new WebClient();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                Stream oFileStream = new FileStream(localFileName, FileMode.Create);
                oFileStream.Write(webClient.DownloadData(address), 0, checked(webClient.DownloadData(address).Length));
                oFileStream.Flush();
                oFileStream.Close();
                downloadFile = true;
            }
            catch (Exception e) {
                this.LogWrite(e.Message);
                Console.WriteLine(e.Message);
                downloadFile = false;
            }
            return downloadFile;
        }

        public bool Exists(string bucketName, string keyName) {
            //do a HEAD operation on the object to see if it exists
            //HEAD Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectHEAD.html

            REST MyREST = new REST();

            string RequestURL;
            RequestURL = MyREST.BuildS3RequestURL(true, "s3.amazonaws.com", bucketName, keyName, "");

            string RequestMethod = "HEAD";

            Dictionary<string, string> ExtraRequestHeaders = new Dictionary<string, string>();
            ExtraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            string AuthorizationValue;
            AuthorizationValue = MyREST.GetS3AuthorizationValue(RequestURL, RequestMethod, ExtraRequestHeaders, this.AccessKeyId, this.SecretAccessKey);
            ExtraRequestHeaders.Add("Authorization", AuthorizationValue);

            bool RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, ExtraRequestHeaders, "");

            Debug.Print("");
            Debug.Print(MyREST.LogData);
            Debug.Print("");

            if (MyREST.ResponseStatusCode == 200) {
                this.Message = "The object exists, you have permission to it.";
            }
            else if (MyREST.ResponseStatusCode == 404) {
                this.Message = "The object does not exists.";
            }
            else if (MyREST.ResponseStatusCode == 403) {
                this.Message = "Permission error.  Someone else owns the object or your access keys are invalid.";
            }
            else {
                this.Message = FormatLogData(MyREST.RequestURL, MyREST.RequestMethod, MyREST.RequestHeaders, MyREST.ResponseStatusCode, MyREST.ResponseStatusDescription, MyREST.ResponseHeaders, MyREST.ResponseStringFormatted, MyREST.ErrorNumber, MyREST.ErrorDescription);
            }
            return RetBool;
        }

        public string Upload(string localFileName, string keyName, string contentType, string bucketName, string region, bool makePublic, StorageClassTypeEnum storageClassTypeEnum = StorageClassTypeEnum.Standar) {
            if (File.Exists(localFileName)) {
                //PUT Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectPUT.html
                Dictionary<string, string> extraRequestHeaders = new Dictionary<string, string>();

                if (contentType != "") {
                    extraRequestHeaders.Add("Content-Type", contentType);
                }

                if (makePublic) {
                    //add a x-amz-acl header with the value of public-read to make the uploaded file public
                    //PUT Object acl: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectPUTacl.html
                    extraRequestHeaders.Add("x-amz-acl", "public-read");
                }
                var usessse = false;
                if (usessse) {
                    //Specifying Server-Side Encryption Using REST API: http://docs.amazonwebservices.com/AmazonS3/latest/dev/SSEUsingRESTAPI.html
                    extraRequestHeaders.Add("x-amz-server-side-encryption", "AES256");
                }

                if (storageClassTypeEnum == StorageClassTypeEnum.ReducedRedundancy) {
                    //add a x-amz-storage-class header with the value of REDUCED_REDUNDANCY to make the uploaded file reduced redundancy
                    extraRequestHeaders.Add("x-amz-storage-class", "REDUCED_REDUNDANCY");
                }

                //add a Content-MD5 header to ensure data is not corrupted over the network
                //Amazon will return an error if the MD5 they calulate does not match the MD5 you send
                CalculateHash myCalculateHash = new CalculateHash();

                string myMD5;
                myMD5 = myCalculateHash.CalculateMD5FromFile(localFileName);
                extraRequestHeaders.Add("Content-MD5", myMD5);

                Upload myUpload = new Upload();

                string requestUrl;

                if (region != "us-east-1") {
                    requestUrl = string.Concat(string.Concat("https://", "s3-", region, ".amazonaws.com"), "/", bucketName, "/", keyName);
                }
                else {
                    requestUrl = string.Concat("https://s3.amazonaws.com", "/", bucketName, "/", keyName);
                }

                string requestMethod = "PUT";

                extraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                string authorizationValue;
                authorizationValue = myUpload.GetS3AuthorizationValue(requestUrl, requestMethod, extraRequestHeaders, this.AccessKeyId, this.SecretAccessKey);
                extraRequestHeaders.Add("Authorization", authorizationValue);

                bool retBool;
                retBool = myUpload.UploadFile(requestUrl, requestMethod, extraRequestHeaders, localFileName);

                Debug.Print("");
                Debug.Print(myUpload.LogData);
                Debug.Print("");

                if (retBool == true) {
                    this.StatusCode = 0;
                    this.Message = "Upload complete.";
                    return requestUrl;
                }
                else {
                    this.Message = FormatLogData(myUpload.RequestURL, myUpload.RequestMethod, myUpload.RequestHeaders, myUpload.ResponseStatusCode, myUpload.ResponseStatusDescription, myUpload.ResponseHeaders, myUpload.ResponseStringFormatted, myUpload.ErrorNumber, myUpload.ErrorDescription);
                    this.StatusCode = 505;
                }
            }
            else {
                this.StatusCode = 2;
                this.Message = "No se tiene acceso al archivo";
            }
            return string.Empty;
        }

        public string Upload(Stream inputStream, string localFileName, string keyName, string contentType, string bucketName, string region, bool makePublic, StorageClassTypeEnum storageClassTypeEnum = StorageClassTypeEnum.Standar) {
            //PUT Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectPUT.html
            Dictionary<string, string> extraRequestHeaders = new Dictionary<string, string>();

            if (contentType != "") {
                extraRequestHeaders.Add("Content-Type", contentType);
            }

            if (makePublic) {
                //add a x-amz-acl header with the value of public-read to make the uploaded file public
                //PUT Object acl: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectPUTacl.html
                extraRequestHeaders.Add("x-amz-acl", "public-read");
            }
            var usessse = false;
            if (usessse) {
                //Specifying Server-Side Encryption Using REST API: http://docs.amazonwebservices.com/AmazonS3/latest/dev/SSEUsingRESTAPI.html
                extraRequestHeaders.Add("x-amz-server-side-encryption", "AES256");
            }

            if (storageClassTypeEnum == StorageClassTypeEnum.ReducedRedundancy) {
                //add a x-amz-storage-class header with the value of REDUCED_REDUNDANCY to make the uploaded file reduced redundancy
                extraRequestHeaders.Add("x-amz-storage-class", "REDUCED_REDUNDANCY");
            }

            //add a Content-MD5 header to ensure data is not corrupted over the network
            //Amazon will return an error if the MD5 they calulate does not match the MD5 you send
            CalculateHash myCalculateHash = new CalculateHash();

            string myMD5;
            myMD5 = myCalculateHash.CalculateMD5FromFile(localFileName);
            extraRequestHeaders.Add("Content-MD5", myMD5);

            Upload myUpload = new Upload();

            string requestUrl;

            if (region != "us-east-1") {
                requestUrl = string.Concat(string.Concat("https://", "s3-", region, ".amazonaws.com"), "/", bucketName, "/", keyName);
            }
            else {
                requestUrl = string.Concat("https://s3.amazonaws.com", "/", bucketName, "/", keyName);
            }

            string requestMethod = "PUT";

            extraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            string authorizationValue;
            authorizationValue = myUpload.GetS3AuthorizationValue(requestUrl, requestMethod, extraRequestHeaders, this.AccessKeyId, this.SecretAccessKey);
            extraRequestHeaders.Add("Authorization", authorizationValue);

            bool retBool;
            retBool = myUpload.UploadFile(requestUrl, requestMethod, extraRequestHeaders, inputStream, localFileName);

            Debug.Print("");
            Debug.Print(myUpload.LogData);
            Debug.Print("");

            if (retBool == true) {
                this.StatusCode = 0;
                this.Message = "Upload complete.";
                return requestUrl;
            }
            else {
                //this.Mensaje = FormatLogData(myUpload.RequestURL, myUpload.RequestMethod, myUpload.RequestHeaders, myUpload.ResponseStatusCode, myUpload.ResponseStatusDescription, myUpload.ResponseHeaders, myUpload.ResponseStringFormatted, myUpload.ErrorNumber, myUpload.ErrorDescription);
                this.StatusCode = 505;
            }
            return string.Empty;
        }

        public void LogWrite(string oError) {
            try {
                if (!File.Exists(fileNameLog)) {
                    File.Create(fileNameLog).Close();
                }
                StreamWriter streamWriter = File.AppendText(fileNameLog);
                object[] type = new object[] { oError, "|", DateTime.Now };
                streamWriter.WriteLine(string.Concat(type));
                streamWriter.Close();
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        public static string FormatLogData(string requestUrl, string requestMethod, Dictionary<string, string> requestHeaders, int responseStatusCode, string responseStatusDescription, Dictionary<string, string> responseHeaders, string responseString, int errorNumber, string errorDescription) {
            string returnString = "";

            if (errorNumber != 0) {
                returnString += "SoftS3 ErrorDescription: " + errorDescription + Environment.NewLine;
                returnString += "SoftS3 ErrorNumber: " + errorNumber + Environment.NewLine;
                returnString += Environment.NewLine;
            }

            if (responseStatusCode != 0) {
                returnString += "Request URL: " + requestUrl + Environment.NewLine;
                returnString += "Request Method: " + requestMethod + Environment.NewLine;

                foreach (KeyValuePair<string, string> MyHeader in requestHeaders) {
                    returnString += "Request Header: " + MyHeader.Key + ":" + MyHeader.Value + Environment.NewLine;
                }

                returnString += Environment.NewLine;
                returnString += "Response Status Code: " + responseStatusCode + Environment.NewLine;
                returnString += "Response Status Description: " + responseStatusDescription + Environment.NewLine;

                foreach (KeyValuePair<string, string> MyHeader in responseHeaders) {
                    returnString += "Response Header: " + MyHeader.Key + ":" + MyHeader.Value + Environment.NewLine;
                }

                if (responseString != "") {
                    returnString += Environment.NewLine;

                    try {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(responseString);

                        returnString += "Response XML: " + Environment.NewLine + responseString + Environment.NewLine;

                        XmlNode xmlNode;
                        xmlNode = xmlDoc.SelectSingleNode("/Error/Message");

                        if (xmlNode != null) {
                            returnString += Environment.NewLine;
                            returnString += "Amazon Error Message: " + xmlNode.InnerText + Environment.NewLine;
                        }
                    }
                    catch (Exception e) {
                        Console.WriteLine(e.Message);
                        returnString += "Response String: " + Environment.NewLine + responseString + Environment.NewLine;
                    }

                }
            }

            return returnString;
        }
    }
}
