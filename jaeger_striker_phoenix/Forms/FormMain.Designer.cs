﻿namespace Jaeger.UI.Forms {
    partial class FormMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ComboBoxInterface = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TextBoxRegion = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.TextBoxAWSAccessKeyId = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.TextBoxAWSSecretAccessKey = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ButtonDownloadFile1 = new System.Windows.Forms.Button();
            this.ButtonDownloadBrowse1 = new System.Windows.Forms.Button();
            this.TextBoxDownloadFileName1 = new System.Windows.Forms.TextBox();
            this.TextBoxDownloadURL = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ButtonDownloadBrowse = new System.Windows.Forms.Button();
            this.Label19 = new System.Windows.Forms.Label();
            this.ButtonDownloadFile = new System.Windows.Forms.Button();
            this.TextBoxDownloadFileName = new System.Windows.Forms.TextBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.TextBoxDownloadKeyName = new System.Windows.Forms.TextBox();
            this.Label11 = new System.Windows.Forms.Label();
            this.TextBoxDownloadBucketName = new System.Windows.Forms.TextBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.TextBoxUploadBucketName = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.ComboBoxStorageClass = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Label22 = new System.Windows.Forms.Label();
            this.Label21 = new System.Windows.Forms.Label();
            this.CheckBoxUploadMakePublic = new System.Windows.Forms.CheckBox();
            this.TextBoxUploadContentType = new System.Windows.Forms.TextBox();
            this.Label20 = new System.Windows.Forms.Label();
            this.Label18 = new System.Windows.Forms.Label();
            this.ButtonUploadBrowse = new System.Windows.Forms.Button();
            this.TextBoxUploadKeyName = new System.Windows.Forms.TextBox();
            this.TextBoxUploadFileName = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.ButtonUploadFile = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.GroupUnzip = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.TextBoxUnzipTargetFolder = new System.Windows.Forms.TextBox();
            this.TextBoxUnzipSourceFolder = new System.Windows.Forms.TextBox();
            this.ButtonUnzipTarget = new System.Windows.Forms.Button();
            this.ButtonUnzipSource = new System.Windows.Forms.Button();
            this.ButtonUnZipStart = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.ButtonUploadLog = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.ButtonCreateZIP = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.TextBoxOutput = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.Label16 = new System.Windows.Forms.Label();
            this.ButtonGenerateURL = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.ButtonSetObjectMetadata = new System.Windows.Forms.Button();
            this.ButtonGetObjectACLs = new System.Windows.Forms.Button();
            this.TextBoxObjectKeyName = new System.Windows.Forms.TextBox();
            this.ButtonDeleteObject = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.ButtonSetObjectACLs = new System.Windows.Forms.Button();
            this.TextBoxObjectBucketName = new System.Windows.Forms.TextBox();
            this.ButtonGetObjectProperties = new System.Windows.Forms.Button();
            this.ButtonCopyObject = new System.Windows.Forms.Button();
            this.ButtonObjectExists = new System.Windows.Forms.Button();
            this.menuStrip.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.GroupUnzip.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(717, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(93, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 400);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(717, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(35, 17);
            this.toolStripStatusLabel1.Text = "Listo.";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.textBox5);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.GroupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 376);
            this.panel1.TabIndex = 2;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(3, 249);
            this.textBox5.MaxLength = 1032767;
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox5.Size = new System.Drawing.Size(242, 122);
            this.textBox5.TabIndex = 27;
            this.textBox5.WordWrap = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ComboBoxInterface);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(3, 180);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(242, 63);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Options";
            // 
            // ComboBoxInterface
            // 
            this.ComboBoxInterface.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxInterface.FormattingEnabled = true;
            this.ComboBoxInterface.Items.AddRange(new object[] {
            "SimpleStorageServiceAWS3",
            "SoftAWS"});
            this.ComboBoxInterface.Location = new System.Drawing.Point(6, 35);
            this.ComboBoxInterface.Name = "ComboBoxInterface";
            this.ComboBoxInterface.Size = new System.Drawing.Size(230, 21);
            this.ComboBoxInterface.TabIndex = 3;
            this.ComboBoxInterface.SelectedIndexChanged += new System.EventHandler(this.ComboBoxInterface_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Interface";
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.label6);
            this.GroupBox1.Controls.Add(this.TextBoxRegion);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.TextBoxAWSAccessKeyId);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.TextBoxAWSSecretAccessKey);
            this.GroupBox1.Location = new System.Drawing.Point(3, 7);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(242, 167);
            this.GroupBox1.TabIndex = 26;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Global Settings (Enter your values below)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Region";
            // 
            // TextBoxRegion
            // 
            this.TextBoxRegion.Location = new System.Drawing.Point(6, 138);
            this.TextBoxRegion.Name = "TextBoxRegion";
            this.TextBoxRegion.Size = new System.Drawing.Size(230, 20);
            this.TextBoxRegion.TabIndex = 24;
            this.TextBoxRegion.Text = "us-east-1";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(3, 74);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(91, 13);
            this.Label3.TabIndex = 22;
            this.Label3.Text = "SecretAccessKey";
            // 
            // TextBoxAWSAccessKeyId
            // 
            this.TextBoxAWSAccessKeyId.Location = new System.Drawing.Point(6, 40);
            this.TextBoxAWSAccessKeyId.Name = "TextBoxAWSAccessKeyId";
            this.TextBoxAWSAccessKeyId.Size = new System.Drawing.Size(230, 20);
            this.TextBoxAWSAccessKeyId.TabIndex = 16;
            this.TextBoxAWSAccessKeyId.Text = "AKIAIF5K3IMRBYEVYGIQ";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(3, 24);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(69, 13);
            this.Label2.TabIndex = 21;
            this.Label2.Text = "AccessKeyId";
            // 
            // TextBoxAWSSecretAccessKey
            // 
            this.TextBoxAWSSecretAccessKey.Location = new System.Drawing.Point(6, 90);
            this.TextBoxAWSSecretAccessKey.Name = "TextBoxAWSSecretAccessKey";
            this.TextBoxAWSSecretAccessKey.Size = new System.Drawing.Size(230, 20);
            this.TextBoxAWSSecretAccessKey.TabIndex = 18;
            this.TextBoxAWSSecretAccessKey.Text = "MttLLerFIcLfurUS06xXGBJyE4AIy9vOKidDO8Ix";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(459, 350);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Download";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ButtonDownloadFile1);
            this.groupBox4.Controls.Add(this.ButtonDownloadBrowse1);
            this.groupBox4.Controls.Add(this.TextBoxDownloadFileName1);
            this.groupBox4.Controls.Add(this.TextBoxDownloadURL);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Location = new System.Drawing.Point(6, 182);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(445, 162);
            this.groupBox4.TabIndex = 52;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Option 2";
            // 
            // ButtonDownloadFile1
            // 
            this.ButtonDownloadFile1.Location = new System.Drawing.Point(105, 71);
            this.ButtonDownloadFile1.Name = "ButtonDownloadFile1";
            this.ButtonDownloadFile1.Size = new System.Drawing.Size(97, 23);
            this.ButtonDownloadFile1.TabIndex = 61;
            this.ButtonDownloadFile1.Text = "Download File";
            this.ButtonDownloadFile1.UseVisualStyleBackColor = true;
            this.ButtonDownloadFile1.Click += new System.EventHandler(this.ButtonDownloadFile1_Click);
            // 
            // ButtonDownloadBrowse1
            // 
            this.ButtonDownloadBrowse1.Location = new System.Drawing.Point(361, 43);
            this.ButtonDownloadBrowse1.Name = "ButtonDownloadBrowse1";
            this.ButtonDownloadBrowse1.Size = new System.Drawing.Size(75, 23);
            this.ButtonDownloadBrowse1.TabIndex = 60;
            this.ButtonDownloadBrowse1.Text = "Browse";
            this.ButtonDownloadBrowse1.UseVisualStyleBackColor = true;
            this.ButtonDownloadBrowse1.Click += new System.EventHandler(this.ButtonDownloadBrowse1_Click);
            // 
            // TextBoxDownloadFileName1
            // 
            this.TextBoxDownloadFileName1.Location = new System.Drawing.Point(105, 45);
            this.TextBoxDownloadFileName1.Name = "TextBoxDownloadFileName1";
            this.TextBoxDownloadFileName1.Size = new System.Drawing.Size(250, 20);
            this.TextBoxDownloadFileName1.TabIndex = 57;
            // 
            // TextBoxDownloadURL
            // 
            this.TextBoxDownloadURL.Location = new System.Drawing.Point(105, 19);
            this.TextBoxDownloadURL.Name = "TextBoxDownloadURL";
            this.TextBoxDownloadURL.Size = new System.Drawing.Size(250, 20);
            this.TextBoxDownloadURL.TabIndex = 56;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 58;
            this.label5.Text = "URL:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 13);
            this.label13.TabIndex = 59;
            this.label13.Text = "Local File Name";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ButtonDownloadBrowse);
            this.groupBox3.Controls.Add(this.Label19);
            this.groupBox3.Controls.Add(this.ButtonDownloadFile);
            this.groupBox3.Controls.Add(this.TextBoxDownloadFileName);
            this.groupBox3.Controls.Add(this.Label10);
            this.groupBox3.Controls.Add(this.TextBoxDownloadKeyName);
            this.groupBox3.Controls.Add(this.Label11);
            this.groupBox3.Controls.Add(this.TextBoxDownloadBucketName);
            this.groupBox3.Controls.Add(this.Label12);
            this.groupBox3.Location = new System.Drawing.Point(6, 10);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(445, 166);
            this.groupBox3.TabIndex = 51;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Option 1";
            // 
            // ButtonDownloadBrowse
            // 
            this.ButtonDownloadBrowse.Location = new System.Drawing.Point(361, 96);
            this.ButtonDownloadBrowse.Name = "ButtonDownloadBrowse";
            this.ButtonDownloadBrowse.Size = new System.Drawing.Size(75, 23);
            this.ButtonDownloadBrowse.TabIndex = 55;
            this.ButtonDownloadBrowse.Text = "Browse";
            this.ButtonDownloadBrowse.UseVisualStyleBackColor = true;
            this.ButtonDownloadBrowse.Click += new System.EventHandler(this.ButtonDownloadBrowse_Click);
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Location = new System.Drawing.Point(15, 22);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(200, 13);
            this.Label19.TabIndex = 50;
            this.Label19.Text = "Use the following form to download a file.";
            // 
            // ButtonDownloadFile
            // 
            this.ButtonDownloadFile.Location = new System.Drawing.Point(105, 133);
            this.ButtonDownloadFile.Name = "ButtonDownloadFile";
            this.ButtonDownloadFile.Size = new System.Drawing.Size(97, 23);
            this.ButtonDownloadFile.TabIndex = 46;
            this.ButtonDownloadFile.Text = "Download File";
            this.ButtonDownloadFile.UseVisualStyleBackColor = true;
            this.ButtonDownloadFile.Click += new System.EventHandler(this.ButtonDownloadFile_Click);
            // 
            // TextBoxDownloadFileName
            // 
            this.TextBoxDownloadFileName.Location = new System.Drawing.Point(105, 98);
            this.TextBoxDownloadFileName.Name = "TextBoxDownloadFileName";
            this.TextBoxDownloadFileName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxDownloadFileName.TabIndex = 45;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(15, 49);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(72, 13);
            this.Label10.TabIndex = 47;
            this.Label10.Text = "Bucket Name";
            // 
            // TextBoxDownloadKeyName
            // 
            this.TextBoxDownloadKeyName.Location = new System.Drawing.Point(105, 72);
            this.TextBoxDownloadKeyName.Name = "TextBoxDownloadKeyName";
            this.TextBoxDownloadKeyName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxDownloadKeyName.TabIndex = 44;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(15, 75);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(56, 13);
            this.Label11.TabIndex = 48;
            this.Label11.Text = "Key Name";
            // 
            // TextBoxDownloadBucketName
            // 
            this.TextBoxDownloadBucketName.Location = new System.Drawing.Point(105, 46);
            this.TextBoxDownloadBucketName.Name = "TextBoxDownloadBucketName";
            this.TextBoxDownloadBucketName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxDownloadBucketName.TabIndex = 43;
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(16, 101);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(83, 13);
            this.Label12.TabIndex = 49;
            this.Label12.Text = "Local File Name";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.TextBoxUploadBucketName);
            this.tabPage1.Controls.Add(this.Label8);
            this.tabPage1.Controls.Add(this.ComboBoxStorageClass);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.Label22);
            this.tabPage1.Controls.Add(this.Label21);
            this.tabPage1.Controls.Add(this.CheckBoxUploadMakePublic);
            this.tabPage1.Controls.Add(this.TextBoxUploadContentType);
            this.tabPage1.Controls.Add(this.Label20);
            this.tabPage1.Controls.Add(this.Label18);
            this.tabPage1.Controls.Add(this.ButtonUploadBrowse);
            this.tabPage1.Controls.Add(this.TextBoxUploadKeyName);
            this.tabPage1.Controls.Add(this.TextBoxUploadFileName);
            this.tabPage1.Controls.Add(this.Label9);
            this.tabPage1.Controls.Add(this.Label7);
            this.tabPage1.Controls.Add(this.ButtonUploadFile);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(459, 350);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Upload";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // TextBoxUploadBucketName
            // 
            this.TextBoxUploadBucketName.Location = new System.Drawing.Point(105, 72);
            this.TextBoxUploadBucketName.Name = "TextBoxUploadBucketName";
            this.TextBoxUploadBucketName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadBucketName.TabIndex = 69;
            this.TextBoxUploadBucketName.Text = "correo";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(16, 75);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(72, 13);
            this.Label8.TabIndex = 70;
            this.Label8.Text = "Bucket Name";
            // 
            // ComboBoxStorageClass
            // 
            this.ComboBoxStorageClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxStorageClass.FormattingEnabled = true;
            this.ComboBoxStorageClass.Items.AddRange(new object[] {
            "STANDARD",
            "REDUCED_REDUNDANCY"});
            this.ComboBoxStorageClass.Location = new System.Drawing.Point(105, 178);
            this.ComboBoxStorageClass.Name = "ComboBoxStorageClass";
            this.ComboBoxStorageClass.Size = new System.Drawing.Size(163, 21);
            this.ComboBoxStorageClass.TabIndex = 68;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 181);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 67;
            this.label4.Text = "Storage Class ";
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Location = new System.Drawing.Point(358, 127);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(52, 13);
            this.Label22.TabIndex = 66;
            this.Label22.Text = "(Optional)";
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Location = new System.Drawing.Point(16, 154);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(66, 13);
            this.Label21.TabIndex = 64;
            this.Label21.Text = "Make Public";
            // 
            // CheckBoxUploadMakePublic
            // 
            this.CheckBoxUploadMakePublic.AutoSize = true;
            this.CheckBoxUploadMakePublic.Checked = true;
            this.CheckBoxUploadMakePublic.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBoxUploadMakePublic.Location = new System.Drawing.Point(105, 155);
            this.CheckBoxUploadMakePublic.Name = "CheckBoxUploadMakePublic";
            this.CheckBoxUploadMakePublic.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxUploadMakePublic.TabIndex = 63;
            this.CheckBoxUploadMakePublic.UseVisualStyleBackColor = true;
            // 
            // TextBoxUploadContentType
            // 
            this.TextBoxUploadContentType.Location = new System.Drawing.Point(105, 124);
            this.TextBoxUploadContentType.Name = "TextBoxUploadContentType";
            this.TextBoxUploadContentType.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadContentType.TabIndex = 62;
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Location = new System.Drawing.Point(16, 127);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(71, 13);
            this.Label20.TabIndex = 61;
            this.Label20.Text = "Content-Type";
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Location = new System.Drawing.Point(16, 16);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(186, 13);
            this.Label18.TabIndex = 60;
            this.Label18.Text = "Use the following form to upload a file.";
            // 
            // ButtonUploadBrowse
            // 
            this.ButtonUploadBrowse.Location = new System.Drawing.Point(361, 44);
            this.ButtonUploadBrowse.Name = "ButtonUploadBrowse";
            this.ButtonUploadBrowse.Size = new System.Drawing.Size(75, 23);
            this.ButtonUploadBrowse.TabIndex = 54;
            this.ButtonUploadBrowse.Text = "Browse";
            this.ButtonUploadBrowse.UseVisualStyleBackColor = true;
            this.ButtonUploadBrowse.Click += new System.EventHandler(this.ButtonUploadBrowse_Click);
            // 
            // TextBoxUploadKeyName
            // 
            this.TextBoxUploadKeyName.Location = new System.Drawing.Point(105, 98);
            this.TextBoxUploadKeyName.Name = "TextBoxUploadKeyName";
            this.TextBoxUploadKeyName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadKeyName.TabIndex = 56;
            // 
            // TextBoxUploadFileName
            // 
            this.TextBoxUploadFileName.Location = new System.Drawing.Point(105, 46);
            this.TextBoxUploadFileName.Name = "TextBoxUploadFileName";
            this.TextBoxUploadFileName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadFileName.TabIndex = 53;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(16, 101);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(56, 13);
            this.Label9.TabIndex = 59;
            this.Label9.Text = "Key Name";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(16, 49);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(83, 13);
            this.Label7.TabIndex = 57;
            this.Label7.Text = "Local File Name";
            // 
            // ButtonUploadFile
            // 
            this.ButtonUploadFile.Location = new System.Drawing.Point(105, 212);
            this.ButtonUploadFile.Name = "ButtonUploadFile";
            this.ButtonUploadFile.Size = new System.Drawing.Size(96, 23);
            this.ButtonUploadFile.TabIndex = 65;
            this.ButtonUploadFile.Text = "Upload File";
            this.ButtonUploadFile.UseVisualStyleBackColor = true;
            this.ButtonUploadFile.Click += new System.EventHandler(this.ButtonUploadFile_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(250, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(467, 376);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.GroupUnzip);
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Controls.Add(this.TextBoxOutput);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(459, 350);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Log Operations";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // GroupUnzip
            // 
            this.GroupUnzip.Controls.Add(this.label29);
            this.GroupUnzip.Controls.Add(this.label28);
            this.GroupUnzip.Controls.Add(this.TextBoxUnzipTargetFolder);
            this.GroupUnzip.Controls.Add(this.TextBoxUnzipSourceFolder);
            this.GroupUnzip.Controls.Add(this.ButtonUnzipTarget);
            this.GroupUnzip.Controls.Add(this.ButtonUnzipSource);
            this.GroupUnzip.Controls.Add(this.ButtonUnZipStart);
            this.GroupUnzip.Location = new System.Drawing.Point(8, 239);
            this.GroupUnzip.Name = "GroupUnzip";
            this.GroupUnzip.Size = new System.Drawing.Size(443, 108);
            this.GroupUnzip.TabIndex = 71;
            this.GroupUnzip.TabStop = false;
            this.GroupUnzip.Text = "Unzip";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 51);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(73, 13);
            this.label29.TabIndex = 67;
            this.label29.Text = "Target Folder:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 22);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(76, 13);
            this.label28.TabIndex = 67;
            this.label28.Text = "Source Folder:";
            // 
            // TextBoxUnzipTargetFolder
            // 
            this.TextBoxUnzipTargetFolder.Location = new System.Drawing.Point(88, 47);
            this.TextBoxUnzipTargetFolder.Name = "TextBoxUnzipTargetFolder";
            this.TextBoxUnzipTargetFolder.Size = new System.Drawing.Size(247, 20);
            this.TextBoxUnzipTargetFolder.TabIndex = 68;
            // 
            // TextBoxUnzipSourceFolder
            // 
            this.TextBoxUnzipSourceFolder.Location = new System.Drawing.Point(88, 18);
            this.TextBoxUnzipSourceFolder.Name = "TextBoxUnzipSourceFolder";
            this.TextBoxUnzipSourceFolder.Size = new System.Drawing.Size(247, 20);
            this.TextBoxUnzipSourceFolder.TabIndex = 68;
            // 
            // ButtonUnzipTarget
            // 
            this.ButtonUnzipTarget.Location = new System.Drawing.Point(341, 46);
            this.ButtonUnzipTarget.Name = "ButtonUnzipTarget";
            this.ButtonUnzipTarget.Size = new System.Drawing.Size(96, 23);
            this.ButtonUnzipTarget.TabIndex = 69;
            this.ButtonUnzipTarget.Text = "Browser";
            this.ButtonUnzipTarget.UseVisualStyleBackColor = true;
            this.ButtonUnzipTarget.Click += new System.EventHandler(this.ButtonUnzipTarget_Click);
            // 
            // ButtonUnzipSource
            // 
            this.ButtonUnzipSource.Location = new System.Drawing.Point(341, 17);
            this.ButtonUnzipSource.Name = "ButtonUnzipSource";
            this.ButtonUnzipSource.Size = new System.Drawing.Size(96, 23);
            this.ButtonUnzipSource.TabIndex = 69;
            this.ButtonUnzipSource.Text = "Browser";
            this.ButtonUnzipSource.UseVisualStyleBackColor = true;
            this.ButtonUnzipSource.Click += new System.EventHandler(this.ButtonUnzipSource_Click);
            // 
            // ButtonUnZipStart
            // 
            this.ButtonUnZipStart.Location = new System.Drawing.Point(341, 75);
            this.ButtonUnZipStart.Name = "ButtonUnZipStart";
            this.ButtonUnZipStart.Size = new System.Drawing.Size(96, 23);
            this.ButtonUnZipStart.TabIndex = 66;
            this.ButtonUnZipStart.Text = "Start";
            this.ButtonUnZipStart.UseVisualStyleBackColor = true;
            this.ButtonUnZipStart.Click += new System.EventHandler(this.ButtonUnZipStart_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.textBox1);
            this.groupBox5.Controls.Add(this.textBox4);
            this.groupBox5.Controls.Add(this.textBox3);
            this.groupBox5.Controls.Add(this.textBox2);
            this.groupBox5.Controls.Add(this.numericUpDown1);
            this.groupBox5.Controls.Add(this.ButtonUploadLog);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.ButtonCreateZIP);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Location = new System.Drawing.Point(8, 128);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(443, 105);
            this.groupBox5.TabIndex = 70;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Create Logs";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 21);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "Folder Log:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(207, 78);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(36, 13);
            this.label27.TabIndex = 69;
            this.label27.Text = "Prefix:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(10, 78);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(75, 13);
            this.label26.TabIndex = 69;
            this.label26.Text = "Bucket Name:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(72, 17);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(263, 20);
            this.textBox1.TabIndex = 24;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(249, 74);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(86, 20);
            this.textBox4.TabIndex = 68;
            this.textBox4.Text = "Logs";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(88, 74);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(113, 20);
            this.textBox3.TabIndex = 68;
            this.textBox3.Text = "xaxx010101000";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(43, 45);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(40, 20);
            this.textBox2.TabIndex = 24;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(149, 45);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(52, 20);
            this.numericUpDown1.TabIndex = 67;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // ButtonUploadLog
            // 
            this.ButtonUploadLog.Location = new System.Drawing.Point(341, 16);
            this.ButtonUploadLog.Name = "ButtonUploadLog";
            this.ButtonUploadLog.Size = new System.Drawing.Size(96, 23);
            this.ButtonUploadLog.TabIndex = 66;
            this.ButtonUploadLog.Text = "Browser";
            this.ButtonUploadLog.UseVisualStyleBackColor = true;
            this.ButtonUploadLog.Click += new System.EventHandler(this.ButtonUploadLog_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 49);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(31, 13);
            this.label25.TabIndex = 23;
            this.label25.Text = "Files:";
            // 
            // ButtonCreateZIP
            // 
            this.ButtonCreateZIP.Location = new System.Drawing.Point(341, 44);
            this.ButtonCreateZIP.Name = "ButtonCreateZIP";
            this.ButtonCreateZIP.Size = new System.Drawing.Size(96, 23);
            this.ButtonCreateZIP.TabIndex = 66;
            this.ButtonCreateZIP.Text = "Start";
            this.ButtonCreateZIP.UseVisualStyleBackColor = true;
            this.ButtonCreateZIP.Click += new System.EventHandler(this.ButtonCreateZIP_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(207, 49);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(50, 13);
            this.label24.TabIndex = 23;
            this.label24.Text = "Estimate:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(89, 49);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 13);
            this.label23.TabIndex = 23;
            this.label23.Text = "By pages:";
            // 
            // TextBoxOutput
            // 
            this.TextBoxOutput.Location = new System.Drawing.Point(8, 8);
            this.TextBoxOutput.MaxLength = 1032767;
            this.TextBoxOutput.Multiline = true;
            this.TextBoxOutput.Name = "TextBoxOutput";
            this.TextBoxOutput.ReadOnly = true;
            this.TextBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TextBoxOutput.Size = new System.Drawing.Size(443, 113);
            this.TextBoxOutput.TabIndex = 4;
            this.TextBoxOutput.WordWrap = false;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.Label16);
            this.tabPage4.Controls.Add(this.ButtonGenerateURL);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.ButtonSetObjectMetadata);
            this.tabPage4.Controls.Add(this.ButtonGetObjectACLs);
            this.tabPage4.Controls.Add(this.TextBoxObjectKeyName);
            this.tabPage4.Controls.Add(this.ButtonDeleteObject);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Controls.Add(this.ButtonSetObjectACLs);
            this.tabPage4.Controls.Add(this.TextBoxObjectBucketName);
            this.tabPage4.Controls.Add(this.ButtonGetObjectProperties);
            this.tabPage4.Controls.Add(this.ButtonCopyObject);
            this.tabPage4.Controls.Add(this.ButtonObjectExists);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(459, 350);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Object Operations";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Location = new System.Drawing.Point(15, 16);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(283, 13);
            this.Label16.TabIndex = 52;
            this.Label16.Text = "Use the following buttons to preform object related actions.";
            // 
            // ButtonGenerateURL
            // 
            this.ButtonGenerateURL.Location = new System.Drawing.Point(244, 138);
            this.ButtonGenerateURL.Name = "ButtonGenerateURL";
            this.ButtonGenerateURL.Size = new System.Drawing.Size(96, 23);
            this.ButtonGenerateURL.TabIndex = 51;
            this.ButtonGenerateURL.Text = "Generate URL";
            this.ButtonGenerateURL.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 49);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 13);
            this.label15.TabIndex = 40;
            this.label15.Text = "Bucket Name";
            // 
            // ButtonSetObjectMetadata
            // 
            this.ButtonSetObjectMetadata.Location = new System.Drawing.Point(244, 109);
            this.ButtonSetObjectMetadata.Name = "ButtonSetObjectMetadata";
            this.ButtonSetObjectMetadata.Size = new System.Drawing.Size(96, 23);
            this.ButtonSetObjectMetadata.TabIndex = 50;
            this.ButtonSetObjectMetadata.Text = "Set Metadata";
            this.ButtonSetObjectMetadata.UseVisualStyleBackColor = true;
            // 
            // ButtonGetObjectACLs
            // 
            this.ButtonGetObjectACLs.Location = new System.Drawing.Point(131, 109);
            this.ButtonGetObjectACLs.Name = "ButtonGetObjectACLs";
            this.ButtonGetObjectACLs.Size = new System.Drawing.Size(96, 23);
            this.ButtonGetObjectACLs.TabIndex = 47;
            this.ButtonGetObjectACLs.Text = "Get ACLs";
            this.ButtonGetObjectACLs.UseVisualStyleBackColor = true;
            // 
            // TextBoxObjectKeyName
            // 
            this.TextBoxObjectKeyName.Location = new System.Drawing.Point(93, 72);
            this.TextBoxObjectKeyName.Name = "TextBoxObjectKeyName";
            this.TextBoxObjectKeyName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxObjectKeyName.TabIndex = 42;
            // 
            // ButtonDeleteObject
            // 
            this.ButtonDeleteObject.Location = new System.Drawing.Point(18, 138);
            this.ButtonDeleteObject.Name = "ButtonDeleteObject";
            this.ButtonDeleteObject.Size = new System.Drawing.Size(96, 23);
            this.ButtonDeleteObject.TabIndex = 45;
            this.ButtonDeleteObject.Text = "Delete Object";
            this.ButtonDeleteObject.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(17, 75);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 43;
            this.label17.Text = "Key Name";
            // 
            // ButtonSetObjectACLs
            // 
            this.ButtonSetObjectACLs.Location = new System.Drawing.Point(131, 138);
            this.ButtonSetObjectACLs.Name = "ButtonSetObjectACLs";
            this.ButtonSetObjectACLs.Size = new System.Drawing.Size(96, 23);
            this.ButtonSetObjectACLs.TabIndex = 48;
            this.ButtonSetObjectACLs.Text = "Set ACLs";
            this.ButtonSetObjectACLs.UseVisualStyleBackColor = true;
            // 
            // TextBoxObjectBucketName
            // 
            this.TextBoxObjectBucketName.Location = new System.Drawing.Point(93, 46);
            this.TextBoxObjectBucketName.Name = "TextBoxObjectBucketName";
            this.TextBoxObjectBucketName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxObjectBucketName.TabIndex = 41;
            // 
            // ButtonGetObjectProperties
            // 
            this.ButtonGetObjectProperties.Location = new System.Drawing.Point(18, 109);
            this.ButtonGetObjectProperties.Name = "ButtonGetObjectProperties";
            this.ButtonGetObjectProperties.Size = new System.Drawing.Size(96, 23);
            this.ButtonGetObjectProperties.TabIndex = 44;
            this.ButtonGetObjectProperties.Text = "Get Properties";
            this.ButtonGetObjectProperties.UseVisualStyleBackColor = true;
            // 
            // ButtonCopyObject
            // 
            this.ButtonCopyObject.Location = new System.Drawing.Point(131, 167);
            this.ButtonCopyObject.Name = "ButtonCopyObject";
            this.ButtonCopyObject.Size = new System.Drawing.Size(96, 23);
            this.ButtonCopyObject.TabIndex = 49;
            this.ButtonCopyObject.Text = "Copy Object";
            this.ButtonCopyObject.UseVisualStyleBackColor = true;
            // 
            // ButtonObjectExists
            // 
            this.ButtonObjectExists.Location = new System.Drawing.Point(18, 167);
            this.ButtonObjectExists.Name = "ButtonObjectExists";
            this.ButtonObjectExists.Size = new System.Drawing.Size(96, 23);
            this.ButtonObjectExists.TabIndex = 46;
            this.ButtonObjectExists.Text = "Object Exists";
            this.ButtonObjectExists.UseVisualStyleBackColor = true;
            this.ButtonObjectExists.Click += new System.EventHandler(this.ButtonObjectExists_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 422);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Soft S3 All Operations for C# (Tester)";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.GroupUnzip.ResumeLayout(false);
            this.GroupUnzip.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox TextBoxAWSAccessKeyId;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox TextBoxAWSSecretAccessKey;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox ComboBoxInterface;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl1;
        internal System.Windows.Forms.ComboBox ComboBoxStorageClass;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label Label22;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.CheckBox CheckBoxUploadMakePublic;
        internal System.Windows.Forms.TextBox TextBoxUploadContentType;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.Button ButtonUploadBrowse;
        internal System.Windows.Forms.TextBox TextBoxUploadKeyName;
        internal System.Windows.Forms.TextBox TextBoxUploadFileName;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Button ButtonUploadFile;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.TextBox TextBoxRegion;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        internal System.Windows.Forms.TextBox TextBoxUploadBucketName;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.TextBox TextBoxDownloadFileName;
        internal System.Windows.Forms.TextBox TextBoxDownloadKeyName;
        internal System.Windows.Forms.TextBox TextBoxDownloadBucketName;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Button ButtonDownloadFile;
        private System.Windows.Forms.GroupBox groupBox3;
        internal System.Windows.Forms.Button ButtonDownloadBrowse;
        private System.Windows.Forms.GroupBox groupBox4;
        internal System.Windows.Forms.Button ButtonDownloadBrowse1;
        internal System.Windows.Forms.TextBox TextBoxDownloadFileName1;
        internal System.Windows.Forms.TextBox TextBoxDownloadURL;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label label13;
        internal System.Windows.Forms.Button ButtonDownloadFile1;
        private System.Windows.Forms.TabPage tabPage3;
        internal System.Windows.Forms.Label label14;
        internal System.Windows.Forms.Button ButtonUploadLog;
        internal System.Windows.Forms.TextBox textBox1;
        internal System.Windows.Forms.TextBox TextBoxOutput;
        private System.Windows.Forms.TabPage tabPage4;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.Button ButtonGenerateURL;
        internal System.Windows.Forms.Label label15;
        internal System.Windows.Forms.Button ButtonSetObjectMetadata;
        internal System.Windows.Forms.Button ButtonGetObjectACLs;
        internal System.Windows.Forms.TextBox TextBoxObjectKeyName;
        internal System.Windows.Forms.Button ButtonDeleteObject;
        internal System.Windows.Forms.Label label17;
        internal System.Windows.Forms.Button ButtonSetObjectACLs;
        internal System.Windows.Forms.TextBox TextBoxObjectBucketName;
        internal System.Windows.Forms.Button ButtonGetObjectProperties;
        internal System.Windows.Forms.Button ButtonCopyObject;
        internal System.Windows.Forms.Button ButtonObjectExists;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        internal System.Windows.Forms.Label label23;
        internal System.Windows.Forms.Label label24;
        internal System.Windows.Forms.TextBox textBox2;
        internal System.Windows.Forms.Label label25;
        internal System.Windows.Forms.Button ButtonCreateZIP;
        internal System.Windows.Forms.Label label26;
        internal System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.GroupBox GroupUnzip;
        private System.Windows.Forms.GroupBox groupBox5;
        internal System.Windows.Forms.TextBox textBox5;
        internal System.Windows.Forms.Label label27;
        internal System.Windows.Forms.TextBox textBox4;
        internal System.Windows.Forms.Label label29;
        internal System.Windows.Forms.Label label28;
        internal System.Windows.Forms.TextBox TextBoxUnzipTargetFolder;
        internal System.Windows.Forms.TextBox TextBoxUnzipSourceFolder;
        internal System.Windows.Forms.Button ButtonUnzipTarget;
        internal System.Windows.Forms.Button ButtonUnzipSource;
        internal System.Windows.Forms.Button ButtonUnZipStart;
    }
}