﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Ionic.Zip;

namespace Jaeger.UI.Forms {
    public partial class FormMain : Form {
        protected Amazon.S3.Contracts.ISimpleStorageService editaS3 = null;

        public FormMain() {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.ComboBoxInterface.SelectedIndex = 0;
        }

        #region Upload

        private void ButtonUploadBrowse_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog();
            if (openFile.ShowDialog(this) == DialogResult.Cancel)
                return;
            this.TextBoxUploadFileName.Text = openFile.FileName;
            this.TextBoxUploadKeyName.Text = Path.GetFileName(this.TextBoxUploadFileName.Text);
        }

        private void ButtonUploadFile_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.UpLoad)) {
                espera.ShowDialog(this);
            }
        }

        private void UpLoad() {
            this.toolStripStatusLabel1.Text = editaS3.Upload(this.TextBoxUploadFileName.Text, this.TextBoxUploadBucketName.Text, this.TextBoxUploadContentType.Text, this.TextBoxUploadBucketName.Text,this.TextBoxRegion.Text, this.CheckBoxUploadMakePublic.Checked, Amazon.S3.ValueObjects.StorageClassTypeEnum.Standar);
        }

        #endregion

        #region Download

        private void ButtonDownloadBrowse_Click(object sender, EventArgs e) {
            var saveFile = new SaveFileDialog() { FileName = Path.GetFileName(this.TextBoxDownloadKeyName.Text) };
            if (saveFile.ShowDialog(this) == DialogResult.Cancel)
                return;
            this.TextBoxDownloadFileName.Text = saveFile.FileName;
        }

        private void ButtonDownloadFile_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Download1)) {
                espera.ShowDialog(this);
            }
        }

        private void ButtonDownloadBrowse1_Click(object sender, EventArgs e) {
            var saveFile = new SaveFileDialog() { FileName = Path.GetFileName(this.TextBoxDownloadURL.Text) };
            if (saveFile.ShowDialog(this) == DialogResult.Cancel)
                return;
            this.TextBoxDownloadFileName1.Text = saveFile.FileName;
        }

        private void ButtonDownloadFile1_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Download2)) {
                espera.ShowDialog(this);
            }
        }

        private void Download1() {
            this.editaS3.Download(this.TextBoxDownloadFileName.Text, this.TextBoxDownloadBucketName.Text, this.TextBoxDownloadKeyName.Text);
        }

        private void Download2() {
            this.editaS3.Download(this.TextBoxDownloadURL.Text, this.TextBoxDownloadFileName1.Text);
        }

        #endregion

        #region configuracion y servicio

        private void CreateService() {
            if (this.ComboBoxInterface.Text == this.ComboBoxInterface.Items[0].ToString()) {
                editaS3 = this.GetSimpleStorageServiceAWS3();
            }
            else {
                editaS3 = this.GetServiceSoftS3();
            }
        }

        private SimpleStorageServiceSoftS3 GetServiceSoftS3() {
            var d = this.Config();
            return new SimpleStorageServiceSoftS3(d.AccessKeyId, d.SecretAccessKey, d.Region);
        }

        private Amazon.S3.Helpers.SimpleStorageServiceAWS3 GetSimpleStorageServiceAWS3() {
            var d = this.Config();
            return new Amazon.S3.Helpers.SimpleStorageServiceAWS3(d.AccessKeyId, d.SecretAccessKey, d.Region);
        }

        private Amazon.S3.Settings Config() {
            return new Amazon.S3.Settings { AccessKeyId = this.TextBoxAWSAccessKeyId.Text, SecretAccessKey = this.TextBoxAWSSecretAccessKey.Text, Region = this.TextBoxRegion.Text };
        }

        #endregion

        #region operaciones

        private void ButtonObjectExists_Click(object sender, EventArgs e) {
            (sender as Button).Enabled = false;
            using (var espera = new WaitingForm(this.Exist)) {
                espera.Text = "Comprobando ...";
                espera.ShowDialog(this);
            }
            (sender as Button).Enabled = true;
        }

        private void Exist() {
            var salida = this.editaS3.Exists(TextBoxObjectBucketName.Text, TextBoxObjectKeyName.Text);
            this.toolStripStatusLabel1.Text = (salida ? "Existe" : "Nones!");
        }

        #endregion

        private void ComboBoxInterface_SelectedIndexChanged(object sender, EventArgs e) {
            this.CreateService();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ButtonUploadLog_Click(object sender, EventArgs e) {
            var openFolder = new FolderBrowserDialog() { Description = "Selecciona ruta" };

            if (openFolder.ShowDialog(this) != DialogResult.OK)
                return;

            if (Directory.Exists(openFolder.SelectedPath) == false) {
                MessageBox.Show(this, "No se encontro una ruta valida!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            textBox1.Text = openFolder.SelectedPath;

            // Take a snapshot of the file system.  
            string startFolder = openFolder.SelectedPath; 

            // Used in WriteLine to trim output lines.  
            int trimLength = startFolder.Length;

            // Take a snapshot of the file system.  
            DirectoryInfo dir = new DirectoryInfo(startFolder);

            // This method assumes that the application has discovery permissions  
            // for all folders under the specified path.  
            IEnumerable<FileInfo> fileList = dir.GetFiles("*.zip", SearchOption.AllDirectories);
            textBox2.Text = fileList.Count().ToString();
            numericUpDown1.Value = 25;
            this.TextBoxOutput.Text = this.TextBoxOutput.Text + "Total archivos: " + fileList.Count().ToString() + Environment.NewLine;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e) {
            this.label24.Text = "Estimate: " + Math.Ceiling((new decimal(int.Parse(textBox2.Text)) / (int)numericUpDown1.Value)).ToString();
        }

        private void CrearZIP() {
            List<string> resulta = new List<string>();
            this.TextBoxOutput.Text = string.Empty;
            // Take a snapshot of the file system.  
            DirectoryInfo dir = new DirectoryInfo(this.textBox1.Text);
            IEnumerable<FileInfo> fileList = dir.GetFiles("*.zip", SearchOption.AllDirectories);
            var r = _30s.Chunk(fileList, (int)numericUpDown1.Value);
            this.TextBoxOutput.Text = string.Empty;
            int numgrup = 0;
            foreach (var item in r) {
                numgrup++;
                var archivo = @"C:\Jaeger\Jaeger.Log\log_" + DateTime.Now.ToString("ddMMyyhhmmss") + "_" + numgrup.ToString("00") + ".zip";
                this.TextBoxOutput.Text = this.TextBoxOutput.Text + archivo + Environment.NewLine;
                using (ZipFile zip = new ZipFile()) {
                    foreach (var f in item) {
                        this.TextBoxOutput.Text = this.TextBoxOutput.Text + f.FullName + Environment.NewLine;
                        ZipEntry e = zip.AddEntry(f.Name, Jaeger.Util.Helpers.HelperFiles.ReadFileByte(f.FullName));
                    }
                    zip.Comment = "create by echo_delta";
                    zip.Save(archivo);
                    resulta.Add(archivo);
                }
            }
            this.TextBoxOutput.Text = this.TextBoxOutput.Text + "Total archivos: " + fileList.Count().ToString() + Environment.NewLine;
            this.TextBoxOutput.Text = this.TextBoxOutput.Text + "Total grupos: " + r.Count().ToString() + Environment.NewLine;
            this.ButtonCreateZIP.Tag = resulta;
        }

        private void ButtonCreateZIP_Click(object sender, EventArgs e) {
            using(var espera = new WaitingForm(this.CrearZIP)) {
                espera.Text = "Creando...";
                espera.ShowDialog(this);
            }

            List<string> resulta = this.ButtonCreateZIP.Tag as List<string>;
            if (resulta != null) {
                for (int i = 0; i < resulta.Count; i++) {
                    this.TextBoxOutput.Text = this.TextBoxOutput.Text + this.editaS3.Upload(resulta[i], Path.GetFileName(resulta[i]), "", this.textBox3.Text, this.TextBoxRegion.Text, false) + Environment.NewLine;
                }
            }
        }

        private void ButtonUnzipSource_Click(object sender,EventArgs e) {
            var openFolder = new FolderBrowserDialog() { Description = "Selecciona la carpeta de origen" };

            if (openFolder.ShowDialog(this) != DialogResult.OK)
                return;

            this.TextBoxUnzipSourceFolder.Text = openFolder.SelectedPath;
        }

        private void ButtonUnzipTarget_Click(object sender,EventArgs e) {
            var openFolder = new FolderBrowserDialog() { Description = "Selecciona la carpeta destino" };

            if (openFolder.ShowDialog(this) != DialogResult.OK)
                return;
            this.TextBoxUnzipTargetFolder.Text = openFolder.SelectedPath;
        }

        private void ButtonUnZipStart_Click(object sender,EventArgs e) {
            AgruparLogs();
            return;
            using (var espera = new WaitingForm(this.UnzipFolder)) {
                espera.Text = "Procesando...";
                espera.ShowDialog(this);
            }
        }

        private void UnzipFolder() {
            if (Directory.Exists(this.TextBoxUnzipTargetFolder.Text) == false) {
                Directory.CreateDirectory(this.TextBoxUnzipTargetFolder.Text);
            }

            DirectoryInfo dir = new DirectoryInfo(this.TextBoxUnzipSourceFolder.Text);
            IEnumerable<FileInfo> fileList = dir.GetFiles("*.zip",SearchOption.TopDirectoryOnly);
            foreach (var item in fileList) {
                var options = new ReadOptions { StatusMessageWriter = System.Console.Out };
                using (ZipFile zip = ZipFile.Read(item.FullName,options)) {
                    zip.ExtractAll(this.TextBoxUnzipTargetFolder.Text,ExtractExistingFileAction.OverwriteSilently);
                }
            }
        }

        private void AgruparLogs() {

            // Take a snapshot of the file system.  
            string startFolder = @"D:\bitbucket\Log_Revisar";

            // Used in WriteLine to trim output lines.  
            int trimLength = startFolder.Length;

            var dir = new DirectoryInfo(startFolder);
            IEnumerable<FileInfo> fileList = dir.GetFiles("*.json",SearchOption.TopDirectoryOnly);

            // Create the query.  
            var queryGroupByExt =
                from file in fileList
                group file by file.Name.Substring(12,4) into fileGroup
                orderby fileGroup.Key
                select fileGroup;

            // Iterate through the outer collection of groups.  
            foreach (var filegroup in queryGroupByExt) {
                Console.WriteLine(filegroup.Key == string.Empty ? "[none]" : filegroup.Key);
                var directorio = @"D:\bitbucket\Log_Revisar\" + filegroup.Key;
                Directory.CreateDirectory(directorio);
                foreach (var f in filegroup) {
                    Console.WriteLine("\t{0}",f.FullName.Substring(trimLength));
                    f.MoveTo(directorio + "\\" + f.Name);
                }
            }
        }
    }

    /// <summary>
    /// clase estatica para crear sublistas de una lista
    /// </summary>
    public static partial class _30s {
        public static List<List<T>> Chunk<T>(IEnumerable<T> data, int size) {
            return data
              .Select((x, i) => new { Index = i, Value = x })
              .GroupBy(x => x.Index / size)
              .Select(x => x.Select(v => v.Value).ToList())
              .ToList();
        }
    }
}
