﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jaeger.UI.Forms {
    public partial class WaitingForm : Form {
        public Action Worker { get; set; }

        public WaitingForm(Action worker) {
            InitializeComponent();
            if (worker == null)
                throw new ArgumentOutOfRangeException();
            this.Worker = worker;
            this.Text = "";
        }

        private void ViewWaitingForm_Load(object sender, EventArgs e) {
            if (this.Text != "")
                this.label.Text = this.Text;
            this.progressBar.Style = ProgressBarStyle.Marquee;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            Task.Factory.StartNew(this.Worker).ContinueWith(it => { this.Close(); }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
