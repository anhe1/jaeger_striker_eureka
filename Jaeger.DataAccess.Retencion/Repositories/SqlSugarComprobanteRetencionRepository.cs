﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Retencion.Contracts;
using Jaeger.Domain.Retencion.Entities;
using Jaeger.Domain.Retencion.ValueObjects;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarComprobanteRetencionRepository : MySqlSugarContext<ComprobanteRetencionModel>, IComprobanteRetencionRepository {

        public SqlSugarComprobanteRetencionRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public ComprobanteRetencionDetailModel GetComprobante(int index) {
            return this.Db.Queryable<ComprobanteRetencionDetailModel>().Where(it => it.Id == index).Single();
        }

        public ComprobanteRetencionDetailModel Save(ComprobanteRetencionDetailModel item) {
            if (item.Id == 0) {
                item.Creo = this.User;
                item.FechaNuevo = DateTime.Now;
                var result = this.Db.Insertable(item);
                item.Id = this.Execute(result);
            } else {
                item.Modifica = this.User;
                item.FechaModifica = DateTime.Now;
                var result = this.Db.Updateable<ComprobanteRetencionDetailModel>(item);
                this.Execute(result);
            }
            return item;
        }

        public IEnumerable<RetencionSingle> ComprobanteSingles(CFDISubTypeEnum subTipo, int year, int month) {
            return this.Db.Queryable<RetencionSingle>().Where(it => it.FechaEmision.Year == year).WhereIF(month > 0, it => it.FechaEmision.Month == month).ToList();
        }

        public IEnumerable<RetencionSingle> GetSearch(int folio) {
            return this.Db.Queryable<RetencionSingle>().WhereIF(folio > 0, it => it.Folio == folio.ToString()).ToList();
        }

        public bool Create() {
            try {
                this.Db.CodeFirst.InitTables<ComprobanteRetencionModel>();
                return true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool UpdateUrlXml(int index, string url) {
            return this.Db.Updateable<ComprobanteRetencionModel>()
                .SetColumns(it => new ComprobanteRetencionModel() {
                    FileXML = url
                }).Where(it => it.Id == index).ExecuteCommand() > 0;
        }

        public bool UpdateUrlPdf(int index, string url) {
            return this.Db.Updateable<ComprobanteRetencionModel>()
                .SetColumns(it => new ComprobanteRetencionModel() {
                    FilePDF = url
                }).Where(it => it.Id == index).ExecuteCommand() > 0;
        }

        public bool UpdateUrlXmlAcuse(int index, string url) {
            return this.Db.Updateable<ComprobanteRetencionModel>()
                .SetColumns(it => new ComprobanteRetencionModel() {
                    FileAccuseXML = url
                }).Where(it => it.Id == index).ExecuteCommand() > 0;
        }

        public bool UpdateUrlPdfAcuse(int index, string url) {
            return this.Db.Updateable<ComprobanteRetencionModel>()
                .SetColumns(it => new ComprobanteRetencionModel() {
                    FileAccusePDF = url
                }).Where(it => it.Id == index).ExecuteCommand() > 0;
        }

        public bool UpdateEstado(string idDocumento, string estado) {
            var result = this.Db.Updateable<ComprobanteRetencionModel>()
                .SetColumns(it => new ComprobanteRetencionModel {
                    Estado = estado,
                    FechaEstado = DateTime.Now
                }).Where(it => it.IdDocumento == idDocumento);
            return this.Execute(result) > 0;
        }

        public bool Update(int indice, string status) {
            var result = this.Db.Updateable<ComprobanteRetencionModel>()
                .SetColumns(it => new ComprobanteRetencionModel {
                Status = status,
                Modifica = this.User,
                FechaModifica = DateTime.Now,
                FechaEntrega = DateTime.Now
            })
               .Where(it => it.Id == indice);
            return this.Execute(result) > 0;
        }
    }
}
