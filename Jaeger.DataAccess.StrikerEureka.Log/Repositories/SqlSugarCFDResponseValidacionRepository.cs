﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.StrikerEureka.Log.Contracts;
using Jaeger.Domain.StrikerEureka.Log.Entities;

namespace Jaeger.DataAccess.StrikerEureka.Log.Repositories {
    public class SqlSugarCFDResponseValidacionRepository : SQLiteContext<CFDResponseValidacionModel>, ISqlCFDResponseValidacionRepository {
        public SqlSugarCFDResponseValidacionRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public void CrearRepositorio() {
            //if (this.CreateDB()) {
            //    this.Create();
            //}
        }

        public IEnumerable<CFDResponseValidacionModel> GetList(string rfc, string comprobado = "", int cantidad = 100) {
            return this.Db.Queryable<CFDResponseValidacionModel>().Where(it => it.ReceptorRFC == rfc).WhereIF(comprobado != "", it => it.Comprobado == comprobado).Take(cantidad).ToList();
        }

        public CFDResponseValidacionModel Save(CFDResponseValidacionModel item) {
            try {
                item = this.Db.Saveable(item).ExecuteReturnEntity();
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return item;
        }
    }
}
