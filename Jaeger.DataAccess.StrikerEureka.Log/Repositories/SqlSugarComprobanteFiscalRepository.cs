﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Entities.Complemento;
using Jaeger.Domain.Services;
using Jaeger.Domain.StrikerEureka.Log.Contracts;

namespace Jaeger.DataAccess.StrikerEureka.Log.Repositories {
    public class SqlSugarComprobanteFiscalRepository : SqlSugarContext<ComprobanteFiscalModel>, ISqlComprobanteFiscalRepository {
        public SqlSugarComprobanteFiscalRepository(Jaeger.Domain.Empresa.Entities.DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        /// <summary>
        /// obtener indice a partir del folio fiscal (UUID)
        /// </summary>
        /// <param name="uuid">Folio Fiscal(UUID)</param>
        /// <returns>int</returns>
        public int ReturnId(string uuid) {
            int indice = 0;
            try {
                indice = this.Db.Queryable<ComprobanteFiscalModel>().Where(it => it.IdDocumento == uuid.ToUpper()).Select(it => it.Id).Single();
            } catch (SqlSugarException ex) {
                Console.WriteLine(ex.Message);
                indice = this.Db.Queryable<ComprobanteFiscalModel>().Where(it => it.IdDocumento == uuid.ToUpper()).Select(it => it.Id).First();
            }
            return indice;
        }

        /// <summary>
        /// obtener lista de objetos
        /// </summary>
        /// <param name="uuids"></param>
        /// <returns></returns>
        public IEnumerable<ComprobanteFiscalModel> GetList(string[] uuids) {
            List<string> lista = uuids.OfType<string>().ToList();
            return this.Db.Queryable<ComprobanteFiscalModel>().Where(it => lista.Contains(it.IdDocumento)).ToList();
        }

        /// <summary>
        /// almacenar comprobante fiscal
        /// </summary>
        public ComprobanteFiscalDetailModel Save(ComprobanteFiscalDetailModel item) {
            if (item.Id == 0) {
                // si es un comprobante emitido creamos el nuevo folio a partir de la serie
                //if (item.SubTipo == EnumCfdiSubType.Emitido)
                //    item.Folio = this.GetFolio(item.Emisor.RFC, item.Serie).ToString();
                item.Id = this.Insert(item);
            } else {
                this.Update(item);
            }

            if (item.Conceptos.Count > 0) {
                for (int i = 0; i < item.Conceptos.Count; i++) {
                    if (item.Conceptos[i].Id == 0) {
                        item.Conceptos[i].SubId = item.Id;
                        item.Conceptos[i].FechaNuevo = item.FechaNuevo;
                        item.Conceptos[i].Creo = item.Creo;
                        item.Conceptos[i].Id = this.Db.Insertable<ComprobanteConceptoModel>(item.Conceptos[i]).ExecuteReturnIdentity();
                    } else {
                        item.Conceptos[i].Modifica = item.Modifica;
                        item.Conceptos[i].FechaModifica = DateTime.Now;
                        this.Db.Updateable<ComprobanteConceptoModel>(item.Conceptos[i]).ExecuteCommand();
                    }
                }
            }

            if (item.TimbreFiscal != null) {
                // si contiene un complemento de pagos
                if (item.ComplementoPagos != null)
                    this.AplicarComprobantePagos(item.ComplementoPagos);

                // cfdi relacionados
                if (item.CfdiRelacionados != null)
                    if (item.CfdiRelacionados.CfdiRelacionado != null)
                        this.AplicarCFDIRelacionados(item.CfdiRelacionados);
            }

            return item;
        }

        /// <summary>
        /// actualizar objeto Complemento de pagos, con las partidas de los comprobantes relacionados
        /// </summary>
        public bool AplicarComprobantePagos(ComplementoPagos objeto) {
            if (objeto != null) {
                if (objeto.Pago != null) {
                    // recorremos la lista de objetos de pago
                    foreach (ComplementoPagosPago pago in objeto.Pago) {
                        // recorremos la lista de documentos relacionados
                        if (pago.DoctoRelacionado != null) {
                            foreach (ComplementoPagoDoctoRelacionado docto in pago.DoctoRelacionado) {
                                List<ComplementoPagoDoctoRelacionado> tabla = this.Rep(docto.IdDocumento);
                                decimal suma = docto.ImpPagado;
                                if (tabla != null) {
                                    if (tabla.Count > 0) {
                                        suma = tabla.Where(p => ValidacionService.UUID(p.IdDocumento)).Sum(p => p.ImpPagado);
                                    }
                                }
                                this.Db.Updateable<ComprobanteFiscalModel>().SetColumns(it => new ComprobanteFiscalModel() { Acumulado = suma, NumParcialidad = docto.NumParcialidad, FechaRecepcionPago = pago.FechaPago }).Where(it => it.IdDocumento == docto.IdDocumento).ExecuteCommand();
                            }
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// aplicar reglas a los cfdi relacionados del comprobante fiscal
        /// </summary>
        public bool AplicarCFDIRelacionados(ComprobanteCfdiRelacionados objeto) {
            if (objeto != null) {
                // para la clave de notas de credito relacionadas
                if (objeto.TipoRelacion.Clave == "01" | objeto.TipoRelacion.Clave == "02" | objeto.TipoRelacion.Clave == "03") {
                    foreach (ComprobanteCfdiRelacionadosCfdiRelacionado item in objeto.CfdiRelacionado) {
                        decimal suma = 0;
                        List<ComplementoDoctoRelacionado> notas = this.Notas(item.IdDocumento);
                        if (notas != null) {
                            if (notas.Count > 0) {
                                suma = notas.Where(p => ValidacionService.UUID(p.IdDocumento)).Sum(p => p.Total);
                            }
                        }
                        //Entities.Base.ComprobanteGeneral d = this.GetComprobanteGeneral(item.IdDocumento);
                        string CommandText = "update _cfdi set _cfdi_dscntc=@descuento where _cfdi_uuid=@index";
                        return this.Db.Ado.ExecuteCommand(CommandText, new List<SugarParameter>()
                        {
                            new SugarParameter("@descuento", suma),
                            new SugarParameter("@@index", item.IdDocumento)
                        }) > 0;

                    }
                }
            }
            return false;
        }

        /// <summary>
        /// obtener lista de comprobantes fiscales relacionados (Recibo Electronico de Pago)
        /// </summary>
        public List<ComplementoPagoDoctoRelacionado> Rep(string idDocumento) {
            List<ComprobanteFiscalModel> lista = this.Db.Queryable<ComprobanteFiscalModel>().Where(it => it.JComplementoPagos.Contains(idDocumento)).ToList();
            List<ComplementoPagoDoctoRelacionado> lista1 = new List<ComplementoPagoDoctoRelacionado>();
            foreach (ComprobanteFiscalModel item in lista) {
                if (item.ComplementoPagos != null) {
                    foreach (ComplementoPagosPago pagosPago in item.ComplementoPagos.Pago) {
                        List<ComplementoPagoDoctoRelacionado> doctos = pagosPago.DoctoRelacionado.Where(p => p.IdDocumento == idDocumento).ToList();
                        if (doctos != null) {
                            foreach (ComplementoPagoDoctoRelacionado docto in doctos) {
                                lista1.Add(docto);
                            }
                        }
                    }
                }
            }
            return lista1;
        }

        /// <summary>
        /// obtener notas de credito relacionados al comprobante fiscal
        /// </summary>
        public List<ComplementoDoctoRelacionado> Notas(string folioFiscal) {
            if (ValidacionService.UUID(folioFiscal)) {
                string CommandText = string.Concat("select _cfdi_doc_id, _cfdi_estado,_cfdi_folio, _cfdi_serie, _cfdi_uuid,_cfdi_rfce,_cfdi_rfcr,_cfdi_nome,_cfdi_nomr, _cfdi_fecems, _cfdi_comrel, _cfdi_total, _cfdi_moneda,_cfdi_frmpg,_cfdi_mtdpg,_cfdi_par,_cfdi_estado,(_cfdi_total - (_cfdi_cbrd + _cfdi_dscntc)) as _cfdi_saldo ",
                        "from _cfdi where _cfdi_comrel like '%buscar%'");
                CommandText = CommandText.Replace("buscar", folioFiscal);
                DataTable tabla = this.Db.Ado.GetDataTable(CommandText);
                List<ComplementoDoctoRelacionado> lista = new List<ComplementoDoctoRelacionado>();
                foreach (DataRow fila in tabla.Rows) {
                    ComprobanteCfdiRelacionados t = ComprobanteCfdiRelacionados.Json(DbConvert.ConvertString(fila["_cfdi_comrel"]));
                    if (t != null) {
                        ComplementoDoctoRelacionado docto = new ComplementoDoctoRelacionado();
                        docto.TipoRelacion.Clave = t.TipoRelacion.Clave;
                        docto.TipoRelacion.Descripcion = t.TipoRelacion.Descripcion;
                        docto.SubTipoText = DbConvert.ConvertString(fila["_cfdi_doc_id"]);
                        docto.Folio = DbConvert.ConvertString(fila["_cfdi_folio"]);
                        docto.Serie = DbConvert.ConvertString(fila["_cfdi_serie"]);
                        docto.IdDocumento = DbConvert.ConvertString(fila["_cfdi_uuid"]);
                        docto.FechaEmision = DbConvert.ConvertDateTime(fila["_cfdi_fecems"]);
                        docto.Total = DbConvert.ConvertDecimal(fila["_cfdi_total"]);
                        docto.EstadoSAT = DbConvert.ConvertString(fila["_cfdi_estado"]);
                        docto.Moneda = DbConvert.ConvertString(fila["_cfdi_moneda"]);
                        docto.FormaPago = DbConvert.ConvertString(fila["_cfdi_frmpg"]);
                        docto.MetodoPago = DbConvert.ConvertString(fila["_cfdi_mtdpg"]);
                        docto.EstadoSAT = DbConvert.ConvertString(fila["_cfdi_estado"]);
                        if (docto.SubTipo == Domain.Comprobante.ValueObjects.CFDISubTipoEnum.Emitido) {
                            docto.RFC = DbConvert.ConvertString(fila["_cfdi_rfcr"]);
                            docto.Nombre = DbConvert.ConvertString(fila["_cfdi_nomr"]);
                        } else if (docto.SubTipo == Domain.Comprobante.ValueObjects.CFDISubTipoEnum.Recibido) {
                            docto.RFC = DbConvert.ConvertString(fila["_cfdi_rfce"]);
                            docto.Nombre = DbConvert.ConvertString(fila["_cfdi_nome"]);
                        }
                        lista.Add(docto);
                    }
                }
                return lista;
            }
            return null;
        }

        /// <summary>
        /// actualizar url del archivo XML del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">direccion URL</param>
        /// <returns>verdadero si la acción se realiza con exito</returns>
        public bool UpdateUrlXml(int index, string url) {
            return this.Db.Updateable<ComprobanteFiscalModel>()
                .SetColumns(it => new ComprobanteFiscalModel() {
                    UrlFileXML = url
                }).Where(it => it.Id == index).ExecuteCommand() > 0;
        }

        /// <summary>
        /// actualizar url del archivo XML del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">direccion URL</param>
        /// <returns>verdadero si la acción se realiza con exito</returns>
        public bool UpdateUrlPdf(int index, string url) {
            return this.Db.Updateable<ComprobanteFiscalModel>()
                .SetColumns(it => new ComprobanteFiscalModel() {
                    UrlFilePDF = url
                }).Where(it => it.Id == index).ExecuteCommand() > 0;
        }
    }
}
