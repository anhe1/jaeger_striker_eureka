﻿using Jaeger.Domain.Empresa.Entities;
using System;
using System.IO;

namespace Jaeger.Aplication.Comprobantes {
    /// <summary>
    /// Servicio de almacenamiento AWS S3
    /// </summary>
    public class EditaBucketService : IEditaBucketService {
        protected Amazon.S3.Contracts.ISimpleStorageService s3;
        protected Amazon.S3.Helpers.FileContentType contentType;
        private SimpleStorageService configuracion;

        public EditaBucketService(SimpleStorageService configuracion, bool produccion) {
            this.produccion = produccion;
            this.configuracion = configuracion;
            this.s3 = new Amazon.S3.Helpers.SimpleStorageServiceAWS3(configuracion.AccessKeyId, configuracion.SecretAccessKey, configuracion.Region);
            this.contentType = new Amazon.S3.Helpers.FileContentType();
        }

        public bool produccion { get; set; }

        public bool Download(string localFileName, string requestURL) {
            return this.s3.Download(requestURL, localFileName);
        }

        public bool Exists(string requestURL) {
            return this.s3.Exists(this.configuracion.BucketName, this.CreateURL( Path.GetFileName(requestURL)));
        }

        public string Upload(string localFileName, string keyName) {
            var response = "";
            using (var fileToUpload = new FileStream(localFileName, FileMode.Open, FileAccess.Read)) {
                response = this.s3.Upload(fileToUpload, localFileName, this.CreateURL(keyName),this.contentType.GetContentType(localFileName), this.configuracion.BucketName, this.configuracion.Region, true);
            }
            return response;
        }

        public string Upload(string base64, string localFileName, string keyName) {
            var response = string.Empty;
            byte[] numArray = Convert.FromBase64String(base64);
            MemoryStream stream = new MemoryStream(numArray);
            using (var fileToUpload = new MemoryStream(numArray)) {
                response = this.s3.Upload(fileToUpload, localFileName, this.CreateURL(keyName), this.contentType.GetContentType(localFileName), this.configuracion.BucketName, this.configuracion.Region, true);
            }
            return response;
        }

        private string CreateURL(string keyName) {
            if (this.produccion) {
                if (this.configuracion.Folder != "")
                    return string.Format("{0}/{1}", this.configuracion.Folder, keyName);
            }
            else {
                return string.Format("{0}/{1}", "sandbox", keyName);
            }
            return keyName;
        }

        private string BucketFolder() {
            var bucketFolder = this.configuracion.BucketName;
            if (this.configuracion.Folder != "")
                bucketFolder = this.configuracion.BucketName + "/" + this.configuracion.Folder;
            return bucketFolder;
        }
    }
}
