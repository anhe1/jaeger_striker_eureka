﻿namespace Jaeger.Aplication.Comprobantes {
    /// <summary>
    /// contrato para el servicio de almacenamiento AWS S3
    /// </summary>
    public interface IEditaBucketService {
        /// <summary>
        /// modo de prueba
        /// </summary>
        bool produccion { get; set; }

        string Upload(string localFileName, string keyName);

        string Upload(string base64, string localFileName, string keyName);

        bool Download(string localFileName, string requestURL);

        bool Exists(string requestURL);
    }
}
