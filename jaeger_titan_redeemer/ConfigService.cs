﻿/// develop: anhe 10092019 1230
/// purpose clase statica para contener la configuracion basica del sistema
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Edita.V2.Perfil;

namespace Jaeger.Aplication {
    public class HelperConfig {
        public static bool IsDeveloper = false;
        public static ViewModelKaiju Piloto;
        public static PerfilGeneral PerfilPiloto;
        public static PerfilGeneral GeneralPerfil = new PerfilGeneral();
        public static Configuracion OSynapsis;

        static HelperConfig() {
            Piloto = new ViewModelKaiju();
            PerfilPiloto = new PerfilGeneral();
        }
    }
}
