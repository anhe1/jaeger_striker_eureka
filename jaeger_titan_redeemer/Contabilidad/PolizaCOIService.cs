﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Jaeger.Edita.V2.Nomina.Entities;

namespace Jaeger.Aplication.Contabilidad {
    /// <summary>
    /// servicio para exportacion de poliza tipo COI ver80 o 90
    /// </summary>
    public class PolizaCOIService : IPolizaCOIService {
        public PolizaCOIService() {

        }

        /// <summary>
        /// crear poliza tipo coi para pago de incentivos a vendedores de la nomina
        /// </summary>
        /// <param name="concepto">concepto de la poliza</param>
        /// <param name="datos">Lista dee comprobantes</param>
        /// <param name="localFileBeneficiarios">archivo layout de beneficiarios</param>
        /// <returns>Objeto Poliza Layout</returns>
        public Layout.COI.PolizaDetail PolizaIncentivos (string concepto, List<NominaViewSingle> datos, string localFileBeneficiarios) {
            var d = new Layout.Helpers.LayoutExcelBeneficiario();
            d.InfoFile = new FileInfo(localFileBeneficiarios);
            d.Reader();
            var beneficiarios = d.Reader("Importar");
            var poliza = new Layout.COI.PolizaDetail();

            poliza.TIPO_POLI = "Eg";
            poliza.CONCEP_PO = concepto;

            foreach (var item in datos) {
                var buscar = beneficiarios.First(it => it.RFC == item.ReceptorRFC);
                if (buscar != null) {
                    // primer auxiliar o cuenta
                    var a1 = new Layout.COI.AuxiliarDetail {
                        NUM_CTA = buscar.CuentaContable,
                        TIPO_POLI = "Eg",
                        CONCEP_PO = buscar.Nombre,
                        MONTOMOV = item.Total,
                        DEBE_HABER = "D"
                    };
                    // comprobante
                    var c1 = new Layout.COI.ComprobanteFiscal {
                        FECHA = item.FechaPago,
                        FOLIO = item.Folio,
                        MONTO = item.Total,
                        SERIE = item.Serie,
                        VERSIONCFDI = item.Version,
                        UUIDTIMBRE = item.IdDocumento,
                        RFCRECEPTOR = item.ReceptorRFC,
                        RFCEMISOR = item.EmisorRFC
                    };
                    a1.Comprobantes.Add(c1);
                    // detalles del segundo auxiliar o cuenta
                    var a2 = new Layout.COI.AuxiliarDetail {
                        NUM_CTA = buscar.CuentaContable,
                        TIPO_POLI = "Eg",
                        CONCEP_PO = buscar.Nombre,
                        MONTOMOV = item.Total,
                        DEBE_HABER = "H"
                    };
                    // informacion del pago
                    a2.FormaPago = new Layout.COI.INFADIPAR {
                        FECHA = (DateTime)item.FechaPago,
                        BENEF = buscar.Nombre,
                        MONTO = item.Total, 
                        BANCO = int.Parse(buscar.ClaveBancoOrigen),
                        CTAORIG = buscar.CuentaBancoOrigen,
                        BANCODEST = int.Parse(buscar.ClaveBanco),
                        CTADEST = buscar.NumCuenta,
                        ClaveFormaPago = buscar.ClaveFormaPago,
                        RFC = buscar.RFC, 
                    };
                    
                    a2.Comprobantes.Add(c1);
                    poliza.Auxiliar.Add(a1);
                    poliza.Auxiliar.Add(a2);

                }
            }

            return poliza;
        }

        public Layout.COI.PolizaDetail PolizaAsimilados(string concepto, List<NominaViewSingle> datos, string rfc) {
            var poliza = new Layout.COI.PolizaDetail();

            poliza.TIPO_POLI = "Dr";
            poliza.CONCEP_PO = concepto;

            var a1 = new Layout.COI.AuxiliarDetail {
                NUM_CTA = "5101-0001-0001-0000",
                TIPO_POLI = "Dr",
                CONCEP_PO = concepto,
                MONTOMOV = datos.Sum(it => it.SubTotal),
                DEBE_HABER = "D"
            };

            foreach (var item in datos) {
                // comprobante
                var c1 = new Layout.COI.ComprobanteFiscal {
                    FECHA = item.FechaPago,
                    FOLIO = item.Folio,
                    MONTO = item.Total,
                    SERIE = item.Serie,
                    VERSIONCFDI = item.Version,
                    UUIDTIMBRE = item.IdDocumento,
                    RFCRECEPTOR = item.ReceptorRFC,
                    RFCEMISOR = item.EmisorRFC
                };
                a1.Comprobantes.Add(c1);
            }
            poliza.Auxiliar.Add(a1);

            foreach (var item in datos) {
                var a2 = new Layout.COI.AuxiliarDetail {
                    NUM_CTA = "5101-0001-0001-0000",
                    TIPO_POLI = "Dr",
                    CONCEP_PO = concepto,
                    MONTOMOV = item.Total,
                    DEBE_HABER = "H"
                };
                if (rfc == "SOC190205N7A") {
                    a2.NUM_CTA = "2102-0002-0003-0000";
                } else if (rfc == "AAM1901216I3") {
                    a2.NUM_CTA = "2102-0002-0003-0000";
                }

                var c2 = new Layout.COI.ComprobanteFiscal {
                    FECHA = item.FechaPago,
                    FOLIO = item.Folio,
                    MONTO = item.Total,
                    SERIE = item.Serie,
                    VERSIONCFDI = item.Version,
                    UUIDTIMBRE = item.IdDocumento,
                    RFCRECEPTOR = item.ReceptorRFC,
                    RFCEMISOR = item.EmisorRFC
                };
                a2.Comprobantes.Add(c2);
                poliza.Auxiliar.Add(a2);
            }
            return poliza;
        }
    }
}
