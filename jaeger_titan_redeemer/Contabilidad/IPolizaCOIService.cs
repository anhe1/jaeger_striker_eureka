﻿using System.Collections.Generic;
using Jaeger.Edita.V2.Nomina.Entities;

namespace Jaeger.Aplication.Contabilidad {
    /// <summary>
    /// servicio para exportacion de poliza tipo COI ver80 o 90
    /// </summary>
    public interface IPolizaCOIService {
        /// <summary>
        /// crear poliza tipo coi para pago de incentivos a vendedores de la nomina
        /// </summary>
        /// <param name="concepto">concepto de la poliza</param>
        /// <param name="datos">Lista dee comprobantes</param>
        /// <param name="localFileBeneficiarios">archivo layout de beneficiarios</param>
        /// <returns>Objeto Poliza Layout</returns>
        Layout.COI.PolizaDetail PolizaIncentivos(string concepto, List<NominaViewSingle> datos, string localFileBeneficiarios);
    }
}
