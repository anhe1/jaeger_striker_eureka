﻿using System;
using HtmlAgilityPack;
using Jaeger.SAT.Retencion.Interfaces;
using Jaeger.SAT.Retencion.Abstracts;

namespace Jaeger.SAT.Retencion {
    public class Service : ServiceBase, IService {
        public Service() : base() {

        }

        public IRetencionResponse Execute(IRetencionRequest request) {
            return this.Process(request);
        }

        protected override string GetRequest(string url) {
            try {
                var web = new HtmlWeb();
                var doc = web.Load(url);
                return doc.DocumentNode.OuterHtml.ToString();
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return string.Empty;
            }
        }
    }
}
