﻿using System;
using System.Net;
using Jaeger.SAT.Retencion.Interfaces;

namespace Jaeger.SAT.Retencion.Abstracts {
    public abstract class ServiceBase {
        #region declaraciones
        private readonly string _UrlService = "https://prodretencionverificacion.clouda.sat.gob.mx/Home/ConsultaRetencion{0}";
        #endregion

        public ServiceBase() {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }

        public string Version {
            get { return "1.0.4"; }
        }

        protected abstract string GetRequest(string url);

        protected IRetencionResponse Process(IRetencionRequest request) {
            var _response = new RetencionResponse(request) {
                IsError = false
            };

            try {
                string _urlService = string.Format(this._UrlService, request.GetElementoImpreso());
                var _tablaResultado = this.GetRequest(_urlService);
                // si encontramos id noresultados
                if (_tablaResultado.Contains("noresultados")) {
                    _response.Mensaje = Properties.Resources.msg_NoResultados;
                    _response.Existe = false;
                    return _response;
                }
                // procesar la tabla
                var htmlDataTable = HtmlToDatatableService.GetDataTable(_tablaResultado, "tbl_resultado");
                if (htmlDataTable == null || htmlDataTable.Rows.Count <= 0) {
                    _response.StatusComprobante = "Sin_Validar";
                    _response.Existe = false;
                    _response.StatusCancelacion = string.Empty;
                    _response.ProcesoCancelacion = string.Empty;
                    _response.Mensaje = string.Empty;
                    return _response;
                } else {
                    _response.Existe = true;
                    if (htmlDataTable.Rows[0]["Estado CFDI Retención"].ToString().ToLower() != "vigente") {
                        string str1 = htmlDataTable.Rows[0]["Fecha de Cancelación"].ToString();
                        _response.StatusComprobante = "Cancelado";
                        _response.FechaCancelacion = new DateTime?(DateTime.Parse(str1));
                    } else {
                        _response.StatusComprobante = "Vigente";
                    }
                    return _response;
                }
            } catch (Exception ex) {
                _response.IsError = true;
                _response.Existe = false;
                _response.StatusComprobante = "Sin Validar";
                _response.Mensaje = string.Concat("Falla en la conexión al servicio de SAT :", Environment.NewLine, ex.ToString());
            }
            return _response;
        }
    }
}
