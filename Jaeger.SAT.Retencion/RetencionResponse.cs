﻿using System;
using Jaeger.SAT.Retencion.Interfaces;

namespace Jaeger.SAT.Retencion {
    public class RetencionResponse : RetencionRequest, IRetencionResponse, IRetencionRequest {
        #region declaraciones
        private string _Mensaje;
        private bool _Existe;
        private string _StatusComprobante;
        private string _StatusCancelacion;
        private string _ProcesoCancelacion;
        private DateTime? _FechaCancelacion;
        private string _NombreRazonSocialEmisor;
        private string _NombreRazonSocialReceptor;
        private string _PACCertifico;
        private bool _IsError;
        #endregion

        public RetencionResponse() {
        }

        public RetencionResponse(string emisorRFC, string receptorRFC, string idDocumento) : base(emisorRFC, receptorRFC, idDocumento) {
        }

        public RetencionResponse(IRetencionRequest request) {
            this.EmisorRFC = request.EmisorRFC;
            this.ReceptorRFC = request.ReceptorRFC;
            this.IdDocumento = request.IdDocumento;
        }

        /// <summary>
        /// obtener o establecer si existe algun error al crear la peticion
        /// </summary>
        public bool IsError {
            get { return this._IsError; }
            set { this._IsError = value; }
        }

        /// <summary>
        /// obtener o establecer el mensaje de respuesta del servicio
        /// </summary>
        public string Mensaje {
            get { return _Mensaje; }
            set { _Mensaje = value; }
        }

        /// <summary>
        /// obtener o establecer si el comprobante existe 
        /// </summary>
        public bool Existe {
            get { return _Existe; }
            set { this._Existe = value; }
        }

        /// <summary>
        /// obtener o establecer el status del comprobante
        /// </summary>
        public string StatusComprobante {
            get { return _StatusComprobante; }
            set { this._StatusComprobante = value; }
        }

        /// <summary>
        /// obtener o establecer el status del proceso de cancelacion 
        /// </summary>
        public string StatusCancelacion {
            get { return _StatusCancelacion; }
            set { this._StatusCancelacion = value; }
        }

        /// <summary>
        /// obtener o establecer el status del proceso de cancelacion
        /// </summary>
        public string ProcesoCancelacion {
            get { return this._ProcesoCancelacion; }
            set { this._ProcesoCancelacion = value; }
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        public DateTime? FechaCancelacion {
            get {
                if (this._FechaCancelacion >= new DateTime(1989, 1, 1))
                    return this._FechaCancelacion;
                return null;
            }
            set { this._FechaCancelacion = value; }
        }

        /// <summary>
        /// obtener o establecer el nombre o razon social del emisor del comprobante
        /// </summary>
        public string NombreRazonSocialEmisor {
            get { return this._NombreRazonSocialEmisor; }
            set { this._NombreRazonSocialEmisor = value; }
        }

        /// <summary>
        /// obtener o establecer el nombre o razon social del receptor del comprobante
        /// </summary>
        public string NombreRazonSocialReceptor {
            get { return this._NombreRazonSocialReceptor; }
            set { this._NombreRazonSocialReceptor = value; }
        }

        /// <summary>
        /// obtener o establecer el RFC del proveedor de certificacion de comprobantes fiscales digitales que genera el timbre fiscal digital.
        /// </summary>
        public string PaCCertifico {
            get { return this._PACCertifico; }
            set { this._PACCertifico = value; }
        }

        public override string ToString() {
            return string.Concat("Mensaje: ", this.Mensaje, Environment.NewLine,
                "Existe: ", (this.Existe == true ? "Correcto" : "Inexistente"), Environment.NewLine,
                "Estado: ", this.StatusComprobante, Environment.NewLine,
                "Emisor: ", this.EmisorRFC, " ", this.NombreRazonSocialEmisor, Environment.NewLine,
                "Receptor: ", this.ReceptorRFC, " ", this.NombreRazonSocialReceptor, Environment.NewLine,
                "IdDocumento: ", this.IdDocumento, " ", this.NombreRazonSocialReceptor, Environment.NewLine);
        }
    }
}
