﻿using Jaeger.SAT.Retencion.Interfaces;

namespace Jaeger.SAT.Retencion.Build {
    public interface IRequestRetencionBuilder {
        IRequestRetencionReceptorBuilder WithEmisorRFC(string emisorRFC);
    }

    public interface IRequestRetencionReceptorBuilder {
        IRequestRetencionIdDocumentoBuilder WithReceptorRFC(string receptorRFC);
    }

    public interface IRequestRetencionIdDocumentoBuilder {
        IRequestBuilder WithIdDocumento(string idDocumento);
    }

    public interface IRequestBuilder {
        IRetencionRequest Build();
    }
}
