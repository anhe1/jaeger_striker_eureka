﻿using Jaeger.SAT.Retencion.Interfaces;

namespace Jaeger.SAT.Retencion.Build {
    public class RequestRetencionBuilder : IRequestRetencionBuilder, IRequestRetencionReceptorBuilder, IRequestRetencionIdDocumentoBuilder, IRequestBuilder {
        private IRetencionRequest request;

        internal RequestRetencionBuilder() {
            this.request = new RetencionRequest();
        }

        public IRequestRetencionReceptorBuilder WithEmisorRFC(string emisorRFC) {
            this.request.EmisorRFC = emisorRFC;
            return this;
        }

        public IRequestRetencionIdDocumentoBuilder WithReceptorRFC(string receptorRFC) {
            this.request.ReceptorRFC = receptorRFC;
            return this;
        }

        public IRequestBuilder WithIdDocumento(string idDocumento) {
            this.request.IdDocumento = idDocumento;
            return this;
        }

        public IRetencionRequest Build() {
            return this.request;
        }
    }
}
