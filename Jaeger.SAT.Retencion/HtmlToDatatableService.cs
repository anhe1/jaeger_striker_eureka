﻿using System;
using System.Collections.Generic;
using System.Data;
using HtmlAgilityPack;

namespace Jaeger.SAT.Retencion {
    public class HtmlToDatatableService {
        public HtmlToDatatableService() { }

        private static DataTable GetDataTable(HtmlDocument doc) {
            var headers = doc.DocumentNode.SelectNodes("//tr/th");
            var table = new DataTable();
            foreach (HtmlNode header in headers)
                table.Columns.Add(header.InnerText.Replace("\r", "").Replace("\n", "").Replace("&oacute;", "ó").Trim()); // create columns from th

            // select rows with td elements
            var columns = new List<string>();
            foreach (var row in doc.DocumentNode.SelectNodes("//tr/td"))
                columns.Add(row.InnerText.Replace("\r", "").Replace("\n", "").Trim());

            var dataRow = table.NewRow();
            int num = 0;
            if (columns.Count != table.Columns.Count) {
                throw new Exception(Properties.Resources.msg_NumColumnasError);
            }

            foreach (string str1 in columns) {
                int num1 = num;
                num = num1 + 1;
                dataRow[num1] = str1;
            }

            table.Rows.Add(dataRow);
            return table;
        }

        public static DataTable GetDataTable(string html, string tablename) {

            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(html);

            if (htmlDocument == null) {
                throw new Exception(Properties.Resources.msg_HtmlError);
            }

            var htmlTable = HtmlToDatatableService.GetHtmlTable(htmlDocument, tablename);
            if (htmlTable == null) {
                throw new Exception(Properties.Resources.String3);
            }
            return GetDataTable(htmlDocument);
        }

        public static HtmlNode GetHtmlTable(HtmlDocument doc, string tablename) {
            return doc.GetElementbyId(tablename);
        }
    }
}
