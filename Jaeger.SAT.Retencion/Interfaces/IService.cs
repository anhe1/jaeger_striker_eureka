﻿namespace Jaeger.SAT.Retencion.Interfaces {
    public interface IService {
        string Version { get; }

        IRetencionResponse Execute(IRetencionRequest request);
    }
}
