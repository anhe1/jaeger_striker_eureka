﻿using System;

namespace Jaeger.SAT.Retencion.Interfaces {
    public interface IRetencionResponse {
        /// <summary>
        /// obtener o establecer si existe algun error al crear la peticion
        /// </summary>
        bool IsError { get; set; }

        /// <summary>
        /// obtener o establecer el mensaje de respuesta del servicio
        /// </summary>
        string Mensaje { get; set; }

        /// <summary>
        /// obtener o establecer si el comprobante existe 
        /// </summary>
        bool Existe { get; set; }

        /// <summary>
        /// obtener o establecer el status del comprobante
        /// </summary>
        string StatusComprobante { get; set; }

        /// <summary>
        /// obtener o establecer el status del proceso de cancelacion 
        /// </summary>
        string StatusCancelacion { get; set; }

        /// <summary>
        /// obtener o establecer el status del proceso de cancelacion
        /// </summary>
        string ProcesoCancelacion { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        DateTime? FechaCancelacion { get; set; }

        /// <summary>
        /// obtener o establecer el nombre o razon social del emisor del comprobante
        /// </summary>
        string NombreRazonSocialEmisor { get; set; }

        /// <summary>
        /// obtener o establecer el nombre o razon social del receptor del comprobante
        /// </summary>
        string NombreRazonSocialReceptor { get; set; }

        /// <summary>
        /// obtener o establecer el RFC del proveedor de certificacion de comprobantes fiscales digitales que genera el timbre fiscal digital.
        /// </summary>
        string PaCCertifico { get; set; }
    }
}
