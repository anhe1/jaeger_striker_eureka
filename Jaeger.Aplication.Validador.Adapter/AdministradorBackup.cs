﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.Comprobante.Mappers;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Aplication.Validador.Adapter.Abstracts;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Aplication.Validador.Adapter {
    public class AdministradorBackup : AdministradorBackupService, IAdministradorBackup {
        public AdministradorBackup() {
            this.Comprobante = new ComprobantesFiscalesService();
            this.contribuyente = new Contribuyentes.Services.ContribuyenteService();
            this.extensions = new ComprobanteExtensions();
        }

        public IAdministradorBackup WitConfiguration(IConfiguration configuration) {
            this.Configuration = configuration;
            return this;
        }

        public IAdministradorBackup WitRFC(string rfc) {
            this._RFC = rfc;
            return this;
        }

        public IConfiguration Configuration { get; set; }

        public override BindingList<IDocumentoFiscal> Procesar(BindingList<IDocumentoFiscal> dataSource, IProgress<Progreso> progreso) {
            Stopwatch stopwatch = new Stopwatch();
            this.progreso = progreso;
            this.dataSource = dataSource;
            this.Verificar();

            if (this.Configuration.Registrar) {
                this.Registrar();
                this.Backup();
            }

            TimeSpan elapsed = stopwatch.Elapsed;
            var d1 = string.Format("{0:00}:{1:00}:{2:00}", elapsed.Hours, elapsed.Minutes, elapsed.Seconds);
            var reporte = new Progreso() { Caption = d1 };
            progreso.Report(reporte);
            return dataSource;
        }

        internal void Verificar() {
            var ids = dataSource.Select(it => it.IdDocumento).ToArray();
            this.Existentes = this.Comprobante.GetComprobante1(ids);
            var reporte = new Progreso() { Caption = "..." };
            for (int i = 0; i < dataSource.Count; i++) {
                var local = this.dataSource[i];
                reporte.Caption = local.IdDocumento;
                progreso.Report(reporte);
                ComprobanteFiscalDetailSingleModel single = null;

                try {
                    single = this.Existentes.Where(it => it.IdDocumento == local.IdDocumento).FirstOrDefault();
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                    single = null;
                }
                
                if (single != null) {
                    if (local.IdDocumento == single.IdDocumento) {
                        local.Registrado = true;
                        local.Id = single.Id;
                        // validar URL
                        if (ValidacionService.URL(single.UrlFileXML)) {
                            local.UrlXML = single.UrlFileXML;
                        }

                        if (ValidacionService.URL(single.UrlFilePDF)) {
                            local.UrlPDF = single.UrlFilePDF;
                        }
                    }
                }
            }
        }

        internal void Registrar() {
            for (int i = 0; i < this.dataSource.Count; i++) {
                var local = this.dataSource[i];
                this.Registrar(local);
            }
        }

        private void Registrar(IDocumentoFiscal documentoFiscal) {
            if (documentoFiscal.Id == 0 && documentoFiscal.Validacion.Valido == Domain.Base.ValueObjects.CFDIValidacionEnum.Valido) {
                ComprobanteFiscalDetailModel comun = null;
                if (documentoFiscal.Version == "3.3") {
                    var cfdi33 = SAT.CFDI.V33.Comprobante.LoadBase64(documentoFiscal.ContentXML);
                    comun = ComprobanteExtensions.ConvertTo(cfdi33);
                    comun.XML = cfdi33.OriginalXmlString;
                } else if (documentoFiscal.Version == "4.0") {
                    var cfdi40 = SAT.CFDI.V40.Comprobante.LoadBase64(documentoFiscal.ContentXML);
                    comun = ComprobanteExtensions.ConvertTo(cfdi40);
                    comun.XML = cfdi40.OriginalXmlString;
                }

                if (documentoFiscal.Validacion != null) {
                    comun.FechaValidacion = documentoFiscal.Validacion.FechaValidacion;
                    comun.JValidacion = documentoFiscal.Validacion.Json();
                    comun.Estado = documentoFiscal.Estado;
                    comun.Status = "Importado";
                    comun.Creo = ConfigService.Piloto.Clave;
                    comun.FechaEstado = DateTime.Now;
                }

                if (comun.EmisorRFC == this._RFC) {
                    comun.SubTipo = Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido;
                    if (documentoFiscal.TipoComprobante == "Nomina") {
                        comun.Receptor.Relacion = "Empleado";
                        comun.SubTipo = Domain.Base.ValueObjects.CFDISubTipoEnum.Nomina;
                    } else {
                        comun.Receptor.Relacion = "Proveedor";
                    }
                    comun.Receptor = this.WithDirectorio(comun.Receptor);
                } else if (comun.ReceptorRFC == this._RFC) {
                    var d2 = comun.Emisor;
                    d2.Relacion = "Proveedor";
                    d2 = this.WithDirectorio(d2);
                    comun.IdDirectorio = d2.IdDirectorio;
                    comun.SubTipo = Domain.Base.ValueObjects.CFDISubTipoEnum.Recibido;
                } else {
                    comun.SubTipo = Domain.Base.ValueObjects.CFDISubTipoEnum.None;
                }
                // complemento de pagos
                if (comun.RecepcionPago != null) {
                    comun.RecepcionPago = this.Comprobante.Verificar(comun.RecepcionPago);
                }

                if (comun.CfdiRelacionados != null) {
                    comun.CfdiRelacionados = this.Comprobante.Verificar(comun.CfdiRelacionados);
                }

                documentoFiscal.Id = this.Comprobante.Save(comun).Id;
            }
        }

        internal void Backup() {
            for (int i = 0; i < this.dataSource.Count; i++) {
                var local = this.dataSource[i];
                this.Backup(local);
            }
        }

        private void Backup(IDocumentoFiscal local) {
            if (local.Id > 0) {
                bool completed = false;
                if (ValidacionService.URL(local.UrlXML)) {
                    completed = this.Comprobante.Exits(local.UrlXML);
                }
                
                if (completed == false) {
                    local.UrlXML = this.Comprobante.Upload(local.ContentXML, local.PathXML, local.KeyName + ".xml");
                    this.Comprobante.UpdateUrlXml(local.Id, local.UrlXML);
                }

                completed = false;
                if (ValidacionService.URL(local.UrlPDF)) {
                    completed = this.Comprobante.Exits(local.UrlPDF);
                }

                if (completed == false) {
                    if (ValidacionService.IsBase64String(local.ContentPDF)) {
                        local.UrlPDF = this.Comprobante.Upload(local.ContentPDF, local.PathPDF, local.KeyName + ".pdf");
                        this.Comprobante.UpdateUrlPdf(local.Id, local.UrlPDF);
                    }
                }
            }
        }
    }
}
