﻿using System;
using SqlSugar;
using System.ComponentModel;
using Jaeger.Edita.V2.Almacen.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    /// <summary>
    /// clase para el manejo del catalogo de clasificaciones
    /// </summary>
    public class SqlSugarClasificacion : MySqlSugarContext<ViewModelClasificacionSingle>
    {
        public MySqlClasificacion Data2 { get; set; }
        public SqlSugarClasificacion(DataBaseConfiguracion configuracion) : base(configuracion)
        {
            this.Data2 = new MySqlClasificacion(configuracion);
        }

        /// <summary>
        /// catalogo de categorias de almacen
        /// </summary>
        /// <returns>BindingList<ViewItemCategoria></returns>
        public BindingList<ViewModelCategoriaSingle> GetCategorias()
        {
            return new BindingList<ViewModelCategoriaSingle>(this.Db.Queryable<ViewModelClasificacionSingle, ViewModelClasificacionSingle, ViewModelClasificacionSingle>((c1, c2, c3) => new JoinQueryInfos(
                JoinType.Inner, c2.SubId == c1.Id,
                JoinType.Inner, c3.SubId == c2.Id
            )).Select((c1, c2, c3) => new ViewModelCategoriaSingle
            {
                Id = c3.Id,
                SubId = c2.Id,
                Almacen = c1.Almacen,
                Tipo = SqlFunc.IF(c1.Descripcion == "" && c2.Descripcion == "").Return(c3.Descripcion).ElseIF(c1.Descripcion == "").Return(c2.Descripcion).End(c1.Descripcion),
                Clase = SqlFunc.IIF(c2.Descripcion == "", c3.Descripcion, c2.Descripcion),
                Descripcion = c3.Descripcion,
                Clave = c3.Clave
            }).ToList());
        }
    }
}
