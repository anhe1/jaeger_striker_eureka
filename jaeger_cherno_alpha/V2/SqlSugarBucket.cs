﻿using System.Data;
using Jaeger.Edita.Entities.Amazon;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class SqlSugarBucket : MySqlSugarContext<ViewModelBucketContent>
    {
        public SqlSugarBucket(DataBaseConfiguracion configuracion) : base(configuracion)
        {
        }

        public ViewModelBucketContent GetByKeyName(string keyname)
        {
            return this.Db.Queryable<ViewModelBucketContent>().Where(it => it.KeyName == keyname).First();
        }

        public DataTable GetBuckets(string uuid)
        {
            return this.Db.Queryable<ViewModelBucketContent>().Where(it => it.Uuid == uuid).ToDataTable();
        }
    }
}
