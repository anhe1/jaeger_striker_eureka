﻿// develop: 010220180144
// purpose: obtener información de los certificados almacenados.
using Jaeger.Edita.Entities.Basico;
using Jaeger.Edita.Interfaces;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class SqlSugarCertificado : MySqlSugarContext<ViewModelCertificado>, ISqlCertificado
    {
        public SqlSugarCertificado(DataBaseConfiguracion configuracion) : base(configuracion)
        {
        }

    }
}
