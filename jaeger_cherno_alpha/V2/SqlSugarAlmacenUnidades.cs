﻿/// develop: anhe 010620190052
/// purpose: catalogo de unidades de almacen de materia prima
using System;
using SqlSugar;
using Jaeger.Edita.V2.Almacen.Entities;
using Jaeger.Edita.Interfaces;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using System.Collections.Generic;

namespace Jaeger.Edita.V2
{
    public class SqlSugarAlmacenUnidades : MySqlSugarContext<ViewModelUnidad>, ISqlUnidades
    {
        public SqlSugarAlmacenUnidades(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public bool Create()
        {
            try
            {
                this.Db.CodeFirst.InitTables<ViewModelUnidad>();
                return true;
            }
            catch (SqlSugarException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public int Save(System.Collections.Generic.List<ViewModelUnidad> items)
        {
            return this.Db.Updateable(items.ToArray()).ExecuteCommand();
        }

        public new List<ViewModelUnidad> GetList() {
            throw new NotImplementedException();
        }
    }
}
