﻿using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2.Empresa.Entities;

namespace Jaeger.Edita.V2
{
    /// <summary>
    /// Configuraciones de empresas en EDITA (empr)
    /// </summary>
    public class SqlSugarConfiguracionEmpresas : MySqlSugarContext<ViewModelEmpresa>, ISqlConfiguracionEmpresas
    {
        public SqlSugarConfiguracionEmpresas(DataBaseConfiguracion configuracion)
            : base(configuracion)
        {
        }

        public ViewModelEmpresa GetByRFC(string rfc)
        {
            return this.Db.Queryable<ViewModelEmpresa>().Where(it => it.RFC == rfc).Single();
        }
    }
}
