﻿/// develop: anhe
/// purpose: 
using System.Data;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Edita.Enums;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Edita.V2.Nomina.Entities;
using Jaeger.SAT.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class SqlSugarComprobanteNomina : MySqlSugarContext<ViewModelComplementoNominaSingle>, ISqlComprobanteNomina
    {
        public SqlSugarComprobanteNomina(DataBaseConfiguracion configuracion) : base(configuracion)
        {
        }

        public int Insert(ComplementoNomina complemento)
        {
            return 0;
        }

        public ViewModelNominaParteSingle Insert(ViewModelNominaParteSingle single)
        {
            return null;
        }

        public ComplementoNomina GetBy(int subIndex)
        {
            var single = this.Db.Queryable<ViewModelComplementoNominaSingle>().Where(it => it.SubId == subIndex).Single() as ViewModelComplementoNominaSingle;
            if (single != null)
            {
                var response = new ComplementoNomina();
                response.Id = single.Id;
                response.IdDocumento = single.IdDocumento;

                List<ViewModelNominaParteSingle> responses = this.Db.Queryable<ViewModelNominaParteSingle>().Where(it => it.SubId == response.Id).ToList();
                foreach (ViewModelNominaParteSingle item in responses)
                {
                    if (item.IdDoc == 1)
                    {
                        var p = item as ComplementoNominaPercepcion;
                        response.Percepciones.Percepcion.Add(p);
                    }
                    else if (item.IdDoc == 2)
                    {
                        var d = item as ComplementoNominaDeduccion;
                        response.Deducciones.Deduccion.Add(d);
                    }
                }
                return response;
            }
            return null;
        }

        public ComplementoNomina Save(ComplementoNomina complemento)
        {
            ViewModelComplementoNominaSingle temporal = complemento as ViewModelComplementoNominaSingle;
            complemento.Id = this.Insert(temporal);

            if (complemento.Id > 0)
            {
                // percepciones
                if (complemento.Percepciones != null)
                    for (int index = 0; index < complemento.Percepciones.Percepcion.Count; index++)
                    {
                        ViewModelNominaParteSingle single = complemento.Percepciones.Percepcion[index] as ViewModelNominaParteSingle;
                        if (single != null)
                            complemento.Percepciones.Percepcion[index].Id = this.Db.Insertable(single).ExecuteReturnIdentity();
                        if (complemento.Percepciones.Percepcion[index].HorasExtra != null)
                        {
                        
                        }
                    }

                // deducciones
                if (complemento.Deducciones != null)
                    if (complemento.Deducciones.Deduccion != null)
                    {
                        for (var index = 0; index < complemento.Deducciones.Deduccion.Count; index++)
                        {
                            ViewModelNominaParteSingle single = complemento.Deducciones.Deduccion[index] as ViewModelNominaParteSingle;
                            complemento.Deducciones.Deduccion[index].SubId = complemento.Id;
                            complemento.Deducciones.Deduccion[index].Id = this.Db.Insertable(complemento.Deducciones.Deduccion[index]).ExecuteReturnIdentity();
                        }
                    }

                // otros pagos
                if (complemento.OtrosPagos != null)
                {
                    for (int index = 0; index < complemento.OtrosPagos.Count; index++)
                    {
                        ViewModelNominaParteSingle single = complemento.OtrosPagos[index] as ViewModelNominaParteSingle;
                        complemento.OtrosPagos[index].SubId = complemento.Id;
                        complemento.OtrosPagos[index].Id = this.Db.Insertable(single).ExecuteReturnIdentity();
                    }
                }

                // incapacidades
                if (complemento.Incapacidades != null)
                    for (int index = 0; index < complemento.Incapacidades.Count; index++)
                    {
                        complemento.Incapacidades[index].SubId = complemento.Id;
                        complemento.Incapacidades[index].Id = this.Db.Insertable(complemento.Incapacidades[index]).ExecuteReturnIdentity();
                    }

            }
            return complemento;
        }

        public DataTable GetEmpleados()
        {
            return this.Db.Ado.GetDataTable("select distinct(_cfdi_nomr),_cfdnmn_numem from _cfdnmn,_cfdi where _cfdnmn_cfdi_id=_cfdi_id and _cfdi_nomr <> '' order by _cfdnmn_numem asc;","");
        }

        /// <summary>
        ///  obtener lista de resumen de nomina para tabla dinamica
        /// </summary>
        /// <param name="anio"></param>
        /// <param name="estado"></param>
        /// <param name="mes"></param>
        /// <returns></returns>
        public DataTable GetResumen(int anio, EnumCfdiEstado estado = EnumCfdiEstado.Vigente, EnumMonthsOfYear mes = EnumMonthsOfYear.None)
        {
            string sqlCommand = string.Concat("select _nmnprt_id as Indice,_nmnprt_tipo as Tipo,_nmnprt_dias as Dias,_nmnprt_hrsxtr as HorasExtra,_nmnprt_dsincp as DiasIncapacidad,_nmnprt_impgrv as ImporteGravado,_nmnprt_impext as ImporteExento,_nmnprt_dscnt as Descuento,_nmnprt_imppgd ImportePagado,_nmnprt_tphrs as TipoHoras,_nmnprt_clv as Clave,_nmnprt_cncpt as Concepto,_nmnprt_numem as NumEmpleado,_cfdnmn_fchpgo as FechaPagoNoSemana, _cfdnmn_numem EmpleadoNum, _cfdnmn_rfc EmpleadoRfc, _cfdi_nomr Empleado, _cfdi_estado as Estado, _cfdi_uuid iddocumento ",
                                              "from _nmnprt, _cfdnmn, _cfdi ",
                                              "where _nmnprt_cfdnmn_id=_cfdnmn_id and _cfdnmn_cfdi_id=_cfdi_id and _cfdi_uuid<>'' ",
                                              "and year(_cfdnmn_fchpgo) = @anio and month(_cfdnmn_fchpgo) = @mes and _cfdi_estado=@estado");
            if (mes == EnumMonthsOfYear.None)
                sqlCommand = string.Concat("select _nmnprt_id as Indice,_nmnprt_tipo as Tipo,_nmnprt_dias as Dias,_nmnprt_hrsxtr as HorasExtra,_nmnprt_dsincp as DiasIncapacidad,_nmnprt_impgrv as ImporteGravado,_nmnprt_impext as ImporteExento,_nmnprt_dscnt as Descuento,_nmnprt_imppgd ImportePagado,_nmnprt_tphrs as TipoHoras,_nmnprt_clv as Clave,_nmnprt_cncpt as Concepto,_nmnprt_numem as NumEmpleado,_cfdnmn_fchpgo as FechaPagoNoSemana, _cfdnmn_numem EmpleadoNum, _cfdnmn_rfc EmpleadoRfc, _cfdi_nomr Empleado, _cfdi_estado as Estado, _cfdi_uuid iddocumento ",
                                           "from _nmnprt, _cfdnmn, _cfdi ",
                                           "where _nmnprt_cfdnmn_id=_cfdnmn_id and _cfdnmn_cfdi_id=_cfdi_id and _cfdi_uuid<>'' ",
                                            "and year(_cfdnmn_fchpgo) = @anio and _cfdi_estado=@estado");
            if (estado == EnumCfdiEstado.Todos)
                sqlCommand = sqlCommand.Replace("and _cfdi_estado=@estado", ";");
            return this.Db.Ado.GetDataTable(sqlCommand, new List<SugarParameter>() {
                new SugarParameter("@mes", mes),
                new SugarParameter("@anio", anio),
                new SugarParameter("@estado", estado.ToString())
            });
        }

        public DataTable GetResumen(int indice) {
            string sqlCommand = string.Concat("select _nmnprt_id as Indice,_nmnprt_tipo as Tipo,_nmnprt_dias as Dias,_nmnprt_hrsxtr as HorasExtra,_nmnprt_dsincp as DiasIncapacidad,_nmnprt_impgrv as ImporteGravado,_nmnprt_impext as ImporteExento,_nmnprt_dscnt as Descuento,_nmnprt_imppgd ImportePagado,_nmnprt_tphrs as TipoHoras,_nmnprt_clv as Clave,_nmnprt_cncpt as Concepto,_nmnprt_numem as NumEmpleado,_cfdnmn_fchpgo as FechaPagoNoSemana, _cfdnmn_numem EmpleadoNum, _cfdnmn_rfc EmpleadoRfc, _cfdi_nomr Empleado, _cfdi_estado as Estado, _cfdi_uuid iddocumento ",
                                              "from _nmnprt, _cfdnmn, _cfdi ",
                                              "where _nmnprt_cfdnmn_id=_cfdnmn_id and _cfdnmn_cfdi_id=_cfdi_id and _cfdi_uuid<>'' ",
                                              "and _cfdnmn_nmnctrl_id=@index");
            return this.Db.Ado.GetDataTable(sqlCommand, new List<SugarParameter>() {
                new SugarParameter("@index", indice.ToString())
            });
        }

        public List<NominaViewSingle> GetCfdNominas(int index, bool activos = true)
        {
            throw new System.NotImplementedException();
        }

        public List<NominaViewSingle> GetCfdUuid(string uuid, bool showDelete)
        {
            throw new System.NotImplementedException();
        }

        public List<NominaViewSingle> GetCfdNominasBy(string nombre)
        {
            throw new System.NotImplementedException();
        }

        public List<NominaViewSingle> GetCfdNominaBy(EnumCfdiDates dateBy, System.DateTime dateStart, System.DateTime dateEnd, string depto, string employee)
        {
            throw new System.NotImplementedException();
        }

        public DataTable GetNominas()
        {
            string sqlCommand = "select * from _nmnctrl where _nmnctrl_a=1 order by _nmnctrl_id desc;";
            return this.Db.Ado.GetDataTable(sqlCommand);
        }

        public DataTable GetNominaDeptos()
        {
            throw new System.NotImplementedException();
        }

        public bool Update(SatQueryResult response) {
            throw new System.NotImplementedException();
        }
    }
}
