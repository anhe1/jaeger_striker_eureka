﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using MySql.Data.MySqlClient;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Edita.V2.Directorio.Enums;
using Jaeger.Edita.Interfaces;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using SqlSugar;

namespace Jaeger.Edita.V2
{
    public class MySqlDirectorio : MySqlSugarContext<ViewModelDomicilio>, ISqlContext<ViewModelContribuyente>, ISqlDirectorio
    {
        
        public MySqlDirectorio(DataBaseConfiguracion objeto) : base(objeto)
        {
            this.Domicilio = new MySqlDirectorioDomicilio(objeto);
        }

        public void MySqlDirectorio_ProcessError(object sender, Jaeger.Entities.ProgressError e)
        {
            Console.WriteLine(e.Data);
        }

        public MySqlDirectorioDomicilio Domicilio { get; set; }

        public MySqlDirectorioBancos Bancos { get; set; }

        public int ReturnId(string rfc)
        {
            var sqlCommand = new MySqlCommand() { CommandText = "select _drctr_id from _drctr where _drctr_rfc like @rfc limit 1" };
            sqlCommand.Parameters.AddWithValue("@rfc", rfc);
            return this.ExecuteScalar(sqlCommand);
        }

        public int Clave(string rfc)
        {
            var sqlCommand = new MySqlCommand() { CommandText = "select _drctr_id from _drctr where _drctr_clv like @clave limit 1" };
            sqlCommand.Parameters.AddWithValue("@clave", rfc);
            return this.ExecuteScalar(sqlCommand);
        }

        /// <summary>
        /// obtener un objeto contribuyente a traves de su indice
        /// </summary>
        public ViewModelContribuyente GetByRFC(string rfc)
        {
            var sqlCommand = new MySqlCommand() { CommandText = "select * from _drctr where _drctr_rfc=@rfc" };
            sqlCommand.Parameters.AddWithValue("@rfc", rfc);
            DataTable tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelContribuyente>();
            ViewModelContribuyente response = mapper.Map(tabla).ToList().FirstOrDefault();
            if (response != null)
            {
                response.Domicilios = this.GetDomicilios(response.Id);
                response.CuentasBancarias = this.GetCuentaBanco(response.Id);
                response.Contactos = this.GetContactos(response.Id);
                return response;
            }
            return response;
        }

        /// <summary>
        /// obtener un objeto contribuyente a traves de su indice
        /// </summary>
        public ViewModelContribuyente GetById(int index)
        {
            var sqlCommand = new MySqlCommand
            {
                CommandText = "select _drctr_id,_drctr_a,_drctr_usr,_drctr_dsc,_drctr_dsp,_drctr_rfc,_drctr_clv,_drctr_curp,_drctr_psw,_drctr_rgf,_drctr_rlcn,_drctr_web,_drctr_tel,_drctr_mail,_drctr_nom,_drctr_rgfsc,_drctr_nts,_drctr_crd,_drctr_pctd,_drctr_iva,_drctr_fn,_drctr_fm,_drctr_usr_n,_drctr_usr_m,_drctr_hstl,_drctr_file,_drctr_usocfdi,_drctr_cmsn_id,_drctr_dscnt_id, _drctr_domfis from _drctr where _drctr_id=@index limit 1;"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            DataTable tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelContribuyente>();
            ViewModelContribuyente response = mapper.Map(tabla).FirstOrDefault();
            if (response != null)
            {
                response.Domicilios = this.GetDomicilios(index);
                response.CuentasBancarias = this.GetCuentaBanco(index);
                response.Contactos = this.GetContactos(index);
                return response;
            }
            return null;
        }

        public List<ViewModelContribuyenteSingle> GetList(EnumRelationType tipo)
        {
            throw new IndexOutOfRangeException();
        }

        /// <summary>
        /// obtener lista de contribuyentes dependiendo del tipo, la vista incluye el domicilio fiscal
        /// </summary>
        /// <param name="tipo">Tipo de relacion con la empresa</param>
        /// <returns>Listado de contribuyentes con domicilio fiscal</returns>
        public BindingList<ViewModelContribuyenteDomicilio> GetListBy(EnumRelationType tipo)
        {
            var sqlCommand = new MySqlCommand()
            {
                CommandText = string.Concat(str0: "select _drctr_id,_drctr_a,_drctr_usocfdi,_drctr_usr_n,_drctr_rfc,_drctr_clv,_drctr_curp,_drctr_nmreg,_drctr_tel,_drctr_mail,_drctr_nom,_drctr_fn,_drccn_cll,_drccn_extr,_drccn_intr,_drccn_cln,_drccn_dlg,_drccn_cp,_drccn_std,_drccn_ps, _drctr_domfis ",
                                            str1: "from _drctr left join _drccn on _drctr._drctr_id=_drccn._drccn_drctr_id and _drccn_tp=@type and _drccn_a=1 ",
                                            str2: "where _drctr_a=@activo and find_in_set(@relationship,_drctr_rlcn) order by _drctr_nom;")
            };

            sqlCommand.Parameters.AddWithValue(parameterName: "@relationship", value: Enum.GetName(typeof(EnumRelationType), tipo));
            sqlCommand.Parameters.AddWithValue(parameterName: "@type", value: Enum.GetName(typeof(EnumDomicilioTipo), EnumDomicilioTipo.Fiscal));
            sqlCommand.Parameters.AddWithValue(parameterName: "@activo", value: 1);
            List<SugarParameter> parameters = new List<SugarParameter>() {
                new SugarParameter("@relationship", Enum.GetName(typeof(EnumRelationType), tipo)),
                new SugarParameter("@type", Enum.GetName(typeof(EnumDomicilioTipo), EnumDomicilioTipo.Fiscal)),
                new SugarParameter("@activo", 1)
            };
            return new BindingList<ViewModelContribuyenteDomicilio>(this.GetMapper<ViewModelContribuyenteDomicilio>(sqlCommand.CommandText, parameters).ToList());
        }

        public DataTable GetContactos(EnumRelationType relationship, bool showDeletes = false)
        {
            MySqlCommand sqlCommand = new MySqlCommand()
            {
                CommandText = string.Concat("select _drctr_id,_drctr_a,_drctr_usocfdi,_drctr_usr_n,_drctr_rfc,_drctr_clv,_drctr_curp,_drctr_nmreg,_drctr_tel,_drctr_mail,_drctr_nom, _drctr_domfis ",
                                            "from _drctr left join _drccn on _drctr._drctr_id=_drccn._drccn_drctr_id and _drccn_tp=@type and _drccn_a=1 where _drctr_a=@activo and find_in_set(@relationship,_drctr_rlcn) order by _drctr_nom;")
            };
            sqlCommand.Parameters.AddWithValue("@relationship", Enum.GetName(typeof(EnumRelationType), relationship));
            sqlCommand.Parameters.AddWithValue("@type", Enum.GetName(typeof(EnumDomicilioTipo), EnumDomicilioTipo.Fiscal));
            sqlCommand.Parameters.AddWithValue("@activo", 1);
            return this.ExecuteReader(sqlCommand);
        }

        /// <summary>
        /// guardar un contribuyente nuevo
        /// </summary>
        public ViewModelContribuyente Save(ViewModelContribuyente objeto)
        {
            if (objeto.Id == 0)
            {
                objeto.Id = this.Insert(objeto);
                if (objeto.Id > 0)
                {
                    for (int i = 0; i < objeto.Domicilios.Count; i++)
                    {
                        if (objeto.Domicilios[i].Id == 0)
                        {
                            objeto.Domicilios[i].SubId = objeto.Id;
                            objeto.Domicilios[i].Id = this.Domicilio.Insert(objeto.Domicilios[i]);
                        }
                    }
                }
            }
            else
            {
                if (this.Update(objeto) > 0)
                {

                }
            }
            return objeto;
        }

        public int Insert(ViewModelContribuyente objeto)
        {
            var sqlInsert = new MySqlCommand()
            {
                CommandText = "insert into _drctr (_drctr_id,_drctr_a,_drctr_dsc,_drctr_dsp,_drctr_rfc,_drctr_clv,_drctr_curp,_drctr_rgf,_drctr_rlcn,_drctr_web,_drctr_tel,_drctr_mail,_drctr_nom,_drctr_rgfsc,_drctr_nts,_drctr_crd,_drctr_pctd,_drctr_iva,_drctr_usr_n,_drctr_hstl,_drctr_file,_drctr_usocfdi, _drctr_domfis,_drctr_fn) " +
                                         " values (@drctr_id,@drctr_a,@drctr_dsc,@drctr_dsp,@drctr_rfc,@drctr_clv,@drctr_curp,@drctr_rgf,@drctr_rlcn,@drctr_web,@drctr_tel,@drctr_mail,@drctr_nom,@drctr_rgfsc,@drctr_nts,@drctr_crd,@drctr_pctd,@drctr_iva,@drctr_usr_n,@drctr_hstl,@drctr_file,@drctr_usocfdi,@_drctr_domfis,@drctr_fn);"
            };

            sqlInsert.Parameters.AddWithValue("@drctr_a", objeto.IsActive);
            sqlInsert.Parameters.AddWithValue("@drctr_rfc", objeto.RFC);
            sqlInsert.Parameters.AddWithValue("@drctr_curp", objeto.CURP);
            sqlInsert.Parameters.AddWithValue("@drctr_clv", objeto.Clave);
            sqlInsert.Parameters.AddWithValue("@drctr_rgf", objeto.Regimen);
            sqlInsert.Parameters.AddWithValue("@drctr_rlcn", objeto.Relacion);
            sqlInsert.Parameters.AddWithValue("@drctr_web", objeto.SitioWEB);
            sqlInsert.Parameters.AddWithValue("@drctr_tel", objeto.Telefono);
            sqlInsert.Parameters.AddWithValue("@drctr_mail", objeto.Correo);
            sqlInsert.Parameters.AddWithValue("@drctr_nom", objeto.Nombre);
            sqlInsert.Parameters.AddWithValue("@drctr_rgfsc", objeto.RegimenFiscal);
            sqlInsert.Parameters.AddWithValue("@drctr_usr_n", objeto.Creo);
            sqlInsert.Parameters.AddWithValue("@drctr_crd", objeto.Credito);
            sqlInsert.Parameters.AddWithValue("@drctr_dsp", objeto.DiasEntrega);
            sqlInsert.Parameters.AddWithValue("@drctr_nts", objeto.Nota);
            sqlInsert.Parameters.AddWithValue("@drctr_pctd", objeto.FactorDescuento);
            sqlInsert.Parameters.AddWithValue("@drctr_iva", objeto.FactorIvaPactado);
            sqlInsert.Parameters.AddWithValue("@drctr_hstl", "");
            sqlInsert.Parameters.AddWithValue("@drctr_dsc", DBNull.Value); 
            sqlInsert.Parameters.AddWithValue("@drctr_file", objeto.Avatar);
            sqlInsert.Parameters.AddWithValue("@drctr_usocfdi", objeto.ClaveUsoCFDI);
            sqlInsert.Parameters.AddWithValue("@_drctr_domfis", objeto.DomicilioFiscal);
            sqlInsert.Parameters.AddWithValue("@drctr_fn", DateTime.Now);
            objeto.Id = (int)this.GetIndex("_drctr_id", "_drctr");
            sqlInsert.Parameters.AddWithValue("@drctr_id", objeto.Id);

            if (ExecuteTransaction(sqlInsert) > 0)
            {
            return objeto.Id;
            }
            return objeto.Id;
        }

        public int Update(ViewModelContribuyente objeto)
        {
            var sqlUpdate = new MySqlCommand
            {
                CommandText = string.Concat("update _drctr set _drctr_id = @drctr_id, _drctr_a = @drctr_a, _drctr_rfc = @drctr_rfc, _drctr_clv = @drctr_clv, _drctr_nom = @drctr_nom, _drctr_rgfsc = @drctr_rgfsc,", "_drctr_rlcn = @drctr_rlcn, _drctr_curp = @drctr_curp, _drctr_rgf = @drctr_rgf, _drctr_web = @drctr_web, _drctr_tel = @drctr_tel, _drctr_mail = @drctr_mail, _drctr_resfis = @drctr_resfis, _drctr_nmreg = @drctr_nmreg, _drctr_fm = @drctr_fm, _drctr_usr_m = @drctr_usr_m, _drctr_usocfdi = @drctr_usocfdi, _drctr_domfis=@_drctr_domfis ", "where _drctr_id=@drctr_id;")
            };

            sqlUpdate.Parameters.AddWithValue("@drctr_a", objeto.IsActive);
            sqlUpdate.Parameters.AddWithValue("@drctr_rfc", objeto.RFC);
            sqlUpdate.Parameters.AddWithValue("@drctr_curp", objeto.CURP);
            sqlUpdate.Parameters.AddWithValue("@drctr_clv", objeto.Clave);
            sqlUpdate.Parameters.AddWithValue("@drctr_rgf", objeto.Regimen);
            sqlUpdate.Parameters.AddWithValue("@drctr_rlcn", objeto.Relacion);
            sqlUpdate.Parameters.AddWithValue("@drctr_web", objeto.SitioWEB);
            sqlUpdate.Parameters.AddWithValue("@drctr_tel", objeto.Telefono);
            sqlUpdate.Parameters.AddWithValue("@drctr_mail", objeto.Correo);
            sqlUpdate.Parameters.AddWithValue("@drctr_nom", objeto.Nombre);
            sqlUpdate.Parameters.AddWithValue("@drctr_rgfsc", objeto.RegimenFiscal);
            sqlUpdate.Parameters.AddWithValue("@drctr_usr", objeto.Creo);
            sqlUpdate.Parameters.AddWithValue("@drctr_crd", objeto.Credito);
            sqlUpdate.Parameters.AddWithValue("@drctr_dsp", objeto.DiasEntrega);
            sqlUpdate.Parameters.AddWithValue("@drctr_nts", objeto.Nota);
            sqlUpdate.Parameters.AddWithValue("@drctr_pctd", objeto.FactorDescuento);
            sqlUpdate.Parameters.AddWithValue("@drctr_iva", objeto.FactorIvaPactado);
            sqlUpdate.Parameters.AddWithValue("@drctr_usr_m", objeto.Modifica);
            sqlUpdate.Parameters.AddWithValue("@drctr_fm", DateTime.Now);
            sqlUpdate.Parameters.AddWithValue("@drctr_hstl", "");
            sqlUpdate.Parameters.AddWithValue("@drctr_dsc", DBNull.Value); // objeto.Descuento);
            sqlUpdate.Parameters.AddWithValue("@drctr_file", objeto.Avatar);
            sqlUpdate.Parameters.AddWithValue("@drctr_resfis", objeto.ResidenciaFiscal);
            sqlUpdate.Parameters.AddWithValue("@drctr_nmreg", objeto.NumRegIdTrib);
            sqlUpdate.Parameters.AddWithValue("@drctr_usocfdi", objeto.ClaveUsoCFDI);
            sqlUpdate.Parameters.AddWithValue("@drctr_id", objeto.Id);
            sqlUpdate.Parameters.AddWithValue("@_drctr_domfis", objeto.DomicilioFiscal);

            return this.ExecuteScalar(sqlUpdate);
        }

        public bool Delete(int index)
        {
            throw new NotImplementedException();
        }

        public List<ViewModelContribuyente> GetList()
        {
            var sqlCommand = new MySqlCommand() { CommandText = "select * from _drctr where _drctr_a = 1" };
            DataTable tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelContribuyente>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// actualizar el correo electronico del directorio
        /// </summary>
        /// <param name="index">indice del directoio</param>
        /// <param name="correo">correo electronic valido</param>
        /// <returns></returns>
        public bool Update(int index, string correo)
        {
            var update = new MySqlCommand()
            {
                CommandText = "update _drctr set _drctr_mail=@correo where _drctr_id = @index;"
            };
            update.Parameters.AddWithValue(parameterName: "@correo", value: correo);
            update.Parameters.AddWithValue(parameterName: "@index", value: index);
            return this.ExecuteTransaction(update) > 0;
        }

        /// <summary>
        /// obtener lista de domicilios registrados relacionados al indice del directorio
        /// </summary>
        /// <param name="index">indice de relacion con el contribuyente</param>
        /// <returns></returns>
        public BindingList<ViewModelDomicilio> GetDomicilios(int index)
        {
            var sqlCommand = new MySqlCommand
            {
                CommandText = "select * from _drccn where _drccn_a = 1 and _drccn_drctr_id=@index"
            };

            sqlCommand.Parameters.AddWithValue("@index", index);
            DataTable tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelDomicilio>();
            List<ViewModelDomicilio> lista = mapper.Map(tabla).ToList();
            return new BindingList<ViewModelDomicilio>(lista);
        }

        public BindingList<ViewModelCuentaBancaria> GetCuentaBanco(int index)
        {
            var sqlCommand = new MySqlCommand
            {
                CommandText = "select _dcbnc.* from _dcbnc where _dcbnc_a = 1 and _dcbnc_drctr_id=@index;"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            DataTable tabla = ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelCuentaBancaria>();
            List<ViewModelCuentaBancaria> lista = mapper.Map(tabla).ToList();
            return new BindingList<ViewModelCuentaBancaria>(lista);
        }

        public BindingList<ViewModelContacto> GetContactos(int index)
        {
            var sqlCommand = new MySqlCommand
            {
                CommandText = "select * from _dcntc where _dcntc_a = 1 and _dcntc_drctr_id=@index"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            DataTable tabla = ExecuteReader(sqlCommand);
            DataNamesMapper<ViewModelContacto> mapper = new DataNamesMapper<ViewModelContacto>();
            List<ViewModelContacto> lista = mapper.Map(tabla).ToList();
            return new BindingList<ViewModelContacto>(lista);
        }

        #region operaciones con las tablas de intercambio con solicitantes de recibos fiscales

        /// <summary>
        /// obtener lista del directorio de clientes y solicitantes, unicamente se utiliza en AFASA
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public BindingList<ViewModelContribuyenteDomicilio> GetListBySolicitantes()
        {
            var sqlCommand = new MySqlCommand()
            {
                CommandText = string.Concat(str0: "select _drctr_id,_drctr_a,_drctr_usocfdi,_drctr_usr_n,_drctr_rfc,_drctr_clv,_drctr_curp,_drctr_nmreg,_drctr_tel,_drctr_mail,_drctr_nom,_drctr_fn,_drccn_cll,_drccn_extr,_drccn_intr,_drccn_cln,_drccn_dlg,_drccn_cp,_drccn_std,_drccn_ps ",
                                            str1: "from _drctr left join _drccn on _drctr._drctr_id=_drccn._drccn_drctr_id and _drccn_tp=@type and _drccn_a=1 ",
                                            str2: "where _drctr_a=1 GROUP BY _drctr_rfc order by _drctr_nom;")
            };

            sqlCommand.Parameters.AddWithValue(parameterName: "@type", value: Enum.GetName(typeof(EnumDomicilioTipo), EnumDomicilioTipo.Fiscal));

            DataTable tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelContribuyenteDomicilio>();
            return new BindingList<ViewModelContribuyenteDomicilio>(mapper.Map(tabla).ToList());
        }

        public ViewModelContribuyenteDomicilio GetSingleRFC(string rfc)
        {
            var sqlCommand = new MySqlCommand()
            {
                CommandText = string.Concat(str0: "select _drctr_id,_drctr_a,_drctr_usocfdi,_drctr_usr_n,_drctr_rlcn,_drctr_rfc,_drctr_clv,_drctr_curp,_drctr_nmreg,_drctr_tel,_drctr_mail,_drctr_nom,_drctr_fn,_drccn_cll,_drccn_extr,_drccn_intr,_drccn_cln,_drccn_dlg,_drccn_cp,_drccn_std,_drccn_ps ",
                                            str1: "from _drctr left join _drccn on _drctr._drctr_id=_drccn._drccn_drctr_id and _drccn_tp=@type and _drccn_a=1 ",
                                            str2: "where _drctr_a=@activo and _drctr_rfc =@rfc order by _drctr_nom;")
            };

            sqlCommand.Parameters.AddWithValue(parameterName: "@rfc", value: rfc);
            sqlCommand.Parameters.AddWithValue(parameterName: "@type", value: Enum.GetName(typeof(EnumDomicilioTipo), EnumDomicilioTipo.Fiscal));
            sqlCommand.Parameters.AddWithValue(parameterName: "@activo", value: 1);

            DataTable tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelContribuyenteDomicilio>();
            return mapper.Map(tabla).ToList().FirstOrDefault();
        }

        #endregion
    }
}
