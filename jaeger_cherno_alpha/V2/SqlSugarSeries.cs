﻿using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Edita.V2.Empresa.Entities;

namespace Jaeger.Edita.V2
{
    public class SqlSugarSeries : MySqlSugarContext<ViewModelSerieFolio>
    {
        public SqlSugarSeries(DataBaseConfiguracion configuracion) : base(configuracion)
        {
        }
    }
}
