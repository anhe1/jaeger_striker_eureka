﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.SAT.Entities;
using Jaeger.Edita.Enums;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2.Contable.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Enums;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2.CFDI.Entities.Cancel;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class SqlSugarComprobanteFiscal : MySqlSugarContext<ViewModelComprobante>, ISqlComprobanteFiscal
    {
        public SqlSugarComprobanteFiscal(DataBaseConfiguracion objeto) : base(objeto)
        {
            this.Data2 = new MySqlComprobanteFiscal(objeto);
            this.Nomina = new SqlSugarComprobanteNomina(objeto);
        }

        public MySqlComprobanteFiscal Data2 { get; set; }

        public SqlSugarComprobanteNomina Nomina { get;  set; }

        /// <summary>
        /// obtener el indice de un comprobante por su folio fiscal (UUID)
        /// </summary>
        public int ReturnId(string idDocumento)
        {
            int indice = 0;
            try
            {
                indice = this.Db.Queryable<ViewModelComprobanteSingle>().Where(it => it.IdDocumento == idDocumento.ToUpper()).Select(it => it.Id).Single();
            }
            catch (SqlSugarException ex)
            {
                Console.WriteLine(ex.Message);
                indice = this.Db.Queryable<ViewModelComprobanteSingle>().Where(it => it.IdDocumento == idDocumento.ToUpper()).Select(it => it.Id).First();
            }
            return indice;
        }

        /// <summary>
        /// almacenar un objeto Comprobante en la base de datos
        /// </summary>
        /// <param name="comprobante">ViewModelComprobante</param>
        /// <returns>ViewModelComprobante</returns>
        public ViewModelComprobante Save(ViewModelComprobante comprobante)
        {
            // insertar el comprobante
            if (comprobante.Id == 0)
            {
                // si es un comprobante emitido creamos el nuevo folio a partir de la serie
                if (comprobante.SubTipo == EnumCfdiSubType.Emitido)
                    comprobante.Folio = this.GetFolio(comprobante.Emisor.RFC, comprobante.Serie).ToString();
                comprobante.FechaNuevo = DateTime.Now;
                comprobante.Id = this.Insert(comprobante);
            }
            else
            {
                if (this.Update(comprobante) == 0)
                    return comprobante;
            }

            // almacenar los conceptos
            if (comprobante.Conceptos != null)
            {
                for (int i = 0; i < comprobante.Conceptos.Count; i++)
                {
                    if (comprobante.Conceptos[i].Id == 0)
                    {
                        comprobante.Conceptos[i].SubId = comprobante.Id;
                        comprobante.Conceptos[i].FechaNuevo = DateTime.Now;
                        comprobante.Conceptos[i].Creo = comprobante.Creo;
                        comprobante.Conceptos[i].Id = this.Db.Insertable(comprobante.Conceptos[i]).ExecuteReturnIdentity();
                    }
                    else
                    {
                        this.Db.Updateable(comprobante.Conceptos[i]);
                    }
                }
            }

            if (comprobante.TimbreFiscal != null)
            {
                // si contiene un complemento de pagos
                if (comprobante.ComplementoPagos != null)
                    this.AplicarComprobantePagos(comprobante.ComplementoPagos);

                // cfdi relacionados
                if (comprobante.CfdiRelacionados != null)
                    if (comprobante.CfdiRelacionados.CfdiRelacionado != null)
                        this.AplicarCFDIRelacionados(comprobante.CfdiRelacionados);
            }
            

            if (comprobante.Nomina != null)
            {
                
            }

            return comprobante;
        }

        /// <summary>
        /// actualizar url del archivo XML del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">direccion URL</param>
        /// <returns>verdadero si la acción se realiza con exito</returns>
        public bool UpdateUrlXml(int index, string url)
        {
            return this.Db.Updateable<ViewModelComprobanteSingle>().SetColumns(it => new ViewModelComprobanteSingle() { UrlXml = url }).Where(it => it.Id == index).ExecuteCommand() > 0;
        }

        /// <summary>
        /// actualizar url del archivo XML del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">direccion URL</param>
        /// <returns>verdadero si la acción se realiza con exito</returns>
        public bool UpdateUrlPdf(int index, string url)
        {
            return this.Db.Updateable<ViewModelComprobanteSingle>().SetColumns(it => new ViewModelComprobanteSingle() { UrlPdf = url }).Where(it => it.Id == index).ExecuteCommand() > 0;
        }

        /// <summary>
        /// actualizar url del acuse de cancelacion del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">url</param>
        /// <returns>verdadero si la accion se realiza con exito</returns>
        public bool UpdateUrlXmlAcuse(int index, string url)
        {
            return this.Db.Updateable<ViewModelComprobanteSingle>().SetColumns(it => new ViewModelComprobanteSingle() { UrlAcuseXml = url }).Where(it => it.Id == index).ExecuteCommand() > 0;
        }

        /// <summary>
        /// actualizar url del acuse de cancelacion del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">url</param>
        /// <returns>verdadero si la accion se realiza con exito</returns>
        public bool UpdateUrlPdfAcuse(int index, string url)
        {
            return this.Db.Updateable<ViewModelComprobanteSingle>().SetColumns(it => new ViewModelComprobanteSingle() { UrlAcusePdf = url }).Where(it => it.Id == index).ExecuteCommand() > 0;
        }

        /// <summary>
        /// actualizar estado del comprobante segun SAT
        /// </summary>
        /// <param name="response">objeto respuesta del servicio SAT</param>
        /// <returns>verdadero si fue actualizado el registro con exito</returns>
        public bool Update(SatQueryResult response)
        {
            bool actualizado = this.Db.Updateable<ViewModelComprobanteSingleC>().SetColumns(it => new ViewModelComprobanteSingleC { Estado = response.Status, FechaEstado = DateTime.Now }).Where(it => it.Id == response.Id).ExecuteCommand() > 0;
            if (actualizado == false)
            {
                //this.Message = new MessageError { Type = "Error", NoError = 100, Value = "Error al actualizar el estado del comprobante" };
                //this.Message = new MessageError { Data = "Error al actualizar el estado del comprobante" };
                Console.WriteLine("No se actualizo el estado del comprobante.");
            }
            return actualizado;
        }

        /// <summary>
        /// actualizar el objeto del de cancelacion de un comprobante fiscal
        /// </summary>
        public bool Update(string uuid, CancelaCFDResponse item, string usuario)
        {
            return this.Db.Updateable<ViewModelComprobante>().SetColumns(it => new ViewModelComprobante { JAccuse = item.Xml(), FechaCancela = item.CancelaCFDResult.Fecha, Status = "Cancelado", Modifica = usuario, FechaMod = DateTime.Now }).Where(it => it.UUID == uuid).ExecuteCommand() > 0;
        }

        /// <summary>
        /// actualizar status desde la lista de comprobantes
        /// </summary>
        public bool Update(int indice, string status, string usuario)
        {
            return this.Db.Updateable<ViewModelComprobante>().SetColumns(it => new ViewModelComprobante { StatusText = status, FechaEntrega = DateTime.Now, Modifica = usuario, FechaMod = DateTime.Now }).Where(it => it.Id == indice).ExecuteCommand() > 0;
        }

        public ViewModelComprobante GetComprobante(int indice)
        {
            ViewModelComprobante response = this.GetById(indice);
            if (response != null)
                response.Conceptos = this.GetConceptos(response.Id);

            if (response.SubTipo == EnumCfdiSubType.Nomina)
                response.Nomina = this.Data2.Nomina.GetBy(response.Id);
                //response.Nomina = this.Nomina.GetBy(response.Id);
            return response;
        }

        /// <summary>
        /// obtener un objeto simple de comprobante fiscal
        /// </summary>
        /// <param name="idDocumento">IdDocumento (uuid)</param>
        /// <returns>objeto ViewModelComprobanteSingle</returns>
        public ViewModelComprobanteSingle GetSingle(string idDocumento)
        {
            try
            {
                return this.Db.Queryable<ViewModelComprobanteSingle>().Where(it => it.IdDocumento == idDocumento).Where(it => it.Activo == true).Single();
            }
            catch (SqlSugarException ex)
            {
                Console.WriteLine(ex.Message);
                return this.Db.Queryable<ViewModelComprobanteSingle>().Where(it => it.IdDocumento == idDocumento).Where(it => it.Activo == true).First();
            }
        }

        /// <summary>
        /// metodo para obtener un comprobante fiscal con el metodo anterior
        /// </summary>
        /// <param name="indice">indice del comprobante</param>
        /// <returns>CFDI.Entities.Comprobante version anterior</returns>
        public Jaeger.CFDI.Entities.Comprobante GetAdoComprobante(int indice)
        {
            string commandText = "select _cfdi.*, (_cfdi_total - (_cfdi_cbrd + _cfdi_dscntc)) as _cfdi_saldo from _cfdi where _cfdi_id=@index;";
            DataTable data = this.Db.Ado.GetDataTable(commandText, 
                new List<SugarParameter>(){ new SugarParameter("@index", indice) });
            var mapper = new DataNamesMapper<Jaeger.CFDI.Entities.Comprobante>();
            Jaeger.CFDI.Entities.Comprobante comprobante = mapper.Map(data).FirstOrDefault();
            if (comprobante != null)
                comprobante.Conceptos = this.GetAdoConceptos(indice);
            return comprobante;
        }

        public BindingList<ViewModelComprobanteSingleC> GetListBy(EnumCfdiSubType subtipo, string status, EnumMonthsOfYear mes = 0, int anio = 0, bool ado = true)
        {
            if (ado)
                return this.GetAdoListBy(subtipo, status, mes, anio);
            else 
                return this.GetListBy(subtipo, status, mes, anio);
        }

        public BindingList<ViewModelComprobanteSingle> GetListSingleBy(EnumCfdiSubType subtipo, string status, EnumMonthsOfYear mes = 0, int anio = 0, bool ado = true)
        {
            return new BindingList<ViewModelComprobanteSingle>(
                this.Db.Queryable<ViewModelComprobanteSingle>()
                    .Where(it => it.FechaEmision.Month == (int)mes)
                    .Where(it => it.FechaEmision.Year == anio)
                    .Where(it => it.SubTipo == subtipo)
                    .WhereIF(status != "Todos", it => it.Status == status).ToList());
        }
        
        public BindingList<ViewModelComprobanteSingle> GetListSingleBy(EnumCfdiSubType subtipo, string rfc, string folio = "", string status = "", string metodo = "")
        {
            return new BindingList<ViewModelComprobanteSingle>(
                this.Db.Queryable<ViewModelComprobanteSingle>()
                    .WhereIF(subtipo == EnumCfdiSubType.Recibido, it => it.EmisorRFC == rfc)
                    .WhereIF(subtipo == EnumCfdiSubType.Emitido, it => it.ReceptorRFC == rfc)
                    .WhereIF(status != "", it => it.Status.Contains(status))
                    .WhereIF(metodo != "", it => it.MetodoPago == metodo)
                    .WhereIF(folio != "", it => it.Folio.Contains(folio)).OrderBy(it => it.Id, OrderByType.Desc).ToList());
        }

        /// <summary>
        /// obtener listado de conceptos de un comprobante fiscal por su id
        /// </summary>
        /// <param name="indice">indice relacionado</param>
        /// <returns></returns>
        public BindingList<ViewModelComprobanteConcepto> GetConceptos(int indice, bool ado = true)
        {
            if (ado)
                return this.GetAdoConceptos(indice);
            else
                return this.GetConceptos(indice);
        }

        /// <summary>
        /// obtener una lista de comprobantes fiscales a partir de un array de uuids
        /// </summary>
        /// <param name="uuids">array de uuid de comprobantes</param>
        /// <returns>Lista de objetos ViewModelComprobanteSingle</returns>
        public BindingList<ViewModelComprobanteSingle> GetListIn(string[] uuids)
        {
            return new BindingList<ViewModelComprobanteSingle>(this.Db.Queryable<ViewModelComprobanteSingle>().In(it => it.IdDocumento, uuids).ToList());
        }

        /// <summary>
        /// listado de recibos fiscales por mes año y la verifcacion del emisor del RFC, solo funciona con la serie RF
        /// </summary>
        /// <param name="mes">mes</param>
        /// <param name="anio">año</param>
        /// <param name="rfc">rfc</param>
        /// <returns></returns>
        public BindingList<ViewModelReciboFiscal> GetRecibosFiscales(int mes, int anio, int dia, string rfc)
        {
            string commandText = "select * from _cfdi where _cfdi_rfce=@rfc and _cfdi_serie like 'RF' and month(_cfdi_fecems)=@mes and year(_cfdi_fecems)=@anio order by _cfdi._cfdi_feccert desc";
            DataTable tabla = this.Db.Ado.GetDataTable(commandText, 
                new SugarParameter[] {
                    new SugarParameter("@anio", anio),
                    new SugarParameter("@mes", mes),
                    new SugarParameter("@rfc", rfc),
            });
            DataNamesMapper<ViewModelReciboFiscal> mapper = new DataNamesMapper<ViewModelReciboFiscal>();
            return new BindingList<ViewModelReciboFiscal>(mapper.Map(tabla).ToList());
        }

        #region operaciones prepolizas

        public ViewModelPrePoliza CrearRecibo(List<string> folioFiscal, ComplementoPagos complemento = null)
        {
            // recibo nuevo
            ViewModelPrePoliza nuevo = new ViewModelPrePoliza();

            // comprobamos si es un comprobante que tenga el complemento
            if (complemento != null)
            {
                foreach (ComplementoPagosPago item in complemento.Pago)
                {
                    folioFiscal.AddRange(item.DoctoRelacionado.Where(x => x != null).Select(x => x.IdDocumento).ToList());
                }
            }

            List<ViewModelComprobanteSingle> lista = new List<ViewModelComprobanteSingle>(this.GetListIn(folioFiscal.ToArray()));
            foreach (var item in lista)
            {
                ViewModelPrePolizaComprobante documento = new ViewModelPrePolizaComprobante(item);
                if (documento != null)
                {
                    documento.IsActive = true;
                    documento.Abono = (documento.SubTipoComprobante == EnumCfdiSubType.Emitido ? documento.Total - documento.Acumulado : 0);
                    documento.Cargo = (documento.SubTipoComprobante == EnumCfdiSubType.Recibido ? documento.Total - documento.Acumulado : 0);
                    nuevo.Concepto = string.Concat(nuevo.Concepto, documento.Serie, " ", documento.Folio, " ");
                    nuevo.Comprobantes.Add(documento);

                    if (documento.SubTipoComprobante == EnumCfdiSubType.Emitido)
                    {
                        nuevo.Tipo = Jaeger.Edita.V2.Contable.Enums.EnumPolizaTipo.Ingreso;
                        nuevo.Receptor.Nombre = documento.Receptor;
                        nuevo.Receptor.Rfc = documento.ReceptorRFC;
                    }
                    else if (documento.SubTipoComprobante == EnumCfdiSubType.Recibido)
                    {
                        nuevo.Tipo = Jaeger.Edita.V2.Contable.Enums.EnumPolizaTipo.Egreso;
                        nuevo.Receptor.Nombre = documento.Emisor;
                        nuevo.Receptor.Rfc = documento.EmisorRFC;
                    }
                }
            }

            nuevo.Concepto = string.Concat((nuevo.Tipo == Jaeger.Edita.V2.Contable.Enums.EnumPolizaTipo.Ingreso ? "Cobro de factura " : "Pago de factura "), nuevo.Concepto);
            nuevo.FechaNuevo = DateTime.Now;
            return nuevo;
        }

        /// <summary>
        /// obtener lista de prepolizas relacionadas a un comprobante fiscal por su uuid
        /// </summary>
        /// <param name="idDocumento">folio fiscal (uuid)</param>
        /// <returns></returns>
        public BindingList<ViewModelPrePoliza> Recibos(string idDocumento)
        {
            return new BindingList<ViewModelPrePoliza>(this.Db.Queryable<ViewModelPrePoliza, ViewModelPrePolizaComprobante>(
                (p, c) => new object[]{
                    JoinType.Left, p.NoIndet == c.NoIndet
                }).Where((p, c) => c.IsActive == true && c.UUID == idDocumento).Select((p, c) => new ViewModelPrePoliza
                {
                    Id = p.Id,
                    IsActive = p.IsActive,
                    EstadoText = p.EstadoText,
                    TipoText = p.TipoText,
                    NoIndet = p.NoIndet,
                    Folio = p.Folio,
                    Serie = p.Serie,
                    FechaEmision = p.FechaEmision,
                    FechaDocto = p.FechaDocto,
                    FechaPago = p.FechaPago,
                    FechaVence = p.FechaVence,
                    FechaBoveda = p.FechaBoveda,
                    FechaCancela = p.FechaCancela,
                    NumDocto = p.NumDocto,
                    NumAutorizacion = p.NumAutorizacion,
                    Referencia = p.Referencia,
                    Concepto = p.Concepto,
                    Cargo = p.Cargo,
                    Abono = p.Abono,
                    PorJustificar = p.PorJustificar,
                    Notas = p.Notas,
                    FechaNuevo = p.FechaNuevo,
                    FechaMod = p.FechaMod,
                    FormaDePagoText = p.FormaDePagoText,
                    EmisorCodigo = p.EmisorCodigo,
                    ReceptorCodigo = p.ReceptorCodigo,
                    Creo = p.Creo,
                    EmisorClave = p.EmisorClave,
                    ReceptorClave = p.ReceptorClave,
                    EmisorRFC = p.EmisorRFC,
                    ReceptorRFC = p.ReceptorRFC,
                    ReceptorNumCta = p.ReceptorNumCta,
                    ReceptorSucursal = p.ReceptorSucursal,
                    EmisorNumCta = p.EmisorNumCta,
                    ReceptorCLABE = p.ReceptorCLABE,
                    FormaPagoDescripcion = p.FormaPagoDescripcion,
                    ReceptorBanco = p.ReceptorBanco,
                    ReceptorBeneficiario = p.ReceptorBeneficiario,
                    Modifica = p.Modifica,
                    Cancela = p.Cancela,
                    Autoriza = p.Autoriza,
                }).ToList());
        }

        /// <summary>
        /// obtener lista de comprobantes fiscales relacionados (Recibo Electronico de Pago)
        /// </summary>
        public List<ComplementoPagoDoctoRelacionado> Rep(string idDocumento)
        {
            List<ViewModelComprobanteSingleC> lista = this.Db.Queryable<ViewModelComprobanteSingleC>().Where(it => it.JComplementoPagos.Contains(idDocumento)).ToList();
            List<ComplementoPagoDoctoRelacionado> lista1 = new List<ComplementoPagoDoctoRelacionado>();
            foreach (ViewModelComprobanteSingleC item in lista)
            {
                if (item.ComplementoPagos != null)
                {
                    foreach (ComplementoPagosPago pagosPago in item.ComplementoPagos.Pago)
                    {
                        List<ComplementoPagoDoctoRelacionado> doctos = pagosPago.DoctoRelacionado.Where(p => p.IdDocumento == idDocumento).ToList();
                        if (doctos != null)
                        {
                            foreach (ComplementoPagoDoctoRelacionado docto in doctos)
                            {
                                lista1.Add(docto);
                            }
                        }
                    }
                }
            }
            return lista1;
        }

        /// <summary>
        /// crear comprobante de Pagos
        /// </summary>
        /// <param name="lista">lista de objetos ViewModelComprobantes</param>
        /// <returns></returns>
        public ViewModelComprobante CrearReciboElectronicoPago(List<ViewModelComprobanteSingleC> lista)
        {
            ComplementoPagosPago pago = new ComplementoPagosPago();
            pago.DoctoRelacionado = new BindingList<ComplementoPagoDoctoRelacionado>();
            pago.AutoSuma = false;
            ViewModelComprobante cfdiV33 = new ViewModelComprobante();
            cfdiV33.TipoComprobante = EnumCfdiType.Pagos;
            cfdiV33.SubTipo = EnumCfdiSubType.Emitido;
            cfdiV33.Default();
            cfdiV33.Status = "Pendiente";
            cfdiV33.Receptor.ClaveUsoCFDI = "P01";
            cfdiV33.ComplementoPagos = new ComplementoPagos();
            cfdiV33.ComplementoPagos.Pago = new BindingList<ComplementoPagosPago>();
            cfdiV33.Complementos = new Complementos();
            cfdiV33.Complementos.Objeto = new BindingList<Complemento>();
            /// obtener lista de comprobantes
            List<ViewModelComprobanteSingle> documentos = this.Db.Queryable<ViewModelComprobanteSingle>().In(it => it.IdDocumento, lista.Select(x=> x.IdDocumento).ToList()).ToList();
            foreach (ViewModelComprobanteSingle documento in documentos)
            {
                ComplementoPagoDoctoRelacionado d = new ComplementoPagoDoctoRelacionado
                {
                    AutoCalcular = true,
                    TipoCambio = documento.TipoCambio,
                    IdDocumento = documento.IdDocumento,
                    Serie = documento.Serie,
                    Folio = documento.Folio,
                    FechaEmision = documento.FechaEmision,
                    MetodoPago = documento.MetodoPago,
                    Nombre = documento.Receptor,
                    RFC = documento.ReceptorRFC,
                    Moneda = documento.Moneda,
                    ImpPagado = documento.SaldoPagos,
                    ImpSaldoAnt = documento.SaldoPagos,
                    ImpSaldoInsoluto = 0,
                    NumParcialidad = documento.NumParcialidad + 1,
                    Estado = EnumEdoPagoDoctoRel.Relacionado
                };
                
                pago.DoctoRelacionado.Add(d);
                cfdiV33.Receptor.RFC = documento.ReceptorRFC;
                cfdiV33.Receptor.Nombre = documento.Receptor;
            }
            pago.Monto = pago.DoctoRelacionado.Sum(x => x.ImpPagado);
            pago.FechaPago = DateTime.Now;
            pago.MonedaP = "MXN";
            pago.FormaDePagoP = new ComplementoPagoFormaPago { Clave = "03" };
            cfdiV33.ComplementoPagos.Pago.Add(pago);
            cfdiV33.Complementos.Objeto.Add(new Complemento
            {
                Nombre = EnumCfdiComplementos.Pagos10,
                Data = pago.Json()
            });

            return cfdiV33;
        }

        #endregion
        
        public ViewModelAccuseCancelacion GetAccuse(string uuid)
        {
            string commandText = "select _cfdi_folio,_cfdi_serie,_cfdi_nome,_cfdi_rfce,_cfdi_nomr,_cfdi_rfcr,_cfdi_efecto,_cfdi_fecems,_cfdi_nocert,_cfdi_acuse from _cfdi where _cfdi_uuid = @uuid;";
            DataTable data = this.Db.Ado.GetDataTable(commandText, new List<SugarParameter>() { new SugarParameter("@uuid", uuid) });
            DataNamesMapper<ViewModelAccuseCancelacion> mapper = new DataNamesMapper<ViewModelAccuseCancelacion>();
            return mapper.Map(data).FirstOrDefault();
        }

        /// <summary>
        ///  Generar folio para un comprobante
        /// </summary>
        /// <param name="rfc">RFC del emisor del comprobante</param>
        /// <param name="serie">Serie del comprobante fiscal</param>
        /// <returns>numero entero convertido en cadena</returns>
        public string GetFolio(string rfc, string serie)
        {
            try
            {
                int folio = this.Db.Ado.GetInt("select (Max(cast(_cfdi._cfdi_folio as UNSIGNED)) + 1) as folio from _cfdi where _cfdi_rfce = @rfc and _cfdi._cfdi_serie = @serie", new { rfc = rfc, serie = serie });
                return folio.ToString();
            }
            catch (SqlSugarException ex)
            {
                Console.WriteLine(ex.Message);
                return "0";
            }
        }

        #region reportes

        /// <summary>
        /// obtener reporte del estado de cuenta del contribyente seleccionado
        /// </summary>
        /// <param name="rfc"></param>
        /// <param name="subtipo"></param>
        /// <param name="status"></param>
        /// <param name="mes"></param>
        /// <param name="anio"></param>
        /// <param name="fechas"></param>
        /// <returns></returns>
        public Jaeger.Entities.Reportes.EstadoDeCuenta ReporteEstadoCuenta(string rfc, EnumCfdiSubType subtipo, string status, EnumMonthsOfYear mes = 0, int anio = 0, EnumCfdiDates fechas = EnumCfdiDates.FechaEmision)
        {
            string commandText = string.Concat("SELECT _cfdi._cfdi_efecto, _cfdi._cfdi_fecems,_cfdi_fecupc,_cfdi_fecvnc, _cfdi._cfdi_status, _cfdi._cfdi_rfce, _cfdi._cfdi_nome, _cfdi._cfdi_rfcr, _cfdi._cfdi_nomr, _cfdi._cfdi_folio, _cfdi._cfdi_serie, _cfdi._cfdi_sbttl, _cfdi._cfdi_dec, _cfdi._cfdi_total, _cfdi._cfdi_cbrd, (_cfdi_total - (_cfdi_cbrd + _cfdi_dscntc)) as _cfdi_saldo, _cfdi._cfdi_cbrdp ",
                                                "FROM _cfdi ",
                                                "WHERE _cfdi_rfce = @rfc AND MONTH(_cfdi._cfdi_fecems)=@mes AND YEAR(_cfdi_fecems)=@anio AND _cfdi_a=1 ",
                                                "ORDER BY _cfdi._cfdi_nomr;");

            if (subtipo == EnumCfdiSubType.Emitido)
                commandText = commandText.Replace("_cfdi_rfce", "_cfdi_rfcr");

            if (mes == EnumMonthsOfYear.None)
                commandText = commandText.Replace("AND MONTH(_cfdi._cfdi_fecems)=@mes", "");

            DataTable tabla = this.Db.Ado.GetDataTable(commandText, new List<SugarParameter>()
            {
                new SugarParameter("@rfc", rfc),
                new SugarParameter("@mes", (int)mes),
                new SugarParameter("@anio", (int)anio)
            });

            if (tabla != null)
            {
                if (tabla.Rows.Count > 0)
                {
                    Jaeger.Entities.Reportes.EstadoDeCuenta reporte = new Jaeger.Entities.Reportes.EstadoDeCuenta();
                    reporte.RFC = rfc;
                    reporte.Items = new BindingList<Jaeger.Entities.Reportes.EstadoDeCuentaComprobantes>();
                    reporte.Periodo = Enum.GetName(typeof(EnumMonthsOfYear), mes);
                    foreach (DataRow fila in tabla.Rows)
                    {
                        Jaeger.Entities.Reportes.EstadoDeCuentaComprobantes nuevo = new Jaeger.Entities.Reportes.EstadoDeCuentaComprobantes();
                        nuevo.Acumulado = Jaeger.Helpers.DbConvert.ConvertDecimal(fila["_cfdi_cbrd"]);
                        nuevo.Descuento = Jaeger.Helpers.DbConvert.ConvertDecimal(fila["_cfdi_dec"]);
                        nuevo.Saldo = Jaeger.Helpers.DbConvert.ConvertDecimal(fila["_cfdi_saldo"]);
                        nuevo.FechaEmision = Jaeger.Helpers.DbConvert.ConvertDateTime(fila["_cfdi_fecems"]);
                        nuevo.Descuento = Jaeger.Helpers.DbConvert.ConvertDecimal(fila["_cfdi_dec"]);
                        nuevo.FechaUltCobro = Jaeger.Helpers.DbConvert.ConvertDateTime(fila["_cfdi_fecupc"]);
                        nuevo.FechaVence = Jaeger.Helpers.DbConvert.ConvertDateTime(fila["_cfdi_fecvnc"]);
                        nuevo.Folio = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_folio"]);
                        nuevo.Serie = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_serie"]);
                        nuevo.Status = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_status"]);
                        nuevo.SubTotal = Jaeger.Helpers.DbConvert.ConvertDecimal(fila["_cfdi_sbttl"]);
                        nuevo.TipoComprobanteText = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_efecto"]);
                        nuevo.Total = Jaeger.Helpers.DbConvert.ConvertDecimal(fila["_cfdi_total"]);
                        reporte.Items.Add(nuevo);
                    }
                    return reporte;
                }
            }
            return null;
        }
        #endregion

        public List<ViewModelEstadoCuenta> Prueba(EnumCfdiSubType subtipo, string rfc, int anio, EnumMonthsOfYear mes)
        {
            List<ViewModelEstadoCuenta> lista = 
            this.Db.Queryable<ViewModelComprobante>()
                .WhereIF(subtipo == EnumCfdiSubType.Emitido, it => it.ReceptorRFC == rfc)
                .WhereIF(subtipo == EnumCfdiSubType.Recibido, it => it.EmisorRFC == rfc)
                .WhereIF(mes != EnumMonthsOfYear.None, it => it.FechaEmision.Month == (int)mes)
                .Where(it => it.FechaEmision.Year == anio)
                .Select(c => new ViewModelEstadoCuenta 
                { 
                    Tipo = c.TipoComprobanteText,
                    Status = c.StatusText,
                    Receptor = c.ReceptorNombre, 
                    RFC = c.ReceptorRFC,
                    IdDocumento = c.UUID,
                    Descuento = c.Descuento,
                    Acumulado = c.Acumulado,
                    Total = c.Total,
                    FechaEmision = c.FechaEmision,
                    FechaCobro = c.FechaUltPago
                }).ToList();
            return lista;
        }

        /// <summary>
        /// obtener resumen de comprobantes fiscales para tabla dinamica
        /// </summary>
        /// <param name="subtipo">sub tipo de comprobante Emitido, Recibido, Nomina</param>
        /// <param name="anio">año</param>
        /// <param name="mes">mes</param>
        /// <returns></returns>
        public List<ViewModelEstadoCuenta> Resumen(EnumCfdiSubType subtipo, int anio, EnumMonthsOfYear mes)
        {
            if (subtipo == EnumCfdiSubType.Emitido)
            {
                List<ViewModelEstadoCuenta> lista =
                this.Db.Queryable<ViewModelComprobante>()
                    .WhereIF(mes != EnumMonthsOfYear.None, it => it.FechaEmision.Month == (int)mes)
                    .Where(it => it.FechaEmision.Year == anio)
                    .Where(it => it.SubTipo == subtipo)
                    .Select(c => new ViewModelEstadoCuenta
                    {
                        Tipo = c.TipoComprobanteText,
                        Status = c.StatusText,
                        Receptor = c.ReceptorNombre,
                        RFC = c.ReceptorRFC,
                        IdDocumento = c.UUID,
                        Descuento = c.Descuento,
                        Acumulado = c.Acumulado,
                        Total = c.Total,
                        FechaEmision = c.FechaEmision,
                        FechaCobro = c.FechaUltPago
                    }).ToList();
                return lista;
            }
            else if (subtipo == EnumCfdiSubType.Recibido)
            {
                List<ViewModelEstadoCuenta> lista =
                this.Db.Queryable<ViewModelComprobante>()
                    .WhereIF(mes != EnumMonthsOfYear.None, it => it.FechaEmision.Month == (int)mes)
                    .Where(it => it.FechaEmision.Year == anio)
                    .Where(it => it.SubTipo == subtipo)
                    .Select(c => new ViewModelEstadoCuenta
                    {
                        Tipo = c.TipoComprobanteText,
                        Status = c.StatusText,
                        Receptor = c.EmisorNombre,
                        RFC = c.EmisorRFC,
                        IdDocumento = c.UUID,
                        Descuento = c.Descuento,
                        Acumulado = c.Acumulado,
                        Total = c.Total,
                        FechaEmision = c.FechaEmision,
                        FechaCobro = c.FechaUltPago
                    }).ToList();
                return lista;
            }
            return null;
        }

        public DataTable Prueba3(int ejercicio, EnumCfdiSubType subtipo, string estado, string rfc = "")
        {
            if (subtipo == EnumCfdiSubType.Emitido)
            {
                return this.Db.Queryable("_cfdi", "_cfdi")
                .Where("_cfdi._cfdi_doc_id = @tipo")
                .AddParameters(new { tipo = subtipo })
                .Where("EXTRACT(YEAR FROM _cfdi._cfdi_fecems) = @anio")
                .AddParameters(new { anio = ejercicio })
                .Where("_cfdi._cfdi_efecto LIKE 'Ingreso'")
                .WhereIF(estado != "Todos", "_cfdi._cfdi_status = @status").AddParameters(new { status = estado })
                .WhereIF(rfc != "", "_cfdi._cfdi_rfcr = @rfc").AddParameters(new { rfc = rfc })
                .GroupBy("_cfdi._cfdi_rfcr, EXTRACT(month FROM _cfdi._cfdi_fecems)")
                .OrderBy("_cfdi._cfdi_rfcr,_cfdi._cfdi_fecems asc")
                .Select("_cfdi._cfdi_nomr Receptor, _cfdi._cfdi_rfcr RFC, ELT(EXTRACT(MONTH FROM _cfdi._cfdi_fecems),'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre') Mes, AVG(DATEDIFF(_cfdi_fecupc, _cfdi_fecems)) DiasCobro, count(_cfdi_uuid) TotalComprobantes, sum(_cfdi_total) TotalFacturado, sum(_cfdi_cbrd) TotalCobrado, AVG(_cfdi_total) PromedioFacturado, AVG(_cfdi_cbrd) PromedioCobrado").ToDataTable();
            }
            else
            {
                return this.Db.Queryable("_cfdi", "_cfdi")
                .Where("_cfdi._cfdi_doc_id = @tipo")
                .AddParameters(new { tipo = subtipo })
                .Where("EXTRACT(YEAR FROM _cfdi._cfdi_fecems) = @anio")
                .AddParameters(new { anio = ejercicio })
                .Where("_cfdi._cfdi_efecto LIKE 'Ingreso'")
                .WhereIF(estado != "Todos", "_cfdi._cfdi_status = @status").AddParameters(new { status = estado })
                .WhereIF(rfc != "", "_cfdi._cfdi_rfce = @rfc").AddParameters(new { rfc = rfc })
                .GroupBy("_cfdi._cfdi_rfce, EXTRACT(month FROM _cfdi._cfdi_fecems)")
                .OrderBy("_cfdi._cfdi_rfce,_cfdi._cfdi_fecems asc")
                .Select("_cfdi._cfdi_nome Receptor, _cfdi._cfdi_rfce RFC, ELT(EXTRACT(MONTH FROM _cfdi._cfdi_fecems),'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre') Mes, AVG(DATEDIFF(_cfdi_fecupc, _cfdi_fecems)) DiasCobro, count(_cfdi_uuid) TotalComprobantes, sum(_cfdi_total) TotalFacturado, sum(_cfdi_cbrd) TotalCobrado, AVG(_cfdi_total) PromedioFacturado, AVG(_cfdi_cbrd) PromedioCobrado").ToDataTable();
            }
            //"SELECT _cfdi_nomr Receptor, EXTRACT(MONTH FROM _cfdi._cfdi_fecems) Mes, AVG(DATEDIFF(_cfdi_fecupc, _cfdi_fecems)) DiasCobro, count(_cfdi_uuid) TotalComprobantes, sum(_cfdi_total) TotalFacturado, sum(_cfdi_cbrd) TotalCobrado, AVG(_cfdi_total) PromedioFacturado, AVG(_cfdi_cbrd) PromedioCobrado " +
            //"FROM _cfdi " +
            //"WHERE _cfdi._cfdi_doc_id = 1 " +
            //"    AND EXTRACT(YEAR FROM _cfdi._cfdi_fecems) = 2019 " +
            //"    AND EXTRACT(MONTH FROM _cfdi._cfdi_fecems) >= 3 " +
            //"    AND _cfdi_efecto LIKE 'Ingreso' " +
            //"GROUP BY _cfdi._cfdi_rfcr, EXTRACT(month FROM _cfdi._cfdi_fecems) " +
            //"ORDER BY _cfdi._cfdi_rfcr,_cfdi._cfdi_fecems desc ");
            //return this.Db.Queryable<ViewModelComprobantes>()
            //    .Where(it => it.SubTipo == EnumCfdiSubType.Emitido)
            //    .Where(it => it.FechaEmision.Year == 2019)
            //    .GroupBy(it => it.ReceptorRfc)
            //    .OrderBy(it => it.ReceptorRfc)
            //    .Select(it => new { Nombre = it.ReceptorNombre, Mes = it.FechaEmision.Month, TotalFacturado = SqlFunc.AggregateSum(it.Total), TotalCobrado = SqlFunc.AggregateSum(it.Cobrado), TotalComprobantes = SqlFunc.AggregateCount(it.IdDocumento), PromedioFacturado = SqlFunc.AggregateAvg(it.Total) })
            //    .ToDataTable();
        }

        /// <summary>
        /// actualizar objeto Complemento de pagos, con las partidas de los comprobantes relacionados
        /// </summary>
        public bool AplicarComprobantePagos(ComplementoPagos objeto)
        {
            if (objeto != null)
            {
                if (objeto.Pago != null)
                {
                    // recorremos la lista de objetos de pago
                    foreach (ComplementoPagosPago pago in objeto.Pago)
                    {
                        // recorremos la lista de documentos relacionados
                        if (pago.DoctoRelacionado != null)
                        {
                            foreach (ComplementoPagoDoctoRelacionado docto in pago.DoctoRelacionado)
                            {
                                List<ComplementoPagoDoctoRelacionado> tabla = this.Rep(docto.IdDocumento);
                                decimal suma = docto.ImpPagado;
                                if (tabla != null)
                                {
                                    if (tabla.Count > 0)
                                    {
                                        suma = tabla.Where(p => Jaeger.Helpers.HelperValidacion.UUID(p.IdDocumento)).Sum(p => p.ImpPagado);
                                    }
                                }
                                this.Db.Updateable<ViewModelComprobante>().SetColumns(it => new ViewModelComprobante() { Acumulado = suma, Parcialidad = docto.NumParcialidad, FechaRecepcionPago = pago.FechaPago }).Where(it => it.UUID == docto.IdDocumento).ExecuteCommand();
                            }
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// aplicar reglas a los cfdi relacionados del comprobante fiscal
        /// </summary>
        public bool AplicarCFDIRelacionados(ComprobanteCfdiRelacionados objeto)
        {
            if (objeto != null)
            {
                // para la clave de notas de credito relacionadas
                if (objeto.TipoRelacion.Clave == "01" | objeto.TipoRelacion.Clave == "02" | objeto.TipoRelacion.Clave == "03")
                {
                    foreach (ComprobanteCfdiRelacionadosCfdiRelacionado item in objeto.CfdiRelacionado)
                    {
                        decimal suma = 0;
                        List<ComplementoDoctoRelacionado> notas = this.Notas(item.IdDocumento);
                        if (notas != null)
                        {
                            if (notas.Count > 0)
                            {
                                suma = notas.Where(p => Jaeger.Helpers.HelperValidacion.UUID(p.IdDocumento)).Sum(p => p.Total);
                            }
                        }
                        //Entities.Base.ComprobanteGeneral d = this.GetComprobanteGeneral(item.IdDocumento);
                        string CommandText = "update _cfdi set _cfdi_dscntc=@descuento where _cfdi_uuid=@index";
                        return this.Db.Ado.ExecuteCommand(CommandText, new List<SugarParameter>()
                        {
                            new SugarParameter("@descuento", suma),
                            new SugarParameter("@@index", item.IdDocumento)
                        }) > 0;
                        
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// obtener notas de credito relacionados al comprobante fiscal
        /// </summary>
        public List<ComplementoDoctoRelacionado> Notas(string folioFiscal)
        {
            if (Jaeger.Helpers.HelperValidacion.UUID(folioFiscal))
            {
                string CommandText = string.Concat("select _cfdi_doc_id, _cfdi_estado,_cfdi_folio, _cfdi_serie, _cfdi_uuid,_cfdi_rfce,_cfdi_rfcr,_cfdi_nome,_cfdi_nomr, _cfdi_fecems, _cfdi_comrel, _cfdi_total, _cfdi_moneda,_cfdi_frmpg,_cfdi_mtdpg,_cfdi_par,_cfdi_estado,(_cfdi_total - (_cfdi_cbrd + _cfdi_dscntc)) as _cfdi_saldo ",
                        "from _cfdi where _cfdi_comrel like '%buscar%'");
                CommandText = CommandText.Replace("buscar", folioFiscal);
                DataTable tabla = this.Db.Ado.GetDataTable(CommandText);
                List<ComplementoDoctoRelacionado> lista = new List<ComplementoDoctoRelacionado>();
                foreach (DataRow fila in tabla.Rows)
                {
                    ComprobanteCfdiRelacionados t = ComprobanteCfdiRelacionados.Json(Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_comrel"]));
                    if (t != null)
                    {
                        ComplementoDoctoRelacionado docto = new ComplementoDoctoRelacionado();
                        docto.TipoRelacion.Clave = t.TipoRelacion.Clave;
                        docto.TipoRelacion.Descripcion = t.TipoRelacion.Descripcion;
                        docto.SubTipoText = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_doc_id"]);
                        docto.Folio = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_folio"]);
                        docto.Serie = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_serie"]);
                        docto.IdDocumento = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_uuid"]);
                        docto.FechaEmision = Jaeger.Helpers.DbConvert.ConvertDateTime(fila["_cfdi_fecems"]);
                        docto.Total = Jaeger.Helpers.DbConvert.ConvertDecimal(fila["_cfdi_total"]);
                        docto.EstadoSAT = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_estado"]);
                        docto.Moneda = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_moneda"]);
                        docto.FormaPago = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_frmpg"]);
                        docto.MetodoPago = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_mtdpg"]);
                        docto.EstadoSAT = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_estado"]);
                        if (docto.SubTipo == EnumCfdiSubType.Emitido)
                        {
                            docto.RFC = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_rfcr"]);
                            docto.Nombre = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_nomr"]);
                        }
                        else if (docto.SubTipo == EnumCfdiSubType.Recibido)
                        {
                            docto.RFC = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_rfce"]);
                            docto.Nombre = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_nome"]);
                        }
                        lista.Add(docto);
                    }
                }
                return lista;
            }
            return null;
        }

        /// <summary>
        /// Retorna lista de archivos para descarga
        /// </summary>
        public List<ViewModelBackupComprobante> GetBackupFiles(EnumCfdiSubType subTipo, int anio, int mes = 0, EnumCfdiDates porFecha = EnumCfdiDates.FechaTimbre)
        {
            string commandText = "";
            if (subTipo == EnumCfdiSubType.Nomina)
                commandText = string.Concat("select _cfdi_id as id,_cfdi_a as activo,_cfdi_doc_id,_cfdi_doc,concat('cfdi-',_cfdi_rfce,'-',_cfdi_rfcr,'-',_cfdi_uuid,'-',date_format(_cfdi_fecems,'%Y-%m-%d-%H%i%s')) as _cfdi_keyname,_cfdi_doc,_cfdnmn_tipo,_cfdi_rfce,_cfdi_nome,_cfdi_rfcr,_cfdi_nomr,_cfdi_feccert,_cfdnmn_fchpgo,_cfdi_uuid,year(fecha) as anio,month(fecha) as mes, _cfdnmn_depto as depto, _cfdi_url_xml,_cfdi_url_pdf,_cfdi_url_pdfacu,_cfdi_url_xmlacu,_nmnctrl_tp, _cfdi_save ",
                    "from _cfdi, _cfdnmn, _nmnctrl ",
                    "where _cfdi_a=1 and _cfdnmn_cfdi_id=_cfdi_id and year(fecha)=@anio and month(fecha)=@mes and _cfdi_doc_id=@tipo and _cfdnmn_nmnctrl_id=_nmnctrl_id and _cfdi_uuid is not null;");
            else if (subTipo == EnumCfdiSubType.Emitido)
                commandText = string.Concat("select _cfdi_id as id,_cfdi_a as activo,_cfdi_doc_id,_cfdi_doc,concat('cfdi-',_cfdi_rfce,'-',_cfdi_rfcr,'-',_cfdi_uuid,'-',date_format(_cfdi_fecems,'%Y-%m-%d-%H%i%s')) as _cfdi_keyname,_cfdi_doc,_cfdi_rfce,_cfdi_nome,_cfdi_rfcr,_cfdi_nomr,_cfdi_feccert,_cfdi_uuid,year(fecha) as anio,month(fecha) as mes, _cfdi_url_xml,_cfdi_url_pdf,_cfdi_url_pdfacu,_cfdi_url_xmlacu, _cfdi_save ",
                    "from _cfdi ",
                    "where _cfdi_a=1 and year(fecha)=@anio and month(fecha)=@mes and _cfdi_doc_id=@tipo and _cfdi_uuid is not null;");
            else if (subTipo == EnumCfdiSubType.Recibido)
                commandText = string.Concat("select _cfdi_id as id,_cfdi_a as activo,_cfdi_doc_id,_cfdi_doc,concat('cfdi-',_cfdi_rfce,'-',_cfdi_rfcr,'-',_cfdi_uuid,'-',date_format(_cfdi_fecems,'%Y-%m-%d-%H%i%s')) as _cfdi_keyname,_cfdi_doc,_cfdi_rfce,_cfdi_nome,_cfdi_rfcr,_cfdi_nomr,_cfdi_feccert,_cfdi_uuid,year(fecha) as anio,month(fecha) as mes, _cfdi_url_xml,_cfdi_url_pdf,_cfdi_url_pdfacu,_cfdi_url_xmlacu, _cfdi_save ",
                    "from _cfdi ",
                    "where _cfdi_a=1 and year(fecha)=@anio and month(fecha)=@mes and _cfdi_doc_id=@tipo and _cfdi_uuid is not null;");

            if (porFecha == EnumCfdiDates.FechaTimbre)
                commandText = commandText.Replace("fecha", "_cfdi_feccert");
            else if (porFecha == EnumCfdiDates.FechaEmision)
                commandText = commandText.Replace("fecha", "_cfdi_fecems");
            else if (porFecha == EnumCfdiDates.FechaDePago)
                commandText = commandText.Replace("fecha", "_cfdi_fecupc");
            else if (porFecha == EnumCfdiDates.FechaValidacion)
                commandText = commandText.Replace("fecha", "_cfdi_fecval");

            DataTable tabla = this.Db.Ado.GetDataTable(commandText, new List<SugarParameter> { new SugarParameter("@tipo", subTipo),
                                                                                               new SugarParameter("@anio", anio),
                                                                                               new SugarParameter("@mes", mes) });

            if (tabla != null)
            {
                List<ViewModelBackupComprobante> lista = new List<ViewModelBackupComprobante>();
                foreach (DataRow fila in tabla.Rows)
                {
                    ViewModelBackupComprobante objeto = new ViewModelBackupComprobante();
                    objeto.SubTipo = subTipo;
                    objeto.Id = Jaeger.Helpers.DbConvert.ConvertInt32(fila["id"]);
                    objeto.KeyName = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_keyname"]);
                    objeto.Ejercicio = Jaeger.Helpers.DbConvert.ConvertInt32(fila["anio"]);
                    objeto.Periodo = Jaeger.Helpers.DbConvert.ConvertInt32(fila["mes"]);
                    objeto.UrlPdf = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_url_pdf"]);
                    objeto.UrlXml = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_url_xml"]);
                    objeto.UrlPdfAcuse = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_url_pdfacu"]);
                    objeto.IdComprobante = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_uuid"]);
                    objeto._cfdi_save = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_save"]);
                    objeto.Categoria = null;
                    objeto.Nomina = null;

                    if (subTipo == EnumCfdiSubType.Nomina)
                    {
                        objeto.Categoria = Jaeger.Helpers.DbConvert.ConvertString(fila["depto"]);
                        objeto.Nomina = Jaeger.Helpers.DbConvert.ConvertString(fila["_nmnctrl_tp"]);
                        objeto.Nombre = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_nomr"]);
                        objeto.RFC = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_rfcr"]);
                    }
                    else if (subTipo == EnumCfdiSubType.Recibido)
                    {
                        objeto.Nombre = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_nome"]);
                        objeto.RFC = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_rfce"]);
                    }
                    else if (subTipo == EnumCfdiSubType.Emitido)
                    {
                        objeto.Nombre = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_nomr"]);
                        objeto.RFC = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_rfcr"]);
                        objeto.Categoria = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_doc"]);
                    }
                    lista.Add(objeto);
                }
                return lista;
            }
            return null;
        }

        public List<ViewModelBackup> GetInfoBackUp(EnumCfdiSubType objType, EnumCfdiDates porFecha)
        {
            string commandText = string.Concat("select concat(year(_cfdi_feccert),'0') as Indice,0 as ParentId,year(_cfdi_feccert) as anio,0 as mes, count(_cfdi_uuid) as conta, year(_cfdi_feccert) as nombre from _cfdi where _cfdi_doc_id=@index and _cfdi_uuid<>'' and _cfdi_feccert is not null group by anio union ",
                                               "select concat(year(_cfdi_feccert),month(_cfdi_feccert)) as Indice,date_format(_cfdi_feccert,'%Y%0') as ParentId,year(_cfdi_feccert) as anio,month(_cfdi_feccert) as mes, count(_cfdi_uuid) as conta,month(_cfdi_feccert) as nombre from _cfdi where _cfdi_doc_id=@index and _cfdi_uuid<>'' and _cfdi_feccert is not null group by anio,mes");

            if (porFecha == EnumCfdiDates.FechaEmision)
            {
                commandText = commandText.Replace("_cfdi_feccert", "_cfdi_fecems");
            }
            else if (porFecha == EnumCfdiDates.FechaValidacion)
            {
                commandText = commandText.Replace("_cfdi_feccert", "_cfdi_fecval");
                if (porFecha == EnumCfdiDates.FechaTimbre)
                {
                }
            }
            else if (porFecha == EnumCfdiDates.FechaTimbre)
            {
            }
            else
                return null;

            DataTable tabla = this.Db.Ado.GetDataTable(commandText, new List<SugarParameter> { new SugarParameter("@index", objType) });
            DataNamesMapper<ViewModelBackup> mapper = new DataNamesMapper<ViewModelBackup>();
            return mapper.Map(tabla).ToList();
        }

        public Jaeger.Entities.Reportes.EstadoCuentaComprobantes Reporte(string rfc, EnumCfdiSubType objType, string status, EnumMonthsOfYear mes = 0, int anio = 0, EnumCfdiDates fechas = EnumCfdiDates.FechaEmision) {
            return this.Data2.Reporte(rfc, objType, status, mes, anio, fechas);
        }
        #region metodos privados

        private BindingList<ViewModelComprobanteSingleC> GetListBy(EnumCfdiSubType subtipo, string status, EnumMonthsOfYear mes = 0, int anio = 0)
        {
            return new BindingList<ViewModelComprobanteSingleC>(
                this.Db.Queryable<ViewModelComprobanteSingleC>()
                    .Where(it => it.FechaEmision.Month == (int)mes)
                    .Where(it => it.FechaEmision.Year == anio)
                    .Where(it => it.SubTipo == subtipo)
                    .WhereIF(status != "Todos", it => it.Status == status).ToList());
        }

        private BindingList<ViewModelComprobanteSingleC> GetAdoListBy(EnumCfdiSubType subtipo, string status, EnumMonthsOfYear mes = 0, int anio = 0)
        {
            string sqlCommand = string.Concat("select _cfdi.*, (_cfdi_total - (_cfdi_cbrd + _cfdi_dscntc)) as _cfdi_saldo, if (_cfdi_frmpg='99' and _cfdi_mtdpg='PPD' and _cfdi_efecto like 'I%', (_cfdi_total - (_cfdi_cbrdp + _cfdi_dscntc)), 0) as _cfdi_saldopagos ",
                                              "from _cfdi where _cfdi_a = 1 and _cfdi_doc_id=@tipo status buscar anios order by _cfdi_fecems desc;");

            if (status == "Todos")
                sqlCommand = sqlCommand.Replace("status", "");
            else
                sqlCommand = sqlCommand.Replace("status", "and _cfdi_status=@status");

            if (mes == EnumMonthsOfYear.None)
                sqlCommand = sqlCommand.Replace("buscar", "");
            else
                sqlCommand = sqlCommand.Replace("buscar", "and month(_cfdi_fecems)=@mes");

            if (anio == 0)
                sqlCommand = sqlCommand.Replace("anios", "");
            else
                sqlCommand = sqlCommand.Replace("anios", "and year(_cfdi_fecems)=@anio");

            //if (fechas == EnumCfdiDates.FechaTimbre)
            //{
            //    sqlCommand = sqlCommand.Replace("_cfdi_fecems", "_cfdi_feccert");
            //}
            //else if (fechas == EnumCfdiDates.FechaValidacion)
            //{
            //    sqlCommand = sqlCommand.Replace("_cfdi_fecems", "_cfdi_fecval");
            //}

            DataTable data = this.Db.Ado.GetDataTable(sqlCommand, new List<SugarParameter>()
                {
                    new SugarParameter("@tipo", (int)subtipo),
                    new SugarParameter("@mes", (int)mes),
                    new SugarParameter("@anio", anio)
                });

            DataNamesMapper<ViewModelComprobanteSingleC> mapper = new DataNamesMapper<ViewModelComprobanteSingleC>();
            return new BindingList<ViewModelComprobanteSingleC>(mapper.Map(data).ToList());
        }

        /// <summary>
        /// obtener listado de conceptos de un comprobante fiscal por su id
        /// </summary>
        /// <param name="indice">indice relacionado</param>
        /// <returns></returns>
        private BindingList<ViewModelComprobanteConcepto> GetConceptos(int indice)
        {
            return new BindingList<ViewModelComprobanteConcepto>(this.Db.Queryable<ViewModelComprobanteConcepto>().Where(it => it.SubId == indice).Where(it => it.IsActive == true).ToList());
        }

        private BindingList<ViewModelComprobanteConcepto> GetAdoConceptos(int indice)
        {
            string sqlCommand = "select * from _cfdcnp where _cfdcnp_a = 1 and _cfdcnp_cfds_id = @index ";
            DataTable data = this.Db.Ado.GetDataTable(sqlCommand, new List<SugarParameter>()
            {
                new SugarParameter("@index", indice)
            });
            DataNamesMapper<ViewModelComprobanteConcepto> mapper = new DataNamesMapper<ViewModelComprobanteConcepto>();
            return new BindingList<ViewModelComprobanteConcepto>(mapper.Map(data).ToList());
        }

        #endregion
    }
}
