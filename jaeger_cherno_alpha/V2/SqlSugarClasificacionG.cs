﻿using System.Collections.Generic;
using SqlSugar;
using Jaeger.Edita.V2.Almacen.Entities;
using Jaeger.Edita.Interfaces;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class SqlSugarClasificacionG : MySqlSugarContext<ViewModelClasificacionG>, ISqlClasificacionG
    {
        public SqlSugarClasificacionG(DataBaseConfiguracion configuracion)
            : base(configuracion)
        {
        }

        /// <summary>
        /// obtener listado del arbol de clases
        /// </summary>
        /// <returns></returns>
        public List<ViewModelClase> GetListBy()
        {
            return this.Db.Queryable<ViewModelClasificacionG, ViewModelClasificacionG, ViewModelClasificacionG>((c1, c2, c3) => new JoinQueryInfos(
                JoinType.Inner, c2.SubId == c1.Id,
                JoinType.Inner, c3.SubId == c2.Id
            )).Select((c1, c2, c3) => new ViewModelClase
            {
                Id = c3.Id,
                SubId = c2.Id,
                Tipo = SqlFunc.IF(c1.Clase == "" && c2.Clase == "").Return(c3.Clase).ElseIF(c1.Clase == "").Return(c2.Clase).End(c1.Clase),
                Clase = SqlFunc.IIF(c2.Clase == "", c3.Clase, c2.Clase),
                Descripcion = c3.Clase
            }).ToList();
        }
    }
}
