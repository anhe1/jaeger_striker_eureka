﻿using System.ComponentModel;
using Jaeger.Edita.Interfaces;
using SqlSugar;
using Jaeger.Edita.V2.Almacen.Entities;
using Jaeger.Edita.V2.Almacen.Enums;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class SqlSugarProdServ : MySqlSugarContext<ViewModelProductoServicioSingle>, ISqlProdServ
    {
        public SqlSugarProdServ(DataBaseConfiguracion configuracion) : base(configuracion)
        {
            this.Categorias = new SqlSugarClasificacion(configuracion);
            this.Unidades = new SqlSugarAlmacenUnidades(configuracion);
            this.Modelos = new SqlSugarModelos(configuracion);
            this.Proveedor = new SqlSugarProdServProveedor(configuracion);
        }

        public SqlSugarClasificacion Categorias { get; set; }

        public SqlSugarAlmacenUnidades Unidades { get; set; }

        public ISqlModelos Modelos { get; set; }

        public ISqlProdServProveedor Proveedor { get; set; }

        public ProductoModelo Save(ProductoModelo producto)
        {
            if (producto.IdProducto == 0)
            {
                producto.IdProducto = this.Insert(producto);
                producto.Modelo.IdModelo = this.Modelos.Insert(producto.Modelo);
            }
            else
            {
                if (this.Update(producto) > 0)
                    this.Modelos.Update(producto.Modelo);
            }
            return producto;
        }

        public ProductoModelo GetById(int identificador)
        {
            return
            this.Db.Queryable<ViewModelProductoServicioSingle, ViewModelModelo>((producto, modelo) => new JoinQueryInfos(JoinType.Left, producto.IdProducto == modelo.IdProducto)).Where((producto, modelo) => modelo.Activo == true && producto.IdProducto == identificador).Select((producto, modelo) => new ProductoModelo
            {
                IdProducto = producto.IdProducto,
                IdCategoria = producto.IdCategoria,
                Nombre = producto.Nombre,
                IdModelo = modelo.IdModelo,
                Activo = modelo.Activo,
                Almacen = modelo.Almacen,
                Alto = modelo.Alto,
                Ancho = modelo.Ancho,
                ClaveProdServ = modelo.ClaveProdServ,
                ClaveUnidad = modelo.ClaveUnidad,
                Codigo = modelo.Codigo,
                Creo = modelo.Creo,
                Descripcion = modelo.Descripcion,
                Especificacion = modelo.Especificacion,
                Etiquetas = modelo.Etiquetas,
                Existencia = modelo.Existencia,
                FechaModifica = modelo.FechaModifica,
                FechaNuevo = modelo.FechaNuevo,
                IdUnidad = modelo.IdUnidad,
                Largo = modelo.Largo,
                Marca = modelo.Marca,
                Maximo = modelo.Maximo,
                Minimo = modelo.Minimo,
                Modifica = modelo.Modifica,
                NoIdentificacion = modelo.NoIdentificacion,
                NumRequerimiento = modelo.NumRequerimiento,
                Peso = modelo.Peso,
                ReOrden = modelo.ReOrden,
                Tipo = modelo.Tipo,
                Unidad = modelo.Unidad,
                UnidadXY = modelo.UnidadXY,
                UnidadZ = modelo.UnidadZ,
                Unitario = modelo.Unitario,
                Modelo = modelo
            }).Single();
        }

        public ProductoModelo GetByIdentificador(string identificador)
        {
            return
            this.Db.Queryable<ViewModelProductoServicioSingle, ViewModelModeloSingle>((producto, modelo) => new JoinQueryInfos(JoinType.Left, producto.IdProducto == modelo.IdProducto)).Where((producto, modelo) => modelo.Activo == true && modelo.NoIdentificacion == identificador).Select((producto, modelo) => new ProductoModelo
            {
                IdProducto = producto.IdProducto,
                IdCategoria = producto.IdCategoria,
                Nombre = producto.Nombre,
                IdModelo = modelo.IdModelo,
                Activo = modelo.Activo,
                Almacen = modelo.Almacen,
                Alto = modelo.Alto,
                Ancho = modelo.Ancho,
                ClaveProdServ = modelo.ClaveProdServ,
                ClaveUnidad = modelo.ClaveUnidad,
                Codigo = modelo.Codigo,
                Creo = modelo.Creo,
                Descripcion = modelo.Descripcion,
                Especificacion = modelo.Especificacion,
                Etiquetas = modelo.Etiquetas,
                Existencia = modelo.Existencia,
                FechaModifica = modelo.FechaModifica,
                FechaNuevo = modelo.FechaNuevo,
                IdUnidad = modelo.IdUnidad,
                Largo = modelo.Largo,
                Marca = modelo.Marca,
                Maximo = modelo.Maximo,
                Minimo = modelo.Minimo,
                Modifica = modelo.Modifica,
                NoIdentificacion = modelo.NoIdentificacion,
                NumRequerimiento = modelo.NumRequerimiento,
                Peso = modelo.Peso,
                ReOrden = modelo.ReOrden,
                Tipo = modelo.Tipo,
                Unidad = modelo.Unidad,
                UnidadXY = modelo.UnidadXY,
                UnidadZ = modelo.UnidadZ,
                Unitario = modelo.Unitario
            }).Single();
        }

        public BindingList<ProductoModelo> GetList(EnumAlmacen tipo)
        {
            return new BindingList<ProductoModelo>(
            this.Db.Queryable<ViewModelProductoServicioSingle, ViewModelModeloSingle>(
                (producto, modelo) => new JoinQueryInfos(JoinType.Left, producto.IdProducto == modelo.IdProducto))
                .Where((producto, modelo) => producto.Almacen == tipo && producto.Activo == true)
                .Where((producto, modelo) => modelo.Almacen == tipo && modelo.Activo == true).Select((producto, modelo) => new ProductoModelo
            {
                IdProducto = producto.IdProducto,
                IdCategoria = producto.IdCategoria,
                Nombre = producto.Nombre,
                IdModelo = modelo.IdModelo,
                Activo = modelo.Activo,
                Almacen = modelo.Almacen,
                Alto = modelo.Alto,
                Ancho = modelo.Ancho,
                ClaveProdServ = modelo.ClaveProdServ,
                ClaveUnidad = modelo.ClaveUnidad,
                Codigo = modelo.Codigo,
                Creo = modelo.Creo,
                Descripcion = modelo.Descripcion,
                Especificacion = modelo.Especificacion,
                Etiquetas = modelo.Etiquetas,
                Existencia = modelo.Existencia,
                FechaModifica = modelo.FechaModifica,
                FechaNuevo = modelo.FechaNuevo,
                IdUnidad = modelo.IdUnidad,
                Largo = modelo.Largo,
                Marca = modelo.Marca,
                Maximo = modelo.Maximo,
                Minimo = modelo.Minimo,
                Modifica = modelo.Modifica,
                NoIdentificacion = modelo.NoIdentificacion,
                NumRequerimiento = modelo.NumRequerimiento,
                Peso = modelo.Peso,
                ReOrden = modelo.ReOrden,
                Tipo = modelo.Tipo,
                Unidad = modelo.Unidad,
                UnidadXY = modelo.UnidadXY,
                UnidadZ = modelo.UnidadZ,
                Unitario = modelo.Unitario
            }).ToList());
        }
        //public BindingList<ViewModelProductoServicioSingle> GetProductos(EnumAlmacen tipo)
        //{
        //    return new BindingList<ViewModelProductoServicioSingle>(this.Db.Queryable<ViewModelProductoServicioSingle>().Where(it => it.Almacen == tipo).ToList());
        //}
        //public ViewModelProductoServicioSingle Save(ViewModelProductoServicioSingle producto)
        //{
        //    if (producto.Id == 0)
        //    {
        //        producto.Id = this.Insert(producto);
        //    }
        //    else
        //    {
        //        this.Update(producto);
        //    }
        //    return producto;
        //}

        public bool CrearTablas()
        {
            return this.CreateTable();
        }
    }
}
