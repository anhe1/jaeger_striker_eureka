﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Edita.Enums;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.Contable.Entities;
using Jaeger.Edita.V2.Contable.Enums;
using Jaeger.Enums;
using Jaeger.Domain.Services.Mapping;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class SqlSugarContable : SqlSugarContext<ViewModelPrePoliza>, ISqlContable
    {
        public SqlSugarContable(DataBaseConfiguracion objeto) : base(objeto)
        {
            this.Banco = new SqlSugarContableBancos(objeto);
        }

        public ISqlContableBancos Banco { get; set; }

        #region operaciones con prepolizas

        /// <summary>
        /// calcular el numero de indentifiacion de la prepoliza
        /// </summary>
        public string NoIndetificacion(ViewModelPrePoliza objeto)
        {
            int mes = objeto.FechaEmision.Month;
            int anio = objeto.FechaEmision.Year;

            int indice = 0;
            try
            {
                indice = this.Db.Queryable<ViewModelPrePoliza>().Where(it => it.FechaEmision.Year == anio && it.FechaEmision.Month == mes &&
                                                                             it.TipoText == objeto.TipoText).Select(it => new { maximus = SqlSugar.SqlFunc.AggregateCount(it.FechaEmision) }).Single().maximus + 1;
            }
            catch (Exception)
            {
                indice = 0;
            }

            return string.Concat(
                objeto.TipoText[0],
                objeto.FechaEmision.ToString("yyMM"),
                indice.ToString("000#"));
        }

        /// <summary>
        /// calcular el numero consecutivo del recibo segun el tipo
        /// </summary>
        public long Folio(ViewModelPrePoliza objeto)
        {
            long folio;
            try
            {
                folio = this.Db.Queryable<ViewModelPrePoliza>().Where(it => it.TipoText == objeto.TipoText).Select(it => new { maximus = SqlSugar.SqlFunc.AggregateMax(it.Folio) }).Single().maximus + 1;
            }
            catch (Exception)
            {
                folio = 0;
            }

            return folio;
        }

        /// <summary>
        /// insertar un nuevo objeto ViewModelPrePoliza
        /// </summary>
        /// <param name="objeto">ViewModelPrePoliza</param>
        /// <returns>ViewModelPrePoliza</returns>
        public new ViewModelPrePoliza Insert(ViewModelPrePoliza objeto)
        {
            // calcular el folio por el tipo de movimiento
            objeto.Folio = this.Folio(objeto);

            // calcular el numero de idetificacion
            objeto.NoIndet = this.NoIndetificacion(objeto);

            // insertar el nuevo registro
            objeto.Id = this.Db.Insertable(objeto).ExecuteReturnIdentity();

            // en caso de que tenga comprobantes relacionados
            if (objeto.Id > 0)
            {
                if (objeto.Comprobantes != null)
                {
                    if (objeto.Comprobantes.Count > 0)
                    {
                        // actualizar la lista de comprobantes con el numero de indetifiacion y el indice de la relacion de la prepoliza
                        objeto.Comprobantes = new BindingList<ViewModelPrePolizaComprobante>(objeto.Comprobantes.Select(c => { c.NoIndet = objeto.NoIndet; c.SubId = objeto.Id; return c; }).ToList<ViewModelPrePolizaComprobante>());
                        this.Db.Insertable(objeto.Comprobantes.ToArray()).ExecuteCommand();
                    }
                }
            }
            return objeto;
        }

        public new ViewModelPrePoliza Update(ViewModelPrePoliza objeto)
        {
            if (this.Db.Updateable(objeto).ExecuteCommand() > 0)
            {
                if (objeto.Comprobantes != null)
                {
                    if (objeto.Comprobantes.Count > 0)
                    {
                        // actualizar la lista de comprobantes con el numero de indetifiacion y el indice de la relacion de la prepoliza
                        objeto.Comprobantes = new BindingList<ViewModelPrePolizaComprobante>(objeto.Comprobantes.Select(c => { c.NoIndet = objeto.NoIndet; c.SubId = objeto.Id; return c; }).ToList<ViewModelPrePolizaComprobante>());
                        // actualizamos o insertamos
                        for (int i = 0; i < objeto.Comprobantes.Count; i++)
                        {
                            // si no tiene Id entonces lo insertamos
                            if (objeto.Comprobantes[i].Id == 0)
                            {
                                objeto.Comprobantes[i].Id = this.Db.Insertable(objeto.Comprobantes[i]).ExecuteReturnIdentity();
                            }
                            else
                            {
                                objeto.Comprobantes[i].FechaMod = DateTime.Now;
                                objeto.Comprobantes[i].Modifica = objeto.Modifica;
                                this.Db.Updateable(objeto.Comprobantes[i]).ExecuteCommand();
                            }
                        }
                    }
                }
            }
            return objeto;
        }

        public ViewModelPrePoliza GetPrePoliza(string noident)//
        {
            ViewModelPrePoliza response = this.Db.Queryable<ViewModelPrePoliza>().Where(it => it.NoIndet == noident).Single();
            if (response != null)
            {
                //response.Comprobantes = new BindingList<ViewModelPrePolizaComprobante>(this.Db.Queryable<ViewModelPrePolizaComprobante>().Where(it => it.NoIndet == response.NoIndet && it.IsActive == true).ToList());
                response.Comprobantes = this.GetComprobantes(noident);
            }
            return response;
        }

        public BindingList<ViewModelPrePolizaComprobante> GetComprobantes(string noIdentificacion)
        {
            //return new BindingList<ViewModelPrePolizaComprobante>(this.Db.Queryable<ViewModelPrePolizaComprobante>().Where(it => it.NoIndet == noIdentificacion && it.IsActive == true).ToList());
            string consulta = " SELECT  *  FROM `_cntbl3c` p Inner JOIN `_cfdi` c ON ((( `p`.`_cntbl3c_noiden` = @ident ) AND ( `c`.`_cfdi_id` = `p`.`_cntbl3c_idcom` )) AND ( `p`.`_cntbl3c_a` = 1 ))";
            System.Data.DataTable tabla = this.Db.Ado.GetDataTable(consulta, new List<SugarParameter> { new SugarParameter("@ident", noIdentificacion) });
            var mapper = new DataNamesMapper<ViewModelPrePolizaComprobante>();
            return new BindingList<ViewModelPrePolizaComprobante>(mapper.Map(tabla).ToList());
        }

        public BindingList<ViewModelPrePoliza> GetPrePolizas(EnumPolizaTipo tipo, EnumMonthsOfYear mes = 0, int anio = 0, string cuenta = "todos")
        {
            return new BindingList<ViewModelPrePoliza>(this.Db.Queryable<ViewModelPrePoliza>().Where(it => it.IsActive == true).WhereIF(cuenta != "todos", it => it.EmisorNumCta == cuenta).WhereIF(mes != EnumMonthsOfYear.None, it => it.FechaEmision.Month == (int)mes).WhereIF(anio > 0, it => it.FechaEmision.Year == anio).WhereIF(tipo != EnumPolizaTipo.Ninguno, it => it.TipoText == Enum.GetName(typeof(EnumPolizaTipo), tipo)).OrderBy(it => it.Id, SqlSugar.OrderByType.Desc).ToList());
        }

        public BindingList<ViewModelPrePolizaComprobante> GetComprobantes(EnumCfdiSubType objtype, string emisor, string receptor, string status, string metodo = "", string efecto = "")
        {
            string commandText = string.Concat("select _cfdi.*, (_cfdi_total - (_cfdi_cbrd + _cfdi_dscntc)) as _cfdi_saldo,concat('cfdi-',_cfdi_rfce,'-',_cfdi_rfcr,'-',_cfdi_uuid,'-',date_format(_cfdi_fecems,'%Y-%m-%d-%H%i%s')) as _cfdi_keyname ",
                "from _cfdi ",
                "where _cfdi_a = 1 and _cfdi_rfce like '@emisor' and _cfdi_rfcr like '@receptor' and year(_cfdi_fecems) >= 2016 and _cfdi_a=1 and _cfdi_status like '@status' metodo and _cfdi_uuid is not null ",
                "efecto ",
                "order by _cfdi_fecems asc;");

            if (objtype == EnumCfdiSubType.Emitido)
            {
                commandText = commandText.Replace("@emisor", emisor);
                commandText = commandText.Replace("@receptor", receptor);
            }
            else if (objtype == EnumCfdiSubType.Recibido)
            {
                commandText = commandText.Replace("@emisor", receptor);
                commandText = commandText.Replace("@receptor", emisor);
            }

            if (metodo != "")
            {
                commandText = commandText.Replace("metodo", " and _cfdi_mtdpg like '@metodo' ");
            }
            else
            {
                commandText = commandText.Replace("metodo", "");
            }

            if (efecto != "")
            {
                commandText = commandText.Replace("efecto", "and _cfdi_efecto<>'@efecto'");
            }
            else
            {
                commandText = commandText.Replace("efecto", "");
            }

            commandText = commandText.Replace("@status", status);
            commandText = commandText.Replace("@metodo", metodo);
            commandText = commandText.Replace("@efecto", efecto);

            System.Data.DataTable tabla = this.Db.Ado.GetDataTable(commandText);
            if (tabla != null)
            {
                DataNamesMapper<ViewModelPrePolizaComprobante> mapper = new DataNamesMapper<ViewModelPrePolizaComprobante>();
                BindingList<ViewModelPrePolizaComprobante> lista = new BindingList<ViewModelPrePolizaComprobante>(mapper.Map(tabla).ToList());
                
                lista = new BindingList<ViewModelPrePolizaComprobante>(lista.Select(c => { c.Tipo = EnumMetadataType.CFDI; return c; }).ToList<ViewModelPrePolizaComprobante>());
                return lista;
            }
            return null;
        }

        //public BindingList<ViewModelPrePolizaComprobante> GetComprobantes(int indice, string clave, EnumRemisionFiscalStatus status)
        //{
        //    System.Collections.Generic.List<ViewModelRemision> lista =
        //    this.Db.Queryable<ViewModelRemision>()
        //        .Where(it => it.IsActive == true && it.StatusText == Enum.GetName(typeof(EnumRemisionFiscalStatus), status) && it.IdDirectorio == indice).ToList();
        //    BindingList<ViewModelPrePolizaComprobante> lista1 = new BindingList<ViewModelPrePolizaComprobante>();
        //    foreach (ViewModelRemision item in lista)
        //    {
        //        lista1.Add(new ViewModelPrePolizaComprobante(item));
        //    }
        //    return lista1;
        //}

        public BindingList<ViewModelPrePolizaComprobante> GetComprobantes11(EnumCfdiSubType objtype, string emisor, string receptor, string status, string metodo = "", string efecto = "")
        {
            var response = this.Db.Queryable<ViewModelComprobanteSingle>().Where(it => it.Activo == true)
                               .WhereIF(efecto != "", it => it.TipoComprobanteText != efecto)
                               .WhereIF(metodo != "", it => it.MetodoPago.Contains(metodo))
                               .WhereIF(objtype == EnumCfdiSubType.Emitido, it => it.EmisorRFC == emisor && it.ReceptorRFC == receptor)
                               .WhereIF(objtype == EnumCfdiSubType.Recibido, it => it.EmisorRFC == receptor && it.ReceptorRFC == emisor)
                               .WhereIF(status != "", it => it.Status == status)
                               .Select(it => new ViewModelPrePolizaComprobante
                                      {
                                          IdComprobante = it.Id,
                                          Version = it.Version,
                                          Abono = 0,
                                          Acumulado = it.Acumulado,
                                          Cargo = 0,
                                          ClaveFormaPago = it.FormaPago,
                                          ClaveMetodoPago = it.MetodoPago,
                                          ClaveMoneda = it.Moneda, Creo = "",
                                          Descuento = it.Descuento,
                                          Emisor = it.Emisor,
                                          EmisorRFC = it.EmisorRFC,
                                          Estado = it.Estado,
                                          FechaEmision = it.FechaEmision,
                                          FechaTimbre = it.FechaTimbrado,
                                          Folio = it.Folio,
                                          IsActive = it.Activo,
                                          NumParcialidad = it.NumParcialidad,
                                          PrecisionDecimal = it.PrecisionDecimal,
                                          Receptor = it.Receptor,
                                          ReceptorRFC = it.ReceptorRFC,
                                          RetencionIEPS = it.RetencionIEPS,
                                          RetencionISR = it.RetencionISR,
                                          RetencionIVA = it.RetencionIVA,
                                          Serie = it.Serie,
                                          Status = it.Status,
                                          SubTipoComprobante = it.SubTipo,
                                          Total = it.Total,
                                          TrasladoIVA =
                                              it.TrasladoIVA,
                                          UUID = it.IdDocumento,
                                          Subtotal = it.SubTotal,
                                          Tipo = EnumMetadataType.CFDI
                                      }).ToList();
            return new BindingList<ViewModelPrePolizaComprobante>(response);
        }

        public ViewModelPrePoliza Save(ViewModelPrePoliza objeto)
        {
            if (objeto.Id == 0)
            {
                return this.Insert(objeto);
            }
            else
            {
                return this.Update(objeto);
            }
        }

        public ViewModelPrePoliza CrearRecibo(List<ViewModelComprobanteSingleC> lista)
        {
            // recibo nuevo
            ViewModelPrePoliza nuevo = new ViewModelPrePoliza();
            // obtenemos la lista de documentos a procesar
            List<string> uuids = lista.Select(x => x.IdDocumento).ToList();
            // por si tienen complemento de pagos
            foreach (ViewModelComprobanteSingleC item in lista)
            {
                if (item.ComplementoPagos != null)
                {
                    foreach (var item2 in item.ComplementoPagos.Pago)
                    {
                        uuids.AddRange(item2.DoctoRelacionado.Where(x => x != null).Select(x => x.IdDocumento).ToList());
                    }
                }
            }
            // obtenemos la lista de documentos a procesar
            List<ViewModelComprobanteSingle> documentos = this.Db.Queryable<ViewModelComprobanteSingle>().In(it => it.IdDocumento, uuids).ToList();
            foreach (ViewModelComprobanteSingle item in documentos)
            {
                ViewModelPrePolizaComprobante documento = new ViewModelPrePolizaComprobante(item);
                documento.IsActive = true;
                documento.Abono = (documento.SubTipoComprobante == EnumCfdiSubType.Emitido ? documento.Total - documento.Acumulado : 0);
                documento.Cargo = (documento.SubTipoComprobante == EnumCfdiSubType.Recibido ? documento.Total - documento.Acumulado : 0);
                nuevo.Concepto = string.Concat(nuevo.Concepto, documento.Serie, " ", documento.Folio, " ");
                nuevo.Comprobantes.Add(documento);

                if (documento.SubTipoComprobante == EnumCfdiSubType.Emitido)
                {
                    nuevo.Tipo = EnumPolizaTipo.Ingreso;
                    nuevo.Receptor.Nombre = documento.Receptor;
                    nuevo.Receptor.Rfc = documento.ReceptorRFC;
                }
                else if (documento.SubTipoComprobante == EnumCfdiSubType.Recibido)
                {
                    nuevo.Tipo = EnumPolizaTipo.Egreso;
                    nuevo.Receptor.Nombre = documento.Emisor;
                    nuevo.Receptor.Rfc = documento.EmisorRFC;
                }
            }
            nuevo.NoIndet = "";
            nuevo.Concepto = string.Concat((nuevo.Tipo == EnumPolizaTipo.Ingreso ? "Cobro de factura " : "Pago de factura "), nuevo.Concepto);
            nuevo.FechaNuevo = DateTime.Now;
            return nuevo;
        }

        #endregion

        #region operaciones con cuentas de banco propias

        public ViewModelBancoMovimiento Aplicar(ViewModelBancoMovimiento objeto)
        {
            objeto.FechaMod = DateTime.Now;
            if (this.Db.Updateable(objeto).UpdateColumns(it => new { it.EstadoText, it.NumAutorizacion, it.Referencia, it.ReferenciaNumerica, it.JPrepoliza, it.Modifica, it.FechaMod }).ExecuteCommand() > 0)
            {
                this.Aplicar(objeto.NoIndet);
            }
            return objeto;
        }

        public BancoEstadoCuenta EstadoCuenta(EnumPolizaTipo tipo, EnumMonthsOfYear mes = 0, int anio = 0, ViewModelBancoCuenta cuenta = null)
        {
            BancoEstadoCuenta response = new BancoEstadoCuenta();
            response.Movimientos = new BindingList<ViewModelBancoMovimiento>(this.Db.Queryable<ViewModelBancoMovimiento>().Where(it => it.IsActive == true).WhereIF(cuenta != null, it => it.EmisorNumCta == cuenta.NumeroDeCuenta).WhereIF(mes != EnumMonthsOfYear.None, it => it.FechaEmision.Month == (int)mes).WhereIF(anio > 0, it => it.FechaEmision.Year == anio).WhereIF(tipo != EnumPolizaTipo.Ninguno, it => it.TipoText == Enum.GetName(typeof(EnumPolizaTipo), tipo)).OrderBy(it => it.FechaPago, SqlSugar.OrderByType.Asc).OrderBy(it => it.NoIndet, SqlSugar.OrderByType.Asc).ToList());
            if (cuenta != null)
            {
                int mes1 = (int)mes;
                if (mes1 != 0)
                {
                    response.Saldo = this.Banco.GetSaldo(cuenta, new DateTime(anio, mes1, 1));
                }
                else
                {
                    response.Saldo = null;
                }
                if (response.Saldo == null)
                {
                    response.Saldo = new ViewModelBancoSaldo() { SaldoInicial = 0 };
                }
            }
            return response;
        }

        public ViewModelPrePoliza Traspaso(ViewModelPrePoliza objeto)
        {
            
            objeto = this.Save(objeto);
            if (objeto.Id > 0)
            {
                // el sgundo movimiento es de tipo ingreso que es la cuenta de destino
                ViewModelPrePoliza mov2 = (ViewModelPrePoliza)objeto.Clone();
                if (mov2 != null)
                {
                    mov2.NoIndet = null;
                    mov2.Id = 0;
                    mov2.Tipo = EnumPolizaTipo.Ingreso;
                    mov2.Receptor = objeto.Emisor;
                    mov2.Emisor = objeto.Receptor;
                    mov2.Cargo = objeto.Abono;
                    mov2.Abono = 0;
                    mov2 = this.Save(mov2);
                    if (mov2.Id == 0)
                    {
                        //this.Message = new Jaeger.Entities.MessageError()
                        //{
                        //    Data = "No fué posible crear el segundo movimiento, por favor informe al administrador del sistema"
                        //};
                        //this.Message = new Entities.SqlSugarMessage()
                        //{
                        //    DateTime = DateTime.Now,
                        //    Type = "Error",
                        //    Value = "No fué posible crear el segundo movimiento, por favor informe al administrador del sistema"
                        //};
                    }
                }
            }
            return objeto;
        }

        public ViewModelBancoSaldo Save(ViewModelBancoSaldo objeto)
        {
            if (objeto.Id > 0)
            {
                this.Db.Updateable(objeto).ExecuteCommand();
                return objeto;
            }
            else
            {
                objeto.Mes = objeto.FechaInicial.Month;
                objeto.Anio = objeto.FechaInicial.Year;
                objeto.FechaNuevo = DateTime.Now;
                objeto.Id = this.Db.Insertable(objeto).ExecuteReturnIdentity();
                return objeto;
            }
        }
        
        #endregion

        /// <summary>
        /// aplicar el cambio de status de la prepoliza
        /// </summary>
        public bool AplicarStatus(string noident, string newStatus, string usuario)
        {
            if (newStatus == "Cancelado")
            {
                return this.Db.Updateable<ViewModelPrePoliza>().UpdateColumns(it => new ViewModelPrePoliza() { EstadoText = newStatus, Cancela = usuario, FechaCancela = DateTime.Now }).Where(it => it.NoIndet == noident).ExecuteCommand() > 0;
            }
            else if (newStatus == "NoAplicado")
            {
                return this.Db.Updateable<ViewModelPrePoliza>().UpdateColumns(it => new ViewModelPrePoliza() { EstadoText = newStatus }).Where(it => it.NoIndet == noident).ExecuteCommand() > 0;
            }
            else if (newStatus == "Aplicado")
            {
                return this.Db.Updateable<ViewModelPrePoliza>().UpdateColumns(it => new ViewModelPrePoliza() { EstadoText = newStatus, Modifica = usuario, FechaMod = DateTime.Now }).Where(it => it.NoIndet == noident).ExecuteCommand() > 0;
            }
            return false;
        }

        /// <summary>
        /// aplicar los cambios a los comprobantes relacionados a la prepoliza
        /// </summary>
        public bool Aplicar(string noident)
        {
            string commandText = string.Concat("select a._cntbl3_tipo as efecto, b._cntbl3c_status as status, b._cntbl3c_idcom as indice, b._cntbl3c_tipo as doc, b._cntbl3c_uuid as uuid, b._cntbl3c_total as total, a._cntbl3_json,",
                "(select sum(c._cntbl3c_abono) from _cntbl3c c, _cntbl3 d where d._cntbl3_noiden=c._cntbl3c_noiden and c._cntbl3c_idcom=b._cntbl3c_idcom and d._cntbl3_status='Aplicado') as abono,",
                "(select sum(c._cntbl3c_cargo) from _cntbl3c c, _cntbl3 d where d._cntbl3_noiden=c._cntbl3c_noiden and c._cntbl3c_idcom=b._cntbl3c_idcom and d._cntbl3_status='Aplicado') as cargo,",
                "(select max(d._cntbl3_fccbr) from _cntbl3c c, _cntbl3 d where d._cntbl3_noiden=c._cntbl3c_noiden and c._cntbl3c_idcom=b._cntbl3c_idcom and d._cntbl3_status='Aplicado') as fecha ",
                "from _cntbl3 a, _cntbl3c b where b._cntbl3c_noiden=a._cntbl3_noiden and a._cntbl3_noiden=@index ",
                "order by a._cntbl3_fccbr desc;");

            //CommandText = CommandText.Replace("@index", noident);
            System.Data.DataTable tabla = this.Db.Ado.GetDataTable(commandText, new SqlSugar.SugarParameter("@index", noident));
            bool resultado = false;

            if (tabla.Rows.Count > 0)
            {
                foreach (System.Data.DataRow item in tabla.Rows)
                {
                    string efecto = Jaeger.Helpers.DbConvert.ConvertString(item["efecto"]);
                    string doc = Jaeger.Helpers.DbConvert.ConvertString(item["doc"]);
                    string nuevoStatus = Jaeger.Helpers.DbConvert.ConvertString(item["status"]);
                    decimal abono = Jaeger.Helpers.DbConvert.ConvertDecimal(item["abono"]);
                    decimal cargo = Jaeger.Helpers.DbConvert.ConvertDecimal(item["cargo"]);
                    decimal total = Jaeger.Helpers.DbConvert.ConvertDecimal(item["total"]);
                    decimal saldo = total;
                    long indice = Jaeger.Helpers.DbConvert.ConvertInt32(item["indice"]);
                    DateTime fechaPago = Jaeger.Helpers.DbConvert.ConvertDateTime(item["fecha"]);

                    DateTime firstGooDate = new DateTime(1900, 1, 1);
                    if (!(fechaPago >= firstGooDate))
                    {
                        fechaPago = DateTime.Now;
                    }

                    EnumMetadataType tipo = (EnumMetadataType)Enum.Parse(typeof(EnumMetadataType), doc);

                    if (efecto == "Ingreso")
                    {
                        if (abono >= total)
                        {
                            nuevoStatus = Enum.GetName(typeof(EnumCfdiStatusIngreso), EnumCfdiStatusIngreso.Cobrado);
                        }
                        else
                        {
                            nuevoStatus = Enum.GetName(typeof(EnumCfdiStatusIngreso), EnumCfdiStatusIngreso.PorCobrar);
                        }
                        saldo = total - abono;
                    }
                    else if (efecto == "Egreso")
                    {
                        if (cargo >= total)
                        {
                            nuevoStatus = Enum.GetName(typeof(EnumCfdiStatusEgreso), EnumCfdiStatusEgreso.Pagado);
                        }
                        else
                        {
                            nuevoStatus = Enum.GetName(typeof(EnumCfdiStatusEgreso), EnumCfdiStatusEgreso.PorPagar);
                        }
                        saldo = total - cargo;
                        abono = cargo; // este solo para los comprobantes recibidos
                    }

                    // tipo de documento relacionado
                    if (tipo == EnumMetadataType.CFDI)
                    {
                        //CommandText = "update _cfdi set _cfdi_cbrd=@acumulado, _cfdi_status=@status, _cfdi_fecupc=@fechapago where _cfdi_id=@index";
                        resultado = this.Db.Updateable<ViewModelComprobanteSingle>().UpdateColumns(it => new ViewModelComprobanteSingle { Status = nuevoStatus, Acumulado = abono, FechaUltimoPago = fechaPago }).Where(it => it.Id == indice).ExecuteCommand() > 0;
                    }
                    else if (tipo == EnumMetadataType.Remision)
                    {
                        //CommandText = "update _rmsn set _rmsn_cbrd=@acumulado, _rmsn_status=@status, _rmsn_fecpgr=@fechapago, _rmsn_sld=@saldo where _rmsn_id=@index";
                        //resultado = this.Db.Updateable<ViewModelRemision>().UpdateColumns(it => new ViewModelRemision { StatusText = nuevoStatus, Acumulado = saldo, FechaPago = fechaPago }).Where(it => it.Id == indice).ExecuteCommand() > 0;
                    }
                    //sqlUpdate.Parameters.AddWithValue("@acumulado", abono);
                    //sqlUpdate.Parameters.AddWithValue("@fechapago", fechaPago);
                    //sqlUpdate.Parameters.AddWithValue("@status", nuevoStatus);
                    //sqlUpdate.Parameters.AddWithValue("@index", indice);
                    //sqlUpdate.Parameters.AddWithValue("@saldo", saldo);
                    //resultado = this.ExecuteTransaccion(sqlUpdate);
                }
            }
            return resultado;
        }

        /// <summary>
        /// replicar un contenido al conjunto de comprobantes relacionados a una prepoliza
        /// </summary>
        public bool Replicar(ViewModelPrePoliza objeto)
        {
            string commandText = string.Concat("update _cfdi ",
                                                "inner join ",
                                                "_cntbl3c on _cfdi_uuid = _cntbl3c_uuid ",
                                                "set _cfdi_obsrv = @contenido ",
                                                "where _cntbl3c._cntbl3c_noiden = @index;");

            string contenido = string.Concat("Docto: ", objeto.NumDocto, " ", objeto.Concepto, " ", objeto.Concepto);
            return this.Db.Ado.ExecuteCommand(commandText, new List<SugarParameter> { new SugarParameter("@index", objeto.NoIndet), new SugarParameter("@contenido", contenido) }) > 0;
        }

        public void Create()
        {
            this.Db.CodeFirst.InitTables(typeof(ViewModelBancoSaldo));
        }

        #region solo para revisiones

        public void Revisar(BindingList<ViewModelPrePoliza> datos)
        {
            foreach (var item in datos)
            {
                if (item.Estado == EnumPrePolizaStatus.Aplicado)
                {
                    this.Aplicar(item.NoIndet);
                }
            }
        }

        #endregion
    }
}