﻿/// develop: anhe 18062019 1715
/// purpose: control 
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Edita.V2.Nomina.Entities;

namespace Jaeger.Edita.V2
{
    public class SqlSugarControlNomina : MySqlSugarContext<ViewModelControlNominaSingle>
    {
        public SqlSugarControlNomina(DataBaseConfiguracion configuracion) : base(configuracion)
        {
        }
    }
}
