using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Edita.Interfaces;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class MySqlDirectorioDomicilio : MySqlSugarContext<ViewModelDomicilio>, ISqlContext<ViewModelDomicilio>
    {
        public MySqlDirectorioDomicilio(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public int Insert(ViewModelDomicilio objeto)
        {
            var sqlCommand = new MySqlCommand()
            {
                CommandText = string.Concat("insert into _drccn (_drccn_a,_drccn_drctr_id,_drccn_cp,_drccn_tp,_drccn_cll,_drccn_cln,_drccn_dlg,_drccn_cdd,_drccn_std,_drccn_extr,_drccn_intr,_drccn_ps,_drccn_lcldd,_drccn_rfrnc,_drccn_usr_n,_drccn_fn) ",
                                                        "values (@drccn_a,@drccn_drctr_id,@drccn_cp,@drccn_tp,@drccn_cll,@drccn_cln,@drccn_dlg,@drccn_cdd,@drccn_std,@drccn_extr,@drccn_intr,@drccn_ps,@drccn_lcldd,@drccn_rfrnc,@drccn_usr_n,@drccn_fn)")
            };

            sqlCommand.Parameters.AddWithValue("@drccn_a", 1);
            sqlCommand.Parameters.AddWithValue("@drccn_drctr_id", objeto.SubId);
            sqlCommand.Parameters.AddWithValue("@drccn_cp", objeto.CodigoPostal);
            sqlCommand.Parameters.AddWithValue("@drccn_tp", objeto.TipoText);
            sqlCommand.Parameters.AddWithValue("@drccn_cll", objeto.Calle);
            sqlCommand.Parameters.AddWithValue("@drccn_cln", objeto.Colonia);
            sqlCommand.Parameters.AddWithValue("@drccn_dlg", objeto.Municipio);
            sqlCommand.Parameters.AddWithValue("@drccn_cdd", objeto.Ciudad);
            sqlCommand.Parameters.AddWithValue("@drccn_std", objeto.Estado);
            sqlCommand.Parameters.AddWithValue("@drccn_extr", objeto.NoExterior);
            sqlCommand.Parameters.AddWithValue("@drccn_intr", objeto.NoInterior);
            sqlCommand.Parameters.AddWithValue("@drccn_ps", objeto.Pais);
            sqlCommand.Parameters.AddWithValue("@drccn_lcldd", objeto.Localidad);
            sqlCommand.Parameters.AddWithValue("@drccn_rfrnc", objeto.Referencia);
            sqlCommand.Parameters.AddWithValue("@drccn_usr_n", objeto.Creo);
            sqlCommand.Parameters.AddWithValue("@drccn_fn", DateTime.Now);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return objeto.Id;
            return 0;
        }

        public int Update(ViewModelDomicilio objeto)
        {
            var sqlUpdate = new MySqlCommand()
            {
                CommandText = "update _drccn set _drccn_a=@drccn_a,_drccn_cp=@drccn_cp,_drccn_tp=@drccn_tp,_drccn_cll=@drccn_cll,_drccn_cln=@drccn_cln,_drccn_dlg=@drccn_dlg,_drccn_cdd=@drccn_cdd,_drccn_std=@drccn_std,_drccn_extr=@drccn_extr,_drccn_intr=@drccn_intr,_drccn_ps=@drccn_ps,_drccn_lcldd=@drccn_lcldd,_drccn_rfrnc=@drccn_rfrnc,_drccn_usr_m=@drccn_usr_m,_drccn_fm=@drccn_fm where _drccn_id=@index "
            };
            sqlUpdate.Parameters.AddWithValue("@index", objeto.Id);
            sqlUpdate.Parameters.AddWithValue("@drccn_a", objeto.IsActive);
            sqlUpdate.Parameters.AddWithValue("@drccn_cp", objeto.CodigoPostal);
            sqlUpdate.Parameters.AddWithValue("@drccn_tp", objeto.TipoText);
            sqlUpdate.Parameters.AddWithValue("@drccn_cll", objeto.Calle);
            sqlUpdate.Parameters.AddWithValue("@drccn_cln", objeto.Colonia);
            sqlUpdate.Parameters.AddWithValue("@drccn_dlg", objeto.Municipio);
            sqlUpdate.Parameters.AddWithValue("@drccn_cdd", objeto.Ciudad);
            sqlUpdate.Parameters.AddWithValue("@drccn_std", objeto.Estado);
            sqlUpdate.Parameters.AddWithValue("@drccn_extr", objeto.NoExterior);
            sqlUpdate.Parameters.AddWithValue("@drccn_intr", objeto.NoInterior);
            sqlUpdate.Parameters.AddWithValue("@drccn_ps", objeto.Pais);
            sqlUpdate.Parameters.AddWithValue("@drccn_lcldd", objeto.Localidad);
            sqlUpdate.Parameters.AddWithValue("@drccn_rfrnc", objeto.Referencia);
            sqlUpdate.Parameters.AddWithValue("@drccn_usr_m", objeto.Modifica);
            sqlUpdate.Parameters.AddWithValue("@drccn_fm", DateTime.Now);
            return this.ExecuteScalar(sqlUpdate);
        }

        public bool Delete(int index)
        {
            throw new NotImplementedException();
        }

        public ViewModelDomicilio Save(ViewModelDomicilio objeto)
        {
            throw new NotImplementedException();
        }

        public ViewModelDomicilio GetById(int index)
        {
            var sqlCommand = new MySqlCommand() { CommandText = "select * from _drccn where _drccn_a = 1 and _drccn_id = @index " };
            sqlCommand.Parameters.AddWithValue("@index", index);
            DataTable data = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelDomicilio>();
            return mapper.Map(data).FirstOrDefault();
        }

        public List<ViewModelDomicilio> GetList()
        {
            var sqlCommand = new MySqlCommand() { CommandText = "select * from _drccn where _drccn_a = 1; " };
            DataTable data = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelDomicilio>();
            return mapper.Map(data).ToList();
        }

        public BindingList<ViewModelDomicilio> GetListBy(int index)
        {
            var sqlCommad = new MySqlCommand() { CommandText = "select * from _drccn where _drccn_a=1 and _drccn_drctr_id=@index" };
            sqlCommad.Parameters.AddWithValue("@index", index);
            DataTable data = this.ExecuteReader(sqlCommad);
            var mapper = new DataNamesMapper<ViewModelDomicilio>();
            return new BindingList<ViewModelDomicilio>(mapper.Map(data).ToList());
        }
        
    }
}