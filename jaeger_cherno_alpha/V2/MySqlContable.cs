﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using MySql.Data.MySqlClient;
using Jaeger.Edita.Enums;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2.Contable.Enums;
using Jaeger.Edita.V2.Contable.Entities;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class MySqlContable : MySqlSugarContext<ViewModelPrePoliza>, ISqlContable
    {
        public MySqlContable(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public ISqlContableBancos Banco { get; set; }

        public string NoIndetificacion(ViewModelPrePoliza objeto)
        {
            var sqlFolio = new MySqlCommand { CommandText = "select count(_cntbl3_fecems) + 1 from _cntbl3 where year(_cntbl3_fecems) = @anio and month(_cntbl3_fecems) = @mes and _cntbl3_tipo =@tipo;" };
            sqlFolio.Parameters.AddWithValue("@anio", objeto.FechaEmision.Year);
            sqlFolio.Parameters.AddWithValue("@mes", objeto.FechaEmision.Month);
            sqlFolio.Parameters.AddWithValue("@tipo", objeto.TipoText);
            int index = ExecuteScalar(sqlFolio);
            return string.Concat(objeto.TipoText[0], objeto.FechaEmision.ToString("yyMM"), index.ToString("000#"));
        }

        /// <summary>
        /// calcular el numero consecutivo del recibo segun el tipo
        /// </summary>
        /// <param name="objeto"></param>
        /// <returns></returns>
        public long Folio(ViewModelPrePoliza objeto)
        {
            var sqlCommand = new MySqlCommand { CommandText = "select max(_cntbl3_folio) + 1 from _cntbl3 where _cntbl3_tipo=@tipo;" };
            sqlCommand.Parameters.AddWithValue("@tipo", Enum.GetName(typeof(EnumPolizaTipo), objeto.TipoText));
            long folio = ExecuteScalar(sqlCommand);
            if (folio == 0)
                folio = 1;
            return folio;
        }

        /// <summary>
        /// insertar un nuevo objeto ViewModelPrePoliza
        /// </summary>
        /// <param name="objeto">ViewModelPrePoliza</param>
        /// <returns>ViewModelPrePoliza</returns>
        public ViewModelPrePoliza Insert(ViewModelPrePoliza objeto)
        {
            // calcular el folio por el tipo de movimiento
            objeto.Folio = this.Folio(objeto);

            // calcular el numero de identificacion
            objeto.NoIndet = this.NoIndetificacion(objeto);

            // insertar el numero de registro
            objeto.Id = this.Insert(objeto).Id;

            // en caso de que tenga comprobantes relacionados
            if (objeto.Id > 0)
            {
                if (objeto.Comprobantes != null)
                {
                    if (objeto.Comprobantes.Count > 0)
                    {
                        // actualizar la lista de comprobantes con el numero de indetifiacion y el indice de la relacion de la prepoliza
                        objeto.Comprobantes = new BindingList<ViewModelPrePolizaComprobante>(objeto.Comprobantes.Select(c => { c.NoIndet = objeto.NoIndet; c.SubId = objeto.Id; return c; }).ToList<ViewModelPrePolizaComprobante>());
                        this.Insertable(objeto.Comprobantes);
                    }
                }
            }
            return objeto;
        }

        public ViewModelPrePoliza Update(ViewModelPrePoliza objeto)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public ViewModelPrePoliza GetPrePoliza(string noident)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public BindingList<ViewModelPrePolizaComprobante> GetComprobantes(string noIdentificacion)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public BindingList<ViewModelPrePoliza> GetPrePolizas(EnumPolizaTipo tipo, EnumMonthsOfYear mes = 0, int anio = 0, string cuenta = "todos")
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public BindingList<ViewModelPrePolizaComprobante> GetComprobantes(EnumCfdiSubType objtype, string emisor, string receptor, string status, string metodo = "", string efecto = "")
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public BindingList<ViewModelPrePolizaComprobante> GetComprobantes1(EnumCfdiSubType objtype, string emisor, string receptor, string status, string metodo = "", string efecto = "")
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public ViewModelPrePoliza Save(ViewModelPrePoliza objeto)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public ViewModelPrePoliza CrearRecibo(List<ViewModelComprobanteSingleC> lista)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public ViewModelBancoMovimiento Aplicar(ViewModelBancoMovimiento objeto)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public BancoEstadoCuenta EstadoCuenta(EnumPolizaTipo tipo, EnumMonthsOfYear mes = 0, int anio = 0, ViewModelBancoCuenta cuenta = null)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public ViewModelPrePoliza Traspaso(ViewModelPrePoliza objeto)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public ViewModelBancoSaldo Save(ViewModelBancoSaldo objeto)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public bool AplicarStatus(string noident, string newStatus, string usuario)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public bool Aplicar(string noident)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public bool Replicar(ViewModelPrePoliza objeto)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public void Create()
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        private bool Insertable(BindingList<ViewModelPrePolizaComprobante> comprobantes)
        {
            foreach (var item in comprobantes)
            {
                this.Insert(item);
            }
            return false;
        }

        private int Insert(ViewModelPrePolizaComprobante objeto)
        {
            var sqlCommand = new MySqlCommand
            {
                CommandText = string.Concat("insert into _cntbl3c (_cntbl3c_a,_cntbl3c_sbtp,_cntbl3c_subid,_cntbl3c_idcom,_cntbl3c_status,_cntbl3c_estado,_cntbl3c_folio,_cntbl3c_serie,_cntbl3c_tipo,_cntbl3c_uuid,_cntbl3c_rfce,_cntbl3c_nome,_cntbl3c_rfcr,_cntbl3c_nomr,_cntbl3c_ivat,_cntbl3c_total,_cntbl3c_acumulado,_cntbl3c_cargo,_cntbl3c_abono,_cntbl3c_usr_n,_cntbl3c_fecems,_cntbl3c_fn,_cntbl3c_noiden,_cntbl3c_subtotal,_cntbl3c_desc,_cntbl3c_feculpg,_cntbl3c_ver,_cntbl3c_vnddr,_cntbl3c_clvr) ",
                                                          "values (@cntbl3c_a,@cntbl3c_sbtp,@cntbl3c_subid,@cntbl3c_idcom,@cntbl3c_status,@cntbl3c_estado,@cntbl3c_folio,@cntbl3c_serie,@cntbl3c_tipo,@cntbl3c_uuid,@cntbl3c_rfce,@cntbl3c_nome,@cntbl3c_rfcr,@cntbl3c_nomr,@cntbl3c_ivat,@cntbl3c_total,@cntbl3c_acumulado,@cntbl3c_cargo,@cntbl3c_abono,@cntbl3c_usr_n,@cntbl3c_fecems,@cntbl3c_fn,@cntbl3c_noiden,@cntbl3c_subtotal,@cntbl3c_desc,@cntbl3c_feculpg,@cntbl3c_ver,@cntbl3c_vnddr,@cntbl3c_clvr);")
            };
            sqlCommand.Parameters.AddWithValue("@cntbl3c_a", 1);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_sbtp", objeto.Tipo);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_subid", objeto.SubId);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_idcom", objeto.IdComprobante);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_status", objeto.Status);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_estado", objeto.Estado);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_folio", objeto.Folio);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_serie", objeto.Serie);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_tipo", objeto.TipoText);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_uuid", objeto.UUID);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_rfce", objeto.EmisorRFC);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_nome", objeto.Emisor);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_rfcr", objeto.ReceptorRFC);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_nomr", objeto.Receptor);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_ivat", objeto.TrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_total", objeto.Total);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_acumulado", objeto.Acumulado);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_cargo", objeto.Cargo);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_abono", objeto.Abono);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_usr_n", objeto.Creo);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_fecems", objeto.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_fn", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_noiden", objeto.NoIndet);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_subtotal", objeto.Subtotal);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_desc", objeto.Descuento);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_feculpg", objeto.FechaPago);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_ver", objeto.Version);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_vnddr", objeto.Vendedor);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_clvr", objeto.ReceptorClave);
            return this.ExecuteScalar(sqlCommand);
        }

        public bool Update(ViewModelPrePolizaComprobante objeto)
        {
            var sqlCommand = new MySqlCommand
            {
                CommandText = string.Concat("update _cntbl3c set ",
                                            "_cntbl3c_a=@cntbl3c_a,_cntbl3c_sbtp=@cntbl3c_sbtp,_cntbl3c_subid=@cntbl3c_subid,_cntbl3c_idcom=@cntbl3c_idcom,_cntbl3c_status=@cntbl3c_status,_cntbl3c_estado=@cntbl3c_estado,_cntbl3c_folio=@cntbl3c_folio,_cntbl3c_serie=@cntbl3c_serie,_cntbl3c_tipo=@cntbl3c_tipo,_cntbl3c_uuid=@cntbl3c_uuid,_cntbl3c_rfce=@cntbl3c_rfce,_cntbl3c_nome=@cntbl3c_nome,_cntbl3c_rfcr=@cntbl3c_rfcr,_cntbl3c_nomr=@cntbl3c_nomr,_cntbl3c_ivat=@cntbl3c_ivat,_cntbl3c_total=@cntbl3c_total,_cntbl3c_acumulado=@cntbl3c_acumulado,_cntbl3c_cargo=@cntbl3c_cargo,_cntbl3c_abono=@cntbl3c_abono,_cntbl3c_usr_m=@cntbl3c_usr_m,_cntbl3c_fecems=@cntbl3c_fecems,_cntbl3c_fm=@cntbl3c_fm,_cntbl3c_noiden=@cntbl3c_noiden, ",
                                            "_cntbl3c_subtotal=@cntbl3c_subtotal,_cntbl3c_desc=@cntbl3c_desc,_cntbl3c_clvr=@cntbl3c_clvr,_cntbl3c_nmpdd=@cntbl3c_nmpdd,_cntbl3c_vnddr=@cntbl3c_vnddr,_cntbl3c_ver=@cntbl3c_ver ",
                                            "where _cntbl3c_id=@index;")
            };

            sqlCommand.Parameters.AddWithValue("@index", objeto.Id);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_sbtp", objeto.Tipo);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_subid", objeto.SubId);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_idcom", objeto.IdComprobante);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_status", objeto.Status);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_estado", objeto.Estado);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_folio", objeto.Folio);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_serie", objeto.Serie);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_tipo", objeto.TipoText);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_uuid", objeto.UUID);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_rfce", objeto.EmisorRFC);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_nome", objeto.Emisor);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_rfcr", objeto.ReceptorRFC);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_nomr", objeto.Receptor);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_ivat", objeto.TrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_total", objeto.Total);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_acumulado", objeto.Acumulado);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_cargo", objeto.Cargo);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_abono", objeto.Abono);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_usr_m", objeto.Modifica);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_fecems", objeto.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_feculpg", objeto.FechaPago);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_fm", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_noiden", objeto.NoIndet);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_subtotal", objeto.Subtotal);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_desc", objeto.Descuento);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_clvr", objeto.ReceptorClave);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_nmpdd", objeto.NumPedido);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_vnddr", objeto.Vendedor);
            sqlCommand.Parameters.AddWithValue("@cntbl3c_ver", objeto.Version);

            if (objeto.IsActive == true)
                sqlCommand.Parameters.AddWithValue("@cntbl3c_a", 1);
            else
                sqlCommand.Parameters.AddWithValue("@cntbl3c_a", 0);

            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public void Revisar(BindingList<ViewModelPrePoliza> datos)
        {
            
        }
    }
}
