﻿/// develop: ANHE190620191753
/// purpose: configuraciones almacenadas para la empresa
using System;
using Jaeger.Edita.V2.Empresa.Entities;
using Jaeger.Edita.Interfaces;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class SqlSugarConfiguracion : MySqlSugarContext<ViewModelEmpresaConfiguracion>, ISqlConfiguracion
    {
        public SqlSugarConfiguracion(DataBaseConfiguracion configuracion) : base(configuracion)
        {
        }

        public enum KeyConfiguracion
        {
            cfdi,
            ocompra11,
            nom12,
            valida6
        }

        /// <summary>
        /// obtener objeto de configuracion por la llave key
        /// </summary>
        public ViewModelEmpresaConfiguracion GetByKey(string key1)
        {
            return this.Db.Queryable<ViewModelEmpresaConfiguracion>().Where(it => it.Key == key1).Single();
        }

        /// <summary>
        /// obtener objeto de configuracion por la llave key
        /// </summary>
        public ViewModelEmpresaConfiguracion GetByKey(SqlSugarConfiguracion.KeyConfiguracion key)
        {
            return this.Db.Queryable<ViewModelEmpresaConfiguracion>().Where(it => it.Key == Enum.GetName(typeof(SqlSugarConfiguracion.KeyConfiguracion), key).ToLower()).Single();
        }

        public ViewModelEmpresaConfiguracion Save(ViewModelEmpresaConfiguracion item)
        {
            if (item.Id == 0)
                item.Id = this.Insert(item);
            else
                this.Update(item);
            return item;
        }

        /// <summary>
        /// obtener la configuracion de los datos generales de la empresa
        /// </summary>
        /// <returns></returns>
        public EmpresaData GetSynapsis()
        {
            ViewModelEmpresaConfiguracion d = this.GetByKey("cfdi");
            if (d != null)
            {
                EmpresaData synapsis = EmpresaData.Json(d.Data);
                return synapsis;
            }
            return null;
        }
    }
}
