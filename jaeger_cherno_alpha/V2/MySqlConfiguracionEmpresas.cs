﻿using System.Linq;
using MySql.Data.MySqlClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2.Empresa.Entities;

namespace Jaeger.Edita.V2
{
    /// <summary>
    /// Configuraciones de empresas en EDITA (empr)
    /// </summary>
    public class MySqlConfiguracionEmpresas : MySqlSugarContext<ViewModelEmpresa>, ISqlConfiguracionEmpresas
    {
        public MySqlConfiguracionEmpresas(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public ViewModelEmpresa GetByRFC(string rfc)
        {
            if (rfc == null)
                return null;
            var sqlComand = new MySqlCommand() { CommandText = "select * from empr where empr_rfc = @rfc limit 1;" };
            sqlComand.Parameters.AddWithValue("@rfc", rfc.ToUpper());
            return this.GetMapper<ViewModelEmpresa>(sqlComand).FirstOrDefault();
        }
    }
}
