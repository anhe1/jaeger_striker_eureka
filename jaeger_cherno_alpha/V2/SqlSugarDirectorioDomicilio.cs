﻿using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class SqlSugarDirectorioDomicilio : MySqlSugarContext<ViewModelDomicilio>
    {
        public SqlSugarDirectorioDomicilio(DataBaseConfiguracion configuracion) : base(configuracion)
        {
        }

        public BindingList<ViewModelDomicilio> GetList(int index)
        {
            return new BindingList<ViewModelDomicilio>(this.Db.Queryable<ViewModelDomicilio>().Where(it => it.SubId == index).ToList());
        }

        public ViewModelDomicilio Save(ViewModelDomicilio objeto)
        {
            if (objeto.Id == 0)
                objeto.Id = this.Insert(objeto);
            else
            {
                if (this.Update(objeto) > 0)
                    return objeto;
            }
            return objeto;
        }

        public BindingList<ViewModelDomicilio> AdoGetList(int index)
        {
            string sqlString = "select * from _drccn where _drccn_a=1 and _drccn_drctr_id=@index";
            DataTable data = this.Db.Ado.GetDataTable(sqlString, new List<SugarParameter> { new SugarParameter("@index", index) });
            DataNamesMapper<ViewModelDomicilio> mapper = new DataNamesMapper<ViewModelDomicilio>();
            return new BindingList<ViewModelDomicilio>(mapper.Map(data).ToList());
        }
    }
}
