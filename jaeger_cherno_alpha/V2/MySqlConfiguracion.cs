using System;
using System.Linq;
using MySql.Data.MySqlClient;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2.Empresa.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class MySqlConfiguracion : MySqlSugarContext<ViewModelEmpresaConfiguracion>, ISqlConfiguracion
    {
        public MySqlConfiguracion(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public void MySqlComprobanteFiscal_ProcessError(object sender, Jaeger.Entities.ProgressError e)
        {
            Console.WriteLine(e.Data);
        }

        public ViewModelEmpresaConfiguracion GetByKey(string key)
        {
            var sqlCommand = new MySqlCommand() { CommandText = "select * from _conf where _conf_key = lower(@key) first 1" };
            //SELECT `_conf_id`,`_conf_key`,`_conf_data` FROM `_conf`  WHERE( `_conf_key` = (LOWER(@constant0)))
            sqlCommand.Parameters.AddWithValue("@key", key);
            
            return this.GetMapper<ViewModelEmpresaConfiguracion>(sqlCommand).First();
        }

        public ViewModelEmpresaConfiguracion GetByKey(SqlSugarConfiguracion.KeyConfiguracion key)
        {
            return this.GetByKey(Enum.GetName(typeof(SqlSugarConfiguracion.KeyConfiguracion), key).ToLower());
        }

        public EmpresaData GetSynapsis()
        {
            return null;
        }

        public ViewModelEmpresaConfiguracion Save(ViewModelEmpresaConfiguracion item)
        {
            return item;
        }
    }
}