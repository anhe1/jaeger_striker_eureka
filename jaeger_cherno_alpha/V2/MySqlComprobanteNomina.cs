﻿//elemento                      tipo                clave               conceto             exento              gravado
//1=Percepcion                  TipoPercepcion      Clave               Concepto            ImporteGravado      ImporteExento
//2=Deduccion                   TipoDeduccion       Clave               Concepto                                Importe
//3=HorasExtra                                      Dias                TipoHoras           HorasExtra          ImportePagado
//4=Incapacidad                                                         DiasIncapacidad     TipoIncapacidad     ImporteMonetario
//5=AccionesOTitulos                                                                        ValorMercado        PrecioAlOtorgarse
//6=JubilacionPensionRetiro     TotalUnaExhibicion  TotalParcialidad    MontoDiario         IngresoAcumulable   IngresoNoAcumulable
//7=SeparacionIndemnizacion     TotalPagado         NumAñosServicio     UltimoSueldoMensOrd IngresoAcumulable   IngresoNoAcumulable
//8=OtroPago                    TipoOtroPago        Clave               Concepto                                Importe
//9=CompensacionSaldosAFavor                                            Año                 SaldoAFavor         RemanenteSalFav
using System;
using System.Data;
using System.Linq;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using Jaeger.Edita.Enums;
using Jaeger.Edita.Helpers;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.Nomina.Entities;
using Jaeger.SAT.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using SqlSugar;

namespace Jaeger.Edita.V2
{
    public class MySqlComprobanteNomina : MySqlSugarContext<ComplementoNomina>, ISqlComprobanteNomina
    {
        public MySqlComprobanteNomina(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public int Insert(ComplementoNomina complemento)
        {
            var sqlInsert = new MySqlCommand()
            {
                CommandText = string.Concat(str0: "insert into _cfdnmn (_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tipo,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,_cfdnmn_dttlexn,_cfdnmn_dscnt,_cfdnmn_hrsxtr,_cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,_cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,_cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,_cfdnmn_fn,_cfdnmn_usr_n,_cfdnmn_uuid,_cfdnmn_rfc) ", 
                                                         str1: "values (@cfdnmn_drctr_id,@cfdnmn_cfdi_id,@cfdnmn_tipo,@cfdnmn_tpreg,@cfdnmn_rsgpst,@cfdnmn_qnc,@cfdnmn_anio,@cfdnmn_antgdd,@cfdnmn_dspgds,@cfdnmn_bsctap,@cfdnmn_drintg,@cfdnmn_pttlgrv,@cfdnmn_pttlexn,@cfdnmn_dttlgrv,@cfdnmn_dttlexn,@cfdnmn_dscnt,@cfdnmn_hrsxtr,@cfdnmn_ver,@cfdnmn_banco,@cfdnmn_rgp,@cfdnmn_numem,@cfdnmn_curp,@cfdnmn_numsgr,@cfdnmn_clabe,@cfdnmn_depto,@cfdnmn_puesto,@cfdnmn_tpcntr,@cfdnmn_tpjrn,@cfdnmn_prddpg,@cfdnmn_fchpgo,@cfdnmn_fchini,@cfdnmn_fchfnl,@cfdnmn_fchrel,@cfdnmn_fn,@cfdnmn_usr_n,@cfdnmn_uuid,@cfdnmn_rfc);", str2: "select last_insert_id();")
            };

            //sqlInsert.Parameters.AddWithValue("@cfdnmn_id", complemento.Id);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_tipo", complemento.TipoNomina);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_drctr_id", complemento.Receptor.Id);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_cfdi_id", complemento.SubId);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_tpreg", complemento.Receptor.TipoRegimen);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_rsgpst", complemento.Receptor.RiesgoPuesto);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_qnc", complemento.FechaPago.Value.Day > 15 ? 2 : 1);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_anio", complemento.FechaPago.Value.Year);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_antgdd", complemento.Receptor.Antiguedad);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_dspgds", complemento.NumDiasPagados);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_bsctap", complemento.Receptor.SalarioBaseCotApor);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_drintg", complemento.Receptor.SalarioDiarioIntegrado);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_pttlgrv", complemento.Percepciones.TotalGravado);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_pttlexn", complemento.Percepciones.TotalExento);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_dttlgrv", complemento.Deducciones.TotalImpuestosRetenidos);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_dttlexn", complemento.Deducciones.TotalOtrasDeducciones);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_hrsxtr", 0);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_ver", complemento.Version);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_banco", complemento.Receptor.Banco);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_rgp", complemento.Emisor.RegistroPatronal);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_numem", complemento.Receptor.Num);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_curp", complemento.Receptor.CURP);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_numsgr", complemento.Receptor.NumSeguridadSocial);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_clabe", complemento.Receptor.CuentaBancaria);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_depto", complemento.Receptor.Departamento);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_puesto", complemento.Receptor.Puesto);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_tpcntr", complemento.Receptor.TipoContrato);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_tpjrn", complemento.Receptor.TipoJornada);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_prddpg", complemento.Receptor.PeriodicidadPago);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_fchpgo", complemento.FechaPago);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_fchini", complemento.FechaInicialPago);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_fchfnl", complemento.FechaFinalPago);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_fchrel", complemento.Receptor.FechaInicioRelLaboral);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_fn", DateTime.Now);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_usr_n", complemento.Creo);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_uuid", complemento.IdDocumento);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_dscnt", complemento.Descuento);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_rfc", complemento.Receptor.RFC);

            return this.ExecuteScalar(sqlInsert);
        }

        public ViewModelNominaParteSingle Insert(ViewModelNominaParteSingle single)
        {
            MySqlCommand sqlInsert = new MySqlCommand()
            {
                CommandText = string.Concat("insert into _nmnprt (_nmnprt_a,_nmnprt_cfdnmn_id,_nmnprt_doc,_nmnprt_tipo,_nmnprt_qnc,_nmnprt_anio,_nmnprt_dias,_nmnprt_hrsxtr,_nmnprt_dsincp,_nmnprt_impgrv,_nmnprt_impext,_nmnprt_dscnt,_nmnprt_imppgd,_nmnprt_tphrs,_nmnprt_clv,_nmnprt_cncpt,_nmnprt_fn,_nmnprt_usr_n,_nmnprt_numem,_nmnprt_imprt) ",
                                                         "values (@nmnprt_a,@nmnprt_cfdnmn_id,@nmnprt_doc,@nmnprt_tipo,@nmnprt_qnc,@nmnprt_anio,@nmnprt_dias,@nmnprt_hrsxtr,@nmnprt_dsincp,@nmnprt_impgrv,@nmnprt_impext,@nmnprt_dscnt,@nmnprt_imppgd,@nmnprt_tphrs,@nmnprt_clv,@nmnprt_cncpt,@nmnprt_fn,@nmnprt_usr_n,@nmnprt_numem,@nmnprt_imprt);",
                                                         "select last_insert_id();")
            };

            sqlInsert.Parameters.AddWithValue("@nmnprt_a", 1);
            sqlInsert.Parameters.AddWithValue("@nmnprt_cfdnmn_id", single.SubId);
            sqlInsert.Parameters.AddWithValue("@nmnprt_doc", single.IdDoc);
            sqlInsert.Parameters.AddWithValue("@nmnprt_tipo", single.Tipo);
            sqlInsert.Parameters.AddWithValue("@nmnprt_clv", single.Clave);
            sqlInsert.Parameters.AddWithValue("@nmnprt_qnc", single.Quincena);
            sqlInsert.Parameters.AddWithValue("@nmnprt_anio", single.Anio);
            sqlInsert.Parameters.AddWithValue("@nmnprt_dias", single.DiasIncapacidad);
            sqlInsert.Parameters.AddWithValue("@nmnprt_hrsxtr", single.HorasExtra);
            sqlInsert.Parameters.AddWithValue("@nmnprt_dsincp", single.DiasIncapacidad);
            sqlInsert.Parameters.AddWithValue("@nmnprt_impgrv", single.ImporteGravado);
            sqlInsert.Parameters.AddWithValue("@nmnprt_impext", single.ImporteExento);
            sqlInsert.Parameters.AddWithValue("@nmnprt_dscnt", single.DescuentoIncapcidad);
            sqlInsert.Parameters.AddWithValue("@nmnprt_imppgd", single.ImportePagadoHorasExtra);
            sqlInsert.Parameters.AddWithValue("@nmnprt_tphrs", single.TipoHoras);
            sqlInsert.Parameters.AddWithValue("@nmnprt_cncpt", single.Concepto);
            sqlInsert.Parameters.AddWithValue("@nmnprt_numem", single.NumEmpleado);
            sqlInsert.Parameters.AddWithValue("@nmnprt_imprt", single.Importe);
            sqlInsert.Parameters.AddWithValue("@nmnprt_fn", DateTime.Now);
            sqlInsert.Parameters.AddWithValue("@nmnprt_usr_n", single.Creo);

            single.Id = this.ExecuteScalar(sqlInsert);
            return single;
        }

        public ComplementoNomina GetBy(int subIndex)
        {
            var sqlCommand = new MySqlCommand()
            {
                CommandText = "select * from _cfdnmn where _cfdnmn_cfdi_id=@index"
            };

            sqlCommand.Parameters.AddWithValue(parameterName: "@index", value: subIndex);
            var parameters = new List<SugarParameter>() {
                new SugarParameter("@index", subIndex)
            };
            var single = this.GetMapper<ViewModelComplementoNominaSingle>(sqlCommand.CommandText, parameters).FirstOrDefault();

            if (single != null)
            {
                var response = new ComplementoNomina()
                {
                    Id = single.Id,
                    Activo = single.Activo,
                    Anio = single.Anio,
                    Antiguedad = single.Antiguedad,
                    Banco = single.Banco,
                    CLABE = single.CLABE,
                    ControlId = single.ControlId,
                    Creo = single.Creo,
                    CtaBanco = single.CtaBanco,
                    CURP = single.CURP,
                    Version = single.Version,
                    Departamento = single.Departamento,
                    DeduccionTotalExento = single.DeduccionTotalExento,
                    DeduccionTotalGravado = single.DeduccionTotalExento,
                    Descuento = single.Descuento,
                    FechaFinalPago = single.FechaFinalPago,
                    FechaInicialPago = single.FechaInicialPago,
                    FechaNuevo = single.FechaNuevo,
                    FechaInicioRelLaboral = single.FechaInicioRelLaboral,
                    FechaPago = single.FechaPago,
                    FecModificacion = single.FecModificacion,
                    IdDocumento = single.IdDocumento,
                    ImportePagado = single.ImportePagado,
                    Modifica = single.Modifica,
                    Sindicalizado = single.Sindicalizado,
                    NumDiasPagados = single.NumDiasPagados,
                    NumEmpleado = single.NumEmpleado,
                    NumSeguridadSocial = single.NumSeguridadSocial,
                    PercepcionTotalExento = single.PercepcionTotalExento,
                    PercepcionTotalGravado = single.PercepcionTotalGravado,
                    PeriodicidadPago = single.PeriodicidadPago,
                    Puesto = single.Puesto,
                    EntidadFederativa = single.EntidadFederativa,
                    ReceptorId = single.ReceptorId,
                    RegistroPatronal = single.RegistroPatronal,
                    RiesgoPuesto = single.RiesgoPuesto,
                    RFC = single.RFC,
                    SalarioBaseCotApor = single.SalarioBaseCotApor,
                    SalarioDiarioIntegrado = single.SalarioDiarioIntegrado,
                    SubId = single.SubId,
                    TipoContrato = single.TipoContrato,
                    TipoJornada = single.TipoJornada,
                    TipoNomina = single.TipoNomina,
                    TipoRegimen = single.TipoRegimen,
                };

                response.Receptor = new ComplementoNominaReceptor()
                {
                    Id = single.ReceptorId,
                    RFC = single.RFC,
                    Antiguedad = single.Antiguedad,
                    ClaveEntFed = single.EntidadFederativa,
                    CURP = single.CURP,
                    CuentaBancaria = single.CtaBanco,
                    Departamento = single.Departamento,
                    Num = int.Parse( single.NumEmpleado),
                    FechaInicioRelLaboral = single.FechaInicioRelLaboral,
                    NumSeguridadSocial = single.NumSeguridadSocial,
                    RegistroPatronal = single.RegistroPatronal,
                    SalarioBaseCotApor = single.SalarioBaseCotApor,
                    PeriodicidadPago = single.PeriodicidadPago,
                    RiesgoPuesto = single.RiesgoPuesto,
                    Puesto = single.Puesto,
                    TipoRegimen = single.TipoRegimen,
                    TipoContrato = single.TipoContrato,
                    SalarioDiarioIntegrado = single.SalarioDiarioIntegrado,
                    Sindicalizado = single.Sindicalizado,
                    TipoJornada = single.TipoJornada
                };
                
                sqlCommand = new MySqlCommand()
                {
                    CommandText = "select * from _nmnprt where _nmnprt_cfdnmn_id=@index"
                };

                List<ViewModelNominaParteSingle> responses = this.GetMapper<ViewModelNominaParteSingle>(sqlCommand.CommandText, parameters).ToList(); ;
                foreach (ViewModelNominaParteSingle item in responses)
                {
                    // percepcion
                    if (item.IdDoc == 1)
                    {
                        var p = item as ComplementoNominaPercepcion;
                        response.Percepciones.Percepcion.Add(p);
                    } // deduccion
                    else if (item.IdDoc == 2)
                    {
                        var d = item as ComplementoNominaDeduccion;
                        response.Deducciones.Deduccion.Add(d);
                    } // horas extra
                    else if (item.IdDoc == 3)
                    {
                        var h = item as ComplementoNominaPercepcionHorasExtra;
                        //response.Percepciones.Percepcion.Add(h);
                    } // incapacidad
                    else if (item.IdDoc == 4)
                    {
                        var i = item as ComplementoNominaIncapacidad;
                        response.Incapacidades.Add(i);
                    } // acciones o titulos
                    else if (item.IdDoc == 5)
                    {
                        var a = item as ComplementoNominaPercepcionAccionesOTitulos;
                    } // Jubilacion, Pension o Retiro
                    else if(item.Id == 6)
                    {
                        //var j = item as ComplementoNominaPercepcionesJubilacionPensionRetiro;
                    } //  SeparacionIndemnizacion
                    else if (item.IdDoc == 7)
                    {
                        //var s = item as ComplementoNominaPercepcionesSeparacionIndemnizacion;
                    }
                    else if (item.IdDoc == 8)
                    {
                        var o = item as ComplementoNominaOtroPago;
                        response.OtrosPagos.Add(o);
                    } else if (item.IdDoc == 9)
                    {
                        //var c = item as ComplementoNominaOtroPagoCompensacionSaldosAFavor;
                        
                    }
                }
                return response;
            }

            return null;
        }

        public ViewModelComplementoNominaSingle GetSingleBySubId(int subIndex)
        {
            var sqlCommand = new MySqlCommand()
            {
                CommandText = "select * from _cfdnmn where _cfdnmn_cfdi_id=@index "
            };
            sqlCommand.Parameters.AddWithValue("@index", subIndex);
            var parameters = new List<SugarParameter>() {
                new SugarParameter("@index", subIndex)
            };
            return this.GetMapper<ViewModelComplementoNominaSingle>(sqlCommand.CommandText, parameters).SingleOrDefault();
        }

        public List<ViewModelNominaParteSingle> GetPartesBySubIndex(int subIndex)
        {
            var sqlCommand = new MySqlCommand()
            {
                CommandText = "select * from _nmnprt_cfdnmn_id where _nmnprt_cfdnmn_id=@index "
            };
            sqlCommand.Parameters.AddWithValue("@index", subIndex);
            var parameters = new List<SugarParameter>() {
                new SugarParameter("@index", subIndex)
            };

            return this.GetMapper<ViewModelNominaParteSingle>(sqlCommand.CommandText, parameters).ToList();
        }

        //1=Percepcion                  TipoPercepcion      Clave               Concepto            ImporteGravado      ImporteExento
        //2=Deduccion                   TipoDeduccion       Clave               Concepto                                Importe
        //3=HorasExtra                                      Dias                TipoHoras           HorasExtra          ImportePagado
        //4=Incapacidad                                                         DiasIncapacidad     TipoIncapacidad     ImporteMonetario
        //5=AccionesOTitulos                                                                        ValorMercado        PrecioAlOtorgarse
        //6=JubilacionPensionRetiro     TotalUnaExhibicion  TotalParcialidad    MontoDiario         IngresoAcumulable   IngresoNoAcumulable
        //7=SeparacionIndemnizacion     TotalPagado         NumAñosServicio     UltimoSueldoMensOrd IngresoAcumulable   IngresoNoAcumulable
        //8=OtroPago                    TipoOtroPago        Clave               Concepto                                Importe
        //9=CompensacionSaldosAFavor                                            Año                 SaldoAFavor         RemanenteSalFav
        public ComplementoNomina Save(ComplementoNomina complemento)
        {
            complemento.Id = this.Insert(complemento);
            
            if (complemento.Id > 0)
            {
                // percepciones
                if (complemento.Percepciones != null)
                    if (complemento.Percepciones.Percepcion != null)
                    {
                        for (int index = 0; index < complemento.Percepciones.Percepcion.Count; index++)
                        {
                            complemento.Percepciones.Percepcion[index].SubId = complemento.Id;
                            complemento.Percepciones.Percepcion[index].Creo = complemento.Creo;
                            complemento.Percepciones.Percepcion[index].FechaNuevo = complemento.FechaNuevo;
                            complemento.Percepciones.Percepcion[index].Id = this.Insert(complemento.Percepciones.Percepcion[index]).Id;

                            // horas extra
                            if (complemento.Percepciones.Percepcion[index].HorasExtra != null)
                            {
                                for (int i = 0; i < complemento.Percepciones.Percepcion[index].HorasExtra.Count; i++)
                                {
                                    complemento.Percepciones.Percepcion[index].HorasExtra[i].Creo = complemento.Creo;
                                    complemento.Percepciones.Percepcion[index].HorasExtra[i].FechaNuevo = complemento.FechaNuevo;
                                    complemento.Percepciones.Percepcion[index].HorasExtra[i].SubId = complemento.Id;
                                    complemento.Percepciones.Percepcion[index].HorasExtra[i].Id = this.Insert(complemento.Percepciones.Percepcion[index].HorasExtra[i]).Id;
                                }

                                // acciones o titulos
                                if (complemento.Percepciones.Percepcion[index].AccionesOTitulos != null)
                                {
                                    complemento.Percepciones.Percepcion[index].AccionesOTitulos.Creo = complemento.Creo;
                                    complemento.Percepciones.Percepcion[index].AccionesOTitulos.SubId = complemento.Id;
                                    complemento.Percepciones.Percepcion[index].AccionesOTitulos.FechaNuevo = complemento.FechaNuevo;
                                    complemento.Percepciones.Percepcion[index].AccionesOTitulos.Id = this.Insert(complemento.Percepciones.Percepcion[index].AccionesOTitulos).Id;
                                }
                            }
                        }
                    }

                // deducciones
                if (complemento.Deducciones != null)
                    if (complemento.Deducciones.Deduccion != null)
                    {
                        for (var index = 0; index < complemento.Deducciones.Deduccion.Count; index++)
                        {
                            complemento.Deducciones.Deduccion[index].SubId = complemento.Id;
                            complemento.Deducciones.Deduccion[index].Creo = complemento.Creo;
                            complemento.Deducciones.Deduccion[index].FechaNuevo = complemento.FechaNuevo;
                            complemento.Deducciones.Deduccion[index].Id = this.Insert(complemento.Deducciones.Deduccion[index]).Id;
                        }
                    }

                // otros pagos
                if (complemento.OtrosPagos != null)
                {
                    for (int index = 0; index < complemento.OtrosPagos.Count; index++)
                    {
                        complemento.OtrosPagos[index].SubId = complemento.Id;
                        complemento.OtrosPagos[index].Creo = complemento.Creo;
                        complemento.OtrosPagos[index].FechaNuevo = complemento.FechaNuevo;
                        complemento.OtrosPagos[index].Id = this.Insert(complemento.OtrosPagos[index]).Id;
                    }
                }
            }

            return complemento;
        }

        public DataTable GetResumen(int anio, EnumCfdiEstado estado = EnumCfdiEstado.Vigente, EnumMonthsOfYear mes = EnumMonthsOfYear.None)
        {
            return null;
        }

        public List<NominaViewSingle> GetCfdNominas(int index, bool activos = true) {
            MySqlCommand mySqlCommand = new MySqlCommand() {
                CommandText = string.Concat("select _cfdnmn_id,_cfdnmn_a,_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,_cfdnmn_dttlexn,_cfdnmn_dscnt,_cfdnmn_hrsxtr,_cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_tipo,_cfdnmn_uuid,_cfdi_uuid,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,_cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,_cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,_cfdnmn_fn,_cfdnmn_fm,_cfdnmn_usr_n,_cfdnmn_usr_m,_cfdi_nomr,_cfdi_sbttl,_cfdi_total,_cfdi_dscnt,_cfdi_estado,_cfdi_fecedo,_cfdi_feccert,_cfdi_rfce,_cfdi_rfcr, _cfdi_id, _cfdi_url_xml, _cfdi_url_pdf ", 
                "from _cfdi, _cfdnmn ", "where _cfdnmn_cfdi_id = _cfdi_id and _cfdnmn_a = 1 and _cfdnmn_nmnctrl_id = @index ")
            };
            MySqlCommand sqlCommans = mySqlCommand;
            sqlCommans.Parameters.AddWithValue("@index", index);
            var parameters = new List<SugarParameter>() {
                new SugarParameter("@index", index)
            };
            return this.GetMapper<NominaViewSingle>(sqlCommans.CommandText, parameters).ToList();
        }

        public List<NominaViewSingle> GetCfdUuid(string uuid, bool showDelete)
        {
            var mySqlCommand = new MySqlCommand()
            {
                CommandText = string.Concat("select _cfdnmn_id,_cfdnmn_a,_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,_cfdnmn_dttlexn,_cfdnmn_dscnt,_cfdnmn_hrsxtr,_cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_tipo,_cfdnmn_uuid,_cfdi_uuid,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,_cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,_cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,_cfdnmn_fn,_cfdnmn_fm,_cfdnmn_usr_n,_cfdnmn_usr_m,_cfdi_nomr,_cfdi_sbttl,_cfdi_total,_cfdi_dscnt,_cfdi_estado,_cfdi_fecedo,_cfdi_feccert,_cfdi_rfce,_cfdi_rfcr, _cfdi_id ", "from _cfdi, _cfdnmn ", "where _cfdnmn_cfdi_id = _cfdi_id and _cfdnmn_a = 1 and _cfdi_uuid like @uuid")
            };

            mySqlCommand.Parameters.AddWithValue("@uuid", uuid);
            var parameters = new List<SugarParameter>() {
                new SugarParameter("@uuid", uuid)
            };
            return this.GetMapper<NominaViewSingle>(mySqlCommand.CommandText, parameters).ToList();
        }

        public List<NominaViewSingle> GetCfdNominasBy(string nombre)
        {
            MySqlCommand mySqlCommand = new MySqlCommand()
            {
                CommandText = string.Concat("select _cfdnmn_id,_cfdnmn_a,_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,_cfdnmn_dttlexn,_cfdnmn_dscnt,_cfdnmn_hrsxtr,_cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_tipo,_cfdnmn_uuid,_cfdi_uuid,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,_cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,_cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,_cfdnmn_fn,_cfdnmn_fm,_cfdnmn_usr_n,_cfdnmn_usr_m,_cfdi_nomr,_cfdi_sbttl,_cfdi_total,_cfdi_dscnt,_cfdi_estado,_cfdi_fecedo,_cfdi_feccert,_cfdi_rfce,_cfdi_rfcr, _cfdi_id ",
                                            "from _cfdi, _cfdnmn ",
                                            "where _cfdnmn_cfdi_id = _cfdi_id and _cfdi_nomr like @empleado")
            };

            mySqlCommand.Parameters.AddWithValue("@empleado", nombre);
            var parameters = new List<SugarParameter>() {
                new SugarParameter("@empleado", nombre)
            };
            return this.GetMapper<NominaViewSingle>(mySqlCommand.CommandText, parameters).ToList();
        }

        public List<NominaViewSingle> GetCfdNominaBy(EnumCfdiDates dateBy, DateTime dateStart, DateTime dateEnd, string depto, string employee)
        {
            string wheres = string.Empty;
            if (depto != "")
            {
                wheres = " and _cfdnmn_depto like @depto";
            }
            if (employee != "")
            {
                wheres = string.Concat(wheres, " and _cfdi_nomr like @employee");
            }
            if (dateBy == EnumCfdiDates.FechaEmision)
            {
                wheres = string.Concat(wheres, " and (_cfdi_fecems >= '@fec_inicio') and (_cfdi_fecems < '@fec_fin')");
            }
            else if (dateBy == EnumCfdiDates.FechaDePago)
            {
                wheres = string.Concat(wheres, " and (_cfdnmn_fchpgo >= '@fec_inicio') and (_cfdnmn_fchpgo < '@fec_fin')");
            }
            else if (dateBy == EnumCfdiDates.FechaTimbre)
            {
                wheres = string.Concat(wheres, " and (_cfdi_feccert >= '@fec_inicio') and (_cfdi_feccert <= '@fec_fin')");
            }
            wheres = wheres.Replace("@fec_inicio", dateStart.ToString("yyyy/MM/dd"));// strings.Format(dateStart, "yyyy/MM/dd"));
            wheres = wheres.Replace("@fec_fin", dateEnd.AddDays(1).ToString("yyyy/MM/dd")); //Strings.Format(dateEnd.AddDays(1), "yyyy/MM/dd"));
            MySqlCommand mySqlCommand = new MySqlCommand()
            {
                CommandText = string.Concat("select _cfdnmn_id,_cfdnmn_a,_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,_cfdnmn_dttlexn,_cfdnmn_dscnt,_cfdnmn_hrsxtr,_cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_tipo,_cfdnmn_uuid,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,_cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,_cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,_cfdnmn_fn,_cfdnmn_fm,_cfdnmn_usr_n,_cfdnmn_usr_m,_cfdi_nomr,_cfdi_rfce,_cfdnmn_rfc,_cfdi_total,_cfdi_estado,_cfdi_fecedo,_cfdi_feccert,_cfdi_id, _cfdi_uuid ", "from _cfdnmn,_cfdi,_ctlemp ", "where _cfdnmn_cfdi_id=_cfdi_id and _cfdnmn_drctr_id=_ctlemp_drctr_id ", wheres)
            };
            mySqlCommand.Parameters.AddWithValue("@depto", depto);
            mySqlCommand.Parameters.AddWithValue("@employee", employee);
            var parameters = new List<SugarParameter>() {
                new SugarParameter("@depto", depto),
                new SugarParameter("@employee", employee)
            };
            return this.GetMapper<NominaViewSingle>(mySqlCommand.CommandText, parameters).ToList();
        }

        /// <summary>
        /// actualizar estado del comprobante segun SAT
        /// </summary>
        /// <param name="response">objeto respuesta del servicio SAT</param>
        /// <returns>verdadero si fue actualizado el registro con exito</returns>
        public bool Update(SatQueryResult response) {
            var sqlCommand = new MySqlCommand() { CommandText = "update _cfdi set _cfdi_estado=@estado, _cfdi_fecedo=@fecha where _cfdi_id=@index;" };
            sqlCommand.Parameters.AddWithValue("@estado",response.Status);
            sqlCommand.Parameters.AddWithValue("@fecha",DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@index",response.Id);
            if (this.ExecuteTransaction(sqlCommand) == 0) {
                HelperLogError.LogWrite("No se actualizo el estado del comprobante.");
                return false;
            }
            return true;
        }

        public DataTable GetNominas()
        {
            MySqlCommand sqlCommand = new MySqlCommand()
            {
                CommandText = "select * from _nmnctrl where _nmnctrl_a=1 order by _nmnctrl_id desc;"
            };
            sqlCommand.Parameters.AddWithValue("", 1);
            return this.ExecuteReader(sqlCommand);
        }

        public DataTable GetNominaDeptos()
        {
            return this.ExecuteReader(new MySqlCommand()
            {
                CommandText = "select distinct(_cfdnmn_depto) from _cfdnmn where _cfdnmn_depto <> '';"
            });
        }

        public DataTable GetEmpleados()
        {
            return this.ExecuteReader(new MySqlCommand()
            {
                CommandText = "select distinct(_cfdi_nomr),_cfdnmn_numem from _cfdnmn,_cfdi where _cfdnmn_cfdi_id=_cfdi_id and _cfdi_nomr <> '' order by _cfdnmn_numem asc;"
            });
        }
    }
}
