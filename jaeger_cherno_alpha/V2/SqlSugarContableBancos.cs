﻿using System.ComponentModel;
using System;
using Jaeger.Edita.V2.Contable.Entities;
using Jaeger.Edita.Interfaces;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class SqlSugarContableBancos : MySqlSugarContext<ViewModelBancoCuenta>, ISqlContableBancos
    {
        public SqlSugarContableBancos(DataBaseConfiguracion configuracion) : base(configuracion)
        {
        }

        public BindingList<ViewModelBancoCuenta> GetCuentasDeBanco()
        {
            return new BindingList<ViewModelBancoCuenta>(this.Db.Queryable<ViewModelBancoCuenta>().Where(it => it.IsActive == true).ToList());
        }

        public ViewModelBancoSaldo Insert(ViewModelBancoSaldo objeto)
        {
            if (this.Db.Insertable(objeto).ExecuteCommand() > 0)
            {
                objeto.Mes = objeto.FechaInicial.Month;
                objeto.Anio = objeto.FechaInicial.Year;
                return objeto;
            }
            return null;
        }

        public ViewModelBancoSaldo GetSaldo(ViewModelBancoCuenta objeto, DateTime fecha)
        {
            int mes = fecha.Month;
            int anio = fecha.Year;

            return this.Db.Queryable<ViewModelBancoSaldo>().Where(it => it.SubId == objeto.Id && it.Mes == mes && it.Anio == anio).Single();
        }

        public ViewModelBancoCuenta Save(ViewModelBancoCuenta objeto)
        {
            if (objeto.Id == 0)
            {
                objeto.Id = this.Db.Insertable(objeto).ExecuteReturnIdentity();
            }
            else
            {
                this.Db.Updateable(objeto).ExecuteCommand();
            }
            return objeto;
        }
    }
}
