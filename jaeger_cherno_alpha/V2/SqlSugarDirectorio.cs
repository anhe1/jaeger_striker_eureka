﻿using System;
using System.ComponentModel;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Edita.V2.Directorio.Enums;
using Jaeger.Edita.Interfaces;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class SqlSugarDirectorio : MySqlSugarContext<ViewModelContribuyente>, ISqlDirectorio
    {
        public SqlSugarDirectorio(DataBaseConfiguracion objeto) : base(objeto)
        {
            this.Bancos = new SqlSugarDirectorioBancos(objeto);
            this.Domicilio = new SqlSugarDirectorioDomicilio(objeto);
        }

        /// <summary>
        /// Cuentas de Bancos relacionadas en el directorio
        /// </summary>
        public SqlSugarDirectorioBancos Bancos { get; set; }

        /// <summary>
        /// Domicilios relacionados al directorio
        /// </summary>
        public SqlSugarDirectorioDomicilio Domicilio { get; set; }

        public int ReturnId(string rfc)
        {
            int indice = this.Db.Queryable<ViewModelContribuyente>().Where(it => it.RFC == rfc).Select(it => it.Id).First();
            return indice;
        }

        public int Clave(string clave)
        {
            return this.Db.Queryable<ViewModelContribuyente>().Where(it => it.Clave == clave).Select(it => it.Id).First();
        }

        /// <summary>
        /// obtener un objeto contribuyente a traves de su indice
        /// </summary>
        public ViewModelContribuyente GetByRFC(string rfc)
        {
            ViewModelContribuyente response = this.Db.Queryable<ViewModelContribuyente>().Where(it => it.RFC == rfc).Single();
            if (response != null)
            {
                response.Domicilios = this.Domicilio.GetList(response.Id);
                response.CuentasBancarias = this.Bancos.GetList(response.Id, false);
                response.Contactos = new BindingList<ViewModelContacto>(this.Db.Queryable<ViewModelContacto>().Where(it => it.SubId == response.Id).ToList());
            }
            return response;
        }

        public ViewModelContribuyenteDomicilio GetSingleRFC(string rfc)
        {
            return this.Db.Queryable<ViewModelContribuyente, ViewModelDomicilio>(
                (c, d) => new object[]
                {
                    JoinType.Left, c.Id == d.SubId && d.TipoText == Enum.GetName(typeof(EnumDomicilioTipo), EnumDomicilioTipo.Fiscal)
                })
                .Where((c, d) => c.IsActive == true)
                .OrderBy(c => c.Nombre, OrderByType.Asc)
                .Select((c, d) => new ViewModelContribuyenteDomicilio
                {
                    Id = c.Id,
                    Clave = c.Clave,
                    RFC = c.RFC,
                    Nombre = c.Nombre,
                    Correo = c.Correo,
                    Calle = d.Calle,
                    CodigoPostal = d.CodigoPostal,
                    Colonia = d.Colonia,
                    Estado = d.Estado,
                    Municipio = d.Municipio,
                    NoExterior = d.NoExterior,
                    NoInterior = d.NoInterior,
                    Pais = d.Pais,
                    Telefono = c.Telefono,
                    Ciudad = d.Ciudad,
                    Creo = c.Creo,
                    ClaveUsoCFDI = c.ClaveUsoCFDI,
                    ResidenciaFiscal = c.ResidenciaFiscal,
                    FechaNuevo = c.FechaNuevo
                }).First();
        }

        /// <summary>
        /// obtener un objeto contribuyente a traves de su indice
        /// </summary>
        public new ViewModelContribuyente GetById(int index)
        {
            ViewModelContribuyente response = this.Db.Queryable<ViewModelContribuyente>().Where(it => it.Id == index).Single();
            if (response != null)
            {
                response.Domicilios = this.Domicilio.GetList(index);
                response.CuentasBancarias = this.Bancos.GetList(index, false);
                response.Contactos = new BindingList<ViewModelContacto>(this.Db.Queryable<ViewModelContacto>().Where(it => it.SubId == index).ToList());
            }
            return response;
        }

        /// <summary>
        /// obtener lista de contribuyentes dependiendo del tipo, la vista incluye el domicilio fiscal
        /// </summary>
        /// <param name="tipo">Tipo de relacion con la empresa</param>
        /// <returns>Listado de contribuyentes con domicilio fiscal</returns>
        public BindingList<ViewModelContribuyenteDomicilio> GetListBy(EnumRelationType tipo)
        {
            List<ViewModelContribuyenteDomicilio> lista = this.Db.Queryable<ViewModelContribuyente, ViewModelDomicilio>(
                (c, d) => new object[] 
                { 
                    JoinType.Left, c.Id == d.SubId && d.TipoText == Enum.GetName(typeof(EnumDomicilioTipo), EnumDomicilioTipo.Fiscal)
                })
                .Where((c, d) => c.IsActive == true && c.Relacion.Contains(Enum.GetName(typeof(EnumRelationType), tipo)))
                .OrderBy(c => c.Nombre, OrderByType.Asc)
                .Select((c, d) => new ViewModelContribuyenteDomicilio
                {
                    Id = c.Id,
                    Clave = c.Clave,
                    RFC = c.RFC,
                    Nombre = c.Nombre,
                    Correo = c.Correo,
                    Calle = d.Calle,
                    CodigoPostal = d.CodigoPostal,
                    Colonia = d.Colonia,
                    Estado = d.Estado,
                    Municipio = d.Municipio,
                    NoExterior = d.NoExterior,
                    NoInterior = d.NoInterior,
                    Pais = d.Pais,
                    Telefono = c.Telefono,
                    Ciudad = d.Ciudad,
                    Creo = c.Creo,
                    ClaveUsoCFDI = c.ClaveUsoCFDI,
                    ResidenciaFiscal = c.ResidenciaFiscal,
                    FechaNuevo = c.FechaNuevo
                }).ToList();

            return new BindingList<ViewModelContribuyenteDomicilio>(lista);
        }

        /// <summary>
        /// obtener listado de contrinuyente por el tipo de relacion con la empresa
        /// </summary>
        /// <param name="tipo">Cliente, Proveedor</param>
        /// <returns>Lista de objetos ViewModelContribuyenteSingle</returns>
        public List<ViewModelContribuyenteSingle> GetList(EnumRelationType tipo)
        {
            return new List<ViewModelContribuyenteSingle>(this.Db.Queryable<ViewModelContribuyenteSingle>().Where(it => it.Relacion.Contains(Enum.GetName(typeof(EnumRelationType), tipo)) && it.IsActive == true).OrderBy(it => it.Nombre).ToList());
        }

        public DataTable GetContactos(EnumRelationType relationship, bool showDeletes = false)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        /// <summary>
        /// guardar un contribuyente nuevo
        /// </summary>
        public ViewModelContribuyente Save(ViewModelContribuyente objeto)
        {
            if (objeto.Id == 0)
            {
                objeto.Id = this.Insert(objeto);
                if (objeto.Id > 0)
                {
                    objeto = this.GuardaDomicilios(objeto);
                    objeto = this.GuardaCuentasBancarias(objeto);
                    objeto = this.GuardarContactos(objeto);
                }
            }
            else
            {
                if (this.Update(objeto) > 0)
                {
                    objeto = this.GuardaDomicilios(objeto);
                    objeto = this.GuardaCuentasBancarias(objeto);
                    objeto = this.GuardarContactos(objeto);
                }
            }
            return objeto;
        }        
        
        private ViewModelContribuyente AdoGetById(int index)
        {
            string sqlString = "select * from _drctr where _drctr_id = @index";
            DataTable data = this.Db.Ado.GetDataTable(sqlString, new List<SugarParameter> { new SugarParameter("@index", index) });
            var mapper = new DataNamesMapper<ViewModelContribuyente>();
            ViewModelContribuyente response = mapper.Map(data).FirstOrDefault();
            if (response != null)
            {
                response.Domicilios = this.Domicilio.AdoGetList(index);
                response.CuentasBancarias = this.Bancos.GetList(index, false);
                response.Contactos = new BindingList<ViewModelContacto>();
                return response;
            }
            return null;
        }

        private List<ViewModelContribuyenteDomicilio> AdoGetListBy(EnumRelationType tipo)
        {
            string sqlString = "select * from _drctr left join _drccn on _drctr._drctr_id=_drccn._drccn_drctr_id and _drccn_tp=@type and _drccn_a=1 where _drctr_a=@activo and find_in_set(@relationship,_drctr_rlcn) order by _drctr_nom;";
            DataTable data = this.Db.Ado.GetDataTable(sqlString, 
                new List<SugarParameter> {
                    new SugarParameter("@activo", 1),
                    new SugarParameter("@relationship", Enum.GetName(typeof(EnumRelationType), tipo)),
                    new SugarParameter("@type", Enum.GetName(typeof(EnumDomicilioTipo), EnumDomicilioTipo.Fiscal))
                });
            var mapper = new DataNamesMapper<ViewModelContribuyenteDomicilio>();
            return new List<ViewModelContribuyenteDomicilio>(mapper.Map(data).ToList());
        }

        public BindingList<ViewModelVendedor> GetVendedores()
        {
            return null;
        }

        private ViewModelContribuyente GuardaDomicilios(ViewModelContribuyente objeto)
        {
            for (int i = 0; i < objeto.Domicilios.Count; i++)
            {
                if (objeto.Domicilios[i].Id == 0)
                {
                    objeto.Domicilios[i].SubId = objeto.Id;
                    objeto.Domicilios[i].Id = this.Domicilio.Insert(objeto.Domicilios[i]);
                }
                else
                {
                    this.Domicilio.Update(objeto.Domicilios[i]);
                }
            }

            return objeto;
        }

        public BindingList<ViewModelDomicilio> GetDomicilios(int index)
        {
            return this.Domicilio.GetList(index);
        }

        private ViewModelContribuyente GuardaCuentasBancarias(ViewModelContribuyente objeto)
        {
            for (int i = 0; i < objeto.CuentasBancarias.Count; i++)
            {
                if (objeto.CuentasBancarias[i].Id == 0)
                {
                    objeto.CuentasBancarias[i].SubId = objeto.Id;
                    objeto.CuentasBancarias[i].Id = this.Bancos.Insert(objeto.CuentasBancarias[i]);
                }
                else
                {
                    this.Bancos.Update(objeto.CuentasBancarias[i]);
                }
            }

            return objeto;
        }

        private ViewModelContribuyente GuardarContactos(ViewModelContribuyente objeto)
        {
            for (int i = 0; i < objeto.Contactos.Count; i++)
            {
                if (objeto.Contactos[i].Id == 0)
                {
                    objeto.Contactos[i].SubId = objeto.Id;
                    objeto.Contactos[i].Id = this.Db.Insertable(objeto.Contactos[i]).ExecuteReturnIdentity();
                }
                else
                {
                    this.Db.Updateable(objeto.Contactos[i]).ExecuteCommand();
                }
            }

            return objeto;
        }

    }
}
