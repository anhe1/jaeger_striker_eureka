﻿using System.Collections.Generic;
using SqlSugar;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2.Almacen.Enums;
using Jaeger.Edita.V2.Almacen.Entities;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    /// <summary>
    /// catalogo de modelos, utilizado para productos del almacen de materia prima
    /// </summary>
    public class SqlSugarModelos : MySqlSugarContext<ViewModelModelo>, ISqlModelos
    {
        public SqlSugarModelos(DataBaseConfiguracion configuracion)
            : base(configuracion)
        {
            this.Categorias = new SqlSugarClasificacion(configuracion);
            this.Unidades = new SqlSugarAlmacenUnidades(configuracion);
        }

        /// <summary>
        /// Categorias
        /// </summary>
        public SqlSugarClasificacion Categorias { get; set; }

        /// <summary>
        /// catalogo de unidades del almacen
        /// </summary>
        public SqlSugarAlmacenUnidades Unidades { get; set; }

        /// <summary>
        /// almacenar un objeto modelo del almacen
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ViewModelModelo Save(ViewModelModelo item)
        {
            if (item.IdModelo == 0)
                item.IdModelo = this.Insert(item);
            else
                this.Update(item);
            return item;
        }

        /// <summary>
        /// obtener listado de productos / modelos por el tipo de almacen
        /// </summary>
        /// <param name="tipo">Almacen MP ó Almacen PT</param>
        /// <returns></returns>
        public List<ViewModelModelo> GetList(EnumAlmacen tipo, bool activo = true)
        {
            return this.Db.Queryable<ViewModelModelo>().Where(it => it.Almacen == tipo).WhereIF(activo == true, it => it.Activo == activo).ToList();
        }

        /// <summary>
        /// obtener un modelo por numero de indentifiacion SKU
        /// </summary>
        /// <param name="sku"></param>
        /// <returns>objeto modelo</returns>
        public ViewModelModelo GetByIdentificador(string sku)
        {
            return this.Db.Queryable<ViewModelModelo>().Where(it => it.NoIdentificacion == sku).Where(it => it.Activo == true).Single();
        }

        /// <summary>
        /// obtener una lista de objetos por la descripcion o etiquetas del modelo
        /// </summary>
        /// <param name="descripcion">descripcion o etiqueta relacionado al modelo</param>
        /// <returns>Lista de objeto ViewModelModelo</returns>
        //public List<ProductoModelo> GetListByDescripcion(string descripcion)
        //{
        //    return 
        //    this.Db.Queryable<ViewModelProductoServicioSingle, ViewModelModeloSingle>(
        //    (producto, modelo) => new JoinQueryInfos(JoinType.Left, producto.IdProducto == modelo.IdProducto)).Where((producto, modelo) => producto.Nombre.Contains(descripcion) | modelo.Descripcion.Contains(descripcion) && modelo.Activo == true && producto.Activo == true).Select((producto, modelo) => new ProductoModelo
        //    {
        //        IdProducto = producto.IdProducto,
        //        IdCategoria = producto.IdCategoria,
        //        Nombre = producto.Nombre,
        //        IdModelo = modelo.IdModelo,
        //        Activo = modelo.Activo,
        //        Almacen = modelo.Almacen,
        //        Alto = modelo.Alto,
        //        Ancho = modelo.Ancho,
        //        ClaveProdServ = modelo.ClaveProdServ,
        //        ClaveUnidad = modelo.ClaveUnidad,
        //        Codigo = modelo.Codigo,
        //        Creo = modelo.Creo,
        //        Descripcion = modelo.Descripcion,
        //        Especificacion = modelo.Especificacion,
        //        Etiquetas = modelo.Etiquetas,
        //        Existencia = modelo.Existencia,
        //        FechaModifica = modelo.FechaModifica,
        //        FechaNuevo = modelo.FechaNuevo,
        //        IdUnidad = modelo.IdUnidad,
        //        Largo = modelo.Largo,
        //        Marca = modelo.Marca,
        //        Maximo = modelo.Maximo,
        //        Minimo = modelo.Minimo,
        //        Modifica = modelo.Modifica,
        //        NoIdentificacion = modelo.NoIdentificacion,
        //        NumRequerimiento = modelo.NumRequerimiento,
        //        Peso = modelo.Peso,
        //        ReOrden = modelo.ReOrden,
        //        Tipo = modelo.Tipo,
        //        Unidad = modelo.Unidad,
        //        UnidadXY = modelo.UnidadXY,
        //        UnidadZ = modelo.UnidadZ,
        //        Unitario = modelo.Unitario
        //    }).ToList();
        //}

        /// <summary>
        /// obtener listado de producto y modelos corresponientes al almacen, descripion y el indice del proveedor
        /// </summary>
        public List<ProductoModeloBusqueda> GetListByDescripcion(EnumAlmacen almacen, string descripcion, int idProveedor)
        {
            return
            this.Db.Queryable<ViewModelProductoServicioSingle, ViewModelModeloSingle, ViewModelModeloProveedor>(
            (producto, modelo, proveedor) => new JoinQueryInfos(JoinType.Left, producto.IdProducto == modelo.IdProducto, JoinType.Left, modelo.IdModelo == proveedor.IdModelo))
            .Where((producto, modelo, proveedor) => producto.Nombre.Contains(descripcion) | modelo.Descripcion.Contains(descripcion) && modelo.Activo == true && producto.Activo == true && proveedor.IdProveedor == idProveedor)
            .Select((producto, modelo, proveedor) => new ProductoModeloBusqueda
            {
                IdProducto = producto.IdProducto,
                IdCategoria = producto.IdCategoria,
                Nombre = producto.Nombre,
                IdModelo = modelo.IdModelo,
                Almacen = modelo.Almacen,
                Alto = modelo.Alto,
                Ancho = modelo.Ancho,
                ClaveProdServ = modelo.ClaveProdServ,
                ClaveUnidad = modelo.ClaveUnidad,
                Codigo = modelo.Codigo,
                Descripcion = modelo.Descripcion,
                Especificacion = modelo.Especificacion,
                Etiquetas = modelo.Etiquetas,
                FechaNuevo = modelo.FechaNuevo,
                IdUnidad = modelo.IdUnidad,
                Largo = modelo.Largo,
                Marca = modelo.Marca,
                NoIdentificacion = modelo.NoIdentificacion,
                NumRequerimiento = modelo.NumRequerimiento,
                Peso = modelo.Peso,
                Tipo = modelo.Tipo,
                Unidad = modelo.Unidad,
                UnidadXY = modelo.UnidadXY,
                UnidadZ = modelo.UnidadZ,
                Unitario = proveedor.Unitario,
                UnitarioBase = modelo.Unitario,
                FactorRetencionIEPS = modelo.FactorRetencionIEPS,
                FactorTrasladoIEPS = modelo.FactorTrasladoIEPS,
                FactorTrasladoIVA = modelo.FactorTrasladoIVA,
                ValorRetecionISR = modelo.ValorRetecionISR,
                ValorRetencionIEPS = modelo.ValorRetencionIEPS,
                ValorRetencionIVA = modelo.ValorRetencionIVA,
                ValorTrasladoIEPS = modelo.ValorTrasladoIEPS,
                ValorTrasladoIVA = modelo.ValorTrasladoIVA,
                IdProveedor = proveedor.IdProveedor
            }).ToList();
        }

        /// <summary>
        /// obtener listado de producto y modelos corresponientes al almacen, descripion
        /// </summary>
        public List<ProductoModeloBusqueda> GetListByDescripcion1(EnumAlmacen almacen, string descripcion)
        {
            return
            this.Db.Queryable<ViewModelProductoServicioSingle, ViewModelModeloSingle>(
            (producto, modelo) => new JoinQueryInfos(JoinType.Left, producto.IdProducto == modelo.IdProducto))
            .Where((producto, modelo) => producto.Nombre.Contains(descripcion) | modelo.Descripcion.Contains(descripcion) && modelo.Activo == true && producto.Activo == true && modelo.Almacen == almacen)
            .Select((producto, modelo) => new ProductoModeloBusqueda
            {
                IdProducto = modelo.IdProducto,
                IdCategoria = producto.IdCategoria,
                Nombre = producto.Nombre,
                IdModelo = modelo.IdModelo,
                Almacen = modelo.Almacen,
                Alto = modelo.Alto,
                Ancho = modelo.Ancho,
                ClaveProdServ = modelo.ClaveProdServ,
                ClaveUnidad = modelo.ClaveUnidad,
                Codigo = modelo.Codigo,
                Descripcion = modelo.Descripcion,
                Especificacion = modelo.Especificacion,
                Etiquetas = modelo.Etiquetas,
                FechaNuevo = modelo.FechaNuevo,
                IdUnidad = modelo.IdUnidad,
                Largo = modelo.Largo,
                Marca = modelo.Marca,
                NoIdentificacion = modelo.NoIdentificacion,
                NumRequerimiento = modelo.NumRequerimiento,
                Peso = modelo.Peso,
                Tipo = modelo.Tipo,
                Unidad = modelo.Unidad,
                UnidadXY = modelo.UnidadXY,
                UnidadZ = modelo.UnidadZ,
                Unitario = modelo.Unitario,
                UnitarioBase = modelo.Unitario,
                FactorRetencionIEPS = modelo.FactorRetencionIEPS,
                FactorTrasladoIEPS = modelo.FactorTrasladoIEPS,
                FactorTrasladoIVA = modelo.FactorTrasladoIVA,
                ValorRetecionISR = modelo.ValorRetecionISR,
                ValorRetencionIEPS = modelo.ValorRetencionIEPS,
                ValorRetencionIVA = modelo.ValorRetencionIVA,
                ValorTrasladoIEPS = modelo.ValorTrasladoIEPS,
                ValorTrasladoIVA = modelo.ValorTrasladoIVA
            }).ToList();
        }

        /// <summary>
        /// obtener listado de productos del almacen de producto terminado para CFDI
        /// </summary>
        /// <param name="almacen">tipo de almacen</param>
        /// <returns>lista de objetos List<ViewModelComprobanteModelo></returns>
        public List<ViewModelComprobanteModelo> GetListConceptos(EnumAlmacen almacen, string descripcion = "")
        {
            return this.Db.Queryable<ViewModelComprobanteModelo>().Where(it => it.Almacen == almacen).Where(it => it.Activo == true).WhereIF(descripcion != "", it=> it.Descripcion.Contains(descripcion)).ToList();
        }

        public bool Delete(ViewModelModelo modelo)
        {
            return this.Update(modelo) > 0;
        }

        public bool CrearTablas()
        {
            return this.CreateTable();
        }
    }
}
