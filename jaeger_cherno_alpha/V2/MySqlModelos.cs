﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using Jaeger.Edita.V2.Almacen.Enums;
using Jaeger.Edita.V2.Almacen.Entities;
using Jaeger.Edita.Interfaces;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.Edita.V2
{
    public class MySqlModelos : MySqlSugarContext<ViewModelModelo>, ISqlModelos
    {
        public MySqlModelos(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public SqlSugarClasificacion Categorias { get; set; }

        public SqlSugarAlmacenUnidades Unidades { get; set; }

        public int Insert(ViewModelModelo item)
        {
            var sqlCommand = new MySqlCommand()
            {
                CommandText = "insert into "
            };

            return this.ExecuteScalar(sqlCommand);
        }

        public int Update(ViewModelModelo item)
        {
            var sqlCommand = new MySqlCommand()
            {
                CommandText = "insert into "
            };
            return 0;
        }

        public ViewModelModelo Save(ViewModelModelo item)
        {
            if (item.IdModelo == 0)
                item.IdModelo = this.Insert(item as ViewModelModeloSingle);
            else
                this.Update(item as ViewModelModeloSingle);
            return item;
        }

        public List<ViewModelModelo> GetList(EnumAlmacen tipo, bool activo = true)
        {
            var sqlCommand = new MySqlCommand() { CommandText = "select * from _ctlmdl where _ctlmdl_alm_id=@tipo and _ctlmdl_a = 1;" };

            if (activo == false)
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("and _ctlmdl_a = 1", "");

            sqlCommand.Parameters.AddWithValue("@tipo", tipo);
            DataTable tabla = this.ExecuteReader(sqlCommand);
            DataNamesMapper<ViewModelModelo> mapper = new DataNamesMapper<ViewModelModelo>();
            return mapper.Map(tabla).ToList();
        }

        public ViewModelModelo GetByIdentificador(string sku)
        {
            var sqlCommand = new MySqlCommand() { CommandText = "select * from _ctlmdl where _ctlmdl_a = 1 and _ctlmdl_sku = @sku limit 1;" };
            sqlCommand.Parameters.AddWithValue("@sku", sku);
            DataTable tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelModelo>();
            return mapper.Map(tabla).FirstOrDefault();
        }

        //public List<ProductoModelo> GetListByDescripcion(string descripcion)
        //{
        //    var sqlCommand = new MySqlCommand() { CommandText = "SELECT * FROM _ctlprd producto Left JOIN _ctlmdl modelo ON (producto._ctlprd_id = modelo._ctlmdl_idpro) WHERE ((((producto._ctlprd_nom like concat('%',@MethodConst0,'%')) OR (modelo._ctlmdl_dscrc like concat('%',@MethodConst1,'%')) ) AND ( modelo._ctlmdl_a = @Activo2)) AND (producto._ctlprd_a = @Activo3)) ;" };
        //    sqlCommand.Parameters.AddWithValue("@MethodConst0", descripcion);
        //    sqlCommand.Parameters.AddWithValue("@MethodConst1", descripcion);
        //    sqlCommand.Parameters.AddWithValue("@Activo2",1);
        //    sqlCommand.Parameters.AddWithValue("@Activo2", 1);
        //    DataTable tabla = this.GetDataTable(sqlCommand);
        //    var mapper = new DataNamesMapper<ProductoModelo>();
        //    return mapper.Map(tabla).ToList();
        //}

        public bool Delete(ViewModelModelo modelo)
        {
            var sqlCommand = new MySqlCommand() { CommandText = "update _ctlmdl set _ctlmdl_a = @valor where _ctlmdl_id = @index;" };
            sqlCommand.Parameters.AddWithValue("@index", modelo.IdModelo);
            sqlCommand.Parameters.AddWithValue("@valor", (modelo.Activo == true ? 1 : 0));
            return this.ExecuteTransaction(sqlCommand) > 0;
        }


        public List<CFDI.Entities.ViewModelComprobanteModelo> GetListConceptos(EnumAlmacen almacen, string descripcion = "")
        {
            var sqlCommand = new MySqlCommand() { CommandText = "select * from _ctlmdl where (_ctlmdl_alm_id = @Const0) and (_ctlmdl_a = @Activo1) and (_ctlmdl_dscrc like concat('%',@MethodConst2,'%'));" };
            sqlCommand.Parameters.AddWithValue("@Const0", almacen);
            sqlCommand.Parameters.AddWithValue("@Activo1", 1);
            sqlCommand.Parameters.AddWithValue("@MethodConst2", descripcion);
            DataTable tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CFDI.Entities.ViewModelComprobanteModelo>();
            return mapper.Map(tabla).ToList();
        }

        public List<ProductoModeloBusqueda> GetListByDescripcion(EnumAlmacen almacen, string descripcion, int id)
        {
            return null;
        }

        public List<ProductoModeloBusqueda> GetListByDescripcion1(EnumAlmacen almacen, string descripcion)
        {
            return null;
        }

        public bool CrearTablas()
        {
            return false;
        }
    }
}
