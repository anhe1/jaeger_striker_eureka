using System.ComponentModel;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2.Almacen.Entities;

namespace Jaeger.Edita.V2
{
    /// <summary>
    /// clase para control de proveedores de productos o servicios
    /// </summary>
    public class SqlSugarProdServProveedor : MySqlSugarContext<ViewModelModeloProveedor>, ISqlProdServProveedor
    {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public SqlSugarProdServProveedor(DataBaseConfiguracion configuracion) : base(configuracion)
        {
        }

        /// <summary>
        /// activar o desactivar un registro
        /// </summary>
        public bool Delete(ViewModelModeloProveedor proveedor)
        {
            proveedor.Activo = !proveedor.Activo;
            return this.Update(proveedor) > 0;
        }

        public ViewModelModeloProveedor Save(ViewModelModeloProveedor proveedor)
        {
            if (proveedor.Id == 0)
                proveedor.Id = this.Insert(proveedor);
            else
                this.Update(proveedor);
            return proveedor;
        }

        public BindingList<ViewModelModeloProveedor> Save(BindingList<ViewModelModeloProveedor> proveedor)
        {
            for (int i = 0; i < proveedor.Count; i++)
            {
                proveedor[i] = this.Save(proveedor[i]);
            }
            return proveedor;
        }

        public BindingList<ViewModelModeloProveedor> GetListBy(int index, bool activo = true)
        {
            //DataTable response = this.Db.Queryable<ViewModelModeloProveedor, Directorio.Entities.ViewModelContribuyenteSingle>((p, d) => new JoinQueryInfos(JoinType.Left, p.IdProveedor == d.Id))
            //    .Where(p => p.IdModelo == index)
            //    .WhereIF(activo, p => p.Activo == true).Select((p, d) => new ViewModelModeloProveedor
            //    {
            //        Id = p.Id,
            //        IdProveedor = p.IdProveedor,
            //        Activo = p.Activo,
            //        Creo = p.Creo,
            //        IdModelo = p.IdModelo,
            //        FechaNuevo = p.FechaNuevo,
            //        Unitario = p.Unitario,
            //        Nombre = d.Nombre
            //    }).ToDataTable();
            //DataNamesMapper<ViewModelModeloProveedor> map = new DataNamesMapper<ViewModelModeloProveedor>();
            //return new BindingList<ViewModelModeloProveedor>(map.Map(response).ToList());
            return new BindingList<ViewModelModeloProveedor>(this.Db.Queryable<ViewModelModeloProveedor>().Where(it => it.IdModelo == index).WhereIF(activo, it => it.Activo == true).ToList());
        }

        public bool CrearTablas()
        {
            return this.CreateTable();
        }
    }
}