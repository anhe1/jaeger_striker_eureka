﻿using System.ComponentModel;
using System.Linq;
using MySql.Data.MySqlClient;
using Jaeger.Edita.V2.Almacen.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class MySqlClasificacion : MySqlSugarContext<ViewModelCategoriaSingle> {
        public MySqlClasificacion(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public BindingList<ViewModelCategoriaSingle> GetCategorias()
        {
            var sqlCommand = new MySqlCommand()
            {
                CommandText = string.Concat("select Nivel3._ctlclss_id AS Id, Nivel2._ctlclss_sbid AS SubId, ",
                                            "Case When (Nivel1._ctlclss_class1 = '' And Nivel2._ctlclss_class1 = '') then Nivel3._ctlclss_class1 When (Nivel1._ctlclss_class1 = '') then Nivel2._ctlclss_class1 Else Nivel1._ctlclss_class1 END AS Tipo, ",
                                            "Case When Nivel2._ctlclss_class1 = '' then Nivel3._ctlclss_class1 Else Nivel2._ctlclss_class1 END AS Clase, Nivel3._ctlclss_class1 AS Descripcion, Nivel3._ctlclss_class2 AS Clave ",
                                            "from _ctlclss Nivel1 ",
                                            "join _ctlclss Nivel2 on Nivel2._ctlclss_sbid = Nivel1._ctlclss_id ",
                                            "join _ctlclss Nivel3 on Nivel3._ctlclss_sbid = Nivel2._ctlclss_id  ")
            };
            return new BindingList<ViewModelCategoriaSingle>(this.GetMapper<ViewModelCategoriaSingle>(sqlCommand.CommandText).ToList());
        }
    }
}
