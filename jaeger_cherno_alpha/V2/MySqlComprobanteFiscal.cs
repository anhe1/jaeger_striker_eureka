﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.Enums;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2.Contable.Entities;
using Jaeger.Entities.Reportes;
using Jaeger.SAT.Entities;
using Jaeger.Edita.V2.CFDI.Entities.Cancel;
using Jaeger.Enums;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Services;
using Jaeger.Helpers;

namespace Jaeger.Edita.V2 {
    public class MySqlComprobanteFiscal : MySqlSugarContext<ViewModelComprobante>, ISqlComprobanteFiscal {
        public MySqlComprobanteFiscal(DataBaseConfiguracion objeto) : base(objeto) {
            this.Nomina = new MySqlComprobanteNomina(objeto);
        }

        public MySqlComprobanteNomina Nomina { get; set; }

        public void MySqlComprobanteFiscal_ProcessError(object sender, Jaeger.Entities.ProgressError e) {
            Console.WriteLine(e.Data);
        }

        /// <summary>
        /// obtener el indice de un comprobante por su folio fiscal (UUID)
        /// </summary>
        public int ReturnId(string idDocumento) {
            int indice = 0;
            var sqlCommand = new MySqlCommand() { CommandText = "select _cfdi_id from _cfdi where _cfdi_uuid like @uuid;" };
            sqlCommand.Parameters.AddWithValue("@uuid", idDocumento);
            indice = this.ExecuteScalar(sqlCommand);
            return indice;
        }

        /// <summary>
        /// almacenar un objeto Comprobante en la base de datos
        /// </summary>
        /// <param name="comprobante">ViewModelComprobante</param>
        /// <returns>ViewModelComprobante</returns>
        public ViewModelComprobante Save(ViewModelComprobante item) {
            // insertar el comprobante
            if (item.Id == 0) {
                // si es un comprobante emitido creamos el nuevo folio a partir de la serie
                if (item.SubTipo == EnumCfdiSubType.Emitido)
                    item.Folio = this.GetFolio(item.Emisor.RFC, item.Serie).ToString();
                item.FechaNuevo = DateTime.Now;
                //item.Id = this.Insert(item).Id;
                var result = this.Db.Insertable(item);
                item.Id = this.Execute(result);
            } else {
                if (this.Update(item).Id == 0)
                    return item;
            }

            // almacenar los conceptos
            //if (item.Conceptos != null) {
            //    for (int i = 0; i < item.Conceptos.Count; i++) {
            //        if (item.Conceptos[i].Id == 0) {
            //            item.Conceptos[i].SubId = item.Id;
            //            item.Conceptos[i].FechaNuevo = DateTime.Now;
            //            item.Conceptos[i].Creo = item.Creo;
            //            item.Conceptos[i].Id = this.Insert(item.Conceptos[i]).Id;
            //        } else {
            //            this.Update(item.Conceptos[i]);
            //        }
            //    }
            //}

            if (item.TimbreFiscal != null) {
                // si contiene un complemento de pagos
                if (item.ComplementoPagos != null)
                    this.AplicarComprobantePagos(item.ComplementoPagos);

                // cfdi relacionados
                if (item.CfdiRelacionados != null)
                    if (item.CfdiRelacionados.CfdiRelacionado != null)
                        this.AplicarCFDIRelacionados(item.CfdiRelacionados);
            }

            if (item.Nomina != null) {

            }

            return item;
        }

        /// <summary>
        /// actualizar url del archivo XML del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">direccion URL</param>
        /// <returns>verdadero si la acción se realiza con exito</returns>
        public bool UpdateUrlXml(int index, string url) {
            var sqlUpdate = new MySqlCommand() { CommandText = "update _cfdi set _cfdi_url_xml=@url where _cfdi_id=@index;" };
            sqlUpdate.Parameters.AddWithValue("@index", index);
            sqlUpdate.Parameters.AddWithValue("@url", url);
            return this.ExecuteTransaction(sqlUpdate) > 0;
        }

        /// <summary>
        /// actualizar url de la representacion impresa del comprobante fiscal (pfd)
        /// </summary>
        /// <param name="indice">indice</param>
        /// <param name="url">url</param>
        /// <returns></returns>
        public bool UpdateUrlPdf(int index, string url) {
            var sqlUpdate = new MySqlCommand() {
                CommandText = "update _cfdi set _cfdi_url_pdf=@url where _cfdi_id=@index"
            };
            sqlUpdate.Parameters.AddWithValue("@index", index);
            sqlUpdate.Parameters.AddWithValue("@url", url);
            return this.ExecuteTransaction(sqlUpdate) > 0;
        }

        /// <summary>
        /// actualizar url del acuse de cancelacion del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">url</param>
        /// <returns>verdadero si la accion se realiza con exito</returns>
        public bool UpdateUrlXmlAcuse(int index, string url) {
            var sqlUpdate = new MySqlCommand() { CommandText = "update _cfdi set _cfdi_url_xmlacu=@url where _cfdi_id=@index;" };
            sqlUpdate.Parameters.AddWithValue("@index", index);
            sqlUpdate.Parameters.AddWithValue("@url", url);
            return this.ExecuteTransaction(sqlUpdate) > 0;
        }

        /// <summary>
        /// actualizar url del acuse de cancelacion del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">url</param>
        /// <returns>verdadero si la accion se realiza con exito</returns>
        public bool UpdateUrlPdfAcuse(int index, string url) {
            var sqlUpdate = new MySqlCommand() {
                CommandText = "update _cfdi set _cfdi_url_pdfacu=@url where _cfdi_id=@index"
            };
            sqlUpdate.Parameters.AddWithValue("@index", index);
            sqlUpdate.Parameters.AddWithValue("@url", url);
            return this.ExecuteTransaction(sqlUpdate) > 0;
        }

        /// <summary>
        /// actualizar estado del comprobante segun SAT
        /// </summary>
        /// <param name="response">objeto respuesta del servicio SAT</param>
        /// <returns>verdadero si fue actualizado el registro con exito</returns>
        public bool Update(SatQueryResult response) {
            var sqlCommand = new MySqlCommand() { CommandText = "update _cfdi set _cfdi_estado=@estado, _cfdi_fecedo=@fecha where _cfdi_id=@index;" };
            sqlCommand.Parameters.AddWithValue("@estado", response.Status);
            sqlCommand.Parameters.AddWithValue("@fecha", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@index", response.Id);
            if (this.ExecuteTransaction(sqlCommand) > 0) {
                //this.Message = new MessageError { Data = "Error al actualizar el estado del comprobante" };
                return true;
            }
            return false;
            Console.WriteLine("No se actualizo el estado del comprobante.");
        }

        /// <summary>
        /// actualizar el objeto del de cancelacion de un comprobante fiscal
        /// </summary>
        public bool Update(string uuid, CancelaCFDResponse item, string usuario) {
            var sqlCommand = new MySqlCommand() { CommandText = "update _cfdi set _cfdi_acuse=@acuse, _cfdi_feccnc=@fecha1, _cfdi_status=@status, _cfdi_usr_m=@modifica, _cfdi_fm=@fecha2 where _cfdi_uuid=@uuid;" };
            sqlCommand.Parameters.AddWithValue("@acuse", item.Xml());
            sqlCommand.Parameters.AddWithValue("@fecha1", item.CancelaCFDResult.Fecha);
            sqlCommand.Parameters.AddWithValue("@modifica", usuario);
            sqlCommand.Parameters.AddWithValue("@fecha2", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@uuid", uuid);
            if (this.ExecuteTransaction(sqlCommand) > 0) {
                //this.Message = new MessageError { Data = "Error al actualizar el estado del comprobante" };
                return true;
            }
            Console.WriteLine("No se actualizo el estado del comprobante.");
            return false;
        }

        /// <summary>
        /// actualizar status desde la lista de comprobantes
        /// </summary>
        public bool Update(int indice, string status, string usuario) {
            var sqlCommand = new MySqlCommand() { CommandText = "update _cfdi set _cfdi_status = @status, _cfdi_fecent = @fecha1, _cfdi_fm = @fecha2, _cfdi_usr_m = @modifica where _cfdi_id = @index;" };
            sqlCommand.Parameters.AddWithValue("@index", indice);
            sqlCommand.Parameters.AddWithValue("@status", status);
            sqlCommand.Parameters.AddWithValue("@fecha1", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@fecha2", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@modifica", usuario);
            if (this.ExecuteTransaction(sqlCommand) == 0) {
                //this.Message = new MessageError { Data = "Error al actualizar el estado del comprobante" };
                Console.WriteLine("No se actualizo el estado del comprobante.");
                return false;
            }
            return true;
        }

        public ViewModelComprobante GetComprobante(int indice) {
            var sqlCommand = new MySqlCommand() { CommandText = "select * from _cfdi where _cfdi_id = @index limit 1;" };
            sqlCommand.Parameters.AddWithValue("@index", indice);
            DataTable tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelComprobante>();
            ViewModelComprobante response = mapper.Map(tabla).First();
            if (response != null)
                response.Conceptos = this.GetConceptos(response.Id);

            if (response.SubTipo == EnumCfdiSubType.Nomina)
                response.Nomina = this.Nomina.GetBy(response.Id);
            return response;
        }

        /// <summary>
        /// obtener un objeto simple de comprobante fiscal
        /// </summary>
        /// <param name="idDocumento">IdDocumento (uuid)</param>
        /// <returns>objeto ViewModelComprobanteSingle</returns>
        public ViewModelComprobanteSingle GetSingle(string idDocumento) {
            var sqlCommand = new MySqlCommand() { CommandText = "select * from _cfdi where _cfdi_uuid = @uuid limit 1;" };
            sqlCommand.Parameters.AddWithValue("@uuid", idDocumento);
            DataTable tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelComprobanteSingle>();
            return mapper.Map(tabla).FirstOrDefault();
        }

        /// <summary>
        /// metodo para obtener un comprobante fiscal con el metodo anterior
        /// </summary>
        /// <param name="indice">indice del comprobante</param>
        /// <returns>CFDI.Entities.Comprobante version anterior</returns>
        public Jaeger.CFDI.Entities.Comprobante GetAdoComprobante(int indice) {
            var sqlCommand = new MySqlCommand() { CommandText = "select _cfdi.*, (_cfdi_total - (_cfdi_cbrd + _cfdi_dscntc)) as _cfdi_saldo from _cfdi where _cfdi_id=@index;" };

            sqlCommand.Parameters.AddWithValue("@index", indice);
            DataTable data = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<Jaeger.CFDI.Entities.Comprobante>();
            Jaeger.CFDI.Entities.Comprobante comprobante = mapper.Map(data).FirstOrDefault();
            if (comprobante != null)
                comprobante.Conceptos = this.GetConceptos(indice);
            return comprobante;
        }

        public BindingList<ViewModelComprobanteSingleC> GetListBy(EnumCfdiSubType subtipo, string status, EnumMonthsOfYear mes = 0, int anio = 0, bool ado = true) {
            string sqlCommand = string.Concat("select _cfdi.*, (_cfdi_total - (_cfdi_cbrd + _cfdi_dscntc)) as _cfdi_saldo, if (_cfdi_frmpg='99' and _cfdi_mtdpg='PPD' and _cfdi_efecto like 'I%', (_cfdi_total - (_cfdi_cbrdp + _cfdi_dscntc)), 0) as _cfdi_saldopagos ",
                                              "from _cfdi where _cfdi_a = 1 and _cfdi_doc_id=@tipo status buscar anios order by _cfdi_fecems desc;");

            if (status == "Todos")
                sqlCommand = sqlCommand.Replace("status", "");
            else
                sqlCommand = sqlCommand.Replace("status", "and _cfdi_status=@status");

            if (mes == EnumMonthsOfYear.None)
                sqlCommand = sqlCommand.Replace("buscar", "");
            else
                sqlCommand = sqlCommand.Replace("buscar", "and month(_cfdi_fecems)=@mes");

            if (anio == 0)
                sqlCommand = sqlCommand.Replace("anios", "");
            else
                sqlCommand = sqlCommand.Replace("anios", "and year(_cfdi_fecems)=@anio");

            var command = new MySqlCommand { CommandText = sqlCommand };
            command.Parameters.AddWithValue("@tipo", (int)subtipo);
            command.Parameters.AddWithValue("@mes", (int)mes);
            command.Parameters.AddWithValue("@anio", anio);

            DataTable data = this.ExecuteReader(command);
            DataNamesMapper<ViewModelComprobanteSingleC> mapper = new DataNamesMapper<ViewModelComprobanteSingleC>();

            return new BindingList<ViewModelComprobanteSingleC>(mapper.Map(data).ToList());
        }

        public BindingList<ViewModelComprobanteSingle> GetListSingleBy(EnumCfdiSubType subtipo, string status, EnumMonthsOfYear mes = EnumMonthsOfYear.None, int anio = 0, bool ado = true) {
            throw new NotImplementedException();
        }

        public BindingList<ViewModelComprobanteSingle> GetListSingleBy(EnumCfdiSubType subtipo, string rfc, string folio = "", string status = "", string metodo = "") {
            var sqlCommand = new MySqlCommand() { CommandText = "select * from _cfdi where " };

            if (subtipo == EnumCfdiSubType.Recibido)
                sqlCommand.CommandText = sqlCommand.CommandText + " _cfdi_rfce = @rfc";
            else if (subtipo == EnumCfdiSubType.Emitido)
                sqlCommand.CommandText = sqlCommand.CommandText + " _cfdi_rfcr = @rfc";

            if (status != "")
                sqlCommand.CommandText = sqlCommand.CommandText + " and _cfdi_status like @status";

            if (metodo != "")
                sqlCommand.CommandText = sqlCommand.CommandText + " and _cfdi_ like @metodo";

            if (folio != "")
                sqlCommand.CommandText = sqlCommand.CommandText + " and _cfdi_folio like @folio";

            sqlCommand.CommandText = sqlCommand.CommandText + " order by _cfdi_id desc;";

            sqlCommand.Parameters.AddWithValue("@rfc", rfc);
            sqlCommand.Parameters.AddWithValue("@status", status);
            sqlCommand.Parameters.AddWithValue("@metodo", metodo);
            sqlCommand.Parameters.AddWithValue("@folio", folio);

            DataTable tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelComprobanteSingle>();

            return new BindingList<ViewModelComprobanteSingle>(mapper.Map(tabla).ToList());
        }

        /// <summary>
        /// obtener listado de conceptos de un comprobante fiscal por su id
        /// </summary>
        /// <param name="indice">indice relacionado</param>
        /// <returns></returns>
        public BindingList<ViewModelComprobanteConcepto> GetConceptos(int indice, bool ado = true) {
            var sqlCommand = new MySqlCommand() { CommandText = "select * from _cfdcnp where _cfdcnp_cfds_id=@index;" };
            sqlCommand.Parameters.AddWithValue("@index", indice);
            DataTable tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelComprobanteConcepto>();
            return new BindingList<ViewModelComprobanteConcepto>(mapper.Map(tabla).ToList());
        }

        /// <summary>
        /// obtener una lista de comprobantes fiscales a partir de un array de uuids
        /// </summary>
        /// <param name="uuids">array de uuid de comprobantes</param>
        /// <returns>Lista de objetos ViewModelComprobanteSingle</returns>
        public BindingList<ViewModelComprobanteSingle> GetListIn(string[] uuids) {
            var sqlCommand = new MySqlCommand() { CommandText = "select * from _cfdi where _cfdi_a = 1 and _cfdi_uuid in (@array);" };
            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@array", "'" + string.Join("','", uuids) + "'");
            //sqlCommand.Parameters.AddWithValue("@array", string.Join(",", uuids));

            DataTable tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelComprobanteSingle>();
            return new BindingList<ViewModelComprobanteSingle>(mapper.Map(tabla).ToList());
        }

        /// <summary>
        /// reporte de recibos ficales por rfc del receptor
        /// </summary>
        /// <param name="mes">mes</param>
        /// <param name="anio">año</param>
        /// <param name="rfc">rfc</param>
        /// <returns></returns>
        public BindingList<ViewModelReciboFiscal> GetRecibosFiscales(int mes, int anio, int dia, string rfc) {
            var sqlCommand = new MySqlCommand() {
                CommandText = "select * from _cfdi where _cfdi_rfce=@rfc and _cfdi_serie like 'RF' and month(_cfdi_fecems)=@mes and year(_cfdi_fecems)=@anio order by _cfdi._cfdi_feccert desc;"
            };
            sqlCommand.Parameters.AddWithValue("@anio", anio);
            sqlCommand.Parameters.AddWithValue("@mes", mes);
            sqlCommand.Parameters.AddWithValue("@rfc", rfc);
            DataTable tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelReciboFiscal>();
            return new BindingList<ViewModelReciboFiscal>(mapper.Map(tabla).ToList());
        }

        #region operaciones con prepolizas

        public ViewModelPrePoliza CrearRecibo(List<string> folioFiscal, ComplementoPagos complemento = null) {
            // recibo nuevo
            ViewModelPrePoliza nuevo = new ViewModelPrePoliza();

            // comprobamos si es un comprobante que tenga el complemento
            if (complemento != null) {
                foreach (ComplementoPagosPago item in complemento.Pago) {
                    folioFiscal.AddRange(item.DoctoRelacionado.Where(x => x != null).Select(x => x.IdDocumento).ToList());
                }
            }

            // agregamos informacion de los comprobantes
            List<ViewModelComprobanteSingle> lista = new List<ViewModelComprobanteSingle>(this.GetListIn(folioFiscal.ToArray()));
            foreach (var item in lista) {
                ViewModelPrePolizaComprobante documento = new ViewModelPrePolizaComprobante(item);
                if (documento != null) {
                    documento.IsActive = true;
                    documento.Abono = (documento.SubTipoComprobante == EnumCfdiSubType.Emitido ? documento.Total - documento.Acumulado : 0);
                    documento.Cargo = (documento.SubTipoComprobante == EnumCfdiSubType.Recibido ? documento.Total - documento.Acumulado : 0);
                    nuevo.Concepto = string.Concat(nuevo.Concepto, documento.Serie, " ", documento.Folio, " ");
                    nuevo.Comprobantes.Add(documento);

                    if (documento.SubTipoComprobante == EnumCfdiSubType.Emitido) {
                        nuevo.Tipo = Jaeger.Edita.V2.Contable.Enums.EnumPolizaTipo.Ingreso;
                        nuevo.Receptor.Nombre = documento.Receptor;
                        nuevo.Receptor.Rfc = documento.ReceptorRFC;
                    } else if (documento.SubTipoComprobante == EnumCfdiSubType.Recibido) {
                        nuevo.Tipo = Jaeger.Edita.V2.Contable.Enums.EnumPolizaTipo.Egreso;
                        nuevo.Receptor.Nombre = documento.Emisor;
                        nuevo.Receptor.Rfc = documento.EmisorRFC;
                    }
                }
            }

            nuevo.Concepto = string.Concat((nuevo.Tipo == Jaeger.Edita.V2.Contable.Enums.EnumPolizaTipo.Ingreso ? "Cobro de factura " : "Pago de factura "), nuevo.Concepto);
            nuevo.FechaNuevo = DateTime.Now;
            return nuevo;
        }

        /// <summary>
        /// obtener lista de prepolizas relacionadas a un comprobante fiscal por su uuid
        /// </summary>
        /// <param name="idDocumento">folio fiscal (uuid)</param>
        /// <returns></returns>
        public BindingList<ViewModelPrePoliza> Recibos(string idDocumento) {
            var sqlCommand = new MySqlCommand() {
                CommandText = "SELECT  p._cntbl3_id, p._cntbl3_a, p._cntbl3_status, p._cntbl3_tipo, p._cntbl3_noiden, p._cntbl3_folio, p._cntbl3_serie, p._cntbl3_fecems, p._cntbl3_fecdoc, p._cntbl3_fccbr, p._cntbl3_fcvnc, p._cntbl3_fclib, p._cntbl3_fccncl, p._cntbl3_nodocto, p._cntbl3_numauto , p._cntbl3_ref, p._cntbl3_cncpt, p._cntbl3_cargo, p._cntbl3_abono, p._cntbl3_por, p._cntbl3_nota, p._cntbl3_fn, p._cntbl3_fm, p._cntbl3_frmclv, p._cntbl3_bncclve, p._cntbl3_bncclvr, p._cntbl3_usr_n, p._cntbl3_clve, p._cntbl3_clvr, p._cntbl3_rfce, p._cntbl3_rfcr, p._cntbl3_nmctar, p._cntbl3_scrslr, p._cntbl3_nmctae, p._cntbl3_clbr, p._cntbl3_frmpg, p._cntbl3_bancor, p._cntbl3_benef, p._cntbl3_usr_m, p._cntbl3_usr_c, p._cntbl3_usr_a FROM _cntbl3 p Left JOIN _cntbl3c c ON ( p._cntbl3_noiden = c._cntbl3c_noiden ) WHERE (( c._cntbl3c_a = @IsActive0 ) AND ( c._cntbl3c_uuid = @uuid ));"
            };

            sqlCommand.Parameters.AddWithValue("@uuid", idDocumento);
            sqlCommand.Parameters.AddWithValue("@IsActive0", 1);
            DataTable tabla = this.ExecuteReader(sqlCommand);
            DataNamesMapper<ViewModelPrePoliza> mapper = new DataNamesMapper<ViewModelPrePoliza>();
            return new BindingList<ViewModelPrePoliza>(mapper.Map(tabla).ToList());
            //var response = new BindingList<ViewModelPrePoliza>();
            //foreach (DataRow item in tabla.Rows)
            //{
            //    var nuevo = new ViewModelPrePoliza()
            //    {
            //        Id = Jaeger.Helpers.DbConvert.ConvertInt32(item["Id"]),
            //        IsActive = Jaeger.Helpers.DbConvert.ConvertBool(item["IsActive"]),
            //        EstadoText = Jaeger.Helpers.DbConvert.ConvertString(item["EstadoText"]),
            //        TipoText = Jaeger.Helpers.DbConvert.ConvertString(item["TipoText"]),
            //        NoIndet = Jaeger.Helpers.DbConvert.ConvertString(item["NoIndet"]),
            //        Folio = Jaeger.Helpers.DbConvert.ConvertInt32(item["Folio"]),
            //        Serie = Jaeger.Helpers.DbConvert.ConvertString(item["Serie"]),
            //        FechaEmision = Jaeger.Helpers.DbConvert.ConvertDateTime(item["FechaEmision"]),
            //        FechaDocto = Jaeger.Helpers.DbConvert.ConvertDateTime(item["FechaDocto"]),
            //        FechaPago = Jaeger.Helpers.DbConvert.ConvertDateTime(item["FechaPago"]),
            //        FechaVence = Jaeger.Helpers.DbConvert.ConvertDateTime(item["FechaVence"]),
            //        FechaBoveda = Jaeger.Helpers.DbConvert.ConvertDateTime(item["FechaBoveda"]),
            //        FechaCancela = Jaeger.Helpers.DbConvert.ConvertDateTime(item["FechaCancela"]),
            //        NumDocto = Jaeger.Helpers.DbConvert.ConvertString(item["NumDocto"]),
            //        NumAutorizacion = Jaeger.Helpers.DbConvert.ConvertString(item["NumAutorizacion"]),
            //        Referencia = Jaeger.Helpers.DbConvert.ConvertString(item["Referencia"]),
            //        Concepto = Jaeger.Helpers.DbConvert.ConvertString(item["Concepto"]),
            //        Cargo = Jaeger.Helpers.DbConvert.ConvertDecimal(item["Cargo"]),
            //        Abono = Jaeger.Helpers.DbConvert.ConvertDecimal(item["Abono"]),
            //        PorJustificar = Jaeger.Helpers.DbConvert.ConvertBool(item["PorJustificar"]),
            //        Notas = Jaeger.Helpers.DbConvert.ConvertString(item["Notas"]),
            //        FechaNuevo = Jaeger.Helpers.DbConvert.ConvertDateTime(item["FechaNuevo"]),
            //        FechaMod = Jaeger.Helpers.DbConvert.ConvertDateTime(item["FechaMod"]),
            //        FormaDePagoText = Jaeger.Helpers.DbConvert.ConvertString(item["FormaDePagoText"]),
            //        EmisorCodigo = Jaeger.Helpers.DbConvert.ConvertString(item["EmisorCodigo"]),
            //        ReceptorCodigo = Jaeger.Helpers.DbConvert.ConvertString(item["ReceptorCodigo"]),
            //        Creo = Jaeger.Helpers.DbConvert.ConvertString(item["Creo"]),
            //        EmisorClave = Jaeger.Helpers.DbConvert.ConvertString(item["EmisorClave"]),
            //        ReceptorClave = Jaeger.Helpers.DbConvert.ConvertString(item["ReceptorClave"]),
            //        EmisorRFC = Jaeger.Helpers.DbConvert.ConvertString(item["EmisorRFC"]),
            //        ReceptorRFC = Jaeger.Helpers.DbConvert.ConvertString(item["ReceptorRFC"]),
            //        ReceptorNumCta = Jaeger.Helpers.DbConvert.ConvertString(item["ReceptorNumCta"]),
            //        ReceptorSucursal = Jaeger.Helpers.DbConvert.ConvertString(item["ReceptorSucursal"]),
            //        EmisorNumCta = Jaeger.Helpers.DbConvert.ConvertString(item["EmisorNumCta"]),
            //        ReceptorCLABE = Jaeger.Helpers.DbConvert.ConvertString(item["ReceptorCLABE"]),
            //        FormaPagoDescripcion = Jaeger.Helpers.DbConvert.ConvertString(item["FormaPagoDescripcion"]),
            //        ReceptorBanco = Jaeger.Helpers.DbConvert.ConvertString(item["ReceptorBanco"]),
            //        ReceptorBeneficiario = Jaeger.Helpers.DbConvert.ConvertString(item["ReceptorBeneficiario"]),
            //        Modifica = Jaeger.Helpers.DbConvert.ConvertString(item["Modifica"]),
            //        Cancela = Jaeger.Helpers.DbConvert.ConvertString(item["Cancela"]),
            //        Autoriza = Jaeger.Helpers.DbConvert.ConvertString(item["Autoriza"])
            //    };
            //    response.Add(nuevo);
            //}
            //return response;
        }

        /// <summary>
        /// obtener lista de comprobantes fiscales relacionados (Recibo Electronico de Pago)
        /// </summary>
        public List<ComplementoPagoDoctoRelacionado> Rep(string folioFiscal) {
            var sqlCommand = new MySqlCommand() {
                CommandText = "select _cfdi_folio, _cfdi_serie, _cfdi_uuid, _cfdi_fecems, _cfdi_pagos, (_cfdi_total - (_cfdi_cbrd + _cfdi_dscntc)) as _cfdi_saldo from _cfdi where _cfdi_pagos like '%buscar%'"
            };
            sqlCommand.CommandText = sqlCommand.CommandText.Replace("buscar", folioFiscal);
            DataTable tabla = this.ExecuteReader(sqlCommand);
            List<ComplementoPagoDoctoRelacionado> lista = new List<ComplementoPagoDoctoRelacionado>();
            foreach (DataRow fila in tabla.Rows) {
                ComplementoPagos objeto = ComplementoPagos.Json(Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_pagos"]));
                if (objeto != null) {
                    if (objeto.Pago != null) {
                        foreach (ComplementoPagosPago pagosPago in objeto.Pago) {
                            List<ComplementoPagoDoctoRelacionado> doctos = pagosPago.DoctoRelacionado.Where(p => p.IdDocumento == folioFiscal).ToList();
                            if (doctos != null) {
                                foreach (ComplementoPagoDoctoRelacionado docto in doctos) {
                                    // aqui solo cambio la informacion del folio y la serie del REP
                                    docto.Folio = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_folio"]);
                                    docto.Serie = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_serie"]);
                                    docto.IdDocumento = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_uuid"]);
                                    docto.FechaEmision = Jaeger.Helpers.DbConvert.ConvertDateTime(fila["_cfdi_fecems"]);
                                    lista.Add(docto);
                                }
                            }
                        }
                    }
                }
            }
            return lista;
        }

        /// <summary>
        /// crear comprobante de Pagos
        /// </summary>
        /// <param name="lista">lista de objetos ViewModelComprobantes</param>
        /// <returns></returns>
        public ViewModelComprobante CrearReciboElectronicoPago(List<ViewModelComprobanteSingleC> lista) {
            ComplementoPagosPago pago = new ComplementoPagosPago();
            pago.DoctoRelacionado = new BindingList<ComplementoPagoDoctoRelacionado>();
            pago.AutoSuma = false;
            ViewModelComprobante cfdiV33 = new ViewModelComprobante();
            cfdiV33.TipoComprobante = EnumCfdiType.Pagos;
            cfdiV33.SubTipo = EnumCfdiSubType.Emitido;
            cfdiV33.Default();
            cfdiV33.Status = "Pendiente";
            cfdiV33.Receptor.ClaveUsoCFDI = "P01";
            cfdiV33.ComplementoPagos = new ComplementoPagos();
            cfdiV33.ComplementoPagos.Pago = new BindingList<ComplementoPagosPago>();
            cfdiV33.Complementos = new Complementos();
            cfdiV33.Complementos.Objeto = new BindingList<Complemento>();
            /// obtener lista de comprobantes
            List<ViewModelComprobanteSingle> documentos = new List<ViewModelComprobanteSingle>(this.GetListIn(lista.Select(x => x.IdDocumento).ToArray()));
            foreach (ViewModelComprobanteSingle documento in documentos) {
                ComplementoPagoDoctoRelacionado d = new ComplementoPagoDoctoRelacionado {
                    AutoCalcular = true,
                    TipoCambio = documento.TipoCambio,
                    IdDocumento = documento.IdDocumento,
                    Serie = documento.Serie,
                    Folio = documento.Folio,
                    FechaEmision = documento.FechaEmision,
                    MetodoPago = documento.MetodoPago,
                    Nombre = documento.Receptor,
                    RFC = documento.ReceptorRFC,
                    Moneda = documento.Moneda,
                    ImpPagado = documento.SaldoPagos,
                    ImpSaldoAnt = documento.SaldoPagos,
                    ImpSaldoInsoluto = 0,
                    NumParcialidad = documento.NumParcialidad + 1,
                    Estado = EnumEdoPagoDoctoRel.Relacionado
                };

                pago.DoctoRelacionado.Add(d);
                cfdiV33.Receptor.RFC = documento.ReceptorRFC;
                cfdiV33.Receptor.Nombre = documento.Receptor;
            }
            pago.Monto = pago.DoctoRelacionado.Sum(x => x.ImpPagado);
            pago.FechaPago = DateTime.Now;
            pago.MonedaP = "MXN";
            pago.FormaDePagoP = new ComplementoPagoFormaPago { Clave = "03" };
            cfdiV33.ComplementoPagos.Pago.Add(pago);
            cfdiV33.Complementos.Objeto.Add(new Complemento {
                Nombre = EnumCfdiComplementos.Pagos10,
                Data = pago.Json()
            });

            return cfdiV33;
        }

        #endregion

        public ViewModelAccuseCancelacion GetAccuse(string uuid) {
            var sqlCommand = new MySqlCommand() {
                CommandText = "select _cfdi_folio,_cfdi_serie,_cfdi_nome,_cfdi_rfce,_cfdi_nomr,_cfdi_rfcr,_cfdi_efecto,_cfdi_fecems,_cfdi_nocert,_cfdi_acuse from _cfdi where _cfdi_uuid = @uuid;"
            };
            sqlCommand.Parameters.AddWithValue("@uuid", uuid);
            DataTable data = this.ExecuteReader(sqlCommand);
            DataNamesMapper<ViewModelAccuseCancelacion> mapper = new DataNamesMapper<ViewModelAccuseCancelacion>();
            return mapper.Map(data).FirstOrDefault();
        }

        /// <summary>
        /// obtener nuevo folio del comprobante emitido a partir del rfc del emisor y el nombre de la serie
        /// </summary>
        public string GetFolio(string rfc, string serie) {
            MySqlCommand sqlCommand = new MySqlCommand() {
                CommandText = "select (Max(cast(_cfdi._cfdi_folio as UNSIGNED)) + 1) as folio from _cfdi where _cfdi._cfdi_rfce = @rfc and _cfdi._cfdi_serie = @serie;"
            };

            sqlCommand.Parameters.AddWithValue("@serie", serie);
            sqlCommand.Parameters.AddWithValue("@rfc", rfc);
            long folio = this.ExecuteScalar(sqlCommand);
            if (folio == 0) {
                folio = 1;
            }
            return folio.ToString();
        }

        #region reportes

        public EstadoDeCuenta ReporteEstadoCuenta(string rfc, EnumCfdiSubType subtipo, string status, EnumMonthsOfYear mes = EnumMonthsOfYear.None, int anio = 0, EnumCfdiDates fechas = EnumCfdiDates.FechaEmision) {
            var sqlCommand = new MySqlCommand() {
                CommandText = string.Concat("SELECT _cfdi._cfdi_efecto, _cfdi._cfdi_fecems,_cfdi_fecupc,_cfdi_fecvnc, _cfdi._cfdi_status, _cfdi._cfdi_rfce, _cfdi._cfdi_nome, _cfdi._cfdi_rfcr, _cfdi._cfdi_nomr, _cfdi._cfdi_folio, _cfdi._cfdi_serie, _cfdi._cfdi_sbttl, _cfdi._cfdi_dec, _cfdi._cfdi_total, _cfdi._cfdi_cbrd, (_cfdi_total - (_cfdi_cbrd + _cfdi_dscntc)) as _cfdi_saldo, _cfdi._cfdi_cbrdp ",
                                                "FROM _cfdi ",
                                                "WHERE _cfdi_rfce = @rfc AND MONTH(_cfdi._cfdi_fecems)=@mes AND YEAR(_cfdi_fecems)=@anio AND _cfdi_a=1 ",
                                                "ORDER BY _cfdi._cfdi_nomr;")
            };

            if (subtipo == EnumCfdiSubType.Emitido)
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("_cfdi_rfce", "_cfdi_rfcr");

            if (mes == EnumMonthsOfYear.None)
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("AND MONTH(_cfdi._cfdi_fecems)=@mes", "");

            sqlCommand.Parameters.AddWithValue("@rfc", rfc);
            sqlCommand.Parameters.AddWithValue("@mes", (int)mes);
            sqlCommand.Parameters.AddWithValue("@anio", (int)anio);

            DataTable tabla = this.ExecuteReader(sqlCommand);

            if (tabla != null) {
                if (tabla.Rows.Count > 0) {
                    EstadoDeCuenta reporte = new EstadoDeCuenta();
                    reporte.RFC = rfc;
                    reporte.Items = new BindingList<EstadoDeCuentaComprobantes>();
                    reporte.Periodo = Enum.GetName(typeof(EnumMonthsOfYear), mes);
                    foreach (DataRow fila in tabla.Rows) {
                        EstadoDeCuentaComprobantes nuevo = new EstadoDeCuentaComprobantes();
                        nuevo.Acumulado = Jaeger.Helpers.DbConvert.ConvertDecimal(fila["_cfdi_cbrd"]);
                        nuevo.Descuento = Jaeger.Helpers.DbConvert.ConvertDecimal(fila["_cfdi_dec"]);
                        nuevo.Saldo = Jaeger.Helpers.DbConvert.ConvertDecimal(fila["_cfdi_saldo"]);
                        nuevo.FechaEmision = Jaeger.Helpers.DbConvert.ConvertDateTime(fila["_cfdi_fecems"]);
                        nuevo.Descuento = Jaeger.Helpers.DbConvert.ConvertDecimal(fila["_cfdi_dec"]);
                        nuevo.FechaUltCobro = Jaeger.Helpers.DbConvert.ConvertDateTime(fila["_cfdi_fecupc"]);
                        nuevo.FechaVence = Jaeger.Helpers.DbConvert.ConvertDateTime(fila["_cfdi_fecvnc"]);
                        nuevo.Folio = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_folio"]);
                        nuevo.Serie = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_serie"]);
                        nuevo.Status = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_status"]);
                        nuevo.SubTotal = Jaeger.Helpers.DbConvert.ConvertDecimal(fila["_cfdi_sbttl"]);
                        nuevo.TipoComprobanteText = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_efecto"]);
                        nuevo.Total = Jaeger.Helpers.DbConvert.ConvertDecimal(fila["_cfdi_total"]);
                        reporte.Items.Add(nuevo);
                    }
                    return reporte;
                }
            }
            return null;
        }

        #endregion

        public List<ViewModelEstadoCuenta> Prueba(EnumCfdiSubType subtipo, string rfc, int anio, EnumMonthsOfYear mes) {
            var sqlCommand = new MySqlCommand() {
                CommandText = "SELECT  _cfdi_efecto AS Tipo , _cfdi_status AS Status , _cfdi_nomr AS Receptor , _cfdi_rfcr AS RFC , _cfdi_uuid AS IdDocumento , _cfdi_dscnt AS Descuento , _cfdi_cbrd AS Acumulado , _cfdi_total AS Total , _cfdi_fecems AS FechaEmision , _cfdi_fecupc AS FechaCobro  FROM _cfdi  WHERE ( _cfdi_rfcr = @ReceptorRFC0 )  AND (Year(_cfdi_fecems) = @Year2 ) "
            };
            List<ViewModelEstadoCuenta> lista = new List<ViewModelEstadoCuenta>();
            DataTable table = this.ExecuteReader(sqlCommand);

            foreach (DataRow item in table.Rows) {
                var nuevo = new ViewModelEstadoCuenta() {
                    Tipo = Jaeger.Helpers.DbConvert.ConvertString(item["TipoComprobanteText"]),
                    Status = Jaeger.Helpers.DbConvert.ConvertString(item["StatusText"]),
                    Receptor = Jaeger.Helpers.DbConvert.ConvertString(item["ReceptorNombre"]),
                    RFC = Jaeger.Helpers.DbConvert.ConvertString(item["ReceptorRFC"]),
                    IdDocumento = Jaeger.Helpers.DbConvert.ConvertString(item["UUID"]),
                    Descuento = Jaeger.Helpers.DbConvert.ConvertDecimal(item["Descuento"]),
                    Acumulado = Jaeger.Helpers.DbConvert.ConvertDecimal(item["Acumulado"]),
                    Total = Jaeger.Helpers.DbConvert.ConvertDecimal(item["Total"]),
                    FechaEmision = Jaeger.Helpers.DbConvert.ConvertDateTime(item["FechaEmision"]),
                    FechaCobro = Jaeger.Helpers.DbConvert.ConvertDateTime(item["FechaUltPago"])
                };
                lista.Add(nuevo);
            }
            return lista;
        }

        /// <summary>
        /// obtener resumen de comprobantes fiscales para tabla dinamica
        /// </summary>
        /// <param name="subtipo">sub tipo de comprobante Emitido, Recibido, Nomina</param>
        /// <param name="anio">año</param>
        /// <param name="mes">mes</param>
        /// <returns></returns>
        public List<ViewModelEstadoCuenta> Resumen(EnumCfdiSubType subtipo, int anio, EnumMonthsOfYear mes) {
            var sqlCommand = new MySqlCommand() {
                CommandText = ""
            };
            List<ViewModelEstadoCuenta> response = new List<ViewModelEstadoCuenta>();
            //throw new NotImplementedException();
            return response;
        }

        public DataTable Prueba3(int ejercicio, EnumCfdiSubType subtipo, string estado, string rfc = "") {
            throw new NotImplementedException();
        }

        public EstadoCuentaComprobantes Reporte(string rfc, EnumCfdiSubType objType, string status, EnumMonthsOfYear mes = 0, int anio = 0, EnumCfdiDates fechas = EnumCfdiDates.FechaEmision) {
            MySqlCommand sqlCommand = new MySqlCommand() {
                CommandText = string.Concat("select ",
                    "a._cfdi_efecto efecto1, a._cfdi_status status1, a._cfdi_folio folio1, a._cfdi_serie serie1, a._cfdi_rfce rfce1, a._cfdi_nome nome1, a._cfdi_rfcr rfcr1, a._cfdi_nomr nomr1, a._cfdi_uuid uuid1, a._cfdi_fecems fecems1, a._cfdi_feccert feccert1, a._cfdi_feccnc feccnc1, a._cfdi_fecval fecval1, a._cfdi_mtdpg mtdpg1, a._cfdi_frmpg frmpg1, a._cfdi_total total1, a._cfdi_cbrd cbrd1, a._cfdi_cbrdp cbrdp1, a._cfdi_par par1, a._cfdi_estado estado1, a._cfdi_pagos pagos1,",
                    "b._cfdi_efecto efecto2, b._cfdi_status status2, b._cfdi_folio folio2, b._cfdi_serie serie2, b._cfdi_rfce rfce2, b._cfdi_nome nome2, b._cfdi_rfcr rfcr2, b._cfdi_nomr nomr2, b._cfdi_uuid uuid2, b._cfdi_fecems fecems2, b._cfdi_feccert feccert2, b._cfdi_feccnc feccnc2, b._cfdi_fecval fecval2, b._cfdi_mtdpg mtdpg2, b._cfdi_frmpg frmpg2, b._cfdi_total total2, b._cfdi_cbrd cbrd2, b._cfdi_cbrdp cbrdp2, b._cfdi_par par2, b._cfdi_estado estado2, b._cfdi_pagos pagos2 ",
                    "from _cfdi a ",
                    "left join _cfdi b on b._cfdi_pagos like concat('%',a._cfdi_uuid,'%') ",
                    "where year(a._cfdi_fecems)=@anio and month(a._cfdi_fecems)=@mes and a._cfdi_frmpg='99' and a._cfdi_mtdpg='PPD' and a._cfdi_a=1 and a._cfdi_rfcr=@rfc")
            };

            sqlCommand.Parameters.AddWithValue("@anio", anio);
            sqlCommand.Parameters.AddWithValue("@mes", mes);
            sqlCommand.Parameters.AddWithValue("@doc", objType);
            sqlCommand.Parameters.AddWithValue("@rfc", rfc);

            var tabla = this.Db.Ado.GetDataTable(sqlCommand.CommandText, new SqlSugar.SugarParameter[] { new SqlSugar.SugarParameter("@anio", anio), new SqlSugar.SugarParameter("@mes", mes), new SqlSugar.SugarParameter("@doc", objType), new SqlSugar.SugarParameter("@rfc", rfc) });
            var nuevo = new EstadoCuentaComprobantes();

            foreach (DataRow fila in tabla.Rows) {
                var newItem = new ComprobanteNormal();
                newItem.Acumulado = DbConvert.ConvertDecimal(fila["cbrd1"]);
                newItem.ClaveCondicionPago = "";
                newItem.ClaveFormaPago = DbConvert.ConvertString(fila["frmpg1"]);
                newItem.ClaveMetodoPago = DbConvert.ConvertString(fila["mtdpg1"]);
                newItem.ClaveVendedor = "";
                newItem.Creo = "";
                //newItem.Descuento = DbConvert.ConvertDecimal(fila["a._cfdi_dscnt"]);
                newItem.Emisor = DbConvert.ConvertString(fila["nome1"]);
                newItem.EmisorClave = "";
                newItem.EmisorRFC = DbConvert.ConvertString(fila["rfce1"]);
                newItem.Estado = DbConvert.ConvertString(fila["estado1"]);
                newItem.FechaTimbrado = DbConvert.ConvertDateTime(fila["feccert1"]);
                newItem.FechaEmision = DbConvert.ConvertDateTime(fila["fecems1"]);
                newItem.FechaMod = null;
                //newItem.FechaPago = DbConvert.ConvertDateTime(fila["a._cfdi_fecupc"]);
                newItem.Folio = DbConvert.ConvertString(fila["folio1"]);
                newItem.Id = 1;
                newItem.IsActive = true;
                newItem.Modifica = "";
                newItem.ClaveMoneda = "";
                newItem.NumParcialidad = DbConvert.ConvertInt16(fila["par1"]);
                //newItem.PrecisionDecimal = DbConvert.ConvertInt32(fila["a._cfdi_prec"]);
                newItem.Receptor = DbConvert.ConvertString(fila["nomr1"]);
                newItem.ReceptorClave = "";
                newItem.ReceptorRFC = DbConvert.ConvertString(fila["rfcr1"]);
                //newItem.Saldo = DbConvert.ConvertDecimal(fila["saldo"]);
                newItem.Serie = DbConvert.ConvertString(fila["serie1"]);
                newItem.Status = DbConvert.ConvertString(fila["status1"]);
                newItem.SubId = 0;
                //newItem.SubTotal = DbConvert.ConvertDecimal(fila["a._cfdi_sbttl"]);
                newItem.TipoComprobanteText = DbConvert.ConvertString(fila["efecto1"]);
                newItem.TipoCambio = 0;
                newItem.Total = DbConvert.ConvertDecimal(fila["total1"]);
                /*newItem.RetencionIEPS = DbConvert.ConvertDecimal(fila["a._cfdi_retieps"]);
                newItem.TrasladoIEPS = DbConvert.ConvertDecimal(fila["a._cfdi_trsieps"]);
                newItem.RetencionISR = DbConvert.ConvertDecimal(fila["a._cfdi_retisr"]);
                newItem.RetencionIVA = DbConvert.ConvertDecimal(fila["a._cfdi_retiva"]);
                newItem.TrasladoIVA = DbConvert.ConvertDecimal(fila["a._cfdi_trsiva"]);
                newItem.ClaveUsoCFDI = DbConvert.ConvertString(fila["a._cfdi_usocfdi"]);*/
                newItem.UUID = DbConvert.ConvertString(fila["uuid1"]);
                //newItem.Version = DbConvert.ConvertString(fila["a._cfdi_ver"]);
                //newItem.KeyName = DbConvert.ConvertString(fila["keyname"]);
                //newItem.UrlPdf = DbConvert.ConvertString(fila["a._cfdi_url_pdf"]);
                //newItem.UrlXml = DbConvert.ConvertString(fila["a._cfdi_url_xml"]);
                //newItem.Accuse = DbConvert.ConvertString(fila["a._cfdi_url_xmlacu"]);

                if (DbConvert.ConvertString(fila["uuid2"]) != "") {
                    var newItem2 = new ComprobanteNormal();
                    newItem2.Acumulado = DbConvert.ConvertDecimal(fila["cbrd2"]);
                    newItem2.ClaveCondicionPago = "";
                    newItem2.ClaveFormaPago = DbConvert.ConvertString(fila["frmpg2"]);
                    newItem2.ClaveMetodoPago = DbConvert.ConvertString(fila["mtdpg2"]);
                    newItem2.ClaveVendedor = "";
                    newItem2.Creo = "";
                    //newItem2.Descuento = DbConvert.ConvertDecimal(fila["a._cfdi_dscnt"]);
                    newItem2.Emisor = DbConvert.ConvertString(fila["nome2"]);
                    newItem2.EmisorClave = "";
                    newItem2.EmisorRFC = DbConvert.ConvertString(fila["rfce2"]);
                    newItem2.Estado = DbConvert.ConvertString(fila["estado2"]);
                    newItem2.FechaTimbrado = DbConvert.ConvertDateTime(fila["feccert2"]);
                    newItem2.FechaEmision = DbConvert.ConvertDateTime(fila["fecems2"]);
                    newItem2.FechaMod = null;
                    //newItem2.FechaPago = DbConvert.ConvertDateTime(fila["a._cfdi_fecupc"]);
                    newItem2.Folio = DbConvert.ConvertString(fila["folio2"]);
                    newItem2.Id = 2;
                    newItem2.IsActive = true;
                    newItem2.Modifica = "";
                    newItem2.ClaveMoneda = "";
                    newItem2.NumParcialidad = DbConvert.ConvertInt16(fila["par2"]);
                    //newItem2.PrecisionDecimal = DbConvert.ConvertInt32(fila["a._cfdi_prec"]);
                    newItem2.Receptor = DbConvert.ConvertString(fila["nomr2"]);
                    newItem2.ReceptorClave = "";
                    newItem2.ReceptorRFC = DbConvert.ConvertString(fila["rfcr2"]);
                    //newItem2.Saldo = DbConvert.ConvertDecimal(fila["saldo"]);
                    newItem2.Serie = DbConvert.ConvertString(fila["serie2"]);
                    newItem2.Status = DbConvert.ConvertString(fila["status2"]);
                    newItem2.SubId = 0;
                    //newItem2.SubTotal = DbConvert.ConvertDecimal(fila["a._cfdi_sbttl"]);
                    newItem2.TipoComprobanteText = DbConvert.ConvertString(fila["efecto2"]);
                    newItem2.TipoCambio = 0;
                    newItem2.Total = DbConvert.ConvertDecimal(fila["total2"]);
                    /*newItem2.RetencionIEPS = DbConvert.ConvertDecimal(fila["a._cfdi_retieps"]);
                    newItem2.TrasladoIEPS = DbConvert.ConvertDecimal(fila["a._cfdi_trsieps"]);
                    newItem2.RetencionISR = DbConvert.ConvertDecimal(fila["a._cfdi_retisr"]);
                    newItem2.RetencionIVA = DbConvert.ConvertDecimal(fila["a._cfdi_retiva"]);
                    newItem2.TrasladoIVA = DbConvert.ConvertDecimal(fila["a._cfdi_trsiva"]);
                    newItem2.ClaveUsoCFDI = DbConvert.ConvertString(fila["a._cfdi_usocfdi"]);*/
                    newItem2.UUID = DbConvert.ConvertString(fila["uuid2"]);
                    //newItem2.Version = DbConvert.ConvertString(fila["a._cfdi_ver"]);
                    //newItem2.KeyName = DbConvert.ConvertString(fila["keyname"]);
                    //newItem2.UrlPdf = DbConvert.ConvertString(fila["a._cfdi_url_pdf"]);
                    //newItem2.UrlXml = DbConvert.ConvertString(fila["a._cfdi_url_xml"]);
                    //newItem2.Accuse = DbConvert.ConvertString(fila["a._cfdi_url_xmlacu"]);
                    newItem.Pagos.Add(newItem2);
                }

                nuevo.Items.Add(newItem);
            }
            return nuevo;
        }


        /// <summary>
        /// actualizar objeto Complemento de pagos, con las partidas de los comprobantes relacionados
        /// </summary>
        public bool AplicarComprobantePagos(ComplementoPagos objeto) {
            if (objeto != null) {
                if (objeto.Pago != null) {
                    // recorremos la lista de objetos de pago
                    foreach (ComplementoPagosPago pago in objeto.Pago) {
                        // recorremos la lista de documentos relacionados
                        if (pago.DoctoRelacionado != null) {
                            foreach (ComplementoPagoDoctoRelacionado docto in pago.DoctoRelacionado) {
                                List<ComplementoPagoDoctoRelacionado> tabla = this.Rep(docto.IdDocumento);
                                MySqlCommand sqlUpdate = new MySqlCommand() {
                                    CommandText = "update _cfdi set _cfdi_par=@cfdi_par,_cfdi_cbrdp=(@cfdi_cbrdp),_cfdi_fecpgr=@cfdi_fecpgr where _cfdi_uuid=@index;"
                                };
                                decimal suma = docto.ImpPagado;
                                if (tabla != null) {
                                    if (tabla.Count > 0) {
                                        suma = tabla.Where(p => Jaeger.Helpers.HelperValidacion.UUID(p.IdDocumento)).Sum(p => p.ImpPagado);
                                    }
                                }
                                sqlUpdate.Parameters.AddWithValue("@cfdi_cbrdp", suma);
                                sqlUpdate.Parameters.AddWithValue("@cfdi_par", docto.NumParcialidad);
                                sqlUpdate.Parameters.AddWithValue("@cfdi_fecpgr", pago.FechaPago);
                                sqlUpdate.Parameters.AddWithValue("@index", docto.IdDocumento);
                                this.ExecuteTransaction(sqlUpdate);
                            }
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// aplicar reglas a los cfdi relacionados del comprobante fiscal
        /// </summary>
        public bool AplicarCFDIRelacionados(ComprobanteCfdiRelacionados objeto) {
            if (objeto != null) {
                // para la clave de notas de credito relacionadas
                if (objeto.TipoRelacion.Clave == "01" | objeto.TipoRelacion.Clave == "02" | objeto.TipoRelacion.Clave == "03") {
                    foreach (ComprobanteCfdiRelacionadosCfdiRelacionado item in objeto.CfdiRelacionado) {
                        decimal suma = 0;
                        List<ComplementoDoctoRelacionado> notas = this.Notas(item.IdDocumento);
                        if (notas != null) {
                            if (notas.Count > 0) {
                                suma = notas.Where(p => Jaeger.Helpers.HelperValidacion.UUID(p.IdDocumento)).Sum(p => p.Total);
                            }
                        }
                        //Entities.Base.ComprobanteGeneral d = this.GetComprobanteGeneral(item.IdDocumento);
                        MySqlCommand sqlCommand = new MySqlCommand() {
                            CommandText = "update _cfdi set _cfdi_dscntc=@descuento where _cfdi_uuid=@index"
                        };
                        sqlCommand.Parameters.AddWithValue("@index", item.IdDocumento);
                        sqlCommand.Parameters.AddWithValue("@descuento", suma);
                        return this.ExecuteTransaction(sqlCommand) > 0;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// obtener notas de credito relacionados al comprobante fiscal
        /// </summary>
        public List<ComplementoDoctoRelacionado> Notas(string folioFiscal) {
            if (Jaeger.Helpers.HelperValidacion.UUID(folioFiscal)) {
                MySqlCommand sqlCommand = new MySqlCommand() {
                    CommandText = string.Concat("select _cfdi_doc_id, _cfdi_estado,_cfdi_folio, _cfdi_serie, _cfdi_uuid,_cfdi_rfce,_cfdi_rfcr,_cfdi_nome,_cfdi_nomr, _cfdi_fecems, _cfdi_comrel, _cfdi_total, _cfdi_moneda,_cfdi_frmpg,_cfdi_mtdpg,_cfdi_par,_cfdi_estado,(_cfdi_total - (_cfdi_cbrd + _cfdi_dscntc)) as _cfdi_saldo ",
                        "from _cfdi where _cfdi_comrel like '%buscar%'")
                };
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("buscar", folioFiscal);
                DataTable tabla = this.ExecuteReader(sqlCommand);
                List<Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoDoctoRelacionado> lista = new List<Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoDoctoRelacionado>();
                foreach (DataRow fila in tabla.Rows) {
                    ComprobanteCfdiRelacionados t = ComprobanteCfdiRelacionados.Json(Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_comrel"]));
                    if (t != null) {
                        Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoDoctoRelacionado docto = new Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoDoctoRelacionado();
                        docto.TipoRelacion.Clave = t.TipoRelacion.Clave;
                        docto.TipoRelacion.Descripcion = t.TipoRelacion.Descripcion;
                        docto.SubTipoText = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_doc_id"]);
                        docto.Folio = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_folio"]);
                        docto.Serie = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_serie"]);
                        docto.IdDocumento = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_uuid"]);
                        docto.FechaEmision = Jaeger.Helpers.DbConvert.ConvertDateTime(fila["_cfdi_fecems"]);
                        docto.Total = Jaeger.Helpers.DbConvert.ConvertDecimal(fila["_cfdi_total"]);
                        docto.EstadoSAT = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_estado"]);
                        docto.Moneda = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_moneda"]);
                        docto.FormaPago = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_frmpg"]);
                        docto.MetodoPago = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_mtdpg"]);
                        docto.EstadoSAT = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_estado"]);
                        if (docto.SubTipo == EnumCfdiSubType.Emitido) {
                            docto.RFC = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_rfcr"]);
                            docto.Nombre = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_nomr"]);
                        } else if (docto.SubTipo == EnumCfdiSubType.Recibido) {
                            docto.RFC = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_rfce"]);
                            docto.Nombre = Jaeger.Helpers.DbConvert.ConvertString(fila["_cfdi_nome"]);
                        }
                        lista.Add(docto);
                    }
                }
                return lista;
            }
            return null;
        }

        public List<ViewModelBackupComprobante> GetBackupFiles(EnumCfdiSubType subTipo, int anio, int mes = 0, EnumCfdiDates porFecha = EnumCfdiDates.FechaTimbre) {
            throw new NotImplementedException();
        }

        public List<ViewModelBackup> GetInfoBackUp(EnumCfdiSubType objType, EnumCfdiDates porFecha) {
            throw new NotImplementedException();
        }


        #region funciones basicas que ya realiza el context de SqlSugar

        // estas son solo funciones pertenecientes a la misma clase, funciones adicionales 

        /// <summary>
        /// este procedimiento no esta calculando el folio del comprobante emitido
        /// </summary>
        /// <param name="objeto"></param>
        /// <returns></returns>
        public ViewModelComprobante Insert(ViewModelComprobante objeto) {
            MySqlCommand sqlInsert = new MySqlCommand() {
                CommandText = string.Concat("insert into _cfdi (_cfdi_a, _cfdi_doc_id, _cfdi_drctr_id, _cfdi_vence, _cfdi_sbttl, _cfdi_dscnt, _cfdi_trsiva, _cfdi_trsieps, _cfdi_retieps, _cfdi_retiva, _cfdi_retisr, _cfdi_total, _cfdi_cbrd, _cfdi_per, _cfdi_dec, _cfdi_status, _cfdi_usr_n, _cfdi_rfce, _cfdi_rfcr, _cfdi_pac, _cfdi_moneda, _cfdi_folio, _cfdi_cntpg, _cfdi_edita, _cfdi_nocert, _cfdi_serie, _cfdi_estado, _cfdi_rslt, _cfdi_uuid, _cfdi_doc, _cfdi_frmpg, _cfdi_desct, _cfdi_mtdpg, _cfdi_cndpg, _cfdi_nome, _cfdi_nomr, _cfdi_obsrv, _cfdi_val, _cfdi_fn, _cfdi_fecems, _cfdi_feccert, _cfdi_fecval, _cfdi_acuse, _cfdi_adenda, _cfdi_usocfdi, _cfdi_efecto, _cfdi_comrel, _cfdi_lgrexp, _cfdi_compl, _cfdi_ver,_cfdi_pagos, _cfdi_save, _cfdi_nomina) ",
                                                       "values (@cfdi_a, @cfdi_doc_id, @cfdi_drctr_id, @cfdi_vence, @cfdi_sbttl, @cfdi_dscnt, @cfdi_trsiva, @cfdi_trsieps, @cfdi_retieps, @cfdi_retiva, @cfdi_retisr, @cfdi_total, @cfdi_cbrd, @cfdi_per, @cfdi_dec, @cfdi_status, @cfdi_usr_n, @cfdi_rfce, @cfdi_rfcr, @cfdi_pac, @cfdi_moneda, @cfdi_folio, @cfdi_cntpg, @cfdi_edita, @cfdi_nocert, @cfdi_serie, @cfdi_estado, @cfdi_rslt, @cfdi_uuid, @cfdi_doc, @cfdi_frmpg, @cfdi_desct, @cfdi_mtdpg, @cfdi_cndpg, @cfdi_nome, @cfdi_nomr, @cfdi_obsrv, @cfdi_val, @cfdi_fn, @cfdi_fecems, @cfdi_feccert, @cfdi_fecval, @cfdi_acuse, @cfdi_adenda, @cfdi_usocfdi, @cfdi_efecto, @cfdi_comrel, @cfdi_lgrexp, @cfdi_compl, @cfdi_ver,@cfdi_pagos, @cfdi_save, @cfdi_nomina);")
            };
            if (objeto.TimbreFiscal != null) {
                sqlInsert.CommandText = string.Concat(sqlInsert.CommandText, "select _cfdi_id from _cfdi where _cfdi_uuid = @uuid;");
                sqlInsert.Parameters.AddWithValue("@uuid", objeto.TimbreFiscal.UUID);
            } else {
                objeto.Id = (int)this.GetIndex("_cfdi_id", "_cfdi");
                sqlInsert.CommandText = sqlInsert.CommandText.Replace("(_cfdi_a", "(_cfdi_id, _cfdi_a");
                sqlInsert.CommandText = sqlInsert.CommandText.Replace("(@cfdi_a", "(@cfdi_id, @cfdi_a");
                sqlInsert.Parameters.AddWithValue("@cfdi_id", objeto.Id);
            }
            if (objeto.SubTipo == EnumCfdiSubType.Emitido | objeto.SubTipo == EnumCfdiSubType.Nomina) {
                sqlInsert.Parameters.AddWithValue("@cfdi_drctr_id", objeto.Receptor.Id);
            } else {
                sqlInsert.Parameters.AddWithValue("@cfdi_drctr_id", objeto.Emisor.Id);
            }

            sqlInsert.Parameters.AddWithValue("@cfdi_a", 1);
            sqlInsert.Parameters.AddWithValue("@cfdi_doc_id", objeto.SubTipo);
            sqlInsert.Parameters.AddWithValue("@cfdi_vence", 0);
            sqlInsert.Parameters.AddWithValue("@cfdi_sbttl", objeto.SubTotal);
            sqlInsert.Parameters.AddWithValue("@cfdi_dscnt", objeto.Descuento);
            sqlInsert.Parameters.AddWithValue("@cfdi_retisr", objeto.RetencionIsr);
            sqlInsert.Parameters.AddWithValue("@cfdi_retiva", objeto.RetencionIva);
            sqlInsert.Parameters.AddWithValue("@cfdi_trsiva", objeto.TrasladoIva);
            sqlInsert.Parameters.AddWithValue("@cfdi_retieps", objeto.RetencionIeps);
            sqlInsert.Parameters.AddWithValue("@cfdi_trsieps", objeto.TrasladoIeps);
            sqlInsert.Parameters.AddWithValue("@cfdi_total", objeto.Total);
            sqlInsert.Parameters.AddWithValue("@cfdi_cbrd", objeto.Acumulado);
            sqlInsert.Parameters.AddWithValue("@cfdi_per", objeto.TotalPecepcion);
            sqlInsert.Parameters.AddWithValue("@cfdi_dec", objeto.TotalDeduccion);
            sqlInsert.Parameters.AddWithValue("@cfdi_status", objeto.StatusText);
            sqlInsert.Parameters.AddWithValue("@cfdi_efecto", objeto.TipoComprobanteText);
            sqlInsert.Parameters.AddWithValue("@cfdi_ver", objeto.Version);
            sqlInsert.Parameters.AddWithValue("@cfdi_usr_n", objeto.Creo);
            sqlInsert.Parameters.AddWithValue("@cfdi_rfce", objeto.Emisor.RFC);
            sqlInsert.Parameters.AddWithValue("@cfdi_rfcr", objeto.Receptor.RFC);
            sqlInsert.Parameters.AddWithValue("@cfdi_moneda", objeto.Moneda.Clave);
            sqlInsert.Parameters.AddWithValue("@cfdi_cntpg", objeto.CtaPago);
            sqlInsert.Parameters.AddWithValue("@cfdi_edita", "");
            sqlInsert.Parameters.AddWithValue("@cfdi_nocert", objeto.NoCertificado);
            sqlInsert.Parameters.AddWithValue("@cfdi_serie", objeto.Serie);
            sqlInsert.Parameters.AddWithValue("@cfdi_rslt", objeto.Result);
            sqlInsert.Parameters.AddWithValue("@cfdi_doc", "");
            sqlInsert.Parameters.AddWithValue("@cfdi_frmpg", objeto.FormaPago.Clave);
            sqlInsert.Parameters.AddWithValue("@cfdi_desct", objeto.MotivoDescuento);
            sqlInsert.Parameters.AddWithValue("@cfdi_mtdpg", objeto.MetodoPago.Clave);
            sqlInsert.Parameters.AddWithValue("@cfdi_cndpg", objeto.CondicionPago);
            sqlInsert.Parameters.AddWithValue("@cfdi_nome", objeto.Emisor.Nombre);
            sqlInsert.Parameters.AddWithValue("@cfdi_nomr", objeto.Receptor.Nombre);
            sqlInsert.Parameters.AddWithValue("@cfdi_obsrv", objeto.Notas);
            sqlInsert.Parameters.AddWithValue("@cfdi_fn", DateTime.Now);
            sqlInsert.Parameters.AddWithValue("@cfdi_fecems", objeto.FechaEmision);
            sqlInsert.Parameters.AddWithValue("@cfdi_usocfdi", objeto.UsoCfdi.Clave);
            sqlInsert.Parameters.AddWithValue("@cfdi_lgrexp", objeto.LugarExpedicion);
            sqlInsert.Parameters.AddWithValue("@cfdi_save", objeto.Xml);

            // calcular el folio en dado caso que sea un comprobante emitido, que no se encuentre timbrado
            //if (objeto.SubTipo == EnumCfdiSubType.Emitido)
            //    if (objeto.TimbreFiscal == null)
            //        objeto.Folio = this.GetFolio(objeto.Emisor.RFC, objeto.Serie).ToString();

            sqlInsert.Parameters.AddWithValue("@cfdi_folio", objeto.Folio);

            // cfdi's relacionados
            if (objeto.CfdiRelacionados == null)
                sqlInsert.Parameters.AddWithValue("@cfdi_comrel", DBNull.Value);
            else
                sqlInsert.Parameters.AddWithValue("@cfdi_comrel", objeto.CfdiRelacionados.Json());

            // informacion del timbre fiscal
            if (objeto.TimbreFiscal == null) {
                sqlInsert.Parameters.AddWithValue("@cfdi_uuid", DBNull.Value);
                sqlInsert.Parameters.AddWithValue("@cfdi_feccert", DBNull.Value);
                sqlInsert.Parameters.AddWithValue("@cfdi_pac", DBNull.Value);
            } else {
                sqlInsert.Parameters.AddWithValue("@cfdi_uuid", objeto.TimbreFiscal.UUID.ToString().ToUpper());
                sqlInsert.Parameters.AddWithValue("@cfdi_feccert", objeto.FechaCert);
                sqlInsert.Parameters.AddWithValue("@cfdi_pac", objeto.TimbreFiscal.RFCProvCertif);
            }

            // objeto de validacion del comprobante
            if (objeto.Validacion != null) {
                sqlInsert.Parameters.AddWithValue("@cfdi_val", objeto.Validacion.Json(Formatting.None));
                sqlInsert.Parameters.AddWithValue("@cfdi_fecval", objeto.FechaVal);
                sqlInsert.Parameters.AddWithValue("@cfdi_estado", objeto.Estado);
            } else {
                sqlInsert.Parameters.AddWithValue("@cfdi_val", DBNull.Value);
                sqlInsert.Parameters.AddWithValue("@cfdi_fecval", DBNull.Value);
                sqlInsert.Parameters.AddWithValue("@cfdi_estado", DBNull.Value);
            }

            // informacion del acuse de cancelacion
            if (objeto.Accuse != null)
                sqlInsert.Parameters.AddWithValue("@cfdi_acuse", objeto.Accuse.Xml());
            else
                sqlInsert.Parameters.AddWithValue("@cfdi_acuse", DBNull.Value);

            // informacion de addendas
            if (objeto.Addendas != null)
                sqlInsert.Parameters.AddWithValue("@cfdi_adenda", objeto.Addendas.Json(Formatting.None));
            else
                sqlInsert.Parameters.AddWithValue("@cfdi_adenda", DBNull.Value);

            // informacion de addendas
            if (objeto.Complementos != null)
                sqlInsert.Parameters.AddWithValue("@cfdi_compl", objeto.Complementos.Json(Formatting.None));
            else
                sqlInsert.Parameters.AddWithValue("@cfdi_compl", DBNull.Value);

            // complemento de pagos
            if (objeto.ComplementoPagos != null)
                sqlInsert.Parameters.AddWithValue("@cfdi_pagos", objeto.ComplementoPagos.Json());
            else
                sqlInsert.Parameters.AddWithValue("@cfdi_pagos", DBNull.Value);

            // complemento de nomina
            if (objeto.Nomina != null)
                sqlInsert.Parameters.AddWithValue("@cfdi_nomina", objeto.JNomina);
            else
                sqlInsert.Parameters.AddWithValue("@cfdi_nomina", DBNull.Value);

            // ejecucion de la consulta
            if (objeto.TimbreFiscal != null)
                objeto.Id = (int)this.ExecuteScalar(sqlInsert);
            //else if (!this.Execute(sqlInsert))
            //    objeto.Id = 0;

            return objeto;
        }

        public ViewModelComprobante Update(ViewModelComprobante objeto) {
            MySqlCommand sqlUpdate = new MySqlCommand() {
                CommandText = String.Concat("update _cfdi set ",
                                             "_cfdi_a=@cfdi_a, _cfdi_doc_id=@cfdi_doc_id, _cfdi_vence=@cfdi_vence,_cfdi_sbttl=@cfdi_sbttl,_cfdi_dscnt=@cfdi_dscnt,_cfdi_retieps=@cfdi_retieps,_cfdi_trsiva=@cfdi_trsiva,_cfdi_trsieps=@cfdi_trsieps,_cfdi_retiva=@cfdi_retiva,_cfdi_retisr=@cfdi_retisr,_cfdi_total=@cfdi_total,_cfdi_cbrd=@cfdi_cbrd,_cfdi_per=@cfdi_per,_cfdi_dec=@cfdi_dec,_cfdi_status=@cfdi_status,_cfdi_efecto=@cfdi_efecto,_cfdi_usr_m=@cfdi_usr_m,_cfdi_rfce=@cfdi_rfce,_cfdi_rfcr=@cfdi_rfcr,_cfdi_pac=@cfdi_pac,_cfdi_moneda=@cfdi_moneda,_cfdi_folio=@cfdi_folio,_cfdi_cntpg=@cfdi_cntpg,_cfdi_edita=@cfdi_edita,_cfdi_nocert=@cfdi_nocert,_cfdi_serie=@cfdi_serie,_cfdi_estado=@cfdi_estado,_cfdi_rslt=@cfdi_rslt,_cfdi_uuid=@cfdi_uuid,_cfdi_doc=@cfdi_doc,_cfdi_frmpg=@cfdi_frmpg,_cfdi_desct=@cfdi_desct,_cfdi_mtdpg=@cfdi_mtdpg,_cfdi_cndpg=@cfdi_cndpg,_cfdi_nome=@cfdi_nome,_cfdi_nomr=@cfdi_nomr,_cfdi_obsrv=@cfdi_obsrv,_cfdi_val=@cfdi_val,_cfdi_fm=@cfdi_fm,_cfdi_fecems=@cfdi_fecems,_cfdi_feccert=@cfdi_feccert,_cfdi_fecval=@cfdi_fecval,_cfdi_acuse=@cfdi_acuse,_cfdi_adenda=@cfdi_adenda,_cfdi_usocfdi=@cfdi_usocfdi, _cfdi_comrel=@cfdi_comrel, _cfdi_lgrexp=@cfdi_lgrexp, _cfdi_compl=@cfdi_compl, _cfdi_ver=@cfdi_ver, _cfdi_url_xml=@cfdi_url_xml, _cfdi_url_pdf=@cfdi_url_pdf, ",
                                             "_cfdi_pagos=@cfdi_pagos ",
                                             "where _cfdi_id = @index")
            };

            if (objeto.TimbreFiscal == null) {
                sqlUpdate.CommandText = sqlUpdate.CommandText.Replace("_cfdi_uuid=@cfdi_uuid,", "");
            }

            sqlUpdate.Parameters.AddWithValue("@index", objeto.Id);
            sqlUpdate.Parameters.AddWithValue("@cfdi_a", 1);
            sqlUpdate.Parameters.AddWithValue("@cfdi_doc_id", objeto.SubTipo);
            sqlUpdate.Parameters.AddWithValue("@cfdi_vence", 0);
            sqlUpdate.Parameters.AddWithValue("@cfdi_sbttl", objeto.SubTotal);
            sqlUpdate.Parameters.AddWithValue("@cfdi_dscnt", objeto.Descuento);
            sqlUpdate.Parameters.AddWithValue("@cfdi_retisr", objeto.RetencionIsr);
            sqlUpdate.Parameters.AddWithValue("@cfdi_retiva", objeto.RetencionIva);
            sqlUpdate.Parameters.AddWithValue("@cfdi_trsiva", objeto.TrasladoIva);
            sqlUpdate.Parameters.AddWithValue("@cfdi_retieps", objeto.RetencionIeps);
            sqlUpdate.Parameters.AddWithValue("@cfdi_trsieps", objeto.TrasladoIeps);
            sqlUpdate.Parameters.AddWithValue("@cfdi_total", objeto.Total);
            sqlUpdate.Parameters.AddWithValue("@cfdi_cbrd", objeto.Acumulado);
            sqlUpdate.Parameters.AddWithValue("@cfdi_per", objeto.TotalPecepcion);
            sqlUpdate.Parameters.AddWithValue("@cfdi_dec", objeto.TotalDeduccion);
            sqlUpdate.Parameters.AddWithValue("@cfdi_status", objeto.StatusText);
            sqlUpdate.Parameters.AddWithValue("@cfdi_efecto", objeto.TipoComprobanteText);
            sqlUpdate.Parameters.AddWithValue("@cfdi_ver", objeto.Version);
            sqlUpdate.Parameters.AddWithValue("@cfdi_usr_m", objeto.Creo);
            sqlUpdate.Parameters.AddWithValue("@cfdi_rfce", objeto.Emisor.RFC);
            sqlUpdate.Parameters.AddWithValue("@cfdi_rfcr", objeto.Receptor.RFC);
            sqlUpdate.Parameters.AddWithValue("@cfdi_moneda", objeto.Moneda.Clave);
            sqlUpdate.Parameters.AddWithValue("@cfdi_folio", objeto.Folio);
            sqlUpdate.Parameters.AddWithValue("@cfdi_cntpg", objeto.CtaPago);
            sqlUpdate.Parameters.AddWithValue("@cfdi_edita", "");
            sqlUpdate.Parameters.AddWithValue("@cfdi_nocert", objeto.NoCertificado);
            sqlUpdate.Parameters.AddWithValue("@cfdi_serie", objeto.Serie);
            sqlUpdate.Parameters.AddWithValue("@cfdi_estado", objeto.Estado);
            sqlUpdate.Parameters.AddWithValue("@cfdi_rslt", objeto.Result);
            sqlUpdate.Parameters.AddWithValue("@cfdi_doc", "");
            sqlUpdate.Parameters.AddWithValue("@cfdi_frmpg", objeto.FormaPago.Clave);
            sqlUpdate.Parameters.AddWithValue("@cfdi_desct", objeto.MotivoDescuento);
            sqlUpdate.Parameters.AddWithValue("@cfdi_mtdpg", objeto.MetodoPago.Clave);
            sqlUpdate.Parameters.AddWithValue("@cfdi_cndpg", objeto.CondicionPago);
            sqlUpdate.Parameters.AddWithValue("@cfdi_nome", objeto.Emisor.Nombre);
            sqlUpdate.Parameters.AddWithValue("@cfdi_nomr", objeto.Receptor.Nombre);
            sqlUpdate.Parameters.AddWithValue("@cfdi_obsrv", objeto.Notas);
            sqlUpdate.Parameters.AddWithValue("@cfdi_fm", DateTime.Now);
            sqlUpdate.Parameters.AddWithValue("@cfdi_fecems", objeto.FechaEmision);
            sqlUpdate.Parameters.AddWithValue("@cfdi_fecval", objeto.FechaVal);
            sqlUpdate.Parameters.AddWithValue("@cfdi_usocfdi", objeto.UsoCfdi.Clave);
            sqlUpdate.Parameters.AddWithValue("@cfdi_lgrexp", objeto.LugarExpedicion);
            sqlUpdate.Parameters.AddWithValue("@cfdi_url_xml", objeto.FileXml);
            sqlUpdate.Parameters.AddWithValue("@cfdi_url_pdf", objeto.FilePdf);

            if (objeto.TimbreFiscal == null) {
                sqlUpdate.Parameters.AddWithValue("@cfdi_uuid", DBNull.Value);
                sqlUpdate.Parameters.AddWithValue("@cfdi_pac", DBNull.Value);
                sqlUpdate.Parameters.AddWithValue("@cfdi_feccert", DBNull.Value);
            } else {
                sqlUpdate.Parameters.AddWithValue("@cfdi_uuid", objeto.TimbreFiscal.UUID);
                sqlUpdate.Parameters.AddWithValue("@cfdi_pac", objeto.TimbreFiscal.RFCProvCertif);
                sqlUpdate.Parameters.AddWithValue("@cfdi_feccert", objeto.TimbreFiscal.FechaTimbrado);
            }

            if (objeto.CfdiRelacionados == null)
                sqlUpdate.Parameters.AddWithValue("@cfdi_comrel", DBNull.Value);
            else
                sqlUpdate.Parameters.AddWithValue("@cfdi_comrel", objeto.CfdiRelacionados.Json());

            if (!(objeto.SubTipo == EnumCfdiSubType.Emitido | objeto.SubTipo == EnumCfdiSubType.Nomina))
                sqlUpdate.Parameters.AddWithValue("@cfdi_drctr_id", objeto.Emisor.Id);
            else
                sqlUpdate.Parameters.AddWithValue("@cfdi_drctr_id", objeto.Receptor.Id);

            if (objeto.Addendas == null)
                sqlUpdate.Parameters.AddWithValue("@cfdi_adenda", DBNull.Value);
            else
                sqlUpdate.Parameters.AddWithValue("@cfdi_adenda", objeto.Addendas.Json(Formatting.None));

            if (objeto.Accuse == null)
                sqlUpdate.Parameters.AddWithValue("@cfdi_acuse", DBNull.Value);
            else
                sqlUpdate.Parameters.AddWithValue("@cfdi_acuse", objeto.Accuse.Xml());

            if (objeto.Validacion == null)
                sqlUpdate.Parameters.AddWithValue("@cfdi_val", DBNull.Value);
            else
                sqlUpdate.Parameters.AddWithValue("@cfdi_val", objeto.Validacion.Json(Formatting.None));

            if (objeto.Complementos == null)
                sqlUpdate.Parameters.AddWithValue("@cfdi_compl", DBNull.Value);
            else
                sqlUpdate.Parameters.AddWithValue("@cfdi_compl", objeto.Complementos.Json(Formatting.None));

            // complemento de pagos
            if (objeto.ComplementoPagos != null)
                sqlUpdate.Parameters.AddWithValue("@cfdi_pagos", objeto.ComplementoPagos.Json());
            else
                sqlUpdate.Parameters.AddWithValue("@cfdi_pagos", DBNull.Value);

            this.ExecuteTransaction(sqlUpdate);

            return objeto;
        }

        /// <summary>
        /// insertar un objeto nuevo concepto en la base de datos
        /// </summary>
        public ViewModelComprobanteConcepto Insert(ViewModelComprobanteConcepto objeto) {
            MySqlCommand sqlInsert = new MySqlCommand() {
                CommandText = String.Concat("insert into _cfdcnp (_cfdcnp_a,_cfdcnp_cfds_id,_cfdcnp_undd,_cfdcnp_noidnt,_cfdcnp_cncpt,_cfdcnp_cntdd,_cfdcnp_untr,_cfdcnp_sbttl,_cfdcnp_usr_n,_cfdcnp_fn,_cfdcnp_ctapre,_cfdcnp_clvprds,_cfdcnp_clvund,_cfdcnp_dscnt, _cfdcnp_parte, _cfdcnp_impto, _cfdcnp_pdd_id) ",
                    "values (@cfdcnp_a,@cfdcnp_cfds_id,@cfdcnp_undd,@cfdcnp_noidnt,@cfdcnp_cncpt,@cfdcnp_cntdd,@cfdcnp_untr,@cfdcnp_sbttl,@cfdcnp_usr_n,@cfdcnp_fn,@cfdcnp_ctapre,@cfdcnp_clvprds,@cfdcnp_clvund,@cfdcnp_dscnt, @cfdcnp_parte, @cfdcnp_impto, @cfdcnp_pdd_id); select last_insert_id();")
            };

            sqlInsert.Parameters.AddWithValue("@index", objeto.Id);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_a", 1);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_cfds_id", objeto.SubId);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_cntdd", objeto.Cantidad);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_cncpt", objeto.Descripcion);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_sbttl", objeto.Importe);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_noidnt", objeto.NoIdentificacion);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_undd", objeto.Unidad);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_untr", objeto.ValorUnitario);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_usr_n", objeto.Creo);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_fn", DateTime.Now);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_ctapre", objeto.CtaPredial);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_clvprds", objeto.ClaveProdServ);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_clvund", objeto.ClaveUnidad);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_dscnt", objeto.Descuento);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_parte", objeto.JParte);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_impto", objeto.JImpuestos);
            sqlInsert.Parameters.AddWithValue("@cfdcnp_pdd_id", objeto.NumPedido);
            objeto.Id = (int)this.ExecuteScalar(sqlInsert);
            return objeto;
        }

        /// <summary>
        /// actualizar un objeto concepto en la base de datos
        /// </summary>
        public bool Update(ViewModelComprobanteConcepto objeto) {
            MySqlCommand sqlUpdate = new MySqlCommand {
                CommandText = "update _cfdcnp set _cfdcnp_a=@cfdcnp_a,_cfdcnp_cfds_id=@cfdcnp_cfds_id,_cfdcnp_undd=@cfdcnp_undd,_cfdcnp_noidnt=@cfdcnp_noidnt,_cfdcnp_cncpt=@cfdcnp_cncpt,_cfdcnp_cntdd=@cfdcnp_cntdd,_cfdcnp_untr=@cfdcnp_untr,_cfdcnp_sbttl=@cfdcnp_sbttl,	_cfdcnp_usr_m=@cfdcnp_usr_m,_cfdcnp_fm=@cfdcnp_fm,_cfdcnp_ctapre=@cfdcnp_ctapre,_cfdcnp_clvprds=@cfdcnp_clvprds,_cfdcnp_clvund=@cfdcnp_clvund,_cfdcnp_dscnt=@cfdcnp_dscnt, _cfdcnp_parte=@cfdcnp_parte, _cfdcnp_impto=@cfdcnp_impto, _cfdcnp_pdd_id=@cfdcnp_pdd_id where _cfdcnp_id=@index"
            };

            sqlUpdate.Parameters.AddWithValue("@index", objeto.Id);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_a", objeto.IsActive);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_cfds_id", objeto.SubId);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_cntdd", objeto.Cantidad);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_cncpt", objeto.Descripcion);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_sbttl", objeto.Importe);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_noidnt", objeto.NoIdentificacion);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_undd", objeto.Unidad);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_untr", objeto.ValorUnitario);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_usr_m", objeto.Modifica);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_fm", DateTime.Now);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_ctapre", objeto.CtaPredial);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_clvprds", objeto.ClaveProdServ);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_clvund", objeto.ClaveUnidad);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_dscnt", objeto.Descuento);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_parte", objeto.JParte);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_impto", objeto.JImpuestos);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_pdd_id", objeto.NumPedido);
            return ExecuteTransaction(sqlUpdate) > 0;
        }

        public ComplementoNomina Insert(ComplementoNomina objeto) {
            MySqlCommand sqlInsert = new MySqlCommand() {
                CommandText = string.Concat("insert into _cfdnmn (_cfdnmn_id,_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tipo,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,_cfdnmn_dttlexn,_cfdnmn_dscnt,_cfdnmn_hrsxtr,_cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,_cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,_cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,_cfdnmn_fn,_cfdnmn_usr_n,_cfdnmn_uuid,_cfdnmn_rfc) ",
                    "values (@cfdnmn_id,@cfdnmn_drctr_id,@cfdnmn_cfdi_id,@cfdnmn_tipo,@cfdnmn_tpreg,@cfdnmn_rsgpst,@cfdnmn_qnc,@cfdnmn_anio,@cfdnmn_antgdd,@cfdnmn_dspgds,@cfdnmn_bsctap,@cfdnmn_drintg,@cfdnmn_pttlgrv,@cfdnmn_pttlexn,@cfdnmn_dttlgrv,@cfdnmn_dttlexn,@cfdnmn_dscnt,@cfdnmn_hrsxtr,@cfdnmn_ver,@cfdnmn_banco,@cfdnmn_rgp,@cfdnmn_numem,@cfdnmn_curp,@cfdnmn_numsgr,@cfdnmn_clabe,@cfdnmn_depto,@cfdnmn_puesto,@cfdnmn_tpcntr,@cfdnmn_tpjrn,@cfdnmn_prddpg,@cfdnmn_fchpgo,@cfdnmn_fchini,@cfdnmn_fchfnl,@cfdnmn_fchrel,@cfdnmn_fn,@cfdnmn_usr_n,@cfdnmn_uuid,@cfdnmn_rfc) ")
            };

            sqlInsert.Parameters.AddWithValue("@cfdnmn_id", objeto.Id);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_tipo", objeto.TipoNomina);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_drctr_id", objeto.Receptor.Id);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_cfdi_id", objeto.SubId);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_tpreg", objeto.Receptor.TipoRegimen);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_rsgpst", objeto.Receptor.RiesgoPuesto);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_qnc", objeto.FechaPago.Value.Day > 15 ? 2 : 1);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_anio", objeto.FechaPago.Value.Year);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_antgdd", objeto.Receptor.Antiguedad);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_dspgds", objeto.NumDiasPagados);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_bsctap", objeto.Receptor.SalarioBaseCotApor);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_drintg", objeto.Receptor.SalarioDiarioIntegrado);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_pttlgrv", objeto.Percepciones.TotalGravado);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_pttlexn", objeto.Percepciones.TotalExento);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_dttlgrv", objeto.Deducciones.TotalImpuestosRetenidos);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_dttlexn", objeto.Deducciones.TotalOtrasDeducciones);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_hrsxtr", 0);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_ver", objeto.Version);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_banco", objeto.Receptor.Banco);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_rgp", objeto.Emisor.RegistroPatronal);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_numem", objeto.Receptor.Num);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_curp", objeto.Receptor.CURP);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_numsgr", objeto.Receptor.NumSeguridadSocial);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_clabe", objeto.Receptor.CuentaBancaria);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_depto", objeto.Receptor.Departamento);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_puesto", objeto.Receptor.Puesto);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_tpcntr", objeto.Receptor.TipoContrato);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_tpjrn", objeto.Receptor.TipoJornada);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_prddpg", objeto.Receptor.PeriodicidadPago);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_fchpgo", objeto.FechaPago);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_fchini", objeto.FechaInicialPago);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_fchfnl", objeto.FechaFinalPago);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_fchrel", objeto.Receptor.FechaInicioRelLaboral);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_fn", DateTime.Now);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_usr_n", objeto.Creo);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_uuid", objeto.IdDocumento);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_dscnt", objeto.Descuento);
            sqlInsert.Parameters.AddWithValue("@cfdnmn_rfc", objeto.Receptor.RFC);

            if (this.ExecuteTransaction(sqlInsert) > 0) {
                return objeto;
            }
            return null;
        }

        /// <summary>
        /// obtener listado de comprobantes no sincronizados del año en curso (utilizado principalmente para la sincronizacion)
        /// </summary>
        /// <returns></returns>
        public BindingList<ViewModelComprobanteSingle> GetList() {
            var sqlCommand = new MySqlCommand() {
                //CommandText = "select _cfdi.* from _cfdi where _cfdi_doc_id = 3 and _cfdi_a=1 and year(_cfdi_fecems)=@anio and _cfdi_uuid<>'' and _cfdi_sync=0 order by _cfdi_fecems desc"
                CommandText = "select _cfdi.* from _cfdi where _cfdi_doc_id = 3 and _cfdi_a=1 and year(_cfdi_fecems)=2019 and _cfdi_uuid<>'' order by _cfdi_fecems DESC LIMIT 100"
            };
            sqlCommand.Parameters.AddWithValue("@anio", 2019);
            DataTable data = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelComprobanteSingle>();
            return new BindingList<ViewModelComprobanteSingle>(mapper.Map(data).ToList());
        }

        public BindingList<ViewModelComprobanteSingleS> GetListTest() {
            var sqlCommand = new MySqlCommand() {
                CommandText = "select _cfdi.* from _cfdi where _cfdi_a=1 and year(_cfdi_fecems)=@anio and _cfdi_uuid<>'' and _cfdi_sync=0 order by _cfdi_fecems desc"
            };
            sqlCommand.Parameters.AddWithValue("@anio", 2019);
            DataTable data = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelComprobanteSingleS>();
            return new BindingList<ViewModelComprobanteSingleS>(mapper.Map(data).ToList());
        }

        #endregion

        /// <summary>
        /// solo actualizar la bandera de la sincronizacion
        /// </summary>
        /// <param name="index">indice del comprobante fiscal</param>
        /// <returns>verdadero o falso</returns>
        public bool Update(int index) {
            var sqlUpdate = new MySqlCommand() {
                CommandText = "update _cfdi set _cfdi_sync = 1 where _cfdi_id=@index"
            };
            sqlUpdate.Parameters.AddWithValue("@index", index);
            return this.ExecuteScalar(sqlUpdate) > 0;
        }
    }
}
