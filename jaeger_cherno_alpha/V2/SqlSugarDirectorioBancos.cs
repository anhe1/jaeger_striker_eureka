﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using SqlSugar;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.V2
{
    public class SqlSugarDirectorioBancos : MySqlSugarContext<ViewModelCuentaBancaria>
    {
        public SqlSugarDirectorioBancos(DataBaseConfiguracion configuracion) : base(configuracion)
        {
        }

        public BindingList<ViewModelCuentaBancaria> GetList(int index, bool verificado = false)
        {
            return new BindingList<ViewModelCuentaBancaria>(this.Db.Queryable<ViewModelCuentaBancaria>().Where(it => it.SubId == index).WhereIF(verificado == true, it => it.Verificado == true).ToList());
        }

        public BindingList<ViewModelCuentaBancaria> AdoGetList(int index, bool verificado = false)
        {
            string sqlString = "select * from _dcbnc where _dcbnc_drctr_id=@index";
            DataTable data = this.Db.Ado.GetDataTable(sqlString, new List<SugarParameter> { new SugarParameter("@index", index), new SugarParameter("@verificado", verificado) });
            DataNamesMapper<ViewModelCuentaBancaria> mapper = new DataNamesMapper<ViewModelCuentaBancaria>();
            return new BindingList<ViewModelCuentaBancaria>(mapper.Map(data).ToList());
        }
    }

    public class MySqlDirectorioBancos : MySqlSugarContext<ViewModelCuentaBancaria> {
        public MySqlDirectorioBancos(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public BindingList<ViewModelCuentaBancaria> AdoGetList(int index, bool verificado = false)
        {
            var sqlCommand = new MySql.Data.MySqlClient.MySqlCommand() { CommandText = "select * from _dcbnc where _dcbnc_drctr_id=@index" };
            sqlCommand.Parameters.AddWithValue("@index", index);
            sqlCommand.Parameters.AddWithValue("@verificado", verificado);
            DataTable data = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ViewModelCuentaBancaria>();
            return new BindingList<ViewModelCuentaBancaria>(mapper.Map(data).ToList());
        }
    }
}
