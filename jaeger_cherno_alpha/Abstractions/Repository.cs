﻿using MySql.Data.MySqlClient;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.Edita.Abstractions {
    /// <summary>
    /// clase abstracta para del repositorio
    /// </summary>
    public abstract class Repository {
        private readonly string connectionString;
        private readonly DataBaseConfiguracion configuracion;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public Repository(DataBaseConfiguracion configuracion) {
            this.configuracion = configuracion;
            this.connectionString = this.StringBuilder(configuracion).ToString();
        }

        /// <summary>
        /// obtener conexion valida a la base de datos
        /// </summary>
        /// <returns>retorna un objeto FbConnection</returns>
        protected MySqlConnection GetConnection() {
            return new MySqlConnection(this.connectionString);
        }

        /// <summary>
        /// generador de cadena de conexion de la base de datos
        /// </summary>
        /// <param name="configuracion">objeto de configuracion de la base de datos</param>
        /// <returns>retorna un objeto FbConnectionStringBuilder</returns>
        public MySqlConnectionStringBuilder StringBuilder(DataBaseConfiguracion configuracion) {
            var builder = new MySqlConnectionStringBuilder();
            int puerto = 4530;
            if (uint.Parse(configuracion.PortNumber.ToString()) != 0)
                puerto = configuracion.PortNumber;
            builder.UserID = configuracion.UserID;
            builder.Password = configuracion.Password;
            builder.Server = configuracion.HostName;
            builder.Database = configuracion.Database;
            builder.Port = uint.Parse(puerto.ToString());
            builder.SslMode = (MySqlSslMode)configuracion.ModoSSL;
            builder.Pooling = configuracion.Pooling;
            builder.ConnectionLifeTime = 0;
            builder.ConnectionTimeout = 60;
            return builder;
        }
    }
}
