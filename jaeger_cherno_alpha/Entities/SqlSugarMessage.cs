﻿using System;
using System.Linq;

namespace Jaeger.Edita.Entities
{
    public class SqlSugarMessage
    {
        public SqlSugarMessage()
        {
            this.DateTime = DateTime.Now;
            this.Type = "Advertencia";
        }

        public SqlSugarMessage(string message, int noError, string type)
        {
            this.DateTime = DateTime.Now;
            this.Value = message;
            this.NoError = noError;
            this.Type = type;
        }

        public DateTime DateTime { get; set; }
        public int NoError { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }

        public override string ToString()
        {
            return string.Concat(this.DateTime.ToShortDateString(), "|", this.Type, "|", this.NoError.ToString(), this.Value);
        }
    }
}
