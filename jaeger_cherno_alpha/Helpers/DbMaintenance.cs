﻿using System;
using System.Linq;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using SqlSugar;
using Jaeger.Edita.Entities;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.Helpers
{
    public class DbMaintenance
    {
        public partial class Tabla
        {
            public string Nombre { get; set; }

            public string Descripcion { get; set; }
        }

        public DbMaintenance(DataBaseConfiguracion configuracion)
        {
            this.Configuracion = configuracion;
            this.Message = new SqlSugarMessage();
            this.dbase = new SqlSugarClient(new ConnectionConfig()
            {
                ConnectionString = this.StringBuilderToString(),
                DbType = DbType.MySql,
                IsAutoCloseConnection = true,
                InitKeyType = InitKeyType.Attribute,
                AopEvents = new AopEvents()
                {
                    OnLogExecuting = (sql, p) =>
                    {
                        Console.WriteLine(string.Concat("Executing SQL: ", sql));
                        Console.WriteLine(string.Join(",", p.Select(it => it.ParameterName + ":" + it.Value)));
                    }
                }
            });
        }

        public SqlSugarClient dbase { get; set; }

        public DataBaseConfiguracion Configuracion { get; set; }

        public SqlSugarMessage Message { get; set; }

        public void CreateClass()
        {
            this.dbase.DbFirst.IsCreateAttribute().CreateClassFile("c:\\Jaeger\\Jaeger.Temporal", "Models");
        }

        public void CreateClass(string nombre)
        {
            this.dbase.DbFirst.IsCreateAttribute().Where(nombre).CreateClassFile("c:\\Jaeger\\Jaeger.Temporal", "Models");
        }

        public List<Tabla> GetTablesInfo()
        {
            List<Tabla> lista = new List<Tabla>();
            var tables = this.dbase.DbMaintenance.GetTableInfoList();
            foreach (var item in tables)
            {
                lista.Add(new Tabla { Nombre = item.Name, Descripcion = item.Description });
            }
            return lista;
        }

        private string StringBuilderToString()
        {
            MySqlConnectionStringBuilder cs = new MySqlConnectionStringBuilder()
            {
                Pooling = false,
                Database = this.Configuracion.Database,
                Server = this.Configuracion.HostName,
                UserID = this.Configuracion.UserID,
                Port = 3306,
                Password = this.Configuracion.Password,
            };
            return cs.ConnectionString;
        }

    }
}
