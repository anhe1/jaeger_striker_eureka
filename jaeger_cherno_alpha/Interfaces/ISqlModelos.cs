﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.Edita.V2.Almacen.Entities;
using Jaeger.Edita.V2.Almacen.Enums;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqlModelos
    {
        SqlSugarClasificacion Categorias { get; set; }

        SqlSugarAlmacenUnidades Unidades { get; set; }

        int Insert(ViewModelModelo item);

        int Update(ViewModelModelo item);

        ViewModelModelo Save(ViewModelModelo item);

        List<ViewModelModelo> GetList(EnumAlmacen tipo, bool activo = true);

        ViewModelModelo GetByIdentificador(string sku);

        /// <summary>
        /// obtener una lista de objetos por la descripcion o etiquetas del modelo
        /// </summary>
        /// <param name="descripcion">descripcion o etiqueta relacionado al modelo</param>
        /// <returns>Lista de objeto ViewModelModelo</returns>
        //List<ProductoModelo> GetListByDescripcion(string descripcion);

        /// <summary>
        /// obtener listado de productos del almacen de producto terminado para CFDI
        /// </summary>
        /// <param name="almacen">tipo de almacen</param>
        /// <returns>lista de objetos List<ViewModelComprobanteModelo></returns>
        List<ViewModelComprobanteModelo> GetListConceptos(EnumAlmacen almacen, string descripcion = "");

        /// <summary>
        /// obtener listado de producto y modelos corresponientes al almacen, descripion y el indice del proveedor
        /// </summary>
        List<ProductoModeloBusqueda> GetListByDescripcion(EnumAlmacen almacen, string descripcion, int proveedor);

        /// <summary>
        /// obtener listado de producto y modelos corresponientes al almacen, descripion
        /// </summary>
        List<ProductoModeloBusqueda> GetListByDescripcion1(EnumAlmacen almacen, string descripcion);

        /// <summary>
        /// dar de baja un modelo
        /// </summary>
        /// <param name="modelo"></param>
        /// <returns></returns>
        bool Delete(ViewModelModelo modelo);

        bool CrearTablas();
    }
}
