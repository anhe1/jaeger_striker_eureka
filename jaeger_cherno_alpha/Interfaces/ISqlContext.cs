﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqlContext<T>
    {
        int Insert(T objeto);

        int Update(T objeto);

        bool Delete(int index);

        T Save(T objeto);

        T GetById(int index);

        List<T> GetList();
    }
}
