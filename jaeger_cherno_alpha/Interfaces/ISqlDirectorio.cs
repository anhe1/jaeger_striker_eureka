﻿using System.ComponentModel;
using System.Data;
using System.Collections.Generic;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Edita.V2.Directorio.Enums;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqlDirectorio
    {
        int ReturnId(string rfc);

        ViewModelContribuyente GetById(int index);

        int Clave(string clave);
        
        ViewModelContribuyente GetByRFC(string rfc);

        ViewModelContribuyenteDomicilio GetSingleRFC(string rfc);

        List<ViewModelContribuyenteSingle> GetList(EnumRelationType tipo);

        BindingList<ViewModelContribuyenteDomicilio> GetListBy(EnumRelationType tipo);
        
        DataTable GetContactos(EnumRelationType relationship, bool showDeletes = false);
        
        ViewModelContribuyente Save(ViewModelContribuyente objeto);
        
        BindingList<ViewModelDomicilio> GetDomicilios(int index);

        int Insert(ViewModelContribuyente objeto);

        int Update(ViewModelContribuyente objeto);
    }
}