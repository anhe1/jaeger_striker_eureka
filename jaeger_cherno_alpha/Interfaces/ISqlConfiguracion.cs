﻿using System;
using Jaeger.Edita.V2.Empresa.Entities;
using Jaeger.Edita.V2;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqlConfiguracion
    {
        /// <summary>
        /// obtener la configuracion para la empresa segun la llave
        /// </summary>
        ViewModelEmpresaConfiguracion GetByKey(string key);

        ViewModelEmpresaConfiguracion GetByKey(SqlSugarConfiguracion.KeyConfiguracion key);

        /// <summary>
        /// almacenar configuracion
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        ViewModelEmpresaConfiguracion Save(ViewModelEmpresaConfiguracion item);

        /// <summary>
        /// obtener la configuracion de los datos generales de la empresa
        /// </summary>
        /// <returns></returns>
        EmpresaData GetSynapsis();
    }
}
