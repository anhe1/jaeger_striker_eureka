﻿using Jaeger.Edita.V2.Empresa.Entities;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqlConfiguracionEmpresas
    {
        ViewModelEmpresa GetByRFC(string rfc);
    }
}
