using System.Collections.Generic;
using Jaeger.Edita.V2.Almacen.Entities;
using Jaeger.Edita.V2.Almacen.Enums;
using Jaeger.Edita.V2.CFDI.Entities;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqModelos
    {
        ViewModelModelo Save(ViewModelModelo item);

        List<ViewModelModelo> GetList(EnumAlmacen tipo);

        ViewModelModelo GetByIdentificador(string sku);

        List<ProductoModelo> GetListByDescripcion(string descripcion);

        List<ViewModelComprobanteModelo> GetListConceptos(EnumAlmacen almacen, string descripcion = "");
    }
}