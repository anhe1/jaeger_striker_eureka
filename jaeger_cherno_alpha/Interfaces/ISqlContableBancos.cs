﻿using System;
using System.ComponentModel;
using Jaeger.Edita.V2.Contable.Entities;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqlContableBancos
    {
        BindingList<ViewModelBancoCuenta> GetCuentasDeBanco();

        ViewModelBancoSaldo GetSaldo(ViewModelBancoCuenta objeto, DateTime fecha);

        ViewModelBancoSaldo Insert(ViewModelBancoSaldo objeto);

        ViewModelBancoCuenta Save(ViewModelBancoCuenta objeto);
    }
}
