﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using Jaeger.Edita.Enums;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2.CFDI.Entities.Cancel;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.Contable.Entities;
using Jaeger.Entities;
using Jaeger.Entities.Reportes;
using Jaeger.SAT.Entities;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqlComprobanteFiscal
    {
        /// <summary>
        /// obtener el indice de un comprobante por su folio fiscal (UUID)
        /// </summary>
        int ReturnId(string idDocumento);
        

        /// <summary>
        /// almacenar un objeto Comprobante en la base de datos
        /// </summary>
        /// <param name="comprobante">ViewModelComprobante</param>
        /// <returns>ViewModelComprobante</returns>
        ViewModelComprobante Save(ViewModelComprobante objeto);

        /// <summary>
        /// actualizar url del archivo XML del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">direccion URL</param>
        /// <returns>verdadero si la acción se realiza con exito</returns>
        bool UpdateUrlXml(int indice, string url);

        /// <summary>
        /// actualizar url del archivo XML del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">direccion URL</param>
        /// <returns>verdadero si la acción se realiza con exito</returns>
        bool UpdateUrlPdf(int indice, string url);

        /// <summary>
        /// actualizar url del acuse de cancelacion del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">url</param>
        /// <returns>verdadero si la accion se realiza con exito</returns>
        bool UpdateUrlXmlAcuse(int indice, string url);

        /// <summary>
        /// actualizar url del acuse de cancelacion del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">url</param>
        /// <returns>verdadero si la accion se realiza con exito</returns>
        bool UpdateUrlPdfAcuse(int indice, string url);

        /// <summary>
        /// actualizar estado del comprobante segun SAT
        /// </summary>
        /// <param name="response">objeto respuesta del servicio SAT</param>
        /// <returns>verdadero si fue actualizado el registro con exito</returns>
        bool Update(SatQueryResult response);

        /// <summary>
        /// actualizar el objeto del de cancelacion de un comprobante fiscal
        /// </summary>
        bool Update(string uuid, CancelaCFDResponse item, string usuario);

        /// <summary>
        /// actualizar status desde la lista de comprobantes
        /// </summary>
        bool Update(int indice, string status, string usuario);

        ViewModelComprobante GetComprobante(int indice);

        ViewModelComprobanteSingle GetSingle(string idDocumento);

        CFDI.Entities.Comprobante GetAdoComprobante(int indice);

        BindingList<ViewModelComprobanteSingle> GetListSingleBy(EnumCfdiSubType subtipo, string status, EnumMonthsOfYear mes = EnumMonthsOfYear.None, int anio = 0, bool ado = true);

        BindingList<ViewModelComprobanteSingle> GetListSingleBy(EnumCfdiSubType subtipo, string rfc, string folio = "", string status = "", string metodo = "");

        BindingList<ViewModelComprobanteConcepto> GetConceptos(int indice, bool ado = true);

        BindingList<ViewModelComprobanteSingle> GetListIn(string[] uuids);

        BindingList<ViewModelReciboFiscal> GetRecibosFiscales(int mes, int anio, int dia, string rfc);

        #region operaciones con prepolizas

        ViewModelPrePoliza CrearRecibo(List<string> folioFiscal, ComplementoPagos complemento = null);

        BindingList<ViewModelPrePoliza> Recibos(string idDocumento);

        List<ComplementoPagoDoctoRelacionado> Rep(string idDocumento);

        #endregion

        bool AplicarCFDIRelacionados(ComprobanteCfdiRelacionados objeto);

        bool AplicarComprobantePagos(ComplementoPagos objeto);

        ViewModelComprobante CrearReciboElectronicoPago(List<ViewModelComprobanteSingleC> lista);

        ViewModelAccuseCancelacion GetAccuse(string uuid);

        List<ViewModelBackupComprobante> GetBackupFiles(EnumCfdiSubType subTipo, int anio, int mes = 0, EnumCfdiDates porFecha = EnumCfdiDates.FechaTimbre);
        
        string GetFolio(string rfc, string serie);

        List<ViewModelBackup> GetInfoBackUp(EnumCfdiSubType objType, EnumCfdiDates porFecha);

        BindingList<ViewModelComprobanteSingleC> GetListBy(EnumCfdiSubType subtipo, string status, EnumMonthsOfYear mes = EnumMonthsOfYear.None, int anio = 0, bool ado = true);
        
        List<ViewModelEstadoCuenta> Prueba(EnumCfdiSubType subtipo, string rfc, int anio, EnumMonthsOfYear mes);

        DataTable Prueba3(int ejercicio, EnumCfdiSubType subtipo, string estado, string rfc = "");
        
        EstadoDeCuenta ReporteEstadoCuenta(string rfc, EnumCfdiSubType subtipo, string status, EnumMonthsOfYear mes = EnumMonthsOfYear.None, int anio = 0, EnumCfdiDates fechas = EnumCfdiDates.FechaEmision);

        List<ViewModelEstadoCuenta> Resumen(EnumCfdiSubType subtipo, int anio, EnumMonthsOfYear mes);

        EstadoCuentaComprobantes Reporte(string rfc, EnumCfdiSubType objType, string status, EnumMonthsOfYear mes = 0, int anio = 0, EnumCfdiDates fechas = EnumCfdiDates.FechaEmision);
    }
}