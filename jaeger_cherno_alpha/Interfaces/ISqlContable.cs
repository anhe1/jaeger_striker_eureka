using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Edita.Enums;
using Jaeger.Edita.Helpers;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.Contable.Entities;
using Jaeger.Edita.V2.Contable.Enums;
using Jaeger.Edita.V2.Empresa.Entities;
using Jaeger.Enums;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqlContable
    {
        ISqlContableBancos Banco { get; set; }

        /// <summary>
        /// calcular el numero de indentifiacion de la prepoliza
        /// </summary>
        string NoIndetificacion(ViewModelPrePoliza objeto);

        /// <summary>
        /// calcular el numero consecutivo del recibo segun el tipo
        /// </summary>
        long Folio(ViewModelPrePoliza objeto);

        /// <summary>
        /// insertar un nuevo objeto ViewModelPrePoliza
        /// </summary>
        /// <param name="objeto">ViewModelPrePoliza</param>
        /// <returns>ViewModelPrePoliza</returns>
        ViewModelPrePoliza Insert(ViewModelPrePoliza objeto);

        ViewModelPrePoliza Update(ViewModelPrePoliza objeto);

        ViewModelPrePoliza GetPrePoliza(string noident);

        BindingList<ViewModelPrePolizaComprobante> GetComprobantes(string noIdentificacion);

        BindingList<ViewModelPrePoliza> GetPrePolizas(EnumPolizaTipo tipo, EnumMonthsOfYear mes = 0, int anio = 0, string cuenta = "todos");

        BindingList<ViewModelPrePolizaComprobante> GetComprobantes(EnumCfdiSubType objtype, string emisor, string receptor, string status, string metodo = "", string efecto = "");

        ViewModelPrePoliza Save(ViewModelPrePoliza objeto);

        ViewModelPrePoliza CrearRecibo(List<ViewModelComprobanteSingleC> lista);

        ViewModelBancoMovimiento Aplicar(ViewModelBancoMovimiento objeto);

        BancoEstadoCuenta EstadoCuenta(EnumPolizaTipo tipo, EnumMonthsOfYear mes = 0, int anio = 0, ViewModelBancoCuenta cuenta = null);

        ViewModelPrePoliza Traspaso(ViewModelPrePoliza objeto);

        ViewModelBancoSaldo Save(ViewModelBancoSaldo objeto);

        /// <summary>
        /// aplicar el cambio de status de la prepoliza
        /// </summary>
        bool AplicarStatus(string noident, string newStatus, string usuario);

        /// <summary>
        /// aplicar los cambios a los comprobantes relacionados a la prepoliza
        /// </summary>
        bool Aplicar(string noident);

        /// <summary>
        /// replicar un contenido al conjunto de comprobantes relacionados a una prepoliza
        /// </summary>
        bool Replicar(ViewModelPrePoliza objeto);

        void Create();

        void Revisar(BindingList<ViewModelPrePoliza> datos);
    }
}