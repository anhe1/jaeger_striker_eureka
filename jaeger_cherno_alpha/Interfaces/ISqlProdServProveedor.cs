﻿using System;
using System.ComponentModel;
using Jaeger.Edita.V2.Almacen.Entities;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqlProdServProveedor
    {
        bool Delete(ViewModelModeloProveedor proveedor);

        BindingList<ViewModelModeloProveedor> GetListBy(int index, bool activo = true);

        ViewModelModeloProveedor Save(ViewModelModeloProveedor proveedor);

        BindingList<ViewModelModeloProveedor> Save(BindingList<ViewModelModeloProveedor> proveedor);

        bool CrearTablas();
    }
}
