﻿using System;
using Jaeger.Edita.Enums;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Edita.V2.CFDI.Entities;
using System.Data;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.Nomina.Entities;
using System.Collections.Generic;
using Jaeger.SAT.Entities;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqlComprobanteNomina
    {
        int Insert(ComplementoNomina complemento);

        ViewModelNominaParteSingle Insert(ViewModelNominaParteSingle single);

        ComplementoNomina GetBy(int subIndex);

        ComplementoNomina Save(ComplementoNomina complemento);

        DataTable GetResumen(int anio, EnumCfdiEstado estado = EnumCfdiEstado.Vigente, EnumMonthsOfYear mes = EnumMonthsOfYear.None);

        List<NominaViewSingle> GetCfdNominas(int index, bool activos = true);

        List<NominaViewSingle> GetCfdUuid(string uuid, bool showDelete);

        List<NominaViewSingle> GetCfdNominasBy(string nombre);

        List<NominaViewSingle> GetCfdNominaBy(EnumCfdiDates dateBy, DateTime dateStart, DateTime dateEnd, string depto, string employee);

        /// <summary>
        /// actualizar estado del comprobante segun SAT
        /// </summary>
        /// <param name="response">objeto respuesta del servicio SAT</param>
        /// <returns>verdadero si fue actualizado el registro con exito</returns>
        bool Update(SatQueryResult response);

        DataTable GetNominas();

        DataTable GetNominaDeptos();

        DataTable GetEmpleados();
    }
}
