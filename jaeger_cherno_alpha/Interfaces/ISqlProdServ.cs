using System.ComponentModel;
using Jaeger.Edita.V2.Almacen.Entities;
using Jaeger.Edita.V2.Almacen.Enums;
using Jaeger.Edita.V2;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqlProdServ
    {
        SqlSugarClasificacion Categorias { get; set; }

        SqlSugarAlmacenUnidades Unidades { get; set; }

        ISqlModelos Modelos { get; set; }

        ISqlProdServProveedor Proveedor { get; set; }

        ProductoModelo Save(ProductoModelo producto);

        ProductoModelo GetById(int identificador);

        ProductoModelo GetByIdentificador(string identificador);

        BindingList<ProductoModelo> GetList(EnumAlmacen tipo);

        bool CrearTablas();
    }
}