﻿using System;
using System.Collections.Generic;
using Jaeger.Edita.V2.Almacen.Entities;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqlClasificacionG
    {
        List<ViewModelClase> GetListBy();
    }
}
