﻿using System.Collections.Generic;
using SqlSugar;
using Jaeger.Entities;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqlSugarContext<T> where T : class, new()
    {
        SimpleClient<T> CurrentDb { get; }
        SqlSugarClient Db { get; }
        MessageError Message { get; set; }
        DataBaseConfiguracion Settings { get; }

        string CreateGuid(string[] datos);
        bool Delete(int id);
        T GetById(int id);
        List<T> GetList();
        int Insert(T objeto);
        int Update(T objeto);
    }
}