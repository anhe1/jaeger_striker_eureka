﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.Edita.V2.Almacen.Entities;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqlUnidades
    {
        int Insert(ViewModelUnidad item);

        int Update(ViewModelUnidad item);

        List<ViewModelUnidad> GetList();

        int Save(List<ViewModelUnidad> items);
    }
}
