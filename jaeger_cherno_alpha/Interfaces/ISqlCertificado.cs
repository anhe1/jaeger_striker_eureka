﻿using Jaeger.Edita.Entities.Basico;
using System.Collections.Generic;

namespace Jaeger.Edita.Interfaces
{
    public interface ISqlCertificado
    {
        IEnumerable<ViewModelCertificado> GetList();
    }
}
