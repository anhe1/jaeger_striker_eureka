﻿using System;
using System.Linq;
using System.ComponentModel;
using Jaeger.Edita.Enums;
using Jaeger.Repositorio.V3.Entities;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Repositorio.Helpers
{
    public class SQLiteRepositorio : SQLiteContext<ViewModelDescargaResponse>
    {
        public SQLiteRepositorio(DataBaseConfiguracion configuracion) : base(configuracion)
        {
        }

        public BindingList<ViewModelDescargaResponse> GetList(EnumMonthsOfYear mes = 0, int anio = 0, string efecto = "Todos", string tipo = "Recibidos")
        {
            var sql1 = @"SELECT `IdDocumento`,`Tipo`,`EmisorRFC`,`Emisor`,`ReceptorRFC`,`Receptor`,`FechaEmision`,`FechaCerfificacion`,`PAC`,`RetencionISR`,`RetencionIVA`,`RetencionIEPS`,`TrasladoIVA`,`TrasladoIEPS`,`SubTotal`,`Descuento`,`Total`,`Efecto`,`StatusCancelacion`,`Estado`,`StatusProcesoCancelar`,`FechaStatusCancela`,`FechaValidacion`,`FechaNuevo`,`PathXML`,`PathPDF`,`Resultado`,`XMLB64`,`PDFB64`,`AcuseB64`,`AcuseFinalB64`,`Comprobado` FROM `Comprobantes`  WHERE substr(`FechaEmision`, 1, {0}) = '{1}' AND ( `Tipo` = '{2}' ) @otro ORDER BY `FechaEmision`";
            var par = 5;
            var d1 = anio.ToString() + "-";
            if ((int)mes > 0) {
                d1 = d1 + ((int)mes).ToString("00");
                par = par + 2;
            }

            if (efecto != "Todos") {
                sql1 = sql1.Replace("@otro", "'" + efecto + "'");
            } else {
                sql1 = sql1.Replace("@otro", "");
            }

            try {
                var sql2 = string.Format(sql1, par, d1, tipo);
                return new BindingList<ViewModelDescargaResponse>(this.Db.Ado.SqlQuery<ViewModelDescargaResponse>(sql2));
                //return new BindingList<ViewModelDescargaResponse>(
                //this.Db.Queryable<ViewModelDescargaResponse>()
                //.Where(it => it.FechaEmision.Year == anio)
                //.WhereIF(efecto != "Todos", it => it.Efecto == efecto)
                //.Where(it => it.Tipo == tipo)
                //.WhereIF(mes != 0, it => it.FechaEmision.Month == (int)mes)
                //.ToList());
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return this.GetList1((int)mes, anio, efecto, tipo);
            }
        }

        public BindingList<ViewModelDescargaResponse> GetList1(int month = 0, int year = 0, string tipoComprobante = "Todos", string subTipoComprobante = "Recibidos") {
            return new BindingList<ViewModelDescargaResponse>(
                this.Db.Queryable<ViewModelDescargaResponse>()
                .Where(it => it.FechaEmision.Year == year)
                .WhereIF(tipoComprobante != "Todos", it => it.Efecto == tipoComprobante)
                .Where(it => it.Tipo == subTipoComprobante)
                .WhereIF(month != 0, it => it.FechaEmision.Month == (int)month).Select((c) => new ViewModelDescargaResponse {
                    IdDocumento = c.IdDocumento,
                    Comprobado = c.Comprobado,
                    Efecto = c.Efecto,
                    Emisor = c.Emisor,
                    EmisorRFC = c.EmisorRFC,
                    Estado = c.Estado,
                    FechaCerfificacion = c.FechaCerfificacion,
                    FechaEmision = c.FechaEmision,
                    FechaNuevo = c.FechaNuevo,
                    FechaStatusCancela = c.FechaStatusCancela,
                    FechaValidacion = c.FechaValidacion,
                    PACCertifica = c.PACCertifica,
                    PathPDF = c.PathPDF,
                    PathXML = c.PathXML,
                    PdfAcuseB64 = c.PdfAcuseB64,
                    PdfAcuseFinalB64 = c.PdfAcuseFinalB64,
                    PdfRepresentacionImpresaB64 = c.PdfRepresentacionImpresaB64,
                    Receptor = c.Receptor,
                    ReceptorRFC = c.ReceptorRFC,
                    Result = c.Result,
                    StatusCancelacion = c.StatusCancelacion,
                    StatusProcesoCancelar = c.StatusProcesoCancelar,
                    Tipo = c.Tipo,
                    Total = c.Total,
                    XmlContentB64 = c.XmlContentB64
                })
                .ToList());
        }

        public BindingList<ViewModelDescargaResponse> Comprobantes { get; set; }

        public void SaveResults()
        {
            //List<ViewModelDescargaResponse> lista = new List<ViewModelDescargaResponse>(this.Comprobantes);
            this.Db.Saveable<ViewModelDescargaResponse>(this.Comprobantes.ToList()).ExecuteCommand();
        }

        public new ViewModelDescargaResponse Update(ViewModelDescargaResponse item) {
            try {
                this.Db.Updateable<ViewModelDescargaResponse>(item).ExecuteCommand();
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return item;
        }

        public VersionModel GetVersion() {
            try {
                var d = this.Db.Queryable<VersionModel>().Where(it => it.Version == "3.0").First();
                return d;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public bool CreateTables()
        {
            try
            {
                this.Db.CodeFirst.InitTables<ViewModelDescargaResponse>();
                this.Db.CodeFirst.InitTables<VersionModel>();
                try {
                    this.Db.Insertable<VersionModel>(new VersionModel { Version = "3.0" }).ExecuteCommand();
                } catch (Exception ex1) {
                    Console.WriteLine(ex1.Message);
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
