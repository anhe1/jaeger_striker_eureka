﻿/// develop: anhe1 29052019
/// purpose: 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Jaeger.Domain.DataBase.Entities;
using SqlSugar;

namespace Jaeger.Repositorio.Helpers
{
    public class SQLiteContext<T> where T : class, new()
    {
        public partial class SQLiteMessage
        {
            public SQLiteMessage()
            {
                this.DateTime = DateTime.Now;
                this.Type = "Advertencia";
            }

            public SQLiteMessage(string message, int noError, string type)
            {
                this.DateTime = DateTime.Now;
                this.Value = message;
                this.NoError = noError;
                this.Type = type;
            }

            public DateTime DateTime { get; set; }
            public int NoError { get; set; }
            public string Value { get; set; }
            public string Type { get; set; }

            public string ToString()
            {
                return string.Concat(this.DateTime.ToShortDateString(), "|", this.Type, "|", this.NoError.ToString(), this.Value);
            }
        }

        private SQLiteMessage menssageField = new SQLiteMessage();
        private DataBaseConfiguracion configuracionField;
        private readonly SqlSugarClient dbase;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion"></param>
        public SQLiteContext(DataBaseConfiguracion configuracion)
        {
            this.configuracionField = configuracion;
            this.Message = new SQLiteMessage();
            this.dbase = new SqlSugarClient(new ConnectionConfig()
            {
                ConnectionString = "DataSource = " + configuracion.Database,
                DbType = DbType.Sqlite,
                IsAutoCloseConnection = true,
                InitKeyType = InitKeyType.Attribute,
                AopEvents = new AopEvents()
                {
                    OnLogExecuting = (sql, p) =>
                    {
                        Console.WriteLine(string.Concat("Executing SQL: ", sql));
                        Console.WriteLine(string.Join(",", p.Select(it => it.ParameterName + ":" + it.Value)));
                    }
                }
            });
        }

        /// <summary>
        /// obtener o establecer la configuracion para las conexiones a la base de datos
        /// </summary>
        public DataBaseConfiguracion Settings
        {
            get
            {
                return this.configuracionField;
            }
            set
            {
                this.configuracionField = value;
            }
        }

        public SqlSugarClient Db
        {
            get
            {
                return this.dbase;
            }
        }

        public SimpleClient<T> CurrentDb
        {
            get
            {
                return new SimpleClient<T>(Db);
            }
        }

        public SQLiteMessage Message
        {
            get
            {
                return this.menssageField;
            }
            set
            {
                this.menssageField = value;
            }
        }

        public virtual T GetById(int id)
        {
            return CurrentDb.GetById(id);
        }

        public virtual List<T> GetList()
        {
            return CurrentDb.GetList();
        }

        public virtual bool Delete(int id)
        {
            return CurrentDb.DeleteById(id);
        }

        public virtual int Insert(T item)
        {
            return this.CurrentDb.InsertReturnIdentity(item);
        }

        public virtual bool Insert(List<T> items)
        {
            return this.CurrentDb.InsertRange(items);
        }

        public virtual int Update(T objeto)
        {
            return this.CurrentDb.AsUpdateable(objeto).ExecuteCommand();
        }

        /// <summary>
        /// Crear tabla del modelo
        /// </summary>
        /// <returns>mensaje</returns>
        public virtual SQLiteMessage Create()
        {
            try
            {
                this.Db.CodeFirst.InitTables<T>();
                return new SQLiteMessage() { DateTime = DateTime.Now, Value = "Ëxito!" };
            }
            catch (SqlSugarException ex)
            {
                return new SQLiteMessage() { DateTime = DateTime.Now, Value = ex.Message };
            }
        }

        public virtual bool CreateDB()
        {
            try
            {
                //this.Db.DbMaintenance.CreateDatabase();
                return true;
            }
            catch (SqlSugarException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public string CreateGuid(string[] datos)
        {
            //use MD5 hash to get a 16-byte hash of the string:
            var provider = new MD5CryptoServiceProvider();

            byte[] inputBytes = Encoding.Default.GetBytes(string.Join("", datos).Trim().ToUpper());

            byte[] hashBytes = provider.ComputeHash(inputBytes);

            //generate a guid from the hash:

            var hashGuid = new Guid(hashBytes);

            return hashGuid.ToString().ToUpper();
        }

    }
}