﻿using System;
using System.Linq;

namespace Jaeger.Repositorio.V3.Enums
{
    public enum EnumDescargaStatus
    {
        Pendiente,
        Descargado,
        Error
    }
}
