﻿/// <summary>
/// declara el tipo de sub tipo comprobantes
/// </summary>
namespace Jaeger.Repositorio.V3.Enums
{
    public enum EnumDescargaTipo
    {
        Todas,
        Emitidos,
        Recibidos,
        RecurperarDescargaCFDI,
        SolucitudesCancelacion
    }
}
