﻿using System;

namespace Jaeger.Repositorio.V3.Enums
{
    public enum EnumDescargaCFDIStatus
    {
        Todos,
        Vigente,
        Cancelado
    }
}
