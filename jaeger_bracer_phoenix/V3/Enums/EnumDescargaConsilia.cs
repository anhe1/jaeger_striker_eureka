﻿using System;
using System.Linq;

namespace Jaeger.Repositorio.V3.Enums
{
    public enum EnumDescargaConsilia
    {
        SinVerificar,
        Verificado,
        NoEncontrado
    }
}
