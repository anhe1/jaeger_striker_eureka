﻿// develop: anhe
// purpose: consulta y descarga de comprobantes fiscales con el metodo tradicional
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Globalization;
using System.ComponentModel;
using Jaeger.Repositorio.Interface;
using Jaeger.Repositorio.V3.Entities;
using Jaeger.Repositorio.V3.Enums;

namespace Jaeger.Repositorio.V3.Helpers
{
    public class DescargaXml1SAT : DescargaXmlSATComun, IDescargaMasiva
    {
        private WebBrowser webSAT;
        
        private TextBox gtxtLog = null;
        private TextBox gtxtStatus = null;
        private string currentURL;
        
        private bool seProcesoWBSat = false;
        private int intentos = 0;
        private bool isConsulta = false;
        private string errorLog = "";
        
        private System.Progress<DescargaProcess> proceso;
        private BackgroundWorker procesar = new BackgroundWorker();

        public event EventHandler<DescargaMasivaStartProcess> StartProcess;
        
        public event EventHandler<DescargaMasivaCompletedProcess> EndProcess;
        
        /// <summary>
        /// constructor
        /// </summary>
        public DescargaXml1SAT(TextBox txtLog, TextBox txtStatus)
        {
            this.gtxtLog = txtLog;
            this.gtxtStatus = txtStatus;
            this.procesar.DoWork += procesar_DoWork;
        }

        private void procesar_DoWork(object sender, DoWorkEventArgs e)
        {
            throw new NotImplementedException();
        }

        #region propiedades

        public int Cuantos
        {
            get
            {
                return this.Resultados.Count();
            }
        }

        #endregion

        #region metodos publicos

        public void Consultar(WebBrowser _wsSat2, Configuracion conf)
        {
            this.Configuracion = conf;
            this.Resultados = new BindingList<ViewModelDescargaResponse>();
            webSAT = _wsSat2;

            webSAT.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this.WebBrowser_DocumentCompleted);

            this.Authenticate();
            this.isConsulta = true;
        }

        public void Reset()
        {
            this.TimerLogin.Enabled = false;
            this.TimerClose.Enabled = false;
            this.TimerError.Enabled = false;
            this.TimerCloseAlert.Enabled = false;
            this.TimerFiltrarRecibidas.Enabled = false;
            this.TimerFiltrarEmitidas.Enabled = false;
            this.TimerRecopilarRecibidas.Enabled = false;
            this.TimerRecopilarEmitidas.Enabled = false;
            this.TimerEvitarCierre.Enabled = false;
        }

        #endregion

        #region metodos privados


        public void Registrar(ViewModelDescargaResponse objeto)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        private void Authenticate()
        {
            try
            {
                this.Message(string.Format("Iniciando sesion del usuario {0}.", this.Configuracion.RFC.Trim().ToUpper()));

                string postData = string.Format("&Ecom_User_ID={0}&Ecom_Password={1}&jcaptcha={2}&submit=Enviar", this.Configuracion.RFC, this.Configuracion.CIEC, this.Configuracion.Captcha);
                this.MessageStatus("Datos a posterar en la url  " + postData);

                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                byte[] bytes = encoding.GetBytes(postData);

                this.MessageStatus("Navegando a la url " + webSAT.Url.ToString());
                webSAT.Navigate(webSAT.Url, string.Empty, bytes, "Content-Type: application/x-www-form-urlencoded");

                #region ESPERA A QUE SE CARGUE LA PAGINA WEB Y SE PROCESE EL METODOD wbSAT_DocumentCompleted
                seProcesoWBSat = false;

                intentos = 0;
                while (true)
                {
                    Application.DoEvents();
                    if (!this.webSAT.IsBusy)
                    {
                        if (seProcesoWBSat)
                        {
                            break;
                        }

                        if (intentos > 100000)
                        {
                            this.Message("CONTRASEÑA CIEC ..... ");
                            this.MessageStatus("AVISO IMPORTANTE: CONTRAEÑA CIEC INCORRECTA  " + intentos.ToString());
                            break;
                        }
                        intentos = intentos + 1;
                    }
                }

                #endregion

            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        private void WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

            DateTime FI = this.Configuracion.FechaInicial;
            DateTime FF = this.Configuracion.FechaFinal;
            
            bool flag;
            bool flag1;

            if (this.webSAT.Document != null)
            {
                if (this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=en_US")
                                    || this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_MX")
                                    || this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_ES")
                                    || this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app?sid=0"))
                {
                    this.MessageStatus(string.Format("Usuario {0} Autentificado correctamente.", this.Configuracion.RFC));

                    if (this.Configuracion.Tipo == EnumDescargaTipo.Recibidos)
                    {
                        this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx");
                    }

                    if (this.Configuracion.Tipo == EnumDescargaTipo.Emitidos)
                    {
                        this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx");
                    }
                }

                if (this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app/login?sid=1&sid=1"))
                {
                    this.MessageStatus("Clave CIEC incorrecta, favor de verificar RFC y Clave.");
                    webSAT.Document.GetElementsByTagName("input").GetElementsByName("Ecom_User_ID")[0].SetAttribute("value", this.Configuracion.RFC.Trim().ToUpper());
                    webSAT.Document.GetElementsByTagName("input").GetElementsByName("Ecom_Password")[0].SetAttribute("value", this.Configuracion.CIEC.Trim());
                    webSAT.Document.GetElementById("jcaptcha").SetAttribute("value", this.Configuracion.Captcha);
                    webSAT.Document.GetElementById("submit").InvokeMember("click");
                }

                flag = (this.webSAT.Url != new Uri("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx") ? true : !(this.webSAT.Url != new Uri(this.currentURL)));
                if (!flag)
                {
                    #region Documentos Recibidos
                    for (DateTime fecha = FI; fecha <= FF; fecha = fecha.AddDays(1))
                    {
                        this.MessageStatus(string.Format("Procesando documentos recibidos de la fecha " + fecha.ToString("dd/MM/yyyy")));

                        int vecesQueSeHaLLamdoADocumentos = 0;
                        int totalArchivosDescargados = 0;
                        DateTime ultimaFechaEmision = DateTime.Now;
                        do
                        {
                            totalArchivosDescargados = 0;
                            vecesQueSeHaLLamdoADocumentos = vecesQueSeHaLLamdoADocumentos + 1;
                            this.GetReceivedDocuments(fecha.Year, fecha.Month, fecha.Day, ref totalArchivosDescargados, ref ultimaFechaEmision, vecesQueSeHaLLamdoADocumentos);
                        }
                        while (totalArchivosDescargados >= MaximoDescargaSAT);
                    }
                    #endregion
                    
                    this.FinishSession();
                }

                flag1 = (this.webSAT.Url != new Uri("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx") ? true : !(this.webSAT.Url != new Uri(this.currentURL)));
                if (!flag1)
                {
                    #region Documentos Enviados
                    for (DateTime fecha = FI; fecha <= FF; fecha = fecha.AddDays(1))
                    {
                        this.MessageStatus(string.Format("Procesando documentos enviados de la fecha." + fecha.ToString("dd/MM/yyyy")));

                        int vecesQueSeHaLLamdoADocumentos = 0;
                        int totalArchivosDescargados = 0;
                        DateTime ultimaFechaEmision = DateTime.Now;
                        do
                        {
                            totalArchivosDescargados = 0;
                            vecesQueSeHaLLamdoADocumentos = vecesQueSeHaLLamdoADocumentos + 1;
                            this.GetSendedDocuments(fecha.Year, fecha.Month, fecha.Day, ref totalArchivosDescargados, ref ultimaFechaEmision, vecesQueSeHaLLamdoADocumentos);
                        }
                        while (totalArchivosDescargados >= MaximoDescargaSAT);
                    }
                    #endregion
                    
                    this.FinishSession();
                }

                int timeout = 0;
                while (true)
                {
                    Application.DoEvents();
                    if (!this.webSAT.IsBusy)
                    {
                        break;
                    }
                    timeout = timeout + 1;
                    if (timeout > 40000) break;

                }
                this.currentURL = this.webSAT.Url.ToString();
            }
        }

        /// <summary>
        /// metodo para insertar consulta de los comprobantes emitidos
        /// </summary>
        private void GetReceivedDocuments(int year, int month, int day, ref int totalArchivosDescargados, ref DateTime ultimaFechaEmision, int vecesQueSeHaLLamadoAEsteMetodo)
        {
            bool flag1;
            bool flag2;
            try
            {
                if (this.webSAT.Document != null)
                {
                    #region Captura los datos de la pagina web
                    #region Selecciona el RdoFechas con ub CLICK
                    HtmlElement elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_RdoFechas");
                    if (elementById != null)
                    {
                        elementById.InvokeMember("click");
                    }

                    while (true)
                    {
                        Application.DoEvents();
                        HtmlElement htmlElement = this.webSAT.Document.GetElementById("DdlAnio");
                        flag1 = (htmlElement == null ? true : !(htmlElement.GetAttribute("disabled") != "disabled"));
                        if (!flag1)
                        {
                            break;
                        }
                    }
                    #endregion

                    #region Captura la FECHA del día
                    HtmlElement elementById1 = this.webSAT.Document.GetElementById("DdlAnio");
                    if (elementById1 != null)
                    {
                        elementById1.SetAttribute("value", year.ToString(CultureInfo.InvariantCulture));
                    }
                    HtmlElement htmlElement1 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMes");
                    if (htmlElement1 != null)
                    {
                        htmlElement1.SetAttribute("value", month.ToString("0"));
                    }
                    HtmlElement elementById2 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlDia");
                    if (elementById2 != null)
                    {
                        elementById2.SetAttribute("value", day.ToString("00"));
                    }
                    #endregion
                    // 

                    if (vecesQueSeHaLLamadoAEsteMetodo > 1)
                    {
                        #region Set atributo de la Hora Inicial
                        HtmlElement elementById4 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlHora");
                        if (elementById4 != null)
                        {
                            elementById4.SetAttribute("value", ultimaFechaEmision.Hour.ToString());
                        }
                        HtmlElement elementById5 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMinuto");
                        if (elementById5 != null)
                        {
                            elementById5.SetAttribute("value", ultimaFechaEmision.Minute.ToString());
                        }
                        HtmlElement elementById6 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlSegundo");
                        if (elementById6 != null)
                        {
                            elementById6.SetAttribute("value", ultimaFechaEmision.Second.ToString());
                        }
                        #endregion

                        if (false)
                        {
                            #region Set atributo de la Hora Final
                            HtmlElement elementById7 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlHoraFin");
                            if (elementById7 != null)
                            {
                                elementById7.SetAttribute("value", "14");
                            }
                            HtmlElement elementById8 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMinutoFin");
                            if (elementById8 != null)
                            {
                                elementById8.SetAttribute("value", "59");
                            }
                            HtmlElement elementById9 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlSegundoFin");
                            if (elementById9 != null)
                            {
                                elementById9.SetAttribute("value", "59");
                            }
                            #endregion
                        }
                    }

                    //if (gEstatus.Trim() != "Todos")
                    if (this.Configuracion.Status != EnumDescargaCFDIStatus.Todos)
                    {
                        #region Set atributo de Estatus
                        HtmlElement elementById10 = this.webSAT.Document.GetElementById("ctl00_MainContent_DdlEstadoComprobante");
                        if (elementById10 != null)
                        {
                            //if (gEstatus.Trim() == "Cancelados") elementById10.SetAttribute("value", "0");
                            //if (gEstatus.Trim() == "Vigentes") elementById10.SetAttribute("value", "1");
                            if (this.Configuracion.Status == EnumDescargaCFDIStatus.Cancelado) elementById10.SetAttribute("value", "0");
                            if (this.Configuracion.Status == EnumDescargaCFDIStatus.Vigente) elementById10.SetAttribute("value", "1");
                        }
                        #endregion
                    }

                    #region Se le da CLICK al btnBusqueda
                    HtmlElement htmlElement2 = this.webSAT.Document.GetElementById("ctl00_MainContent_BtnBusqueda");
                    if (htmlElement2 != null)
                    {
                        htmlElement2.InvokeMember("click");
                    }

                    int num = 0;
                    while (true)
                    {
                        Application.DoEvents();
                        HtmlElement elementById3 = this.webSAT.Document.GetElementById("ctl00_MainContent_UpdateProgress1");
                        if (!(elementById3 == null))
                        {
                            flag2 = (elementById3.Style != "display: none;" ? true : num <= 100000);
                            if (!flag2)
                            {
                                break;
                            }
                            num++;
                        }
                    }
                    #endregion
                    #endregion

                    this.ProcessResult(ref totalArchivosDescargados, ref ultimaFechaEmision, ref this.webSAT);
                }
            }
            catch (Exception exception)
            {
                this.Message("ERROR EN EL METODO GetRecivedDocuments: " + exception.Message);

                MessageBox.Show(exception.Message);
            }
        }

        private void GetSendedDocuments(int year, int month, int day, ref int TotalArchivosDescargados, ref DateTime UltimaFechaEmision, int VecesQueSeHaLLamadoAEsteMetodo)
        {
            HtmlElement elementById;
            bool flag;
            bool flag1;
            bool flag2;
            bool flag3;
            bool flag4;
            try
            {
                if (this.webSAT.Document != null)
                {
                    #region Selecciona el RdoFechas con un Click
                    HtmlElement htmlElement = this.webSAT.Document.GetElementById("ctl00_MainContent_RdoFechas");
                    if (htmlElement != null)
                    {
                        htmlElement.InvokeMember("click");
                    }
                    while (true)
                    {
                        flag = true;
                        Application.DoEvents();
                        HtmlElement elementById1 = this.webSAT.Document.GetElementById("ctl00_MainContent_TxtRfcReceptor");
                        flag1 = (elementById1 == null ? true : !(elementById1.GetAttribute("disabled") != "disabled"));
                        if (!flag1)
                        {
                            break;
                        }
                    }
                    #endregion

                    #region Capturamos la FECHA
                    DateTime dateTime = new DateTime(year, month, day);
                    HtmlElement htmlElement1 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_Calendario_text");
                    if (htmlElement1 != null)
                    {
                        htmlElement1.SetAttribute("value", dateTime.ToString("dd/MM/yyyy"));
                    }
                    HtmlElement elementById2 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_BtnFecha2");
                    if (elementById2 != null)
                    {
                        elementById2.InvokeMember("click");
                    }

                    while (true)
                    {
                        flag = true;
                        Application.DoEvents();
                        elementById = this.webSAT.Document.GetElementById("datepicker");
                        flag2 = (elementById == null ? true : !elementById.Style.Contains("visibility: visible"));
                        if (!flag2)
                        {
                            break;
                        }
                    }
                    #endregion

                    if (VecesQueSeHaLLamadoAEsteMetodo > 1)
                    {
                        #region Set atributo de la Hora Inicial
                        HtmlElement elementById4 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlHora");
                        if (elementById4 != null)
                        {
                            elementById4.SetAttribute("value", UltimaFechaEmision.Hour.ToString());
                        }
                        HtmlElement elementById5 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlMinuto");
                        if (elementById5 != null)
                        {
                            elementById5.SetAttribute("value", UltimaFechaEmision.Minute.ToString());
                        }
                        HtmlElement elementById6 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlSegundo");
                        if (elementById6 != null)
                        {
                            elementById6.SetAttribute("value", UltimaFechaEmision.Second.ToString());
                        }
                        #endregion

                        if (false)
                        {
                            #region Set atributo de la Hora Final
                            HtmlElement elementById7 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlHora");
                            if (elementById7 != null)
                            {
                                elementById7.SetAttribute("value", "14");
                            }
                            HtmlElement elementById8 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlMinuto");
                            if (elementById8 != null)
                            {
                                elementById8.SetAttribute("value", "59");
                            }
                            HtmlElement elementById9 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlSegundo");
                            if (elementById9 != null)
                            {
                                elementById9.SetAttribute("value", "59");
                            }
                            #endregion
                        }
                    }

                    //if (gEstatus.Trim() != "Todos")
                    if (this.Configuracion.Status != EnumDescargaCFDIStatus.Todos)
                    {
                        #region Set atributo de Estatus
                        HtmlElement elementById10 = this.webSAT.Document.GetElementById("ctl00_MainContent_DdlEstadoComprobante");
                        if (elementById10 != null)
                        {
                            //if (gEstatus.Trim() == "Cancelados") elementById10.SetAttribute("value", "0");
                            //if (gEstatus.Trim() == "Vigentes") elementById10.SetAttribute("value", "1");
                            if (this.Configuracion.Status == EnumDescargaCFDIStatus.Cancelado) elementById10.SetAttribute("value", "0");
                            if (this.Configuracion.Status == EnumDescargaCFDIStatus.Vigente) elementById10.SetAttribute("value", "1");
                        }
                        #endregion
                    }


                    IEnumerable<HtmlElement> htmlElements = ElementsByClass(this.webSAT.Document, "dpDayHighlightTD");
                    if (htmlElements != null)
                    {
                        htmlElements.First<HtmlElement>().InvokeMember("click");
                    }
                    while (true)
                    {
                        flag = true;
                        Application.DoEvents();
                        elementById = this.webSAT.Document.GetElementById("datepicker");
                        flag3 = (elementById == null ? true : elementById.Style.Contains("visibility: visible"));
                        if (!flag3)
                        {
                            break;
                        }
                    }

                    #region Dar CLICK en BUSQUEDA
                    HtmlElement htmlElement2 = this.webSAT.Document.GetElementById("ctl00_MainContent_BtnBusqueda");
                    if (htmlElement2 != null)
                    {
                        htmlElement2.InvokeMember("click");
                    }
                    int num = 0;
                    while (true)
                    {
                        flag = true;
                        Application.DoEvents();
                        HtmlElement elementById3 = this.webSAT.Document.GetElementById("ctl00_MainContent_UpdateProgress1");
                        if (!(elementById3 == null))
                        {
                            flag4 = (elementById3.Style != "display: none;" ? true : num <= 100000);
                            if (!flag4)
                            {
                                break;
                            }
                            num++;
                        }
                    }
                    #endregion

                    this.ProcessResult(ref TotalArchivosDescargados, ref UltimaFechaEmision, ref this.webSAT);
                }
            }
            catch (Exception exception)
            {
                this.Message("ERROR EN EL METODO GetSendedDocuments: " + exception.Message);
                MessageBox.Show(exception.Message);
            }
        }

        private static IEnumerable<HtmlElement> ElementsByClass(HtmlDocument doc, string className)
        {
            IEnumerable<HtmlElement> htmlElements = doc.All.Cast<HtmlElement>().Where<HtmlElement>((HtmlElement e) =>
            {
                bool attribute = e.GetAttribute("className") == className;
                return attribute;
            });
            return htmlElements;
        }

        private void FinishSession()
        {
            try
            {
                this.MessageStatus(string.Format("Cerrar sesion usuario {0}.", this.Configuracion.RFC.Trim()));
                this.webSAT.Navigate(new Uri("https://portalcfdi.facturaelectronica.sat.gob.mx/logout.aspx?salir=y"));
                this.Message("Proceso terminado.");
                seProcesoWBSat = true;
                //this.OnEndProcess(new DescargaMasivaCompletedProcess("El proceso concluyo satisfactoriamente!"));
            }
            catch (Exception exception)
            {
                this.Message("ERROR AL TRATAR DE TERMINAR LA SECION: " + exception.Message);
                throw new Exception(exception.Message);
            }
        }

        private void Message(string message)
        {
            try
            {
                gtxtLog.AppendText(string.Concat(message, "\n"));
            }
            catch (Exception exception)
            {
                System.Windows.Forms.MessageBox.Show(exception.Message);
            }
        }

        /// <summary>
        /// unicamente para establecer 
        /// </summary>
        private void MessageStatus(string menssage)
        {
            this.gtxtStatus.Text = menssage;
        }

        #endregion

        
    }
}
