﻿/// develop:
/// purpose: descarga masiva de comprobantes emitidos
using System;
using System.Linq;
using System.Windows.Forms;
using System.ComponentModel;
using Jaeger.Repositorio.Interface;
using Jaeger.Repositorio.V3.Entities;
using Jaeger.Repositorio.V3.Enums;

namespace Jaeger.Repositorio.V3.Helpers
{
    public class DescargaXml4SAT : DescargaXmlSATComun, IDescargaMasiva
    {
        
        /// <summary>
        /// constructor
        /// </summary>
        public DescargaXml4SAT(TextBox txtLog, TextBox txtStatus)
        {
            this.LbError = txtLog;
            this.LbMensaje = txtStatus;
            this.InicializeComponents();
            ModoDescarga = false;
        }

        #region metodos publicos

        public void Consultar(WebBrowser wsSat2, Configuracion conf)
        {
            this.webSAT = wsSat2;
            this.Configuracion = conf;
            this.webSAT.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this.WebBrowser_DocumentCompleted);
            this.Resultados = new BindingList<ViewModelDescargaResponse>();
            this.ListLapsosEmitidosNoDescargables.Clear();
            this.LlenarLista();
            this.IterarDiasEmitidos();
            this.TimerError.Enabled = true;
            this.ModoDescarga = false;
            this.Authenticate();
        }

        public void Reset()
        {
            this.TimerLogin.Enabled = false;
            this.TimerClose.Enabled = false;
            this.TimerError.Enabled = false;
            this.TimerCloseAlert.Enabled = false;
            this.TimerFiltrarRecibidas.Enabled = false;
            this.TimerFiltrarEmitidas.Enabled = false;
            this.TimerRecopilarRecibidas.Enabled = false;
            this.TimerRecopilarEmitidas.Enabled = false;
            this.TimerEvitarCierre.Enabled = false;
        }

        public void Registrar(ViewModelDescargaResponse objeto)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        #endregion

        #region metodos privados

        private void InicializeComponents()
        {
            // establecer los eventos de los objetos timer's
            this.TimerClose.Tick += new EventHandler(this.TimerClose_Tick);
            this.TimerError.Tick += new EventHandler(this.TimerError_Tick);
            this.TimerFiltrarEmitidas.Tick += new EventHandler(this.TimerFiltrarEmitidas_Tick);
            this.TimerLogin.Tick += new EventHandler(this.TimerLogin_Tick);
            this.TimerRecopilarEmitidas.Tick += new EventHandler(this.TimerRecopilarEmitidas_Tick);
        }

        private void Authenticate()
        {
            try
            {
                this.OnStartProcess(new DescargaMasivaStartProcess());
                this.AuthenticateComun();
            }
            catch (Exception ex)
            {
                this.OnCompletedProcess(new DescargaMasivaCompletedProcess("Autenticación", ex.Message, true));
                this.LbError.Text = ex.Message;
            }
        }

        private void ContinuarRevisandoEmitidos()
        {
            if (this.HayDiasPorRevisar(this.ListaFechasEmitidos))
            {
                this.IndexDiaEmitido = checked(this.IndexDiaEmitido + 1);
                this.IterarDiasEmitidos();
                this.TimerFiltrarEmitidas.Enabled = true;
                return;
            }

            this.LbMensaje.Text = "Iniciando descarga";
            this.LbError.Text = "";
            this.ErrorLog = "";
            this.TimerFiltrarEmitidas.Enabled = false;
            this.TimerRecopilarEmitidas.Enabled = false;
            this.Cerrando = false;
            this.IniciarDescarga();
        }
        
        private void NextRangos()
        {
            int count = checked(this.ListLapsosEmitidos.Count - 1);
            int num = 0;
            while (num <= count)
            {
                if (!(this.ListLapsosEmitidos[num].Status == Jaeger.Repositorio.V3.Enums.EnumDescargaLapsoStatus.Pendiente))
                {
                    num = checked(num + 1);
                }
                else
                {
                    this.IndexLapsoEmitido = num;
                    break;
                }
            }
            this.TimerRecopilarEmitidas.Enabled = false;
            this.TimerFiltrarEmitidas.Enabled = true;
        }

        private void IniciarDescarga()
        {
            this.OnProgressChanged(new DescargaMasivaProgreso(0, this.Resultados.Count((ViewModelDescargaResponse p) => p.Result == ""), 0, string.Concat(this.Resultados.Count((ViewModelDescargaResponse p) => p.Result == ""), " archivos para descargar")));
            this.ProcessDownloads();
            this.OnProgressChanged(new DescargaMasivaProgreso(100, this.Resultados.Count((ViewModelDescargaResponse p) => p.Result == "Completado"), 0, "Descarga terminada."));
        }

        private void CloseDescarga()
        {
            this.TimerLogin.Enabled = false;
            this.TimerFiltrarEmitidas.Enabled = false;
            this.TimerRecopilarEmitidas.Enabled = false;
            this.TimerClose.Enabled = false;
            
            this.Cerrando = false;
            MessageBox.Show("El RFC o contraseña son incorrectos. Verifique su información e inténtelo de nuevo.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (this.webSAT.Document != null && !this.Cerrando)
            {
                if (this.Configuracion.Tipo == Jaeger.Repositorio.V3.Enums.EnumDescargaTipo.Emitidos)
                {
                    try
                    {
                        
                        if (this.ModoDescarga)
                        {
                            return;
                        }
                        else
                        {
                            this.LbMensaje.Text = "Ingresando ...";
                            if (!this.UrlErroLogin(this.webSAT.Url.ToString()))
                            {
                                if (this.UrlLoad(this.UrlRedir))
                                {
                                    this.GoUrl(this.UrlAutentificacion);
                                }
                                if (this.UrlLoad(this.UrlError2))
                                {
                                    this.LoginScrapSAT();
                                    this.TimerLogin.Enabled = true;
                                }
                                if (this.webSAT.Url.ToString().Contains("https://portalcfdi.facturaelectronica.sat.gob.mx") & this.EstaEnSelector() && !this.TrucoHide)
                                {
                                    this.GoUrl(this.UrlOculta);
                                    this.TrucoHide = true;
                                }
                                if (this.webSAT.Url.ToString() == this.UrlOculta & this.EstaEnSelector())
                                {
                                    this.TrucoHide = false;
                                    if (this.Configuracion.Tipo == EnumDescargaTipo.Emitidos)
                                    {
                                        this.ClickEnSelector(EnumDescargaTipo.Emitidos);
                                    }
                                }
                                if (this.webSAT.Url.ToString() == this.UrlEmitidas & this.Configuracion.Tipo == EnumDescargaTipo.Emitidos | this.webSAT.Url.ToString() == this.UrlRecibidas)
                                {
                                    this.GetElementHtmlById("ctl00_MainContent_RdoFechas", ref this.webSAT).InvokeMember("click");
                                    this.TimerLogin.Enabled = false;
                                    if (!this.Truco)
                                    {
                                        this.TimerFiltrarEmitidas.Enabled = true;
                                    }
                                }
                                if (this.webSAT.Url.ToString().Contains("https://cfdiau.sat.gob.mx/nidp/wsfed_redir_cont_portalcfdi.jsp?wa="))
                                {
                                    this.GoUrl(this.UrlAutentificacion);
                                    return;
                                }
                                else if (this.webSAT.Url.ToString() == this.UrlFailCaptcha)
                                {
                                    this.LoginScrapSAT();
                                    this.TimerLogin.Enabled = true;
                                }

                                if (this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app?sid=0"))
                                {
                                    if (this.Configuracion.Tipo == EnumDescargaTipo.Recibidos)
                                    {
                                        this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx");
                                    }

                                    if (this.Configuracion.Tipo == EnumDescargaTipo.Emitidos)
                                    {
                                        this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx");
                                    }
                                }

                                if (this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=en_US")
                                    || this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_MX")
                                    || this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_ES")
                                    || this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app?sid=0"))
                                {
                                    if (this.Configuracion.Tipo == EnumDescargaTipo.Recibidos)
                                    {
                                        this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx");
                                    }

                                    if (this.Configuracion.Tipo == EnumDescargaTipo.Emitidos)
                                    {
                                        this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx");
                                    }
                                }
                            }
                            else
                            {
                                this.GoUrl(this.UrlError1);
                                return;
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(exception.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                if (this.Configuracion.Tipo == Jaeger.Repositorio.V3.Enums.EnumDescargaTipo.Recibidos)
                {
                    if (this.ModoDescarga)
                    {
                        return;
                    }
                    if (this.webSAT.Url.ToString() == this.UrlOculta & this.EstaEnSelector())
                    {
                        this.TrucoHide = false;
                        this.ClickEnSelector(this.Configuracion.Tipo);
                    }
                    if (this.webSAT.Url.ToString() == this.UrlRecibidas & this.Configuracion.Tipo == Jaeger.Repositorio.V3.Enums.EnumDescargaTipo.Recibidos | this.webSAT.Url.ToString() == this.UrlRecibidas)
                    {
                        this.GetElementHtmlById("ctl00_MainContent_RdoFechas", ref this.webSAT).InvokeMember("click");
                        this.TimerLogin.Enabled = false;
                        if (!this.Truco)
                        {
                            this.TimerFiltrarRecibidas.Enabled = true;
                        }
                    }
                }
            }
            while (true)
            {
                Application.DoEvents();
                if (!this.webSAT.IsBusy)
                {
                    break;
                }
                if (this.webSAT.ReadyState == WebBrowserReadyState.Loading)
                {
                    Application.DoEvents();
                }
            }
        }

        #region timers

        private void TimerLogin_Tick(object sender, EventArgs e)
        {
            if (this.CredencialesNoValidas() | this.ErrroDeCredenciales())
            {
                this.CloseDescarga();
            }
        }

        private void TimerFiltrarEmitidas_Tick(object sender, EventArgs e)
        {
            bool enabled = false;
            HtmlElement elementById = null;
            elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_TxtRfcReceptor");
            if (elementById != null)
            {
                enabled = elementById.Enabled;
            }
            if (!this.ExisteUpdateProgres() & enabled)
            {
                this.TimerFiltrarEmitidas.Enabled = false;
                this.GetElementHtmlById("ctl00_MainContent_CldFechaInicial2_Calendario_text", ref this.webSAT).InnerText = this.ListLapsosEmitidos[this.IndexLapsoEmitido].Fecha.ToShortDateString();
                this.GetElementHtmlById("ctl00_MainContent_CldFechaFinal2_Calendario_text", ref this.webSAT).InnerText = this.ListLapsosEmitidos[this.IndexLapsoEmitido].Fecha.ToShortDateString();
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlHora");
                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosEmitidos[this.IndexLapsoEmitido].SegundoInicial)[1]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlMinuto");
                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosEmitidos[this.IndexLapsoEmitido].SegundoInicial)[2]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlSegundo");
                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosEmitidos[this.IndexLapsoEmitido].SegundoInicial)[3]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlHora");
                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosEmitidos[this.IndexLapsoEmitido].SegundoFinal)[1]);

                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlMinuto");
                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosEmitidos[this.IndexLapsoEmitido].SegundoFinal)[2]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlSegundo");
                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosEmitidos[this.IndexLapsoEmitido].SegundoFinal)[3]);
                }
                if (this.Configuracion.FiltrarPorRFC == true)
                {
                    this.GetElementHtmlById("ctl00_MainContent_TxtRfcReceptor", ref this.webSAT).SetAttribute("value", this.Configuracion.BuscarPorRFC);
                }
                this.GetElementHtmlById("ctl00_MainContent_DdlEstadoComprobante", ref this.webSAT).SetAttribute("value", this.Configuracion.Status.ToString());
                this.GetElementHtmlById("ddlComplementos", ref this.webSAT).SetAttribute("value", this.Configuracion.Complemento.ToString());
                this.GetElementHtmlById("ctl00_MainContent_BtnBusqueda", ref this.webSAT).InvokeMember("click");
                this.LbMensaje.Text = string.Concat("Filtrando día ", this.ListLapsosEmitidos[this.IndexLapsoEmitido].Fecha.ToShortDateString(), "...");
                this.TimerRecopilarEmitidas.Enabled = true;
            }
        }

        private void TimerRecopilarEmitidas_Tick(object sender, EventArgs e)
        {
            if (!this.ExisteUpdateProgres() & this.webSAT.ReadyState == WebBrowserReadyState.Complete & !this.webSAT.IsBusy)
            {
                this.TimerRecopilarEmitidas.Enabled = false;
                this.LbMensaje.Text = "Recopilando XML ...";
                if (this.GetCuantosXML() <= 0)
                {
                    this.ListLapsosEmitidos[this.IndexLapsoEmitido].Status = Jaeger.Repositorio.V3.Enums.EnumDescargaLapsoStatus.Vacio;
                    this.LbMensaje.Text = "Periodo vacío";
                }
                else if (!this.GetLimiteDe500())
                {
                    this.ListLapsosEmitidos[this.IndexLapsoEmitido].Status = Jaeger.Repositorio.V3.Enums.EnumDescargaLapsoStatus.Ok;
                    DateTime ahora = DateTime.Now;
                    int countXml = 0;
                    this.ProcessResult(ref countXml, ref this.webSAT);
                    //this.ProcessResult(ref countXML, ref ahora, ref this.webSAT);
                    this.ListaFechasEmitidos[this.IndexDiaEmitido].Total = this.ListaFechasEmitidos[this.IndexDiaEmitido].Total + countXml;
                    this.ListLapsosEmitidos[this.IndexLapsoEmitido].Cuantos = countXml;
                    this.LbMensaje.Text = string.Format("Disponibles: {0} xml's; Recopilados: {1}", countXml, this.Resultados.Count);
                    this.LbError.Text = "";
                }
                else
                {
                    this.LbMensaje.Text = "Creando Lapsos ...";
                    this.ListLapsosEmitidos[this.IndexLapsoEmitido].Status = Jaeger.Repositorio.V3.Enums.EnumDescargaLapsoStatus.X;
                    this.CrearLapsosEmitidos(this.IndexLapsoEmitido);
                }

                if (this.HayRangosPorRevisar(ListLapsosEmitidos))
                {
                    this.NextRangos();
                    return;
                }
                this.ContinuarRevisandoEmitidos();
            }
        }

        private void TimerClose_Tick(object sender, EventArgs e)
        {
            this.LbMensaje.Text = "Cerrando sesión ...";
            
            if (this.GetNavegadorCerrado())
            {
                try
                {
                    this.webSAT.Document.ExecCommand("ClearAuthenticationCache", false, null);
                    this.OnCompletedProcess(new DescargaMasivaCompletedProcess("El proceso concluyo satisfactoriamente!", ""));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    LbError.Text = "TimerClose: " + ex.Message;
                }
                this.TimerClose.Enabled = false;
                this.TimerError.Enabled = false;
                this.TimerFiltrarEmitidas.Enabled = false;
                this.TimerLogin.Enabled = false;
                this.LbMensaje.Text = "Sesión Terminada ...";
                this.Cerrando = false;
            }
        }

        private void TimerError_Tick(object sender, EventArgs e)
        {
            if (this.ErrroDeAutentificacion())
            {
                this.TimerError.Enabled = false;
                this.GoUrl(this.UrlAutentificacion);
            }
        }

        #endregion

        #endregion
    }
}
