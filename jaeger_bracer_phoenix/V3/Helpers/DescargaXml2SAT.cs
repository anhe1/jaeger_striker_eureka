﻿// develop: anhe
// purpose: descarga masiva de comprobantes fiscales recibidos
using System;
using System.Linq;
using System.Windows.Forms;
using Jaeger.Repositorio.Helpers;
using Jaeger.Repositorio.Interface;
using Jaeger.Repositorio.V3.Entities;
using Jaeger.Repositorio.V3.Enums;

namespace Jaeger.Repositorio.V3.Helpers
{
    public class DescargaXml2SAT : DescargaXmlSATComun, IDescargaMasiva
    {
        
        private readonly Timer timerCloseAlert = new Timer() { Interval = 500, Enabled = false };
        
        /// <summary>
        /// constructor
        /// </summary>
        public DescargaXml2SAT(TextBox txtLog, TextBox txtStatus)
        {
            this.LbError = txtLog;
            this.LbMensaje = txtStatus;
            this.InicializeComponents();
            this.ModoDescarga = false;
        }

        #region metodos publicos

        public void Consultar(WebBrowser wsSat2, Configuracion conf)
        {
            this.webSAT = wsSat2;
            this.Configuracion = conf;
            this.webSAT.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this.WebBrowser_DocumentCompleted);
            this.ListLapsosRecibidosNoDescargables.Clear();
            this.LlenarLista();
            this.IterarDiasRecibidos();
            this.TimerError.Enabled = true;
            this.ModoDescarga = false;
            this.Authenticate();
        }

        public void Reset()
        {
            this.TimerLogin.Enabled = false;
            this.TimerClose.Enabled = false;
            this.TimerError.Enabled = false;
            this.TimerCloseAlert.Enabled = false;
            this.TimerFiltrarRecibidas.Enabled = false;
            this.TimerFiltrarEmitidas.Enabled = false;
            this.TimerRecopilarRecibidas.Enabled = false;
            this.TimerRecopilarEmitidas.Enabled = false;
            this.TimerEvitarCierre.Enabled = false;
        }

        public void Registrar(ViewModelDescargaResponse objeto)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        #endregion

        #region metodos privados

        public void InicializeComponents()
        {
            // establecer los eventos de los objetos timer's
            this.TimerClose.Tick += new EventHandler(this.TimerClose_Tick);
            this.TimerError.Tick += new EventHandler(this.TimerError_Tick);
            this.TimerRecopilarRecibidas.Tick += new EventHandler(this.TimerRecopilarRecibidas_Tick);
            this.TimerLogin.Tick += new EventHandler(this.TimerLogin_Tick);
            this.timerCloseAlert.Tick += new EventHandler(this.TimerCloseAlert_Tick);
            this.TimerFiltrarRecibidas.Tick += new EventHandler(this.TimerFiltrarRecibidas_Tick);
        }

        private void Authenticate()
        {
            try
            {
                this.OnStartProcess(new DescargaMasivaStartProcess());
                this.AuthenticateComun();
            }
            catch (Exception ex)
            {
                this.OnCompletedProcess(new DescargaMasivaCompletedProcess("Autenticación", ex.Message, true));
                this.LbError.Text = ex.Message;
            }
        }

        private void ContinuarRevisando()
        {
            if (this.HayDiasPorRevisar(this.ListaFechasRecibidos))
            {
                this.IndexDiaRecibido = checked(this.IndexDiaRecibido + 1);
                this.IterarDiasRecibidos();
                this.TimerFiltrarRecibidas.Enabled = true;
                return;
            }

            this.LbMensaje.Text = "Iniciando descarga";
            this.LbError.Text = "";
            this.ErrorLog = "";
            this.TimerFiltrarRecibidas.Enabled = false;
            this.TimerRecopilarRecibidas.Enabled = false;
            this.Cerrando = false;
            this.IniciarDescarga();
        }

        private void NextRangos()
        {
            int count = checked(this.ListLapsosRecibidos.Count - 1);
            int num = 0;
            while (num <= count)
            {
                if (!(this.ListLapsosRecibidos[num].Status == EnumDescargaLapsoStatus.Pendiente))
                {
                    num = checked(num + 1);
                }
                else
                {
                    this.IndexLapsoRecibido = num;
                    break;
                }
            }
            this.TimerRecopilarRecibidas.Enabled = false;
            this.TimerFiltrarRecibidas.Enabled = true;
        }

        private void IniciarDescarga()
        {
            this.OnProgressChanged(new DescargaMasivaProgreso(0, this.Resultados.Count((ViewModelDescargaResponse p) => p.Result == ""), 0, string.Concat(this.Resultados.Count((ViewModelDescargaResponse p) => p.Result == ""), " archivos para descargar")));
            this.ProcessDownloads();
            this.OnProgressChanged(new DescargaMasivaProgreso(100, this.Resultados.Count((ViewModelDescargaResponse p) => p.Result == "Completado"), 0, "Descarga terminada."));
        }

        private void CloseDescarga()
        {
            this.TimerLogin.Enabled = false;
            this.TimerFiltrarRecibidas.Enabled = false;
            this.TimerRecopilarRecibidas.Enabled = false;
            this.TimerClose.Enabled = false;
            this.Cerrando = false;
            this.LbMensaje.Text = "Error en la sesion";
        }

        private void WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (this.webSAT.Document != null && !this.Cerrando)
            {
                try
                {
                    if (ModoDescarga)
                    {
                        return;
                    }
                    else
                    {
                        this.LbMensaje.Text = "Ingresando ...";
                        if (!this.UrlErroLogin(this.webSAT.Url.ToString()))
                        {
                            if (this.UrlLoad(this.UrlRedir))
                            {
                                this.GoUrl(this.UrlAutentificacion);
                            }

                            if (this.UrlLoad(this.UrlError2))
                            {
                                this.LoginScrapSAT();
                                this.TimerLogin.Enabled = true;
                            }

                            if (this.webSAT.Url.ToString().Contains("https://portalcfdi.facturaelectronica.sat.gob.mx") & this.EstaEnSelector() && !this.TrucoHide)
                            {
                                this.GoUrl(this.UrlOculta);
                                this.TrucoHide = true;
                            }

                            if (this.webSAT.Url.ToString() == this.UrlOculta & this.EstaEnSelector())
                            {
                                this.TrucoHide = false;
                                this.ClickEnSelector(this.Configuracion.Tipo);
                                if (this.Configuracion.Tipo == EnumDescargaTipo.Recibidos)
                                {
                                    this.ClickEnSelector(EnumDescargaTipo.Recibidos);
                                }
                            }

                            if (this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app?sid=0"))
                            {
                                if (this.Configuracion.Tipo == EnumDescargaTipo.Recibidos)
                                {
                                    this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx");
                                }

                                if (this.Configuracion.Tipo == EnumDescargaTipo.Emitidos)
                                {
                                    this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx");
                                }
                            }

                            if (this.webSAT.Url.ToString() == this.UrlRecibidas && this.Configuracion.Tipo == EnumDescargaTipo.Recibidos | this.webSAT.Url.ToString() == this.UrlRecibidas)
                            {
                                this.GetElementHtmlById("ctl00_MainContent_RdoFechas", ref this.webSAT).InvokeMember("click");
                                this.TimerLogin.Enabled = false;
                                if (!this.Truco)
                                {
                                    this.TimerFiltrarRecibidas.Enabled = true;
                                }
                            }

                            if (this.webSAT.Url.ToString().Contains("https://cfdiau.sat.gob.mx/nidp/wsfed_redir_cont_portalcfdi.jsp?wa="))
                            {
                                this.GoUrl(this.UrlAutentificacion);
                                return;
                            }
                            else if (this.webSAT.Url.ToString() == this.UrlFailCaptcha)
                            {
                                this.LoginScrapSAT();
                                this.TimerLogin.Enabled = true;
                            }

                            if (this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=en_US")
                                || this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_MX")
                                || this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_ES")
                                || this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app?sid=0")
                                )
                                //if (this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_MX"))
                            {
                                if (this.Configuracion.Tipo == EnumDescargaTipo.Recibidos)
                                {
                                    this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx");
                                }

                                if (this.Configuracion.Tipo == EnumDescargaTipo.Emitidos)
                                {
                                    this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx");
                                }
                            }
                        }
                        else
                        {
                             this.GoUrl(this.UrlError1);
                             return;
                        }
                    }
                }
                catch (Exception exception)
                {
                    LogErrorService.LogWrite("WebBrowser_DocumentCompleted");
                    LogErrorService.LogWrite(this.webSAT.Url.ToString());
                    //LogErrorService.LogWrite(this.webSAT.Document.Body.InnerHtml.ToString());
                    Console.WriteLine("webBrowserCompleted: " + exception.Message);
                    LbError.Text = "webBrowserCompleted: " + exception.Message;
                }
            }
            while (true)
            {
                Application.DoEvents();
                if (!this.webSAT.IsBusy)
                {
                    break;
                }
                if (this.webSAT.ReadyState == WebBrowserReadyState.Loading)
                {
                    Application.DoEvents();
                }
            }
        }

        #region timers

        private void TimerLogin_Tick(object sender, EventArgs e)
        {
            if (this.CredencialesNoValidas() | this.ErrroDeCredenciales())
            {
                this.CloseDescarga();
            }
        }

        private void TimerFiltrarRecibidas_Tick(object sender, EventArgs e)
        {
            bool enabled = false;
            HtmlElement elementById = null;
            elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_TxtRfcReceptor");
            if (elementById != null)
            {
                enabled = elementById.Enabled;
            }
            if (!this.ExisteUpdateProgres() & enabled)
            {
                this.TimerFiltrarRecibidas.Enabled = false;
                // introducir los datos de la fecha
                this.GetElementHtmlById("DdlAnio", ref this.webSAT).SetAttribute("value", this.ListaFechasRecibidos[this.IndexDiaRecibido].Fecha.Year.ToString(System.Globalization.CultureInfo.InvariantCulture));
                this.GetElementHtmlById("ctl00_MainContent_CldFecha_DdlMes", ref this.webSAT).SetAttribute("value", this.ListaFechasRecibidos[this.IndexDiaRecibido].Fecha.Month.ToString("0"));
                this.GetElementHtmlById("ctl00_MainContent_CldFecha_DdlMes", ref this.webSAT).InvokeMember("Change");
                this.GetElementHtmlById("ctl00_MainContent_CldFecha_DdlDia", ref this.webSAT).SetAttribute("value", this.ListaFechasRecibidos[this.IndexDiaRecibido].Fecha.Day.ToString("00"));
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlHora");

                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosRecibidos[this.IndexLapsoRecibido].SegundoInicial)[1]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMinuto");

                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosRecibidos[this.IndexLapsoRecibido].SegundoInicial)[2]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlSegundo");

                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosRecibidos[this.IndexLapsoRecibido].SegundoInicial)[3]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlHoraFin");

                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosRecibidos[this.IndexLapsoRecibido].SegundoFinal)[1]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMinutoFin");

                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosRecibidos[this.IndexLapsoRecibido].SegundoFinal)[2]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlSegundoFin");

                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosRecibidos[this.IndexLapsoRecibido].SegundoFinal)[3]);
                }

                if (this.Configuracion.FiltrarPorRFC == true)
                {
                    this.GetElementHtmlById("ctl00_MainContent_TxtRfcReceptor", ref this.webSAT).SetAttribute("value", this.Configuracion.BuscarPorRFC);
                }

                //this.GetElementHtmlById("ctl00_MainContent_DdlEstadoComprobante").SetAttribute("value", this.configuracion.Status);

                if (this.Configuracion.Status == EnumDescargaCFDIStatus.Cancelado) this.GetElementHtmlById("ctl00_MainContent_DdlEstadoComprobante", ref this.webSAT).SetAttribute("value", "0");
                if (this.Configuracion.Status == EnumDescargaCFDIStatus.Vigente) this.GetElementHtmlById("ctl00_MainContent_DdlEstadoComprobante", ref this.webSAT).SetAttribute("value", "1");

                this.GetElementHtmlById("ddlComplementos", ref this.webSAT).SetAttribute("value", this.Configuracion.Complemento);
                this.GetElementHtmlById("ctl00_MainContent_BtnBusqueda", ref this.webSAT).InvokeMember("click");
                this.LbMensaje.Text = string.Concat("Filtrando día ", this.ListLapsosRecibidos[this.IndexLapsoRecibido].Fecha.ToShortDateString(), "...");
                this.TimerRecopilarRecibidas.Enabled = true;
            }
        }

        private void TimerRecopilarRecibidas_Tick(object sender, EventArgs e)
        {
            if (!this.ExisteUpdateProgres() & this.webSAT.ReadyState == WebBrowserReadyState.Complete & !this.webSAT.IsBusy)
            {
                this.TimerRecopilarRecibidas.Enabled = false;
                this.LbMensaje.Text = "Recopilando XML ...";

                if (this.GetCuantosXML2() <= 0)
                {
                    this.ListLapsosRecibidos[this.IndexLapsoRecibido].Status = EnumDescargaLapsoStatus.Vacio;
                    this.LbMensaje.Text = "Periodo vacío";
                }
                else if (!this.GetLimiteDe500())
                {
                    this.ListLapsosRecibidos[this.IndexLapsoRecibido].Status = EnumDescargaLapsoStatus.Ok;
                    DateTime ahora = DateTime.Now;
                    int countXml = 0;
                    this.ProcessResult(ref countXml, ref this.webSAT);
                    //this.ProcessResult(ref countXML, ref ahora, ref this.webSAT);
                    this.ListaFechasRecibidos[this.IndexDiaRecibido].Total = this.ListaFechasRecibidos[this.IndexDiaRecibido].Total + countXml;
                    this.ListLapsosRecibidos[this.IndexLapsoRecibido].Cuantos = countXml;
                    this.LbMensaje.Text = string.Format("Disponibles: {0} xml's; Recopilados: {1}", countXml, this.Resultados.Count);
                    this.LbError.Text = "";
                }
                else
                {
                    this.LbMensaje.Text = "Creando Lapsos ...";
                    this.ListLapsosRecibidos[this.IndexLapsoRecibido].Status = EnumDescargaLapsoStatus.X;
                    this.CrearLapsosRecibidos(this.IndexLapsoRecibido);
                }

                if (this.HayRangosPorRevisar(this.ListLapsosRecibidos))
                {
                    this.NextRangos();
                    return;
                }
                this.ContinuarRevisando();
            }
        }

        private void TimerClose_Tick(object sender, EventArgs e)
        {
            this.LbMensaje.Text = "Cerrando sesión ...";

            if (this.GetNavegadorCerrado())
            {
                try
                {
                    this.webSAT.Document.ExecCommand("ClearAuthenticationCache", false, null);
                    this.OnCompletedProcess(new DescargaMasivaCompletedProcess("El proceso concluyo satisfactoriamente!", ""));
                }
                catch (Exception ex)
                {
                    Console.WriteLine("TimerClose: " + ex.Message);
                    LbError.Text = "TimerClose: " + ex.Message;
                }
                this.TimerClose.Enabled = false;
                this.timerCloseAlert.Enabled = false;
                this.TimerError.Enabled = false;
                this.TimerError.Dispose();
                this.TimerFiltrarRecibidas.Enabled = false;
                this.TimerLogin.Enabled = false;
                this.LbMensaje.Text = "Sesión Terminada ...";
                this.Cerrando = false;
            }
        }

        private void TimerCloseAlert_Tick(object sender, EventArgs e)
        {
            if ((long)FindWindow(null, "Mensaje de página web") != 0)
            {
                SendKeys.SendWait("{Enter}");
                this.timerCloseAlert.Enabled = false;
            }
        }

        private void TimerError_Tick(object sender, EventArgs e)
        {
            if (this.ErrroDeAutentificacion())
            {
                this.TimerError.Enabled = false;
                this.GoUrl(this.UrlAutentificacion);
            }
        }

        #endregion

        #endregion

    }
}
