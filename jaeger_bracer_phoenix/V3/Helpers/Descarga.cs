﻿using System;
using System.Net;
using System.Linq;
using System.IO;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Jaeger.Repositorio.Helpers;
using Jaeger.Repositorio.V3.Entities;

namespace Jaeger.Repositorio.V3.Helpers
{
    public class Descarga
    {
        public static bool DownloadFile(string address, string fileName, string urlweb)
        {
            bool blnResult = false;
            try
            {
                string cookie = CookieReader.GetCookie(urlweb);
                WebClient webClient = new WebClient();
                if (!File.Exists(fileName))
                {
                    webClient.Headers.Add(HttpRequestHeader.Cookie, cookie);
                    Stream fileStream = new FileStream(fileName, FileMode.Create);
                    fileStream.Write(webClient.DownloadData(address), 0, webClient.DownloadData(address).Length);
                    fileStream.Flush();
                    fileStream.Close();
                    blnResult = true;
                }
                else
                {
                    blnResult = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                blnResult = false;
            }
            return blnResult;
        }

        public static string DownloadFile(string address, string urlweb)
        {
            string base64String = "";
            if (ValidaUrl(address))
            {
                byte[] downloadFile = null;
                try
                {
                    string cookie = CookieReader.GetCookie(urlweb);
                    WebClient webClient = new WebClient();
                    webClient.Headers.Add(HttpRequestHeader.Cookie, cookie);
                    downloadFile = webClient.DownloadData(address);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("DownloadFile: " + ex.Message);
                    downloadFile = null;
                }
                finally
                {
                    if (downloadFile != null)
                    {
                        base64String = Convert.ToBase64String(downloadFile);
                    }
                }
                
            }
            return base64String;
        }

        public static bool WriteFileFromB64(string base64, string archivo)
        {
            bool result = false;
            byte[] numArray;

            if ((base64 == null ? true : base64.Length == 0))
            {
                numArray = null;
            }
            else
            {
                try
                {
                    numArray = Convert.FromBase64String(base64);
                }
                catch (FormatException fex)
                {
                    throw new FormatException(string.Concat("The provided string does not appear to be Base64 encoded:", Environment.NewLine, base64, Environment.NewLine), fex);
                }
            }

            if (numArray != null)
            {
                string directorio = Path.GetDirectoryName(archivo);

                if (!Directory.Exists(directorio))
                {
                    Directory.CreateDirectory(directorio);
                }

                try
                {
                    using (FileStream fileStream = File.Create(archivo))
                    {
                        fileStream.Write(numArray, 0, checked(numArray.Length));
                        fileStream.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return result;
        }

        public static async Task<BindingList<ViewModelDescargaResponse>> Run(BindingList<ViewModelDescargaResponse> listfiles, string urlWeb, IProgress<DescargaMasivaProgreso> proceso)
        {
            BindingList<ViewModelDescargaResponse> output = new BindingList<ViewModelDescargaResponse>();
            int contador = 0;
            await Task.Run(() =>
            {
                Parallel.ForEach<ViewModelDescargaResponse>(listfiles, (comprobante) =>
                {
                    ViewModelDescargaResponse results = Des(comprobante, urlWeb);
                    output.Add(results);
                    contador = contador+1;
                    proceso.Report(new DescargaMasivaProgreso((output.Count * 100) / listfiles.Count, listfiles.Count, contador, "Descargando "));
                });
            });

            return output;
        }

        public static ViewModelDescargaResponse Des(ViewModelDescargaResponse item, string urlweb)
        {
            item.XmlContentB64 = Descarga.DownloadFile(item.UrlXML, urlweb);
            item.PdfRepresentacionImpresaB64 = Descarga.DownloadFile(item.UrlPDF, urlweb);
            item.PdfAcuseB64 = Descarga.DownloadFile(item.PdfAcuseB64, urlweb);
            item.PdfAcuseFinalB64 = Descarga.DownloadFile(item.UrlRecuperaAcuseFinal, urlweb);
            return item;
        }

        public static bool ValidaUrl(string sUrl)
        {
            if (sUrl == null)
            {
                return false;
            }
            else
            {
                if ((new Regex("^(https?|ftp|file)://[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|]", RegexOptions.IgnoreCase)).Match(sUrl).Captures.Count == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
