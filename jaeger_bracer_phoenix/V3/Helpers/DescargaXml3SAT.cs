﻿// develop:
// purpose: clase para descarga masiva de comprobantes, para emitidos y recibidos;
using System;
using System.Linq;
using System.Windows.Forms;
using Jaeger.Repositorio.Interface;
using Jaeger.Repositorio.V3.Entities;
using Jaeger.Repositorio.V3.Enums;

namespace Jaeger.Repositorio.V3.Helpers
{
    public class DescargaXml3SAT : DescargaXmlSATComun, IDescargaMasiva
    {
        /// <summary>
        /// constructor
        /// </summary>
        public DescargaXml3SAT(TextBox txtLog, TextBox txtStatus)
        {
            this.LbMensaje = txtLog;
            this.LbError = txtStatus;
            this.InicializeComponents();
            ModoDescarga = false;
        }

        #region metodos publicos

        public void Consultar(WebBrowser _wsSat2, Configuracion conf)
        {
            this.Configuracion = conf;
            this.webSAT = _wsSat2;
            this.webSAT.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this.WebBrowser_DocumentCompleted);
            this.Configuracion.Tipo = Jaeger.Repositorio.V3.Enums.EnumDescargaTipo.Emitidos;
            this.LlenarLista();
            this.IterarDiasEmitidos();
            
            this.TimerError.Enabled = true;
            this.Authenticate();
        }

        public void Reset()
        {
            this.TimerLogin.Enabled = false;
            this.TimerClose.Enabled = false;
            this.TimerError.Enabled = false;
            this.TimerCloseAlert.Enabled = false;
            this.TimerFiltrarRecibidas.Enabled = false;
            this.TimerFiltrarEmitidas.Enabled = false;
            this.TimerRecopilarRecibidas.Enabled = false;
            this.TimerRecopilarEmitidas.Enabled = false;
            this.TimerEvitarCierre.Enabled = false;
        }

        public void Registrar(ViewModelDescargaResponse objeto)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        #endregion

        #region metodos privados

        private void InicializeComponents()
        {
            this.TimerClose.Tick += new EventHandler(this.TimerClose_Tick);
            
            this.TimerError.Tick += new EventHandler(this.TimerError_Tick);
            this.TimerFiltrarEmitidas.Tick += new EventHandler(this.TimerFiltrarEmitidas_Tick);
            this.TimerFiltrarRecibidas.Tick += new EventHandler(this.TimerFiltrarRecibidas_Tick);
            this.TimerLogin.Tick += new EventHandler(this.TimerLogin_Tick);
            this.TimerRecopilarEmitidas.Tick += new EventHandler(this.TimerRecopilarEmitidas_Tick);
            this.TimerRecopilarRecibidas.Tick += new EventHandler(this.TimerRecopilarRecibidas_Tick);
            
        }

        private void ContinuarRevisandoEmitidos()
        {
            if (this.HayDiasPorRevisar(this.ListaFechasEmitidos))
            {
                this.IndexDiaEmitido = checked(this.IndexDiaEmitido + 1);
                this.IterarDiasEmitidos();
                this.TimerFiltrarEmitidas.Enabled = true;
                return;
            }
            
            this.LbError.Text = "";
            this.LbMensaje.Text = "Descargar Recibidos";
            this.TimerFiltrarEmitidas.Enabled = false;
            this.TimerRecopilarEmitidas.Enabled = false;
            this.ErrorLog = "";
            this.Cerrando = false;
            this.Configuracion.Tipo = Jaeger.Repositorio.V3.Enums.EnumDescargaTipo.Recibidos;
            this.IterarDiasRecibidos();
            this.GoUrl(this.UrlOculta);
        }

        private void ContinuarRevisandoRecibidos()
        {
            if (this.HayDiasPorRevisar(ListaFechasRecibidos))
            {
                this.IndexDiaRecibido = checked(this.IndexDiaRecibido + 1);
                this.IterarDiasRecibidos();
                this.TimerFiltrarRecibidas.Enabled = true;
                return;
            }
            
            this.LbMensaje.Text = "Iniciar Descarga...";
            this.LbError.Text = "";
            this.TimerFiltrarRecibidas.Enabled = false;
            this.TimerRecopilarRecibidas.Enabled = false;
            this.ErrorLog = "";
            this.IniciarDescarga();
        }
        
        private void IniciarDescarga()
        {
            this.OnProgressChanged(new DescargaMasivaProgreso(0, this.Resultados.Count((ViewModelDescargaResponse p) => p.Result == ""),0, string.Concat(this.Resultados.Count((ViewModelDescargaResponse p) => p.Result == ""), " archivos para descargar")));
            this.ProcessDownloads();
            this.OnProgressChanged(new DescargaMasivaProgreso(100, this.Resultados.Count((ViewModelDescargaResponse p) => p.Result == "Completado"), 0, "Descarga terminada."));
        }

        private void NextLapsoEmitido()
        {
            int count = checked(this.ListLapsosEmitidos.Count - 1);
            int num = 0;
            while (num <= count)
            {
                if (!(this.ListLapsosEmitidos[num].Status == Jaeger.Repositorio.V3.Enums.EnumDescargaLapsoStatus.Pendiente))
                {
                    num = checked(num + 1);
                }
                else
                {
                    this.IndexLapsoEmitido = num;
                    break;
                }
            }
            this.TimerRecopilarEmitidas.Enabled = false;
            this.TimerFiltrarEmitidas.Enabled = true;
        }

        private void NextLapsoRecibido()
        {
            int count = checked(this.ListLapsosRecibidos.Count - 1);
            int num = 0;
            while (num <= count)
            {
                if (!(this.ListLapsosRecibidos[num].Status == Jaeger.Repositorio.V3.Enums.EnumDescargaLapsoStatus.Pendiente))
                {
                    num = checked(num + 1);
                }
                else
                {
                    this.IndexLapsoRecibido = num;
                    break;
                }
            }
            this.TimerFiltrarRecibidas.Enabled = false;
            this.TimerRecopilarRecibidas.Enabled = true;
        }

        private void CloseDescarga()
        {
            this.TimerLogin.Enabled = false;
            this.TimerFiltrarEmitidas.Enabled = false;
            this.TimerRecopilarEmitidas.Enabled = false;
            this.TimerClose.Enabled = false;
            
            this.Cerrando = false;
            MessageBox.Show("El RFC o contraseña son incorrectos. Verifique su información e inténtelo de nuevo.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void TimerClose_Tick(object sender, EventArgs e)
        {
            this.LbMensaje.Text = "Cerrando sesión ...";
            
            if (this.GetNavegadorCerrado())
            {
                try
                {
                    this.webSAT.Document.ExecCommand("ClearAuthenticationCache", false, null);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("TimerClose: " + ex.Message);
                }
                this.TimerClose.Enabled = false;
                this.LbMensaje.Text = "Sesión Terminada ...";
                this.OnCompletedProcess(new DescargaMasivaCompletedProcess("El proceso concluyo satisfactoriamente!", ""));
                this.Cerrando = false;
            }
        }

        private void TimerError_Tick(object sender, EventArgs e)
        {
            if (this.ErrroDeAutentificacion())
            {
                this.TimerError.Enabled = false;
                this.GoUrl(this.UrlAutentificacion);
            }
        }

        private void TimerFiltrarEmitidas_Tick(object sender, EventArgs e)
        {
            bool enabled = false;
            HtmlElement elementById = null;
            elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_TxtRfcReceptor");
            if (elementById != null)
            {
                enabled = elementById.Enabled;
            }
            if (!this.ExisteUpdateProgres() & enabled)
            {
                this.TimerFiltrarEmitidas.Enabled = false;
                this.GetElementHtmlById("ctl00_MainContent_CldFechaInicial2_Calendario_text", ref this.webSAT).InnerText = this.ListLapsosEmitidos[this.IndexLapsoEmitido].Fecha.ToShortDateString();
                this.GetElementHtmlById("ctl00_MainContent_CldFechaFinal2_Calendario_text", ref this.webSAT).InnerText = this.ListLapsosEmitidos[this.IndexLapsoEmitido].Fecha.ToShortDateString();
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlHora");
                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosEmitidos[this.IndexLapsoEmitido].SegundoInicial)[1]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlMinuto");
                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosEmitidos[this.IndexLapsoEmitido].SegundoInicial)[2]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlSegundo");
                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosEmitidos[this.IndexLapsoEmitido].SegundoInicial)[3]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlHora");
                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosEmitidos[this.IndexLapsoEmitido].SegundoFinal)[1]);

                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlMinuto");
                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosEmitidos[this.IndexLapsoEmitido].SegundoFinal)[2]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlSegundo");
                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosEmitidos[this.IndexLapsoEmitido].SegundoFinal)[3]);
                }
                if (this.Configuracion.FiltrarPorRFC == true)
                {
                    this.GetElementHtmlById("ctl00_MainContent_TxtRfcReceptor", ref this.webSAT).SetAttribute("value", this.Configuracion.BuscarPorRFC);
                }
                this.GetElementHtmlById("ctl00_MainContent_DdlEstadoComprobante", ref this.webSAT).SetAttribute("value", this.Configuracion.Status.ToString());
                this.GetElementHtmlById("ddlComplementos", ref this.webSAT).SetAttribute("value", this.Configuracion.Complemento.ToString());
                this.GetElementHtmlById("ctl00_MainContent_BtnBusqueda", ref this.webSAT).InvokeMember("click");
                this.LbMensaje.Text = string.Concat("Emitidas día ", this.ListLapsosEmitidos[this.IndexLapsoEmitido].Fecha, "...");
                this.TimerRecopilarEmitidas.Enabled = true;
            }
        }

        private void TimerFiltrarRecibidas_Tick(object sender, EventArgs e)
        {
            bool enabled = false;
            HtmlElement elementById = null;
            elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_TxtRfcReceptor");
            if (elementById != null)
            {
                enabled = elementById.Enabled;
            }
            if (!this.ExisteUpdateProgres() & enabled)
            {
                this.TimerFiltrarRecibidas.Enabled = false;
                // introducir los datos de la fecha
                this.GetElementHtmlById("DdlAnio", ref this.webSAT).SetAttribute("value", this.ListaFechasRecibidos[this.IndexDiaRecibido].Fecha.Year.ToString(System.Globalization.CultureInfo.InvariantCulture));
                this.GetElementHtmlById("ctl00_MainContent_CldFecha_DdlMes", ref this.webSAT).SetAttribute("value", this.ListaFechasRecibidos[this.IndexDiaRecibido].Fecha.Month.ToString("0"));
                this.GetElementHtmlById("ctl00_MainContent_CldFecha_DdlMes", ref this.webSAT).InvokeMember("Change");
                this.GetElementHtmlById("ctl00_MainContent_CldFecha_DdlDia", ref this.webSAT).SetAttribute("value", this.ListaFechasRecibidos[this.IndexDiaRecibido].Fecha.Day.ToString("00"));
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlHora");

                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosRecibidos[this.IndexLapsoRecibido].SegundoInicial)[1]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMinuto");

                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosRecibidos[this.IndexLapsoRecibido].SegundoInicial)[2]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlSegundo");

                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosRecibidos[this.IndexLapsoRecibido].SegundoInicial)[3]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlHoraFin");

                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosRecibidos[this.IndexLapsoRecibido].SegundoFinal)[1]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMinutoFin");

                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosRecibidos[this.IndexLapsoRecibido].SegundoFinal)[2]);
                }
                elementById = null;
                elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlSegundoFin");

                if (elementById != null)
                {
                    elementById.SetAttribute("value", this.GetTiempoInteger(this.ListLapsosRecibidos[this.IndexLapsoRecibido].SegundoFinal)[3]);
                }

                if (this.Configuracion.FiltrarPorRFC == true)
                {
                    this.GetElementHtmlById("ctl00_MainContent_TxtRfcReceptor", ref this.webSAT).SetAttribute("value", this.Configuracion.BuscarPorRFC);
                }

                this.GetElementHtmlById("ctl00_MainContent_DdlEstadoComprobante", ref this.webSAT).SetAttribute("value", this.Configuracion.Status.ToString());
                this.GetElementHtmlById("ddlComplementos", ref this.webSAT).SetAttribute("value", this.Configuracion.Complemento);
                this.GetElementHtmlById("ctl00_MainContent_BtnBusqueda", ref this.webSAT).InvokeMember("click");
                this.LbMensaje.Text = string.Concat("Filtrando día ", this.ListLapsosRecibidos[this.IndexLapsoRecibido].Fecha.ToShortDateString(), "...");
                this.TimerRecopilarRecibidas.Enabled = true;
            }
        }

        private void TimerLogin_Tick(object sender, EventArgs e)
        {
            if (this.CredencialesNoValidas() | this.ErrroDeCredenciales())
            {
                this.CloseDescarga();
            }
        }

        private void TimerRecopilarEmitidas_Tick(object sender, EventArgs e)
        {
            if (!this.ExisteUpdateProgres() & this.webSAT.ReadyState == WebBrowserReadyState.Complete & !this.webSAT.IsBusy)
            {
                this.TimerRecopilarEmitidas.Enabled = false;
                this.LbMensaje.Text = "Recopilando XML ...";
                if (this.GetCuantosXML() <= 0)
                {
                    this.ListLapsosEmitidos[this.IndexLapsoEmitido].Status = Jaeger.Repositorio.V3.Enums.EnumDescargaLapsoStatus.Vacio;
                }
                else if (!this.GetLimiteDe500())
                {
                    this.ListLapsosEmitidos[this.IndexLapsoEmitido].Status = Jaeger.Repositorio.V3.Enums.EnumDescargaLapsoStatus.Ok;
                    DateTime ahora = DateTime.Now;
                    int countXML = 0;
                    this.ProcessResult(ref countXML, ref ahora, ref this.webSAT);
                    this.ListaFechasEmitidos[this.IndexDiaEmitido].Total = this.ListaFechasEmitidos[this.IndexDiaEmitido].Total + countXML;
                    this.ListLapsosEmitidos[this.IndexLapsoEmitido].Cuantos = countXML;
                    this.LbMensaje.Text = string.Concat("Recopilando ", countXML, " XML");
                    this.LbError.Text = "";
                    this.LbError.Text = this.LbMensaje.Text;
                }
                else
                {
                    this.LbMensaje.Text = "Creando Lapsos ...";
                    this.ListLapsosEmitidos[this.IndexLapsoEmitido].Status = Jaeger.Repositorio.V3.Enums.EnumDescargaLapsoStatus.X;
                    this.CrearLapsosEmitidos(this.IndexLapsoEmitido);
                }
                if (this.HayRangosPorRevisar(this.ListLapsosEmitidos))
                {
                    this.NextLapsoEmitido();
                    return;
                }
                this.ContinuarRevisandoEmitidos();
            }
        }

        private void TimerRecopilarRecibidas_Tick(object sender, EventArgs e)
        {
            if (!this.ExisteUpdateProgres() & this.webSAT.ReadyState == WebBrowserReadyState.Complete & !this.webSAT.IsBusy)
            {
                this.TimerRecopilarRecibidas.Enabled = false;
                this.LbMensaje.Text = "Recopilando XML ...";
                if (this.GetCuantosXML() <= 0)
                {
                    this.ListLapsosRecibidos[this.IndexLapsoRecibido].Status = Jaeger.Repositorio.V3.Enums.EnumDescargaLapsoStatus.Vacio;
                }
                else if (!this.GetLimiteDe500())
                {
                    this.ListLapsosRecibidos[this.IndexLapsoRecibido].Status = Jaeger.Repositorio.V3.Enums.EnumDescargaLapsoStatus.Ok;
                    DateTime ahora = DateTime.Now;
                    int countXML = 0;
                    this.ProcessResult(ref countXML, ref ahora, ref this.webSAT);
                    this.ListaFechasRecibidos[this.IndexDiaRecibido].Total = this.ListaFechasRecibidos[this.IndexDiaRecibido].Total + countXML;
                    this.ListLapsosRecibidos[this.IndexLapsoRecibido].Cuantos = countXML;
                    this.LbMensaje.Text = string.Concat("Recopilando ", countXML, " XML");
                    this.LbError.Text = "";
                    this.LbError.Text = this.LbMensaje.Text;
                }
                else
                {
                    this.LbMensaje.Text = "Creando Lapsos ...";
                    this.ListLapsosRecibidos[this.IndexLapsoRecibido].Status = Jaeger.Repositorio.V3.Enums.EnumDescargaLapsoStatus.X;
                    this.CrearLapsosRecibidos(this.IndexLapsoRecibido);
                }
                if (this.HayRangosPorRevisar(this.ListLapsosRecibidos))
                {
                    this.NextLapsoRecibido();
                    return;
                }
                this.ContinuarRevisandoRecibidos();
            }
        }

        private void WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (this.webSAT.Document != null && !this.Cerrando)
            {
                if (this.Configuracion.Tipo == Jaeger.Repositorio.V3.Enums.EnumDescargaTipo.Emitidos)
                {
                    try
                    {
                        
                        if (this.ModoDescarga)
                        {
                            return;
                        }
                        else
                        {
                            this.LbMensaje.Text = "Ingresando ...";
                            if (!this.UrlErroLogin(this.webSAT.Url.ToString()))
                            {
                                if (this.UrlLoad(this.UrlRedir))
                                {
                                    this.GoUrl(this.UrlAutentificacion);
                                }
                                if (this.UrlLoad(this.UrlError2))
                                {
                                    this.LoginScrapSAT();
                                    this.TimerLogin.Enabled = true;
                                }
                                if (this.webSAT.Url.ToString().Contains("https://portalcfdi.facturaelectronica.sat.gob.mx") & this.EstaEnSelector() && !this.TrucoHide)
                                {
                                    this.GoUrl(this.UrlOculta);
                                    this.TrucoHide = true;
                                }
                                if (this.webSAT.Url.ToString() == this.UrlOculta & this.EstaEnSelector())
                                {
                                    this.TrucoHide = false;
                                    if (this.Configuracion.Tipo == EnumDescargaTipo.Emitidos)
                                    {
                                        this.ClickEnSelector(EnumDescargaTipo.Emitidos);
                                    }
                                }
                                if (this.webSAT.Url.ToString() == this.UrlEmitidas & this.Configuracion.Tipo == EnumDescargaTipo.Emitidos | this.webSAT.Url.ToString() == this.UrlRecibidas)
                                {
                                    this.GetElementHtmlById("ctl00_MainContent_RdoFechas", ref this.webSAT).InvokeMember("click");
                                    this.TimerLogin.Enabled = false;
                                    if (!this.Truco)
                                    {
                                        this.TimerFiltrarEmitidas.Enabled = true;
                                    }
                                }
                                if (this.webSAT.Url.ToString().Contains("https://cfdiau.sat.gob.mx/nidp/wsfed_redir_cont_portalcfdi.jsp?wa="))
                                {
                                    this.GoUrl(this.UrlAutentificacion);
                                    return;
                                }
                                else if (this.webSAT.Url.ToString() == this.UrlFailCaptcha)
                                {
                                    this.LoginScrapSAT();
                                    this.TimerLogin.Enabled = true;
                                }

                                if (this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app?sid=0"))
                                {
                                    if (this.Configuracion.Tipo == EnumDescargaTipo.Recibidos)
                                    {
                                        this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx");
                                    }

                                    if (this.Configuracion.Tipo == EnumDescargaTipo.Emitidos)
                                    {
                                        this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx");
                                    }
                                }

                                if (this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_MX"))
                                {
                                    if (this.Configuracion.Tipo == EnumDescargaTipo.Recibidos)
                                    {
                                        this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx");
                                    }

                                    if (this.Configuracion.Tipo == EnumDescargaTipo.Emitidos)
                                    {
                                        this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx");
                                    }
                                }
                            }
                            else
                            {
                                this.GoUrl(this.UrlError1);
                                return;
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(exception.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                if (this.Configuracion.Tipo == Jaeger.Repositorio.V3.Enums.EnumDescargaTipo.Recibidos)
                {
                    if (this.ModoDescarga)
                    {
                        return;
                    }
                    if (this.webSAT.Url.ToString() == this.UrlOculta & this.EstaEnSelector())
                    {
                        this.TrucoHide = false;
                        this.ClickEnSelector(this.Configuracion.Tipo);
                    }
                    if (this.webSAT.Url.ToString() == this.UrlRecibidas & this.Configuracion.Tipo == Jaeger.Repositorio.V3.Enums.EnumDescargaTipo.Recibidos | this.webSAT.Url.ToString() == this.UrlRecibidas)
                    {
                        this.GetElementHtmlById("ctl00_MainContent_RdoFechas", ref this.webSAT).InvokeMember("click");
                        this.TimerLogin.Enabled = false;
                        if (!this.Truco)
                        {
                            this.TimerFiltrarRecibidas.Enabled = true;
                        }
                    }
                }
            }
            while (true)
            {
                Application.DoEvents();
                if (!this.webSAT.IsBusy)
                {
                    break;
                }
                if (this.webSAT.ReadyState == WebBrowserReadyState.Loading)
                {
                    Application.DoEvents();
                }
            }
        }

        private void Authenticate()
        {
            try
            {
                this.OnStartProcess(new DescargaMasivaStartProcess());
                this.AuthenticateComun();
            }
            catch (Exception ex)
            {
                this.OnCompletedProcess(new DescargaMasivaCompletedProcess("Autenticación", ex.Message, true));
                this.LbError.Text = ex.Message;
            }
        }

        private void LlenarLista()
        {
            DateTime dateTime = this.Configuracion.FechaInicial;
            DateTime f2 = this.Configuracion.FechaFinal;
            f2 = f2.AddDays(1);

            while (dateTime < f2)
            {
                dateTime = dateTime.AddDays(1);
                this.ListaFechasEmitidos.Add(new DescargaFechas500 { Fecha = dateTime });
                this.ListaFechasRecibidos.Add(new DescargaFechas500 { Fecha = dateTime });
            }
        }

        #endregion
    }
}
