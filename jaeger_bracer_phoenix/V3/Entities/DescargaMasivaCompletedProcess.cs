﻿using System;

namespace Jaeger.Repositorio.V3.Entities
{
    public class DescargaMasivaCompletedProcess : EventArgs
    {
        public DescargaMasivaCompletedProcess(string mensaje, string logError, bool isError = false)
        {
            this.Data = mensaje;
            this.LogError = logError;
        }

        public string Data { get; set; }

        public string LogError { get; set; }

        public bool IsError { get; set; }
    }
}
