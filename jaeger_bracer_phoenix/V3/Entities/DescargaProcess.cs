﻿using System;
using System.Linq;
using System.ComponentModel;

namespace Jaeger.Repositorio.V3.Entities
{
    public class DescargaProcess
    {
        public DescargaProcess()
        {
            this.Complete = 0;
            this.Downloaded = new BindingList<ViewModelDescargaResponse>();
        }

        public int Complete { get; set; }
        public BindingList<ViewModelDescargaResponse> Downloaded { get; set; }
    }
}
