﻿/// develop: anhe 261120180043
/// purpose: representa fila de la tabla de comprobantes del SAT
using System;
using Jaeger.Repositorio.V3.Enums;
using Newtonsoft.Json;
using SqlSugar;

namespace Jaeger.Repositorio.V3.Entities {
    [JsonObject]
    [SugarTable("Comprobantes", "comprobantes fiscales")]
    public class ViewModelDescargaResponse : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation {
        private string idDocumentoField;
        private string tipoField;
        private string emisorRFCField;
        private string emisorField;
        private string receptorRFCField;
        private string receptorField;
        private DateTime fechaEmisionField;
        private DateTime fechaCerfificacionField;
        private string pacCertificaField;
        private decimal totalField;
        private string efectoField;
        private string statusCancelacionField;
        private string estadoField;
        private string statusProcesoCancelarField;
        private DateTime? fechaStatusCancelaField;
        private DateTime? fechaValidacionField;
        private DateTime? fechaNuevoField;
        private string urlXMLField;
        private string urlPDFField;
        private string urlDetalleField;
        private string urlRecuperarAcuseField;
        private string urlRecuperaAcuseFinalField;
        private string pathXMLField;
        private string pathPDFField;
        private string resultField;
        private string xmlContentB64Field;
        private string pdfRepresentacionImpresaB64Field;
        private string pdfAcuseB64Field;
        private string pdfAcuseFinalB64Field;
        private EnumDescargaConsilia comprobadoField;
        private decimal totalRetencionISR;
        private decimal totalRetencionIVA;
        private decimal totalRetencionIEPS;
        private decimal totalTrasladoIVA;
        private decimal totalTrasladoIEPS;
        private decimal subTotalField;
        private decimal descuentoField;

        /// <summary>
        /// constructor
        /// </summary>
        public ViewModelDescargaResponse() {
            this.comprobadoField = EnumDescargaConsilia.SinVerificar;
            this.fechaNuevoField = DateTime.Now;
            this.pathPDFField = "";
            this.pdfRepresentacionImpresaB64Field = "";
            this.resultField = "";
        }

        /// <summary>
        /// obtener o establecer el folio fiscal (uuid) del comprobante
        /// </summary>
        [JsonProperty("idDocumento")]
        [SugarColumn(ColumnName = "IdDocumento", IsPrimaryKey = true, Length = 36)]
        public string IdDocumento {
            get {
                return this.idDocumentoField;
            }
            set {
                this.idDocumentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo del comprobante (Emitido o Recibido)
        /// </summary>
        [SugarColumn(ColumnName = "Tipo", Length = 10)]
        public string Tipo {
            get {
                return this.tipoField;
            }
            set {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el rfc del emisor del comprobante
        /// </summary>
        [JsonProperty("emisorRFC")]
        [SugarColumn(ColumnName = "EmisorRFC", Length = 16)]
        public string EmisorRFC {
            get {
                return this.emisorRFCField;
            }
            set {
                this.emisorRFCField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el Nombre o Razón Social del Emisor
        /// </summary>
        [JsonProperty("emisor")]
        [SugarColumn(ColumnName = "Emisor", Length = 255)]
        public string Emisor {
            get {
                return this.emisorField;
            }
            set {
                this.emisorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer el RFC del receptor del comprobante
        /// </summary>
        [JsonProperty("receptorRFC")]
        [SugarColumn(ColumnName = "ReceptorRFC", Length = 16)]
        public string ReceptorRFC {
            get {
                return this.receptorRFCField;
            }
            set {
                this.receptorRFCField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el Nombre o Razón Social del Receptor
        /// </summary>
        [JsonProperty("receptor")]
        [SugarColumn(ColumnName = "Receptor", Length = 255)]
        public string Receptor {
            get {
                return this.receptorField;
            }
            set {
                this.receptorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emisión del comprobante
        /// </summary>
        [JsonProperty("fechaEmision")]
        [SugarColumn(ColumnName = "FechaEmision")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmisionField;
            }
            set {
                this.fechaEmisionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de certificación del comprobante
        /// </summary>
        [JsonProperty("fechacertificacion")]
        [SugarColumn(ColumnName = "FechaCerfificacion")]
        public DateTime FechaCerfificacion {
            get {
                return this.fechaCerfificacionField;
            }
            set {
                this.fechaCerfificacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del PAC que Certificó
        /// </summary>
        [JsonProperty("pac")]
        [SugarColumn(ColumnName = "PAC", Length = 16)]
        public string PACCertifica {
            get {
                return this.pacCertificaField;
            }
            set {
                this.pacCertificaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor total del impuesto retenido ISR
        /// </summary>
        [SugarColumn(ColumnName = "RetencionISR", Length = 14, DecimalDigits = 4)]
        public decimal RetencionISR {
            get {
                return this.totalRetencionISR;
            }
            set {
                this.totalRetencionISR = value;
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA
        /// </summary>
        [SugarColumn(ColumnName = "RetencionIVA", Length = 14, DecimalDigits = 4)]
        public decimal RetencionIVA {
            get {
                return this.totalRetencionIVA;
            }
            set {
                this.totalRetencionIVA = value;
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS
        /// </summary>
        [SugarColumn(ColumnName = "RetencionIEPS", Length = 14, DecimalDigits = 4)]
        public decimal RetencionIEPS {
            get {
                return this.totalRetencionIEPS;
            }
            set {
                this.totalRetencionIEPS = value;
            }
        }

        /// <summary>
        /// importe del traslado de IVA
        /// </summary>
        [SugarColumn(ColumnName = "TrasladoIVA", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIVA {
            get {
                return this.totalTrasladoIVA;
            }
            set {
                this.totalTrasladoIVA = value;
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto traslado IEPS
        /// </summary>
        [SugarColumn(ColumnName = "TrasladoIEPS", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIEPS {
            get {
                return this.totalTrasladoIEPS;
            }
            set {
                this.totalTrasladoIEPS = value;
            }
        }

        /// <summary>
        /// obtener o establecer el subTotal del comprobante
        /// </summary>
        [SugarColumn(ColumnName = "SubTotal", Length = 14, DecimalDigits = 4)]
        public decimal SubTotal {
            get {
                return this.subTotalField;
            }
            set {
                this.subTotalField = value;
            }
        }

        /// <summary>
        /// monto del descuento aplicado
        /// </summary>
        [SugarColumn(ColumnName = "Descuento", Length = 14, DecimalDigits = 4)]
        public decimal Descuento {
            get {
                return this.descuentoField;
            }
            set {
                this.descuentoField = value;

            }
        }

        /// <summary>
        /// obtener o establecer valor total del comprobante
        /// </summary>
        [JsonProperty("total")]
        [SugarColumn(ColumnName = "Total", Length = 14, DecimalDigits = 4)]
        public decimal Total {
            get {
                return this.totalField;
            }
            set {
                this.totalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [JsonProperty("efecto")]
        [SugarColumn(ColumnName = "Efecto", Length = 16)]
        public string Efecto {
            get {
                return this.efectoField;
            }
            set {
                this.efectoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer leyenda de Estatus de cancelación (si es posible cancelar con o sin aceptación)
        /// </summary>
        [JsonProperty("statusCancelacion")]
        [SugarColumn(ColumnName = "StatusCancelacion", Length = 32)]
        public string StatusCancelacion {
            get {
                return this.statusCancelacionField;
            }
            set {
                this.statusCancelacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante Cancelado o Vigente
        /// </summary>
        [JsonProperty("estado")]
        [SugarColumn(ColumnName = "Estado", Length = 10)]
        public string Estado {
            get {
                return this.estadoField;
            }
            set {
                this.estadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Estatus de Proceso de Cancelación
        /// </summary>
        [JsonProperty("statusProcesoCancelar")]
        [SugarColumn(ColumnName = "StatusProcesoCancelar", Length = 16)]
        public string StatusProcesoCancelar {
            get {
                return this.statusProcesoCancelarField;
            }
            set {
                this.statusProcesoCancelarField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Fecha de Proceso de Cancelación
        /// </summary>
        [JsonProperty("fechaStatusCancela")]
        [SugarColumn(ColumnName = "FechaStatusCancela", IsNullable = true)]
        public DateTime? FechaStatusCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaStatusCancelaField >= firstGoodDate)
                    return this.fechaStatusCancelaField;
                else
                    return null;
            }
            set {
                this.fechaStatusCancelaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string Motivo { get; set; }

        [SugarColumn(IsIgnore = true)]
        public string FolioSustitucion { get; set; }

        [JsonProperty("fechaValidacion")]
        [SugarColumn(ColumnName = "FechaValidacion", IsNullable = true)]
        public DateTime? FechaValidacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaValidacionField >= firstGoodDate)
                    return this.fechaValidacionField;
                else
                    return null;
            }
            set {
                this.fechaValidacionField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "FechaNuevo", IsNullable = true)]
        public DateTime? FechaNuevo {
            get {
                return this.fechaNuevoField;
            }
            set {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer URL de descarga del archivo XML del comprobante
        /// </summary>
        [JsonProperty("urlXML")]
        [SugarColumn(IsIgnore = true)]
        public string UrlXML {
            get {
                return this.urlXMLField;
            }
            set {
                this.urlXMLField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer URL de descarga de la representacion impresa (PDF) del comprobante
        /// </summary>
        [JsonProperty("urlPDF")]
        [SugarColumn(IsIgnore = true)]
        public string UrlPDF {
            get {
                return this.urlPDFField;
            }
            set {
                this.urlPDFField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer URL de descarga del detalle del comprobante (PDF)
        /// </summary>
        [JsonProperty("urlDetalle")]
        [SugarColumn(IsIgnore = true)]
        public string UrlDetalle {
            get {
                return this.urlDetalleField;
            }
            set {
                this.urlDetalleField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer url para recuperar la representación impresa (pdf) del acuse de solicitud  de cancelacion del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string UrlRecuperarAcuse {
            get {
                return this.urlRecuperarAcuseField;
            }
            set {
                this.urlRecuperarAcuseField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer url para recuperar la representación impresa el Acuse del status final de la cancelacion del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string UrlRecuperaAcuseFinal {
            get {
                return this.urlRecuperaAcuseFinalField;
            }
            set {
                this.urlRecuperaAcuseFinalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer la ruta del archivo XML
        /// </summary>
        [SugarColumn(ColumnName = "PathXML")]
        public string PathXML {
            get {
                return this.pathXMLField;
            }
            set {
                this.pathXMLField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ruta del archivo PDF
        /// </summary>
        [SugarColumn(ColumnName = "PathPDF", IsNullable = true)]
        public string PathPDF {
            get {
                return this.pathPDFField;
            }
            set {
                this.pathPDFField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "Resultado", Length = 16, IsNullable = true)]
        public string Result {
            get {
                return this.resultField;
            }
            set {
                this.resultField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "XMLB64")]
        public string XmlContentB64 {
            get {
                return this.xmlContentB64Field;
            }
            set {
                this.xmlContentB64Field = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "PDFB64", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string PdfRepresentacionImpresaB64 {
            get {
                return this.pdfRepresentacionImpresaB64Field;
            }
            set {
                this.pdfRepresentacionImpresaB64Field = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "AcuseB64", IsNullable = true)]
        public string PdfAcuseB64 {
            get {
                return this.pdfAcuseB64Field;
            }
            set {
                this.pdfAcuseB64Field = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "AcuseFinalB64", IsNullable = true)]
        public string PdfAcuseFinalB64 {
            get {
                return this.pdfAcuseFinalB64Field;
            }
            set {
                this.pdfAcuseFinalB64Field = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la bandera que identifica el status de la consilicacion (0 = Sin Comprobar, 1 = Comprobado 2 = No Encontrado)
        /// </summary>
        [SugarColumn(ColumnName = "Comprobado", Length = 1)]
        public EnumDescargaConsilia Comprobado {
            get {
                return this.comprobadoField;
            }
            set {
                this.comprobadoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string ComprobadoText {
            get {
                return Enum.GetName(typeof(EnumDescargaConsilia), this.comprobadoField);
            }
            set {
                if (value == Enum.GetName(typeof(EnumDescargaConsilia), EnumDescargaConsilia.SinVerificar)) {
                    this.comprobadoField = EnumDescargaConsilia.SinVerificar;
                }
                else if (value == Enum.GetName(typeof(EnumDescargaConsilia), EnumDescargaConsilia.Verificado)) {
                    this.comprobadoField = EnumDescargaConsilia.Verificado;
                }
                else if (value == Enum.GetName(typeof(EnumDescargaConsilia), EnumDescargaConsilia.NoEncontrado)) {
                    this.comprobadoField = EnumDescargaConsilia.NoEncontrado;
                }
            }
        }

        /// <summary>
        /// obtener 
        /// </summary>
        public string KeyName() {
            return string.Concat(this.EmisorRFC, "-", this.ReceptorRFC, "-", this.IdDocumento, "-", this.FechaEmision.ToString("yyyyMMddHHmmss"));
        }

        public string KeyNameXml() {
            return string.Concat(this.KeyName(), ".xml");
        }

        public string KeyNamePdf() {
            return string.Concat(this.KeyName(), ".pdf");
        }

        /// <summary>
        /// obtener el nombre del archivo de la representación impresa del acuse de cancelacion, en este caso solo para los comprobantes emitidos.
        /// </summary>
        public string KeyNameAcuse() {
            return string.Concat(this.KeyName(), "-Acuse.pdf");
        }

        /// <summary>
        /// obtener el nombre del archivo de la representacion impresa del acuse final de la cancelacion del comprobante.
        /// </summary>
        public string KeyNameAcuseFinal() {
            return string.Concat(this.KeyName(), "-AcuseFinal.pdf");
        }
    }
}