﻿/// develop: anhe 261120180043
/// purpose: representa fila de la tabla de comprobantes del SAT
using SqlSugar;

namespace Jaeger.Repositorio.V3.Entities {
    [SugarTable("Version", "comprobantes fiscales")]
    public class VersionModel {
        public VersionModel() {
            this.Version = "3.0";
        }

        [SugarColumn(ColumnName = "Numero", IsPrimaryKey = true, Length = 3)]
        public string Version {
            get; set;
        }
    }
}