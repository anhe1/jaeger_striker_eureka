﻿using System;
using System.Linq;
using Jaeger.Repositorio.V3.Enums;

namespace Jaeger.Repositorio.V3.Entities
{
    public class DescargaLapso
    {
        public DateTime Fecha { get; set; }
        public int SegundoInicial { get; set; }
        public int SegundoFinal { get; set; }
        public EnumDescargaLapsoStatus Status { get; set; }
        public int Cuantos { get; set; }

        public override string ToString()
        {
            return string.Format("Lapso: Fecha: {0} Seg. Inicial: {1} Seg. Final: {2} Status: {3} Cuantos: {4}", Fecha.ToShortDateString(), SegundoInicial.ToString(), SegundoFinal.ToString(), Enum.GetName(typeof(EnumDescargaLapsoStatus), Status), Cuantos);
        }
    }
}
