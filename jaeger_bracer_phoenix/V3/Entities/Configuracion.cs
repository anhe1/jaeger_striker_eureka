﻿/// develop: anhe 241120180031
/// purpose: configuración de la descarga masiva de comprobantes
using System;
using Newtonsoft.Json;
using Jaeger.Repositorio.V3.Enums;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Repositorio.V3.Entities
{
    [JsonObject]
    public class Configuracion : BasePropertyChangeImplementation
    {
        private string rfcField;
        private string ciecField;
        private EnumDescargaTipo tipoField;
        private DateTime fechaField;
        private DateTime fechaInicialField;
        private DateTime fechaFinalField;
        private string carpetaDescargaField;
        private bool reporteField;
        private EnumDescargaCFDIStatus statusField;
        private string rfcbuscarField;
        private string complementoField;
        private string captchaField;
        private bool filtrorfcField;
        private bool separarField;
        private bool agruparField;
        private bool mismodiaField;
        private bool renombrarArchivos;
        private bool soloConsultaField;

        public Configuracion()
        {
            this.fechaField = DateTime.Now;
            this.FechaInicial = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            this.FechaFinal = DateTime.Now;
            this.rfcField = "";
            this.rfcbuscarField = "";
            this.tipoField = EnumDescargaTipo.Recibidos;
            this.statusField = EnumDescargaCFDIStatus.Todos;
        }

        /// <summary>
        /// obtener o establecer el RFC del contribuyente a procesar.
        /// </summary>
        [JsonProperty("rfc")]
        public string RFC
        {
            get
            {
                return this.rfcField;
            }
            set
            {
                this.rfcField = value.ToUpper().Trim();
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clase CIEC del contribuyente
        /// </summary>
        [JsonProperty("ciec")]
        public string CIEC
        {
            get
            {
                return this.ciecField;
            }
            set
            {
                this.ciecField = value.Trim();
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de descaga a realizar
        /// </summary>
        [JsonProperty("tipo")]
        public EnumDescargaTipo Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        public string TipoText
        {
            get
            {
                return Enum.GetName(typeof(EnumDescargaTipo), this.Tipo);
            }
            set
            {
                this.tipoField = (EnumDescargaTipo)Enum.Parse(typeof(EnumDescargaTipo), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha del lote a descargar para control interno
        /// </summary>
        [JsonProperty("fecha")]
        public DateTime Fecha
        {
            get
            {
                return this.fechaField;
            }
            set
            {
                this.fechaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha inicial de la consulta
        /// </summary>
        [JsonProperty("fechaInicial")]
        public DateTime FechaInicial
        {
            get
            {
                return this.fechaInicialField;
            }
            set
            {
                this.fechaInicialField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha final de la consulta
        /// </summary>
        [JsonProperty("fechaFinal")]
        public DateTime FechaFinal
        {
            get
            {
                return this.fechaFinalField;
            }
            set
            {
                this.fechaFinalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la carpeta de descarga
        /// </summary>
        [JsonProperty("carpetaDescarga")]
        public string CarpetaDescarga
        {
            get
            {
                return this.carpetaDescargaField;
            }
            set
            {
                this.carpetaDescargaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el usuario requiere visualizar el reporte de la descarga
        /// </summary>
        [JsonProperty("reporte")]
        public bool Reporte
        {
            get
            {
                return this.reporteField;
            }
            set
            {
                this.reporteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el status de la busqueda
        /// </summary>
        [JsonProperty("status")]
        public EnumDescargaCFDIStatus Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si se buscar un RFC del emisor o receptor
        /// </summary>
        [JsonProperty("buscarRFC")]
        public string BuscarPorRFC
        {
            get
            {
                return this.rfcbuscarField;
            }
            set
            {
                this.rfcbuscarField = value.Trim().ToUpper();
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer el tipo de complemento a buscar
        /// </summary>
        [JsonProperty("complemento")]
        public string Complemento
        {
            get
            {
                return this.complementoField;
            }
            set
            {
                this.complementoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtner o establacer el captcha a utilizar durante la descarga
        /// </summary>
        public string Captcha
        {
            get
            {
                return this.captchaField;
            }
            set
            {
                this.captchaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer si se buscara por captcha
        /// </summary>
        public bool FiltrarPorRFC
        {
            get
            {
                return this.filtrorfcField;
            }
            set
            {
                this.filtrorfcField = value;
                this.OnPropertyChanged();
            }
        }

        public bool Separar
        {
            get
            {
                return this.separarField;
            }
            set
            {
                this.separarField = value;
                this.OnPropertyChanged();
            }
        }

        public bool Agrupar
        {
            get
            {
                return this.agruparField;
            }
            set
            {
                this.agruparField = value;
                this.OnPropertyChanged();
            }
        }

        public bool Mismodia
        {
            get
            {
                return this.mismodiaField;
            }
            set
            {
                this.mismodiaField = value;
                this.OnPropertyChanged();
            }
        }

        public bool Renombrar
        {
            get
            {
                return this.renombrarArchivos;
            }
            set
            {
                this.renombrarArchivos = value;
                this.OnPropertyChanged();
            }
        }

        public bool SoloConsulta
        {
            get
            {
                return this.soloConsultaField;
            }
            set
            {
                this.soloConsultaField = value;
            }
        }

        public string Json(Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static Configuracion Json(string inputJson)
        {
            try
            {
                return JsonConvert.DeserializeObject<Configuracion>(inputJson);
            }
            catch (JsonException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
    }
}
