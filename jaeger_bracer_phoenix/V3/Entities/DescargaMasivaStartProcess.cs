using System;

namespace Jaeger.Repositorio.V3.Entities
{
    public class DescargaMasivaStartProcess : EventArgs
    {
        public DescargaMasivaStartProcess()
        {
        }

        public string Data { get; set; }
    }
}