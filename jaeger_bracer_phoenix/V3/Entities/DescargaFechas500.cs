﻿using System;
using System.Linq;
using Jaeger.Repositorio.V3.Enums;

namespace Jaeger.Repositorio.V3.Entities
{
    public class DescargaFechas500 : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private DateTime fechaField;
        private EnumDescargaLapsoStatus statusField;
        private int totalField;

        public DescargaFechas500()
        {
            this.statusField = EnumDescargaLapsoStatus.Pendiente;
            this.totalField = 0;
        }

        public DateTime Fecha
        {
            get
            {
                return this.fechaField;
            }
            set
            {
                this.fechaField = value;
                this.OnPropertyChanged();
            }
        }

        public EnumDescargaLapsoStatus Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
                this.OnPropertyChanged();
            }
        }

        public int Total
        {
            get
            {
                return this.totalField;
            }
            set
            {
                this.totalField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
