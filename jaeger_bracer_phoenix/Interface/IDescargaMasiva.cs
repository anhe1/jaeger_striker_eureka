﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.ComponentModel;
using Jaeger.Repositorio.V3.Entities;

namespace Jaeger.Repositorio.Interface
{
    public interface IDescargaMasiva
    {

        #region propiedades

        Configuracion Configuracion { get; set; }
        BindingList<ViewModelDescargaResponse> Resultados { get; set; }
        int Cuantos { get; }

        #endregion

        #region metodos publicos 

        void Consultar(WebBrowser _wsSat2, Configuracion conf);

        void Reset();

        /// <summary>
        /// crear el registro en la base de datos
        /// </summary>
        void Registrar(ViewModelDescargaResponse objeto);

        #endregion

        #region eventos de la clase

        /// <summary>
        /// al iniciar el proceso de consulta
        /// </summary>
        event EventHandler<DescargaMasivaStartProcess> StartProcess;

        /// <summary>
        /// progreso de la descarga
        /// </summary>
        event EventHandler<DescargaMasivaProgreso> ProgressChanged;

        /// <summary>
        /// al terminar el proceso 
        /// </summary>
        event EventHandler<DescargaMasivaCompletedProcess> CompletedProcess;
        
        #endregion
    }
}
