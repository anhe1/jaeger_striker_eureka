﻿using System.Collections.Generic;
using System.Linq;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.ReciboFiscal.Contracts;
using Jaeger.Domain.ReciboFiscal.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.ReciboFiscal.Repositories {
    public class SqlSugarReciboFiscalRepository : MySqlSugarContext<ReciboFiscalModel>, ISqlReciboFiscalRepository {
        public SqlSugarReciboFiscalRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var condiciones = this.Convierte(conditionals);
            if (typeof(T1) == typeof(ReciboFiscalModel)) {
                var sqlCommand = "select * from _cfdi where _cfdi_rfce=@rfc and _cfdi_serie like 'RF' and month(_cfdi_fecems)=@mes and year(_cfdi_fecems)=@anio order by _cfdi._cfdi_feccert desc";
                return this.GetMapper<T1>(sqlCommand, conditionals);
            }
            return this.Db.Queryable<T1>().Where(condiciones).ToList();
        }

        /// <summary>
        /// listado de recibos fiscales por mes año y la verifcacion del emisor del RFC, solo funciona con la serie RF
        /// </summary>
        /// <param name="mes">mes</param>
        /// <param name="anio">año</param>
        /// <param name="rfc">rfc</param>
        public IEnumerable<ReciboFiscalModel> GetRecibosFiscales(int mes, int anio, int dia, string rfc) {
            string commandText = "select * from _cfdi where _cfdi_rfce=@rfc and _cfdi_serie like 'RF' and month(_cfdi_fecems)=@mes and year(_cfdi_fecems)=@anio order by _cfdi._cfdi_feccert desc";
            var tabla = this.Db.Ado.GetDataTable(commandText,
                new SugarParameter[] {
                new SugarParameter("@anio", anio),
                new SugarParameter("@mes", mes),
                new SugarParameter("@rfc", rfc),
            });
            var mapper = new DataNamesMapper<ReciboFiscalModel>();
            return mapper.Map(tabla).ToList();
        }
    }
}
