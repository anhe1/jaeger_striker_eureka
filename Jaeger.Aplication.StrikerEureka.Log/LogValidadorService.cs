﻿using System.Linq;
using System.ComponentModel;
using Jaeger.DataAccess.StrikerEureka.Log.Repositories;
using Jaeger.Domain.StrikerEureka.Log.Contracts;
using Jaeger.Domain.StrikerEureka.Log.Entities;

namespace Jaeger.Aplication.StrikerEureka.Log {
    public class LogValidadorService : ILogValidadorService {
        protected ISqlCFDResponseValidacionRepository validacionRepository;

        public LogValidadorService() {
            this.validacionRepository = new SqlSugarCFDResponseValidacionRepository(ConfigService.currentRepositorio);
        }
        public void CrearBase() {
            throw new System.NotImplementedException();
        }

        public BindingList<CFDResponseValidacionModel> GetList(string estado, int cantidad) {
            return new BindingList<CFDResponseValidacionModel>(this.validacionRepository.GetList(Base.ConfigService.Synapsis.Empresa.RFC, estado, cantidad).ToList());
        }

        public void LoadFolderLog(string carpeta, bool incluirSubCarpetas) {
            throw new System.NotImplementedException();
        }

        public BindingList<CFDResponseValidacionModel> Verificar(BindingList<CFDResponseValidacionModel> lista) {
            throw new System.NotImplementedException();
        }

        public void WriteLog(string localFileName) {
            throw new System.NotImplementedException();
        }

        public BindingList<EstadoModel> GetEstados() {
            return new BindingList<EstadoModel>() { new EstadoModel(0, "Todo"), new EstadoModel(1, "NoVerificado"), new EstadoModel(2, "Verificado") };
        }
    }
}
