﻿using Jaeger.Domain.StrikerEureka.Log.Entities;
using System.ComponentModel;

namespace Jaeger.Aplication.StrikerEureka.Log {
    public interface ILogValidadorService {
        BindingList<EstadoModel> GetEstados();

        BindingList<CFDResponseValidacionModel> GetList(string estado, int cantidad);

        void WriteLog(string localFileName);

        void LoadFolderLog(string carpeta, bool incluirSubCarpetas);

        //BindingList<ComprobanteFiscalModel> Lista(string[] lista);

        BindingList<CFDResponseValidacionModel> Verificar(BindingList<CFDResponseValidacionModel> lista);

        /// <summary>
        /// crear base de datos
        /// </summary>
        void CrearBase();
    }
}
