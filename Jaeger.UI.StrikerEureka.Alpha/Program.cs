﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Base;
using Jaeger.UI.Login.Forms;

namespace Jaeger.UI {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginForm(args));
            if (ConfigService.Piloto != null && ConfigService.Synapsis != null)
                Application.Run(new Forms.MainRibbonForm());
        }
    }
}