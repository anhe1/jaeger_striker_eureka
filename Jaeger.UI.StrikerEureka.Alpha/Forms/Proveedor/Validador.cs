﻿using Jaeger.Aplication.Base;
using Jaeger.Aplication.Validador.Adapter;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.UI.Forms.Proveedor {
    public class Validador : UI.Validador.Forms.ComprobanteFiscalForm {
        public Validador(UIMenuElement menuElement) : base() {
            this.Text = "Proveedor: Validar Comprobantes";
            this.Load += Validador_Load;
        }

        private void Validador_Load(object sender, System.EventArgs e) {
            this._Backup = new AdministradorBackup().WitRFC(ConfigService.Synapsis.Empresa.RFC).WitConfiguration(this._Configuration);
        }

        protected override void PDFValidacion() {
            foreach (var item in this._Administrador.DataSource) {
                if (item.Version == "3.3") {
                    var val = ComprobanteValidacionPrinter.Json(item.Validacion.Json());
                    if (val != null) {
                          UI.Comprobante.Services.ComunService.ValidacionPDF(val, System.IO.Path.Combine((string)this.Tag, val.KeyName() + ".pdf"));
                    }
                }
            }
        }
    }
}
