﻿using Jaeger.Domain.Base.Abstractions;
using System;

namespace Jaeger.UI.Forms.Proveedor {
    public class SolicitudCotizacionesForm : UI.Adquisiciones.Forms.SolicitudCotizacionesBaseForm {
        public SolicitudCotizacionesForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.SolicitudCotizacionesForm_Load;
        }

        private void SolicitudCotizacionesForm_Load(object sender, EventArgs e) {

        }
    }
}
