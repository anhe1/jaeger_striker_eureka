﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Base;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Proveedor {
    public class OrdenProveedorCatalogoForm : UI.Adquisiciones.Forms.OrdenCompraCatalogoForm {
        protected internal RadMenuItem RequerimientoCompra = new RadMenuItem { Text = "Requerimiento de Compra", Name = "cppro_gpro_reqcompra" };
        protected internal RadMenuItem SolicitudCotizacion = new RadMenuItem { Text = "Solicitud de cotización", Name = "adm_gprv_solcotiza" };

        public OrdenProveedorCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Text = "Orden de Compra";
            this.Load += this.OrdenProveedorCatalogo_Load;
        }

        private void OrdenProveedorCatalogo_Load(object sender, EventArgs e) {
            this.GOrdenCompra.Herramientas.Items.Add(this.RequerimientoCompra);
            this.GOrdenCompra.Herramientas.Items.Add(this.SolicitudCotizacion);
            this.RequerimientoCompra.Click += this.RequerimientoCompra_Click;
            this.SolicitudCotizacion.Click += this.SolicitudCotizacion_Click;

            this.SolicitudCotizacion.Tag = ConfigService.GeMenuElement(this.SolicitudCotizacion.Name);
            this.SolicitudCotizacion.Enabled = ((UIMenuElement)this.SolicitudCotizacion.Tag).IsEnable;
            this.Solicitud.Enabled = this.SolicitudCotizacion.Enabled;
            this.RequerimientoCompra.Tag = ConfigService.GeMenuElement(this.RequerimientoCompra.Name);

        }

        private void SolicitudCotizacion_Click(object sender, EventArgs e) {
            var element = (UIMenuElement)this.SolicitudCotizacion.Tag;
            var catalogo = new SolicitudCotizacionesForm(element) { MdiParent = this.ParentForm };
            catalogo.Show();
        }

        private void RequerimientoCompra_Click(object sender, EventArgs e) {
            var element = (UIMenuElement)this.RequerimientoCompra.Tag;
            var catalogo = new UI.CP.Forms.Adquisiciones.ReqsCompraForm(element) { MdiParent = this.ParentForm };
            catalogo.Show();
        }
    }
}
