﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Forms.Proveedor {
    public class RecibidosResumen2Form : Comprobante.Forms.ComprobantesFiscalesResumen2 {
        public RecibidosResumen2Form(UIMenuElement menuElement) : base(CFDISubTipoEnum.Recibido, menuElement) {
            this.Load += ComprobantesRecibidosResumen2_Load;
        }

        private void ComprobantesRecibidosResumen2_Load(object sender, System.EventArgs e) {
            this.Text = "Facturación vs Pagos";
        }
    }
}
