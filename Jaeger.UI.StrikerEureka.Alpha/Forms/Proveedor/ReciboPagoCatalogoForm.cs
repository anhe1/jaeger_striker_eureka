﻿using System;

namespace Jaeger.UI.Forms.Proveedor {
    public class ReciboPagoCatalogoForm : UI.Banco.Forms.MovimientosForm {

        public ReciboPagoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(Domain.Banco.ValueObjects.MovimientoBancarioEfectoEnum.Egreso) {
            this.Text = "Proveedores: Pagos";
            this.Load += ReciboPagoCatalogoForm_Load;
        }

        private void ReciboPagoCatalogoForm_Load(object sender, EventArgs e) {
            this.TMovimiento.Nuevo.Click += this.Nuevo_Click;
        }

        private void Nuevo_Click(object sender, EventArgs e) {
            //var recibo = new ReciboPagoForm(this.service);
            //recibo.ShowDialog(this);
        }
    }
}
