﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Repositorio {
    public class RepositorioForm : UI.Repositorio.Forms.ComprobantesRepositorioForm {

        public RepositorioForm(UIMenuElement menuElement) : base() {
            this.Load += this.RepositorioForm_Load;
        }

        private void RepositorioForm_Load(object sender, EventArgs e) {
            this.Text = "| Repositorio";
        }
    }
}
