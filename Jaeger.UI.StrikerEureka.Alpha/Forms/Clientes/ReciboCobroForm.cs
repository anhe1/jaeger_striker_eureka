﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;

namespace Jaeger.UI.Forms.Clientes {
    public class ReciboCobroForm : UI.Banco.Forms.MovimientoBancarioForm {
        public ReciboCobroForm() : base() {

        }

        public ReciboCobroForm(Aplication.Banco.IBancoService service) : base(service) {
            this.Load += ReciboCobroForm_Load;
        }

        public ReciboCobroForm(MovimientoBancarioDetailModel movimiento, Aplication.Banco.IBancoService service) : base(movimiento, service) {
            this.Load += ReciboCobroForm_Load;
        }

        private void ReciboCobroForm_Load(object sender, EventArgs e) {
            this.Text = "Banco: Recibo de Cobro";
            
        }
    }
}
