﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Base;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.General {
    public partial class ConfiguracionForm : RadForm {
        public ConfiguracionForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        public ConfiguracionForm() {
        }

        private void ConfiguracionForm_Load(object sender, EventArgs e) {
            this.label1.Text = string.Format(this.label1.Text, ConfigService.Synapsis.Empresa.RFC);
            this.propertyGrid1.SelectedObject = ConfigService.Synapsis;
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Guardar_Click(object sender, EventArgs e) {

        }
    }
}
