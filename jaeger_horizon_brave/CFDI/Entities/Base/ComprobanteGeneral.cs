using Jaeger.Domain.Services.Mapping;
using Newtonsoft.Json;

namespace Jaeger.CFDI.Entities.Base
{
    public class ComprobanteGeneral : ComprobanteBasico
    {
        private string claveVendedorField;
        private decimal saldoPagosField;

        [JsonProperty("claveVendedor")]
        public string ClaveVendedor
        {
            get
            {
                return this.claveVendedorField;
            }
            set
            {
                this.claveVendedorField = value;
            }
        }

        [JsonIgnore]
        [DataNames("_cfdi_salgopagos")]
        public decimal SaldoPagos
        {
            get{
                return this.saldoPagosField;
            }
            set
            {
                this.saldoPagosField=value;
                this.OnPropertyChanged();
            }
        }
    }
}