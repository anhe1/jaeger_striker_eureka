﻿using System;
using System.Linq;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Domain.Services.Mapping;
using Newtonsoft.Json;

namespace Jaeger.CFDI.Entities.Base
{
    public class ComprobanteBase : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private long indexField;
        private string versionField;
        private EnumCfdiType tipoComprobanteField;
        private EnumCfdiSubType subTipoDeComprobanteField;
        private string folioField;
        private string serieField;
        private DateTime fechaEmisionField;
        private DateTime? fechaPagoField;

        private decimal tipoDeCambioField;
        private string statusField;
        private string estadoField;
        private int numParcialidadField;
        private int precisionDecimalField;

        // emisor del comprobante
        private string emisorField;
        private string emisorRfcField;

        // receptor del comprobante
        private string receptorField;
        private string receptorRfcField;

        // forma de pago
        private string claveMonedaField;
        private string claveFormaPagoField;
        private string claveMetodoPagoField;
        private string claveCondicionPagoField;

        // montos
        private decimal subTotalField;
        private decimal descuentoField;
        private decimal totalField;
        private decimal acumuladoField;
        private decimal saldoField;

        private string urlXmlField;
        private string urlPdfField;
        private string urlXmlAcuseField;
        private string urlPdfAcuseField;
        private string accuseField;
        private string keyNameField;

        /// <summary>
        /// constructor
        /// </summary>
        public ComprobanteBase()
        {

        }

        #region propiedades

        [JsonIgnore]
        [DataNames("_cfdi_id")]
        public long Id
        {
            get
            {
                return this.indexField;
            }
            set
            {
                this.indexField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("ver")]
        [DataNames("_cfdi_ver")]
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public EnumCfdiType TipoComprobante
        {
            get
            {
                return this.tipoComprobanteField;
            }
            set
            {
                this.tipoComprobanteField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("tipoComprobante")]
        [DataNames("_cfdi_efecto")]
        public string TipoComprobanteText
        {
            get
            {
                return Enum.GetName(typeof(EnumCfdiType), this.TipoComprobante);
            }
            set
            {
                if (value == "Ingreso" || value == "I")
                {
                    this.TipoComprobante = EnumCfdiType.Ingreso;
                }
                else if (value == "Egreso" || value == "E")
                {
                    this.TipoComprobante = EnumCfdiType.Egreso;
                }
                else if (value == "Traslado" || value == "T")
                {
                    this.TipoComprobante = EnumCfdiType.Traslado;
                }
                else if (value == "Nomina" || value == "N")
                {
                    this.TipoComprobante = EnumCfdiType.Nomina;
                }
                else if (value == "Pagos" || value == "P")
                {
                    this.TipoComprobante = EnumCfdiType.Pagos;
                }
                else
                {
                    this.TipoComprobante = EnumCfdiType.Ingreso;
                }
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public EnumCfdiSubType SubTipoComprobante
        {
            get
            {
                return this.subTipoDeComprobanteField;
            }
            set
            {
                this.subTipoDeComprobanteField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("subTipoComprobante")]
        [DataNames("_cfdi_doc_id")]
        public string SubTipoComprobanteText
        {
            get
            {
                return Enum.GetName(typeof(EnumCfdiSubType), this.subTipoDeComprobanteField);
            }
            set
            {
                this.subTipoDeComprobanteField = (EnumCfdiSubType)Enum.Parse(typeof(EnumCfdiSubType), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para precisar el folio del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        [JsonProperty("folio")]
        [DataNames("_cfdi_folio")]
        public string Folio
        {
            get
            {
                return this.folioField;
            }
            set
            {
                this.folioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para precisar la serie del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        [JsonProperty("serie")]
        [DataNames("_cfdi_serie")]
        public string Serie
        {
            get
            {
                return this.serieField;
            }
            set
            {
                this.serieField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("fecEmision")]
        [DataNames("_cfdi_fecems")]
        public DateTime FechaEmision
        {
            get
            {
                return this.fechaEmisionField;
            }
            set
            {
                this.fechaEmisionField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("fecPago")]
        [DataNames("_cfdi_fecupc")]
        public DateTime? FechaPago
        {
            get
            {
                return this.fechaPagoField;
            }
            set
            {
                this.fechaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("tipoCambio")]
        public decimal TipoCambio
        {
            get
            {
                return this.tipoDeCambioField;
            }
            set
            {
                this.tipoDeCambioField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("status")]
        [DataNames("_cfdi_status")]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("estado")]
        [DataNames("_cfdi_estado")]
        public string Estado
        {
            get
            {
                return this.estadoField;
            }
            set
            {
                this.estadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el número de parcialidad que corresponde al pago. Es requerido cuando MetodoDePagoDR contiene: “PPD” Pago en parcialidades o diferido.
        /// </summary>
        [JsonProperty("numPar")]
        [DataNames("_cfdi_par")]
        public int NumParcialidad
        {
            get
            {
                return this.numParcialidadField;
            }
            set
            {
                this.numParcialidadField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("presc")]
        [DataNames("_cfdi_prec")]
        public int PrecisionDecimal
        {
            get
            {
                return this.precisionDecimalField;
            }
            set
            {
                this.precisionDecimalField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("emisor")]
        [DataNames("_cfdi_nome")]
        public string Emisor
        {
            get
            {
                return this.emisorField;
            }
            set
            {
                this.emisorField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("emisorRFC")]
        [DataNames("_cfdi_rfce")]
        public string EmisorRFC
        {
            get
            {
                return this.emisorRfcField;
            }
            set
            {
                this.emisorRfcField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("receptor")]
        [DataNames("_cfdi_nomr")]
        public string Receptor
        {
            get
            {
                return this.receptorField;
            }
            set
            {
                this.receptorField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("receptorRFC")]
        [DataNames("_cfdi_rfcr")]
        public string ReceptorRFC
        {
            get
            {
                return this.receptorRfcField;
            }
            set
            {
                this.receptorRfcField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para identificar la clave de la moneda utilizada en los importes del documento relacionado, cuando se usa moneda nacional o el documento relacionado no especifica la moneda se registra MXN. Los importes registrados en los atributos “ImpSaldoAnt”, “ImpPagado” e “ImpSaldoInsoluto” de éste nodo, deben corresponder a esta moneda. Conforme con la especificación ISO 4217.
        /// </summary>
        [JsonProperty("claveMoneda")]
        [DataNames("_cfdi_moneda")]
        public string ClaveMoneda
        {
            get
            {
                return this.claveMonedaField;
            }
            set
            {
                this.claveMonedaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("claveFormaPago")]
        [DataNames("_cfdi_frmpg")]
        public string ClaveFormaPago
        {
            get
            {
                return this.claveFormaPagoField;
            }
            set
            {
                this.claveFormaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la clave del método de pago que se registró en el documento relacionado
        /// </summary>
        [JsonProperty("claveMetodoPago")]
        [DataNames("_cfdi_mtdpg")]
        public string ClaveMetodoPago
        {
            get
            {
                return this.claveMetodoPagoField;
            }
            set
            {
                this.claveMetodoPagoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("claveCondicionPago")]
        [DataNames("_cfdi_cndpg")]
        public string ClaveCondicionPago
        {
            get
            {
                return this.claveCondicionPagoField;
            }
            set
            {
                this.claveCondicionPagoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("subTotal")]
        [DataNames("_cfdi_sbttl")]
        public decimal SubTotal
        {
            get
            {
                return this.subTotalField;
            }
            set
            {
                this.subTotalField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("descuento")]
        [DataNames("_cfdi_dscnt")]
        public decimal Descuento
        {
            get
            {
                return this.descuentoField;
            }
            set
            {
                this.descuentoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("total")]
        [DataNames("_cfdi_total")]
        public decimal Total
        {
            get
            {
                return this.totalField;
            }
            set
            {
                this.totalField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("acumulado")]
        [DataNames("_cfdi_cbrd")]
        public decimal Acumulado
        {
            get
            {
                return this.acumuladoField;
            }
            set
            {
                this.acumuladoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("saldo")]
        public decimal Saldo
        {
            get
            {
                return this.saldoField;
            }
            set
            {
                this.saldoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cfdi_url_xml")]
        public string UrlXml
        {
            get
            {
                return this.urlXmlField;
            }
            set
            {
                this.urlXmlField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cfdi_url_pdf")]
        public string UrlPdf
        {
            get
            {
                return this.urlPdfField;
            }
            set
            {
                this.urlPdfField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cfdi_acuse")]
        public string Accuse
        {
            get
            {
                return this.accuseField;
            }
            set
            {
                this.accuseField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cfdi_url_xmlacu")]
        public string UrlAcuseXml
        {
            get
            {
                return this.urlXmlAcuseField;
            }
            set
            {
                this.urlXmlAcuseField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cfdi_url_pdfacu")]
        public string UrlAcusePdf
        {
            get
            {
                return this.urlPdfAcuseField;
            }
            set
            {
                this.urlPdfAcuseField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cfdi_keyname")]
        public string KeyName
        {
            get
            {
                return this.keyNameField;
            }
            set
            {
                this.keyNameField = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
