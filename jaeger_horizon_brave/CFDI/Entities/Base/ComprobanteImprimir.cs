namespace Jaeger.CFDI.Entities.Base
{
    public class ComprobanteImprimir : ComprobanteGeneral
    {
        private string cadenaOriginalField;

        public ComprobanteImprimir()
        {
        }

        public string CadenaOriginal
        {
            get
            {
                return this.cadenaOriginalField;
            }
            set
            {
                this.cadenaOriginalField = value;
            }
        }
    }
}