﻿/// develop: 180620182320
/// purpose: información temporal de un objeto contenido en el bucket de AWS
using System;
using System.Linq;
using Newtonsoft.Json;

namespace Jaeger.Entities.Backup
{
    [JsonObject("item")]
    public class BucketInfo : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private long indexField;
        private long relIdField;
        private string keyNameField;
        private string bucketField;
        private string extensionField;
        private string urlField;
        private string contadorField;
        private string errorField;

        /// <summary>
        /// indice de la tabla de buckets
        /// </summary>
        [JsonProperty("id")]
        public long Index
        {
            get
            {
                return this.indexField;
            }
            set
            {
                this.indexField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// indice de la tabla de CFDI
        /// </summary>
        [JsonProperty("relId")]
        public long RelId
        {
            get
            {
                return this.relIdField;
            }
            set
            {
                this.relIdField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("keyName")]
        public string KeyName
        {
            get
            {
                return this.keyNameField;
            }
            set
            {
                this.keyNameField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("bucketName")]
        public string Bucket
        {
            get
            {
                return this.bucketField;
            }
            set
            {
                this.bucketField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("ext")]
        public string Ext
        {
            get
            {
                return this.extensionField;
            }
            set
            {
                this.extensionField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("url")]
        public string Url
        {
            get
            {
                return this.urlField;
            }
            set
            {
                this.urlField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("error", NullValueHandling = NullValueHandling.Ignore)]
        public string Error
        {
            get
            {
                return this.errorField;
            }
            set
            {
                this.errorField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public string Contador
        {
            get
            {
                return this.contadorField;
            }
            set
            {
                this.contadorField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
