﻿using System;

namespace Jaeger.Entities.Backup
{
    public class BackUpCfdi : Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string lngIndex;

        private string lngParent;

        private string strName;

        private string strType;

        private int lngYear;

        private int lngMonth;

        private long lngCount;

        /// <summary>
        /// Cantidad de elementos
        /// </summary>
        public long Count
        {
            get
            {
                return this.lngCount;
            }
            set
            {
                this.lngCount = value;
                this.OnPropertyChanged();
            }
        }

        public string Index
        {
            get
            {
                return this.lngIndex;
            }
            set
            {
                this.lngIndex = value;
                this.OnPropertyChanged();
            }
        }

        public int Month
        {
            get
            {
                return this.lngMonth;
            }
            set
            {
                this.lngMonth = value;
                this.OnPropertyChanged();
            }
        }

        public string Name
        {
            get
            {
                return this.strName;
            }
            set
            {
                this.strName = value;
                this.OnPropertyChanged();
            }
        }

        public string Parent
        {
            get
            {
                return this.lngParent;
            }
            set
            {
                this.lngParent = value;
                this.OnPropertyChanged();
            }
        }

        public string Type
        {
            get
            {
                return this.strType;
            }
            set
            {
                this.strType = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Año
        /// </summary>
        public int Year
        {
            get
            {
                return this.lngYear;
            }
            set
            {
                this.lngYear = value;
                this.OnPropertyChanged();
            }
        }

        public BackUpCfdi()
        {
            this.lngIndex = string.Empty;
            this.lngParent = string.Empty;
            this.strName = string.Empty;
            this.strType = string.Empty;
            this.lngYear = 0;
            this.lngMonth = 0;
            this.lngCount = 0;
        }
    }
}