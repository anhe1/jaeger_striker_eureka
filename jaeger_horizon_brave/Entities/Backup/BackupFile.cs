﻿using System;
using System.Xml.Serialization;

namespace Jaeger.Entities.Backup
{
    public class BackupFile : Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string strEmisorRfc;

        private string strReceptorRfc;

        private string emisorField;

        private string receptorField;

        private string deptoField;

        private string strYear;

        private string strMonth;

        private string keyField;

        private string strFileName;

        private string strKeyName;

        private string strFileExt;

        private string strContentType;

        private string strDescription;

        private string strUrl;

        private string strUuid;

        private string strBucketName;

        private bool blnPublic;

        private bool blnCompleted;

        private long lngFileSize;

        private long lngclassification;

        private long lngIndex;

        private bool blnIsActive;

        [XmlAttribute]
        public string BucketName
        {
            get
            {
                return this.strBucketName;
            }
            set
            {
                this.strBucketName = value;
                this.OnPropertyChanged("BucketName");
            }
        }

        [XmlAttribute]
        public long Category
        {
            get
            {
                return this.lngclassification;
            }
            set
            {
                this.lngclassification = value;
                this.OnPropertyChanged("Category");
            }
        }

        [XmlAttribute]
        public bool Completed
        {
            get
            {
                return this.blnCompleted;
            }
            set
            {
                this.blnCompleted = value;
                this.OnPropertyChanged("Completed");
            }
        }

        [XmlAttribute]
        public string ContentType
        {
            get
            {
                return this.strContentType;
            }
            set
            {
                this.strContentType = value;
                this.OnPropertyChanged("ContentType");
            }
        }

        public string Departamento
        {
            get
            {
                return this.deptoField;
            }
            set
            {
                this.deptoField = value;
                this.OnPropertyChanged("Departamento");
            }
        }

        [XmlAttribute]
        public string Description
        {
            get
            {
                return this.strDescription;
            }
            set
            {
                this.strDescription = value;
                this.OnPropertyChanged("Description");
            }
        }

        public string Emisor
        {
            get
            {
                return this.emisorField;
            }
            set
            {
                this.emisorField = value;
                this.OnPropertyChanged("Emisor");
            }
        }

        public string EmisorRfc
        {
            get
            {
                return this.strEmisorRfc;
            }
            set
            {
                this.strEmisorRfc = value;
                this.OnPropertyChanged("EmisorRfc");
            }
        }

        [XmlAttribute]
        public string FileExt
        {
            get
            {
                return this.strFileExt;
            }
            set
            {
                this.strFileExt = value;
                this.OnPropertyChanged("FileExt");
            }
        }

        [XmlAttribute]
        public string FileName
        {
            get
            {
                return this.strFileName;
            }
            set
            {
                if (this.strFileName != value)
                {
                    this.strFileName = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [XmlAttribute]
        public long FileSize
        {
            get
            {
                return this.lngFileSize;
            }
            set
            {
                this.lngFileSize = value;
                this.OnPropertyChanged();
            }
        }

        public long Id
        {
            get
            {
                return this.lngIndex;
            }
            set
            {
                this.lngIndex = value;
                this.OnPropertyChanged();
            }
        }

        public bool IsActive
        {
            get
            {
                return this.blnIsActive;
            }
            set
            {
                this.blnIsActive = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public bool IsPublic
        {
            get
            {
                return this.blnPublic;
            }
            set
            {
                this.blnPublic = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string KeyName
        {
            get
            {
                return this.strKeyName;
            }
            set
            {
                this.strKeyName = value;
                this.OnPropertyChanged();
            }
        }

        public string Month
        {
            get
            {
                return this.strMonth;
            }
            set
            {
                this.strMonth = value;
                this.OnPropertyChanged();
            }
        }

        public string Receptor
        {
            get
            {
                return this.receptorField;
            }
            set
            {
                this.receptorField = value;
                this.OnPropertyChanged();
            }
        }

        public string ReceptorRfc
        {
            get
            {
                return this.strReceptorRfc;
            }
            set
            {
                this.strReceptorRfc = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string Url
        {
            get
            {
                return this.strUrl;
            }
            set
            {
                this.strUrl = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string Uuid
        {
            get
            {
                return this.strUuid;
            }
            set
            {
                this.strUuid = value;
                this.OnPropertyChanged();
            }
        }

        public string Year
        {
            get
            {
                return this.strYear;
            }
            set
            {
                this.strYear = value;
                this.OnPropertyChanged();
            }
        }

        public BackupFile()
        {
            this.strEmisorRfc = string.Empty;
            this.strReceptorRfc = string.Empty;
            this.emisorField = string.Empty;
            this.receptorField = string.Empty;
            this.deptoField = string.Empty;
            this.strYear = string.Empty;
            this.strMonth = string.Empty;
            this.keyField = string.Empty;
            this.strFileName = string.Empty;
            this.strKeyName = string.Empty;
            this.strFileExt = string.Empty;
            this.strContentType = string.Empty;
            this.strDescription = string.Empty;
            this.strUrl = string.Empty;
            this.strUuid = string.Empty;
            this.strBucketName = string.Empty;
            this.blnPublic = false;
            this.blnCompleted = false;
            this.lngFileSize = 0;
            this.lngIndex = 0;
        }
    }
}