﻿using Newtonsoft.Json;
using System;

namespace Jaeger.Entities
{
    [JsonObject]
    public class InfoArchivoDescargado : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private long idField;
        private string uuidField;
        private string rfcEmisorField;
        private string emisorField;
        private string rfcReceptorField;
        private string receptorField;
        private DateTime fechaEmisionField;
        private DateTime? fechaTimbreField;
        private string rfcProveedorCertificacionField;
        private double totalField;
        private string tipoComprobanteField;
        private string estadoField;
        private DateTime? fechaCancelacionField;
        private string resultadoField;
        private string urlXmlField;
        private string urlPdfField;
        private string urlDetallesField;
        private string urlAccuseField;
        private string rutaArchivoField;

        public InfoArchivoDescargado()
        {
            this.idField = 0;
            this.fechaTimbreField = null;
            this.fechaCancelacionField = null;
            this.fechaTimbreField = null;
            this.fechaCancelacionField = null;
        }

        /// <summary>
        /// index 
        /// </summary>
        [JsonProperty("id")]
        public long Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
                this.OnPropertyChanged("Id");
            }
        }

        /// <summary>
        /// Efecto ó Tipo de comprobante
        /// </summary>
        [JsonProperty("efecto")]
        public string Effect
        {
            get
            {
                return this.tipoComprobanteField;
            }
            set
            {
                this.tipoComprobanteField = value;
                this.OnPropertyChanged("Effect");
            }
        }

        [JsonProperty("estado")]
        public string ProofStatus
        {
            get
            {
                return this.estadoField;
            }
            set
            {
                this.estadoField = value;
                this.OnPropertyChanged("ProofStatus");
            }
        }

        [JsonProperty("uuid")]
        public string Guid
        {
            get
            {
                return this.uuidField;
            }
            set
            {
                this.uuidField = value;
                this.OnPropertyChanged("Guid");
            }
        }

        /// <summary>
        /// RFC del emisor del comprobante
        /// </summary>
        [JsonProperty("emisorRFC")]
        public string IssueRfc
        {
            get
            {
                return this.rfcEmisorField;
            }
            set
            {
                this.rfcEmisorField = value;
                this.OnPropertyChanged("IssueRfc");
            }
        }

        [JsonProperty("emisor")]
        public string IssueName
        {
            get
            {
                return this.emisorField;
            }
            set
            {
                this.emisorField = value;
                this.OnPropertyChanged("IssueName");
            }
        }

        /// <summary>
        /// Rfc del receptor del comprobante
        /// </summary>
        [JsonProperty("ReceptorRFC")]
        public string CustomerRfc
        {
            get
            {
                return this.rfcReceptorField;
            }
            set
            {
                this.rfcReceptorField = value;
                this.OnPropertyChanged("CustomerRfc");
            }
        }

        [JsonProperty("repector")]
        public string CustomerName
        {
            get
            {
                return this.receptorField;
            }
            set
            {
                this.receptorField = value;
                this.OnPropertyChanged("CustomerName");
            }
        }

        /// <summary>
        /// fecha de emision
        /// </summary>
        [JsonProperty("fechaEmision")]
        public DateTime DateOfIssue
        {
            get
            {
                return this.fechaEmisionField;
            }
            set
            {
                this.fechaEmisionField = value;
                this.OnPropertyChanged("DateOfIssue");
            }
        }

        [JsonProperty("fechaTimbre")]
        public DateTime CertificationDate
        {
            get
            {
                DateTime certificationDate;
                bool? nullable;
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                DateTime? nullable1 = this.fechaTimbreField;
                if (nullable1.HasValue)
                {
                    bool? nullable2 = new bool?(DateTime.Compare(nullable1.GetValueOrDefault(), firstGoodDate) >= 0);
                    nullable = nullable2;
                }
                else
                {
                    nullable = null;
                }
                certificationDate = (!nullable.GetValueOrDefault() ? DateTime.MinValue : (DateTime)this.fechaTimbreField);
                return certificationDate;
            }
            set
            {
                this.fechaTimbreField = value;
                this.OnPropertyChanged("CertificationDate");
            }
        }

        /// <summary>
        /// RFC del PAC que certifica
        /// </summary>
        [JsonProperty("certificador")]
        public string Certifier
        {
            get
            {
                return this.rfcProveedorCertificacionField;
            }
            set
            {
                this.rfcProveedorCertificacionField = value;
                this.OnPropertyChanged("Certifier");
            }
        }

        [JsonProperty("total")]
        public double Amount
        {
            get
            {
                return this.totalField;
            }
            set
            {
                this.totalField = value;
                this.OnPropertyChanged("Amount");
            }
        }

        /// <summary>
        /// Fecha de cancelacion del comprobante
        /// </summary>
        [JsonProperty("fechaCancelacion")]
        public DateTime? CancellationDate
        {
            get
            {
                DateTime? nullable = new DateTime?();
                bool? nullable1;
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                DateTime? nullable2 = this.fechaCancelacionField;
                if (nullable2.HasValue)
                {
                    bool? nullable3 = new bool?(DateTime.Compare(nullable2.GetValueOrDefault(), firstGoodDate) >= 0);
                    nullable1 = nullable3;
                }
                else
                {
                    nullable1 = null;
                }
                return (!nullable1.GetValueOrDefault() ? nullable : this.fechaCancelacionField);
            }
            set
            {
                this.fechaCancelacionField = value;
            }
        }

        [JsonProperty("Resultado")]
        public string Result
        {
            get
            {
                return this.resultadoField;
            }
            set
            {
                this.resultadoField = value;
                this.OnPropertyChanged("Result");
            }
        }

        [JsonProperty("urlPdf")]
        public string UrlPdf
        {
            get
            {
                return this.urlPdfField;
            }
            set
            {
                this.urlPdfField = value;
                this.OnPropertyChanged("UrlPdf");
            }
        }

        /// <summary>
        /// Url del comprobante fiscal
        /// </summary>
        [JsonProperty("urlXml")]
        public string UrlXml
        {
            get
            {
                return this.urlXmlField;
            }
            set
            {
                this.urlXmlField = value;
                this.OnPropertyChanged("UrlXml");
            }
        }

        [JsonProperty("urlAcuse")]
        public string UrlAccuse
        {
            get
            {
                return this.urlAccuseField;
            }
            set
            {
                this.urlAccuseField = value;
                this.OnPropertyChanged("UrlAccuse");
            }
        }

        /// <summary>
        /// Url de detalles del comprobante
        /// </summary>
        [JsonProperty("urlDetalle")]
        public string UrlDetails
        {
            get
            {
                return this.urlDetallesField;
            }
            set
            {
                this.urlDetallesField = value;
                this.OnPropertyChanged("UrlDetails");
            }
        }

        /// <summary>
        /// Ruta de acceso al archivo descargado (Xml)
        /// </summary>
        [JsonProperty("archivo")]
        public string FilePath
        {
            get
            {
                return this.rutaArchivoField;
            }
            set
            {
                this.rutaArchivoField = value;
                this.OnPropertyChanged("FilePath");
            }
        }

        [JsonIgnore]
        public string Year
        {
            get
            {
                return this.fechaEmisionField.Year.ToString();
            }
        }

        [JsonIgnore]
        public string Month
        {
            get
            {
                return this.fechaEmisionField.Month.ToString();
            }
        }

        [JsonIgnore]
        public string Day
        {
            get
            {
                return this.fechaEmisionField.Day.ToString();
            }
        }

        [JsonIgnore]
        public string KeyName
        {
            get
            {
                return string.Concat("cfdi-", this.IssueRfc, "-", this.CustomerRfc, "-", this.Guid, "-", this.DateOfIssue.ToString("yyyyMMddHHmmss"));
            }
        }
    }
}