﻿using System;
using System.Diagnostics;

namespace Jaeger.Entities
{
    public class SerieYFolio : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private long idField;

        private string tipoField;

        private string nombreField;

        private long serieConsecutivoField;

        private long folioConsecutivoField;

        private Rootobject confField;

        public Rootobject Conf
        {
            get
            {
                return this.confField;
            }
            set
            {
                this.confField = value;
            }
        }

        public long Folio
        {
            get
            {
                return this.folioConsecutivoField;
            }
            set
            {
                this.folioConsecutivoField = value;
                this.OnPropertyChanged();
            }
        }

        public long Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
                this.OnPropertyChanged();
            }
        }

        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        public long SerieConsecutivo
        {
            get
            {
                return this.serieConsecutivoField;
            }
            set
            {
                this.serieConsecutivoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        [DebuggerNonUserCode]
        public SerieYFolio()
        {
        }
    }
}