﻿using System;
using System.Diagnostics;

namespace Jaeger.Entities
{
    public class Serie
    {
        public string name
        {
            [DebuggerNonUserCode]
            get;
            [DebuggerNonUserCode]
            set;
        }

        public string type
        {
            [DebuggerNonUserCode]
            get;
            [DebuggerNonUserCode]
            set;
        }

        public string @value
        {
            [DebuggerNonUserCode]
            get;
            [DebuggerNonUserCode]
            set;
        }

        [DebuggerNonUserCode]
        public Serie()
        {
        }
    }
}