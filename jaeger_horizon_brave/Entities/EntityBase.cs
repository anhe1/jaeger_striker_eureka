﻿
using Newtonsoft.Json;
using System;
using System.Xml.Serialization;

namespace Jaeger.Entities
{
    public class EntityBase : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private long lngIndex;
        private long lngSubIndex;
        private bool blnIsActive;
        private string strKaijuCreate;
        private string strKaijuChange;
        private DateTime dateCreateNew;
        private DateTime? dateChangeDate;

        /// <summary>
        /// constructor
        /// </summary>
        public EntityBase()
        {
            this.lngIndex = 0;
            this.lngSubIndex = 0;
            this.blnIsActive = true;
            this.strKaijuCreate = string.Empty;
            this.strKaijuChange = string.Empty;
            this.dateCreateNew = DateTime.Now;
            this.dateChangeDate = DateTime.Now;
        }

        [JsonIgnore]
        [XmlIgnore]
        public long Id
        {
            get
            {
                return this.lngIndex;
            }
            set
            {
                if (value != this.lngIndex)
                {
                    this.lngIndex = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        [XmlIgnore]
        public bool IsActive
        {
            get
            {
                return this.blnIsActive;
            }
            set
            {
                if (value != this.blnIsActive)
                {
                    this.blnIsActive = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        [XmlIgnore]
        public long SubId
        {
            get
            {
                return this.lngSubIndex;
            }
            set
            {
                if (this.lngSubIndex != value)
                {
                    this.lngSubIndex = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// clave de usuario que creó el registro
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public string Creo
        {
            get
            {
                return this.strKaijuCreate;
            }
            set
            {
                if (this.strKaijuCreate != value)
                {
                    this.strKaijuCreate = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        [XmlIgnore]
        public string Modifica
        {
            get
            {
                return this.strKaijuChange;
            }
            set
            {
                if (this.strKaijuChange != value)
                {
                    this.strKaijuChange = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        [XmlIgnore]
        public DateTime FechaNuevo
        {
            get
            {
                return this.dateCreateNew;
            }
            set
            {
                if (DateTime.Compare(this.dateCreateNew, value) != 0)
                {
                    this.dateCreateNew = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        [XmlIgnore]
        public DateTime? FechaMod
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                    if (this.dateChangeDate >= firstGoodDate)
                    {
                        return this.dateChangeDate.Value;
                    }
                    else
                    {
                        return null;
                    }
            }
            set
            {
                this.dateChangeDate = value;
            }
        }
    }
}