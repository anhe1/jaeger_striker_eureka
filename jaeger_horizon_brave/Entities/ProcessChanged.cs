﻿namespace Jaeger.Entities
{
    public partial class ProcessChanged
    {
        public ProcessChanged()
        {

        }

        public ProcessChanged(string data, int total, int contador)
        {
            this.Data = data;
            this.Total = total;
            this.Contador = contador;
        }

        public string Data { get; set; }

        public int Completado { get; set; }

        public int Contador { get; set; }

        public int Total { get; set; }
    }
}
