﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Jaeger.Entities
{
    public class Status
    {
        private List<string> listStatus;

        private object objEmumStatus;

        public object EnumStatus
        {
            get
            {
                return this.objEmumStatus;
            }
            set
            {
                this.objEmumStatus = RuntimeHelpers.GetObjectValue(value);
            }
        }

        public List<string> Status1
        {
            get
            {
                return this.listStatus;
            }
            set
            {
                this.listStatus = value;
            }
        }

        public Status()
        {
            this.listStatus = new List<string>();
            this.objEmumStatus = RuntimeHelpers.GetObjectValue(new object());
        }

        public Status(string status)
        {
            this.listStatus = new List<string>();
            this.objEmumStatus = RuntimeHelpers.GetObjectValue(new object());
            string[] strArrays = status.Split(','); // Strings.Split(status, ",", -1, CompareMethod.Binary);
            for (int i = 0; i < checked(strArrays.Length); i = checked(i + 1))
            {
                string objeto = strArrays[i];
                this.listStatus.Add(objeto);
            }
        }

        /// <summary>
        /// validar cambio de status
        /// </summary>
        /// <param name="newStatus">determinar nuevo status</param>
        /// <param name="oldStatus">status anterior</param>
        public bool ValidateStatus(string newStatus, string oldStatus)
        {
            return true;
        }
    }
}