﻿using Jaeger.Enums;
using System;
using System.Xml.Serialization;

namespace Jaeger.Entities
{
    public class Property : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private EnumPropertyType objType;

        private string strCode;

        private string strName;

        private string strValue;

        /// <summary>
        /// Constructor
        /// </summary>
        public Property()
        {
            this.objType = EnumPropertyType.None;
            this.strCode = string.Empty;
            this.strName = string.Empty;
            this.strValue = string.Empty;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pType">Tipo</param>
        /// <param name="pCode">Codigo de Error</param>
        /// <param name="pName">Nombre de la propiedad</param>
        /// <param name="pValue">Valor de la propiedad</param>
        public Property(EnumPropertyType pType, string pCode, string pName, string pValue)
        {
            this.objType = pType;
            this.strCode = pCode;
            this.strName = pName;
            this.strValue = pValue;
        }

        [XmlAttribute("Codigo")]
        public string Code
        {
            get
            {
                return this.strCode;
            }
            set
            {
                this.strCode = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute("Propiedad")]
        public string Name
        {
            get
            {
                return this.strName;
            }
            set
            {
                this.strName = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute("Tipo")]
        public EnumPropertyType Type
        {
            get
            {
                return this.objType;
            }
            set
            {
                this.objType = value;
            }
        }

        [XmlAttribute("Valor")]
        public string Value
        {
            get
            {
                return this.strValue;
            }
            set
            {
                this.strValue = value;
                this.OnPropertyChanged();
            }
        }
    }
}