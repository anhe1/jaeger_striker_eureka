﻿using System;
using System.Linq;
using SqlSugar;

namespace Jaeger.Entities
{
    [SugarTable("_ctlarea")]
    public class ViewModelArea : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private int indiceField;
        private bool activoField = true;
        private string creoField;
        private string modificaField;
        private string nombreField;
        private string descripcionField;
        private DateTime fechaNuevoField;
        private DateTime? fechaModificaField;

        public ViewModelArea()
        {
            
        }

        [SugarColumn(ColumnName = "_ctlarea_id", ColumnDescription = "indice", IsIdentity = true, IsPrimaryKey = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlarea_a", ColumnDescription = "registro activo", IsNullable = false)]
        public bool Activo
        {
            get
            {
                return this.activoField;
            }
            set
            {
                this.activoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlarea_usr_n", ColumnDescription = "clave del usuario que crea el registro", IsNullable = false)]
        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlarea_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsNullable = true)]
        public string Modifica
        {
            get
            {
                return this.modificaField;
            }
            set
            {
                this.modificaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlarea_nom", ColumnDescription = "nombre del area", IsNullable = false, Length = 36)]
        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlarea_desc", ColumnDescription = "descripcion del area", IsNullable = true, Length = 128)]
        public string Descripcion
        {
            get
            {
                return this.descripcionField;
            }
            set
            {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlarea_fn", ColumnDescription = "fecha del sistema", IsNullable = false)]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
            }
        }

        [SugarColumn(ColumnName = "_ctlarea_fm", ColumnDescription = "fecha de ultima modificacion del registro", IsNullable = true)]
        public DateTime? FechaModifica
        {
            get
            {
                return this.fechaModificaField;
            }
            set
            {
                this.fechaModificaField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
