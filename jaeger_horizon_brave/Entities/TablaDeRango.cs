using Newtonsoft.Json;

namespace Jaeger.Entities
{
    [JsonObject("tabla")]
    public class TablaDeRango : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private decimal rango1Field;
        private decimal rango2Field;
        private decimal factorField;
        private string descripcionField;

        [JsonProperty("rango1")]
        public decimal Rango1
        {
            get
            {
                return this.rango1Field;
            }
            set
            {
                this.rango1Field = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("rango2")]
        public decimal Rango2
        {
            get
            {
                return this.rango2Field;
            }
            set
            {
                this.rango2Field = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("factor")]
        public decimal Factor
        {
            get
            {
                return this.factorField;
            }
            set
            {
                this.factorField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("desc")]
        public string Descripcion
        {
            get
            {
                return this.descripcionField;
            }
            set
            {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }
    }
}