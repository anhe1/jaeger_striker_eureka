﻿namespace Jaeger.Entities
{
    public partial class ProcessCompleted
    {
        public ProcessCompleted()
        {

        }

        public ProcessCompleted(string data, string logError)
        {
            this.Data = data;
            this.LogError = logError;
        }

        public string Data { get; set; }

        public string LogError { get; set; }
    }
}
