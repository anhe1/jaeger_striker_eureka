﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Text;
using System.Text.RegularExpressions;

namespace Jaeger.Entities.Layout
{
    public class BancomerTraspasoPagosInterbancarios : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        BindingList<BancomerTraspasoPagosInterbancariosRow> itemsField;

        public BancomerTraspasoPagosInterbancarios()
        {
            this.itemsField = new BindingList<BancomerTraspasoPagosInterbancariosRow>();
        }

        public BindingList<BancomerTraspasoPagosInterbancariosRow> Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
                this.OnPropertyChanged();
            }
        }

        public string Crear(string denominacion)
        {
            string archivo = "";
            foreach (BancomerTraspasoPagosInterbancariosRow item in this.itemsField)
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(this.CloneChain(18 - item.AsuntoBeneficiario.Length,"0") + item.AsuntoBeneficiario);
                stringBuilder.Append(item.AsuntoOrdenante);
                stringBuilder.Append(item.Divisa);
                stringBuilder.Append(this.CloneChain(16 - item.ImporteOperacion.Length, "0") + item.ImporteOperacion);

                // Justificado a la izquierda relleno con espacios
                if (item.TitularAsuntoBeneficiario.Length > 40)
                {
                    stringBuilder.Append(this.Left(item.TitularAsuntoBeneficiario, 40));
                }
                else
                {
                    stringBuilder.Append(item.TitularAsuntoBeneficiario + this.CloneChain(30 - item.TitularAsuntoBeneficiario.Length, " "));
                }

                // Posibles valores 03-Tarjeta de Débito, 40-Cuenta clave
                stringBuilder.Append(item.TipoCuenta);

                stringBuilder.Append(item.NumeroDeBanco);

                // la información aqui plasmada sera enviada al estado de cuenta del ordenante y del beneficiario
                if (item.MotivoDelPago.Length > 30)
                {
                    stringBuilder.Append(this.Left(item.MotivoDelPago, 30));
                }
                else
                {
                    stringBuilder.Append(item.MotivoDelPago + this.CloneChain(30 - item.MotivoDelPago.Length, " "));
                }

                // referencia numerica
                if (item.ReferenciaNumerica.Length > 7)
                {
                    stringBuilder.Append(this.Left(item.ReferenciaNumerica, 7));
                }
                else
                {
                    stringBuilder.Append(this.CloneChain(item.ReferenciaNumerica.Length - 7, " ") + item.ReferenciaNumerica);
                }

                stringBuilder.Append(item.Disponibilidad);
                Jaeger.Util.Helpers.HelperFiles.WriteFileText("C:\\Jaeger\\Jaeger.Temporal\\" + denominacion + ".txt", stringBuilder.ToString() + "\r\n", true);
            }
            return archivo;
        }

        private string CloneChain(int n, string chr)
        {
            string str = "";
            if (chr.Length > 0)
            {
                string str1 = chr.Substring(0, 1);
                int num = n;
                for (int i = 1; i <= num; i = checked(i + 1))
                {
                    str = string.Concat(str, str1);
                }
            }
            return str;
        }

        private string Left(string str, int longitud)
        {
            string str1;
            if (longitud >= 0)
            {
                if (longitud > str.Length)
                {
                    longitud = str.Length;
                }
                str1 = str.Substring(0, longitud);
            }
            else
            {
                str1 = "";
            }
            return str1;
        }
    }

    public partial class BancomerTraspasoPagosInterbancariosRow : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string asuntoBeneficiarioField;
        private string asuntoOrdenanteField;
        private string divisaField;
        private string importeOperacionField;
        private string titularAsuntoBeneficiarioField;
        private string tipoCuentaField;
        private string numeroDeBancoField;
        private string motivoDelPagoField;
        private string referenciaNumericaField;
        private string disponibilidadField;

        public BancomerTraspasoPagosInterbancariosRow()
        {
            this.disponibilidadField = "MXP";
        }

        /// <summary>
        /// (Obligatorio) Asunto Beneficiario: No. de cuenta definida para el abono. Long. = 18, Inicio = 01, Fin = 18
        /// </summary>
        public string AsuntoBeneficiario
        {
            get
            {
                return this.asuntoBeneficiarioField;
            }
            set
            {
                this.asuntoBeneficiarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (Obligatorio) Asunto Ordenante: No. de cuenta definida para el cargo. Long = 18, Inicio = 19, Fin = 36
        /// </summary>
        public string AsuntoOrdenante
        {
            get
            {
                return this.asuntoOrdenanteField;
            }
            set
            {
                this.asuntoOrdenanteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (Obligatorio) Divisa de la Operación: Solo divisa MXP (Pesos mexicanos). Long. = 3, Inicio = 37, Fin = 39
        /// </summary>
        public string Divisa
        {
            get
            {
                return this.divisaField;
            }
            set
            {
                this.divisaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (Obligatorio) Importe de la Operación: Justificado a la derecha relleno con ceros formato: ####.dd. Long. = 16, Inicio = 40, Fin = 55
        /// </summary>
        public string ImporteOperacion
        {
            get
            {
                return this.importeOperacionField;
            }
            set
            {
                this.importeOperacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (Obligatorio) Titular Asunto Beneficiario: Justificado a la izquierda relleno con espacios. Long. = 40, Inicio = 56 Fin = 85
        /// </summary>
        public string TitularAsuntoBeneficiario
        {
            get
            {
                return this.titularAsuntoBeneficiarioField;
            }
            set
            {
                this.titularAsuntoBeneficiarioField = value.ToUpper();
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (Obligatorio) Tipo de Cuenta: Posibles valores 03 Tarjeta de Débito, 40 Cuenta CLABE. Long. = 2, Inicio = 86, Fin = 87
        /// </summary>
        public string TipoCuenta
        {
            get
            {
                return this.tipoCuentaField;
            }
            set
            {
                this.tipoCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (Obligatorio) Número de Banco del Asunto Beneficiario: Número Oficial BANXICO, Consultar catálogo en Banxico, se consideran los 3 últimos campos. Long. = 3, Inicio = 88, Fin = 90
        /// </summary>
        public string NumeroDeBanco
        {
            get
            {
                return this.numeroDeBancoField;
            }
            set
            {
                this.numeroDeBancoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (Obligatorio) Motivo de Pago: La referencia aquí plasmada será enviada al estado de cuenta del ordenante y del beneficiario. Long. = 30, Inicio = 91, Fin = 120
        /// </summary>
        public string MotivoDelPago
        {
            get
            {
                return this.motivoDelPagoField;
            }
            set
            {
                this.motivoDelPagoField = value.ToUpper();
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (Obligatorio) Referencia Numerica: La referencia aquí plasmada será enviada al estado de cuenta del ordenante y del beneficiario. Long. = 7, Inicio = 121, Fin = 127
        /// </summary>
        public string ReferenciaNumerica
        {
            get
            {
                return this.referenciaNumericaField;
            }
            set
            {
                this.referenciaNumericaField = Regex.Replace(value, "[^\\d]", "");
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (Obligatorio) Disponibilidad: Posibles valores H = Mismo día vía SPEI, M = Día siguiente vía CECOBAN. Long. = 1, Inicio = 128, Fin = 128
        /// </summary>
        public string Disponibilidad
        {
            get
            {
                return this.disponibilidadField;
            }
            set
            {
                this.disponibilidadField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
