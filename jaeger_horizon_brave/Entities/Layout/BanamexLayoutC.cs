﻿/// develop: anhe 20072121
/// purpose: El Layout C es uno de dos layouts utilizados dentro de  Pagos / Cobros Masivos (TEF) en BancaNet Empresarial y Digitem. Este Layout es capaz de operar transacciones con todas las naturalezas 
/// (04,05,06,07,11 y 12). 
/// Dentro de este archivo se podrá observar un Formato de Importación y uno de Exportación. Formato de Importación es el cómo debe armar el cliente su archivo para ser transmitido al Banco y el Formato 
/// de Exportación es la respuesta que arma el Banco para entregarla al cliente con los movimientos aplicados. 
using System;
using System.Linq;
using System.ComponentModel;
using System.Text;
using Jaeger.Edita.V2.Contable.Entities;

namespace Jaeger.Entities.Layout
{
    public class BanamexLayoutC : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private BanamexRegistroControl controlField;
        private BanamexRegistroGlobal globalField;
        private BindingList<BanamexRegistroCargoOAbono> movimientosField;
        private BanamexRegistroTotales totalesField;

        public BanamexLayoutC()
        {
            this.controlField = new BanamexRegistroControl();
            this.globalField = new BanamexRegistroGlobal();
            this.movimientosField = new BindingList<BanamexRegistroCargoOAbono>();
            this.totalesField = new BanamexRegistroTotales();
        }

        public BanamexRegistroControl Control
        {
            get
            {
                return this.controlField;
            }
            set
            {
                this.controlField = value;
                this.OnPropertyChanged();
            }
        }

        public BanamexRegistroGlobal Global
        {
            get
            {
                return this.globalField;
            }
            set
            {
                this.globalField = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<BanamexRegistroCargoOAbono> Movimientos
        {
            get
            {
                return this.movimientosField;
            }
            set
            {
                this.movimientosField = value;
                this.OnPropertyChanged();
            }
        }

        public BanamexRegistroTotales Totales
        {
            get
            {
                return this.totalesField;
            }
            set
            {
                this.totalesField = value;
            }
        }

        public string Create(ViewModelPrePoliza objeto, bool clabe)
        {
            // registro de control - importación
            this.controlField.TipoRegistro = 1;
            this.controlField.NumeroIdentificacion = Jaeger.Helpers.DbConvert.ConvertInt32(objeto.Emisor.NumCliente);
            this.controlField.FechaPago = DateTime.Now;
            this.controlField.SecuenciaArchivo = 1;
            this.controlField.NombreEmpresa = objeto.Emisor.Nombre;
            this.controlField.Descripcion = objeto.Concepto;
            this.controlField.NaturalezaArchivo = 12;
            this.controlField.Instrucciones = "";
            this.controlField.VersionLayout = "C";
            this.controlField.Volumen = 0;
            this.controlField.Caracteristica = 0;
            // registro global de importacion
            this.Global.TipoRegistro = 2;
            this.Global.TipoOperacion = 1;
            this.Global.TipoCuenta = 1;
            this.Global.NumeroSucursal = Jaeger.Helpers.DbConvert.ConvertInt32(objeto.Emisor.Sucursal);
            this.Global.NumeroCuenta = objeto.Emisor.NumCta;
            this.Global.Importe = objeto.Abono;


            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(this.Control.Create() + "\r\n");
            stringBuilder.Append(this.Global.Create() + "\r\n");
            
            foreach (ViewModelPrePolizaComprobante item in objeto.Comprobantes)
            {
                BanamexRegistroCargoOAbono nuevo = new BanamexRegistroCargoOAbono();
                nuevo.TipoRegistro = 3;
                nuevo.TipoOperacion = 0;
                nuevo.ClaveMoneda = 1;
                nuevo.Importe = item.Cargo;
                nuevo.TipoCuenta = 1;

                if (clabe == true)
                {
                    nuevo.NumeroCuenta = objeto.Receptor.Clabe;
                }
                else
                {
                    if (objeto.Receptor.NumCta == null)
                    {
                        nuevo.NumeroCuenta = objeto.Receptor.Clabe;
                    }
                    else
                    {
                        nuevo.NumeroCuenta = objeto.Receptor.NumCta;
                    }
                }

                if (nuevo.ReferenciaAlfanumerica != null)
                {
                    if (nuevo.ReferenciaAlfanumerica.Trim().Length > 0)
                    {
                        nuevo.ReferenciaAlfanumerica = objeto.Receptor.RefAlfaNumerica;
                    }
                }
                if (nuevo.ReferenciaAlfanumerica.Trim().Length == 0) 
                {
                    nuevo.ReferenciaAlfanumerica = "PAGO FAC " + item.Folio.ToString();
                }

                if (objeto.Receptor.RefNumerica != null)
                {
                    if (objeto.Receptor.RefNumerica.Trim().Length > 0)
                    {
                        nuevo.ReferenciaNumerica = objeto.Receptor.RefNumerica;
                    }
                }
                if (nuevo.ReferenciaNumerica.Length == 0) 
                {
                    nuevo.ReferenciaNumerica = DateTime.Now.ToString("ddMMyy");
                }
                
                nuevo.ClaveBanco = objeto.Receptor.Codigo;

                if (objeto.Receptor.PrimerApellido.Trim().Length > 0 && objeto.Receptor.SegundoApellido.Trim().Length > 0)
                {
                    nuevo.Beneficiario = string.Concat(objeto.Receptor.Nombres, ",", objeto.Receptor.PrimerApellido, "/", objeto.Receptor.SegundoApellido);
                }
                else
                {
                    nuevo.Beneficiario = string.Concat(objeto.Receptor.Nombre, ",/");
                }

                
                stringBuilder.Append(nuevo.Create() + "\r\n");
            }

            this.Totales.NumeroAbonos = 1;
            this.Totales.ImporteTotalAbonos = objeto.Abono;
            this.Totales.NumeroCargos = objeto.Comprobantes.Count;
            this.Totales.ImporteTotalCargos = objeto.Comprobantes.Sum<ViewModelPrePolizaComprobante>(c => c.Cargo); 
            stringBuilder.Append(this.Totales.Create() + "\r\n");
            
            return stringBuilder.ToString();
        }
    }

    public partial class BanamexRegistroControl : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private int tipoRegistroField;
        private int numeroIdentificacionField;
        private DateTime fechaPagoField;
        private int secuenciaArchivoField;
        private string nombreEmpresaField;
        private string descripcionField;
        private int naturalezaArchivoField;
        private string instruccionesField;
        private string versionLayoutField;
        private int volumenField;
        private int caracteristicaField;

        public BanamexRegistroControl()
        {
            this.versionLayoutField = "C";
            this.volumenField = 0;
            this.caracteristicaField = 0;
        }

        /// <summary>
        /// Registro de Control, es constante y siempre es = 1. 
        /// Long:01 No. Campo: 01 al 01
        /// </summary>
        public int TipoRegistro
        {
            get
            {
                return this.tipoRegistroField;
            }
            set
            {
                this.tipoRegistroField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Número de contrato de B.E. justificado a la derecha  y completar con ceros a la izquierda 
        /// Long:12 No. Campo: 02 al 13
        /// </summary>
        public int NumeroIdentificacion
        {
            get
            {
                return this.numeroIdentificacionField;
            }
            set
            {
                this.numeroIdentificacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Fecha en que se realizará el cargo a su cuenta DDMMAA, donde: DD = Dia MM = Mes AA = Año
        /// Long:06 No. Campo: 14 al 19
        /// </summary>
        public DateTime FechaPago
        {
            get
            {
                return this.fechaPagoField;
            }
            set
            {
                this.fechaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Número consecutivo del archivo, pueden ser de 0001 hasta 0099, No pueden repetirse secuenciales para la misma fecha de pago. 
        /// Long:04 No. Campo: 20 al 23
        /// </summary>
        public int SecuenciaArchivo
        {
            get
            {
                return this.secuenciaArchivoField;
            }
            set
            {
                this.secuenciaArchivoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Rzón social de la empresa, justificado a la izquierda y completar con espacios en blanco.
        /// Long:36 No. Campo: 24 al 59
        /// </summary>
        public string NombreEmpresa
        {
            get
            {
                return this.nombreEmpresaField;
            }
            set
            {
                this.nombreEmpresaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Descripción del tipo de pago que esta realizando. Esta descripción se mostrará en su estado de cuenta de Banca Electrónica e impreso. Justificado a la izquierda y completar con espacios en blanco. 
        /// Long:20 No. Campo: 60 al 79
        /// </summary>
        public string Descripcion
        {
            get
            {
                return this.descripcionField;
            }
            set
            {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Las naturalezas (o tipos de archivos) pueden ser: 04 = Cobros, 05 = Pago de Nómina (Pagomático), a cuentas Banamex, 06 = Pago a proveedores (Incluye Orden de Pago), 07 =Nómina Interbancaria, 11 =Pensiones Interbancarias, 12 = Pagos interbancarios CLABE 
        /// Long:02 No. Campo: 80 al 81
        /// </summary>
        public int NaturalezaArchivo
        {
            get
            {
                return this.naturalezaArchivoField;
            }
            set
            {
                this.naturalezaArchivoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Campo opcional, sólo se utiliza para órdenes de pago, justificado a la izquierda  y completar con blancos. 
        /// Long:40 No. Campo: 82 al 121
        /// </summary>
        public string Instrucciones
        {
            get
            {
                return this.instruccionesField;
            }
            set
            {
                this.instruccionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Constante y siempre es C (version del layput)
        /// Long:01 No. Campo: 122 al 122
        /// </summary>
        public string VersionLayout
        {
            get
            {
                return this.versionLayoutField;
            }
            set
            {
                this.versionLayoutField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Constante y siempre es C (version del layput)
        /// Long:01 No. Campo: 123 al 123
        /// </summary>
        public int Volumen
        {
            get
            {
                return this.volumenField;
            }
            set
            {
                this.volumenField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 0 = Si es modificable (permite edición en el software) 1 = Si es de sólo lectura (no permite edicion en el software)
        /// Long:01 No. Campo: 124 al 124
        /// </summary>
        public int Caracteristica
        {
            get
            {
                return this.caracteristicaField;
            }
            set
            {
                this.caracteristicaField = value;
                this.OnPropertyChanged();
            }
        }

        public string Create()
        {
            return string.Concat(this.tipoRegistroField.ToString(),
                                 this.numeroIdentificacionField.ToString().PadLeft(12, '0'),
                                 this.fechaPagoField.ToString("ddMMyy"),
                                 this.secuenciaArchivoField.ToString().PadLeft(4, '0'),
                                 this.nombreEmpresaField.ToUpper().PadRight(36, ' '),
                                 this.descripcionField.ToUpper().PadRight(20, ' '),
                                 this.naturalezaArchivoField.ToString().PadRight(2, '0'),
                                 this.instruccionesField.PadLeft(40, ' '),
                                 this.versionLayoutField,
                                 this.volumenField.ToString(),
                                 this.caracteristicaField.ToString());
        }
    }

    public partial class BanamexRegistroGlobal : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private int tipoRegistroField;
        private int tipoOperacionField;
        private int claveMonedaField;
        private int tipoCuentaField;
        private decimal importeField;
        private int numeroSucursalField;
        private string numeroCuentaField;

        public BanamexRegistroGlobal()
        {
            this.tipoRegistroField = 2;
            this.claveMonedaField = 1;
            this.numeroCuentaField = "";
        }

        /// <summary>
        /// Es constante y siempre es  = 2 (Registro global) 
        /// Long:01 No. Campo: 01 al 01
        /// </summary>
        public int TipoRegistro
        {
            get
            {
                return this.tipoRegistroField;
            }
            set
            {
                this.tipoRegistroField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 0 = Si es Abono (solo aplica para cobros) 1 = Si es Cargo 
        /// Long:01 No. Campo: 02 al 02
        /// </summary>
        public int TipoOperacion
        {
            get
            {
                return this.tipoOperacionField;
            }
            set
            {
                this.tipoOperacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Es constante y siempre es = 001 (Pesos M.N.) 
        /// Long: 03 No. Campo: 03 al 05
        /// </summary>
        public int ClaveMoneda
        {
            get
            {
                return this.claveMonedaField;
            }
            set
            {
                this.claveMonedaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Justificado a la derecha, no lleva punto decimal. 
        /// Long: 16 Enteros y 02 decimales No. Campo: 06 al 23 
        /// </summary>
        public decimal Importe
        {
            get
            {
                return Math.Round(this.importeField, 2);
            }
            set
            {
                this.importeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 01 = Si es Cheques 
        /// Long:04 No. Campo: 24 al 25
        /// </summary>
        public int TipoCuenta
        {
            get
            {
                return this.tipoCuentaField;
            }
            set
            {
                this.tipoCuentaField = value;
            }
        }

        /// <summary>
        /// Clave de sucursal de la cuenta de cargo o abono, justificar a la derecha y completar con ceros.  
        /// Long:04 No. Campo: 26 al 29
        /// </summary>
        public int NumeroSucursal
        {
            get
            {
                return this.numeroSucursalField;
            }
            set
            {
                this.numeroSucursalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Número de cuenta de cargo o abono, justificar a la derecha y completar con ceros. 
        /// Long:20 Ini:30 Fin:49
        /// </summary>
        public string NumeroCuenta
        {
            get
            {
                return this.numeroCuentaField;
            }
            set
            {
                this.numeroCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        public string Create()
        {
            return string.Concat(
                this.tipoRegistroField.ToString(), 
                this.tipoOperacionField.ToString(),
                this.claveMonedaField.ToString().PadLeft(3, '0'), 
                this.importeField.ToString("#0.00").PadLeft(19, '0').Replace(".", ""), 
                this.tipoCuentaField.ToString("00"), 
                this.numeroSucursalField.ToString().PadLeft(4, '0'), 
                this.numeroCuentaField.PadLeft(20, '0'), 
                "".PadLeft(20, ' '));
        }
    }

    public partial class BanamexRegistroCargoOAbono : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private int tipoRegistroField;
        private int tipoOperacionField;
        private int claveMonedaField;
        private decimal importeField;
        private int tipoCuentaField;
        private string numeroCuentaField;
        private string referenciaAlfanumericaField;
        private string beneficiarioField;
        private string intruccionesField;
        private string descripcionField;
        private string claveBancoField;
        private string referenciaNumericaField;
        private string plazoField;

        public BanamexRegistroCargoOAbono()
        {

            this.tipoRegistroField = 3;
            this.tipoOperacionField = 0;
            this.claveMonedaField = 1;
            this.importeField = 0;
            this.tipoCuentaField = 1;
            this.numeroCuentaField = "";
            this.referenciaAlfanumericaField ="";
            this.beneficiarioField = "";
            this.intruccionesField = "";
            this.descripcionField = "";
            this.claveBancoField = "";
            this.referenciaNumericaField = "";
            this.plazoField = "00";
        }

        #region propiedades

        /// <summary>
        /// Es constante y siempre es = 3 (Registro de Movtos. Individuales) 
        /// Long:01 Ini:01 Fin:01
        /// </summary>
        public int TipoRegistro
        {
            get
            {
                return this.tipoRegistroField;
            }
            set
            {
                this.tipoRegistroField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 0 = Si es Abono 1 = Si es Cargo (solo aplica para cobros)
        /// Long:01 Ini: 02 Fin: 02
        /// </summary>
        public int TipoOperacion
        {
            get
            {
                return this.tipoOperacionField;
            }
            set
            {
                this.tipoOperacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Es constante y siempre es = 001 (Pesos M.N.)
        /// Long:03 Ini:06 Fin:23
        /// </summary>
        public int ClaveMoneda
        {
            get
            {
                return this.claveMonedaField;
            }
            set
            {
                this.claveMonedaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Importe a abonar o cargar, justificado a la derecha sin punto decimal. 
        /// Long: 16 Enterios y 2 decimales Ini:06 Fin:23
        /// </summary>
        public decimal Importe
        {
            get
            {
                return this.importeField;
            }
            set
            {
                this.importeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Clave para la cuenta de abono: 
        /// 01=Cheques / CLABE  
        /// 03=Plásticos
        /// 04=Orden de pago
        /// 15=Cuenta concentradoraNota. 
        /// La CLABE solo seráreconocida para pagoInterbancario, Naturalezas: 07,11
        /// Long:2 Ini:24 Fin:25
        /// </summary>
        public int TipoCuenta
        {
            get
            {
                return this.tipoCuentaField;
            }
            set
            {
                this.tipoCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Numero de cuenta de abono o cargo, justificado a la derecha y completar con ceros. Para las naturalezas: 04, 05, 06; la longitud de la cuenta debe ser 11 posiciones considerando 7 para la cuenta y 4 para la sucursal. Si en cualquiera de los casos la longitud es menos, se debera completar con ceros a la izquierda. Ej.: 00000000003210054321=suc:321 y cuenta :54321 
        /// Long:20 Ini:26 Fin:45
        /// </summary>
        public string NumeroCuenta
        {
            get
            {
                if (this.numeroCuentaField == null)
                {
                    return string.Empty;
                }
                return this.numeroCuentaField;
            }
            set
            {
                this.numeroCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Debe ser diferente de cero. Para el caso de abono a cuentas Banamex (naturaleza 06 y 06) el campo es numerico de 1. posiciones. Los restantes 30 llenar con espacios. Solo para el caso de Pagos Interbancarios es alfanumerico de 40. Esta referencia se reflejara en el estao de cuenta impreso de su beneficiario.
        /// Long:40 Ini:46 Fin:85
        /// </summary>
        public string ReferenciaAlfanumerica
        {
            get
            {
                return this.referenciaAlfanumericaField;
            }
            set
            {
                this.referenciaAlfanumericaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Orden de pago y pagos interbancarios: Si es persona fisica deberá llenarse Nombre(s), Apellido Paterno/Apellido Materno y Si es Persona Moral: Razon Social/ 
        /// Long:55 Ini:86 Fin:140
        /// </summary>
        public string Beneficiario
        {
            get
            {
                return this.beneficiarioField;
            }
            set
            {
                this.beneficiarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Este campo solo aplicara para pagos a cuentas Banamex (naturaleza 05 y 06). Este campo es la referencia alfanumerica que se reflejara en el estado de cuenta de Banca Electrónica e impreso de su beneficiario. Long 40 Ini: 141 Fin: 180
        /// </summary>
        public string Intrucciones
        {
            get
            {
                return this.intruccionesField;
            }
            set
            {
                this.intruccionesField = value;
            }
        }

        /// <summary>
        /// Llenar con espacios en blanco. Long: 24 Ini: 205 Fin 208
        /// </summary>
        public string Descripcion
        {
            get
            {
                return this.descripcionField;
            }
            set
            {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Clave debanco de la cuenta de abono, solo para pago interbancario. Se validará de acuerdo a la cuenta CLABE utilizada. Long: 4 Ini: 209 Fin: 215
        /// </summary>
        public string ClaveBanco
        {
            get
            {
                if (this.claveBancoField == null)
                {
                    return string.Empty;
                }
                return this.claveBancoField;
            }
            set
            {
                this.claveBancoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Diferente de 0. Este campo solo aplica para Pagos Interbancarios (naturaleza 07,11 y 12). Este campo es la Referencia Numérica que se reflejará en el estado de cuenta de su beneficiario. Long: 7 Ini: 209 Fin: 215
        /// </summary>
        public string ReferenciaNumerica
        {
            get
            {
                return this.referenciaNumericaField;
            }
            set
            {
                this.referenciaNumericaField = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Aplica únicamente para Pagos Interbancarios. Este campo indica la fecha en que se realizará el abono a la cuenta de sus beneficiarios con respecto al campo de Fecha Valor (encabezado del archivo). 00 = Mismo dia | 24 = 24 hrs. Long.: 2 Ini: 216 Fin: 217
        /// </summary>
        public string Plazo
        {
            get
            {
                return this.plazoField;
            }
            set
            {
                this.plazoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Create()
        {
            return string.Concat(
                this.TipoRegistro.ToString(),
                this.TipoOperacion.ToString(),
                this.ClaveMoneda.ToString("000"),
                this.Importe.ToString("#0.00").PadLeft(19, '0').Replace(".", ""),
                this.TipoCuenta.ToString("00"),
                this.NumeroCuenta.PadLeft(20, '0'),
                this.ReferenciaAlfanumerica.PadRight(40, ' '),
                string.Concat(this.Beneficiario).ToUpper().PadRight(55, ' '),
                this.Intrucciones.PadRight(40, ' '),
                this.Descripcion.ToUpper().PadRight(24, ' '),
                this.ClaveBanco.PadLeft(4, '0'),
                this.ReferenciaNumerica.PadLeft(7, '0'),
                this.Plazo.PadLeft(2, '0'));
        }
        #endregion
    }

    public partial class BanamexRegistroTotales : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private int tipoRegistroField;
        private string claveMonedaField;
        private int numeroAbonosField;
        private decimal importeTotalAbonosField;
        private int numeroCargosField;
        private decimal importeTotalCargosField;

        public BanamexRegistroTotales()
        {
            this.tipoRegistroField = 4;
            this.claveMonedaField = "001";
            this.numeroAbonosField = 0;
            this.importeTotalAbonosField = 0;
            this.numeroCargosField = 0;
            this.importeTotalCargosField = 0;
        }

        /// <summary>
        /// Es constante y siempre es = 4 (Registro de totales)
        /// Long: 01 No. Campo: 01 al 01
        /// </summary>
        public int TipoRegistro
        {
            get
            {
                return this.tipoRegistroField;
            }
            set
            {
                this.tipoRegistroField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Es constante y siempre es = 001(Pesos M.N.) 
        /// Long: 03 No. Campo: 02 al 04
        /// </summary>
        public string ClaveMoneda
        {
            get
            {
                return this.claveMonedaField;
            }
            set
            {
                this.claveMonedaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Número total de abonos del archivo. 
        /// Long: 06 No. Campo: 05 al 10 
        /// </summary>
        public int NumeroAbonos
        {
            get
            {
                return this.numeroAbonosField;
            }
            set
            {
                this.numeroAbonosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Sumatoria de abonos
        /// Long: 16 Enteros 02 decimales No. Campo: 11 al 28 
        /// </summary>
        public decimal ImporteTotalAbonos
        {
            get
            {
                return this.importeTotalAbonosField;
            }
            set
            {
                this.importeTotalAbonosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Número de cargos del archivo
        /// Long: 06 No. Campo: 29 al 34
        /// </summary>
        public int NumeroCargos
        {
            get
            {
                return this.numeroCargosField;
            }
            set
            {
                this.numeroCargosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Sumatoria de los cargos
        /// Long: 16 Enteros 02 decimales No. Campo: 35 al 52
        /// </summary>
        public decimal ImporteTotalCargos
        {
            get
            {
                return this.importeTotalCargosField;
            }
            set
            {
                this.importeTotalCargosField = value;
                this.OnPropertyChanged();
            }
        }

        public string Create()
        {
            return string.Concat(this.TipoRegistro.ToString(),
                                this.ClaveMoneda.ToString().PadLeft(3, '0'),
                                this.NumeroAbonos.ToString().PadLeft(6, '0'),
                                this.ImporteTotalAbonos.ToString("#0.00").PadLeft(19, '0').Replace(".", ""),
                                this.NumeroCargos.ToString().PadLeft(6, '0'),
                                this.ImporteTotalCargos.ToString("#0.00").PadLeft(19, '0').Replace(".", ""));
        }
    }
}
