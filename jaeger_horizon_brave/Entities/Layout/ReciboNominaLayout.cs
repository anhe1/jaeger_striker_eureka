﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Entities.Layout
{
    public class ReciboNominaLayout : IDataErrorInfo
    {
        private readonly DateTime firstGoodDate = new DateTime(1900, 1, 1);
        private DateTime? fechqInicialPago;
        private DateTime? fechaFinalPago;
        private DateTime? fechaInicioRelLaboral;
        private DateTime? fechaPago;

        [DisplayName("Emisor (RFC)")]
        [DataNames("RfcEmisor")]
        public string RFCEmisor { get; set; }

        [DisplayName("Nombre del Emisor")]
        [DataNames("NombreEmisor")]
        public string NombreEmisor { get; set; }

        [DisplayName("Regimen Fiscal")]
        [DataNames("RegimenFiscal")]
        public string RegimenFiscal { get; set; }

        [DisplayName("Registro Patronal")]
        [DataNames("RegistroPatronal")]
        public string RegistroPatronal { get; set; }

        [DisplayName("Tio Nómina")]
        [DataNames("TipoNomina")]
        public string TipoNomina { get; set; }

        [DisplayName("Fecha de Pago")]
        [DataNames("FechaPago")]
        public DateTime? FechaPago
        {
            get
            {
                if (this.fechaPago >= this.firstGoodDate)
                    return this.fechaPago;
                return null;
            }
            set
            {
                if (value >= this.firstGoodDate)
                    this.fechaPago = value;
                else
                    this.fechaPago = null;
            }
        }

        [DisplayName("Fecha Inicial de Pago")]
        [DataNames("FechaInicialPago")]
        public DateTime? FechaInicialPago
        {
            get
            {
                
                if (this.fechqInicialPago >= firstGoodDate)
                    return this.fechqInicialPago;
                return null;
            }
            set
            {
                if (value >= firstGoodDate)
                    this.fechqInicialPago = value;
                else
                    this.fechqInicialPago = null;
            }
        }

        [DisplayName("Fecha Final de Pago")]
        [DataNames("FechaFinalPago")]
        public DateTime? FechaFinalPago
        {
            get
            {
                if (this.fechaFinalPago >= this.firstGoodDate)
                    return this.fechaFinalPago;
                return null;
            }
            set
            {
                if (value >= this.firstGoodDate)
                    this.fechaFinalPago = value;
                else
                    this.fechaFinalPago = null;
            }
        }

        [DisplayName("Núm. Dias Pagados")]
        [DataNames("NumDiasPagados")]
        public int NumDiasPagados { get; set; }

        [DisplayName("Lugar de Exped.")]
        [DataNames("LugarExpedicion")]
        public string LugarExpedicion { get; set; }

        [DisplayName("Receptor (RFC)")]
        [DataNames("RfcReceptor")]
        public string RfcReceptor { get; set; }

        [DisplayName("Nombre del Receptor")]
        [DataNames("NombreReceptor")]
        public string NombreReceptor { get; set; }

        [DisplayName("Correo")]
        [DataNames("Correo")]
        public string Correo { get; set; }

        [DisplayName("Receptor (CURP)")]
        [DataNames("CurpReceptor")]
        public string CurpReceptor { get; set; }

        [DisplayName("Núm. Seguridad Social")]
        [DataNames("NumSeguridadSocial")]
        public string NumSeguridadSocial { get; set; }

        [DisplayName("Fec. Inicio Rel. Laboral")]
        [DataNames("FechaInicioRelLaboral")]
        public DateTime? FechaInicioRelLaboral
        {
            get
            {
                if (this.fechaInicioRelLaboral >= this.firstGoodDate)
                    return this.fechaInicioRelLaboral;
                else
                    return null;
            }
            set
            {
                if (value >= this.firstGoodDate)
                    this.fechaInicioRelLaboral = value;
                else
                    this.fechaInicioRelLaboral = null;
            }
        }

        [DisplayName("Antigüedad")]
        [DataNames("Antigüedad")]
        public string Antiguedad { get; set; }

        [DisplayName("Tipo Contrato")]
        [DataNames("TipoContrato")]
        public string TipoContrato { get; set; }

        [DisplayName("Tipo Jornada")]
        [DataNames("TipoJornada")]
        public string TipoJornada { get; set; }

        [DisplayName("Tipo Régimen")]
        [DataNames("TipoRegimen")]
        public string TipoRegimen { get; set; }

        [DisplayName("Núm. Empleado")]
        [DataNames("NumEmpleado")]
        public string NumEmpleado { get; set; }

        [DisplayName("Departamento")]
        [DataNames("Departamento")]
        public string Departamento { get; set; }

        [DisplayName("Puesto")]
        [DataNames("Puesto")]
        public string Puesto { get; set; }

        [DisplayName("Riesgo Puesto")]
        [DataNames("RiesgoPuesto")]
        public string RiesgoPuesto { get; set; }

        [DisplayName("Periodicidad de Pago")]
        [DataNames("PeriodicidadPago")]
        public string PeriodicidadPago { get; set; }

        [DisplayName("Clave Banco")]
        [DataNames("Banco")]
        public string Banco { get; set; }

        [DisplayName("Cta. Bancaria")]
        [DataNames("CuentaBancaria")]
        public string CuentaBancaria { get; set; }

        [DisplayName("SalarioBaseCotApor")]
        [DataNames("SalarioBaseCotApor")]
        public decimal SalarioBaseCotApor { get; set; }

        [DisplayName("SalarioDiarioIntegrado")]
        [DataNames("SalarioDiarioIntegrado")]
        public decimal SalarioDiarioIntegrado { get; set; }

        [DisplayName("ClaveEntFed")]
        [DataNames("ClaveEntFed")]
        public string ClaveEntFed { get; set; }

        [DisplayName("SubsidioCausado")]
        [DataNames("SubsidioCausado")]
        public decimal SubsidioCausado { get; set; }

        [DisplayName("DiasIncapacidad")]
        [DataNames("DiasIncapacidad")]
        public int DiasIncapacidad { get; set; }

        [DisplayName("TipoIncapacidad")]
        [DataNames("TipoIncapacidad")]
        public string TipoIncapacidad { get; set; }

        [DisplayName("ImporteMonetario")]
        [DataNames("ImporteMonetario")]
        public decimal ImporteMonetario { get; set; }

        [DisplayName("TipoRelacion")]
        [DataNames("TipoRelacion")]
        public string TipoRelacion { get; set; }

        [DisplayName("UUIDRelacionado")]
        [DataNames("UUIDRelacionado")]
        public string UUIDRelacionado { get; set; }

        [Browsable(false)]
        public string Error
        {
            get
            {
                if (!this.IsCorreoValido() || !this.IsAntiguedadValido() || !this.IsEmisorRFCValido() || !this.IsReceptorRFCValido() || !this.IsRegistroPatronalValido() || !this.IsNumEmpleadoValido())
                    return "Atención en esta fila!";
                return string.Empty;
            }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "Correo" && !this.IsCorreoValido())
                    return "Correo no válido";
                if (columnName == "Antiguedad" && !this.IsAntiguedadValido())
                    return "Expresar la fecha de inicio de la relación laboral entre el empleador y el empleado";
                if (columnName == "RfcReceptor" && !this.IsReceptorRFCValido())
                    return "RFC del receptor no válido.";
                if (columnName == "RegistroPatronal" && this.IsRegistroPatronalValido())
                    return "Formato de registro patronal no válido";
                if (columnName == "NumEmpleado" && !this.IsNumEmpleadoValido())
                    return "Núm. de empleado no válido!";
                return string.Empty;
            }
        }

        private bool RegexValido(string valor, string patron)
        {
            if (!(valor == null))
            {
                bool flag = Regex.IsMatch(valor, string.Concat("^", patron, "$"));
                return flag;
            }
            return false;
        }

        private bool IsCorreoValido()
        {
            return this.RegexValido(this.Correo, "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
        }

        private bool IsAntiguedadValido()
        {
            return this.RegexValido(this.Antiguedad, "P(([1-9][0-9]{0,3})|0)W|P([1-9][0-9]?Y)?(([1-9]|1[012])M)?(0|[1-9]|[12][0-9]|3[01])D");
        }

        private bool IsEmisorRFCValido()
        {
            return this.RegexValido(this.RFCEmisor, "^[a-zA-Z&ñÑ]{3,4}(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\\d])|([0-9]{2})([0][2])([0][1-9]|[1][\\d]|[2][0-8]))(\\w{2}[A|a|0-9]{1})$");
        }

        private bool IsReceptorRFCValido()
        {
            return this.RegexValido(this.RfcReceptor, "^[a-zA-Z&ñÑ]{3,4}(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\\d])|([0-9]{2})([0][2])([0][1-9]|[1][\\d]|[2][0-8]))(\\w{2}[A|a|0-9]{1})$");
        }

        private bool IsRegistroPatronalValido()
        {
            return this.RegexValido(this.RegistroPatronal, "[^|]{1,20}");
        }

        private bool IsNumEmpleadoValido()
        {
            return this.RegexValido(this.NumEmpleado, "[^|]{1,15}");
        }
    }
}
