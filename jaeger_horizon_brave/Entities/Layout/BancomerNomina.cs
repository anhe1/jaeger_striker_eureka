﻿using System.Globalization;
using System;
using System.Linq;
using System.Text;
using System.IO;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Jaeger.Entities.Layout
{
    [JsonObject("bancomer")]
    public class BancomerNomina : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string tituloField;
        private string nombreCompletoField;
        BindingList<BancomerNominaRegistro> itemsField;

        public BancomerNomina()
        {
            this.tituloField = string.Concat("Layout-", DateTime.Now.ToString("yyyymmddhhmmss"));
            this.itemsField = new BindingList<BancomerNominaRegistro>();
        }

        [JsonProperty("titulo")]
        public string Titulo
        {
            get
            {
                return this.tituloField;
            }
            set
            {
                this.tituloField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public string Nombre
        {
            get
            {
                return this.nombreCompletoField;
            }
            set
            {
                this.nombreCompletoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("items")]
        public BindingList<BancomerNominaRegistro> Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
                this.OnPropertyChanged();
            }
        }

        public string Create(string denominacion)
        {
            string archivo = "";
            int contador = 1;
            StringBuilder stringBuilder = new StringBuilder();
            foreach (BancomerNominaRegistro item in this.itemsField)
            {
                item.Secuencia = contador;
                stringBuilder.Append(item.Create() + "\r\n");
                contador = contador + 1;
            }
            Jaeger.Util.Helpers.HelperFiles.WriteFileText(denominacion, stringBuilder.ToString(), false);
            return archivo;
        }

        public bool Save(string destino)
        {
            Jaeger.Util.Helpers.HelperFiles.WriteFileText(destino, JsonConvert.SerializeObject(this));
            return true;
        }

        public static BancomerNomina Load(string archivo)
        {
            if (File.Exists(archivo))
            {
                StreamReader oStreamReader = new StreamReader(archivo);
                string xmlString = oStreamReader.ReadToEnd();
                
                try
                {
                    return JsonConvert.DeserializeObject<BancomerNomina>(xmlString);
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    oStreamReader.Close();
                }
            }
            return null;
        }
    }
}

namespace Jaeger.Entities.Layout
{
    /// <summary>
    /// Nómina Bancomer, Tarjeta de Pagos y Tarjeta Mi Despensa.
    /// </summary>
    [JsonObject("item")]
    public partial class BancomerNominaRegistro : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation, IDataErrorInfo
    {
        private long secuenciaField;
        private string rfcBeneficiarioField;
        private string tipoCuentaField;
        private string numeroCuentaField;
        private double importePagarField;
        private string nombreTrabajadorField;
        private readonly string bancoDestinoField;
        private readonly string plazaDestinoField;

        /// <summary>
        /// constructor
        /// </summary>
        public BancomerNominaRegistro()
        {
            this.secuenciaField = 0;
            this.rfcBeneficiarioField = "";
            this.tipoCuentaField = "99";
            this.numeroCuentaField = "";
            this.importePagarField = 0;
            this.nombreTrabajadorField = "";
            this.bancoDestinoField = "001";
            this.plazaDestinoField = "001";
        }

        /// <summary>
        /// Se refiere al número consecutivo del registro, Debe ser llenado con ceros a la izquierda antes del número consecutivo, Ejemplo: 000000001. (formato: numérico, long: 9, posición 1-9, alineación: Izq.)
        /// </summary>
        [DisplayName("Secuencia")]
        [JsonProperty("sec")]
        public long Secuencia
        {
            get
            {
                return this.secuenciaField;
            }
            set
            {
                this.secuenciaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// No  es  necesario  que  sea  informado  el, Puede ser llenado con ceros o espacios en blanco. (formato: numérico, long: 2, posición 10-25, alineación: Izq.)
        /// </summary>
        [DisplayName("RFC (Beneficiario)")]
        [JsonProperty("rfc")]
        public string RFCBeneficiario
        {
            get
            {
                return this.rfcBeneficiarioField;
            }
            set
            {
                this.rfcBeneficiarioField = value.ToUpper().Trim();
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nombre del trabajador llenado con espacios en blanco a la derecha. (formato: Alanumérico, long: 40, posicion: 63-102, alineacion: Izq.)
        /// </summary>
        [DisplayName("Nombre")]
        [JsonProperty("nombre")]
        public string NombreDelTrabajador
        {
            get
            {
                return string.Format("{0} {1} {2}", this.Nombre, this.PrimerApellido, this.SegundoApellido);
            }
        }

        private string nombreField;
        [Browsable(false)]
        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        private string primerApellidoField;
        public string PrimerApellido
        {
            get
            {
                return this.primerApellidoField;
            }
            set
            {
                this.primerApellidoField = value;
                this.OnPropertyChanged();
            }
        }

        private string segundoApellidoField;
        public string SegundoApellido
        {
            get
            {
                return this.segundoApellidoField;
            }
            set
            {
                this.segundoApellidoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Para Nómina debe ser 99, Para Tarjeta de pagos debe ser 98, Para Tarjeta Mi Despensa debe ser 97. (formato: numérico, long: 2, posición 26-27, alineación: Izq.)
        /// </summary>
        [DisplayName("Tipo de Cuenta")]
        [JsonProperty("tipo")]
        public string TipoDeCuenta
        {
            get
            {
                return this.tipoCuentaField;
            }
            set
            {
                this.tipoCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Para Nómina: La cuenta es de 10 posiciones y en formato BBVA Bancomer, Llenar con espacios en blanco, hasta tener las 20 posiciones, Para Tarjeta de pagos y  Mi Despensa: La  tarjeta  es de 16 posiciones, Llenar con espacios en blanco hasta tener las 20 posiciones. (formato: alfanumérico, long: 20, posición: 28-47, alineación: Izq.)
        /// </summary>
        [DisplayName("Número de Cuenta")]
        [JsonProperty("numCta")]
        public string NumeroDeCuenta
        {
            get
            {
                return this.numeroCuentaField;
            }
            set
            {
                this.numeroCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Debe ser llenado conceros a la izquierda antes de poner el importe, 13 posiciones para los pesos, las dos ultimas posicones para los centavos. el punto es virtual. (formato: numérico, long: 15, posición: 48-62, alineacion: Der.)
        /// </summary>
        [DisplayName("Importe")]
        [JsonProperty("importe")]
        public double ImportePagar
        {
            get
            {
                return Math.Round(this.importePagarField, 2);
            }
            set
            {
                this.importePagarField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Este campo siempre deber ser 001. (formato numérico, long: 3, posicion: 103-105, alineación: Izq.)
        /// </summary>
        [DisplayName("Banco Destino")]
        [JsonProperty("bancoDes")]
        public string BancoDestino
        {
            get
            {
                return this.bancoDestinoField;
            }
        }

        /// <summary>
        /// Este campo siempre deber ser 001. (Formato numérico, long: 3, posicion: 106-108, alineación: Izq.)
        /// </summary>
        [DisplayName("Plaza Destino")]
        [JsonProperty("plaza")]
        public string PlazaDestino
        {
            get
            {
                return this.plazaDestinoField;
            }
        }

        public string Create()
        {
            return string.Concat(
                this.Secuencia.ToString().PadLeft(9, '0'),
                this.RFCBeneficiario.ToUpper().PadLeft(16, ' '),
                this.TipoDeCuenta.PadLeft(2, '0'),
                this.NumeroDeCuenta.PadRight(20, ' '),
                this.ImportePagar.ToString("#0.00").PadLeft(16, '0').Replace(".", ""),
                this.StripAccents(this.NombreDelTrabajador.ToUpper()).PadRight(40, ' '),
                this.BancoDestino,
                this.PlazaDestino);
        }

        public string StripAccents(string word)
        {
            string normal = word.Normalize(NormalizationForm.FormD);

            var converted = normal.Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark);

            return new string(converted.ToArray());
        }

        [JsonIgnore]
        public string this[string columnName]
        {
            get
            {
                if (this.NumeroDeCuenta != null)
                {
                    if (columnName == "NumeroDeCuenta" && this.NumeroDeCuenta.Length < 10)
                    {
                        return "Para Nómina: La cuenta es de 10 posiciones y en formato BBVA Bancomer";
                    }
                }
                return string.Empty;
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        public string Error
        {
            get
            {
                if (this.NumeroDeCuenta != null)
                {
                    if (this.NumeroDeCuenta.Length < 10)
                    {
                        return "Verifica los datos de este registro.";
                    }
                }
                return string.Empty;
            }
        }
    }
}
