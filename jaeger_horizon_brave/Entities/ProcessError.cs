﻿namespace Jaeger.Entities
{
    public partial class ProcessError
    {
        public ProcessError()
        {

        }

        public ProcessError(string data)
        {
            this.Data = data;
        }

        public string Data { get; set; }
    }
}
