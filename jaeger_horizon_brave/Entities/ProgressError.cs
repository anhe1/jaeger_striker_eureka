﻿namespace Jaeger.Entities
{
    /// <summary>
    /// clase para contener error de proceso
    /// </summary>
    public partial class ProgressError
    {
        public ProgressError()
        {

        }

        public ProgressError(string data)
        {
            this.Data = data;
        }

        public string Data { get; set; }
    }
}
