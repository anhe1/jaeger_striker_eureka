﻿namespace Jaeger.Entities
{
    public partial class ProcessStart
    {
        public ProcessStart()
        {

        }

        public ProcessStart(string data)
        {
            this.Data = data;
        }

        public string Data { get; set; }
    }
}
