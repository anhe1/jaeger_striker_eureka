﻿namespace Jaeger.Entities
{
    /// <summary>
    /// clase para contener mensaje de error
    /// </summary>
    public class MessageError
    {
        public MessageError()
        {

        }

        public MessageError(string data)
        {
            this.Data = data;
        }

        public string Data { get; set; }
    }
}
