﻿using System;
using System.Linq;

namespace Jaeger.Entities.Basico
{
    public class DocumentoRelacionado : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string idDocumentoField;
        private string serieField;
        private string folioField;
        private string monedaField;
        private decimal tipoCambioField;
        private string metodoDePagoField;
        private int numParcialidadField;
        private double impSaldoAnteriorField;
        private double impPagadoField;
        private double impSaldoInsolutoField;

        public DocumentoRelacionado()
        {
        }

        public string IdDocumento
        {
            get
            {
                return this.idDocumentoField;
            }
            set
            {
                this.idDocumentoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Serie
        {
            get
            {
                return this.serieField;
            }
            set
            {
                this.serieField = value;
                this.OnPropertyChanged();
            }
        }

        public string Folio
        {
            get
            {
                return this.folioField;
            }
            set
            {
                this.folioField = value;
                this.OnPropertyChanged();
            }
        }

        public string Moneda
        {
            get
            {
                return this.monedaField;
            }
            set
            {
                this.monedaField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal TipoCambio
        {
            get
            {
                return this.tipoCambioField;
            }
            set
            {
                this.tipoCambioField = value;
                this.OnPropertyChanged();
            }
        }

        public string MetodoDePago
        {
            get
            {
                return this.metodoDePagoField;
            }
            set
            {
                this.metodoDePagoField = value;
                this.OnPropertyChanged();
            }
        }

        public int NumParcialidad
        {
            get
            {
                return this.numParcialidadField;
            }
            set
            {
                this.numParcialidadField = value;
                this.OnPropertyChanged();
            }
        }

        public double ImpSaldoAnterior
        {
            get
            {
                return this.impSaldoAnteriorField;
            }
            set
            {
                this.impSaldoAnteriorField = value;
                this.OnPropertyChanged();
            }
        }

        public double ImpPagado
        {
            get
            {
                return this.impPagadoField;
            }
            set
            {
                this.impPagadoField = value;
                this.OnPropertyChanged();
            }
        }

        public double ImpSaldoInsoluto
        {
            get
            {
                return this.impSaldoInsolutoField;
            }
            set
            {
                this.impSaldoInsolutoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
