﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace Jaeger.Entities.Basico
{
    [XmlRoot("Concepto")]
    public class Concepto : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string strClaveProdServ;
        private string strNoIdentificacion;
        private decimal strCantidad;
        private string strClaveUnidad;
        private string strUnidad;
        private string strDescripcion;
        private decimal strValorUnitario;
        private decimal strImporte;
        private decimal strDescuento;
        private string strTipoCambio;
        private string strImpuestosTraslados;
        private string strImpuestosRetenciones;
        private string strInfoAduNumeroPedimento;
        private string strNumeroCuentaPredial;

        public Concepto()
        {
        }

        [DisplayName("Cantidad")]
        [XmlAttribute("Cantidad")]
        public decimal Cantidad
        {
            get
            {
                return this.strCantidad;
            }
            set
            {
                this.strCantidad = value;
                this.OnPropertyChanged();
            }
        }

        [Description("Atributo requerido para expresar la clave del producto o del servicio amparado por la presente parte. Es requerido y deben utilizar las claves del catálogo de productos y servicios, cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.")]
        [DisplayName("Clave de Producto / Servicio")]
        [XmlAttribute("ClaveProdServ")]
        public string ClaveProdServ
        {
            get
            {
                return this.strClaveProdServ;
            }
            set
            {
                this.strClaveProdServ = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Clave Unidad")]
        [XmlAttribute("ClaveUnidad")]
        public string ClaveUnidad
        {
            get
            {
                return this.strClaveUnidad;
            }
            set
            {
                this.strClaveUnidad = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Descripción")]
        [XmlAttribute("Descrip")]
        public string Descripcion
        {
            get
            {
                return this.strDescripcion;
            }
            set
            {
                this.strDescripcion = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Descuento")]
        [XmlAttribute("Descuento")]
        public decimal Descuento
        {
            get
            {
                return this.strDescuento;
            }
            set
            {
                this.strDescuento = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Importe")]
        [XmlAttribute("Importe")]
        public decimal Importe
        {
            get
            {
                return this.strImporte;
            }
            set
            {
                this.strImporte = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute("Retenciones")]
        public string ImpuestosRetenciones
        {
            get
            {
                return this.strImpuestosRetenciones;
            }
            set
            {
                this.strImpuestosRetenciones = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Trasladados")]
        [XmlAttribute("ImpTraslado")]
        public string ImpuestosTraslados
        {
            get
            {
                return this.strImpuestosTraslados;
            }
            set
            {
                this.strImpuestosTraslados = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute("InformacionAduanera")]
        public string InfoAduNumeroPedimento
        {
            get
            {
                return this.strInfoAduNumeroPedimento;
            }
            set
            {
                this.strInfoAduNumeroPedimento = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("No. de Identificación")]
        [XmlAttribute("NoIdentificacion")]
        public string NoIdentificacion
        {
            get
            {
                return this.strNoIdentificacion;
            }
            set
            {
                this.strNoIdentificacion = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Cuenta Predial")]
        [XmlAttribute("CuentaPredial")]
        public string NumeroCuentaPredial
        {
            get
            {
                return this.strNumeroCuentaPredial;
            }
            set
            {
                this.strNumeroCuentaPredial = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Tipo de Cambio")]
        [XmlAttribute("TipoCambio")]
        public string TipoCambio
        {
            get
            {
                return this.strTipoCambio;
            }
            set
            {
                this.strTipoCambio = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Unidad")]
        [XmlAttribute("Unidad")]
        public string Unidad
        {
            get
            {
                return this.strUnidad;
            }
            set
            {
                this.strUnidad = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Valor Unitario")]
        [XmlAttribute("ValorUnitario")]
        public decimal ValorUnitario
        {
            get
            {
                return this.strValorUnitario;
            }
            set
            {
                this.strValorUnitario = value;
                this.OnPropertyChanged();
            }
        }
    }
}