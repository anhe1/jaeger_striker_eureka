﻿using System;
using System.Linq;

namespace Jaeger.Entities.Basico
{
    public class ComplementoPagos10 : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string versionField;
        private string fechaPagoField;
        private string formaDePagoField;
        private string monedaField;
        private decimal tipoDeCambioField;
        private decimal montoField;
        private string numOperacionField;
        private string rfcEmisorCtaOrdField;
        private string nomBancoOrdExtField;
        private string ctaOrdenanteField;
        private string rfcEmisorCtaBenField;
        private string ctaBeneficiarioField;
        private string tipoCadPagoField;
        private string certPagoField;
        private string cadPagoField;
        private string selloPagoField;

        public ComplementoPagos10()
        {
        }

        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        public string FechaPago
        {
            get
            {
                return this.fechaPagoField;
            }
            set
            {
                this.fechaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        public string FormaDePago
        {
            get
            {
                return this.formaDePagoField;
            }
            set
            {
                this.formaDePagoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Moneda
        {
            get
            {
                return this.monedaField;
            }
            set
            {
                this.monedaField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal TipoDeCambio
        {
            get
            {
                return this.tipoDeCambioField;
            }
            set
            {
                this.tipoDeCambioField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Monto
        {
            get
            {
                return this.montoField;
            }
            set
            {
                this.montoField = value;
                this.OnPropertyChanged();
            }
        }

        public string NumOperacion
        {
            get
            {
                return this.numOperacionField;
            }
            set
            {
                this.numOperacionField = value;
                this.OnPropertyChanged();
            }
        }

        public string RfcEmisorCtaOrd
        {
            get
            {
                return this.rfcEmisorCtaOrdField;
            }
            set
            {
                this.rfcEmisorCtaOrdField = value;
                this.OnPropertyChanged();
            }
        }

        public string NomBancoOrdExt
        {
            get
            {
                return this.nomBancoOrdExtField;
            }
            set
            {
                this.nomBancoOrdExtField = value;
                this.OnPropertyChanged();
            }
        }

        public string CtaOrdenante
        {
            get
            {
                return this.ctaOrdenanteField;
            }
            set
            {
                this.ctaOrdenanteField = value;
                this.OnPropertyChanged();
            }
        }

        public string RfcEmisorCtaBen
        {
            get
            {
                return this.rfcEmisorCtaBenField;
            }
            set
            {
                this.rfcEmisorCtaBenField = value;
                this.OnPropertyChanged();
            }
        }

        public string CtaBeneficiario
        {
            get
            {
                return this.ctaBeneficiarioField;
            }
            set
            {
                this.ctaBeneficiarioField = value;
                this.OnPropertyChanged();
            }
        }

        public string TipoCadPago
        {
            get
            {
                return this.tipoCadPagoField;
            }
            set
            {
                this.tipoCadPagoField = value;
                this.OnPropertyChanged();
            }
        }

        public string CertPago
        {
            get
            {
                return this.certPagoField;
            }
            set
            {
                this.certPagoField = value;
                this.OnPropertyChanged();
            }
        }

        public string CadPago
        {
            get
            {
                return this.cadPagoField;
            }
            set
            {
                this.cadPagoField = value;
                this.OnPropertyChanged();
            }
        }

        public string SelloPago
        {
            get
            {
                return this.selloPagoField;
            }
            set
            {
                this.selloPagoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
