﻿// develop: 100120172315
// purpose: datos basicos del contacto para el registro del directorio proveedores o clientes
using System;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Entities.Basico
{
    public class Contacto : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private long indiceField;
        private long subIndiceField;
        private bool isActiveField;
        private string strName = string.Empty;
        private string strSuggestedDeal = string.Empty;
        private string strJob = string.Empty;
        private string strDetails = string.Empty;
        private string strTypePhone = string.Empty;
        private string strTypeEmail = string.Empty;
        private string strPhone = string.Empty;
        private string strEmail = string.Empty;
        private DateTime dateCreateNew;
        private DateTime? dateChangeDate;
        private string creoField;
        private string modificaField;
        private string strDateSpecial = string.Empty;

        [DataNames("_dcntc_id", "_dcntc_id")]
        public long Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_dcntc_a", "_dcntc_a")]
        public bool IsActive
        {
            get
            {
                return this.isActiveField;
            }
            set
            {
                this.isActiveField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_dcntc_drctr_id", "_dcntc_drctr_id")]
        public long SubId
        {
            get
            {
                return this.subIndiceField;
            }
            set
            {
                if (this.subIndiceField != value)
                {
                    this.subIndiceField = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// nombre del contacto
        /// </summary>
        [DataNames("_dcntc_nom", "_dcntc_nom")]
        public string Nombre
        {
            get 
            { 
                return this.strName; 
            }
            set
            {
                this.strName = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// trato sugerido
        /// </summary>
        [DataNames("_dcntc_sug", "_dcntc_sug")]
        public string TratoSugerido
        {
            get
            {
                return this.strSuggestedDeal;
            }
            set
            {
                this.strSuggestedDeal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// puesto
        /// </summary>
        [DataNames("_dcntc_pst", "_dcntc_pst")]
        public string Puesto
        {
            get
            {
                return this.strJob;
            }
            set
            {
                this.strJob = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// detalles
        /// </summary>
        [DataNames("_dcntc_dtlls", "_dcntc_dtlls")]
        public string Detalles
        {
            get
            {
                return this.strDetails;
            }
            set
            {
                this.strDetails = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de telefono
        /// </summary>
        [DataNames("_dcntc_ttlfn", "_dcntc_ttlfn")]
        public string TelefonoTipo
        {
            get
            {
                return this.strTypePhone;
            }
            set
            {
                this.strTypePhone = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de correo electronico
        /// </summary>
        [DataNames("_dcntc_tmail", "_dcntc_tmail")]
        public string CorreoTipo
        {
            get
            {
                return this.strTypeEmail;
            }
            set
            {
                this.strTypeEmail = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// telefono
        /// </summary>
        [DataNames("_dcntc_tlfn", "_dcntc_tlfn")]
        public string Telefono
        {
            get
            {
                return this.strPhone;
            }
            set
            {
                this.strPhone = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// correo electronico
        /// </summary>
        [DataNames("_dcntc_mail", "_dcntc_mail")]
        public string Correo
        {
            get
            {
                return this.strEmail;
            }
            set
            {
                this.strEmail = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha especial
        /// </summary>
        public string FechaEspecial
        {
            get
            {
                return this.strDateSpecial;
            }
            set
            {
                this.strDateSpecial = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_dcntc_usr_n", "_dcntc_usr_n")]
        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_dcntc_usr_m", "_dcntc_usr_m")]
        public string Modifica
        {
            get
            {
                return this.modificaField;
            }
            set
            {
                this.modificaField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_dcntc_fn", "_dcntc_fn")]
        public DateTime FechaNuevo
        {
            get
            {
                return this.dateCreateNew;
            }
            set
            {
                this.dateCreateNew = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_dcntc_fm", "_dcntc_fm")]
        public DateTime? FechaMod
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.dateChangeDate >= firstGoodDate)
                {
                    return this.dateChangeDate.Value;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                this.dateChangeDate = value;
                this.OnPropertyChanged();
            }
        }
    }
}