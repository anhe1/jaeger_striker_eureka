﻿using System;
using System.Linq;

namespace Jaeger.Entities.Basico
{
    public class ReciboComision
    {

        public string TipoText{ get; set; }
        public string Estado{ get; set; }
        public string NoIndet{ get; set; }
        public long Folio{ get; set; }
        public string Serie{ get; set; }
        public DateTime FechaEmision{ get; set; }
        public DateTime? FechaDocto{ get; set; }
        public DateTime? FechaPago{ get; set; }
        public DateTime? FechaVence{ get; set; }

        public string Emisor { get; set; }
        public string EmisorClave { get; set; }
        public string EmisorRfc{ get; set; }
        public string EmisorNumCta{ get; set; }
        public string EmisorBanco { get; set; }
        public string EmisorBancoClave{ get; set; }
        public string EmisorBancoSucursal{ get; set; }
        public string EmisorBancoClabe{ get; set; }
        public string EmisorBancoTipo{ get; set; }

        public string Receptor{ get; set; }
        public string ReceptorClave { get; set; }
        public string ReceptorRfc{ get; set; }
        public string ReceptorNumCta{ get; set; }
        public string ReceptorBanco { get; set; }
        public string ReceptorBancoClave{ get; set; }
        public string ReceptorBancoSucursal{ get; set; }
        public string ReceptorBancoClabe{ get; set; }
        public string ReceptorBancoTipo{ get; set; }

        public string FormaPagoClave{ get; set; }
        public string FormaPagoDescripcion{ get; set; }
        
        public string NumDocto{ get; set; }
        public string NumOperacion{ get; set; }
        public string NumAutorizacion{ get; set; }
        public string Referencia{ get; set; }
        public string Concepto{ get; set; }
        public decimal Abono { get; set; }
        public decimal TotalComprobantes{ get; set; }
        public string Notas{ get; set; }

        public string TotalEnLetra { get; set; }
        public byte[] LogoTipo{ get; set; }
        public byte[] CodigoBarras{ get; set; }

    }
}
