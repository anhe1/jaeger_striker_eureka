﻿using System;
using System.Linq;

namespace Jaeger.Entities.Basico
{
    public class RemisionFiscal
    {
        public string Status { set; get; }
        public string Version { set; get; }
        public string Serie { set; get; }
        public long Folio { set; get; }
        public string UUID { set; get; }
        public DateTime FechaEmision { set; get; }
        public DateTime? FechaEstado { set; get; }
        public DateTime? FechaCancela { set; get; }
        public DateTime? FechaEntrega { set; get; }
        public DateTime? FechaCobro { set; get; }
        public string Emisor { set; get; }
        public string EmisorRfc { set; get; }
        public string Receptor { set; get; }
        public string ReceptorRfc { set; get; }
        public string DomicilioFiscal { set; get; }
        public string DomicilioEntrega { set; get; }
        public decimal SubTotal { set; get; }
        public decimal Descuento { set; get; }
        public decimal TrasladoIva { set; get; }
        public decimal Total { set; get; }
        public decimal Acumulado { set; get; }
        public string CondicionPago { set; get; }
        public string MotivoDescuento { set; get; }
        public string Notas { set; get; }
        public int DiasDeVence { set; get; }
        public int PresionDecimal { set; get; }
        public int NumPedido { set; get; }
        public string Contacto { set; get; }
        public string Vendedor { set; get; }
        public string TotalEnLetra { set; get; }
        public bool IncluyePagare { get; set; }
        public string Pagare { get; set; }
        public string Creo { get; set; }
        public byte[] Cbb { set; get; }
        public byte[] Logo { set; get; }
    }

    public partial class RemisionFiscalConcepto
    {
        
    }

    public partial class RemisionFiscalRelacionado
    {
        public int NumPedido { set; get; }
        public string Serie { set; get; }
        public long Folio { set; get; }
        public DateTime FechaEmision { set; get; }
        public DateTime? FechaCancela { set; get; }
        public string Emisor { set; get; }
        public string EmisorRfc { set; get; }
        public string Receptor { set; get; }
        public string ReceptorRfc { set; get; }
        public decimal Total { set; get; }
    }

    public partial class RemisionFiscalPagare
    {
        public string TextoPagare { set; get; }
        public string Emisor { set; get; }
        public decimal Total { set; get; }
    }
}
