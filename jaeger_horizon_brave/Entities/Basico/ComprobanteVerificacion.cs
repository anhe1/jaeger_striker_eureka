﻿using System;
using System.Linq;

namespace Jaeger.Entities.Basico
{
    public class ComprobanteVerificacion
    {
        private bool readyField;
        private string folioFiscalField;
        private string emisorRFCField;
        private string receptorRFCField;

        public ComprobanteVerificacion()
        {

        }

        public bool Ready
        {
            get
            {
                return this.readyField;
            }
            set
            {
                this.readyField = value;
            }
        }

        public string FolioFiscal
        {
            get
            {
                return this.folioFiscalField;
            }
            set
            {
                this.folioFiscalField = value;
            }
        }

        public string EmisorRFC
        {
            get
            {
                return this.emisorRFCField;
            }
            set
            {
                this.emisorRFCField = value;
            }
        }

        public string ReceptorRFC
        {
            get
            {
                return this.receptorRFCField;
            }
            set
            {
                this.receptorRFCField = value;
            }
        }
    }
}
