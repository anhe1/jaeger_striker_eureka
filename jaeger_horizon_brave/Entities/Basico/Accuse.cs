﻿using System;
using System.Linq;
using Jaeger.Edita.V2.CFDI.Entities.Cancel;

namespace Jaeger.Entities.Basico
{
    public class Accuse
    {
        private DateTime fechaField;
        private string folioYSerieField;
        private string rfcField;
        private string emisorField;
        private string receptorRfcField;
        private string receprtoField;
        private string selloSatField;
        private AcuseFolios[] folio;

        public DateTime Fecha
        {
            get
            {
                return this.fechaField;
            }
            set
            {
                this.fechaField = value;
            }
        }

        public string FolioYSerie
        {
            get
            {
                return this.folioYSerieField;
            }
            set
            {
                this.folioYSerieField = value;
            }
        }

        public string Rfc
        {
            get
            {
                return this.rfcField;
            }
            set
            {
                this.rfcField = value;
            }
        }

        public string Emisor
        {
            get
            {
                return this.emisorField;
            }
            set
            {
                this.emisorField = value;
            }
        }

        public string ReceptorRfc
        {
            get
            {
                return this.receptorRfcField;
            }
            set
            {
                this.receptorRfcField = value;
            }
        }

        public string Receptor
        {
            get
            {
                return this.receprtoField;
            }
            set
            {
                this.receprtoField = value;
            }
        }

        public string SelloSat
        {
            get
            {
                return this.selloSatField;
            }
            set
            {
                this.selloSatField = value;
            }
        }

        public AcuseFolios[] Folios
        {
            get
            {
                return this.folio;
            }
            set
            {
                this.folio = value;
            }
        }
    }
}
