﻿using System;
using System.Linq;
using Jaeger.Enums;
using SqlSugar;

namespace Jaeger.Edita.Entities.Basico
{
    [SugarTable("_ctlcer")]
    public class ViewModelCertificado : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        #region declaraciones
        private int indiceField;
        private bool activoField;
        private EnumOrigenCertificado origenField;
        private string creoField;
        private string rfcField;
        private string serieField;
        private string cerB64Field;
        private string keyB64Field;
        private string keyPassB64Field;
        private DateTime notBeforeField;
        private DateTime notAfterField;
        private DateTime fechaNuevoField;
        private byte[] keyFileField;
        private byte[] cerFileField;
        private string passwordField;
        private string receptorField;
        private string emisorField;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ViewModelCertificado()
        {
            this.rfcField = string.Empty;
            this.serieField = string.Empty;
            this.cerB64Field = string.Empty;
            this.keyB64Field = string.Empty;
            this.keyPassB64Field = string.Empty;
            this.Origen = EnumOrigenCertificado.BaseDeDatos;
        }

        #region propiedades

        [SugarColumn(ColumnName = "_ctlcer_id", IsIdentity = true, IsPrimaryKey = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlcer_a", ColumnDescription = "", DefaultValue = "1")]
        public bool Activo
        {
            get
            {
                return this.activoField;
            }
            set
            {
                this.activoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlcer_usr_n", ColumnDescription = "", Length = 15)]
        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlcer_rfc", ColumnDescription = "rfc del controbiyente", Length = 16)]
        public string RFC
        {
            get
            {
                return this.rfcField;
            }
            set
            {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de serie del certificado
        /// </summary>
        [SugarColumn(ColumnName = "_ctlcer_serie", ColumnDescription = "numero de serie del certificado", Length = 21)]
        public string Serie
        {
            get
            {
                return this.serieField;
            }
            set
            {
                this.serieField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlcer_subjectname", ColumnDescription = "a nombre de", ColumnDataType = "MEDIUMTEXT")]
        public string Receptor
        {
            get
            {
                return this.receptorField;
            }
            set
            {
                this.receptorField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlcer_issuerName", ColumnDescription = "nombre del emisor", ColumnDataType = "TEXT")]
        public string Emisor
        {
            get
            {
                return this.emisorField;
            }
            set
            {
                this.emisorField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlcer_cerb64")]
        public string CerB64
        {
            get
            {
                return this.cerB64Field;
            }
            set
            {
                this.cerB64Field = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlcer_keyb64")]
        public string KeyB64
        {
            get
            {
                return this.keyB64Field;
            }
            set
            {
                this.keyB64Field = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlcer_psw64")]
        public string KeyPassB64
        {
            get
            {
                return this.keyPassB64Field;
            }
            set
            {
                this.keyPassB64Field = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// vigencia del certificado, despues de
        /// </summary>
        [SugarColumn(ColumnName = "_ctlcer_notafter")]
        public DateTime NotAfter
        {
            get
            {
                return this.notAfterField;
            }
            set
            {
                this.notAfterField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlcer_notbefore")]
        public DateTime NotBefore
        {
            get
            {
                return this.notBeforeField;
            }
            set
            {
                this.notBeforeField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlcer_fn")]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlcer_filecer")]
        public byte[] CerFile
        {
            get
            {
                return this.cerFileField;
            }
            set
            {
                this.cerFileField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlcer_filekey")]
        public byte[] KeyFile
        {
            get
            {
                return this.keyFileField;
            }
            set
            {
                this.keyFileField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// indetificar el origen del certificado archivo o base de datos
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public EnumOrigenCertificado Origen
        {
            get
            {
                return this.origenField;
            }
            set
            {
                this.origenField = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
