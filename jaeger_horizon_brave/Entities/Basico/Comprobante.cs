﻿using System;
using System.Linq;

namespace Jaeger.Entities.Basico
{
    public class Comprobante
    {
        // datos del comprobante
        private string versionField;
        private string folioField;
        private string serieField;
        private string fechaField;
        private string efectoField;
        private string tipoDeComprobanteField;
        private string lugarDeExpedicionField;
        private string cuentaDePagoField;
        private string formaDePagoField;
        private string metodoDePagoField;
        private string condicionesDePagoField;
        private string monedaField;
        private string tipoDeCambioField;
        private string noCertificadoSatField;
        private string noCertificadoField;

        private string motivoDescuentoField;
        private string folioFiscalOrigField;
        private string serieFolioFiscalOrigField;
        private DateTime fechaFolioFiscalOrigField;
        private DateTime fechaCertificacionField;
        private decimal montoFolioFiscalOrigField;

        // montos
        private decimal descuentoField;
        private decimal totalImpuestosRetenidosField;
        private decimal totalImpuestosTrasladadosField;
        private decimal subTotalField;
        private decimal totalField;
        private decimal ivaField;
        private decimal isrField;

        // impuestos
        private decimal isrRetencionField;
        private decimal ivaRetencionField;
        private decimal ivaTrasladoField;
        private decimal iepsRetencionField;
        private decimal iepsTrasladoField;

        // datos del TFD
        private string uuidField;
        private string totalEnLetraField;
        private string selloCfdiField;
        private string selloSatField;
        private string cadenaOriginalSatField;
        private string rfcProvCertifField;

        // datos de emisor del comprobante
        private string emisorField;
        private string emisorRfcField;
        private string emisorCurpField;
        private string emisorDireccionField;
        private string emisorNoCertificadoField;
        private string emisorRegimenFiscalField;

        // datos del receptor del comprobante
        private string receptorField;
        private string receptorRfcField;
        private string receptorCurpField;
        private string receptorDireccionField;

        private string residenciaFiscalField;
        private string usoDeCfdiField;
        private string claveDeConfirmacionField;
        private string tipoRelacionField;
        private string cfdiRelacionadoField;

        private byte[] codigoBarras;

        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }

        public string Folio
        {
            get
            {
                return this.folioField;
            }
            set
            {
                this.folioField = value;
            }
        }

        public string Serie
        {
            get
            {
                return this.serieField;
            }
            set
            {
                this.serieField = value;
            }
        }

        public string Fecha
        {
            get
            {
                return this.fechaField;
            }
            set
            {
                this.fechaField = value;
            }
        }

        public string Efecto
        {
            get
            {
                return this.efectoField;
            }
            set
            {
                this.efectoField = value;
            }
        }

        public string TipoDeComprobante
        {
            get
            {
                return this.tipoDeComprobanteField;
            }
            set
            {
                this.tipoDeComprobanteField = value;
            }
        }

        public string LugarDeExpedicion
        {
            get
            {
                return this.lugarDeExpedicionField;
            }
            set
            {
                this.lugarDeExpedicionField = value;
            }
        }

        public string CuentaDePago
        {
            get
            {
                return this.cuentaDePagoField;
            }
            set
            {
                this.cuentaDePagoField = value;
            }
        }

        public string FormaDePago
        {
            get
            {
                return this.formaDePagoField;
            }
            set
            {
                this.formaDePagoField = value;
            }
        }

        public string MetodoDePago
        {
            get
            {
                return this.metodoDePagoField;
            }
            set
            {
                this.metodoDePagoField = value;
            }
        }

        public string CondicionesDePago
        {
            get
            {
                return this.condicionesDePagoField;
            }
            set
            {
                this.condicionesDePagoField = value;
            }
        }

        public string Moneda
        {
            get
            {
                return this.monedaField;
            }
            set
            {
                this.monedaField = value;
            }
        }

        public string TipoDeCambio
        {
            get
            {
                return this.tipoDeCambioField;
            }
            set
            {
                this.tipoDeCambioField = value;
            }
        }

        public string NoCertificadoSat
        {
            get
            {
                return this.noCertificadoSatField;
            }
            set
            {
                this.noCertificadoSatField = value;
            }
        }

        public string NoCertificado
        {
            get
            {
                return this.noCertificadoField;
            }
            set
            {
                this.noCertificadoField = value;
            }
        }

        public string MotivoDescuento
        {
            get
            {
                return this.motivoDescuentoField;
            }
            set
            {
                this.motivoDescuentoField = value;
            }
        }

        public string FolioFiscalOrig
        {
            get
            {
                return this.folioFiscalOrigField;
            }
            set
            {
                this.folioFiscalOrigField = value;
            }
        }

        public string SerieFolioFiscalOrig
        {
            get
            {
                return this.serieFolioFiscalOrigField;
            }
            set
            {
                this.serieFolioFiscalOrigField = value;
            }
        }

        public DateTime FechaFolioFiscalOrig
        {
            get
            {
                return this.fechaFolioFiscalOrigField;
            }
            set
            {
                this.fechaFolioFiscalOrigField = value;
            }
        }

        public DateTime FechaCertificacion
        {
            get
            {
                return this.fechaCertificacionField;
            }
            set
            {
                this.fechaCertificacionField = value;
            }
        }

        public decimal MontoFolioFiscalOrig
        {
            get
            {
                return this.montoFolioFiscalOrigField;
            }
            set
            {
                this.montoFolioFiscalOrigField = value;
            }
        }

        public decimal Descuento
        {
            get
            {
                return this.descuentoField;
            }
            set
            {
                this.descuentoField = value;
            }
        }

        public decimal TotalImpuestosRetenidos
        {
            get
            {
                return this.totalImpuestosRetenidosField;
            }
            set
            {
                this.totalImpuestosRetenidosField = value;
            }
        }

        public decimal TotalImpuestosTrasladados
        {
            get
            {
                return this.totalImpuestosTrasladadosField;
            }
            set
            {
                this.totalImpuestosTrasladadosField = value;
            }
        }

        public decimal SubTotal
        {
            get
            {
                return this.subTotalField;
            }
            set
            {
                this.subTotalField = value;
            }
        }

        public decimal Total
        {
            get
            {
                return this.totalField;
            }
            set
            {
                this.totalField = value;
            }
        }

        public decimal Iva
        {
            get
            {
                return this.ivaField;
            }
            set
            {
                this.ivaField = value;
            }
        }

        public decimal Isr
        {
            get
            {
                return this.isrField;
            }
            set
            {
                this.isrField = value;
            }
        }

        public decimal IsrRetencion
        {
            get
            {
                return this.isrRetencionField;
            }
            set
            {
                this.isrRetencionField = value;
            }
        }

        public decimal IvaRetencion
        {
            get
            {
                return this.ivaRetencionField;
            }
            set
            {
                this.ivaRetencionField = value;
            }
        }

        public decimal IvaTraslado
        {
            get
            {
                return this.ivaTrasladoField;
            }
            set
            {
                this.ivaTrasladoField = value;
            }
        }

        public decimal IepsRetencion
        {
            get
            {
                return this.iepsRetencionField;
            }
            set
            {
                this.iepsRetencionField = value;
            }
        }

        public decimal IepsTraslado
        {
            get
            {
                return this.iepsTrasladoField;
            }
            set
            {
                this.iepsTrasladoField = value;
            }
        }

        public string Uuid
        {
            get
            {
                return this.uuidField;
            }
            set
            {
                this.uuidField = value;
            }
        }

        public string TotalEnLetra
        {
            get
            {
                return this.totalEnLetraField;
            }
            set
            {
                this.totalEnLetraField = value;
            }
        }

        public string SelloCfdi
        {
            get
            {
                return this.selloCfdiField;
            }
            set
            {
                this.selloCfdiField = value;
            }
        }

        public string SelloSat
        {
            get
            {
                return this.selloSatField;
            }
            set
            {
                this.selloSatField = value;
            }
        }

        public string CadenaOriginalSat
        {
            get
            {
                return this.cadenaOriginalSatField;
            }
            set
            {
                this.cadenaOriginalSatField = value;
            }
        }

        public string RfcProvCertif
        {
            get
            {
                return this.rfcProvCertifField;
            }
            set
            {
                this.rfcProvCertifField = value;
            }
        }

        public string Emisor
        {
            get
            {
                return this.emisorField;
            }
            set
            {
                this.emisorField = value;
            }
        }

        public string EmisorRfc
        {
            get
            {
                return this.emisorRfcField;
            }
            set
            {
                this.emisorRfcField = value;
            }
        }

        public string EmisorCurp
        {
            get
            {
                return this.emisorCurpField;
            }
            set
            {
                this.emisorCurpField = value;
            }
        }

        public string EmisorDireccion
        {
            get
            {
                return this.emisorDireccionField;
            }
            set
            {
                this.emisorDireccionField = value;
            }
        }

        public string EmisorNoCertificado
        {
            get
            {
                return this.emisorNoCertificadoField;
            }
            set
            {
                this.emisorNoCertificadoField = value;
            }
        }

        public string EmisorRegimenFiscal
        {
            get
            {
                return this.emisorRegimenFiscalField;
            }
            set
            {
                this.emisorRegimenFiscalField = value;
            }
        }

        public string Receptor
        {
            get
            {
                return this.receptorField;
            }
            set
            {
                this.receptorField = value;
            }
        }

        public string ReceptorRfc
        {
            get
            {
                return this.receptorRfcField;
            }
            set
            {
                this.receptorRfcField = value;
            }
        }

        public string ReceptorCurp
        {
            get
            {
                return this.receptorCurpField;
            }
            set
            {
                this.receptorCurpField = value;
            }
        }

        public string ReceptorDireccion
        {
            get
            {
                return this.receptorDireccionField;
            }
            set
            {
                this.receptorDireccionField = value;
            }
        }

        public string ResidenciaFiscal
        {
            get
            {
                return this.residenciaFiscalField;
            }
            set
            {
                this.residenciaFiscalField = value;
            }
        }

        public string UsoDeCfdi
        {
            get
            {
                return this.usoDeCfdiField;
            }
            set
            {
                this.usoDeCfdiField = value;
            }
        }

        public string ClaveDeConfirmacion
        {
            get
            {
                return this.claveDeConfirmacionField;
            }
            set
            {
                this.claveDeConfirmacionField = value;
            }
        }

        public string TipoRelacion
        {
            get
            {
                return this.tipoRelacionField;
            }
            set
            {
                this.tipoRelacionField = value;
            }
        }

        public string CfdiRelacionado
        {
            get
            {
                return this.cfdiRelacionadoField;
            }
            set
            {
                this.cfdiRelacionadoField = value;
            }
        }

        public byte[] Cbb
        {
            get
            {
                return this.codigoBarras;
            }
            set
            {
                this.codigoBarras = value;
            }
        }

        public string CbbB64
        {
            get
            {
                return Convert.ToBase64String(this.Cbb);
            }
        }
    }
}
