﻿using System;
using System.Linq;

namespace Jaeger.Entities.Basico
{
    public class ContactoSimple : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string rfcField;
        private string nombreField;

        public ContactoSimple()
        {
        }
        public string RFC
        {
            get
            {
                return this.rfcField;
            }
            set
            {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
