﻿using System;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Entities
{
    public class HelperBasic
    {
        private string sConnectionString;
        private Property objMessage;
        private DataBaseConfiguracion objSettings;

        public string ConnectionString
        {
            get
            {
                return this.sConnectionString;
            }
            set
            {
                this.sConnectionString = value;
            }
        }

        public Property Message
        {
            get
            {
                return this.objMessage;
            }
            set
            {
                this.objMessage = value;
            }
        }

        public DataBaseConfiguracion Settings
        {
            get
            {
                return this.objSettings;
            }
            set
            {
                this.objSettings = value;
            }
        }

        public HelperBasic()
        {
            this.sConnectionString = string.Empty;
            this.objSettings = new DataBaseConfiguracion();
        }

        public HelperBasic(DataBaseConfiguracion objeto)
        {
            this.sConnectionString = string.Empty;
            this.objSettings = new DataBaseConfiguracion();
            this.objSettings = objeto;
        }
    }
}