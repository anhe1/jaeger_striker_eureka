﻿using System;
using System.Diagnostics;

namespace Jaeger.Entities
{
    public class Rootobject
    {
        public Folio folio
        {
            [DebuggerNonUserCode]
            get;
            [DebuggerNonUserCode]
            set;
        }

        public Serie serie
        {
            [DebuggerNonUserCode]
            get;
            [DebuggerNonUserCode]
            set;
        }

        [DebuggerNonUserCode]
        public Rootobject()
        {
        }

        public class Folio
        {
            public string name
            {
                [DebuggerNonUserCode]
                get;
                [DebuggerNonUserCode]
                set;
            }

            public string type
            {
                [DebuggerNonUserCode]
                get;
                [DebuggerNonUserCode]
                set;
            }

            [DebuggerNonUserCode]
            public Folio()
            {
            }
        }
    }
}