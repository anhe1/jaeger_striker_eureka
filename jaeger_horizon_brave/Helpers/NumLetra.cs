﻿using System;
using System.Text.RegularExpressions;

namespace Jaeger.Helpers
{
    public class NumLetra
    {
        public enum Moneda
        {
            PesoMexicano = 1,
            DolarEstadounidense = 2,
        }

        private readonly string[] Unidades = new string[10] { "", "un ", "dos ", "tres ", "cuatro ", "cinco ", "seis ", "siete ", "ocho ", "nueve " };
        private readonly string[] Decenas = new string[18] { "diez ", "once ", "doce ", "trece ", "catorce ", "quince ", "dieciseis ", "diecisiete ", "dieciocho ", "diecinueve", "veinte ", "treinta ", "cuarenta ", "cincuenta ", "sesenta ", "setenta ", "ochenta ", "noventa " };
        private readonly string[] Centenas = new string[10] { "", "ciento ", "doscientos ", "trecientos ", "cuatrocientos ", "quinientos ", "seiscientos ", "setecientos ", "ochocientos ", "novecientos " };
        private Regex r;

        public string Convertir(string numero, bool mayusculas, Moneda moneda = Moneda.PesoMexicano)
        {
            string str1 = string.Empty;
            string str2 = string.Empty;
            bool flag = Convert.ToDecimal(numero) >= new Decimal(2);
            switch (moneda)
            {
                case Moneda.PesoMexicano:
                    str1 = flag ? "pesos " : "peso ";
                    str2 = "M.N.";
                    break;
                case Moneda.DolarEstadounidense:
                    str1 = flag ? "dolares " : "dolar ";
                    str2 = "USD.";
                    break;
            }
            string str3 = "";
            numero = numero.Replace(".", ",");
            if (numero.IndexOf(",") == -1)
                numero += ",00";
            this.r = new Regex("\\d{1,9},\\d{1,2}");
            if (this.r.Matches(numero).Count <= 0)
                return str3 = (string)null;
            string[] strArray = numero.Split(',');
            string str4 = strArray[1] + "/100 " + str2;
            string str5 = (int.Parse(strArray[0]) != 0 ? (int.Parse(strArray[0]) <= 999999 ? (int.Parse(strArray[0]) <= 999 ? (int.Parse(strArray[0]) <= 99 ? (int.Parse(strArray[0]) <= 9 ? this.GetUnidades(strArray[0]) : this.GetDecenas(strArray[0])) : this.GetCentenas(strArray[0])) : this.GetMiles(strArray[0])) : this.GetMillones(strArray[0])) : "cero ") + str1;
            if (mayusculas)
                return (str5 + str4).ToUpper();
            return str5 + str4;
        }

        private string GetUnidades(string numero)
        {
            return this.Unidades[int.Parse(numero.Substring(numero.Length - 1))];
        }

        private string GetDecenas(string num)
        {
            int num1 = int.Parse(num);
            if (num1 < 10)
                return this.GetUnidades(num);
            if (num1 <= 19)
                return this.Decenas[num1 - 10];
            string unidades = this.GetUnidades(num);
            if (unidades.Equals(""))
                return this.Decenas[int.Parse(num.Substring(0, 1)) + 8];
            return this.Decenas[int.Parse(num.Substring(0, 1)) + 8] + "y " + unidades;
        }

        private string GetCentenas(string num)
        {
            if (int.Parse(num) <= 99)
                return this.GetDecenas(string.Concat((object)int.Parse(num)));
            if (int.Parse(num) == 100)
                return " cien ";
            return this.Centenas[int.Parse(num.Substring(0, 1))] + this.GetDecenas(num.Substring(1));
        }

        private string GetMiles(string numero)
        {
            string num = numero.Substring(numero.Length - 3);
            string str = numero.Substring(0, numero.Length - 3);
            if (int.Parse(str) > 0)
                return this.GetCentenas(str) + "mil " + this.GetCentenas(num);
            return this.GetCentenas(num) ?? "";
        }

        private string GetMillones(string numero)
        {
            string numero1 = numero.Substring(numero.Length - 6);
            string str = numero.Substring(0, numero.Length - 6);
            return (str.Length <= 1 ? this.GetUnidades(str) + "millon " : this.GetCentenas(str) + "millones ") + this.GetMiles(numero1);
        }
    }
}
