﻿using System;
using System.Collections.Generic;

namespace Jaeger.Helpers
{
    public static class ParseCommand
    {
        public partial class CommandLine
        {
            public CommandLine()
            {
            }

            public CommandLine(string name, string value)
            {
                this.Name = name;
                this.Value = value;
            }

            public string Name { get; set; }
            public string Value { get; set; }
        }

        public static List<ParseCommand.CommandLine> ParseCommandLine(string value, bool testing = false)
        {
            List<ParseCommand.CommandLine> parseCommandLine;
            List<ParseCommand.CommandLine> commandLineArgs = new List<ParseCommand.CommandLine>();
            if (string.IsNullOrEmpty(value))
            {
                parseCommandLine = new List<ParseCommand.CommandLine>();
            }
            else if (value.Contains("/"))
            {
                string[] separadores = new string[] {"/"};
                string[] strArrays = value.Split(separadores,StringSplitOptions.None);
                for (int i = 0; i < checked(strArrays.Length); i++)
                {
                    string arg = strArrays[i];
                    if (!string.IsNullOrEmpty(arg))
                    {
                        if (arg.Contains("="))
                        {
                            int idx = arg.IndexOf("=");
                            if (idx < checked(arg.Length - 1))
                            {
                                commandLineArgs.Add(new CommandLine { Name = arg.Substring(0, idx).Trim(), Value = arg.Substring(checked(idx + 1)).Trim() });
                            }
                        }
                    }
                }
                parseCommandLine = commandLineArgs;
            }
            else
            {
                parseCommandLine = new List<CommandLine>();
            }
            return parseCommandLine;
        }

        public static List<ParseCommand.CommandLine> ParseCommandLine(string value)
        {
            List<ParseCommand.CommandLine> parseCommandLine;
            List<ParseCommand.CommandLine> commandLineArgs = new List<ParseCommand.CommandLine>();
            if (string.IsNullOrEmpty(value))
            {
                parseCommandLine = new List<ParseCommand.CommandLine>();
            }
            else if (value.Contains("-"))
            {
                string[] separadores = new string[] {"-"};
                string[] strArrays = value.Split(separadores,StringSplitOptions.None);
                for (int i = 0; i < checked(strArrays.Length); i++)
                {
                    string arg = strArrays[i];
                    if (!string.IsNullOrEmpty(arg))
                    {
                        if (arg.Contains(" "))
                        {
                            int idx = arg.IndexOf(" ");
                            if (idx < checked(arg.Length - 1))
                            {
                                commandLineArgs.Add(new CommandLine { Name = arg.Substring(0, idx).Trim(), Value = arg.Substring(checked(idx + 1)).Trim() });
                            }
                        }
                    }
                }
                parseCommandLine = commandLineArgs;
            }
            else
            {
                parseCommandLine = new List<CommandLine>();
            }
            return parseCommandLine;
        }
    }
}