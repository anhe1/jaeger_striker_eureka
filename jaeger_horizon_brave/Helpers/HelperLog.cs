﻿// develop: 030620171043
// purpose:
using System;
using System.IO;
using Jaeger.Enums;

namespace Jaeger.Helpers
{
    public class HelperLog
    {

        public static string FileName = Path.Combine("C:/Jaeger/jaeger_tacit_ronin.log");
        public HelperLog()
            : base()
        {
        }

        public static void LogWrite(Jaeger.Entities.Property oError)
        {
            try
            {
                if ((!File.Exists(HelperLog.FileName)))
                {
                    File.Create(HelperLog.FileName).Close();
                }
                System.IO.StreamWriter streamWriter = File.AppendText(HelperLog.FileName);
                streamWriter.WriteLine(string.Concat(DateTime.Now, ",", Enum.GetName(typeof(EnumPropertyType), oError.Type), ",", oError.Type, oError.Code, ",", oError.Name, ",", oError.Value));
                streamWriter.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public bool LogDelete()
        {
            try
            {
                File.Delete(HelperLog.FileName);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

    }
}