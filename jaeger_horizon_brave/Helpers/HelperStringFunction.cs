﻿using System;
using System.Linq;

namespace Jaeger.Helpers
{
    public static class HelperStringFunction
    {
        public static string ToTitleCase(this string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                str = string.Concat(char.ToUpper(str[0]), str.Substring(1));
            }
            return str;
        }

        public static string CloneChain(int n, string chr)
        {
            string str = "";
            if (chr.Length > 0)
            {
                string str1 = chr.Substring(0, 1);
                int num = n;
                for (int i = 1; i <= num; i = checked(i + 1))
                {
                    str = string.Concat(str, str1);
                }
            }
            return str;
        }

        public static string ColumnString(int max, string valor, string relleno, bool alineacion = false)
        {
            if (valor == null)
            {
                valor = HelperStringFunction.CloneChain(max, relleno);
                return valor;
            }

            if (max > valor.Length)
            {

            }

            if (alineacion == false)
            {
                valor = valor + HelperStringFunction.CloneChain(max - valor.Length, relleno);
            }
            else
            {
                valor = HelperStringFunction.CloneChain(max - valor.Length, relleno) + valor;
            }
            return valor;
        }
    }
}
