﻿using System;
using System.ComponentModel;
using System.Linq;

namespace Jaeger.Helpers
{
    public class HelperEnum
    {
        public HelperEnum()
        {
        }

        public static string EnumDescripcion(Enum enumConstant)
        {
            string str;
            DescriptionAttribute[] customAttributes = (DescriptionAttribute[])enumConstant.GetType().GetField(enumConstant.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            str = (customAttributes.Length <= 0 ? enumConstant.ToString() : customAttributes[0].Description);
            return str;
        }

        public static bool EnumHasName<T>(string name)
        {
            return Enumerable.Contains<string>(Enum.GetNames(typeof(T)), name);
        }

        public static T GetEnumByName<T>(string enumMemberName)
        {
            return (T)Enum.Parse(typeof(T), enumMemberName);
        }
    }
}
