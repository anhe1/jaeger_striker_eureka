﻿using System;
using System.Linq;
using System.IO;
using System.Reflection;

namespace Jaeger.Helpers
{
    public class HelperResource
    {
        public HelperResource()
        {
        }

        public bool GetResource(string nameResource, string fileName)
        {
            using (Stream oStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Concat("Jaeger.", nameResource)))
            {
                if (oStream == null)
                {
                    return false;
                }
                FileStream oFileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                oStream.CopyTo(oFileStream);
                oFileStream.Close();
            }
            return File.Exists(fileName);
        }

        /// <summary>
        /// Obtener recurso desde ensamblado especifico
        /// </summary>
        public bool GetResource(string assembly, string nameResource, string fileName)
        {
            Assembly localisationAssembly = Assembly.Load(assembly);
        
            using (Stream oStream = localisationAssembly.GetManifestResourceStream(string.Concat("Jaeger.", nameResource)))
            {
                if (oStream == null)
                {
                    return false;
                }
                FileStream oFileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                oStream.CopyTo(oFileStream);
                oFileStream.Close();
            }
            return File.Exists(fileName);
        }

        public Stream GetResourceStream(string assembly, string nameResource)
        {
            Assembly localisationAssembly = Assembly.Load(assembly);

            Stream oStream = localisationAssembly.GetManifestResourceStream(nameResource);
            return oStream;
        }

        /// <summary>
        /// obtener recurso en un array de bytes[]
        /// </summary>
        public byte[] GetEmbeddedResourceAsBytes(string resourceName, Assembly assembly)
        {
            using (Stream resourceStream = assembly.GetManifestResourceStream(resourceName))
            {
                byte[] content = new byte[resourceStream.Length];
                resourceStream.Read(content, 0, content.Length);

                return content;
            }
        }

        /// <summary>
        /// obtener las rutas y nombres de los rescursos de un ensablado especificado
        /// </summary>
        public void GetPathResources(string assembly)
        {
            Assembly localisationAssembly = Assembly.Load(assembly);

            //will loop tru all assembly files
            foreach (string assemblya in localisationAssembly.GetManifestResourceNames())
            {
                Console.Write(assemblya);
            }

        }
    }
}
