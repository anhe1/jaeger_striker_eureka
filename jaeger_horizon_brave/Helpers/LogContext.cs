﻿using System;
using System.Collections.Generic;
using Jaeger.Edita.Enums;
using Newtonsoft.Json;

namespace Jaeger.Helpers
{
    public class LogContext<T> where T : class, new()
    {
        [JsonObject("item")]
        public class LogErrorEvent
        {
            [JsonProperty("fec")]
            public DateTime Fecha { get; set; }
            [JsonProperty("mod")]
            public string Modulo { get; set; }
            [JsonProperty("desc")]
            public string Descripcion { get; set; }
        }

        public enum LogFormatting
        {
            Json
        }

        public LogContext()
        {

        }

        private List<T> items;

        public string FileName { get; set; }
        public string Version { get; set; }
        public string Title { get; set; }
        public LogFormatting SaveFormat { get; set; }
        public EnumPaths StartPath { get; set; }

        public List<T> Items
        {
            get
            {
                if (this.items == null)
                    this.items = new List<T>();
                return this.items;
            }
            set
            {
                this.items = value;
            }
        }
    }
}
