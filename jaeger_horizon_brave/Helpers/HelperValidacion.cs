﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Jaeger.Helpers
{
    public class HelperValidacion
    {
        public static bool Clabe(string sClabe)
        {
            bool clabe;
            clabe = ((new Regex("^[0-9]{18}", RegexOptions.IgnoreCase)).Match(sClabe).Captures.Count != 0 ? true : false);
            return clabe;
        }

        public static bool VerificarDigitoControlClabe(string clabe)
        {
            string str = "";
            short[] numArray = new short[] { 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7 };
            string str1 = clabe.Substring(17, 1);
            string str2 = clabe.Substring(0, 17);
            int[] num = new int[17];
            for (int i = 0; i < 17; i++)
            {
                short num1 = numArray[i];
                char chr = str2[i];
                num[i] = num1 * Convert.ToInt16(chr.ToString()) % 10;
            }
            int num2 = 10 - num.Sum() % 10;
            str = num2.ToString();
            return str == str1;
        }

        public static bool CodigoPostal(string codigo)
        {
            return (!Regex.IsMatch(codigo, "^[0-9]{5,5}([-]?[0-9]{4,4})?$") ? false : true);
        }

        public static bool Curp(string sCurp)
        {
            bool curp;
            curp = ((new Regex("^[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]", RegexOptions.IgnoreCase)).Match(sCurp).Captures.Count != 0 ? true : false);
            return curp;
        }

        public static bool EsCorreo(string email)
        {
            return Regex.IsMatch(email, "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
        }

        public static bool Rfc(string sRfc)
        {
            if (sRfc == null)
                return false;
            return (!Regex.IsMatch(sRfc, "^[a-zA-Z&ñÑ]{3,4}(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\\d])|([0-9]{2})([0][2])([0][1-9]|[1][\\d]|[2][0-8]))(\\w{2}[A|a|0-9]{1})$") ? false : true);
        }

        public static string StringClean(string strIn)
        {
            return Regex.Replace(strIn, "[^\\w\\.@-]", "");
        }

        /// <summary>
        /// validar formato de folio fiscal (uuid)
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        public static bool UUID(string uuid)
        {
            if (uuid != null)
            {
                return (!Regex.IsMatch(uuid, "[a-f0-9A-F]{8}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{12}") ? false : true);
            }
            return false;
        }

        public static bool ValidaUrl(string sUrl)
        {
            if (sUrl == null)
            {
                return false;
            }
            else
            {
                if ((new Regex("^(https?|ftp|file)://[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|]", RegexOptions.IgnoreCase)).Match(sUrl).Captures.Count == 0)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool ValidaTelefono(string inputString)
        {
            if ((new Regex("^\\d+$", RegexOptions.IgnoreCase)).Match(inputString).Captures.Count == 0)
            {
                return false;
            }
            return true;
        }

        public bool ValidaFecha(string fecha)
        {
            if (!(new Regex("(19|20)\\d{2}(-|\\/)(0[1-9]|1[012])\\2(0[1-9]|[12][0-9]|3[01])")).IsMatch(fecha))
            {
                return false;
            }
            return true;
        }

        public bool ValidaNumOrden(string campo)
        {
            if ((new Regex("[A-Z]{3}[0-6][0-9][0-9]{5}(\\/)[0-9]{2}")).IsMatch(campo))
            {
                return true;
            }
            return false;
        }

        public string ValidarFecha(string fecha)
        {
            string str;
            try
            {
                DateTime dateTime = Convert.ToDateTime(fecha);
                DateTime dateTime1 = DateTime.Now.AddDays(-4);
                if ((dateTime > Convert.ToDateTime(DateTime.Now) ? true : !(dateTime >= Convert.ToDateTime(dateTime1))))
                {
                    str = "";
                }
                else
                {
                    string str1 = dateTime.ToString(string.Format("{0:yyyy-MM-ddTHH:mm:ss}", dateTime));
                    str = str1;
                }
            }
            catch
            {
                str = "";
            }
            return str;
        }

        public bool ValidaFechaCorta(string fecha)
        {
            bool flag;
            flag = (!(new Regex("\\d{4}-\\d{2}-\\d{2}")).IsMatch(fecha) ? false : true);
            return flag;
        }

        protected string ValidaFechaISO(string fecha)
        {
            string str;
            try
            {
                DateTime dateTime = DateTime.ParseExact(fecha, "yyyy-MM-ddThh:mm:ss", CultureInfo.CurrentCulture);
                str = dateTime.ToString("s");
            }
            catch
            {
                str = "";
            }
            return str;
        }

        public bool ValidaFechaISOT(string fecha)
        {
            bool flag;
            flag = (!(new Regex("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}")).IsMatch(fecha) ? false : true);
            return flag;
        }

        public string ValidaFechaNomina(string fecha)
        {
            string str;
            try
            {
                DateTime dateTime = Convert.ToDateTime(fecha);
                string str1 = dateTime.ToString(string.Format("{0:yyyy-MM-dd}", dateTime));
                str = str1;
            }
            catch
            {
                str = "";
            }
            return str;
        }

        public static string ValidaHora(string hora)
        {
            string str;
            str = (!(new Regex("(([01][0-9])|(2[0-3]))(:[0-5][0-9]){2}(\\.[0-9]+)?")).IsMatch(hora) ? "" : hora);
            return str;
        }

        public static string ValidaPadron(string clave)
        {
            string str;
            str = (!(new Regex("[A-ZÑ&]{3}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z0-9]?[A-Z0-9]?[0-9A-Z]-(18|19|20)\\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])-[0-9]{3}")).IsMatch(clave) ? "" : clave);
            return str;
        }

        public string ValidarPatron(string dato, string patron, string nodo)
        {
            string str;
            if (string.IsNullOrEmpty(dato))
            {
                str = "";
            }
            else if (!(new Regex(patron)).IsMatch(dato))
            {
                string[] strArrays = new string[] { "El dato ", dato, " en el nodo ", nodo, " no cumple con el patron requerido" };
                Console.WriteLine(string.Concat(strArrays));
                str = dato;
            }
            else
            {
                str = dato;
            }
            return str;
        }

        public string ValidarPatron(string dato, string nodo, int noMaximo)
        {
            string str;
            if (dato.Length == noMaximo)
            {
                int num = 0;
                while (num < dato.Length)
                {
                    if (!char.IsLetter(dato[num]))
                    {
                        num++;
                    }
                    else
                    {
                        Console.WriteLine(string.Concat(" > El dato ", dato, " no cumple con el patro requerido [0-9] {3} en el nodo ", nodo));
                        str = dato;
                        return str;
                    }
                }
                str = dato;
            }
            else
            {
                Console.WriteLine(string.Concat(" El dato ", dato, " no cumple con el patro requerido [0-9] {3} en el nodo ", nodo));
                str = dato;
            }
            return str;
        }

        public bool EsValidoNumTramiteCE(string cadNumTramiteCE)
        {
            bool flag;
            flag = (!(new Regex("^[0-9]{10}$")).IsMatch(cadNumTramiteCE) ? false : true);
            return flag;
        }

        public static bool ValidaFechaEmisionMenor72H(DateTime fechaComprobante, int offset)
        {
            bool flag = true;

            try
            {
                DateTime currenPactDateTime = DateTime.Now;
                currenPactDateTime = currenPactDateTime.ToUniversalTime();
                DateTimeOffset dateTimeOffset = new System.DateTimeOffset(fechaComprobante, new TimeSpan(offset, 0, 0));
                fechaComprobante = dateTimeOffset.ToUniversalTime().DateTime;
                if (fechaComprobante > currenPactDateTime)
                {
                    flag = false;
                }
                else if (fechaComprobante < currenPactDateTime.AddHours(-72))
                {
                    flag = false;
                }
            }
            catch (Exception)
            {
                flag = true;   
            }
            return flag;
        }

        /// <summary>
        /// validar
        /// </summary>
        /// <param name="base64">cadena base 64</param>
        /// <returns></returns>
        public static bool IsBase64String(string base64)
        {
            if (base64 != null) {
                base64 = base64.Trim();
                return (base64.Length % 4 == 0) && Regex.IsMatch(base64, @"^[-A-Za-z0-9+=]{1,50}|=[^=]|={3,}$", RegexOptions.None);
            }
            return false;
        }
    }
}
