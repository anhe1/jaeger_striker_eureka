﻿/// develop: 070820172356
/// purpose: clase para contener consulta de estado con el webservice del SAT
using System;

namespace Jaeger.SAT.Entities {
    public class SatQueryResult : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation {
        private long indexField;
        private string statusField;
        private string codigoField;
        private DateTime fechaField;
        private string keyField;

        public string Clave {
            get {
                return this.codigoField;
            }
            set {
                this.codigoField = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime Fecha {
            get {
                return this.fechaField;
            }
            set {
                this.fechaField = value;
                this.OnPropertyChanged();
            }
        }

        public long Id {
            get {
                return this.indexField;
            }
            set {
                this.indexField = value;
                this.OnPropertyChanged("Id");
            }
        }

        public string Key {
            get {
                return this.keyField;
            }
            set {
                this.keyField = value;
                this.OnPropertyChanged();
            }
        }

        public string Status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
                this.OnPropertyChanged();
            }
        }

        public string EFOS { get; set; }
        public SatQueryResult() {
            this.indexField = 0;
            this.statusField = string.Empty;
            this.codigoField = string.Empty;
            this.fechaField = DateTime.Now;
            this.keyField = string.Empty;
            this.fechaField = DateTime.Now;
        }
    }
}