﻿using System;

namespace Jaeger.Edita.SAT
{
    public class ContableTipoEnvio : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string claveField;

        private string nombreField;

        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        public string Descripcion
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        public ContableTipoEnvio()
        {
        }
    }
}