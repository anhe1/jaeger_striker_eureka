﻿using Jaeger.Edita.Enums;
using Jaeger.Edita.SAT;
using System;

namespace Jaeger.Edita.SAT
{
    public class ContableLayout : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string versionField;

        private string rfcField;

        private ClaveContable claveField;

        private int anioField;

        private EnumMonthsOfYear periodoField;

        private string numOrdenField;

        private string numTramiteField;

        private ContableTipoEnvio tipoEnvioField;

        private ContableTipoSolicitud tipoSolicitudField;

        private DateTime fechaField;

        private string archivoField;

        private string hojaField;

        private string nombreField;

        public string Archivo
        {
            get
            {
                return this.archivoField;
            }
            set
            {
                this.archivoField = value;
                this.OnPropertyChanged();
            }
        }

        public ClaveContable Documento
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        public int Ejercicio
        {
            get
            {
                return this.anioField;
            }
            set
            {
                if ((value < 2014 || value > 2099 ? false : true))
                {
                    this.anioField = value;
                }
                else
                {
                    this.anioField = 2014;
                }
                this.OnPropertyChanged();
            }
        }

        public DateTime Fecha
        {
            get
            {
                return this.fechaField;
            }
            set
            {
                this.fechaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre de la hoja en donde se encuentra contenido de la información del layout
        /// </summary>
        public string Hoja
        {
            get
            {
                return this.hojaField;
            }
            set
            {
                this.hojaField = value;
                this.OnPropertyChanged();
            }
        }

        public string Nomenclatura
        {
            get
            {
                int numeral = (int)this.Periodo;
                object[] rfc = new object[] { this.Rfc, this.Ejercicio, numeral.ToString("D2"), this.Documento.Clave, ".xml" };
                this.nombreField = string.Concat(rfc);
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        public string NumOrden
        {
            get
            {
                return this.numOrdenField;
            }
            set
            {
                this.numOrdenField = value;
                this.OnPropertyChanged();
            }
        }

        public string NumTramite
        {
            get
            {
                return this.numTramiteField;
            }
            set
            {
                this.numTramiteField = value;
                this.OnPropertyChanged();
            }
        }

        public EnumMonthsOfYear Periodo
        {
            get
            {
                return this.periodoField;
            }
            set
            {
                this.periodoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Rfc
        {
            get
            {
                return this.rfcField;
            }
            set
            {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        public ContableTipoEnvio TipoEnvio
        {
            get
            {
                return this.tipoEnvioField;
            }
            set
            {
                this.tipoEnvioField = value;
                this.OnPropertyChanged();
            }
        }

        public ContableTipoSolicitud TipoSolicitud
        {
            get
            {
                return this.tipoSolicitudField;
            }
            set
            {
                this.tipoSolicitudField = value;
                this.OnPropertyChanged();
            }
        }

        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        public ContableLayout()
        {
            this.claveField = new ClaveContable();
            this.tipoEnvioField = new ContableTipoEnvio();
            this.tipoSolicitudField = new ContableTipoSolicitud();
        }
    }
}