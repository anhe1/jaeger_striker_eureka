﻿using System;
using System.Linq;
using System.ComponentModel;

namespace Jaeger.Edita.V2.Nomina.Layout
{
    public class NominaLayOut : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string tipoNominaField;
        private string nombreField;
        private string archivoExcelField;
        private BindingList<NominaLayOutEmpleado> empleadosField;

        public NominaLayOut()
        {
            this.empleadosField = new BindingList<NominaLayOutEmpleado>();
        }

        /// <summary>
        /// requerido para indicar el tipo de nómina, puede ser O= Nómina ordinaria o E= Nómina extraordinaria
        /// </summary>
        public string TipoNomina
        {
            get
            {
                return this.tipoNominaField;
            }
            set
            {
                this.tipoNominaField = value;
                this.OnPropertyChanged();
            }
        }

        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        public string ArchivoExcel
        {
            get
            {
                return this.archivoExcelField;
            }
            set
            {
                this.archivoExcelField = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<NominaLayOutEmpleado> Empleados
        {
            get
            {
                return this.empleadosField;
            }
            set
            {
                this.empleadosField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
