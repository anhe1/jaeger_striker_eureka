﻿using System;

namespace Jaeger.Edita.V2.Nomina.Layout
{
    public class LayoutTablaISR
    {
        public string Etiqueta { get; set; }

        public int Periodo { get; set; }

        public partial class ISR
        {
            public ISR()
            {

            }

            public decimal LimiteInferior { get; set; }
            public decimal LimiteSuperior { get; set; }
            public decimal CuotaFija { get; set; }
            public decimal PorcentajeExcedente { get; set; }
        }
    }
}
