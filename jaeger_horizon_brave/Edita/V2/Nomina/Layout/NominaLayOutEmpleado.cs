﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Jaeger.Edita.V2.Nomina.Entities;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Edita.V2.Nomina.Layout
{
    public class NominaLayOutEmpleado : BasePropertyChangeImplementation, IDataErrorInfo
    {
        string tipoNominaField;
        string registroPatronalField;
        DateTime? fechaPagoField;
        DateTime? fechaInicialPagoField;
        DateTime? fechaFinalPagoField;
        int numDiasPagadosField;
        string lugarExpedicionField;
        string rfcReceptorField;
        string nombreReceptorField;
        string correoField;
        string curpReceptorField;
        string numSeguridadSocialField;
        DateTime? fechaInicioRelLaboralField;
        string antiguedadField;
        string tipoContratoField;
        string sindicalizadoField;
        string tipoJornadaField;
        string tipoRegimenField;
        string numEmpleadoField;
        string departamentoField;
        string puestoField;
        string riesgoPuestoField;
        string periodicidadPagoField;
        string claveBancoField;
        string cuentaBancariaField;
        decimal salarioBaseCotAporField;
        decimal salarioDiarioIntegradoField;
        string claveEntFedField;
        string subsidioAlEmpleoField;
        decimal diasIncapacidadField;
        string tipoIncapacidadField;
        decimal importeMonetarioField;
        string uuidField;
        DateTime? fechaCertificacionField;
        BindingList<NominaElemento> items = new BindingList<NominaElemento>();

        #region propiedades

        public string RegistroPatronal
        {
            get
            {
                return this.registroPatronalField;
            }
            set
            {
                this.registroPatronalField = value.Trim().ToUpper();
                this.OnPropertyChanged();
            }
        }

        public string TipoNomina
        {
            get
            {
                return this.tipoNominaField;
            }
            set
            {
                if (value != null)
                {
                    this.tipoNominaField = value.Trim().ToUpper();
                }
                else
                {
                    this.tipoNominaField = value;
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// requerido para la expresión de la fecha efectiva de erogación del gasto. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601.
        /// </summary>
        public DateTime? FechaPago
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaPagoField >= firstGoodDate)
                {
                    return this.fechaPagoField;
                }
                return null;
            }
            set
            {
                this.fechaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// requerido para la expresión de la fecha inicial del período de pago. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601.
        /// </summary>
        public DateTime? FechaInicialPago
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaInicialPagoField >= firstGoodDate)
                {
                    return this.fechaInicialPagoField;
                }
                return null;
            }
            set
            {
                this.fechaInicialPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// requerido para la expresión de la fecha final del período de pago. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601
        /// </summary>
        public DateTime? FechaFinalPago
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaFinalPagoField >= firstGoodDate)
                {
                    return this.fechaFinalPagoField;
                }
                return null;
            }
            set
            {
                this.fechaFinalPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// requerido para la expresión del número o la fracción de días pagados.
        /// </summary>
        public int NumDiasPagados
        {
            get
            {
                return this.numDiasPagadosField;
            }
            set
            {
                this.numDiasPagadosField = value;
                this.OnPropertyChanged();
            }
        }

        public string LugarExpedicion
        {
            get
            {
                return this.lugarExpedicionField;
            }
            set
            {
                this.lugarExpedicionField = value;
                this.OnPropertyChanged();
            }
        }

        public string ReceptorRFC
        {
            get
            {
                return this.rfcReceptorField;
            }
            set
            {
                this.rfcReceptorField = value;
                this.OnPropertyChanged();
            }
        }

        public string NombreReceptor
        {
            get
            {
                return this.nombreReceptorField;
            }
            set
            {
                this.nombreReceptorField = value;
                this.OnPropertyChanged();
            }
        }

        public string Correo
        {
            get
            {
                return this.correoField;
            }
            set
            {
                this.correoField = value;
                this.OnPropertyChanged();
            }
        }

        public string ReceptorCURP
        {
            get
            {
                return this.curpReceptorField;
            }
            set
            {
                this.curpReceptorField = value.ToString().Trim().ToUpper().Replace("-", "");
                this.OnPropertyChanged();
            }
        }

        public string NumSeguridadSocial
        {
            get
            {
                return this.numSeguridadSocialField;
            }
            set
            {
                this.numSeguridadSocialField = Regex.Replace(value, "[^\\d]", "");
                this.OnPropertyChanged();
            }
        }

        public DateTime? FechaInicioRelLaboral
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaInicioRelLaboralField >= firstGoodDate)
                {
                    return this.fechaInicioRelLaboralField;
                }
                return null;
            }
            set
            {
                this.fechaInicioRelLaboralField = value;
                this.OnPropertyChanged();
            }
        }

        public string Antiguedad
        {
            get
            {
                return this.antiguedadField;
            }
            set
            {
                this.antiguedadField = value;
                this.OnPropertyChanged();
            }
        }

        public string TipoContrato
        {
            get
            {
                return this.tipoContratoField;
            }
            set
            {
                this.tipoContratoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Sindicalizado
        {
            get
            {
                return this.sindicalizadoField;
            }
            set
            {
                this.sindicalizadoField = value;
                this.OnPropertyChanged();
            }
        }

        public string TipoJornada
        {
            get
            {
                return this.tipoJornadaField;
            }
            set
            {
                this.tipoJornadaField = value;
                this.OnPropertyChanged();
            }
        }

        public string TipoRegimen
        {
            get
            {
                return this.tipoRegimenField;
            }
            set
            {
                this.tipoRegimenField = value;
                this.OnPropertyChanged();
            }
        }

        public string NumEmpleado
        {
            get
            {
                return this.numEmpleadoField;
            }
            set
            {
                this.numEmpleadoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Departamento
        {
            get
            {
                return this.departamentoField;
            }
            set
            {
                this.departamentoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Puesto
        {
            get
            {
                return this.puestoField;
            }
            set
            {
                this.puestoField = value;
                this.OnPropertyChanged();
            }
        }

        public string RiesgoPuesto
        {
            get
            {
                return this.riesgoPuestoField;
            }
            set
            {
                this.riesgoPuestoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// forma en que se establece el pago del salario.
        /// </summary>
        public string PeriodicidadPago
        {
            get
            {
                return this.periodicidadPagoField;
            }
            set
            {
                this.periodicidadPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// clave del Banco conforme al catálogo, donde se realiza el depósito de nómina.
        /// </summary>
        public string ClaveBanco
        {
            get
            {
                return this.claveBancoField;
            }
            set
            {
                this.claveBancoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de monedero electrónico, donde se realiza el depósito de nómina.
        /// </summary>
        public string CuentaBancaria
        {
            get
            {
                return this.cuentaBancariaField;
            }
            set
            {
                this.cuentaBancariaField = Regex.Replace(value, "[^\\d]", "");
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// retribución otorgada al trabajador, que se integra por los pagos hechos en efectivo por cuota diaria, gratificaciones, percepciones, alimentación, habitación, primas, comisiones, prestaciones en especie y cualquiera otra cantidad o prestación que se entregue al trabajador por su trabajo, sin considerar los conceptos que se excluyen de conformidad con el Artículo 27 de la Ley del Seguro Social, o la integración de los pagos conforme la normatividad del Instituto de Seguridad Social del trabajador. (Se emplea para pagar las cuotas y aportaciones de Seguridad Social). Se debe ingresar cuando se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        public decimal SalarioBaseCotApor
        {
            get
            {
                return this.salarioBaseCotAporField;
            }
            set
            {
                this.salarioBaseCotAporField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// salario que se integra con los pagos hechos en efectivo por cuota diaria, gratificaciones, percepciones, habitación, primas, comisiones, prestaciones en especie y cualquier otra cantidad o prestación que se entregue al trabajador por su trabajo, de conformidad con el Art. 84 de la Ley Federal del Trabajo. (Se utiliza para el cálculo de las indemnizaciones).Se debe ingresar cuando se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        public decimal SalarioDiarioIntegrado
        {
            get
            {
                return this.salarioDiarioIntegradoField;
            }
            set
            {
                this.salarioDiarioIntegradoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// clave de la entidad federativa en donde el receptor del recibo prestó el servicio.
        /// </summary>
        public string ClaveEntFed
        {
            get
            {
                return this.claveEntFedField;
            }
            set
            {
                this.claveEntFedField = value;
            }
        }

        public string SubsidioAlEmpleo
        {
            get
            {
                return this.subsidioAlEmpleoField;
            }
            set
            {
                this.subsidioAlEmpleoField = Regex.Replace(value, "[^\\d]", "");
                this.OnPropertyChanged();
            }
        }

        public decimal DiasIncapacidad
        {
            get
            {
                return this.diasIncapacidadField;
            }
            set
            {
                this.diasIncapacidadField = value;
                this.OnPropertyChanged();
            }
        }

        public string TipoIncapacidad
        {
            get
            {
                return this.tipoIncapacidadField;
            }
            set
            {
                this.tipoIncapacidadField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal ImporteMonetario
        {
            get
            {
                return this.importeMonetarioField;
            }
            set
            {
                this.importeMonetarioField = value;
                this.OnPropertyChanged();
            }
        }

        public string UUID
        {
            get
            {
                return this.uuidField;
            }
            set
            {
                this.uuidField = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime? FechaCertificacion
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCertificacionField >= firstGoodDate)
                {
                    return this.fechaCertificacionField;
                }
                return null;
            }
            set
            {
                this.fechaCertificacionField = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<NominaElemento> Items
        {
            get
            {
                return this.items;
            }
            set
            {
                this.items = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region metodos

        public string this[string columnName]
        {
            get
            {
                if (columnName == "TipoNomina" && this.TipoNomina != "O" && this.TipoNomina != "E")
                {
                    return this.TipoNominaValidar();
                }
                if (columnName == "TipoRegimen" && !this.RegexValido(this.tipoRegimenField, "0[0-9]"))
                {
                    return this.TipoRegimenValidar();
                }
                if (columnName == "TipoJornada" && !this.RegexValido(this.tipoJornadaField, "0[0-9]") && this.tipoNominaField != "E")
                {
                    return this.TipoJornadaValidar();
                }
                return string.Empty;
            }
        }

        [Browsable(false)]
        public string Error
        {
            get
            {
                if ((this.TipoNomina != "O" && this.TipoNomina != "E") || !this.RfcValido() || !this.RegexValido(this.tipoRegimenField, "0|[0-9]") || (!this.RegexValido(this.tipoJornadaField, "0[0-9]") && this.tipoNominaField != "E"))
                {
                    return "Es necesario revisar información de esta fila.";
                }
                return string.Empty;
            }
        }

        private string TipoNominaValidar()
        {
            if (this.TipoNomina != "O" && this.TipoNomina != "E")
            {
                return "El valor del atributo Nomina.TipoNomina no cumple con un valor del catálogo c_TipoNomina.";
            }
            if ((this.TipoNomina != "O" ? false : !this.RegexValido(this.PeriodicidadPago, "0[1-9]")))
            {
                return "El valor del atributo tipo de periodicidad no se encuentra entre 01 al 09.";
            }
            if ((this.TipoNomina != "E" ? false : this.PeriodicidadPago != "99"))
            {
                return "El valor del atributo tipo de periodicidad no es 99.";
            }
            return string.Empty;
        }

        private bool RfcValido()
        {
            if (this.ReceptorRFC != null)
            {
                bool flag;
                flag = (!(new Regex("^[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9A]$")).IsMatch(this.ReceptorRFC) ? false : true);
                return flag;
            }
            return false;
        }

        //private string RegistroPatronalValidar()
        //{
        //    if (this.RegexValido(this.TipoContrato, "0[1-8]"))
        //    {
        //        if (this.RegistroPatronal == null)
        //        {
        //            return "El atributo Nomina.Emisor.RegistroPatronal se debe registrar.";
        //        }
        //    }
        //    else if (this.RegistroPatronal != null)
        //    {
        //        return "El atributo Nomina.Emisor.RegistroPatronal  no se debe registrar.";
        //    }
        //    if (this.RegistroPatronal != null)
        //    {
        //        if (this.NumSeguridadSocial == null)
        //        {
        //            return "El atributo nomina12:Receptor:NumSeguridadSocial debe existir.";
        //        }
        //        if (this.FechaInicioRelLaboral == null)
        //        {
        //            return "El atributo nomina12:Receptor:FechaInicioRelLaboral debe existir.";
        //        }
        //        if (this.Antiguedad == null)
        //        {
        //            return "El atributo nomina12:Receptor:Antigüedad debe existir.";
        //        }
        //        if (this.RiesgoPuesto == null)
        //        {
        //            return "El atributo nomina12:Receptor:RiesgoPuesto debe existir.";
        //        }
        //        if (this.SalarioDiarioIntegrado == null)
        //        {
        //            return "El atributo nomina12:Receptor:SalarioDiarioIntegrado debe existir.";
        //        }
        //    }
        //    return string.Empty;
        //}

        //private bool UuidValido()
        //{
        //    bool flag;
        //    flag = (!(new Regex("[a-f0-9A-F]{8}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{12}")).IsMatch(this.uuidField) ? false : true);
        //    return flag;
        //}

        private string TipoJornadaValidar()
        {
            if (this.tipoJornadaField != null)
            {
                if (!this.RegexValido(this.tipoJornadaField, "0[0-9]"))
                {
                    return "El valor del atributo Nomina.Receptor.TipoJornada no cumple con un valor del catálogo c_TipoJornada.";
                }
            }
            return string.Empty;
        }

        //private string PeriodicidadPagoValidar()
        //{
        //    if (this.periodicidadPagoField == null)
        //    {
        //        return "El valor del atributo Nomina.Receptor.RiesgoPuesto no cumple con un valor del catálogo c_RiesgoPuesto.";
        //    }
        //    return string.Empty;
        //}

        //private string RiesgoPuestoValidar()
        //{
        //    if ((this.riesgoPuestoField == null ? false : !this.RegexValido(this.riesgoPuestoField, "0[0-9]")))
        //    {
        //        return "El valor del atributo Nomina.Receptor.RiesgoPuesto no cumple con un valor del catálogo c_RiesgoPuesto.";
        //    }
        //    return string.Empty;
        //}

        //private bool VerificarDigitoControlCLABE()
        //{
        //    string str = "";
        //    short[] numArray = new short[] { 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7 };
        //    string str1 = this.cuentaBancariaField.Substring(17, 1);
        //    string str2 = this.cuentaBancariaField.Substring(0, 17);
        //    int[] num = new int[17];
        //    for (int i = 0; i < 17; i++)
        //    {
        //        short num1 = numArray[i];
        //        char chr = str2[i];
        //        num[i] = num1 * Convert.ToInt16(chr.ToString()) % 10;
        //    }
        //    int num2 = 10 - Enumerable.Sum(num) % 10;
        //    str = num2.ToString();
        //    return str == str1;
        //}

        //private string ValidaNumero(double numero, int digitos)
        //{
        //    string str = "";
        //    for (int i = 0; i < digitos; i++)
        //    {
        //        str = string.Concat(str, "0");
        //    }
        //    string str1 = string.Format(string.Concat("{0:0.", str, "}"), numero);
        //    return str1;
        //}

        private string TipoRegimenValidar()
        {
            if (!this.RegexValido(this.tipoRegimenField, "09|[1-9][0-9]"))
            {
                return "El valor del atributo Nomina.Receptor.TipoRegimen no cumple con un valor del catálogo c_TipoRegimen.";
            }
            if ((!this.RegexValido(this.TipoContrato, "0[1-8]") ? false : !this.RegexValido(this.tipoRegimenField, "0[2-4]")))
            {
                return "El atributo Nomina.Receptor.TipoRegimen no es 02, 03 ó 04.";
            }
            if ((!this.RegexValido(this.TipoContrato, "09|[1-9][0-9]") ? false : !this.RegexValido(this.tipoRegimenField, "0[5-9]|[1-9][0-9]")))
            {
                return "El atributo Nomina.Receptor.TipoRegimen no está entre 05 a 99.";
            }
            return string.Empty;
        }

        //private string AntiguedadValidar()
        //{
        //    DateTime dateTime;
        //    if (this.antiguedadField != null)
        //    {
        //        dateTime = (this.FechaInicioRelLaboral == null ? DateTime.MinValue : Convert.ToDateTime(this.FechaInicioRelLaboral));
        //        DateTime dateTime1 = Convert.ToDateTime(this.FechaFinalPago);
        //        string str = this.Antiguedad.Replace("P", "");
        //        if (!this.RegexValido(str, "[1-9][0-9]{0,3}W"))
        //        {
        //            int num = 0;
        //            int num1 = 0;
        //            int num2 = 0;
        //            string str1 = Regex.Match(str, "([1-9]|[12][0-9]|3[01])D").ToString().Replace("D", "");
        //            string str2 = Regex.Match(str, "([1-9]|1[012])M").ToString().Replace("M", "");
        //            string str3 = Regex.Match(str, "[1-9][0-9]?Y").ToString().Replace("Y", "");
        //            if (!string.IsNullOrEmpty(str1))
        //            {
        //                num = Convert.ToInt16(str1);
        //            }
        //            if (!string.IsNullOrEmpty(str2))
        //            {
        //                num1 = Convert.ToInt16(str2);
        //            }
        //            if (!string.IsNullOrEmpty(str3))
        //            {
        //                num2 = Convert.ToInt16(str3);
        //            }
        //            if (dateTime.AddYears(num2).AddMonths(num1).AddDays((double)(num - 1)) != dateTime1)
        //            {
        //                return "El valor del atributo Nomina.Receptor.Antigüedad. no cumple con el número de años, meses y días transcurridos entre la FechaInicioRelLaboral y la FechaFinalPago.";
        //            }
        //        }
        //        else
        //        {
        //            TimeSpan timeSpan = dateTime1 - dateTime;
        //            if (Convert.ToDouble(str.Replace("W", "")) > Math.Floor((timeSpan.TotalDays + 1) / 7))
        //            {
        //                return "El valor numérico del atributo Nomina.Receptor.Antigüedad no es menor o igual al cociente de (la suma del número de días transcurridos entre la FechaInicioRelLaboral y la FechaFinalPago más uno) dividido entre siete.";
        //            }
        //        }
        //    }
        //    return string.Empty;
        //}

        private bool RegexValido(string valor, string patron)
        {
            bool flag = Regex.IsMatch(valor, string.Concat("^", patron, "$"));
            return flag;
        }

        #endregion
    }
}
