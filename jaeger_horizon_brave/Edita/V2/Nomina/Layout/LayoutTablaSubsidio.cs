﻿using System;
using System.Linq;

namespace Jaeger.Edita.V2.Nomina.Layout
{
    public class LayoutTablaSubsidio
    {
        /// <summary>
        /// constructor
        /// </summary>
        public LayoutTablaSubsidio()
        {

        }

        public string Etiqueta { get; set; }

        public int Periodo { get; set; }

        private partial class LayoutSubsidio
        {
            public LayoutSubsidio()
            {

            }

            public decimal IngresosDesde { get; set; }

            public decimal IngresosHasta { get; set; }

            public decimal Cantidad { get; set; }
        }

    }
}
