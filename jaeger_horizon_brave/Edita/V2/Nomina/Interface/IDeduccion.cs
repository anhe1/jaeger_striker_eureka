﻿/// <summary>
/// develop: antonio hernandez 141120181506
/// pupose: interface para percepciones
/// </summary>
namespace Jaeger.Edita.V2.Nomina.Interface
{
    public interface IDeduccion
    {
        string Indetificador { get; set; }
        string Clave { get; set; }
        string Tipo { get; set; }
        string Concepto { get; }
        string Etiqueta { get; set; }
        decimal Importe { get; set; }
        void Calcular();
    }
}