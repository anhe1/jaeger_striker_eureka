﻿/// develop: Antonio Hernandez R. 
/// purpose:
using System;
using Jaeger.Edita.V2.Directorio.Enums;
using Jaeger.Edita.V2.Directorio.Interfaces;

namespace Jaeger.Edita.V2.Nomina.Interface
{
    public interface IEmpleado : IPersona
    {
        /// <summary>
        /// Atributo requerido para expresar el número de empleado de 1 a 15 posiciones. [^|]{1,15}
        /// </summary>
        int Num { get; set; }

        string Clave { get; set; }

        string Nombre { get; set; }

        string PrimerApellido { get; set; }

        string SegundoApellido { get; set; }

        string RFC { get; set; }

        string CURP { get; set; }

        /// <summary>
        /// Atributo opcional para la expresión del departamento o área a la que pertenece el trabajador. [^|]{1,100}
        /// </summary>
        string Departamento { get; set; }

        /// <summary>
        /// Atributo opcional para la expresión del puesto asignado al empleado o actividad que realiza. [^|]{1,100}
        /// </summary>
        string Puesto { get; set; }

        /// <summary>
        /// Atributo opcional para expresar la clave conforme a la Clase en que deben inscribirse los patrones, de acuerdo con las actividades que desempeñan sus trabajadores, según lo previsto en el artículo 196 del Reglamento en Materia de Afiliación Clasificación de Empresas, Recaudación y Fiscalización, o conforme con la normatividad del Instituto de Seguridad Social del trabajador.  Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        string RiesgoPuesto { get; set; }

        /// <summary>
        /// requerido para la forma en que se establece el pago del salario.
        /// </summary>
        string PeriodicidadPago { get; set; }

        /// <summary>
        /// Atributo requerido para expresar el tipo de contrato que tiene el trabajador
        /// </summary>
        string TipoContrato { get; set; }

        /// <summary>
        /// Atributo condicional para la expresión de la cuenta bancaria a 11 posiciones o número de teléfono celular a 10 posiciones o número de tarjeta de crédito, débito o servicios a 15 ó 16 posiciones o la CLABE a 18 posiciones o número de monedero electrónico, donde se realiza el depósito de nómina.
        /// </summary>
        string CuentaBancaria { get; set; }

        /// <summary>
        /// Atributo condicional para expresar el tipo de jornada que cubre el trabajador. Se debe ingresar cuando se esté obligado conforme a otras disposiciones distintas a las fiscales
        /// </summary>
        string TipoJornada { get; set; }

        /// <summary>
        /// Atributo requerido para la expresión de la clave del régimen por el cual se tiene contratado al trabajador.
        /// </summary>
        string TipoRegimen { get; set; }

        /// <summary>
        /// obtener o establacer clave del banco del beneficiario
        /// </summary>
        string ClaveBanco { get; set; }

        /// <summary>
        /// obtener o establecer el numero de cuenta bancaria o numero telefonico para el desposito
        /// </summary>
        string NumCuenta { get; set; }

        /// <summary>
        /// obtener o establecer el metodo de pago
        /// </summary>
        string MetodoPago { get; set; }

        /// <summary>
        /// número de seguridad social del trabajador. Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales. [0-9]{1,15}
        /// </summary>
        string NumSeguridadSocial { get; set; }

        string NumFonacot { get; set; }

        string Afore { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de nacimineto
        /// </summary>
        DateTime? FechaNacimiento { get; set; }

        /// <summary>
        /// Expresar la fecha de inicio de la relación laboral entre el empleador y el empleado. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601. Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales
        /// </summary>
        DateTime FechaInicioRelLaboral { get; set; }

        /// <summary>
        /// obtener o establcer fecha de fin de la relación laboral
        /// </summary>
        DateTime? FechaFinRelLaboral { get; set; }

        /// <summary>
        /// obtener o establacer genero
        /// </summary>
        EnumGenero Genero { get; set; }

        decimal SalarioBase { get; set; }

        decimal SalarioDiario { get; set; }

        decimal SalarioDiarioIntegrado { get; set; }
    }
}
