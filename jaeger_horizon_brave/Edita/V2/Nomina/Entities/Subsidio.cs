﻿/// <summary>
/// develop: anhe1 161120181644
/// purpose: subsidio al empleo
/// </summary>
namespace Jaeger.Edita.V2.Nomina.Entities
{
    public class Subsidio : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private decimal ingresosDesdeField;
        private decimal ingresosHastaField;
        private decimal cantidadField;

        public Subsidio()
        {

        }

        public Subsidio(decimal ingresosDesdeField, decimal ingresosHastaField, decimal cantidadField)
        {
            this.ingresosDesdeField = ingresosDesdeField;
            this.ingresosHastaField = ingresosHastaField;
            this.cantidadField = cantidadField;
        }

        public decimal IngresosDesde
        {
            get
            {
                return this.ingresosDesdeField;
            }
            set
            {
                this.ingresosDesdeField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal IngresosHasta
        {
            get
            {
                return this.ingresosHastaField;
            }
            set
            {
                this.ingresosHastaField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Cantidad
        {
            get
            {
                return this.cantidadField;
            }
            set
            {
                this.cantidadField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
