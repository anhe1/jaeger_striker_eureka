﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Jaeger.Edita.V2.Nomina.Enums;

namespace Jaeger.Edita.V2.Nomina.Entities
{
    public class NominaElemento : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation, IDataErrorInfo
    {
        private long indiceField;
        private EnumNominaElemento elementoField;
        private string tipoField;
        private string claveField;
        private string conceptoField;
        private decimal importeExentoField;
        private decimal importeGravadoField;

        private string empleadoField;
        private DateTime? fechaPagoField;
                
        private string numEmpleadoField;

        // incapacidad
        private int diasIncapacidadField;
        private string tipoIncapacidadField;
        private decimal importeMonetarioField;

        // bloque de horas extra (deberia ser un objeto independiente)
        private int diasHorasExtraField;
        private string tipoHorasExtraField;
        private int horasExtraField;
        private decimal importePagadoField;

        #region percepcion o deduccion

        public long Indice
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de elmento (percepcion, deduccion, etc.
        /// </summary>
        public EnumNominaElemento Elemento
        {
            get
            {
                return this.elementoField;
            }
            set
            {
                this.elementoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de elmento en modo texto (percepcion, deduccion, etc.
        /// </summary>
        public string ElementoText
        {
            get
            {
                return Enum.GetName(typeof(EnumNominaElemento), this.Elemento);
            }
            set
            {
                this.Elemento = (EnumNominaElemento)Enum.Parse(typeof(EnumNominaElemento), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// clave agrupadora bajo la cual se clasifica la percepcion o deducción.
        /// </summary>
        public string Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// clave de nómina propia de la contabilidad de cada patrón, puede conformarse desde 3 hasta 15 caracteres.
        /// </summary>
        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// descripción del concepto de la percepcion o la deduccion
        /// </summary>
        public string Concepto
        {
            get
            {
                return this.conceptoField;
            }
            set
            {
                this.conceptoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// representa el importe exento de un concepto de la percepcion o la deduccion
        /// </summary>
        public decimal ImporteExento
        {
            get
            {
                return this.importeExentoField;
            }
            set
            {
                this.importeExentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// representa el importe gravado de un concepto
        /// </summary>
        public decimal ImporteGravado
        {
            get
            {
                return this.importeGravadoField;
            }
            set
            {
                this.importeGravadoField = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region OtrosPagos

        /// <summary>
        /// atributo requerido para expresar la clave agrupadora bajo la cual se clasifica el otro pago
        /// </summary>
        public string TipoOtroPago
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        #endregion

        #region Incapacidades

        /// <summary>
        /// atributo requerido para expresar el numero de dias enteros que el trabajador se incapacito en el periodo
        /// </summary>
        public int DiasIncapacidad
        {
            get
            {
                return this.diasIncapacidadField;
            }
            set
            {
                this.diasIncapacidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// atributo requerido para expresar la razon de la incapacidad (catNomina:c_TipoIncapacidad)
        /// </summary>
        public string TipoIncapacidad
        {
            get
            {
                return this.tipoIncapacidadField;
            }
            set
            {
                this.tipoIncapacidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// atributo adicional para expresar el monto del importe monetario de la incapaciadad
        /// </summary>
        public decimal ImporteMonetario
        {
            get
            {
                return this.importeMonetarioField;
            }
            set
            {
                this.importeMonetarioField = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region NominaPercepcionesPercepcionHorasExtra

        /// <summary>
        ///  para expresar el número de días en que el trabajador realizó horas extra en el periodo (atributo Dias-requerido)
        /// </summary>
        public int DiasHorasExtra
        {
            get
            {
                return this.diasHorasExtraField;
            }
            set
            {
                this.diasHorasExtraField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// requerido para expresar el tipo de pago de horas extra (catNomina:c_TipoHoras)
        /// </summary>
        public string TipoHorasExtra
        {
            get
            {
                return this.tipoHorasExtraField;
            }
            set
            {
                this.tipoHorasExtraField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// para expresar el numero de otas extra trabajadas en el periodo.
        /// </summary>
        public int HorasExtra
        {
            get
            {
                return this.horasExtraField;
            }
            set
            {
                this.horasExtraField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// para expresar el importe pagado por las horas extra
        /// </summary>
        public decimal ImportePagado
        {
            get
            {
                return this.importePagadoField;
            }
            set
            {
                this.importePagadoField = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region informacion del empleado

        public string Empleado
        {
            get
            {
                return this.empleadoField;
            }
            set
            {
                this.empleadoField = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime? FechaPago
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaPagoField >= firstGoodDate)
                {
                    return this.fechaPagoField;
                }
                return null;
            }
            set
            {
                this.fechaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        public int NumQuincena
        {
            get
            {
                if (this.FechaPago != null)
                {
                    return this.FechaPago.Value.Day > 15 ? 2 : 1;
                }
                return 0;
            }
        }

        public int Anio
        {
            get
            {
                if (this.FechaPago != null)
                {
                    return this.FechaPago.Value.Year;
                }
                return 0;
            }
        }

        public string NumEmpleado
        {
            get
            {
                return this.numEmpleadoField;
            }
            set
            {
                this.numEmpleadoField = value;
            }
        }

        #endregion

        #region validaciones

        public string this[string columnName]
        {
            get
            {
                if (columnName == "Clave" && !this.RegexValido(this.Clave, "[^|]{3,15}"))
                {
                    return "Clave de percepción de nómina propia de la contabilidad de cada patrón, puede conformarse desde 3 hasta 15 caracteres.";
                }

                if (columnName == "Tipo" && !this.RegexValido(this.Tipo, "[^|]{3,15}"))
                {
                    return "Clave agrupadora bajo la cual se clasifica la percepcion o deducción.";
                }
                return string.Empty;
            }
        }

        public string Error
        {
            get
            {
                return string.Empty;
            }
        }

        private bool RegexValido(string valor, string patron)
        {
            if (!(valor == null))
            {
                bool flag = Regex.IsMatch(valor, string.Concat("^", patron, "$"));
                return flag;
            }
            return false;
        }
        #endregion
    }
}