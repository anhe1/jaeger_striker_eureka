﻿using SqlSugar;

namespace Jaeger.Edita.V2.Nomina.Entities
{
    [SugarTable("_ctldpt", "nomina: catalogo de departamentos")]
    public class ViewModelDepartamentoSingle
    {
        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_id", ColumnDescription = "indice", IsIdentity = true, IsPrimaryKey = true, Length = 11)]
        public int Id { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_a", ColumnDescription = "registro activo", Length = 1)]
        public bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice de la clasifiacion
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_sbid", ColumnDescription = "indice del area al que pertenece el departamento", Length = 11, IsNullable = true)]
        public int IdClase { get; set; }

        /// <summary>
        /// obtener o establecer clave del departamento
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_clv", ColumnDescription = "clave de departamento", IsNullable = true, Length = 10)]
        public string Clave { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del departamento
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_depto", ColumnDescription = "nombre del departamento", Length = 128, IsNullable = false)]
        public string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer el nombre de la persona responsable del departamento
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_nom", ColumnDescription = "responsable del departamento", Length = 128, IsNullable = false)]
        public string Responsable { get; set; }

        /// <summary>
        /// obtener o establecer una breve descripcion del departamento
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_desc", ColumnDescription = "descripcion del departamento", Length = 128, IsNullable = true)]
        public string Descripcion { get; set; }
    }
}
