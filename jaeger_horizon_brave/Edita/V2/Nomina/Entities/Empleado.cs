﻿using System;
using System.Linq;
using SqlSugar;

namespace Jaeger.Edita.V2.Nomina.Entities
{
    public class Empleado : Jaeger.Edita.V2.Directorio.Entities.Persona
    {
        private int idEmpleado;
        private int numEmpleadoField;
        private string claveField;
        private string primerapellidoField;
        private string segundoapellidoField;
        private string telefonoField;
        private string telefono2Field;
        private string correoField;
        private string departamentoField;
        private string puestoField;
        private string riesgoPuestoField;
        private string periodicidadPagoField;
        private string tipoContratoField;
        private string cuentaBancariaField;
        private string tipoJornadaField;
        private string tipoRegimenField;
        private string numSeguridadSocialField;
        private DateTime? fechaInicioRelLaboralField;
        private decimal salarioBaseField;
        private decimal salarioDiarioIntegreadoField;
        private decimal salarioDiarioField;
        private string claveBancoField;
        private string antiguedad;
        private decimal salarioBaseCotApor;
        private string registroPatronal;

        public Empleado()
        {
            this.fechaInicioRelLaboralField = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice del directorio
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_id", IsPrimaryKey = true, IsIdentity = true)]
        public new int Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_drctr_id", IsNullable = true, ColumnDescription = "indice con el directorio principal")]
        public int IdDirectorio
        {
            get
            {
                return this.idEmpleado;
            }
            set
            {
                this.idEmpleado = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_a", ColumnDescription = "registro activo")]
        public new bool IsActive
        {
            get
            {
                return base.IsActive;
            }
            set
            {
                base.IsActive = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_tpreg", Length = 2, IsNullable = true, ColumnDescription = "clave de tipo de regimen")]
        public string TipoRegimen
        {
            get
            {
                return this.tipoRegimenField;
            }
            set
            {
                this.tipoRegimenField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_rsgpst", Length = 3, IsNullable = true, ColumnDescription = "clave riesgo de puesto")]
        public string RiesgoPuesto
        {
            get
            {
                return this.riesgoPuestoField;
            }
            set
            {
                this.riesgoPuestoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_prddpg", Length = 3, IsNullable = true, ColumnDescription = "clave periodicidad de pago")]
        public string PeriodicidadPago
        {
            get
            {
                return this.periodicidadPagoField;
            }
            set
            {
                this.periodicidadPagoField = value.Trim();
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_tpcntr", Length = 3, IsNullable = true, ColumnDescription = "clave tipo de contrato")]
        public string TipoContrato
        {
            get
            {
                return this.tipoContratoField;
            }
            set
            {
                this.tipoContratoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_tpjrn", Length = 3, IsNullable = true, ColumnDescription = "clave de tipo de jornada")]
        public string TipoJornada
        {
            get
            {
                return this.tipoJornadaField;
            }
            set
            {
                this.tipoJornadaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_clvbnc", Length = 3, IsNullable = true, ColumnDescription = "clave banco")]
        public string ClaveBanco
        {
            get
            {
                return this.claveBancoField;
            }
            set
            {
                this.claveBancoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave interna del empleado
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_clv", Length = 10, ColumnDescription = "clave de control de empleado")]
        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value.Trim().ToUpper();
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer Registro Federal de Contribuyentes
        /// </summary
        [SugarColumn(ColumnName = "_ctlemp_rfc", Length = 14, IsNullable = true, ColumnDescription = "registro federal de causantes")]
        public new string RFC
        {
            get
            {
                return base.RFC;
            }
            set
            {
                base.RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer NumSeguridadSocial: opcional para la expresion del numero de seguridad social aplicable al trabajador
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_nss", Length = 16, IsNullable = true, ColumnDescription = "NumSeguridadSocial: opcional para la expresion del numero de seguridad social aplicable al trabajador")]
        public string NumSeguridadSocial
        {
            get
            {
                return this.numSeguridadSocialField;
            }
            set
            {
                this.numSeguridadSocialField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer el numero de la cuenta de banco o numero de tarjeta
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_ctaban", Length = 16, IsNullable = true, ColumnDescription = "numero de cuenta de banco")]
        public string CuentaBancaria
        {
            get
            {
                return this.cuentaBancariaField;
            }
            set
            {
                this.cuentaBancariaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_numem", Length = 20, ColumnDescription = "numero de empleado")]
        public int Num
        {
            get
            {
                return this.numEmpleadoField;
            }
            set
            {
                this.numEmpleadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Clave Unica de Registro de Población
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_curp", Length = 20, IsNullable = true, ColumnDescription = "clave unica de registro")]
        public new string CURP
        {
            get
            {
                return base.CURP;
            }
            set
            {
                base.CURP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer telefono
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_tlfn", Length = 20, IsNullable = true, ColumnDescription = "numero de telefono")]
        public string Telefono
        {
            get
            {
                return this.telefonoField;
            }
            set
            {
                this.telefonoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer telefono
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_tlfn2", Length = 20, IsNullable = true, ColumnDescription = "numero de telefono 2, opcional")]
        public string Telefono2
        {
            get
            {
                return this.telefono2Field;
            }
            set
            {
                this.telefono2Field = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_depto", Length = 32, IsNullable = true, ColumnDescription = "departamento")]
        public string Departamento
        {
            get
            {
                return this.departamentoField;
            }
            set
            {
                this.departamentoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_puesto", Length = 32, IsNullable = true, ColumnDescription = "puesto")]
        public string Puesto
        {
            get
            {
                return this.puestoField;
            }
            set
            {
                this.puestoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_nom", Length = 80, ColumnDescription = "nombre(s) del empleado")]
        public new string Nombre
        {
            get
            {
                return base.Nombre;
            }
            set
            {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_pape", Length = 80, ColumnDescription = "primer apellido")]
        public string PrimerApellido
        {
            get
            {
                return this.primerapellidoField;
            }
            set
            {
                this.primerapellidoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_sape", Length = 80, ColumnDescription = "segundo apellido")]
        public string SegundoApellido
        {
            get
            {
                return this.segundoapellidoField;
            }
            set
            {
                this.segundoapellidoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer correo electronico
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_mail", Length = 80, IsNullable = true, ColumnDescription = "correo electronico")]
        public string Correo
        {
            get
            {
                return this.correoField;
            }
            set
            {
                this.correoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_fchrel", ColumnDescription = "fecha de inicio de relacion laboral")]
        public DateTime? FechaInicioRelLaboral
        {
            get
            {
                return this.fechaInicioRelLaboralField;
            }
            set
            {
                this.fechaInicioRelLaboralField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_sb", DecimalDigits = 4, IsNullable = true, ColumnDescription = "salario base")]
        public decimal SalarioBase
        {
            get
            {
                return this.salarioBaseField;
            }
            set
            {
                this.salarioBaseField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_drintg", DecimalDigits = 4, IsNullable = true, ColumnDescription = "salario diario integrado")]
        public decimal SalarioDiario
        {
            get
            {
                return this.salarioDiarioField;
            }
            set
            {
                this.salarioDiarioField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_sd", DecimalDigits = 4, IsNullable = true, ColumnDescription = "salario base")]
        public decimal SalarioDiarioIntegrado
        {
            get
            {
                return this.salarioDiarioIntegreadoField;
            }
            set
            {
                this.salarioDiarioIntegreadoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_fn", ColumnDescription = "fecha de registro")]
        public new DateTime FechaNuevo
        {
            get
            {
                return base.FechaNuevo;
            }
            set
            {
                base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_fm", IsNullable = true, ColumnDescription = "ultima fecha de modificaciones", IsOnlyIgnoreInsert = true)]
        public new DateTime? FechaModifica
        {
            get
            {
                return base.FechaModifica;
            }
            set
            {
                base.FechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_usr_n", IsNullable = true, ColumnDescription = "usuario creo registro", Length = 10)]
        public new string Creo
        {
            get
            {
                return base.Creo;
            }
            set
            {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_usr_m", IsNullable = true, ColumnDescription = "ultima usuario que modifico", Length = 10, IsOnlyIgnoreInsert = true)]
        public new string Modifica
        {
            get
            {
                return base.Modifica;
            }
            set
            {
                base.Modifica = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string Antiguedad
        {
            get
            {
                return this.antiguedad;
            }
            set
            {
                this.antiguedad = value;
                this.OnPropertyChanged();
            }
        }
        
        [SugarColumn(IsIgnore = true)]
        public decimal SalarioBaseCotApor
        {
            get
            {
                return this.salarioBaseCotApor;
            }
            set
            {
                this.salarioBaseCotApor = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string RegistroPatronal
        {
            get
            {
                return this.registroPatronal;
            }
            set
            {
                this.registroPatronal = value;
                this.OnPropertyChanged();
            }
        }

        public string NombreCompleto()
        {
            return string.Format("{0} {1} {2}", this.PrimerApellido, this.SegundoApellido, this.Nombre);
        }

    }
}
