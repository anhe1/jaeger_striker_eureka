/// develop: Antonio Hernández R.
/// purpose: control de nominas
using System;
using SqlSugar;

namespace Jaeger.Edita.V2.Nomina.Entities
{
    [SugarTable("_nmnctrl")]
    public class ViewModelControlNominaSingle : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private int estadoField;
        private string tipoNominaField;
        private DateTime? finPagoField;
        private int tipoPeriodoField;
        private DateTime? inicioPagoField;
        private string descripconField;
        private DateTime? fechaPagoField;
        private decimal totalValesDespensaField;
        private int indiceField;
        private decimal totalDeduccionesField;
        private bool activoField;
        private decimal fotalFondoAhorroField;
        private int diasPeriodoField;
        private decimal totalPercepcionesField;

        public ViewModelControlNominaSingle()
        {
        }

        /// <summary>
        /// Desc:indice
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnctrl_id", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:activo
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnctrl_a", ColumnDescription = "registro activo", Length = 1)]
        public bool Activo
        {
            get
            {
                return this.activoField;
            }
            set
            {
                this.activoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:estado
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnctrl_edo", ColumnDescription = "estado", Length = 2)]
        public int Estado 
        {
            get
            {
                return this.estadoField;
            }
            set
            {
                this.estadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:tipo de nomina
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnctrl_tp", ColumnDescription = "tipo de nomina", Length = 2)]
        public string TipoNomina
        {
            get
            {
                return this.tipoNominaField;
            }
            set
            {
                this.tipoNominaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:tipo de periodo
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnctrl_tpprd", ColumnDescription = "tipo de periodo", Length = 3)]
        public int TipoPeriodo
        {
            get
            {
                return this.tipoPeriodoField;
            }
            set
            {
                this.tipoPeriodoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:dias del periodo
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnctrl_dsprd", ColumnDescription = "dias del periodo", Length = 11)]
        public int DiasPeriodo
        {
            get
            {
                return this.diasPeriodoField;
            }
            set
            {
                this.diasPeriodoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de inicio de pago
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnctrl_fcini", ColumnDescription = "fecha de inicio de pago")]
        public DateTime? InicioPago
        {
            get
            {
                return this.inicioPagoField;
            }
            set
            {
                this.inicioPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de fin de pago
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnctrl_fcfin", ColumnDescription = "fecha de fin de pago")]
        public DateTime? FinPago
        {
            get
            {
                return this.finPagoField;
            }
            set
            {
                this.finPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de pago
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnctrl_fcpgo", ColumnDescription = "fecha de pago")]
        public DateTime? FechaPago
        {
            get
            {
                return this.fechaPagoField;
            }
            set
            {
                this.fechaPagoField = value;
            }
        }

        /// <summary>
        /// Desc:total de deducciones
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnctrl_tded", ColumnDescription = "total de deducciones", Length = 14, DecimalDigits = 4)]
        public decimal TotalDeducciones
        {
            get
            {
                return this.totalDeduccionesField;
            }
            set
            {
                this.totalDeduccionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:total de percepciones
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnctrl_tper", ColumnDescription = "total de deducciones", Length = 14, DecimalDigits = 4)]
        public decimal TotalPercepciones
        {
            get
            {
                return this.totalPercepcionesField;
            }
            set
            {
                this.totalPercepcionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:total de fondo de ahorro
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnctrl_tfda", ColumnDescription = "total de fondo de ahorro", Length = 14, DecimalDigits = 4)]
        public decimal TotalFondoAhorro
        {
            get
            {
                return this.fotalFondoAhorroField;
            }
            set
            {
                this.fotalFondoAhorroField = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Desc:total de vales de despensa
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnctrl_tvdd", ColumnDescription = "total de vales de despensa", Length = 14, DecimalDigits = 4)]
        public decimal TotalValesDespensa
        {
            get
            {
                return this.totalValesDespensaField;
            }
            set
            {
                this.totalValesDespensaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:descripcion
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnctrl_ttl", ColumnDescription = "descripcion", Length = 65)]
        public string Descripcion
        {
            get
            {
                return this.descripconField;
            }
            set
            {
                this.descripconField = value;
                this.OnPropertyChanged();
            }
        }

        private string archivoExcel;
        [SugarColumn(ColumnName = "_nmnctrl_excl", ColumnDescription = "archivo excel", Length = 255)]
        public string ArchivoExcel
        {
            get
            {
                return this.archivoExcel;
            }
            set
            {
                this.archivoExcel = value;
                this.OnPropertyChanged();
            }
        }

        private string archivoExcelNombre;
        [SugarColumn(ColumnName = "_nmnctrl_exclnm", ColumnDescription = "nombre del archivo de excel", Length = 255)]
        public string NombreArchivo
        {
            get
            {
                return this.archivoExcelNombre;
            }
            set
            {
                this.archivoExcelNombre = value;
                this.OnPropertyChanged();
            }
        }

        private DateTime? fechaSubida;
        [SugarColumn(ColumnName = "_nmnctrl_fch", ColumnDescription = "fecha de subida")]
        public DateTime? FechaSubida
        {
            get
            {
                return this.fechaSubida;
            }
            set
            {
                this.fechaSubida = value;
                this.OnPropertyChanged();
            }
        }

        private int totalFilas;
        [SugarColumn(ColumnName = "_nmnctrl_fls", ColumnDescription = "total de filas", Length = 4)]
        public int TotalFilas
        {
            get
            {
                return this.totalFilas;
            }
            set
            {
                this.totalFilas = value;
                this.OnPropertyChanged();
            }
        }

        private int registrosCargados;
        [SugarColumn(ColumnName = "_nmnctrl_crgds", ColumnDescription = "registros cargados", Length = 4)]
        public int RegistrosCargados
        {
            get
            {
                return this.registrosCargados;
            }
            set
            {
                this.registrosCargados = value;
                this.OnPropertyChanged();
            }
        }

        private int enviados;
        [SugarColumn(ColumnName = "_nmnctrl_envds", ColumnDescription = "enviados por correo", Length = 11)]
        public int Enviados
        {
            get
            {
                return this.enviados;
            }
            set
            {
                this.enviados = value;
                this.OnPropertyChanged();
            }
        }
    }
}