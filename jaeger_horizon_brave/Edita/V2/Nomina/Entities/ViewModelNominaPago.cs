﻿using System;
using System.ComponentModel;
using SqlSugar;
using Newtonsoft.Json;
using Jaeger.Edita.V2.Contable.Entities;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Edita.V2.Nomina.Entities
{
    [SugarTable("", "nomina: layout de pagos de nominas a empleados")]
    public class ViewModelNominaPago : BasePropertyChangeImplementation
    {
        private int index;
        private bool activo = true;
        private string grupoAfinidad;
        private int consecutivo;
        private DateTime fechaEmision;
        private DateTime? fechaAplicacion;
        private DateTime? fechaModifica;
        private string concepto;
        private string creo;
        private string modifica;
        private PrePolizaCuenta emisor;

        public ViewModelNominaPago()
        {
            this.emisor = new PrePolizaCuenta();
            this.fechaEmision = DateTime.Now;
            this.fechaAplicacion = DateTime.Now;
            this.empleados = new BindingList<ViewModelNominaPagoConcepto>();
        }

        [SugarColumn(ColumnName = "_id", ColumnDescription = "indice de la tabla")]
        public int Id
        {
            get { return index; }
            set { index = value; }
        }

        [SugarColumn(ColumnName = "_a", ColumnDescription = "")]
        public bool Activo
        {
            get
            {
                return this.activo;
            }
            set
            {
                this.activo = value;
            }
        }
        
        /// <summary>
        /// obtener o establecer el grupo de afinidad, utilizado para banjico
        /// </summary>
        [SugarColumn(ColumnName = "_grpaf", ColumnDescription = "")]
        public string GrupoAfinidad
        {
            get { return grupoAfinidad; }
            set { grupoAfinidad = value; }
        }

        /// <summary>
        /// obtener o establecer el consecutivo del archivo del día
        /// </summary>
        [SugarColumn(ColumnName = "_cons", ColumnDescription = "")]
        public int Consecutivo
        {
            get { return consecutivo; }
            set { consecutivo = value; }
        }

        /// <summary>
        /// obtener o establcer la fecha de aplicación para el banco
        /// </summary>
        [SugarColumn(ColumnName = "_fecapl", ColumnDescription = "")]
        public DateTime? FechaAplicacion
        {
            get { return fechaAplicacion; }
            set { fechaAplicacion = value; }
        }

        /// <summary>
        /// obtener o establcer la fecha de emisión 
        /// </summary>
        [SugarColumn(ColumnName = "_fn", ColumnDescription = "")]
        public DateTime FechaEmision
        {
            get { return fechaEmision; }
            set { fechaEmision = value; }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [SugarColumn(ColumnName = "_fm", ColumnDescription = "")]
        public DateTime? FechaModifica
        {
            get { return fechaModifica; }
            set { fechaModifica = value; }
        }

        /// <summary>
        /// obtener o establecer el concepto de pago
        /// </summary>
        [SugarColumn(ColumnName = "_desc", ColumnDescription = "")]
        public string Concepto
        {
            get { return concepto; }
            set { concepto = value; }
        }

        /// <summary>
        /// obtener o establcer la clave del usuario que crea el registro
        /// </summary>
        [SugarColumn(ColumnName = "_usr_n", ColumnDescription = "")]
        public string Creo
        {
            get { return creo; }
            set { creo = value; }
        }

        /// <summary>
        /// obtener o establcer la clave del ultimo ususario que modifica el registro
        /// </summary>
        [SugarColumn(ColumnName = "_usr_m", ColumnDescription = "")]
        public string Modifica
        {
            get { return modifica; }
            set { modifica = value; }
        }

        public PrePolizaCuenta CuentaEmisora
        {
            get
            {
                return this.emisor;
            }
            set
            {
                this.emisor = value;
                this.OnPropertyChanged();
            }
        }

        private BindingList<ViewModelNominaPagoConcepto> empleados;

        public BindingList<ViewModelNominaPagoConcepto> Empleados
        {
            get { return empleados; }
            set { empleados = value; }
        }

    }

    [JsonObject("item")]
    public class ViewModelNominaPagoConcepto : CuentaBancaria
    {
        private int consecutivo;
        private decimal importe;

        public int Consecutivo
        {
            get 
            { 
                return this.consecutivo; 
            }
            set 
            {
                this.consecutivo = value;
                this.OnPropertyChanged();
             }
        }
        
        public decimal Importe
        {
            get { return this.importe; }
            set 
            { 
                this.importe = value;
                this.OnPropertyChanged();
            }
        }

        public string NombreCompleto()
        {
            return string.Format("{0} {1} {2}", this.PrimerApellido, this.SegundoApellido, this.Nombre);
        }
    }
}
