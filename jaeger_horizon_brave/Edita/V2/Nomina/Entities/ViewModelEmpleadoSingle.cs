﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;

namespace Jaeger.Edita.V2.Nomina.Entities
{
    [SugarTable("_ctlemp")]
    public class ViewModelEmpleadoSingle
    {
        /// <summary>
        /// obtener o establecer el indice del directorio
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_id", IsPrimaryKey = true, IsIdentity = true)]
        public int Id { get; set; }

        [SugarColumn(ColumnName = "_ctlemp_drctr_id", IsNullable = true, ColumnDescription = "indice con el directorio principal")]
        public int IdDirectorio { get; set; }

        [SugarColumn(ColumnName = "_ctlemp_a", ColumnDescription = "registro activo")]
        public new bool IsActive { get; set; }

        [SugarColumn(ColumnName = "_ctlemp_rsgpst", Length = 3, IsNullable = true, ColumnDescription = "clave riesgo de puesto")]
        public string RiesgoPuesto { get; set; }

        /// <summary>
        /// obtener o establecer la clave interna del empleado
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_clv", Length = 10, ColumnDescription = "clave de control de empleado")]
        public string Clave { get; set; }

        /// <summary>
        /// obtener o establacer Registro Federal de Contribuyentes
        /// </summary
        [SugarColumn(ColumnName = "_ctlemp_rfc", Length = 14, IsNullable = true, ColumnDescription = "registro federal de causantes")]
        public string RFC { get; set; }

        [SugarColumn(ColumnName = "_ctlemp_depto", Length = 32, IsNullable = true, ColumnDescription = "departamento")]
        public string Departamento { get; set; }

        [SugarColumn(ColumnName = "_ctlemp_puesto", Length = 32, IsNullable = true, ColumnDescription = "puesto")]
        public string Puesto { get; set; }

        [SugarColumn(ColumnName = "_ctlemp_nom", Length = 80, ColumnDescription = "nombre(s) del empleado")]
        public string Nombres { get; set; }

        [SugarColumn(ColumnName = "_ctlemp_pape", Length = 80, ColumnDescription = "primer apellido")]
        public string PrimerApellido { get; set; }

        [SugarColumn(ColumnName = "_ctlemp_sape", Length = 80, ColumnDescription = "segundo apellido")]
        public string SegundoApellido { get; set; }

        [SugarColumn(IsIgnore = true)]
        public string Nombre
        {
            get
            {
                return string.Format("{0} {1} {2}", this.PrimerApellido, this.SegundoApellido, this.Nombres);
            }
        }
    }
}
