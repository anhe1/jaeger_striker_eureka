﻿/// develop: anhe 040120191643
/// purpose: configuracion del periodo
using System;
using System.Linq;

namespace Jaeger.Edita.V2.Nomina.Entities
{
    public class Periodo : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string tituloField;
        private DateTime fechaInicioField;
        private DateTime fechaFinField;

        /// <summary>
        /// obtener o establecer el titulo
        /// </summary>
        public string Titulo
        {
            get
            {
                return this.tituloField;
            }
            set
            {
                this.tituloField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime FechaInicio
        {
            get
            {
                return this.fechaInicioField;
            }
            set
            {
                this.fechaInicioField = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime FechaFin
        {
            get
            {
                return this.fechaFinField;
            }
            set
            {
                this.fechaFinField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int DiasPeriodo
        {
            get
            {
                TimeSpan tspan = this.FechaInicio - this.FechaFin;
                return tspan.Days;
            }
        }
    }
}
