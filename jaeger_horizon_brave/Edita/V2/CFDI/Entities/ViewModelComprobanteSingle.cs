﻿using System;
using Newtonsoft.Json;
using SqlSugar;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.CFDI.Entities {
    [SugarTable("_cfdi")]
    public class ViewModelComprobanteSingle : BasePropertyChangeImplementation {
        #region declaraciones

        private int lngIndex;
        private int lngSubIndex;
        private bool blnIsActive;
        private string strKaijuCreate;
        private string strKaijuChange;
        private DateTime dateCreateNew;
        private DateTime? dateChangeDate;

        private string versionField;
        private EnumCfdiType tipoComprobanteField;
        private EnumCfdiSubType subTipoDeComprobanteField;
        private string folioField;
        private string serieField;
        private DateTime fechaEmisionField;
        private DateTime? fechaPagoField;

        private decimal tipoDeCambioField;
        private string statusField;
        private string estadoField;
        private int numParcialidadField;
        private int precisionDecimalField;

        // emisor del comprobante
        private string emisorField;
        private string emisorRfcField;
        private string emisorClaveField;

        // receptor del comprobante
        private string receptorField;
        private string receptorRfcField;
        private string receptorClaveField;
        private string claveUsoCFDIField;

        private string uuidField;
        private DateTime? fechaTimbradoField = null;

        // forma de pago
        private string claveMonedaField;
        private string claveFormaPagoField;
        private string claveMetodoPagoField;
        private string claveCondicionPagoField;

        // impuestos
        private decimal retencionIsrField;
        private decimal retencionIvaField;
        private decimal trasladoIvaField;
        private decimal retencionIepsField;
        private decimal trasladoIepsField;

        // montos
        private decimal subTotalField;
        private decimal descuentoField;
        private decimal totalField;
        private decimal acumuladoField;
        private decimal saldoField;

        private string urlXmlField;
        private string urlPdfField;
        

        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ViewModelComprobanteSingle() {

        }

        #region propiedades

        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        [JsonIgnore]
        [DataNames("_cfdi_id")]
        [SugarColumn(ColumnName = "_cfdi_id", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this.lngIndex;
            }
            set {
                if (value != this.lngIndex) {
                    this.lngIndex = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el estado del registro
        /// </summary>
        [JsonIgnore]
        [DataNames("_cfdi_a")]
        [SugarColumn(ColumnName = "_cfdi_a", ColumnDescription = "registro activo", IsNullable = false)]
        public bool Activo {
            get {
                return this.blnIsActive;
            }
            set {
                if (value != this.blnIsActive) {
                    this.blnIsActive = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer sub indice de relacion, actualmente esta ignorado por la base
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public int SubId {
            get {
                return this.lngSubIndex;
            }
            set {
                if (this.lngSubIndex != value) {
                    this.lngSubIndex = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// version del comprobante fiscal
        /// </summary>
        [JsonProperty("ver")]
        [DataNames("_cfdi_ver")]
        [SugarColumn(ColumnName = "_cfdi_ver", ColumnDescription = "version del comprobante fiscal", Length = 3, IsNullable = true)]
        public string Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de comprobante
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public EnumCfdiType TipoComprobante {
            get {
                return this.tipoComprobanteField;
            }
            set {
                this.tipoComprobanteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer status del comprobante (0-Cancelado,1-Importado,2-Por Pagar)
        /// </summary>
        [JsonProperty("tipoComprobante")]
        [DataNames("_cfdi_efecto")]
        [SugarColumn(ColumnName = "_cfdi_efecto", ColumnDescription = "status del comprobante (0-Cancelado,1-Importado,2-Por Pagar)", Length = 10, IsNullable = true)]
        public string TipoComprobanteText {
            get {
                return Enum.GetName(typeof(EnumCfdiType), this.TipoComprobante);
            }
            set {
                if (value == "Ingreso" || value == "I") {
                    this.TipoComprobante = EnumCfdiType.Ingreso;
                }
                else if (value == "Egreso" || value == "E") {
                    this.TipoComprobante = EnumCfdiType.Egreso;
                }
                else if (value == "Traslado" || value == "T") {
                    this.TipoComprobante = EnumCfdiType.Traslado;
                }
                else if (value == "Nomina" || value == "N") {
                    this.TipoComprobante = EnumCfdiType.Nomina;
                }
                else if (value == "Pagos" || value == "P") {
                    this.TipoComprobante = EnumCfdiType.Pagos;
                }
                else {
                    this.TipoComprobante = EnumCfdiType.Ingreso;
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de documento (1=emitido,2=recibido,3=nomina)
        /// </summary>
        [JsonIgnore]
        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "_cfdi_doc_id", ColumnDescription = "tipo de documento (1=emitido,2=recibido,3=nomina)", Length = 1, IsNullable = true)]
        public EnumCfdiSubType SubTipo {
            get {
                return this.subTipoDeComprobanteField;
            }
            set {
                this.subTipoDeComprobanteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de documento (1=emitido,2=recibido,3=nomina) se agrega debido a que no existe un metodo en la utilidad mapper para asingar el valor
        /// </summary>
        [DataNames("_cfdi_doc_id")]
        [SugarColumn(IsIgnore = true)]
        public int SubTipoInt {
            get {
                return (int)this.subTipoDeComprobanteField;
            }
            set {
                if (value == 1)
                    this.subTipoDeComprobanteField = EnumCfdiSubType.Emitido;
                else if (value == 2)
                    this.subTipoDeComprobanteField = EnumCfdiSubType.Recibido;
                else if (value == 3)
                    this.subTipoDeComprobanteField = EnumCfdiSubType.Nomina;
                else
                    this.subTipoDeComprobanteField = EnumCfdiSubType.None;
            }
        }

        [JsonProperty("subTipoComprobante")]
        [SugarColumn(IsIgnore = true)]
        public string SubTipoText {
            get {
                return Enum.GetName(typeof(EnumCfdiSubType), this.subTipoDeComprobanteField);
            }
            set {
                this.subTipoDeComprobanteField = (EnumCfdiSubType)Enum.Parse(typeof(EnumCfdiSubType), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para precisar el folio del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        [JsonProperty("folio")]
        [DataNames("_cfdi_folio")]
        [SugarColumn(ColumnName = "_cfdi_folio", ColumnDescription = "para control interno del contribuyente que acepta un valor numerico entero superior a 0 que expresa el folio del comprobante", Length = 22, IsNullable = true)]
        public string Folio {
            get {
                return this.folioField;
            }
            set {
                this.folioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para precisar la serie del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        [JsonProperty("serie")]
        [DataNames("_cfdi_serie")]
        [SugarColumn(ColumnName = "_cfdi_serie", ColumnDescription = "opcional para precisar la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres alfabeticos de 1 a 25 caracteres sin incluir caracteres acentuados", Length = 25, IsNullable = true)]
        public string Serie {
            get {
                return this.serieField;
            }
            set {
                this.serieField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("tipoCambio")]
        [SugarColumn(IsIgnore = true)]
        public decimal TipoCambio {
            get {
                return this.tipoDeCambioField;
            }
            set {
                this.tipoDeCambioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el status del comprobante
        /// </summary>
        [JsonProperty("status")]
        [DataNames("_cfdi_status")]
        [SugarColumn(ColumnName = "_cfdi_status", ColumnDescription = "status del comprobante (0-Cancelado,1-Importado,2-Por Pagar", Length = 10, IsNullable = true)]
        public string Status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el estado de comprobante segun servicio SAT
        /// </summary>
        [JsonProperty("estado")]
        [DataNames("_cfdi_estado")]
        [SugarColumn(ColumnName = "_cfdi_estado", ColumnDescription = "estado del comprobante (correcto,error!,cancelado,vigente)", Length = 25, IsNullable = true)]
        public string Estado {
            get {
                return this.estadoField;
            }
            set {
                this.estadoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("emisor")]
        [DataNames("_cfdi_nome")]
        [SugarColumn(ColumnName = "_cfdi_nome", ColumnDescription = "identidad fiscal del receptor o emisor del comprobante fiscal", Length = 255, IsNullable = true)]
        public string Emisor {
            get {
                return this.emisorField;
            }
            set {
                this.emisorField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("emisorRFC")]
        [DataNames("_cfdi_rfce")]
        [SugarColumn(ColumnName = "_cfdi_rfce", ColumnDescription = "rfc del emisor del comprobante", Length = 14, IsNullable = true)]
        public string EmisorRFC {
            get {
                return this.emisorRfcField;
            }
            set {
                this.emisorRfcField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("emisorClave")]
        [SugarColumn(IsIgnore = true)]
        public string EmisorClave {
            get {
                return this.emisorClaveField;
            }
            set {
                this.emisorClaveField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("receptor")]
        [DataNames("_cfdi_nomr")]
        [SugarColumn(ColumnName = "_cfdi_nomr", ColumnDescription = "identidad fiscal del receptor o emisor del comprobante fiscal", Length = 255, IsNullable = true)]
        public string Receptor {
            get {
                return this.receptorField;
            }
            set {
                this.receptorField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("receptorRFC")]
        [DataNames("_cfdi_rfcr")]
        [SugarColumn(ColumnName = "_cfdi_rfcr", ColumnDescription = "rfc del receptor del comprobante", Length = 14, IsNullable = true)]
        public string ReceptorRFC {
            get {
                return this.receptorRfcField;
            }
            set {
                this.receptorRfcField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("receptorClave")]
        [SugarColumn(IsIgnore = true)]
        public string ReceptorClave {
            get {
                return this.receptorClaveField;
            }
            set {
                this.receptorClaveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el número de parcialidad que corresponde al pago. Es requerido cuando MetodoDePagoDR contiene: “PPD” Pago en parcialidades o diferido.
        /// </summary>
        [JsonProperty("numPar")]
        [DataNames("_cfdi_par")]
        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "_cfdi_par", ColumnDescription = "numero de parcialidad", Length = 1, IsNullable = true)]
        public int NumParcialidad {
            get {
                return this.numParcialidadField;
            }
            set {
                this.numParcialidadField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("presc")]
        [DataNames("_cfdi_prec")]
        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "_cfdi_prec", ColumnDescription = "precision de decimales", Length = 1, IsNullable = true)]
        public int PrecisionDecimal {
            get {
                return this.precisionDecimalField;
            }
            set {
                this.precisionDecimalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtiene o establece los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        [JsonProperty("uuid")]
        [DataNames("_cfdi_uuid")]
        [SugarColumn(ColumnName = "_cfdi_uuid", ColumnDescription = "folio fiscal", Length = 36, IsNullable = true)]
        public string IdDocumento {
            get {
                return this.uuidField;
            }
            set {
                this.uuidField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("fecEmision")]
        [DataNames("_cfdi_fecems")]
        [SugarColumn(ColumnDataType = "TIMESTAMP", ColumnName = "_cfdi_fecems", ColumnDescription = "fecha de emisión del comprobante", IsNullable = false)]
        public DateTime FechaEmision {
            get {
                return this.fechaEmisionField;
            }
            set {
                this.fechaEmisionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha y hora, de la generación del timbre por la certificación digital del SAT. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora de la Zona Centro del Sistema de Horario en México.
        /// </summary>
        [JsonProperty("fecTimbre")]
        [DataNames("_cfdi_feccert")]
        [SugarColumn(ColumnName = "_cfdi_feccert", ColumnDescription = "fecha de certificacion", IsNullable = true)]
        public DateTime? FechaTimbrado {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaTimbradoField >= firstGoodDate)
                    return this.fechaTimbradoField.Value;
                return null;
            }
            set {
                this.fechaTimbradoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha del estado del comprobante (SAT)
        /// </summary>
        [DataNames("_cfdi_fecedo")]
        [SugarColumn(ColumnName = "_cfdi_fecedo")]
        public DateTime? FechaEstado { get; set; }

        private DateTime? fechaCancelaField;
        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdi_feccnc")]
        [SugarColumn(ColumnName = "_cfdi_feccnc")]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancelaField >= firstGoodDate)
                    return this.fechaCancelaField;
                return null;
            }
            set {
                this.fechaCancelaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de entrega o recepcion del comprobante
        /// </summary>
        [JsonIgnore]
        [DataNames("_cfdi_fecent")]
        [SugarColumn(ColumnName = "_cfdi_fecent", IsNullable = true)]
        public DateTime? FechaEntrega { get; set; }

        [JsonProperty("fecPago")]
        [DataNames("_cfdi_fecupc")]
        [SugarColumn(ColumnName = "_cfdi_fecupc", ColumnDescription = "fecha del ultimo pago o cobro de los recibos de pagoo cobro", IsNullable = true)]
        public DateTime? FechaUltimoPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaPagoField >= firstGoodDate)
                    return this.fechaPagoField;
                return null;
            }
            set {
                this.fechaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        private DateTime? fechaValidacionField;
        /// <summary>
        /// obtener o establecer fecha de validacion del comprobante
        /// </summary>
        [DataNames("_cfdi_fecval")]
        [SugarColumn(ColumnName = "_cfdi_fecval")]
        public DateTime? FechaValidacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaValidacionField >= firstGoodDate)
                    return this.fechaValidacionField;
                return null;
            }
            set {
                this.fechaValidacionField = value;
                this.OnPropertyChanged();
            }
        }

        private DateTime? fechaRecepcionPagoField;
        /// <summary>
        /// fecha del comprobante fiscal de repecion de pago
        /// </summary>
        [DataNames("_cfdi_fecpgr")]
        [SugarColumn(ColumnName = "_cfdi_fecpgr")]
        public DateTime? FechaRecepcionPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaRecepcionPagoField >= firstGoodDate)
                    return this.fechaRecepcionPagoField;
                return null;
            }
            set {
                this.fechaRecepcionPagoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("claveUsoCFDI")]
        [DataNames("_cfdi_usocfdi")]
        [SugarColumn(ColumnName = "_cfdi_usocfdi", ColumnDescription = "para expresar la clave del uso que dará a esta factura el receptor del CFDI", Length = 4, IsNullable = true)]
        public string ClaveUsoCFDI {
            get {
                return this.claveUsoCFDIField;
            }
            set {
                this.claveUsoCFDIField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para identificar la clave de la moneda utilizada en los importes del documento relacionado, cuando se usa moneda nacional o el documento relacionado no especifica la moneda se registra MXN. Los importes registrados en los atributos “ImpSaldoAnt”, “ImpPagado” e “ImpSaldoInsoluto” de éste nodo, deben corresponder a esta moneda. Conforme con la especificación ISO 4217.
        /// </summary>
        [JsonProperty("claveMoneda")]
        [DataNames("_cfdi_moneda")]
        [SugarColumn(ColumnName = "_cfdi_moneda", ColumnDescription = "para representar el tipo de cambio conforme a la moneda usada para representar el tipo de cambio conforme a la moneda usada", Length = 21, IsNullable = true)]
        public string Moneda {
            get {
                return this.claveMonedaField;
            }
            set {
                this.claveMonedaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("claveFormaPago")]
        [DataNames("_cfdi_frmpg")]
        [SugarColumn(ColumnName = "_cfdi_frmpg", ColumnDescription = "forma de pago que aplica para este comprobante fiscal digital. Se utiliza para expresar Pago en una sola exhibicion o numero de parcialidad pagada contra el total de parcialidades, Parcialidad 1 de X", Length = 64, IsNullable = true)]
        public string FormaPago {
            get {
                return this.claveFormaPagoField;
            }
            set {
                this.claveFormaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la clave del método de pago que se registró en el documento relacionado
        /// </summary>
        [JsonProperty("claveMetodoPago")]
        [DataNames("_cfdi_mtdpg")]
        [SugarColumn(ColumnName = "_cfdi_mtdpg", ColumnDescription = "para expresar el metodo de pago de los bienes o servicios amparados por el comprobante. Se entiende como metodo de pago leyendas tales como: cheque, tarjeta de credito o debito, deposito en cuenta, etc.", Length = 64, IsNullable = true)]
        public string MetodoPago {
            get {
                return this.claveMetodoPagoField;
            }
            set {
                this.claveMetodoPagoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("claveCondicionPago")]
        [DataNames("_cfdi_cntpg")]
        [SugarColumn(ColumnName = "_cfdi_cntpg", ColumnDescription = "para incorporar al menos los cuatro ultimos digitos del numero de cuenta con la que se realizo el pago", Length = 64, IsNullable = true)]
        public string CondicionPago {
            get {
                return this.claveCondicionPagoField;
            }
            set {
                this.claveCondicionPagoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("subTotal")]
        [DataNames("_cfdi_sbttl")]
        [SugarColumn(ColumnName = "_cfdi_sbttl", ColumnDescription = "representar la suma de los importes antes de descuentos e impuestos", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal SubTotal {
            get {
                return this.subTotalField;
            }
            set {
                this.subTotalField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("retencionISR")]
        [DataNames("_cfdi_retisr")]
        [SugarColumn(ColumnName = "_cfdi_retisr", ColumnDescription = "retencion del impuesto sobre la renta", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal RetencionISR {
            get {
                return this.retencionIsrField;
            }
            set {
                this.retencionIsrField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("retencionIVA")]
        [DataNames("_cfdi_retiva")]
        [SugarColumn(ColumnName = "_cfdi_retiva", ColumnDescription = "importe de la retencion del IVA", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal RetencionIVA {
            get {
                return this.retencionIvaField;
            }
            set {
                this.retencionIvaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("trasladoIVA")]
        [DataNames("_cfdi_trsiva")]
        [SugarColumn(ColumnName = "_cfdi_trsiva", ColumnDescription = "importe del impuesto del IVA trasladado", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal TrasladoIVA {
            get {
                return this.trasladoIvaField;
            }
            set {
                this.trasladoIvaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("retencionIEPS")]
        [DataNames("_cfdi_retieps")]
        [SugarColumn(ColumnName = "_cfdi_retieps", ColumnDescription = "importe de la retencion del impuesto IEPS", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal RetencionIEPS {
            get {
                return this.retencionIepsField;
            }
            set {
                this.retencionIepsField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("trasladoIEPS")]
        [DataNames("_cfdi_trsieps")]
        [SugarColumn(ColumnName = "_cfdi_trsieps", ColumnDescription = "impuesto especial sobre productos y servicios", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal TrasladoIEPS {
            get {
                return this.trasladoIepsField;
            }
            set {
                this.trasladoIepsField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("descuento")]
        [DataNames("_cfdi_dscnt")]
        [SugarColumn(ColumnName = "_cfdi_dscnt", ColumnDescription = "monto de los descuentos aplicados a el comprobante fiscal (nota de credito)", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal Descuento {
            get {
                return this.descuentoField;
            }
            set {
                this.descuentoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("total")]
        [DataNames("_cfdi_total")]
        [SugarColumn(ColumnName = "_cfdi_total", ColumnDescription = "representa la suma del subtotal, menos los descuentos aplicables, mas los impuestos, menos los impuestos retenidos", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal Total {
            get {
                return this.totalField;
            }
            set {
                this.totalField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("acumulado")]
        [DataNames("_cfdi_cbrd")]
        [SugarColumn(ColumnName = "_cfdi_cbrd", ColumnDescription = "monto cobrado o bien lo que se a pagado", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal Acumulado {
            get {
                return this.acumuladoField;
            }
            set {
                this.acumuladoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("saldo")]
        [SugarColumn(ColumnName = "_cfdi_saldo", Length = 14, DecimalDigits = 4, IsNullable = true, IsIgnore = true)]
        public decimal Saldo {
            get {
                return this.Total - this.Acumulado;
                //return this.saldoField;
            }
            set {
                this.saldoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el acuse de cancelacion del comprobante
        /// </summary>
        //[JsonIgnore]
        //[DataNames("_cfdi_acuse")]
        //[SugarColumn(ColumnDataType = "Text", ColumnName = "_cfdi_acuse", ColumnDescription = "acuse de cancelacion", IsNullable = true)]
        //public string Accuse
        //{
        //    get
        //    {
        //        return this.accuseField;
        //    }
        //    set
        //    {
        //        this.accuseField = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        //[JsonIgnore]
        //[SugarColumn(ColumnName = "_cfdi_keyname", IsIgnore = )]
        //public string KeyName
        //{
        //    get
        //    {
        //        return this.keyNameField;
        //    }
        //    set
        //    {
        //        this.keyNameField = value;
        //        this.OnPropertyChanged();
        //    }
        //}



        //(_cfdi_total - (_cfdi_cbrd + _cfdi_dscntc)) as _cfdi_saldo, (_cfdi_total - _cfdi_cbrdp) as _cfdi_saldopagos
        [SugarColumn(IsIgnore = true)]
        public decimal SaldoPagos {
            get {
                return this.Total - this.Acumulado;
            }
        }

        /// <summary>
        /// importe del pago del comprobante de recepcion de pagos
        /// </summary>
        [SugarColumn(ColumnName = "_cfdi_cbrdp")]
        [DataNames("_cfdi_cbrdp")]
        public decimal ImportesPagos { get; set; }

        [JsonIgnore]
        [DataNames("_cfdi_url_xml")]
        [SugarColumn(ColumnDataType = "Text", ColumnName = "_cfdi_url_xml", ColumnDescription = "url de descarga del archivo xml del acuse de cancelacion del comprobante", IsNullable = true)]
        public string UrlXml {
            get {
                return this.urlXmlField;
            }
            set {
                this.urlXmlField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cfdi_url_pdf")]
        [SugarColumn(ColumnDataType = "Text", ColumnName = "_cfdi_url_pdf", ColumnDescription = "url de descarga del archivo pdf del acuse de cancelacion del comprobante", IsNullable = true)]
        public string UrlPdf {
            get {
                return this.urlPdfField;
            }
            set {
                this.urlPdfField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// url del acuse de cancelacion
        /// </summary>
        [JsonIgnore]
        [DataNames("_cfdi_url_xmlacu")]
        [SugarColumn(ColumnName = "_cfdi_url_xmlacu")]
        public string UrlAcuseXml { get; set; }

        /// <summary>
        /// url de la representacion impresa del acuse de cancelacion (pdf)
        /// </summary>
        [JsonIgnore]
        [DataNames("_cfdi_url_pdfacu")]
        [SugarColumn(ColumnName = "_cfdi_url_pdfacu")]
        public string UrlAcusePdf { get; set; }

        /// <summary>
        /// clave de usuario que creó el registro
        /// </summary>
        [JsonIgnore]
        [DataNames("_cfdi_usr_n")]
        [SugarColumn(ColumnName = "_cfdi_usr_n", ColumnDescription = "clave de usuario que crea el registro", Length = 10, IsNullable = false)]
        public string Creo {
            get {
                return this.strKaijuCreate;
            }
            set {
                if (this.strKaijuCreate != value) {
                    this.strKaijuCreate = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifico el registro
        /// </summary>
        [JsonIgnore]
        [DataNames("_cfdi_usr_m")]
        [SugarColumn(ColumnName = "_cfdi_usr_m", ColumnDescription = "clave del ultimo usuario que modifico el registro", Length = 10, IsNullable = true)]
        public string Modifica {
            get {
                return this.strKaijuChange;
            }
            set {
                if (this.strKaijuChange != value) {
                    this.strKaijuChange = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de registro en el sistema
        /// </summary>
        [JsonIgnore]
        [DataNames("_cfdi_fn")]
        [SugarColumn(ColumnName = "_cfdi_fn", ColumnDescription = "fecha de sistema", IsNullable = false)]
        public DateTime FechaNuevo {
            get {
                return this.dateCreateNew;
            }
            set {
                if (DateTime.Compare(this.dateCreateNew, value) != 0) {
                    this.dateCreateNew = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de la modificacion del registro
        /// </summary>
        [JsonIgnore]
        [DataNames("_cfdi_fm")]
        [SugarColumn(ColumnName = "_cfdi_fm", ColumnDescription = "ultima fecha de la modificacion del registro", IsNullable = true)]
        public DateTime? FechaMod {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.dateChangeDate >= firstGoodDate)
                    return this.dateChangeDate.Value;
                return null;
            }
            set {
                this.dateChangeDate = value;
            }
        }

        #endregion
    }
}
