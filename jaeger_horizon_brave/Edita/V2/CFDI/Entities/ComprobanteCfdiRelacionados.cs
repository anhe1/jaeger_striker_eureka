﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.CFDI.Entities
{
    public partial class ComprobanteCfdiRelacionadosCfdiRelacionado : Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoDoctoRelacionadoBase, IDataErrorInfo
    {
        private decimal importeAplicadoField;

        /// <summary>
        /// obtener o establecer el importe aplicado al comprobante, este aplica cuando es una nota de credito
        /// </summary>
        [JsonProperty("importe")]
        public decimal ImporteAplicado
        {
            get
            {
                return this.importeAplicadoField;
            }
            set
            {
                this.importeAplicadoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public string Error
        {
            get
            {
                if (!this.RegexValido(this.RFC, "[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9A]"))
                {
                    return "Por favor ingrese datos válidos en esta fila!";
                }
                return string.Empty;
            }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "RFC" && !this.RegexValido(this.RFC, "[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9A]") || columnName == "UUID" && !this.RegexValido(this.IdDocumento, "[a-f0-9A-F]{8}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{12}"))
                {
                    return string.Format("El RFC: {0} tiene un formato invalido en el nodo ", this.RFC);
                }
                return string.Empty;
            }
        }

        private bool RegexValido(string valor, string patron)
        {
            if (!(valor == null))
            {
                bool flag = Regex.IsMatch(valor, string.Concat("^", patron, "$"));
                return flag;
            }
            return false;
        }
    }
    //public partial class ComprobanteCfdiRelacionadosCfdiRelacionado : BasePropertyChangeImplementation, IDataErrorInfo
    //{
    //    private string folioField;
    //    private string folioFiscalField;
    //    private string rfcField;
    //    private string nombreField;
    //    private string serieField;
    //    private decimal totalField;
    //    private decimal importeAplicadoField;
    //    private string estadoSATField;
    //    /// <summary>
    //    /// constructor
    //    /// </summary>
    //    public ComprobanteCfdiRelacionadosCfdiRelacionado()
    //    {
    //    }
    //    /// <summary>
    //    /// Atributo requerido para registrar el folio fiscal (UUID) de un CFDI relacionado con el presente comprobante, por ejemplo: Si el CFDI relacionado es un comprobante de traslado que sirve 
    //    /// para registrar el movimiento de la mercancía. Si este comprobante se usa como nota de crédito o nota de débito del comprobante relacionado. Si este comprobante es una devolución sobre 
    //    /// el comprobante relacionado. Si éste sustituye a una factura cancelada.
    //    /// </summary>
    //    [JsonProperty("uuid")]
    //    public string UUID
    //    {
    //        get
    //        {
    //            return this.folioFiscalField;
    //        }
    //        set
    //        {
    //            if (!(value == null))
    //            {
    //                this.folioFiscalField = value.ToUpper();
    //            }
    //            else
    //            {
    //                this.folioFiscalField = value;
    //            }
    //            this.OnPropertyChanged();
    //        }
    //    }
    //    /// <summary>
    //    /// obtener o establecer el RFC del emisor o receptor del comprobante fiscal
    //    /// </summary>
    //    [JsonProperty("rfc")]
    //    public string RFC
    //    {
    //        get
    //        {
    //            return this.rfcField;
    //        }
    //        set
    //        {
    //            if (!(value == null))
    //            {
    //                this.rfcField = value.ToUpper();
    //            }
    //            else
    //            {
    //                this.rfcField = value;
    //            }
    //            this.OnPropertyChanged();
    //        }
    //    }
    //    /// <summary>
    //    /// obtener o establecer el nombre del emisor o receptor del comprobante fiscal
    //    /// </summary>
    //    [JsonProperty("nombre")]
    //    public string Nombre
    //    {
    //        get
    //        {
    //            return this.nombreField;
    //        }
    //        set
    //        {
    //            this.nombreField = value;
    //            this.OnPropertyChanged();
    //        }
    //    }
    //    /// <summary>
    //    /// obtener o establecer la serie de control interno del comprobante fiscal
    //    /// </summary>
    //    [JsonProperty("serie")]
    //    public string Serie
    //    {
    //        get
    //        {
    //            return this.serieField;
    //        }
    //        set
    //        {
    //            this.serieField = value;
    //            this.OnPropertyChanged();
    //        }
    //    }
    //    /// <summary>
    //    /// obtener o establecer el folio de control interno del comprobante fiscal
    //    /// </summary>
    //    [JsonProperty("folio")]
    //    public string Folio
    //    {
    //        get
    //        {
    //            return this.folioField;
    //        }
    //        set
    //        {
    //            this.folioField = value;
    //            this.OnPropertyChanged();
    //        }
    //    }
    //    /// <summary>
    //    /// obtener o establecer el total del comprobante fiscal
    //    /// </summary>
    //    [JsonProperty("total")]
    //    public decimal Total
    //    {
    //        get
    //        {
    //            return Math.Round(this.totalField, 2);
    //        }
    //        set
    //        {
    //            this.totalField = value;
    //            this.OnPropertyChanged();
    //        }
    //    }
    //    /// <summary>
    //    /// obtener o establecer el importe aplicado al comprobante
    //    /// </summary>
    //    [JsonProperty("importe")]
    //    public decimal ImporteAplciado
    //    {
    //        get
    //        {
    //            return this.importeAplicadoField;
    //        }
    //        set
    //        {
    //            this.importeAplicadoField = value;
    //            this.OnPropertyChanged();
    //        }
    //    }
    //    [JsonProperty("estadoSAT")]
    //    public string EstadoSAT
    //    {
    //        get
    //        {
    //            return this.estadoSATField;
    //        }
    //        set
    //        {
    //            this.estadoSATField = value;
    //            this.OnPropertyChanged();
    //        }
    //    }
    //    [JsonIgnore]
    //    public string Error
    //    {
    //        get
    //        {
    //            if (!this.RegexValido(this.RFC, "[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9A]")) 
    //            {
    //                return "Por favor ingrese datos válidos en esta fila!";
    //            }
    //            return string.Empty;
    //        }
    //    }
    //    public string this[string columnName]
    //    {
    //        get
    //        {
    //            if (columnName == "RFC" && !this.RegexValido(this.RFC, "[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9A]") || columnName == "UUID" && !this.RegexValido(this.UUID,"[a-f0-9A-F]{8}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{12}"))
    //            {
    //                return string.Format("El RFC: {0} tiene un formato invalido en el nodo ", this.RFC);
    //            }
    //            return string.Empty;
    //        }
    //    }
    //    private bool RegexValido(string valor, string patron)
    //    {
    //        if (!(valor == null))
    //        {
    //            bool flag = Regex.IsMatch(valor, string.Concat("^", patron, "$"));
    //            return flag;
    //        }
    //        return false;
    //    }
    //}
    //[JsonObject("tipoDeRelacion")]
    //public partial class ComprobanteCfdiRelacionadosTipoRelacion : BasePropertyChangeImplementation
    //{
    //    private string claveField;
    //    private string descripcionField;
    //    [JsonProperty("clave")]
    //    public string Clave
    //    {
    //        get
    //        {
    //            return this.claveField;
    //        }
    //        set
    //        {
    //            this.claveField = value;
    //            this.OnPropertyChanged();
    //        }
    //    }
    //    /// <summary>
    //    /// obtener o establacer 
    //    /// </summary>
    //    [JsonProperty("desc")]
    //    public string Descripcion
    //    {
    //        get
    //        {
    //            return this.descripcionField;
    //        }
    //        set
    //        {
    //            this.descripcionField = value;
    //            this.OnPropertyChanged();
    //        }
    //    }
    //}
}

namespace Jaeger.Edita.V2.CFDI.Entities
{
    /// <summary>
    /// Nodo opcional para precisar la información de los comprobantes relacionados.
    /// </summary>
    public partial class ComprobanteCfdiRelacionados : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private BindingList<ComprobanteCfdiRelacionadosCfdiRelacionado> cfdiRelacionadoField;
        private Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoDoctoRelacionadoRelacion tipoRelacionField; //c_TipoRelacion

        public ComprobanteCfdiRelacionados()
        {
            this.tipoRelacionField = new Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoDoctoRelacionadoRelacion();
            this.cfdiRelacionadoField = new BindingList<ComprobanteCfdiRelacionadosCfdiRelacionado>() { RaiseListChangedEvents = true };
            this.cfdiRelacionadoField.AddingNew += CfdiRelacionadoField_AddingNew;
            this.cfdiRelacionadoField.ListChanged += CfdiRelacionadoField_ListChanged;
        }

        /// <summary>
        /// Atributo requerido para indicar la clave de la relación que existe entre éste que se esta generando y el o los CFDI previos.
        /// </summary>
        [JsonProperty("tipoRelacion")]
        public Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoDoctoRelacionadoRelacion TipoRelacion
        {
            get
            {
                return this.tipoRelacionField;
            }
            set
            {
                this.tipoRelacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo requerido para precisar la información de los comprobantes relacionados.
        /// </summary>
        [JsonProperty("cfdiRelacionado")]
        public BindingList<ComprobanteCfdiRelacionadosCfdiRelacionado> CfdiRelacionado
        {
            get
            {
                return this.cfdiRelacionadoField;
            }
            set
            {
                if (this.cfdiRelacionadoField != null)
                {
                    this.cfdiRelacionadoField.AddingNew -= CfdiRelacionadoField_AddingNew;
                    this.cfdiRelacionadoField.ListChanged -= CfdiRelacionadoField_ListChanged;
                }
                this.cfdiRelacionadoField = value;
                if (this.cfdiRelacionadoField != null)
                {
                    this.cfdiRelacionadoField.AddingNew += CfdiRelacionadoField_AddingNew;
                    this.cfdiRelacionadoField.ListChanged += CfdiRelacionadoField_ListChanged;
                }
                this.OnPropertyChanged();
            }
        }

        #region metodos

        private void CfdiRelacionadoField_ListChanged(object sender, ListChangedEventArgs e)
        {
        }

        private void CfdiRelacionadoField_AddingNew(object sender, AddingNewEventArgs e)
        {
        }

        public string Json()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static ComprobanteCfdiRelacionados Json(string jsonIn)
        {
            try {
                return JsonConvert.DeserializeObject<ComprobanteCfdiRelacionados>(jsonIn);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        #endregion
    }
}
