using System;
using System.ComponentModel;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Enums;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.CFDI.Entities
{
    [JsonObject]
    public partial class ComprobanteConceptoImpuestos : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private BindingList<ComprobanteConceptoImpuesto> conceptosImpuestosField;

        /// <summary>
        /// constructor
        /// </summary>
        public ComprobanteConceptoImpuestos()
        {
            this.conceptosImpuestosField = new BindingList<ComprobanteConceptoImpuesto>();
            this.OnPropertyChanged();
        }

        #region propiedades

        [JsonProperty()]
        public BindingList<ComprobanteConceptoImpuesto> ConceptosImpuestos
        {
            get
            {
                return this.conceptosImpuestosField;
            }
            set
            {
                this.conceptosImpuestosField = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region metodos

        public string Json(Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static ComprobanteConceptoImpuestos Json(string inputJson)
        {
            try
            {
                return JsonConvert.DeserializeObject<ComprobanteConceptoImpuestos>(inputJson);
            }
            catch
            {
                return null;
            }
        }
        #endregion
    }

    public class ComprobanteConceptoImpuesto : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private EnumTipoImpuesto tipoField;
        private decimal baseField;
        private EnumImpuesto impuestoField;
        private EnumFactor tipoFactorField;
        private decimal tasaOCuotaField;
        private decimal importeField;

        public ComprobanteConceptoImpuesto()
        {
            this.tipoField = EnumTipoImpuesto.Traslado;
            this.impuestoField = EnumImpuesto.IVA;
        }

        [JsonProperty("tipo")]
        public EnumTipoImpuesto Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("base")]
        public decimal Base
        {
            get
            {
                return this.baseField;
            }
            set
            {
                this.baseField = value;
                this.CalcularImporte();
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("impuesto")]
        public EnumImpuesto Impuesto
        {
            get
            {
                return this.impuestoField;
            }
            set
            {
                this.impuestoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("tipoFactor")]
        public EnumFactor TipoFactor
        {
            get
            {
                return this.tipoFactorField;
            }
            set
            {
                this.tipoFactorField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("tasaOCuota")]
        public decimal TasaOCuota
        {
            get
            {
                return this.tasaOCuotaField;
            }
            set
            {
                this.tasaOCuotaField = value;
                this.CalcularImporte();
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("importe")]
        public decimal Importe
        {
            get
            {
                return Math.Round(this.importeField, 4);
            }
            set
            {
                this.importeField = value;
                this.OnPropertyChanged();
            }
        }

        public void CalcularImporte()
        {
            this.Importe = this.baseField * this.tasaOCuotaField;
        }
    }
}