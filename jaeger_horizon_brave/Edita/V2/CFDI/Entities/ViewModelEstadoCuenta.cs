﻿using System;
using System.Linq;

namespace Jaeger.Edita.V2.CFDI.Entities
{
    public class ViewModelEstadoCuenta
    {
        public string RFC { get; set; }
        public string Tipo { get; set; }
        public string Receptor { get; set; }
        public string Status { get; set; }
        public string IdDocumento { get; set; }
        public string Folio { get; set; }
        public string Serie { get; set; }
        public DateTime FechaEmision { get; set; }
        public DateTime? FechaCobro { get; set; }
        public decimal Total { get; set; }
        public decimal Descuento { get; set; }
        public decimal Acumulado { get; set; }

        public decimal Saldo
        {
            get
            {
                return this.Total - this.Acumulado;
            }
        }

        public int DiasCobranza
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if(this.FechaCobro>= firstGoodDate)
                    return ((TimeSpan)(this.FechaCobro - this.FechaEmision)).Days;
                return ((DateTime.Now - this.FechaEmision)).Days; 
            }
        }
    }
}
