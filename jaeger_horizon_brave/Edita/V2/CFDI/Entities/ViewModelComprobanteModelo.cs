﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jaeger.Edita.V2.Almacen.Entities;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.CFDI.Entities
{
    /// <summary>
    /// clase del producto o servicios del catalogo de conceptos de CFDI
    /// </summary>
    [SugarTable("_ctlmdl", "catalogo de modelos de productos")]
    public class ViewModelComprobanteModelo : ViewModelModelo
    {
        
    }
}
