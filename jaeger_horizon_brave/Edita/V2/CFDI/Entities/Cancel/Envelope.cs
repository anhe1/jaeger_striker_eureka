using System;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace Jaeger.Edita.V2.CFDI.Entities.Cancel
{
	[XmlRoot(ElementName="Envelope", Namespace="http://schemas.xmlsoap.org/soap/envelope/")]
	public class Envelope
	{
        private Jaeger.Edita.V2.CFDI.Entities.Cancel.Body mBody;

		private string mS;

        [XmlElement(ElementName="Body", Namespace="http://schemas.xmlsoap.org/soap/envelope/")]
        public Jaeger.Edita.V2.CFDI.Entities.Cancel.Body Body
        {
			get
			{
				return this.mBody;
			}
			set
			{
				this.mBody = value;
			}
		}

		[XmlAttribute(AttributeName="s", Namespace="http://www.w3.org/2000/xmlns/")]
		public string S
		{
			get
			{
				return this.mS;
			}
			set
			{
				this.mS = value;
			}
		}

		[DebuggerNonUserCode]
		public Envelope()
		{
		}

		public Envelope Load(FileInfo fileName)
		{
			Envelope load;
			if (fileName.Exists)
			{
                string stringXml = Jaeger.Util.Helpers.HelperFiles.ReadFileText(fileName);
                Envelope objEnvelope = new Envelope();
				try
                {
                    objEnvelope = Jaeger.Helpers.HelperSerializer.DeserializeObject<Envelope>(stringXml);
                }
				catch (Exception ex)
				{
                    Console.WriteLine(ex.Message);
				}
				load = objEnvelope;
			}
			else
			{
				load = null;
			}
			return load;
		}

		public Envelope LoadXml(string stringXml)
		{
			Envelope loadXml;
			try
			{
                loadXml = Jaeger.Helpers.HelperXmlConvert.DeserializeObject<Envelope>(stringXml);
                return loadXml;
			}
			catch (Exception ex)
			{
                Console.WriteLine(ex.Message);
			}
			loadXml = null;
			return loadXml;
		}

        public string Xml()
        {
            return Jaeger.Helpers.HelperXmlConvert.SerializeObject<Envelope>(this);
        }
	}
}