using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Jaeger.Edita.V2.CFDI.Entities.Cancel
{
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("xsd", "4.0.30319.33440")]
	[Serializable]
	[XmlRoot(Namespace="http://cancelacfd.sat.gob.mx", IsNullable=false)]
	[XmlType(AnonymousType=true, Namespace="http://cancelacfd.sat.gob.mx")]
	public class CancelaCFD
	{
        private Jaeger.Edita.V2.CFDI.Entities.Cancel.Cancelacion cancelacionField;

		/// <comentarios />
        public Jaeger.Edita.V2.CFDI.Entities.Cancel.Cancelacion Cancelacion
        {
			get
			{
				return this.cancelacionField;
			}
			set
			{
				this.cancelacionField = value;
			}
		}

		[DebuggerNonUserCode]
		public CancelaCFD()
		{
		}
	}
}