﻿using System;
using Jaeger.Edita.V2.CFDI.Entities.Cancel;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;

namespace Jaeger.Edita.V2.CFDI.Entities
{
    [SugarTable("_cfdi")]
    public class ViewModelAccuseCancelacion : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        string folioField;
        string serieField;
        string tipoComprobanteField;
        string folioFiscalField;
        string estadoField;
        string rfcEmisorField;
        string emisorNombreField;
        string rfcReceptorField;
        DateTime fechaSolicitudField;
        DateTime fechaCancelacionField;
        DateTime fechaEmisionField;
        string receptorNombreField;
        string selloDigitalSatField;
        string noCertificadoEmisorField;
        string noCertificadoField;

        public ViewModelAccuseCancelacion()
        {
        }

        [DataNames("_cfdi_folio")]
        [SugarColumn(ColumnName = "_cfdi_folio")]
        public string Folio
        {
            get
            {
                return this.folioField;
            }
            set
            {
                this.folioField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_serie")]
        [SugarColumn(ColumnName = "_cfdi_serie")]
        public string Serie
        {
            get
            {
                return this.serieField;
            }
            set
            {
                this.serieField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_efecto")]
        [SugarColumn(ColumnName = "_cfdi_efecto")]
        public string TipoComprobante
        {
            get
            {
                return this.tipoComprobanteField;
            }
            set
            {
                this.tipoComprobanteField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_uuid")]
        [SugarColumn(ColumnName = "_cfdi_uuid")]
        public string FolioFiscal
        {
            get
            {
                return this.folioFiscalField;
            }
            set
            {
                this.folioFiscalField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_estado")]
        [SugarColumn(ColumnName = "_cfdi_estado")]
        public string Estado
        {
            get
            {
                return this.estadoField;
            }
            set
            {
                this.estadoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_rfce")]
        [SugarColumn(ColumnName = "_cfdi_rfce")]
        public string RfcEmisor
        {
            get
            {
                return this.rfcEmisorField;
            }
            set
            {
                this.rfcEmisorField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_nome")]
        [SugarColumn(ColumnName = "_cfdi_nome")]
        public string EmisorNombre
        {
            get
            {
                return this.emisorNombreField;
            }
            set
            {
                this.emisorNombreField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_rfcr")]
        [SugarColumn(ColumnName = "_cfdi_rfcr")]
        public string RfcReceptor
        {
            get
            {
                return this.rfcReceptorField;
            }
            set
            {
                this.rfcReceptorField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_nomr")]
        [SugarColumn(ColumnName = "_cfdi_nomr")]
        public string ReceptorNombre
        {
            get
            {
                return this.receptorNombreField;
            }
            set
            {
                this.receptorNombreField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public DateTime FechaSolicitud
        {
            get
            {
                return this.fechaSolicitudField;
            }
            set
            {
                this.fechaSolicitudField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public DateTime FechaCancelacion
        {
            get
            {
                return this.fechaCancelacionField;
            }
            set
            {
                this.fechaCancelacionField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_fecems")]
        [SugarColumn(ColumnName = "_cfdi_fecems")]
        public DateTime FechaEmision
        {
            get
            {
                return this.fechaEmisionField;
            }
            set
            {
                this.fechaEmisionField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string SelloDigital
        {
            get
            {
                return this.selloDigitalSatField;
            }
            set
            {
                this.selloDigitalSatField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_nocert")]
        [SugarColumn(ColumnName = "_cfdi_nocert")]
        public string NoCertificadoEmisor
        {
            get
            {
                return this.noCertificadoEmisorField;
            }
            set
            {
                this.noCertificadoEmisorField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string NoCertificado
        {
            get
            {
                return this.noCertificadoField;
            }
            set
            {
                this.noCertificadoField = value;
                this.OnPropertyChanged();
            }
        }

        private CancelaCFDResponse accuse;
        private string accuseXML;

        [DataNames("_cfdi_acuse")]
        public string AccuseXML
        {
            get
            {
                return this.accuseXML;
            }
            set
            {
                this.accuseXML = value;
                this.Accuse = CancelaCFDResponse.LoadXml(value);
                this.OnPropertyChanged();
            }
        }

        public CancelaCFDResponse Accuse
        {
            get
            {
                return this.accuse;
            }
            set
            {
                this.accuse = value;
                this.OnPropertyChanged();
            }
        }
    }
}
