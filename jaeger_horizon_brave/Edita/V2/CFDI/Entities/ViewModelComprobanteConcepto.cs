﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Jaeger.Edita.V2.CFDI.Enums;
using Newtonsoft.Json;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Enums;

namespace Jaeger.Edita.V2.CFDI.Entities {
    [SugarTable("_cfdcnp")]
    public class ViewModelComprobanteConcepto : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation, IDataErrorInfo
    {
        private int idField;
        private int subIdField;
        private bool isVisibleField;   
        private int numPedidoField;
        private string claveProdServField;
        private string noIdentificacionField;
        private decimal cantidadField;
        private string claveUnidadField;
        private string unidadField;
        private string descripcionField;
        private decimal valorUnitarioField;
        private decimal importeField;
        private decimal subTotalField;
        private decimal descuentoField;
        private string ctaPredialField;
        private string _claveObjetoImpuesto;

        // impuestos del concepto
        private decimal trasladoIvaField;
        private decimal trasladoiepsField;
        private decimal retencionIvaField;
        private decimal retencionIsrField;

        private DateTime? fechaNuevoField;
        private DateTime? fechaModificaField;
        private string creoField;
        private string modField;

        private BindingList<ConceptoParte> parteField;
        private BindingList<ComprobanteConceptoImpuesto> impuestosField;
        private BindingList<ComprobanteInformacionAduanera> informacionAduaneraField;
        private ComprobanteConceptoACuentaTerceros _ACuentaTerceros;
        private int presionDecimalField;

        public ViewModelComprobanteConcepto()
        {
            this.impuestosField = new BindingList<ComprobanteConceptoImpuesto>() { RaiseListChangedEvents = true };
            this.impuestosField.ListChanged += Impuestos_ListChanged;
            this.impuestosField.AddingNew += Impuestos_AddingNew;
            this.parteField = new BindingList<ConceptoParte>() { RaiseListChangedEvents = true };
            this.parteField.ListChanged += Parte_ListChanged;
            this.informacionAduaneraField = new BindingList<ComprobanteInformacionAduanera>() { RaiseListChangedEvents = true };
            this.isVisibleField = true;
        }

        #region propiedades

        [JsonIgnore]
        [DataNames("_cfdcnp_id")]
        [SugarColumn(ColumnName = "_cfdcnp_id", IsIdentity = true, IsPrimaryKey = true)]
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cfdcnp_cfds_id")]
        [SugarColumn(ColumnName = "_cfdcnp_cfds_id")]
        public int SubId
        {
            get
            {
                return this.subIdField;
            }
            set
            {
                this.subIdField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cfdcnp_a")]
        [SugarColumn(ColumnName = "_cfdcnp_a")]
        public bool IsActive
        {
            get
            {
                return this.isVisibleField;
            }
            set
            {
                this.isVisibleField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// asignar numero de pedido 
        /// </summary>
        [JsonIgnore]
        [DataNames("_cfdcnp_pdd_id")]
        [SugarColumn(ColumnName = "_cfdcnp_pdd_id")]
        public int NumPedido
        {
            get
            {
                return this.numPedidoField;
            }
            set
            {
                this.numPedidoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del producto o del servicio amparado por la presente parte. Es requerido y deben utilizar las claves del catálogo de productos y servicios, 
        /// cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.
        /// </summary>
        [Description("Atributo requerido para expresar la clave del producto o del servicio amparado por la presente parte. Es requerido y deben utilizar las claves del catálogo de productos y servicios, cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.")]
        [DisplayName("Clave de Producto / Servicio")]
        [DataNames("_cfdcnp_clvprds")]
        [SugarColumn(ColumnName = "_cfdcnp_clvprds")]
        public string ClaveProdServ
        {
            get
            {
                return this.claveProdServField;
            }
            set
            {
                if (!(value == null))
                {
                    this.claveProdServField = value;
                }
                else
                {
                    this.claveProdServField = Regex.Replace(value.ToString(), "[^\\d]", "");
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número de serie, número de parte del bien o identificador del producto o del servicio amparado por la presente parte. Opcionalmente se puede utilizar claves 
        /// del estándar GTIN.
        /// pattern value="[^|]{1,100}", Longitud: 100
        /// </summary>
        [DisplayName("No. de Identificación")]
        [DataNames("_cfdcnp_noidnt")]
        [SugarColumn(ColumnName = "_cfdcnp_noidnt")]
        public string NoIdentificacion
        {
            get
            {
                return this.noIdentificacionField;
            }
            set
            {
                this.noIdentificacionField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Cantidad")]
        [DataNames("_cfdcnp_cntdd")]
        [SugarColumn(ColumnName = "_cfdcnp_cntdd")]
        public decimal Cantidad
        {
            get
            {
                return this.cantidadField;
            }
            set
            {
                this.cantidadField = value;
                this.ValorUnitario = this.valorUnitarioField;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Clave Unidad")]
        [DataNames("_cfdcnp_clvund")]
        [SugarColumn(ColumnName = "_cfdcnp_clvund")]
        public string ClaveUnidad
        {
            get
            {
                return this.claveUnidadField;
            }
            set
            {
                this.claveUnidadField = (value != null ? value.ToUpper().Trim() : value);
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Unidad")]
        [DataNames("_cfdcnp_undd")]
        [SugarColumn(ColumnName = "_cfdcnp_undd")]
        public string Unidad
        {
            get
            {
                return this.unidadField;
            }
            set
            {
                this.unidadField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Descripción")]
        [DataNames("_cfdcnp_cncpt")]
        [SugarColumn(ColumnName = "_cfdcnp_cncpt")]
        public string Descripcion
        {
            get
            {
                return this.descripcionField;
            }
            set
            {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Unitario")]
        [DataNames("_cfdcnp_untr")]
        [SugarColumn(ColumnName = "_cfdcnp_untr")]
        public decimal ValorUnitario
        {
            get
            {
                return this.valorUnitarioField;
            }
            set
            {
                this.valorUnitarioField = value;
                this.Importe = (this.cantidadField * this.valorUnitarioField);
                this.SubTotal = (this.Importe - this.Descuento);
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Importe")]
        [DataNames("_cfdcnp_sbttl")]
        [SugarColumn(ColumnName = "_cfdcnp_sbttl")]
        public decimal Importe
        {
            get
            {
                return this.importeField;
            }
            set
            {
                this.importeField = value;
                foreach (ComprobanteConceptoImpuesto item in this.Impuestos)
                {
                    item.Base = value;
                }
                this.OnPropertyChanged();
            }
        }

        [DisplayName("SubTotal")]
        [SugarColumn(IsIgnore = true)]
        public decimal SubTotal
        {
            get
            {
                return this.subTotalField;
            }
            set
            {
                this.subTotalField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Descuento")]
        [DataNames("_cfdcnp_dscnt")]
        [SugarColumn(ColumnName = "_cfdcnp_dscnt")]
        public decimal Descuento
        {
            get
            {
                return this.descuentoField;
            }
            set
            {
                this.descuentoField = value;
                this.ValorUnitario = this.valorUnitarioField;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si la operación comercial es objeto o no de impuesto.
        /// </summary>
        [DisplayName("Obj. Impuesto")]
        [DataNames("_cfdcnp_clvobj")]
        [SugarColumn(ColumnName = "_cfdcnp_clvobj", ColumnDescription = "establecer si la operacion es objeto o no de impuesto", Length = 2)]
        public string ObjetoImp {
            get { return this._claveObjetoImpuesto; }
            set {
                this._claveObjetoImpuesto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número de la cuenta predial del inmueble cubierto por el presente concepto, o bien para incorporar los datos de identificación del certificado 
        /// de participación inmobiliaria no amortizable, tratándose de arrendamiento.
        /// pattern value="[0-9]{1,150}", Longitud: 150
        /// </summary>
        [DisplayName("Cuenta Predial")]
        [DataNames("_cfdcnp_ctapre")]
        [SugarColumn(ColumnName = "_cfdcnp_ctapre")]
        public string CtaPredial
        {
            get
            {
                return this.ctaPredialField;
            }
            set
            {
                this.ctaPredialField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public BindingList<ComprobanteInformacionAduanera> InformacionAduanera
        {
            get
            {
                return this.informacionAduaneraField;
            }
            set
            {
                this.informacionAduaneraField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public decimal TrasladoIva
        {
            get
            {
                return this.trasladoIvaField;
            }
            set
            {
                this.trasladoIvaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public decimal Trasladoieps
        {
            get
            {
                return this.trasladoiepsField;
            }
            set
            {
                this.trasladoiepsField = value;
                this.OnPropertyChanged();
            }
        }
        
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public decimal RetencionIva
        {
            get
            {
                return this.retencionIvaField;
            }
            set
            {
                this.retencionIvaField = value;
                this.OnPropertyChanged();
            }
        }
        
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public decimal RetencionIsr
        {
            get
            {
                return this.retencionIsrField;
            }
            set
            {
                this.retencionIsrField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer lista de parte del conepto
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public BindingList<ConceptoParte> Parte
        {
            get
            {
                return this.parteField;
            }
            set
            {
                if (this.parteField != null)
                {
                    this.parteField.ListChanged += new ListChangedEventHandler(Parte_ListChanged);
                }
                this.parteField = value;
                if (this.parteField != null)
                {
                    this.parteField.ListChanged += new ListChangedEventHandler(Parte_ListChanged);
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer lista de impuestos aplicables al concepto.
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public BindingList<ComprobanteConceptoImpuesto> Impuestos
        {
            get
            {
                return this.impuestosField;
            }
            set
            {
                if (this.impuestosField != null)
                {
                    this.impuestosField.ListChanged -= new ListChangedEventHandler(Impuestos_ListChanged);
                    this.impuestosField.AddingNew -= new AddingNewEventHandler(Impuestos_AddingNew);
                }
                this.impuestosField = value;
                if (this.impuestosField != null)
                {
                    this.impuestosField.ListChanged += new ListChangedEventHandler(Impuestos_ListChanged);
                    this.impuestosField.AddingNew += new AddingNewEventHandler(Impuestos_AddingNew);
                }
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cfdcnp_usr_n")]
        [SugarColumn(ColumnName = "_cfdcnp_usr_n")]
        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cfdcnp_usr_m")]
        [SugarColumn(ColumnName = "_cfdcnp_usr_m")]
        public string Modifica
        {
            get
            {
                return this.modField;
            }
            set
            {
                this.modField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cfdcnp_fn")]
        [SugarColumn(ColumnName = "_cfdcnp_fn")]
        public DateTime? FechaNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cfdcnp_fm")]
        [SugarColumn(ColumnName = "_cfdcnp_fm")]
        public DateTime? FechaModifica
        {
            get
            {
                return this.fechaModificaField;
            }
            set
            {
                this.fechaModificaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public int PresionDecimal
        {
            get
            {
                return this.presionDecimalField;
            }
            set
            {
                this.presionDecimalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtiene objeto json o establece el valor del objeto de impuestos
        /// </summary>
        [JsonIgnore]
        [DataNames("_cfdcnp_impto")]
        [SugarColumn(ColumnName = "_cfdcnp_impto")]
        public string JImpuestos
        {
            get
            {
                return ViewModelComprobanteConcepto.JsonImpuestos(this.Impuestos);
            }
            set
            {
                this.Impuestos = ViewModelComprobanteConcepto.JsonImpuestos(value);
            }
        }

        /// <summary>
        /// obtener o establecer objeto json que representa las partes del concepto
        /// </summary>
        [JsonIgnore]
        [DataNames("_cfdcnp_parte")]
        [SugarColumn(ColumnName = "_cfdcnp_parte")]
        public string JParte
        {
            get
            {
                return ViewModelComprobanteConcepto.JsonParte(this.Parte);
            }
            set
            {
                this.Parte = ViewModelComprobanteConcepto.JsonParte(value);
            }
        }

        [DataNames("_cfdcnp_atrcr")]
        [SugarColumn(ColumnName = "_cfdcnp_atrcr", ColumnDataType = "Text")]
        public string JAcuentaTercera {
            get {
                if (this.ACuentaTerceros != null)
                    return this.ACuentaTerceros.Json();
                return null;
            }
            set {
                if (value != null) {
                    this.ACuentaTerceros = ComprobanteConceptoACuentaTerceros.Json(value);
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public ComprobanteConceptoACuentaTerceros ACuentaTerceros {
            get {
                if (this._ACuentaTerceros != null)
                    return this._ACuentaTerceros;
                return null;
            }
            set {
                if (value != null) {
                    this._ACuentaTerceros = value;
                }
                this.OnPropertyChanged();
            }
        }

        #endregion

        private void Parte_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (this.parteField.Count > 0)
            {
                this.ValorUnitario = this.parteField.Sum((p) => p.ValorUnitario);
            }
        }

        private void Impuestos_AddingNew(object sender, AddingNewEventArgs e)
        {
            e.NewObject = new ComprobanteConceptoImpuesto() { Base = this.Importe };
        }

        private void Impuestos_ListChanged(object sender, ListChangedEventArgs e)
        {
            this.TrasladoIva = (from p in this.impuestosField
                                where p.Tipo == EnumTipoImpuesto.Traslado & p.Impuesto == EnumImpuesto.IVA
                                select p).Sum<ComprobanteConceptoImpuesto>((ComprobanteConceptoImpuesto s) => s.Importe);

            this.Trasladoieps = (from p in this.impuestosField
                                 where p.Tipo == EnumTipoImpuesto.Traslado & p.Impuesto == EnumImpuesto.IEPS
                                 select p).Sum<ComprobanteConceptoImpuesto>((ComprobanteConceptoImpuesto s) => s.Importe);

            this.RetencionIva = (from p in this.impuestosField
                                 where p.Tipo == EnumTipoImpuesto.Retencion & p.Impuesto == EnumImpuesto.IVA
                                 select p).Sum<ComprobanteConceptoImpuesto>((ComprobanteConceptoImpuesto s) => s.Importe);

            this.RetencionIsr = (from p in this.impuestosField
                                 where p.Tipo == EnumTipoImpuesto.Retencion & p.Impuesto == EnumImpuesto.ISR
                                 select p).Sum<ComprobanteConceptoImpuesto>((ComprobanteConceptoImpuesto s) => s.Importe);
        }

        /// <summary>
        /// obtener objeto json en formato string de la lista de la lista de ConceptoParte del Concepto
        /// </summary>
        public static string JsonParte(BindingList<ConceptoParte> objeto)
        {
            return JsonConvert.SerializeObject(objeto);
        }

        /// <summary>
        /// obtener lista del objeto ConceptoParte para el concepto del comprobante
        /// </summary>
        public static BindingList<ConceptoParte> JsonParte(string jsonIn)
        {
            if (jsonIn != "")
            {
                return new BindingList<ConceptoParte>(JsonConvert.DeserializeObject<List<ConceptoParte>>(jsonIn));
            }
            else
            {
                return new BindingList<ConceptoParte>();
            }
        }

        /// <summary>
        /// obtener objeto json en formato string de la lista de impuestos aplicables al concepto del comprobante
        /// </summary>
        public static string JsonImpuestos(BindingList<ComprobanteConceptoImpuesto> objetos)
        {
            return JsonConvert.SerializeObject(objetos);
        }

        /// <summary>
        /// obtener lista de objetos ConceptoImpuesto para el conceto del comprobante
        /// </summary>
        public static BindingList<ComprobanteConceptoImpuesto> JsonImpuestos(string jsonIn)
        {
            if (!(string.IsNullOrEmpty(jsonIn)))
            {
                try
                {
                    return new BindingList<ComprobanteConceptoImpuesto>(JsonConvert.DeserializeObject<BindingList<ComprobanteConceptoImpuesto>>(jsonIn));
                }
                catch (JsonException ex)
                {
                    Console.WriteLine(ex.Message);
                    return null;
                }
            }
            else
            {
                return new BindingList<ComprobanteConceptoImpuesto>();
            }
        }

        [JsonIgnore]
        [Browsable(false)]
        [SugarColumn(IsIgnore = true)]
        public string Error
        {
            get
            {
                if (!this.RegexValido(this.ClaveProdServ, "[0-9]{8}") || !this.RegexValido(this.ClaveUnidad, "[0-9,A-Z]{3}|[0-9,A-Z]{2}") || !this.RegexValido(this.Cantidad.ToString(), "[0-9]{1,18}(.[0-9]{1,2})?") || !(this.Descripcion != null ? !(this.Descripcion.Trim() == "") : false))
                {
                    return "Por favor ingrese datos válidos en esta fila!";
                }
                return string.Empty;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string this[string columnName]
        {
            get
            {
                if (columnName == "ClaveUnidad" && !this.RegexValido(this.ClaveUnidad, "[0-9,A-Z]{3}|[0-9,A-Z]{2}"))
                {
                    return "Formato de clave unidad no valida!";
                }

                if (columnName == "ClaveProdServ" && !this.RegexValido(this.ClaveProdServ, "[0-9]{8}"))
                {
                    return "Formato de clave de producto ó servicio no válida!";
                }

                if (columnName == "Descripcion" && !(this.Descripcion != null))
                {
                    return "Debes ingresar un concepto.";
                }

                if (columnName == "Descripcion" && !(this.Descripcion != null ? !(this.Descripcion.Trim() == "") : false))
                {
                    return "Debe ingresar un concepto.";
                }

                return string.Empty;
            }
        }

        private bool RegexValido(string valor, string patron)
        {
            if (!(valor == null))
            {
                bool flag = Regex.IsMatch(valor, string.Concat("^", patron, "$"));
                return flag;
            }
            return false;
        }
    }
}
