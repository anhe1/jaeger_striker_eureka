namespace Jaeger.CFDI.Entities
{
    public enum TuristaPasajeroExtranjeroDatosTransitoVia
    {
        /// <remarks/>
        Aérea,
    
        /// <remarks/>
        Marítima,
    
        /// <remarks/>
        Terrestre,
    }
}