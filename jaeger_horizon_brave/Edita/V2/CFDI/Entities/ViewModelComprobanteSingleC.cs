﻿using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Edita.V2.CFDI.Entities.Cancel;
using Jaeger.CFDI.Entities.Addenda;
using Jaeger.Edita.V2.Validador.Entities;

namespace Jaeger.Edita.V2.CFDI.Entities {
    /// <summary>
    /// clase de vista de comprobante fiscal simple que incluye los objetos de los complementos
    /// </summary>
    public class ViewModelComprobanteSingleC : ViewModelComprobanteSingle {
        public ViewModelComprobanteSingleC() {

        }

        [DataNames("_cfdi_compl")]
        [SugarColumn(ColumnName = "_cfdi_compl")]
        public string JComplementos {
            get {
                if (this.complementosField != null)
                    return this.complementosField.Json();
                return "";
            }
            set {
                this.complementosField = Complementos.Json(value);
                this.OnPropertyChanged();
            }
        }

        private Complementos complementosField;
        /// <summary>
        /// complementos del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public Complementos Complementos {
            get {
                return this.complementosField;
            }
            set {
                this.complementosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la información de los comprobantes relacionados.
        /// </summary>
        [DataNames("_cfdi_comrel")]
        [SugarColumn(ColumnName = "_cfdi_comrel")]
        public string JCfdiRelacionados {
            get {
                if (this.cfdiRelacionadosField != null)
                    return this.cfdiRelacionadosField.Json();
                return "";
            }
            set {
                if (this.cfdiRelacionadosField == null)
                    this.cfdiRelacionadosField = new ComprobanteCfdiRelacionados();
                this.cfdiRelacionadosField = ComprobanteCfdiRelacionados.Json(value);
            }
        }

        private ComprobanteCfdiRelacionados cfdiRelacionadosField;
        [SugarColumn(IsIgnore = true)]
        public ComprobanteCfdiRelacionados CfdiRelacionados {
            get {
                return cfdiRelacionadosField;
            }
            set {
                this.cfdiRelacionadosField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_nomina")]
        [SugarColumn(ColumnName = "_cfdi_nomina", IsNullable = true)]
        public string JNomina {
            get {
                if (this.nominaField != null)
                    return this.nominaField.Json();
                return null;
            }
            set {
                this.nominaField = ComplementoNomina.Json(value);
                this.OnPropertyChanged();
            }
        }

        private ComplementoNomina nominaField;
        [SugarColumn(IsIgnore = true)]
        public ComplementoNomina Nomina {
            get {
                return nominaField;
            }
            set {
                this.nominaField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_val")]
        [SugarColumn(ColumnName = "_cfdi_val")]
        public string JValidacion {
            get {
                if (this.Validacion != null)
                    return this.Validacion.Json();
                return null;
            }
            set {
                this.Validacion = ValidateResponse.Json(value);
                this.OnPropertyChanged();
            }
        }

        private ValidateResponse validacionField;
        /// <summary>
        /// respuesta de validación del comprobante fiscal
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ValidateResponse Validacion {
            get {
                return this.validacionField;
            }
            set {
                this.validacionField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_pagos")]
        [SugarColumn(ColumnName = "_cfdi_pagos")]
        public string JComplementoPagos {
            get {
                if (this.pagosField != null)
                    return this.pagosField.Json();
                return null;
            }
            set {
                this.pagosField = ComplementoPagos.Json(value);
                this.OnPropertyChanged();
            }
        }

        private string nota;
        [DataNames("_cfdi_obsrv")]
        [SugarColumn(ColumnName = "_cfdi_obsrv", Length = 255)]
        public string Nota {
            get {
                return nota;
            }
            set {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }

        private ComplementoPagos pagosField;
        /// <summary>
        /// obtener o establecer objeto del complemento de pagos
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ComplementoPagos ComplementoPagos {
            get {
                return this.pagosField;
            }
            set {
                this.pagosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer formato xml del acuse de cancelacion
        /// </summary>
        [DataNames("_cfdi_acuse")]
        [SugarColumn(ColumnName = "_cfdi_acuse")]
        public string JAccuse {
            get {
                if (this.accuseField != null)
                    return this.accuseField.Xml();
                return null;
            }
            set {
                this.accuseField = CancelaCFDResponse.LoadXml(value);
            }
        }

        private CancelaCFDResponse accuseField;
        /// <summary>
        /// acuse de cancelacion del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CancelaCFDResponse Accuse {
            get {
                return this.accuseField;
            }
            set {
                this.accuseField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_adenda")]
        [SugarColumn(ColumnName = "_cfdi_adenda")]
        public string JAddenda {
            get {
                if (this.addendasField != null)
                    return this.addendasField.Json();
                return null;
            }
            set {
                this.addendasField = Addendas.Json(value);
                this.OnPropertyChanged();
            }
        }

        private Addendas addendasField;
        /// <summary>
        /// obtiene ó establece objeto de addenda
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public Addendas Addendas {
            get {
                if (this.addendasField != null)
                    return this.addendasField;
                return null;
            }
            set {
                this.addendasField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
