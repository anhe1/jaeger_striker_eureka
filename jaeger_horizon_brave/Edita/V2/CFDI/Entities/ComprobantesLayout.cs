﻿/// develop: 130420180026
/// purpose: definicion del layout para carga masiva de comprobantes
using System;
using System.Linq;
using System.ComponentModel;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2.CFDI.Enums;

namespace Jaeger.CFDI.Entities
{
    public class ComprobantesLayout : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private EnumCfdiType tipoComprobanteField;
        private string serieField;
        private string folioField;
        private string rfcReceptorField;
        private string receptorField;
        private string claveUsoCfdiField;
        private string claveRegimenFiscalField;
        private string numRegTribField;
        private string claveMonedaField;
        private string claveMetodoPagoField;
        private string claveFormaPagoField;
        private string condicionesField;
        private decimal tipoCambioField;
        private BindingList<ViewModelComprobanteConcepto> itemsField;

        public EnumCfdiType TipoComprobante
        {
            get
            {
                return this.tipoComprobanteField;
            }
            set
            {
                this.tipoComprobanteField = value;
                this.OnPropertyChanged();
            }
        }

        public string TipoComprobanteText
        {
            get
            {
                return Enum.GetName(typeof(EnumCfdiType), this.tipoComprobanteField);
            }
            set
            {
                this.TipoComprobante = (EnumCfdiType)Enum.Parse(typeof(EnumCfdiType), value[0].ToString().ToUpper() + value.Substring(2));
            }
        }

        public string Serie
        {
            get
            {
                return this.serieField;
            }
            set
            {
                this.serieField = value;
                this.OnPropertyChanged();
            }
        }

        public string Folio
        {
            get
            {
                return this.folioField;
            }
            set
            {
                this.folioField = value;
                this.OnPropertyChanged();
            }
        }

        public string RfcReceptor
        {
            get
            {
                return this.rfcReceptorField;
            }
            set
            {
                this.rfcReceptorField = value;
                this.OnPropertyChanged();
            }
        }

        public string Receptor
        {
            get
            {
                return this.receptorField;
            }
            set
            {
                this.receptorField = value;
                this.OnPropertyChanged();
            }
        }

        public string ClaveUsoCfdi
        {
            get
            {
                return this.claveUsoCfdiField;
            }
            set
            {
                this.claveUsoCfdiField = value;
                this.OnPropertyChanged();
            }
        }

        public string ClaveRegimenFiscal
        {
            get
            {
                return this.claveRegimenFiscalField;
            }
            set
            {
                this.claveRegimenFiscalField = value;
                this.OnPropertyChanged();
            }
        }

        public string NumRegTrib
        {
            get
            {
                return this.numRegTribField;
            }
            set
            {
                this.numRegTribField = value;
                this.OnPropertyChanged();
            }
        }

        public string ClaveMoneda
        {
            get
            {
                return this.claveMonedaField;
            }
            set
            {
                this.claveMonedaField = value;
                this.OnPropertyChanged();
            }
        }

        public string ClaveMetodoPago
        {
            get
            {
                return this.claveMetodoPagoField;
            }
            set
            {
                this.claveMetodoPagoField = value;
                this.OnPropertyChanged();
            }
        }

        public string ClaveFormaPago
        {
            get
            {
                return this.claveFormaPagoField;
            }
            set
            {
                this.claveFormaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Condiciones
        {
            get
            {
                return this.condicionesField;
            }
            set
            {
                this.condicionesField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal TipoCambio
        {
            get
            {
                return this.tipoCambioField;
            }
            set
            {
                this.tipoCambioField = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<ViewModelComprobanteConcepto> Conceptos
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
