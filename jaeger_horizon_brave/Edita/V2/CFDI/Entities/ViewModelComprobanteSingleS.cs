﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Edita.V2.CFDI.Enums;

namespace Jaeger.Edita.V2.CFDI.Entities
{
    /// <summary>
    /// Clase que representa un objeto simple de la base de datos de un comprobante fiscal
    /// </summary>
    public class ViewModelComprobanteSingleS
    {
        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_cfdi_id")]
        [SugarColumn(ColumnName = "_cfdi_id", IsPrimaryKey = true, IsIdentity = true)]
        public int Id { get; set; }

        /// <summary>
        /// Desc:registro activo
        /// Default:1
        /// Nullable:False
        /// </summary>           
        [DataNames("_cfdi_a")]
        public bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer tipo de documento (1=emitido,2=recibido,3=nomina) se agrega debido a que no existe un metodo en la utilidad mapper para asingar el valor
        /// </summary>
        [DataNames("_cfdi_doc_id")]
        public int SubTipoInt
        {
            get
            {
                return (int) this.SubTipo;
            }
            set
            {
                if (value == 1)
                    this.SubTipo = EnumCfdiSubType.Emitido;
                else if (value == 2)
                    this.SubTipo = EnumCfdiSubType.Recibido;
                else if (value == 3)
                    this.SubTipo = EnumCfdiSubType.Nomina;
                else
                    this.SubTipo = EnumCfdiSubType.None;
            }
        }

        [DataNames("_cfdi_doc_id")]
        public EnumCfdiSubType SubTipo { get; set; }

        /// <summary>
        /// Desc:numero de parcialidad
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_par")]
        public int NumParcialidad { get; set; }

        /// <summary>
        /// Desc:dias vencido
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_vence")]
        public int DiasVencido { get; set; }

        /// <summary>
        /// Desc:bandera de sincronizacion
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_sync")]
        public bool Sincronizado { get; set; }

        /// <summary>
        /// Desc:presicion
        /// Default:2
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_prec")]
        public int Presicion { get; set; }

        /// <summary>
        /// Desc:id del directorio
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_drctr_id")]
        public int IdDirectorio { get; set; }

        /// <summary>
        /// Desc:Domicilio del emisor
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_domie")]
        public int? IdDomicilioEmisor { get; set; }

        /// <summary>
        /// Desc:Domicilio del receptor
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_domir")]
        public int? IdDomicilioReceptor { get; set; }

        /// <summary>
        /// Desc:ID de serie para el modo edición
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_idserie")]
        public int? IdSerie { get; set; }

        /// <summary>
        /// Desc:ID de adenda para el modo edición
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_idaden")]
        public int? IdAddenda { get; set; }

        /// <summary>
        /// Desc:representar la suma de los importes antes de descuentos e impuestos
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_sbttl")]
        public decimal? SubTotal { get; set; }

        /// <summary>
        /// Desc:para representar el importe de los descuentos aplicables antes de impuestos
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_dscnt")]
        public decimal? Descuento { get; set; }

        /// <summary>
        /// Desc:Impuesto al Valor Agregado
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_trsiva")]
        public decimal? TrasladoIVA { get; set; }

        /// <summary>
        /// Desc:Impuesto especial sobre productos y servicios
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_trsieps")]
        public decimal? TrasladoIEPS { get; set; }

        /// <summary>
        /// Desc:Impuesto al Valor Agregado
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_retiva")]
        public decimal? RetencionIVA { get; set; }

        /// <summary>
        /// Desc:Impuesto sobre la renta
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_retisr")]
        public decimal? RetencionISR { get; set; }

        /// <summary>
        /// Desc:Impuesto especial sobre productos y servicios
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_retieps")]
        public decimal? RetencionIEPS { get; set; }

        /// <summary>
        /// Desc:representa la suma del subtotal, menos los descuentos aplicables, mas los impuestos, menos los impuestos retenidos
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_total")]
        public decimal? Total { get; set; }

        /// <summary>
        /// Desc:monto de los descuentos aplicados a el comprobante fiscal (nota de credito)
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_dscntc")]
        public decimal? _cfdi_dscntc { get; set; }

        /// <summary>
        /// Desc:monto cobrado o bien lo que se a pagado
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_cbrd")]
        public decimal? Acumulado { get; set; }

        /// <summary>
        /// Desc:monto cobrado o pagado con respecto a los comprobantes de pago
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_cbrdp")]
        public decimal? ImportesPagos { get; set; }

        /// <summary>
        /// Desc:total de percepciones
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_per")]
        public decimal? _cfdi_per { get; set; }

        /// <summary>
        /// Desc:total de deducciones
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_dec")]
        public decimal? _cfdi_dec { get; set; }

        /// <summary>
        /// Desc:version del comprobante fiscal
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_ver")]
        public string Version { get; set; }

        /// <summary>
        /// Desc:para expresar la clave del uso que dará a esta factura el receptor del CFDI
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_usocfdi")]
        public string ClaveUsoCFDI { get; set; }

        /// <summary>
        /// Desc:status del comprobante (0-Cancelado,1-Importado,2-Por Pagar
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_status")]
        public string Status { get; set; }

        /// <summary>
        /// Desc:para expresar el efecto del comprobante fiscal para el contribuyente emisor. Ingreso/egreso/traslado
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_efecto")]
        public string TipoComprobante { get; set; }

        /// <summary>
        /// Desc:clave de usuario que crea el registro
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_usr_n")]
        public string Creo { get; set; }

        /// <summary>
        /// Desc:clave del ultimo usuario que modifico el registro
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_usr_m")]
        public string Modifica { get; set; }

        /// <summary>
        /// Desc:rfc del emisor del comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_rfce")]
        public string EmisorRFC { get; set; }

        /// <summary>
        /// Desc:rfc del receptor del comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_rfcr")]
        public string ReceptorRFC { get; set; }

        /// <summary>
        /// Desc:representa el rfc del pac que certifica el comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_pac")]
        public string _cfdi_pac { get; set; }

        /// <summary>
        /// Desc:para representar el tipo de cambio conforme a la moneda usada para representar el tipo de cambio conforme a la moneda usada
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_moneda")]
        public string ClaveMoneda { get; set; }

        /// <summary>
        /// Desc:para control interno del contribuyente que acepta un valor numerico entero superior a 0 que expresa el folio del comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_folio")]
        public string Folio { get; set; }

        /// <summary>
        /// Desc:para incorporar al menos los cuatro ultimos digitos del numero de cuenta con la que se realizo el pago
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_cntpg")]
        public string CuentaPago { get; set; }

        /// <summary>
        /// Desc:bandera para indicar la existencia del comprobante en EDITA (Sí,No)
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_edita")]
        public string _cfdi_edita { get; set; }

        /// <summary>
        /// Desc:numero de serie del certificado que ampara al comprobante, de acuerdo al acuse correspondiente a 20 posiciones otorgado por el sistema del SAT.
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_nocert")]
        public string _cfdi_nocert { get; set; }

        /// <summary>
        /// Desc:opcional para precisar la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres alfabeticos de 1 a 25 caracteres sin incluir caracteres acentuados
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_serie")]
        public string Serie { get; set; }

        /// <summary>
        /// Desc:estado del comprobante (correcto,error!,cancelado,vigente)
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_estado")]
        public string Estado { get; set; }

        /// <summary>
        /// Desc:resultado de la descarga del comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_rslt")]
        public string Resultado { get; set; }

        /// <summary>
        /// Desc:folio fiscal
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_uuid")]
        public string IdDocumento { get; set; }

        /// <summary>
        /// Desc:documento (factura, nota de credito,nota de cargo)
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_doc")]
        public string Documento { get; set; }

        /// <summary>
        /// Desc:indicar llugar de expedición del comprobante fiscal
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_lgrexp")]
        public string LugarExpedicion { get; set; }

        /// <summary>
        /// Desc:forma de pago que aplica para este comprobante fiscal digital. Se utiliza para expresar Pago en una sola exhibicion o numero de parcialidad pagada contra el total de parcialidades, Parcialidad 1 de X
        /// Default:
        /// Nullable:True
        /// </summary>
        [DataNames("_cfdi_frmpg")]
        public string ClaveFormaPago { get; set; }

        /// <summary>
        /// Desc:para expresar el motivo del descuento aplicable
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_desct")]
        public string MotivoDescuento { get; set; }

        /// <summary>
        /// Desc:para expresar el metodo de pago de los bienes o servicios amparados por el comprobante. Se entiende como metodo de pago leyendas tales como: cheque, tarjeta de credito o debito, deposito en cuenta, etc.
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_mtdpg")]
        public string ClaveMetodoPago { get; set; }

        /// <summary>
        /// Desc:las condiciones comerciales aplicables para el pago del comprobante fiscal digital.
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_cndpg")]
        public string CondicionPago { get; set; }

        /// <summary>
        /// Desc:identidad fiscal del receptor o emisor del comprobante fiscal
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_nome")]
        public string Emisor { get; set; }

        /// <summary>
        /// Desc:identidad fiscal del receptor o emisor del comprobante fiscal
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_nomr")]
        public string Receptor { get; set; }

        /// <summary>
        /// Desc:observaciones
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_obsrv")]
        public string Nota { get; set; }

        /// <summary>
        /// Desc:fecha  de sistema
        /// Default:CURRENT_TIMESTAMP
        /// Nullable:False
        /// </summary>           
        [DataNames("_cfdi_fn")]
        public DateTime FechaNuevo { get; set; }

        /// <summary>
        /// Desc:fecha de emision
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_fecems")]
        public DateTime? FechaEmision { get; set; }

        /// <summary>
        /// Desc:fecha de certificacion
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_feccert")]
        public DateTime? _cfdi_feccert { get; set; }

        /// <summary>
        /// Desc:última fecha en que fué consultado en edita
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_fecedi")]
        public DateTime? FechaTimbrado { get; set; }

        /// <summary>
        /// Desc:última fecha en que fué consultado el estado del comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_fecedo")]
        public DateTime? FechaEstado { get; set; }

        /// <summary>
        /// Desc:fecha de vencimiento
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_fecvnc")]
        public DateTime? _cfdi_fecvnc { get; set; }

        /// <summary>
        /// Desc:fecha de entrega o recepcion del comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_fecent")]
        public DateTime? _cfdi_fecent { get; set; }

        /// <summary>
        /// Desc:fecha de cancelacion
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_feccnc")]
        public DateTime? FechaCancela { get; set; }

        /// <summary>
        /// Desc:fecha de autorizacion
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_fecaut")]
        public DateTime? _cfdi_fecaut { get; set; }

        /// <summary>
        /// Desc:fecha del ultimo pago o cobro de los recibos de pagoo cobro
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_fecupc")]
        public DateTime? FechaUltimoPago { get; set; }

        /// <summary>
        /// Desc:fecha del ultimo pago del comprobante de recepcion pagos, emitidos o recibidos
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_fecpgr")]
        public DateTime? FechaRecepcionPago { get; set; }

        /// <summary>
        /// Desc:fecha de modificacion
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_fm")]
        public DateTime? FechaModifica { get; set; }

        /// <summary>
        /// Desc:fecha de validacion
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_fecval")]
        public DateTime? FechaValidacion { get; set; }

        /// <summary>
        /// Desc:validación del comprobante fiscal
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_val")]
        public string _cfdi_val { get; set; }

        /// <summary>
        /// Desc:complemento nomina
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_nomina")]
        public string _cfdi_nomina { get; set; }

        /// <summary>
        /// Desc:url de descarga para el archivo xml
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_url_xml")]
        public string UrlXml { get; set; }

        /// <summary>
        /// Desc:url de descarga para la representación impresa
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_url_pdf")]
        public string UrlPdf { get; set; }

        /// <summary>
        /// Desc:url de descarga del archivo xml del acuse de cancelacion del comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_url_xmlacu")]
        public string UrlAcuseXml { get; set; }

        /// <summary>
        /// Desc:url de descarga del archivo pdf del acuse de cancelacion del comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_url_pdfacu")]
        public string UrlAcusePdf { get; set; }

        /// <summary>
        /// Desc:url de descarga de la representacio impresa del acusde de cancelacion del comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_url_acu")]
        public string _cfdi_url_acu { get; set; }

        /// <summary>
        /// Desc:listado de complemento (json)
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_compl")]
        public string _cfdi_compl { get; set; }

        /// <summary>
        /// Desc:complemento de pagos
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_pagos")]
        public string _cfdi_pagos { get; set; }

        /// <summary>
        /// obtener o establecer la información de los comprobantes relacionados.
        /// </summary>
        [DataNames("_cfdi_comrel")]
        [SugarColumn(ColumnName = "_cfdi_comrel")]
        public string JCfdiRelacionados
        {
            get
            {
                if (this.CfdiRelacionados != null)
                    return this.CfdiRelacionados.Json();
                return null;
            }
            set
            {
                if (this.CfdiRelacionados == null)
                {
                    this.CfdiRelacionados = new ComprobanteCfdiRelacionados();
                }
                this.CfdiRelacionados = ComprobanteCfdiRelacionados.Json(value);
            }
        }

        [SugarColumn(IsIgnore = true)]
        public ComprobanteCfdiRelacionados CfdiRelacionados { get; set; }

        /// <summary>
        /// Desc:acuse de cancelacion
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_acuse")]
        public string _cfdi_acuse { get; set; }

        /// <summary>
        /// Desc:Adenda del documento
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_adenda")]
        public string _cfdi_adenda { get; set; }

        /// <summary>
        /// Desc:XML Completo en formato JSON (Al guardar en S3 se borra)
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_save")]
        public string _cfdi_save { get; set; }

        /// <summary>
        /// Desc:para mediciones
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdi_medir")]
        public string _cfdi_medir { get; set; }

    }
}
