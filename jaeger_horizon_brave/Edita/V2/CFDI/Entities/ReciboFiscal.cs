﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.CFDI.Entities.Addenda;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.CFDI.Entities
{
    public class ViewModelReciboFiscal : BasePropertyChangeImplementation
    {
        private int lngIndex;
        private string strEstado;
        private string emisor;
        private string emisorRFC;
        private string strReceptorRfc;
        private string strReceptor;
        private string strSerie;
        private string strFolio;
        private string strUuid;
        private decimal dblSubTotal;
        private decimal dblIva;
        private decimal dblTotal;
        private DateTime? datFecEmision;
        private DateTime? datFecTimbre;
        private DateTime? datFecCancela;

        public ViewModelReciboFiscal()
        {
            this.lngIndex = 0;
            this.strEstado = string.Empty;
            this.strReceptorRfc = string.Empty;
            this.strReceptor = string.Empty;
            this.emisor = string.Empty;
            this.emisorRFC = string.Empty;
            this.strSerie = string.Empty;
            this.strFolio = string.Empty;
            this.strUuid = string.Empty;
            this.dblSubTotal = 0;
            this.dblIva = 0;
            this.dblTotal = 0;
            this.datFecTimbre = null;
            this.datFecCancela = null;
            this.Addenda = new AddendaReciboFiscalBoleta();
        }

        [DisplayName("Consecutivo")]
        public string Consecutivo
        {
            get
            {
                return this.Addenda.Consecutivo;
            }
        }

        [DisplayName("Cuota Auto")]
        public double CuotaAutomovil
        {
            get
            {
                try {
                    return Convert.ToDouble(this.Addenda.CuotaAuto);
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                    return 0;
                }
            }
        }

        [DisplayName("Cuota Gastos Admon.")]
        public double CuotaGastosAdmon
        {
            get
            {
                return Convert.ToDouble(this.Addenda.CuotaGastos);
            }
        }

        [DisplayName("C. Iva")]
        public double CuotaIva
        {
            get
            {
                return Convert.ToDouble(this.Addenda.Iva);
            }
        }

        [DisplayName("Cuota Segudo Vida")]
        public double CuotaSeguroVida
        {
            get
            {
                return Convert.ToDouble(this.Addenda.CuotaSeguroVida);
            }
        }

        [Browsable(false)]
        [DisplayName("Estado")]
        [DataNames("_cfdi_estado")]
        public string Estado
        {
            get
            {
                return this.strEstado;
            }
            set
            {
                this.strEstado = value;
                this.OnPropertyChanged("Estado");
            }
        }

        [DataNames("_cfdi_feccnc")]
        [DisplayName("Fec. Cancelación")]
        public DateTime? FecCancela
        {
            get
            {
                DateTime? firstGoodDate = new DateTime?();
                if (this.datFecCancela >= firstGoodDate)
                    return this.datFecCancela;
                return null;
            }
            set
            {
                this.datFecCancela = value;
                this.OnPropertyChanged("FecCancela");
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emisión del comprobante fiscal
        /// </summary>
        [DataNames("_cfdi_fecems")]
        [DisplayName("Fec. Emisión")]
        public DateTime? FecEmision
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.datFecEmision >= firstGoodDate)
                    return this.datFecEmision;
                return null;
            }
            set
            {
                this.datFecEmision = value;
                this.OnPropertyChanged("FecEmision");
            }
        }

        /// <summary>
        /// obtener el número del plan al que pertenece
        /// </summary>
        [DisplayName("No Plan")]
        public int NoPlan
        {
            get
            {
                int cp = 0;
                try {
                    cp = Convert.ToInt32(this.Consecutivo);
                }
                catch (Exception) {
                    cp = 0;
                }
                if (cp >= 50001 && cp <= 51500)
                    return 5;
                else if (cp >= 51501 && cp <= 59999)
                    return 7;
                else if (cp >= 200001 && cp <= 299999)
                    return 6;
                else if (cp >= 420001 && cp <= 499999)
                    return 1;
                else if (cp >= 716001 && cp <= 744269)
                    return 4;
                else if (cp >= 726001 && cp <= 734000)
                    return 3;
                else if (cp >= 744301 && cp <= 799999)
                    return 2;
                if (cp == 0) {
                    if (this.Consecutivo.Trim().StartsWith("A"))
                        return 8;
                    else if (this.Consecutivo.Trim().StartsWith("C"))
                        return 9;
                    else if (this.Consecutivo.Trim().StartsWith("B"))
                        return 10;
                }

                return 0;
            }
        }

        public string NoPlanText
        {
            get
            {
                try {
                    int cp = Convert.ToInt32(this.Consecutivo);
                    if (cp >= 50001 && cp <= 51500)
                        return "Autof Clásico";
                    else if (cp >= 51501 && cp <= 59999)
                        return "Afasa 3";
                    else if (cp >= 200001 && cp <= 299999)
                        return "Estrena ya";
                    else if (cp >= 420001 && cp <= 499999)
                        return "Flexible";
                    else if (cp >= 716001 && cp <= 744269)
                        return "Clásico N.R.";
                    else if (cp >= 726001 && cp <= 734000)
                        return "Económico";
                    else if (cp >= 744301 && cp <= 799999)
                        return "A la medida";
                    return "Desconocido";
                }
                catch (Exception) {
                    if (this.Consecutivo.Trim().StartsWith("A"))
                        return "AFARAPIDO60";
                    else if (this.Consecutivo.Trim().StartsWith("C"))
                        return "AFACOMODO60";
                    else if (this.Consecutivo.Trim().StartsWith("B"))
                        return "AFAFLEX60";
                    return "Desconocido";
                }
            }
        }

        /// <summary>
        /// obtener el día de la fecha de emision del comprobante
        /// </summary>
        [DisplayName("Día")]
        public int Dia
        {
            get
            {
                if (this.FecEmision != null)
                {
                    return this.FecEmision.Value.Day;
                }
                return 0;
            }
        }

        [DataNames("_cfdi_feccert")]
        [DisplayName("Fec. Certificación")]
        public DateTime? FecTimbre
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.datFecTimbre >= firstGoodDate)
                    return this.datFecTimbre;
                return null;
            }
            set
            {
                this.datFecTimbre = value;
            }
        }

        /// <summary>
        /// obtener o establecer el folio de control interno del comprobante fiscal
        /// </summary>
        [DataNames("_cfdi_folio")]
        [DisplayName("Folio")]
        public string Folio
        {
            get
            {
                return this.strFolio;
            }
            set
            {
                this.strFolio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        [Browsable(false)]
        [DataNames("_cfdi_id")]
        public int Id
        {
            get
            {
                return this.lngIndex;
            }
            set
            {
                this.lngIndex = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_trsiva")]
        [DisplayName("Iva")]
        public decimal Iva
        {
            get
            {
                return this.dblIva;
            }
            set
            {
                this.dblIva = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Mensualidad")]
        public string Mensualidad
        {
            get
            {
                return this.Addenda.Mensualidad;
            }
        }

        [DisplayName("Otros")]
        public decimal Otros
        {
            get
            {
                return Convert.ToDecimal(this.Addenda.Otro);
            }
        }

        [DataNames("_cfdi_nome")]
        [DisplayName("Emisor")]
        public string Emisor
        {
            get
            {
                return this.emisor;
            }
            set
            {
                this.emisor = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_rfce")]
        [DisplayName("Emisor (RFC)")]
        public string EmisorRFC
        {
            get
            {
                return this.emisorRFC;
            }
            set
            {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_nomr")]
        [DisplayName("Receptor")]
        public string Receptor
        {
            get
            {
                return this.strReceptor;
            }
            set
            {
                this.strReceptor = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_rfcr")]
        [DisplayName("Receptor (RFC)")]
        public string ReceptorRfc
        {
            get
            {
                return this.strReceptorRfc;
            }
            set
            {
                this.strReceptorRfc = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Cuota Seguro Auto.")]
        public decimal SeguroAuto
        {
            get {
                try {
                    return Convert.ToDecimal(this.Addenda.Seguro);
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                    return 0;
                }
            }
        }

        [DataNames("_cfdi_serie")]
        [DisplayName("Serie")]
        public string Serie
        {
            get
            {
                return this.strSerie;
            }
            set
            {
                this.strSerie = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_sbttl")]
        [DisplayName("SubTotal")]
        public decimal SubTotal
        {
            get
            {
                return this.dblSubTotal;
            }
            set
            {
                this.dblSubTotal = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_total")]
        [DisplayName("Total")]
        public decimal Total
        {
            get
            {
                return this.dblTotal;
            }
            set
            {
                this.dblTotal = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Total Pagado")]
        public decimal TotalPagado
        {
            get
            {
                return Convert.ToDecimal(this.Addenda.Total);
            }
        }

        [DataNames("_cfdi_uuid")]
        [DisplayName("Folio Fiscal (uuid)")]
        public string Uuid
        {
            get
            {
                return this.strUuid;
            }
            set
            {
                this.strUuid = value;
                this.OnPropertyChanged("Uuid");
            }
        }

        [DataNames("_cfdi_adenda")]
        public string JSon
        {
            set
            {
                if (this.FecEmision.Value.Year < 2017)
                    this.Json(value);
                else if (this.FecEmision.Value.Year >= 2018)
                {
                    AddendaAfasaBoletaPago t = AddendaAfasaBoletaPago.Json(value);
                    if (t.Name == "afasaBoletaPago")
                    {
                        AddendaReciboFiscalBoleta t2 = new AddendaReciboFiscalBoleta();
                        List<Parametros>.Enumerator enumerator1 = new List<Parametros>.Enumerator();
                        enumerator1 = t.Items.GetEnumerator();
                        try
                        {
                            while (enumerator1.MoveNext())
                            {
                                Parametros item2 = enumerator1.Current;
                                string field = item2.Field;
                                if (field == "cons")
                                    t2.Consecutivo = item2.Value;
                                else if (field == "mens")
                                    t2.Mensualidad = item2.Value;
                                else if (field == "otro")
                                    t2.Otro = item2.Value;
                                else if (field == "cuat")
                                    t2.CuotaAuto = item2.Value;
                                else if (field == "cuse")
                                    t2.CuotaSeguroVida = item2.Value;
                                else if (field == "cuga")
                                    t2.CuotaGastos = item2.Value;
                                else if (field == "iva")
                                    t2.Iva = item2.Value;
                                else if (field == "segu")
                                    t2.Seguro = item2.Value;
                                else if (field == "tota")
                                    t2.Total = item2.Value;
                                else if (field == "fepa")
                                    t2.FechaPago = item2.Value;
                            }
                        }
                        finally
                        {
                            ((IDisposable)enumerator1).Dispose();
                        }
                        this.Addenda = t2;
                    }
                }
            }
            get { return string.Empty; }
        }

        [Browsable(false)]
        public AddendaReciboFiscalBoleta Addenda { get; set; }

        public void Json(string strJson)
        {
            this.Addenda = AddendaReciboFiscalBoleta.Json(strJson);
        }

        public string Json()
        {
            return this.Addenda.Json();
        }
    }
}