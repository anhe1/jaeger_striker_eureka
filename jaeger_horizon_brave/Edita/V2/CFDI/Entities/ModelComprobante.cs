﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Edita.V2.CFDI.Entities
{
    public class ModelComprobante
    {
        public ModelComprobante()
        {

        }
        // emisor del comprobante
        public string EmisorRFC { get; set; }
        public string EmisorNombre { get; set; }
        public string RegimenFiscal { get; set; }
        public string EmisorDomicilio { get; set; }
        // receptor del comprobante
        public string ReceptorRFC { get; set; }
        public string ReceptorCURP { get; set; }
        public string ReceptorNombre { get; set; }
        public string ReceptorDomicilio { get; set; }
        public string ResidenciaFiscal { get; set; }
        public string UsoCFDI { get; set; }
        // informacion del comprobante
        public string Version { get; set; }
        public string IdDocumento { get; set; }
        public string TipoComprobante { get; set; }
        public string Serie { get; set; }
        public string Folio { get; set; }
        public string Moneda { get; set; }
        public string TipoCambio { get; set; }
        public string MetodoPago { get; set; }
        public string FormaPago { get; set; }
        public string CondicionPago { get; set; }
        public string MotivoDescuento { get; set; }
        public string LugarExpedicion { get; set; }
        public string NoCertificado { get; set; }
        public DateTime? FechaEmision { get; set; }
        public decimal TotalTraslados { get; set; }
        public decimal TotalRetenciones { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }

        public string FechaFolioFiscalOrig { get; set; }
        public decimal MontoFolioFiscalOrig { get; set; }

        // datos del timbre
        public DateTime? FechaTimbrado { get; set; }
        public string NoCertificadoSAT { get; set; }
        public string SelloDigitalSAT { get; set; }
        public string CadenaOriginalCertificacion { get; set; }
        public string RFCProvCertif { get; set; }
        public string SelloDigital { get; set; }
        public string CadenaOriginal { get; set; }

        //
        public string TotalLetra { get; set; }
        public byte[] CBB { get; set; }
        public byte[] LogoTipo { get; set; }

    }
}
