﻿using System;
using System.Linq;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Edita.V2.CFDI.Entities.Cancel;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.CFDI.Entities.Addenda;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Edita.V2.CFDI.Entities {
    [SugarTable("_cfdi")]
    public class ViewModelComprobante : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private int indiceField;
        private bool esActivoField = true;
        private EnumCfdiType tipoComprobanteField;
        private EnumCfdiSubType subTipoDeComprobanteField;
        private string statusField;
        private string versionField;
        private string serieField;
        private string folioField;
        private string estadoField;
        // emisor del comprobante
        private ViewModelContribuyente emisorField;
        // receptor del comprobante
        private ViewModelContribuyente receptorField;
        private decimal tipoDeCambioField;
        private ClaveMoneda claveMonedaField;
        private ClaveMetodoPago claveMetodoPagoField;
        private ClaveFormaPago claveFormaPagoField;
        private ClaveUsoCFDI claveUsoCfdiField;
        private DateTime fechaEmisionField;
        private DateTime? fechaEstadoField;
        private DateTime? fechaCancelaField;
        private DateTime? fechaEntregaField;
        private DateTime? fechaUltPagoField;
        private DateTime? fechaRecepcionPagoField;
        private string ctaPagoField;
        private string condicionPagoField;
        private string noCertificadoField;
        private string motivoDescuentoField;
        private string lugarExpedicionField;
        private int diasDeVenceField;
        private int presionDecimalField;
        private int numPacialidadField;
        private string tipoCambioField;
        private decimal retencionIsrField;
        private decimal retencionIvaField;
        private decimal trasladoIvaField;
        private decimal retencionIepsField;
        private decimal trasladoIepsField;
        private decimal totalPecepcionField;
        private decimal totalDeduccionField;
        private decimal subTotalField;
        private decimal descuentoField;
        private decimal totalField;
        private decimal acumuladoField;
        private decimal importePagadoField;
        private string resultField;
        private string notasField;
        // ligas de descarga
        private string fileXmlField;
        private string filePdfField;
        private string fileAccuseField;
        // complementos
        private ComplementoTimbreFiscal timbreFiscalField;
        private Complementos complementosField;
        private ComprobanteCfdiRelacionados cfdiRelacionadosField;
        private ComplementoNomina nominaField;
        private ComplementoPagos pagosField;
        private CancelaCFDResponse accuseField;
        private BindingList<ViewModelComprobanteConcepto> objConceptos;
        private ValidateResponse validacionField;
        private Addendas addendasField;
        private bool syncronizadoField;
        private string creoField;
        private string modificaField;
        private DateTime fechaNuevoField;
        private DateTime? fechaModificaField;
        private string originalXmlField;
        private string documento;
        private int idAddenda;
        private int idSerie;
        private string regimenFiscalReceptor;
        private string domicilioFiscalReceptor;
        private string receptorResidenciaFiscal;
        private string receptorNumRegIdTrib;
        private string exportacionField;

        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ViewModelComprobante() {
            this.claveMonedaField = new ClaveMoneda() { Clave = "MXN" };
            this.emisorField = new ViewModelContribuyente();
            this.receptorField = new ViewModelContribuyente();
            this.claveUsoCfdiField = new ClaveUsoCFDI();
            this.claveMetodoPagoField = new ClaveMetodoPago();
            this.claveFormaPagoField = new ClaveFormaPago();
            this.complementosField = new Complementos();
            this.cfdiRelacionadosField = new ComprobanteCfdiRelacionados();
            this.objConceptos = new BindingList<ViewModelComprobanteConcepto>() { RaiseListChangedEvents = true };
            this.objConceptos.AddingNew += new AddingNewEventHandler(this.Conceptos_AddingNew);
            this.objConceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
            this.versionField = "3.3";
            this.fechaEmisionField = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
            this.presionDecimalField = 2;
            this.timbreFiscalField = null;
            this.cfdiRelacionadosField = null;
            this.addendasField = null;
        }

        #region propiedades

        /// <summary>
        /// obtener ó establecer el indice de la tabla.
        /// </summary>
        [DataNames("_cfdi_id")]
        [SugarColumn(ColumnName = "_cfdi_id", IsIdentity = true, IsPrimaryKey = true)]
        public int Id {
            get {
                return this.indiceField;
            }
            set {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_drctr_id")]
        [SugarColumn(ColumnName = "_cfdi_drctr_id", IsNullable = true)]
        public int SubId {
            get {
                if (this.SubTipo == EnumCfdiSubType.Nomina || this.SubTipo == EnumCfdiSubType.Emitido) {
                    return this.Receptor.Id;
                }
                else {
                    return this.Emisor.Id;
                }
            }
            set {
                if (this.SubTipo == EnumCfdiSubType.Nomina || this.SubTipo == EnumCfdiSubType.Emitido) {
                    this.Receptor.Id = value;
                }
                else {
                    this.Emisor.Id = value;
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener ó establecer si el registro es activo.
        /// </summary>
        [DataNames("_cfdi_a")]
        [SugarColumn(ColumnName = "_cfdi_a", ColumnDescription = "registro activo")]
        public bool Activo {
            get {
                return this.esActivoField;
            }
            set {
                this.esActivoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_idaden")]
        [SugarColumn(ColumnName = "_cfdi_idaden", ColumnDescription = "ID de adenda para el modo edición")]
        public int IdAddenda {
            get {
                return this.idAddenda;
            }
            set {
                this.idAddenda = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_idserie")]
        [SugarColumn(ColumnName = "_cfdi_idserie", ColumnDescription = "ID de serie para el modo edición")]
        public int IdSerie {
            get {
                return this.idSerie;
            }
            set {
                this.idSerie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de comprobante en modo texto
        /// </summary>
        [DataNames("_cfdi_efecto")]
        [SugarColumn(ColumnName = "_cfdi_efecto")]
        public string TipoComprobanteText {
            get {
                return Enum.GetName(typeof(EnumCfdiType), this.TipoComprobante);
            }
            set {
                if (value == "Ingreso" || value == "I") {
                    this.TipoComprobante = EnumCfdiType.Ingreso;
                }
                else if (value == "Egreso" || value == "E") {
                    this.TipoComprobante = EnumCfdiType.Egreso;
                }
                else if (value == "Traslado" || value == "T") {
                    this.TipoComprobante = EnumCfdiType.Traslado;
                }
                else if (value == "Nomina" || value == "N") {
                    this.TipoComprobante = EnumCfdiType.Nomina;
                }
                else if (value == "Pagos" || value == "P") {
                    this.TipoComprobante = EnumCfdiType.Pagos;
                }
                else {
                    this.TipoComprobante = EnumCfdiType.Ingreso;
                }
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string SubTipoText {
            get {
                return Enum.GetName(typeof(EnumCfdiSubType), this.subTipoDeComprobanteField);
            }
            set {
                this.subTipoDeComprobanteField = (EnumCfdiSubType)Enum.Parse(typeof(EnumCfdiSubType), value);
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_status")]
        [SugarColumn(ColumnName = "_cfdi_status")]
        public string StatusText {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// version del comprobante
        /// </summary>
        [DataNames("_cfdi_ver")]
        [SugarColumn(ColumnName = "_cfdi_ver")]
        public string Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        [DataNames("_cfdi_serie")]
        [SugarColumn(ColumnName = "_cfdi_serie", ColumnDescription = "serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres.", Length = 25)]
        public string Serie {
            get {
                return this.serieField;
            }
            set {
                if (!(value == null)) {
                    this.serieField = value.ToUpper();
                }
                else {
                    this.serieField = value;
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        [DataNames("_cfdi_folio")]
        [SugarColumn(ColumnName = "_cfdi_folio", ColumnDescription = "atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.", Length = 21)]
        public string Folio {
            get {
                return this.folioField;
            }
            set {
                if (!(value == null)) {
                    if (value.Length >= 21) {
                        this.folioField = value.Substring(0, 21);
                    }
                    else {
                        this.folioField = value;
                    }
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        [DataNames("_cfdi_estado")]
        [SugarColumn(ColumnName = "_cfdi_estado")]
        public string Estado {
            get {
                return this.estadoField;
            }
            set {
                this.estadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_rfce")]
        [SugarColumn(ColumnName = "_cfdi_rfce")]
        public string EmisorRFC {
            get {
                return this.emisorField.RFC;
            }
            set {
                this.emisorField.RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_nome")]
        [SugarColumn(ColumnName = "_cfdi_nome")]
        public string EmisorNombre {
            get {
                return this.emisorField.Nombre;
            }
            set {
                this.emisorField.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_rfcr")]
        [SugarColumn(ColumnName = "_cfdi_rfcr")]
        public string ReceptorRFC {
            get {
                return this.receptorField.RFC;
            }
            set {
                this.receptorField.RFC = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_nomr")]
        [SugarColumn(ColumnName = "_cfdi_nomr")]
        public string ReceptorNombre {
            get {
                return this.receptorField.Nombre;
            }
            set {
                this.receptorField.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el comprobante ampara una operación de exportación.
        /// </summary>
        [DataNames("_cfdi_clvexp")]
        [SugarColumn(ColumnName = "_cfdi_clvexp", ColumnDescription = "expresar si el comprobante ampara una operación de exportación.", Length = 2)]
        public string ClaveExportacion {
            get {
                return this.exportacionField;
            }
            set {
                this.exportacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de registro de identidad fiscal del receptor cuando sea residente en el extranjero. Es requerido cuando se incluya el complemento de comercio exterior.
        /// </summary>
        [DataNames("_cfdi_nmreg")]
        [SugarColumn(ColumnName = "_cfdi_nmreg", ColumnDescription = "numero de registro de identidad fiscal del receptor cuando sea residente en el extranjero. Es requerido cuando se incluya el complemento de comercio exterior.", Length = 40, IsNullable = true)]
        public string NumRegIdTrib {
            get { return this.receptorNumRegIdTrib; }
            set {
                this.receptorNumRegIdTrib = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del país de residencia para efectos fiscales del receptor del comprobante, cuando se trate de un extranjero, y que es conforme con la especificación ISO 3166-1 alpha-3. 
        /// Es requerido cuando se incluya el complemento de comercio exterior o se registre el atributo NumRegIdTrib.
        /// </summary>
        [DataNames("_cfdi_resfis")]
        [SugarColumn(ColumnName = "_cfdi_resfis", ColumnDescription = "clave del país de residencia para efectos fiscales del receptor del comprobante", Length = 20, IsNullable = false)]
        public string ResidenciaFiscal {
            get { return this.receptorResidenciaFiscal; }
            set {
                this.receptorResidenciaFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el código postal del domicilio fiscal del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_domfis")]
        [SugarColumn(ColumnName = "_cfdi_domfis", ColumnDescription = "codigo postal del domicilio fiscal del receptor del comprobante", Length = 5, IsNullable = false)]
        public string DomicilioFiscal {
            get { return this.domicilioFiscalReceptor; }
            set {
                this.domicilioFiscalReceptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del régimen fiscal del contribuyente receptor al que aplicará el efecto fiscal de este comprobante
        /// </summary>
        [DataNames("_cfdi_rgfsc")]
        [SugarColumn(ColumnName = "_cfdi_rgfsc", ColumnDescription = "clave de regimen fiscal", Length = 255, IsNullable = false)]
        public string ClaveRegimenFiscal {
            get { return this.regimenFiscalReceptor; }
            set {
                this.regimenFiscalReceptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cambio utilizado en el comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public decimal TipoDeCambio {
            get {
                return this.tipoDeCambioField;
            }
            set {
                this.tipoDeCambioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        [DataNames("_cfdi_moneda")]
        [SugarColumn(ColumnName = "_cfdi_moneda", ColumnDescription = "clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.", Length = 3)]
        public string ClaveMoneda {
            get {
                return this.claveMonedaField.Clave;
            }
            set {
                this.claveMonedaField.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.
        /// </summary>
        [DataNames("_cfdi_mtdpg")]
        [SugarColumn(ColumnName = "_cfdi_mtdpg", ColumnDescription = "Atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.", Length = 3)]
        public string ClaveMetodoPago {
            get {
                return this.claveMetodoPagoField.Clave;
            }
            set {
                this.claveMetodoPagoField.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.
        /// </summary>
        [DataNames("_cfdi_frmpg")]
        [SugarColumn(ColumnName = "_cfdi_frmpg", ColumnDescription = "obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.", Length = 2)]
        public string ClaveFormaPago {
            get {
                return this.claveFormaPagoField.Clave;
            }
            set {
                this.claveFormaPagoField.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.
        /// </summary>
        [DataNames("_cfdi_usocfdi")]
        [SugarColumn(ColumnName = "_cfdi_usocfdi", ColumnDescription = "atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.", Length = 3)]
        public string ClaveUsoCfdi {
            get {
                return this.claveUsoCfdiField.Clave;
            }
            set {
                if (this.claveUsoCfdiField == null) {
                    this.claveUsoCfdiField = new ClaveUsoCFDI();
                }
                this.claveUsoCfdiField.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// </summary>
        [DataNames("_cfdi_fecems")]
        [SugarColumn(ColumnName = "_cfdi_fecems", ColumnDescription = "fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmisionField;
            }
            set {
                this.fechaEmisionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de certificación
        /// </summary>
        [DataNames("_cfdi_feccert")]
        [SugarColumn(ColumnName = "_cfdi_feccert")]
        public DateTime? FechaCert {
            get {
                if (!(this.timbreFiscalField == null)) {
                    DateTime firstGoodDate = new DateTime(1900, 1, 1);
                    if (this.timbreFiscalField.FechaTimbrado >= firstGoodDate) {
                        return this.timbreFiscalField.FechaTimbrado;
                    }
                    else {
                        return null;
                    }
                }
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate) {
                    if (this.timbreFiscalField == null) {
                        this.timbreFiscalField = new ComplementoTimbreFiscal();
                    }
                    this.timbreFiscalField.FechaTimbrado = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        [DataNames("_cfdi_uuid")]
        [SugarColumn(ColumnName = "_cfdi_uuid")]
        public string UUID {
            get {
                if (!(this.timbreFiscalField == null)) {
                    return this.timbreFiscalField.UUID;
                }
                return null;
            }
            set {
                if (Jaeger.Helpers.HelperValidacion.UUID(value)) {
                    if (this.timbreFiscalField == null) {
                        this.timbreFiscalField = new ComplementoTimbreFiscal();
                    }
                    this.timbreFiscalField.UUID = value.ToUpper();
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer la fecha del estado del comprobante (SAT)
        /// </summary>
        [DataNames("_cfdi_fecedo")]
        [SugarColumn(ColumnName = "_cfdi_fecedo")]
        public DateTime? FechaEstado {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEstadoField >= firstGoodDate) {
                    return this.fechaEstadoField;
                }
                return null;
            }
            set {
                this.fechaEstadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdi_feccnc")]
        [SugarColumn(ColumnName = "_cfdi_feccnc")]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancelaField >= firstGoodDate) {
                    return this.fechaCancelaField;
                }
                return null;
            }
            set {
                this.fechaCancelaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de entrega o recepcion del comprobante
        /// </summary>
        [DataNames("_cfdi_fecent")]
        [SugarColumn(ColumnName = "_cfdi_fecent", IsNullable = true)]
        public DateTime? FechaEntrega {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEntregaField >= firstGoodDate) {
                    return this.fechaEntregaField;
                }
                return null;
            }
            set {
                this.fechaEntregaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de ultimo pago o cobro del comprobante
        /// </summary>
        [DataNames("_cfdi_fecupc")]
        [SugarColumn(ColumnName = "_cfdi_fecupc")]
        public DateTime? FechaUltPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaUltPagoField >= firstGoodDate) {
                    return this.fechaUltPagoField;
                }
                return null;
            }
            set {
                this.fechaUltPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha del comprobante fiscal de repecion de pago
        /// </summary>
        [DataNames("_cfdi_fecpgr")]
        [SugarColumn(ColumnName = "_cfdi_fecpgr")]
        public DateTime? FechaRecepcionPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaRecepcionPagoField >= firstGoodDate) {
                    return this.fechaRecepcionPagoField;
                }
                return null;
            }
            set {
                this.fechaRecepcionPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de validacion del comprobante
        /// </summary>
        [DataNames("_cfdi_fecval")]
        [SugarColumn(ColumnName = "_cfdi_fecval")]
        public DateTime? FechaVal {
            get {
                if (this.validacionField != null) {
                    return this.validacionField.FechaValidacion;
                }
                return null;
            }
        }

        /// <summary>
        /// obtener o establecer cuenta de pago del emisor del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string CtaPago {
            get {
                return this.ctaPagoField;
            }
            set {
                this.ctaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo condicional para expresar las condiciones comerciales aplicables para el pago del comprobante fiscal digital por Internet. Este atributo puede ser condicionado mediante atributos o complementos. Maximo de caracteres 1000
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,1000}"/>
        /// </summary>
        [DataNames("_cfdi_cndpg")]
        [SugarColumn(ColumnName = "_cfdi_cndpg", ColumnDescription = "condiciones comerciales aplicables para el pago del comprobante fiscal digital por Internet. Este atributo puede ser condicionado mediante atributos o complementos. Maximo de caracteres 1000", Length = 254)]
        public string CondicionPago {
            get {
                return this.condicionPagoField;
            }
            set {
                this.condicionPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de certificado del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_nocert")]
        [SugarColumn(ColumnName = "_cfdi_nocert")]
        public string NoCertificado {
            get {
                return this.noCertificadoField;
            }
            set {
                this.noCertificadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del proveedor de certificación de comprobantes fiscales digitales que genera el timbre fiscal digital.
        /// </summary>
        [DataNames("_cfdi_pac")]
        [SugarColumn(ColumnName = "_cfdi_pac")]
        public string RfcProvCertif {
            get {
                if (!(this.timbreFiscalField == null)) {
                    return this.timbreFiscalField.RFCProvCertif;
                }
                else {
                    return null;
                }
            }
            set {
                if (!(string.IsNullOrEmpty(value))) {
                    if (this.timbreFiscalField == null) {
                        this.timbreFiscalField = new ComplementoTimbreFiscal();
                    }
                    this.timbreFiscalField.RFCProvCertif = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// motivo del descuento del comprobante
        /// </summary>
        [DataNames("_cfdi_desct")]
        [SugarColumn(ColumnName = "_cfdi_desct")]
        public string MotivoDescuento {
            get {
                return this.motivoDescuentoField;
            }
            set {
                this.motivoDescuentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el código postal del lugar de expedición del comprobante (domicilio de la matriz o de la sucursal).
        /// </summary>
        [DataNames("_cfdi_lgrexp")]
        [SugarColumn(ColumnName = "_cfdi_lgrexp")]
        public string LugarExpedicion {
            get {
                return this.lugarExpedicionField;
            }
            set {
                this.lugarExpedicionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// dias de vencimiento del comprobante
        /// </summary>
        [DataNames("_cfdi_vence")]
        [SugarColumn(ColumnName = "_cfdi_vence")]
        public int DiasDeVence {
            get {
                return this.diasDeVenceField;
            }
            set {
                this.diasDeVenceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la precision decimal utilizada para el comprobante
        /// </summary>
        [DataNames("_cfdi_prec")]
        [SugarColumn(ColumnName = "_cfdi_prec")]
        public int PrecisionDecimal {
            get {
                return this.presionDecimalField;
            }
            set {
                this.presionDecimalField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string TipoCambio {
            get {
                return this.tipoCambioField;
            }
            set {
                this.tipoCambioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de parcialidad
        /// </summary>
        [DataNames("_cfdi_par")]
        [SugarColumn(ColumnName = "_cfdi_par")]
        public int Parcialidad {
            get {
                return this.numPacialidadField;
            }
            set {
                this.numPacialidadField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_retisr")]
        [SugarColumn(ColumnName = "_cfdi_retisr")]
        public decimal RetencionIsr {
            get {
                return Math.Round(this.retencionIsrField, 4);
            }
            set {
                this.retencionIsrField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_retiva")]
        [SugarColumn(ColumnName = "_cfdi_retiva")]
        public decimal RetencionIva {
            get {
                return Math.Round(this.retencionIvaField, 4);
            }
            set {
                this.retencionIvaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// importe del traslado de IVA
        /// </summary>
        [DataNames("_cfdi_trsiva")]
        [SugarColumn(ColumnName = "_cfdi_trsiva")]
        public decimal TrasladoIva {
            get {
                return Math.Round(this.trasladoIvaField, 4);
            }
            set {
                this.trasladoIvaField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_retieps")]
        [SugarColumn(ColumnName = "_cfdi_retieps")]
        public decimal RetencionIeps {
            get {
                return this.retencionIepsField;
            }
            set {
                this.retencionIepsField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_trsieps")]
        [SugarColumn(ColumnName = "_cfdi_trsieps")]
        public decimal TrasladoIeps {
            get {
                return this.trasladoIepsField;
            }
            set {
                this.trasladoIepsField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_per")]
        [SugarColumn(ColumnName = "_cfdi_per")]
        public decimal TotalPecepcion {
            get {
                return this.totalPecepcionField;
            }
            set {
                this.totalPecepcionField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_dec")]
        [SugarColumn(ColumnName = "_cfdi_dec")]
        public decimal TotalDeduccion {
            get {
                return this.totalDeduccionField;
            }
            set {
                this.totalDeduccionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el subTotal del comprobante
        /// </summary>
        [DataNames("_cfdi_sbttl")]
        [SugarColumn(ColumnName = "_cfdi_sbttl")]
        public decimal SubTotal {
            get {
                return this.subTotalField;
            }
            set {
                this.subTotalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// monto del descuento aplicado
        /// </summary>
        [DataNames("_cfdi_dscnt")]
        [SugarColumn(ColumnName = "_cfdi_dscnt")]
        public decimal Descuento {
            get {
                return Math.Round(this.descuentoField, 4);
            }
            set {
                this.descuentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// monto del comprobante
        /// </summary>
        [DataNames("_cfdi_total")]
        [SugarColumn(ColumnName = "_cfdi_total")]
        public decimal Total {
            get {
                return Math.Round(this.totalField, 4);
            }
            set {
                this.totalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// monto cobrado o pagado del comprobante
        /// </summary>
        [DataNames("_cfdi_cbrd")]
        [SugarColumn(ColumnName = "_cfdi_cbrd")]
        public decimal Acumulado {
            get {
                return this.acumuladoField;
            }
            set {
                this.acumuladoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// importe del pago del comprobante de recepcion de pagos
        /// </summary>
        [DataNames("_cfdi_cbrdp")]
        [SugarColumn(ColumnName = "_cfdi_cbrdp")]
        public decimal ImportePagado {
            get {
                return this.importePagadoField;
            }
            set {
                this.importePagadoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_obsrv")]
        [SugarColumn(ColumnName = "_cfdi_obsrv")]
        public string Notas {
            get {
                return this.notasField;
            }
            set {
                this.notasField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// resultado de la descarga del comprobante
        /// </summary>
        [DataNames("_cfdi_rslt")]
        [SugarColumn(ColumnName = "_cfdi_rslt")]
        public string Result {
            get {
                return this.resultField;
            }
            set {
                this.resultField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_url_xml")]
        [SugarColumn(ColumnName = "_cfdi_url_xml")]
        public string FileXml {
            get {
                return this.fileXmlField;
            }
            set {
                this.fileXmlField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_url_pdf")]
        [SugarColumn(ColumnName = "_cfdi_url_pdf")]
        public string FilePdf {
            get {
                return this.filePdfField;
            }
            set {
                this.filePdfField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_url_acu")]
        [SugarColumn(ColumnName = "_cfdi_url_acu")]
        public string FileAccuse {
            get {
                return this.fileAccuseField;
            }
            set {
                this.fileAccuseField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_compl")]
        [SugarColumn(ColumnName = "_cfdi_compl")]
        public string JComplementos {
            get {
                return this.complementosField.Json();
            }
            set {
                this.complementosField = Complementos.Json(value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// complementos del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public Complementos Complementos {
            get {
                return this.complementosField;
            }
            set {
                this.complementosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la información de los comprobantes relacionados.
        /// </summary>
        [DataNames("_cfdi_comrel")]
        [SugarColumn(ColumnName = "_cfdi_comrel")]
        public string JCfdiRelacionados {
            get {
                if (this.cfdiRelacionadosField != null)
                    return this.cfdiRelacionadosField.Json();
                return null;
            }
            set {
                if (this.cfdiRelacionadosField == null) {
                    this.cfdiRelacionadosField = new ComprobanteCfdiRelacionados();
                }
                this.cfdiRelacionadosField = ComprobanteCfdiRelacionados.Json(value);
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public ComprobanteCfdiRelacionados CfdiRelacionados {
            get {
                return cfdiRelacionadosField;
            }
            set {
                this.cfdiRelacionadosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// complemento del timbre fiscal
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ComplementoTimbreFiscal TimbreFiscal {
            get {
                return this.timbreFiscalField;
            }
            set {
                this.timbreFiscalField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_nomina")]
        [SugarColumn(ColumnName = "_cfdi_nomina")]
        public string JNomina {
            get {
                if (this.nominaField != null)
                    return this.nominaField.Json();
                return null;
            }
            set {
                this.nominaField = ComplementoNomina.Json(value);
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public ComplementoNomina Nomina {
            get {
                return nominaField;
            }
            set {
                this.nominaField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_val")]
        [SugarColumn(ColumnName = "_cfdi_val")]
        public string JValidacion {
            get {
                if (this.Validacion != null)
                    return this.Validacion.Json();
                return null;
            }
            set {
                this.Validacion = ValidateResponse.Json(value);
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_pagos")]
        [SugarColumn(ColumnName = "_cfdi_pagos")]
        public string JComplementoPagos {
            get {
                if (this.pagosField != null)
                    return this.pagosField.Json();
                return null;
            }
            set {
                this.pagosField = ComplementoPagos.Json(value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer objeto del complemento de pagos
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ComplementoPagos ComplementoPagos {
            get {
                return this.pagosField;
            }
            set {
                this.pagosField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_acuse")]
        [SugarColumn(ColumnName = "_cfdi_acuse")]
        public string JAccuse {
            get {
                if (this.accuseField != null)
                    return this.accuseField.Xml();
                return null;
            }
            set {
                this.accuseField = CancelaCFDResponse.LoadXml(value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// acuse de cancelacion del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CancelaCFDResponse Accuse {
            get {
                return this.accuseField;
            }
            set {
                this.accuseField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_adenda")]
        [SugarColumn(ColumnName = "_cfdi_adenda")]
        public string JAddenda {
            get {
                if (this.addendasField != null)
                    return this.addendasField.Json();
                return null;
            }
            set {
                this.addendasField = Addendas.Json(value);
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_sync")]
        [SugarColumn(ColumnName = "_cfdi_sync")]
        public bool Sync {
            get {
                return this.syncronizadoField;
            }
            set {
                this.syncronizadoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_usr_n")]
        [SugarColumn(ColumnName = "_cfdi_usr_n")]
        public string Creo {
            get {
                return this.creoField;
            }
            set {
                if (this.creoField != value) {
                    this.creoField = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DataNames("_cfdi_usr_m")]
        [SugarColumn(ColumnName = "_cfdi_usr_m")]
        public string Modifica {
            get {
                return this.modificaField;
            }
            set {
                if (this.modificaField != value) {
                    this.modificaField = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DataNames("_cfdi_fn")]
        [SugarColumn(ColumnName = "_cfdi_fn")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevoField;
            }
            set {
                if (DateTime.Compare(this.fechaNuevoField, value) != 0) {
                    this.fechaNuevoField = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DataNames("_cfdi_fm")]
        [SugarColumn(ColumnName = "_cfdi_fm")]
        public DateTime? FechaMod {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModificaField >= firstGoodDate)
                    return this.fechaModificaField.Value;
                return null;
            }
            set {
                this.fechaModificaField = value;
            }
        }

        /// <summary>
        /// obtener o establecer el contenido del archivo XML
        /// </summary>
        [SugarColumn(ColumnName = "_cfdi_save")]
        [DataNames("_cfdi_save")]
        public string Xml {
            get {
                return this.originalXmlField;
            }
            set {
                this.originalXmlField = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region propiedades ignoradas

        /// <summary>
        /// obtiene si el comprobante es editable, si existe timbre fiscal entonces el comprobante no es editable
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public bool Editable {
            get {
                if (this.TimbreFiscal != null) {
                    if (this.TimbreFiscal.UUID != "") {
                        return false;
                    }
                }
                return true;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public ClaveMetodoPago MetodoPago {
            get {
                return this.claveMetodoPagoField;
            }
            set {
                this.claveMetodoPagoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public ClaveFormaPago FormaPago {
            get {
                return this.claveFormaPagoField;
            }
            set {
                this.claveFormaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public ClaveUsoCFDI UsoCfdi {
            get {
                return this.claveUsoCfdiField;
            }
            set {
                this.claveUsoCfdiField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public ViewModelContribuyente Emisor {
            get {
                return this.emisorField;
            }
            set {
                this.emisorField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public ViewModelContribuyente Receptor {
            get {
                return this.receptorField;
            }
            set {
                this.receptorField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public ClaveMoneda Moneda {
            get {
                return this.claveMonedaField;
            }
            set {
                this.claveMonedaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public EnumCfdiType TipoComprobante {
            get {
                return this.tipoComprobanteField;
            }
            set {
                this.tipoComprobanteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        [DataNames("_cfdi_doc_id")]
        [SugarColumn(ColumnName = "_cfdi_doc_id")]
        public EnumCfdiSubType SubTipo {
            get {
                return this.subTipoDeComprobanteField;
            }
            set {
                this.subTipoDeComprobanteField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_doc")]
        [SugarColumn(ColumnName = "_cfdi_doc", ColumnDescription = "documento (factura, nota de credito,nota de cargo)", Length = 36)]
        public string Documento {
            get {
                return this.documento;
            }
            set {
                this.documento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtner o establecer status interno del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string Status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// lista de conceptos del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<ViewModelComprobanteConcepto> Conceptos {
            get {
                return this.objConceptos;
            }
            set {
                if (this.objConceptos != null) {
                    this.objConceptos.AddingNew -= new AddingNewEventHandler(this.Conceptos_AddingNew);
                    this.objConceptos.ListChanged -= new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this.objConceptos = value;
                if (this.objConceptos != null) {
                    this.objConceptos.AddingNew += new AddingNewEventHandler(this.Conceptos_AddingNew);
                    this.objConceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// respuesta de validación del comprobante fiscal
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ValidateResponse Validacion {
            get {
                return this.validacionField;
            }
            set {
                this.validacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtiene ó establece objeto de addenda
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public Addendas Addendas {
            get {
                return this.addendasField;
            }
            set {
                this.addendasField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public bool ExiteCfdiRelacionado {
            get {
                if (this.CfdiRelacionados != null) {
                    return true;
                }
                return false;
            }
        }

        #endregion

        #region metodos privados

        private void Conceptos_AddingNew(object sender, AddingNewEventArgs e) {
            e.NewObject = new ViewModelComprobanteConcepto { SubId = this.Id };
        }

        private void Conceptos_ListChanged(object sender, ListChangedEventArgs e) {
            this.SubTotal = this.objConceptos.Where((ViewModelComprobanteConcepto p) => p.IsActive == true).Sum((ViewModelComprobanteConcepto p) => p.Importe);
            this.Descuento = this.objConceptos.Where((ViewModelComprobanteConcepto p) => p.IsActive == true).Sum((ViewModelComprobanteConcepto p) => p.Descuento);
            this.TrasladoIva = this.objConceptos.Where((ViewModelComprobanteConcepto p) => p.IsActive == true).Sum((ViewModelComprobanteConcepto p) => p.TrasladoIva);
            this.TrasladoIeps = this.objConceptos.Where((ViewModelComprobanteConcepto p) => p.IsActive == true).Sum((ViewModelComprobanteConcepto p) => p.Trasladoieps);
            this.RetencionIva = this.objConceptos.Where((ViewModelComprobanteConcepto p) => p.IsActive == true).Sum((ViewModelComprobanteConcepto p) => p.RetencionIva);
            this.RetencionIsr = this.objConceptos.Where((ViewModelComprobanteConcepto p) => p.IsActive == true).Sum((ViewModelComprobanteConcepto p) => p.RetencionIsr);
            this.Total = (this.SubTotal - this.Descuento) + (this.TrasladoIva + this.TrasladoIeps) - (this.RetencionIva + this.RetencionIsr);
        }

        #endregion

        #region metodos publicos

        public string KeyName() {
            string nombre = "CFDI-";
            if (this.TimbreFiscal != null) {
                nombre = String.Concat("CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.TimbreFiscal.UUID, "-", this.TimbreFiscal.FechaTimbrado.Value.ToString("yyyyMMddHHmmss"));
            }
            else {
                nombre = String.Concat("CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.Serie, "-", this.Folio, DateTime.Now.ToString("yyyyMMddHHmmss"));
            }
            return nombre;
        }

        /// <summary>
        /// establecer el contenido del comprobante con las opciones por default
        /// </summary>
        public void Default() {
            if (this.SubTipo == EnumCfdiSubType.Emitido) {
                this.FormaPago = new ClaveFormaPago();
                this.UsoCfdi = new ClaveUsoCFDI { Clave = "P01" };
                this.Moneda = new ClaveMoneda() { Clave = "MXN" };
                this.CondicionPago = null;

                ViewModelComprobanteConcepto o = new ViewModelComprobanteConcepto();
                if (this.TipoComprobante == EnumCfdiType.Nomina) {
                    this.FormaPago = new ClaveFormaPago() { Clave = "99" };
                    this.MetodoPago = new ClaveMetodoPago() { Clave = "PUE" };
                    o = new ViewModelComprobanteConcepto() { ClaveProdServ = "84111505", Cantidad = 1, ClaveUnidad = "ACT", Descripcion = "Pago de nómina", ValorUnitario = 0, Importe = 0 };
                }
                else if (this.TipoComprobante == EnumCfdiType.Pagos) {
                    this.presionDecimalField = 0;
                    this.Moneda = new ClaveMoneda() { Clave = "XXX", Descripcion = "Los códigos asignados para las transacciones en que intervenga ninguna moneda" };
                    this.MetodoPago = new ClaveMetodoPago() { Clave = null, Descripcion = null };
                    o = new ViewModelComprobanteConcepto() { ClaveProdServ = "84111506", Cantidad = 1, ClaveUnidad = "ACT", Descripcion = "Pago", ValorUnitario = 0, Importe = 0, PresionDecimal = 0 };
                    this.ComplementoPagos = new ComplementoPagos();
                    this.ComplementoPagos.Pago = new BindingList<ComplementoPagosPago>();
                    this.ComplementoPagos.Pago.AddNew();
                }
                else {
                    o = null;
                    this.objConceptos.Clear();
                }

                if (o != null) {
                    if (this.Search(o) == null) {
                        this.objConceptos.Clear();
                        this.Conceptos.Add(o);
                    }
                }
            }
        }

        /// <summary>
        /// buscar un objeto concepto dentro de la lista de conceptos del comprobante, retorna el objeto encontrado o nulo si no existe
        /// </summary>
        public ViewModelComprobanteConcepto Search(ViewModelComprobanteConcepto objeto) {
            ViewModelComprobanteConcepto search;
            if (this.objConceptos == null) {
                search = null;
            }
            else {
                objeto = this.objConceptos.FirstOrDefault<ViewModelComprobanteConcepto>((ViewModelComprobanteConcepto p) => p.ClaveProdServ == objeto.ClaveProdServ);
                search = objeto;
            }
            return search;
        }

        /// <summary>
        /// crear copia de la clase actual
        /// </summary>
        public void Clonar() {
            if (this.SubTipo == EnumCfdiSubType.Emitido) {
                // datos generales
                this.Id = 0;
                this.folioField = null;
                this.timbreFiscalField = null;
                this.FechaEmision = DateTime.Now;
                this.FilePdf = null;
                this.FileXml = null;
                this.Accuse = null;
                this.Estado = null;
                // eliminar el id relacionado en las partidas
                foreach (ViewModelComprobanteConcepto item in this.objConceptos) {
                    item.Id = 0;
                    item.SubId = 0;
                    item.Cantidad = item.Cantidad;
                    if (item.IsActive == false) {
                        this.objConceptos.Remove(item);
                    }
                }
            }
        }

        /// <summary>
        /// sustituir comprobante, se relaciona la información del comprobante actual
        /// </summary>
        public void Sustituir() {
            if (this.SubTipo == EnumCfdiSubType.Emitido) {
                this.CfdiRelacionados = new Jaeger.Edita.V2.CFDI.Entities.ComprobanteCfdiRelacionados();
                this.CfdiRelacionados.TipoRelacion = new Jaeger.Edita.V2.CFDI.Entities.Complemento.ComplementoDoctoRelacionadoRelacion { Clave = "04" };
                this.CfdiRelacionados.CfdiRelacionado.Add(new Jaeger.Edita.V2.CFDI.Entities.ComprobanteCfdiRelacionadosCfdiRelacionado { Folio = this.Folio, Serie = this.Serie, Total = this.Total, Nombre = this.Receptor.Nombre, RFC = this.ReceptorRFC, IdDocumento = this.TimbreFiscal.UUID });
                this.Clonar();
            }
        }

        public void OtraVez() {
            if (this.SubTipo == EnumCfdiSubType.Emitido) {
                if (this.objConceptos != null) {
                    // actualizar partidas
                    foreach (ViewModelComprobanteConcepto item in this.objConceptos) {
                        item.Cantidad = item.Cantidad;
                        if (item.IsActive == false) {
                            this.objConceptos.Remove(item);
                        }
                    }
                }
            }
        }
        #endregion
    }
}
