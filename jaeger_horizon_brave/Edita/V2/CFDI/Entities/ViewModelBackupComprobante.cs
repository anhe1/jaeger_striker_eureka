﻿using System;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Domain.Base.Abstractions;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.CFDI.Entities
{
    public class ViewModelBackupComprobante : BasePropertyChangeImplementation
    {
        private long indexField;
        private EnumCfdiSubType subTipoField;
        private string keyNameField;
        private string idComprobanteField;
        private DateTime fechaField;
        private string nombreField;
        private string rfcField;
        private int ejercicioField;
        private int periodoField;
        private string urlXmlField;
        private string urlPdfField;
        private string urlAccuseField;
        private string urlPdfAcuseField;
        private string categoriaField;
        private string nominaField;

        public ViewModelBackupComprobante()
        {
        }

        [JsonIgnore]
        public long Id
        {
            get
            {
                return this.indexField;
            }
            set
            {
                this.indexField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public EnumCfdiSubType SubTipo
        {
            get
            {
                return this.subTipoField;
            }
            set
            {
                this.subTipoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("subTipo")]
        public string SubTipoText
        {
            get
            {
                return Enum.GetName(typeof(EnumCfdiSubType), this.subTipoField);
            }
            set
            {
                this.subTipoField = (EnumCfdiSubType)Enum.Parse(typeof(EnumCfdiSubType), value);
            }
        }

        [JsonProperty("keyname")]
        public string KeyName
        {
            get
            {
                return this.keyNameField;
            }
            set
            {
                this.keyNameField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("uuid")]
        public string IdComprobante
        {
            get
            {
                return this.idComprobanteField;
            }
            set
            {
                this.idComprobanteField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("fecha")]
        public DateTime Fecha
        {
            get
            {
                return this.fechaField;
            }
            set
            {
                this.fechaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("nombre")]
        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("rfc")]
        public string RFC
        {
            get
            {
                return this.rfcField;
            }
            set
            {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("ejercicio")]
        public int Ejercicio
        {
            get
            {
                return this.ejercicioField;
            }
            set
            {
                this.ejercicioField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("periodo")]
        public int Periodo
        {
            get
            {
                return this.periodoField;
            }
            set
            {
                this.periodoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("xml")]
        public string UrlXml
        {
            get
            {
                return this.urlXmlField;
            }
            set
            {
                this.urlXmlField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("pdf")]
        public string UrlPdf
        {
            get
            {
                return this.urlPdfField;
            }
            set
            {
                this.urlPdfField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("accuse")]
        public string UrlAccuse
        {
            get
            {
                return this.urlAccuseField;
            }
            set
            {
                this.urlAccuseField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("accusePDF")]
        public string UrlPdfAcuse
        {
            get
            {
                return this.urlPdfAcuseField;
            }
            set
            {
                this.urlPdfAcuseField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("cat")]
        public string Categoria
        {
            get
            {
                return this.categoriaField;
            }
            set
            {
                this.categoriaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("nomina")]
        public string Nomina
        {
            get
            {
                return this.nominaField;
            }
            set
            {
                this.nominaField = value;
                this.OnPropertyChanged();
            }
        }

        public string _cfdi_save {
            get;set;
        }
    }
}
