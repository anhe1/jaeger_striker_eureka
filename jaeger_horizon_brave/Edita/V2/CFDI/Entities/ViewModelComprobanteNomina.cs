﻿using System;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.CFDI.Entities
{
    public class ViewModelComprobanteNomina
    {
        [DataNames("_cfdnmn_id")]
        public int Id { get; set; }
        [DataNames("_cfdnmn_a")]
        public bool Activo { get; set; }
        [DataNames("_cfdnmn_drctr_id")]
        public int IdDirectorio { get; set; }
        [DataNames("_cfdnmn_tpreg")]
        public string TipoRegimen { get; set; }
        //[DataNames("_cfdnmn_rsgpst")]
        [DataNames("_cfdnmn_qnc")]
        public int Quincena  { get; set; }
        [DataNames("_cfdnmn_anio")]
        public int Anio { get; set; }
        [DataNames("_cfdnmn_antgdd2")]
        public string Antiguedad { get; set; }
        [DataNames("_cfdnmn_dspgds")]
        public int DiasPagados { get; set; }
        //[DataNames("_cfdnmn_bsctap")]
        [DataNames("_cfdnmn_drintg")]
        public decimal DiarioIntegrado { get; set; }
        //[DataNames("_cfdnmn_pttlgrv")]
        //[DataNames("_cfdnmn_pttlexn")]
        //[DataNames("_cfdnmn_dttlgrv")]
        //[DataNames("_cfdnmn_dttlexn")]
        [DataNames("_cfdnmn_dscnt")]
        public decimal Descuento { get; set; }
        [DataNames("_cfdnmn_hrsxtr")]
        public decimal HorasExtra { get; set; }
        [DataNames("_cfdnmn_ver")]
        public string Version { get; set; }
    [DataNames("_cfdnmn_banco")]
        public string Banco { get; set; }
    [DataNames("_cfdnmn_rgp")]
        public string RiesgoPuesto { get; set; }
    [DataNames("_cfdnmn_numem")]
    public string NumEmpleado { get; set; }
[DataNames("_cfdnmn_curp")]
public string CURP { get; set; }
        //[DataNames("_cfdnmn_numsgr")]
        //[DataNames("_cfdnmn_clabe")]
        //[DataNames("_cfdnmn_tipo")]
        //[DataNames("_cfdnmn_uuid")]
        //[DataNames("_cfdnmn_depto")]
        //[DataNames("_cfdnmn_puesto")]
        //[DataNames("_cfdnmn_tpcntr")]
        //[DataNames("_cfdnmn_tpjrn")]
        //[DataNames("_cfdnmn_prddpg")]
        //[DataNames("_cfdnmn_fchpgo")]
        //[DataNames("_cfdnmn_fchini")]
        //[DataNames("_cfdnmn_fchfnl")]
        //[DataNames("_cfdnmn_fchrel")]
        [DataNames("_cfdnmn_fn")]
        public DateTime FechaNuevo { get; set; }
    [DataNames("_cfdnmn_fm")]
        public DateTime FechaModifica { get; set; }
    [DataNames("_cfdnmn_usr_n")]
        public string Creo { get; set; }
    [DataNames("_cfdnmn_usr_m")]
        public string Modifica { get; set; }
        [DataNames("_cfdi_nomr")]
        public string Receptor { get; set; }
    [DataNames("_cfdi_rfce")]
        public string RFCe  { get; set; }
    [DataNames("_cfdnmn_rfc")]
        public string RFCr { get; set; }
    [DataNames("_cfdi_total")]
        public decimal Total { get; set; }
    [DataNames("_cfdi_estado")]
        public  string EstadoSAT{ get; set; }
    [DataNames("_cfdi_fecedo")]
        public DateTime FechaEstado { get; set; }
    [DataNames("_cfdi_feccert")]
        public DateTime FechaCertifica { get; set; }
    [DataNames("_cfdi_id")]
        public int IdCFDI { get; set; }
        [DataNames(" _cfdi_uuid")]
        public string IdDocumento { get; set; }
    }
}
