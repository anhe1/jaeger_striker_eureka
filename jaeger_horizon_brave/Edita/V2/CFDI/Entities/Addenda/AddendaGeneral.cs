﻿/// objeto general de addenda
using System;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Jaeger.CFDI.Entities.Addenda
{
    public partial class AddendaGeneralParametro
    {
        private string campoField;
        private string etiquetaField;
        private string tipoField;
        private string valorField;

        public AddendaGeneralParametro()
        {
        }

        [JsonProperty("field")]
        public string Campo
        {
            get
            {
                return this.campoField;
            }
            set
            {
                this.campoField = value;
            }
        }

        [JsonProperty("label")]
        public string Etiqueta
        {
            get
            {
                return this.etiquetaField;
            }
            set
            {
                this.etiquetaField = value;
            }
        }

        [JsonProperty("type")]
        public string Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
            }
        }

        [JsonProperty("value")]
        public string Valor
        {
            get
            {
                return this.valorField;
            }
            set
            {
                this.valorField = value;
            }
        }
    }
}

namespace Jaeger.CFDI.Entities.Addenda
{
    [JsonObject]
    public partial class AddendaGeneral
    {
        private string nombreField;
        private List<AddendaGeneralParametro> parametrosField;
        
        public AddendaGeneral()
        {
        }

        [JsonProperty("name")]
        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
            }
        }

        [JsonProperty("params")]
        public List<AddendaGeneralParametro> ParametrosField
        {
            get
            {
                return this.parametrosField;
            }
            set
            {
                this.parametrosField = value;
            }
        }
    }
}
