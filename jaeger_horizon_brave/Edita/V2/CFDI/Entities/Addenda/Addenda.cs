﻿using System;
using System.Linq;
using Jaeger.Edita.V2.CFDI.Enums;
using Newtonsoft.Json;

namespace Jaeger.CFDI.Entities.Addenda
{
    public class Addenda : Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private EnumCfdiAddendas nombreField;
        private string dataField;

        /// <summary>
        ///  constructor
        /// </summary>
        public Addenda()
        {
        }

        #region propiedades

        /// <summary>
        /// nombre del objeto
        /// </summary>
        [JsonProperty("nombre", Order=1)]
        public EnumCfdiAddendas Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// representa objeto json
        /// </summary>
        [JsonProperty("data", Order=2)]
        public string Data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
