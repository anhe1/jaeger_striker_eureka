﻿using Newtonsoft.Json;
using System.Diagnostics;

namespace Jaeger.CFDI.Entities.Addenda
{
    public class Parametros : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string fieldField;
        private string labelField;
        private string typeField;
        private string valueField;

        [JsonProperty("field")]
        public string Field
        {
            get
            {
                return this.fieldField;
            }
            set
            {
                this.fieldField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("label")]
        public string Label
        {
            get
            {
                return this.labelField;
            }
            set
            {
                this.labelField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("type")]
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("value")]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
                this.OnPropertyChanged();
            }
        }

        [DebuggerNonUserCode]
        public Parametros()
        {
        }
    }
}