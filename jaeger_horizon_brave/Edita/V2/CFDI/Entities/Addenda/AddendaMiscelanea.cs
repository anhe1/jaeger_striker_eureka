﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Jaeger.CFDI.Entities.Addenda
{
    [JsonObject("AfasaMiscelanea")]
    [XmlRoot("AfasaMiscelanea")]
    public class AddendaMiscelanea : Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string strMarca;

        private string strTipo;

        private string strColor;

        private string strUsado;

        private string strNumMotor;

        private string strChasis;

        private string strModelo;

        private string strProcedencia;

        private string strClaveVehicular;

        private string strNumPedimento;

        private string strFechaDeImportacion;

        private string strNombreDeAduana;

        private static XmlSerializer objSerializer;

        [JsonProperty("chas")]
        [XmlAttribute("Chasis")]
        public string Chasis
        {
            get
            {
                return this.strChasis;
            }
            set
            {
                this.strChasis = value;
                this.OnPropertyChanged("Chasis");
            }
        }

        [JsonProperty("clav")]
        [XmlAttribute("ClaveVehicular")]
        public string ClaveVehicular
        {
            get
            {
                return this.strClaveVehicular;
            }
            set
            {
                this.strClaveVehicular = value;
                this.OnPropertyChanged("ClaveVehicular");
            }
        }

        [JsonProperty("colo")]
        [XmlAttribute("Color")]
        public string Color
        {
            get
            {
                return this.strColor;
            }
            set
            {
                this.strColor = value;
                this.OnPropertyChanged("Color");
            }
        }

        [JsonProperty("fimp")]
        [XmlAttribute("FechaImportacion")]
        public string FechaImportacion
        {
            get
            {
                return this.strFechaDeImportacion;
            }
            set
            {
                this.strFechaDeImportacion = value;
                this.OnPropertyChanged("FechaImportacion");
            }
        }

        [JsonProperty("marc")]
        [XmlAttribute("Marca")]
        public string Marca
        {
            get
            {
                return this.strMarca;
            }
            set
            {
                this.strMarca = value;
                this.OnPropertyChanged("Marca");
            }
        }

        [JsonProperty("mode")]
        [XmlAttribute("Modelo")]
        public string Modelo
        {
            get
            {
                return this.strModelo;
            }
            set
            {
                this.strModelo = value;
                this.OnPropertyChanged("Modelo");
            }
        }

        [JsonProperty("nadu")]
        [XmlAttribute("NombreAduana")]
        public string NombreAduana
        {
            get
            {
                return this.strNombreDeAduana;
            }
            set
            {
                this.strNombreDeAduana = value;
                this.OnPropertyChanged("NombreAduana");
            }
        }

        [JsonProperty("anti")]
        [XmlAttribute("Usado")]
        public string NuevoUsado
        {
            get
            {
                return this.strUsado;
            }
            set
            {
                this.strUsado = value;
                this.OnPropertyChanged("NuevoUsado");
            }
        }

        [JsonProperty("nomo")]
        [XmlAttribute("NumMotor")]
        public string NumMotor
        {
            get
            {
                return this.strNumMotor;
            }
            set
            {
                this.strNumMotor = value;
                this.OnPropertyChanged("NumMotor");
            }
        }

        [JsonProperty("nope")]
        [XmlAttribute("NumPedimento")]
        public string NumPedimento
        {
            get
            {
                return this.strNumPedimento;
            }
            set
            {
                this.strNumPedimento = value;
                this.OnPropertyChanged("NumPedimento");
            }
        }

        [JsonProperty("proc")]
        [XmlAttribute("Procedencia")]
        public string Procedencia
        {
            get
            {
                return this.strProcedencia;
            }
            set
            {
                this.strProcedencia = value;
                this.OnPropertyChanged("Procedencia");
            }
        }

        private static XmlSerializer Serializer
        {
            get
            {
                if (AddendaMiscelanea.objSerializer == null)
                {
                    AddendaMiscelanea.objSerializer = (new XmlSerializerFactory()).CreateSerializer(typeof(AddendaMiscelanea));
                }
                return AddendaMiscelanea.objSerializer;
            }
        }

        [JsonProperty("tipo")]
        [XmlAttribute("Tipo")]
        public string Tipo
        {
            get
            {
                return this.strTipo;
            }
            set
            {
                this.strTipo = value;
                this.OnPropertyChanged();
            }
        }

        public AddendaMiscelanea()
        {
            this.strMarca = string.Empty;
            this.strTipo = string.Empty;
            this.strColor = string.Empty;
            this.strUsado = string.Empty;
            this.strNumMotor = string.Empty;
            this.strChasis = string.Empty;
            this.strModelo = string.Empty;
            this.strProcedencia = string.Empty;
            this.strClaveVehicular = string.Empty;
            this.strNumPedimento = string.Empty;
            this.strFechaDeImportacion = string.Empty;
            this.strNombreDeAduana = string.Empty;
        }

        public static bool Deserialize(string input, out AddendaMiscelanea obj, out Exception exception)
        {
            bool flag;
            exception = null;
            obj = null;
            try
            {
                obj = AddendaMiscelanea.Deserialize(input);
                flag = true;
            }
            catch (Exception exception1)
            {
                exception = exception1;
                flag = false;
            }
            return flag;
        }

        public static bool Deserialize(string input, out AddendaMiscelanea obj)
        {
            Exception exception = new Exception();
            return AddendaMiscelanea.Deserialize(input, out obj, out exception);
        }

        public static AddendaMiscelanea Deserialize(string input)
        {
            StringReader objStringReader = new StringReader(input);
            AddendaMiscelanea objPagos = (AddendaMiscelanea)AddendaMiscelanea.Serializer.Deserialize(XmlReader.Create(objStringReader));
            return objPagos;
        }

        public static AddendaMiscelanea Deserialize(Stream s)
        {
            return (AddendaMiscelanea)AddendaMiscelanea.Serializer.Deserialize(s);
        }

        public string Json(Newtonsoft.Json.Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this);
        }

        public static AddendaMiscelanea Json(string inputJson)
        {
            AddendaMiscelanea json;
            try
            {
                json = JsonConvert.DeserializeObject<AddendaMiscelanea>(inputJson);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                json = null;
            }
            return json;
        }

        public virtual string Serialize()
        {
            string end;
            StreamReader streamReader = null;
            MemoryStream memoryStream = null;
            try
            {
                memoryStream = new MemoryStream();
                XmlWriterSettings xmlWriterSetting = new XmlWriterSettings()
                {
                    Encoding = Encoding.UTF8,
                    Indent = true
                };
                XmlWriter xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSetting);
                XmlSerializerNamespaces xmlSerializerNamespace = new XmlSerializerNamespaces();
                AddendaMiscelanea.Serializer.Serialize(xmlWriter, this, xmlSerializerNamespace);
                memoryStream.Seek((long)0, SeekOrigin.Begin);
                streamReader = new StreamReader(memoryStream, Encoding.UTF8);
                end = streamReader.ReadToEnd();
            }
            finally
            {
                if (streamReader != null)
                {
                    streamReader.Dispose();
                }
                if (memoryStream != null)
                {
                    memoryStream.Dispose();
                }
            }
            return end;
        }
    }
}