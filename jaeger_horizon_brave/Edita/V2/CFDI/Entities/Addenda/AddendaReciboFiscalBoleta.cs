﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace Jaeger.CFDI.Entities.Addenda
{
    [JsonObject("boleta")]
    [XmlRoot("Boleta")]
    public class AddendaReciboFiscalBoleta : Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string fechaPagoField;

        private string strConsecutivo;

        private string strMensualidad;

        private string strOtro;

        private string strCuotaAuto;

        private string strCuotaSeguro;

        private string strCuotaGastos;

        private string strIva;

        private string strSeguro;

        private string strTotal;

        /// <summary>
        /// Consecutivo
        /// </summary>
        [DisplayName("Consecutivo")]
        [JsonProperty("cons")]
        [XmlAttribute("Consecutivo")]
        public string Consecutivo
        {
            get
            {
                return this.strConsecutivo;
            }
            set
            {
                this.strConsecutivo = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Cuota del Automóvil")]
        [JsonProperty("cuat")]
        [XmlAttribute("CuotaAutomovil")]
        public string CuotaAuto
        {
            get
            {
                return this.strCuotaAuto;
            }
            set
            {
                this.strCuotaAuto = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Cuota Gastos Admon")]
        [JsonProperty("cuga")]
        [XmlAttribute("CuotaGastosAdmon")]
        public string CuotaGastos
        {
            get
            {
                return this.strCuotaGastos;
            }
            set
            {
                this.strCuotaGastos = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Seguro de Vida
        /// </summary>
        [DisplayName("Cuota del Seguro de Vida")]
        [JsonProperty("cuse")]
        [XmlAttribute("CuotaSeguroVida")]
        public string CuotaSeguroVida
        {
            get
            {
                return this.strCuotaSeguro;
            }
            set
            {
                this.strCuotaSeguro = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Fecha")]
        [JsonProperty("fecha")]
        [XmlAttribute("Fecha")]
        public string FechaPago
        {
            get
            {
                return this.fechaPagoField;
            }
            set
            {
                this.fechaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Iva
        /// </summary>
        [DisplayName("IVA")]
        [JsonProperty("iva")]
        [XmlAttribute("IVA")]
        public string Iva
        {
            get
            {
                return this.strIva;
            }
            set
            {
                this.strIva = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Mensualidad")]
        [JsonProperty("mens")]
        [XmlAttribute("Mensualidad")]
        public string Mensualidad
        {
            get
            {
                return this.strMensualidad;
            }
            set
            {
                this.strMensualidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Otro
        /// </summary>
        [DisplayName("Otro")]
        [JsonProperty("otro")]
        [XmlAttribute("Otro")]
        public string Otro
        {
            get
            {
                return this.strOtro;
            }
            set
            {
                this.strOtro = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Seguro Auto")]
        [JsonProperty("segu")]
        [XmlAttribute("CuotaSeguroAutomovil")]
        public string Seguro
        {
            get
            {
                return this.strSeguro;
            }
            set
            {
                this.strSeguro = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Total
        /// </summary>
        [DisplayName("Total Pagado")]
        [JsonProperty("tota")]
        [XmlAttribute("Total")]
        public string Total
        {
            get
            {
                return this.strTotal;
            }
            set
            {
                this.strTotal = value;
                this.OnPropertyChanged();
            }
        }

        public AddendaReciboFiscalBoleta()
        {
            this.fechaPagoField = string.Empty;
            this.strConsecutivo = string.Empty;
            this.strMensualidad = string.Empty;
            this.strOtro = string.Empty;
            this.strCuotaAuto = string.Empty;
            this.strCuotaSeguro = string.Empty;
            this.strCuotaGastos = string.Empty;
            this.strIva = string.Empty;
            this.strSeguro = string.Empty;
            this.strTotal = string.Empty;
        }

        public string Json()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static AddendaReciboFiscalBoleta Json(string jsonString)
        {
            AddendaReciboFiscalBoleta json;
            try
            {
                json = JsonConvert.DeserializeObject<AddendaReciboFiscalBoleta>(jsonString);
            }
            catch (Exception e)
            {
                Console.WriteLine("Addenda: " + e.Message);
                json = null;
            }
            return json;
        }

        public string Xml()
        {
            return Jaeger.Helpers.HelperSerializer.SerializeObject<AddendaReciboFiscalBoleta>(this);
        }

        public static AddendaReciboFiscalBoleta Xml(string xmlString)
        {
            return Jaeger.Helpers.HelperSerializer.DeserializeObject<AddendaReciboFiscalBoleta>(xmlString);
        }
    }
}