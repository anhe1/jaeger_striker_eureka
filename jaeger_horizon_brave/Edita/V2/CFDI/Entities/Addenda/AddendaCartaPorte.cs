﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Jaeger.CFDI.Entities.Addenda
{
    [JsonObject]
    [XmlRoot]
    public class AddendaCartaPorte : Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string remitenteField;

        private string remitenteRfcField;

        private string remitenteDomicilioField;

        private string recogerEnField;

        private string destinatarioField;

        private string destinatarioDomicilioField;

        private string entregarEnField;

        private string fechaOPlazoField;

        private double valorDeclaradoField;

        private string materialResiduoPeligrosoField;

        private BindingList<CartaPorteConcepto> conceptosField;

        private static XmlSerializer objSerializer;

        [JsonProperty("concepto")]
        [XmlElement("Concepto")]
        public BindingList<CartaPorteConcepto> Conceptos
        {
            get
            {
                return this.conceptosField;
            }
            set
            {
                this.conceptosField = value;
                this.OnPropertyChanged("Conceptos");
            }
        }

        [JsonProperty("destinatario")]
        [XmlAttribute("Destinatario")]
        public string Destinatario
        {
            get
            {
                return this.destinatarioField;
            }
            set
            {
                this.destinatarioField = value;
                this.OnPropertyChanged("Destinatario");
            }
        }

        [JsonProperty("domDestinatario")]
        [XmlAttribute("DomDestinatario")]
        public string DestinatarioDomicilio
        {
            get
            {
                return this.destinatarioDomicilioField;
            }
            set
            {
                this.destinatarioDomicilioField = value;
                this.OnPropertyChanged("DestinatarioDomicilio");
            }
        }

        [JsonProperty("entregarEn")]
        [XmlAttribute("EntregarEn")]
        public string EntregarEn
        {
            get
            {
                return this.entregarEnField;
            }
            set
            {
                this.entregarEnField = value;
                this.OnPropertyChanged("EntregarEn");
            }
        }

        [JsonProperty("fechaOPlazo")]
        [XmlAttribute("FechaOPlazo")]
        public string FechaOPlazo
        {
            get
            {
                return this.fechaOPlazoField;
            }
            set
            {
                this.fechaOPlazoField = value;
                this.OnPropertyChanged("FechaOPlazo");
            }
        }

        [JsonProperty("mrPeligroso")]
        [XmlAttribute("mrPeligroso")]
        public string MaterialResiduoPeligroso
        {
            get
            {
                return this.materialResiduoPeligrosoField;
            }
            set
            {
                this.materialResiduoPeligrosoField = value;
                this.OnPropertyChanged("MaterialResiduoPeligroso");
            }
        }

        [JsonProperty("recogerEn")]
        [XmlAttribute("RecogerEn")]
        public string RecogerEn
        {
            get
            {
                return this.recogerEnField;
            }
            set
            {
                this.recogerEnField = value;
                this.OnPropertyChanged("RecogerEn");
            }
        }

        [JsonProperty("remitente")]
        [XmlAttribute("Remitente")]
        public string Remitente
        {
            get
            {
                return this.remitenteField;
            }
            set
            {
                this.remitenteField = value;
                this.OnPropertyChanged("Remitente");
            }
        }

        [JsonProperty("domRemitente")]
        [XmlAttribute("DomRemitente")]
        public string RemitenteDomicilio
        {
            get
            {
                return this.remitenteDomicilioField;
            }
            set
            {
                this.remitenteDomicilioField = value;
                this.OnPropertyChanged("RemitenteDomicilio");
            }
        }

        [JsonProperty("remitenteRfc")]
        [XmlAttribute("RemitenteRfc")]
        public string RemitenteRfc
        {
            get
            {
                return this.remitenteRfcField;
            }
            set
            {
                this.remitenteRfcField = value;
                this.OnPropertyChanged("RemitenteRfc");
            }
        }

        private static XmlSerializer Serializer
        {
            get
            {
                if (AddendaCartaPorte.objSerializer == null)
                {
                    AddendaCartaPorte.objSerializer = (new XmlSerializerFactory()).CreateSerializer(typeof(AddendaCartaPorte));
                }
                return AddendaCartaPorte.objSerializer;
            }
        }

        [JsonProperty("valorDeclarado")]
        [XmlAttribute("ValorDeclarado")]
        public double ValorDeclarado
        {
            get
            {
                return this.valorDeclaradoField;
            }
            set
            {
                this.valorDeclaradoField = value;
                this.OnPropertyChanged("ValorDeclarado");
            }
        }

        public AddendaCartaPorte()
        {
            this.conceptosField = new BindingList<CartaPorteConcepto>();
        }

        public static bool Deserialize(string input, out AddendaCartaPorte obj, out Exception exception)
        {
            bool flag;
            exception = null;
            obj = null;
            try
            {
                obj = AddendaCartaPorte.Deserialize(input);
                flag = true;
            }
            catch (Exception exception1)
            {
                exception = exception1;
                flag = false;
            }
            return flag;
        }

        public static bool Deserialize(string input, out AddendaCartaPorte obj)
        {
            Exception exception = new Exception();
            return AddendaCartaPorte.Deserialize(input, out obj, out exception);
        }

        public static AddendaCartaPorte Deserialize(string input)
        {
            StringReader objStringReader = new StringReader(input);
            AddendaCartaPorte objPagos = (AddendaCartaPorte)AddendaCartaPorte.Serializer.Deserialize(XmlReader.Create(objStringReader));
            return objPagos;
        }

        public static AddendaCartaPorte Deserialize(Stream s)
        {
            return (AddendaCartaPorte)AddendaCartaPorte.Serializer.Deserialize(s);
        }

        public string Json(Newtonsoft.Json.Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this);
        }

        public static AddendaCartaPorte Json(string inputJson)
        {
            AddendaCartaPorte j;
            try
            {
                j = JsonConvert.DeserializeObject<AddendaCartaPorte>(inputJson);
            }
            catch (Exception e)
            {
                Console.WriteLine("Addenda: " + e.Message);
                j = null;
            }
            return j;
        }

        public virtual string Serialize()
        {
            string end;
            StreamReader streamReader = null;
            MemoryStream memoryStream = null;
            try
            {
                memoryStream = new MemoryStream();
                XmlWriterSettings xmlWriterSetting = new XmlWriterSettings()
                {
                    Encoding = Encoding.UTF8,
                    Indent = true
                };
                XmlWriter xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSetting);
                XmlSerializerNamespaces xmlSerializerNamespace = new XmlSerializerNamespaces();
                AddendaCartaPorte.Serializer.Serialize(xmlWriter, this, xmlSerializerNamespace);
                memoryStream.Seek((long)0, SeekOrigin.Begin);
                streamReader = new StreamReader(memoryStream, Encoding.UTF8);
                end = streamReader.ReadToEnd();
            }
            finally
            {
                if (streamReader != null)
                {
                    streamReader.Dispose();
                }
                if (memoryStream != null)
                {
                    memoryStream.Dispose();
                }
            }
            return end;
        }
    }
}