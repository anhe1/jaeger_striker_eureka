﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Jaeger.CFDI.Entities.Addenda
{
    [JsonObject("reciboFiscal")]
    [XmlRoot("ReciboFiscal")]
    public class AddendaReciboFiscal : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private BindingList<AddendaReciboFiscalBoleta> itemsField;

        private static XmlSerializer objSerializer;

        [JsonProperty("boleta")]
        [XmlElement("Boleta")]
        public BindingList<AddendaReciboFiscalBoleta> Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        private static XmlSerializer Serializer
        {
            get
            {
                if (AddendaReciboFiscal.objSerializer == null)
                {
                    AddendaReciboFiscal.objSerializer = (new XmlSerializerFactory()).CreateSerializer(typeof(AddendaReciboFiscal));
                }
                return AddendaReciboFiscal.objSerializer;
            }
        }

        public AddendaReciboFiscal()
        {
            this.itemsField = new BindingList<AddendaReciboFiscalBoleta>();
        }

        public static bool Deserialize(string input, out AddendaReciboFiscal obj, out Exception exception)
        {
            bool flag;
            exception = null;
            obj = null;
            try
            {
                obj = AddendaReciboFiscal.Deserialize(input);
                flag = true;
            }
            catch (Exception exception1)
            {
                exception = exception1;
                flag = false;
            }
            return flag;
        }

        public static bool Deserialize(string input, out AddendaReciboFiscal obj)
        {
            Exception exception = new Exception();
            return AddendaReciboFiscal.Deserialize(input, out obj, out exception);
        }

        public static AddendaReciboFiscal Deserialize(string input)
        {
            StringReader objStringReader = new StringReader(input);
            AddendaReciboFiscal objPagos = (AddendaReciboFiscal)AddendaReciboFiscal.Serializer.Deserialize(XmlReader.Create(objStringReader));
            return objPagos;
        }

        public static AddendaReciboFiscal Deserialize(Stream s)
        {
            return (AddendaReciboFiscal)AddendaReciboFiscal.Serializer.Deserialize(s);
        }

        public string Json()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static AddendaReciboFiscal Json(string jsonString)
        {
            AddendaReciboFiscal json;
            try
            {
                json = JsonConvert.DeserializeObject<AddendaReciboFiscal>(jsonString);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                json = null;
            }
            return json;
        }

        public virtual string Serialize()
        {
            string end;
            StreamReader streamReader = null;
            MemoryStream memoryStream = null;
            try
            {
                memoryStream = new MemoryStream();
                XmlWriterSettings xmlWriterSetting = new XmlWriterSettings()
                {
                    Encoding = Encoding.UTF8,
                    Indent = true
                };
                XmlWriter xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSetting);
                XmlSerializerNamespaces xmlSerializerNamespace = new XmlSerializerNamespaces();
                AddendaReciboFiscal.Serializer.Serialize(xmlWriter, this, xmlSerializerNamespace);
                memoryStream.Seek((long)0, SeekOrigin.Begin);
                streamReader = new StreamReader(memoryStream, Encoding.UTF8);
                end = streamReader.ReadToEnd();
            }
            finally
            {
                if (streamReader != null)
                {
                    streamReader.Dispose();
                }
                if (memoryStream != null)
                {
                    memoryStream.Dispose();
                }
            }
            return end;
        }
    }
}