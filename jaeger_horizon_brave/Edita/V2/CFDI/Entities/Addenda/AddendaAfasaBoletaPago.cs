﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Jaeger.CFDI.Entities.Addenda
{
    [JsonObject]
    public class AddendaAfasaBoletaPago : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string nameField;

        private List<Parametros> listaField;

        [JsonProperty("params")]
        public List<Parametros> Items
        {
            get
            {
                return this.listaField;
            }
            set
            {
                this.listaField = value;
            }
        }

        [JsonProperty("name")]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
                this.OnPropertyChanged("Name");
            }
        }

        [DebuggerNonUserCode]
        public AddendaAfasaBoletaPago()
        {
        }

        public string Json()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static AddendaAfasaBoletaPago Json(string jsonString)
        {
            AddendaAfasaBoletaPago json;
            try
            {
                json = JsonConvert.DeserializeObject<AddendaAfasaBoletaPago>(jsonString);
            }
            catch (Exception e)
            {
                Console.WriteLine("Addenda: " + e.Message);
                json = null;
            }
            return json;
        }
    }
}