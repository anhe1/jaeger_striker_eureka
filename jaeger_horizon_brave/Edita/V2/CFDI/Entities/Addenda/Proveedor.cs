﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.Serialization;

namespace Jaeger.CFDI.Entities.Addenda
{
    /// <remarks />
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [GeneratedCode("xsd", "4.0.30319.1")]
    [Serializable]
    [XmlRoot(Namespace = "https://edixcfdisecure.ekomercio.com/esquemas", IsNullable = false)]
    [XmlType(AnonymousType = true, Namespace = "https://edixcfdisecure.ekomercio.com/esquemas")]
    public class Proveedor
    {
        private XmlElement[] anyField;

        private string tipoAddendaField;

        /// <remarks />
        [XmlAnyElement]
        public XmlElement[] Any
        {
            get
            {
                return this.anyField;
            }
            set
            {
                this.anyField = value;
            }
        }

        [XmlAttribute]
        public string tipoAddenda
        {
            get
            {
                return this.tipoAddendaField;
            }
            set
            {
                this.tipoAddendaField = value;
            }
        }

        [DebuggerNonUserCode]
        public Proveedor()
        {
        }
    }
}