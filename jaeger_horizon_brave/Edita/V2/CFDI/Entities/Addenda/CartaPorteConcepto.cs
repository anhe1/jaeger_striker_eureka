﻿using Newtonsoft.Json;
using System;
using System.Xml.Serialization;

namespace Jaeger.CFDI.Entities.Addenda
{
    [JsonObject("concepto")]
    [XmlRoot("concepto")]
    public class CartaPorteConcepto : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string descripcionField;

        private string pesoField;

        private string litrosField;

        private string metrosCubicosField;

        [JsonProperty("desc")]
        [XmlAttribute("Desc")]
        public string Descripcion
        {
            get
            {
                return this.descripcionField;
            }
            set
            {
                this.descripcionField = value;
                this.OnPropertyChanged("Descripcion");
            }
        }

        [JsonProperty("litros")]
        [XmlAttribute("Litros")]
        public string Litros
        {
            get
            {
                return this.litrosField;
            }
            set
            {
                this.litrosField = value;
                this.OnPropertyChanged("Litros");
            }
        }

        [JsonProperty("metros3")]
        [XmlAttribute("Metros3")]
        public string MetrosCubicos
        {
            get
            {
                return this.metrosCubicosField;
            }
            set
            {
                this.metrosCubicosField = value;
            }
        }

        [JsonProperty("peso")]
        [XmlAttribute("Peso")]
        public string Peso
        {
            get
            {
                return this.pesoField;
            }
            set
            {
                this.pesoField = value;
                this.OnPropertyChanged("Peso");
            }
        }

        public CartaPorteConcepto()
        {
        }
    }
}