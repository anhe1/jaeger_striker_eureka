﻿using System;
using System.Linq;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Jaeger.CFDI.Entities.Addenda
{
    public class Addendas
    {
        private BindingList<Addenda> objetoField;

        /// <summary>
        /// constructor
        /// </summary>
        public Addendas()
        {
            this.objetoField = new BindingList<Addenda>();
        }

        #region propiedades

        [JsonProperty]
        public BindingList<Addenda> Objeto
        {
            get
            {
                return this.objetoField;
            }
            set
            {
                this.objetoField = value;
            }
        }

        #endregion

        #region metodos

        public string Json(Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static Addendas Json(string inputJson)
        {
            try
            {
                return JsonConvert.DeserializeObject<Addendas>(inputJson);
            }
            catch 
            {
                return null;
            }
        }

        #endregion
    }
}
