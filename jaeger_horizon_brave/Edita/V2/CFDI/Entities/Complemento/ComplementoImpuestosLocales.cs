﻿using System;
using System.Linq;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    [JsonObject("TrasladosLocales")]
    public partial class ComplementoImpuestosLocalesTrasladosLocales : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string impLocTrasladadoField;
        private decimal tasadeTrasladoField;
        private decimal importeField;
    
        /// <remarks/>
        [JsonProperty("impLocTrasladado")]
        public string ImpLocTrasladado
        {
            get
            {
                return this.impLocTrasladadoField;
            }
            set
            {
                this.impLocTrasladadoField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <remarks/>
        [JsonProperty("tasadeTraslado")]
        public decimal TasadeTraslado
        {
            get
            {
                return this.tasadeTrasladoField;
            }
            set
            {
                this.tasadeTrasladoField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <remarks/>
        [JsonProperty("importe")]
        public decimal Importe
        {
            get
            {
                return this.importeField;
            }
            set
            {
                this.importeField = value;
                this.OnPropertyChanged();
            }
        }
    }
}

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    [JsonObject("retencionesLocales")]
    public partial class ComplementoImpuestosLocalesRetencionesLocales : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string impLocRetenidoField;
        private decimal tasadeRetencionField;
        private decimal importeField;

        /// <remarks/>
        [JsonProperty("impLocRetenido")]
        public string ImpLocRetenido
        {
            get
            {
                return this.impLocRetenidoField;
            }
            set
            {
                this.impLocRetenidoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("tasadeRetencion")]
        public decimal TasadeRetencion
        {
            get
            {
                return this.tasadeRetencionField;
            }
            set
            {
                this.tasadeRetencionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("importe")]
        public decimal Importe
        {
            get
            {
                return this.importeField;
            }
            set
            {
                this.importeField = value;
                this.OnPropertyChanged();
            }
        }
    }
}

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    [JsonObject]
    public class ComplementoImpuestosLocales : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private BindingList<ComplementoImpuestosLocalesRetencionesLocales> retencionesLocalesField;
        private BindingList<ComplementoImpuestosLocalesTrasladosLocales> trasladosLocalesField;
        private string versionField;
        private decimal totaldeRetencionesField;
        private decimal totaldeTrasladosField;

        public ComplementoImpuestosLocales()
        {
            this.versionField = "1.0";
        }
    
        /// <remarks/>
        [JsonProperty("version")]
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <remarks/>
        [JsonProperty("totalRetenciones")]
        public decimal TotaldeRetenciones
        {
            get
            {
                return this.totaldeRetencionesField;
            }
            set
            {
                this.totaldeRetencionesField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <remarks/>
        [JsonProperty("totalTraslados")]
        public decimal TotaldeTraslados
        {
            get
            {
                return this.totaldeTrasladosField;
            }
            set
            {
                this.totaldeTrasladosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("retenciones")]
        public BindingList<ComplementoImpuestosLocalesRetencionesLocales> RetencionesLocales
        {
            get
            {
                return this.retencionesLocalesField;
            }
            set
            {
                this.retencionesLocalesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("traslados")]
        public BindingList<ComplementoImpuestosLocalesTrasladosLocales> TrasladosLocales
        {
            get
            {
                return this.trasladosLocalesField;
            }
            set
            {
                this.trasladosLocalesField = value;
                this.OnPropertyChanged();
            }
        }

        #region metodos

        public string Json(Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static ComplementoImpuestosLocales Json(string inputJson)
        {
            try
            {
                return JsonConvert.DeserializeObject<ComplementoImpuestosLocales>(inputJson);
            }
            catch
            {
                return null;
            }
        }
        #endregion
    }
}
