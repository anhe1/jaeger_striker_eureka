﻿using System;
using System.Linq;
using Jaeger.CFDI.Entities;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    /// <summary>
    /// Complemento al Comprobante Fiscal Digital (CFD) y Comprobante Fiscal Digital por Internet (CFDI) para identificar las operaciones de compra y  venta de divisas que realizan los centros cambiarios y las casa de cambio; haciendo mención expresa de que los comprobantes se expiden por la “compra”, o bien, por la “venta” de divisas.
    /// </summary>
    [JsonObject]
    public class ComplementoDivisas : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string versionField;
        private EnumDivisasTipoOperacion tipoOperacionField;
    
        public ComplementoDivisas()
        {
            this.versionField = "1.0";
        }
    
        /// <summary>
        /// Atributo requerido para expresar la versión del complemento de divisas
        /// </summary>
        [JsonProperty("ver")]
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <summary>
        /// Elemento para definir el tipo de operación realizada. venta o compra de divisas
        /// </summary>
        [JsonProperty("tipoOperacion")]
        public EnumDivisasTipoOperacion TipoOperacion
        {
            get
            {
                return this.tipoOperacionField;
            }
            set
            {
                this.tipoOperacionField = value;
                this.OnPropertyChanged();
            }
        }
    }
}

