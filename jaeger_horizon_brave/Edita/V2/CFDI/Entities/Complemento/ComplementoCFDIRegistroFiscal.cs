﻿using System;
using System.Linq;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    /// <summary>
    /// Complemento para incluir los datos de identificación de los CFDIs generados en Registro Fiscal.
    /// </summary>
    [JsonObject]
    public class ComplementoCFDIRegistroFiscal
    {
        private string versionField;
        private string folioField;

        public ComplementoCFDIRegistroFiscal()
        {
            this.versionField = "1.0";
        }
    
        /// <summary>
        /// Atributo requerido que indica la versión del complemento CFDI Registro Fiscal.
        /// </summary>
        [JsonProperty("ver")]
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la relación del CFDI con el Registro Fiscal.
        /// </summary>
        [JsonProperty("folio")]
        public string Folio
        {
            get
            {
                return this.folioField;
            }
            set
            {
                this.folioField = value;
            }
        }

    }
}
