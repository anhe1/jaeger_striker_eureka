﻿using System;
using System.ComponentModel;
using Jaeger.Entities;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    [JsonObject("nomina")]
    public partial class ComplementoNomina : ViewModelComplementoNominaSingle, IDataErrorInfo
    {
        //private string uuidField;
        private ComplementoNominaEmisor emisorField;
        private ComplementoNominaReceptor receptorField;
        private ComplementoNominaPercepciones percepcionesField;
        private ComplementoNominaDeducciones deduccionesField;
        private BindingList<ComplementoNominaOtroPago> otrosPagosField;
        private BindingList<ComplementoNominaIncapacidad> incapacidadesField;
        //private string versionField;
        //private string tipoNominaField; //c_TipoNomina
        //private DateTime? fechaPagoField;
        //private DateTime? fechaInicialPagoField;
        //private DateTime? fechaFinalPagoField;
        //private decimal numDiasPagadosField;
        private decimal totalPercepcionesField;
        private decimal totalDeduccionesField;
        private decimal totalOtrosPagosField;
        //private decimal descuentoField;

        public ComplementoNomina()
        {
            this.Version = "1.2";
        }

        /// <summary>
        /// Nodo condicional para expresar la información del contribuyente emisor del comprobante de nómina.
        /// </summary>
        [JsonProperty("emisor")]
        public ComplementoNominaEmisor Emisor
        {
            get
            {
                return this.emisorField;
            }
            set
            {
                this.emisorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo requerido para precisar la información del contribuyente receptor del comprobante de nómina.
        /// </summary>
        [JsonProperty("receptor")]
        public ComplementoNominaReceptor Receptor
        {
            get
            {
                return this.receptorField;
            }
            set
            {
                this.receptorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar las percepciones aplicables.
        /// </summary>
        [JsonProperty("percepciones")]
        public ComplementoNominaPercepciones Percepciones
        {
            get
            {
                return this.percepcionesField;
            }
            set
            {
                this.percepcionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo opcional para expresar las deducciones aplicables.
        /// </summary>
        [JsonProperty("deducciones")]
        public ComplementoNominaDeducciones Deducciones
        {
            get
            {
                return this.deduccionesField;
            }
            set
            {
                this.deduccionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar otros pagos aplicables.
        /// </summary>
        [JsonProperty("otrosPagos")]
        public BindingList<ComplementoNominaOtroPago> OtrosPagos
        {
            get
            {
                return this.otrosPagosField;
            }
            set
            {
                this.otrosPagosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar información de las incapacidades.
        /// </summary>
        [JsonProperty("incapacidades")]
        public BindingList<ComplementoNominaIncapacidad> Incapacidades
        {
            get
            {
                return this.incapacidadesField;
            }
            set
            {
                this.incapacidadesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión de la versión del complemento.
        /// </summary>
        [JsonProperty("version")]
        public new string Version
        {
            get
            {
                return base.Version;
                //return this.versionField;
            }
            set
            {
                base.Version = value;
                //this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para indicar el tipo de nómina, puede ser O= Nómina ordinaria o E= Nómina extraordinaria.
        /// </summary>
        [JsonProperty("tipo")]
        public new string TipoNomina
        {
            get
            {
                return base.TipoNomina;
                //return this.tipoNominaField;
            }
            set
            {
                base.TipoNomina = value;
                //this.tipoNominaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión de la fecha efectiva de erogación del gasto. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601.
        /// </summary>
        [JsonProperty]
        public new DateTime? FechaPago
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                //if (this.fechaPagoField >= firstGoodDate)
                //    return this.fechaPagoField;
                //return null;
                if (base.FechaPago >= firstGoodDate)
                    return base.FechaPago;
                return null;
            }
            set
            {
                //this.fechaPagoField = value;
                base.FechaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión de la fecha inicial del período de pago. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601.
        /// </summary>
        [JsonProperty]
        public new DateTime? FechaInicialPago
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                //if (this.fechaInicialPagoField >= firstGoodDate)
                //    return this.fechaInicialPagoField;
                if (base.FechaInicialPago >= firstGoodDate)
                    return base.FechaInicialPago;
                return null;
            }
            set
            {
                base.FechaInicialPago = value;
                //this.fechaInicialPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión de la fecha final del período de pago. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601.
        /// </summary>
        [JsonProperty]
        public new DateTime? FechaFinalPago
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                //if (this.fechaFinalPagoField >= firstGoodDate)
                //    return this.fechaFinalPagoField;
                if (base.FechaFinalPago >= firstGoodDate)
                    return base.FechaFinalPago;
                return null;
            }
            set
            {
                base.FechaFinalPago = value;
                //this.fechaFinalPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión del número o la fracción de días pagados.
        /// </summary>
        [JsonProperty]
        public new decimal NumDiasPagados
        {
            get
            {
                return base.NumDiasPagados;
                //return this.numDiasPagadosField;
            }
            set
            {
                base.NumDiasPagados = value;
                //this.numDiasPagadosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para representar la suma de las percepciones.
        /// </summary>
        [JsonProperty]
        public decimal TotalPercepciones
        {
            get
            {
                return this.totalPercepcionesField;
            }
            set
            {
                this.totalPercepcionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para representar la suma de las deducciones aplicables.
        /// </summary>
        [JsonProperty]
        public decimal TotalDeducciones
        {
            get
            {
                return this.totalDeduccionesField;
            }
            set
            {
                this.totalDeduccionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para representar la suma de otros pagos.
        /// </summary>
        [JsonProperty]
        public decimal TotalOtrosPagos
        {
            get
            {
                return this.totalOtrosPagosField;
            }
            set
            {
                this.totalOtrosPagosField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("uuid")]
        public new string IdDocumento
        {
            get
            {
                return base.IdDocumento;
                //return this.uuidField;
            }
            set
            {
                //if (value != null)
                //    this.uuidField = value.ToUpper();
                //else
                //    this.uuidField = value;
                if (value != null)
                    base.IdDocumento = value.ToUpper();
                else
                    base.IdDocumento = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("descuento")]
        public new decimal Descuento
        {
            get
            {
                return base.Descuento;
                //return this.descuentoField;
            }
            set
            {
                base.Descuento = value;
                //this.descuentoField = value;
                this.OnPropertyChanged();
            }
        }

        //[JsonIgnore]
        //public int Anio
        //{
        //    get
        //    {
        //        return this.FechaPago.Value.Year;
        //    }
        //}

        //[JsonIgnore]
        //public int Quincena
        //{
        //    get
        //    {
        //        if (!(this.FechaPago == null))
        //            return this.FechaPago.Value.Day > 15 ? 2 : 1;
        //        return 0;
        //    }
        //}

        [JsonIgnore]
        public string this[string columnName]
        {
            get
            {
                if (columnName == "")
                {
                }
                return string.Empty;
            }
        }

        [JsonIgnore]
        public string Error
        {
            get
            {
                return string.Empty;
            }
        }

        #region metodos

        public string Json(Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static ComplementoNomina Json(string inputJson)
        {
            try
            {
                return JsonConvert.DeserializeObject<ComplementoNomina>(inputJson);
            }
            catch
            {
                return null;
            }
        }
        
        #endregion
    }

    [JsonObject]
    public partial class ComplementoNominaEmisorEntidadSncf : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string origenRecursoField; //c_OrigenRecurso
        private decimal montoRecursoPropioField;
        
        /// <summary>
        /// Atributo requerido para identificar el origen del recurso utilizado para el pago de nómina del personal que presta o desempeña un servicio personal subordinado o asimilado a salarios en las dependencias.
        /// </summary>
        [JsonProperty("origenRecurso")]
        public string OrigenRecurso
        {
            get
            {
                return this.origenRecursoField;
            }
            set
            {
                this.origenRecursoField = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Atributo condicional para expresar el monto del recurso pagado con cargo a sus participaciones u otros ingresos locales (importe bruto de los ingresos propios, es decir total de gravados y exentos), cuando el origen es mixto.
        /// </summary>
        [JsonProperty("montoRecursoPropio")]
        public decimal MontoRecursoPropio
        {
            get
            {
                return this.montoRecursoPropioField;
            }
            set
            {
                this.montoRecursoPropioField = value;
                this.OnPropertyChanged();
            }
        }
    }

    [JsonObject("emisor")]
    public partial class ComplementoNominaEmisor : Directorio.Entities.ViewModelContribuyente //EntityBase
    {
        private ComplementoNominaEmisorEntidadSncf entidadSncfField;
        //private string rfcField;
        //private string curpField;
        //private string registroPatronalField;
        private string rfcPatronOrigenField;
        
        /// <summary>
        /// Nodo condicional para que las entidades adheridas al Sistema Nacional de Coordinación Fiscal realicen la identificación del origen de los recursos utilizados en el pago de nómina del personal que presta o desempeña un servicio personal subordinado en las dependencias de la entidad federativa, del municipio o demarcación territorial de la Ciudad de México, así como en sus respectivos organismos autónomos y entidades paraestatales y paramunicipales
        /// </summary>
        [JsonProperty("EntidadSncf")]
        public ComplementoNominaEmisorEntidadSncf EntidadSncf
        {
            get
            {
                return this.entidadSncfField;
            }
            set
            {
                this.entidadSncfField = value;
                this.OnPropertyChanged();
            }
        }
        
        [JsonProperty("rfc")]
        public new string RFC
        {
            get
            {
                return base.RFC;
                //return this.rfcField;
            }
            set
            {
                base.RFC = value;
                //this.rfcField = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Atributo condicional para expresar la CURP del emisor del comprobante de nómina cuando es una persona física.
        /// </summary>
        [JsonProperty("curp")]
        public new string CURP
        {
            get
            {
                return base.CURP;
                //return this.curpField;
            }
            set
            {
                base.CURP = value;
                //this.curpField = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Atributo condicional para expresar el registro patronal, clave de ramo - pagaduría o la que le asigne la institución de seguridad social al patrón, a 20 posiciones máximo. Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        [JsonProperty("registroPatronal")]
        public new string RegistroPatronal
        {
            get
            {
                return base.RegistroPatronal;
                //return this.registroPatronalField;
            }
            set
            {
                base.RegistroPatronal = value;
                //this.registroPatronalField = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Atributo opcional para expresar el RFC de la persona que fungió como patrón cuando el pago al trabajador se realice a través de un tercero como vehículo o herramienta de pago.
        /// </summary>
        [JsonProperty("rfcPatronOrigen")]
        public string RFCPatronOrigen
        {
            get
            {
                return this.rfcPatronOrigenField;
            }
            set
            {
                this.rfcPatronOrigenField = value;
                this.OnPropertyChanged();
            }
        }
    }

    [JsonObject]
    public partial class NominaEmisorEntidadSNCF : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string origenRecursoField; //c_OrigenRecurso
        private decimal montoRecursoPropioField;
        
        /// <summary>
        /// Atributo requerido para identificar el origen del recurso utilizado para el pago de nómina del personal que presta o desempeña un servicio personal subordinado o asimilado a salarios en las dependencias.
        /// </summary>
        [JsonProperty("origenRecurso")]
        public string OrigenRecurso
        {
            get
            {
                return this.origenRecursoField;
            }
            set
            {
                this.origenRecursoField = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Atributo condicional para expresar el monto del recurso pagado con cargo a sus participaciones u otros ingresos locales (importe bruto de los ingresos propios, es decir total de gravados y exentos), cuando el origen es mixto.
        /// </summary>
        [JsonProperty("montoRecursoPropio")]
        public decimal MontoRecursoPropio
        {
            get
            {
                return this.montoRecursoPropioField;
            }
            set
            {
                this.montoRecursoPropioField = value;
                this.OnPropertyChanged();
            }
        }
    }

    [JsonObject("receptor")]
    public partial class ComplementoNominaReceptor : Jaeger.Edita.V2.Nomina.Entities.Empleado //Jaeger.Directorio.Entities.Empleado
    {
        private BindingList<ComplementoNominaReceptorSubContratacion> subContratacionField;
        //private string rfcField;
        //private string curpField;
        //private string numSeguridadSocialField;
        //private DateTime? fechaInicioRelLaboralField;
        //private string antiguedadField;
        //private string tipoContratoField; //c_TipoContrato
        private string sindicalizadoField; // NominaReceptorSindicalizado
        //private string tipoJornadaField; //c_TipoJornada
        //private string tipoRegimenField; //c_TipoRegimen
        //private string numEmpleadoField;
        //private string departamentoField;
        //private string puestoField;
        //private string riesgoPuestoField; //c_RiesgoPuesto
        //private string periodicidadPagoField; //c_PeriodicidadPago
        private string bancoField; //c_Banco
        private string cuentaBancariaField;
        //private decimal salarioBaseCotAporField;
        //private decimal salarioDiarioIntegradoField;
        private string claveEntFedField; //c_Estado
        
        /// <summary>
        /// Nodo condicional para expresar la lista de las personas que los subcontrataron.
        /// </summary>
        [JsonProperty("subContratacion", NullValueHandling = NullValueHandling.Ignore)]
        public BindingList<ComplementoNominaReceptorSubContratacion> SubContratacion
        {
            get
            {
                return this.subContratacionField;
            }
            set
            {
                this.subContratacionField = value;
                this.OnPropertyChanged();
            }
        }
        
        //[JsonProperty("rfc")]
        //public string Rfc
        //{
        //    get
        //    {
        //        return this.rfcField;
        //    }
        //    set
        //    {
        //        this.rfcField = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        
        ///// <summary>
        ///// Atributo requerido para expresar la CURP del receptor del comprobante de nómina.
        ///// </summary>
        //[JsonProperty("curp")]
        //public string Curp
        //{
        //    get
        //    {
        //        return this.curpField;
        //    }
        //    set
        //    {
        //        this.curpField = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        
        ///// <summary>
        ///// Atributo condicional para expresar el número de seguridad social del trabajador. Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        ///// </summary>
        //[JsonProperty("numSeguridadSocial")]
        //public string NumSeguridadSocial
        //{
        //    get
        //    {
        //        return this.numSeguridadSocialField;
        //    }
        //    set
        //    {
        //        this.numSeguridadSocialField = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        
        ///// <summary>
        ///// Atributo condicional para expresar la fecha de inicio de la relación laboral entre el empleador y el empleado. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601. Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        ///// </summary>
        //[JsonProperty("fechaInicioRelLaboral")]
        //public DateTime? FechaInicioRelLaboral
        //{
        //    get
        //    {
        //        DateTime firstGoodDate = new DateTime(1900, 1, 1);
        //        if (this.fechaInicioRelLaboralField >= firstGoodDate)
        //        {
        //            return this.fechaInicioRelLaboralField;
        //        }
        //        return null;
        //    }
        //    set
        //    {
        //        this.fechaInicioRelLaboralField = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        
        ///// <summary>
        ///// Atributo condicional para expresar el número de semanas o el periodo de años, meses y días que el empleado ha mantenido relación laboral con el empleador. Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        ///// </summary>
        //[JsonProperty("antiguedad")]
        //public string Antiguedad
        //{
        //    get
        //    {
        //        return this.antiguedadField;
        //    }
        //    set
        //    {
        //        this.antiguedadField = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        
        ///// <summary>
        ///// Atributo requerido para expresar el tipo de contrato que tiene el trabajador.
        ///// </summary>
        //[JsonProperty("tipoContrato")]
        //public string TipoContrato
        //{
        //    get
        //    {
        //        return this.tipoContratoField;
        //    }
        //    set
        //    {
        //        this.tipoContratoField = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        
        /// <summary>
        /// Atributo opcional para indicar si el trabajador está asociado a un sindicato. Si se omite se asume que no está asociado a algún sindicato.
        /// </summary>
        [JsonProperty("sindicalizado", NullValueHandling = NullValueHandling.Ignore)]
        public string Sindicalizado
        {
            get
            {
                return this.sindicalizadoField;
            }
            set
            {
                this.sindicalizadoField = value;
                this.OnPropertyChanged();
            }
        }
        
        ///// <summary>
        ///// Atributo condicional para expresar el tipo de jornada que cubre el trabajador. Se debe ingresar cuando se esté obligado conforme a otras disposiciones distintas a las fiscales.
        ///// </summary>
        //[JsonProperty("tipoJornada")]
        //public string TipoJornada
        //{
        //    get
        //    {
        //        return this.tipoJornadaField;
        //    }
        //    set
        //    {
        //        this.tipoJornadaField = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        
        ///// <summary>
        ///// Atributo requerido para la expresión de la clave del régimen por el cual se tiene contratado al trabajador.
        ///// </summary>
        //[JsonProperty("tipoRegimen")]
        //public string TipoRegimen
        //{
        //    get
        //    {
        //        return this.tipoRegimenField;
        //    }
        //    set
        //    {
        //        this.tipoRegimenField = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        
        ///// <summary>
        ///// Atributo requerido para expresar el número de empleado de 1 a 15 posiciones.
        ///// </summary>
        //[JsonProperty("numEmpleado")]
        //public string NumEmpleado
        //{
        //    get
        //    {
        //        return this.numEmpleadoField;
        //    }
        //    set
        //    {
        //        this.numEmpleadoField = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        
        ///// <summary>
        ///// Atributo opcional para la expresión del departamento o área a la que pertenece el trabajador.
        ///// </summary>
        //[JsonProperty("depto")]
        //public string Departamento
        //{
        //    get
        //    {
        //        return this.departamentoField;
        //    }
        //    set
        //    {
        //        this.departamentoField = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        
        ///// <summary>
        ///// Atributo opcional para la expresión del puesto asignado al empleado o actividad que realiza.
        ///// </summary>
        //[JsonProperty("puesto")]
        //public string Puesto
        //{
        //    get
        //    {
        //        return this.puestoField;
        //    }
        //    set
        //    {
        //        this.puestoField = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        
        ///// <summary>
        ///// Atributo opcional para expresar la clave conforme a la Clase en que deben inscribirse los patrones, de acuerdo con las actividades que desempeñan sus trabajadores, según lo previsto en el artículo 196 del Reglamento en Materia de Afiliación Clasificación de Empresas, Recaudación y Fiscalización, o conforme con la normatividad del Instituto de Seguridad Social del trabajador. Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        ///// </summary>
        //[JsonProperty("riesgoPuesto")]
        //public string RiesgoPuesto
        //{
        //    get
        //    {
        //        return this.riesgoPuestoField;
        //    }
        //    set
        //    {
        //        this.riesgoPuestoField = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        
        ///// <summary>
        ///// Atributo requerido para la forma en que se establece el pago del salario.
        ///// </summary>
        //[JsonProperty("periodicidadPago")]
        //public string PeriodicidadPago
        //{
        //    get
        //    {
        //        return this.periodicidadPagoField;
        //    }
        //    set
        //    {
        //        this.periodicidadPagoField = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        
        /// <summary>
        /// Atributo condicional para la expresión de la clave del Banco conforme al catálogo, donde se realiza el depósito de nómina.
        /// </summary>
        [JsonProperty("banco")]
        public string Banco
        {
            get
            {
                return this.bancoField;
            }
            set
            {
                this.bancoField = value;
            }
        }
        
        /// <summary>
        /// Atributo condicional para la expresión de la cuenta bancaria a 11 posiciones o número de teléfono celular a 10 posiciones o número de tarjeta de crédito, débito o servicios a 15 ó 16 posiciones o la CLABE a 18 posiciones o número de monedero electrónico, donde se realiza el depósito de nómina.
        /// </summary>
        [JsonProperty("cuentaBancaria")]
        public string CuentaBancaria
        {
            get
            {
                return this.cuentaBancariaField;
            }
            set
            {
                this.cuentaBancariaField = value;
                this.OnPropertyChanged();
            }
        }
        
        ///// <summary>
        ///// Atributo opcional para expresar la retribución otorgada al trabajador, que se integra por los pagos hechos en efectivo por cuota diaria, gratificaciones, percepciones, alimentación, habitación, primas, comisiones, prestaciones en especie y cualquiera otra cantidad o prestación que se entregue al trabajador por su trabajo, sin considerar los conceptos que se excluyen de conformidad con el Artículo 27 de la Ley del Seguro Social, o la integración de los pagos conforme la normatividad del Instituto de Seguridad Social del trabajador. (Se emplea para pagar las cuotas y aportaciones de Seguridad Social). Se debe ingresar cuando se esté obligado conforme a otras disposiciones distintas a las fiscales.
        ///// </summary>
        //[JsonProperty("SalarioBaseCotApor")]
        //public decimal SalarioBaseCotApor
        //{
        //    get
        //    {
        //        return this.salarioBaseCotAporField;
        //    }
        //    set
        //    {
        //        this.salarioBaseCotAporField = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        
        ///// <summary>
        ///// Atributo opcional para expresar el salario que se integra con los pagos hechos en efectivo por cuota diaria, gratificaciones, percepciones, habitación, primas, comisiones, prestaciones en especie y cualquier otra cantidad o prestación que se entregue al trabajador por su trabajo, de conformidad con el Art. 84 de la Ley Federal del Trabajo. (Se utiliza para el cálculo de las indemnizaciones). Se debe ingresar cuando se esté obligado conforme a otras disposiciones distintas a las fiscales.
        ///// </summary>
        //[JsonProperty("salarioDiarioIntegrado")]
        //public decimal SalarioDiarioIntegrado
        //{
        //    get
        //    {
        //        return this.salarioDiarioIntegradoField;
        //    }
        //    set
        //    {
        //        this.salarioDiarioIntegradoField = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        
        /// <summary>
        /// Atributo requerido para expresar la clave de la entidad federativa en donde el receptor del recibo prestó el servicio.
        /// </summary>
        [JsonProperty("claveEntFed")]
        public string ClaveEntFed
        {
            get
            {
                return this.claveEntFedField;
            }
            set
            {
                this.claveEntFedField = value;
                this.OnPropertyChanged();
            }
        }
    }

    [JsonObject]
    public partial class ComplementoNominaReceptorSubContratacion : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string rfcLaboraField;
        private decimal porcentajeTiempoField;
        
        /// <summary>
        /// Atributo requerido para expresar el RFC de la persona que subcontrata.
        /// </summary>
        [JsonProperty("rfcLabora")]
        public string RFCLabora
        {
            get
            {
                return this.rfcLaboraField;
            }
            set
            {
                this.rfcLaboraField = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Atributo requerido para expresar el porcentaje del tiempo que prestó sus servicios con el RFC que lo subcontrata.
        /// </summary>
        [JsonProperty("porcentajeTiempo")]
        public decimal PorcentajeTiempo
        {
            get
            {
                return this.porcentajeTiempoField;
            }
            set
            {
                this.porcentajeTiempoField = value;
                this.OnPropertyChanged();
            }
        }
    }

    [JsonObject]
    public partial class ComplementoNominaPercepciones : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private BindingList<ComplementoNominaPercepcion> percepcionField;
        private ComplementoNominaPercepcionesJubilacionPensionRetiro jubilacionPensionRetiroField;
        private ComplementoNominaPercepcionesSeparacionIndemnizacion separacionIndemnizacionField;
        private decimal totalSueldosField;
        private bool totalSueldosFieldSpecified;
        private decimal totalSeparacionIndemnizacionField;
        private bool totalSeparacionIndemnizacionFieldSpecified;
        private decimal totalJubilacionPensionRetiroField;
        private bool totalJubilacionPensionRetiroFieldSpecified;
        private decimal totalGravadoField;
        private decimal totalExentoField;
        
        /// <summary>
        /// Nodo requerido para expresar la información detallada de una percepción
        /// </summary>
        [JsonProperty("percepcion")]
        public BindingList<ComplementoNominaPercepcion> Percepcion
        {
            get
            {
                return this.percepcionField;
            }
            set
            {
                this.percepcionField = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Nodo condicional para expresar la información detallada de pagos por jubilación, pensiones o haberes de retiro.
        /// </summary>
        [JsonProperty("jubilacionPensionRetiro")]
        public ComplementoNominaPercepcionesJubilacionPensionRetiro JubilacionPensionRetiro
        {
            get
            {
                return this.jubilacionPensionRetiroField;
            }
            set
            {
                this.jubilacionPensionRetiroField = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Nodo condicional para expresar la información detallada de otros pagos por separación.
        /// </summary>
        [JsonProperty("separacionIndemnizacion")]
        public ComplementoNominaPercepcionesSeparacionIndemnizacion SeparacionIndemnizacion
        {
            get
            {
                return this.separacionIndemnizacionField;
            }
            set
            {
                this.separacionIndemnizacionField = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Atributo condicional para expresar el total de percepciones brutas (gravadas y exentas) por sueldos y salarios y conceptos asimilados a salarios.
        /// </summary>
        [JsonProperty("totalSueldos")]
        public decimal TotalSueldos
        {
            get
            {
                return this.totalSueldosField;
            }
            set
            {
                this.totalSueldosField = value;
                this.OnPropertyChanged();
            }
        }
        
        public bool TotalSueldosSpecified
        {
            get
            {
                return this.totalSueldosFieldSpecified;
            }
            set
            {
                this.totalSueldosFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Atributo condicional para expresar el importe exento y gravado de las claves tipo percepción 022 Prima por Antigüedad, 023 Pagos por separación y 025 Indemnizaciones.
        /// </summary>
        [JsonProperty("totalSeparacionIndemnizacion")]
        public decimal TotalSeparacionIndemnizacion
        {
            get
            {
                return this.totalSeparacionIndemnizacionField;
            }
            set
            {
                this.totalSeparacionIndemnizacionField = value;
                this.OnPropertyChanged();
            }
        }
        
        public bool TotalSeparacionIndemnizacionSpecified
        {
            get
            {
                return this.totalSeparacionIndemnizacionFieldSpecified;
            }
            set
            {
                this.totalSeparacionIndemnizacionFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Atributo condicional para expresar el importe exento y gravado de las claves tipo percepción 039 Jubilaciones, pensiones o haberes de retiro en una exhibición y 044 Jubilaciones, pensiones o haberes de retiro en parcialidades.
        /// </summary>
        [JsonProperty("totalJubilacionPensionRetiro")]
        public decimal TotalJubilacionPensionRetiro
        {
            get
            {
                return this.totalJubilacionPensionRetiroField;
            }
            set
            {
                this.totalJubilacionPensionRetiroField = value;
                this.OnPropertyChanged();
            }
        }
        
        public bool TotalJubilacionPensionRetiroSpecified
        {
            get
            {
                return this.totalJubilacionPensionRetiroFieldSpecified;
            }
            set
            {
                this.totalJubilacionPensionRetiroFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Atributo requerido para expresar el total de percepciones gravadas que se relacionan en el comprobante.
        /// </summary>
        [JsonProperty("totalGravado")]
        public decimal TotalGravado
        {
            get
            {
                return this.totalGravadoField;
            }
            set
            {
                this.totalGravadoField = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Atributo requerido para expresar el total de percepciones exentas que se relacionan en el comprobante.
        /// </summary>
        [JsonProperty("totalExento")]
        public decimal TotalExento
        {
            get
            {
                return this.totalExentoField;
            }
            set
            {
                this.totalExentoField = value;
                this.OnPropertyChanged();
            }
        }
    }

    /// <summary>
    /// >Nodo requerido para expresar la información detallada de una percepción
    /// </summary>
    [JsonObject]
    public partial class ComplementoNominaPercepcion : ViewModelNominaParteSingle //Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private ComplementoNominaPercepcionAccionesOTitulos accionesOTitulosField;
        private BindingList<ComplementoNominaPercepcionHorasExtra> horasExtraField;
        //private string tipoPercepcionField; //c_TipoPercepcion
        //private string claveField;
        //private string conceptoField;
        //private decimal importeGravadoField;
        //private decimal importeExentoField;

        public ComplementoNominaPercepcion()
        {
            this.IdDoc = 1;
        }

        /// <summary>
        /// Nodo condicional para expresar ingresos por acciones o títulos valor que representan bienes. Se vuelve requerido cuando existan ingresos por sueldos derivados de adquisición de acciones o títulos (Art. 94, fracción VII LISR).
        /// </summary>
        [JsonProperty("accionesOTitulos", NullValueHandling = NullValueHandling.Ignore)]
        public ComplementoNominaPercepcionAccionesOTitulos AccionesOTitulos
        {
            get
            {
                return this.accionesOTitulosField;
            }
            set
            {
                this.accionesOTitulosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar ingresos por acciones o títulos valor que representan bienes. Se vuelve requerido cuando existan ingresos por sueldos derivados de adquisición de acciones o títulos (Art. 94, fracción VII LISR).
        /// </summary>
        [JsonProperty("horasExtra", NullValueHandling = NullValueHandling.Ignore)]
        public new BindingList<ComplementoNominaPercepcionHorasExtra> HorasExtra
        {
            get
            {
                return this.horasExtraField;
            }
            set
            {
                this.horasExtraField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Clave agrupadora bajo la cual se clasifica la percepción. (catNomina:c_TipoPercepcion)
        /// </summary>
        [JsonProperty("tipoPercepcion")]
        public string TipoPercepcion
        {
            get
            {
                return base.Tipo;
                //return this.tipoPercepcionField;
            }
            set
            {
                base.Tipo = value;
                //this.tipoPercepcionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de percepción de nómina propia de la contabilidad de cada patrón, puede conformarse desde 3 hasta 15 caracteres.
        /// pattern value="[^|]{3,15}"
        /// </summary>
        [JsonProperty("clave")]
        public new string Clave
        {
            get
            {
                return base.Clave;
                //return this.claveField;
            }
            set
            {
                base.Clave = value;
                //this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripción del concepto de percepción.
        /// pattern value="[^|]{1,100}"
        /// </summary>
        [JsonProperty("concepto")]
        public new string Concepto
        {
            get
            {
                return base.Concepto;
                //return this.conceptoField;
            }
            set
            {
                base.Concepto = value;
                //this.conceptoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe gravado de un concepto de percepción.
        /// </summary>
        [JsonProperty("importeGravado")]
        public new decimal ImporteGravado
        {
            get
            {
                return base.ImporteGravado;
                //return this.importeGravadoField;
            }
            set
            {
                base.ImporteGravado = value;
                //this.importeGravadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe exento de un concepto de percepción.
        /// </summary>
        [JsonProperty("importeExento")]
        public new decimal ImporteExento
        {
            get
            {
                return base.ImporteExento;
                //return this.importeExentoField;
            }
            set
            {
                base.ImporteExento = value;
                //this.importeExentoField = value;
                this.OnPropertyChanged();
            }
        }
    }

    /// <summary>
    /// Nodo condicional para expresar ingresos por acciones o títulos valor que representan bienes. Se vuelve requerido cuando existan ingresos por sueldos derivados de adquisición de acciones o títulos (Art. 94, fracción VII LISR).
    /// </summary>
    [JsonObject]
    public partial class ComplementoNominaPercepcionAccionesOTitulos : ViewModelNominaParteSingle ///Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        //private decimal valorMercadoField;
        //private decimal precioAlOtorgarseField;

        public ComplementoNominaPercepcionAccionesOTitulos()
        {
            this.IdDoc = 5;
        }

        /// <summary>
        /// obtener o establecer el valor de mercado de las Acciones o Títulos valor al ejercer la opción.
        /// </summary>
        [JsonProperty("valorMercado")]
        public decimal ValorMercado
        {
            get
            {
                return base.ImporteExento;
                //return this.valorMercadoField;
            }
            set
            {
                base.ImporteExento = value;
                ///this.valorMercadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el precio establecido al otorgarse la opción de ingresos en acciones o títulos valor.
        /// </summary>
        [JsonProperty("precioAlOtorgarse")]
        public decimal PrecioAlOtorgarse
        {
            get
            {
                return base.ImporteGravado;
                //return this.precioAlOtorgarseField;
            }
            set
            {
                base.ImporteGravado = value;
                ///this.precioAlOtorgarseField = value;
                this.OnPropertyChanged();
            }
        }
    }

    /// <summary>
    /// Nodo condicional para expresar las horas extra aplicables.
    /// </summary>
    [JsonObject]
    public partial class ComplementoNominaPercepcionHorasExtra : ViewModelNominaParteSingle //Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        //private int diasField;
        //private string tipoHorasField; //c_TipoHoras
        //private int horasExtraField;
        //private decimal importePagadoField;

        public ComplementoNominaPercepcionHorasExtra()
        {
            this.IdDoc = 3;
        }

        /// <summary>
        /// obtener o establecer el número de días en que el trabajador realizó horas extra en el periodo.
        /// </summary>
        [JsonProperty("dias")]
        public new int Dias
        {
            get
            {
                return base.Dias;
                //return this.diasField;
            }
            set
            {
                base.Dias = value;
                //this.diasField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de pago de las horas extra.
        /// </summary>
        [JsonProperty("tipoHoras")]
        public new string TipoHoras
        {
            get
            {
                return base.TipoHoras;
                //return this.tipoHorasField;
            }
            set
            {
                base.TipoHoras = value;
                //this.tipoHorasField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número de horas extra trabajadas en el periodo.
        /// </summary>
        [JsonProperty("horasExtra")]
        public new int HorasExtra
        {
            get
            {
                return base.HorasExtra;
                //return this.horasExtraField;
            }
            set
            {
                base.HorasExtra = value;
                //this.horasExtraField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe pagado por las horas extra.
        /// </summary>
        [JsonProperty("importePagado")]
        public decimal ImportePagado
        {
            get
            {
                return base.ImportePagadoHorasExtra;
                //return this.importePagadoField;
            }
            set
            {
                base.ImportePagadoHorasExtra = value;
                //this.importePagadoField = value;
                this.OnPropertyChanged();
            }
        }
    }

    /// <summary>
    /// Nodo condicional para expresar la información detallada de pagos por jubilación, pensiones o haberes de retiro.
    /// </summary>
    [JsonObject]
    public partial class ComplementoNominaPercepcionesJubilacionPensionRetiro : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private decimal totalUnaExhibicionField;
        private bool totalUnaExhibicionFieldSpecified;
        private decimal totalParcialidadField;
        private bool totalParcialidadFieldSpecified;
        private decimal montoDiarioField;
        private bool montoDiarioFieldSpecified;
        private decimal ingresoAcumulableField;
        private decimal ingresoNoAcumulableField;

        public ComplementoNominaPercepcionesJubilacionPensionRetiro()
        {
            
        }

        /// <summary>
        /// obtener o establecer el monto total del pago cuando se realiza en una sola exhibición.
        /// </summary>
        public decimal TotalUnaExhibicion
        {
            get
            {
                return this.totalUnaExhibicionField;
            }
            set
            {
                this.totalUnaExhibicionField = value;
                this.OnPropertyChanged();
            }
        }
        
        public bool TotalUnaExhibicionSpecified
        {
            get
            {
                return this.totalUnaExhibicionFieldSpecified;
            }
            set
            {
                this.totalUnaExhibicionFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer los ingresos totales por pago cuando se hace en parcialidades.
        /// </summary>
        public decimal TotalParcialidad
        {
            get
            {
                return this.totalParcialidadField;
            }
            set
            {
                this.totalParcialidadField = value;
                this.OnPropertyChanged();
            }
        }
        
        public bool TotalParcialidadSpecified
        {
            get
            {
                return this.totalParcialidadFieldSpecified;
            }
            set
            {
                this.totalParcialidadFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el monto diario percibido por jubilación, pensiones o haberes de retiro cuando se realiza en parcialidades.
        /// </summary>
        public decimal MontoDiario
        {
            get
            {
                return this.montoDiarioField;
            }
            set
            {
                this.montoDiarioField = value;
                this.OnPropertyChanged();
            }
        }
        
        public bool MontoDiarioSpecified
        {
            get
            {
                return this.montoDiarioFieldSpecified;
            }
            set
            {
                this.montoDiarioFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer los ingresos acumulables.
        /// </summary>
        public decimal IngresoAcumulable
        {
            get
            {
                return this.ingresoAcumulableField;
            }
            set
            {
                this.ingresoAcumulableField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer los ingresos no acumulables
        /// </summary>
        public decimal IngresoNoAcumulable
        {
            get
            {
                return this.ingresoNoAcumulableField;
            }
            set
            {
                this.ingresoNoAcumulableField = value;
                this.OnPropertyChanged();
            }
        }
    }

    /// <summary>
    /// Nodo condicional para expresar la información detallada de otros pagos por separación.
    /// </summary>
    [JsonObject]
    public partial class ComplementoNominaPercepcionesSeparacionIndemnizacion : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private decimal totalPagadoField;
        private int numAñosServicioField;
        private decimal ultimoSueldoMensOrdField;
        private decimal ingresoAcumulableField;
        private decimal ingresoNoAcumulableField;

        public ComplementoNominaPercepcionesSeparacionIndemnizacion()
        {

        }

        /// <summary>
        /// obtener o establecer el monto total del pago.
        /// </summary>
        [JsonProperty]
        public decimal TotalPagado
        {
            get
            {
                return this.totalPagadoField;
            }
            set
            {
                this.totalPagadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número de años de servicio del trabajador. Se redondea al entero superior si la cifra contiene años y meses y hay más de 6 meses.
        /// </summary>
        [JsonProperty]
        public int NumaniosServicio
        {
            get
            {
                return this.numAñosServicioField;
            }
            set
            {
                this.numAñosServicioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el último sueldo mensual ordinario.
        /// </summary>
        [JsonProperty]
        public decimal UltimoSueldoMensOrd
        {
            get
            {
                return this.ultimoSueldoMensOrdField;
            }
            set
            {
                this.ultimoSueldoMensOrdField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer los ingresos acumulables.
        /// </summary>
        [JsonProperty]
        public decimal IngresoAcumulable
        {
            get
            {
                return this.ingresoAcumulableField;
            }
            set
            {
                this.ingresoAcumulableField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer los ingresos no acumulables.
        /// </summary>
        [JsonProperty]
        public decimal IngresoNoAcumulable
        {
            get
            {
                return this.ingresoNoAcumulableField;
            }
            set
            {
                this.ingresoNoAcumulableField = value;
                this.OnPropertyChanged();
            }
        }
    }

    /// <summary>
    /// Nodo opcional para expresar las deducciones aplicables.
    /// </summary>
    [JsonObject]
    public partial class ComplementoNominaDeducciones : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private BindingList<ComplementoNominaDeduccion> deduccionField;
        private decimal totalOtrasDeduccionesField;
        private bool totalOtrasDeduccionesFieldSpecified;
        private decimal totalImpuestosRetenidosField;
        private bool totalImpuestosRetenidosFieldSpecified;
        private decimal totalGravadoField; // campo para la version 1.1
        private decimal totalExentoField; // campo para la version 1.1
        
        public BindingList<ComplementoNominaDeduccion> Deduccion
        {
            get
            {
                return this.deduccionField;
            }
            set
            {
                this.deduccionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total de deducciones que se relacionan en el comprobante, donde la clave de tipo de deducción sea distinta a la 002 correspondiente a ISR
        /// </summary>
        public decimal TotalOtrasDeducciones
        {
            get
            {
                return this.totalOtrasDeduccionesField;
            }
            set
            {
                this.totalOtrasDeduccionesField = value;
                this.OnPropertyChanged();
            }
        }

        public bool TotalOtrasDeduccionesSpecified
        {
            get
            {
                return this.totalOtrasDeduccionesFieldSpecified;
            }
            set
            {
                this.totalOtrasDeduccionesFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer el total de los impuestos federales retenidos, es decir, donde la clave de tipo de deducción sea 002 correspondiente a ISR.
        /// </summary>
        public decimal TotalImpuestosRetenidos
        {
            get
            {
                return this.totalImpuestosRetenidosField;
            }
            set
            {
                this.totalImpuestosRetenidosField = value;
                this.OnPropertyChanged();
            }
        }
        
        public bool TotalImpuestosRetenidosSpecified
        {
            get
            {
                return this.totalImpuestosRetenidosFieldSpecified;
            }
            set
            {
                this.totalImpuestosRetenidosFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// campo para la version 1.1
        /// </summary>
        public decimal TotalGravado
        {
            get
            {
                return this.totalGravadoField;
            }
            set
            {
                this.totalGravadoField = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// campo para la version 1.1
        /// </summary>
        public decimal TotalExento
        {
            get
            {
                return this.totalExentoField;
            }
            set
            {
                this.totalExentoField = value;
                this.OnPropertyChanged();
            }
        }
    }

    /// <summary>
    /// Nodo opcional para expresar las deducciones aplicables.
    /// </summary>
    [JsonObject]
    public partial class ComplementoNominaDeduccion : ViewModelNominaParteSingle //Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        //private string tipoDeduccionField; //c_TipoDeduccion
        //private string claveField;
        //private string conceptoField;
        //private decimal importeField;

        public ComplementoNominaDeduccion()
        {
            this.IdDoc = 2;
        }

        /// <summary>
        /// obtener o establecer la clave agrupadora que clasifica la deducción.
        /// </summary>
        [JsonProperty]
        public string TipoDeduccion
        {
            get
            {
                return base.Tipo;
                //return this.tipoDeduccionField;
            }
            set
            {
                base.Tipo = value;
                //this.tipoDeduccionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de deducción de nómina propia de la contabilidad de cada patrón, puede conformarse desde 3 hasta 15 caracteres.
        /// pattern value="[^|]{3,15}"
        /// </summary>
        [JsonProperty]
        public new string Clave
        {
            get
            {
                return base.Clave;
                //return this.claveField;
            }
            set
            {
                base.Clave = value;
                //this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripción del concepto de deducción.
        /// pattern value="[^|]{1,100}"
        /// </summary>
        [JsonProperty]
        public new string Concepto
        {
            get
            {
                return base.Concepto;
                //return this.conceptoField;
            }
            set
            {
                base.Concepto = value;
                //this.conceptoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del concepto de deducción.
        /// </summary>
        [JsonProperty]
        public new decimal Importe
        {
            get
            {
                return base.ImporteGravado;
                //return this.importeField;
            }
            set
            {
                base.ImporteGravado = value;
                //this.importeField = value;
                this.OnPropertyChanged();
            }
        }
    }

    /// <summary>
    /// Nodo condicional para expresar otros pagos aplicables.
    /// </summary>
    [JsonObject]
    public partial class ComplementoNominaOtroPago : ViewModelNominaParteSingle // Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private ComplementoNominaOtroPagoSubsidioAlEmpleo subsidioAlEmpleoField;
        private ComplementoNominaOtroPagoCompensacionSaldosAFavor compensacionSaldosAFavorField;
        //private string tipoOtroPagoField; //c_TipoOtroPago
        //private string claveField;
        //private string conceptoField;
        //private decimal importeField;

        public ComplementoNominaOtroPago()
        {
            this.IdDoc = 8;
        }

        /// <summary>
        /// Nodo requerido para expresar la información referente al subsidio al empleo del trabajador.
        /// </summary>
        [JsonProperty("subsidioAlEmpleo")]
        public ComplementoNominaOtroPagoSubsidioAlEmpleo SubsidioAlEmpleo
        {
            get
            {
                return this.subsidioAlEmpleoField;
            }
            set
            {
                this.subsidioAlEmpleoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar la información referente a la compensación de saldos a favor de un trabajador.
        /// </summary>
        [JsonProperty("compensacionSaldosAFavor")]
        public ComplementoNominaOtroPagoCompensacionSaldosAFavor CompensacionSaldosAFavor
        {
            get
            {
                return this.compensacionSaldosAFavorField;
            }
            set
            {
                this.compensacionSaldosAFavorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave agrupadora bajo la cual se clasifica el otro pago. (catNomina:c_TipoOtroPago)
        /// </summary>
        [JsonProperty("tipoOtroPago")]
        public string TipoOtroPago
        {
            get
            {
                return base.Tipo;
                //return this.tipoOtroPagoField;
            }
            set
            {
                base.Tipo = value;
                //this.tipoOtroPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de otro pago de nómina propia de la contabilidad de cada patrón, puede conformarse desde 3 hasta 15 caracteres.
        /// pattern value="[^|]{3,15}"
        /// </summary>
        [JsonProperty("clave")]
        public new string Clave
        {
            get
            {
                return base.Clave;
                //return this.claveField;
            }
            set
            {
                base.Clave = value;
                //this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripción del concepto de otro pago.
        /// </summary>
        [JsonProperty("concepto")]
        public new string Concepto
        {
            get
            {
                return base.Concepto;
                //return this.conceptoField;
            }
            set
            {
                base.Concepto = value;
                //this.conceptoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del concepto de otro pago.
        /// </summary>
        [JsonProperty("importe")]
        public new decimal Importe
        {
            get
            {
                return base.Importe;
                //return this.importeField;
            }
            set
            {
                base.Importe = value;
                //this.importeField = value;
                this.OnPropertyChanged();
            }
        }
    }

    /// <summary>
    /// Nodo requerido para expresar la información referente al subsidio al empleo del trabajador.
    /// </summary>
    [JsonObject]
    public partial class ComplementoNominaOtroPagoSubsidioAlEmpleo : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private decimal subsidioCausadoField;

        /// <summary>
        /// obtener o establecer el subsidio causado conforme a la tabla del subsidio para el empleo publicada en el Anexo 8 de la RMF vigente.
        /// </summary>
        [JsonProperty("subsidioCausado")]
        public decimal SubsidioCausado
        {
            get
            {
                return this.subsidioCausadoField;
            }
            set
            {
                this.subsidioCausadoField = value;
                this.OnPropertyChanged();
            }
        }
    }

    /// <summary>
    /// Nodo condicional para expresar la información referente a la compensación de saldos a favor de un trabajador.
    /// </summary>
    [JsonObject]
    public partial class ComplementoNominaOtroPagoCompensacionSaldosAFavor : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private decimal saldoAFavorField;
        private short anioField;
        private decimal remanenteSalFavField;

        public ComplementoNominaOtroPagoCompensacionSaldosAFavor()
        {
            
        }

        /// <summary>
        /// obtener o establecer el saldo a favor determinado por el patrón al trabajador en periodos o ejercicios anteriores.
        /// </summary>
        [JsonProperty("saldoAFavor")]
        public decimal SaldoAFavor
        {
            get
            {
                return this.saldoAFavorField;
            }
            set
            {
                this.saldoAFavorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer expresar el año en que se determinó el saldo a favor del trabajador por el patrón que se incluye en el campo “RemanenteSalFav”.
        /// </summary>
        [JsonProperty("anio")]
        public short Anio
        {
            get
            {
                return this.anioField;
            }
            set
            {
                this.anioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el remanente del saldo a favor del trabajador
        /// </summary>
        [JsonProperty("remanenteSalFav")]
        public decimal RemanenteSalFav
        {
            get
            {
                return this.remanenteSalFavField;
            }
            set
            {
                this.remanenteSalFavField = value;
                this.OnPropertyChanged();
            }
        }
    }

    /// <summary>
    /// Nodo condicional para expresar información de las incapacidades.
    /// </summary>
    [JsonObject("incapacidad")]
    public partial class ComplementoNominaIncapacidad : ViewModelNominaParteSingle // Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        //private int diasIncapacidadField;
        //private string tipoIncapacidadField; //c_TipoIncapacidad
        //private decimal importeMonetarioField;
        private bool importeMonetarioFieldSpecified;

        public ComplementoNominaIncapacidad()
        {
            this.IdDoc = 4;
        }

        /// <summary>
        /// obtener o establecer el número de días enteros que el trabajador se incapacitó en el periodo.
        /// </summary>
        [JsonProperty("diasIncapacidad")]
        public new  int DiasIncapacidad
        {
            get
            {
                return base.DiasIncapacidad;
                //return this.diasIncapacidadField;
            }
            set
            {
                base.DiasIncapacidad = value;
                //this.diasIncapacidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la razón de la incapacidad. (catNomina:c_TipoIncapacidad)
        /// </summary>
        [JsonProperty("tipoIncapacidad")]
        public string TipoIncapacidad
        {
            get
            {
                return base.Tipo;
                //return this.tipoIncapacidadField;
            }
            set
            {
                base.Tipo = value;
                //this.tipoIncapacidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el monto del importe monetario de la incapacidad.
        /// </summary>
        [JsonProperty("importeMonetario")]
        public decimal ImporteMonetario
        {
            get
            {
                return base.DescuentoIncapcidad;
                //return this.importeMonetarioField;
            }
            set
            {
                base.DescuentoIncapcidad = value;
                //this.importeMonetarioField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty]
        public bool ImporteMonetarioSpecified
        {
            get
            {
                return this.importeMonetarioFieldSpecified;
            }
            set
            {
                this.importeMonetarioFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }
    }
}