﻿using System;
using System.Linq;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    /// <summary>
    /// Complemento al Comprobante Fiscal Digital por Internet (CFDI) para integrar la información emitida por un prestador de servicios de monedero electrónico de vales de despensa.
    /// </summary>
    public partial class ComplementoValesDeDespensa : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private BindingList<ComplementoValesDeDespensaConcepto> conceptosField;
        private string versionField;
        private string tipoOperacionField;
        private string registroPatronalField;
        private string numeroDeCuentaField;
        private decimal totalField;

        public ComplementoValesDeDespensa()
        {
            this.versionField = "1.0";
            this.tipoOperacionField = "monedero electrónico";
        }

        /// <summary>
        /// Nodo requerido para enlistar los conceptos cubiertos por los monederos electrónicos de vales de despensa.
        /// </summary>
        public BindingList<ComplementoValesDeDespensaConcepto> Conceptos
        {
            get
            {
                return this.conceptosField;
            }
            set
            {
                this.conceptosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido con valor prefijado a 1.0 que indica la versión del estándar bajo el que se encuentra expresado el comprobante.
        /// </summary>
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el tipo de operación de acuerdo con el medio de pago.
        /// </summary>
        public string TipoOperacion
        {
            get
            {
                return this.tipoOperacionField;
            }
            set
            {
                this.tipoOperacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar el registro patronal del adquirente del monedero electrónico.
        /// </summary>
        public string RegistroPatronal
        {
            get
            {
                return this.registroPatronalField;
            }
            set
            {
                this.registroPatronalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el numero de cuenta del adquiriente del monedero electrónico.
        /// </summary>
        public string NumeroDeCuenta
        {
            get
            {
                return this.numeroDeCuentaField;
            }
            set
            {
                this.numeroDeCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el monto total de vales de despensa otorgados.
        /// </summary>
        public decimal Total
        {
            get
            {
                return this.totalField;
            }
            set
            {
                this.totalField = value;
                this.OnPropertyChanged();
            }
        }

        #region metodos

        public string Json(Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static ComplementoValesDeDespensa Json(string inputJson)
        {
            try
            {
                return JsonConvert.DeserializeObject<ComplementoValesDeDespensa>(inputJson);
            }
            catch
            {
                return null;
            }
        }
        #endregion
    }
}

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    /// <summary>
    /// Nodo requerido para enlistar los conceptos cubiertos por los monederos electrónicos de vales de despensa.
    /// </summary>
    public partial class ComplementoValesDeDespensaConcepto : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string identificadorField;
        private DateTime fechaField;
        private string rfcField;
        private string curpField;
        private string nombreField;
        private string numSeguridadSocialField;
        private decimal importeField;

        /// <summary>
        /// Atributo requerido para expresar el identificador o numero del monedero electrónico.
        /// </summary>
        public string Identificador
        {
            get
            {
                return this.identificadorField;
            }
            set
            {
                this.identificadorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión de la Fecha y hora de expedición de la operación reportada. Se expresa en la forma aaaa-mm-ddThh:mm:ss, de acuerdo con la especificación ISO 8601.
        /// </summary>
        public DateTime Fecha
        {
            get
            {
                return this.fechaField;
            }
            set
            {
                this.fechaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión del Registro Federal de Contribuyentes del trabajador al que se le otorgó el monedero electrónico sin guiones o espacios
        /// </summary>
        public string RFC
        {
            get
            {
                return this.rfcField;
            }
            set
            {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión de la CURP del trabajador al que se le otorgó el monedero electrónico.
        /// </summary>
        public string CURP
        {
            get
            {
                return this.curpField;
            }
            set
            {
                this.curpField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión del Nombre del trabajador al que se le otorgó el monedero electrónico sin guiones o espacios
        /// </summary>
        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para la expresión del numero de seguridad social aplicable al trabajador.
        /// </summary>
        public string NumSeguridadSocial
        {
            get
            {
                return this.numSeguridadSocialField;
            }
            set
            {
                this.numSeguridadSocialField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el importe del depósito efectuado al trabajador en el monedero electrónico.
        /// </summary>
        public decimal Importe
        {
            get
            {
                return this.importeField;
            }
            set
            {
                this.importeField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
