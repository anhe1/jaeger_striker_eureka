﻿using System;
using System.Linq;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    [JsonObject]
    public class ComplementoAerolineas : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private ComplementoAerolineasOtrosCargos otrosCargosField;
        private string versionField;
        private decimal tUAField;

        public ComplementoAerolineas()
        {
            this.versionField = "1.0";
        }
    
        /// <remarks/>
        [JsonProperty("version")]
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <remarks/>
        [JsonProperty("tua")]
        public decimal TUA
        {
            get
            {
                return this.tUAField;
            }
            set
            {
                this.tUAField = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        public ComplementoAerolineasOtrosCargos OtrosCargos
        {
            get
            {
                return this.otrosCargosField;
            }
            set
            {
                this.otrosCargosField = value;
                this.OnPropertyChanged();
            }
        }

        #region metodos

        public string Json(Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static ComplementoAerolineas Json(string inputJson)
        {
            try
            {
                return JsonConvert.DeserializeObject<ComplementoAerolineas>(inputJson);
            }
            catch
            {
                return null;
            }
        }
        #endregion
    }
}

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    [JsonObject("otrosCargos")]
    public partial class ComplementoAerolineasOtrosCargos : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private BindingList<ComplementoAerolineasOtrosCargosCargo> cargoField;

        private decimal totalCargosField;

        /// <remarks/>
        [JsonProperty("cargo")]
        public BindingList<ComplementoAerolineasOtrosCargosCargo> Cargo
        {
            get
            {
                return this.cargoField;
            }
            set
            {
                this.cargoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("totalCargos")]
        public decimal TotalCargos
        {
            get
            {
                return this.totalCargosField;
            }
            set
            {
                this.totalCargosField = value;
                this.OnPropertyChanged();
            }
        }
    }
}

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    [JsonObject("cargosCargo")]
    public partial class ComplementoAerolineasOtrosCargosCargo : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string codigoCargoField;
        private decimal importeField;

        /// <remarks/>
        [JsonProperty("codigoCargo")]
        public string CodigoCargo
        {
            get
            {
                return this.codigoCargoField;
            }
            set
            {
                this.codigoCargoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("importe")]
        public decimal Importe
        {
            get
            {
                return this.importeField;
            }
            set
            {
                this.importeField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
