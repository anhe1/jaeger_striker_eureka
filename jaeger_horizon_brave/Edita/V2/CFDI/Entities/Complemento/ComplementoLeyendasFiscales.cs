﻿using System;
using System.Linq;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    /// <summary>
    /// Nodo para expresar la(s) leyenda(s) fiscal(es) que apliquen al comprobante
    /// </summary>
    public partial class ComplementoLeyendasFiscalesLeyenda : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string disposicionFiscalField;
        private string normaField;
        private string textoLeyendaField;

        /// <summary>
        /// Atributo opcional para especificar la Ley, Resolución o Disposición fiscal que regula la leyenda, deberá expresarse en siglas de mayúsculas y sin puntuación (p. ej: ISR)
        /// </summary>
        public string DisposicionFiscal
        {
            get
            {
                return this.disposicionFiscalField;
            }
            set
            {
                this.disposicionFiscalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para especificar el número de Artículo o en su caso Regla que regula la obligación de la leyenda
        /// </summary>
        public string Norma
        {
            get
            {
                return this.normaField;
            }
            set
            {
                this.normaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para especificar la leyenda fiscal
        /// </summary>
        public string TextoLeyenda
        {
            get
            {
                return this.textoLeyendaField;
            }
            set
            {
                this.textoLeyendaField = value;
                this.OnPropertyChanged();
            }
        }
    }
}

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    /// <summary>
    /// Nodo opcional para incluir leyendas previstas en disposiciones fiscales, distintas a las contenidas en el estándar de Comprobante Fiscal Digital (CFD) o Comprobante Fiscal Digital a través de Internet (CFDI).
    /// </summary>
    [JsonObject()]
    public partial class ComplementoLeyendasFiscales : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private BindingList<ComplementoLeyendasFiscalesLeyenda> leyendaField;
        private string versionField;

        public ComplementoLeyendasFiscales()
        {
            this.versionField = "1.0";
        }

        /// <summary>
        /// Nodo para expresar la(s) leyenda(s) fiscal(es) que apliquen al comprobante
        /// </summary>
        public BindingList<ComplementoLeyendasFiscalesLeyenda> Leyenda
        {
            get
            {
                return this.leyendaField;
            }
            set
            {
                this.leyendaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la versión del complemento de Leyendas Fiscales
        /// </summary>
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        #region metodos

        public string Json(Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static ComplementoLeyendasFiscales Json(string inputJson)
        {
            try
            {
                return JsonConvert.DeserializeObject<ComplementoLeyendasFiscales>(inputJson);
            }
            catch
            {
                return null;
            }
        }
        #endregion
    }
}
