﻿using System;
using System.Linq;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    /// <summary>
    /// Este complemento permite incorporar a un Comprobante Fiscal Digital (CFD) o a un Comprobante Fiscal Digital a través de Internet (CFDI) los datos de identificación del vehículo que corresponda a personas
    /// físicas integrantes de coordinados, que opten por pagar el impuesto individualmente de conformidad con lo establecido por el artículo 83, séptimo párrafo de la Ley del Impuesto sobre la Renta.
    /// </summary>
    [JsonObject]
    public class ComplementoPFintegranteCoordinado : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string versionField;
        private string claveVehicularField;
        private string placaField;
        private string rFcpfField;

        public ComplementoPFintegranteCoordinado()
        {
            this.versionField = "1.0";
        }
    
        /// <summary>
        /// Atributo requerido con valor prefijado a 1.0 que indica la versión del estándar bajo el que se encuentra expresado el complemento.
        /// </summary>
        [JsonProperty("version")]
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <summary>
        /// Atributo requerido para precisar Clave vehicular que corresponda a la versión del vehículo enajenado.
        /// </summary>
        [JsonProperty("claveVehicular")]
        public string ClaveVehicular
        {
            get
            {
                return this.claveVehicularField;
            }
            set
            {
                this.claveVehicularField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <summary>
        /// Atributo requerido para señalar la placa o número de folio del permiso del vehículo que corresponda.
        /// </summary>
        [JsonProperty("placa")]
        public string Placa
        {
            get
            {
                return this.placaField;
            }
            set
            {
                this.placaField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <summary>
        /// Atributo opcional para precisar el RFC de la persona física integrante de coordinados, que opte por pagar el impuesto individualmente.
        /// </summary>
        [JsonProperty("rfcPF")]
        public string Rfcpf
        {
            get
            {
                return this.rFcpfField;
            }
            set
            {
                this.rFcpfField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
