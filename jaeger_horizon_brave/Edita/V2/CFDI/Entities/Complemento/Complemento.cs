﻿using System;
using Jaeger.Edita.V2.CFDI.Enums;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    public class Complemento : BasePropertyChangeImplementation
    {
        private EnumCfdiComplementos nombreField;
        private string dataField;

        [JsonIgnore]
        public EnumCfdiComplementos Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("nombre", Order = 1)]
        public string NombreText
        {
            get
            {
                return Enum.GetName(typeof(EnumCfdiComplementos), this.Nombre);
            }
            set
            {
                this.Nombre = (EnumCfdiComplementos)Enum.Parse(typeof(EnumCfdiComplementos), value);
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("data", Order = 2)]
        public string Data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
