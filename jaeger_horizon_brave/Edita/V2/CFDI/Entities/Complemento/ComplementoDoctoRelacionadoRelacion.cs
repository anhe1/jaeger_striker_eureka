using Newtonsoft.Json;

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    [JsonObject("tipoDeRelacion")]
    public class ComplementoDoctoRelacionadoRelacion : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string claveField;
        private string descripcionField;

        public ComplementoDoctoRelacionadoRelacion()
        {
        }

        [JsonProperty("clave")]
        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("desc")]
        public string Descripcion
        {
            get
            {
                return this.descripcionField;
            }
            set
            {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }
    }
}