﻿using System;
using System.Linq;
using Jaeger.CFDI.Entities;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    [JsonObject]
    public class ComplementoTuristaPasajeroExtranjero : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private TuristaPasajeroExtranjeroDatosTransito datosTransitoField;
        private string versionField;
        private System.DateTime fechadeTransitoField;
        private TuristaPasajeroExtranjeroTipoTransito tipoTransitoField;

        public ComplementoTuristaPasajeroExtranjero()
        {
            this.versionField = "1.0";
        }
    
        public TuristaPasajeroExtranjeroDatosTransito DatosTransito
        {
            get
            {
                return this.datosTransitoField;
            }
            set
            {
                this.datosTransitoField = value;
                this.OnPropertyChanged();
            }
        }
    
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }
    
        public System.DateTime FechadeTransito
        {
            get
            {
                return this.fechadeTransitoField;
            }
            set
            {
                this.fechadeTransitoField = value;
                this.OnPropertyChanged();
            }
        }
    
        public TuristaPasajeroExtranjeroTipoTransito TipoTransito
        {
            get
            {
                return this.tipoTransitoField;
            }
            set
            {
                this.tipoTransitoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    public partial class TuristaPasajeroExtranjeroDatosTransito : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private TuristaPasajeroExtranjeroDatosTransitoVia viaField;
        private string tipoIdField;
        private string numeroIdField;
        private string nacionalidadField;
        private string empresaTransporteField;
        private string idTransporteField;
    
        public TuristaPasajeroExtranjeroDatosTransitoVia Via
        {
            get
            {
                return this.viaField;
            }
            set
            {
                this.viaField = value;
                this.OnPropertyChanged();
            }
        }
    
        public string TipoId
        {
            get
            {
                return this.tipoIdField;
            }
            set
            {
                this.tipoIdField = value;
                this.OnPropertyChanged();
            }
        }
    
        public string NumeroId
        {
            get
            {
                return this.numeroIdField;
            }
            set
            {
                this.numeroIdField = value;
                this.OnPropertyChanged();
            }
        }
    
        public string Nacionalidad
        {
            get
            {
                return this.nacionalidadField;
            }
            set
            {
                this.nacionalidadField = value;
                this.OnPropertyChanged();
            }
        }
    
        public string EmpresaTransporte
        {
            get
            {
                return this.empresaTransporteField;
            }
            set
            {
                this.empresaTransporteField = value;
                this.OnPropertyChanged();
            }
        }
    
        public string IdTransporte
        {
            get
            {
                return this.idTransporteField;
            }
            set
            {
                this.idTransporteField = value;
                this.OnPropertyChanged();
            }
        }
    }
}



