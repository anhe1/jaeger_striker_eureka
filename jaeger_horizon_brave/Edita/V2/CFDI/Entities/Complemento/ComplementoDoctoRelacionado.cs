﻿// purpose: para representar información basica de un comprobante relacionado a cualquier complemento.
// develop: anhe 161020181642
using System;
using System.Linq;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.CFDI.Entities.Complemento
{
    public class ComplementoDoctoRelacionado : ComplementoDoctoRelacionadoBase
    {
        private ComplementoDoctoRelacionadoRelacion tipoRelacionField;

        public ComplementoDoctoRelacionado()
        {
            this.tipoRelacionField = new ComplementoDoctoRelacionadoRelacion();
        }

        [JsonProperty("tipoRelacion")]
        public ComplementoDoctoRelacionadoRelacion TipoRelacion
        {
            get
            {
                return this.tipoRelacionField;
            }
            set
            {
                this.tipoRelacionField = value;
                this.OnPropertyChanged();
            }
        }

        public new string Json(Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public new static ComplementoDoctoRelacionado Json(string inputJson)
        {
            try
            {
                return JsonConvert.DeserializeObject<ComplementoDoctoRelacionado>(inputJson);
            }
            catch
            {
                return null;
            }
        }
    }
}
