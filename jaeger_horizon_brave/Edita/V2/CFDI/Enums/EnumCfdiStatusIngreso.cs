﻿using System;
using System.ComponentModel;

namespace Jaeger.Edita.V2.CFDI.Enums
{
    public enum EnumCfdiStatusIngreso
    {
        [Description("Cancelado")]
        Cancelado,
        [Description("Pendiente")]
        Pendiente,
        [Description("Importado")]
        Importado,
        [Description("Entregado")]
        Entregado,
        [Description("Por Cobrar")]
        PorCobrar,
        [Description("Cobrado")]
        Cobrado,
        [Description("Rechazado")]
        Rechazado
    }
}