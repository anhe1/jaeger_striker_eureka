using System.ComponentModel;

namespace Jaeger.Edita.V2.CFDI.Enums
{
    public enum EnumCfdiAddendas
    {
        [Description("AFASA Miscelanea")]
        AfasaMiscelanea,
        [Description("AFASA Recibo Fiscal")]
        AfasaReciboFiscal,
        [Description("Carta Porte")]
        CartaPorte
    }
}