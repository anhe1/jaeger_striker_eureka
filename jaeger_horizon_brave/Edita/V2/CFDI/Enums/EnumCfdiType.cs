﻿using System;
using System.Xml.Serialization;

namespace Jaeger.Edita.V2.CFDI.Enums
{
    public enum EnumCfdiType
    {
        [XmlEnum("I")]
        Ingreso,
        [XmlEnum("E")]
        Egreso,
        [XmlEnum("T")]
        Traslado,
        [XmlEnum("N")]
        Nomina,
        [XmlEnum("P")]
        Pagos
    }
}