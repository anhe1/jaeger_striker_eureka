﻿using System;

namespace Jaeger.Edita.V2.CFDI.Enums
{
    public enum EnumCfdiEstado
    {
        Todos,
        Vigente,
        Cancelado,
        NoEncontado
    }
}