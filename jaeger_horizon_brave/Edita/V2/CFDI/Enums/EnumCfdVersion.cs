﻿using System;

namespace Jaeger.Edita.V2.CFDI.Enums
{
    public enum EnumCfdVersion
    {
        None,
        V10,
        V13,
        V20,
        V22,
        V30,
        V32,
        V33,
        V40
    }
}