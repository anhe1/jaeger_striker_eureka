﻿using System;
using System.ComponentModel;

namespace Jaeger.Edita.V2.CFDI.Enums
{
    public enum EnumCfdiStatusEgreso
    {
        [Description("Cancelado")]
        Cancelado,
        [Description("Importado")]
        Importado,
        [Description("Entregado")]
        Entregado,
        [Description("Por Pagar")]
        PorPagar,
        [Description("Pagado")]
        Pagado,
        [Description("Rechazado")]
        Rechazado
    }
}