﻿using System;

namespace Jaeger.Edita.V2.CFDI.Enums
{
    public enum EnumCfdiSubType
    {
        None,
        Emitido,
        Recibido,
        Nomina
    }
}