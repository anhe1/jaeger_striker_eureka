using System.ComponentModel;

namespace Jaeger.Edita.V2.CFDI.Enums
{
    public enum EnumCfdiComplementos
    {
        [Description("Pago 10")]
        Pagos10,
        [Description("Leyendas Fiscales")]
        LeyendasFiscales,
        [Description("Nomina 1.1")]
        Nomina11,
        [Description("Nomina 1.2")]
        Nomina12,
        [Description("Vales Despensa")]
        ValesDeDespensa,
        [Description("Impuestos Locales")]
        ImpuestosLocales,
        [Description("Aerolineas")]
        Aerolineas
    }
}