﻿using System;
using System.ComponentModel;

namespace Jaeger.Edita.V2.CFDI.Enums
{
    public enum EnumCfdiDates
    {
        [Description("Todos")]
        None,
        [Description("Fecha Emisión")]
        FechaEmision,
        [Description("Fecha Timbre")]
        FechaTimbre,
        [Description("Fecha Pago")]
        FechaDePago,
        [Description("Fecha Validación")]
        FechaValidacion
    }
}