﻿using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Enums;
using System;

namespace Jaeger.Entities.Validador
{
    public class MetaDocument
    {
        private string strVersion;

        private EnumCfdVersion objVersion;

        private EnumMetadataType objMetaDoc;

        private string strError;

        public string Error
        {
            get
            {
                return this.strError;
            }
            set
            {
                this.strError = value;
            }
        }

        public EnumMetadataType MetaType
        {
            get
            {
                return this.objMetaDoc;
            }
            set
            {
                this.objMetaDoc = value;
            }
        }

        public EnumCfdVersion Version
        {
            get
            {
                return this.objVersion;
            }
            set
            {
                this.objVersion = value;
            }
        }

        public string VerText
        {
            get
            {
                return this.strVersion;
            }
            set
            {
                this.strVersion = value;
            }
        }

        public MetaDocument()
        {
            this.strVersion = string.Empty;
            this.objMetaDoc = EnumMetadataType.Other;
            this.strError = string.Empty;
        }

        public MetaDocument(EnumCfdVersion oVersion, EnumMetadataType oMeta, string strError = "")
        {
            this.strVersion = string.Empty;
            this.objMetaDoc = EnumMetadataType.Other;
            this.strError = string.Empty;
            this.objVersion = oVersion;
            this.objMetaDoc = oMeta;
            this.strError = strError;
        }
    }
}