using System.IO;

namespace Jaeger.Edita.V2.Validador.Entities
{
    /// <summary>
    /// informacion de archivo
    /// </summary>
    public class DocumentoAnexo : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string base64Field;
        private string contentField;
        private bool completedField;
        private FileInfo fileInfoField;
        private string urlField;
        private bool updatedField = false;

        public string FullFileName
        {
            get
            {
                return this.fileInfoField.FullName;
            }
            set
            {
                this.fileInfoField = new FileInfo(value);
                this.OnPropertyChanged();
            }
        }

        public string ContentFile
        {
            get
            {
                return this.contentField;
            }
            set
            {
                this.contentField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el comprobante en base 64
        /// </summary>
        public string B64
        {
            get
            {
                return this.base64Field;
            }
            set
            {
                this.base64Field = value;
                this.OnPropertyChanged();
            }
        }

        public bool Completed
        {
            get
            {
                return this.completedField;
            }
            set
            {
                this.completedField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si es necesario actualizar la url del comprobante
        /// </summary>
        public bool Updated
        {
            get
            {
                return this.updatedField;
            }
            set
            {
                this.updatedField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener nombre del archivo sin la ruta.
        /// </summary>
        public string FileName
        {
            get
            {
                return this.fileInfoField.Name;
            }
        }

        /// <summary>
        /// obtener extensi�n del archivo.
        /// </summary>
        public string FileExtension
        {
            get
            {
                return this.fileInfoField.Extension;
            }
        }

        public FileInfo FileInfo
        {
            get
            {
                return this.fileInfoField;
            }
        }

        public string URL
        {
            get
            {
                return this.urlField;
            }
            set
            {
                this.urlField = value;
                this.OnPropertyChanged();
            }
        }
    }
}