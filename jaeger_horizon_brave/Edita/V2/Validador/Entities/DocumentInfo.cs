﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Jaeger.Edita.V2.Validador.Entities
{
    [XmlRoot]
    public class DocumentInfo : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string fileNameField;
        private string fileExtField;
        private string contentTypeField;
        private bool completedField;
        private long fileSizeField;
        private string base64Field;

        [XmlElement]
        public string Base64
        {
            get
            {
                return this.base64Field;
            }
            set
            {
                this.base64Field = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtiene o establece si fue creado el backup del archivo
        /// </summary>
        [XmlAttribute]
        public bool Completed
        {
            get
            {
                return this.completedField;
            }
            set
            {
                this.completedField = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string ContentType
        {
            get
            {
                return this.contentTypeField;
            }
            set
            {
                this.contentTypeField = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string FileExt
        {
            get
            {
                return this.fileExtField;
            }
            set
            {
                this.fileExtField = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string FileName
        {
            get
            {
                return this.fileNameField;
            }
            set
            {
                this.fileNameField = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public long Size
        {
            get
            {
                return this.fileSizeField;
            }
            set
            {
                this.fileSizeField = value;
                this.OnPropertyChanged();
            }
        }

        public DocumentInfo()
        {
            this.fileNameField = string.Empty;
            this.fileExtField = string.Empty;
            this.contentTypeField = string.Empty;
            this.completedField = false;
            this.fileSizeField = 0;
        }

        public void Insert(string pFile)
        {
            FileInfo fileInfo = new FileInfo(pFile);
            this.FileName = fileInfo.FullName;
            this.FileExt = fileInfo.Extension;
            try
            {
                this.Size = fileInfo.Length;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Size = 0;
            }
        }
    }
}