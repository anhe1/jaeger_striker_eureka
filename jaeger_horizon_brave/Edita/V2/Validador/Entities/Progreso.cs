﻿using System;

namespace Jaeger.Edita.V2.Validador.Entities
{
    public class Progreso : EventArgs
    {
        public Progreso()
        {

        }

        public Progreso(int completado, int contador, string caption)
        {
            this.Completado = completado;
            this.Contador = contador;
            this.Caption = caption;
        }

        /// <summary>
        /// obtener o establecer el % completado
        /// </summary>
        public int Completado { get; set; }

        /// <summary>
        /// obtener o establecer el contador de items
        /// </summary>
        public int Contador { get; set; }

        /// <summary>
        /// obtener o establecer texto
        /// </summary>
        public string Caption { get; set; }
    }
}
