﻿using System;
using System.ComponentModel;
using System.Linq;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2.CFDI.Entities.Cancel;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Entities.Basico;
using Jaeger.Enums;

namespace Jaeger.Edita.V2.Validador.Entities {
    public class Documento : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation {
        private int indexField;
        private EnumCfdVersion enumVersionField;
        private EnumMetadataType enumMetaDataField;
        private DateTime? fechaField;
        private DateTime? fechaCertificacionField;
        private string tipoComprobanteField;
        private string idDocumentoField;
        private string estadoSATField;
        private string folioField;
        private string serieField;
        private string emisorRFCField;
        private string emisorField;
        private string receptorRFCField;
        private string receptorField;
        private string noCertificadoEmisorField;
        private string noCertificadoProvCertifField;
        private string rfcProvCertifField;
        private string lugarExpedicionField;
        private string metodoPagoField;
        private string formaPagoField;
        private string usoCFDIField;
        private decimal totalRetencionIsrField;
        private decimal totalRetencionIvaField;
        private decimal totalRetencionIepsField;
        private decimal totalTrasladoIvaField;
        private decimal totalTrasladoIepsField;
        private decimal subTotalField;
        private decimal descuentoField;
        private decimal totalField;
        private string complementosField;
        private BindingList<Concepto> conceptosField;
        private ComplementoPagos pagos10Field;
        private ComprobanteCfdiRelacionados cfdiRelacionadoField;
        private ValidateResponse validacionField;
        private CancelaCFDResponse acuseField;
        private DocumentoAnexo xmlField;
        private DocumentoAnexo pdfField;
        private DocumentoAnexo acuseXmlField;
        private DocumentoAnexo acusePdfField;
        private bool registradoField;

        public Documento() {
            this.Version = EnumCfdVersion.V33;
            this.MetaData = EnumMetadataType.CFDI;
            this.Conceptos = new BindingList<Concepto>();
        }

        public int Id {
            get {
                return this.indexField;
            }
            set {
                this.indexField = value;
            }
        }

        public EnumCfdVersion Version {
            get {
                return this.enumVersionField;
            }
            set {
                this.enumVersionField = value;
            }
        }

        public string VersionText {
            get {
                return Enum.GetName(typeof(EnumCfdVersion), this.Version);
            }
        }

        public EnumMetadataType MetaData {
            get {
                return this.enumMetaDataField;
            }
            set {
                this.enumMetaDataField = value;
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        public DateTime? Fecha {
            get {
                return this.fechaField;
            }
            set {
                this.fechaField = value;

            }
        }

        /// <summary>
        /// obtener o establecer la fecha de certificacion del comprobante
        /// </summary>
        public DateTime? FechaCertificacion {
            get {
                return this.fechaCertificacionField;
            }
            set {
                this.fechaCertificacionField = value;

            }
        }

        public string KeyName {
            get {
                string[] emisorRfc;
                if (this.FechaCertificacion != null) {
                    emisorRfc = new string[] { "CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.IdDocumento, "-", this.FechaCertificacion.Value.ToString("yyyyMMddHHmmss") };
                } else if (this.Fecha != null) {
                    emisorRfc = new string[] { "CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.IdDocumento, "-", this.Fecha.Value.ToString("yyyyMMddHHmmss") };
                } else {
                    emisorRfc = new string[] { "CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.IdDocumento, "-00000000000000" };
                }
                return string.Concat(emisorRfc);
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de comprobante (ingreso, egreso, pagos, traslado, nomina)
        /// </summary>
        public string TipoComprobante {
            get {
                return this.tipoComprobanteField;
            }
            set {
                this.tipoComprobanteField = value;

            }
        }

        public string IdDocumento {
            get {
                return this.idDocumentoField;
            }
            set {
                this.idDocumentoField = value.ToUpper();

            }
        }

        public string EstadoSAT {
            get {
                return this.estadoSATField;
            }
            set {
                this.estadoSATField = value;

            }
        }

        public string Folio {
            get {
                return this.folioField;
            }
            set {
                this.folioField = value;

            }
        }

        public string Serie {
            get {
                return this.serieField;
            }
            set {
                this.serieField = value;

            }
        }

        public string EmisorRFC {
            get {
                return this.emisorRFCField;
            }
            set {
                this.emisorRFCField = value;

            }
        }

        public string Emisor {
            get {
                return this.emisorField;
            }
            set {
                this.emisorField = value;

            }
        }

        public string EmisorRegimen { get; set; }

        public string ReceptorRFC {
            get {
                return this.receptorRFCField;
            }
            set {
                this.receptorRFCField = value;

            }
        }

        public string Receptor {
            get {
                return this.receptorField;
            }
            set {
                this.receptorField = value;

            }
        }

        public string ReceptorRegimen { get; set; }

        /// <summary>
        /// obtener o establacer el numero de certificado del emisor del comprobante
        /// </summary>
        public string NoCertificado {
            get {
                return this.noCertificadoEmisorField;
            }
            set {
                this.noCertificadoEmisorField = value;

            }
        }

        /// <summary>
        /// obtener o establecer el numero de certificado del proveedor de certificacion
        /// </summary>
        public string NoCertificadoProvCertif {
            get {
                return this.noCertificadoProvCertifField;
            }
            set {
                this.noCertificadoProvCertifField = value;

            }
        }

        /// <summary>
        /// obtener o establecer el RFC del proveedor de certificacion
        /// </summary>
        public string RFCProvCertif {
            get {
                return this.rfcProvCertifField;
            }
            set {
                this.rfcProvCertifField = value;

            }
        }

        /// <summary>
        /// obtener o establecer el lugar de expedicion de comprobante, en el caso de CFDI v33 es el codigo postal.
        /// </summary>
        public string LugarExpedicion {
            get {
                return this.lugarExpedicionField;
            }
            set {
                this.lugarExpedicionField = value;

            }
        }

        /// <summary>
        /// obtener o establecer clave del metodo de pago del comprobante.
        /// </summary>
        public string MetodoPago {
            get {
                return this.metodoPagoField;
            }
            set {
                this.metodoPagoField = value;

            }
        }

        /// <summary>
        /// obtener o establacer clave del metodo de pago del comprobante
        /// </summary>
        public string FormaPago {
            get {
                return this.formaPagoField;
            }
            set {
                this.formaPagoField = value;

            }
        }

        [Newtonsoft.Json.JsonIgnore]
        public string MetodoVsForma {
            get {
                if (this.Version == EnumCfdVersion.V33 | this.Version == EnumCfdVersion.V40) {
                    if (this.TipoComprobante != "Pagos" && this.TipoComprobante != "Nomina" && this.TipoComprobante != "Egreso") {
                        if (this.MetodoPago == "PUE" && this.FormaPago != "99") {
                            return "Correcto";
                        } else if (this.MetodoPago == "PPD" && this.FormaPago == "99") {
                            return "Correcto";
                        }
                        return "No coincide";
                    }
                }
                return "N/A";
            }
        }

        /// <summary>
        /// obtener o establacer clave de uso de CFDI del receptor del comprobante
        /// </summary>
        public string UsoCFDI {
            get {
                return this.usoCFDIField;
            }
            set {
                this.usoCFDIField = value;

            }
        }

        public decimal TotalRetencionISR {
            get {
                return this.totalRetencionIsrField;
            }
            set {
                this.totalRetencionIsrField = value;
            }
        }

        public decimal TotalRetencionIVA {
            get {
                return this.totalRetencionIvaField;
            }
            set {
                this.totalRetencionIvaField = value;
            }
        }

        public decimal TotalRetencionIEPS {
            get {
                return this.totalRetencionIepsField;
            }
            set {
                this.totalRetencionIepsField = value;
            }
        }

        public decimal TotalTrasladoIVA {
            get {
                return this.totalTrasladoIvaField;
            }
            set {
                this.totalTrasladoIvaField = value;
            }
        }

        public decimal TotalTrasladoIEPS {
            get {
                return this.totalTrasladoIepsField;
            }
            set {
                this.totalTrasladoIepsField = value;
            }
        }

        public decimal SubTotal {
            get {
                return this.subTotalField;
            }
            set {
                this.subTotalField = value;
            }
        }

        public decimal Descuento {
            get {
                return this.descuentoField;
            }
            set {
                this.descuentoField = value;

            }
        }

        /// <summary>
        /// obtener o establecer el total de comprobante
        /// </summary>
        public decimal Total {
            get {
                return this.totalField;
            }
            set {
                this.totalField = value;

            }
        }

        /// <summary>
        /// lista de complementos
        /// </summary>
        public string Complementos {
            get {
                return this.complementosField;
            }
            set {
                this.complementosField = value;

            }
        }

        public BindingList<Concepto> Conceptos {
            get {
                return this.conceptosField;
            }
            set {
                this.conceptosField = value;

            }
        }

        public ComplementoPagos ComplementoPago {
            get {
                return this.pagos10Field;
            }
            set {
                this.pagos10Field = value;

            }
        }

        public Jaeger.Edita.V2.CFDI.Entities.ComprobanteCfdiRelacionados CFDIRelacionado {
            get {
                return this.cfdiRelacionadoField;
            }
            set {
                this.cfdiRelacionadoField = value;

            }
        }

        public ValidateResponse Validacion {
            get {
                return this.validacionField;
            }
            set {
                this.validacionField = value;

            }
        }

        public CancelaCFDResponse Acuse {
            get {
                return this.acuseField;
            }
            set {
                this.acuseField = value;

            }
        }

        public DocumentoAnexo ArchivoXML {
            get {
                return this.xmlField;
            }
            set {
                this.xmlField = value;

            }
        }

        /// <summary>
        /// obtener la ruta completo del archivo XML
        /// </summary>
        public string XML {
            get {
                return (this.ArchivoXML == null ? "" : this.ArchivoXML.FullFileName);
            }
        }

        public DocumentoAnexo ArchivoPDF {
            get {
                return this.pdfField;
            }
            set {
                this.pdfField = value;

            }
        }

        /// <summary>
        /// obtener la ruta completa del archivo PDF
        /// </summary>
        public string PDF {
            get {
                return (this.ArchivoPDF == null ? "" : this.ArchivoPDF.FullFileName);
            }
        }

        public DocumentoAnexo AcuseXML {
            get {
                return this.acuseXmlField;
            }
            set {
                this.acuseXmlField = value;

            }
        }

        public DocumentoAnexo AcusePDF {
            get {
                return this.acusePdfField;
            }
            set {
                this.acusePdfField = value;

            }
        }

        /// <summary>
        /// bandera para indicar si el comprobante ya habia sido registrado.
        /// </summary>
        public bool Registrado {
            get {
                return this.registradoField;
            }
            set {
                this.registradoField = value;

            }
        }

        /// <summary>
        /// obtener lista de claves de unidad
        /// </summary>
        public string ClaveUnidad {
            get {
                string claveUnidad = string.Join(",",
                                                     from p in this.conceptosField
                                                     select p.ClaveUnidad);
                return claveUnidad;
            }
        }

        /// <summary>
        /// obtener lista de claves de conceptos
        /// </summary>
        public string ClaveConceptos {
            get {
                string claveConceptos = string.Join(", ",
                                                         from p in this.conceptosField
                                                         select string.Concat(p.ClaveProdServ, "-", p.Descripcion));
                return claveConceptos;
            }
        }

        /// <summary>
        /// comprobar rfc del emisor del comprobante en el catalog 69B de operaciones simuladas
        /// </summary>
        public string Situacion {
            get {
                if (this.validacionField != null) {
                    try {

                        //var d1 = this.validacionField.Validations.Where(it => it != null).Where(it => it.Name.Contains("69B")).Select(it => it.Value).FirstOrDefault().ToString();
                        var d1 = this.validacionField.Validations.Where(it => it != null).Where(it => it.Name.Contains("69B")).FirstOrDefault();
                        if (d1 != null)
                            return d1.Value;
                    } catch (Exception ex) {
                        Console.WriteLine("Situacion: " + ex.Message);
                        return "";
                    }
                }
                return "Sin Comprobar";
            }
        }

        public string DomicilioFiscal { get; set; }

        public EnumValidateResult Resultado {
            get {
                return (this.validacionField == null ? EnumValidateResult.EnEspera : this.validacionField.IsValid);
            }
        }
    }
}
