﻿using Newtonsoft.Json;
using System.ComponentModel;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Edita.V2.Validador.Entities
{
    [JsonObject]
    public class Configuracion : BasePropertyChangeImplementation
    {
        #region declaraciones

        private TipoValidacion tipoField;
        private string lugarExpedicionField;
        private bool modoEstrictoField;
        private bool validarModoParaleloField;
        private bool renombrarOrigenField;
        private bool localizarPDFField;
        private bool estadoSAT;
        private bool descargaCertificados;
        private bool metodoPago;
        private bool formaPago;

        #endregion

        public enum TipoValidacion
        {
            Rapida,
            Optima,
            Estricto
        }

        public Configuracion()
        {
            this.Estricto = true;
            this.ModoParalelo = true;
            this.RenombrarOrigen = false;
            this.LocalizarPDF = false;
        }

        [JsonProperty("tipo")]
        [DisplayName("Tipo de Validación")]
        public TipoValidacion Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("modoParalelo")]
        [DisplayName("Modo paralelo"), Description("El modo de validación")]
        public bool ModoParalelo
        {
            get
            {
                return this.validarModoParaleloField;
            }
            set
            {
                this.validarModoParaleloField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("estricto")]
        [DisplayName("Estricto"), Description("")]
        public bool Estricto
        {
            get
            {
                return this.modoEstrictoField;
            }
            set
            {
                this.modoEstrictoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("estadoSAT")]
        [DisplayName("Estado SAT"), Description("Verificar el estado del comprobante SAT.")]
        public bool EstadoSAT
        {
            get
            {
                return this.estadoSAT;
            }
            set
            {
                this.estadoSAT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer expresion regular para validar lugar de expedición, usado para la versión 3.2
        /// </summary>
        [JsonProperty("expedicion")]
        [DisplayName("Lugar de Expedición"), Description("Expresión regular para la comprobación del codigo postal ó lugar de expedición")]
        public string LugarExpedicion
        {
            get
            {
                return this.lugarExpedicionField;
            }
            set
            {
                this.lugarExpedicionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si deberan renombrarse los archivos origen con el KeyName
        /// </summary>
        [JsonProperty("renombrar")]
        [DisplayName("Renombrar Origen"), Description("Renombrar los archivos de origen.")]
        public bool RenombrarOrigen
        {
            get
            {
                return this.renombrarOrigenField;
            }
            set
            {
                this.renombrarOrigenField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer si se escanea el directorio para obtener las representaciones impresas de los comprobantes
        /// </summary>
        [JsonProperty("pdf")]
        [DisplayName("Buscar PDF")]
        public bool LocalizarPDF
        {
            get
            {
                return this.localizarPDFField;
            }
            set
            {
                this.localizarPDFField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("certificados")]
        [DisplayName("Decarga de Cetificados"), Description("Descarga automática de certificados para validación")]
        public bool DescargaCertificados
        {
            get { return descargaCertificados; }
            set
            {
                descargaCertificados = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("metodoPago")]
        [DisplayName("Método de Pago"), Description("Validar clave SAT de Método de pago")]
        public bool MetodoPago
        {
            get { return metodoPago; }
            set
            {
                metodoPago = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("formaPago")]
        [DisplayName("Forma de Pago"), Description("Validar clave SAT de forma de pago.")]
        public bool FormaPago
        {
            get { return formaPago; }
            set
            {
                formaPago = value;
                this.OnPropertyChanged();
            }
        }

        private bool claveConcepto;
        [JsonProperty("claveProductoServicio")]
        [DisplayName("Cat. Producto / Servicios"), Description("Identificar la clave del producto o servicio")]
        public bool ClaveProductoServicio
        {
            get { return claveConcepto; }
            set
            {
                claveConcepto = value;
                this.OnPropertyChanged();
            }
        }

        private bool claveUnidad;
        [JsonProperty("claveUnidad")]
        [DisplayName("Cat. Unidad de Medida"), Description("Identificar la clave de unidades de medida.")]
        public bool ClaveUnidad
        {
            get { return claveUnidad; }
            set
            {
                claveUnidad = value;
                this.OnPropertyChanged();
            }
        }

        private bool articulo69b;
        [JsonProperty("articulo69b")]
        [DisplayName("Artículo 69B FF"), Description("Validar si el contribuyente se ubica en el listado SAT presunción de llevar a cabo operaciones inexistentes a través de la emisión de facturas o comprobantes fiscales.")]
        public bool Articulo69B
        {
            get { return articulo69b; }
            set
            {
                articulo69b = value;
                this.OnPropertyChanged();
            }
        }

        private bool claveUsoCFDI;

        public bool ClaveUsoCFDI
        {
            get { return claveUsoCFDI; }
            set
            {
                claveUsoCFDI = value;
                this.OnPropertyChanged();
            }
        }

        public string Json()
        {
            return JsonConvert.SerializeObject(this, Formatting.None);
        }

        public static Configuracion Json(string inputString)
        {
            return JsonConvert.DeserializeObject<Configuracion>(inputString);
        }
    }
}
