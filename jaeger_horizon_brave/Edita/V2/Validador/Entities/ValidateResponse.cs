﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Jaeger.Enums;

namespace Jaeger.Edita.V2.Validador.Entities
{
    [JsonObject]
    public class ValidateResponse : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string objService;
        private EnumValidateResult blnIsValid;
        private string strVersion;
        private string strEffect;
        private string strUuid;
        private string strFolio;
        private string strSerieName;
        private string strEmisor;
        private string strEmisorRfc;
        private string strCustomer;
        private string strCustomerRfc;
        private string strTimeElapsed;
        private string strProofStatus;
        private DateTime date;
        private DateTime dateValidation;
        private decimal decTotal;
        private List<ValidationProperty> lstValidation;
        private List<Jaeger.Entities.Property> lstErrors;

        /// <summary>
        /// contructor
        /// </summary>
        public ValidateResponse()
        {
            this.objService = String.Empty;
            this.blnIsValid = EnumValidateResult.EnEspera;
            this.strVersion = string.Empty;
            this.strEffect = string.Empty;
            this.strUuid = string.Empty;
            this.strFolio = string.Empty;
            this.strSerieName = string.Empty;
            this.strEmisor = string.Empty;
            this.strEmisorRfc = string.Empty;
            this.strCustomer = string.Empty;
            this.strCustomerRfc = string.Empty;
            this.strTimeElapsed = string.Empty;
            this.strProofStatus = string.Empty;
            this.date = DateTime.Now;
            this.dateValidation = DateTime.Now;
            this.decTotal = new decimal();
            this.lstValidation = new List<Jaeger.Edita.V2.Validador.Entities.ValidationProperty>();
            this.lstErrors = new List<Jaeger.Entities.Property>();
        }

        [JsonProperty("valido")]
        [XmlAttribute("Valido")]
        public EnumValidateResult IsValid
        {
            get
            {
                return this.blnIsValid;
            }
            set
            {
                this.blnIsValid = value;
            }
        }

        [JsonIgnore]
        [XmlIgnore]
        public string IsValidText
        {
            get
            {
                return Enum.GetName(typeof(EnumValidateResult), this.blnIsValid);
            }
            set
            {
                this.blnIsValid = (EnumValidateResult)(Enum.Parse(typeof(EnumValidateResult), value));
            }
        }

        [JsonProperty("provider")]
        [XmlAttribute("Provider")]
        public string Provider
        {
            get
            {
                return this.objService;
            }
            set
            {
                this.objService = value;
            }
        }

        /// <summary>
        /// obtener o establecer la versión del estándar bajo el que se encuentra expresado el comprobante.
        /// </summary>
        [JsonProperty("version")]
        [XmlAttribute("Version")]
        public string Version
        {
            get
            {
                return this.strVersion;
            }
            set
            {
                this.strVersion = value;
            }
        }

        [JsonProperty("efecto")]
        [XmlAttribute("Efecto")]
        public string Effect
        {
            get
            {
                return this.strEffect;
            }
            set
            {
                this.strEffect = value;
            }
        }

        /// <summary>
        /// obtener o establecer folio de control interno del contribuyente.
        /// </summary>
        [JsonProperty("folio")]
        [XmlAttribute("Folio")]
        public string Folio
        {
            get
            {
                return this.strFolio;
            }
            set
            {
                this.strFolio = value;
            }
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. 
        /// </summary>
        [JsonProperty("serie")]
        [XmlAttribute("Serie")]
        public string Serie
        {
            get
            {
                return this.strSerieName;
            }
            set
            {
                this.strSerieName = value;
            }
        }

        [JsonProperty("fecha")]
        [XmlAttribute("Fecha")]
        public DateTime DateTime
        {
            get
            {
                return this.date;
            }
            set
            {
                this.date = value;
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de validación
        /// </summary>
        [JsonProperty("fechaValidacion")]
        [XmlAttribute("FechaValidacion")]
        public DateTime FechaValidacion
        {
            get
            {
                return this.dateValidation;
            }
            set
            {
                this.dateValidation = value;
            }
        }

        [JsonProperty("uuid")]
        [XmlAttribute("UUID")]
        public string Uuid
        {
            get
            {
                return this.strUuid;
            }
            set
            {
                this.strUuid = value;
            }
        }

        /// <summary>
        /// obtener o establecer el nombre, denominación o razón social del contribuyente emisor del comprobante
        /// </summary>
        [JsonProperty("emisor")]
        [XmlAttribute("Emisor")]
        public string IssueName
        {
            get
            {
                return this.strEmisor;
            }
            set
            {
                this.strEmisor = value;
            }
        }

        /// <summary>
        /// obtener o establecer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente emisor del comprobante
        /// </summary>
        [JsonProperty("emisorRFC")]
        [XmlAttribute("EmisorRFC")]
        public string IssueRfcId
        {
            get
            {
                return this.strEmisorRfc;
            }
            set
            {
                this.strEmisorRfc = value;
            }
        }

        [JsonProperty("receptor")]
        [XmlAttribute("Receptor")]
        public string CustomerName
        {
            get
            {
                return this.strCustomer;
            }
            set
            {
                this.strCustomer = value;
            }
        }

        /// <summary>
        /// obtener o establecer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente receptor del comprobante.
        /// </summary>
        [JsonProperty("receptorRFC")]
        [XmlAttribute("ReceptorRFC")]
        public string CustomerRfcId
        {
            get
            {
                return this.strCustomerRfc;
            }
            set
            {
                this.strCustomerRfc = value;
            }
        }

        [JsonProperty("timeElapsed")]
        [XmlAttribute("TimeElapsed")]
        public string TimeElapsed
        {
            get
            {
                return this.strTimeElapsed;
            }
            set
            {
                this.strTimeElapsed = value;
            }
        }

        [JsonProperty("total")]
        [XmlAttribute("Total")]
        public decimal Total
        {
            get
            {
                return this.decTotal;
            }
            set
            {
                this.decTotal = value;
            }
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante
        /// </summary>
        [JsonProperty("status")]
        [XmlAttribute("Status")]
        public string ProofStatus
        {
            get
            {
                return this.strProofStatus;
            }
            set
            {
                this.strProofStatus = value;
            }
        }

        /// <summary>
        /// obtener o establecer el listado de validaciones del comprobante
        /// </summary>
        [JsonProperty("validation")]
        [XmlElement("Validation")]
        public List<Jaeger.Edita.V2.Validador.Entities.ValidationProperty> Validations
        {
            get
            {
                return this.lstValidation;
            }
            set
            {
                this.lstValidation = value;
            }
        }

        [JsonProperty("errors")]
        [XmlElement("Errors")]
        public List<Jaeger.Entities.Property> Errors
        {
            get
            {
                return this.lstErrors;
            }
            set
            {
                this.lstErrors = value;
            }
        }
        
        public string Json(Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static ValidateResponse Json(string inputJson)
        {
            return JsonConvert.DeserializeObject<ValidateResponse>(inputJson);
        }

        public string Xml()
        {
            return Jaeger.Helpers.HelperSerializer.SerializeObject<ValidateResponse>(this);
        }

        public ValidateResponse Xml(string xmlString)
        {
            return Jaeger.Helpers.HelperSerializer.DeserializeObject<ValidateResponse>(xmlString);
        }
    }
}