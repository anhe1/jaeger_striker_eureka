﻿using Jaeger.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using Jaeger.Enums;

namespace Jaeger.Edita.V2.Validador.Entities
{
    [JsonObject]
    public class ValidationProperty : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private EnumPropertyType enumType;
        private string code;
        private string name;
        private string value;
        private bool valid;
        private List<Property> objErrors;

        public ValidationProperty()
        {
            this.enumType = EnumPropertyType.Information;
            this.code = string.Empty;
            this.name = string.Empty;
            this.value = string.Empty;
            this.valid = true;
            this.objErrors = new List<Property>();
        }

        [DisplayName("Codigo")]
        [JsonProperty("code")]
        [XmlAttribute("Code")]
        public string Code
        {
            get
            {
                return this.code;
            }
            set
            {
                this.code = value;
            }
        }

        [DisplayName("Errores")]
        [JsonProperty("errors")]
        [XmlElement("Errors")]
        public List<Property> Errors
        {
            get
            {
                return this.objErrors;
            }
            set
            {
                this.objErrors = value;
                this.OnPropertyChanged("Errors");
            }
        }

        [DisplayName("Nombre")]
        [JsonProperty("name")]
        [XmlAttribute("Name")]
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
                this.OnPropertyChanged("Name");
            }
        }

        [DisplayName("Tipo")]
        [JsonProperty("type")]
        [XmlAttribute("Type")]
        public EnumPropertyType Type
        {
            get
            {
                return this.enumType;
            }
            set
            {
                this.enumType = value;
                this.OnPropertyChanged("Type");
            }
        }

        [DisplayName("Valido")]
        [JsonProperty("valid")]
        [XmlAttribute("Valid")]
        public bool Valid
        {
            get
            {
                return this.valid;
            }
            set
            {
                this.valid = value;
                this.OnPropertyChanged("Valid");
            }
        }

        [DisplayName("Valor")]
        [JsonProperty("value")]
        [XmlAttribute("Value")]
        public string Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
                this.OnPropertyChanged("Value");
            }
        }

        [DisplayName("Tag1")]
        [JsonIgnore]
        [XmlIgnore]
        public string Tag1 { get; set; }
    }
}