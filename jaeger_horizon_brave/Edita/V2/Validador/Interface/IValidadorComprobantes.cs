/// develop: anhe1 251020182320
/// purpose: interface para validación de comprobantes fiscales
using System.Collections.Generic;
using Jaeger.Edita.Entities.Validador;
using Jaeger.Entities.Validador;

namespace Jaeger.Edita.V2.Validador.Interface
{
    public interface IValidadorComprobantes
    {
        ValidadorConfiguracion Configuracion { get; set; }

        ValidationProperty Esquemas(object objeto);

        ValidationProperty Estructura(object objeto, byte[] b);

        List<ValidationProperty> SelloCFDI(object objeto);

        ValidationProperty SelloSAT(object objeto);

        ValidationProperty MetodoPago(object objeto);

        ValidationProperty FormaPago(object objeto);

        ValidationProperty EstadoSAT(object objeto);

        ValidationProperty LugarExpedicion(object objeto);

        ValidationProperty UsodeCFDI(object objeto);

        List<ValidationProperty> ProductosServicios(object objeto);
    }
}