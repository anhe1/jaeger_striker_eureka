﻿using System;
using System.Linq;

namespace Jaeger.Edita.V2.Validador.Interface
{
    public interface IHelperCatalogo
    {
        object Search(string findId);
    }
}
