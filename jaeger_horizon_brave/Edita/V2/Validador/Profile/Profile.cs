﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.Validador.Profile
{
    public class Profile : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        public static string ProfileFile = "jaeger_striker_eureka_profile_.json";
        public static string Password = "$ipr981125pn9";

        private string nameField;
        private List<ProfileTab> profileTabsField;

        public Profile()
        {
        }

        [DisplayName("Nombre"), Description("Nombre asignado al perfil.")]
        [JsonProperty("name", Order = 0)]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Pestañas"), Description("Lista de pestañas de opciones del programa.")]
        [JsonProperty("tabs", Order = 1)]
        public List<ProfileTab> ProfileTabs
        {
            get
            {
                return this.profileTabsField;
            }
            set
            {
                this.profileTabsField = value;
                this.OnPropertyChanged();
            }
        }

        public string Json()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public static Profile Load(string fileName = "")
        {
            return null;
        }

        /// <summary>
        /// almacenar configuración en archivo
        /// </summary>
        /// <param name="fileName">nombre del archivo de salida</param>
        /// <param name="bEncrypt">si debe ser encriptado</param>
        /// <returns>verdadero al crear correctamente el archivo</returns>
        public bool Save(string fileName = "", bool bEncrypt = false)
        {
            bool save;
            fileName = (fileName == "" ? Profile.ProfileFile : fileName);
            StreamWriter streamWriter = new StreamWriter(fileName, false);
            try
            {
                if (!bEncrypt)
                {
                    streamWriter.Write(this.Json());
                }
                else
                {
                    //streamWriter.Write(CsTripleDes.Encrypt(this.ToJson(), Profile.Password, true));
                }
                streamWriter.Close();
                save = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                save = false;
            }
            return save;
        }
    }
}
