﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.Validador.Profile
{
    public class ProfileTab : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string nombreField;
        private bool esVisibleField;
        private List<ProfileGroup> gruposField;

        [DisplayName("Nombre")]
        [JsonProperty("name", Order = 0)]
        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Visible")]
        [JsonProperty("visible", Order = 1)]
        public bool EsVisible
        {
            get
            {
                return this.esVisibleField;
            }
            set
            {
                this.esVisibleField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Grupos")]
        [JsonProperty("groups", Order = 2)]
        public List<ProfileGroup> Grupos
        {
            get
            {
                return this.gruposField;
            }
            set
            {
                this.gruposField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
