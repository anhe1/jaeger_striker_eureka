﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.Validador.Profile
{
    public class ProfileGroup : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string nombreField;
        private bool esVisibleField;
        private List<ProfileButton> botonesField;
        private List<ProfileGroup> gruposField;

        [DisplayName("Nombre del Grupo")]
        [JsonProperty("name", Order = 0)]
        public string Name
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
            }
        }

        [JsonProperty("visible", Order = 1)]
        public bool IsVisible
        {
            get
            {
                return this.esVisibleField;
            }
            set 
            {
                this.esVisibleField = value;
            }
        }

        [DisplayName("Botones")]
        [JsonProperty("buttons", Order = 2)]
        public List<ProfileButton> Botones
        {
            get
            {
                return this.botonesField;
            }
            set
            {
                this.botonesField = value;
            }
        }

        [DisplayName("SubGrupos")]
        [JsonProperty("subGrupos", Order = 2, NullValueHandling = NullValueHandling.Ignore)]
        public List<ProfileGroup> Grupos
        {
            get
            {
                return this.gruposField;
            }
            set
            {
                this.gruposField = value;
            }
        }
    }
}
