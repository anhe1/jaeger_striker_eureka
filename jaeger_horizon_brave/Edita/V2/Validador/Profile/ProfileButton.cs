﻿using System;
using System.Linq;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.Validador.Profile
{
    public class ProfileButton : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string nameField;
        private string captionField;
        private bool isVisibleField;
        private bool enabledField;
        private bool exportField;
        private bool newField;
        private bool editField;
        private bool deleteField;

        public ProfileButton()
        {
            this.nameField="Titulo";
            this.captionField = "Titulo";
            this.isVisibleField = true;
            this.enabledField = true;
            this.exportField = true;
            this.newField = false;
            this.editField = false;
            this.deleteField = false;
        }

        [Category("General"), DisplayName("Nombre"), ReadOnly(true)]
        [JsonProperty("name", Order = 0)]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("General"), DisplayName("Etiqueta")]
        [JsonProperty("label", Order = 1)]
        public string Caption
        {
            get
            {
                return this.captionField;
            }
            set
            {
                this.captionField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("General"), DisplayName("Visible")]
        [JsonProperty("visible", Order = 2)]
        public bool IsVisible
        {
            get
            {
                return this.isVisibleField;
            }
            set
            {
                this.isVisibleField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("General"), DisplayName("Activo")]
        [JsonProperty("enabled", Order = 3)]
        public bool Enabled
        {
            get
            {
                return this.enabledField;
            }
            set
            {
                this.enabledField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("Acciones"), DisplayName("Exportación"), Description("Permite la libre exportación de la información a formatos soportados.")]
        [JsonProperty("export", Order = 4)]
        public bool Export
        {
            get
            {
                return this.exportField;
            }
            set
            {
                this.exportField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("Acciones"), DisplayName("Nuevo"), Description("Permite la crear un registro nuevo.")]
        [JsonProperty("new", Order = 5)]
        public bool New
        {
            get
            {
                return this.newField;
            }
            set
            {
                this.newField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("Acciones"), DisplayName("Editar"), Description("Permite la edicion del registro activo.")]
        [JsonProperty("edit", Order = 6)]
        public bool Edit
        {
            get
            {
                return this.editField;
            }
            set
            {
                this.editField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("Acciones"), DisplayName("Eliminar"), Description("Permite eliminar el registro activo.")]
        [JsonProperty("delete", Order = 7)]
        public bool Delete
        {
            get
            {
                return this.deleteField;
            }
            set
            {
                this.deleteField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
