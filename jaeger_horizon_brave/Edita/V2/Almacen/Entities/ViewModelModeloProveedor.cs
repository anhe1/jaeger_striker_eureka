﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.Almacen.Entities
{
    /// <summary>
    /// clase de los proveedores del modelo del producto o servicio
    /// </summary>
    [SugarTable("_ctlprv", "alm:|mp| catalogo de proveedores del almacen de materia prima")]
    public class ViewModelModeloProveedor : BasePropertyChangeImplementation
    {
        private int index;
        private int subindex;
        private int idProveedor;
        private bool activo = true; 
        private decimal unitario;
        private string nombre;
        private string unidad;
        private string nota;
        private string creo;
        private string modifica;
        private string moneda;
        private DateTime fechaEmision;
        private DateTime? fechaModifica;

        public ViewModelModeloProveedor()
        {
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice prinpal de la tabla
        /// </summary>
        [DataNames("_ctlprv_id")]
        [SugarColumn(ColumnName = "_ctlprv_id", ColumnDescription = "indice principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id
        {
            get
            {
                return this.index;
            }
            set
            {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        [DataNames("_ctlprv_a")]
        [SugarColumn(ColumnName = "_ctlprv_a", ColumnDescription = "registro activo", Length = 1, DefaultValue = "1", IsNullable = false)]
        public bool Activo
        {
            get
            {
                return this.activo;
            }
            set
            {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del modelo del producto
        /// </summary>
        [DataNames("_ctlprv_mdl_id")]
        [SugarColumn(ColumnName = "_ctlprv_mdl_id", ColumnDescription = "indice de relacion con el modelo del producto")]
        public int IdModelo
        {
            get
            {
                return this.subindex;
            }
            set
            {
                this.subindex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del proveedor en el directorio
        /// </summary>
        [DataNames("_ctlprv_drctr_id")]
        [SugarColumn(ColumnName = "_ctlprv_drctr_id", ColumnDescription = "indice de relacion con el directorio de proveedores")]
        public int IdProveedor
        {
            get
            {
                return this.idProveedor;
            }
            set
            {
                this.idProveedor = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlprv_nom")]
        [SugarColumn(IsIgnore = true, ColumnName = "_ctlprv_nom", ColumnDescription = "nombre del proveedor", Length = 255)]
        public string Nombre
        {
            get
            {
                return this.nombre;
            }
            set
            {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor unitario del producto ó modelo
        /// </summary>
        [DataNames("_ctlprv_untr")]
        [SugarColumn(ColumnName = "_ctlprv_untr", ColumnDescription = "alm:|mp,pt,tw| desc: precio unitario", Length = 11, DecimalDigits = 4)]
        public decimal Unitario
        {
            get
            {
                return this.unitario;
            }
            set
            {
                this.unitario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de surtimento 
        /// </summary>
        [DataNames("_ctlprv_undd")]
        [SugarColumn(ColumnName = "_ctlprv_undd", ColumnDescription = "alm:|mp,pt,tw| desc: unidad personalizada", Length = 20, IsNullable = true)]
        public string Unidad
        {
            get
            {
                return this.unidad;
            }
            set
            {
                this.unidad = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlprv_mnd")]
        [SugarColumn(ColumnName = "_ctlprv_mnd", ColumnDescription = "clave de moneda utilizada", Length = 20, IsNullable = true)]
        public string Moneda
        {
            get
            {
                return this.moneda;
            }
            set
            {
                this.moneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nota
        /// </summary>
        [DataNames("_ctlprv_obs")]
        [SugarColumn(ColumnName = "_ctlprv_obs", ColumnDescription = "notas", Length = 32, IsNullable = true)]
        public string Nota
        {
            get
            {
                return this.nota;
            }
            set
            {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("_ctlprv_fn")]
        [SugarColumn(ColumnName = "_ctlprv_fn", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaEmision;
            }
            set
            {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("_ctlprv_fm")]
        [SugarColumn(ColumnName = "_ctlprv_fm", ColumnDescription = "ultima fecha de modificacion del regidstro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set
            {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        [DataNames("_ctlprv_usr_n")]
        [SugarColumn(ColumnName = "_ctlprv_usr_n", ColumnDescription = "clave del usuario que crea el registro", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo
        {
            get
            {
                return this.creo;
            }
            set
            {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("_ctlprv_usr_m")]
        [SugarColumn(ColumnName = "_ctlprv_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifico
        {
            get
            {
                return this.modifica;
            }
            set
            {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
