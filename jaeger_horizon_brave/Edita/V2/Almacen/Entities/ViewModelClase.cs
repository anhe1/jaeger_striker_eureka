﻿using System.ComponentModel;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Edita.V2.Almacen.Entities
{
    /// <summary>
    /// clase que contiene un elemento de la clasificacion general
    /// </summary>
    public class ViewModelClase : BasePropertyChangeImplementation
    {
        private BindingList<ViewModelClasificacionG> clases;

        public ViewModelClase()
        {
            this.clases = new BindingList<ViewModelClasificacionG>() { RaiseListChangedEvents = true };
            this.clases.AddingNew += Clases_AddingNew;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// obtener o establecer el sub indice de relacion
        /// </summary>
        public int SubId { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string Tipo { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string Clase { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer ruta completa creada
        /// </summary>
        public string Resume
        {
            get
            {
                return string.Format("{0} - {1} - {2}", this.Tipo, this.Clase, this.Descripcion);
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public BindingList<ViewModelClasificacionG> Clases
        {
            get
            {
                return this.clases;
            }
            set
            {
                if (this.clases != null)
                    this.clases.AddingNew -= new AddingNewEventHandler(this.Clases_AddingNew);
                this.clases = value;
                if (this.clases != null)
                    this.clases.AddingNew += new AddingNewEventHandler(this.Clases_AddingNew);
            }
        }

        private void Clases_AddingNew(object sender, AddingNewEventArgs e)
        {
            e.NewObject = new ViewModelClasificacionG { SubId = this.Id };
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", this.Tipo, this.Clase, this.Descripcion);
        }
    }
}
