using System;
using System.ComponentModel;
using Jaeger.Edita.V2.Almacen.Enums;
namespace Jaeger.Edita.V2.Almacen.Entities
{
    public class ProductoModelo : ViewModelProductoServicioSingle, ICloneable
    {
        public ProductoModelo()
        {
            this.Modelo = new ViewModelModelo();
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        public new int IdProducto
        {
            get
            {
                return base.IdProducto;
            }
            set
            {
                base.IdProducto = value;
                this.Modelo.IdProducto = value;
            }
        }

        /// <summary>
        /// obtener o establecer el indice del modelo
        /// </summary>
        public int IdModelo
        {
            get
            {
                return this.Modelo.IdModelo;
            }
            set
            {
                this.Modelo.IdModelo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro esta activo
        /// </summary>
        public new bool Activo
        {
            get
            {
                return base.Activo;
            }
            set
            {
                base.Activo = value;
                this.Modelo.Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion de la categoria
        /// </summary>
        public new int IdCategoria
        {
            get
            {
                return base.IdCategoria;
            }
            set
            {
                base.IdCategoria = value;
                this.Modelo.IdCategoria = value;
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de almacen al que pertenece el producto
        /// </summary>
        public new EnumAlmacen Almacen
        {
            get
            {
                return base.Almacen;
            }
            set
            {
                base.Almacen = value;
                this.Modelo.Almacen = value;
            }
        }

        /// <summary>
        /// Desc:(1-producto, 2-servicio, 3-kit, 4-grupo de productos)
        /// Default:1
        /// Nullable:True
        /// </summary>           
        public new EnumProdServTipo Tipo
        {
            get
            {
                return base.Tipo;
            }
            set
            {
                base.Tipo = value;
                this.Modelo.Tipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad utilizada en largo y ancho
        /// </summary>
        public int UnidadXY
        {
            get
            {
                return this.Modelo.UnidadXY;
            }
            set
            {
                this.Modelo.UnidadXY = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad utilizada en alto o calibre
        /// </summary>
        public int UnidadZ
        {
            get
            {
                return this.Modelo.UnidadZ;
            }
            set
            {
                this.Modelo.UnidadZ = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de almacen
        /// </summary>
        public int IdUnidad
        {
            get
            {
                return this.Modelo.IdUnidad;
            }
            set
            {
                this.Modelo.IdUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre o descripcion del producto
        /// </summary>
        public new string Nombre
        {
            get
            {
                return base.Nombre;
            }
            set
            {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer valor unitario del modelo
        /// </summary>
        public decimal Unitario
        {
            get
            {
                return this.Modelo.Unitario;
            }
            set
            {
                this.Modelo.Unitario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la existencia del modelo en el almacen
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        public decimal Existencia
        {
            get
            {
                return this.Modelo.Existencia;
            }
            set
            {
                this.Modelo.Existencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Stock minimo
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        public decimal Minimo
        {
            get
            {
                return this.Modelo.Minimo;
            }
            set
            {
                this.Modelo.Minimo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Stock maximo
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        public decimal Maximo
        {
            get
            {
                return this.Modelo.Maximo;
            }
            set
            {
                this.Modelo.Maximo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Reorden
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        public decimal ReOrden
        {
            get
            {
                return this.Modelo.ReOrden;
            }
            set
            {
                this.Modelo.ReOrden = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Largo
        {
            get
            {
                return this.Modelo.Largo;
            }
            set
            {
                this.Modelo.Largo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el ancho
        /// </summary>
        public decimal Ancho
        {
            get
            {
                return this.Modelo.Ancho;
            }
            set
            {
                this.Modelo.Ancho = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el alto o calibre
        /// </summary>
        public decimal Alto
        {
            get
            {
                return this.Modelo.Alto;
            }
            set
            {
                this.Modelo.Alto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el alto o calibre
        /// </summary>
        public decimal Peso
        {
            get
            {
                return this.Modelo.Peso;
            }
            set
            {
                this.Modelo.Peso = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripci�n del concepto.
        /// </summary>
        public string ClaveUnidad
        {
            get
            {
                return this.Modelo.ClaveUnidad; ;
            }
            set
            {
                this.Modelo.ClaveUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del producto o del servicio amparado por el presente concepto. Es requerido y deben utilizar las claves del cat�logo de productos y servicios, 
        /// cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.
        /// </summary>
        public string ClaveProdServ
        {
            get
            {
                return this.Modelo.ClaveProdServ;
            }
            set
            {
                this.Modelo.ClaveProdServ = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:C�digo de barras
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string Codigo
        {
            get
            {
                return this.Modelo.Codigo;
            }
            set
            {
                this.Modelo.Codigo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de medida propia de la operaci�n del emisor, aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripci�n del concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |�|�|!|&quot;|%|&amp;|'|�|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|�|�|�|�|�|�|�|�|�|�|�|�){1,20}"
        /// </summary>
        public string Unidad
        {
            get
            {
                return this.Modelo.Unidad;
            }
            set
            {
                this.Modelo.Unidad = value;
                this.OnPropertyChanged();
            }
        }

        public string NumRequerimiento
        {
            get
            {
                return this.Modelo.NumRequerimiento;
            }
            set
            {
                this.Modelo.NumRequerimiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el n�mero de parte, identificador del producto o del servicio, la clave de producto o servicio, SKU o equivalente, propia de la operaci�n del emisor, 
        /// amparado por el presente concepto. Opcionalmente se puede utilizar claves del est�ndar GTIN.
        /// </summary>
        /// pattern value="([A-Z]|[a-z]|[0-9]| |�|�|!|&quot;|%|&amp;|'|�|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|�|�|�|�|�|�|�|�|�|�|�|�){1,100}"
        public string NoIdentificacion
        {
            get
            {
                return this.Modelo.NoIdentificacion;
            }
            set
            {
                this.Modelo.NoIdentificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:marca del producto
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string Marca
        {
            get
            {
                return this.Modelo.Marca;
            }
            set
            {
                this.Modelo.Marca = value;
                this.OnPropertyChanged();
            }
        }

        public string Especificacion
        {
            get
            {
                return this.Modelo.Especificacion;
            }
            set
            {
                this.Modelo.Especificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:descripcion corta del producto
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string Descripcion
        {
            get
            {
                return this.Modelo.Descripcion;
            }
            set
            {
                this.Modelo.Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de producto para formar URL
        /// </summary>
        public new string Clave
        {
            get
            {
                return base.Clave;
            }
            set
            {
                base.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Etiquetas
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string Etiquetas
        {
            get
            {
                return this.Modelo.Etiquetas;
            }
            set
            {
                this.Modelo.Etiquetas = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de usuario que crea el registro
        /// </summary>
        public new string Creo
        {
            get
            {
                return base.Creo;
            }
            set
            {
                base.Creo = value;
                this.Modelo.Creo = value;
                this.OnPropertyChanged();
            }
        }

        public string Modifica
        {
            get
            {
                return this.Modelo.Modifica;
            }
            set
            {
                this.Modelo.Modifica = value;
                this.OnPropertyChanged();
            }
        }

        public new DateTime FechaNuevo
        {
            get
            {
                return base.FechaNuevo;
            }
            set
            {
                base.FechaNuevo = value;
                this.Modelo.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime? FechaModifica
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.Modelo.FechaModifica >= firstGoodDate)
                    return this.Modelo.FechaModifica;
                return null;
            }
            set
            {
                this.Modelo.FechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el modelo
        /// </summary>
        public ViewModelModelo Modelo { get; set; }

        public object Clone()
        {
            return (ProductoModelo)this.MemberwiseClone();
        }
    }

    public class ProductoModeloProveedores : ProductoModelo
    {
        private BindingList<ViewModelModeloProveedor> proveedores;


        public ProductoModeloProveedores()
        {

        }

        public BindingList<ViewModelModeloProveedor> Proveedores
        {
            get
            {
                return this.proveedores;
            }
            set
            {
                this.proveedores = value;
                this.OnPropertyChanged();

            }
        }
    }
}