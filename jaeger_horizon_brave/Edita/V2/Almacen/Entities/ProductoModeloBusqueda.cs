﻿using Jaeger.Edita.V2.Almacen.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Edita.V2.Almacen.Entities
{
    /// <summary>
    /// clase para contener el resultado de la busqueda de productos o modelos
    /// </summary>
    public class ProductoModeloBusqueda
    {
        public EnumAlmacen Almacen { get; set; }

        public int AlmacenInt
        {
            get
            {
                return (int)this.Almacen;
            }
            set
            {
                this.Almacen = (EnumAlmacen)Enum.Parse(typeof(EnumAlmacen), value.ToString());
            }
        }

        public EnumProdServTipo Tipo { get; set; }

        public int IdProveedor { get; set; }

        public int IdCategoria { get; set; }

        public int IdProducto { get; set; }

        public int IdModelo { get; set; }

        public int IdUnidad { get; set; }

        /// <summary>
        /// obtener o establecer el precio unitario del proveedor
        /// </summary>
        public decimal Unitario { get; set; }

        public decimal UnitarioBase { get; set; }

        private string factorTrasladoIVA;
        private string factorTrasladoIEPS;

        /// <summary>
        /// obtener o establecer el factor del impuesto IVA trasladado, puede ser TASA y valor FIJO
        /// </summary>
        public string FactorTrasladoIVA
        {
            get
            {
                if (this.factorTrasladoIVA == "N/A")
                    return null;
                return this.factorTrasladoIVA;
            }
            set
            {
                if (value == "N/A")
                    this.ValorTrasladoIVA = null;
                this.factorTrasladoIVA = value;
            }
        }

        /// <summary>
        /// obtener o establecer el valor del IVA trasladado %
        /// </summary>
        public decimal? ValorTrasladoIVA { get; set; }

        /// <summary>
        /// obtener o establecer el factor del impuesto IEPS trasladado, puede ser TASA ó CUOTA
        /// </summary>
        public string FactorTrasladoIEPS
        {
            get
            {
                if (this.factorTrasladoIEPS == "N/A")
                    return null;
                return this.factorTrasladoIEPS;
            }
            set
            {
                if (value == "N/A")
                    this.trasladoIEPS = null;
                this.factorTrasladoIEPS = value;
            }
        }
        private decimal? trasladoIEPS;
        /// <summary>
        /// obtener o establecer el valor % del impuesto IEPS, puede ser valor FIJO o RANGO, si el factor es TASA es fijo, si el factor es CUOTA puede ser rango
        /// </summary>
        public decimal? ValorTrasladoIEPS { get; set; }

        //private string factorRetencionIVA;
        ///// <summary>
        ///// obtener o establecer el factor del impuesto retenido IVA, puese ser TASA
        ///// </summary>
        //[DataNames("_ctlmdl_retivaf")]
        //[SugarColumn(ColumnName = "_ctlmdl_retivaf", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el factor del impuesto retenido IVA", IsNullable = true, Length = 6)]
        //public string FactorRetencionIVA
        //{
        //    get
        //    {
        //        return this.factorRetencionIVA;
        //    }
        //    set
        //    {
        //        this.factorRetencionIVA = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA, puede ser un rango cuando el factor es TASA
        /// </summary>
        public decimal? ValorRetencionIVA { get; set; }

        private string factorRetencionIEPS;
        /// <summary>
        /// obtener o establecer el factor del impuesto IEPS que puede ser TASA ó CUOTA
        /// </summary>
        public string FactorRetencionIEPS
        {
            get
            {
                if (this.factorRetencionIEPS == "N/A")
                    return null;
                return this.factorRetencionIEPS;
            }
            set
            {
                if (value == "N/A")
                    this.ValorRetencionIEPS = null;
                this.factorRetencionIEPS = value;
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS, puede ser FIJO si al factor es TASA ó RANGO si el factor es CUOTA
        /// </summary>
        public decimal? ValorRetencionIEPS { get; set; }

        //private string factorRetencionISR;
        ///// <summary>
        ///// obtener o establecer el factor del impuesto retenido ISR, es este caso siempre es TASA
        ///// </summary>
        //[DataNames("_ctlmdl_retisrf")]
        //[SugarColumn(ColumnName = "_ctlmdl_retisrf", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el factor del impuesto retenido ISR", IsNullable = true, Length = 6)]
        //public string FactorRetencionISR
        //{
        //    get
        //    {
        //        return this.factorRetencionISR;
        //    }
        //    set
        //    {
        //        this.factorRetencionISR = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido ISR, si el factor es TASA el valor puede ser un RANGO
        /// </summary>
        public decimal? ValorRetecionISR { get; set; }

        public string Unidad { get; set; }

        public string Codigo { get; set; }

        public string ClaveUnidad { get; set; }

        public string ClaveProdServ { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del producto
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion o nombre del modelo
        /// </summary>
        public string Descripcion { get; set; }

        public string NoIdentificacion { get; set; }

        public string Especificacion { get; set; }

        public string Marca { get; set; }

        public string NumRequerimiento { get; set; }

        public string Etiquetas { get; set; }

        public decimal Largo { get; set; }

        public decimal Ancho { get; set; }

        public decimal Alto { get; set; }

        public decimal Peso { get; set; }

        public int UnidadXY { get; set; }

        public int UnidadZ { get; set; }

        public DateTime FechaNuevo { get; set; }
    }
}
