﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Edita.V2.Almacen.Enums;

namespace Jaeger.Edita.V2.Almacen.Entities
{
    /// <summary>
    /// clase simple para el catalogo de productos
    /// </summary>
    [SugarTable("_ctlprd", "catalogo de productos")]
    public class ViewModelProductoServicioSingle : ViewModelProductoServicio
    {
        private string clave;

        public ViewModelProductoServicioSingle()
        {
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        [DataNames("_ctlprd_id")]
        [SugarColumn(ColumnName = "_ctlprd_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public new int IdProducto
        {
            get
            {
                return base.IdProducto;
            }
            set
            {
                base.IdProducto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro esta activo
        /// </summary>
        [DataNames("_ctlprd_a")]
        [SugarColumn(ColumnName = "_ctlprd_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public new bool Activo
        {
            get
            {
                return base.Activo;
            }
            set
            {
                base.Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de almacen al que pertenece el producto
        /// </summary>
        [DataNames("_ctlprd_alm_id")]
        [SugarColumn(ColumnName = "_ctlprd_alm_id", ColumnDescription = "alm:|tw| desc: almacen al que pertenece (1-mp|2-pt|3-tw)", ColumnDataType = "SMALLINT", Length = 1)]
        public new EnumAlmacen Almacen
        {
            get
            {
                return base.Almacen;
            }
            set
            {
                base.Almacen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:(1-producto, 2-servicio, 3-kit, 4-grupo de productos)
        /// Default:1
        /// Nullable:True
        /// </summary>           
        [DataNames("_ctlprd_tipo")]
        [SugarColumn(ColumnName = "_ctlprd_tipo", ColumnDescription = "alm:|mp,pt,tw| desc: tipo de producto o servicio (1-producto, 2-servicio, 3-kit, 4-grupo de productos)", ColumnDataType = "SMALLINT", DefaultValue = "1", Length = 1)]
        public new EnumProdServTipo Tipo
        {
            get
            {

                return base.Tipo;
            }
            set
            {
                base.Tipo = value;
                this.OnPropertyChanged();
            }
        }

        //[DataNames("_ctlprd_vis")]
        //[SugarColumn(ColumnName = "_ctlprd_vis", ColumnDescription = "alm:|tw| desc: para indicar si es visible en la tienda web (0=ninguno, 1=distribuidores, 2=tienda web, 3=ambos)", ColumnDataType = "SMALLINT", DefaultValue = "0", Length = 1)]
        //public int Visible
        //{
        //    get
        //    {
        //        return this.visible;
        //    }
        //    set
        //    {
        //        this.visible = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer indice de relacion de la categoria
        /// </summary>
        [DataNames("_ctlprd_subid")]
        [SugarColumn(ColumnName = "_ctlprd_subid", ColumnDescription = "indice de relacion de las categorias", DefaultValue = "0")]
        public new int IdCategoria
        {
            get
            {
                return base.IdCategoria;
            }
            set
            {
                base.IdCategoria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de producto para formar URL
        /// </summary>
        [DataNames("_ctlprd_clv")]
        [SugarColumn(ColumnName = "_ctlprd_clv", ColumnDescription = "obtener o establecer clave de producto para formar URL", DefaultValue = "", Length = 128)]
        public string Clave
        {
            get
            {
                return this.clave;
            }
            set
            {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre o descripcion del producto
        /// </summary>
        [DataNames("_ctlprd_nom")]
        [SugarColumn(ColumnName = "_ctlprd_nom", ColumnDescription = "descripcion del producto", Length = 128)]
        public new string Nombre
        {
            get
            {
                return base.Nombre;
            }
            set
            {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor unitario del producto
        /// </summary>
        //[DataNames("_ctlprd_untr")]
        //[SugarColumn(ColumnName = "_ctlprd_untr", ColumnDescription = "precio unitario del producto", Length = 11, DecimalDigits = 4)]
        //public decimal Unitario
        //{
        //    get
        //    {
        //        return this.valorUnitario;
        //    }
        //    set
        //    {
        //        this.valorUnitario = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer clave de usuario que crea el registro
        /// </summary>
        [DataNames("_ctlprd_usr_n")]
        [SugarColumn(ColumnName = "_ctlprd_usr_n", ColumnDescription = "clave de usuario que crea el registro", Length = 20)]
        public new string Creo
        {
            get
            {
                return base.Creo;
            }
            set
            {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("_ctlprd_fn")]
        [SugarColumn(ColumnName = "_ctlprd_fn", ColumnDescription = "fecha de creacion del registro", ColumnDataType = "TIMESTAMP", DefaultValue = "CURRENT_TIMESTAMP")]
        public new DateTime FechaNuevo
        {
            get
            {
                return base.FechaNuevo;
            }
            set
            {
                base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

    }
}
