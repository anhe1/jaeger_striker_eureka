﻿using SqlSugar;

namespace Jaeger.Edita.V2.Almacen.Entities
{
    /// <summary>
    /// clasificacion general 
    /// </summary>
    [SugarTable("_ctlcla", "catalogo de clasificaciones")]
    public class ViewModelClasificacionG
    {
        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [SugarColumn(ColumnName = "_ctlcla_id", ColumnDescription = "indice principal de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int Id { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion en si mismo
        /// </summary>
        [SugarColumn(ColumnName = "_ctlcla_sbid", ColumnDescription = "indice de relacion", IsNullable = false)]
        public int SubId { get; set; }

        /// <summary>
        /// obtener o establecer nombre de la clase
        /// </summary>
        [SugarColumn(ColumnName = "_cltcla_cla", ColumnDescription = "descripcion de la clase", Length = 64)]
        public string Clase { get; set; }
    }
}
