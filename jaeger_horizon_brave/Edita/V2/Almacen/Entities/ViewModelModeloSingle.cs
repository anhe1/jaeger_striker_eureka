﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Edita.V2.Almacen.Interfaces;

namespace Jaeger.Edita.V2.Almacen.Entities
{
    [SugarTable("_ctlmdl", "catalogo de modelos de productos")]
    public class ViewModelModeloSingle : ViewModelModelo, IModelo
    {
        #region declaraciones

        private string clave;
        private string publicacion;
        private string cuentaPredial;
        private int visible;
        private decimal unitario1;
        private decimal unitario2;
        private decimal cantidad1;
        private decimal cantidad2;

        #endregion

        public ViewModelModeloSingle()
        {
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice del catalogo de prdoductos
        /// </summary>
        [DataNames("_ctlmdl_idpro")]
        [SugarColumn(ColumnName = "_ctlmdl_idpro", ColumnDescription = "indice de relación con la tabla de productos", IsNullable = false)]
        public new int IdProducto
        {
            get
            {
                return base.IdProducto;
            }
            set
            {
                base.IdProducto = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlmdl_vis")]
        [SugarColumn(ColumnName = "_ctlmdl_vis", ColumnDescription = "alm:|tw| desc: para indicar si es visible en la tienda web (0=ninguno, 1=distribuidores, 2=tienda web, 3=ambos)", ColumnDataType = "SMALLINT", DefaultValue = "0")]
        public int Visible
        {
            get
            {
                return this.visible;
            }
            set
            {
                this.visible = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlmdl_untr1")]
        [SugarColumn(ColumnName = "_ctlmdl_untr1", ColumnDescription = "alm:|mp,pt,tw| desc: precio unitario de primer mayoreo")]
        public decimal Unitario1 
        {
            get
            {
                return this.unitario1;
            }
            set
            {
                this.unitario1 = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlmdl_untr2")]
        [SugarColumn(ColumnName = "_ctlmdl_untr2", ColumnDescription = "alm:|mp,pt,tw| desc: precio unitario de segundo mayoreo")]
        public decimal Unitario2
        {
            get
            {
                return this.unitario2;
            }
            set
            {
                this.unitario2 = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlmdl_cant1")]
        [SugarColumn(ColumnName = "_ctlmdl_cant1", ColumnDescription = "alm:|mp,pt,tw| desc: cantidad de unidades para primer mayoreo")]
        public decimal Cantidad1
        {
            get
            {
                return this.cantidad1;
            }
            set
            {
                this.cantidad1 = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlmdl_cant2")]
        [SugarColumn(ColumnName = "_ctlmdl_cant2", ColumnDescription = "alm:|pm,pt,tw| desc: cantidad de unidades para segundo mayoreo")]
        public decimal Cantidad2
        {
            get
            {
                return this.cantidad2;
            }
            set
            {
                this.cantidad2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo opcional para asentar el número de cuenta predial con el que fue registrado el inmueble, en el sistema catastral de la entidad federativa de que trate, o bien para incorporar 
        /// los datos de identificación del certificado de participación inmobiliaria no amortizable.
        /// obtener o establecer el número de la cuenta predial del inmueble cubierto por el presente concepto, o bien para incorporar los datos de identificación del certificado de participación 
        /// inmobiliaria no amortizable, tratándose de arrendamiento.
        /// pattern value="[0-9]{2} [0-9]{2} [0-9]{4} [0-9]{7}", Longitud: 21
        /// </summary>
        [DataNames("_ctlmdl_ctapre")]
        [SugarColumn(ColumnName = "_ctlmdl_ctapre", ColumnDescription = "alm:|mp,pt| desc: número de cuenta predial con el que fue registrado el inmueble, en el sistema catastral de la entidad federativa de que trate, o bien para incorporar los datos de identificación del certificado de participación inmobiliaria no amortizable.", Length = 20, IsNullable = true)]
        public new string CtaPredial
        {
            get
            {
                return this.cuentaPredial;
            }
            set
            {
                this.cuentaPredial = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Desc:clave del producto
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_ctlmdl_clv")]
        [SugarColumn(ColumnName = "_ctlmdl_clv", ColumnDescription = "alm:|pt,tw| desc: clave del modelo (url)", Length = 128, IsNullable = true)]
        public string Clave
        {
            get
            {
                return this.clave;
            }
            set
            {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlmdl_dscr")]
        [SugarColumn(ColumnName = "_ctlmdl_dscr", ColumnDescription = "alm:|tw| desc: descripcion larga del producto", ColumnDataType = "TEXT", IsNullable = true)]
        public string Publicacion
        {
            get
            {
                return this.publicacion;
            }
            set
            {
                this.publicacion = value;
                this.OnPropertyChanged();
            }
        }
    }
}
