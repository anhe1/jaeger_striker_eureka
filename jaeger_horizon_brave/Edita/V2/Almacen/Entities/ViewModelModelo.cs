﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using System.Text.RegularExpressions;
using Jaeger.Edita.V2.Almacen.Enums;
using Jaeger.Edita.V2.Almacen.Interfaces;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Edita.V2.Almacen.Entities
{
    /// <summary>
    /// clase con solo las propiedades del modelo 
    /// </summary>
    [SugarTable("_ctlmdl", "catalogo de modelos de productos")]
    public class ViewModelModelo : BasePropertyChangeImplementation, ICloneable, IModelo
    {
        #region declaraciones

        private int index;
        private int idCategoria;
        private int idProducto;
        private bool activo = true;
        private decimal valorUnitario;
        private string codigoDeBarras;
        private string marca;
        private string descripcion;
        private string etiquetas;
        private decimal existencias;
        private decimal minimo;
        private decimal maximo;
        private decimal reorden;
        private string creo;
        private string modifica;
        private string claveProdServField;
        private string claveUnidadField;
        private string unidad;
        private DateTime fechaNuevo = DateTime.Now;
        private DateTime? fechaModifica;
        private string especificacionField;
        private string noidentificacion;
        private string numRequerimiento;
        private string cuentaPredial;
        private int unidad1Field;
        private int unidad2Field;
        private int unidadAlmacenField;
        private EnumAlmacen idAlmacen;
        private EnumProdServTipo idTipo;
        private decimal largo;
        private decimal ancho;
        private decimal alto;
        private decimal peso;
        private decimal? retecionISR;
        private decimal? retencionIVA;
        private decimal? trasladoIVA;
        private string factorTrasladoIVA;
        private decimal? retencionIEPS;
        private decimal? trasladoIEPS;
        private string factorTrasladoIEPS;
        private int idPrecio;
        #endregion

        public ViewModelModelo()
        {
            this.fechaNuevo = DateTime.Now;
        }

        [DataNames("_ctlmdl_id")]
        [SugarColumn(ColumnName = "_ctlmdl_id", ColumnDescription = "indice principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdModelo
        {
            get
            {
                return this.index;
            }
            set
            {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        [DataNames("_ctlmdl_a")]
        [SugarColumn(ColumnName = "_ctlmdl_a", ColumnDescription = "registro activo", Length = 1, DefaultValue = "1", IsNullable = false)]
        public bool Activo
        {
            get
            {
                return this.activo;
            }
            set
            {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlmdl_idpre")]
        [SugarColumn(ColumnName = "_ctlmdl_idpre")]
        public int IdPrecio
        {
            get
            {
                return this.idPrecio;
            }
            set
            {
                this.idPrecio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:id categoría
        /// Default:0
        /// Nullable:False
        /// </summary>           
        [DataNames("_ctlmdl_idcat")]
        [SugarColumn(ColumnName = "_ctlmdl_idcat", ColumnDescription = "indice de relacion con la tabla de categorias", IsNullable = false)]
        public int IdCategoria
        {
            get
            {
                return this.idCategoria;
            }
            set
            {
                this.idCategoria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del catalogo de prdoductos
        /// </summary>
        [DataNames("_ctlmdl_idpro")]
        [SugarColumn(ColumnName = "_ctlmdl_idpro", ColumnDescription = "indice de relación con la tabla de productos", IsNullable = false)]
        public int IdProducto
        {
            get
            {
                return this.idProducto;
            }
            set
            {
                this.idProducto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el almacen al que pertenece Materia Prima o Producto terminado
        /// </summary>
        [DataNames("_ctlmdl_alm_id")]
        [SugarColumn(IsIgnore = true)]
        public int AlmacenInt
        {
            get
            {
                return (int)this.idAlmacen;
            }
            set
            {
                this.idAlmacen = (EnumAlmacen)Enum.Parse(typeof(EnumAlmacen), value.ToString());
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el almacen al que pertenece Materia Prima o Producto terminado
        /// </summary>
        [SugarColumn(ColumnName = "_ctlmdl_alm_id", ColumnDescription = "alm: |mp,pt| desc: almacen al que pertenece")]
        public EnumAlmacen Almacen
        {
            get
            {
                return this.idAlmacen;
            }
            set
            {
                this.idAlmacen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si es un producto o servicio
        /// </summary>
        [DataNames("_ctlmdl_tipo")]
        [SugarColumn(IsIgnore = true)]
        public int TipoInt
        {
            get
            {
                return (int)this.idTipo;
            }
            set
            {
                this.idTipo = (EnumProdServTipo)Enum.Parse(typeof(EnumProdServTipo), value.ToString());
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si es un producto o servicio
        /// </summary>
        [SugarColumn(ColumnName = "_ctlmdl_tipo", ColumnDescription = "alm:|mp,pt,tw| desc: tipo de producto o servicio (1-producto, 2-servicio, 3-kit, 4-grupo de productos). Es utilizado en CFDI para hacer la distinción de producto o servicio.")]
        public EnumProdServTipo Tipo
        {
            get
            {
                return this.idTipo;
            }
            set
            {
                this.idTipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad utilizada en largo y ancho
        /// </summary>
        [DataNames("_ampctl_unddxy")]
        [SugarColumn(ColumnName = "_ctlmdl_unddxy", ColumnDescription = "alm:|mp,pt| desc: indice de la unidad utilizada en largo y ancho", IsNullable = false)]
        public int UnidadXY
        {
            get
            {
                return this.unidad1Field;
            }
            set
            {
                this.unidad1Field = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad utilizada en alto o calibre
        /// </summary>
        [DataNames("_ampctl_unddz")]
        [SugarColumn(ColumnName = "_ctlmdl_unddz", ColumnDescription = "alm:|mp,pt| desc: indice de la unidad utiliza en alto o calibre eje Z", IsNullable = false)]
        public int UnidadZ
        {
            get
            {
                return this.unidad2Field;
            }
            set
            {
                this.unidad2Field = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de almacen
        /// </summary>
        [DataNames("_ampctl_undda")]
        [SugarColumn(ColumnName = "_ctlmdl_undda", ColumnDescription = "alm:|mp,pt| desc: indice de la unidad de almacenamiento en el el almacen", IsNullable = false)]
        public int IdUnidad
        {
            get
            {
                return this.unidadAlmacenField;
            }
            set
            {
                this.unidadAlmacenField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer le valor unitario del modelo
        /// </summary>
        [DataNames("_ctlmdl_untr")]
        [SugarColumn(ColumnName = "_ctlmdl_untr", ColumnDescription = "alm:|mp,pt,tw| desc: precio unitario", Length = 11, DecimalDigits = 6)]
        public decimal Unitario
        {
            get
            {
                return this.valorUnitario;
            }
            set
            {
                this.valorUnitario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor del impuesto IVA trasladado, puede ser TASA y valor FIJO
        /// </summary>
        [DataNames("_ctlmdl_trsivaf")]
        [SugarColumn(ColumnName = "_ctlmdl_trsivaf", ColumnDescription = "obtener o establecer el factor del impuesto trasladado IVA", IsNullable = true, Length = 6)]
        public string FactorTrasladoIVA
        {
            get
            {
                if (this.factorTrasladoIVA == "N/A")
                    return null;
                return this.factorTrasladoIVA;
            }
            set
            {
                if (value == "N/A")
                    this.ValorTrasladoIVA = null;
                this.factorTrasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del IVA trasladado %
        /// </summary>
        [DataNames("_ctlmdl_trsiva")]
        [SugarColumn(ColumnName = "_ctlmdl_trsiva", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el valor del impuesto trasladado IVA", IsNullable = true, Length = 6, DecimalDigits = 6)]
        public decimal? ValorTrasladoIVA
        {
            get
            {
                return this.trasladoIVA;
            }
            set
            {
                this.trasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor del impuesto IEPS trasladado, puede ser TASA ó CUOTA
        /// </summary>
        [DataNames("_ctlmdl_trsiepsf")]
        [SugarColumn(ColumnName = "_ctlmdl_trsiepsf", ColumnDescription = "obtener o establecer el factor del impuesto trasladado IEPS", IsNullable = true, Length = 6)]
        public string FactorTrasladoIEPS
        {
            get
            {
                if (this.factorTrasladoIEPS == "N/A")
                    return null;
                return this.factorTrasladoIEPS;
            }
            set
            {
                if (value == "N/A")
                    this.trasladoIEPS = null;
                this.factorTrasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor % del impuesto IEPS, puede ser valor FIJO o RANGO, si el factor es TASA es fijo, si el factor es CUOTA puede ser rango
        /// </summary>
        [DataNames("_ctlmdl_trsieps")]
        [SugarColumn(ColumnName = "_ctlmdl_trsieps", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el valor del impuesto trasladado IEPS", IsNullable = true, Length = 6, DecimalDigits = 6)]
        public decimal? ValorTrasladoIEPS
        {
            get
            {
                return this.trasladoIEPS;
            }
            set
            {
                this.trasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        //private string factorRetencionIVA;
        ///// <summary>
        ///// obtener o establecer el factor del impuesto retenido IVA, puese ser TASA
        ///// </summary>
        //[DataNames("_ctlmdl_retivaf")]
        //[SugarColumn(ColumnName = "_ctlmdl_retivaf", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el factor del impuesto retenido IVA", IsNullable = true, Length = 6)]
        //public string FactorRetencionIVA
        //{
        //    get
        //    {
        //        return this.factorRetencionIVA;
        //    }
        //    set
        //    {
        //        this.factorRetencionIVA = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA, puede ser un rango cuando el factor es TASA
        /// </summary>
        [DataNames("_ctlmdl_retiva")]
        [SugarColumn(ColumnName = "_ctlmdl_retiva", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el valor del impuesto retenido IVA", IsNullable = true, Length = 6, DecimalDigits = 6)]
        public decimal? ValorRetencionIVA
        {
            get
            {
                return this.retencionIVA;
            }
            set
            {
                this.retencionIVA = value;
                this.OnPropertyChanged();
            }
        }

        private string factorRetencionIEPS;
        /// <summary>
        /// obtener o establecer el factor del impuesto IEPS que puede ser TASA ó CUOTA
        /// </summary>
        [DataNames("_ctlmdl_retiepsf")]
        [SugarColumn(ColumnName = "_ctlmdl_retiepsf", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el factor del impuesto retenido IEPS", IsNullable = true, Length = 6)]
        public string FactorRetencionIEPS
        {
            get
            {
                if (this.factorRetencionIEPS == "N/A")
                    return null;
                return this.factorRetencionIEPS;
            }
            set
            {
                if (value == "N/A")
                    this.ValorRetencionIEPS = null;
                this.factorRetencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS, puede ser FIJO si al factor es TASA ó RANGO si el factor es CUOTA
        /// </summary>
        [DataNames("_ctlmdl_retieps")]
        [SugarColumn(ColumnName = "_ctlmdl_retieps", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el valor del impuesto retenido IEPS", IsNullable = true, Length = 6, DecimalDigits = 6)]
        public decimal? ValorRetencionIEPS
        {
            get
            {
                return this.retencionIEPS;
            }
            set
            {
                this.retencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        //private string factorRetencionISR;
        ///// <summary>
        ///// obtener o establecer el factor del impuesto retenido ISR, es este caso siempre es TASA
        ///// </summary>
        //[DataNames("_ctlmdl_retisrf")]
        //[SugarColumn(ColumnName = "_ctlmdl_retisrf", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el factor del impuesto retenido ISR", IsNullable = true, Length = 6)]
        //public string FactorRetencionISR
        //{
        //    get
        //    {
        //        return this.factorRetencionISR;
        //    }
        //    set
        //    {
        //        this.factorRetencionISR = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido ISR, si el factor es TASA el valor puede ser un RANGO
        /// </summary>
        [DataNames("_ctlmdl_retisr")]
        [SugarColumn(ColumnName = "_ctlmdl_retisr", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el valor del impuesto retenido ISR", IsNullable = true, Length = 6, DecimalDigits = 6)]
        public decimal? ValorRetecionISR
        {
            get
            {
                return retecionISR;
            }
            set
            {
                this.retecionISR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:existencia
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_ctlmdl_ext")]
        [SugarColumn(ColumnName = "_ctlmdl_ext", ColumnDescription = "alm:|mp,pt,tw| desc: obtener o establecer la cantidad de la existencia", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Existencia
        {
            get
            {
                return this.existencias;
            }
            set
            {
                this.existencias = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Stock minimo
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_ctlmdl_min")]
        [SugarColumn(ColumnName = "_ctlmdl_min", ColumnDescription = "alm:|mp,pt,tw| desc: obtener o establecer el stock minimo del almacen", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Minimo
        {
            get
            {
                return this.minimo;
            }
            set
            {
                this.minimo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Stock maximo
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_ctlmdl_max")]
        [SugarColumn(ColumnName = "_ctlmdl_max", ColumnDescription = "alm:|mp,pt,tw| desc: obtener o establecer el stock maximo del almacen", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Maximo
        {
            get
            {
                return this.maximo;
            }
            set
            {
                this.maximo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Reorden
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_ctlmdl_reord")]
        [SugarColumn(ColumnName = "_ctlmdl_reord", ColumnDescription = "alm:|mp,pt,tw| desc: obtener o establecer la cantidad del punto de reorden", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal ReOrden
        {
            get
            {
                return this.reorden;
            }
            set
            {
                this.reorden = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlmdl_largo")]
        [SugarColumn(ColumnName = "_ctlmdl_largo", ColumnDescription = "alm:|mp,pt,tw| desc: largo", IsNullable = true, Length = 14, DecimalDigits = 4)]
        public decimal Largo
        {
            get
            {
                return this.largo;
            }
            set
            {
                this.largo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el ancho
        /// </summary>
        [DataNames("_ctlmdl_ancho")]
        [SugarColumn(ColumnName = "_ctlmdl_ancho", ColumnDescription = "alm:|mp,pt,tw| desc: ancho", IsNullable = true, Length = 14, DecimalDigits = 4)]
        public decimal Ancho
        {
            get
            {
                return this.ancho;
            }
            set
            {
                this.ancho = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el alto o calibre
        /// </summary>
        [DataNames("_ctlmdl_alto")]
        [SugarColumn(ColumnName = "_ctlmdl_alto", ColumnDescription = "alm:|mp,pt,tw| desc: alto o calibre", IsNullable = true, Length = 14, DecimalDigits = 4)]
        public decimal Alto
        {
            get
            {
                return this.alto;
            }
            set
            {
                this.alto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el alto o calibre
        /// </summary>
        [DataNames("_ctlmdl_peso")]
        [SugarColumn(ColumnName = "_ctlmdl_peso", ColumnDescription = "alm:|mp,pt,tw| desc: alto o calibre", IsNullable = true, Length = 14, DecimalDigits = 4)]
        public decimal Peso
        {
            get
            {
                return this.peso;
            }
            set
            {
                this.peso = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// </summary>
        [DataNames("_ctlmdl_clvund")]
        [SugarColumn(ColumnName = "_ctlmdl_clvund", ColumnDescription = "alm:|mp,pt,tw| desc: clave de unidad SAT", Length = 3, IsNullable = true)]
        public string ClaveUnidad
        {
            get
            {
                return this.claveUnidadField;
            }
            set
            {
                this.claveUnidadField = (value != null ? value.ToUpper().Trim() : value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del producto o del servicio amparado por el presente concepto. Es requerido y deben utilizar las claves del catálogo de productos y servicios, 
        /// cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.
        /// </summary>
        [DataNames("_ctlmdl_clvprds")]
        [SugarColumn(ColumnName = "_ctlmdl_clvprds", ColumnDescription = "alm:|mp,pt,tw| desc: clave del producto o del servicio SAT", Length = 8, IsNullable = true)]
        public string ClaveProdServ
        {
            get
            {
                return this.claveProdServField;
            }
            set
            {
                if (value == null)
                    this.claveProdServField = "";
                else
                    this.claveProdServField = Regex.Replace(value.ToString(), "[^\\d]", "");
                //if (!(value == null))
                //    this.claveProdServField = value;
                //else
                //    this.claveProdServField = Regex.Replace(value.ToString(), "[^\\d]", "");
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Código de barras
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_ctlmdl_cdg")]
        [SugarColumn(ColumnName = "_ctlmdl_cdg", ColumnDescription = "alm:|mp,pt,tw| desc: codigo de barras", Length = 12, IsNullable = true)]
        public string Codigo
        {
            get
            {
                return this.codigoDeBarras;
            }
            set
            {
                this.codigoDeBarras = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de medida propia de la operación del emisor, aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,20}"
        /// </summary>
        [DataNames("_ctlmdl_undd")]
        [SugarColumn(ColumnName = "_ctlmdl_undd", ColumnDescription = "alm:|mp,pt,tw| desc: unidad personalizada", Length = 20, IsNullable = true)]
        public string Unidad
        {
            get
            {
                return this.unidad;
            }
            set
            {
                this.unidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número del pedimento que ampara la importación del bien que se expresa en el siguiente formato: 
        /// últimos 2 dígitos del año de validación seguidos por dos espacios, 2 dígitos de la aduana de despacho seguidos por dos espacios, 
        /// 4 dígitos del número de la patente seguidos por dos espacios, 1 dígito que corresponde al último dígito del año en curso, salvo 
        /// que se trate de un pedimento consolidado iniciado en el año inmediato anterior o del pedimento original de una rectificación, 
        /// seguido de 6 dígitos de la numeración progresiva por aduana.
        /// pattern value="[0-9]{2} [0-9]{2} [0-9]{4} [0-9]{7}" longitud: 21
        /// </summary>
        [DataNames("_ctlmdl_numreq")]
        [SugarColumn(ColumnName = "_ctlmdl_numreq", ColumnDescription = "alm:|mp,pt,tw| desc: numero de requerimiento de aduana", Length = 50, IsNullable = true)]
        public string NumRequerimiento
        {
            get
            {
                return this.numRequerimiento;
            }
            set
            {
                this.numRequerimiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo opcional para asentar el número de cuenta predial con el que fue registrado el inmueble, en el sistema catastral de la entidad federativa de que trate, o bien para incorporar 
        /// los datos de identificación del certificado de participación inmobiliaria no amortizable.
        /// obtener o establecer el número de la cuenta predial del inmueble cubierto por el presente concepto, o bien para incorporar los datos de identificación del certificado de participación 
        /// inmobiliaria no amortizable, tratándose de arrendamiento.
        /// pattern value="[0-9]{2} [0-9]{2} [0-9]{4} [0-9]{7}", Longitud: 21
        /// </summary>
        [DataNames("_ctlmdl_ctapre")]
        [SugarColumn(ColumnName = "_ctlmdl_ctapre", ColumnDescription = "alm:|mp,pt| desc: número de cuenta predial con el que fue registrado el inmueble, en el sistema catastral de la entidad federativa de que trate, o bien para incorporar los datos de identificación del certificado de participación inmobiliaria no amortizable.", Length = 20, IsNullable = true)]
        public string CtaPredial
        {
            get
            {
                return this.cuentaPredial;
            }
            set
            {
                this.cuentaPredial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número de parte, identificador del producto o del servicio, la clave de producto o servicio, SKU o equivalente, propia de la operación del emisor, 
        /// amparado por el presente concepto. Opcionalmente se puede utilizar claves del estándar GTIN.
        /// </summary>
        /// pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,100}"
        [DataNames("_ctlmdl_sku")]
        [SugarColumn(ColumnName = "_ctlmdl_sku", ColumnDescription = "alm:|mp,pt,tw| desc: obtener o establecer el número de parte, identificador del producto o del servicio, la clave de producto o servicio, SKU o equivalente, propia de la operación del emisor (en cfdi: NoIdentificacion)", Length = 100, IsNullable = true)]
        public string NoIdentificacion
        {
            get
            {
                return noidentificacion;
            }
            set
            {
                this.noidentificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:marca del producto
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_ctlmdl_mrc")]
        [SugarColumn(ColumnName = "_ctlmdl_mrc", ColumnDescription = "alm:|mp,pt| desc: marca o fabricante", Length = 128, IsNullable = true)]
        public string Marca
        {
            get
            {
                return this.marca;
            }
            set
            {
                this.marca = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlmdl_espc")]
        [SugarColumn(ColumnName = "_ctlmdl_espc", ColumnDescription = "alm:|mp,pt| desc: espcificaciones", IsNullable = true, Length = 128)]
        public string Especificacion
        {
            get
            {
                return this.especificacionField;
            }
            set
            {
                this.especificacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:descripcion corta del producto
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_ctlmdl_dscrc")]
        [SugarColumn(ColumnName = "_ctlmdl_dscrc", ColumnDescription = "alm:|mp,pt,tw| desc: descripcion corta", Length = 1000, IsNullable = true)]
        public string Descripcion
        {
            get
            {
                return this.descripcion;
            }
            set
            {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Etiquetas
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_ctlmdl_etqts")]
        [SugarColumn(ColumnName = "_ctlmdl_etqts", ColumnDescription = "alm:|mp,pt,tw| desc: etiquetas de busqueda", Length = 254, IsNullable = true)]
        public string Etiquetas
        {
            get
            {
                return etiquetas;
            }
            set
            {
                this.etiquetas = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave del usuario que crea el registro
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_ctlmdl_usr_n")]
        [SugarColumn(ColumnName = "_ctlmdl_usr_n", ColumnDescription = "clave del usuario que crea el registro", IsNullable = true)]
        public string Creo
        {
            get
            {
                return this.creo;
            }
            set
            {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave del usuario que modifica
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_ctlmdl_usr_m")]
        [SugarColumn(ColumnName = "_ctlmdl_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsNullable = true)]
        public string Modifica
        {
            get
            {
                return this.modifica;
            }
            set
            {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlmdl_fn")]
        [SugarColumn(ColumnName = "_ctlmdl_fn", ColumnDescription = "fecha de creacion del registro", ColumnDataType = "TIMESTAMP", DefaultValue = "CURRENT_TIMESTAMP")]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevo;
            }
            set
            {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlmdl_fm")]
        [SugarColumn(ColumnName = "_ctlmdl_fm", ColumnDescription = "fecha de creacion del registro", IsNullable = true)]
        public DateTime? FechaModifica
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set
            {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        public object Clone()
        {
            return (ViewModelModelo)this.MemberwiseClone();
        }

        

        
    }
}
