﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jaeger.Edita.V2.Almacen.Enums;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.Almacen.Entities
{
    /// <summary>
    /// clase simple para el catalogo de productos
    /// </summary>
    [SugarTable("_ctlprd", "catalogo de productos")]
    public class ViewModelProductoServicio : BasePropertyChangeImplementation
    {
        private int index;
        private bool activo = true;
        private int subIndex;
        private string nombre;
        private string creo;
        private DateTime fechaNuevo;
        private EnumProdServTipo tipo;
        private EnumAlmacen almacen;

        public ViewModelProductoServicio()
        {
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        [DataNames("_ctlprd_id")]
        [SugarColumn(ColumnName = "_ctlprd_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdProducto
        {
            get
            {
                return this.index;
            }
            set
            {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro esta activo
        /// </summary>
        [DataNames("_ctlprd_a")]
        [SugarColumn(ColumnName = "_ctlprd_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo
        {
            get
            {
                return this.activo;
            }
            set
            {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de almacen al que pertenece el producto
        /// </summary>
        [DataNames("_ctlprd_alm_id")]
        [SugarColumn(ColumnName = "_ctlprd_alm_id", ColumnDescription = "alm:|tw| desc: almacen al que pertenece (1-mp|2-pt|3-tw)", ColumnDataType = "SMALLINT", Length = 1)]
        public EnumAlmacen Almacen
        {
            get
            {
                return this.almacen;
            }
            set
            {
                this.almacen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:(1-producto, 2-servicio, 3-kit, 4-grupo de productos)
        /// Default:1
        /// Nullable:True
        /// </summary>           
        [DataNames("_ctlprd_tipo")]
        [SugarColumn(ColumnName = "_ctlprd_tipo", ColumnDescription = "alm:|mp,pt,tw| desc: tipo de producto o servicio (1-producto, 2-servicio, 3-kit, 4-grupo de productos)", ColumnDataType = "SMALLINT", DefaultValue = "1", Length = 1)]
        public EnumProdServTipo Tipo
        {
            get
            {

                return this.tipo;
            }
            set
            {
                this.tipo = value;
                this.OnPropertyChanged();
            }
        }

        //[DataNames("_ctlprd_vis")]
        //[SugarColumn(ColumnName = "_ctlprd_vis", ColumnDescription = "alm:|tw| desc: para indicar si es visible en la tienda web (0=ninguno, 1=distribuidores, 2=tienda web, 3=ambos)", ColumnDataType = "SMALLINT", DefaultValue = "0", Length = 1)]
        //public int Visible
        //{
        //    get
        //    {
        //        return this.visible;
        //    }
        //    set
        //    {
        //        this.visible = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer indice de relacion de la categoria
        /// </summary>
        [DataNames("_ctlprd_subid")]
        [SugarColumn(ColumnName = "_ctlprd_subid", ColumnDescription = "indice de relacion de las categorias", DefaultValue = "0")]
        public int IdCategoria
        {
            get
            {
                return this.subIndex;
            }
            set
            {
                this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre o descripcion del producto
        /// </summary>
        [DataNames("_ctlprd_nom")]
        [SugarColumn(ColumnName = "_ctlprd_nom", ColumnDescription = "descripcion del producto", Length = 128)]
        public string Nombre
        {
            get
            {
                return this.nombre;
            }
            set
            {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor unitario del producto
        /// </summary>
        //[DataNames("_ctlprd_untr")]
        //[SugarColumn(ColumnName = "_ctlprd_untr", ColumnDescription = "precio unitario del producto", Length = 11, DecimalDigits = 4)]
        //public decimal Unitario
        //{
        //    get
        //    {
        //        return this.valorUnitario;
        //    }
        //    set
        //    {
        //        this.valorUnitario = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer clave de usuario que crea el registro
        /// </summary>
        [DataNames("_ctlprd_usr_n")]
        [SugarColumn(ColumnName = "_ctlprd_usr_n", ColumnDescription = "clave de usuario que crea el registro", Length = 20)]
        public string Creo
        {
            get
            {
                return this.creo;
            }
            set
            {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("_ctlprd_fn")]
        [SugarColumn(ColumnName = "_ctlprd_fn", ColumnDescription = "fecha de creacion del registro", ColumnDataType = "TIMESTAMP", DefaultValue = "CURRENT_TIMESTAMP")]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevo;
            }
            set
            {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
