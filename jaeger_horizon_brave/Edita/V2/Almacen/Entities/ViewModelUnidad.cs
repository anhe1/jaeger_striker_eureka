﻿/// develop: anhe1 081120191127
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.Almacen.Entities
{
    [SugarTable("_ampund", "almacen de materia prima: catalogo de unidades")]
    public class ViewModelUnidad : BasePropertyChangeImplementation
    {
        private int indiceField;
        private bool activoField;
        private string nombreField;
        private string creoField;
        private DateTime fechaNuevoField;

        public ViewModelUnidad()
        {
        }

        [DataNames("_ampund_id")]
        [SugarColumn(ColumnName = "_ampund_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ampund_a")]
        [SugarColumn(ColumnName = "_ampund_a", ColumnDescription = "registro activo", Length = 1)]
        public bool Activo
        {
            get
            {
                return this.activoField;
            }
            set
            {
                this.activoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ampund_nom")]
        [SugarColumn(ColumnName = "_ampund_nom", ColumnDescription = "nombre de la unidad", Length = 32)]
        public string Descripcion
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ampund_usr_n")]
        [SugarColumn(ColumnName = "_ampund_usr_n", ColumnDescription = "clave del usuario que creo el registro")]
        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ampund_fn")]
        [SugarColumn(ColumnName = "_ampund_fn", ColumnDescription = "fecha de creacion del registro")]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
