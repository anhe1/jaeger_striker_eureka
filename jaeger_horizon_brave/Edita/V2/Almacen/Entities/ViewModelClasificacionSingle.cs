﻿using System;
using System.Linq;
using SqlSugar;

namespace Jaeger.Edita.V2.Almacen.Entities
{
    ///<summary>
    ///catalogo de clasificaciones BPS
    ///</summary>
    [SugarTable("_ctlclss")]
    public class ViewModelClasificacionSingle : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private int index;
        private bool activo;
        private int subIndex;
        private int tipo;
        private string creo;
        private DateTime fechaNuevo;
        private string descripcion;
        private string clave;

        public ViewModelClasificacionSingle()
        {

        }

        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_ctlclss_id", IsPrimaryKey = true, IsIdentity = true)]
        public int Id
        {
            get
            {
                return this.index;;
            }
            set
            {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:activo (_ctlclss_a)
        /// Default:1
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_ctlclss_a", IsNullable = false)]
        public bool Activo 
        {
            get
            {
                return this.activo;
            }
            set
            {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlclss_sbid")]
        public int SubId
        {
            get
            {
                return this.subIndex;
            }
            set
            {
                this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:index de documento (_ctlclss_alm_id)
        /// Default:0
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_ctlclss_alm_id", IsNullable = false)]
        public int Almacen
        {
            get
            {
                return this.tipo;
            }
            set
            {
                this.tipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:nombre de la clasificacion (_ctlclss_class1)
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_ctlclss_class1", IsNullable = false)]
        public string Descripcion
        {
            get
            {
                return this.descripcion;
            }
            set
            {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:sub clasificacion (_ctlclss_class2)
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_ctlclss_class2", IsNullable = true)]
        public string Clave
        {
            get
            {
                return this.clave;
            }
            set
            {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Desc:Creo (_ctlclss_usr_n)
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_ctlclss_usr_n", IsNullable = true)]
        public string Creo 
        {
            get
            {
                return this.creo;
            }
            set
            {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Fec. sist. (_ctlclss_fn)
        /// Default:CURRENT_TIMESTAMP
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_ctlclss_fn", IsNullable = false)]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevo;
            }
            set
            {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
