﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.Almacen.Entities
{
    //[SugarTable("_ctlclss", "catalogo de categorias BPS")]
    //public class ViewModelCategoria : BasePropertyChangeImplementation
    //{
    //    private int index;
    //    private int subIndex;
    //    private string nombre;
    //    private string clave;

    //    public ViewModelCategoria()
    //    {
    //    }

    //    /// <summary>
    //    /// indice principal de la tabla
    //    /// </summary>
    //    [DataNames("_ctlclss_id")]
    //    [SugarColumn(ColumnName = "_ctlclss_id", ColumnDescription = "id pricipal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
    //    public int Id
    //    {
    //        get
    //        {
    //            return this.index;
    //        }
    //        set
    //        {
    //            this.index = value;
    //            this.OnPropertyChanged();
    //        }
    //    }

    //    /// <summary>
    //    /// obtener o establecer el indice de relacion de la tabla
    //    /// </summary>
    //    [DataNames("_ctlclss_sbid")]
    //    [SugarColumn(ColumnName = "_ctlclss_sbid", ColumnDescription = "indice de relacion de la categoria")]
    //    public int SubId
    //    {
    //        get
    //        {
    //            return this.subIndex;
    //        }
    //        set
    //        {
    //            this.subIndex = value;
    //            this.OnPropertyChanged();
    //        }
    //    }

    //    /// <summary>
    //    /// obtener o establecer la descripcion o nombre de la categoría
    //    /// </summary>
    //    [DataNames("_ctlclss_nom")]
    //    [SugarColumn(ColumnName = "_ctlclss_class1", ColumnDescription = "descripcion o nombre de la categoria")]
    //    public string Descripcion
    //    {
    //        get
    //        {
    //            return this.nombre;
    //        }
    //        set
    //        {
    //            this.nombre = value;
    //            this.OnPropertyChanged();
    //        }
    //    }

    //    /// <summary>
    //    ///  descripcion o clave de la categoria
    //    /// </summary>
    //    [DataNames("_ctlclss_clv")]
    //    [SugarColumn(ColumnName = "_ctlclss_class2", ColumnDescription = "descripcion o clave de la categoria")]
    //    public string Clave
    //    {
    //        get
    //        {
    //            return this.clave;
    //        }
    //        set
    //        {
    //            this.clave = value;
    //            this.OnPropertyChanged();
    //        }
    //    }
    //}

    /// <summary>
    /// clase simple para contener la informacion de un nivel de la categoría
    /// </summary>
    public class ViewModelCategoriaSingle : ViewModelClasificacionSingle
    {
        private string class1;
        private string tipo;

        public ViewModelCategoriaSingle()
        {
        }

        /// <summary>
        /// indice principal de la tabla
        /// </summary>
        [DataNames("Id")]
        public new int Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion de la tabla
        /// </summary>
        [DataNames("SubId")]
        public new int SubId
        {
            get
            {
                return base.SubId;
            }
            set
            {
                base.SubId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion o nombre de la categoría
        /// </summary>
        [DataNames("Descripcion")]
        public new string Descripcion
        {
            get
            {
                return base.Descripcion;
            }
            set
            {
                base.Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///  descripcion o clave de la categoria
        /// </summary>
        [DataNames("Clave")]
        public new string Clave
        {
            get
            {
                return base.Clave;
            }
            set
            {
                base.Clave = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("Clase")]
        public string Clase
        {
            get
            {
                return this.class1;
            }
            set
            {
                this.class1 = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("Tipo")]
        public string Tipo
        {
            get
            {
                return this.tipo;
            }
            set
            {
                this.tipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener la descripcion compuesta de la descripción
        /// </summary>
        public string Descripcion2
        {
            get
            {
                return string.Concat(this.Tipo, " / ", this.Clase, " / ", this.Descripcion);
            }
        }

        public override string ToString()
        {
            return string.Concat("Id: ", this.Id, " SubId: ", this.SubId, " Tipo: ", this.Tipo, " Class: ", this.Clase, " Descripcion: ", this.Descripcion);
        }
    }
}
