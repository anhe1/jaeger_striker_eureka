﻿using System;
using Jaeger.Edita.V2.Almacen.Enums;

namespace Jaeger.Edita.V2.Almacen.Interfaces
{
    /// <summary>
    /// interface modelo de la base de datos
    /// </summary>
    public interface IModelo
    {
        /// <summary>
        /// obtener o establecer el indice de la tabla de modelos
        /// </summary>
        int IdModelo { get; set; }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer id de categoria
        /// </summary>
        int IdCategoria { get; set; }

        /// <summary>
        /// obtener o establecer el almacen al que pertenece Materia Prima o Producto terminado
        /// </summary>
        EnumAlmacen Almacen { get; set; }

        /// <summary>
        /// obtener o establecer si es un producto o servicio
        /// </summary>
        EnumProdServTipo Tipo { get; set; }

        /// <summary>
        /// obtener o establecer la unidad de almacen
        /// </summary>
        int IdUnidad { get; set; }

        /// <summary>
        /// obtener o establecer la unidad utilizada en largo y ancho
        /// </summary>
        int UnidadXY { get; set; }

        /// <summary>
        /// obtener o establecer la unidad utilizada en alto o calibre
        /// </summary>
        int UnidadZ { get; set; }

        /// <summary>
        /// obtener o establecer el alto o calibre
        /// </summary>
        decimal Alto { get; set; }

        /// <summary>
        /// obtener o establecer el ancho
        /// </summary>
        decimal Ancho { get; set; }

        /// <summary>
        /// obtener o establecer largo
        /// </summary>
        decimal Largo { get; set; }

        /// <summary>
        /// obtener o establecer peso
        /// </summary>
        decimal Peso { get; set; }

        /// <summary>
        /// obtener o establecer el minimo permitido en el almacen
        /// </summary>
        decimal Minimo { get; set; }

        /// <summary>
        /// obtener o establecer el maximo permitido en el almacen
        /// </summary>
        decimal Maximo { get; set; }

        /// <summary>
        /// obtener o establecer reorden
        /// </summary>
        decimal ReOrden { get; set; }

        /// <summary>
        /// obtener o establecer la existencia en el almacen
        /// </summary>
        decimal Existencia { get; set; }

        /// <summary>
        /// obtener o establecer el valor unitario del modelo
        /// </summary>
        decimal Unitario { get; set; }

        /// <summary>
        /// obtener o establecer la unidad personalizada de la unidad
        /// </summary>
        string Unidad { get; set; }

        /// <summary>
        /// obtener o establecer clave de unidad SAT
        /// </summary>
        string ClaveUnidad { get; set; }

        /// <summary>
        /// obtener o establecer clave del producto o servicio SAT
        /// </summary>
        string ClaveProdServ { get; set; }

        /// <summary>
        /// obtener o establecer codigo de barras
        /// </summary>
        string Codigo { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer especificacones del modelo
        /// </summary>
        string Especificacion { get; set; }

        /// <summary>
        /// obtener o establecer la marca del modelo
        /// </summary>
        string Marca { get; set; }

        /// <summary>
        /// obtener o establecer el numero de requerimiento de aduana
        /// </summary>
        string NumRequerimiento { get; set; }

        /// <summary>
        /// obtener o establecer identificador del modelo SKU
        /// </summary>
        string NoIdentificacion { get; set; }

        /// <summary>
        /// obtener o establecer etiquetas de busqueda
        /// </summary>
        string Etiquetas { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creación del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer la clave del usuario que creo el registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica { get; set; }
    }

    public interface IModeloImpuestos
    {
        /// <summary>
        /// obtener o establecer el factor del impuesto IVA trasladado, puede ser TASA y valor FIJO
        /// </summary>
        string FactorTrasladoIVA { get; set; }

        decimal ValorTrasladoIVA { get; set; }

        string FactorTrasladoIEPS { get; set; }

        decimal ValorTrasladoIEPS { get; set; }
        string FactorRetencionIVA { get; set; }
        decimal ValorRetencionIVA { get; set; }
        string FactorRetencionIEPS { get; set; }
        decimal ValorRetencionIEPS { get; set; }
        string FactorRetencionISR { get; set; }
        decimal ValorRetencionISR { get; set; }

    }
}
