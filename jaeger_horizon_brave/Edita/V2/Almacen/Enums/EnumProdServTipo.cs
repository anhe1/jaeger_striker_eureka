﻿using System;
using System.ComponentModel;

namespace Jaeger.Edita.V2.Almacen.Enums
{
    public enum EnumProdServTipo
    {
        [Description("No Definido")]
        NoDefinido,
        [Description("Producto")]
        Producto,
        [Description("Servicio")]
        Servicio
    }
}