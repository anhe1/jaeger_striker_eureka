﻿using System;
using System.ComponentModel;

namespace Jaeger.Edita.V2.Almacen.Enums
{
    public enum EnumAlmacen
    {
        [Description("MP")]
        MP = 1,
        [Description("PT")]
        PT = 2
    }
}
