﻿using System;
using System.Linq;
using Jaeger.Edita.V2.Directorio.Enums;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.Directorio.Entities
{
    [SugarTable("_drctr")]
    public class ContribuyenteReceptor : ViewModelContribuyenteSingle
    {
        private string numRegIdTrib;
        private string usoCFDI;
        private string residenciaFiscal;

        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_drctr_id")]
        [SugarColumn(ColumnName = "_drctr_id", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public new int Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:registro activo=1 registro inactivo=2
        /// Default:1
        /// Nullable:False
        /// </summary>           
        [DataNames("_drctr_a")]
        [SugarColumn(ColumnName = "_drctr_a", ColumnDescription = "registro activo=1 registro inactivo=2", IsNullable = false, Length = 1)]
        public new bool IsActive
        {
            get
            {
                return base.IsActive;
            }
            set
            {
                base.IsActive = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:nombre y apellido o razon social
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_nom")]
        [SugarColumn(ColumnName = "_drctr_nom", IsNullable = false)]
        public new string Nombre
        {
            get
            {
                return base.Nombre;
            }
            set
            {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Reg. Fed. De Contribuyentes (RFC)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_rfc")]
        [SugarColumn(ColumnName = "_drctr_rfc", IsNullable = false)]
        public new string RFC
        {
            get
            {
                return base.RFC;
            }
            set
            {
                base.RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:CURP: requerido para la expresión de la CURP del trabajador
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_curp")]
        [SugarColumn(ColumnName = "_drctr_curp", IsNullable = false)]
        public new string CURP
        {
            get
            {
                return base.CURP;
            }
            set
            {
                base.CURP = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public new EnumRegimen Regimen
        {
            get
            {
                return base.Regimen;
            }
            set
            {
                base.Regimen = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_rgf")]
        [SugarColumn(ColumnName = "_drctr_rgf", IsNullable = false)]
        public new string RegimenText
        {
            get
            {
                return base.RegimenText;
            }
            set
            {
                base.RegimenText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:regimen fiscal
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_rgfsc")]
        [SugarColumn(ColumnName = "_drctr_rgfsc", IsNullable = false)]
        public new string RegimenFiscal
        {
            get
            {
                return base.RegimenFiscal;
            }
            set
            {
                base.RegimenFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:relacion comercial con la empresa (27=Cliente,163=Proveedor)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_rlcn")]
        [SugarColumn(ColumnName = "_drctr_rlcn", IsNullable = false)]
        public new string Relacion
        {
            get
            {
                return base.Relacion;
            }
            set
            {
                base.Relacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_resfis")]
        [SugarColumn(ColumnName = "_drctr_resfis", IsNullable = false)]
        public string ResidenciaFiscal
        {
            get
            {
                return this.residenciaFiscal;
            }
            set
            {
                this.residenciaFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el número de registro de identidad fiscal del receptor cuando sea residente en el  extranjero. Es requerido cuando se incluya el complemento de comercio exterior.
        /// </summary>
        public string NumRegIdTrib
        {
            get
            {
                return this.numRegIdTrib;
            }
            set
            {
                this.numRegIdTrib = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.
        /// </summary>
        public string ClaveUsoCFDI
        {
            get
            {
                return this.usoCFDI;
            }
            set
            {
                this.usoCFDI = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_usr_n")]
        [SugarColumn(ColumnName = "_drctr_usr_n", IsNullable = false)]
        public new string Creo
        {
            get
            {
                return base.Creo;
            }
            set
            {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_usr_m")]
        [SugarColumn(ColumnName = "_drctr_usr_m", IsNullable = false)]
        public new string Modifica
        {
            get
            {
                return base.Modifica;
            }
            set
            {
                base.Modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_fn")]
        [SugarColumn(ColumnName = "_drctr_fn", ColumnDescription = "fecha de creacion", IsNullable = false)]
        public new DateTime FechaNuevo
        {
            get
            {
                return base.FechaNuevo;
            }
            set
            {
                base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_fm")]
        [SugarColumn(ColumnName = "_drctr_fm", ColumnDescription = "fecha ultima de modificacion", IsNullable = false)]
        public new DateTime? FechaModifica
        {
            get
            {
                return base.FechaModifica;
            }
            set
            {
                base.FechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
