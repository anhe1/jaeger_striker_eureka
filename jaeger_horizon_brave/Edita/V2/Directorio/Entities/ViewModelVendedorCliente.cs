﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.Directorio.Entities
{
    /// <summary>
    /// cartera de clientes
    /// </summary>
    [SugarTable("_vndcli", "vendedores: cartera de clientes")]
    public class ViewModelVendedorCliente : BasePropertyChangeImplementation
    {
        public ViewModelVendedorCliente()
        {

        }

        private int index;
        private bool Activo;
        private int idCliente;
        private int idVendedor;
        private string creo;
        private string modifica;
        private string nota;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private decimal comision;

        [DataNames("_vndcli_id")]
        [SugarColumn(ColumnName = "_vndcli_id", ColumnDescription = "indice de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int Id
        {
            get { return index; }
            set 
            { 
                index = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_vndcli_a")]
        [SugarColumn(ColumnName = "_vndcli_a", ColumnDescription = "registro activo", IsNullable = false, DefaultValue = "1", Length = 1)]
        public bool activo
        {
            get { return Activo; }
            set 
            { 
                Activo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_vndcli_drctr_id")]
        [SugarColumn(ColumnName = "_vndcli_drctr_id", ColumnDescription = "indice del directorio", IsNullable = false)]
        public int IdCliente
        {
            get { return idCliente; }
            set 
            { 
                idCliente = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_vndcli_vnddr_id")]
        [SugarColumn(ColumnName = "_vndcli_vnddr_id", ColumnDescription = "indice del directorio", IsNullable = false)]
        public int IdVendedor
        {
            get { return idVendedor; }
            set 
            { 
                idVendedor = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_vndcli_nota")]
        [SugarColumn(ColumnName = "_vndcli_nota", ColumnDescription = "nota", IsNullable = false, Length = 255)]
        public string Nota
        {
            get { return nota; }
            set 
            { 
                nota = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_vndcli_com")]
        [SugarColumn(ColumnName = "_vndcli_com", ColumnDescription = "% de comision asignada por cliente", IsNullable = false, Length = 11, DecimalDigits = 4)]
        public decimal Comision
        {
            get { return comision; }
            set 
            { 
                comision = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_vndcli_usr_n")]
        [SugarColumn(ColumnName = "_vndcli_usr_n", ColumnDescription = "clave del usuario que crea el registro", IsOnlyIgnoreUpdate = true, IsNullable = false, Length = 10)]
        public string Creo
        {
            get { return creo; }
            set 
            { 
                creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_vndcli_usr_m")]
        [SugarColumn(ColumnName = "_vndcli_usr_m", ColumnDescription = "clave del ultimo usuaqui que modifica el registro", IsOnlyIgnoreInsert = true, IsNullable = true, Length = 10)]
        public string Modifica
        {
            get { return modifica; }
            set 
            { 
                modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_vndcli_fn")]
        [SugarColumn(ColumnName = "_vndcli_fn", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo
        {
            get { return fechaNuevo; }
            set 
            { 
                fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_vndcli_fm")]
        [SugarColumn(ColumnName = "_vndcli_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsOnlyIgnoreInsert = true, IsNullable = true)]
        public DateTime? FechaModifica
        {
            get { return fechaModifica; }
            set 
            { 
                fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
        
    }
}
