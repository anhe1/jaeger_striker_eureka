﻿/// develop:
/// purpose: clase para representar un objeto contribuyente del directorio, puede contener la informacion de un cliente o proveedor
using System;
using System.ComponentModel;
using System.Linq;
using SqlSugar;
using Jaeger.Edita.V2.Directorio.Enums;
using Jaeger.Edita.V2.Directorio.Interfaces;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.Directorio.Entities
{
    [SugarTable("_drctr")]
    public class ViewModelContribuyente : ViewModelContribuyenteSingle, IContribuyente
    {
        #region declaraciones
        private int lngSubIndex;
        private string notaField;
        private string registroPatronalField;
        private BindingList<ViewModelDomicilio> domiciliosField;
        private BindingList<ViewModelCuentaBancaria> cuentasBancariasField;
        private BindingList<ViewModelContacto> contactosField;
        private byte[] byteFile;

        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ViewModelContribuyente()
        {
            this.Domicilios = new BindingList<ViewModelDomicilio>() { RaiseListChangedEvents = true };
            this.Domicilios.AddingNew += Domicilios_AddingNew;
            this.Domicilios.ListChanged += Domicilios_ListChanged;
            this.CuentasBancarias = new BindingList<ViewModelCuentaBancaria>() { RaiseListChangedEvents = true };
            this.CuentasBancarias.AddingNew += CuentasBancarias_AddingNew;
            this.Contactos = new BindingList<ViewModelContacto>() { RaiseListChangedEvents = true };
            this.Contactos.AddingNew += Contactos_AddingNew;
            this.Contactos.ListChanged += Contactos_ListChanged;
            this.ClaveUsoCFDI = "P01";
            this.IsActive = true;
            this.Avatar = null;
            this.FechaNuevo = DateTime.Now;
        }

        #region propiedades
        
        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_drctr_id")]
        [SugarColumn(ColumnName = "_drctr_id", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public int Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:registro activo=1 registro inactivo=2
        /// Default:1
        /// Nullable:False
        /// </summary>           
        [DataNames("_drctr_a")]
        [SugarColumn(ColumnName = "_drctr_a", ColumnDescription = "registro activo=1 registro inactivo=2", IsNullable = false, Length = 1)]
        public bool IsActive
        {
            get
            {
                return base.IsActive;
            }
            set
            {
                base.IsActive = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public int SubId
        {
            get
            {
                return this.lngSubIndex;
            }
            set
            {
                if (this.lngSubIndex != value)
                {
                    this.lngSubIndex = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Desc:
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_dsp")]
        [SugarColumn(ColumnName = "_drctr_dsp", IsNullable = true)]
        public int DiasEntrega
        {
            get
            {
                return base.DiasEntrega;
            }
            set
            {
                base.DiasEntrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:monto cobrado
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_crd")]
        [SugarColumn(ColumnName = "_drctr_crd", IsNullable = false)]
        public decimal Credito
        {
            get
            {
                return base.Credito;
            }
            set
            {
                base.Credito = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:descuento pactado
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_pctd")]
        [SugarColumn(ColumnName = "_drctr_pctd", IsNullable = false)]
        public decimal FactorDescuento
        {
            get
            {
                return base.FactorDescuento;
            }
            set
            {
                base.FactorDescuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:id de catalogo de descuentos asignado cuando es cliente
        /// Default:0
        /// Nullable:False
        /// </summary>           
        [DataNames("_drctr_dscnt_id")]
        [SugarColumn(ColumnName = "_drctr_dscnt_id", ColumnDescription = "id de catalogo de descuentos asignado cuando es cliente", IsNullable = true, Length = 11)]
        public int IndexDescuento
        {
            get
            {
                return base.IndexDescuento;
            }
            set
            {
                base.IndexDescuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:iva pactado
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_iva")]
        [SugarColumn(ColumnName = "_drctr_iva", IsNullable = false)]
        public decimal FactorIvaPactado
        {
            get
            {
                return base.FactorIvaPactado;
            }
            set
            {
                base.FactorIvaPactado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:regimen fiscal (fisica o moral)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_rgf")]
        [SugarColumn(ColumnName = "_drctr_rgf", IsNullable = false)]
        public string RegimenText
        {
            get
            {
                return base.RegimenText;
            }
            set
            {
                base.RegimenText = value;
            }
        }

        /// <summary>
        /// Desc:clave unica de registro en sistema
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_clv")]
        [SugarColumn(ColumnName = "_drctr_clv", IsNullable = false)]
        public string Clave
        {
            get
            {
                return base.Clave;
            }
            set
            {
                base.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:CURP: requerido para la expresión de la CURP del trabajador
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_curp")]
        [SugarColumn(ColumnName = "_drctr_curp", IsNullable = false)]
        public string CURP
        {
            get
            {
                return base.CURP; ;
            }
            set
            {
                base.CURP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Reg. Fed. De Contribuyentes (RFC)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_rfc")]
        [SugarColumn(ColumnName = "_drctr_rfc", IsNullable = false)]
        public string RFC
        {
            get
            {
                return base.RFC;
            }
            set
            {
                base.RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:nombre y apellido o razon social
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_nom")]
        [SugarColumn(ColumnName = "_drctr_nom", IsNullable = false)]
        public string Nombre
        {
            get
            {
                return base.Nombre;
            }
            set
            {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:telefonos de contacto, separados por (;)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_tel")]
        [SugarColumn(ColumnName = "_drctr_tel", IsNullable = false)]
        public string Telefono
        {
            get
            {
                return base.Telefono;
            }
            set
            {
                base.Telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:correo electronic, separados por (;)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_mail")]
        [SugarColumn(ColumnName = "_drctr_mail", IsNullable = false)]
        public string Correo
        {
            get
            {
                return base.Correo;
            }
            set
            {
                base.Correo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:sitio web
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_web")]
        [SugarColumn(ColumnName = "_drctr_web", IsNullable = false)]
        public string SitioWEB
        {
            get
            {
                return base.SitioWEB;
            }
            set
            {
                base.SitioWEB = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:relacion comercial con la empresa (27=Cliente,163=Proveedor)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_rlcn")]
        [SugarColumn(ColumnName = "_drctr_rlcn", IsNullable = false)]
        public string Relacion
        {
            get
            {
                return base.Relacion;
            }
            set
            {
                base.Relacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:notas adicionales del contacto
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_nts")]
        [SugarColumn(ColumnName = "_drctr_nts", IsNullable = false)]
        public string Nota
        {
            get
            {
                return this.notaField;
            }
            set
            {
                this.notaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:usuario creo
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_usr_n")]
        [SugarColumn(ColumnName = "_drctr_usr_n", IsNullable = false)]
        public string Creo
        {
            get
            {
                return base.Creo;
            }
            set
            {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ultimo usuario que modifico
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_usr_m")]
        [SugarColumn(ColumnName = "_drctr_usr_m", IsNullable = false)]
        public string Modifica
        {
            get
            {
                return base.Modifica;
            }
            set
            {
                base.Modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de creacion
        /// Default:'0000-00-00 00:00:00'
        /// Nullable:False
        /// </summary>           
        [DataNames("_drctr_fn")]
        [SugarColumn(ColumnName = "_drctr_fn", ColumnDescription = "fecha de creacion", IsNullable = false)]
        public DateTime FechaNuevo
        {
            get
            {
                return base.FechaNuevo;
            }
            set
            {
                base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de modificacion
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_fm")]
        [SugarColumn(ColumnName = "_drctr_fm", ColumnDescription = "fecha ultima de modificacion", IsNullable = false)]
        public DateTime? FechaModifica
        {
            get
            {
                return base.FechaModifica;
            }
            set
            {
                base.FechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:regimen fiscal
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_rgfsc")]
        [SugarColumn(ColumnName = "_drctr_rgfsc", IsNullable = false)]
        public string RegimenFiscal
        {
            get
            {
                return base.RegimenFiscal;
            }
            set
            {
                base.RegimenFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:número de registro de identidad fiscal
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_nmreg")]
        [SugarColumn(ColumnName = "_drctr_nmreg", IsNullable = true)]
        public string NumRegIdTrib
        {
            get
            {
                return base.NumRegIdTrib;
            }
            set
            {
                base.NumRegIdTrib = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave del país de residencia para efectos fiscales del receptor del comprobante
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_resfis")]
        [SugarColumn(ColumnName = "_drctr_resfis", IsNullable = false)]
        public string ResidenciaFiscal
        {
            get
            {
                return base.ResidenciaFiscal;
            }
            set
            {
                base.ResidenciaFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave de uso de cfdi por default
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_usocfdi")]
        [SugarColumn(ColumnName = "_drctr_usocfdi", IsNullable = true)]
        public string ClaveUsoCFDI
        {
            get
            {
                return base.ClaveUsoCFDI;
            }
            set
            {
                base.ClaveUsoCFDI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Registgro patronal
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_rgstrptrnl")]
        [SugarColumn(ColumnName = "_drctr_rgstrptrnl", IsNullable = false)]
        public string RegistroPatronal
        {
            get
            {
                return this.registroPatronalField;
            }
            set
            {
                this.registroPatronalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:imagen
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_file")]
        [SugarColumn(ColumnName = "_drctr_file", IsNullable = false)]
        public byte[] Avatar
        {
            get
            {
                return this.byteFile;
            }
            set
            {
                this.byteFile = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public int DiasCredito
        {
            get
            {
                return base.DiasCredito;
            }
            set
            {
                base.DiasCredito = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region propiedades ignoradas por SqlSugar

        /// <summary>
        /// regimen fiscal (fisica o moral)
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public EnumRegimen Regimen
        {
            get
            {
                return base.Regimen;
            }
            set
            {
                base.Regimen = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ViewModelDomicilio> Domicilios
        {
            get
            {
                return this.domiciliosField;
            }
            set
            {
                if (this.domiciliosField != null)
                {
                    this.Domicilios.AddingNew -= Domicilios_AddingNew;
                    this.Domicilios.ListChanged -= Domicilios_ListChanged;
                }
                this.domiciliosField = value;
                if (this.domiciliosField != null)
                {
                    this.Domicilios.AddingNew += Domicilios_AddingNew;
                    this.Domicilios.ListChanged += Domicilios_ListChanged;
                }
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ViewModelCuentaBancaria> CuentasBancarias
        {
            get
            {
                return this.cuentasBancariasField;
            }
            set
            {
                if (this.cuentasBancariasField != null)
                {
                    this.CuentasBancarias.AddingNew -= this.CuentasBancarias_AddingNew;
                }
                this.cuentasBancariasField = value;
                if (this.cuentasBancariasField != null)
                {
                    this.CuentasBancarias.AddingNew += this.CuentasBancarias_AddingNew;
                }
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ViewModelContacto> Contactos
        {
            get
            {
                return this.contactosField;
            }
            set
            {
                this.contactosField = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region metodos

        private void Domicilios_AddingNew(object sender, AddingNewEventArgs e)
        {
            e.NewObject = new ViewModelDomicilio() { SubId = this.Id, Creo = this.Creo, FechaNuevo = DateTime.Now };
        }

        private void Domicilios_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemAdded)
            {
                this.Domicilios[e.NewIndex].Creo = this.Creo;
            }
            else if (e.ListChangedType == ListChangedType.ItemChanged && this.Domicilios[e.NewIndex].Id > 0)
            {
                //this.Domicilios[e.NewIndex].Modifica = this.Modifica;
            }
        }

        private void CuentasBancarias_AddingNew(object sender, AddingNewEventArgs e)
        {
            e.NewObject = new ViewModelCuentaBancaria { SubId = this.Id, Creo = this.Creo, FechaNuevo = DateTime.Now };
        }

        private void Contactos_AddingNew(object sender, AddingNewEventArgs e)
        {
            e.NewObject = new ViewModelContacto() { SubId = this.Id, Creo = this.Creo, FechaNuevo = DateTime.Now };
        }

        private void Contactos_ListChanged(object sender, ListChangedEventArgs e)
        {
            
        }
        #endregion
    }
}
