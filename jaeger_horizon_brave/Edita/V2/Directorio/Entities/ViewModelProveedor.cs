﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Edita.V2.Directorio.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class ViewModelProveedor : BasePropertyChangeImplementation
    {
        private int index;
        private string nombre;
        private string rfc;
        private string vendedor;
        private string telefono;
        private string correo;
        private ViewModelDomicilio domicilioFiscal;

        public ViewModelProveedor()
        {
            this.DomicilioFiscal = new ViewModelDomicilio();
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public int Id
        {
            get
            {
                return this.index;
            }
            set
            {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string Nombre
        {
            get
            {
                return this.nombre;
            }
            set
            {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string RFC
        {
            get
            {
                return this.rfc;
            }
            set
            {
                this.rfc = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string Vendedor
        {
            get
            {
                return this.vendedor;
            }
            set
            {
                this.vendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string Telefono
        {
            get
            {
                return this.telefono;
            }
            set
            {
                this.telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string Correo
        {
            get
            {
                return this.correo;
            }
            set
            {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public ViewModelDomicilio DomicilioFiscal
        {
            get
            {
                return this.domicilioFiscal;
            }
            set
            {
                this.domicilioFiscal = value;
                this.OnPropertyChanged();
            }
        }
    }
}
