﻿using Jaeger.Edita.V2.Directorio.Interfaces;
using Newtonsoft.Json;
using System;
using System.Diagnostics;

namespace Jaeger.Edita.V2.Directorio.Entities
{
    [JsonObject("domicilioFiscal")]
    public class DomicilioFiscal : ViewModelDomicilio, IDomicilioFiscal
    {
        public new string Calle
        {
            get
            {
                return base.Calle;
            }
            set
            {
                base.Calle = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_cdd")]
        public new string Ciudad
        {
            get
            {
                return base.Ciudad;
            }
            set
            {
                base.Ciudad = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_cpa")]
        public string CodigoPais
        {
            get
            {
                return base.CodigoDePais;
            }
            set
            {
                base.CodigoDePais = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_cp")]
        public new string CodigoPostal
        {
            get
            {
                return base.CodigoPostal;
            }
            set
            {
                base.CodigoPostal = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_cln")]
        public new string Colonia
        {
            get
            {
                return base.Colonia;
            }
            set
            {
                base.Colonia = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_dlg")]
        public string Delegacion
        {
            get
            {
                return base.Municipio;
            }
            set
            {
                base.Municipio = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_std")]
        public new string Estado
        {
            get
            {
                return base.Estado;
            }
            set
            {
                base.Estado = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_lcldd")]
        public new string Localidad
        {
            get
            {
                return base.Localidad;
            }
            set
            {
                base.Localidad = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_extr")]
        public new string NoExterior
        {
            get
            {
                return base.NoExterior;
            }
            set
            {
                base.NoExterior = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_intr")]
        public new string NoInterior
        {
            get
            {
                return base.NoInterior;
            }
            set
            {
                base.NoInterior = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_ps")]
        public new string Pais
        {
            get
            {
                return base.Pais;
            }
            set
            {
                base.Pais = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_rfrnc")]
        public new string Referencia
        {
            get
            {
                return base.Referencia;
            }
            set
            {
                base.Referencia = value;
                this.OnPropertyChanged();
            }
        }

        [DebuggerNonUserCode]
        public DomicilioFiscal()
        {
        }

        public override string ToString()
        {
            string direccion = string.Empty;

            if (!(String.IsNullOrEmpty(this.Calle)))
            {
                direccion = string.Concat("Calle ", this.Calle);
            }

            if (!(String.IsNullOrEmpty(this.NoExterior)))
            {
                direccion = string.Concat(direccion, " No. Ext. ", this.NoExterior);
            }

            if (!(String.IsNullOrEmpty(this.NoInterior)))
            {
                direccion = string.Concat(direccion, " No. Int. ", this.NoInterior);
            }

            if (!(String.IsNullOrEmpty(this.Colonia)))
            {
                direccion = string.Concat(direccion, " Col. ", this.Colonia);
            }

            if (!(String.IsNullOrEmpty(this.CodigoPostal)))
            {
                direccion = string.Concat(direccion, " C.P. ", this.CodigoPostal);
            }

            if (!(String.IsNullOrEmpty(this.Delegacion)))
            {
                direccion = string.Concat(direccion, " ", this.Delegacion);
            }

            if (!(String.IsNullOrEmpty(this.Estado)))
            {
                direccion = string.Concat(direccion, ", ", this.Estado);
            }

            if (!(String.IsNullOrEmpty(this.Referencia)))
            {
                direccion = string.Concat(direccion, ", Ref: ", this.Referencia);
            }

            if (!(String.IsNullOrEmpty(this.Localidad)))
            {
                direccion = string.Concat(direccion, ", Loc: ", this.Localidad);
            }
            return direccion;
        }
    }
}