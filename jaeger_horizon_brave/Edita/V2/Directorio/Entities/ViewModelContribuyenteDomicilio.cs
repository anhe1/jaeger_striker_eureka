﻿/// develop:
/// purpose: para crear una vista de contribuyentes que contega la direccion fiscal
using System;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.Directorio.Entities
{
    public class ViewModelContribuyenteDomicilio 
    {
        public ViewModelContribuyenteDomicilio()
        {
            this.DomicilioFiscal = new ViewModelDomicilio();
        }

        [DataNames("_drctr_id")]
        public int Id { get; set; }

        [DataNames("_drctr_clv")]
        public string Clave { get; set; }

        [DataNames("_drctr_rfc")]
        public string RFC { get; set; }

        [DataNames("_drctr_curp")]
        public string CURP { get; set; }

        [DataNames("_drctr_rlcn")]
        public string Relacion { get; set; }

        [DataNames("_drctr_nom")]
        public string Nombre { get; set; }

        [DataNames("_drctr_rgfsc")]
        public string RegimenFiscal { get; set; }

        [DataNames("_drctr_usocfdi")]
        public string ClaveUsoCFDI { get; set; }

        [DataNames("_drctr_resfis")]
        public string ResidenciaFiscal { get; set; }

        [DataNames("_drctr_nmreg")]
        public string NumRegIdTrib { get; set; }

        [DataNames("_drctr_mail")]
        public string Correo { get; set; }

        [DataNames("_drccn_cll")]
        public string Calle
        {
            get
            {
                return this.DomicilioFiscal.Calle;
            }
            set
            {
                this.DomicilioFiscal.Calle = value;
            }
        }

        [DataNames("_drccn_extr")]
        public string NoExterior
        {
            get
            {
                return this.DomicilioFiscal.NoExterior;
            }
            set
            {
                this.DomicilioFiscal.NoExterior = value;
            }
        }

        [DataNames("_drccn_intr")]
        public string NoInterior
        {
            get
            {
                return this.DomicilioFiscal.NoInterior;
            }
            set
            {
                this.DomicilioFiscal.NoInterior = value;
            }
        }

        [DataNames("_drccn_cln")]
        public string Colonia
        {
            get
            {
                return this.DomicilioFiscal.Colonia;
            }
            set
            {
                this.DomicilioFiscal.Colonia = value;
            }
        }

        [DataNames("_drccn_cp")]
        public string CodigoPostal
        {
            get
            {
                return this.DomicilioFiscal.CodigoPostal;
            }
            set
            {
                this.DomicilioFiscal.CodigoPostal = value;
            }
        }

        [DataNames("_drccn_std")]
        public string Estado
        {
            get
            {
                return this.DomicilioFiscal.Estado;
            }
            set
            {
                this.DomicilioFiscal.Estado = value;
            }
        }

        [DataNames("_drccn_dlg")]
        public string Municipio
        {
            get
            {
                return this.DomicilioFiscal.Municipio;
            }
            set
            {
                this.DomicilioFiscal.Municipio = value;
            }
        }

        [DataNames("_drccn_ps")]
        public string Pais
        {
            get
            {
                return this.DomicilioFiscal.Pais;
            }
            set
            {
                this.DomicilioFiscal.Pais = value;
            }
        }

        [DataNames("_drccn_lcldd")]
        public string Localidad
        {
            get
            {
                return this.DomicilioFiscal.Localidad;
            }
            set
            {
                this.DomicilioFiscal.Localidad = value;
            }
        }

        [DataNames("_drccn_rfrnc")]
        public string Referencia
        {
            get
            {
                return this.DomicilioFiscal.Referencia;
            }
            set
            {
                this.DomicilioFiscal.Referencia = value;
            }
        }

        [DataNames("_drctr_tel")]
        public string Telefono
        {
            get
            {
                return this.DomicilioFiscal.Telefono;
            }
            set
            {
                this.DomicilioFiscal.Telefono = value;
            }
        }

        [DataNames("_drccn_cdd")]
        public string Ciudad
        {
            get
            {
                return this.DomicilioFiscal.Ciudad;
            }
            set
            {
                this.DomicilioFiscal.Ciudad = value;
            }
        }

        [DataNames("_drctr_usr_n")]
        public string Creo { get; set; }

        [DataNames("_drctr_fn")]
        public DateTime FechaNuevo { get; set; }

        public ViewModelDomicilio DomicilioFiscal { get; set; }
    }
}
