﻿using System;
using Newtonsoft.Json;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Edita.V2.Directorio.Enums;
using Jaeger.Edita.V2.Directorio.Interfaces;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Edita.V2.Directorio.Entities
{
    [SugarTable("_drccn", "directorio: domicilios")]
    public class ViewModelDomicilio : BasePropertyChangeImplementation, IDomicilio
    {
        private int indiceField;
        private bool activoField;
        private int directorioIdField;
        private EnumDomicilioTipo enumTypeOfAddress;
        private string codigoPaisField;
        private string creoField;
        private string modificoField;
        private string codigoPostalField;
        private string asentamientoField;
        private string noExteriorField;
        private string noInteriorField;
        private string calleField;
        private string coloniaField;
        private string delegacionField;
        private string ciudadField;
        private string estadoField;
        private string paisField;
        private string localidadField;
        private string referenciaField;
        private string telefonoField;
        private string ubicacionField;
        private string requeridoField;
        private DateTime fechaNuevoField;
        private DateTime? fechaModificaField;

        public ViewModelDomicilio()
        {
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>           
        [JsonProperty("id")]
        [DataNames("_drccn_id")]
        [SugarColumn(ColumnName = "_drccn_id", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:registro activo
        /// Default:1
        /// Nullable:False
        /// </summary>           
        [JsonIgnore]
        [DataNames("_drccn_a")]
        [SugarColumn(ColumnName = "_drccn_a", ColumnDescription = "registro activo", IsNullable = false, Length = 1)]
        public bool IsActive
        {
            get
            {
                return this.activoField;
            }
            set
            {
                this.activoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:relacion con el directorio
        /// Default:0
        /// Nullable:False
        /// </summary>           
        [JsonProperty("iddir")]
        [DataNames("_drccn_drctr_id")]
        [SugarColumn(ColumnName = "_drccn_drctr_id", ColumnDescription = "indice del relacion con el directorio", IsNullable = false, Length = 11)]
        public int SubId
        {
            get
            {
                return this.directorioIdField;
            }
            set
            {
                this.directorioIdField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:codigo de pais
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("cdgops")]
        [DataNames("_drccn_cdgps")]
        [SugarColumn(ColumnName = "_drccn_cdgps", ColumnDescription = "codigo de pais", IsNullable = true, Length = 3)]
        public string CodigoDePais
        {
            get
            {
                return this.codigoPaisField;
            }
            set
            {
                this.codigoPaisField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:usuario creo
        /// Default:
        /// Nullable:False
        /// </summary>           
        [JsonIgnore]
        [DataNames("_drccn_usr_n")]
        [SugarColumn(ColumnName = "_drccn_usr_n", ColumnDescription = "usuario creo", IsNullable = false, Length = 10)]
        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ultimo usuario que modifico
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_drccn_usr_m")]
        [SugarColumn(ColumnName = "_drccn_usr_m", ColumnDescription = "usuario que modifico", IsNullable = true, Length = 10)]
        public string Modifica
        {
            get
            {
                return this.modificoField;
            }
            set
            {
                this.modificoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:codigo postal
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("cp")]
        [DataNames("_drccn_cp")]
        [SugarColumn(ColumnName = "_drccn_cp", ColumnDescription = "codigo postal", IsNullable = true, Length = 10)]
        public string CodigoPostal
        {
            get
            {
                return this.codigoPostalField;
            }
            set
            {
                this.codigoPostalField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public EnumDomicilioTipo Tipo
        {
            get
            {
                return this.enumTypeOfAddress;
            }
            set
            {
                this.enumTypeOfAddress = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=Envio)
        /// Default:
        /// Nullable:False
        /// </summary>           
        [JsonProperty("tp")]
        [DataNames("_drccn_tp")]
        [SugarColumn(ColumnName = "_drccn_tp", ColumnDescription = "", IsNullable = true, Length = 0)]
        public string TipoText
        {
            get
            {
                return Enum.GetName(typeof(EnumDomicilioTipo), this.enumTypeOfAddress);
            }
            set
            {
                this.enumTypeOfAddress = (EnumDomicilioTipo)(Enum.Parse(typeof(EnumDomicilioTipo), value));
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:asentamiento humano
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("ase")]
        [DataNames("_drccn_asnh")]
        [SugarColumn(ColumnName = "_drccn_asnh", ColumnDescription = "tipo de asentamiento humano", IsNullable = true, Length = 0)]
        public string Asentamiento
        {
            get
            {
                return this.asentamientoField;;
            }
            set
            {
                this.asentamientoField = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Desc:numero exterior
        /// Default:
        /// Nullable:False
        /// </summary>           
        [JsonProperty("noext")]
        [DataNames("_drccn_extr")]
        [SugarColumn(ColumnName = "_drccn_extr", ColumnDescription = "numero exterior", IsNullable = true, Length = 0)]
        public string NoExterior
        {
            get
            {
                return this.noExteriorField;
            }
            set
            {
                this.noExteriorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:numero interior
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("noint")]
        [DataNames("_drccn_intr")]
        [SugarColumn(ColumnName = "_drccn_intr", ColumnDescription = "numero interior", IsNullable = true, Length = 0)]
        public string NoInterior
        {
            get
            {
                return this.noInteriorField;
            }
            set
            {
                this.noInteriorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:calle
        /// Default:
        /// Nullable:False
        /// </summary>           
        [JsonProperty("cll")]
        [DataNames("_drccn_cll")]
        [SugarColumn(ColumnName = "_drccn_cll", ColumnDescription = "calle", IsNullable = true, Length = 0)]
        public string Calle
        {
            get
            {
                return this.calleField;
            }
            set
            {
                this.calleField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:colonia
        /// Default:
        /// Nullable:False
        /// </summary>           
        [JsonProperty("col")]
        [DataNames("_drccn_cln")]
        [SugarColumn(ColumnName = "_drccn_cln", ColumnDescription = "colonia", IsNullable = true, Length = 0)]
        public string Colonia
        {
            get
            {
                return this.coloniaField;
            }
            set
            {
                this.coloniaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:delegacion / municipio
        /// Default:
        /// Nullable:False
        /// </summary>           
        [JsonProperty("dlg")]
        [DataNames("_drccn_dlg")]
        [SugarColumn(ColumnName = "_drccn_dlg", ColumnDescription = "delegacion o municipio", IsNullable = true, Length = 0)]
        public string Municipio
        {
            get
            {
                return this.delegacionField;
            }
            set
            {
                this.delegacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ciudad
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("cdd")]
        [DataNames("_drccn_cdd")]
        [SugarColumn(ColumnName = "_drccn_cdd", ColumnDescription = "ciudad", IsNullable = true, Length = 0)]
        public string Ciudad
        {
            get
            {
                return this.ciudadField;
            }
            set
            {
                this.ciudadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:estado
        /// Default:
        /// Nullable:False
        /// </summary>           
        [JsonProperty("est")]
        [DataNames("_drccn_std")]
        [SugarColumn(ColumnName = "_drccn_std", ColumnDescription = "estado", IsNullable = true, Length = 0)]
        public string Estado
        {
            get
            {
                return this.estadoField;
            }
            set
            {
                this.estadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:pais
        /// Default:
        /// Nullable:False
        /// </summary>           
        [JsonProperty("ps")]
        [DataNames("_drccn_ps")]
        [SugarColumn(ColumnName = "_drccn_ps", ColumnDescription = "Pais", IsNullable = true, Length = 0)]
        public string Pais
        {
            get
            {
                return this.paisField;
            }
            set
            {
                this.paisField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:localidad
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("loc")]
        [DataNames("_drccn_lcldd")]
        [SugarColumn(ColumnName = "_drccn_lcldd", ColumnDescription = "localidad", IsNullable = true, Length = 0)]
        public string Localidad
        {
            get
            {
                return this.localidadField;
            }
            set
            {
                this.localidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:referencia
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("ref")]
        [DataNames("_drccn_rfrnc")]
        [SugarColumn(ColumnName = "_drccn_rfrnc", ColumnDescription = "referencia", IsNullable = true, Length = 0)]
        public string Referencia
        {
            get
            {
                return this.referenciaField;
            }
            set
            {
                this.referenciaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:telefonos de contacto
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("tel")]
        [DataNames("_drccn_tlfns")]
        [SugarColumn(ColumnName = "_drccn_tlfns", ColumnDescription = "telefonos de contactos", IsNullable = true, Length = 0)]
        public string Telefono
        {
            get
            {
                return this.telefonoField;
            }
            set
            {
                this.telefonoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:descripcion de la ubicacion
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_drccn_dsp")]
        [SugarColumn(ColumnName = "_drccn_dsp", ColumnDescription = "descripcion de la ubicacion", IsNullable = true, Length = 0)]
        public string Notas
        {
            get
            {
                return this.ubicacionField;
            }
            set
            {
                this.ubicacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:horario y requisito de acceso
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_drccn_req")]
        [SugarColumn(ColumnName = "_drccn_req", ColumnDescription = "horario y requisito de acceso", IsNullable = true, Length = 0)]
        public string Requerimiento
        {
            get
            {
                return this.requeridoField;
            }
            set
            {
                this.requeridoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fec. sist.
        /// Default:current_timestamp()
        /// Nullable:False
        /// </summary>           
        [JsonIgnore]
        [DataNames("_drccn_fn")]
        [SugarColumn(ColumnName = "_drccn_fn", ColumnDescription = "fecha de sistema", IsNullable = false)]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de modificacion
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_drccn_fm")]
        [SugarColumn(ColumnName = "_drccn_fm", ColumnDescription = "ultima fecha de modificacion", IsNullable = true, Length = 0)]
        public DateTime? FechaModifica
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);

                if (this.fechaModificaField >= firstGoodDate)
                    return this.fechaModificaField;
                return null;
            }
            set
            {
                this.fechaModificaField = value;
                this.OnPropertyChanged();
            }
        }

        public override string ToString()
        {
            string direccion = string.Empty;

            if (!(String.IsNullOrEmpty(this.Calle)))
            {
                direccion = string.Concat("Calle ", this.Calle);
            }

            if (!(String.IsNullOrEmpty(this.NoExterior)))
            {
                direccion = string.Concat(direccion, " No. Ext. ", this.NoExterior);
            }

            if (!(String.IsNullOrEmpty(this.NoInterior)))
            {
                direccion = string.Concat(direccion, " No. Int. ", this.NoInterior);
            }

            if (!(String.IsNullOrEmpty(this.Colonia)))
            {
                direccion = string.Concat(direccion, " Col. ", this.Colonia);
            }

            if (!(String.IsNullOrEmpty(this.CodigoPostal)))
            {
                direccion = string.Concat(direccion, " C.P. ", this.CodigoPostal);
            }

            if (!(String.IsNullOrEmpty(this.Municipio)))
            {
                direccion = string.Concat(direccion, " ", this.Municipio);
            }

            if (!(String.IsNullOrEmpty(this.Estado)))
            {
                direccion = string.Concat(direccion, ", ", this.Estado);
            }

            if (!(String.IsNullOrEmpty(this.Referencia)))
            {
                direccion = string.Concat(direccion, ", Ref: ", this.Referencia);
            }

            if (!(String.IsNullOrEmpty(this.Localidad)))
            {
                direccion = string.Concat(direccion, ", Loc: ", this.Localidad);
            }
            return direccion;
        }

        public string Json(Formatting objFormat = 0)
        {
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy" };
            return JsonConvert.SerializeObject(this, objFormat, conf);
        }

        public static ViewModelDomicilio Json(string inputString)
        {
            try
            {
                var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy" };
                return JsonConvert.DeserializeObject<ViewModelDomicilio>(inputString, conf);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
