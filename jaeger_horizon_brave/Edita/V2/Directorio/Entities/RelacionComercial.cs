﻿using System;

namespace Jaeger.Edita.V2.Directorio.Entities
{
    public class RelacionComercial : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private int idField;

        private bool selectedField;

        private string nameField;

        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
                this.OnPropertyChanged();
            }
        }

        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
                this.OnPropertyChanged();
            }
        }

        public bool Selected
        {
            get
            {
                return this.selectedField;
            }
            set
            {
                this.selectedField = value;
                this.OnPropertyChanged();
            }
        }

        public RelacionComercial()
        {
            this.selectedField = true;
            this.nameField = string.Empty;
        }
    }
}