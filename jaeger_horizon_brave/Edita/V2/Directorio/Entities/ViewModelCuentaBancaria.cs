﻿/// purpose: descripcion de la cuenta bancaria de un contacto en el directorio
using System;
using System.Linq;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Jaeger.Edita.V2.Directorio.Interfaces;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;

namespace Jaeger.Edita.V2.Directorio.Entities
{
    [SugarTable("_dcbnc")]
    public class ViewModelCuentaBancaria : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation, ICuentaBancaria, IDataErrorInfo
    {
        private long indiceField;
        private long subIndiceField;
        private bool isActiveField = true;
        private double cargoMaximoField;
        private string codeField;
        private string monedaField;
        private string sucursalField;
        private string numeroDeCuentaField;
        private string clabeField;
        private string insitucionBancariaNCField;
        private string aliasField;
        private string bancoField;
        private string nombreField;
        private string primerApellidoField;
        private string segundoApellidoField;
        private string rfcField;
        private string tipoCuentaField;
        private string refNumericaField;
        private string refAlfanumericaField;
        private bool verificadoField;
        private DateTime fechaNuevoField;
        private DateTime? fechaModificaField;
        private string creoField;
        private string modificaField;

        /// <summary>
        /// constructor
        /// </summary>
        public ViewModelCuentaBancaria()
        {
        }

        [DataNames("_dcbnc_id", "_dcbnc_id")]
        [SugarColumn(ColumnName = "_dcbnc_id", ColumnDescription = "", IsIdentity = true, IsPrimaryKey = true, Length = 11)]
        public long Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_dcbnc_drctr_id", "_dcbnc_drctr_id")]
        [SugarColumn(ColumnName = "_dcbnc_drctr_id", ColumnDescription = "", IsNullable = true, Length = 11)]
        public long SubId
        {
            get
            {
                return this.subIndiceField;
            }
            set
            {
                this.subIndiceField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_dcbnc_a", "_dcbnc_a")]
        [SugarColumn(ColumnName = "_dcbnc_a", ColumnDescription = "", IsNullable = true, Length = 1)]
        public bool IsActive
        {
            get
            {
                return this.isActiveField;
            }
            set
            {
                this.isActiveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// alias de la cuenta
        /// </summary>
        [DataNames("_dcbnc_alias", "_dcbnc_alias")]
        [SugarColumn(ColumnName = "_dcbnc_alias", ColumnDescription = "", IsNullable = true, Length = 255)]
        public string Alias
        {
            get
            {
                return this.aliasField;
            }
            set
            {
                this.aliasField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del banco
        /// </summary>
        [DataNames("_dcbnc_banco", "_dcbnc_banco")]
        [SugarColumn(ColumnName = "_dcbnc_banco", ColumnDescription = "", IsNullable = true, Length = 255)]
        public string Banco
        {
            get
            {
                return this.bancoField;
            }
            set
            {
                this.bancoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del beneficiario de la cuenta
        /// </summary>
        [DataNames("_dcbnc_bnfcr", "_dcbnc_bnfcr")]
        [SugarColumn(ColumnName = "_dcbnc_bnfcr", ColumnDescription = "", IsNullable = true, Length = 300)]
        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer apellido paterno
        /// </summary>
        [DataNames("_dcbnc_prapll", "_dcbnc_prapll")]
        [SugarColumn(ColumnName = "_dcbnc_prapll", ColumnDescription = "", IsNullable = true, Length = 125)]
        public string PrimerApellido
        {
            get
            {
                return this.primerApellidoField;
            }
            set
            {
                this.primerApellidoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer apellido materno
        /// </summary>
        [DataNames("_dcbnc_sgapll", "_dcbnc_sgapll")]
        [SugarColumn(ColumnName = "_dcbnc_sgapll", ColumnDescription = "", IsNullable = true, Length = 125)]
        public string SegundoApellido
        {
            get
            {
                return this.segundoApellidoField;
            }
            set
            {
                this.segundoApellidoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cargo maximo que se puede hacer a la cuenta
        /// </summary>
        [DataNames("_dcbnc_crgmx", "_dcbnc_crgmx")]
        [SugarColumn(ColumnName = "_dcbnc_crgmx", ColumnDescription = "", IsNullable = true, Length = 14, DecimalDigits = 4)]
        public double CargoMaximo
        {
            get
            {
                return this.cargoMaximoField;
            }
            set
            {
                this.cargoMaximoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cuenta CLABE
        /// </summary>
        [DataNames("_dcbnc_cntclb", "_dcbnc_cntclb")]
        [SugarColumn(ColumnName = "_dcbnc_cntclb", ColumnDescription = "", IsNullable = true, Length = 50)]
        public string Clabe
        {
            get
            {
                return this.clabeField;
            }
            set
            {
                this.clabeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        [DataNames("_dcbnc_clv", "_dcbnc_clv")]
        [SugarColumn(ColumnName = "_dcbnc_clv", ColumnDescription = "", IsNullable = true, Length = 4)]
        public string Clave
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre corto de la institución bancaria
        /// </summary>
        [DataNames("_dcbnc_nmcrt", "_dcbnc_nmcrt")]
        [SugarColumn(ColumnName = "_dcbnc_nmcrt", ColumnDescription = "", IsNullable = true, Length = 64)]
        public string InsitucionBancaria
        {
            get
            {
                return this.insitucionBancariaNCField;
            }
            set
            {
                this.insitucionBancariaNCField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// clave de la moneda
        /// </summary>
        [DataNames("_dcbnc_mnd", "_dcbnc_mnd")]
        [SugarColumn(ColumnName = "_dcbnc_mnd", ColumnDescription = "", IsNullable = true, Length = 10)]
        public string Moneda
        {
            get
            {
                return this.monedaField;
            }
            set
            {
                this.monedaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de cuenta bancaria
        /// </summary>
        [DataNames("_dcbnc_nmcta", "_dcbnc_nmcta")]
        [SugarColumn(ColumnName = "_dcbnc_nmcta", ColumnDescription = "", IsNullable = true, Length = 50)]
        public string NumeroDeCuenta
        {
            get
            {
                return this.numeroDeCuentaField;
            }
            set
            {
                this.numeroDeCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de la sucursal
        /// </summary>
        [DataNames("_dcbnc_scrsl", "_dcbnc_scrsl")]
        [SugarColumn(ColumnName = "_dcbnc_scrsl", ColumnDescription = "", IsNullable = true, Length = 20)]
        public string Sucursal
        {
            get
            {
                return this.sucursalField;
            }
            set
            {
                this.sucursalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// RFC del beneficiario
        /// </summary>
        [DataNames("_dcbnc_rfc", "_dcbnc_rfc")]
        [SugarColumn(ColumnName = "_dcbnc_rfc", ColumnDescription = "", IsNullable = true, Length = 14)]
        public string Rfc
        {
            get
            {
                return this.rfcField;
            }
            set
            {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_dcbnc_tipo", "_dcbnc_tipo")]
        [SugarColumn(ColumnName = "_dcbnc_tipo", ColumnDescription = "", IsNullable = true, Length = 20)]
        public string TipoCuenta
        {
            get
            {
                return this.tipoCuentaField;
            }
            set
            {
                this.tipoCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// referencia numerica constante utilizada por el beneficiario de la cuenta
        /// </summary>
        [DataNames("_dcbnc_refnum", "_dcbnc_refnum")]
        [SugarColumn(ColumnName = "_dcbnc_refnum", ColumnDescription = "", IsNullable = true, Length = 7)]
        public string RefNumerica
        {
            get
            {
                return this.refNumericaField;
            }
            set
            {
                this.refNumericaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// referencia alfanumerica constante utilizada por el beneficiario de la cuenta
        /// </summary>
        [DataNames("_dcbnc_refalf", "_dcbnc_refalf")]
        [SugarColumn(ColumnName = "_dcbnc_refalf", ColumnDescription = "", IsNullable = true, Length = 20)]
        public string RefAlfanumerica
        {
            get
            {
                return this.refAlfanumericaField;
            }
            set
            {
                if (this.verificadoField = false)
                {
                    this.refAlfanumericaField = value;
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si la cuenta ya se encuentra verificada
        /// </summary>
        [DataNames("_dcbnc_vrfcd")]
        [SugarColumn(ColumnName = "_dcbnc_vrfcd", ColumnDescription = "", IsNullable = true, Length = 1)]
        public bool Verificado
        {
            get
            {
                return this.verificadoField;
            }
            set
            {
                this.verificadoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_dcbnc_fn", "_dcbnc_fn")]
        [SugarColumn(ColumnName = "_dcbnc_fn", ColumnDescription = "", IsNullable = true)]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
            }
        }

        [DataNames("_dcbnc_fm", "_dcbnc_fm")]
        [SugarColumn(ColumnName = "_dcbnc_fm", ColumnDescription = "", IsNullable = true)]
        public DateTime? FechaMod
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModificaField >= firstGoodDate)
                {
                    return this.fechaModificaField;
                }
                return null;
            }
            set
            {
                this.fechaModificaField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_dcbnc_usr_n", "_dcbnc_usr_n")]
        [SugarColumn(ColumnName = "_dcbnc_usr_n", ColumnDescription = "", IsNullable = true, Length = 10)]
        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_dcbnc_usr_m", "_dcbnc_usr_m")]
        [SugarColumn(ColumnName = "_dcbnc_usr_m", ColumnDescription = "", IsNullable = true, Length = 20)]
        public string Modifica
        {
            get
            {
                return this.modificaField;
            }
            set
            {
                this.modificaField = value;
                this.OnPropertyChanged();
            }
        }

        #region metodos

        [SugarColumn(IsIgnore = true)]
        public string Error
        {
            get
            {
                if (!this.RegexValido(this.clabeField, "[0-9]{10,18}$"))
                {
                    return "Por favor ingrese datos válidos en esta fila!";
                }
                return string.Empty;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string this[string columnName]
        {
            get
            {
                if (columnName == "Clabe" && !this.RegexValido(this.clabeField, "[0-9]{10,18}$"))
                {
                    return "Formato de CLABE no válido!";
                }
                return string.Empty;
            }
        }

        private bool RegexValido(string valor, string patron)
        {
            if (!(valor == null))
            {
                bool flag = Regex.IsMatch(valor, string.Concat("^", patron, "$"));
                return flag;
            }
            return false;
        }
        #endregion
    }
}
