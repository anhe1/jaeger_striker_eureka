using System;
using Jaeger.Edita.V2.Directorio.Enums;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;

namespace Jaeger.Edita.V2.Directorio.Entities {
    [SugarTable("_drctr")]
    public class ViewModelContribuyenteSingle : Contribuyente {
        #region declaraciones

        private int diasEntregaField;
        private int diasCreditoField;
        private int indexDescuentoField;
        private decimal factorDescuentoField;
        private decimal factorIvaPactadoField;
        private decimal limiteCreditoField;
        private string regimenFiscalField;
        private string numRegIdTribField;
        private string residenciaFiscalField;
        private string claveUsoCfdiField;
        private EnumRegimen regimenField;
        private string domicilioFiscalReceptor;
        private bool extranjero;

        #endregion

        [DataNames("_drctr_id")]
        [SugarColumn(ColumnName = "_drctr_id", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public new int Id {
            get {
                return base.Id;
            }
            set {
                base.Id = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_a")]
        [SugarColumn(ColumnName = "_drctr_a", ColumnDescription = "registro activo=1 registro inactivo=2", IsNullable = false, Length = 1)]
        public new bool IsActive {
            get {
                return base.IsActive;
            }
            set {
                base.IsActive = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_clv")]
        [SugarColumn(ColumnName = "_drctr_clv", IsNullable = false)]
        public new string Clave {
            get {
                return base.Clave;
            }
            set {
                base.Clave = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_curp")]
        [SugarColumn(ColumnName = "_drctr_curp", IsNullable = false)]
        public new string CURP {
            get {
                return base.CURP;
            }
            set {
                base.CURP = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_rfc")]
        [SugarColumn(ColumnName = "_drctr_rfc", IsNullable = false)]
        public new string RFC {
            get {
                return base.RFC;
            }
            set {
                base.RFC = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_nom")]
        [SugarColumn(ColumnName = "_drctr_nom", IsNullable = false)]
        public new string Nombre {
            get {
                return base.Nombre;
            }
            set {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_mail")]
        [SugarColumn(ColumnName = "_drctr_mail", IsNullable = false)]
        public new string Correo {
            get {
                return base.Correo;
            }
            set {
                base.Correo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_tel")]
        [SugarColumn(ColumnName = "_drctr_tel", IsNullable = false)]
        public new string Telefono {
            get {
                return base.Telefono;
            }
            set {
                base.Telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:sitio web
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_web")]
        [SugarColumn(ColumnName = "_drctr_web", IsNullable = false)]
        public new string SitioWEB {
            get {
                return base.SitioWEB;
            }
            set {
                base.SitioWEB = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:relacion comercial con la empresa (27=Cliente,163=Proveedor)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_rlcn")]
        [SugarColumn(ColumnName = "_drctr_rlcn", IsNullable = false)]
        public new string Relacion {
            get {
                return base.Relacion;
            }
            set {
                base.Relacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_dsp")]
        [SugarColumn(ColumnName = "_drctr_dsp", IsNullable = true)]
        public int DiasEntrega {
            get {
                return this.diasEntregaField;
            }
            set {
                this.diasEntregaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public int DiasCredito {
            get {
                return this.diasCreditoField;
            }
            set {
                this.diasCreditoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_crd")]
        [SugarColumn(ColumnName = "_drctr_crd", IsNullable = false)]
        public decimal Credito {
            get {
                return this.limiteCreditoField;
            }
            set {
                this.limiteCreditoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_pctd")]
        [SugarColumn(ColumnName = "_drctr_pctd", IsNullable = false)]
        public decimal FactorDescuento {
            get {
                return this.factorDescuentoField;
            }
            set {
                this.factorDescuentoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_dscnt_id")]
        [SugarColumn(ColumnName = "_drctr_dscnt_id", ColumnDescription = "id de catalogo de descuentos asignado cuando es cliente", IsNullable = true, Length = 11)]
        public int IndexDescuento {
            get {
                return this.indexDescuentoField;
            }
            set {
                this.indexDescuentoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_iva")]
        [SugarColumn(ColumnName = "_drctr_iva", IsNullable = false)]
        public decimal FactorIvaPactado {
            get {
                return this.factorIvaPactadoField;
            }
            set {
                this.factorIvaPactadoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public new EnumRegimen Regimen {
            get {
                return base.Regimen;
            }
            set {
                this.regimenField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Bandera para identificar la nacionalidad (0 => Nacional, 1 => Extranjero)
        /// </summary>
        [DataNames("_drctr_extrnjr")]
        [SugarColumn(ColumnName = "_drctr_extrnjr", ColumnDescription = "Bandera para identificar la nacionalidad (0 => Nacional, 1 => Extranjero)", IsNullable = true, Length = 1)]
        public bool Extranjero {
            get {
                return this.extranjero;
            }
            set {
                this.extranjero = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_rgf")]
        [SugarColumn(ColumnName = "_drctr_rgf", IsNullable = false)]
        public string RegimenText {
            get {
                return Enum.GetName(typeof(EnumRegimen), this.regimenField);
            }
            set {
                if (!Enum.TryParse<EnumRegimen>(value, out this.regimenField)) {
                    this.regimenField = (EnumRegimen)(Enum.Parse(typeof(EnumRegimen), Enum.GetName(typeof(EnumRegimen), EnumRegimen.NoDefinido)));
                } else {
                    this.regimenField = (EnumRegimen)(Enum.Parse(typeof(EnumRegimen), value));
                }
            }
        }

        [DataNames("_drctr_rgfsc")]
        [SugarColumn(ColumnName = "_drctr_rgfsc", IsNullable = false)]
        public string RegimenFiscal {
            get {
                return this.regimenFiscalField;
            }
            set {
                this.regimenFiscalField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_nmreg")]
        [SugarColumn(ColumnName = "_drctr_nmreg", IsNullable = true)]
        public string NumRegIdTrib {
            get {
                return this.numRegIdTribField;
            }
            set {
                this.numRegIdTribField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_resfis")]
        [SugarColumn(ColumnName = "_drctr_resfis", IsNullable = false)]
        public string ResidenciaFiscal {
            get {
                return this.residenciaFiscalField;
            }
            set {
                this.residenciaFiscalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el c�digo postal del domicilio fiscal del receptor del comprobante
        /// </summary>
        [DataNames("_drctr_domfis")]
        [SugarColumn(ColumnName = "_drctr_domfis", ColumnDescription = "codigo postal del domicilio fiscal del receptor del comprobante", Length = 5, IsNullable = false)]
        public string DomicilioFiscal {
            get { return this.domicilioFiscalReceptor; }
            set {
                this.domicilioFiscalReceptor = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_usocfdi")]
        [SugarColumn(ColumnName = "_drctr_usocfdi", IsNullable = true)]
        public string ClaveUsoCFDI {
            get {
                return this.claveUsoCfdiField;
            }
            set {
                this.claveUsoCfdiField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_usr_n")]
        [SugarColumn(ColumnName = "_drctr_usr_n", IsNullable = false)]
        public new string Creo {
            get {
                return base.Creo;
            }
            set {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_usr_m")]
        [SugarColumn(ColumnName = "_drctr_usr_m", IsNullable = false)]
        public new string Modifica {
            get {
                return base.Modifica;
            }
            set {
                base.Modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_fn")]
        [SugarColumn(ColumnName = "_drctr_fn", ColumnDescription = "fecha de creacion", IsNullable = false)]
        public new DateTime FechaNuevo {
            get {
                return base.FechaNuevo;
            }
            set {
                base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_fm")]
        [SugarColumn(ColumnName = "_drctr_fm", ColumnDescription = "fecha ultima de modificacion", IsNullable = false)]
        public new DateTime? FechaModifica {
            get {
                return base.FechaModifica;
            }
            set {
                base.FechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}