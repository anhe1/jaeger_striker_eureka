using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Edita.V2.Directorio.Entities
{
    public class Persona : BasePropertyChangeImplementation
    {
        private int index;
        private bool activo;
        private string curp;
        private string rfc;
        private string nombre;
        private string creo;
        private string modifica;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;

        public Persona()
        {
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice 
        /// </summary>           
        public int Id
        {
            get
            {
                return this.index;
            }
            set
            {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        public bool IsActive
        {
            get
            {
                return this.activo;
            }
            set
            {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Clave Unica de Registro de Poblacion
        /// </summary>
        public string CURP
        {
            get
            {
                return this.curp;
            }
            set
            {
                this.curp = value.Trim().ToUpper().Replace("-", "");
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// </summary>
        public string RFC
        {
            get
            {
                return this.rfc;
            }
            set
            {
                this.rfc = value.Trim().Replace("-", "").Replace(" ", "").ToUpper(); 
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre(s) de la persona
        /// </summary>
        public string Nombre
        {
            get
            {
                return this.nombre;
            }
            set
            {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        public string Creo
        {
            get
            {
                return this.creo;
            }
            set
            {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        public string Modifica
        {
            get
            {
                return this.modifica;
            }
            set
            {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevo;
            }
            set
            {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime? FechaModifica
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set
            {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}