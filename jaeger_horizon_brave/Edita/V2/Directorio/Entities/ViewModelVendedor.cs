﻿using System;
using System.Linq;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.Directorio.Entities
{
    /// <summary>
    /// clase vendedor
    /// </summary>
    public class ViewModelVendedor : BasePropertyChangeImplementation
    {
        private int index;
        private string claveField;
        private string nombre;
        private string correo;

        /// <summary>
        /// obtener o establecer id principal de la tabla
        /// </summary>           
        [DataNames("_drctr_id")]
        [SugarColumn(ColumnName = "_drctr_id", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public int Id
        {
            get
            {
                return this.index;
            }
            set
            {
                if (value != this.index)
                {
                    this.index = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer clave unica de registro en sistema
        /// </summary>           
        [DataNames("_drctr_clv")]
        [SugarColumn(ColumnName = "_drctr_clv", IsNullable = false)]
        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre y apellido o razon social
        /// </summary>           
        [DataNames("_drctr_nom")]
        [SugarColumn(ColumnName = "_drctr_nom", IsNullable = false)]
        public string Nombre
        {
            get
            {
                return this.nombre;
            }
            set
            {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el correo electronico, separados por (;)
        /// </summary>           
        [DataNames("_drctr_mail")]
        [SugarColumn(ColumnName = "_drctr_mail", IsNullable = false)]   
        public string Correo
        {
            get
            {
                return this.correo;
            }
            set
            {
                this.Correo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
