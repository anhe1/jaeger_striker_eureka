using System;
using Jaeger.Edita.V2.Directorio.Enums;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;

namespace Jaeger.Edita.V2.Directorio.Entities
{
    public class Contribuyente : Persona
    {
        private string clave;
        private string correo;
        private string telefono;
        private string relacion;
        private string sitio;
        private EnumRegimen regimenField;
        private string regimenFiscalField;
        private string numRegIdTribField;
        private string residenciaFiscalField;
        private string domicilioFiscalReceptor;

        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_drctr_id")]
        [SugarColumn(ColumnName = "_drctr_id", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public new int Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_a")]
        [SugarColumn(ColumnName = "_drctr_a", ColumnDescription = "registro activo=1 registro inactivo=2", IsNullable = false, Length = 1)]
        public new bool IsActive
        {
            get
            {
                return base.IsActive;
            }
            set
            {
                base.IsActive = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// regimen fiscal (fisica o moral)
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public EnumRegimen Regimen
        {
            get
            {
                return this.regimenField;
            }
            set
            {
                this.regimenField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave unica de registro en sistema
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_clv")]
        [SugarColumn(ColumnName = "_drctr_clv", IsNullable = false)]
        public string Clave
        {
            get
            {
                return this.clave;
            }
            set
            {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:CURP: requerido para la expresi�n de la CURP del trabajador
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_curp")]
        [SugarColumn(ColumnName = "_drctr_curp", IsNullable = false)]
        public new string CURP
        {
            get
            {
                return base.CURP;
            }
            set
            {
                base.CURP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Reg. Fed. De Contribuyentes (RFC)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_rfc")]
        [SugarColumn(ColumnName = "_drctr_rfc", IsNullable = false)]
        public new string RFC
        {
            get
            {
                return base.RFC;
            }
            set
            {
                base.RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:nombre y apellido o razon social
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_nom")]
        [SugarColumn(ColumnName = "_drctr_nom", IsNullable = false)]
        public new string Nombre
        {
            get
            {
                return base.Nombre;
            }
            set
            {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:correo electronic, separados por (;)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_mail")]
        [SugarColumn(ColumnName = "_drctr_mail", IsNullable = false)]
        public string Correo
        {
            get
            {
                return this.correo;
            }
            set
            {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:telefonos de contacto, separados por (;)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_tel")]
        [SugarColumn(ColumnName = "_drctr_tel", IsNullable = false)]
        public string Telefono
        {
            get
            {
                return this.telefono;
            }
            set
            {
                this.telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:sitio web
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_web")]
        [SugarColumn(ColumnName = "_drctr_web", IsNullable = false)]
        public string SitioWEB
        {
            get
            {
                return this.sitio;
            }
            set
            {
                this.sitio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:relacion comercial con la empresa (27=Cliente,163=Proveedor)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_rlcn")]
        [SugarColumn(ColumnName = "_drctr_rlcn", IsNullable = false)]
        public string Relacion
        {
            get
            {
                return this.relacion;
            }
            set
            {
                this.relacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_rgfsc")]
        [SugarColumn(ColumnName = "_drctr_rgfsc", IsNullable = false)]
        public string RegimenFiscal {
            get {
                return this.regimenFiscalField;
            }
            set {
                this.regimenFiscalField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_nmreg")]
        [SugarColumn(ColumnName = "_drctr_nmreg", IsNullable = true)]
        public string NumRegIdTrib {
            get {
                return this.numRegIdTribField;
            }
            set {
                this.numRegIdTribField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_resfis")]
        [SugarColumn(ColumnName = "_drctr_resfis", IsNullable = false)]
        public string ResidenciaFiscal {
            get {
                return this.residenciaFiscalField;
            }
            set {
                this.residenciaFiscalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el c�digo postal del domicilio fiscal del receptor del comprobante
        /// </summary>
        [DataNames("_drctr_domfis")]
        [SugarColumn(ColumnName = "_drctr_domfis", ColumnDescription = "codigo postal del domicilio fiscal del receptor del comprobante", Length = 5, IsNullable = false)]
        public string DomicilioFiscal {
            get { return this.domicilioFiscalReceptor; }
            set {
                this.domicilioFiscalReceptor = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_usr_n")]
        [SugarColumn(ColumnName = "_drctr_usr_n", IsNullable = false)]
        public new string Creo
        {
            get
            {
                return base.Creo;
            }
            set
            {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_usr_m")]
        [SugarColumn(ColumnName = "_drctr_usr_m", IsNullable = false)]
        public new string Modifica
        {
            get
            {
                return base.Modifica;
            }
            set
            {
                base.Modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_fn")]
        [SugarColumn(ColumnName = "_drctr_fn", ColumnDescription = "fecha de creacion", IsNullable = false)]
        public new DateTime FechaNuevo
        {
            get
            {
                return base.FechaNuevo;
            }
            set
            {
                base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_fm")]
        [SugarColumn(ColumnName = "_drctr_fm", ColumnDescription = "fecha ultima de modificacion", IsNullable = false)]
        public new DateTime? FechaModifica
        {
            get
            {
                return base.FechaModifica;
            }
            set
            {
                base.FechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}