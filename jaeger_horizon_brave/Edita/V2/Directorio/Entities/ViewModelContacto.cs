﻿using System;
using System.Linq;
using Jaeger.Edita.V2.Directorio.Interfaces;
using SqlSugar;

namespace Jaeger.Edita.V2.Directorio.Entities
{
    [SugarTable("_dcntc")]
    public class ViewModelContacto : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation, IContacto
    {
        private long indiceField;
        private long subIndiceField;
        private bool isActiveField;
        private string strName = string.Empty;
        private string strSuggestedDeal = string.Empty;
        private string strJob = string.Empty;
        private string strDetails = string.Empty;
        private string strTypePhone = string.Empty;
        private string strTypeEmail = string.Empty;
        private string strPhone = string.Empty;
        private string strEmail = string.Empty;
        private DateTime dateCreateNew;
        private DateTime? dateChangeDate;
        private string creoField;
        private string modificaField;
        private string strDateSpecial = string.Empty;

        public ViewModelContacto()
        {
            this.IsActive = true;
        }

        [SugarColumn(ColumnName = "_dcntc_id")]
        public long Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_dcntc_a")]
        public bool IsActive
        {
            get
            {
                return this.isActiveField;
            }
            set
            {
                this.isActiveField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_dcntc_drctr_id")]
        public long SubId
        {
            get
            {
                return this.subIndiceField;
            }
            set
            {
                if (this.subIndiceField != value)
                {
                    this.subIndiceField = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// nombre del contacto
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_nom")]
        public string Nombre
        {
            get
            {
                return this.strName;
            }
            set
            {
                this.strName = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// trato sugerido
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_sug")]
        public string TratoSugerido
        {
            get
            {
                return this.strSuggestedDeal;
            }
            set
            {
                this.strSuggestedDeal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// puesto
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_pst")]
        public string Puesto
        {
            get
            {
                return this.strJob;
            }
            set
            {
                this.strJob = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// detalles
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_dtlls")]
        public string Detalles
        {
            get
            {
                return this.strDetails;
            }
            set
            {
                this.strDetails = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de telefono
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_ttlfn")]
        public string TelefonoTipo
        {
            get
            {
                return this.strTypePhone;
            }
            set
            {
                this.strTypePhone = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de correo electronico
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_tmail")]
        public string CorreoTipo
        {
            get
            {
                return this.strTypeEmail;
            }
            set
            {
                this.strTypeEmail = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// telefono
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_tlfn")]
        public string Telefono
        {
            get
            {
                return this.strPhone;
            }
            set
            {
                this.strPhone = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// correo electronico
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_mail")]
        public string Correo
        {
            get
            {
                return this.strEmail;
            }
            set
            {
                this.strEmail = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha especial
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string FechaEspecial
        {
            get
            {
                return this.strDateSpecial;
            }
            set
            {
                this.strDateSpecial = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_dcntc_usr_n")]
        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_dcntc_usr_m")]
        public string Modifica
        {
            get
            {
                return this.modificaField;
            }
            set
            {
                this.modificaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_dcntc_fn")]
        public DateTime FechaNuevo
        {
            get
            {
                return this.dateCreateNew;
            }
            set
            {
                this.dateCreateNew = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_dcntc_fm")]
        public DateTime? FechaMod
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.dateChangeDate >= firstGoodDate)
                    return this.dateChangeDate.Value;
                return null;
            }
            set
            {
                this.dateChangeDate = value;
                this.OnPropertyChanged();
            }
        }
    }
}
