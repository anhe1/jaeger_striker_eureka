﻿using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.Directorio.Entities
{
    public class ContribuyenteProveedor : Contribuyente
    {
        [DataNames("_drctr_id")]
        [SugarColumn(ColumnName = "_drctr_id", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public new int Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_a")]
        [SugarColumn(ColumnName = "_drctr_a", ColumnDescription = "registro activo=1 registro inactivo=2", IsNullable = false, Length = 1)]
        public new bool IsActive
        {
            get
            {
                return base.IsActive;
            }
            set
            {
                base.IsActive = value;
                this.OnPropertyChanged();
            }

        }

        [DataNames("_drctr_clv")]
        [SugarColumn(ColumnName = "_drctr_clv", IsNullable = false)]
        public new string Clave
        {
            get
            {
                return base.Clave;
            }
            set
            {
                base.Clave = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_rfc")]
        [SugarColumn(ColumnName = "_drctr_rfc", IsNullable = false)]
        public new string RFC
        {
            get
            {
                return base.RFC;
            }
            set
            {
                base.RFC = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_nom")]
        [SugarColumn(ColumnName = "_drctr_nom", IsNullable = false)]
        public new string Nombre
        {
            get
            {
                return base.Nombre;
            }
            set
            {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_mail")]
        [SugarColumn(ColumnName = "_drctr_mail", IsNullable = false)]
        public new string Correo
        {
            get
            {
                return base.Correo;
            }
            set
            {
                base.Correo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_tel")]
        [SugarColumn(ColumnName = "_drctr_tel", IsNullable = false)]
        public new string Telefono
        {
            get
            {
                return base.Telefono;
            }
            set
            {
                base.Telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:relacion comercial con la empresa (27=Cliente,163=Proveedor)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_rlcn")]
        [SugarColumn(ColumnName = "_drctr_rlcn", IsNullable = false)]
        public new string Relacion
        {
            get
            {
                return base.Relacion;
            }
            set
            {
                base.Relacion = value;
                this.OnPropertyChanged();
            }
        }

        private int diasEntregaField;
        [DataNames("_drctr_dsp")]
        [SugarColumn(ColumnName = "_drctr_dsp", IsNullable = true)]
        public int DiasEntrega
        {
            get
            {
                return this.diasEntregaField;
            }
            set
            {
                this.diasEntregaField = value;
                this.OnPropertyChanged();
            }
        }

        private int diasCreditoField;
        [SugarColumn(IsIgnore = true)]
        public int DiasCredito
        {
            get
            {
                return this.diasCreditoField;
            }
            set
            {
                this.diasCreditoField = value;
                this.OnPropertyChanged();
            }
        }

        private decimal limiteCreditoField;
        [DataNames("_drctr_crd")]
        [SugarColumn(ColumnName = "_drctr_crd", IsNullable = false)]
        public decimal Credito
        {
            get
            {
                return this.limiteCreditoField;
            }
            set
            {
                this.limiteCreditoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
