﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Jaeger.Enums;

namespace Jaeger.Edita.V2.Directorio.Entities
{
    public class Email : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private EnumEmailStatus emailStatusField;
        private string emisorField;
        private string receptorField;
        private string asuntoField;
        private DateTime fechaRecepcionField;
        private string contenidoField;
        private List<EmailAttachment> adjuntosField;

        public Email()
        {
            this.emisorField = string.Empty;
            this.receptorField = string.Empty;
            this.asuntoField = string.Empty;
            this.fechaRecepcionField = DateTime.Now;
            this.contenidoField = string.Empty;
            this.adjuntosField = new List<EmailAttachment>();
        }

        public Email(string sender, string recipient, string subject, DateTime received)
        {
            this.emisorField = string.Empty;
            this.receptorField = string.Empty;
            this.asuntoField = string.Empty;
            this.fechaRecepcionField = DateTime.Now;
            this.contenidoField = string.Empty;
            this.adjuntosField = new List<EmailAttachment>();
            this.Emisor = sender;
            this.Receptor = recipient;
            this.Asunto = subject;
            this.Fecha = received;
        }

        [XmlElement]
        public List<EmailAttachment> Adjuntos
        {
            get
            {
                return this.adjuntosField;
            }
            set
            {
                this.adjuntosField = value;
                this.OnPropertyChanged(null);
            }
        }

        [XmlAttribute]
        public string Contenido
        {
            get
            {
                return this.contenidoField;
            }
            set
            {
                this.contenidoField = value;
                this.OnPropertyChanged(null);
            }
        }

        [XmlAttribute]
        public DateTime Fecha
        {
            get
            {
                return this.fechaRecepcionField;
            }
            set
            {
                this.fechaRecepcionField = value;
                this.OnPropertyChanged(null);
            }
        }

        [XmlAttribute]
        public string Receptor
        {
            get
            {
                return this.receptorField;
            }
            set
            {
                this.receptorField = value;
                this.OnPropertyChanged(null);
            }
        }

        [XmlAttribute]
        public string Emisor
        {
            get
            {
                return this.emisorField;
            }
            set
            {
                this.emisorField = value;
                this.OnPropertyChanged(null);
            }
        }

        [XmlAttribute]
        public EnumEmailStatus Status
        {
            get
            {
                return this.emailStatusField;
            }
            set
            {
                if (this.emailStatusField != value)
                {
                    this.emailStatusField = value;
                    this.OnPropertyChanged(null);
                }
            }
        }

        [XmlAttribute]
        public string Asunto
        {
            get
            {
                return this.asuntoField;
            }
            set
            {
                this.asuntoField = value;
                this.OnPropertyChanged(null);
            }
        }
    }
}

namespace Jaeger.Edita.V2.Directorio.Entities
{
    public partial class EmailAttachment : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private int privateId;
        private bool privateSelected;
        private string privateNameFile;
        private string privateContentType;
        private string privateContentB64;

        public EmailAttachment()
        {
            this.privateSelected = true;
            this.privateNameFile = string.Empty;
            this.privateContentType = "application/octet-stream";
            this.privateContentB64 = string.Empty;
        }

        [XmlAttribute]
        public string ContentB64
        {
            get
            {
                return this.privateContentB64;
            }
            set
            {
                this.privateContentB64 = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string ContentType
        {
            get
            {
                return this.privateContentType;
            }
            set
            {
                this.privateContentType = value;
                this.OnPropertyChanged();
            }
        }

        [XmlIgnore]
        public int Id
        {
            get
            {
                return this.privateId;
            }
            set
            {
                this.privateId = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string NombreArchivo
        {
            get
            {
                return this.privateNameFile;
            }
            set
            {
                this.privateNameFile = value;
                this.OnPropertyChanged();
            }
        }

        [XmlIgnore]
        public bool Seleccionado
        {
            get
            {
                return this.privateSelected;
            }
            set
            {
                this.privateSelected = value;
                this.OnPropertyChanged();
            }
        }
    }
}