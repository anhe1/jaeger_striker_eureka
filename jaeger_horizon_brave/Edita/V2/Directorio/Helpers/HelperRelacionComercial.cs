﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Edita.V2.Directorio.Enums;

namespace Jaeger.Edita.V2.Directorio.Helpers
{
    public class HelperRelacionComercial
    {
        private List<RelacionComercial> privateList;

        private string privateSearch;

        public HelperRelacionComercial()
        {
            this.privateList = new List<RelacionComercial>();
            this.privateSearch = string.Empty;
            string[] names = Enum.GetNames(typeof(EnumRelationType));
            for (int i = 0; i < checked(names.Length); i = checked(i + 1))
            {
                string objeto = names[i];
                List<RelacionComercial> relacionComercials = this.privateList;
                RelacionComercial relacionComercial = new RelacionComercial()
                {
                    Id = checked(this.privateList.Count + 1),
                    Selected = false,
                    Name = objeto
                };
                relacionComercials.Add(relacionComercial);
            }
        }

        public List<RelacionComercial> Items
        {
            get
            {
                return this.privateList;
            }
            set
            {
                this.privateList = value;
            }
        }

        private bool FindID(RelacionComercial bk)
        {
            bool findID;
            findID = bk.Name == this.privateSearch;
            return findID;
        }

        public string GetRelacion()
        {
            IEnumerable<RelacionComercial> result =
                from x in this.privateList
                where x.Selected
                select x;
            List<string> objetos = (
                from o in result
                select o.Name).ToList<string>();
            return string.Join(",", objetos);
        }

        public void SetRelacion(string objeto)
        {
            if (objeto != null)
            {
                string[] strArrays = objeto.Split(new char[] { ',' });
                for (int i = 0; i < checked(strArrays.Length); i = checked(i + 1))
                {
                    this.privateSearch = strArrays[i];
                    HelperRelacionComercial helperRelacionComercial = this;
                    this.privateList.Find(new Predicate<RelacionComercial>(helperRelacionComercial.FindID)).Selected = true;
                }
            }
        }
    }
}