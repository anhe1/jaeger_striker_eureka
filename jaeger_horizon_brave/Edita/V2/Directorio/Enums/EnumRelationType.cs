﻿using System;

namespace Jaeger.Edita.V2.Directorio.Enums
{
    public enum EnumRelationType
    {
        Cliente,
        Proveedor,
        Empleado,
        Vendedor,
        Usuario,
        Solicitante,
        Minorista,
        Mayorista
    }
}