﻿using System;

namespace Jaeger.Edita.V2.Directorio.Enums
{
    public enum EnumDomicilioTipo
    {
        Fiscal,
        Particular,
        Entrega,
        Envio,
        Otro
    }
}