﻿using System;

namespace Jaeger.Edita.V2.Directorio.Enums
{
    public enum EnumRegimen
    {
        NoDefinido,
        Fisica,
        Moral
    }
}