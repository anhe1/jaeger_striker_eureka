﻿using System;

namespace Jaeger.Edita.V2.Directorio.Enums
{
    public enum EnumGenero
    {
        Ninguno,
        Masculino,
        Femenino
    }
}