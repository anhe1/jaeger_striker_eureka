using System;
using Jaeger.Edita.V2.Directorio.Enums;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.Directorio.Interfaces
{
    public interface IDomicilio
    {
        DateTime FechaNuevo { get; set; }

        bool IsActive { get; set; }

        string Modifica { get; set; }

        string Creo { get; set; }

        int SubId { get; set; }

        int Id { get; set; }

        EnumDomicilioTipo Tipo { get; set; }

        string TipoText { get; set; }

        string Calle { get; set; }

        string NoExterior { get; set; }

        string NoInterior { get; set; }

        string Colonia { get; set; }

        string Localidad { get; set; }

        string Referencia { get; set; }

        string Municipio { get; set; }

        string Estado { get; set; }

        string Ciudad { get; set; }

        string Pais { get; set; }

        string Asentamiento { get; set; }

        string CodigoPostal { get; set; }

        string CodigoDePais { get; set; }

        string Telefono { get; set; }

        string Notas { get; set; }

        string Json(Formatting objFormat = 0);

        string ToString();
    }
}