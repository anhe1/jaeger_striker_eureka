using System;

namespace Jaeger.Edita.V2.Directorio.Interfaces
{
    public interface IPersona
    {
        string CURP { get; set; }

        string RFC { get; set; }

        string Nombre { get; set; }

        string PrimerApellido { get; set; }

        string SegundoApellido { get; set; }

        string Telefono { get; set; }

        string Correo { get; set; }

        byte[] Avatar { get; set; }
    }
}