using System;

namespace Jaeger.Edita.V2.Directorio.Interfaces
{
    public interface IDomicilioFiscal
    {
        string Calle { get; set; }

        string Ciudad { get; set; }

        string CodigoDePais { get; set; }

        string CodigoPostal { get; set; }

        string Colonia { get; set; }

        string Delegacion { get; set; }

        string Estado { get; set; }

        string Localidad { get; set; }

        string NoExterior { get; set; }

        string NoInterior { get; set; }

        string Pais { get; set; }

        string Referencia { get; set; }

        string ToString();
    }
}