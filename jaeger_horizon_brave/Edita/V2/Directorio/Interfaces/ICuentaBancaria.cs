using System;

namespace Jaeger.Edita.V2.Directorio.Interfaces
{
    public interface ICuentaBancaria
    {
        long Id { get; set; }

        long SubId { get; set; }

        bool IsActive { get; set; }

        /// <summary>
        /// alias de la cuenta
        /// </summary>
        string Alias { get; set; }

        /// <summary>
        /// nombre del banco
        /// </summary>
        string Banco { get; set; }

        /// <summary>
        /// nombre del beneficiario de la cuenta
        /// </summary>
        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer apellido paterno
        /// </summary>
        string PrimerApellido { get; set; }

        /// <summary>
        /// obtener o establecer apellido materno
        /// </summary>
        string SegundoApellido { get; set; }

        /// <summary>
        /// cargo maximo que se puede hacer a la cuenta
        /// </summary>
        double CargoMaximo { get; set; }

        /// <summary>
        /// cuenta CLABE
        /// </summary>
        string Clabe { get; set; }

        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// nombre corto de la institución bancaria
        /// </summary>
        string InsitucionBancaria { get; set; }

        /// <summary>
        /// clave de la moneda
        /// </summary>
        string Moneda { get; set; }

        /// <summary>
        /// numero de cuenta bancaria
        /// </summary>
        string NumeroDeCuenta { get; set; }

        /// <summary>
        /// numero de la sucursal
        /// </summary>
        string Sucursal { get; set; }

        /// <summary>
        /// RFC del beneficiario
        /// </summary>
        string Rfc { get; set; }

        string TipoCuenta { get; set; }

        /// <summary>
        /// referencia numerica constante utilizada por el beneficiario de la cuenta
        /// </summary>
        string RefNumerica { get; set; }

        /// <summary>
        /// referencia alfanumerica constante utilizada por el beneficiario de la cuenta
        /// </summary>
        string RefAlfanumerica { get; set; }

        /// <summary>
        /// obtener o establecer si la cuenta ya se encuentra verificada
        /// </summary>
        bool Verificado { get; set; }

        DateTime FechaNuevo { get; set; }

        DateTime? FechaMod { get; set; }

        string Creo { get; set; }

        string Modifica { get; set; }

        string Error { get; }

        string this[string columnName] { get; }
    }
}