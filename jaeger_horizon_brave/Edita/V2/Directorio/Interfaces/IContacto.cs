/// develop:
/// purpose: interface para definir los conceptos de un contacto relacionado al un registro del directorio
using System;

namespace Jaeger.Edita.V2.Directorio.Interfaces
{
    public interface IContacto
    {
        long Id { get; set; }

        bool IsActive { get; set; }

        long SubId { get; set; }

        /// <summary>
        /// nombre del contacto
        /// </summary>
        string Nombre { get; set; }

        /// <summary>
        /// trato sugerido
        /// </summary>
        string TratoSugerido { get; set; }

        /// <summary>
        /// puesto
        /// </summary>
        string Puesto { get; set; }

        /// <summary>
        /// detalles
        /// </summary>
        string Detalles { get; set; }

        /// <summary>
        /// tipo de telefono
        /// </summary>
        string TelefonoTipo { get; set; }

        /// <summary>
        /// tipo de correo electronico
        /// </summary>
        string CorreoTipo { get; set; }

        /// <summary>
        /// telefono
        /// </summary>
        string Telefono { get; set; }

        /// <summary>
        /// correo electronico
        /// </summary>
        string Correo { get; set; }

        /// <summary>
        /// fecha especial
        /// </summary>
        string FechaEspecial { get; set; }

        string Creo { get; set; }

        string Modifica { get; set; }

        DateTime FechaNuevo { get; set; }

        DateTime? FechaMod { get; set; }
    }
}