using System;
using System.ComponentModel;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Edita.V2.Directorio.Enums;

namespace Jaeger.Edita.V2.Directorio.Interfaces
{
    public interface IPersona2
    {
        /// <summary>
        /// indice del directorio
        /// </summary>
        int Id { get; set; }

        bool IsActive { get; set; }

        int SubId { get; set; }

        /// <summary>
        /// regimen fiscal (fisica o moral)
        /// </summary>
        EnumRegimen Regimen { get; set; }

        string RegimenText { get; set; }

        /// <summary>
        /// clave de control interno
        /// </summary>
        string Clave { get; set; }

        string CURP { get; set; }

        string RFC { get; set; }

        string Nombre { get; set; }

        string Telefono { get; set; }

        string Correo { get; set; }

        string SitioWEB { get; set; }

        string Relacion { get; set; }

        string Nota { get; set; }

        BindingList<ViewModelDomicilio> Domicilios { get; set; }

        BindingList<ViewModelCuentaBancaria> CuentasBancarias { get; set; }

        BindingList<ViewModelContacto> Contactos { get; set; }

        byte[] Avatar { get; set; }

        string Creo { get; set; }

        string Modifica { get; set; }

        DateTime FechaNuevo { get; set; }

        DateTime? FechaMod { get; set; }
    }
}