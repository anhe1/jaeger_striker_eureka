namespace Jaeger.Edita.V2.Directorio.Interfaces
{
    public interface IContribuyente
    {
        int Id { get; set; }

        string RegimenFiscal { get; set; }

        string NumRegIdTrib { get; set; }

        string ResidenciaFiscal { get; set; }

        string ClaveUsoCFDI { get; set; }

        int DiasEntrega { get; set; }

        int DiasCredito { get; set; }

        decimal Credito { get; set; }

        decimal FactorDescuento { get; set; }

        int IndexDescuento { get; set; }

        /// <summary>
        /// obtener o establecer Factor del IVA pactado
        /// </summary>
        decimal FactorIvaPactado { get; set; }

        string RegistroPatronal { get; set; }
    }
}