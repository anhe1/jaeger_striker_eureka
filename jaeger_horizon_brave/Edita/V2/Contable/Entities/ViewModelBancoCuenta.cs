using Jaeger.Catalogos.Entities;
using Newtonsoft.Json;
using SqlSugar;
using System;
using System.ComponentModel;

namespace Jaeger.Edita.V2.Contable.Entities
{
    [SugarTable("_cntbl3b", "informacion de cuentas bancarias propias")]
    public class ViewModelBancoCuenta : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        #region declaraciones
        private int indiceField;
        private bool isActiveField;
        private int diaCorteField;
        private decimal cargoMaximoField;
        private DateTime fechaNuevoField;
        private DateTime? fechaModificaField;
        private string creoField;
        private string modificaField;
        private string claveField;
        private string monedaField;
        private string sucursalField;
        private string numeroDeCuentaField;
        private string clabeField;
        private string insitucionBancariaNCField;
        private string aliasField;
        private string bancoField;
        private string beneficiarioField;
        private string rfcField;
        private string numClienteField;
        private BindingList<ClaveFormaPago> itemsField;

        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ViewModelBancoCuenta()
        {

            this.IsActive = true;
            this.FechaNuevo = DateTime.Now;
            this.Moneda = "MXN";
            this.itemsField = new BindingList<ClaveFormaPago>()
            {
                RaiseListChangedEvents = true
            };
        }

        #region propiedades

        [SugarColumn(ColumnName = "_cntbl3b_id", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "_cntbl3b_a", ColumnDescription = "registro activo", Length = 1, IsNullable = false)]
        public bool IsActive
        {
            get
            {
                return this.isActiveField;
            }
            set
            {
                this.isActiveField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true, ColumnDataType = "SMALLINT", ColumnName = "_cntbl3b_dcrt", ColumnDescription = "dia del corte", Length = 2, IsNullable = false)]
        public int DiaCorte
        {
            get
            {
                return this.diaCorteField;
            }
            set
            {
                this.diaCorteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cargo maximo que se puede hacer a la cuenta
        /// </summary>
        [JsonProperty("max")]
        [SugarColumn(ColumnName = "_cntbl3b_cmax", ColumnDescription = "cargo maximo que se puede hacer a la cuenta", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal CargoMaximo
        {
            get
            {
                return this.cargoMaximoField;
            }
            set
            {
                this.cargoMaximoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_cntbl3b_fn", ColumnDescription = "fecha de creacion del registro", IsNullable = false)]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_cntbl3b_fm", ColumnDescription = "fecha de la ultima modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModificaField >= firstGoodDate)
                    return this.fechaModificaField;
                return null;
            }
            set
            {
                this.fechaModificaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        [JsonProperty("clave")]
        [SugarColumn(ColumnName = "_cntbl3b_clvb", ColumnDescription = "clave del banco segun catalogo del SAT", Length = 3, IsNullable = true)]
        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// clave de moneda
        /// </summary>
        [JsonProperty("moneda")]
        [SugarColumn(ColumnName = "_cntbl3b_clv", ColumnDescription = "clave de la moneda a utilizar segun catalgo SAT", Length = 3, IsNullable = true)]
        public string Moneda
        {
            get
            {
                return this.monedaField;
            }
            set
            {
                this.monedaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_cntbl3b_usr_n", ColumnDescription = "clave del usuario que creo el registro", Length = 10, IsNullable = false)]
        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_cntbl3b_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica
        {
            get
            {
                return this.modificaField;
            }
            set
            {
                this.modificaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// registro federal de causantes
        /// </summary>
        [JsonProperty("rfc")]
        [SugarColumn(ColumnName = "_cntbl3b_rfc", ColumnDescription = "rfc del beneficiario de la cuenta de banco", Length = 14, IsNullable = true)]
        public string RFC
        {
            get
            {
                return this.rfcField;
            }
            set
            {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("numCliente")]
        [SugarColumn(ColumnName = "_cntbl3b_numcli", ColumnDescription = "numero de identificación del cliente, numero de contrato", Length = 14, IsNullable = true)]
        public string NumCliente
        {
            get
            {
                return this.numClienteField;
            }
            set
            {
                this.numClienteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cuenta CLABE
        /// </summary>
        [JsonProperty("clabe")]
        [SugarColumn(ColumnName = "_cntbl3b_clabe", ColumnDescription = "cuenta clabe", Length = 18, IsNullable = true)]
        public string Clabe
        {
            get
            {
                return this.clabeField;
            }
            set
            {
                this.clabeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// alias de referencia para la cuenta bancaria
        /// </summary>
        [JsonProperty("alias")]
        [SugarColumn(ColumnName = "_cntbl3b_alias", ColumnDescription = "alias de la cuenta", Length = 20, IsNullable = true)]
        public string Alias
        {
            get
            {
                return this.aliasField;
            }
            set
            {
                this.aliasField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de cuenta bancaria
        /// </summary>
        [JsonProperty("numCta")]
        [SugarColumn(ColumnName = "_cntbl3b_numcta", ColumnDescription = "numero de cuenta", Length = 20, IsNullable = true)]
        public string NumeroDeCuenta
        {
            get
            {
                return this.numeroDeCuentaField;
            }
            set
            {
                this.numeroDeCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de la sucursal
        /// </summary>
        [JsonProperty("sucursal")]
        [SugarColumn(ColumnName = "_cntbl3b_scrsl", ColumnDescription = "sucursal bancaria", Length = 20, IsNullable = true)]
        public string Sucursal
        {
            get
            {
                return this.sucursalField;
            }
            set
            {
                this.sucursalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del banco
        /// </summary>
        [JsonProperty("banco")]
        [SugarColumn(ColumnName = "_cntbl3b_banco", ColumnDescription = "nombre del banco", Length = 64, IsNullable = true)]
        public string Banco
        {
            get
            {
                return this.bancoField;
            }
            set
            {
                this.bancoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del beneficiario de la cuenta
        /// </summary>
        [JsonProperty("benef")]
        [SugarColumn(ColumnName = "_cntbl3b_benef", ColumnDescription = "nombre del beneficiario de la cuenta de banco", Length = 256, IsNullable = true)]
        public string Beneficiario
        {
            get
            {
                return this.beneficiarioField;
            }
            set
            {
                this.beneficiarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre corto de la institución bancaria
        /// </summary>
        [JsonProperty("descr")]
        [SugarColumn(ColumnName = "_cntbl3b_nom", ColumnDescription = "nombre de la institucion bancaria", Length = 256, IsNullable = true)]
        public string InsitucionBancaria
        {
            get
            {
                return this.insitucionBancariaNCField;
            }
            set
            {
                this.insitucionBancariaNCField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("items")]
        [SugarColumn(IsIgnore = true)]
        public BindingList<ClaveFormaPago> Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        public string Json(Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static ViewModelBancoCuenta Json(string inputJson)
        {
            try
            {
                return JsonConvert.DeserializeObject<ViewModelBancoCuenta>(inputJson);
            }
            catch (JsonException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
    }
}