﻿using System;
using System.Linq;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.Contable.Entities
{
    public class PrePolizaFormaPago
    {
        private string clave;
        private string descripcion;

        [JsonProperty("clave")]
        public string Clave
        {
            get
            {
                return this.clave;
            }
            set
            {
                this.clave = value;
            }
        }

        [JsonProperty("descrip")]
        public string Descripcion
        {
            get
            {
                return this.descripcion;
            }
            set
            {
                this.descripcion = value;
            }
        }
    }
}
