using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.Contable.Entities
{
    [JsonObject]
    public class PrePolizaRelacionados : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string tipoRelacionField;
        BindingList<PrePolizaRelacionado> prepolizasField;

        public PrePolizaRelacionados()
        {
            this.prepolizasField = new BindingList<PrePolizaRelacionado>() { RaiseListChangedEvents = true };
            this.prepolizasField.AddingNew += PrePolizasField_AddingNew;
            this.prepolizasField.ListChanged += PrePolizasField_ListChanged;
        }
        
        [JsonProperty("tipoRelacion")]
        public string TipoRelacion
        {
            get
            {
                return this.tipoRelacionField;
            }
            set
            {
                this.tipoRelacionField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("prePolizas")]
        public BindingList<PrePolizaRelacionado> PrePolizas
        {
            get
            {
                return this.prepolizasField;
            }
            set
            {
                if (this.prepolizasField != null)
                {
                    this.prepolizasField.AddingNew -= PrePolizasField_AddingNew;
                    this.prepolizasField.ListChanged -= PrePolizasField_ListChanged;
                }
                this.prepolizasField = value;
                if (this.prepolizasField != null)
                {
                    this.prepolizasField.AddingNew += PrePolizasField_AddingNew;
                    this.prepolizasField.ListChanged += PrePolizasField_ListChanged;
                }
                this.OnPropertyChanged();
            }
        }

        private void PrePolizasField_AddingNew(object sender, AddingNewEventArgs e)
        {
        }

        private void PrePolizasField_ListChanged(object sender, ListChangedEventArgs e)
        {
        }
    }
}

namespace Jaeger.Edita.V2.Contable.Entities
{
    [JsonObject]
    public partial class PrePolizaRelacionado : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string noIndetField;
        private DateTime fechaEmisionField;
        private PrePolizaCuenta emisorField;
        private PrePolizaCuenta receptorField;
        private PrePolizaFormaPago formaDePagoField;
        private decimal cargoField;
        private decimal abonoField;

        public string NoIndet
        {
            get
            {
                return this.noIndetField;
            }
            set
            {
                this.noIndetField = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime FechaEmision
        {
            get
            {
                return this.fechaEmisionField;
            }
            set
            {
                this.fechaEmisionField = value;
                this.OnPropertyChanged();
            }
        }

        public PrePolizaCuenta Emisor
        {
            get
            {
                return this.emisorField;
            }
            set
            {
                this.emisorField = value;
                this.OnPropertyChanged();
            }
        }

        public PrePolizaCuenta Receptor
        {
            get
            {
                return this.receptorField;
            }
            set
            {
                this.receptorField = value;
                this.OnPropertyChanged();
            }
        }

        public PrePolizaFormaPago FormaDePago
        {
            get
            {
                return this.formaDePagoField;
            }
            set
            {
                this.formaDePagoField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Cargo
        {
            get
            {
                return this.cargoField;
            }
            set
            {
                this.cargoField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Abono
        {
            get
            {
                return this.abonoField;
            }
            set
            {
                this.abonoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}