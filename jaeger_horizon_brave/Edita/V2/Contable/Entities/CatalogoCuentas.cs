﻿using System.ComponentModel;
using Jaeger.Contable.Entities;

namespace Jaeger.Edita.V2.Contable.Entities
{
    /// <summary>
    /// Estándar de catálogo de cuentas que se entrega como parte de la contabilidad electrónica.
    /// </summary>
    public class CatalogoCuentas : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private BindingList<CatalogoCuenta> ctasField;
        private string versionField;
        private string rFCField;
        private string mesField; 
        private int anioField;
        private string selloField;
        private string noCertificadoField;
        private string certificadoField;

        public CatalogoCuentas()
        {
            this.versionField = "1.3";
        }
    
        /// <summary>
        /// Nodo obligatorio para expresar el detalle de cada cuenta y subcuenta del catálogo.
        /// </summary>
        public BindingList<CatalogoCuenta> Cuentas
        {
            get
            {
                return this.ctasField;
            }
            set
            {
                this.ctasField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <summary>
        /// Atributo requerido para expresar la versión del formato
        /// </summary>
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <summary>
        /// Atributo requerido para expresar el RFC del contribuyente que envía los datos
        /// </summary>
        public string RFC
        {
            get
            {
                return this.rFCField;
            }
            set
            {
                this.rFCField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <summary>
        /// Atributo requerido para expresar el mes en que inicia la vigencia del catálogo para la balanza
        /// </summary>
        public string Mes
        {
            get
            {
                return this.mesField;
            }
            set
            {
                this.mesField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <summary>
        /// Atributo requerido para expresar el año en que inicia la vigencia del catálogo para la balanza
        /// </summary>
        public int Anio
        {
            get
            {
                return this.anioField;
            }
            set
            {
                this.anioField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <summary>
        /// Atributo opcional para contener el sello digital del archivo de contabilidad electrónica. El sello deberá ser expresado cómo una cadena de texto en formato Base 64
        /// </summary>
        public string Sello
        {
            get
            {
                return this.selloField;
            }
            set
            {
                this.selloField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <summary>
        /// Atributo opcional para expresar el número de serie del certificado de sello digital que ampara el archivo de contabilidad electrónica, de acuerdo al acuse correspondiente a 20 posiciones otorgado por el sistema del SAT.
        /// </summary>
        public string NoCertificado
        {
            get
            {
                return this.noCertificadoField;
            }
            set
            {
                this.noCertificadoField = value;
                this.OnPropertyChanged();
            }
        }
    
        /// <summary>
        /// Atributo opcional que sirve para expresar el certificado de sello digital que ampara al archivo de contabilidad electrónica como texto, en formato base 64.
        /// </summary>
        public string Certificado
        {
            get
            {
                return this.certificadoField;
            }
            set
            {
                this.certificadoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}

namespace Jaeger.Contable.Entities
{
    public partial class CatalogoCuenta : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string codAgrupField;
        private string numCtaField;
        private string descField;
        private string subCtaDeField;
        private int nivelField;
        private string naturField;

        /// <summary>
        /// Atributo requerido para expresar el código asociador de cuentas y subcuentas conforme al catálogo publicado en la página de internet del SAT. Se debe asociar cada cuenta y subcuenta que sea más apropiado de acuerdo con la naturaleza y preponderancia de la cuenta o subcuenta.
        /// </summary>
        public string CodAgrupador
        {
            get
            {
                return this.codAgrupField;
            }
            set
            {
                this.codAgrupField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido, es la clave con que se distingue la cuenta o subcuenta en la contabilidad
        /// </summary>
        public string NumCuenta
        {
            get
            {
                return this.numCtaField;
            }
            set
            {
                this.numCtaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el nombre de la cuenta o subcuenta
        /// </summary>
        public string Descripcion
        {
            get
            {
                return this.descField;
            }
            set
            {
                this.descField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional en el caso de subcuentas. Sirve para expresar la clave de la cuenta a la que pertenece dicha subcuenta. Se convierte en requerido cuando se cuente con la información.
        /// </summary>
        public string SubCuentaDe
        {
            get
            {
                return this.subCtaDeField;
            }
            set
            {
                this.subCtaDeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el nivel en el que se encuentra la cuenta o subcuenta en el catálogo.
        /// </summary>
        public int Nivel
        {
            get
            {
                return this.nivelField;
            }
            set
            {
                this.nivelField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la naturaleza de la cuenta o subcuenta. (D - Deudora, A - Acreedora). ( IsActive = D ) ( Pasivo = A ) ( Capital = A ) ( Ingreso = A ) ( Costo = D ) ( Gasto = D ) ( Resultado Integral de Financiamiento = D y/o A ) ( Cuentas de orden = D y/o A ). Existen cuentas de IsActive, Pasivo y Capital que por su naturaleza pueden presentarse de manera Deudora o Acreedora.
        /// </summary>
        public string Naturaleza
        {
            get
            {
                return this.naturField;
            }
            set
            {
                this.naturField = value;
                this.OnPropertyChanged();
            }
        }
    }
}