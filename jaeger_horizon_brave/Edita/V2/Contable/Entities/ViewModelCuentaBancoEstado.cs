﻿using System;
using System.Linq;
using System.ComponentModel;

namespace Jaeger.Edita.V2.Contable.Entities
{
    public partial class BanamexEstadoCuentaMovimiento : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string noAutorizacionField;
        private DateTime fechaField;
        private string tipoField;
        private string field2;
        private string descripcionField;
        private decimal depositoField;
        private decimal retiroField;

        public BanamexEstadoCuentaMovimiento()
        {

        }

        public DateTime Fecha
        {
            get
            {
                return this.fechaField;
            }
            set
            {
                this.fechaField = value;
                this.OnPropertyChanged();
            }
        }

        public string Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Field2
        {
            get
            {
                return this.field2;
            }
            set
            {
                this.field2 = value;
                this.OnPropertyChanged();
            }
        }

        public string Descripcion
        {
            get
            {
                return this.descripcionField;
            }
            set
            {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Deposito
        {
            get
            {
                return this.depositoField;
            }
            set
            {
                this.depositoField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Retiro
        {
            get
            {
                return this.retiroField;
            }
            set
            {
                this.retiroField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estalecer el numero de autorizacion del movimiento
        /// </summary>
        public string NoAutorizacion
        {
            get
            {
                return this.noAutorizacionField;
            }
            set
            {
                this.noAutorizacionField = value.Replace("AUT.", "");
                this.OnPropertyChanged();
            }
        }
    }
}

namespace Jaeger.Edita.V2.Contable.Entities
{
    public class ViewModelCuentaBancoEstado
    {
        /// <summary>
        /// obtener o establecer la fecha del reporte
        /// </summary>
        public DateTime? Fecha;

        /// <summary>
        /// obtener o establecer el numero de cliente
        /// </summary>
        public string NoCliente;

        /// <summary>
        /// obtener o establecer el nombre del beneficiario de la cuenta
        /// </summary>
        public string RazonSocial;

        /// <summary>
        /// obtener o establecer el nombre del usuario que emite el reporte
        /// </summary>
        public string Usuario;

        /// <summary>
        /// obtener o establecer el tipo de cuenta
        /// </summary>
        public string Tipo;

        /// <summary>
        /// obtener o establecer el numero de la sucursal de la cuenta
        /// </summary>
        public string Sucursal;

        /// <summary>
        /// obtener o establecer el numero de la cuenta
        /// </summary>
        public string NoCuenta;

        /// <summary>
        /// obtener o establecer el nombre
        /// </summary>
        public string Nombre;

        /// <summary>
        /// obtener o establecer fecha 3
        /// </summary>
        public DateTime FechaPeriodo;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal SaldoAnterior;

        /// <summary>
        /// obtener o establecer saldo a la fecha del periodo 
        /// </summary>
        public decimal SaldoAl;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal SaldoPromedioPeriodo;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal SaldoPromedioAnual;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int DiasTrascurridosPeriodo;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal Despositos;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int NumDepositos;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int DiasTrascurridosAnual;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int NumRetiros;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal TotalRetiros;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int ChequesGirados;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int ChequesExentos;

        public BindingList<BanamexEstadoCuentaMovimiento> Movimientos { get; set; }
    }
}