﻿using System;
using System.Linq;
using System.ComponentModel;
using Jaeger.Edita.V2.Contable.Enums;

namespace Jaeger.Edita.V2.Contable.Entities
{
    public class BancoEstadoCuenta : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private ViewModelBancoSaldo saldoInicialField;
        private BindingList<ViewModelBancoMovimiento> movimientos;

        public BancoEstadoCuenta()
        {
            this.movimientos = new BindingList<ViewModelBancoMovimiento>();
        }

        public ViewModelBancoSaldo Saldo
        {
            get
            {
                return this.saldoInicialField;
            }
            set
            {
                this.saldoInicialField = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<ViewModelBancoMovimiento> Movimientos
        {
            get
            {
                return this.movimientos;
            }
            set
            {
                this.movimientos = value;
                this.OnPropertyChanged();
            }
        }

        public void Procesar()
        {
            decimal saldo = this.Saldo.SaldoInicial;
            for (int i = 0; i < this.Movimientos.Count; i++)
            {
                    if (this.Movimientos[i].Tipo == EnumPolizaTipo.Ingreso)
                    {
                        saldo = saldo + this.Movimientos[i].Cargo;
                    }
                    else if (this.Movimientos[i].Tipo == EnumPolizaTipo.Egreso)
                    {
                        saldo = saldo - this.Movimientos[i].Abono;
                    }
                    else if (this.movimientos[i].Tipo == EnumPolizaTipo.Diario)
                    {
                        saldo = saldo + this.Movimientos[i].Cargo;
                    }
                
                this.Movimientos[i].Saldo = saldo;
            }
        }
    }
}
