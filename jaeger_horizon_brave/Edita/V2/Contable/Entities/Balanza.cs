﻿using System;
using System.ComponentModel;
using Jaeger.Contable.Entities;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Edita.V2.Contable.Entities
{
    /// <summary>
    /// Estándar de balanza de comprobación que se entrega como parte de la contabilidad electrónica.
    /// </summary>
    public class Balanza : BasePropertyChangeImplementation
    {
        private BindingList<BalanzaCuentas> ctasField;
        private string versionField;
        private string rFCField;
        private string mesField; 
        private int anioField;
        private string tipoEnvioField;
        private DateTime? fechaModBalField;
        private string selloField;
        private string noCertificadoField;
        private string certificadoField;

        public Balanza()
        {
            this.versionField = "1.3";
        }

        /// <summary>
        /// Nodo obligatorio para expresar el detalle de cada cuenta o subcuenta de la balanza de comprobación.
        /// </summary>
        public BindingList<BalanzaCuentas> Cuentas
        {
            get
            {
                return this.ctasField;
            }
            set
            {
                this.ctasField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la versión del formato.
        /// </summary>
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el RFC del contribuyente que envía los datos
        /// </summary>
        public string RFC
        {
            get
            {
                return this.rFCField;
            }
            set
            {
                this.rFCField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el mes al que corresponde la balanza de comprobación
        /// </summary>
        public string Mes
        {
            get
            {
                return this.mesField;
            }
            set
            {
                this.mesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el año al que corresponde la balanza
        /// </summary>
        public int Anio
        {
            get
            {
                return this.anioField;
            }
            set
            {
                this.anioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el tipo de envío de la balanza (N - Normal; C - Complementaria)
        /// </summary>
        public string TipoEnvio
        {
            get
            {
                return this.tipoEnvioField;
            }
            set
            {
                this.tipoEnvioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar la fecha de la última modificación contable de la balanza de comprobación. Es requerido cuando el tipo de Envío es complementario.
        /// </summary>
        public DateTime? FechaModBal
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModBalField >= firstGoodDate)
                {
                    return this.fechaModBalField;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                this.fechaModBalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para contener el sello digital del archivo de contabilidad electrónica. El sello deberá ser expresado cómo una cadena de texto en formato Base 64
        /// </summary>
        public string Sello
        {
            get
            {
                return this.selloField;
            }
            set
            {
                this.selloField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar el número de serie del certificado de sello digital que ampara el archivo de contabilidad electrónica, de acuerdo al acuse correspondiente a 20 posiciones otorgado por el sistema del SAT.
        /// </summary>
        public string NoCertificado
        {
            get
            {
                return this.noCertificadoField;
            }
            set
            {
                this.noCertificadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional que sirve para expresar el certificado de sello digital que ampara al archivo de contabilidad electrónica como texto, en formato base 64.
        /// </summary>
        public string Certificado
        {
            get
            {
                return this.certificadoField;
            }
            set
            {
                this.certificadoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}

namespace Jaeger.Contable.Entities
{
    public partial class BalanzaCuentas : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string numCtaField;
        private decimal saldoIniField;
        private decimal debeField;
        private decimal haberField;
        private decimal saldoFinField;

        /// <summary>
        /// Atributo requerido para expresar la clave asignada con que se distingue la cuenta o subcuenta en el catálogo de cuentas del contribuyente.
        /// </summary>
        public string NumCta
        {
            get
            {
                return this.numCtaField;
            }
            set
            {
                this.numCtaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tributo requerido para expresar el monto del saldo inicial de la cuenta o subcuenta en el periodo. De acuerdo a la naturaleza de la cuenta o subcuenta, deberá de corresponder el saldo inicial, de lo contrario se entenderá que es un saldo inicial de naturaleza inversa. En caso de no existir dato, colocar cero (0)
        /// </summary>
        public decimal SaldoIni
        {
            get
            {
                return this.saldoIniField;
            }
            set
            {
                this.saldoIniField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el monto de los movimientos deudores de la cuenta o subcuenta. En caso de no existir dato, colocar cero (0)
        /// </summary>
        public decimal Debe
        {
            get
            {
                return this.debeField;
            }
            set
            {
                this.debeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el monto de los movimientos acreedores de la cuenta o subcuenta. En caso de no existir dato, colocar cero (0)
        /// </summary>
        public decimal Haber
        {
            get
            {
                return this.haberField;
            }
            set
            {
                this.haberField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el monto del saldo final de la cuenta o subcuenta en el periodo. De acuerdo a la naturaleza de la cuenta o subcuenta, deberá de corresponder el saldo final, de lo contrario se entenderá que es un saldo final de naturaleza inversa. En caso de no existir dato, colocar cero (0)
        /// </summary>
        public decimal SaldoFin
        {
            get
            {
                return this.saldoFinField;
            }
            set
            {
                this.saldoFinField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
