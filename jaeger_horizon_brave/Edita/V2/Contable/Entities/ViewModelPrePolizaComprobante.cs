﻿using System;
using System.Linq;
using Jaeger.Edita.V2.Almacen.Entities;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;
using Jaeger.Enums;
using Newtonsoft.Json;
using Jaeger.Edita.V2.CFDI.Entities;

namespace Jaeger.Edita.V2.Contable.Entities
{
    ///<summary>
    ///comprobantes
    ///</summary>
    [SugarTable("_cntbl3c")]
    public partial class ViewModelPrePolizaComprobante : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private int indiceField;
        private bool isActiveField = true;
        private int subIndiceField;
        private long idComprobanteField;
        private string numPedidoField;
        private string noIndetField;
        private decimal cargoField;
        private decimal abonoField;
        private string vendedorField;
        private EnumMetadataType tipoField;
        private string creoField;
        private string modificoField;
        private DateTime fechaNuevoField;
        private DateTime? fechaModificaField;
        private string versionField;

        public ViewModelPrePolizaComprobante()
        {
            this.FechaNuevo = DateTime.Now;
        }

        //public ViewModelPrePolizaComprobante(ViewModelComprobanteGeneral general)
        //{
        //    this.Id = 0;
        //    this.IdComprobante = general.Id;
        //    this.IsActive = general.IsActive;
        //    this.Version = general.Version;
        //    this.Folio = general.Folio;
        //    this.Serie = general.Serie;
        //    this.ReceptorRFC = general.ReceptorRFC;
        //    this.Receptor = general.Receptor;
        //    this.EmisorRFC = general.EmisorRFC;
        //    this.Emisor = general.Emisor;
        //    this.Total = general.Total;
        //    this.Descuento = general.Descuento;
        //    this.Subtotal = general.SubTotal;
        //    this.UUID = general.UUID;
        //    this.Estado = general.Estado;
        //    this.Status = general.Status;
        //    this.Acumulado = general.Acumulado;
        //    this.Descuento = general.Descuento;
        //    this.FechaPago = general.FechaPago;
        //    this.FechaEmision = general.FechaEmision;
        //    this.TipoComprobanteText = general.TipoComprobanteText;
        //    this.SubTipoComprobanteText = general.SubTipoComprobanteText;
        //    this.PrecisionDecimal = general.PrecisionDecimal;
        //    this.TrasladoIEPS = general.TrasladoIEPS;
        //    this.TrasladoIVA = general.TrasladoIVA;
        //    this.RetencionIEPS = general.RetencionIEPS;
        //    this.RetencionISR = general.RetencionISR;
        //    this.RetencionIVA = general.RetencionIVA;
        //    this.ClaveFormaPago = general.ClaveFormaPago;
        //    this.ClaveMetodoPago = general.ClaveMetodoPago;
        //    this.ClaveMoneda = general.ClaveMoneda;
        //    this.FechaTimbre = general.FechaTimbrado;
        //    this.Tipo = EnumMetadataType.CFDI;
        //    this.FechaNuevo = DateTime.Now;
        //}

        //public ViewModelPrePolizaComprobante(ViewModelRemision general)
        //{
        //    this.Id = 0;
        //    this.IdComprobante = general.Id;
        //    this.IsActive = general.IsActive;
        //    this.Version = general.Version;
        //    this.Folio = general.Folio.ToString();
        //    this.Serie = general.Serie;
        //    this.ReceptorRFC = general.ReceptorRFC;
        //    this.Receptor = general.Receptor.Nombre;
        //    this.EmisorRFC = general.EmisorRFC;
        //    this.Emisor = general.EmisorRFC;
        //    this.Total = general.Total;
        //    this.Descuento = general.Descuento;
        //    this.Subtotal = general.SubTotal;
        //    this.UUID = general.UUID;
        //    this.Estado = "Vigente";
        //    this.Status = general.Status.ToString();
        //    this.Acumulado = general.Acumulado;
        //    this.Descuento = general.Descuento;
        //    this.FechaPago = general.FechaPago;
        //    this.FechaEmision = general.FechaEmision;
        //    this.TipoComprobanteText = general.TipoText;
        //    //this.SubTipoComprobanteText = general.SubTipoComprobanteText;
        //    this.PrecisionDecimal = general.Decimales;
        //    this.TrasladoIEPS = general.TrasladoIEPS;
        //    this.TrasladoIVA = general.TrasladoIVA;
        //    //this.RetencionIEPS = general.RetencionIEPS;
        //    //this.RetencionISR = general.RetencionISR;
        //    //this.RetencionIVA = general.RetencionIVA;
        //    this.ClaveFormaPago = general.FormaPago;
        //    this.ClaveMetodoPago = general.MetodoPago;
        //    this.ClaveMoneda = "MXN";
        //    //this.FechaTimbre = general.FechaTimbrado;
        //    this.Tipo = EnumMetadataType.Remision;
        //    this.FechaNuevo = DateTime.Now;
        //}

        public ViewModelPrePolizaComprobante(ViewModelComprobanteSingle general)
        {
            this.Id = 0;
            this.IdComprobante = general.Id;
            this.IsActive = general.Activo;
            this.Version = general.Version;
            this.Folio = general.Folio;
            this.Serie = general.Serie;
            this.ReceptorRFC = general.ReceptorRFC;
            this.Receptor = general.Receptor;
            this.EmisorRFC = general.EmisorRFC;
            this.Emisor = general.Emisor;
            this.Total = general.Total;
            this.Descuento = general.Descuento;
            this.Subtotal = general.SubTotal;
            this.UUID = general.IdDocumento;
            this.Estado = general.Estado;
            this.Status = general.Status;
            this.Acumulado = general.Acumulado;
            this.Descuento = general.Descuento;
            this.FechaPago = general.FechaUltimoPago;
            this.FechaEmision = general.FechaEmision;
            this.TipoComprobanteText = general.TipoComprobanteText;
            this.SubTipoComprobanteText = general.SubTipoText;
            this.PrecisionDecimal = general.PrecisionDecimal;
            this.TrasladoIEPS = general.TrasladoIEPS;
            this.TrasladoIVA = general.TrasladoIVA;
            this.RetencionIEPS = general.RetencionIEPS;
            this.RetencionISR = general.RetencionISR;
            this.RetencionIVA = general.RetencionIVA;
            this.ClaveFormaPago = general.FormaPago;
            this.ClaveMetodoPago = general.MetodoPago;
            this.ClaveMoneda = general.Moneda;
            this.FechaTimbre = general.FechaTimbrado;
            this.Tipo = EnumMetadataType.CFDI;
            this.FechaNuevo = DateTime.Now;
        }

        public ViewModelPrePolizaComprobante(Validador.Entities.Documento documento)
        {
            this.Abono = 0;
            this.Acumulado = 0;
            this.Cargo = 0;
            this.Creo = "";
            this.Descuento = Jaeger.Helpers.DbConvert.ConvertDecimal(documento.Descuento);
            this.Emisor = documento.Emisor;
            this.EmisorClave = "";
            this.EmisorRFC = documento.EmisorRFC;
            this.Estado = documento.EstadoSAT;
            this.FechaEmision = Jaeger.Helpers.DbConvert.ConvertDateTime(documento.Fecha);
            this.FechaMod = null;
            this.FechaNuevo = DateTime.Now;
            this.FechaPago = null;
            this.Folio = documento.Folio;
            this.Id = 0;
            this.IdComprobante = documento.Id;
            this.IsActive = true;
            this.ClaveMetodoPago = documento.MetodoPago;
            this.Modifica = "";
            this.ClaveMoneda = "";
            this.PrecisionDecimal = 2;
            this.Receptor = documento.Receptor;
            this.ReceptorClave = "";
            this.ReceptorRFC = documento.ReceptorRFC;
            this.Saldo = 0;
            this.Serie = documento.Serie;
            this.Status = "";
            this.SubId = 0;
            this.Subtotal = Jaeger.Helpers.DbConvert.ConvertDecimal(documento.SubTotal);
            this.TipoComprobanteText = documento.TipoComprobante;
            this.Tipo = EnumMetadataType.CFDI;
            this.Total = Jaeger.Helpers.DbConvert.ConvertDecimal(documento.Total);
            //''this.TrasladoIVA = Jaeger.Helpers.DbConvert.ConvertDecimal(documento.tot.TotalTrasladoIva)
            this.UUID = documento.IdDocumento;
            this.Vendedor = "";
            this.Version = documento.VersionText;
            this.Cargo = this.Total;
        }

        /// <summary>
        /// Desc:indice del comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("idCom")]
        [DataNames("_cfdi_id")]
        [SugarColumn(ColumnName = "_cntbl3c_idcom", ColumnDescription = "indice del comprobante", Length = 11, IsNullable = true)]
        public long IdComprobante
        {
            get
            {
                return this.idComprobanteField;
            }
            set
            {
                this.idComprobanteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:numero de pedido
        /// Default:
        /// Nullable:True
        /// </summary>
        [JsonProperty("numPedido")]
        [DataNames("_cntbl3c_nmpdd")]
        [SugarColumn(ColumnName = "_cntbl3c_nmpdd", ColumnDescription = "numero de pedido", Length = 11, IsNullable = true)]
        public string NumPedido
        {
            get
            {
                return this.numPedidoField;
            }
            set
            {
                this.numPedidoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:identificador de la prepoliza poliza
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("noIdent")]
        [DataNames("_cntbl3c_noiden")]
        [SugarColumn(ColumnName = "_cntbl3c_noiden", ColumnDescription = "identificador de la prepoliza", Length = 11, IsNullable = true)]
        public string NoIndet
        {
            get
            {
                return this.noIndetField;
            }
            set
            {
                this.noIndetField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:cargo
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [JsonProperty("cargo")]
        [DataNames("_cntbl3c_cargo")]
        [SugarColumn(ColumnName = "_cntbl3c_cargo", ColumnDescription = "cargo", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Cargo
        {
            get
            {
                return this.cargoField;
            }
            set
            {
                this.cargoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:abono
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [JsonProperty("abono")]
        [DataNames("_cntbl3c_abono")]
        [SugarColumn(ColumnName = "_cntbl3c_abono", ColumnDescription = "abono", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Abono
        {
            get
            {
                return this.abonoField;
            }
            set
            {
                this.abonoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave del vendedor
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("vendedor")]
        [DataNames("_cntbl3c_vnddr")]
        [SugarColumn(ColumnName = "_cntbl3c_vnddr", ColumnDescription = "clave de vendedor", Length = 10, IsNullable = true)]
        public string Vendedor
        {
            get
            {
                return this.vendedorField;
            }
            set
            {
                this.vendedorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:subtipo de comprobante
        /// Default:1
        /// Nullable:False
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3c_sbtp")]
        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "_cntbl3c_sbtp", ColumnDescription = "subtipo del comprobante", Length = 1, IsNullable = true)]
        public EnumMetadataType Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:tipo de comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("tipo")]
        [DataNames("_cntbl3c_tipo")]
        [SugarColumn(ColumnName = "_cntbl3c_tipo", ColumnDescription = "tipo de comprobante", Length = 14, IsNullable = true)]
        public string TipoText
        {
            get
            {
                return Enum.GetName(typeof(EnumMetadataType), this.tipoField);
            }
            set
            {
                try
                {
                    this.tipoField = (EnumMetadataType)Enum.Parse(typeof(EnumMetadataType), value);
                }
                catch (Exception)
                {
                    this.tipoField = EnumMetadataType.NA;
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3c_id")]
        [SugarColumn(ColumnName = "_cntbl3c_id", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id
        {
            get
            {
                return indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:indice de relacion con cntbl3
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3c_subId")]
        [SugarColumn(ColumnName = "_cntbl3c_subId", ColumnDescription = "indice de relacion con cntbl3", Length = 11, IsNullable = true)]
        public int SubId
        {
            get
            {
                return this.subIndiceField;
            }
            set
            {
                this.subIndiceField = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Desc:registro activo
        /// Default:1
        /// Nullable:False
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3c_a")]
        [SugarColumn(ColumnDataType = "SMALINT", ColumnName = "_cntbl3c_a", ColumnDescription = "registro activo", Length = 1, IsNullable = true)]
        public bool IsActive
        {
            get
            {
                return this.isActiveField;
            }
            set
            {
                this.isActiveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:usuario creo
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3c_usr_n")]
        [SugarColumn(ColumnName = "_cntbl3c_usr_n", ColumnDescription = "clave del usuario que creo el registro", Length = 10, IsNullable = true)]
        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ultimo usuario que modifico
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3c_usr_m")]
        [SugarColumn(ColumnName = "_cntbl3c_usr_m", ColumnDescription = "clave del ultimo usuario que modifico el registro", Length = 10, IsNullable = true)]
        public string Modifica
        {
            get
            {
                return this.modificoField;
            }
            set
            {
                this.modificoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fec. sist. (aqui la fecha actua como fecha de emision del documento)
        /// Default:CURRENT_TIMESTAMP
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3c_fn")]
        [SugarColumn(ColumnName = "_cntbl3c_fn", ColumnDescription = "fec. sist. (aqui la fecha actua como fecha de emision del documento)", IsNullable = true)]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de modificacion
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3c_fm")]
        [SugarColumn(ColumnName = "_cntbl3c_fm", ColumnDescription = "fecha de modificacion", IsNullable = true)]
        public DateTime? FechaMod
        {
            get
            {
                return this.fechaModificaField;
            }
            set
            {
                this.fechaModificaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:version del comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("ver")]
        [DataNames("_cfdi_ver")]
        [SugarColumn(ColumnName = "_cntbl3c_ver", ColumnDescription = "version del comprobante", Length = 3, IsNullable = true)]
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        private EnumCfdiType tipoComprobanteField;
        [SugarColumn(IsIgnore = true)]
        [JsonIgnore]
        public EnumCfdiType TipoComprobante
        {
            get
            {
                return this.tipoComprobanteField;
            }
            set
            {
                this.tipoComprobanteField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("tipoComprobante")]
        [DataNames("_cfdi_efecto")]
        [SugarColumn(IsIgnore = true)]
        public string TipoComprobanteText
        {
            get
            {
                return Enum.GetName(typeof(EnumCfdiType), this.TipoComprobante);
            }
            set
            {
                if (value == "Ingreso" || value == "I")
                {
                    this.TipoComprobante = EnumCfdiType.Ingreso;
                }
                else if (value == "Egreso" || value == "E")
                {
                    this.TipoComprobante = EnumCfdiType.Egreso;
                }
                else if (value == "Traslado" || value == "T")
                {
                    this.TipoComprobante = EnumCfdiType.Traslado;
                }
                else if (value == "Nomina" || value == "N")
                {
                    this.TipoComprobante = EnumCfdiType.Nomina;
                }
                else if (value == "Pagos" || value == "P")
                {
                    this.TipoComprobante = EnumCfdiType.Pagos;
                }
                else
                {
                    this.TipoComprobante = EnumCfdiType.Ingreso;
                }
                this.OnPropertyChanged();
            }
        }

        private EnumCfdiSubType subTipoDeComprobanteField;
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public EnumCfdiSubType SubTipoComprobante
        {
            get
            {
                return this.subTipoDeComprobanteField;
            }
            set
            {
                this.subTipoDeComprobanteField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("subTipoComprobante")]
        [DataNames("_cfdi_doc_id")]
        [SugarColumn(IsIgnore = true)]
        public string SubTipoComprobanteText
        {
            get
            {
                return Enum.GetName(typeof(EnumCfdiSubType), this.subTipoDeComprobanteField);
            }
            set
            {
                this.subTipoDeComprobanteField = (EnumCfdiSubType)Enum.Parse(typeof(EnumCfdiSubType), value);
                this.OnPropertyChanged();
            }
        }

        private string folioField;
        /// <summary>
        /// Desc:folio de control interno
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("folio")]
        [DataNames("_cfdi_folio")]
        [SugarColumn(ColumnName = "_cntbl3c_folio", ColumnDescription = "folio de control interno", Length = 10, IsNullable = true)]
        public string Folio
        {
            get
            {
                return this.folioField;
            }
            set
            {
                this.folioField = value;
                this.OnPropertyChanged();
            }
        }

        private string serieField;
        /// <summary>
        /// Desc:serie de control interno
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("serie")]
        [DataNames("_cfdi_serie")]
        [SugarColumn(ColumnName = "_cntbl3c_serie", ColumnDescription = "serie de control interno", Length = 10, IsNullable = true)]
        public string Serie
        {
            get
            {
                return this.serieField;
            }
            set
            {
                this.serieField = value;
                this.OnPropertyChanged();
            }
        }

        private DateTime? fechaEmisionField;
        /// <summary>
        /// Desc:fecha de emision del comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("fecEmision")]
        [DataNames("_cfdi_fecems")]
        [SugarColumn(ColumnName = "_cntbl3c_fecems", ColumnDescription = "fecha de emision del comprobante", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public DateTime? FechaEmision
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEmisionField >= firstGoodDate)
                    return this.fechaEmisionField;
                return null;
            }
            set
            {
                this.fechaEmisionField = value;
                this.OnPropertyChanged();
            }
        }

        private DateTime? fechaPagoField;
        /// <summary>
        /// Desc:fecha de ultimo pago
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("fecPago")]
        [DataNames("_cfdi_fecupc")]
        [SugarColumn(ColumnName = "_cntbl3c_feculpg", ColumnDescription = "fecha del ultimo pago", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public DateTime? FechaPago
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaPagoField >= firstGoodDate)
                    return this.fechaPagoField;
                return null;
            }
            set
            {
                this.fechaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        private decimal tipoDeCambioField;
        [JsonProperty("tipoCambio")]
        [SugarColumn(IsIgnore = true)]
        public decimal TipoCambio
        {
            get
            {
                return this.tipoDeCambioField;
            }
            set
            {
                this.tipoDeCambioField = value;
                this.OnPropertyChanged();
            }
        }

        private string statusField;
        /// <summary>
        /// Desc:status del documento
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("status")]
        [DataNames("_cfdi_status")]
        [SugarColumn(ColumnName = "_cntbl3c_status", ColumnDescription = "status del documento", Length = 10, IsNullable = true)]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
                this.OnPropertyChanged();
            }
        }

        private string estadoField;
        /// <summary>
        /// Desc:ultimo estado del comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("estado")]
        [DataNames("_cfdi_estado")]
        [SugarColumn(ColumnName = "_cntbl3c_estado", ColumnDescription = "ultimo estado del comprobante", Length = 10, IsNullable = true)]
        public string Estado
        {
            get
            {
                return this.estadoField;
            }
            set
            {
                this.estadoField = value;
                this.OnPropertyChanged();
            }
        }

        private int numParcialidadField;
        /// <summary>
        /// Atributo condicional para expresar el número de parcialidad que corresponde al pago. Es requerido cuando MetodoDePagoDR contiene: “PPD” Pago en parcialidades o diferido.
        /// </summary>
        [JsonProperty("numPar")]
        [DataNames("_cfdi_par")]
        [SugarColumn(IsIgnore = true)]
        public int NumParcialidad
        {
            get
            {
                return this.numParcialidadField;
            }
            set
            {

                this.numParcialidadField = value;
                this.OnPropertyChanged();
            }
        }

        private int precisionDecimalField;
        [JsonProperty("presc")]
        [DataNames("_cfdi_prec")]
        [SugarColumn(IsIgnore = true)]
        public int PrecisionDecimal
        {
            get
            {
                return this.precisionDecimalField;
            }
            set
            {
                this.precisionDecimalField = value;
                this.OnPropertyChanged();
            }
        }

        private string emisorField;
        /// <summary>
        /// Desc:nombre del emisor
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("emisor")]
        [DataNames("_cfdi_nome")]
        [SugarColumn(ColumnName = "_cntbl3c_nome", ColumnDescription = "nombre del emisor del comprobante", Length = 256, IsNullable = true)]
        public string Emisor
        {
            get
            {
                return this.emisorField;
            }

            set
            {
                this.emisorField = value;
                this.OnPropertyChanged();
            }
        }

        private string emisorRFCField;
        /// <summary>
        /// Desc:rfc del emisor
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("emisorRFC")]
        [DataNames("_cfdi_rfce")]
        [SugarColumn(ColumnName = "_cntbl3c_rfce", ColumnDescription = "rfc del emisor ", Length = 14, IsNullable = true)]
        public string EmisorRFC
        {
            get
            {
                return this.emisorRFCField;
            }
            set
            {
                this.emisorRFCField = value;
                this.OnPropertyChanged();
            }
        }

        private string emisorClaveField;
        /// <summary>
        /// Desc:clave del receptor
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("emisorClave")]
        [SugarColumn(ColumnName = "_cntbl3c_clve", ColumnDescription = "clave del receptor", Length = 14, IsNullable = true)]
        public string EmisorClave
        {
            get
            {
                return this.emisorClaveField;
            }
            set
            {
                this.emisorClaveField = value;
                this.OnPropertyChanged();
            }
        }

        private string receptorField;
        /// <summary>
        /// Desc:nombre del receptor
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("receptor")]
        [DataNames("_cfdi_nomr")]
        [SugarColumn(ColumnName = "_cntbl3c_nomr", ColumnDescription = "nombre del receptor del comprobante", Length = 256, IsNullable = true)]
        public string Receptor
        {
            get
            {
                return this.receptorField;
            }
            set
            {
                this.receptorField = value;
                this.OnPropertyChanged();
            }
        }

        private string receptorRFCField;
        /// <summary>
        /// Desc:rfc del receptor
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("receptorRFC")]
        [DataNames("_cfdi_rfcr")]
        [SugarColumn(ColumnName = "_cntbl3c_rfcr", ColumnDescription = "rfc del receptor", Length = 14, IsNullable = true)]
        public string ReceptorRFC
        {
            get
            {
                return this.receptorRFCField;
            }
            set
            {
                this.receptorRFCField = value;
                this.OnPropertyChanged();
            }
        }

        private string receptorClaveField;
        /// <summary>
        /// Desc:clave del receptor
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("receptorClave")]
        [SugarColumn(ColumnName = "_cntbl3c_clvr", ColumnDescription = "clave del receptor", Length = 14, IsNullable = true)]
        public string ReceptorClave
        {
            get
            {
                return this.receptorClaveField;
            }
            set
            {
                this.receptorClaveField = value;
                this.OnPropertyChanged();
            }
        }

        private string claveMonedaField;
        /// <summary>
        /// Atributo requerido para identificar la clave de la moneda utilizada en los importes del documento relacionado, cuando se usa moneda nacional o el documento relacionado no especifica la moneda se registra MXN. Los importes registrados en los atributos “ImpSaldoAnt”, “ImpPagado” e “ImpSaldoInsoluto” de éste nodo, deben corresponder a esta moneda. Conforme con la especificación ISO 4217.
        /// </summary>
        [JsonProperty("claveMoneda")]
        [DataNames("_cfdi_moneda")]
        [SugarColumn(IsIgnore = true)]
        public string ClaveMoneda
        {
            get
            {
                return this.claveMonedaField;
            }
            set
            {
                this.claveMonedaField = value;
                this.OnPropertyChanged();
            }
        }

        private string claveFormaPagoField;
        [JsonProperty("claveFormaPago")]
        [DataNames("_cfdi_frmpg")]
        [SugarColumn(IsIgnore = true)]
        public string ClaveFormaPago
        {
            get
            {
                return this.claveFormaPagoField;
            }
            set
            {
                this.claveFormaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        private string claveMetodoPagoField;
        /// <summary>
        /// Atributo requerido para expresar la clave del método de pago que se registró en el documento relacionado
        /// </summary>
        [JsonProperty("claveMetodoPago")]
        [DataNames("_cfdi_mtdpg")]
        [SugarColumn(IsIgnore = true)]
        public string ClaveMetodoPago
        {
            get
            {
                return this.claveMetodoPagoField;
            }
            set
            {
                this.claveMetodoPagoField = value;
                this.OnPropertyChanged();
            }
        }

        private DateTime? fechaTimbreField;
        [JsonProperty("fecTimbre")]
        [DataNames("_cfdi_feccert")]
        [SugarColumn(IsIgnore = true)]
        public DateTime? FechaTimbre
        {
            get
            {
                return this.fechaTimbreField;
            }
            set
            {
                this.fechaTimbreField = value;
                this.OnPropertyChanged();
            }
        }
        
        private string idDocumentoField;
        /// <summary>
        /// Desc:folio fiscal
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("uuid")]
        [DataNames("_cfdi_uuid")]
        [SugarColumn(ColumnName = "_cntbl3c_uuid", ColumnDescription = "folio fiscal", Length = 36, IsNullable = true)]
        public string UUID
        {
            get
            {
                return this.idDocumentoField;
            }
            set
            {
                this.idDocumentoField = value;
                this.OnPropertyChanged();
            }
        }

        private decimal retencionIsrField;
        [JsonProperty("retencionISR")]
        [DataNames("_cfdi_retisr")]
        [SugarColumn(IsIgnore = true)]
        public decimal RetencionISR
        {
            get
            {
                return this.retencionIsrField;
            }
            set
            {
                this.retencionIsrField = value;
                this.OnPropertyChanged();
            }
        }

        private decimal retencionIvaField;
        [JsonProperty("retencionIVA")]
        [DataNames("_cfdi_retiva")]
        [SugarColumn(IsIgnore = true)]
        public decimal RetencionIVA
        {
            get
            {
                return this.retencionIvaField;
            }
            set
            {
                this.retencionIvaField = value;
                this.OnPropertyChanged();
            }
        }
        
        private decimal trasladoIVAField;
        /// <summary>
        /// Desc:total del iva trasladado
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [JsonProperty("trasladoIVA")]
        [DataNames("_cfdi_trsiva")]
        [SugarColumn(ColumnName = "_cntbl3c_ivat", ColumnDescription = "total del iva trasladado", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal TrasladoIVA 
        {
            get
            {
                return this.trasladoIVAField;
            }
            set
            {
                this.trasladoIVAField = value;
                this.OnPropertyChanged();
            }
        }

        private decimal retencionIepsField;
        [JsonProperty("retencionIEPS")]
        [DataNames("_cfdi_retieps")]
        [SugarColumn(IsIgnore = true)]
        public decimal RetencionIEPS
        {
            get
            {
                return this.retencionIepsField;
            }
            set
            {
                this.retencionIepsField = value;
                this.OnPropertyChanged();
            }
        }

        private decimal trasladoIepsField;
        [JsonProperty("trasladoIEPS")]
        [DataNames("_cfdi_trsieps")]
        [SugarColumn(IsIgnore = true)]
        public decimal TrasladoIEPS
        {
            get
            {
                return this.trasladoIepsField;
            }
            set
            {
                this.trasladoIepsField = value;
                this.OnPropertyChanged();
            }
        }

        private decimal subtotalField;
        /// <summary>
        /// Desc:sub total de comprobante
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [JsonProperty("subTotal")]
        [DataNames("_cfdi_sbttl")]
        [SugarColumn(ColumnName = "_cntbl3c_subtotal", ColumnDescription = "subtotal del comprobante", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Subtotal
        {
            get
            {
                return this.subtotalField;
            }
            set
            {
                this.subtotalField = value;
                this.OnPropertyChanged();
            }
        }

        private decimal descuentoField;
        /// <summary>
        /// Desc:monto del descuento aplicable
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [JsonProperty("descuento")]
        [DataNames("_cfdi_dscnt")]
        [SugarColumn(ColumnName = "_cntbl3c_desc", ColumnDescription = "monto de descuento aplicable", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Descuento
        {
            get
            {
                return this.descuentoField;
            }
            set
            {
                this.descuentoField = value;
                this.OnPropertyChanged();
            }
        }

        private decimal totalField;
        /// <summary>
        /// Desc:total del comprobante
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [JsonProperty("total")]
        [DataNames("_cfdi_total")]
        [SugarColumn(ColumnName = "_cntbl3c_total", ColumnDescription = "total del comprobante", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Total 
        {
            get
            {
                return this.totalField;
            }
            set
            {
                this.totalField = value;
                this.OnPropertyChanged();
            }
        }

        private decimal acumuladoField;
        /// <summary>
        /// Desc:acumulado del comprobante
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [JsonProperty("acumulado")]
        [DataNames("_cfdi_cbrd")]
        [SugarColumn(ColumnName = "_cntbl3c_acumulado", ColumnDescription = "acumulado del comprobante", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Acumulado 
        {
            get
            {
                return this.acumuladoField;
            }
            set
            {
                this.acumuladoField = value;
                this.OnPropertyChanged();
            }
        }

        private decimal saldoField;
        [JsonProperty("saldo")]
        [DataNames("_cfdi_saldo")]
        [SugarColumn(IsIgnore = true)]
        public decimal Saldo
        {
            get
            {
                return this.saldoField;
            }
            set
            {
                this.saldoField = value;
                this.OnPropertyChanged();
            }
        }

        private DateTime? FechaEntregaField;
        /// <summary>
        /// Desc:fecha de entrega del comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("fecEntrega")]
        [SugarColumn(ColumnName = "_cntbl3c_fecent", ColumnDescription = "fecha de entrega del comprobante", IsNullable = true)]
        public DateTime? FechaEntrega 
        {
            get
            {
                return this.FechaEntregaField;
            }
            set
            {
                this.FechaEntregaField = value;
                this.OnPropertyChanged();
            }
        }
    }
}