﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Edita.V2.Contable.Entities
{
    [JsonObject]
    public class CuentaBancaria : BasePropertyChangeImplementation
    {
        private string beneficiarioField;
        private string nombresField;
        private string primerApellidoField;
        private string segundoApellidoField;
        private string claveField;
        private string rfcField;
        private string numCtaField;
        private string codigoBancoField;
        private string nombreBancoField;
        private string sucursalField;
        private string clableField;
        private string tipoCuentaField;
        private string numClienteField;
        private string refNumericaField;
        private string refAlfaNumericaField;

        /// <summary>
        /// clave de control interno del beneficiario
        /// </summary>
        [JsonProperty("clave")]
        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre o razon social del beneficiario de la cuenta
        /// </summary>
        [JsonProperty("benef")]
        public string Nombre
        {
            get
            {
                return this.beneficiarioField;
            }
            set
            {
                this.beneficiarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre o nombres del beneficiario de la cuenta
        /// </summary>
        [JsonProperty("nombres")]
        public string Nombres
        {
            get
            {
                return this.nombresField;
            }
            set
            {
                this.nombresField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("primerApellido")]
        public string PrimerApellido
        {
            get
            {
                return this.primerApellidoField;
            }
            set
            {
                this.primerApellidoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("segundoApellido")]
        public string SegundoApellido
        {
            get
            {
                return this.segundoApellidoField;
            }
            set
            {
                this.segundoApellidoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// registro federal de contribuyentes
        /// </summary>
        [JsonProperty("rfc")]
        public string RFC
        {
            get
            {
                return this.rfcField;
            }
            set
            {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de cuenta
        /// </summary>
        [JsonProperty("numcta")]
        public string NumCuenta
        {
            get
            {
                return this.numCtaField;
            }
            set
            {
                this.numCtaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer clave de banco del catalogo SAT
        /// </summary>
        [JsonProperty("codigo")]
        public string Codigo
        {
            get
            {
                return this.codigoBancoField;
            }
            set
            {
                this.codigoBancoField = value;
                this.OnPropertyChanged();
            }
        }


        /// <summary>
        /// obtener o establecer el nombre de la institucion bancaria
        /// </summary>
        [JsonProperty("banco")]
        public string Banco
        {
            get
            {
                return this.nombreBancoField;
            }
            set
            {
                this.nombreBancoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer numero de la sucursal
        /// </summary>
        [JsonProperty("suc")]
        public string Sucursal
        {
            get
            {
                return this.sucursalField;
            }
            set
            {
                this.sucursalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer cuenta clabe para movimientos interbancarios
        /// </summary>
        [JsonProperty("clabe")]
        public string Clabe
        {
            get
            {
                return this.clableField;
            }
            set
            {
                this.clableField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [JsonProperty("tipo")]
        public string TipoCuenta
        {
            get
            {
                return this.tipoCuentaField;
            }
            set
            {
                this.tipoCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de cliente, este utilizando normalmente para layout de bancos
        /// </summary>
        [JsonProperty("numCliente")]
        public string NumCliente
        {
            get
            {
                return this.numClienteField;
            }
            set
            {
                this.numClienteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la referencia numerica
        /// </summary>
        [JsonProperty("refNumerica")]
        public string RefNumerica
        {
            get
            {
                return this.refNumericaField;
            }
            set
            {
                this.refNumericaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer la referencia alfanumerica
        /// </summary>
        [JsonProperty("refAlfaNumerica")]
        public string RefAlfaNumerica
        {
            get
            {
                return this.refAlfaNumericaField;
            }
            set
            {
                this.refAlfaNumericaField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
