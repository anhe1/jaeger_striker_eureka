﻿using System;
using System.ComponentModel;
using System.Linq;
using Jaeger.SAT.Entities;
using Newtonsoft.Json;
using Jaeger.Entities;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Edita.V2.Contable.Entities
{
    public class BancoCuenta : EntityBase
    {
        private double cargoMaximoField;
        private string codeField;
        private string monedaField;
        private string sucursalField;
        private string numeroDeCuentaField;
        private string clabeField;
        private string insitucionBancariaNCField;
        private string aliasField;
        private string bancoField;
        private string beneficiarioField;
        private string rfcField;
        private string numClienteField;
        private BindingList<ClaveFormaPago> itemsField;

        /// <summary>
        /// constructor
        /// </summary>
        public BancoCuenta()
        {
            this.itemsField = new BindingList<ClaveFormaPago>()
            {
                RaiseListChangedEvents = true
            };
        }

        /// <summary>
        /// alias de referencia para la cuenta bancaria
        /// </summary>
        [JsonProperty("alias")]
        public string Alias
        {
            get
            {
                return this.aliasField;
            }
            set
            {
                this.aliasField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del banco
        /// </summary>
        [JsonProperty("banco")]
        public string Banco
        {
            get
            {
                return this.bancoField;
            }
            set
            {
                this.bancoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del beneficiario de la cuenta
        /// </summary>
        [JsonProperty("benef")]
        public string Beneficiario
        {
            get
            {
                return this.beneficiarioField;
            }
            set
            {
                this.beneficiarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// registro federal de causantes
        /// </summary>
        [JsonProperty("rfc")]
        public string Rfc
        {
            get
            {
                return this.rfcField;
            }
            set
            {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cargo maximo que se puede hacer a la cuenta
        /// </summary>
        [JsonProperty("max")]
        public double CargoMaximo
        {
            get
            {
                return this.cargoMaximoField;
            }
            set
            {
                this.cargoMaximoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cuenta CLABE
        /// </summary>
        [JsonProperty("clabe")]
        public string Clabe
        {
            get
            {
                return this.clabeField;
            }
            set
            {
                this.clabeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        [JsonProperty("clave")]
        public string Clave
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre corto de la institución bancaria
        /// </summary>
        [JsonProperty("descr")]
        public string InsitucionBancaria
        {
            get
            {
                return this.insitucionBancariaNCField;
            }
            set
            {
                this.insitucionBancariaNCField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// clave de moneda
        /// </summary>
        [JsonProperty("moneda")]
        public string Moneda
        {
            get
            {
                return this.monedaField;
            }
            set
            {
                this.monedaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de cuenta bancaria
        /// </summary>
        [JsonProperty("numCta")]
        public string NumeroDeCuenta
        {
            get
            {
                return this.numeroDeCuentaField;
            }
            set
            {
                this.numeroDeCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("numCliente")]
        public string NumCliente
        {
            get
            {
                return this.numClienteField;
            }
            set
            {
                this.numClienteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de la sucursal
        /// </summary>
        [JsonProperty("sucursal")]
        public string Sucursal
        {
            get
            {
                return this.sucursalField;
            }
            set
            {
                this.sucursalField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("items")]
        public BindingList<ClaveFormaPago> Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
                this.OnPropertyChanged();
            }
        }

        public string Json(Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static BancoCuenta Json(string inputJson)
        {
            try
            {
                return JsonConvert.DeserializeObject<BancoCuenta>(inputJson);
            }
            catch (JsonException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

    }
}
