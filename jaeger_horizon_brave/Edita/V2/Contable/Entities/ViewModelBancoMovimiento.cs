using SqlSugar;

namespace Jaeger.Edita.V2.Contable.Entities
{
    [SugarTable("_cntbl3")]
    public class ViewModelBancoMovimiento : ViewModelPrePoliza
    {
        #region declaraciones
        
        private decimal saldoField;
        
        #endregion
        
        #region propiedades
        
        [SugarColumn(IsIgnore = true)]
        public decimal Saldo
        {
            get
            {
                return this.saldoField;
            }
            set
            {
                this.saldoField = value;
                this.OnPropertyChanged();
            }
        }
        
        #endregion
    }
}