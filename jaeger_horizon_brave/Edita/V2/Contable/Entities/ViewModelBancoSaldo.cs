﻿using System;
using System.Linq;
using SqlSugar;

namespace Jaeger.Edita.V2.Contable.Entities
{
    [SugarTable("_cntbbnc", "bancos: saldos del control de bancos")]
    public class ViewModelBancoSaldo : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private int anioField;
        private int mesField;
        private int indiceField;
        private int idDocumentoField;
        private decimal saldoInicialField;
        private decimal saldoFinalField;
        private DateTime fechaInicialField;
        private DateTime fechaNuevoField;
        private string creoField;
        private string notaField;

        public ViewModelBancoSaldo()
        {

        }

        [SugarColumn(ColumnName = "_cntbbnc_id", ColumnDescription = "indice de relacion de la cuenta de bancos.", IsIdentity = true, IsPrimaryKey = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// indice de relacion de la cuenta de banco
        /// </summary>
        [SugarColumn(ColumnName = "_cntbbnc_sbid", ColumnDescription = "indice de relacion de la cuenta de bancos.")]
        public int SubId
        {
            get
            {
                return this.idDocumentoField;
            }
            set
            {
                this.idDocumentoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_cntbbnc_anio", ColumnDescription = "ejecicio", Length = 2, IsNullable = false)]
        public int Anio
        {
            get
            {
                return this.anioField;
            }
            set
            {
                this.anioField = value;
            }
        }

        [SugarColumn(ColumnName = "_cntbbnc_mes", ColumnDescription = "periodo", Length = 2, IsNullable = false)]
        public int Mes
        {
            get
            {
                return this.mesField;
            }
            set
            {
                this.mesField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_cntbbnc_inicial", ColumnDescription = "saldo inicial de la cuenta", Length = 14, DecimalDigits = 4, IsNullable = false)]
        public decimal SaldoInicial
        {
            get
            {
                return this.saldoInicialField;
            }
            set
            {
                this.saldoInicialField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_cntbbnc_final", ColumnDescription = "saldo final de la cuenta", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal SaldoFinal
        {
            get
            {
                return this.saldoFinalField;
            }
            set
            {
                this.saldoFinalField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_cntbbnc_fs", ColumnDescription = "fecha inicial del saldo")]
        public DateTime FechaInicial
        {
            get
            {
                return this.fechaInicialField;
            }
            set
            {
                this.fechaInicialField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_cntbbnc_fn", ColumnDescription = "fecha de creacion del nuevo registro", ColumnDataType = "TIMESTAMP")]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_cntbbnc_usr_n", ColumnDescription = "clave del usuario que creo el registro", Length = 10)]
        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_cntbbnc_nota", ColumnDescription = "nota", Length = 255, IsNullable = true)]
        public string Nota
        {
            get
            {
                return this.notaField;
            }
            set
            {
                this.notaField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
