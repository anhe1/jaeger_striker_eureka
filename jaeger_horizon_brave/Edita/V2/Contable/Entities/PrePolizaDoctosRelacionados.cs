﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Jaeger.Edita.V2.Contable.Entities
{
    public class PrePolizaDoctosRelacionados : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private long indexField;
        private bool isActiveField;
        private string noIndetField;
        private string tituloField;
        private string descripcionField;
        private string contentField;
        private DateTime fechaNuevoField;
        private string creoField;
        private string urlDescargaField;

        public PrePolizaDoctosRelacionados()
        {

        }

        public long Id
        {
            get
            {
                return this.indexField;
            }
            set
            {
                this.indexField = value;
                this.OnPropertyChanged();
            }
        }

        public bool IsActive
        {
            get
            {
                return this.isActiveField;
            }
            set
            {
                this.isActiveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero o clave de indentificacion
        /// </summary>
        public string NoIndet
        {
            get
            {
                return this.noIndetField;
            }
            set
            {
                this.noIndetField = value;
                this.OnPropertyChanged();
            }
        }

        public string Titulo
        {
            get
            {
                return this.tituloField;
            }
            set
            {
                this.tituloField = value;
                this.OnPropertyChanged();
            }
        }

        public string Descripcion
        {
            get
            {
                return this.descripcionField;
            }
            set
            {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }

        public string Content
        {
            get
            {
                return this.contentField;
            }
            set
            {
                this.contentField = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        public string URL
        {
            get
            {
                return this.urlDescargaField;
            }
            set
            {
                this.urlDescargaField = value;
                this.OnPropertyChanged();
            }
        }

        public string KeyName()
        {

            //use MD5 hash to get a 16-byte hash of the string:

            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();

            byte[] inputBytes = Encoding.Default.GetBytes(string.Concat(this.noIndetField));

            byte[] hashBytes = provider.ComputeHash(inputBytes);

            //generate a guid from the hash:

            Guid hashGuid = new Guid(hashBytes);

            return hashGuid.ToString().ToUpper();

        }
    }
}
