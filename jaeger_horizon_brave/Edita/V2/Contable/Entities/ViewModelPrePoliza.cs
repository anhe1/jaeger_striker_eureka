﻿using System;
using System.ComponentModel;
using System.Linq;
using SqlSugar;
using Newtonsoft.Json;
using Jaeger.Edita.V2.Contable.Enums;
using Jaeger.Edita.V2.Contable.Interfaces;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Edita.V2.Contable.Entities
{
    [SugarTable("_cntbl3")]
    public class ViewModelPrePoliza : BasePropertyChangeImplementation, IPrePoliza, ICloneable
    {
        #region declaraciones
        private int indiceField;
        private int subIndiceField;
        private bool isActiveField = true;
        private EnumPolizaTipo tipoField;
        private EnumPrePolizaStatus estadoField;
        private string noIndetField;
        private string idDocumentoField;
        private long folioField;
        private string serieField;
        private DateTime fechaEmisionField;
        private DateTime? fechaDoctoField;
        private DateTime? fechaPagoField;
        private DateTime? fechaVenceField;
        private DateTime? fechaBovedaField;
        private DateTime? fechaCancelaField;
        private DateTime fechaNuevoField;
        private DateTime? fechaModificaField;
        private PrePolizaCuenta emisorField;
        private PrePolizaCuenta receptorField;
        private PrePolizaFormaPago formaDePagoField;
        private PrePolizaRelacionados prePolizasRelacionadasField;
        private string claveMonedaField;
        private string numDoctoField;
        private string numOperacionField;
        private string numAutorizacionField;
        private string referenciaField;
        private string referenciaNumericaField;
        private string conceptoField;
        private string creoField;
        private string modificaField;
        private string cancelaField;
        private string autorizaField;
        private decimal cargoField;
        private decimal abonoField;
        private decimal totalComprobantesField;
        private decimal tipoCambioField;
        private bool porJustificarField;
        private bool paraAbonoField;
        private bool multiPagoField;
        private string notasField;
        private string totalLetraField;
        private string recibeField;
        private byte[] logoField;
        private byte[] codigoBarrasField;
        private BindingList<ViewModelPrePolizaComprobante> comprobantesField;
        private BindingList<PrePolizaDoctosRelacionados> doctosRelacionados;

        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ViewModelPrePoliza()
        {
            this.FechaEmision = DateTime.Now;
            this.FechaDocto = DateTime.Now;
            this.FechaNuevo = DateTime.Now;
            this.FechaPago = null;
            this.Moneda = "MXN";
            this.TipoCambio = 1;
            this.emisorField = new PrePolizaCuenta();
            this.receptorField = new PrePolizaCuenta();
            this.formaDePagoField = new PrePolizaFormaPago();
            this.Estado = EnumPrePolizaStatus.NoAplicado;

            this.comprobantesField = new BindingList<ViewModelPrePolizaComprobante>()
            {
                RaiseListChangedEvents = true
            };
            this.comprobantesField.AddingNew += ComprobantesField_AddingNew;
            this.comprobantesField.ListChanged += Comprobantes_ListChanged;

            this.doctosRelacionados = new BindingList<PrePolizaDoctosRelacionados>()
            {
                RaiseListChangedEvents = true
            };
            this.doctosRelacionados.AddingNew += DoctosRelacionados_AddingNew;
            this.doctosRelacionados.ListChanged += DoctosRelacionados_ListChanged;

            this.prePolizasRelacionadasField = new PrePolizaRelacionados();
        }

        /// <summary>
        /// constructor
        /// </summary>
        public ViewModelPrePoliza(string creo)
        {
            this.FechaEmision = DateTime.Now;
            this.FechaDocto = DateTime.Now;
            this.FechaPago = null;
            this.Moneda = "MXN";
            this.TipoCambio = 1;
            this.emisorField = new PrePolizaCuenta();
            this.receptorField = new PrePolizaCuenta();
            this.formaDePagoField = new PrePolizaFormaPago();
            this.Estado = EnumPrePolizaStatus.NoAplicado;
            this.Creo = creo;
            this.comprobantesField = new BindingList<ViewModelPrePolizaComprobante>()
            {
                RaiseListChangedEvents = true
            };
            this.comprobantesField.AddingNew += ComprobantesField_AddingNew;
            this.comprobantesField.ListChanged += Comprobantes_ListChanged;
            this.prePolizasRelacionadasField = new PrePolizaRelacionados();
        }

        #region propiedades

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_id")]
        [SugarColumn(ColumnName = "_cntbl3_id", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public int SubId
        {
            get
            {
                return this.subIndiceField;
            }
            set
            {
                this.subIndiceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:registro activo
        /// Default:1
        /// Nullable:False
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_a")]
        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "_cntbl3_a", ColumnDescription = "registro activo", IsNullable = false, Length = 1)]
        public bool IsActive
        {
            get
            {
                return this.isActiveField;
            }
            set
            {
                this.isActiveField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public EnumPrePolizaStatus Estado
        {
            get
            {
                return this.estadoField;
            }
            set
            {
                this.estadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:status del documento
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("estado")]
        [DataNames("_cntbl3_status")]
        [SugarColumn(ColumnName = "_cntbl3_status", ColumnDescription = "status del documento", IsNullable = true, Length = 10)]
        public string EstadoText
        {
            get
            {
                return Enum.GetName(typeof(EnumPrePolizaStatus), this.Estado);
            }
            set
            {
                try
                {
                    this.Estado = (EnumPrePolizaStatus)Enum.Parse(typeof(EnumPrePolizaStatus), value);
                    this.OnPropertyChanged();
                }
                catch (Exception)
                {
                    this.Estado = EnumPrePolizaStatus.NoAplicado;
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de prepoliza
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public EnumPolizaTipo Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave del documento (I - Ingreso, E-Egreso)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("tipo")]
        [DataNames("_cntbl3_tipo")]
        [SugarColumn(ColumnName = "_cntbl3_tipo", ColumnDescription = "clave del documento (I - Ingreso, E-Egreso)", IsNullable = true, Length = 10)]
        public string TipoText
        {
            get
            {
                return Enum.GetName(typeof(EnumPolizaTipo), this.tipoField);
            }
            set
            {
                try
                {
                    this.tipoField = (EnumPolizaTipo)Enum.Parse(typeof(EnumPolizaTipo), value);
                }
                catch (Exception)
                {
                    this.tipoField = EnumPolizaTipo.Ingreso;
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:identificador de la prepoliza poliza
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("noIdent")]
        [DataNames("_cntbl3_noiden")]
        [SugarColumn(ColumnName = "_cntbl3_noiden", ColumnDescription = "identificador de la prepoliza poliza", IsNullable = true, Length = 11)]
        public string NoIndet
        {
            get
            {
                return this.noIndetField;
            }
            set
            {
                this.noIndetField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:folio de control
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("folio")]
        [DataNames("_cntbl3_folio")]
        [SugarColumn(ColumnName = "_cntbl3_folio", IsNullable = true, Length = 11)]   
        public long Folio
        {
            get
            {
                return this.folioField;
            }
            set
            {
                this.folioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:serie de control
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("serie")]
        [DataNames("_cntbl3_serie")]
        [SugarColumn(ColumnName = "_cntbl3_serie", ColumnDescription = "serie de control interno", IsNullable = true, Length = 10)]
        public string Serie
        {
            get
            {
                return this.serieField;
            }
            set
            {
                this.serieField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de la prepoliza
        /// Default:current_timestamp()
        /// Nullable:True
        /// </summary>           
        [JsonProperty("fecEmision")]
        [DataNames("_cntbl3_fecems")]
        [SugarColumn(ColumnName = "_cntbl3_fecems", ColumnDescription = "fecha de la prepoliza", IsNullable = true)]
        public DateTime FechaEmision
        {
            get
            {
                return this.fechaEmisionField;
            }
            set
            {
                this.fechaEmisionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha del documento
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("fecDocto")]
        [DataNames("_cntbl3_fecdoc")]
        [SugarColumn(ColumnName = "_cntbl3_fecdoc", ColumnDescription = "fecha del documento", IsNullable = true)]
        public DateTime? FechaDocto
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaDoctoField >= firstGoodDate)
                {
                    return this.fechaDoctoField;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                this.fechaDoctoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de cobro o pago
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("fecPago")]
        [DataNames("_cntbl3_fccbr")]
        [SugarColumn(ColumnName = "_cntbl3_fccbr", ColumnDescription = "fecha de cobro o pago", IsNullable = true)]
        public DateTime? FechaPago
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaPagoField >= firstGoodDate)
                {
                    return this.fechaPagoField;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                this.fechaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de vencimiento
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("fecVence")]
        [DataNames("_cntbl3_fcvnc")]
        [SugarColumn(ColumnName = "_cntbl3_fcvnc", ColumnDescription = "fecha de vencimiento", IsNullable = true)]
        public DateTime? FechaVence
        {
            get
            {
                DateTime firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaVenceField >= firstGooDate)
                {
                    return this.fechaVenceField;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                this.fechaVenceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de liberación (transmición al banco)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("fecBoveda")]
        [DataNames("_cntbl3_fclib")]
        [SugarColumn(ColumnName = "_cntbl3_fclib", ColumnDescription = "fecha de liberación (transmición al banco)", IsNullable = true)]
        public DateTime? FechaBoveda
        {
            get
            {
                DateTime firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaBovedaField >= firstGooDate)
                    return this.fechaBovedaField;
                return null;
            }
            set
            {
                this.fechaBovedaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de cancelacion
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_cntbl3_fccncl")]
        [SugarColumn(ColumnName = "_cntbl3_fccncl", ColumnDescription = "fecha de cancelacion", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaCancela
        {
            get
            {
                DateTime firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaCancelaField >= firstGooDate)
                    return this.fechaCancelaField;
                return null;
            }
            set
            {
                this.fechaCancelaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// informacion del emisor del recibo
        /// </summary>
        [JsonProperty("emisor")]
        [SugarColumn(IsIgnore = true)]
        public PrePolizaCuenta Emisor
        {
            get
            {
                return this.emisorField;
            }
            set
            {
                this.emisorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// informacion del receptor del recibo
        /// </summary>
        [JsonProperty("receptor")]
        [SugarColumn(IsIgnore = true)]
        public PrePolizaCuenta Receptor
        {
            get
            {
                return this.receptorField;
            }
            set
            {
                this.receptorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// forma de pago
        /// </summary>
        [JsonProperty("formaPago")]
        [SugarColumn(IsIgnore = true)]
        public PrePolizaFormaPago FormaDePago
        {
            get
            {
                return this.formaDePagoField;
            }
            set
            {
                this.formaDePagoField = value;
            }
        }

        /// <summary>
        /// obtener o establecer objeto de las prepolizas relacionadas a la instancia
        /// </summary>
        [JsonProperty("prePolizaRelacionada")]
        [SugarColumn(IsIgnore = true)]
        public PrePolizaRelacionados Relacion
        {
            get
            {
                return this.prePolizasRelacionadasField;
            }
            set
            {
                this.prePolizasRelacionadasField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("claveMoneda")]
        [SugarColumn(IsIgnore = true)]
        public string Moneda
        {
            get
            {
                return this.claveMonedaField;
            }
            set
            {
                this.claveMonedaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("tipoCambio")]
        [SugarColumn(IsIgnore = true)]
        public decimal TipoCambio
        {
            get
            {
                return this.tipoCambioField;
            }
            set
            {
                this.tipoCambioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:numero de documento
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("numDocto")]
        [DataNames("_cntbl3_nodocto")]
        [SugarColumn(ColumnName = "_cntbl3_nodocto", ColumnDescription = "numero de documento", IsNullable = true, Length = 20)]
        public string NumDocto
        {
            get
            {
                return this.numDoctoField;
            }
            set
            {
                this.numDoctoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// este campo se esta ignorando
        /// </summary>
        [JsonIgnore]
        [DataNames("_cntbl3_uuid")]
        [SugarColumn(IsIgnore = true, ColumnName = "_cntbl3_uuid", ColumnDescription = "id de documento", IsNullable = false, Length = 36)]
        public string IdDocumento
        {
            get
            {
                return this.idDocumentoField;
            }
            set
            {
                this.idDocumentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de operacion
        /// </summary>
        [JsonProperty("numOperacion")]
        [SugarColumn(IsIgnore = true)]
        public string NumOperacion
        {
            get
            {
                return this.numOperacionField;
            }
            set
            {
                this.numOperacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:numero de autorizacion
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("numAuto")]
        [DataNames("_cntbl3_numauto")]
        [SugarColumn(ColumnName = "_cntbl3_numauto", ColumnDescription = "numero de autorizacion", IsNullable = true, Length = 64)]
        public string NumAutorizacion
        {
            get
            {
                return this.numAutorizacionField;
            }
            set
            {
                this.numAutorizacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:referencia alfanumerica
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("ref")]
        [DataNames("_cntbl3_ref")]
        [SugarColumn(ColumnName = "_cntbl3_ref", ColumnDescription = "referencia alfanumerica", IsNullable = true, Length = 64)]
        public string Referencia
        {
            get
            {
                return this.referenciaField;
            }
            set
            {
                this.referenciaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// referencia numerica
        /// </summary>
        [JsonProperty("refnum")]
        [SugarColumn(IsIgnore = true)]
        public string ReferenciaNumerica
        {
            get
            {
                return this.referenciaNumericaField;
            }
            set
            {
                this.referenciaNumericaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:concepto
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("concepto")]
        [DataNames("_cntbl3_cncpt")]
        [SugarColumn(ColumnName = "_cntbl3_cncpt", ColumnDescription = "concepto", IsNullable = true, Length = 256)]
        public string Concepto
        {
            get
            {
                return this.conceptoField;
            }
            set
            {
                this.conceptoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:cargo
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [JsonProperty("cargo")]
        [DataNames("_cntbl3_cargo")]
        [SugarColumn(ColumnName = "_cntbl3_cargo", ColumnDescription = "cargo", IsNullable = true, Length = 11, DecimalDigits = 4)]
        public decimal Cargo
        {
            get
            {
                return this.cargoField;
            }
            set
            {
                this.cargoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:abono
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [JsonProperty("abono")]
        [DataNames("_cntbl3_abono")]
        [SugarColumn(ColumnName = "_cntbl3_abono", ColumnDescription = "abono", IsNullable = true, Length = 11, DecimalDigits = 4)]
        public decimal Abono
        {
            get
            {
                return this.abonoField;
            }
            set
            {
                this.abonoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:bandera solo para indicar si el gasto es por justificar
        /// Default:0
        /// Nullable:False
        /// </summary>           
        [JsonProperty("porJustificar")]
        [DataNames("_cntbl3_por")]
        [SugarColumn(ColumnName = "_cntbl3_por", ColumnDescription = "bandera solo para indicar si el gasto es por justificar", IsNullable = false, Length = 1)]
        public bool PorJustificar
        {
            get
            {
                return this.porJustificarField;
            }
            set
            {
                this.porJustificarField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("paraAbono")]
        [SugarColumn(IsIgnore = true)]
        public bool ParaAbono
        {
            get
            {
                return this.paraAbonoField;
            }
            set
            {
                this.paraAbonoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("multiPago")]
        [SugarColumn(IsIgnore = true)]
        public bool MultiPago
        {
            get
            {
                return this.multiPagoField;
            }
            set
            {
                this.multiPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:notas
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("nota")]
        [DataNames("_cntbl3_nota")]
        [SugarColumn(ColumnName = "_cntbl3_nota", ColumnDescription = "notas", IsNullable = true, Length = 64)]
        public string Notas
        {
            get
            {
                return this.notasField;
            }
            set
            {
                this.notasField = value;
            }
        }

        /// <summary>
        /// Desc:fec. sist. (aqui la fecha actua como fecha de emision del documento)
        /// Default:current_timestamp()
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_fn")]
        [SugarColumn(ColumnDataType = "TIMESTAMP", ColumnName = "_cntbl3_fn", ColumnDescription = "fecha de creacion del registro", IsNullable = false)]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de modificacion
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_fm")]
        [SugarColumn(ColumnName = "_cntbl3_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaMod
        {
            get
            {
                return this.fechaModificaField;
            }
            set
            {
                this.fechaModificaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave forma de pago
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_frmclv")]
        [SugarColumn(ColumnName = "_cntbl3_frmclv")]
        public string FormaDePagoText
        {
            get
            {
                return this.FormaDePago.Clave;
            }
            set
            {
                this.FormaDePago.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave de banco emisor segun catalogo SAT
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_bncclve")]
        [SugarColumn(ColumnName = "_cntbl3_bncclve")]
        public string EmisorCodigo
        {
            get
            {
                return this.Emisor.Codigo;
            }
            set
            {
                this.Emisor.Codigo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave de banco receptor segun catalogo SAT
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_bncclvr")]
        [SugarColumn(ColumnName = "_cntbl3_bncclvr")]
        public string ReceptorCodigo
        {
            get
            {
                return this.Receptor.Codigo;
            }
            set
            {
                this.Receptor.Codigo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:usuario creo
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_usr_n")]
        [SugarColumn(ColumnName = "_cntbl3_usr_n", ColumnDescription = "clave del usuario que crea el registro", IsNullable = true, Length = 10)]
        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave de control interno del emisor
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_clve")]
        [SugarColumn(ColumnName = "_cntbl3_clve", ColumnDescription = "clave de control interno del emisor", IsNullable = true, Length = 14)]
        public string EmisorClave
        {
            get
            {
                return this.Emisor.Clave;
            }
            set
            {
                this.Emisor.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave de control interno del receptor
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_clvr")]
        [SugarColumn(ColumnName = "_cntbl3_clvr")]
        public string ReceptorClave
        {
            get
            {
                return this.Receptor.Clave;
            }
            set
            {
                this.Receptor.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:rfc del beneficiario
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_rfce")]
        [SugarColumn(ColumnName = "_cntbl3_rfce", ColumnDescription = "rfc del beneficiario", IsNullable = true, Length = 14)]
        public string EmisorRFC
        {
            get
            {
                return this.Emisor.Rfc;
            }
            set
            {
                this.Emisor.Rfc = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:rfc del beneficiario
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_rfcr")]
        [SugarColumn(ColumnName = "_cntbl3_rfcr")]
        public string ReceptorRFC
        {
            get
            {
                return this.Receptor.Rfc;
            }
            set
            {
                this.Receptor.Rfc = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:numero de cuenta
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_nmctar")]
        [SugarColumn(ColumnName = "_cntbl3_nmctar")]
        public string ReceptorNumCta
        {
            get
            {
                return this.Receptor.NumCta;
            }
            set
            {
                this.Receptor.NumCta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:numero de la sucursal de banco
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_scrslr")]
        [SugarColumn(ColumnName = "_cntbl3_scrslr")]
        public string ReceptorSucursal
        {
            get
            {
                return this.Receptor.Sucursal;
            }
            set
            {
                this.Receptor.Sucursal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:numero de cuenta del emisor
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_nmctae")]
        [SugarColumn(ColumnName = "_cntbl3_nmctae", ColumnDescription = "numero de cuenta del emisor", IsNullable = true, Length = 14)]
        public string EmisorNumCta
        {
            get
            {
                return this.Emisor.NumCta;
            }
            set
            {
                this.Emisor.NumCta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:cuenta clabe
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_clbr")]
        [SugarColumn(ColumnName = "_cntbl3_clbr")]
        public string ReceptorCLABE
        {
            get
            {
                return this.Receptor.Clabe;
            }
            set
            {
                this.Receptor.Clabe = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:forma de pago
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_frmpg")]
        [SugarColumn(ColumnName = "_cntbl3_frmpg")]
        public string FormaPagoDescripcion
        {
            get
            {
                return this.FormaDePago.Descripcion;
            }
            set
            {
                this.FormaDePago.Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:nombre del banco
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_bancor")]
        [SugarColumn(ColumnName = "_cntbl3_bancor")]
        public string ReceptorBanco
        {
            get
            {
                return this.Receptor.Banco;
            }
            set
            {
                this.Receptor.Banco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:nombre del beneficiario
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_benef")]
        [SugarColumn(ColumnName = "_cntbl3_benef")]
        public string ReceptorBeneficiario
        {
            get
            {
                return this.Receptor.Nombre;
            }
            set
            {
                this.Receptor.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ultimo usuario que modifico
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_usr_m")]
        [SugarColumn(ColumnName = "_cntbl3_usr_m", ColumnDescription = "clave del ultimo usuario que modifico el registro", IsNullable = true, Length = 10)]   
        public string Modifica
        {
            get
            {
                return this.modificaField;
            }
            set
            {
                this.modificaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:usuario cancela
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_usr_c")]
        [SugarColumn(ColumnName = "_cntbl3_usr_c", IsOnlyIgnoreInsert = true)]
        public string Cancela
        {
            get
            {
                return this.cancelaField;
            }
            set
            {
                this.cancelaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:usuario autoriza
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_usr_a")]
        [SugarColumn(ColumnName = "_cntbl3_usr_a", IsOnlyIgnoreInsert = true)]
        public string Autoriza
        {
            get
            {
                return this.autorizaField;
            }
            set
            {
                this.autorizaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cntbl3_json")]
        [SugarColumn(ColumnName = "_cntbl3_json")]
        public string JPrepoliza
        {
            get
            {
                return JsonConvert.SerializeObject(this);
            }
            set
            {
                ViewModelPrePoliza item = JsonConvert.DeserializeObject<ViewModelPrePoliza>(value);
                if (item != null)
                {
                    this.Emisor = item.Emisor;
                    this.Receptor = item.Receptor;
                    this.Comprobantes = item.Comprobantes;
                    
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:informacion de las autorizaciones para el documento
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_cntbl3_jauto")]
        [SugarColumn(ColumnName = "_cntbl3_jauto", IsIgnore = true)]
        public string AutorizacioJson
        {
            get
            {
                return null;
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string TotalLetra
        {
            get
            {
                return this.totalLetraField;
            }
            set
            {
                this.totalLetraField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("recibe")]
        [SugarColumn(IsIgnore = true)]
        public string Recibe
        {
            get
            {
                return this.recibeField;
            }
            set
            {
                this.recibeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// listado de comprobantes relacionados a la prepoliza
        /// </summary>
        [JsonProperty("comprobantes")]
        [SugarColumn(IsIgnore = true)]
        public BindingList<ViewModelPrePolizaComprobante> Comprobantes
        {
            get
            {
                return this.comprobantesField;
            }
            set
            {
                if (this.comprobantesField != null)
                {
                    this.comprobantesField.ListChanged -= Comprobantes_ListChanged;
                }
                this.comprobantesField = value;
                if (this.comprobantesField != null)
                {
                    this.comprobantesField.ListChanged += Comprobantes_ListChanged;
                }
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public BindingList<PrePolizaDoctosRelacionados> Doctos
        {
            get
            {
                return this.doctosRelacionados;
            }
            set
            {
                if (this.doctosRelacionados != null)
                {
                }
                this.doctosRelacionados = value;
                if (this.doctosRelacionados != null)
                {
                }
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public byte[] LogoTipo
        {
            get
            {
                return this.logoField;
            }
            set
            {
                this.logoField = value;
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public byte[] Cbb
        {
            get
            {
                return this.codigoBarrasField;
            }
            set
            {
                this.codigoBarrasField = value;
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public bool Editable
        {
            get
            {
                if (this.Estado == EnumPrePolizaStatus.Aplicado | this.Estado == EnumPrePolizaStatus.Cancelado && this.PorJustificar == false)
                {
                    return false;
                }
                return !(this.Id > 0);
            }
        }

        #endregion

        #region metodos

        public void Clonar()
        {
            if (this.Tipo == EnumPolizaTipo.Ingreso || this.Tipo == EnumPolizaTipo.Egreso)
            {
                this.Id = 0;
                this.SubId = 0;
                this.FechaEmision = DateTime.Now;
                this.FechaNuevo = DateTime.Now;
                this.FechaMod = null;
                this.Modifica = null;
                this.NoIndet = "";
                // eliminar el id relacionado en las partidas
                foreach (ViewModelPrePolizaComprobante item in this.comprobantesField)
                {
                    if (item.IsActive == false)
                    {
                        this.comprobantesField.Remove(item);
                    }
                    else
                    {
                        item.Id = 0;
                        item.SubId = 0;
                    }
                }
            }
        }

        private void ComprobantesField_AddingNew(object sender, AddingNewEventArgs e)
        {
        }

        private void Comprobantes_ListChanged(object sender, ListChangedEventArgs e)
        {
            this.totalComprobantesField = this.Comprobantes.Where((ViewModelPrePolizaComprobante p) => p.IsActive == true).Sum((ViewModelPrePolizaComprobante p) => p.Abono);
            if (this.PorJustificar == false)
                this.Cargo = this.totalComprobantesField;

            this.totalComprobantesField = this.Comprobantes.Where((ViewModelPrePolizaComprobante p) => p.IsActive == true && p.TipoComprobante == CFDI.Enums.EnumCfdiType.Ingreso).Sum((ViewModelPrePolizaComprobante p) => p.Cargo);
            if (this.PorJustificar == false)
                this.Abono = this.totalComprobantesField;
        }

        private void DoctosRelacionados_ListChanged(object sender, ListChangedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void DoctosRelacionados_AddingNew(object sender, AddingNewEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void Sustituir()
        {
            this.Relacion = new PrePolizaRelacionados();
            this.Relacion.TipoRelacion = "03 Sustitución";
            this.Relacion.PrePolizas.Add(new PrePolizaRelacionado { NoIndet = this.NoIndet, Emisor = this.Emisor, Receptor = this.Receptor, FechaEmision = this.FechaEmision, FormaDePago = this.FormaDePago, Abono = this.Abono, Cargo = this.Cargo });
            this.Clonar();
        }

        /// <summary>
        /// buscar un uuid en la lista de comprobantes para evitar duplicados
        /// </summary>
        public ViewModelPrePolizaComprobante Buscar(string uuid)
        {
            return this.Comprobantes.FirstOrDefault<ViewModelPrePolizaComprobante>((ViewModelPrePolizaComprobante p) => p.UUID == uuid);
        }

        public bool AplicarStatus()
        {
            if (this.estadoField == EnumPrePolizaStatus.Cancelado)
            {
                return false;
            }

            if (this.estadoField == EnumPrePolizaStatus.NoAplicado)
            {
                if (this.Comprobantes != null)
                {
                    if (this.Comprobantes.Count == 0)
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }

            if (this.estadoField == EnumPrePolizaStatus.Aplicado)
            {
            }
            return false;
        }

        public static ViewModelPrePoliza Json(string inputJson)
        {
            try
            {
                return JsonConvert.DeserializeObject<ViewModelPrePoliza>(inputJson);
            }
            catch (JsonException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        #endregion

        [Browsable(false)]
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string Error
        {
            get
            {
                if (this.comprobantesField != null)
                {
                    if (this.Cargo != this.Abono)
                    {
                        return "No coincide";
                    }
                }
                return string.Empty;
            }
        }
        [SugarColumn(IsIgnore = true)]
        public string this[string columnName]
        {
            get
            {
                return string.Empty;
            }
        }
    }
}
