﻿using System;
using System.ComponentModel;
using Jaeger.Edita.V2.Contable.Entities;
using Jaeger.Edita.V2.Contable.Enums;

namespace Jaeger.Edita.V2.Contable.Interfaces
{
    interface IPrePoliza
    {
        int Id { get; set; }

        int SubId { get; set; }

        bool IsActive { get; set; }

        EnumPrePolizaStatus Estado { get; set; }

        EnumPolizaTipo Tipo { get; set; }
        
        string NoIndet { get; set; }

        long Folio { get; set; }

        string Serie { get; set; }

        DateTime FechaEmision { get; set; }

        DateTime? FechaDocto { get; set; }

        DateTime? FechaPago { get; set; }

        DateTime? FechaVence { get; set; }

        DateTime? FechaBoveda { get; set; }

        DateTime? FechaCancela { get; set; }

        PrePolizaCuenta Emisor { get; set; }

        PrePolizaCuenta Receptor { get; set; }

        PrePolizaFormaPago FormaDePago { get; set; }

        PrePolizaRelacionados Relacion { get; set; }

        string NumDocto { get; set; }

        string NumOperacion { get; set; }

        string NumAutorizacion { get; set; }

        string Referencia { get; set; }

        string ReferenciaNumerica { get; set; }

        string Concepto { get; set; }

        decimal Cargo { get; set; }

        decimal Abono { get; set; }

        bool PorJustificar { get; set; }

        bool ParaAbono { get; set; }

        bool MultiPago { get; set; }

        string Notas { get; set; }

        string Recibe { get; set; }

        BindingList<ViewModelPrePolizaComprobante> Comprobantes { get; set; }

        BindingList<PrePolizaDoctosRelacionados> Doctos { get; set; }

        string Creo { get; set; }

        string Modifica { get; set; }

        DateTime FechaNuevo { get; set; }

        DateTime? FechaMod { get; set; }

    }
}
