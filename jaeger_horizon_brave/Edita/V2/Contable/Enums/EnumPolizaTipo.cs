﻿using System;

namespace Jaeger.Edita.V2.Contable.Enums
{
    public enum EnumPolizaTipo
    {
        Ninguno,
        Ingreso,
        Egreso,
        Diario
    }
}