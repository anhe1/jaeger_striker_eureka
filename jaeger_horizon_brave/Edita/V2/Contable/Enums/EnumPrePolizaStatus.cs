﻿using System;
using System.ComponentModel;

namespace Jaeger.Edita.V2.Contable.Enums
{
    public enum EnumPrePolizaStatus
    {
        [Description("Cancelado")]
        Cancelado,
        [Description("No Aplicado")]
        NoAplicado,
        [Description("Aplicado")]
        Aplicado
    }
}
