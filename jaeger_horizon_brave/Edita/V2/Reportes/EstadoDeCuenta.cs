﻿using System;
using System.Linq;
using System.ComponentModel;

namespace Jaeger.Entities.Reportes
{
    public class EstadoDeCuenta : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private DateTime fechaEmisionField;
        private string personaField;
        private string rfcField;
        private decimal acumuladoField;
        private decimal totalField;
        private decimal saldoField;
        private string periodoField;
        private string creoField;
        private BindingList<EstadoDeCuentaComprobantes> itemsField;

        public EstadoDeCuenta()
        {
            this.fechaEmisionField = DateTime.Now;
            this.itemsField = new BindingList<EstadoDeCuentaComprobantes>();
        }

        public DateTime FechaEmision
        {
            get
            {
                return this.fechaEmisionField;
            }
            set
            {
                this.fechaEmisionField = value;
                this.OnPropertyChanged();
            }
        }

        public string Emisor
        {
            get
            {
                return this.personaField;
            }
            set
            {
                this.personaField = value;
                this.OnPropertyChanged();
            }
        }

        public string RFC
        {
            get
            {
                return this.rfcField;
            }
            set
            {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Acumulado
        {
            get
            {
                return this.acumuladoField;
            }
            set
            {
                this.acumuladoField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Total
        {
            get
            {
                return this.totalField;
            }
            set
            {
                this.totalField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Saldo
        {
            get
            {
                return this.saldoField;
            }
            set
            {
                this.saldoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Periodo
        {
            get
            {
                return this.periodoField;
            }
            set
            {
                this.periodoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<EstadoDeCuentaComprobantes> Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
