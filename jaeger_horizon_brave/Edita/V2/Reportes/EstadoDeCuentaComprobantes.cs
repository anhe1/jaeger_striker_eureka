﻿using System;
using System.Linq;

namespace Jaeger.Entities.Reportes
{
    public class EstadoDeCuentaComprobantes : ComprobanteNormal
    {
        private DateTime? fechaUltCobroField;
        private DateTime? fechaVenceField;

        public EstadoDeCuentaComprobantes()
        {

        }
        public DateTime? FechaUltCobro
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaUltCobroField >= firstGoodDate)
                {
                    return this.fechaUltCobroField;
                }
                return null;
            }
            set
            {
                this.fechaUltCobroField = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime? FechaVence
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaVenceField >= firstGoodDate)
                {
                    return this.fechaVenceField;
                }
                return null;
            }
            set
            {
                this.fechaVenceField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
