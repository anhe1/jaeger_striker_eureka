﻿using System;
using System.Linq;
using System.ComponentModel;

namespace Jaeger.Entities.Reportes
{
    public class IngresoGastosEstado
    {
        private BindingList<IngresoGastosEstadoT> listaIngresosField;
        private BindingList<IngresoGastosEstadoT> listaEgresosField;
        private BindingList<IngresoGastosEstadoT> listaNominaField;
        
        public BindingList<IngresoGastosEstadoT> Ingresos
        {
            get
            {
                return this.listaIngresosField;
            }
            set
            {
                this.listaIngresosField = value;
            }
        }

        public BindingList<IngresoGastosEstadoT> Egresos
        {
            get
            {
                return this.listaEgresosField;
            }
            set
            {
                this.listaEgresosField = value;
            }
        }

        public BindingList<IngresoGastosEstadoT> Nomina
        {
            get
            {
                return this.listaNominaField;
            }
            set
            {
                this.listaNominaField = value;
            }
        }
    }

    public partial class IngresoGastosEstadoT
    {
        private decimal trasladoIVAField;
        private decimal trasladoIEPSField;
        private decimal retencionISRField;
        private decimal retencionIEPSField;
        private decimal subTotalField;
        private decimal descuentoField;
        private decimal totalField;
        private decimal pagadoField;
        private decimal pagadoRecepcionField;

        public decimal TrasladoIVA
        {
            get
            {
                return this.trasladoIVAField;
            }
            set
            {
                this.trasladoIVAField = value;
            }
        }

        public decimal TrasladoIEPS
        {
            get
            {
                return this.trasladoIEPSField;
            }
            set
            {
                this.trasladoIEPSField = value;
            }
        }

        public decimal RetencionISR
        {
            get
            {
                return this.retencionISRField;
            }
            set
            {
                this.retencionISRField = value;
            }
        }

        public decimal RetencionIEPS
        {
            get
            {
                return this.retencionIEPSField;
            }
            set
            {
                this.retencionIEPSField = value;
            }
        }

        public decimal SubTotal
        {
            get
            {
                return this.subTotalField;
            }
            set
            {
                this.subTotalField = value;
            }
        }

        public decimal Descuento
        {
            get
            {
                return this.descuentoField;
            }
            set
            {
                this.descuentoField = value;
            }
        }

        public decimal Total
        {
            get
            {
                return this.totalField;
            }
            set
            {
                this.totalField = value;
            }
        }

        public decimal Pagado
        {
            get
            {
                return this.pagadoField;
            }
            set
            {
                this.pagadoField = value;
            }
        }

        /// <summary>
        /// importe de pagos recibidos por un comprobante de pago
        /// </summary>
        public decimal PagadoRecepcion
        {
            get
            {
                return this.pagadoRecepcionField;
            }
            set
            {
                this.pagadoRecepcionField = value;
            }
        }
    }
}
