﻿using System.ComponentModel;
using Jaeger.CFDI.Entities.Base;

namespace Jaeger.Entities.Reportes
{
    public class FacturacionDetalles : ComprobanteGeneral
    {
        private BindingList<Jaeger.Edita.V2.CFDI.Entities.ViewModelComprobanteConcepto> conceptosField;

        public FacturacionDetalles()
        {

        }

        public BindingList<Jaeger.Edita.V2.CFDI.Entities.ViewModelComprobanteConcepto> Conceptos
        {
            get
            {
                return this.conceptosField;
            }
            set
            {
                this.conceptosField = value;
            }
        }
    }
}
