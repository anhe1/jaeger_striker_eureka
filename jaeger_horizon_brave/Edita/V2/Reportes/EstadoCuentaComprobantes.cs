﻿using System;
using System.Linq;
using System.ComponentModel;

namespace Jaeger.Entities.Reportes
{
    public class EstadoCuentaComprobantes
    {
        BindingList<ComprobanteNormal> itemsField;

        public EstadoCuentaComprobantes()
        {
            this.itemsField = new BindingList<ComprobanteNormal>();
        }

        public BindingList<ComprobanteNormal> Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }

    public partial class ComprobanteNormal : Jaeger.CFDI.Entities.Base.ComprobanteGeneral
    {
        BindingList<Jaeger.CFDI.Entities.Base.ComprobanteGeneral> pagosField;

        public ComprobanteNormal()
        {
            this.pagosField = new BindingList<Jaeger.CFDI.Entities.Base.ComprobanteGeneral>();
        }

        public BindingList<Jaeger.CFDI.Entities.Base.ComprobanteGeneral> Pagos
        {
            get
            {
                return this.pagosField;
            }
            set
            {
                this.pagosField = value;
            }
        }
    }
}
