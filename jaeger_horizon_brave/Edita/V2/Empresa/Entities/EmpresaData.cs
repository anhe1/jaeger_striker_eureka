﻿using System;
using System.Linq;
using Newtonsoft.Json;

namespace Jaeger.Edita.V2.Empresa.Entities
{
    public partial class General 
    {
        public string Clave {get;set;}

        [JsonProperty("razonSocial")]
        public string RazonSocial {get;set;}

        [JsonProperty("rfc")]
        public string RFC{get;set;}

        [JsonProperty("regimenFiscal")]
        public string RegimenFiscal{get;set;}

        [JsonProperty("registroPatronal")]
        public string RegistroPatronal{get;set;}
    }
}

namespace Jaeger.Edita.V2.Empresa.Entities
{
    /// <summary>
    /// clase para serialización de la configuracion del campo DATA de la tabla CONF
    /// </summary>
    public class EmpresaData
    {
        [JsonProperty("general")]
        public General General { get; set; }

        [JsonProperty("domicilioFiscal")]
        public DomicilioFiscal DomicilioFiscal { get; set; }

        [JsonProperty("ocultarDeducciones")]
        public string[] OcultarDeducciones { get; set; }

        [JsonProperty("ocultarPercepciones")]
        public string[] OcultarPercepciones { get; set; }

        public static EmpresaData Json(string inputString)
        {
            JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.DeserializeObject<EmpresaData>(inputString, conf);
        }
    }
}
