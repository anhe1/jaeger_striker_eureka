﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Edita.V2.Empresa.Entities
{
    [JsonObject("general")]
    public class EmpresaGeneral : BasePropertyChangeImplementation
    {
        private string clave;
        private string razonSocial;
        private string rfc;
        private string regimenFiscal;
        private string registroPatronal;
        private DomicilioFiscal domicilioFiscal;

        [DisplayName("Clave")]
        public string Clave
        {
            get
            {
                return this.clave;
            }
            set
            {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Nombre ó Razon Social")]
        [JsonProperty("razonSocial")]
        public string RazonSocial
        {
            get
            {
                return this.razonSocial;
            }
            set
            {
                this.razonSocial = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Registro Federal de Causantes (RFC)")]
        [JsonProperty("rfc")]
        public string RFC
        {
            get
            {
                return this.rfc;
            }
            set
            {
                this.rfc = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Régimen Fiscal")]
        [JsonProperty("regimenFiscal")]
        public string RegimenFiscal
        {
            get
            {
                return this.regimenFiscal;
            }
            set
            {
                this.regimenFiscal = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Registro Patronal")]
        [JsonProperty("registroPatronal")]
        public string RegistroPatronal
        {
            get
            {
                return this.registroPatronal;
            }
            set
            {
                this.registroPatronal = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Domicilio Fiscal")]
        [JsonProperty("domicilioFiscal")]
        [TypeConverter(typeof(PropertiesConvert))]
        public DomicilioFiscal DomicilioFiscal
        {
            get
            {
                return this.domicilioFiscal;
            }
            set
            {
                this.domicilioFiscal = value;
                this.OnPropertyChanged();
            }
        }

        public string Json()
        {
            //JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.SerializeObject(this);
        }

        public static EmpresaGeneral Json(string inputString)
        {
            JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.DeserializeObject<EmpresaGeneral>(inputString, conf);
        }
    }
}
