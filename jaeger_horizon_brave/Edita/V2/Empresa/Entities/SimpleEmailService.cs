﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Edita.V2.Empresa.Interface;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Edita.V2.Empresa.Entities
{
    /// <summary>
    /// Amazon Simple Email Service (Amazon SES)
    /// </summary>
    public class SimpleEmailService : BasePropertyChangeImplementation, ISimpleEmailService
    {
        private bool enabled;
        private string user;
        private string password;
        private string host;
        private string port;
        private string emial;
        private bool ssl;

        public SimpleEmailService()
        {
        }

        [Category("SES")]
        [Description("")]
        [DisplayName("Habilitado")]
        [JsonProperty("enabled")]
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                this.enabled = value;
                this.OnPropertyChanged();
            }
        }

        [Category("SES")]
        [Description("")]
        [DisplayName("Host")]
        [JsonProperty("host")]
        public string Host
        {
            get
            {
                return this.host;
            }
            set
            {
                this.host = value;
                this.OnPropertyChanged();
            }
        }

        [Category("SES")]
        [Description("")]
        [DisplayName("Port")]
        [JsonProperty("port")]
        public string Port
        {
            get
            {
                return this.port;
            }
            set
            {
                this.port = value;
                this.OnPropertyChanged();
            }
        }

        [Category("SES")]
        [Description("")]
        [DisplayName("SSL")]
        [JsonProperty("ssl")]
        public bool SSL
        {
            get
            {
                return this.ssl;
            }
            set
            {
                this.ssl = value;
                this.OnPropertyChanged();
            }
        }

        [Category("SES")]
        [Description("")]
        [DisplayName("Correo")]
        [JsonProperty("email")]
        public string Email
        {
            get
            {
                return this.emial;
            }
            set
            {
                this.emial = value;
                this.OnPropertyChanged();
            }
        }

        [Category("SES")]
        [Description("")]
        [DisplayName("Usuario")]
        [JsonProperty("user")]
        public string User
        {
            get
            {
                return this.user;
            }
            set
            {
                this.user = value;
                this.OnPropertyChanged();
            }
        }

        [Category("SES")]
        [Description("")]
        [DisplayName("Contraseña")]
        [JsonProperty("pass")]
        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
                this.OnPropertyChanged();
            }
        }
    }
}
