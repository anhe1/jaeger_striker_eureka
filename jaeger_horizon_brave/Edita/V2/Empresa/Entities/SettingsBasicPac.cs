﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.Edita.V2.Empresa.Entities
{
    public class SettingsBasicPac : BasePropertyChangeImplementation
    {
        private IServiceProvider settingsField;

        private string cerBase64Field;

        private string keyBase64Field;

        private string passKeyField;

        private Jaeger.Entities.Property errorsField;

        public string CerBase64
        {
            get
            {
                return this.cerBase64Field;
            }
            set
            {
                this.cerBase64Field = value;
            }
        }

        public Jaeger.Entities.Property Errors
        {
            get
            {
                return this.errorsField;
            }
            set
            {
                this.errorsField = value;
            }
        }

        public string KeyBase64
        {
            get
            {
                return this.keyBase64Field;
            }
            set
            {
                this.keyBase64Field = value;
            }
        }

        public string PassKey
        {
            get
            {
                return this.passKeyField;
            }
            set
            {
                this.passKeyField = value;
            }
        }

        public IServiceProvider Settings
        {
            get
            {
                return this.settingsField;
            }
            set
            {
                this.settingsField = value;
            }
        }

        public SettingsBasicPac()
        {
            this.settingsField = new ServiceProvider();
            this.cerBase64Field = string.Empty;
            this.keyBase64Field = string.Empty;
            this.passKeyField = string.Empty;
            this.errorsField = new Jaeger.Entities.Property();
        }
    }
}