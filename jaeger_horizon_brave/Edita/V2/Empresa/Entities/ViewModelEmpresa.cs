/// develop: anhe 190620191651
/// purpose: modelo para la tabla de empresas, de la base de datos de empresas
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.Empresa.Entities
{
    [SugarTable("empr")]
    public class ViewModelEmpresa : BasePropertyChangeImplementation
    {
        private int indiceField;
        private bool activoField;
        private int regimenFiscalField;
        private string claveField;
        private string creoField;
        private string modificaField;
        private string rfcField;
        private string dominioField;
        private string correoField;
        private string nombreField;
        private string configuracionField;
        private DateTime fechaNuevoField;
        private DateTime fechaModificaField;

        /// <summary>
        /// indice principal de la tabla
        /// </summary>
        [DataNames("empr_id")]
        [SugarColumn(ColumnName = "empr_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// registro activo
        /// </summary>
        [DataNames("empr_a")]
        [SugarColumn(ColumnName = "empr_a", ColumnDescription = "registro activo")]
        public bool Activo
        {
            get
            {
                return this.activoField;
            }
            set
            {
                this.activoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_rgf_id")]
        [SugarColumn(ColumnName = "empr_rgf_id", ColumnDescription = "id del regimen fiscal")]
        public int RegimenFiscal
        {
            get
            {
                return this.regimenFiscalField;
            }
            set
            {
                this.regimenFiscalField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_clv")]
        [SugarColumn(ColumnName = "empr_clv", ColumnDescription = "clave de la empresa")]
        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_usr_n")]
        [SugarColumn(ColumnName = "empr_usr_n", ColumnDescription = "clave del usuario que crea el registro")]
        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_usr_m")]
        [SugarColumn(ColumnName = "empr_usr_m", ColumnDescription = "clave del ultimo usuario que modifico el registro")]
        public string Modifica
        {
            get
            {
                return this.modificaField;
            }
            set
            {
                this.modificaField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_rfc")]
        [SugarColumn(ColumnName = "empr_rfc", ColumnDescription = "clave del registro federal de contribuyentes")]
        public string RFC
        {
            get
            {
                return this.rfcField;
            }
            set
            {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_dom")]
        [SugarColumn(ColumnName = "empr_dom")]
        public string Dominio
        {
            get
            {
                return this.dominioField;
            }
            set
            {
                this.dominioField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_mail")]
        [SugarColumn(ColumnName = "empr_mail")]
        public string Correo
        {
            get
            {
                return this.correoField;
            }
            set
            {
                this.correoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_nom")]
        [SugarColumn(ColumnName = "empr_nom")]
        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_conf")]
        [SugarColumn(ColumnName = "empr_conf", ColumnDescription = "configuracion general de la empresa.")]
        public string Configuracion
        {
            get
            {
                return this.configuracionField;
            }
            set
            {
                this.configuracionField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_fn")]
        [SugarColumn(ColumnName = "empr_fn")]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_fm")]
        [SugarColumn(ColumnName = "empr_fm")]
        public DateTime FechaModifica
        {
            get
            {
                return this.fechaModificaField;
            }
            set
            {
                this.fechaModificaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public EditaV2Synapsis Synapsis
        {
            get
            {
                return EditaV2Synapsis.Json(this.Configuracion);
            }
        }
    }
}