﻿using System;
using Newtonsoft.Json;
using Jaeger.Edita.V2.Directorio.Interfaces;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Edita.V2.Empresa.Entities
{
    [JsonObject("domicilioFiscal")]
    public class DomicilioFiscal : BasePropertyChangeImplementation, IDomicilioFiscal
    {
        private string calle;
        private string noExterior;
        private string noInterior;
        private string colonia;
        private string delegacion;
        private string ciudad;
        private string estado;
        private string pais;
        private string codigoPais;
        private string codigoPostal;
        private string localidad;
        private string referencia;

        [JsonProperty("_drccn_cll")]
        public string Calle
        {
            get
            {
                return this.calle;
            }
            set
            {
                this.calle = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_cdd")]
        public string Ciudad
        {
            get
            {
                return this.ciudad;
            }
            set
            {
                this.ciudad = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_cpa")]
        public string CodigoDePais
        {
            get
            {
                return this.codigoPais;
            }
            set
            {
                this.codigoPais = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_cp")]
        public string CodigoPostal
        {
            get
            {
                return this.codigoPostal;
            }
            set
            {
                this.codigoPostal = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_cln")]
        public string Colonia
        {
            get
            {
                return this.colonia;
            }
            set
            {
                this.colonia = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_dlg")]
        public string Delegacion
        {
            get
            {
                return this.delegacion;
            }
            set
            {
                this.delegacion = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_std")]
        public string Estado
        {
            get
            {
                return this.estado;
            }
            set
            {
                this.estado = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_lcldd")]
        public string Localidad
        {
            get
            {
                return this.localidad;
            }
            set
            {
                this.localidad = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_extr")]
        public string NoExterior
        {
            get
            {
                return this.noExterior;
            }
            set
            {
                this.noExterior = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_intr")]
        public string NoInterior
        {
            get
            {
                return this.noInterior;
            }
            set
            {
                this.noInterior = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_ps")]
        public string Pais
        {
            get
            {
                return this.pais;
            }
            set
            {
                this.pais = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("_drccn_rfrnc")]
        public string Referencia
        {
            get
            {
                return this.referencia;
            }
            set
            {
                this.referencia = value;
                this.OnPropertyChanged();
            }
        }

        public override string ToString()
        {
            string direccion = string.Empty;

            if (!(String.IsNullOrEmpty(this.Calle)))
            {
                direccion = string.Concat("Calle ", this.Calle);
            }

            if (!(String.IsNullOrEmpty(this.NoExterior)))
            {
                direccion = string.Concat(direccion, " No. Ext. ", this.NoExterior);
            }

            if (!(String.IsNullOrEmpty(this.NoInterior)))
            {
                direccion = string.Concat(direccion, " No. Int. ", this.NoInterior);
            }

            if (!(String.IsNullOrEmpty(this.Colonia)))
            {
                direccion = string.Concat(direccion, " Col. ", this.Colonia);
            }

            if (!(String.IsNullOrEmpty(this.CodigoPostal)))
            {
                direccion = string.Concat(direccion, " C.P. ", this.CodigoPostal);
            }

            if (!(String.IsNullOrEmpty(this.Delegacion)))
            {
                direccion = string.Concat(direccion, " ", this.Delegacion);
            }

            if (!(String.IsNullOrEmpty(this.Estado)))
            {
                direccion = string.Concat(direccion, ", ", this.Estado);
            }

            if (!(String.IsNullOrEmpty(this.Referencia)))
            {
                direccion = string.Concat(direccion, ", Ref: ", this.Referencia);
            }

            if (!(String.IsNullOrEmpty(this.Localidad)))
            {
                direccion = string.Concat(direccion, ", Loc: ", this.Localidad);
            }
            return direccion;
        }
    }
}
