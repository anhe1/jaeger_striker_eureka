﻿/// develop: ANHE1 060920191748
/// purpose: 
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.Edita.V2.Empresa.Entities
{
    /// <summary>
    /// clase para serialización de la configuracion de la tabla empresas, campo empr_conf
    /// </summary>
    public class EditaV2Synapsis : BasePropertyChangeImplementation
    {
        private BaseDatos baseDatos;
        private Amazon amazon;
        private ProveedorAutorizado proveedorAutorizado;

        public EditaV2Synapsis()
        {
        }

        [JsonProperty("dbs")]
        public BaseDatos BaseDatos
        {
            get
            {
                return this.baseDatos;
            }
            set
            {
                this.baseDatos = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("amazon")]
        public Amazon Amazon
        {
            get
            {
                return this.amazon;
            }
            set
            {
                this.amazon = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("pac")]
        public ProveedorAutorizado PAC
        {
            get
            {
                return this.proveedorAutorizado;
            }
            set
            {
                this.proveedorAutorizado = value;
                this.OnPropertyChanged();
            }
        }

        public static EditaV2Synapsis Json(string inputString)
        {
            return JsonConvert.DeserializeObject<EditaV2Synapsis>(inputString);
        }
    }
}
