﻿using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Edita.V2.Empresa.Entities
{
    [SugarTable("_conf")]
    public class ViewModelEmpresaConfiguracion : BasePropertyChangeImplementation
    {
        private int indiceField;
        private string key;
        private string dataField;

        [DataNames("_conf_id")]
        [SugarColumn(ColumnName = "_conf_id", IsPrimaryKey = true, IsIdentity = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_conf_key")]
        [SugarColumn(ColumnName = "_conf_key", Length = 20)]
        public string Key
        {
            get
            {
                return key;
            }
            set
            {
                this.key = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_conf_data")]
        [SugarColumn(ColumnName = "_conf_data", IsNullable = true)]
        public string Data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
