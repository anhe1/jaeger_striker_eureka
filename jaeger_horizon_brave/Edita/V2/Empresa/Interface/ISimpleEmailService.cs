﻿namespace Jaeger.Edita.V2.Empresa.Interface
{
    public interface ISimpleEmailService
    {
        string Email { get; set; }
        bool Enabled { get; set; }
        string Host { get; set; }
        string Password { get; set; }
        string Port { get; set; }
        bool SSL { get; set; }
        string User { get; set; }
    }
}