﻿namespace Jaeger.Edita.V2.Empresa.Interface
{
    public interface ISimpleStorageService
    {
        string AccessKeyId { get; set; }
        bool AllowToStore { get; set; }
        string BucketName { get; set; }
        string Folder { get; set; }
        string Region { get; set; }
        string SecretAccessKey { get; set; }
        bool SendAutomatically { get; set; }
    }
}