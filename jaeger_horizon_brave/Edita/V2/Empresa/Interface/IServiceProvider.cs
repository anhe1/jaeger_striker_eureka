﻿using Jaeger.Edita.V2.Empresa.Entities;

namespace Jaeger.Edita.V2.Empresa.Interface
{
    public interface IServiceProvider
    {
        string Pass { get; set; }
        bool Production { get; set; }
        Domain.Empresa.Entities.ServiceProvider.EnumServicePAC Provider { get; set; }
        string RFC { get; set; }
        bool Strinct { get; set; }
        string User { get; set; }
    }
}