﻿using System;
using System.Linq;

namespace Jaeger.Entities
{
    public class Manifiesto : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string rfcField;
        private string razonSocialField;
        private string nombreComercialField;
        private string correoField;
        private Jaeger.Edita.V2.Directorio.Entities.DomicilioFiscal domicilioField;

        public Manifiesto()
        {
            this.domicilioField = new Jaeger.Edita.V2.Directorio.Entities.DomicilioFiscal();
        }

        public string RFC
        {
            get
            {
                return this.rfcField;
            }
            set
            {
                this.rfcField = value.Trim().ToUpper();
                this.OnPropertyChanged();
            }
        }

        public string RazonSocial
        {
            get
            {
                return this.razonSocialField;
            }
            set
            {
                this.razonSocialField = value;
                this.OnPropertyChanged();
            }
        }

        public string NombreComercial
        {
            get
            {
                return this.nombreComercialField;
            }
            set
            {
                this.nombreComercialField = value;
                this.OnPropertyChanged();
            }
        }

        public string Correo
        {
            get
            {
                return this.correoField;
            }
            set
            {
                this.correoField = value;
                this.OnPropertyChanged();
            }
        }

        public Jaeger.Edita.V2.Directorio.Entities.DomicilioFiscal Domicilio
        {
            get
            {
                return this.domicilioField;
            }
            set
            {
                this.domicilioField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
