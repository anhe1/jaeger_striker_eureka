﻿using System;
using System.Linq;

namespace Jaeger.Entities
{
    public class TimbresDisponibles
    {
        private string mensajeField;
        private string statusField;
        private string contratadosField;
        private string disponiblesField;
        private string usadosField;

        public string Mensaje
        {
            get
            {
                return this.mensajeField;
            }
            set
            {
                this.mensajeField = value;
            }
        }

        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        public string Contratados
        {
            get
            {
                return this.contratadosField;
            }
            set
            {
                this.contratadosField = value;
            }
        }

        public string Disponibles
        {
            get
            {
                return this.disponiblesField;
            }
            set
            {
                this.disponiblesField = value;
            }
        }

        public string Usados
        {
            get
            {
                return this.usadosField;
            }
            set
            {
                this.usadosField = value;
            }
        }
    }
}
