﻿/// esta clase es para contener la información de la busqueda de un comprobante fiscal a traves del servicio de utilerias de Solución Factible
using System;
using System.Linq;

namespace Jaeger.Entities
{
    public class BuscarResponse : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string folioField;
        private string serieField;
        private string uuidField;
        private string emisorNombreField;
        private string emisorRfcField;
        private string receptorNombreField;
        private string receptorRfcField;
        private bool canceladoField;
        private DateTime? fechaCancelacionField;
        private DateTime? fechaEmisionField;
        private DateTime? fechaTimbradoField;
        private string selloDigitalField;
        private string selloSatField;
        private decimal totalField;

        public BuscarResponse()
        {
        }

        public string Folio
        {
            get
            {
                return this.folioField;
            }
            set
            {
                this.folioField = value;
                this.OnPropertyChanged();
            }
        }

        public string Serie
        {
            get
            {
                return this.serieField;
            }
            set
            {
                this.serieField = value;
                this.OnPropertyChanged();
            }
        }

        public string Uuid
        {
            get
            {
                return this.uuidField;
            }
            set
            {
                this.uuidField = value;
                this.OnPropertyChanged();
            }
        }

        public string EmisorNombre
        {
            get
            {
                return this.emisorNombreField;
            }
            set
            {
                this.emisorNombreField = value;
                this.OnPropertyChanged();
            }
        }

        public string EmisorRfc
        {
            get
            {
                return this.emisorRfcField;
            }
            set
            {
                this.emisorRfcField = value;
                this.OnPropertyChanged();
            }
        }

        public string ReceptorNombre
        {
            get
            {
                return this.receptorNombreField;
            }
            set
            {
                this.receptorNombreField = value;
                this.OnPropertyChanged();
            }
        }

        public string ReceptorRfc
        {
            get
            {
                return this.receptorRfcField;
            }
            set
            {
                this.receptorRfcField = value;
                this.OnPropertyChanged();
            }
        }

        public bool Cancelado
        {
            get
            {
                return this.canceladoField;
            }
            set
            {
                this.canceladoField = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime? FechaCancelacion
        {
            get
            {
                return this.fechaCancelacionField;
            }
            set
            {
                this.fechaCancelacionField = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime? FechaEmision
        {
            get
            {
                return this.fechaEmisionField;
            }
            set
            {
                this.fechaEmisionField = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime? FechaTimbrado
        {
            get
            {
                return this.fechaTimbradoField;
            }
            set
            {
                this.fechaTimbradoField = value;
                this.OnPropertyChanged();
            }
        }

        public string SelloDigital
        {
            get
            {
                return this.selloDigitalField;
            }
            set
            {
                this.selloDigitalField = value;
                this.OnPropertyChanged();
            }
        }

        public string SelloSat
        {
            get
            {
                return this.selloSatField;
            }
            set
            {
                this.selloSatField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Total
        {
            get
            {
                return this.totalField;
            }
            set
            {
                this.totalField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
