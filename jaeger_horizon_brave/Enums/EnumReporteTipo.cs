﻿using System;

namespace Jaeger.Enums
{
    public enum EnumReporteTipo
    {
        None,
        Accuse,
        Cfdiv32,
        Cfdiv33,
        Validation,
        Nomina11,
        Nomina12,
        ReciboDeCobro,
        ReciboDePago,
        ReciboDeComision,
        Remision,
        ExpedienteEmpleado,
        PreNomina,
        ValeEntrada,
        ValeSalida,
        OrdenCompra,
        EstadoCuenta
    }
}