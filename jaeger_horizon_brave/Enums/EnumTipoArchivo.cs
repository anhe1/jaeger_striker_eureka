﻿using System;
using System.Linq;

namespace Jaeger.Enums
{
    public enum EnumTipoArchivo
    {
        Xml,
        Pdf,
        Acuse
    }
}
