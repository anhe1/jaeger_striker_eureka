﻿using System;
using System.Linq;

namespace Jaeger.Enums
{
    public enum EnumEmailStatus
    {
        OnHold,
        Read,
        Unread,
        Ready,
        Enviado,
        Error
    }
}
