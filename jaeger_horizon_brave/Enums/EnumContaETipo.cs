﻿using System;
using System.Linq;
using System.Xml.Serialization;

namespace Jaeger.Enums
{
    public enum EnumContaETipo
    {
        [XmlEnumAttribute("PL")] 
        PolizasDelPeriodo,
        [XmlEnumAttribute("CT")] 
        CatalogoDeCuentas,
        [XmlEnumAttribute("BN")] 
        BalanzaDeComprobacionNormal,
        [XmlEnumAttribute("BC")] 
        BalanzaDeComprobacion,
        [XmlEnumAttribute("XF")] 
        AuxiliarDeFolios,
        [XmlEnumAttribute("XC")] 
        AuxiliarDeCuentas
    }
}
