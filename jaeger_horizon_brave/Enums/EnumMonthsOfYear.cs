﻿using System;
using System.ComponentModel;

namespace Jaeger.Edita.Enums
{
    public enum EnumMonthsOfYear
    {
        [Description("Todos")]
        None,
        Enero,
        Febrero,
        Marzo,
        Abril,
        Mayo,
        Junio,
        Julio,
        Agosto,
        Septiembre,
        Octubre,
        Noviembre,
        Diciembre
    }
}