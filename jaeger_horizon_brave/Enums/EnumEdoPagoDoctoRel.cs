﻿using System;

namespace Jaeger.Enums
{
    public enum EnumEdoPagoDoctoRel
    {
        SinComprobar,
        Relacionado,
        NoEncontrado
    }
}