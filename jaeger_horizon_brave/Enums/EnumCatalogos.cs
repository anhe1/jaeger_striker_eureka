﻿using System;

namespace Jaeger.Enums
{
    public enum EnumCatalogos
    {
        Aduanas,
        Bancos,
        ClaveUnidad,
        CodigoAgrupador,
        CodigoPostal,
        Estados,
        FormaPago32,
        FormaPago33,
        MetodoPago32,
        MetodoPago33,
        Moneda,
        NominaOrigenRecurso,
        NominaPeriodicidadPago,
        NominaRiesgoPuesto,
        NominaTipoContrato,
        NominaTipoDeduccion,
        NominaTipoHoras,
        NominaTipoIncapacidad,
        NominaTipoJornada,
        NominaTipoOtroPago,
        NominaTipoPercepcion,
        NominaTipoRegimen,
        Pais,
        ProdServ,
        RegimenFiscal,
        TipoFactor,
        UsoCfdi
    }
}