﻿using System;

namespace Jaeger.Enums
{
    public enum EnumPropertyType
    {
        None,
        Error,
        Warning,
        Attention,
        Information,
        Completed
    }
}