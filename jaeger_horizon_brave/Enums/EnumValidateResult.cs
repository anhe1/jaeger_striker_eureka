﻿using System;

namespace Jaeger.Enums
{
    public enum EnumValidateResult
    {
        Error,
        EnEspera,
        Valido,
        NoValido
    }
}