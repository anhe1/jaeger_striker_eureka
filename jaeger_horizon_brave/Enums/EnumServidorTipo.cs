﻿using System;
using System.Linq;

namespace Jaeger.Enums
{
    public enum EnumServidorTipo
    {
        Default = 0,
        Embedded = 1,
    }
}
