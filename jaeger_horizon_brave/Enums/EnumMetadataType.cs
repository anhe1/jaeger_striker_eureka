﻿using System;

namespace Jaeger.Enums
{
    public enum EnumMetadataType
    {
        NA,
        CFD,
        CFDI,
        Certificate,
        Metadata,
        Acuse,
        TimbreFiscal,
        ValidationResult,
        Other,
        Polizas,
        Balanza,
        CatalogoCuentas,
        Retenciones,
        AuxiliarFolios,
        AuxiliarCtas,
        DPIVA,
        Remision,
        Devolucion,
        NotaDescuento
    }
}