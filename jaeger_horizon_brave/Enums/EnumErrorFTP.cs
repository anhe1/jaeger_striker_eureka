﻿using System;
using System.Linq;

namespace Jaeger.Enums
{
    public enum EnumErrorFTP
    {
        NotError,
        NotNetworkAvailable,
        NotFtpAvailable,
        FileNotFound,
        Unknown
    }
}
