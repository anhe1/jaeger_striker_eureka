namespace Jaeger.Enums
{
    public enum EnumImpuesto
    {
        ISR = 1,
        IVA = 2,
        IEPS = 3
    }
}