﻿using Jaeger.Edita.Entities.Amazon;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Jaeger.Entities.Amazon
{
    public class BucketFiles
    {
        private string strVersion;

        private string strJaeger;

        private string strBucketName;

        private string strRfcId;

        private DateTime dateCreation;

        private List<ViewModelBucketContent> listContents;

        [XmlAttribute]
        public string BucketName
        {
            get
            {
                return this.strBucketName;
            }
            set
            {
                this.strBucketName = value;
            }
        }

        [XmlElement]
        public List<ViewModelBucketContent> Contents
        {
            get
            {
                return this.listContents;
            }
            set
            {
                this.listContents = value;
            }
        }

        [XmlAttribute]
        public DateTime Creation
        {
            get
            {
                return this.dateCreation;
            }
            set
            {
                this.dateCreation = value;
            }
        }

        [XmlAttribute]
        public string Jaeger
        {
            get
            {
                return this.strJaeger;
            }
            set
            {
                this.strJaeger = value;
            }
        }

        [XmlAttribute]
        public string RfcId
        {
            get
            {
                return this.strRfcId;
            }
            set
            {
                this.strRfcId = value;
            }
        }

        [XmlAttribute("version")]
        public string Version
        {
            get
            {
                return this.strVersion;
            }
            set
            {
                this.strVersion = value;
            }
        }

        public BucketFiles()
        {
            this.strVersion = "1.2";
            this.strJaeger = "gipsy";
            this.strBucketName = string.Empty;
            this.strRfcId = string.Empty;
            this.dateCreation = DateTime.Now;
            this.listContents = new List<ViewModelBucketContent>();
        }
    }
}