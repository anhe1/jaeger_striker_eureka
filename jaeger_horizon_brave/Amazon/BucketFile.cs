﻿using System;
using System.Xml.Serialization;

namespace Jaeger.Entities.Amazon
{
    public class BucketFile : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private string strRfc;
        private string strKey;
        private string strFileName;
        private string strKeyName;
        private string strFileExt;
        private string strContentType;
        private string strDescription;
        private string strUrl;
        private string strUuid;
        private string strBucketName;
        private bool blnPublic;
        private bool blnCompleted;
        private long lngFileSize;
        private long lngclassification;

        [XmlAttribute]
        public string BucketName
        {
            get
            {
                return this.strBucketName;
            }
            set
            {
                this.strBucketName = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public long Category
        {
            get
            {
                return this.lngclassification;
            }
            set
            {
                this.lngclassification = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public bool Completed
        {
            get
            {
                return this.blnCompleted;
            }
            set
            {
                this.blnCompleted = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string ContentType
        {
            get
            {
                return this.strContentType;
            }
            set
            {
                this.strContentType = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string Description
        {
            get
            {
                return this.strDescription;
            }
            set
            {
                this.strDescription = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string FileExt
        {
            get
            {
                return this.strFileExt;
            }
            set
            {
                this.strFileExt = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string FileName
        {
            get
            {
                return this.strFileName;
            }
            set
            {
                this.strFileName = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public long FileSize
        {
            get
            {
                return this.lngFileSize;
            }
            set
            {
                this.lngFileSize = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public bool IsPublic
        {
            get
            {
                return this.blnPublic;
            }
            set
            {
                this.blnPublic = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string Key
        {
            get
            {
                return this.strKey;
            }
            set
            {
                this.strKey = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string KeyName
        {
            get
            {
                return this.strKeyName;
            }
            set
            {
                this.strKeyName = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string Rfc
        {
            get
            {
                return this.strRfc;
            }
            set
            {
                this.strRfc = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string Url
        {
            get
            {
                return this.strUrl;
            }
            set
            {
                this.strUrl = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string Uuid
        {
            get
            {
                return this.strUuid;
            }
            set
            {
                this.strUuid = value;
                this.OnPropertyChanged();
            }
        }
    }
}