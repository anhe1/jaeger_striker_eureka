﻿using System;
using System.Linq;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;

namespace Jaeger.Edita.Entities.Amazon
{
    [SugarTable("_bckt", "inventario de archivos en bucket de archivos de comprobantes")]
    public class ViewModelBucketContent
    {
        private int indiceField;
        private int subIndiceField;
        private bool isActiveField = true;
        private int lngFileSize;
        private int lngclassification;
        private bool blnPublic;
        private bool blnCompleted;
        private string strKey;
        private string strFileName;
        private string strKeyName;
        private string strFileExt;
        private string strContentType;
        private string strDescription;
        private string strUrl;
        private string strError;
        private string strUuid;
        private string strBucketName;
        private string strNotes;

        public ViewModelBucketContent()
        {
            this.FechaNuevo = DateTime.Now;
        }

        [DataNames("_bckt_id")]
        [SugarColumn(ColumnName = "_bckt_id", IsIdentity = true, IsPrimaryKey = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
            }
        }

        [DataNames("_bckt_a")]
        [SugarColumn(ColumnName = "_bckt_a")]
        public bool IsActive
        {
            get
            {
                return this.isActiveField;
            }
            set
            {
                this.isActiveField = value;
            }
        }

        [DataNames("_bckt_rel_id")]
        [SugarColumn(ColumnName = "_bckt_rel_id")]
        public int SubId
        {
            get
            {
                return this.subIndiceField;
            }
            set
            {
                this.subIndiceField = value;
            }
        }

        [DataNames("_bckt_bucket")]
        [SugarColumn(ColumnName = "_bckt_bucket")]
        public string BucketName
        {
            get
            {
                return this.strBucketName;
            }
            set
            {
                this.strBucketName = value;
            }
        }

        [DataNames("_bckt_doc")]
        [SugarColumn(ColumnName = "_bckt_doc")]
        public int Category
        {
            get
            {
                return this.lngclassification;
            }
            set
            {
                this.lngclassification = value;
            }
        }

        [DataNames("_bckt_type")]
        [SugarColumn(ColumnName = "_bckt_type")]
        public string ContentType
        {
            get
            {
                return this.strContentType;
            }
            set
            {
                this.strContentType = value;
            }
        }

        [DataNames("_bckt_nom")]
        [SugarColumn(ColumnName = "_bckt_nom")]
        public string Description
        {
            get
            {
                return this.strDescription;
            }
            set
            {
                this.strDescription = value;
            }
        }

        [DataNames("_bckt_ext")]
        [SugarColumn(ColumnName = "_bckt_ext")]
        public string FileExt
        {
            get
            {
                return this.strFileExt;
            }
            set
            {
                this.strFileExt = value;
            }
        }

        [DataNames("_bckt_file")]
        [SugarColumn(ColumnName = "_bckt_file")]
        public string FileName
        {
            get
            {
                return this.strFileName;
            }
            set
            {
                if (this.strFileName != value)
                {
                    this.strFileName = value;
                }
            }
        }

        [DataNames("_bckt_size")]
        [SugarColumn(ColumnName = "_bckt_size")]
        public int FileSize
        {
            get
            {
                return this.lngFileSize;
            }
            set
            {
                this.lngFileSize = value;
            }
        }

        [DataNames("_bckt_keyname")]
        [SugarColumn(ColumnName = "_bckt_keyname")]
        public string KeyName
        {
            get
            {
                return this.strKeyName;
            }
            set
            {
                this.strKeyName = value;
            }
        }

        [DataNames("_bckt_url")]
        [SugarColumn(ColumnName = "_bckt_url")]
        public string Url
        {
            get
            {
                return this.strUrl;
            }
            set
            {
                this.strUrl = value;
            }
        }

        [DataNames("_bckt_uuid")]
        [SugarColumn(ColumnName = "_bckt_uuid")]
        public string Uuid
        {
            get
            {
                return this.strUuid;
            }
            set
            {
                this.strUuid = value;
            }
        }

        [DataNames("_bckt_usr_n")]
        [SugarColumn(ColumnName = "_bckt_usr_n")]
        public string Creo { get; set; }

        [DataNames("_bckt_fn")]
        [SugarColumn(ColumnName = "_bckt_fn")]
        public DateTime FechaNuevo { get; set; }

        [SugarColumn(IsIgnore = true)]
        public string Error
        {
            get
            {
                return this.strError;
            }
            set
            {
                this.strError = value;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string Notes
        {
            get
            {
                return this.strNotes;
            }
            set
            {
                this.strNotes = value;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public bool Completed
        {
            get
            {
                return this.blnCompleted;
            }
            set
            {
                this.blnCompleted = value;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public bool IsPublic
        {
            get
            {
                return this.blnPublic;
            }
            set
            {
                this.blnPublic = value;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string Key
        {
            get
            {
                return this.strKey;
            }
            set
            {
                this.strKey = value;
            }
        }
    }
}
