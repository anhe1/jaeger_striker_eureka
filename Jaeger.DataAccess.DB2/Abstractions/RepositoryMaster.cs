﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using IBM.Data.DB2.iSeries;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.Abstractions {
    public abstract class RepositoryMaster<T> : Repository, IRepositoryMaster where T : class, new() {
        /// <summary>
        /// lista de parametros al pasar comando sql en modo texto
        /// </summary>
        protected List<iDB2Parameter> parameters;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public RepositoryMaster(DataBaseConfiguracion configuracion) : base(configuracion) {

        }

        protected bool ConnectionTesting() {
            try {
                var connection = this.GetConnection();
                connection.Open();
                return connection.State == ConnectionState.Open;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        protected int ExecuteNoQuery(iDB2Command sqlCommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                sqlCommand.Connection = connection;
                return sqlCommand.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// ejecutar sentencia SQL en modo texto, es necesario incluir los parametros en propiedad "parametros"
        /// </summary>
        /// <param name="sqlcommand"></param>
        /// <returns></returns>
        protected int ExecuteScalar(string sqlcommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                using (var comando = new iDB2Command()) {
                    comando.Connection = connection;
                    comando.CommandType = CommandType.Text;
                    comando.CommandText = sqlcommand;
                    comando.Parameters.AddRange(this.parameters.ToArray());
                    int indice = 0;
                    try {
                        indice = Convert.ToInt32(comando.ExecuteScalar());
                    } catch (Exception ex) {
                        var fbex = ex as iDB2Exception;
                        if (fbex != null)
                            Console.WriteLine("iDB2Exception: " + fbex.Message);
                        else
                            Console.WriteLine("Exception: " + ex.Message);
                        indice = 0;
                        Services.LogErrorService.LogWrite("iDB2Exception: " + ex.Message);
                    }
                    return indice;
                }
            }
        }

        /// <summary>
        /// ejecutar sentencia SQL en modo texto, es necesario incluir los parametros en propiedad "parametros"
        /// </summary>
        /// <param name="sqlcommand"></param>
        /// <returns></returns>
        protected int ExecuteScalar(iDB2Command command) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                command.Connection = connection;
                command.CommandType = CommandType.Text;
                int indice = 0;
                try {
                    indice = Convert.ToInt32(command.ExecuteScalar());
                } catch (Exception ex) {
                    var fbex = ex as iDB2Exception;
                    if (fbex != null)
                        Console.WriteLine("iDB2Exception: " + fbex.Message);
                    else
                        Console.WriteLine("Exception: " + ex.Message);
                    indice = 0;
                    Services.LogErrorService.LogWrite("iDB2Exception: " + ex.Message);
                }
                return indice;
            }
        }

        /// <summary>
        /// ejecutar comando FbCommand, este debe incluir los parametros
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        protected DataTable ExecuteReader(iDB2Command sqlCommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                sqlCommand.Connection = connection;

                var reader = sqlCommand.ExecuteReader();
                using (var table = new DataTable()) {
                    table.Load(reader);
                    reader.Dispose();
                    return table;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        protected DataTable ExecuteReader(string sqlCommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                using (var comando = new iDB2Command()) {
                    comando.Connection = connection;
                    comando.CommandType = CommandType.Text;
                    comando.CommandText = sqlCommand;
                    if (this.parameters != null)
                        comando.Parameters.AddRange(this.parameters.ToArray());
                    return this.ExecuteReader(comando);
                }
            }
        }

        /// <summary>
        /// ejecutar transaccion 
        /// </summary>
        /// <param name="sqlCommand">objeto FbCommand</param>
        /// <returns></returns>
        protected int ExecuteTransaction(iDB2Command sqlCommand) {
            int response = 0;
            using (var connection = this.GetConnection()) {
                connection.Open();
                using (var transaction = connection.BeginTransaction()) {
                    sqlCommand.Connection = connection;
                    sqlCommand.Transaction = transaction;
                    response = sqlCommand.ExecuteNonQuery();
                    try {
                        transaction.Commit();
                    } catch (Exception ex) {
                        iDB2Exception fbex = ex as iDB2Exception;
                        if (fbex != null)
                            Services.LogErrorService.LogWrite("iDB2Exception: " + ex.Message);
                            Console.WriteLine("iDB2Exception: " + fbex.Message);
                        response = 0;
                    }
                }
            }
            return response;
        }

        protected int GetIndex(string fieldName, string nameTable) {
            int lNewIndex = 0;
            try {
                lNewIndex = this.ExecuteScalar(new iDB2Command { CommandText = string.Concat("select max(", fieldName, ") from ", nameTable) });
            } catch (Exception ex) {
                Services.LogErrorService.LogWrite("iDB2Exception: " + ex.Message);
                Console.WriteLine(ex.Message);
                lNewIndex = -1;
            }
            return lNewIndex + 1;
        }

        protected IEnumerable<T> GetMapper(iDB2Command sqlcommand) {
            var tabla = this.ExecuteReader(sqlcommand);
            var mapper = new DataNamesMapper<T>();
            return mapper.Map(tabla).ToList();
        }

        protected IEnumerable<T1> GetMapper<T1>(DataTable sqlcommand) where T1 : class, new() {
            var tabla = sqlcommand;
            var mapper = new DataNamesMapper<T1>();
            return mapper.Map(tabla).ToList();
        }

        protected IEnumerable<T1> GetMapper<T1>(string sqlcommand) where T1 : class, new() {
            var tabla = this.ExecuteReader(sqlcommand);
            var mapper = new DataNamesMapper<T1>();
            return mapper.Map(tabla).ToList();
        }

        protected IEnumerable<T1> GetMapper<T1>(iDB2Command sqlcommand) where T1 : class, new() {
            var tabla = this.ExecuteReader(sqlcommand);
            var mapper = new DataNamesMapper<T1>();
            return mapper.Map(tabla).ToList();
        }
    }
}
