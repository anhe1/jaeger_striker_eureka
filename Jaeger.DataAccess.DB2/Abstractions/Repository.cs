﻿using System;
using IBM.Data.DB2.iSeries;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Abstractions {
    /// <summary>
    /// clase abstracta para del repositorio
    /// </summary>
    public abstract class Repository {
        private readonly string connectionString;
        private DataBaseConfiguracion configuracion;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public Repository(DataBaseConfiguracion configuracion) {
            this.configuracion = configuracion;
            this.connectionString = this.StringBuilder(this.configuracion).ConnectionString;
        }

        public string Message { get; set; }

        /// <summary>
        /// obtener conexion valida a la base de datos
        /// </summary>
        /// <returns>retorna un objeto FbConnection</returns>
        protected iDB2Connection GetConnection() {
            return new iDB2Connection(this.connectionString);
        }

        /// <summary>
        /// generador de cadena de conexion de la base de datos
        /// </summary>
        /// <param name="configuracion">objeto de configuracion de la base de datos</param>
        /// <returns>retorna un objeto FbConnectionStringBuilder</returns>
        private iDB2ConnectionStringBuilder StringBuilder(DataBaseConfiguracion configuracion) {
            var builder = new iDB2ConnectionStringBuilder();
            builder.UserID = configuracion.UserID;
            builder.Password = configuracion.Password;
            builder.DataSource = configuracion.HostName;
            builder.Database = configuracion.Database;
            builder.Pooling = false;
            Console.WriteLine("StringBuilder: ", builder.ConnectionString);
            return builder;
        }
    }
}
