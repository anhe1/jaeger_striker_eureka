﻿using System.Text;
using System.Collections.Generic;
using IBM.Data.DB2.iSeries;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.DataAccess.Services;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.DB2.Services {
    public static class ExpressionTool {
        public static KeyValuePair<string, iDB2Parameter[]> ConditionalModelToSql(List<IConditional> models) {
            StringBuilder builder = new StringBuilder();
            var parameters = new List<iDB2Parameter>();
            foreach (var item in models) {
                var index = models.IndexOf(item);
                var type = index == 0 ? "" : "AND";
                string temp = " {0} {1} {2} {3}  ";
                // en caso de enviar alguna funcion se remueven los espacios para crear el nombre del parametro
                string parameterName = string.Format("{0}Conditional{1}{2}", "@", item.FieldName.Replace(" ", "").Trim(), index);
                if (parameterName.Contains(".")) {
                    parameterName = parameterName.Replace(".", "_");
                }
                if (parameterName.Contains("[")) {
                    parameterName = parameterName.Replace("[", "_");
                }
                if (parameterName.Contains("]")) {
                    parameterName = parameterName.Replace("]", "_");
                }
                if (parameterName.Contains("(")) {
                    parameterName = parameterName.Replace("(", "");
                }
                if (parameterName.Contains(")")) {
                    parameterName = parameterName.Replace(")", "");
                }

                switch (item.ConditionalType) {
                    case ConditionalTypeEnum.Equal:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "=", parameterName);
                        parameters.Add(new iDB2Parameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.Like:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "LIKE", parameterName);
                        parameters.Add(new iDB2Parameter(parameterName, "%" + item.FieldValue + "%"));
                        break;
                    case ConditionalTypeEnum.GreaterThan:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), ">", parameterName);
                        parameters.Add(new iDB2Parameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.GreaterThanOrEqual:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), ">=", parameterName);
                        parameters.Add(new iDB2Parameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.LessThan:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<", parameterName);
                        parameters.Add(new iDB2Parameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.LessThanOrEqual:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<=", parameterName);
                        parameters.Add(new iDB2Parameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.In:
                        if (item.FieldValue == null)
                            item.FieldValue = string.Empty;
                        var inValue1 = ("(" + item.FieldValue.Split(',').ToJoinSqlInVals() + ")");
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "IN", inValue1);
                        parameters.Add(new iDB2Parameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.NotIn:
                        if (item.FieldValue == null)
                            item.FieldValue = string.Empty;
                        var inValue2 = ("(" + item.FieldValue.Split(',').ToJoinSqlInVals() + ")");
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "NOT IN", inValue2);
                        parameters.Add(new iDB2Parameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.LikeLeft:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "LIKE", parameterName);
                        parameters.Add(new iDB2Parameter(parameterName, item.FieldValue + "%"));
                        break;
                    case ConditionalTypeEnum.NoLike:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), " NOT LIKE", parameterName);
                        parameters.Add(new iDB2Parameter(parameterName, "%" + item.FieldValue + "%"));
                        break;
                    case ConditionalTypeEnum.LikeRight:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "LIKE", parameterName);
                        parameters.Add(new iDB2Parameter(parameterName, "%" + item.FieldValue));
                        break;
                    case ConditionalTypeEnum.NoEqual:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<>", parameterName);
                        parameters.Add(new iDB2Parameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.IsNullOrEmpty:
                        builder.AppendFormat(" {0} (({1}) OR ({2})) ", type, item.FieldName.ToSqlFilter() + " IS NULL ", item.FieldName.ToSqlFilter() + " = '' ");
                        parameters.Add(new iDB2Parameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.IsNot:
                        if (item.FieldValue == null) {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), " IS NOT ", "NULL");
                        }
                        else {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<>", parameterName);
                            parameters.Add(new iDB2Parameter(parameterName, item.FieldValue));
                        }
                        break;
                    case ConditionalTypeEnum.EqualNull:
                        if (item.FieldValue == null) {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "  IS ", " NULL ");
                        }
                        else {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "=", parameterName);
                            parameters.Add(new iDB2Parameter(parameterName, item.FieldValue));
                        }
                        break;
                    default:
                        break;
                }
            }
            return new KeyValuePair<string, iDB2Parameter[]>(builder.ToString(), parameters.ToArray());
        }
    }
}
