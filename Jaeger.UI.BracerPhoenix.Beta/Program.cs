﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Repositorio.Services;

namespace Jaeger.UI {
    static class Program {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            RouterManagerService.CreatePathsFull();
            Application.Run(new Forms.SessionForm());
            if (ConfiguracionService.Contribuyente != null) {
                Application.Run(new Forms.MainForm());
            }
        }
    }
}
