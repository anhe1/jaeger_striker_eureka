﻿namespace Jaeger.UI.Forms {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.marchivo = new System.Windows.Forms.ToolStripMenuItem();
            this.barchivo_abrir = new System.Windows.Forms.ToolStripMenuItem();
            this.barchivo_crear = new System.Windows.Forms.ToolStripMenuItem();
            this.barchivo_empresa = new System.Windows.Forms.ToolStripMenuItem();
            this.barchivo_configuracion = new System.Windows.Forms.ToolStripMenuItem();
            this.barchivo_salir = new System.Windows.Forms.ToolStripMenuItem();
            this.memision = new System.Windows.Forms.ToolStripMenuItem();
            this.bemision_clientes = new System.Windows.Forms.ToolStripMenuItem();
            this.bemision_comprobante = new System.Windows.Forms.ToolStripMenuItem();
            this.bemision_retenciones = new System.Windows.Forms.ToolStripMenuItem();
            this.mrecepcion = new System.Windows.Forms.ToolStripMenuItem();
            this.brecepci_contribuyente = new System.Windows.Forms.ToolStripMenuItem();
            this.brecepci_validador = new System.Windows.Forms.ToolStripMenuItem();
            this.brecepci_comprobante = new System.Windows.Forms.ToolStripMenuItem();
            this.mnomina = new System.Windows.Forms.ToolStripMenuItem();
            this.mnomina_empleados = new System.Windows.Forms.ToolStripMenuItem();
            this.mnomina_recibos = new System.Windows.Forms.ToolStripMenuItem();
            this.mherramientas = new System.Windows.Forms.ToolStripMenuItem();
            this.bherrami_repositorio = new System.Windows.Forms.ToolStripMenuItem();
            this.bherrami_validador = new System.Windows.Forms.ToolStripMenuItem();
            this.bherrami_validarfc = new System.Windows.Forms.ToolStripMenuItem();
            this.bherrami_valretencion = new System.Windows.Forms.ToolStripMenuItem();
            this.bherrami_certificado = new System.Windows.Forms.ToolStripMenuItem();
            this.bherrami_cerinfo = new System.Windows.Forms.ToolStripMenuItem();
            this.bherrami_cerdescarga = new System.Windows.Forms.ToolStripMenuItem();
            this.bherrami_cerverifica = new System.Windows.Forms.ToolStripMenuItem();
            this.bherrami_metadata = new System.Windows.Forms.ToolStripMenuItem();
            this.bherrami_metacomp = new System.Windows.Forms.ToolStripMenuItem();
            this.bherrami_metareten = new System.Windows.Forms.ToolStripMenuItem();
            this.bherrami_cedulafis = new System.Windows.Forms.ToolStripMenuItem();
            this.mventana = new System.Windows.Forms.ToolStripMenuItem();
            this.mayuda = new System.Windows.Forms.ToolStripMenuItem();
            this.bayuda_acercade = new System.Windows.Forms.ToolStripMenuItem();
            this.statusMain = new System.Windows.Forms.StatusStrip();
            this.Empresa = new System.Windows.Forms.ToolStripSplitButton();
            this.Version = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMain.SuspendLayout();
            this.statusMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuMain
            // 
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.marchivo,
            this.memision,
            this.mrecepcion,
            this.mnomina,
            this.mherramientas,
            this.mventana,
            this.mayuda});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.MdiWindowListItem = this.mventana;
            this.menuMain.Name = "menuMain";
            this.menuMain.Size = new System.Drawing.Size(1041, 24);
            this.menuMain.TabIndex = 3;
            this.menuMain.Text = "Menu";
            // 
            // marchivo
            // 
            this.marchivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.barchivo_abrir,
            this.barchivo_crear,
            this.barchivo_empresa,
            this.barchivo_configuracion,
            this.barchivo_salir});
            this.marchivo.Name = "marchivo";
            this.marchivo.Size = new System.Drawing.Size(69, 20);
            this.marchivo.Text = "marchivo";
            // 
            // barchivo_abrir
            // 
            this.barchivo_abrir.Name = "barchivo_abrir";
            this.barchivo_abrir.Size = new System.Drawing.Size(199, 22);
            this.barchivo_abrir.Text = "barchivo_abrir";
            // 
            // barchivo_crear
            // 
            this.barchivo_crear.Name = "barchivo_crear";
            this.barchivo_crear.Size = new System.Drawing.Size(199, 22);
            this.barchivo_crear.Text = "barchivo_crear";
            // 
            // barchivo_empresa
            // 
            this.barchivo_empresa.Name = "barchivo_empresa";
            this.barchivo_empresa.Size = new System.Drawing.Size(199, 22);
            this.barchivo_empresa.Text = "barchivo_empresa";
            // 
            // barchivo_configuracion
            // 
            this.barchivo_configuracion.Name = "barchivo_configuracion";
            this.barchivo_configuracion.Size = new System.Drawing.Size(199, 22);
            this.barchivo_configuracion.Text = "barchivo_configuracion";
            // 
            // barchivo_salir
            // 
            this.barchivo_salir.Name = "barchivo_salir";
            this.barchivo_salir.Size = new System.Drawing.Size(199, 22);
            this.barchivo_salir.Text = "barchivo_salir";
            // 
            // memision
            // 
            this.memision.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bemision_clientes,
            this.bemision_comprobante,
            this.bemision_retenciones});
            this.memision.Name = "memision";
            this.memision.Size = new System.Drawing.Size(72, 20);
            this.memision.Text = "memision";
            // 
            // bemision_clientes
            // 
            this.bemision_clientes.Name = "bemision_clientes";
            this.bemision_clientes.Size = new System.Drawing.Size(200, 22);
            this.bemision_clientes.Text = "bemision_clientes";
            this.bemision_clientes.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // bemision_comprobante
            // 
            this.bemision_comprobante.Name = "bemision_comprobante";
            this.bemision_comprobante.Size = new System.Drawing.Size(200, 22);
            this.bemision_comprobante.Text = "bemision_comprobante";
            this.bemision_comprobante.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // bemision_retenciones
            // 
            this.bemision_retenciones.Name = "bemision_retenciones";
            this.bemision_retenciones.Size = new System.Drawing.Size(200, 22);
            this.bemision_retenciones.Text = "bemision_retenciones";
            // 
            // mrecepcion
            // 
            this.mrecepcion.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.brecepci_contribuyente,
            this.brecepci_validador,
            this.brecepci_comprobante});
            this.mrecepcion.Name = "mrecepcion";
            this.mrecepcion.Size = new System.Drawing.Size(82, 20);
            this.mrecepcion.Text = "mrecepcion";
            // 
            // brecepci_contribuyente
            // 
            this.brecepci_contribuyente.Name = "brecepci_contribuyente";
            this.brecepci_contribuyente.Size = new System.Drawing.Size(198, 22);
            this.brecepci_contribuyente.Text = "brecepci_contribuyente";
            this.brecepci_contribuyente.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // brecepci_validador
            // 
            this.brecepci_validador.Name = "brecepci_validador";
            this.brecepci_validador.Size = new System.Drawing.Size(198, 22);
            this.brecepci_validador.Text = "brecepci_validador";
            // 
            // brecepci_comprobante
            // 
            this.brecepci_comprobante.Name = "brecepci_comprobante";
            this.brecepci_comprobante.Size = new System.Drawing.Size(198, 22);
            this.brecepci_comprobante.Text = "brecepci_comprobante";
            this.brecepci_comprobante.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // mnomina
            // 
            this.mnomina.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnomina_empleados,
            this.mnomina_recibos});
            this.mnomina.Name = "mnomina";
            this.mnomina.Size = new System.Drawing.Size(71, 20);
            this.mnomina.Text = "mnomina";
            // 
            // mnomina_empleados
            // 
            this.mnomina_empleados.Name = "mnomina_empleados";
            this.mnomina_empleados.Size = new System.Drawing.Size(163, 22);
            this.mnomina_empleados.Text = "b.Empleados";
            this.mnomina_empleados.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // mnomina_recibos
            // 
            this.mnomina_recibos.Name = "mnomina_recibos";
            this.mnomina_recibos.Size = new System.Drawing.Size(163, 22);
            this.mnomina_recibos.Text = "b.Comprobantes";
            this.mnomina_recibos.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // mherramientas
            // 
            this.mherramientas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bherrami_repositorio,
            this.bherrami_validador,
            this.bherrami_validarfc,
            this.bherrami_valretencion,
            this.bherrami_certificado,
            this.bherrami_metadata,
            this.bherrami_cedulafis});
            this.mherramientas.Name = "mherramientas";
            this.mherramientas.Size = new System.Drawing.Size(99, 20);
            this.mherramientas.Text = "mherramientas";
            // 
            // bherrami_repositorio
            // 
            this.bherrami_repositorio.Name = "bherrami_repositorio";
            this.bherrami_repositorio.Size = new System.Drawing.Size(192, 22);
            this.bherrami_repositorio.Text = "bherrami_repositorio";
            this.bherrami_repositorio.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // bherrami_validador
            // 
            this.bherrami_validador.Image = global::Jaeger.UI.Properties.Resources.protect_16px;
            this.bherrami_validador.Name = "bherrami_validador";
            this.bherrami_validador.Size = new System.Drawing.Size(192, 22);
            this.bherrami_validador.Text = "bherrami_validador";
            this.bherrami_validador.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // bherrami_validarfc
            // 
            this.bherrami_validarfc.Name = "bherrami_validarfc";
            this.bherrami_validarfc.Size = new System.Drawing.Size(192, 22);
            this.bherrami_validarfc.Text = "bherrami_validarfc";
            this.bherrami_validarfc.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // bherrami_valretencion
            // 
            this.bherrami_valretencion.Name = "bherrami_valretencion";
            this.bherrami_valretencion.Size = new System.Drawing.Size(192, 22);
            this.bherrami_valretencion.Text = "bherrami_valretencion";
            this.bherrami_valretencion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // bherrami_certificado
            // 
            this.bherrami_certificado.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bherrami_cerinfo,
            this.bherrami_cerdescarga,
            this.bherrami_cerverifica});
            this.bherrami_certificado.Name = "bherrami_certificado";
            this.bherrami_certificado.Size = new System.Drawing.Size(192, 22);
            this.bherrami_certificado.Text = "bherrami_certificado";
            // 
            // bherrami_cerinfo
            // 
            this.bherrami_cerinfo.Name = "bherrami_cerinfo";
            this.bherrami_cerinfo.Size = new System.Drawing.Size(146, 22);
            this.bherrami_cerinfo.Text = "bInformacion";
            this.bherrami_cerinfo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // bherrami_cerdescarga
            // 
            this.bherrami_cerdescarga.Name = "bherrami_cerdescarga";
            this.bherrami_cerdescarga.Size = new System.Drawing.Size(146, 22);
            this.bherrami_cerdescarga.Text = "bDescargar";
            this.bherrami_cerdescarga.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // bherrami_cerverifica
            // 
            this.bherrami_cerverifica.Name = "bherrami_cerverifica";
            this.bherrami_cerverifica.Size = new System.Drawing.Size(146, 22);
            this.bherrami_cerverifica.Text = "bVerificar";
            this.bherrami_cerverifica.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // bherrami_metadata
            // 
            this.bherrami_metadata.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bherrami_metacomp,
            this.bherrami_metareten});
            this.bherrami_metadata.Name = "bherrami_metadata";
            this.bherrami_metadata.Size = new System.Drawing.Size(192, 22);
            this.bherrami_metadata.Text = "b.MetaDATA";
            // 
            // bherrami_metacomp
            // 
            this.bherrami_metacomp.Name = "bherrami_metacomp";
            this.bherrami_metacomp.Size = new System.Drawing.Size(163, 22);
            this.bherrami_metacomp.Text = "b.Comprobantes";
            this.bherrami_metacomp.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // bherrami_metareten
            // 
            this.bherrami_metareten.Name = "bherrami_metareten";
            this.bherrami_metareten.Size = new System.Drawing.Size(163, 22);
            this.bherrami_metareten.Text = "b.Retencion";
            this.bherrami_metareten.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // bherrami_cedulafis
            // 
            this.bherrami_cedulafis.Image = global::Jaeger.UI.Properties.Resources.identification_documents_16;
            this.bherrami_cedulafis.Name = "bherrami_cedulafis";
            this.bherrami_cedulafis.Size = new System.Drawing.Size(192, 22);
            this.bherrami_cedulafis.Text = "b.Cedula";
            this.bherrami_cedulafis.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // mventana
            // 
            this.mventana.Name = "mventana";
            this.mventana.Size = new System.Drawing.Size(72, 20);
            this.mventana.Text = "mventana";
            // 
            // mayuda
            // 
            this.mayuda.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bayuda_acercade});
            this.mayuda.Name = "mayuda";
            this.mayuda.Size = new System.Drawing.Size(62, 20);
            this.mayuda.Text = "mayuda";
            // 
            // bayuda_acercade
            // 
            this.bayuda_acercade.Name = "bayuda_acercade";
            this.bayuda_acercade.Size = new System.Drawing.Size(165, 22);
            this.bayuda_acercade.Text = "bayuda_acercade";
            // 
            // statusMain
            // 
            this.statusMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Empresa});
            this.statusMain.Location = new System.Drawing.Point(0, 527);
            this.statusMain.Name = "statusMain";
            this.statusMain.Size = new System.Drawing.Size(1041, 22);
            this.statusMain.TabIndex = 2;
            // 
            // Empresa
            // 
            this.Empresa.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Version});
            this.Empresa.Image = global::Jaeger.UI.Properties.Resources.badge_16px;
            this.Empresa.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Empresa.Name = "Empresa";
            this.Empresa.Size = new System.Drawing.Size(84, 20);
            this.Empresa.Text = "Empresa";
            // 
            // Version
            // 
            this.Version.Name = "Version";
            this.Version.Size = new System.Drawing.Size(122, 22);
            this.Version.Text = "b.Versión";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1041, 549);
            this.Controls.Add(this.statusMain);
            this.Controls.Add(this.menuMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuMain;
            this.Name = "MainForm";
            this.Text = "CFDI Validador (beta)";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.statusMain.ResumeLayout(false);
            this.statusMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem mherramientas;
        private System.Windows.Forms.ToolStripMenuItem mayuda;
        private System.Windows.Forms.ToolStripMenuItem marchivo;
        private System.Windows.Forms.StatusStrip statusMain;
        private System.Windows.Forms.ToolStripMenuItem mventana;
        private System.Windows.Forms.ToolStripMenuItem memision;
        private System.Windows.Forms.ToolStripMenuItem mrecepcion;
        private System.Windows.Forms.ToolStripMenuItem barchivo_abrir;
        private System.Windows.Forms.ToolStripMenuItem barchivo_crear;
        private System.Windows.Forms.ToolStripMenuItem barchivo_empresa;
        private System.Windows.Forms.ToolStripMenuItem barchivo_configuracion;
        private System.Windows.Forms.ToolStripMenuItem barchivo_salir;
        private System.Windows.Forms.ToolStripMenuItem bemision_clientes;
        private System.Windows.Forms.ToolStripMenuItem bemision_comprobante;
        private System.Windows.Forms.ToolStripMenuItem bemision_retenciones;
        private System.Windows.Forms.ToolStripMenuItem brecepci_contribuyente;
        private System.Windows.Forms.ToolStripMenuItem brecepci_validador;
        private System.Windows.Forms.ToolStripMenuItem brecepci_comprobante;
        private System.Windows.Forms.ToolStripMenuItem bherrami_repositorio;
        private System.Windows.Forms.ToolStripMenuItem bherrami_validador;
        private System.Windows.Forms.ToolStripMenuItem bherrami_validarfc;
        private System.Windows.Forms.ToolStripMenuItem bherrami_valretencion;
        private System.Windows.Forms.ToolStripMenuItem bayuda_acercade;
        private System.Windows.Forms.ToolStripMenuItem bherrami_certificado;
        private System.Windows.Forms.ToolStripSplitButton Empresa;
        private System.Windows.Forms.ToolStripMenuItem mnomina;
        private System.Windows.Forms.ToolStripMenuItem mnomina_empleados;
        private System.Windows.Forms.ToolStripMenuItem mnomina_recibos;
        private System.Windows.Forms.ToolStripMenuItem Version;
        private System.Windows.Forms.ToolStripMenuItem bherrami_cerinfo;
        private System.Windows.Forms.ToolStripMenuItem bherrami_cerdescarga;
        private System.Windows.Forms.ToolStripMenuItem bherrami_cerverifica;
        private System.Windows.Forms.ToolStripMenuItem bherrami_metadata;
        private System.Windows.Forms.ToolStripMenuItem bherrami_metacomp;
        private System.Windows.Forms.ToolStripMenuItem bherrami_metareten;
        private System.Windows.Forms.ToolStripMenuItem bherrami_cedulafis;
    }
}