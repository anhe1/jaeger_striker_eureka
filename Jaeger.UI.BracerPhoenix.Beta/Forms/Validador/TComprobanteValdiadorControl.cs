﻿using System;
using System.Windows.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Validador {
    public partial class TComprobanteValdiadorControl : UserControl {
        protected internal Aplication.Validador.Beta.Services.Administrador Administrador;
        protected internal Aplication.Validador.Beta.Services.CarpetaRecienteService recientes;

        public TComprobanteValdiadorControl() {
            InitializeComponent();
        }

        private void TComprobanteValdiadorControl_Load(object sender, EventArgs e) {
            this.Historial.Click += Historial_Click;
            this.recientes = new Aplication.Validador.Beta.Services.CarpetaRecienteService();
            this.recientes.Load();
            this.Carpeta.ComboBox.DisplayMember = "Carpeta";
            this.Carpeta.ComboBox.DataSource = this.recientes.Items;
            this.Carpeta.ComboBox.DropDownWidth();
            this.Buscar.Click += Buscar_Click;
            this.GuardarEn.Click += TValida_GuardarEn_Click;
            this.DescargarTemplete.Click += DescargarTemplete_Click;
        }

        private void TValida_GuardarEn_Click(object sender, EventArgs e) {
            if (this.Administrador._DataSource != null) {
                if (this.Administrador._DataSource.Count > 0) {
                    var guardarEn = new GuardarEnForm(this.Administrador._DataSource);
                    guardarEn.ShowDialog(this);
                }
            }
        }

        private void Historial_Click(object sender, EventArgs e) {
            if (MessageBox.Show(this, "¿Esta seguro de eliminar el historial de carpetas?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                this.recientes.Reset();
            }
        }

        private void Buscar_Click(object sender, EventArgs e) {
            var openFolder = new FolderBrowserDialog();
            if (openFolder.ShowDialog(this) == DialogResult.Cancel)
                return;

            if (System.IO.Directory.Exists(openFolder.SelectedPath)) {
                this.recientes.Add(openFolder.SelectedPath);
                if (this.Historial.Checked) { this.recientes.Save(); }
                //this.Carpeta.ComboBox.DataSource = this.recientes.Items;
                this.Carpeta.SelectedText = openFolder.SelectedPath;
            } else {
                MessageBox.Show(this, string.Format("No se puede encontrar una parte de la ruta de acceso '{0}'", openFolder.SelectedPath), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DescargarTemplete_Click(object sender, EventArgs e) {
            var saveFile = new SaveFileDialog() { FileName = "Validacion_Templete.xlsx", Title = "Descargar templete" };
            if (saveFile.ShowDialog(this)== DialogResult.OK) {
                this.Administrador.DescargarTemplete(saveFile.FileName);
            }
        }

        private void RemoverTodo_Click(object sender, EventArgs e) {
            this.Administrador._DataSource.Clear();
        }
    }
}
