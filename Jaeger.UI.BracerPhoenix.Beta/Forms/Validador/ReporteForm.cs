﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Microsoft.Reporting.WinForms;
using Jaeger.Aplication.Validador.Beta.Domain;
using Jaeger.Util.Services;

namespace Jaeger.UI.Forms.Validador {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private EmbeddedResources horizon = new EmbeddedResources("Jaeger.Aplication.Validador.Beta");

        public ReporteForm() : base() {
        }

        public ReporteForm(string rfc, string razonSocial) : base(rfc, razonSocial) {
        }

        public ReporteForm(object sender) : base("", "") {
            this.CurrentObject = sender;
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            this.PathLogo = @"C:\Jaeger\Jaeger.Media\logo-ipo.png";
            if (this.CurrentObject.GetType() == typeof(ComprobanteValidacionPrinter)) {
                this.CrearReporteValidacion();
            }
        }

        private void CrearReporteValidacion() {
            var current = (ComprobanteValidacionPrinter)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Aplication.Validador.Beta.Reports.ComprobanteValidacion35Report.rdlc");
            this.SetDisplayName("Reporte Validación");
            var d = Domain.Services.DbConvert.ConvertToDataTable<ComprobanteValidacionPrinter>(new List<ComprobanteValidacionPrinter>() { current });
            this.SetDataSource("Validacion", d);
            var d1 = Domain.Services.DbConvert.ConvertToDataTable<ComprobanteValidacionPropiedad>(current.Resultados);
            this.SetDataSource("Resultados", d1);
            this.Finalizar();
        }
    }
}
