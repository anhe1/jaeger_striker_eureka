﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Microsoft.Reporting.WinForms;
using Jaeger.Aplication.Validador.Beta.Domain;
using Jaeger.Util.Services;

namespace Jaeger.UI.Forms.Validador {
    public class ReportePDF {
        private readonly EmbeddedResources resource = new EmbeddedResources("Jaeger.Aplication.Validador.Beta");
        public string PathPDF { get; set; }

        public string Crear(ComprobanteValidacionPrinter validateResponse) {
            if (validateResponse == null) {
                Console.WriteLine("HelperReportToPDF: la validación se encuentra vacía.");
                return "";
            }

            string filename = Path.Combine(this.PathPDF, string.Format("CFDI-{0}_validacion.pdf", validateResponse.KeyName()));


            using (var reportViewer = new ReportViewer()) {
                List<ComprobanteValidacionPrinter> lista = new List<ComprobanteValidacionPrinter>() { validateResponse };
                reportViewer.LocalReport.DisplayName = "Validación de Comprobantes";
                reportViewer.LocalReport.LoadReportDefinition(resource.GetStream("Jaeger.Aplication.Validador.Beta.Reports.ComprobanteValidacion35Report.rdlc"));
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("Validacion", (IEnumerable)lista));
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("Resultados", (IEnumerable)validateResponse.Resultados));
                reportViewer.RefreshReport();

                byte[] bytes = reportViewer.LocalReport.Render(
                    "PDF", null, out string mimeType, out string encoding, out string filenameExtension,
                     out string[] streamids, out Warning[] warnings);

                using (FileStream fs = new FileStream(filename, FileMode.Create)) {
                    fs.Write(bytes, 0, bytes.Length);
                }
            }

            return filename;
        }
    }
}
