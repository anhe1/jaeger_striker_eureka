﻿
namespace Jaeger.UI.Forms.Validador {
    partial class ConfiguracionForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfiguracionForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ObtenerEmisor = new System.Windows.Forms.CheckBox();
            this.ValidarXSD = new System.Windows.Forms.CheckBox();
            this.ValidarClaveUsoCFDI = new System.Windows.Forms.CheckBox();
            this.VerificarCodificacionUTF8 = new System.Windows.Forms.CheckBox();
            this.ValidarClaveMetodoPago = new System.Windows.Forms.CheckBox();
            this.ValidarClaveFormaPago = new System.Windows.Forms.CheckBox();
            this.ValidarClaveConcepto = new System.Windows.Forms.CheckBox();
            this.ValidaAritmetica = new System.Windows.Forms.CheckBox();
            this.RequiereFormaYMetodoPago = new System.Windows.Forms.CheckBox();
            this.VerificarEstadoSAT = new System.Windows.Forms.CheckBox();
            this.ValidarAddendaSin = new System.Windows.Forms.CheckBox();
            this.ValidarSelloCFDI = new System.Windows.Forms.CheckBox();
            this.ValidarSelloSAT = new System.Windows.Forms.CheckBox();
            this.RenombrarOrigen = new System.Windows.Forms.CheckBox();
            this.ModoParalelo = new System.Windows.Forms.CheckBox();
            this.AsociarPDF = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.SoloCFDIValido = new System.Windows.Forms.CheckBox();
            this.RegistrarComprobante = new System.Windows.Forms.CheckBox();
            this.DescargaCertificado = new System.Windows.Forms.CheckBox();
            this.RegistrarConceptos = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ValidarArticulo68 = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.VerificarEmisorCP = new System.Windows.Forms.CheckBox();
            this.LugarExpedicion32B = new System.Windows.Forms.CheckBox();
            this.LugarExpedicion32 = new System.Windows.Forms.TextBox();
            this.Guardar = new System.Windows.Forms.Button();
            this.Cerrar = new System.Windows.Forms.Button();
            this.lblInformacion = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ObtenerEmisor);
            this.groupBox1.Controls.Add(this.ValidarXSD);
            this.groupBox1.Controls.Add(this.ValidarClaveUsoCFDI);
            this.groupBox1.Controls.Add(this.VerificarCodificacionUTF8);
            this.groupBox1.Controls.Add(this.ValidarClaveMetodoPago);
            this.groupBox1.Controls.Add(this.ValidarClaveFormaPago);
            this.groupBox1.Controls.Add(this.ValidarClaveConcepto);
            this.groupBox1.Controls.Add(this.ValidaAritmetica);
            this.groupBox1.Controls.Add(this.RequiereFormaYMetodoPago);
            this.groupBox1.Controls.Add(this.VerificarEstadoSAT);
            this.groupBox1.Controls.Add(this.ValidarAddendaSin);
            this.groupBox1.Controls.Add(this.ValidarSelloCFDI);
            this.groupBox1.Controls.Add(this.ValidarSelloSAT);
            this.groupBox1.Location = new System.Drawing.Point(12, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(269, 405);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "General";
            // 
            // ObtenerEmisor
            // 
            this.ObtenerEmisor.Location = new System.Drawing.Point(6, 322);
            this.ObtenerEmisor.Name = "ObtenerEmisor";
            this.ObtenerEmisor.Size = new System.Drawing.Size(247, 46);
            this.ObtenerEmisor.TabIndex = 2;
            this.ObtenerEmisor.Text = "Obtener nombre o denominación social desde el certificado, cuando no este disponi" +
    "ble el valor del atributo,";
            this.ObtenerEmisor.UseVisualStyleBackColor = true;
            // 
            // ValidarXSD
            // 
            this.ValidarXSD.AutoSize = true;
            this.ValidarXSD.Location = new System.Drawing.Point(6, 19);
            this.ValidarXSD.Name = "ValidarXSD";
            this.ValidarXSD.Size = new System.Drawing.Size(100, 17);
            this.ValidarXSD.TabIndex = 1;
            this.ValidarXSD.Text = "Validación XSD";
            this.ValidarXSD.UseVisualStyleBackColor = true;
            // 
            // ValidarClaveUsoCFDI
            // 
            this.ValidarClaveUsoCFDI.AutoSize = true;
            this.ValidarClaveUsoCFDI.Location = new System.Drawing.Point(6, 65);
            this.ValidarClaveUsoCFDI.Name = "ValidarClaveUsoCFDI";
            this.ValidarClaveUsoCFDI.Size = new System.Drawing.Size(170, 17);
            this.ValidarClaveUsoCFDI.TabIndex = 1;
            this.ValidarClaveUsoCFDI.Text = "Verificar clave de uso de CFDI";
            this.ValidarClaveUsoCFDI.UseVisualStyleBackColor = true;
            // 
            // VerificarCodificacionUTF8
            // 
            this.VerificarCodificacionUTF8.Location = new System.Drawing.Point(6, 374);
            this.VerificarCodificacionUTF8.Name = "VerificarCodificacionUTF8";
            this.VerificarCodificacionUTF8.Size = new System.Drawing.Size(203, 17);
            this.VerificarCodificacionUTF8.TabIndex = 1;
            this.VerificarCodificacionUTF8.Text = "Verificar codificación UTF8";
            this.VerificarCodificacionUTF8.UseVisualStyleBackColor = true;
            // 
            // ValidarClaveMetodoPago
            // 
            this.ValidarClaveMetodoPago.AutoSize = true;
            this.ValidarClaveMetodoPago.Location = new System.Drawing.Point(6, 111);
            this.ValidarClaveMetodoPago.Name = "ValidarClaveMetodoPago";
            this.ValidarClaveMetodoPago.Size = new System.Drawing.Size(138, 17);
            this.ValidarClaveMetodoPago.TabIndex = 1;
            this.ValidarClaveMetodoPago.Text = "Validar método de pago";
            this.ValidarClaveMetodoPago.UseVisualStyleBackColor = true;
            // 
            // ValidarClaveFormaPago
            // 
            this.ValidarClaveFormaPago.AutoSize = true;
            this.ValidarClaveFormaPago.Location = new System.Drawing.Point(6, 88);
            this.ValidarClaveFormaPago.Name = "ValidarClaveFormaPago";
            this.ValidarClaveFormaPago.Size = new System.Drawing.Size(130, 17);
            this.ValidarClaveFormaPago.TabIndex = 1;
            this.ValidarClaveFormaPago.Text = "Validar forma de Pago";
            this.ValidarClaveFormaPago.UseVisualStyleBackColor = true;
            // 
            // ValidarClaveConcepto
            // 
            this.ValidarClaveConcepto.AutoSize = true;
            this.ValidarClaveConcepto.Location = new System.Drawing.Point(6, 42);
            this.ValidarClaveConcepto.Name = "ValidarClaveConcepto";
            this.ValidarClaveConcepto.Size = new System.Drawing.Size(180, 17);
            this.ValidarClaveConcepto.TabIndex = 1;
            this.ValidarClaveConcepto.Text = "Verificar clave SAT de concepto";
            this.ValidarClaveConcepto.UseVisualStyleBackColor = true;
            // 
            // ValidaAritmetica
            // 
            this.ValidaAritmetica.Location = new System.Drawing.Point(6, 269);
            this.ValidaAritmetica.Name = "ValidaAritmetica";
            this.ValidaAritmetica.Size = new System.Drawing.Size(203, 47);
            this.ValidaAritmetica.TabIndex = 1;
            this.ValidaAritmetica.Text = "Valida aritmética en porcentaje sobre el total (configurable en opciones avanzada" +
    "s)";
            this.ValidaAritmetica.UseVisualStyleBackColor = true;
            // 
            // RequiereFormaYMetodoPago
            // 
            this.RequiereFormaYMetodoPago.Location = new System.Drawing.Point(6, 134);
            this.RequiereFormaYMetodoPago.Name = "RequiereFormaYMetodoPago";
            this.RequiereFormaYMetodoPago.Size = new System.Drawing.Size(251, 37);
            this.RequiereFormaYMetodoPago.TabIndex = 1;
            this.RequiereFormaYMetodoPago.Text = "Requerir \"Forma de pago\" y \"Método de pago\" para comprobantes de ingreso y egreso" +
    ".";
            this.RequiereFormaYMetodoPago.UseVisualStyleBackColor = true;
            // 
            // VerificarEstadoSAT
            // 
            this.VerificarEstadoSAT.AutoSize = true;
            this.VerificarEstadoSAT.Location = new System.Drawing.Point(6, 177);
            this.VerificarEstadoSAT.Name = "VerificarEstadoSAT";
            this.VerificarEstadoSAT.Size = new System.Drawing.Size(219, 17);
            this.VerificarEstadoSAT.TabIndex = 1;
            this.VerificarEstadoSAT.Text = "Verificar el estado del comprobante SAT.";
            this.VerificarEstadoSAT.UseVisualStyleBackColor = true;
            // 
            // ValidarAddendaSin
            // 
            this.ValidarAddendaSin.Location = new System.Drawing.Point(6, 200);
            this.ValidarAddendaSin.Name = "ValidarAddendaSin";
            this.ValidarAddendaSin.Size = new System.Drawing.Size(203, 17);
            this.ValidarAddendaSin.TabIndex = 1;
            this.ValidarAddendaSin.Text = "Valida addendas sin Namespace.";
            this.ValidarAddendaSin.UseVisualStyleBackColor = true;
            // 
            // ValidarSelloCFDI
            // 
            this.ValidarSelloCFDI.Location = new System.Drawing.Point(6, 223);
            this.ValidarSelloCFDI.Name = "ValidarSelloCFDI";
            this.ValidarSelloCFDI.Size = new System.Drawing.Size(203, 17);
            this.ValidarSelloCFDI.TabIndex = 1;
            this.ValidarSelloCFDI.Text = "Validar sello CFDI";
            this.ValidarSelloCFDI.UseVisualStyleBackColor = true;
            // 
            // ValidarSelloSAT
            // 
            this.ValidarSelloSAT.Location = new System.Drawing.Point(6, 246);
            this.ValidarSelloSAT.Name = "ValidarSelloSAT";
            this.ValidarSelloSAT.Size = new System.Drawing.Size(203, 17);
            this.ValidarSelloSAT.TabIndex = 1;
            this.ValidarSelloSAT.Text = "Validar sello SAT";
            this.ValidarSelloSAT.UseVisualStyleBackColor = true;
            // 
            // RenombrarOrigen
            // 
            this.RenombrarOrigen.AutoSize = true;
            this.RenombrarOrigen.Location = new System.Drawing.Point(9, 65);
            this.RenombrarOrigen.Name = "RenombrarOrigen";
            this.RenombrarOrigen.Size = new System.Drawing.Size(156, 17);
            this.RenombrarOrigen.TabIndex = 1;
            this.RenombrarOrigen.Text = "Renombrar archivos origen.";
            this.RenombrarOrigen.UseVisualStyleBackColor = true;
            // 
            // ModoParalelo
            // 
            this.ModoParalelo.AutoSize = true;
            this.ModoParalelo.Location = new System.Drawing.Point(9, 19);
            this.ModoParalelo.Name = "ModoParalelo";
            this.ModoParalelo.Size = new System.Drawing.Size(93, 17);
            this.ModoParalelo.TabIndex = 1;
            this.ModoParalelo.Text = "Modo paralelo";
            this.ModoParalelo.UseVisualStyleBackColor = true;
            // 
            // AsociarPDF
            // 
            this.AsociarPDF.AutoSize = true;
            this.AsociarPDF.Location = new System.Drawing.Point(9, 42);
            this.AsociarPDF.Name = "AsociarPDF";
            this.AsociarPDF.Size = new System.Drawing.Size(128, 17);
            this.AsociarPDF.TabIndex = 1;
            this.AsociarPDF.Text = "Buscar y asignar PDF";
            this.AsociarPDF.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.SoloCFDIValido);
            this.groupBox2.Controls.Add(this.RenombrarOrigen);
            this.groupBox2.Controls.Add(this.ModoParalelo);
            this.groupBox2.Controls.Add(this.RegistrarComprobante);
            this.groupBox2.Controls.Add(this.DescargaCertificado);
            this.groupBox2.Controls.Add(this.AsociarPDF);
            this.groupBox2.Controls.Add(this.RegistrarConceptos);
            this.groupBox2.Location = new System.Drawing.Point(287, 36);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(269, 183);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Opciones adicionales";
            // 
            // SoloCFDIValido
            // 
            this.SoloCFDIValido.AutoSize = true;
            this.SoloCFDIValido.Enabled = false;
            this.SoloCFDIValido.Location = new System.Drawing.Point(29, 134);
            this.SoloCFDIValido.Name = "SoloCFDIValido";
            this.SoloCFDIValido.Size = new System.Drawing.Size(193, 17);
            this.SoloCFDIValido.TabIndex = 2;
            this.SoloCFDIValido.Text = "Solo registrar comprobantes validos";
            this.SoloCFDIValido.UseVisualStyleBackColor = true;
            // 
            // RegistrarComprobante
            // 
            this.RegistrarComprobante.AutoSize = true;
            this.RegistrarComprobante.Location = new System.Drawing.Point(9, 88);
            this.RegistrarComprobante.Name = "RegistrarComprobante";
            this.RegistrarComprobante.Size = new System.Drawing.Size(156, 17);
            this.RegistrarComprobante.TabIndex = 1;
            this.RegistrarComprobante.Text = "Registrar Comprobante (S3)";
            this.RegistrarComprobante.UseVisualStyleBackColor = true;
            this.RegistrarComprobante.CheckStateChanged += new System.EventHandler(this.RegistrarComprobante_CheckStateChanged);
            // 
            // DescargaCertificado
            // 
            this.DescargaCertificado.AutoSize = true;
            this.DescargaCertificado.Location = new System.Drawing.Point(9, 157);
            this.DescargaCertificado.Name = "DescargaCertificado";
            this.DescargaCertificado.Size = new System.Drawing.Size(132, 17);
            this.DescargaCertificado.TabIndex = 1;
            this.DescargaCertificado.Text = "Descargar certificados";
            this.DescargaCertificado.UseVisualStyleBackColor = true;
            // 
            // RegistrarConceptos
            // 
            this.RegistrarConceptos.AutoSize = true;
            this.RegistrarConceptos.Location = new System.Drawing.Point(29, 111);
            this.RegistrarConceptos.Name = "RegistrarConceptos";
            this.RegistrarConceptos.Size = new System.Drawing.Size(203, 17);
            this.RegistrarConceptos.TabIndex = 1;
            this.RegistrarConceptos.Text = "Registrar conceptos del comprobante";
            this.RegistrarConceptos.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ValidarArticulo68);
            this.groupBox3.Location = new System.Drawing.Point(287, 225);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(269, 81);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Artículo 69B";
            // 
            // ValidarArticulo68
            // 
            this.ValidarArticulo68.Location = new System.Drawing.Point(7, 17);
            this.ValidarArticulo68.Name = "ValidarArticulo68";
            this.ValidarArticulo68.Size = new System.Drawing.Size(257, 58);
            this.ValidarArticulo68.TabIndex = 2;
            this.ValidarArticulo68.Text = "Validar si el contribuyente se ubica en el listado SAT presunción de llevar a cab" +
    "o operaciones inexistentes a través de la emisión de facturas o comprobantes fis" +
    "cales.";
            this.ValidarArticulo68.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.VerificarEmisorCP);
            this.groupBox4.Controls.Add(this.LugarExpedicion32B);
            this.groupBox4.Controls.Add(this.LugarExpedicion32);
            this.groupBox4.Location = new System.Drawing.Point(287, 312);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(269, 129);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Lugar de Expedición";
            // 
            // VerificarEmisorCP
            // 
            this.VerificarEmisorCP.Location = new System.Drawing.Point(9, 19);
            this.VerificarEmisorCP.Name = "VerificarEmisorCP";
            this.VerificarEmisorCP.Size = new System.Drawing.Size(248, 31);
            this.VerificarEmisorCP.TabIndex = 3;
            this.VerificarEmisorCP.Text = "Verificar Lugar de expedición (Codigo Postal) a partir de la versión 3.3";
            this.VerificarEmisorCP.UseVisualStyleBackColor = true;
            // 
            // LugarExpedicion32B
            // 
            this.LugarExpedicion32B.Location = new System.Drawing.Point(9, 48);
            this.LugarExpedicion32B.Name = "LugarExpedicion32B";
            this.LugarExpedicion32B.Size = new System.Drawing.Size(254, 45);
            this.LugarExpedicion32B.TabIndex = 3;
            this.LugarExpedicion32B.Text = "Expresión regular para la comprobación del codigo postal ó lugar de expedición, p" +
    "ara versión CFDI 3.2";
            this.LugarExpedicion32B.UseVisualStyleBackColor = true;
            this.LugarExpedicion32B.CheckStateChanged += new System.EventHandler(this.LugarExpedicion32B_CheckStateChanged);
            // 
            // LugarExpedicion32
            // 
            this.LugarExpedicion32.Enabled = false;
            this.LugarExpedicion32.Location = new System.Drawing.Point(9, 99);
            this.LugarExpedicion32.MaxLength = 80;
            this.LugarExpedicion32.Name = "LugarExpedicion32";
            this.LugarExpedicion32.Size = new System.Drawing.Size(248, 20);
            this.LugarExpedicion32.TabIndex = 1;
            // 
            // Guardar
            // 
            this.Guardar.Location = new System.Drawing.Point(562, 36);
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(75, 23);
            this.Guardar.TabIndex = 3;
            this.Guardar.Text = "Guardar";
            this.Guardar.UseVisualStyleBackColor = true;
            this.Guardar.Click += new System.EventHandler(this.Guardar_Click);
            // 
            // Cerrar
            // 
            this.Cerrar.Location = new System.Drawing.Point(562, 65);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(75, 23);
            this.Cerrar.TabIndex = 3;
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.UseVisualStyleBackColor = true;
            this.Cerrar.Click += new System.EventHandler(this.Cerrar_Click);
            // 
            // lblInformacion
            // 
            this.lblInformacion.AutoSize = true;
            this.lblInformacion.Location = new System.Drawing.Point(15, 9);
            this.lblInformacion.Name = "lblInformacion";
            this.lblInformacion.Size = new System.Drawing.Size(392, 13);
            this.lblInformacion.TabIndex = 4;
            this.lblInformacion.Text = "Versiones que puede validar con esta herramienta: CFDI 3.2, CFDI 3.3 y CFDI 4.0";
            // 
            // ConfiguracionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 453);
            this.Controls.Add(this.lblInformacion);
            this.Controls.Add(this.Cerrar);
            this.Controls.Add(this.Guardar);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfiguracionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configuración";
            this.Load += new System.EventHandler(this.ConfiguracionForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox RenombrarOrigen;
        private System.Windows.Forms.CheckBox VerificarEstadoSAT;
        private System.Windows.Forms.CheckBox ModoParalelo;
        private System.Windows.Forms.CheckBox AsociarPDF;
        private System.Windows.Forms.CheckBox ValidarXSD;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox ValidarClaveUsoCFDI;
        private System.Windows.Forms.CheckBox ValidarClaveConcepto;
        private System.Windows.Forms.CheckBox ValidarClaveFormaPago;
        private System.Windows.Forms.CheckBox DescargaCertificado;
        private System.Windows.Forms.CheckBox ValidarClaveMetodoPago;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox ValidarArticulo68;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox LugarExpedicion32B;
        private System.Windows.Forms.TextBox LugarExpedicion32;
        private System.Windows.Forms.Button Guardar;
        private System.Windows.Forms.Button Cerrar;
        private System.Windows.Forms.CheckBox VerificarCodificacionUTF8;
        private System.Windows.Forms.CheckBox ValidarSelloCFDI;
        private System.Windows.Forms.CheckBox ValidaAritmetica;
        private System.Windows.Forms.CheckBox ValidarAddendaSin;
        private System.Windows.Forms.CheckBox RegistrarConceptos;
        private System.Windows.Forms.Label lblInformacion;
        private System.Windows.Forms.CheckBox RequiereFormaYMetodoPago;
        private System.Windows.Forms.CheckBox ObtenerEmisor;
        private System.Windows.Forms.CheckBox ValidarSelloSAT;
        private System.Windows.Forms.CheckBox VerificarEmisorCP;
        private System.Windows.Forms.CheckBox RegistrarComprobante;
        private System.Windows.Forms.CheckBox SoloCFDIValido;
    }
}