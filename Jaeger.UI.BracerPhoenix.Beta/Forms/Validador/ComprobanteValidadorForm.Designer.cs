﻿namespace Jaeger.UI.Forms.Validador {
    partial class ComprobanteValidadorForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComprobanteValidadorForm));
            this.Status = new System.Windows.Forms.StatusStrip();
            this.lblProgreso = new System.Windows.Forms.ToolStripStatusLabel();
            this.dataGridResult = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Registrado = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.TipoComprobante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Version = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmisorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmisorNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmisorRegimenFiscal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorRegimenFiscal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorDomicilioFiscal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaEmision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaTimbre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoCertificado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RFCProvCertif = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveMoneda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveMetodoPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveFormaPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveUsoCFDI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetencionISR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetencionIVA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetencionIEPS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrasladoIVA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrasladoIEPS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descuento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Situacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Resultado = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.ContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ContextRemover = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextVerXML = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextValidar = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.TValida = new Jaeger.UI.Forms.Validador.TComprobanteValdiadorControl();
            this.CopiarRuta = new System.Windows.Forms.ToolStripMenuItem();
            this.Status.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridResult)).BeginInit();
            this.ContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // Status
            // 
            this.Status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblProgreso});
            this.Status.Location = new System.Drawing.Point(0, 558);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(1227, 22);
            this.Status.SizingGrip = false;
            this.Status.TabIndex = 1;
            this.Status.Text = "statusStrip1";
            // 
            // lblProgreso
            // 
            this.lblProgreso.Name = "lblProgreso";
            this.lblProgreso.Size = new System.Drawing.Size(32, 17);
            this.lblProgreso.Text = "Listo";
            // 
            // dataGridResult
            // 
            this.dataGridResult.AllowUserToOrderColumns = true;
            this.dataGridResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Registrado,
            this.TipoComprobante,
            this.Version,
            this.Folio,
            this.Serie,
            this.EmisorRFC,
            this.EmisorNombre,
            this.EmisorRegimenFiscal,
            this.ReceptorRFC,
            this.ReceptorNombre,
            this.ReceptorRegimenFiscal,
            this.ReceptorDomicilioFiscal,
            this.FechaEmision,
            this.FechaTimbre,
            this.IdDocumento,
            this.Estado,
            this.NoCertificado,
            this.RFCProvCertif,
            this.ClaveMoneda,
            this.ClaveMetodoPago,
            this.ClaveFormaPago,
            this.ClaveUsoCFDI,
            this.RetencionISR,
            this.RetencionIVA,
            this.RetencionIEPS,
            this.TrasladoIVA,
            this.TrasladoIEPS,
            this.SubTotal,
            this.Descuento,
            this.Total,
            this.Situacion,
            this.Resultado,
            this.Column1,
            this.Column2});
            this.dataGridResult.ContextMenuStrip = this.ContextMenu;
            this.dataGridResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridResult.Location = new System.Drawing.Point(0, 25);
            this.dataGridResult.Name = "dataGridResult";
            this.dataGridResult.Size = new System.Drawing.Size(1227, 533);
            this.dataGridResult.TabIndex = 3;
            this.dataGridResult.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridResult_CellClick);
            this.dataGridResult.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DataGridResult_CellFormatting);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            this.Id.Width = 50;
            // 
            // Registrado
            // 
            this.Registrado.DataPropertyName = "Registrado";
            this.Registrado.HeaderText = "Reg";
            this.Registrado.Name = "Registrado";
            this.Registrado.ReadOnly = true;
            this.Registrado.Visible = false;
            this.Registrado.Width = 40;
            // 
            // TipoComprobante
            // 
            this.TipoComprobante.DataPropertyName = "TipoComprobante";
            this.TipoComprobante.HeaderText = "Tipo";
            this.TipoComprobante.Name = "TipoComprobante";
            this.TipoComprobante.ReadOnly = true;
            this.TipoComprobante.Width = 50;
            // 
            // Version
            // 
            this.Version.DataPropertyName = "Version";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Version.DefaultCellStyle = dataGridViewCellStyle1;
            this.Version.HeaderText = "Versión";
            this.Version.Name = "Version";
            this.Version.ReadOnly = true;
            this.Version.Width = 50;
            // 
            // Folio
            // 
            this.Folio.DataPropertyName = "Folio";
            this.Folio.HeaderText = "Folio";
            this.Folio.Name = "Folio";
            this.Folio.ReadOnly = true;
            // 
            // Serie
            // 
            this.Serie.DataPropertyName = "Serie";
            this.Serie.HeaderText = "Serie";
            this.Serie.Name = "Serie";
            this.Serie.ReadOnly = true;
            // 
            // EmisorRFC
            // 
            this.EmisorRFC.DataPropertyName = "EmisorRFC";
            this.EmisorRFC.HeaderText = "RFC (Emisor)";
            this.EmisorRFC.Name = "EmisorRFC";
            this.EmisorRFC.ReadOnly = true;
            // 
            // EmisorNombre
            // 
            this.EmisorNombre.DataPropertyName = "EmisorNombre";
            this.EmisorNombre.HeaderText = "Emisor";
            this.EmisorNombre.Name = "EmisorNombre";
            this.EmisorNombre.ReadOnly = true;
            this.EmisorNombre.Width = 200;
            // 
            // EmisorRegimenFiscal
            // 
            this.EmisorRegimenFiscal.DataPropertyName = "EmisorRegimenFiscal";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.EmisorRegimenFiscal.DefaultCellStyle = dataGridViewCellStyle2;
            this.EmisorRegimenFiscal.HeaderText = "Reg. Fiscal";
            this.EmisorRegimenFiscal.Name = "EmisorRegimenFiscal";
            this.EmisorRegimenFiscal.ReadOnly = true;
            this.EmisorRegimenFiscal.Width = 50;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.DataPropertyName = "ReceptorRFC";
            this.ReceptorRFC.HeaderText = "RFC (Receptor)";
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.ReadOnly = true;
            // 
            // ReceptorNombre
            // 
            this.ReceptorNombre.DataPropertyName = "ReceptorNombre";
            this.ReceptorNombre.HeaderText = "Receptor";
            this.ReceptorNombre.Name = "ReceptorNombre";
            this.ReceptorNombre.ReadOnly = true;
            this.ReceptorNombre.Width = 200;
            // 
            // ReceptorRegimenFiscal
            // 
            this.ReceptorRegimenFiscal.DataPropertyName = "ReceptorRegimenFiscal";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ReceptorRegimenFiscal.DefaultCellStyle = dataGridViewCellStyle3;
            this.ReceptorRegimenFiscal.HeaderText = "Reg. Fiscal (Receptor)";
            this.ReceptorRegimenFiscal.Name = "ReceptorRegimenFiscal";
            this.ReceptorRegimenFiscal.ReadOnly = true;
            this.ReceptorRegimenFiscal.Width = 50;
            // 
            // ReceptorDomicilioFiscal
            // 
            this.ReceptorDomicilioFiscal.DataPropertyName = "ReceptorDomicilioFiscal";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ReceptorDomicilioFiscal.DefaultCellStyle = dataGridViewCellStyle4;
            this.ReceptorDomicilioFiscal.HeaderText = "Dom. Fiscal (Receptor)";
            this.ReceptorDomicilioFiscal.Name = "ReceptorDomicilioFiscal";
            this.ReceptorDomicilioFiscal.ReadOnly = true;
            this.ReceptorDomicilioFiscal.Width = 50;
            // 
            // FechaEmision
            // 
            this.FechaEmision.DataPropertyName = "FechaEmision";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Format = "d";
            dataGridViewCellStyle5.NullValue = null;
            this.FechaEmision.DefaultCellStyle = dataGridViewCellStyle5;
            this.FechaEmision.HeaderText = "Fec. Emisión";
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.ReadOnly = true;
            this.FechaEmision.Width = 75;
            // 
            // FechaTimbre
            // 
            this.FechaTimbre.DataPropertyName = "FechaTimbre";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.Format = "d";
            this.FechaTimbre.DefaultCellStyle = dataGridViewCellStyle6;
            this.FechaTimbre.HeaderText = "Fec. Cert.";
            this.FechaTimbre.Name = "FechaTimbre";
            this.FechaTimbre.ReadOnly = true;
            this.FechaTimbre.Width = 75;
            // 
            // IdDocumento
            // 
            this.IdDocumento.DataPropertyName = "IdDocumento";
            this.IdDocumento.HeaderText = "IdDocumento";
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.ReadOnly = true;
            this.IdDocumento.Width = 200;
            // 
            // Estado
            // 
            this.Estado.DataPropertyName = "Estado";
            this.Estado.HeaderText = "Estado SAT";
            this.Estado.Name = "Estado";
            this.Estado.ReadOnly = true;
            this.Estado.Width = 65;
            // 
            // NoCertificado
            // 
            this.NoCertificado.DataPropertyName = "NoCertificadoProvCertif";
            this.NoCertificado.HeaderText = "No. Certificado";
            this.NoCertificado.Name = "NoCertificado";
            this.NoCertificado.ReadOnly = true;
            // 
            // RFCProvCertif
            // 
            this.RFCProvCertif.DataPropertyName = "RFCProvCertif";
            this.RFCProvCertif.HeaderText = "RFCProvCertif";
            this.RFCProvCertif.Name = "RFCProvCertif";
            this.RFCProvCertif.ReadOnly = true;
            // 
            // ClaveMoneda
            // 
            this.ClaveMoneda.DataPropertyName = "Moneda";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ClaveMoneda.DefaultCellStyle = dataGridViewCellStyle7;
            this.ClaveMoneda.HeaderText = "Moneda";
            this.ClaveMoneda.Name = "ClaveMoneda";
            this.ClaveMoneda.ReadOnly = true;
            this.ClaveMoneda.Width = 50;
            // 
            // ClaveMetodoPago
            // 
            this.ClaveMetodoPago.DataPropertyName = "MetodoPago";
            this.ClaveMetodoPago.HeaderText = "Met. Pago";
            this.ClaveMetodoPago.Name = "ClaveMetodoPago";
            this.ClaveMetodoPago.ReadOnly = true;
            this.ClaveMetodoPago.Width = 65;
            // 
            // ClaveFormaPago
            // 
            this.ClaveFormaPago.DataPropertyName = "FormaPago";
            this.ClaveFormaPago.HeaderText = "Forma Pago";
            this.ClaveFormaPago.Name = "ClaveFormaPago";
            this.ClaveFormaPago.ReadOnly = true;
            this.ClaveFormaPago.Width = 65;
            // 
            // ClaveUsoCFDI
            // 
            this.ClaveUsoCFDI.DataPropertyName = "UsoCFDI";
            this.ClaveUsoCFDI.HeaderText = "Uso CFDI";
            this.ClaveUsoCFDI.Name = "ClaveUsoCFDI";
            this.ClaveUsoCFDI.ReadOnly = true;
            this.ClaveUsoCFDI.Width = 65;
            // 
            // RetencionISR
            // 
            this.RetencionISR.DataPropertyName = "RetencionISR";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            this.RetencionISR.DefaultCellStyle = dataGridViewCellStyle8;
            this.RetencionISR.HeaderText = "Ret. ISR";
            this.RetencionISR.Name = "RetencionISR";
            this.RetencionISR.ReadOnly = true;
            this.RetencionISR.Width = 75;
            // 
            // RetencionIVA
            // 
            this.RetencionIVA.DataPropertyName = "RetencionIVA";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N2";
            this.RetencionIVA.DefaultCellStyle = dataGridViewCellStyle9;
            this.RetencionIVA.HeaderText = "Ret. IVA";
            this.RetencionIVA.Name = "RetencionIVA";
            this.RetencionIVA.ReadOnly = true;
            this.RetencionIVA.Width = 75;
            // 
            // RetencionIEPS
            // 
            this.RetencionIEPS.DataPropertyName = "RetencionIEPS";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N2";
            this.RetencionIEPS.DefaultCellStyle = dataGridViewCellStyle10;
            this.RetencionIEPS.HeaderText = "Ret. IEPS";
            this.RetencionIEPS.Name = "RetencionIEPS";
            this.RetencionIEPS.ReadOnly = true;
            this.RetencionIEPS.Width = 75;
            // 
            // TrasladoIVA
            // 
            this.TrasladoIVA.DataPropertyName = "TrasladoIVA";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N2";
            this.TrasladoIVA.DefaultCellStyle = dataGridViewCellStyle11;
            this.TrasladoIVA.HeaderText = "Tras. IVA";
            this.TrasladoIVA.Name = "TrasladoIVA";
            this.TrasladoIVA.ReadOnly = true;
            this.TrasladoIVA.Width = 75;
            // 
            // TrasladoIEPS
            // 
            this.TrasladoIEPS.DataPropertyName = "TrasladoIEPS";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "N2";
            this.TrasladoIEPS.DefaultCellStyle = dataGridViewCellStyle12;
            this.TrasladoIEPS.HeaderText = "Tras. IEPS";
            this.TrasladoIEPS.Name = "TrasladoIEPS";
            this.TrasladoIEPS.ReadOnly = true;
            this.TrasladoIEPS.Width = 75;
            // 
            // SubTotal
            // 
            this.SubTotal.DataPropertyName = "SubTotal";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "N2";
            this.SubTotal.DefaultCellStyle = dataGridViewCellStyle13;
            this.SubTotal.HeaderText = "SubTotal";
            this.SubTotal.Name = "SubTotal";
            this.SubTotal.ReadOnly = true;
            // 
            // Descuento
            // 
            this.Descuento.DataPropertyName = "Descuento";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.Format = "N2";
            this.Descuento.DefaultCellStyle = dataGridViewCellStyle14;
            this.Descuento.HeaderText = "Descuento";
            this.Descuento.Name = "Descuento";
            this.Descuento.ReadOnly = true;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle15.Format = "N2";
            this.Total.DefaultCellStyle = dataGridViewCellStyle15;
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // Situacion
            // 
            this.Situacion.DataPropertyName = "Situacion";
            this.Situacion.HeaderText = "Situación";
            this.Situacion.Name = "Situacion";
            this.Situacion.ReadOnly = true;
            // 
            // Resultado
            // 
            this.Resultado.DataPropertyName = "Resultado";
            this.Resultado.HeaderText = "Resultado";
            this.Resultado.Name = "Resultado";
            this.Resultado.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Resultado.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Resultado.Width = 65;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "XML";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 35;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "PDF";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 35;
            // 
            // ContextMenu
            // 
            this.ContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ContextRemover,
            this.ContextVerXML,
            this.ContextValidar,
            this.CopiarRuta});
            this.ContextMenu.Name = "ContextMenu";
            this.ContextMenu.Size = new System.Drawing.Size(192, 114);
            // 
            // ContextRemover
            // 
            this.ContextRemover.Name = "ContextRemover";
            this.ContextRemover.Size = new System.Drawing.Size(191, 22);
            this.ContextRemover.Text = "Remover";
            // 
            // ContextVerXML
            // 
            this.ContextVerXML.Name = "ContextVerXML";
            this.ContextVerXML.Size = new System.Drawing.Size(191, 22);
            this.ContextVerXML.Text = "Visualizar";
            // 
            // ContextValidar
            // 
            this.ContextValidar.Image = global::Jaeger.UI.Properties.Resources.protect_16px;
            this.ContextValidar.Name = "ContextValidar";
            this.ContextValidar.Size = new System.Drawing.Size(191, 22);
            this.ContextValidar.Text = "Validar";
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // TValida
            // 
            this.TValida.Dock = System.Windows.Forms.DockStyle.Top;
            this.TValida.Location = new System.Drawing.Point(0, 0);
            this.TValida.Name = "TValida";
            this.TValida.Size = new System.Drawing.Size(1227, 25);
            this.TValida.TabIndex = 2;
            // 
            // CopiarRuta
            // 
            this.CopiarRuta.Name = "CopiarRuta";
            this.CopiarRuta.Size = new System.Drawing.Size(191, 22);
            this.CopiarRuta.Text = "Copiar ruta de archivo";
            this.CopiarRuta.Click += new System.EventHandler(this.CopiarRuta_Click);
            // 
            // ComprobanteValidadorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1227, 580);
            this.Controls.Add(this.dataGridResult);
            this.Controls.Add(this.TValida);
            this.Controls.Add(this.Status);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ComprobanteValidadorForm";
            this.Text = "Validador de Comprobantes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ComprobanteValidadorForm_Load);
            this.Status.ResumeLayout(false);
            this.Status.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridResult)).EndInit();
            this.ContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip Status;
        private TComprobanteValdiadorControl TValida;
        private System.Windows.Forms.DataGridView dataGridResult;
        private System.Windows.Forms.ToolStripStatusLabel lblProgreso;
        private System.Windows.Forms.ContextMenuStrip ContextMenu;
        private System.Windows.Forms.ToolStripMenuItem ContextVerXML;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStripMenuItem ContextValidar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Registrado;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoComprobante;
        private System.Windows.Forms.DataGridViewTextBoxColumn Version;
        private System.Windows.Forms.DataGridViewTextBoxColumn Folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Serie;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmisorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmisorNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmisorRegimenFiscal;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorRegimenFiscal;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorDomicilioFiscal;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaEmision;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaTimbre;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoCertificado;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFCProvCertif;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveMoneda;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveMetodoPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveFormaPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveUsoCFDI;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetencionISR;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetencionIVA;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetencionIEPS;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrasladoIVA;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrasladoIEPS;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descuento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn Situacion;
        private System.Windows.Forms.DataGridViewButtonColumn Resultado;
        private System.Windows.Forms.DataGridViewImageColumn Column1;
        private System.Windows.Forms.DataGridViewImageColumn Column2;
        private System.Windows.Forms.ToolStripMenuItem ContextRemover;
        private System.Windows.Forms.ToolStripMenuItem CopiarRuta;
    }
}