﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Validador.Beta.Domain;
using Jaeger.Aplication.Validador.Beta.Services;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Forms.Validador {
    public partial class GuardarEnForm : Form {
        public BindingList<DocumentoFiscal> _DataSource;
        protected internal ReportePDF pdfValidacion = new ReportePDF();
        public GuardarEnForm(BindingList<DocumentoFiscal> dataSource) {
            InitializeComponent();
            this._DataSource = new BindingList<DocumentoFiscal>(dataSource);
        }

        private void DescargaForm_Load(object sender, EventArgs e) {

        }

        private void Examinar_Click(object sender, EventArgs e) {
            var folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK) {
                this.GuardarEn.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e) {
            if (!System.IO.Directory.Exists(this.GuardarEn.Text)) {
                return;
            }
            using (var espera = new WaitingForm(this.Procesar)) {
                espera.Text = "Guardando archivos ...";
                espera.ShowDialog(this);
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Procesar() {
            if (this._DataSource != null) {
                if (this._DataSource.Count > 0) {
                    for (int i = 0; i < this._DataSource.Count; i++) {
                        this._DataSource[i].MoveTo(this.GuardarEn.Text);
                        if (this.chkInluirValidacion.Checked) {
                            if (this._DataSource[i].Validacion != null) {
                                this.pdfValidacion.PathPDF = this.GuardarEn.Text;
                                this.pdfValidacion.Crear(new ComprobanteValidacionPrinter(this._DataSource[i].Validacion));
                            }
                        }
                    }
                }
            }
        }
    }
}
