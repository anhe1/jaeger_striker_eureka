﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Jaeger.Aplication.Repositorio.Contracts;
using Jaeger.Aplication.Repositorio.Services;
using Jaeger.Aplication.Validador.Beta.Domain;
using Jaeger.Aplication.Validador.Beta.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Validador {
    public partial class ComprobanteValidadorForm : Form {
        #region declaraciones
        protected internal ToolStripMenuItem PolizaModelb = new ToolStripMenuItem { Text = "Desde archivo" };
        protected internal IValidacionService validador = new ValidadorService();
        protected internal IComprobanteFiscalService comprobanteFiscal;
        #endregion

        private Progress<Administrador.Progreso> progreso;

        public ComprobanteValidadorForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void ComprobanteValidadorForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.dataGridResult.DataGridCommon();
            this.TValida.Administrador = new Administrador();
            this.comprobanteFiscal = ConfiguracionService.Manager.GetComprobantes();
            this.validador.RFC = ConfiguracionService.Contribuyente.RFC;
            
            this.TValida.Agregar.Click += TValida_Agregar_Click;
            this.TValida.Remover.Click += TValida_Remover_Click;
            this.TValida.Actualizar.Click += TValida_Actualizar_Click;
            this.TValida.Validar.Click += TValida_Validar_Click;
            this.TValida.Configuracion.Click += TValida_Configuracion_Click;
            this.TValida.Cerrar.Click += TValida_Cerrar_Click;
            this.TValida.ValidacionPDF.Click += this.TValida_ValidacionPDF_Click;
            this.TValida.ValidacionImprimir.Click += TValida_Imprimir_Click;
            this.TValida.ExportarExcel.Click += TValida_ExportarExcel_Click;
            this.TValida.ExportarTemplete.Click += TValida_ExportarTemplete_Click;
            this.TValida.Backup.Click += this.TValida_Backup_Click;
            this.TValida.PDFBuscar.Click += this.TValdia_PDFBuscar_Click;
            this.TValida.PDFAsignar.Click += this.TValdia_PDFAsignar_Click;
            this.ContextVerXML.Click += ContextVerXML_Click;
            this.ContextValidar.Click += this.ContextValidar_Click;
            this.ContextMenu.Items.Add(this.TValida.PDF);
        }
        

        #region barra de herramientas
        private void TValida_Agregar_Click(object sender, EventArgs e) {
            using (var _openFile = new OpenFileDialog() { Filter = "*.xml|*.XML" }) {
                if (_openFile.ShowDialog(this) == DialogResult.OK) {
                    this.TValida.Agregar.Enabled = false;
                    this.TValida.Administrador.Agregar(_openFile.FileName);
                    this.TValida.Agregar.Enabled = true;
                    this.dataGridResult.DataSource = this.TValida.Administrador._DataSource;
                }
            }
        }

        private void TValida_Remover_Click(object sender, EventArgs e) {
            if (this.dataGridResult.CurrentRow != null) {
                var seleccionado = this.dataGridResult.CurrentRow.DataBoundItem as DocumentoFiscal;
                if (seleccionado != null) {
                    this.TValida.Administrador.Remover(seleccionado);
                }
            }
        }

        private void TValida_Actualizar_Click(object sender, EventArgs e) {
            if (!Directory.Exists(this.TValida.Carpeta.Text)) {
                MessageBox.Show(this, string.Format("No se puede encontrar una parte de la ruta de acceso '{0}'", this.TValida.Carpeta.Text), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            this.TValida.Administrador._DataSource.Clear();
            using (var espera = new WaitingForm(this.Cargar)) {
                espera.Text = "Leyendo archivos ...";
                espera.ShowDialog(this);
            }
            //this.progreso= new Progress<Administrador.Progreso>();
            //this.progreso.ProgressChanged += Progreso_ProgressChanged;
            //await this.TValida.Administrador.SearchParallelAsync(this.TValida.Carpeta.Text, System.IO.SearchOption.AllDirectories, this.progreso);
            this.dataGridResult.DataSource = this.TValida.Administrador._DataSource;
            this.lblProgreso.Text = string.Format("Archivos: {0}", this.TValida.Administrador._DataSource.Count);
            if (this.TValida.Administrador._DataError.Count > 0) {
                var conError = new ErroresForm();
                conError.GridDataError.DataSource = this.TValida.Administrador._DataError;
                conError.ShowDialog(this);
            }
        }

        private void Progreso_ProgressChanged(object sender, Administrador.Progreso e) {
            this.lblProgreso.Text = e.Caption;
        }

        private void TValida_Imprimir_Click(object sender, EventArgs e) {
            if (this.dataGridResult.CurrentRow != null) {
                var seleccionado = this.dataGridResult.CurrentRow.DataBoundItem as DocumentoFiscal;
                if (seleccionado != null) {
                    if (seleccionado.Validacion != null) {
                        var reporte = new ReporteForm(new ComprobanteValidacionPrinter(seleccionado.Validacion));
                        reporte.ShowDialog(this);
                    }
                }
            }
        }

        private void TValida_ValidacionPDF_Click(object sender, EventArgs e) {
            var folder = new FolderBrowserDialog() { Description = "Selecciona ruta de descarga" };
            if (folder.ShowDialog(this) != DialogResult.OK)
                return;

            if (Directory.Exists(folder.SelectedPath) == false) {
                MessageBox.Show(this, "No se encontro una ruta de descarga valida!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            this.TValida.ValidacionPDF.Tag = folder.SelectedPath;

            using (var espera = new WaitingForm(this.PDFValidacion)) {
                espera.Text = "Procesando ...";
                espera.ShowDialog(this);
            }
        }

        private void TValida_Validar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.ProcesarTodo1)) {
                espera.Text = "Validando...";
                espera.ShowDialog(this);
            };
        }

        private void TValida_Configuracion_Click(object sender, EventArgs e) {
            var configuracion = new ConfiguracionForm();
            configuracion.ShowDialog(this);
        }

        private void TValida_ExportarExcel_Click(object sender, EventArgs e) {
            var saveFile = new SaveFileDialog() { Filter = "*.xlsx|*xlsx", DefaultExt = "xlsx", AddExtension = true };
            if (saveFile.ShowDialog() == DialogResult.OK) {
                if (Util.Services.FileService.IsFileinUse(saveFile.FileName)) {
                    MessageBox.Show(this, "El archivo seleccionado actualmente se encuentra en uso.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                this.TValida.Administrador.ExportarExcel(saveFile.FileName);
            }
        }

        private void TValida_ExportarTemplete_Click(object sender, EventArgs eventArgs) {
            var openFile = new OpenFileDialog() { Filter = "*.xlsx|*xlsx", DefaultExt = "xlsx", AddExtension = true, Title = "Selecciona un templete EXCEL" };
            if (openFile.ShowDialog() == DialogResult.OK) {
                if (Util.Services.FileService.IsFileinUse(openFile.FileName)) {
                    MessageBox.Show(this, "El archivo seleccionado actualmente se encuentra en uso.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                var saveFile = new SaveFileDialog() { Filter = "*.xlsx|*xlsx", DefaultExt = "xlsx", AddExtension = true, Title = "Nombre del archivo de exportación", FileName = "Reporte_Validacion_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") };
                if (saveFile.ShowDialog() == DialogResult.OK) {
                    if (Util.Services.FileService.IsFileinUse(saveFile.FileName)) {
                        MessageBox.Show(this, "El archivo seleccionado actualmente se encuentra en uso.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    this.TValida.Administrador.ExportarExcel(saveFile.FileName, openFile.FileName );
                }
            }
        }

        private void TValida_Backup_Click(object sender, EventArgs eventArgs) {
            
        }

        private void TValdia_PDFBuscar_Click(object sender, EventArgs e) {
            var seleccionado = this.dataGridResult.CurrentRow.DataBoundItem as DocumentoFiscal;
            if (seleccionado != null) {
                var resultado = Administrador.BuscarPDF(new FileInfo(seleccionado.PathXML), seleccionado.IdDocumento);
                if (string.IsNullOrEmpty(resultado)) {
                    MessageBox.Show(this, "No se encontro");
                } else {
                    MessageBox.Show(this, resultado);
                    seleccionado.PathPDF = resultado;
                }
            }
        }

        private void TValdia_PDFAsignar_Click(object sender, EventArgs e) {
            var seleccionado = this.dataGridResult.CurrentRow.DataBoundItem as DocumentoFiscal;
            if (seleccionado != null) {
                var openFile = new OpenFileDialog() { Filter = "*.pdf|*.PDF" };
                if (openFile.ShowDialog(this) == DialogResult.OK) {
                    seleccionado.PathPDF = openFile.FileName;
                }
            }
        }

        private void TValida_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ContextValidar_Click(object sender, EventArgs e) {
            var seleccionado = this.dataGridResult.CurrentRow.DataBoundItem as DocumentoFiscal;
            if (seleccionado != null) {
                using (var espera = new WaitingForm(this.ProcesarSeleccionado)) {
                    espera.Text = "Validando...";
                    espera.ShowDialog(this);
                }
            }
        }

        private void ContextVerXML_Click(object sender, EventArgs e) {
            var seleccionado = this.dataGridResult.CurrentRow.DataBoundItem as DocumentoFiscal;
            if (seleccionado != null) {
                if (Jaeger.Domain.Services.ValidacionService.IsBase64String(seleccionado.XmlContentB64)) {
                    var mostrar = new ComprobanteTreeViewForm(seleccionado.XmlContentB64, seleccionado.KeyName) { MdiParent = this.ParentForm };
                    mostrar.Show();
                } else {
                    MessageBox.Show(this, "No es un formato válido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        #endregion

        #region acciones del grid
        private void DataGridResult_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e) {
            if (this.dataGridResult.Columns[e.ColumnIndex].Name == "Column1") {
                var seleccionado = this.dataGridResult.Rows[e.RowIndex].DataBoundItem as DocumentoFiscal;
                if (seleccionado != null) {
                    if (!string.IsNullOrEmpty(seleccionado.XmlContentB64)) {
                        Image bitmap = new Bitmap(Properties.Resources.xml_16px);
                        e.Value = bitmap;
                    }
                }
            } else if (this.dataGridResult.Columns[e.ColumnIndex].Name == "Column2") {
                var seleccionado = this.dataGridResult.Rows[e.RowIndex].DataBoundItem as DocumentoFiscal;
                if (seleccionado != null) {
                    if (!string.IsNullOrEmpty(seleccionado.PathPDF)) {
                        Image bitmap = new Bitmap(Properties.Resources.pdf_16px);
                        e.Value = bitmap;
                    } else {
                        Image bitmap = new Bitmap(Properties.Resources.close_window_16px);
                        e.Value = bitmap;
                    }
                }
            }

        }

        private void DataGridResult_CellClick(object sender, DataGridViewCellEventArgs e) {
            if (this.dataGridResult.Columns[e.ColumnIndex].Name == "Resultado") {
                this.ContextValidar.PerformClick();
            }
        }
        #endregion

        #region metodos privados
        private void Cargar() {
            this.progreso = new Progress<Administrador.Progreso>();
            this.progreso.ProgressChanged += Progreso_ProgressChanged;
            this.TValida.Administrador.SearchNormal(this.TValida.Carpeta.Text, SearchOption.AllDirectories, this.progreso);
        }

        private void ProcesarSeleccionado() {
            var seleccionado = this.dataGridResult.CurrentRow.DataBoundItem as DocumentoFiscal;
            this.validador.Procesar(seleccionado);
        }

        private async void ProcesarTodo() {
            this.progreso = new Progress<Administrador.Progreso>();
            this.progreso.ProgressChanged += Progreso_ProgressChanged;
            //await this.validador2.ValidarParallelAsync(this.TValida.Administrador._DataSource, this.progreso);
        }

        private void ProcesarTodo1() {
            this.progreso = new Progress<Administrador.Progreso>();
            this.progreso.ProgressChanged += Progreso_ProgressChanged;
            IProgress<Administrador.Progreso> d1 = this.progreso;
            var reporte = new Administrador.Progreso();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < this.TValida.Administrador._DataSource.Count; i++) {
                reporte.Caption = "Procesando: " + this.TValida.Administrador._DataSource[i].PathXML;
                d1.Report(reporte);
                this.validador.Procesar(this.TValida.Administrador._DataSource[i]);
            }
            stopwatch.Stop();
            TimeSpan elapsed = stopwatch.Elapsed;
            reporte.Caption = string.Format("{0:00}:{1:00}:{2:00}", elapsed.Hours, elapsed.Minutes, elapsed.Seconds);
            d1.Report(reporte);
        }

        private void PDFValidacion() {
            var pdf = new ReportePDF {
                PathPDF = (string)this.TValida.ValidacionPDF.Tag
            };
            var seleccionado = this.dataGridResult.CurrentRow.DataBoundItem as DocumentoFiscal;
            if (seleccionado != null) {
                if (seleccionado.Validacion != null) {
                    pdf.Crear(new ComprobanteValidacionPrinter(seleccionado.Validacion));
                } else {
                    
                }
            }
        }
        #endregion

        private void CopiarRuta_Click(object sender, EventArgs e) {
            if (this.dataGridResult.CurrentRow != null) {
                var seleccionado = this.dataGridResult.CurrentRow.DataBoundItem as DocumentoFiscal;
                if (seleccionado != null) {
                    Clipboard.SetText(seleccionado.XmlContentB64);
                }
            }
        }
    }
}
