﻿using Jaeger.Aplication.Validador.Beta.Domain;
using System;
using System.Windows.Forms;

namespace Jaeger.UI.Forms.Validador {
    public partial class ConfiguracionForm : Form {
        private ValidaConfiguracion configuracion;

        public ConfiguracionForm(ValidaConfiguracion configuracion) {
            InitializeComponent();
            this.configuracion = configuracion;
        }

        public ConfiguracionForm() {
            InitializeComponent();
        }

        private void ConfiguracionForm_Load(object sender, EventArgs e) {
            if (configuracion == null) {
                this.configuracion = new ValidaConfiguracion();
            }
            this.CreateBinding();
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Guardar_Click(object sender, EventArgs e) {

        }

        private void CreateBinding() {
            this.ValidarXSD.DataBindings.Clear();
            this.ValidarXSD.DataBindings.Add("Checked", this.configuracion, "ValidarXSD", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ValidarClaveConcepto.DataBindings.Clear();
            this.ValidarClaveConcepto.DataBindings.Add("Checked", this.configuracion, "ValidarClaveConcepto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ValidarClaveUsoCFDI.DataBindings.Clear();
            this.ValidarClaveUsoCFDI.DataBindings.Add("Checked", this.configuracion, "ValidarClaveUsoCFDI", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ValidarClaveFormaPago.DataBindings.Clear();
            this.ValidarClaveFormaPago.DataBindings.Add("Checked", this.configuracion, "ValidarClaveFormaPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ValidarClaveMetodoPago.DataBindings.Clear();
            this.ValidarClaveMetodoPago.DataBindings.Add("Checked", this.configuracion, "ValidarClaveMetodoPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RequiereFormaYMetodoPago.DataBindings.Clear();
            this.RequiereFormaYMetodoPago.DataBindings.Add("Checked", this.configuracion, "RequiereFormaYMetodoPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.VerificarEstadoSAT.DataBindings.Clear();
            this.VerificarEstadoSAT.DataBindings.Add("Checked", this.configuracion, "VerificarEstadoSAT", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ValidarAddendaSin.DataBindings.Clear();
            this.ValidarAddendaSin.DataBindings.Add("Checked", this.configuracion, "ValidarAddendaSin", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ValidarSelloCFDI.DataBindings.Clear();
            this.ValidarSelloCFDI.DataBindings.Add("Checked", this.configuracion, "ValidarSelloCFDI", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ValidarSelloSAT.DataBindings.Clear();
            this.ValidarSelloSAT.DataBindings.Add("Checked", this.configuracion, "ValidarSelloSAT", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ValidaAritmetica.DataBindings.Clear();
            this.ValidaAritmetica.DataBindings.Add("Checked", this.configuracion, "ValidaAritmetica", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ObtenerEmisor.DataBindings.Clear();
            this.ObtenerEmisor.DataBindings.Add("Checked", this.configuracion, "ObtenerEmisor", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ModoParalelo.DataBindings.Clear();
            this.ModoParalelo.DataBindings.Add("Checked", this.configuracion, "ModoParalelo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.AsociarPDF.DataBindings.Clear();
            this.AsociarPDF.DataBindings.Add("Checked", this.configuracion, "AsociarPDF", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RenombrarOrigen.DataBindings.Clear();
            this.RenombrarOrigen.DataBindings.Add("Checked", this.configuracion, "RenombrarOrigen", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RegistrarComprobante.DataBindings.Clear();
            this.RegistrarComprobante.DataBindings.Add("Checked", this.configuracion, "RegistrarComprobante", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RegistrarConceptos.DataBindings.Clear();
            this.RegistrarConceptos.DataBindings.Add("Checked", this.configuracion, "RegistrarConceptos", true, DataSourceUpdateMode.OnPropertyChanged);

            this.DescargaCertificado.DataBindings.Clear();
            this.DescargaCertificado.DataBindings.Add("Checked", this.configuracion, "DescargaCertificado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.VerificarCodificacionUTF8.DataBindings.Clear();
            this.VerificarCodificacionUTF8.DataBindings.Add("Checked", this.configuracion, "VerificarCodificacionUTF8", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ValidarArticulo68.DataBindings.Clear();
            this.ValidarArticulo68.DataBindings.Add("Checked", this.configuracion, "ValidarClaveConcepto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.VerificarEmisorCP.DataBindings.Clear();
            this.VerificarEmisorCP.DataBindings.Add("Checked", this.configuracion, "VerificarEmisorCP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.LugarExpedicion32B.DataBindings.Clear();
            this.LugarExpedicion32B.DataBindings.Add("Checked", this.configuracion, "LugarExpedicion32B", true, DataSourceUpdateMode.OnPropertyChanged);

            this.LugarExpedicion32.DataBindings.Clear();
            this.LugarExpedicion32.DataBindings.Add("Text", this.configuracion, "LugarExpedicion32", true, DataSourceUpdateMode.OnPropertyChanged);

            this.SoloCFDIValido.DataBindings.Clear();
            this.SoloCFDIValido.DataBindings.Add("Checked", this.configuracion, "SoloCFDIValido", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void RegistrarComprobante_CheckStateChanged(object sender, EventArgs e) {
            if (RegistrarComprobante.Checked) {
                this.RegistrarConceptos.Enabled = true;
                this.SoloCFDIValido.Enabled = true;
            } else {
                this.RegistrarConceptos.Enabled = false;
                this.RegistrarConceptos.Checked = false;
                this.SoloCFDIValido.Enabled = false;
                this.SoloCFDIValido.Checked = false;
            }
        }

        private void LugarExpedicion32B_CheckStateChanged(object sender, EventArgs e) {
            if (LugarExpedicion32B.Checked) {
                this.LugarExpedicion32.Enabled = true;
            } else {
                this.LugarExpedicion32.Enabled = false;
            }
        }
    }
}
