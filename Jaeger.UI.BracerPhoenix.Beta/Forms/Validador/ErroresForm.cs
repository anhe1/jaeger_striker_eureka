﻿using System;
using System.Windows.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Validador {
    public partial class ErroresForm : Form {
        public ErroresForm() {
            InitializeComponent();
        }

        private void ErroresForm_Load(object sender, EventArgs e) {
            this.GridDataError.AutoGenerateColumns = false;
            this.GridDataError.DataGridCommon(1);
            this.lblArchivos.Text = string.Format(this.lblArchivos.Text, GridDataError.RowCount);
        }

        private void GridDataError_KeyDown(object sender, KeyEventArgs e) {
            if ((e.KeyCode == Keys.C) && (e.Modifiers == Keys.Control)) {
                Clipboard.SetText(this.GridDataError.CurrentCell.Value.ToString());
                e.Handled = true;
            }
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void btnCopiar_Click(object sender, EventArgs e) {
            this.GridDataError.CopyDataGridViewToClipboard();
            MessageBox.Show("Se copio el resultado al porta papeles");
        }
    }
}
