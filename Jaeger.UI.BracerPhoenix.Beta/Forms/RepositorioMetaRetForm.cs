﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms {
    public partial class RepositorioMetaRetForm : Form {
        private List<Domain.Repositorio.Entities.RetencionMeta> lista;
        private Aplication.Repositorio.Contracts.IMetaDataService service;
        private string Archivo;

        public RepositorioMetaRetForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void RepositorioMetaRetForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.dataGrid.DataGridCommon();
            this.service = new Aplication.Repositorio.Services.MetaDataService();
            this.TMeta.Actualizar.Text = "Abrir";
            this.TMeta.Actualizar.Click += Actualizar_Click;
            this.TMeta.Cerrar.Click += Cerrar_Click;
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Actualizar_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog() { Filter = "*.txt|*.TXT" };
            if (openFile.ShowDialog() != DialogResult.OK)
                return;
            this.Archivo = openFile.FileName;
            using (var espera = new WaitingForm(this.Cargar)) {
                espera.Text = "Cargando archivo...";
                espera.ShowDialog(this);
            }
            this.lblInformacion.Text = openFile.FileName;
            if (lista != null) {
                if (lista.Count > 0) {
                    this.dataGrid.DataSource = lista;
                    this.lblFilas.Text = string.Format("Filas: {0}", lista.Count);
                }
            } else {
                MessageBox.Show(this, "Error al cargar el archivo", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Cargar() {
            lista = service.GetRetencionMETA(this.Archivo);
        }
    }
}
