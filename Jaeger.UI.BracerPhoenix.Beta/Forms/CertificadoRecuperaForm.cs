﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Validador.Beta.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms {
    public partial class CertificadoRecuperaForm : Form {
        private DownloaderCertificadoSAT downloader;

        public CertificadoRecuperaForm(UIMenuElement uIMenuElement) {
            InitializeComponent();
        }

        private void CertificadoRecuperaForm_Load(object sender, EventArgs e) {
            this.downloader = new DownloaderCertificadoSAT();    
        }

        private void btnCargar_Click(object sender, EventArgs e) {
            if (this.downloader.ValidaNumeroSerie(this.txtNumeroDeSerie.Text)) {
                var b64 = this.downloader.DescargarArchivo(this.txtNumeroDeSerie.Text);
                if (Jaeger.Domain.Services.ValidacionService.IsBase64String(b64)) {
                    var fileSave = new SaveFileDialog { Title = "Guardar Certificado", AddExtension = true, DefaultExt = "cer", Filter = "*.cer|*.CER" };
                    if (fileSave.ShowDialog(this) == DialogResult.OK) {
                        Jaeger.Util.Services.FileService.WriteFileB64(b64, fileSave.FileName);
                    }
                }
            } else {
                MessageBox.Show("Número de serie no válido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
