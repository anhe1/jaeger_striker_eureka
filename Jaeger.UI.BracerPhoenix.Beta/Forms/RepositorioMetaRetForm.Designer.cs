﻿namespace Jaeger.UI.Forms {
    partial class RepositorioMetaRetForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblFilas = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusBar = new System.Windows.Forms.StatusStrip();
            this.lblInformacion = new System.Windows.Forms.ToolStripStatusLabel();
            this.TMeta = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.dataGrid = new System.Windows.Forms.DataGridView();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmisorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Emisor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Receptor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaEmision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaCertificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RFCProvCertif = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MontoTotalOperacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MontoTotalRetencion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaCancela = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFilas
            // 
            this.lblFilas.Name = "lblFilas";
            this.lblFilas.Size = new System.Drawing.Size(0, 17);
            // 
            // StatusBar
            // 
            this.StatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblInformacion,
            this.lblFilas});
            this.StatusBar.Location = new System.Drawing.Point(0, 428);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Size = new System.Drawing.Size(800, 22);
            this.StatusBar.TabIndex = 6;
            this.StatusBar.Text = "statusStrip1";
            // 
            // lblInformacion
            // 
            this.lblInformacion.Name = "lblInformacion";
            this.lblInformacion.Size = new System.Drawing.Size(0, 17);
            // 
            // TMeta
            // 
            this.TMeta.Dock = System.Windows.Forms.DockStyle.Top;
            this.TMeta.Etiqueta = "Selecciona el archivo";
            this.TMeta.Location = new System.Drawing.Point(0, 0);
            this.TMeta.Name = "TMeta";
            this.TMeta.ShowActualizar = true;
            this.TMeta.ShowCerrar = true;
            this.TMeta.ShowEditar = false;
            this.TMeta.ShowGuardar = false;
            this.TMeta.ShowHerramientas = true;
            this.TMeta.ShowImprimir = false;
            this.TMeta.ShowNuevo = false;
            this.TMeta.ShowRemover = false;
            this.TMeta.Size = new System.Drawing.Size(800, 25);
            this.TMeta.TabIndex = 4;
            // 
            // dataGrid
            // 
            this.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Estado,
            this.EmisorRFC,
            this.Emisor,
            this.ReceptorRFC,
            this.Receptor,
            this.IdDocumento,
            this.FechaEmision,
            this.FechaCertificacion,
            this.RFCProvCertif,
            this.MontoTotalOperacion,
            this.MontoTotalRetencion,
            this.FechaCancela});
            this.dataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGrid.Location = new System.Drawing.Point(0, 25);
            this.dataGrid.Name = "dataGrid";
            this.dataGrid.Size = new System.Drawing.Size(800, 403);
            this.dataGrid.TabIndex = 5;
            // 
            // Estado
            // 
            this.Estado.DataPropertyName = "Estado";
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            this.Estado.Width = 65;
            // 
            // EmisorRFC
            // 
            this.EmisorRFC.DataPropertyName = "EmisorRFC";
            this.EmisorRFC.HeaderText = "Emisor RFC";
            this.EmisorRFC.Name = "EmisorRFC";
            // 
            // Emisor
            // 
            this.Emisor.DataPropertyName = "Emisor";
            this.Emisor.HeaderText = "Emisor";
            this.Emisor.Name = "Emisor";
            this.Emisor.Width = 200;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.DataPropertyName = "ReceptorRFC";
            this.ReceptorRFC.HeaderText = "Receptor RFC";
            this.ReceptorRFC.Name = "ReceptorRFC";
            // 
            // Receptor
            // 
            this.Receptor.DataPropertyName = "Receptor";
            this.Receptor.HeaderText = "Receptor";
            this.Receptor.Name = "Receptor";
            this.Receptor.Width = 200;
            // 
            // IdDocumento
            // 
            this.IdDocumento.DataPropertyName = "IdDocumento";
            this.IdDocumento.HeaderText = "IdDocumento (UUID)";
            this.IdDocumento.MaxInputLength = 36;
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.Width = 215;
            // 
            // FechaEmision
            // 
            this.FechaEmision.DataPropertyName = "FechaEmision";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Format = "dd MMM yy";
            dataGridViewCellStyle1.NullValue = null;
            this.FechaEmision.DefaultCellStyle = dataGridViewCellStyle1;
            this.FechaEmision.HeaderText = "Fecha Emisión";
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.Width = 75;
            // 
            // FechaCertificacion
            // 
            this.FechaCertificacion.DataPropertyName = "FechaCertificacion";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "dd MMM yy";
            this.FechaCertificacion.DefaultCellStyle = dataGridViewCellStyle2;
            this.FechaCertificacion.HeaderText = "Fecha Cerfificación";
            this.FechaCertificacion.Name = "FechaCertificacion";
            this.FechaCertificacion.Width = 75;
            // 
            // RFCProvCertif
            // 
            this.RFCProvCertif.DataPropertyName = "RFCProvCertif";
            this.RFCProvCertif.HeaderText = "PAC";
            this.RFCProvCertif.Name = "RFCProvCertif";
            this.RFCProvCertif.ReadOnly = true;
            // 
            // MontoTotalOperacion
            // 
            this.MontoTotalOperacion.DataPropertyName = "MontoTotalOperacion";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.MontoTotalOperacion.DefaultCellStyle = dataGridViewCellStyle3;
            this.MontoTotalOperacion.HeaderText = "Total de la operación";
            this.MontoTotalOperacion.Name = "MontoTotalOperacion";
            this.MontoTotalOperacion.ToolTipText = "Monto total de la operación";
            this.MontoTotalOperacion.Width = 85;
            // 
            // MontoTotalRetencion
            // 
            this.MontoTotalRetencion.DataPropertyName = "MontoTotalRetencion";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            this.MontoTotalRetencion.DefaultCellStyle = dataGridViewCellStyle4;
            this.MontoTotalRetencion.HeaderText = "Total de la Retención";
            this.MontoTotalRetencion.Name = "MontoTotalRetencion";
            this.MontoTotalRetencion.ReadOnly = true;
            this.MontoTotalRetencion.ToolTipText = "Monto total de la retención";
            this.MontoTotalRetencion.Width = 85;
            // 
            // FechaCancela
            // 
            this.FechaCancela.DataPropertyName = "FechaCancela";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Format = "dd MMM yy";
            this.FechaCancela.DefaultCellStyle = dataGridViewCellStyle5;
            this.FechaCancela.HeaderText = "Fec. Cancelación";
            this.FechaCancela.Name = "FechaCancela";
            this.FechaCancela.Width = 75;
            // 
            // RepositorioMetaRetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGrid);
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.TMeta);
            this.Name = "RepositorioMetaRetForm";
            this.Text = "Retención META";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RepositorioMetaRetForm_Load);
            this.StatusBar.ResumeLayout(false);
            this.StatusBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripStatusLabel lblFilas;
        private System.Windows.Forms.StatusStrip StatusBar;
        private System.Windows.Forms.ToolStripStatusLabel lblInformacion;
        private Common.Forms.ToolBarStandarControl TMeta;
        private System.Windows.Forms.DataGridView dataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmisorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Emisor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Receptor;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaEmision;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaCertificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFCProvCertif;
        private System.Windows.Forms.DataGridViewTextBoxColumn MontoTotalOperacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn MontoTotalRetencion;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaCancela;
    }
}