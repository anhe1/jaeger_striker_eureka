﻿namespace Jaeger.UI.Forms {
    partial class SessionForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SessionForm));
            this.gridData = new System.Windows.Forms.DataGridView();
            this.TSesion = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.gcRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            this.SuspendLayout();
            // 
            // gridData
            // 
            this.gridData.AllowUserToAddRows = false;
            this.gridData.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gcRFC,
            this.gcNombre});
            this.gridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridData.Location = new System.Drawing.Point(0, 25);
            this.gridData.Name = "gridData";
            this.gridData.ReadOnly = true;
            this.gridData.RowHeadersVisible = false;
            this.gridData.Size = new System.Drawing.Size(439, 292);
            this.gridData.TabIndex = 2;
            this.gridData.DoubleClick += new System.EventHandler(this.GridData_DoubleClick);
            // 
            // TSesion
            // 
            this.TSesion.Dock = System.Windows.Forms.DockStyle.Top;
            this.TSesion.Etiqueta = "";
            this.TSesion.Location = new System.Drawing.Point(0, 0);
            this.TSesion.Name = "TSesion";
            this.TSesion.ShowActualizar = true;
            this.TSesion.ShowCerrar = true;
            this.TSesion.ShowEditar = true;
            this.TSesion.ShowGuardar = true;
            this.TSesion.ShowHerramientas = false;
            this.TSesion.ShowImprimir = false;
            this.TSesion.ShowNuevo = true;
            this.TSesion.ShowRemover = true;
            this.TSesion.Size = new System.Drawing.Size(439, 25);
            this.TSesion.TabIndex = 3;
            // 
            // gcRFC
            // 
            this.gcRFC.DataPropertyName = "RFC";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.gcRFC.DefaultCellStyle = dataGridViewCellStyle2;
            this.gcRFC.HeaderText = "RFC";
            this.gcRFC.Name = "gcRFC";
            this.gcRFC.ReadOnly = true;
            // 
            // gcNombre
            // 
            this.gcNombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.gcNombre.DataPropertyName = "RazonSocial";
            this.gcNombre.HeaderText = "Razón Social";
            this.gcNombre.Name = "gcNombre";
            this.gcNombre.ReadOnly = true;
            // 
            // SessionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 317);
            this.Controls.Add(this.gridData);
            this.Controls.Add(this.TSesion);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SessionForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sesión";
            this.Load += new System.EventHandler(this.SessionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridData;
        private Common.Forms.ToolBarStandarControl TSesion;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcNombre;
    }
}