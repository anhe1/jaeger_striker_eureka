﻿namespace Jaeger.UI.Forms {
    partial class ComprobantesNominaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gridData = new System.Windows.Forms.DataGridView();
            this.TipoComprobante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmisorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Emisor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Receptor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaEmision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaCerfificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PACCertifica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaStatusCancela = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaValidacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetencionISR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetencionIVA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetencionIEPS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrasladoIVA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrasladoIEPS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descuento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TComprobante = new Jaeger.UI.Common.Forms.ToolBar1CommonControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            this.SuspendLayout();
            // 
            // gridData
            // 
            this.gridData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TipoComprobante,
            this.Estado,
            this.Folio,
            this.Serie,
            this.EmisorRFC,
            this.Emisor,
            this.ReceptorRFC,
            this.Receptor,
            this.FechaEmision,
            this.FechaCerfificacion,
            this.IdDocumento,
            this.PACCertifica,
            this.FechaStatusCancela,
            this.FechaValidacion,
            this.RetencionISR,
            this.RetencionIVA,
            this.RetencionIEPS,
            this.TrasladoIVA,
            this.TrasladoIEPS,
            this.SubTotal,
            this.Descuento,
            this.Total});
            this.gridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridData.Location = new System.Drawing.Point(0, 25);
            this.gridData.Name = "gridData";
            this.gridData.Size = new System.Drawing.Size(800, 425);
            this.gridData.TabIndex = 3;
            // 
            // TipoComprobante
            // 
            this.TipoComprobante.DataPropertyName = "Tipo";
            this.TipoComprobante.HeaderText = "Tipo";
            this.TipoComprobante.Name = "TipoComprobante";
            this.TipoComprobante.ReadOnly = true;
            this.TipoComprobante.Width = 65;
            // 
            // Estado
            // 
            this.Estado.DataPropertyName = "Estado";
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            this.Estado.ReadOnly = true;
            this.Estado.Width = 65;
            // 
            // Folio
            // 
            this.Folio.DataPropertyName = "Folio";
            this.Folio.HeaderText = "Folio";
            this.Folio.Name = "Folio";
            this.Folio.ReadOnly = true;
            this.Folio.Width = 50;
            // 
            // Serie
            // 
            this.Serie.DataPropertyName = "Serie";
            this.Serie.HeaderText = "Serie";
            this.Serie.Name = "Serie";
            this.Serie.ReadOnly = true;
            this.Serie.Width = 50;
            // 
            // EmisorRFC
            // 
            this.EmisorRFC.DataPropertyName = "EmisorRFC";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.EmisorRFC.DefaultCellStyle = dataGridViewCellStyle1;
            this.EmisorRFC.HeaderText = "RFC (Emisor)";
            this.EmisorRFC.Name = "EmisorRFC";
            this.EmisorRFC.ReadOnly = true;
            // 
            // Emisor
            // 
            this.Emisor.DataPropertyName = "Emisor";
            this.Emisor.HeaderText = "Emisor";
            this.Emisor.Name = "Emisor";
            this.Emisor.ReadOnly = true;
            this.Emisor.Width = 175;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.DataPropertyName = "ReceptorRFC";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ReceptorRFC.DefaultCellStyle = dataGridViewCellStyle2;
            this.ReceptorRFC.HeaderText = "RFC (Receptor)";
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.ReadOnly = true;
            // 
            // Receptor
            // 
            this.Receptor.DataPropertyName = "Receptor";
            this.Receptor.HeaderText = "Receptor";
            this.Receptor.Name = "Receptor";
            this.Receptor.ReadOnly = true;
            this.Receptor.Width = 175;
            // 
            // FechaEmision
            // 
            this.FechaEmision.DataPropertyName = "FechaEmision";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Format = "d";
            dataGridViewCellStyle3.NullValue = null;
            this.FechaEmision.DefaultCellStyle = dataGridViewCellStyle3;
            this.FechaEmision.HeaderText = "Fec. Emisión";
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.ReadOnly = true;
            this.FechaEmision.Width = 65;
            // 
            // FechaCerfificacion
            // 
            this.FechaCerfificacion.DataPropertyName = "FechaCerfificacion";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Format = "d";
            this.FechaCerfificacion.DefaultCellStyle = dataGridViewCellStyle4;
            this.FechaCerfificacion.HeaderText = "Fec. Certificación";
            this.FechaCerfificacion.Name = "FechaCerfificacion";
            this.FechaCerfificacion.ReadOnly = true;
            this.FechaCerfificacion.Width = 65;
            // 
            // IdDocumento
            // 
            this.IdDocumento.DataPropertyName = "IdDocumento";
            this.IdDocumento.HeaderText = "IdDocumento";
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.ReadOnly = true;
            this.IdDocumento.Width = 225;
            // 
            // PACCertifica
            // 
            this.PACCertifica.DataPropertyName = "PACCertifica";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PACCertifica.DefaultCellStyle = dataGridViewCellStyle5;
            this.PACCertifica.HeaderText = "PACCertifica";
            this.PACCertifica.Name = "PACCertifica";
            this.PACCertifica.ReadOnly = true;
            // 
            // FechaStatusCancela
            // 
            this.FechaStatusCancela.DataPropertyName = "FechaStatusCancela";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.Format = "d";
            this.FechaStatusCancela.DefaultCellStyle = dataGridViewCellStyle6;
            this.FechaStatusCancela.HeaderText = "Fec. Cancela";
            this.FechaStatusCancela.Name = "FechaStatusCancela";
            this.FechaStatusCancela.ReadOnly = true;
            this.FechaStatusCancela.Width = 65;
            // 
            // FechaValidacion
            // 
            this.FechaValidacion.DataPropertyName = "FechaValidacion";
            this.FechaValidacion.HeaderText = "Fec. Validación";
            this.FechaValidacion.Name = "FechaValidacion";
            this.FechaValidacion.Width = 65;
            // 
            // RetencionISR
            // 
            this.RetencionISR.DataPropertyName = "RetencionISR";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            this.RetencionISR.DefaultCellStyle = dataGridViewCellStyle7;
            this.RetencionISR.HeaderText = "Ret. ISR";
            this.RetencionISR.Name = "RetencionISR";
            this.RetencionISR.ReadOnly = true;
            this.RetencionISR.Width = 50;
            // 
            // RetencionIVA
            // 
            this.RetencionIVA.DataPropertyName = "RetencionIVA";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            this.RetencionIVA.DefaultCellStyle = dataGridViewCellStyle8;
            this.RetencionIVA.HeaderText = "Ret. IVA";
            this.RetencionIVA.Name = "RetencionIVA";
            this.RetencionIVA.ReadOnly = true;
            this.RetencionIVA.Width = 50;
            // 
            // RetencionIEPS
            // 
            this.RetencionIEPS.DataPropertyName = "RetencionIEPS";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N2";
            this.RetencionIEPS.DefaultCellStyle = dataGridViewCellStyle9;
            this.RetencionIEPS.HeaderText = "Ret. IEPS";
            this.RetencionIEPS.Name = "RetencionIEPS";
            this.RetencionIEPS.ReadOnly = true;
            this.RetencionIEPS.Width = 50;
            // 
            // TrasladoIVA
            // 
            this.TrasladoIVA.DataPropertyName = "TrasladoIVA";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N2";
            this.TrasladoIVA.DefaultCellStyle = dataGridViewCellStyle10;
            this.TrasladoIVA.HeaderText = "Tras. IVA";
            this.TrasladoIVA.Name = "TrasladoIVA";
            this.TrasladoIVA.ReadOnly = true;
            this.TrasladoIVA.Width = 50;
            // 
            // TrasladoIEPS
            // 
            this.TrasladoIEPS.DataPropertyName = "TrasladoIEPS";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N2";
            this.TrasladoIEPS.DefaultCellStyle = dataGridViewCellStyle11;
            this.TrasladoIEPS.HeaderText = "Tras. IEPS";
            this.TrasladoIEPS.Name = "TrasladoIEPS";
            this.TrasladoIEPS.ReadOnly = true;
            this.TrasladoIEPS.Width = 50;
            // 
            // SubTotal
            // 
            this.SubTotal.DataPropertyName = "SubTotal";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "N2";
            this.SubTotal.DefaultCellStyle = dataGridViewCellStyle12;
            this.SubTotal.HeaderText = "Sub Total";
            this.SubTotal.Name = "SubTotal";
            this.SubTotal.ReadOnly = true;
            this.SubTotal.Width = 65;
            // 
            // Descuento
            // 
            this.Descuento.DataPropertyName = "Descuento";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "N2";
            this.Descuento.DefaultCellStyle = dataGridViewCellStyle13;
            this.Descuento.HeaderText = "Descuento";
            this.Descuento.Name = "Descuento";
            this.Descuento.ReadOnly = true;
            this.Descuento.Width = 65;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.Format = "N2";
            this.Total.DefaultCellStyle = dataGridViewCellStyle14;
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.Width = 65;
            // 
            // TComprobante
            // 
            this.TComprobante.Dock = System.Windows.Forms.DockStyle.Top;
            this.TComprobante.Location = new System.Drawing.Point(0, 0);
            this.TComprobante.Name = "TComprobante";
            this.TComprobante.ShowActualizar = true;
            this.TComprobante.ShowCancelar = false;
            this.TComprobante.ShowCerrar = true;
            this.TComprobante.ShowEditar = false;
            this.TComprobante.ShowHerramientas = true;
            this.TComprobante.ShowImprimir = true;
            this.TComprobante.ShowNuevo = false;
            this.TComprobante.ShowPeriodo = true;
            this.TComprobante.Size = new System.Drawing.Size(800, 25);
            this.TComprobante.TabIndex = 4;
            // 
            // ComprobantesNominaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.gridData);
            this.Controls.Add(this.TComprobante);
            this.Name = "ComprobantesNominaForm";
            this.Text = "ComprobantesNominaForm";
            this.Load += new System.EventHandler(this.ComprobantesNominaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridData;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoComprobante;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Serie;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmisorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Emisor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Receptor;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaEmision;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaCerfificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn PACCertifica;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaStatusCancela;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaValidacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetencionISR;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetencionIVA;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetencionIEPS;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrasladoIVA;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrasladoIEPS;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descuento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        protected internal Common.Forms.ToolBar1CommonControl TComprobante;
    }
}