﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Comprobantes {
    public class ComprobantesEmitidosForm : ComprobantesFiscalesForm {
        public ComprobantesEmitidosForm(UIMenuElement menuElement) : base(Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido) {
            this.Text = "Emisión: Facturacion";
        }

        public override void Consultar() {
            this._DataSource = this.service.GetList(this.TComprobante.GetMes(), this.TComprobante.GetEjercicio(), "Todos", "Emitidos");
        }
    }
}
