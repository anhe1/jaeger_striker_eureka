﻿using System;
using System.Windows.Forms;
using Jaeger.Domain.Repositorio.Entities;
using Jaeger.Aplication.Repositorio.Contracts;
using Jaeger.Aplication.Repositorio.Services;

namespace Jaeger.UI.Forms {
    public partial class EmpresaForm : Form {
        protected AdministradorService service;
        protected EmpresaModel empresa = null;
        protected bool IsNew = false;
        public EmpresaForm(EmpresaModel empresa, AdministradorService service) {
            InitializeComponent();
            this.empresa = empresa;
            this.service = service;
        }

        private void ContribuyenteForm_Load(object sender, EventArgs e) {
            Actualizar_Click(sender, e);
            toolTip1.SetToolTip(this.RFC, "Registro Federal de Causantes");
            this.regimenFiscal.DataSource = RegimeFiscalService.Regimenes;
            this.regimenFiscal.DisplayMember = "Descriptor";
            this.regimenFiscal.ValueMember = "Id";
        }

        private void Actualizar_Click(object sender, EventArgs e) {
            if (this.empresa == null) {
                this.empresa = new EmpresaModel();
                this.IsNew = true;
            }
            this.CreateBinding();
        }

        private void Nuevo_Click(object sender, EventArgs e) {
            this.empresa = new EmpresaModel();
            this.CreateBinding();
        }

        private void Guardar_Click(object sender, EventArgs e) {
            if (this.Validar()) {
                using (var espera = new Common.Forms.WaitingForm(this.Guardar)) {
                    espera.Text = "Guardando información, espera...";
                    espera.ShowDialog(this);
                }
            } else {
                MessageBox.Show(this, "Faltan datos necesarios del formulario", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Close();
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Guardar() {
            this.empresa = this.service.Save(this.empresa);
        }

        private void CreateBinding() {
            this.RFC.DataBindings.Clear();
            this.RFC.DataBindings.Add("Text", this.empresa, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);
            this.razonSocial.DataBindings.Clear();
            this.razonSocial.DataBindings.Add("Text", this.empresa, "RazonSocial", true, DataSourceUpdateMode.OnPropertyChanged);
            this.nombreComercial.DataBindings.Clear();
            this.nombreComercial.DataBindings.Add("Text", this.empresa, "NombreComercial", true, DataSourceUpdateMode.OnPropertyChanged);
            this.regimenFiscal.DataBindings.Clear();
            this.regimenFiscal.DataBindings.Add("Text", this.empresa, "RegimenFiscal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.domicilioFiscal.DataBindings.Clear();
            this.domicilioFiscal.DataBindings.Add("Text", this.empresa, "DomicilioFiscal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.correo.DataBindings.Clear();
            this.correo.DataBindings.Add("Text", this.empresa, "Correo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.telefono.DataBindings.Clear();
            this.telefono.DataBindings.Add("Text", this.empresa, "Telefono", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtRepositorio.DataBindings.Clear();
            this.txtRepositorio.DataBindings.Add("Text", this.empresa, "Repositorio", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private bool Validar() {
            this.errorProvider.Clear();
            if (!Domain.Base.Services.ValidacionService.RFC(this.empresa.RFC)) {
                this.errorProvider.SetError(this.RFC, "Estructura de RFC no válida");
                return false;
            }

            if (string.IsNullOrEmpty(this.empresa.RazonSocial)) {
                this.errorProvider.SetError(this.razonSocial, "Es necesario especificar la razón social.");
                return false;
            }

            if (this.IsNew) {
                if (System.IO.File.Exists(this.txtRepositorio.Text)) {

                }
            }

            return true;
        }

        private void btnExaminar_Click(object sender, EventArgs e) {
            if (this.IsNew) {
                var openFile = new SaveFileDialog() { Filter = "*.sqlite|*.SQLITE", AddExtension = true, Title = "Guardar en" };
                if (openFile.ShowDialog(this) == DialogResult.OK) {
                    this.txtRepositorio.Text = openFile.FileName;
                }
            } else {
                var openFile = new OpenFileDialog() { Filter = "*.sqlite|*.SQLITE", AddExtension = true, Title = "Ubicación" };
                if (openFile.ShowDialog(this) == DialogResult.OK) {
                    this.txtRepositorio.Text = openFile.FileName;
                }
            }
        }

        private void RFC_Validated(object sender, EventArgs e) {

            if (this.IsNew) {
                this.txtRepositorio.Text = RouterManagerService.GetPath(Aplication.Repositorio.ValueObjects.PathEnum.Repositorio, "Jaeger_" + this.RFC.Text.ToLower() + ".sqlite");
            }
        }
    }
}
