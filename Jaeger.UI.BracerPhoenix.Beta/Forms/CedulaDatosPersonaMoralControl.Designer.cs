﻿namespace Jaeger.UI.Forms {
    partial class CedulaDatosPersonaMoralControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.grpDatosIdentificacion = new System.Windows.Forms.GroupBox();
            this.txtFecCambio = new System.Windows.Forms.TextBox();
            this.lblFecCambio = new System.Windows.Forms.Label();
            this.txtSituacion = new System.Windows.Forms.TextBox();
            this.lblSituacion = new System.Windows.Forms.Label();
            this.txtFecInicio = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFecConstitucion = new System.Windows.Forms.TextBox();
            this.lblFecNacimiento = new System.Windows.Forms.Label();
            this.txtNombreComercial = new System.Windows.Forms.TextBox();
            this.lblApellidoMaterno = new System.Windows.Forms.Label();
            this.txtRegimenCapital = new System.Windows.Forms.TextBox();
            this.lblApellidoPaterno = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtRFC = new System.Windows.Forms.TextBox();
            this.lblRFC = new System.Windows.Forms.Label();
            this.DatosUbicacion = new Jaeger.UI.Forms.CedulaDatosUbicacionControl();
            this.grpDatosIdentificacion.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDatosIdentificacion
            // 
            this.grpDatosIdentificacion.Controls.Add(this.txtFecCambio);
            this.grpDatosIdentificacion.Controls.Add(this.lblFecCambio);
            this.grpDatosIdentificacion.Controls.Add(this.txtSituacion);
            this.grpDatosIdentificacion.Controls.Add(this.lblSituacion);
            this.grpDatosIdentificacion.Controls.Add(this.txtFecInicio);
            this.grpDatosIdentificacion.Controls.Add(this.label6);
            this.grpDatosIdentificacion.Controls.Add(this.txtFecConstitucion);
            this.grpDatosIdentificacion.Controls.Add(this.lblFecNacimiento);
            this.grpDatosIdentificacion.Controls.Add(this.txtNombreComercial);
            this.grpDatosIdentificacion.Controls.Add(this.lblApellidoMaterno);
            this.grpDatosIdentificacion.Controls.Add(this.txtRegimenCapital);
            this.grpDatosIdentificacion.Controls.Add(this.lblApellidoPaterno);
            this.grpDatosIdentificacion.Controls.Add(this.txtNombre);
            this.grpDatosIdentificacion.Controls.Add(this.lblNombre);
            this.grpDatosIdentificacion.Controls.Add(this.txtRFC);
            this.grpDatosIdentificacion.Controls.Add(this.lblRFC);
            this.grpDatosIdentificacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpDatosIdentificacion.Location = new System.Drawing.Point(0, 0);
            this.grpDatosIdentificacion.Name = "grpDatosIdentificacion";
            this.grpDatosIdentificacion.Size = new System.Drawing.Size(553, 131);
            this.grpDatosIdentificacion.TabIndex = 5;
            this.grpDatosIdentificacion.TabStop = false;
            this.grpDatosIdentificacion.Text = "Datos de Identificación";
            // 
            // txtFecCambio
            // 
            this.txtFecCambio.BackColor = System.Drawing.SystemColors.Window;
            this.txtFecCambio.Location = new System.Drawing.Point(345, 101);
            this.txtFecCambio.Name = "txtFecCambio";
            this.txtFecCambio.ReadOnly = true;
            this.txtFecCambio.Size = new System.Drawing.Size(159, 20);
            this.txtFecCambio.TabIndex = 27;
            this.txtFecCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblFecCambio
            // 
            this.lblFecCambio.AutoSize = true;
            this.lblFecCambio.Location = new System.Drawing.Point(342, 85);
            this.lblFecCambio.Name = "lblFecCambio";
            this.lblFecCambio.Size = new System.Drawing.Size(175, 13);
            this.lblFecCambio.TabIndex = 28;
            this.lblFecCambio.Text = "Fec. del último cambio de situación:";
            // 
            // txtSituacion
            // 
            this.txtSituacion.BackColor = System.Drawing.SystemColors.Window;
            this.txtSituacion.Location = new System.Drawing.Point(360, 23);
            this.txtSituacion.Name = "txtSituacion";
            this.txtSituacion.ReadOnly = true;
            this.txtSituacion.Size = new System.Drawing.Size(134, 20);
            this.txtSituacion.TabIndex = 25;
            this.txtSituacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblSituacion
            // 
            this.lblSituacion.AutoSize = true;
            this.lblSituacion.Location = new System.Drawing.Point(357, 7);
            this.lblSituacion.Name = "lblSituacion";
            this.lblSituacion.Size = new System.Drawing.Size(138, 13);
            this.lblSituacion.TabIndex = 26;
            this.lblSituacion.Text = "Situación del contribuyente:\t";
            // 
            // txtFecInicio
            // 
            this.txtFecInicio.BackColor = System.Drawing.SystemColors.Window;
            this.txtFecInicio.Location = new System.Drawing.Point(180, 101);
            this.txtFecInicio.Name = "txtFecInicio";
            this.txtFecInicio.ReadOnly = true;
            this.txtFecInicio.Size = new System.Drawing.Size(159, 20);
            this.txtFecInicio.TabIndex = 23;
            this.txtFecInicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(177, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(150, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Fec. de Inicio de operaciones:";
            // 
            // txtFecConstitucion
            // 
            this.txtFecConstitucion.BackColor = System.Drawing.SystemColors.Window;
            this.txtFecConstitucion.Location = new System.Drawing.Point(15, 101);
            this.txtFecConstitucion.Name = "txtFecConstitucion";
            this.txtFecConstitucion.ReadOnly = true;
            this.txtFecConstitucion.Size = new System.Drawing.Size(159, 20);
            this.txtFecConstitucion.TabIndex = 23;
            this.txtFecConstitucion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblFecNacimiento
            // 
            this.lblFecNacimiento.AutoSize = true;
            this.lblFecNacimiento.Location = new System.Drawing.Point(12, 85);
            this.lblFecNacimiento.Name = "lblFecNacimiento";
            this.lblFecNacimiento.Size = new System.Drawing.Size(115, 13);
            this.lblFecNacimiento.TabIndex = 24;
            this.lblFecNacimiento.Text = "Fecha de constitución:";
            // 
            // txtNombreComercial
            // 
            this.txtNombreComercial.BackColor = System.Drawing.SystemColors.Window;
            this.txtNombreComercial.Location = new System.Drawing.Point(360, 62);
            this.txtNombreComercial.Name = "txtNombreComercial";
            this.txtNombreComercial.ReadOnly = true;
            this.txtNombreComercial.Size = new System.Drawing.Size(187, 20);
            this.txtNombreComercial.TabIndex = 21;
            this.txtNombreComercial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblApellidoMaterno
            // 
            this.lblApellidoMaterno.AutoSize = true;
            this.lblApellidoMaterno.Location = new System.Drawing.Point(357, 46);
            this.lblApellidoMaterno.Name = "lblApellidoMaterno";
            this.lblApellidoMaterno.Size = new System.Drawing.Size(96, 13);
            this.lblApellidoMaterno.TabIndex = 22;
            this.lblApellidoMaterno.Text = "Nombre Comercial:";
            // 
            // txtRegimenCapital
            // 
            this.txtRegimenCapital.BackColor = System.Drawing.SystemColors.Window;
            this.txtRegimenCapital.Location = new System.Drawing.Point(210, 62);
            this.txtRegimenCapital.Name = "txtRegimenCapital";
            this.txtRegimenCapital.ReadOnly = true;
            this.txtRegimenCapital.Size = new System.Drawing.Size(144, 20);
            this.txtRegimenCapital.TabIndex = 21;
            this.txtRegimenCapital.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblApellidoPaterno
            // 
            this.lblApellidoPaterno.AutoSize = true;
            this.lblApellidoPaterno.Location = new System.Drawing.Point(207, 46);
            this.lblApellidoPaterno.Name = "lblApellidoPaterno";
            this.lblApellidoPaterno.Size = new System.Drawing.Size(101, 13);
            this.lblApellidoPaterno.TabIndex = 22;
            this.lblApellidoPaterno.Text = "Régimen de capital:";
            // 
            // txtNombre
            // 
            this.txtNombre.BackColor = System.Drawing.SystemColors.Window;
            this.txtNombre.Location = new System.Drawing.Point(15, 62);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(189, 20);
            this.txtNombre.TabIndex = 19;
            this.txtNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(12, 46);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(153, 13);
            this.lblNombre.TabIndex = 20;
            this.lblNombre.Text = "Denominación o Razón Social:";
            // 
            // txtRFC
            // 
            this.txtRFC.BackColor = System.Drawing.SystemColors.Window;
            this.txtRFC.Location = new System.Drawing.Point(46, 23);
            this.txtRFC.Name = "txtRFC";
            this.txtRFC.ReadOnly = true;
            this.txtRFC.Size = new System.Drawing.Size(113, 20);
            this.txtRFC.TabIndex = 15;
            this.txtRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblRFC
            // 
            this.lblRFC.AutoSize = true;
            this.lblRFC.Location = new System.Drawing.Point(12, 27);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(31, 13);
            this.lblRFC.TabIndex = 16;
            this.lblRFC.Text = "RFC:";
            // 
            // DatosUbicacion
            // 
            this.DatosUbicacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.DatosUbicacion.Location = new System.Drawing.Point(0, 131);
            this.DatosUbicacion.Name = "DatosUbicacion";
            this.DatosUbicacion.Size = new System.Drawing.Size(553, 149);
            this.DatosUbicacion.TabIndex = 6;
            // 
            // CedulaDatosPersonaMoralControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DatosUbicacion);
            this.Controls.Add(this.grpDatosIdentificacion);
            this.Name = "CedulaDatosPersonaMoralControl";
            this.Size = new System.Drawing.Size(553, 286);
            this.Load += new System.EventHandler(this.CedulaDatosPersonaMoralControl_Load);
            this.grpDatosIdentificacion.ResumeLayout(false);
            this.grpDatosIdentificacion.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDatosIdentificacion;
        private System.Windows.Forms.TextBox txtFecCambio;
        private System.Windows.Forms.Label lblFecCambio;
        private System.Windows.Forms.TextBox txtSituacion;
        private System.Windows.Forms.Label lblSituacion;
        private System.Windows.Forms.TextBox txtFecInicio;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtFecConstitucion;
        private System.Windows.Forms.Label lblFecNacimiento;
        private System.Windows.Forms.TextBox txtNombreComercial;
        private System.Windows.Forms.Label lblApellidoMaterno;
        private System.Windows.Forms.TextBox txtRegimenCapital;
        private System.Windows.Forms.Label lblApellidoPaterno;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtRFC;
        private System.Windows.Forms.Label lblRFC;
        private CedulaDatosUbicacionControl DatosUbicacion;
    }
}
