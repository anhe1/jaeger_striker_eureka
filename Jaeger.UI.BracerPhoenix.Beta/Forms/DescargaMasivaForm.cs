﻿using System;
using System.Net;
using System.Windows.Forms;
using Jaeger.Aplication.Repositorio.Contracts;
using Jaeger.Aplication.Repositorio.Services;
using Jaeger.Repositorio.Entities;
using Jaeger.Repositorio.Interface;
using Jaeger.Repositorio.V3;
using Jaeger.Repositorio.ValueObjects;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms {
    public partial class DescargaMasivaForm : Form {
        #region Inicializa Variables Globales
        private IDescargaMasiva descarga;
        protected IRepositorioService service;
        #endregion

        public DescargaMasivaForm() {
            InitializeComponent();
            this.Size = new System.Drawing.Size(549, 323);
        }

        public DescargaMasivaForm(IRepositorioService service) {
            InitializeComponent();
            this.service = service;
        }

        private void DescargaMasiva_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            //this.FormBorderStyle = FormBorderStyle.Sizable;
            this.WebSAT.ScriptErrorsSuppressed = true;
            this.descargaControl.cmbAccount.Text = ConfiguracionService.Contribuyente.RFC;
            this.descargaControl.GuardarEn.Text = RouterManagerService.GetPath(Aplication.Repositorio.ValueObjects.PathEnum.Repositorio);
            this.lblStatus.Text = "Esperando ...";
            this.descargaControl.Descargar.Enabled = false;
            this.WebSAT.Url = new Uri(Properties.Resources.UrlStart);
        }

        private void descargaControl_ButtonRecargar_Click_1(object sender, EventArgs e) {
            this.WebSAT.Url = new Uri(Properties.Resources.UrlStart);
        }

        private void WebSat_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e) {
            try {
                if (this.WebSAT.Document != null) {
                    if (this.WebSAT != null) {
                        var htmlElementCapchaIMGAGE = this.WebSAT.Document.GetElementById("IDPLogin");
                        if (htmlElementCapchaIMGAGE != null) {
                            Application.DoEvents();
                            var wc = new WebClient();

                            Application.DoEvents();
                            var elemetsimages = WebSAT.Document.GetElementsByTagName("img");

                            foreach (HtmlElement image in elemetsimages) {
                                image.SetAttribute("id", "capchaimage");
                                var end = image.OuterHtml;
                                var _capcha = DataImage.TryParse(end);
                                if (_capcha.Image != null) {
                                    this.descargaControl.picboxCaptcha.Image = _capcha.Image;
                                    this.descargaControl.BoxLog.Text = "Regex: data";
                                } else {
                                    mshtml.HTMLWindow2 w2 = (mshtml.HTMLWindow2)WebSAT.Document.Window.DomWindow;
                                    w2.execScript(Properties.Resources.Capcha, "javascript");
                                    Clipboard.GetImage();
                                    this.descargaControl.BoxLog.Text = "Clipboard: data";
                                }
                                this.descargaControl.Descargar.Enabled = true;
                            }
                        }
                    }

                    int timeout = 0;
                    while (true) {
                        Application.DoEvents();
                        if (!this.WebSAT.IsBusy) {
                            break;
                        }
                        timeout += 1;
                        if (timeout > 40000)
                            break;
                    }
                }
            } catch {
                Console.WriteLine("No");
            }
            this.Direccion.Text = this.WebSAT.Url.ToString();
            this.lblStatus.Text = this.WebSAT.Url.ToString();
        }

        private void descargaControl_ButtonDescargar_Click(object sender, ConfiguracionDetail e) {
            if (e.Tipo == DescargaTipoEnum.Recibidos)
                this.descarga = new DescargaXml2SAT(this.descargaControl.BoxLog, this.descargaControl.BoxLog);
            else if (e.Tipo == DescargaTipoEnum.Emitidos)
                this.descarga = new DescargaXml4SAT(this.descargaControl.BoxLog, this.descargaControl.BoxLog);
            else if (e.Tipo == DescargaTipoEnum.Todas)
                this.descarga = new DescargaXml3SAT(this.descargaControl.BoxLog, this.descargaControl.BoxLog);
            else {
                MessageBox.Show(this, "¡Objeto no válido!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            this.descargaControl.SetDisable();
            this.descarga.Configuracion = e;
            this.descargaControl.Enabled = false;
            this.descarga.StartProcess += Iniciar_Proceso;
            this.descarga.ProgressChanged += Proceso_Changed;
            this.descarga.CompletedProcess += Proceso_Termiando;

            StatusBarProgreso.Minimum = 0;
            StatusBarProgreso.Maximum = 100;
            this.descarga.Consultar(this.WebSAT, e);
        }

        private void Iniciar_Proceso(object sender, DescargaMasivaStartProcess e) {
            this.lblStatus.Text = e.Data;
        }

        private void Proceso_Termiando(object sender, DescargaMasivaCompletedProcess e) {
            this.descarga.Reset();
            this.WebSAT.Dispose();
            this.descargaControl.Enabled = true;
            MessageBox.Show(this, Properties.Resources.msg_DescargaTerminada);
            using (var espera = new UI.Common.Forms.WaitingForm(this.Guardar)) {
                espera.Text = "Almacenando resultados ...";
                espera.ShowDialog(this);
            }
            this.descargaControl.SetEnabled();
            this.Close();
        }

        private void Proceso_Changed(object sender, DescargaMasivaProgreso e) {
            if (e.Completado < 100) {
                this.StatusBarProgreso.Visible = true;
                this.StatusBarProgreso.Value = e.Completado;
            } else { 
                this.StatusBarProgreso.Visible = false;
            }
        }

        private void Guardar() {
            this.service.SaveResults(this.descarga.Resultados);
        }
    }
}
