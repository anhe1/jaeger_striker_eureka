﻿namespace Jaeger.UI.Forms {
    partial class DescargaMasivaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DescargaMasivaForm));
            this.WebSAT = new System.Windows.Forms.WebBrowser();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusBarProgreso = new System.Windows.Forms.ToolStripProgressBar();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.Navegador = new System.Windows.Forms.ToolStrip();
            this.Retroceder = new System.Windows.Forms.ToolStripButton();
            this.Avanzar = new System.Windows.Forms.ToolStripButton();
            this.Actualizar = new System.Windows.Forms.ToolStripButton();
            this.Direccion = new System.Windows.Forms.ToolStripTextBox();
            this.Ir = new System.Windows.Forms.ToolStripButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gridResultados = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.descargaControl = new Jaeger.UI.Forms.DescargaControl();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.Navegador.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridResultados)).BeginInit();
            this.SuspendLayout();
            // 
            // WebSAT
            // 
            this.WebSAT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WebSAT.Location = new System.Drawing.Point(3, 28);
            this.WebSAT.MinimumSize = new System.Drawing.Size(20, 20);
            this.WebSAT.Name = "WebSAT";
            this.WebSAT.Size = new System.Drawing.Size(554, 20);
            this.WebSAT.TabIndex = 0;
            this.WebSAT.Url = new System.Uri("https://cfdiau.sat.gob.mx/nidp/app/login?id=SATUPCFDiCon&sid=0&option=credential&" +
        "sid=0", System.UriKind.Absolute);
            this.WebSAT.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.WebSat_DocumentCompleted);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.StatusBarProgreso});
            this.statusStrip1.Location = new System.Drawing.Point(0, 239);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(568, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 30;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(22, 17);
            this.lblStatus.Text = "---";
            // 
            // StatusBarProgreso
            // 
            this.StatusBarProgreso.Name = "StatusBarProgreso";
            this.StatusBarProgreso.Size = new System.Drawing.Size(100, 16);
            this.StatusBarProgreso.Visible = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 240);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(568, 0);
            this.tabControl1.TabIndex = 32;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.WebSAT);
            this.tabPage1.Controls.Add(this.Navegador);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(560, 0);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Navegador";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // Navegador
            // 
            this.Navegador.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Retroceder,
            this.Avanzar,
            this.Actualizar,
            this.Direccion,
            this.Ir});
            this.Navegador.Location = new System.Drawing.Point(3, 3);
            this.Navegador.Name = "Navegador";
            this.Navegador.Size = new System.Drawing.Size(554, 25);
            this.Navegador.TabIndex = 1;
            this.Navegador.Text = "toolStrip1";
            // 
            // Retroceder
            // 
            this.Retroceder.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Retroceder.Image = global::Jaeger.UI.Properties.Resources.back_to_16px;
            this.Retroceder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Retroceder.Name = "Retroceder";
            this.Retroceder.Size = new System.Drawing.Size(23, 22);
            this.Retroceder.Text = "toolStripButton2";
            // 
            // Avanzar
            // 
            this.Avanzar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Avanzar.Image = global::Jaeger.UI.Properties.Resources.next_page_16px;
            this.Avanzar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Avanzar.Name = "Avanzar";
            this.Avanzar.Size = new System.Drawing.Size(23, 22);
            this.Avanzar.Text = "Avanzar";
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Actualizar.Image = global::Jaeger.UI.Properties.Resources.refresh_16px;
            this.Actualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Size = new System.Drawing.Size(23, 22);
            this.Actualizar.Text = "Actualizar";
            // 
            // Direccion
            // 
            this.Direccion.Name = "Direccion";
            this.Direccion.Size = new System.Drawing.Size(500, 23);
            // 
            // Ir
            // 
            this.Ir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Ir.Image = global::Jaeger.UI.Properties.Resources.forward_button_16px;
            this.Ir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Ir.Name = "Ir";
            this.Ir.Size = new System.Drawing.Size(23, 20);
            this.Ir.Text = "toolStripButton1";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gridResultados);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(560, 0);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Resultados";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gridResultados
            // 
            this.gridResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridResultados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridResultados.Location = new System.Drawing.Point(3, 3);
            this.gridResultados.Name = "gridResultados";
            this.gridResultados.Size = new System.Drawing.Size(554, 0);
            this.gridResultados.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(571, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(481, 222);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Opciones para pruebas";
            // 
            // descargaControl
            // 
            this.descargaControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.descargaControl.Location = new System.Drawing.Point(0, 0);
            this.descargaControl.MaximumSize = new System.Drawing.Size(565, 240);
            this.descargaControl.MinimumSize = new System.Drawing.Size(565, 240);
            this.descargaControl.Name = "descargaControl";
            this.descargaControl.Size = new System.Drawing.Size(565, 240);
            this.descargaControl.TabIndex = 31;
            this.descargaControl.ButtonRecargar_Click += new System.EventHandler<System.EventArgs>(this.descargaControl_ButtonRecargar_Click_1);
            this.descargaControl.ButtonDescargar_Click += new System.EventHandler<Jaeger.Repositorio.Entities.ConfiguracionDetail>(this.descargaControl_ButtonDescargar_Click);
            // 
            // DescargaMasivaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 261);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.descargaControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DescargaMasivaForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Descarga Masiva de Comprobantes Ver.: 3.0.0.6";
            this.Load += new System.EventHandler(this.DescargaMasiva_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.Navegador.ResumeLayout(false);
            this.Navegador.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridResultados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.WebBrowser WebSAT;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ToolStrip Navegador;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ToolStripTextBox Direccion;
        private System.Windows.Forms.DataGridView gridResultados;
        private System.Windows.Forms.ToolStripButton Actualizar;
        private System.Windows.Forms.ToolStripButton Retroceder;
        private System.Windows.Forms.ToolStripButton Avanzar;
        private System.Windows.Forms.ToolStripButton Ir;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripProgressBar StatusBarProgreso;
        public DescargaControl descargaControl;
    }
}