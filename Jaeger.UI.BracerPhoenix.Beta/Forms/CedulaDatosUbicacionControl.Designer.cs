﻿namespace Jaeger.UI.Forms {
    partial class CedulaDatosUbicacionControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.grpDattosUbicacion = new System.Windows.Forms.GroupBox();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.lblTipoVialidad = new System.Windows.Forms.Label();
            this.lblCorreo = new System.Windows.Forms.Label();
            this.lblNumInterior = new System.Windows.Forms.Label();
            this.txtAl = new System.Windows.Forms.TextBox();
            this.lblNumExterior = new System.Windows.Forms.Label();
            this.lblAl = new System.Windows.Forms.Label();
            this.txtNumInterior = new System.Windows.Forms.TextBox();
            this.txtNumExterior = new System.Windows.Forms.TextBox();
            this.lblEntidad = new System.Windows.Forms.Label();
            this.lblNombreVialidad = new System.Windows.Forms.Label();
            this.txtCodigoPostal = new System.Windows.Forms.TextBox();
            this.lblColonia = new System.Windows.Forms.Label();
            this.lblMunicipio = new System.Windows.Forms.Label();
            this.txtColonia = new System.Windows.Forms.TextBox();
            this.lblCodigoPostal = new System.Windows.Forms.Label();
            this.txtNombreVialidad = new System.Windows.Forms.TextBox();
            this.txtEntidad = new System.Windows.Forms.TextBox();
            this.txtTipoVialidad = new System.Windows.Forms.TextBox();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.grpDattosUbicacion.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDattosUbicacion
            // 
            this.grpDattosUbicacion.Controls.Add(this.txtCorreo);
            this.grpDattosUbicacion.Controls.Add(this.lblTipoVialidad);
            this.grpDattosUbicacion.Controls.Add(this.lblCorreo);
            this.grpDattosUbicacion.Controls.Add(this.lblNumInterior);
            this.grpDattosUbicacion.Controls.Add(this.txtAl);
            this.grpDattosUbicacion.Controls.Add(this.lblNumExterior);
            this.grpDattosUbicacion.Controls.Add(this.lblAl);
            this.grpDattosUbicacion.Controls.Add(this.txtNumInterior);
            this.grpDattosUbicacion.Controls.Add(this.txtNumExterior);
            this.grpDattosUbicacion.Controls.Add(this.lblEntidad);
            this.grpDattosUbicacion.Controls.Add(this.lblNombreVialidad);
            this.grpDattosUbicacion.Controls.Add(this.txtCodigoPostal);
            this.grpDattosUbicacion.Controls.Add(this.lblColonia);
            this.grpDattosUbicacion.Controls.Add(this.lblMunicipio);
            this.grpDattosUbicacion.Controls.Add(this.txtColonia);
            this.grpDattosUbicacion.Controls.Add(this.lblCodigoPostal);
            this.grpDattosUbicacion.Controls.Add(this.txtNombreVialidad);
            this.grpDattosUbicacion.Controls.Add(this.txtEntidad);
            this.grpDattosUbicacion.Controls.Add(this.txtTipoVialidad);
            this.grpDattosUbicacion.Controls.Add(this.txtMunicipio);
            this.grpDattosUbicacion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDattosUbicacion.Location = new System.Drawing.Point(0, 0);
            this.grpDattosUbicacion.Name = "grpDattosUbicacion";
            this.grpDattosUbicacion.Size = new System.Drawing.Size(552, 149);
            this.grpDattosUbicacion.TabIndex = 5;
            this.grpDattosUbicacion.TabStop = false;
            this.grpDattosUbicacion.Text = "Datos de Ubicación (domicilio fiscal, vigente)";
            // 
            // txtCorreo
            // 
            this.txtCorreo.BackColor = System.Drawing.SystemColors.Window;
            this.txtCorreo.Location = new System.Drawing.Point(110, 123);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.ReadOnly = true;
            this.txtCorreo.Size = new System.Drawing.Size(201, 20);
            this.txtCorreo.TabIndex = 45;
            // 
            // lblTipoVialidad
            // 
            this.lblTipoVialidad.AutoSize = true;
            this.lblTipoVialidad.Location = new System.Drawing.Point(8, 22);
            this.lblTipoVialidad.Name = "lblTipoVialidad";
            this.lblTipoVialidad.Size = new System.Drawing.Size(85, 13);
            this.lblTipoVialidad.TabIndex = 38;
            this.lblTipoVialidad.Text = "Tipo de vialidad:";
            // 
            // lblCorreo
            // 
            this.lblCorreo.AutoSize = true;
            this.lblCorreo.Location = new System.Drawing.Point(8, 126);
            this.lblCorreo.Name = "lblCorreo";
            this.lblCorreo.Size = new System.Drawing.Size(96, 13);
            this.lblCorreo.TabIndex = 46;
            this.lblCorreo.Text = "Correo electrónico:";
            // 
            // lblNumInterior
            // 
            this.lblNumInterior.AutoSize = true;
            this.lblNumInterior.Location = new System.Drawing.Point(190, 48);
            this.lblNumInterior.Name = "lblNumInterior";
            this.lblNumInterior.Size = new System.Drawing.Size(69, 13);
            this.lblNumInterior.TabIndex = 42;
            this.lblNumInterior.Text = "Núm. interior:";
            // 
            // txtAl
            // 
            this.txtAl.BackColor = System.Drawing.SystemColors.Window;
            this.txtAl.Location = new System.Drawing.Point(340, 97);
            this.txtAl.Name = "txtAl";
            this.txtAl.ReadOnly = true;
            this.txtAl.Size = new System.Drawing.Size(204, 20);
            this.txtAl.TabIndex = 47;
            // 
            // lblNumExterior
            // 
            this.lblNumExterior.AutoSize = true;
            this.lblNumExterior.Location = new System.Drawing.Point(8, 48);
            this.lblNumExterior.Name = "lblNumExterior";
            this.lblNumExterior.Size = new System.Drawing.Size(72, 13);
            this.lblNumExterior.TabIndex = 40;
            this.lblNumExterior.Text = "Núm. exterior:";
            // 
            // lblAl
            // 
            this.lblAl.AutoSize = true;
            this.lblAl.Location = new System.Drawing.Point(315, 100);
            this.lblAl.Name = "lblAl";
            this.lblAl.Size = new System.Drawing.Size(19, 13);
            this.lblAl.TabIndex = 48;
            this.lblAl.Text = "Al:";
            // 
            // txtNumInterior
            // 
            this.txtNumInterior.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumInterior.Location = new System.Drawing.Point(266, 45);
            this.txtNumInterior.Name = "txtNumInterior";
            this.txtNumInterior.ReadOnly = true;
            this.txtNumInterior.Size = new System.Drawing.Size(100, 20);
            this.txtNumInterior.TabIndex = 41;
            // 
            // txtNumExterior
            // 
            this.txtNumExterior.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumExterior.Location = new System.Drawing.Point(84, 45);
            this.txtNumExterior.Name = "txtNumExterior";
            this.txtNumExterior.ReadOnly = true;
            this.txtNumExterior.Size = new System.Drawing.Size(100, 20);
            this.txtNumExterior.TabIndex = 39;
            // 
            // lblEntidad
            // 
            this.lblEntidad.AutoSize = true;
            this.lblEntidad.Location = new System.Drawing.Point(314, 74);
            this.lblEntidad.Name = "lblEntidad";
            this.lblEntidad.Size = new System.Drawing.Size(99, 13);
            this.lblEntidad.TabIndex = 30;
            this.lblEntidad.Text = "Entidad Federativa:";
            // 
            // lblNombreVialidad
            // 
            this.lblNombreVialidad.AutoSize = true;
            this.lblNombreVialidad.Location = new System.Drawing.Point(211, 22);
            this.lblNombreVialidad.Name = "lblNombreVialidad";
            this.lblNombreVialidad.Size = new System.Drawing.Size(112, 13);
            this.lblNombreVialidad.TabIndex = 37;
            this.lblNombreVialidad.Text = "Nombre de la vialidad:";
            // 
            // txtCodigoPostal
            // 
            this.txtCodigoPostal.BackColor = System.Drawing.SystemColors.Window;
            this.txtCodigoPostal.Location = new System.Drawing.Point(474, 45);
            this.txtCodigoPostal.Name = "txtCodigoPostal";
            this.txtCodigoPostal.ReadOnly = true;
            this.txtCodigoPostal.Size = new System.Drawing.Size(70, 20);
            this.txtCodigoPostal.TabIndex = 43;
            this.txtCodigoPostal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblColonia
            // 
            this.lblColonia.AutoSize = true;
            this.lblColonia.Location = new System.Drawing.Point(8, 74);
            this.lblColonia.Name = "lblColonia";
            this.lblColonia.Size = new System.Drawing.Size(45, 13);
            this.lblColonia.TabIndex = 33;
            this.lblColonia.Text = "Colonia:";
            // 
            // lblMunicipio
            // 
            this.lblMunicipio.AutoSize = true;
            this.lblMunicipio.Location = new System.Drawing.Point(8, 100);
            this.lblMunicipio.Name = "lblMunicipio";
            this.lblMunicipio.Size = new System.Drawing.Size(119, 13);
            this.lblMunicipio.TabIndex = 34;
            this.lblMunicipio.Text = "Municipio o delegación:";
            // 
            // txtColonia
            // 
            this.txtColonia.BackColor = System.Drawing.SystemColors.Window;
            this.txtColonia.Location = new System.Drawing.Point(58, 71);
            this.txtColonia.Name = "txtColonia";
            this.txtColonia.ReadOnly = true;
            this.txtColonia.Size = new System.Drawing.Size(253, 20);
            this.txtColonia.TabIndex = 31;
            // 
            // lblCodigoPostal
            // 
            this.lblCodigoPostal.AutoSize = true;
            this.lblCodigoPostal.Location = new System.Drawing.Point(393, 48);
            this.lblCodigoPostal.Name = "lblCodigoPostal";
            this.lblCodigoPostal.Size = new System.Drawing.Size(75, 13);
            this.lblCodigoPostal.TabIndex = 44;
            this.lblCodigoPostal.Text = "Codigo Postal:";
            // 
            // txtNombreVialidad
            // 
            this.txtNombreVialidad.BackColor = System.Drawing.SystemColors.Window;
            this.txtNombreVialidad.Location = new System.Drawing.Point(325, 19);
            this.txtNombreVialidad.Name = "txtNombreVialidad";
            this.txtNombreVialidad.ReadOnly = true;
            this.txtNombreVialidad.Size = new System.Drawing.Size(219, 20);
            this.txtNombreVialidad.TabIndex = 35;
            // 
            // txtEntidad
            // 
            this.txtEntidad.BackColor = System.Drawing.SystemColors.Window;
            this.txtEntidad.Location = new System.Drawing.Point(419, 71);
            this.txtEntidad.Name = "txtEntidad";
            this.txtEntidad.ReadOnly = true;
            this.txtEntidad.Size = new System.Drawing.Size(125, 20);
            this.txtEntidad.TabIndex = 29;
            // 
            // txtTipoVialidad
            // 
            this.txtTipoVialidad.BackColor = System.Drawing.SystemColors.Window;
            this.txtTipoVialidad.Location = new System.Drawing.Point(95, 19);
            this.txtTipoVialidad.Name = "txtTipoVialidad";
            this.txtTipoVialidad.ReadOnly = true;
            this.txtTipoVialidad.Size = new System.Drawing.Size(110, 20);
            this.txtTipoVialidad.TabIndex = 36;
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.BackColor = System.Drawing.SystemColors.Window;
            this.txtMunicipio.Location = new System.Drawing.Point(134, 97);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.ReadOnly = true;
            this.txtMunicipio.Size = new System.Drawing.Size(177, 20);
            this.txtMunicipio.TabIndex = 32;
            // 
            // CedulaDatosUbicacionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grpDattosUbicacion);
            this.Name = "CedulaDatosUbicacionControl";
            this.Size = new System.Drawing.Size(552, 149);
            this.Load += new System.EventHandler(this.CedulaDatosUbicacionControl_Load);
            this.grpDattosUbicacion.ResumeLayout(false);
            this.grpDattosUbicacion.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDattosUbicacion;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.Label lblTipoVialidad;
        private System.Windows.Forms.Label lblCorreo;
        private System.Windows.Forms.Label lblNumInterior;
        private System.Windows.Forms.TextBox txtAl;
        private System.Windows.Forms.Label lblNumExterior;
        private System.Windows.Forms.Label lblAl;
        private System.Windows.Forms.TextBox txtNumInterior;
        private System.Windows.Forms.TextBox txtNumExterior;
        private System.Windows.Forms.Label lblEntidad;
        private System.Windows.Forms.Label lblNombreVialidad;
        private System.Windows.Forms.TextBox txtCodigoPostal;
        private System.Windows.Forms.Label lblColonia;
        private System.Windows.Forms.Label lblMunicipio;
        private System.Windows.Forms.TextBox txtColonia;
        private System.Windows.Forms.Label lblCodigoPostal;
        private System.Windows.Forms.TextBox txtNombreVialidad;
        private System.Windows.Forms.TextBox txtEntidad;
        private System.Windows.Forms.TextBox txtTipoVialidad;
        private System.Windows.Forms.TextBox txtMunicipio;
    }
}
