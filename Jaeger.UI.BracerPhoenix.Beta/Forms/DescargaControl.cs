﻿using System;
using System.Windows.Forms;
using Jaeger.Repositorio.Entities;
using Jaeger.Repositorio.Services;

namespace Jaeger.UI.Forms {
    public partial class DescargaControl : UserControl {
        protected ConfiguracionDetail configuracion = new ConfiguracionDetail();
        public event EventHandler<EventArgs> ButtonRecargar_Click;
        public event EventHandler<EventArgs> ButtonExaminar_Click;
        public event EventHandler<ConfiguracionDetail> ButtonDescargar_Click;

        public DescargaControl() {
            InitializeComponent();
        }

        public void OnButtonRecargarClick(object sender, EventArgs e) {
            if (this.ButtonRecargar_Click != null)
                this.ButtonRecargar_Click(sender, e);
        }

        protected void OnButtonExaminarClick(object sender, EventArgs e) {
            if (this.ButtonExaminar_Click != null)
                this.ButtonExaminar_Click(sender, e);
        }

        protected void OnButtonDescargarClick(object sender, EventArgs e) {
            if (this.ButtonDescargar_Click != null)
                this.ButtonDescargar_Click(sender, this.configuracion);
        }

        private void GeneralControl_Load(object sender, EventArgs e) {
            // tipo de comprobantes
            this.TipoComprobante.DataSource = CommonService.GetTipoComprobante();
            this.TipoComprobante.DisplayMember = "Descripcion";
            this.TipoComprobante.ValueMember = "Id";

            // complementos
            this.TipoComplemento.DataSource = CommonService.GetComplementos();
            this.TipoComplemento.DisplayMember = "Descripcion";
            this.TipoComplemento.ValueMember = "Id";

            // 
            this.Estado.DataSource = CommonService.GetEstado();
            this.Estado.DisplayMember = "Descripion";
            this.Estado.ValueMember = "Id";

            this.Estado.DataBindings.Add("SelectedValue", this.configuracion, "Estado", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TipoComprobante.DataBindings.Add("SelectedValue", this.configuracion, "TipoText", true, DataSourceUpdateMode.OnPropertyChanged);
            this.cmbAccount.DataBindings.Add("Text", this.configuracion, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaInicial.DataBindings.Add("Value", this.configuracion, "FechaInicial", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaFinal.DataBindings.Add("Value", this.configuracion, "FechaFinal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Password.DataBindings.Add("Text", this.configuracion, "CIEC", true, DataSourceUpdateMode.OnPropertyChanged);
            this.GuardarEn.DataBindings.Add("Text", this.configuracion, "CarpetaDescarga", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Captcha.DataBindings.Add("Text", this.configuracion, "Captcha", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Examinar.Click += new EventHandler(this.OnButtonExaminarClick);
            this.Recargar.Click += new EventHandler(this.OnButtonRecargarClick);
        }

        private void Examinar_Click(object sender, EventArgs e) {
            var folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK) {
                this.GuardarEn.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void Descargar_Click(object sender, EventArgs e) {
            if (ValidaService.RFC(this.cmbAccount.Text) == false) {
                this.errorProvider1.SetError(this.cmbAccount, "RFC proporcionado no es válido.");
                return;
            }

            if (this.Password.Text.Length <= 3) {
                this.errorProvider1.SetError(this.Password, "Debes proporcionar una contraseña.");
                return;
            }

            this.OnButtonDescargarClick(sender, e);
        }

        public void SetDisable() {
            this.TipoComprobante.Enabled = false;
            this.FechaInicial.Enabled = false;
            this.FechaFinal.Enabled = false;
            this.Estado.Enabled = false;
            this.TipoComplemento.Enabled = false;
            this.FiltrarRFC.Enabled = false;
            this.GuardarEn.Enabled = false;
            this.Examinar.Enabled = false;
            this.Password.Enabled = false;
            this.Recargar.Enabled = false;
            this.Captcha.Enabled = false;
            this.Descargar.Enabled = false;
        }

        public void SetEnabled() {
            this.TipoComprobante.Enabled = true;
            this.FechaInicial.Enabled = true;
            this.FechaFinal.Enabled = true;
            this.Estado.Enabled = true;
            this.TipoComplemento.Enabled = true;
            this.FiltrarRFC.Enabled = true;
            this.GuardarEn.Enabled = true;
            this.Examinar.Enabled = true;
            this.Password.Enabled = true;
            this.Recargar.Enabled = true;
            this.Captcha.Enabled = true;
            this.Descargar.Enabled = true;
        }
    }
}
