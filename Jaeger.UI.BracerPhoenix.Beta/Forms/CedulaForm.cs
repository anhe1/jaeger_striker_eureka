﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Jaeger.Aplication.Validador.Beta.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Common.Forms;
using Jaeger.SAT.CedulaFiscal.Interfaces;
using Jaeger.SAT.CedulaFiscal;
using Jaeger.SAT.CedulaFiscal.Entities;

namespace Jaeger.UI.Forms {
    public partial class CedulaForm : Form {
        protected internal IService service;
        protected internal IResponseCedula responseCedula;
        protected internal ICedulaFiscal cedulaFiscal;

        public CedulaForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void CedulaForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.service = new Service();
        }

        private void btnExaminar_Click(object sender, EventArgs e) {
            var fileOpen = new OpenFileDialog() { Filter = "*.pdf|*.PDF|*.png|*.PNG|*.jpg|*.JPG" };
            if (fileOpen.ShowDialog(this) == DialogResult.OK) {
                this.txtPath.Text = fileOpen.FileName;
                if (Path.GetExtension(this.txtPath.Text) == ".png") {
                    var image = new Bitmap(this.txtPath.Text);
                    if (image != null) {
                        var url = Util.Services.QRCodeExtension.GetString(image);
                        if (url.Contains("https://siat.sat.gob.mx/app/qr/faces/pages/mobile/validadorqr.jsf?")) {
                            IRequestCedula request = RequestCedula.Create().WithURL(url.Message).Build();
                            this.responseCedula = this.service.Execute(request);
                        } else {
                            MessageBox.Show(this, "No se puede obtener alguna cédula de identificación fiscal de la imagen seleccionada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                } else if (Path.GetExtension(this.txtPath.Text) == ".pdf") {
                    var r = PDFExtractorService.GetData(this.txtPath.Text);
                    if (r != null) {
                        if (r["rfc"] != null) { this.txtRFC.Text = r["rfc"]; }
                        if (r["idcif"] != null) { this.txtCURP.Text = r["idcif"]; }
                        btnBuscar_Click(sender, e);
                    }
                } else {
                    MessageBox.Show(this, "No se puede obtener alguna cédula de identificación fiscal de la imagen seleccionada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                //if (this.responseCedula.IsValida) {
                //    this.cedulaFiscal = this.responseCedula.CedulaFiscal;
                //    this.CreateBinding();
                //} else {
                //    MessageBox.Show(this, this.responseCedula.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //}
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Buscar)) {
                espera.Text = "Buscando...";
                espera.ShowDialog(this);
            }
            this.CreateBinding();
        }

        private void Buscar() {
            this.responseCedula = this.service.GetByRFC(this.txtRFC.Text, this.txtCURP.Text);
            if (this.responseCedula.IsValida) {
                this.cedulaFiscal = this.responseCedula.CedulaFiscal;
            } else {
                MessageBox.Show(this, this.responseCedula.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void CreateBinding() {
            if (this.cedulaFiscal != null) {
                if (this.cedulaFiscal.TipoPersona == ValidaSAT.Entities.CedulaFiscal.TipoPersonaEnum.Moral) {
                    this.PersonaMoral.Cargar((IPersonaMoral)this.cedulaFiscal.Moral);
                    tabControl2.SelectedIndex = 1;
                } else if (responseCedula.CedulaFiscal.TipoPersona == ValidaSAT.Entities.CedulaFiscal.TipoPersonaEnum.Fisica) {
                    this.PersonaFisica.Cargar((IPersonaFisica)this.cedulaFiscal.Fisica);
                    tabControl2.SelectedIndex = 2;
                }
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void btnCopiar_Click(object sender, EventArgs e) {
            if (this.cedulaFiscal != null) {
                if (this.cedulaFiscal.TipoPersona == ValidaSAT.Entities.CedulaFiscal.TipoPersonaEnum.Moral) {
                    this.PersonaMoral.Cargar((IPersonaMoral)this.cedulaFiscal.Moral);
                    tabControl2.SelectedIndex = 1;
                } else if (responseCedula.CedulaFiscal.TipoPersona == ValidaSAT.Entities.CedulaFiscal.TipoPersonaEnum.Fisica) {
                    this.PersonaFisica.Cargar((IPersonaFisica)this.cedulaFiscal.Fisica);
                    tabControl2.SelectedIndex = 2;
                }
            }
        }
    }
}
