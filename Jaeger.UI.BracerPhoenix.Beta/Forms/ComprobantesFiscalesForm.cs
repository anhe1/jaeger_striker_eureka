﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Repositorio.Contracts;
using Jaeger.Aplication.Repositorio.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Repositorio.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class ComprobantesFiscalesForm : Form {
        protected IComprobanteFiscalService service;
        protected internal ContextMenuStrip contextMenuStrip = new ContextMenuStrip();
        protected internal BindingList<ComprobanteFiscalDetailModel> _DataSource;
        protected internal ToolStripMenuItem consultaStatus1 = new ToolStripMenuItem { Text = "Exportar" };
        protected internal ToolStripMenuItem serializar = new ToolStripMenuItem { Text = "Serializar" };
        protected internal ToolStripMenuItem consultaStatus = new ToolStripMenuItem { Text = "Consulta Status SAT" };
        protected internal ToolStripMenuItem ContextMenuVer = new ToolStripMenuItem { Text = "Ver XML" };

        public ComprobantesFiscalesForm() {
            InitializeComponent();
        }

        public ComprobantesFiscalesForm(CFDISubTipoEnum subTipo) {
            InitializeComponent();
        }

        public ComprobantesFiscalesForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void ComprobantesFiscalesForm_Load(object sender, EventArgs e) {
            this.gridData.DataGridCommon();
            this.service = ConfiguracionService.Manager.GetComprobantes();
            this.gridData.ContextMenuStrip = this.contextMenuStrip;

            this.TComprobante.Actualizar.Click += this.TComprobante_Actualizar_Click;
            this.TComprobante.Imprimir.Click += this.TComprobante_Imprimir_Click;

            this.contextMenuStrip.Items.Add(this.consultaStatus);
            this.contextMenuStrip.Items.Add(this.serializar);
            this.contextMenuStrip.Items.Add(this.ContextMenuVer);
            this.TComprobante.Herramientas.DropDownItems.Add(this.consultaStatus1);

            this.consultaStatus.Click += this.ConsultaStatus_Click;
            this.consultaStatus1.Click += ConsultaStatus1_Click;
            this.serializar.Click += TComprobante_Serializar_Click;
            this.ContextMenuVer.Click += this.ContextMenuVer_Click;
            this.TComprobante.Cerrar.Click += this.TComprobante_Cerrar_Click;
        }

        private void TComprobante_Serializar_Click(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                using (var espera = new WaitingForm(this.SerializarSeleccionado)) {
                    espera.Text = "Serializando ...";
                    espera.ShowDialog(this);
                }
            }
        }

        private void ConsultaStatus1_Click(object sender, EventArgs e) {
            this.service.Test(this._DataSource);
        }

        private void ConsultaStatus_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.GetStatus)) {
                espera.Text = "Consultando servicio SAT";
                espera.ShowDialog(this);
            }
            if (this.gridData.CurrentRow != null) {
                var _seleccionado = this.gridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailModel;
                if (_seleccionado != null) {
                    MessageBox.Show(this, (string)_seleccionado.Tag, "Consulta CFDI", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void ContextMenuVer_Click(object sender, EventArgs e) {
            var seleccionado = this.gridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailModel;
            if (seleccionado != null) {
                if (Domain.Base.Services.ValidacionService.IsBase64String(seleccionado.XmlContentB64)) {
                    var mostrar = new ComprobanteTreeViewForm(seleccionado.XmlContentB64, seleccionado.KeyName()) { MdiParent = this.ParentForm };
                    mostrar.Show();
                } else {
                    MessageBox.Show(this, "Properties.Resources.Message_Repositorio_Archivo_Error", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        public virtual void TComprobante_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Consultar)) {
                espera.Text = "Properties.Resources.Message_Consulta";
                espera.ShowDialog(this);
                this.gridData.DataSource = this._DataSource;
            }
        }

        private void TComprobante_Imprimir_Click(object sender, EventArgs e) {
            var seleccionado = this.gridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailModel;
            if (seleccionado != null) {
                if (System.IO.File.Exists(System.IO.Path.Combine(seleccionado.PathXML, seleccionado.KeyName() + ".pdf")) == false) {
                    if (MessageBox.Show(this, "Properties.Resources.Message_Repositorio_Imprimir_Standar", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        if (Domain.Base.Services.ValidacionService.IsBase64String(seleccionado.XmlContentB64)) {
            //                //var d = CFDI.V33.Comprobante.LoadBytes(this.service.GetXmlByte(seleccionado));
            //                //if (d != null) {
            //                //    //var _reporte = new ReporteForm(d);
            //                //    //_reporte.ShowDialog(this);
            //                //}
                        }
                    }
                } else {
                    try {
                        System.Diagnostics.Process.Start(System.IO.Path.Combine(seleccionado.PathXML, seleccionado.KeyName() + ".pdf"));
                    } catch (Exception ex) {
                        MessageBox.Show(this, string.Format("Properties.Resources.Message_Repositorio_Error_Abrir_Documento", ex.Message), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }
                }
            }
        }

        public virtual void TComprobante_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void Consultar() {
            this._DataSource = this.service.GetList(this.TComprobante.GetMes(), this.TComprobante.GetEjercicio());
        }

        private void GetStatus() {
            //var d = new ValidaSAT.Service.SAT.Status();
            //if (this.gridData.CurrentRow != null) {
            //    var _seleccionado = this.gridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailModel;
            //    if (_seleccionado != null) {
            //        var s = d.GetStatus(_seleccionado.EmisorRFC, _seleccionado.ReceptorRFC, _seleccionado.Total, _seleccionado.IdDocumento);
            //        _seleccionado.Tag = s.ToString();
            //    }
            //}
        }

        private void SerializarSeleccionado() {
            var seleccionado = this.gridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailModel;
            if (seleccionado != null) {
                if (Domain.Services.ValidacionService.IsBase64String(seleccionado.XmlContentB64)) {
                    seleccionado = this.service.Serializar(seleccionado);
                    this.service.Save(seleccionado);
                } else {
                    MessageBox.Show(this, "Properties.Resources.Message_Repositorio_Archivo_Error", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }
    }
}
