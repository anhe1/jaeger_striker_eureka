﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Domain.Repositorio.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Repositorio.Services;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Management;

namespace Jaeger.UI.Forms {
    public partial class SessionForm : Form {
        private BindingList<EmpresaModel> contribuyentes;
        protected AdministradorService service;
        public event EventHandler<EmpresaModel> Seleccionado;

        public void OnSeleccionado(EmpresaModel e) {
            if (this.Seleccionado != null) {
                this.Seleccionado(this, e);
            }
        }

        public SessionForm() {
            InitializeComponent();
        }

        private void SessionForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.service = new AdministradorService();
            this.gridData.DataGridCommon();

            this.Licencia();
            //if (this.service.GetDemo() < 0) {
            //    MessageBox.Show(this, "El período de prueba termino. " + this.service.GetDemo().ToString(), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    this.Close();
            //    return;
            //}

            if (this.service.Existe() == false) {
                MessageBox.Show(this, "El repositorio no existe, es necesario crear", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.service.Create();
            }

            if (this.service.Verificar() == false) {
                MessageBox.Show(this, "El repositorio no existe, es necesario crear", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.TSesion.Nuevo.Click += this.TSesion_Nuevo_Click;
            this.TSesion.Editar.Click += this.TSesion_Editar_Click;
            this.TSesion.Remover.Click += this.TSesion_Remover_Click;
            this.TSesion.Cerrar.Click += this.Cerrar_Click;
            this.TSesion.Actualizar.Click += this.TSesion_Actualizar_Click;
            this.TSesion.Guardar.Click += this.TSesion_Seleccionar_Click;
            this.TSesion.Guardar.Text = "Seleccionar";
            this.TSesion.Guardar.Image = Properties.Resources.select_16px;
            this.TSesion.Actualizar.PerformClick();
        }

        private void TSesion_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.gridData.DataSource = contribuyentes;
        }

        private void TSesion_Nuevo_Click(object sender, EventArgs e) {
            var contribuyente = new EmpresaForm(null, this.service);
            contribuyente.ShowDialog(this);
            this.TSesion_Actualizar_Click(sender, e);   
        }

        private void TSesion_Editar_Click(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                var seleccionado = this.gridData.CurrentRow.DataBoundItem as EmpresaModel;
                if (seleccionado != null) {
                    using (var espera = new EmpresaForm(seleccionado, this.service)) {
                        espera.ShowDialog(this);
                    }
                    this.TSesion_Actualizar_Click(sender, e);
                }
            }
        }

        private void TSesion_Remover_Click(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                var seleccionado = this.gridData.CurrentRow.DataBoundItem as EmpresaModel;
                if (MessageBox.Show(this, "¡Esta seguro de remover el objeto seleccionado?", "Contribuyente", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                    //if (this.service.Delete(seleccionado)) {

                    //}
                }
            }
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        private void TSesion_Seleccionar_Click(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                var seleccionado = this.gridData.CurrentRow.DataBoundItem as EmpresaModel;
                if (seleccionado != null) {
                    if (!System.IO.File.Exists(seleccionado.Repositorio)) {
                        if (MessageBox.Show(this, "El repositorio no se encuentra en la ruta especificada. ¿Quieres crearlo?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes) {
                            Jaeger.Util.Services.FileService.WriteFileText(seleccionado.Repositorio, "");

                        } else {
                            return;
                        }
                    }
                    ConfiguracionService.Contribuyente = seleccionado;
                    this.Close();
                    this.OnSeleccionado(seleccionado);
                }
            }
        }

        private void GridData_DoubleClick(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                this.TSesion.Guardar.PerformClick();
            }
        }

        private void Consultar() {
            this.contribuyentes = this.service.GetList();
            if (this.contribuyentes != null) {
                if (this.contribuyentes.Count > 3) {
                    this.TSesion.Nuevo.Visible = false;
                    this.TSesion.Editar.Visible = false;
                    this.TSesion.Remover.Visible = false;
                }
            }
        }

        public void Licencia() {
            int num;
            string _driveName = Environment.CurrentDirectory.Substring(0, 1);
            var managementObject = new ManagementObject(string.Concat("win32_logicaldisk.deviceid=\"", _driveName, ":\""));
            managementObject.Get();
            var serialNumber = managementObject["VolumeSerialNumber"].ToString();
            string producto = "licencia1";
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Concat("https://xaxx010101000.s3.us-east-1.amazonaws.com/SAT/", producto, ".txt"));
            Stream responseStream = ((HttpWebResponse)httpWebRequest.GetResponse()).GetResponseStream();
            StringBuilder stringBuilder = new StringBuilder();
            byte[] numArray = new byte[8192];
            int num1;
            do {
                num1 = responseStream.Read(numArray, 0, (int)numArray.Length);
                if (num1 == 0) {
                    continue;
                }
                string str2 = Encoding.ASCII.GetString(numArray, 0, num1);
                stringBuilder.Append(str2);
            }
            while (num1 > 0);
            DateTime utcNow = DateTime.UtcNow;
            TimeSpan timeSpan = utcNow.Subtract(new DateTime(1970, 1, 1));
            int totalSeconds = (int)timeSpan.TotalSeconds;
            num = (stringBuilder.ToString() != "ERROR" ? int.Parse(stringBuilder.ToString()) : 123);
            if (totalSeconds >= num) {
                MessageBox.Show(string.Format("LEA CON ATENCION \n\nSI ES USUARIO NUEVO SE A INSTALADO \nUNA LICENCIA DE PRUEBA CON VALIDEZ DE 72HRS\nPARA ACTIVARLO CIERRE EL PROGRAMA Y VUELVA A INGRESAR \n\nSI SU LICENCIA A CADUCADO POR FAVOR \nANOTE ESTA CLAVE {0}\nY COMUNIQUESE A www.impostoresprofesionales.com.mx", serialNumber), "LICENCIA INVALIDA", MessageBoxButtons.OKCancel, MessageBoxIcon.Hand);
                return;
                if (!Application.MessageLoop) {
                    Environment.Exit(1);
                } else {
                    Application.Exit();
                }
            }
        }
    }
}
