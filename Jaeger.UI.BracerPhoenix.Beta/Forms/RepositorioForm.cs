﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Generic;
using Jaeger.Domain.Repositorio.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Repositorio.Contracts;
using Jaeger.Aplication.Repositorio.Services;
using Jaeger.Util.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Repositorio {
    public partial class RepositorioForm : Form {
        protected IRepositorioService service;
        private BindingList<ComprobanteFiscalExtended> comprobantes;
        protected UIMenuElement menuElement;

        #region formulario
        public RepositorioForm() {
            InitializeComponent();
        }

        public RepositorioForm(UIMenuElement menuElement) { 
            InitializeComponent();
            this.menuElement = menuElement;
        }

        private void ComprobantesRepositorioForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.dataGrid.DataGridCommon();
            this.Ejercicio.Text = DateTime.Now.Year.ToString();
            this.Ejercicio.KeyPress += new KeyPressEventHandler(Extensions.TextBoxOnlyNumbers_KeyPress);
            this.Ejercicio.MaxLength = 4;
            
            this.service = ConfiguracionService.Manager.GetRepositorio();
            this.Periodo.ComboBox.DisplayMember = "Descripcion";
            this.Periodo.ComboBox.ValueMember = "Id";
            this.Periodo.ComboBox.DataSource = ConfiguracionService.GetMeses();
            this.Periodo.ComboBox.SelectedIndex = DateTime.Now.Month;

            this.Tipo.ComboBox.DisplayMember = "Descripcion";
            this.Tipo.ComboBox.ValueMember = "Id";
            this.Tipo.ComboBox.DataSource = ComunService.GetTipoComprobante();

            this.Comprobante.ComboBox.DisplayMember = "Descripcion";
            this.Comprobante.ComboBox.ValueMember = "Id";
            this.Comprobante.ComboBox.DataSource = ComunService.GetSubTipoComprobante();



            //if (this.service == null) {
            //    MessageBox.Show(this, "Properties.Resources.Message_Repositorio_Error", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //} else {
            //    var d = this.service.GetVersion();
            //    if (d == null) {
            //        MessageBox.Show(this, "Esta utilizando una versión anterior del repositorio, es necesario actualizar la versión.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }
            //}

            //this.TDescargaSAT.Enabled = ConfiguracionService.GeMenuElement("breposit_descarga1").IsEnable;
            //this.TDescargaSAT.Enabled = ConfiguracionService.GeMenuElement("breposit_descarga2").IsEnable;
        }
        #endregion 

        #region barra de herramientas
        private void TActualizar_Click(object sender, EventArgs e) {
            if (string.IsNullOrEmpty(this.Ejercicio.Text)) {
                MessageBox.Show("Ejercicio no válido", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (this.Ejercicio.Text.Length < 4) {
                MessageBox.Show("Ejercicio no válido", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            using (var espera = new WaitingForm(this.Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
                this.dataGrid.DataSource = this.comprobantes;
            }
        }

        private void TCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TImprimir_Click(object sender, EventArgs e) {
            var seleccionado = this.dataGrid.CurrentRow.DataBoundItem as ComprobanteFiscalExtended;
            if (seleccionado != null) {
                if (System.IO.File.Exists(System.IO.Path.Combine(seleccionado.PathXML, seleccionado.KeyNamePdf())) == false) {
                    if (MessageBox.Show(this, "Properties.Resources.Message_Repositorio_Imprimir_Standar", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        if (Domain.Services.ValidacionService.IsBase64String(seleccionado.XmlContentB64)) {
                            var d = CFDI.V33.Comprobante.LoadBytes(this.service.GetXmlByte(seleccionado));
                            if (d != null) {
                                //var _reporte = new ReporteForm(d);
                                //_reporte.ShowDialog(this);
                            }
                        }
                    }
                } else {
                    try {
                        System.Diagnostics.Process.Start(System.IO.Path.Combine(seleccionado.PathXML, seleccionado.KeyNamePdf()));
                    } catch (Exception ex) {
                        MessageBox.Show(this, string.Format("Properties.Resources.Message_Repositorio_Error_Abrir_Documento", ex.Message), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }
                }
            }
        }

        private void TCrearRepositorio_Click(object sender, EventArgs e) {
            if (MessageBox.Show(this, "Properties.Resources.Message_Repositorio_Crear", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                this.service.CreateRepository();
            }
        }

        private void TSerializar_Click(object sender, EventArgs e) {
            if (MessageBox.Show(this, "Properties.Resources.Message_Repositorio_Serializar", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                using (var espera = new WaitingForm(this.Serializar1)) {
                    espera.Text = "Procesando documentos ...";
                    espera.ShowDialog(this);
                }
            }
        }

        private void TInformacion_Click(object sender, EventArgs e) {
            var d = this.service.GetVersion();
            if (d != null) {
                MessageBox.Show(this, "Versión: " + d.Version, "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } else {
                MessageBox.Show(this, "No existe versión del repositorio", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void TDescargaSAT_Click(object sender, EventArgs e) {
            var descarga = new DescargaMasivaForm(this.service) { StartPosition = FormStartPosition.CenterParent };
            descarga.descargaControl.cmbAccount.Text = ConfiguracionService.Contribuyente.RFC;
            descarga.descargaControl.cmbAccount.ReadOnly = true;
            descarga.ShowDialog(this);
        }

        private void TDescargaNavegado_Click(object sender, EventArgs e) {
            var descarga = new DescargaNavegadorForm() { StartPosition = FormStartPosition.CenterParent };
            descarga.ShowDialog(this);
        }

        private void TImportarArchivo_Click(object sender, EventArgs e) {
            if (this.openFileDialog1.ShowDialog(this) == DialogResult.OK) {
                var _lista = new List<ComprobanteFiscalExtended>();
                var _cfdi = this.service.Reader(openFileDialog1.FileName);
                if (_cfdi != null) {
                    _cfdi.Tipo = "Emitidos";
                    _cfdi.Result = "Importado";
                    _cfdi.PathXML = openFileDialog1.FileName;
                    _cfdi.XmlContentB64 = FileService.ReadFileB64(this.openFileDialog1.FileName);
                    _lista.Add(_cfdi);
                    this.service.Save(_lista);
                }
            }
        }

        private void TImportarCarpeta_Click(object sender, EventArgs e) {
            if (this.folderBrowserDialog1.ShowDialog(this) == DialogResult.OK) {

            }
        }

        private void TDescargaSATefiel_Click(object sender, EventArgs e) {
            //var _descargaMasiva = new DescargaServiceSATForm();
            //_descargaMasiva.ShowDialog(this);
        }

        private void TCertificado_Click(object sender, EventArgs e) {
            //var certificadoForm = new Jaeger.UI.Forms.Comprobantes.CertificadoForm();
            //certificadoForm.ShowDialog(this);
        }
        #endregion

        #region menu contextual
        private void ContextMenuArchivoXML_Click(object sender, EventArgs e) {
            var seleccionado = this.dataGrid.CurrentRow.DataBoundItem as ComprobanteFiscalExtended;
            if (seleccionado != null) {
                if (Domain.Services.ValidacionService.IsBase64String(seleccionado.XmlContentB64)) {
                    var saveFile = new SaveFileDialog { AddExtension = true, DefaultExt = "xml", FileName = seleccionado.IdDocumento };
                    if (saveFile.ShowDialog(this) == DialogResult.OK) {
                        if (FileService.WriteFileB64(seleccionado.XmlContentB64, saveFile.FileName)) {
                            if (MessageBox.Show(this, string.Format("Properties.Resources.Message_Documento_Abrir", saveFile.FileName), "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes) {
                                try {
                                    System.Diagnostics.Process.Start(saveFile.FileName);
                                } catch (Exception ex) {
                                    MessageBox.Show(this, ex.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ContextMenuVer_Click(object sender, EventArgs e) {
            var seleccionado = this.dataGrid.CurrentRow.DataBoundItem as ComprobanteFiscalExtended;
            if (seleccionado != null) {
                if (Domain.Services.ValidacionService.IsBase64String(seleccionado.XmlContentB64)) {
                    var mostrar = new ComprobanteTreeViewForm(seleccionado) { MdiParent = this.ParentForm };
                    mostrar.Show();
                } else {
                    MessageBox.Show(this, "Properties.Resources.Message_Repositorio_Archivo_Error", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void ContextMenuSerializar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.SerializarSeleccionado)) {
                espera.Text = "Procesando ...";
                espera.ShowDialog(this);
            }
        }

        private void ContextMenuEstadoSAT_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.ConsultaEstadoSAT)) {
                espera.Text = "Consultando estado ...";
                espera.ShowDialog(this);
            }
            if (this.contextMenuEstadoSAT.Tag != null) {
                var respuesta = contextMenuEstadoSAT.Tag as string;
                MessageBox.Show(this, respuesta);
                this.contextMenuEstadoSAT.Tag = null;
            }
        }

        private void ContextMenuImprimir_Click(object sender, EventArgs e) {
            this.TImprimir_Click(sender, e);
        }
        #endregion

        #region accions del grid
        private void DataGrid_RowContextMenuStripNeeded(object sender, DataGridViewRowContextMenuStripNeededEventArgs e) {
            var seleccionado = this.dataGrid.CurrentRow.DataBoundItem as ComprobanteFiscalExtended;
            if (seleccionado != null) {
                this.contextMenuSerializar.Enabled = seleccionado.Efecto != "Pago";
            }
        }
        #endregion

        #region metodos privados
        private void Consultar() {
            this.comprobantes = this.service.GetList(this.Periodo.ComboBox.SelectedIndex, int.Parse(this.Ejercicio.Text), this.Tipo.Text, this.Comprobante.Text);
        }

        private void ConsultaEstadoSAT() {
            var seleccionado = this.dataGrid.CurrentRow.DataBoundItem as ComprobanteFiscalExtended;
            if (seleccionado != null) {
                seleccionado.Estado = this.service.EstadoSAT(seleccionado.EmisorRFC, seleccionado.ReceptorRFC, seleccionado.Total, seleccionado.IdDocumento);
                contextMenuEstadoSAT.Tag = seleccionado.Estado;
            }
        }

        private void SerializarSeleccionado() {
            var seleccionado = this.dataGrid.CurrentRow.DataBoundItem as ComprobanteFiscalExtended;
            if (seleccionado != null) {
                if (Domain.Services.ValidacionService.IsBase64String(seleccionado.XmlContentB64)) {
                    seleccionado = this.service.Serializar(seleccionado);
                    this.service.Update(seleccionado);
                } else {
                    MessageBox.Show(this, "Properties.Resources.Message_Repositorio_Archivo_Error", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void Serializar1() {
            for (int i = 0; i < this.comprobantes.Count; i++) {
                if (Domain.Services.ValidacionService.IsBase64String(this.comprobantes[i].XmlContentB64)) {
                    this.comprobantes[i] = this.service.Serializar(this.comprobantes[i]);
                    this.service.Update(this.comprobantes[i]);
                } 
            }
        }
        #endregion
    }
}
