﻿namespace Jaeger.UI.Forms {
    partial class CertificadoValidaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CertificadoValidaForm));
            this.lblTitulo = new System.Windows.Forms.Label();
            this.buttonPathPFX = new System.Windows.Forms.Button();
            this.lblOpenSSL = new System.Windows.Forms.Label();
            this.btnOpenSSL = new System.Windows.Forms.Button();
            this.btnGeneraPFX = new System.Windows.Forms.Button();
            this.txtArchivoPFX = new System.Windows.Forms.TextBox();
            this.lblArchivoPFX = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.header = new System.Windows.Forms.PictureBox();
            this.txtRazonSocial = new System.Windows.Forms.TextBox();
            this.txtTipoCertificado = new System.Windows.Forms.TextBox();
            this.lblRazonSocial = new System.Windows.Forms.Label();
            this.lblTipoCertificado = new System.Windows.Forms.Label();
            this.txtRFC = new System.Windows.Forms.TextBox();
            this.txtArchivoCertificado = new System.Windows.Forms.TextBox();
            this.lblCertificado = new System.Windows.Forms.Label();
            this.txtLlavePrivada = new System.Windows.Forms.TextBox();
            this.lblClavePrivada = new System.Windows.Forms.Label();
            this.lblPassword1 = new System.Windows.Forms.Label();
            this.lblRFC = new System.Windows.Forms.Label();
            this.txtValidoHasta = new System.Windows.Forms.TextBox();
            this.lblValidoHasta = new System.Windows.Forms.Label();
            this.txtValidoDesde = new System.Windows.Forms.TextBox();
            this.lblValidoDesde = new System.Windows.Forms.Label();
            this.txtNumeroDeSerieCertificado = new System.Windows.Forms.TextBox();
            this.txtContrasena = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnVerificar = new System.Windows.Forms.Button();
            this.txtConfirmacion = new System.Windows.Forms.TextBox();
            this.lblPassword2 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.buttonKEY = new System.Windows.Forms.Button();
            this.buttonCER = new System.Windows.Forms.Button();
            this.lblNumeroSerie = new System.Windows.Forms.Label();
            this.txtCertificadoB64 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblCertificadoB64 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.header)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.BackColor = System.Drawing.Color.White;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(38, 12);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(434, 14);
            this.lblTitulo.TabIndex = 12;
            this.lblTitulo.Text = "Validación de Certificados";
            // 
            // buttonPathPFX
            // 
            this.buttonPathPFX.Location = new System.Drawing.Point(358, 32);
            this.buttonPathPFX.Name = "buttonPathPFX";
            this.buttonPathPFX.Size = new System.Drawing.Size(29, 20);
            this.buttonPathPFX.TabIndex = 7;
            this.buttonPathPFX.Text = "...";
            this.buttonPathPFX.Click += new System.EventHandler(this.ButtonPathPFX_Click);
            // 
            // lblOpenSSL
            // 
            this.lblOpenSSL.Location = new System.Drawing.Point(6, 60);
            this.lblOpenSSL.Name = "lblOpenSSL";
            this.lblOpenSSL.Size = new System.Drawing.Size(104, 18);
            this.lblOpenSSL.TabIndex = 6;
            this.lblOpenSSL.Text = "ubucación openSSL";
            // 
            // btnOpenSSL
            // 
            this.btnOpenSSL.Location = new System.Drawing.Point(152, 55);
            this.btnOpenSSL.Name = "btnOpenSSL";
            this.btnOpenSSL.Size = new System.Drawing.Size(75, 23);
            this.btnOpenSSL.TabIndex = 5;
            this.btnOpenSSL.Text = "OpenSSL";
            this.btnOpenSSL.Click += new System.EventHandler(this.ButtonOpenSSL_Click);
            // 
            // btnGeneraPFX
            // 
            this.btnGeneraPFX.Location = new System.Drawing.Point(233, 55);
            this.btnGeneraPFX.Name = "btnGeneraPFX";
            this.btnGeneraPFX.Size = new System.Drawing.Size(154, 23);
            this.btnGeneraPFX.TabIndex = 1;
            this.btnGeneraPFX.Text = "Generar archivo PFX";
            this.btnGeneraPFX.Click += new System.EventHandler(this.ButtonGeneraPFX_Click);
            // 
            // txtArchivoPFX
            // 
            this.txtArchivoPFX.Location = new System.Drawing.Point(6, 32);
            this.txtArchivoPFX.Name = "txtArchivoPFX";
            this.txtArchivoPFX.Size = new System.Drawing.Size(346, 20);
            this.txtArchivoPFX.TabIndex = 0;
            // 
            // lblArchivoPFX
            // 
            this.lblArchivoPFX.Location = new System.Drawing.Point(6, 16);
            this.lblArchivoPFX.Name = "lblArchivoPFX";
            this.lblArchivoPFX.Size = new System.Drawing.Size(93, 18);
            this.lblArchivoPFX.TabIndex = 4;
            this.lblArchivoPFX.Text = "Archivo PFX (.pfx)";
            // 
            // groupBox2
            // 
            this.groupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox2.Controls.Add(this.buttonPathPFX);
            this.groupBox2.Controls.Add(this.lblOpenSSL);
            this.groupBox2.Controls.Add(this.btnOpenSSL);
            this.groupBox2.Controls.Add(this.btnGeneraPFX);
            this.groupBox2.Controls.Add(this.txtArchivoPFX);
            this.groupBox2.Controls.Add(this.lblArchivoPFX);
            this.groupBox2.Location = new System.Drawing.Point(0, 278);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(398, 89);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Opciones";
            // 
            // header
            // 
            this.header.BackColor = System.Drawing.Color.White;
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Image = global::Jaeger.UI.Properties.Resources.certificate_32;
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(720, 32);
            this.header.TabIndex = 11;
            this.header.TabStop = false;
            // 
            // txtRazonSocial
            // 
            this.txtRazonSocial.BackColor = System.Drawing.SystemColors.Window;
            this.txtRazonSocial.Location = new System.Drawing.Point(6, 32);
            this.txtRazonSocial.Name = "txtRazonSocial";
            this.txtRazonSocial.ReadOnly = true;
            this.txtRazonSocial.Size = new System.Drawing.Size(295, 20);
            this.txtRazonSocial.TabIndex = 5;
            // 
            // txtTipoCertificado
            // 
            this.txtTipoCertificado.BackColor = System.Drawing.SystemColors.Window;
            this.txtTipoCertificado.Location = new System.Drawing.Point(6, 156);
            this.txtTipoCertificado.Name = "txtTipoCertificado";
            this.txtTipoCertificado.ReadOnly = true;
            this.txtTipoCertificado.Size = new System.Drawing.Size(145, 20);
            this.txtTipoCertificado.TabIndex = 5;
            // 
            // lblRazonSocial
            // 
            this.lblRazonSocial.AutoSize = true;
            this.lblRazonSocial.Location = new System.Drawing.Point(6, 16);
            this.lblRazonSocial.Name = "lblRazonSocial";
            this.lblRazonSocial.Size = new System.Drawing.Size(70, 13);
            this.lblRazonSocial.TabIndex = 16;
            this.lblRazonSocial.Text = "Razón Social";
            // 
            // lblTipoCertificado
            // 
            this.lblTipoCertificado.Location = new System.Drawing.Point(6, 140);
            this.lblTipoCertificado.Name = "lblTipoCertificado";
            this.lblTipoCertificado.Size = new System.Drawing.Size(99, 18);
            this.lblTipoCertificado.TabIndex = 16;
            this.lblTipoCertificado.Text = "Tipo de certificado";
            // 
            // txtRFC
            // 
            this.txtRFC.BackColor = System.Drawing.SystemColors.Window;
            this.txtRFC.Location = new System.Drawing.Point(156, 78);
            this.txtRFC.Name = "txtRFC";
            this.txtRFC.ReadOnly = true;
            this.txtRFC.Size = new System.Drawing.Size(145, 20);
            this.txtRFC.TabIndex = 2;
            this.txtRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtArchivoCertificado
            // 
            this.txtArchivoCertificado.Location = new System.Drawing.Point(6, 45);
            this.txtArchivoCertificado.Name = "txtArchivoCertificado";
            this.txtArchivoCertificado.Size = new System.Drawing.Size(346, 20);
            this.txtArchivoCertificado.TabIndex = 0;
            // 
            // lblCertificado
            // 
            this.lblCertificado.Location = new System.Drawing.Point(6, 29);
            this.lblCertificado.Name = "lblCertificado";
            this.lblCertificado.Size = new System.Drawing.Size(90, 18);
            this.lblCertificado.TabIndex = 0;
            this.lblCertificado.Text = "Certificado (.cer):";
            // 
            // txtLlavePrivada
            // 
            this.txtLlavePrivada.Location = new System.Drawing.Point(6, 84);
            this.txtLlavePrivada.Name = "txtLlavePrivada";
            this.txtLlavePrivada.Size = new System.Drawing.Size(346, 20);
            this.txtLlavePrivada.TabIndex = 2;
            // 
            // lblClavePrivada
            // 
            this.lblClavePrivada.Location = new System.Drawing.Point(6, 68);
            this.lblClavePrivada.Name = "lblClavePrivada";
            this.lblClavePrivada.Size = new System.Drawing.Size(105, 18);
            this.lblClavePrivada.TabIndex = 1;
            this.lblClavePrivada.Text = "Clave privada (.key):";
            // 
            // lblPassword1
            // 
            this.lblPassword1.Location = new System.Drawing.Point(6, 107);
            this.lblPassword1.Name = "lblPassword1";
            this.lblPassword1.Size = new System.Drawing.Size(154, 18);
            this.lblPassword1.TabIndex = 1;
            this.lblPassword1.Text = "Contraseña de clave privada:*";
            // 
            // lblRFC
            // 
            this.lblRFC.AutoSize = true;
            this.lblRFC.Location = new System.Drawing.Point(156, 62);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(28, 13);
            this.lblRFC.TabIndex = 14;
            this.lblRFC.Text = "RFC";
            // 
            // txtValidoHasta
            // 
            this.txtValidoHasta.BackColor = System.Drawing.SystemColors.Window;
            this.txtValidoHasta.Location = new System.Drawing.Point(156, 117);
            this.txtValidoHasta.Name = "txtValidoHasta";
            this.txtValidoHasta.ReadOnly = true;
            this.txtValidoHasta.Size = new System.Drawing.Size(145, 20);
            this.txtValidoHasta.TabIndex = 4;
            this.txtValidoHasta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblValidoHasta
            // 
            this.lblValidoHasta.Location = new System.Drawing.Point(156, 101);
            this.lblValidoHasta.Name = "lblValidoHasta";
            this.lblValidoHasta.Size = new System.Drawing.Size(67, 18);
            this.lblValidoHasta.TabIndex = 12;
            this.lblValidoHasta.Text = "Válido hasta";
            // 
            // txtValidoDesde
            // 
            this.txtValidoDesde.BackColor = System.Drawing.SystemColors.Window;
            this.txtValidoDesde.Location = new System.Drawing.Point(6, 117);
            this.txtValidoDesde.Name = "txtValidoDesde";
            this.txtValidoDesde.ReadOnly = true;
            this.txtValidoDesde.Size = new System.Drawing.Size(145, 20);
            this.txtValidoDesde.TabIndex = 3;
            this.txtValidoDesde.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblValidoDesde
            // 
            this.lblValidoDesde.Location = new System.Drawing.Point(6, 101);
            this.lblValidoDesde.Name = "lblValidoDesde";
            this.lblValidoDesde.Size = new System.Drawing.Size(73, 18);
            this.lblValidoDesde.TabIndex = 10;
            this.lblValidoDesde.Text = "Válido desde:";
            // 
            // txtNumeroDeSerieCertificado
            // 
            this.txtNumeroDeSerieCertificado.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumeroDeSerieCertificado.Location = new System.Drawing.Point(6, 78);
            this.txtNumeroDeSerieCertificado.Name = "txtNumeroDeSerieCertificado";
            this.txtNumeroDeSerieCertificado.ReadOnly = true;
            this.txtNumeroDeSerieCertificado.Size = new System.Drawing.Size(145, 20);
            this.txtNumeroDeSerieCertificado.TabIndex = 1;
            this.txtNumeroDeSerieCertificado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtContrasena
            // 
            this.txtContrasena.Location = new System.Drawing.Point(6, 123);
            this.txtContrasena.Name = "txtContrasena";
            this.txtContrasena.PasswordChar = '*';
            this.txtContrasena.Size = new System.Drawing.Size(220, 20);
            this.txtContrasena.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.btnVerificar);
            this.groupBox1.Controls.Add(this.txtConfirmacion);
            this.groupBox1.Controls.Add(this.lblPassword2);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.buttonKEY);
            this.groupBox1.Controls.Add(this.buttonCER);
            this.groupBox1.Controls.Add(this.txtArchivoCertificado);
            this.groupBox1.Controls.Add(this.txtContrasena);
            this.groupBox1.Controls.Add(this.lblCertificado);
            this.groupBox1.Controls.Add(this.txtLlavePrivada);
            this.groupBox1.Controls.Add(this.lblClavePrivada);
            this.groupBox1.Controls.Add(this.lblPassword1);
            this.groupBox1.Location = new System.Drawing.Point(0, 56);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(398, 222);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos del certificado";
            // 
            // btnVerificar
            // 
            this.btnVerificar.Image = global::Jaeger.UI.Properties.Resources.keepass_16px;
            this.btnVerificar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVerificar.Location = new System.Drawing.Point(287, 193);
            this.btnVerificar.Name = "btnVerificar";
            this.btnVerificar.Size = new System.Drawing.Size(100, 23);
            this.btnVerificar.TabIndex = 7;
            this.btnVerificar.Text = "Verificar";
            this.btnVerificar.Click += new System.EventHandler(this.ButtonVerificar_Click);
            // 
            // txtConfirmacion
            // 
            this.txtConfirmacion.Location = new System.Drawing.Point(6, 162);
            this.txtConfirmacion.Name = "txtConfirmacion";
            this.txtConfirmacion.PasswordChar = '*';
            this.txtConfirmacion.Size = new System.Drawing.Size(220, 20);
            this.txtConfirmacion.TabIndex = 6;
            // 
            // lblPassword2
            // 
            this.lblPassword2.Location = new System.Drawing.Point(6, 146);
            this.lblPassword2.Name = "lblPassword2";
            this.lblPassword2.Size = new System.Drawing.Size(238, 18);
            this.lblPassword2.TabIndex = 6;
            this.lblPassword2.Text = "Confirmación de contraseña de clave privada:*";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(232, 125);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(94, 17);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "ver caracteres";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // buttonKEY
            // 
            this.buttonKEY.Location = new System.Drawing.Point(358, 82);
            this.buttonKEY.Name = "buttonKEY";
            this.buttonKEY.Size = new System.Drawing.Size(29, 23);
            this.buttonKEY.TabIndex = 3;
            this.buttonKEY.Text = "...";
            this.buttonKEY.Click += new System.EventHandler(this.ButtonKEY_Click);
            // 
            // buttonCER
            // 
            this.buttonCER.Location = new System.Drawing.Point(358, 43);
            this.buttonCER.Name = "buttonCER";
            this.buttonCER.Size = new System.Drawing.Size(29, 23);
            this.buttonCER.TabIndex = 1;
            this.buttonCER.Text = "...";
            this.buttonCER.Click += new System.EventHandler(this.ButtonCER_Click);
            // 
            // lblNumeroSerie
            // 
            this.lblNumeroSerie.Location = new System.Drawing.Point(6, 62);
            this.lblNumeroSerie.Name = "lblNumeroSerie";
            this.lblNumeroSerie.Size = new System.Drawing.Size(89, 18);
            this.lblNumeroSerie.TabIndex = 8;
            this.lblNumeroSerie.Text = "Número de serie";
            // 
            // txtCertificadoB64
            // 
            this.txtCertificadoB64.BackColor = System.Drawing.SystemColors.Window;
            this.txtCertificadoB64.Location = new System.Drawing.Point(6, 195);
            this.txtCertificadoB64.Multiline = true;
            this.txtCertificadoB64.Name = "txtCertificadoB64";
            this.txtCertificadoB64.ReadOnly = true;
            this.txtCertificadoB64.Size = new System.Drawing.Size(295, 104);
            this.txtCertificadoB64.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox3.Controls.Add(this.txtRazonSocial);
            this.groupBox3.Controls.Add(this.txtTipoCertificado);
            this.groupBox3.Controls.Add(this.lblRazonSocial);
            this.groupBox3.Controls.Add(this.lblTipoCertificado);
            this.groupBox3.Controls.Add(this.txtRFC);
            this.groupBox3.Controls.Add(this.lblRFC);
            this.groupBox3.Controls.Add(this.txtValidoHasta);
            this.groupBox3.Controls.Add(this.lblValidoHasta);
            this.groupBox3.Controls.Add(this.txtValidoDesde);
            this.groupBox3.Controls.Add(this.lblValidoDesde);
            this.groupBox3.Controls.Add(this.txtNumeroDeSerieCertificado);
            this.groupBox3.Controls.Add(this.lblNumeroSerie);
            this.groupBox3.Controls.Add(this.txtCertificadoB64);
            this.groupBox3.Controls.Add(this.lblCertificadoB64);
            this.groupBox3.Location = new System.Drawing.Point(404, 62);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(310, 305);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Información del certificado";
            // 
            // lblCertificadoB64
            // 
            this.lblCertificadoB64.Location = new System.Drawing.Point(6, 179);
            this.lblCertificadoB64.Name = "lblCertificadoB64";
            this.lblCertificadoB64.Size = new System.Drawing.Size(102, 18);
            this.lblCertificadoB64.TabIndex = 8;
            this.lblCertificadoB64.Text = "Certificado Base 64";
            // 
            // CertificadoValidaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 372);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.header);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CertificadoValidaForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Certificado PFX";
            this.Load += new System.EventHandler(this.CertificadoForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.header)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Button buttonPathPFX;
        private System.Windows.Forms.Label lblOpenSSL;
        private System.Windows.Forms.Button btnOpenSSL;
        private System.Windows.Forms.Button btnGeneraPFX;
        private System.Windows.Forms.TextBox txtArchivoPFX;
        private System.Windows.Forms.Label lblArchivoPFX;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox header;
        private System.Windows.Forms.TextBox txtRazonSocial;
        private System.Windows.Forms.TextBox txtTipoCertificado;
        private System.Windows.Forms.Label lblRazonSocial;
        private System.Windows.Forms.Label lblTipoCertificado;
        private System.Windows.Forms.TextBox txtRFC;
        private System.Windows.Forms.TextBox txtArchivoCertificado;
        private System.Windows.Forms.Label lblCertificado;
        private System.Windows.Forms.TextBox txtLlavePrivada;
        private System.Windows.Forms.Label lblClavePrivada;
        private System.Windows.Forms.Label lblPassword1;
        private System.Windows.Forms.Label lblRFC;
        private System.Windows.Forms.TextBox txtValidoHasta;
        private System.Windows.Forms.Label lblValidoHasta;
        private System.Windows.Forms.TextBox txtValidoDesde;
        private System.Windows.Forms.Label lblValidoDesde;
        private System.Windows.Forms.TextBox txtNumeroDeSerieCertificado;
        private System.Windows.Forms.TextBox txtContrasena;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnVerificar;
        private System.Windows.Forms.TextBox txtConfirmacion;
        private System.Windows.Forms.Label lblPassword2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button buttonKEY;
        private System.Windows.Forms.Button buttonCER;
        private System.Windows.Forms.Label lblNumeroSerie;
        private System.Windows.Forms.TextBox txtCertificadoB64;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblCertificadoB64;
    }
}