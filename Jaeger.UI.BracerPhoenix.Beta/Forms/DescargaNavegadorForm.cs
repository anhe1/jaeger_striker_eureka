﻿using Jaeger.Repositorio.V3;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jaeger.UI.Forms {
    public partial class DescargaNavegadorForm : Form {
        DescargaXml5SAT descargaXml5= new DescargaXml5SAT();
        public DescargaNavegadorForm() {
            InitializeComponent();
        }

        private void DescargaNavegadorForm_Load(object sender, EventArgs e) {
            descargaXml5.Descargar(this.webBrowser);
        }

        #region navegador
        private void webBrowser_Navigated(object sender, WebBrowserNavigatedEventArgs e) {

        }

        private void webBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e) {

        }

        private void webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e) {
            this.lblStatus.Text = e.Url.ToString();
        }
        #endregion

        private void TDownload_Click(object sender, EventArgs e) {
            this.descargaXml5.Procesar();
        }
    }
}
