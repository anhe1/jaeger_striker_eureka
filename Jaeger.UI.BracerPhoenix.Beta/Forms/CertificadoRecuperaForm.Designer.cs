﻿namespace Jaeger.UI.Forms {
    partial class CertificadoRecuperaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCargar = new System.Windows.Forms.Button();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.txtRazonSocial = new System.Windows.Forms.TextBox();
            this.txtTipoCertificado = new System.Windows.Forms.TextBox();
            this.lblRazonSocial = new System.Windows.Forms.Label();
            this.lblTipoCertificado = new System.Windows.Forms.Label();
            this.RFC = new System.Windows.Forms.TextBox();
            this.lblRFC = new System.Windows.Forms.Label();
            this.txtValidoHasta = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblValidoHasta = new System.Windows.Forms.Label();
            this.txtValidoDesde = new System.Windows.Forms.TextBox();
            this.lblValidoDesde = new System.Windows.Forms.Label();
            this.txtNumeroDeSerie = new System.Windows.Forms.TextBox();
            this.lblSerie = new System.Windows.Forms.Label();
            this.txtCertificadoB64 = new System.Windows.Forms.TextBox();
            this.lblBase64 = new System.Windows.Forms.Label();
            this.grpInformacion = new System.Windows.Forms.GroupBox();
            this.lblInformacion = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grpInformacion.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCargar
            // 
            this.btnCargar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCargar.Location = new System.Drawing.Point(206, 375);
            this.btnCargar.Name = "btnCargar";
            this.btnCargar.Size = new System.Drawing.Size(75, 23);
            this.btnCargar.TabIndex = 54;
            this.btnCargar.Text = "Cargar";
            this.btnCargar.UseVisualStyleBackColor = true;
            this.btnCargar.Click += new System.EventHandler(this.btnCargar_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar.Location = new System.Drawing.Point(287, 375);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(75, 23);
            this.btnCerrar.TabIndex = 53;
            this.btnCerrar.Text = "Cerrar";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // txtRazonSocial
            // 
            this.txtRazonSocial.BackColor = System.Drawing.SystemColors.Window;
            this.txtRazonSocial.Location = new System.Drawing.Point(6, 32);
            this.txtRazonSocial.Name = "txtRazonSocial";
            this.txtRazonSocial.ReadOnly = true;
            this.txtRazonSocial.Size = new System.Drawing.Size(335, 20);
            this.txtRazonSocial.TabIndex = 5;
            // 
            // txtTipoCertificado
            // 
            this.txtTipoCertificado.BackColor = System.Drawing.SystemColors.Window;
            this.txtTipoCertificado.Location = new System.Drawing.Point(6, 156);
            this.txtTipoCertificado.Name = "txtTipoCertificado";
            this.txtTipoCertificado.ReadOnly = true;
            this.txtTipoCertificado.Size = new System.Drawing.Size(145, 20);
            this.txtTipoCertificado.TabIndex = 5;
            // 
            // lblRazonSocial
            // 
            this.lblRazonSocial.AutoSize = true;
            this.lblRazonSocial.Location = new System.Drawing.Point(6, 16);
            this.lblRazonSocial.Name = "lblRazonSocial";
            this.lblRazonSocial.Size = new System.Drawing.Size(70, 13);
            this.lblRazonSocial.TabIndex = 16;
            this.lblRazonSocial.Text = "Razón Social";
            // 
            // lblTipoCertificado
            // 
            this.lblTipoCertificado.Location = new System.Drawing.Point(6, 140);
            this.lblTipoCertificado.Name = "lblTipoCertificado";
            this.lblTipoCertificado.Size = new System.Drawing.Size(99, 18);
            this.lblTipoCertificado.TabIndex = 16;
            this.lblTipoCertificado.Text = "Tipo de certificado";
            // 
            // RFC
            // 
            this.RFC.BackColor = System.Drawing.SystemColors.Window;
            this.RFC.Location = new System.Drawing.Point(174, 78);
            this.RFC.Name = "RFC";
            this.RFC.ReadOnly = true;
            this.RFC.Size = new System.Drawing.Size(167, 20);
            this.RFC.TabIndex = 2;
            this.RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblRFC
            // 
            this.lblRFC.AutoSize = true;
            this.lblRFC.Location = new System.Drawing.Point(174, 62);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(28, 13);
            this.lblRFC.TabIndex = 14;
            this.lblRFC.Text = "RFC";
            // 
            // txtValidoHasta
            // 
            this.txtValidoHasta.BackColor = System.Drawing.SystemColors.Window;
            this.txtValidoHasta.Location = new System.Drawing.Point(174, 117);
            this.txtValidoHasta.Name = "txtValidoHasta";
            this.txtValidoHasta.ReadOnly = true;
            this.txtValidoHasta.Size = new System.Drawing.Size(167, 20);
            this.txtValidoHasta.TabIndex = 4;
            this.txtValidoHasta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(374, 50);
            this.pictureBox1.TabIndex = 50;
            this.pictureBox1.TabStop = false;
            // 
            // lblValidoHasta
            // 
            this.lblValidoHasta.Location = new System.Drawing.Point(174, 101);
            this.lblValidoHasta.Name = "lblValidoHasta";
            this.lblValidoHasta.Size = new System.Drawing.Size(67, 18);
            this.lblValidoHasta.TabIndex = 12;
            this.lblValidoHasta.Text = "Válido hasta";
            // 
            // txtValidoDesde
            // 
            this.txtValidoDesde.BackColor = System.Drawing.SystemColors.Window;
            this.txtValidoDesde.Location = new System.Drawing.Point(6, 117);
            this.txtValidoDesde.Name = "txtValidoDesde";
            this.txtValidoDesde.ReadOnly = true;
            this.txtValidoDesde.Size = new System.Drawing.Size(162, 20);
            this.txtValidoDesde.TabIndex = 3;
            this.txtValidoDesde.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblValidoDesde
            // 
            this.lblValidoDesde.Location = new System.Drawing.Point(6, 101);
            this.lblValidoDesde.Name = "lblValidoDesde";
            this.lblValidoDesde.Size = new System.Drawing.Size(73, 18);
            this.lblValidoDesde.TabIndex = 10;
            this.lblValidoDesde.Text = "Válido desde:";
            // 
            // txtNumeroDeSerie
            // 
            this.txtNumeroDeSerie.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumeroDeSerie.Location = new System.Drawing.Point(6, 78);
            this.txtNumeroDeSerie.Name = "txtNumeroDeSerie";
            this.txtNumeroDeSerie.Size = new System.Drawing.Size(162, 20);
            this.txtNumeroDeSerie.TabIndex = 1;
            this.txtNumeroDeSerie.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblSerie
            // 
            this.lblSerie.Location = new System.Drawing.Point(6, 62);
            this.lblSerie.Name = "lblSerie";
            this.lblSerie.Size = new System.Drawing.Size(89, 18);
            this.lblSerie.TabIndex = 8;
            this.lblSerie.Text = "Número de serie";
            // 
            // txtCertificadoB64
            // 
            this.txtCertificadoB64.BackColor = System.Drawing.SystemColors.Window;
            this.txtCertificadoB64.Location = new System.Drawing.Point(6, 195);
            this.txtCertificadoB64.Multiline = true;
            this.txtCertificadoB64.Name = "txtCertificadoB64";
            this.txtCertificadoB64.ReadOnly = true;
            this.txtCertificadoB64.Size = new System.Drawing.Size(335, 104);
            this.txtCertificadoB64.TabIndex = 0;
            // 
            // lblBase64
            // 
            this.lblBase64.Location = new System.Drawing.Point(6, 179);
            this.lblBase64.Name = "lblBase64";
            this.lblBase64.Size = new System.Drawing.Size(102, 18);
            this.lblBase64.TabIndex = 8;
            this.lblBase64.Text = "Certificado Base 64";
            // 
            // grpInformacion
            // 
            this.grpInformacion.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grpInformacion.Controls.Add(this.txtRazonSocial);
            this.grpInformacion.Controls.Add(this.txtTipoCertificado);
            this.grpInformacion.Controls.Add(this.lblRazonSocial);
            this.grpInformacion.Controls.Add(this.lblTipoCertificado);
            this.grpInformacion.Controls.Add(this.RFC);
            this.grpInformacion.Controls.Add(this.lblRFC);
            this.grpInformacion.Controls.Add(this.txtValidoHasta);
            this.grpInformacion.Controls.Add(this.lblValidoHasta);
            this.grpInformacion.Controls.Add(this.txtValidoDesde);
            this.grpInformacion.Controls.Add(this.lblValidoDesde);
            this.grpInformacion.Controls.Add(this.txtNumeroDeSerie);
            this.grpInformacion.Controls.Add(this.lblSerie);
            this.grpInformacion.Controls.Add(this.txtCertificadoB64);
            this.grpInformacion.Controls.Add(this.lblBase64);
            this.grpInformacion.Location = new System.Drawing.Point(12, 56);
            this.grpInformacion.Name = "grpInformacion";
            this.grpInformacion.Size = new System.Drawing.Size(347, 305);
            this.grpInformacion.TabIndex = 52;
            this.grpInformacion.TabStop = false;
            // 
            // lblInformacion
            // 
            this.lblInformacion.AutoSize = true;
            this.lblInformacion.BackColor = System.Drawing.Color.White;
            this.lblInformacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInformacion.Location = new System.Drawing.Point(9, 12);
            this.lblInformacion.Name = "lblInformacion";
            this.lblInformacion.Size = new System.Drawing.Size(192, 16);
            this.lblInformacion.TabIndex = 51;
            this.lblInformacion.Text = "Información del Certificado";
            // 
            // CertificadoRecuperaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 410);
            this.Controls.Add(this.btnCargar);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.lblInformacion);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.grpInformacion);
            this.Name = "CertificadoRecuperaForm";
            this.Text = "CertificadoRecuperaForm";
            this.Load += new System.EventHandler(this.CertificadoRecuperaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grpInformacion.ResumeLayout(false);
            this.grpInformacion.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCargar;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.TextBox txtRazonSocial;
        private System.Windows.Forms.TextBox txtTipoCertificado;
        private System.Windows.Forms.Label lblRazonSocial;
        private System.Windows.Forms.Label lblTipoCertificado;
        private System.Windows.Forms.TextBox RFC;
        private System.Windows.Forms.Label lblRFC;
        private System.Windows.Forms.TextBox txtValidoHasta;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblValidoHasta;
        private System.Windows.Forms.TextBox txtValidoDesde;
        private System.Windows.Forms.Label lblValidoDesde;
        private System.Windows.Forms.TextBox txtNumeroDeSerie;
        private System.Windows.Forms.Label lblSerie;
        private System.Windows.Forms.TextBox txtCertificadoB64;
        private System.Windows.Forms.Label lblBase64;
        private System.Windows.Forms.GroupBox grpInformacion;
        private System.Windows.Forms.Label lblInformacion;
    }
}