﻿namespace Jaeger.UI.Forms {
    partial class DescargaControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.picboxCaptcha = new System.Windows.Forms.PictureBox();
            this.Examinar = new System.Windows.Forms.Button();
            this.Captcha = new System.Windows.Forms.TextBox();
            this.lblCaptch = new System.Windows.Forms.Label();
            this.Estado = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TipoComprobante = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.Password = new System.Windows.Forms.TextBox();
            this.cmbAccount = new System.Windows.Forms.TextBox();
            this.lblRFC = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FechaFinal = new System.Windows.Forms.DateTimePicker();
            this.FechaInicial = new System.Windows.Forms.DateTimePicker();
            this.GuardarEn = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Descargar = new System.Windows.Forms.Button();
            this.gConsulta = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.FiltrarRFC = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TipoComplemento = new System.Windows.Forms.ComboBox();
            this.gAutenticacion = new System.Windows.Forms.GroupBox();
            this.Recargar = new System.Windows.Forms.Button();
            this.BoxLog = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picboxCaptcha)).BeginInit();
            this.gConsulta.SuspendLayout();
            this.gAutenticacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // picboxCaptcha
            // 
            this.picboxCaptcha.BackColor = System.Drawing.Color.White;
            this.picboxCaptcha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picboxCaptcha.Location = new System.Drawing.Point(9, 69);
            this.picboxCaptcha.Name = "picboxCaptcha";
            this.picboxCaptcha.Size = new System.Drawing.Size(235, 70);
            this.picboxCaptcha.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picboxCaptcha.TabIndex = 39;
            this.picboxCaptcha.TabStop = false;
            // 
            // Examinar
            // 
            this.Examinar.Location = new System.Drawing.Point(188, 199);
            this.Examinar.Name = "Examinar";
            this.Examinar.Size = new System.Drawing.Size(75, 23);
            this.Examinar.TabIndex = 6;
            this.Examinar.Text = "Examinar";
            this.Examinar.UseVisualStyleBackColor = true;
            this.Examinar.Click += new System.EventHandler(this.Examinar_Click);
            // 
            // Captcha
            // 
            this.Captcha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Captcha.Location = new System.Drawing.Point(57, 145);
            this.Captcha.Name = "Captcha";
            this.Captcha.Size = new System.Drawing.Size(129, 22);
            this.Captcha.TabIndex = 2;
            // 
            // lblCaptch
            // 
            this.lblCaptch.AutoSize = true;
            this.lblCaptch.Location = new System.Drawing.Point(6, 150);
            this.lblCaptch.Name = "lblCaptch";
            this.lblCaptch.Size = new System.Drawing.Size(47, 13);
            this.lblCaptch.TabIndex = 35;
            this.lblCaptch.Text = "Captcha";
            // 
            // Estado
            // 
            this.Estado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Estado.FormattingEnabled = true;
            this.Estado.Location = new System.Drawing.Point(137, 95);
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(126, 21);
            this.Estado.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "Estado del comprobante";
            // 
            // TipoComprobante
            // 
            this.TipoComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TipoComprobante.FormattingEnabled = true;
            this.TipoComprobante.Location = new System.Drawing.Point(125, 16);
            this.TipoComprobante.Name = "TipoComprobante";
            this.TipoComprobante.Size = new System.Drawing.Size(138, 21);
            this.TipoComprobante.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "Tipo de Comprobante:";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(6, 47);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(88, 13);
            this.lblPassword.TabIndex = 29;
            this.lblPassword.Text = "Contraseña CIEC";
            // 
            // Password
            // 
            this.Password.Location = new System.Drawing.Point(129, 43);
            this.Password.Name = "Password";
            this.Password.PasswordChar = '*';
            this.Password.Size = new System.Drawing.Size(138, 20);
            this.Password.TabIndex = 1;
            // 
            // cmbAccount
            // 
            this.cmbAccount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.cmbAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAccount.Location = new System.Drawing.Point(129, 16);
            this.cmbAccount.MaxLength = 14;
            this.cmbAccount.Name = "cmbAccount";
            this.cmbAccount.Size = new System.Drawing.Size(138, 22);
            this.cmbAccount.TabIndex = 0;
            // 
            // lblRFC
            // 
            this.lblRFC.AutoSize = true;
            this.lblRFC.Location = new System.Drawing.Point(6, 21);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(28, 13);
            this.lblRFC.TabIndex = 28;
            this.lblRFC.Text = "RFC";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Periodo de Descarga";
            // 
            // FechaFinal
            // 
            this.FechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaFinal.Location = new System.Drawing.Point(145, 69);
            this.FechaFinal.Name = "FechaFinal";
            this.FechaFinal.Size = new System.Drawing.Size(118, 20);
            this.FechaFinal.TabIndex = 2;
            // 
            // FechaInicial
            // 
            this.FechaInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaInicial.Location = new System.Drawing.Point(9, 69);
            this.FechaInicial.Name = "FechaInicial";
            this.FechaInicial.Size = new System.Drawing.Size(118, 20);
            this.FechaInicial.TabIndex = 1;
            // 
            // GuardarEn
            // 
            this.GuardarEn.Location = new System.Drawing.Point(78, 202);
            this.GuardarEn.Name = "GuardarEn";
            this.GuardarEn.Size = new System.Drawing.Size(104, 20);
            this.GuardarEn.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 205);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 33;
            this.label2.Text = "Guardar en:";
            // 
            // Descargar
            // 
            this.Descargar.Location = new System.Drawing.Point(192, 145);
            this.Descargar.Name = "Descargar";
            this.Descargar.Size = new System.Drawing.Size(75, 23);
            this.Descargar.TabIndex = 3;
            this.Descargar.Text = "Descargar";
            this.Descargar.UseVisualStyleBackColor = true;
            this.Descargar.Click += new System.EventHandler(this.Descargar_Click);
            // 
            // gConsulta
            // 
            this.gConsulta.Controls.Add(this.label8);
            this.gConsulta.Controls.Add(this.FiltrarRFC);
            this.gConsulta.Controls.Add(this.label5);
            this.gConsulta.Controls.Add(this.label2);
            this.gConsulta.Controls.Add(this.GuardarEn);
            this.gConsulta.Controls.Add(this.FechaInicial);
            this.gConsulta.Controls.Add(this.Examinar);
            this.gConsulta.Controls.Add(this.FechaFinal);
            this.gConsulta.Controls.Add(this.label1);
            this.gConsulta.Controls.Add(this.TipoComprobante);
            this.gConsulta.Controls.Add(this.label7);
            this.gConsulta.Controls.Add(this.label6);
            this.gConsulta.Controls.Add(this.TipoComplemento);
            this.gConsulta.Controls.Add(this.Estado);
            this.gConsulta.Location = new System.Drawing.Point(3, 3);
            this.gConsulta.Name = "gConsulta";
            this.gConsulta.Size = new System.Drawing.Size(275, 233);
            this.gConsulta.TabIndex = 0;
            this.gConsulta.TabStop = false;
            this.gConsulta.Text = "Consulta";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 175);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 35;
            this.label8.Text = "Filtrar RFC:";
            // 
            // FiltrarRFC
            // 
            this.FiltrarRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.FiltrarRFC.Location = new System.Drawing.Point(78, 172);
            this.FiltrarRFC.MaxLength = 14;
            this.FiltrarRFC.Name = "FiltrarRFC";
            this.FiltrarRFC.Size = new System.Drawing.Size(104, 20);
            this.FiltrarRFC.TabIndex = 34;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 126);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(182, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Tipo de Comprobante (Complemento)";
            // 
            // TipoComplemento
            // 
            this.TipoComplemento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TipoComplemento.FormattingEnabled = true;
            this.TipoComplemento.Location = new System.Drawing.Point(12, 145);
            this.TipoComplemento.Name = "TipoComplemento";
            this.TipoComplemento.Size = new System.Drawing.Size(251, 21);
            this.TipoComplemento.TabIndex = 4;
            // 
            // gAutenticacion
            // 
            this.gAutenticacion.Controls.Add(this.Recargar);
            this.gAutenticacion.Controls.Add(this.BoxLog);
            this.gAutenticacion.Controls.Add(this.lblRFC);
            this.gAutenticacion.Controls.Add(this.picboxCaptcha);
            this.gAutenticacion.Controls.Add(this.cmbAccount);
            this.gAutenticacion.Controls.Add(this.Descargar);
            this.gAutenticacion.Controls.Add(this.Password);
            this.gAutenticacion.Controls.Add(this.lblPassword);
            this.gAutenticacion.Controls.Add(this.lblCaptch);
            this.gAutenticacion.Controls.Add(this.Captcha);
            this.gAutenticacion.Location = new System.Drawing.Point(284, 3);
            this.gAutenticacion.Name = "gAutenticacion";
            this.gAutenticacion.Size = new System.Drawing.Size(275, 233);
            this.gAutenticacion.TabIndex = 1;
            this.gAutenticacion.TabStop = false;
            this.gAutenticacion.Text = "Autenticación";
            // 
            // Recargar
            // 
            this.Recargar.Image = global::Jaeger.UI.Properties.Resources.replay_16px;
            this.Recargar.Location = new System.Drawing.Point(244, 69);
            this.Recargar.Name = "Recargar";
            this.Recargar.Size = new System.Drawing.Size(23, 70);
            this.Recargar.TabIndex = 40;
            this.Recargar.UseVisualStyleBackColor = true;
            // 
            // BoxLog
            // 
            this.BoxLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoxLog.Location = new System.Drawing.Point(6, 174);
            this.BoxLog.Multiline = true;
            this.BoxLog.Name = "BoxLog";
            this.BoxLog.Size = new System.Drawing.Size(258, 50);
            this.BoxLog.TabIndex = 4;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // DescargaControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gAutenticacion);
            this.Controls.Add(this.gConsulta);
            this.MaximumSize = new System.Drawing.Size(565, 240);
            this.MinimumSize = new System.Drawing.Size(565, 240);
            this.Name = "DescargaControl";
            this.Size = new System.Drawing.Size(565, 240);
            this.Load += new System.EventHandler(this.GeneralControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picboxCaptcha)).EndInit();
            this.gConsulta.ResumeLayout(false);
            this.gConsulta.PerformLayout();
            this.gAutenticacion.ResumeLayout(false);
            this.gAutenticacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button Examinar;
        private System.Windows.Forms.Label lblCaptch;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblRFC;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gConsulta;
        private System.Windows.Forms.GroupBox gAutenticacion;
        internal System.Windows.Forms.PictureBox picboxCaptcha;
        internal System.Windows.Forms.TextBox Captcha;
        internal System.Windows.Forms.ComboBox Estado;
        internal System.Windows.Forms.ComboBox TipoComprobante;
        internal System.Windows.Forms.TextBox Password;
        internal System.Windows.Forms.DateTimePicker FechaFinal;
        internal System.Windows.Forms.DateTimePicker FechaInicial;
        internal System.Windows.Forms.TextBox GuardarEn;
        private System.Windows.Forms.Label label7;
        internal System.Windows.Forms.ComboBox TipoComplemento;
        public System.Windows.Forms.TextBox BoxLog;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button Recargar;
        private System.Windows.Forms.Label label8;
        internal System.Windows.Forms.TextBox FiltrarRFC;
        protected internal System.Windows.Forms.Button Descargar;
        public System.Windows.Forms.TextBox cmbAccount;
    }
}
