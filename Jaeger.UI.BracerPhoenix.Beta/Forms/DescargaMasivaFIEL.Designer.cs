﻿namespace Jaeger.UI.Forms {
    partial class DescargaMasivaFIEL {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.FiltrarRFC = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.FechaInicial = new System.Windows.Forms.DateTimePicker();
            this.lblCaptch = new System.Windows.Forms.Label();
            this.FechaFinal = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.TipoComprobante = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TipoSolicitud = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PathPFX = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.ArchivoPXF = new System.Windows.Forms.TextBox();
            this.BoxLog = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SolicitanteRFC = new System.Windows.Forms.TextBox();
            this.Descargar = new System.Windows.Forms.Button();
            this.Password = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Estado = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.Estado.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(599, 30);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.FiltrarRFC);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.FechaInicial);
            this.groupBox1.Controls.Add(this.lblCaptch);
            this.groupBox1.Controls.Add(this.FechaFinal);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TipoComprobante);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.TipoSolicitud);
            this.groupBox1.Location = new System.Drawing.Point(12, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(292, 154);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Consulta";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 125);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 35;
            this.label8.Text = "Filtrar RFC:";
            // 
            // FiltrarRFC
            // 
            this.FiltrarRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.FiltrarRFC.Location = new System.Drawing.Point(125, 122);
            this.FiltrarRFC.MaxLength = 14;
            this.FiltrarRFC.Name = "FiltrarRFC";
            this.FiltrarRFC.Size = new System.Drawing.Size(158, 20);
            this.FiltrarRFC.TabIndex = 34;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "Tipo de Comprobante:";
            // 
            // FechaInicial
            // 
            this.FechaInicial.CustomFormat = "";
            this.FechaInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaInicial.Location = new System.Drawing.Point(125, 43);
            this.FechaInicial.Name = "FechaInicial";
            this.FechaInicial.Size = new System.Drawing.Size(158, 20);
            this.FechaInicial.TabIndex = 1;
            // 
            // lblCaptch
            // 
            this.lblCaptch.AutoSize = true;
            this.lblCaptch.Location = new System.Drawing.Point(69, 73);
            this.lblCaptch.Name = "lblCaptch";
            this.lblCaptch.Size = new System.Drawing.Size(38, 13);
            this.lblCaptch.TabIndex = 35;
            this.lblCaptch.Text = "Hasta:";
            // 
            // FechaFinal
            // 
            this.FechaFinal.CustomFormat = "";
            this.FechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaFinal.Location = new System.Drawing.Point(125, 69);
            this.FechaFinal.Name = "FechaFinal";
            this.FechaFinal.Size = new System.Drawing.Size(158, 20);
            this.FechaFinal.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Periodo de Descarga";
            // 
            // TipoComprobante
            // 
            this.TipoComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TipoComprobante.FormattingEnabled = true;
            this.TipoComprobante.Location = new System.Drawing.Point(125, 16);
            this.TipoComprobante.Name = "TipoComprobante";
            this.TipoComprobante.Size = new System.Drawing.Size(158, 21);
            this.TipoComprobante.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "Tipo de Solicitud:";
            // 
            // TipoSolicitud
            // 
            this.TipoSolicitud.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TipoSolicitud.FormattingEnabled = true;
            this.TipoSolicitud.Location = new System.Drawing.Point(125, 95);
            this.TipoSolicitud.Name = "TipoSolicitud";
            this.TipoSolicitud.Size = new System.Drawing.Size(158, 21);
            this.TipoSolicitud.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.PathPFX);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.ArchivoPXF);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.SolicitanteRFC);
            this.groupBox2.Controls.Add(this.Descargar);
            this.groupBox2.Controls.Add(this.Password);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(310, 36);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(275, 154);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Autenticación";
            // 
            // PathPFX
            // 
            this.PathPFX.Location = new System.Drawing.Point(192, 115);
            this.PathPFX.Name = "PathPFX";
            this.PathPFX.Size = new System.Drawing.Size(75, 23);
            this.PathPFX.TabIndex = 32;
            this.PathPFX.Text = "Examinar";
            this.PathPFX.UseVisualStyleBackColor = true;
            this.PathPFX.Click += new System.EventHandler(this.PathPFX_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "Certificado en formato PFX:";
            // 
            // ArchivoPXF
            // 
            this.ArchivoPXF.Location = new System.Drawing.Point(9, 91);
            this.ArchivoPXF.Name = "ArchivoPXF";
            this.ArchivoPXF.Size = new System.Drawing.Size(258, 20);
            this.ArchivoPXF.TabIndex = 30;
            // 
            // BoxLog
            // 
            this.BoxLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoxLog.Location = new System.Drawing.Point(12, 196);
            this.BoxLog.Multiline = true;
            this.BoxLog.Name = "BoxLog";
            this.BoxLog.Size = new System.Drawing.Size(573, 80);
            this.BoxLog.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "RFC";
            // 
            // SolicitanteRFC
            // 
            this.SolicitanteRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.SolicitanteRFC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SolicitanteRFC.Location = new System.Drawing.Point(129, 16);
            this.SolicitanteRFC.MaxLength = 14;
            this.SolicitanteRFC.Name = "SolicitanteRFC";
            this.SolicitanteRFC.Size = new System.Drawing.Size(138, 22);
            this.SolicitanteRFC.TabIndex = 0;
            // 
            // Descargar
            // 
            this.Descargar.Location = new System.Drawing.Point(111, 115);
            this.Descargar.Name = "Descargar";
            this.Descargar.Size = new System.Drawing.Size(75, 23);
            this.Descargar.TabIndex = 3;
            this.Descargar.Text = "Crear";
            this.Descargar.UseVisualStyleBackColor = true;
            this.Descargar.Click += new System.EventHandler(this.Descargar_Click);
            // 
            // Password
            // 
            this.Password.Location = new System.Drawing.Point(129, 43);
            this.Password.Name = "Password";
            this.Password.PasswordChar = '*';
            this.Password.Size = new System.Drawing.Size(138, 20);
            this.Password.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "Contraseña:";
            // 
            // Estado
            // 
            this.Estado.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.Estado.Location = new System.Drawing.Point(0, 283);
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(599, 22);
            this.Estado.SizingGrip = false;
            this.Estado.TabIndex = 3;
            this.Estado.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(18, 17);
            this.lblStatus.Text = ":D";
            // 
            // DescargaMasivaFIEL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 305);
            this.Controls.Add(this.Estado);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.BoxLog);
            this.Controls.Add(this.pictureBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DescargaMasivaFIEL";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Crear consulta";
            this.Load += new System.EventHandler(this.DescargaMasivaFIEL_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.Estado.ResumeLayout(false);
            this.Estado.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        internal System.Windows.Forms.TextBox FiltrarRFC;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.DateTimePicker FechaInicial;
        internal System.Windows.Forms.DateTimePicker FechaFinal;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.ComboBox TipoComprobante;
        private System.Windows.Forms.Label label6;
        internal System.Windows.Forms.ComboBox TipoSolicitud;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.TextBox BoxLog;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.TextBox SolicitanteRFC;
        private System.Windows.Forms.Button Descargar;
        internal System.Windows.Forms.TextBox Password;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblCaptch;
        private System.Windows.Forms.Button PathPFX;
        private System.Windows.Forms.Label label7;
        internal System.Windows.Forms.TextBox ArchivoPXF;
        private System.Windows.Forms.StatusStrip Estado;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
    }
}