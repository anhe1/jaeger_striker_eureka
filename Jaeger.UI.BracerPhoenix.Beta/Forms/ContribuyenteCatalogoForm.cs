﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Jaeger.Aplication.Repositorio.Contracts;
using Jaeger.Aplication.Repositorio.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Repositorio.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Contribuyentes {
    public partial class ContribuyenteCatalogoForm : Form {
        protected TipoRelacionComericalEnum relacionComericalEnum;
        protected IContribuyentesService Service;
        protected BindingList<ContribuyenteModel> contribuyenteModels;
        protected ToolStripMenuItem Prueba = new ToolStripMenuItem { Text = "Crear" };
        public ContribuyenteCatalogoForm() {
            InitializeComponent();
        }

        /// <summary>
        /// tipo de relacion 1 = Cliente, 2 = Provedor
        /// </summary>
        public ContribuyenteCatalogoForm(int relacionComericalEnum) {
            InitializeComponent();
            this.relacionComericalEnum = (TipoRelacionComericalEnum)relacionComericalEnum;
        }

        private void ContribuyenteCatalogoForm_Load(object sender, EventArgs e) {
            this.GridData.DataGridCommon();
            this.Service = ConfiguracionService.Manager.GetContribuyentes();
            this.TContribuyente.Herramientas.DropDownItems.Add(this.Prueba);
            this.Prueba.Click += Prueba_Click;
        }

        private void Prueba_Click(object sender, EventArgs e) {
            if (this.relacionComericalEnum == TipoRelacionComericalEnum.Cliente) {
                this.Service.Test("Emitidos");
            } else if (this.relacionComericalEnum == TipoRelacionComericalEnum.Proveedor) {
                this.Service.Test("Recibidos");
            }
        }

        public virtual void TContribuyente_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this.contribuyenteModels;
        }

        public virtual void TContribuyente_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void TContribuyente_Nuevo_Click(object sender, EventArgs e) {
            
        }

        public virtual void TContribuyente_Editar_Click(object sender, EventArgs e) {
            //if (this.GridData.CurrentRow != null) {
            //    using (var espera = new WaitingForm(this.d)) {
            //        espera.Text = "Consultando información ...";
            //        espera.ShowDialog(this);
            //    }
            //    if (this.Tag != null) {
            //        var seleccionado = this.Tag as ContribuyenteDetailModel;
            //        if (seleccionado != null) {
            //            using (var editar = new ContribuyenteForm(seleccionado)) {
            //                editar.ShowDialog(this);
            //            }
            //        }
            //    }
            //}
        }

        public virtual void TContribuyente_Remover_Click(object sender, EventArgs e) {
            //if (this.GridData.CurrentRow != null) {
            //    if (RadMessageBox.Show(this, Properties.Resources.Pregunta_RemoveRegistro, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {

            //    }
            //}
        }

        public virtual void TContribuyente_Filtro_Click(object sender, EventArgs e) {
            //this.GridData.ActivateFilterRow(this.ToolBarButtonFiltro.ToggleState);
        }

        public virtual void Consultar() {
            this.contribuyenteModels = new BindingList<ContribuyenteModel>(this.Service.GetList((int)this.relacionComericalEnum).ToList());
        }

        public virtual void d() {
            //if (this.GridData.CurrentRow != null) {
            //    var seleccionado = this.GridData.CurrentRow.DataBoundItem as ContribuyenteDomicilioSingleModel;
            //    this.Tag = this.service.GetById(seleccionado.IdDirectorio);
            //}
        }
    }
}
