﻿namespace Jaeger.UI.Forms.Contribuyentes {
    partial class ContribuyenteCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContribuyenteCatalogoForm));
            this.GridData = new System.Windows.Forms.DataGridView();
            this.RFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RazonSocial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Telefono = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Calle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoExterior = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoInterior = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Colonia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Municipio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ciudad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pais = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Correo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Creo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaNuevo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TContribuyente = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            this.SuspendLayout();
            // 
            // GridData
            // 
            this.GridData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RFC,
            this.RazonSocial,
            this.Telefono,
            this.Calle,
            this.NoExterior,
            this.NoInterior,
            this.Colonia,
            this.Municipio,
            this.Estado,
            this.Ciudad,
            this.Pais,
            this.Correo,
            this.Creo,
            this.FechaNuevo});
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 25);
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1313, 425);
            this.GridData.TabIndex = 2;
            // 
            // RFC
            // 
            this.RFC.DataPropertyName = "RFC";
            this.RFC.HeaderText = "RFC";
            this.RFC.Name = "RFC";
            // 
            // RazonSocial
            // 
            this.RazonSocial.DataPropertyName = "RazonSocial";
            this.RazonSocial.HeaderText = "Nombre ó Razón Social";
            this.RazonSocial.Name = "RazonSocial";
            this.RazonSocial.Width = 250;
            // 
            // Telefono
            // 
            this.Telefono.DataPropertyName = "Telefono";
            this.Telefono.HeaderText = "Telefono";
            this.Telefono.Name = "Telefono";
            // 
            // Calle
            // 
            this.Calle.DataPropertyName = "Calle";
            this.Calle.HeaderText = "Calle";
            this.Calle.Name = "Calle";
            this.Calle.Width = 200;
            // 
            // NoExterior
            // 
            this.NoExterior.DataPropertyName = "NoExterior";
            this.NoExterior.HeaderText = "No. Exterior";
            this.NoExterior.Name = "NoExterior";
            // 
            // NoInterior
            // 
            this.NoInterior.DataPropertyName = "NoInterior";
            this.NoInterior.HeaderText = "No. Interior";
            this.NoInterior.Name = "NoInterior";
            // 
            // Colonia
            // 
            this.Colonia.DataPropertyName = "Colonia";
            this.Colonia.HeaderText = "Colonia";
            this.Colonia.Name = "Colonia";
            // 
            // Municipio
            // 
            this.Municipio.DataPropertyName = "Municipio";
            this.Municipio.HeaderText = "Delegación / Municipio";
            this.Municipio.Name = "Municipio";
            // 
            // Estado
            // 
            this.Estado.DataPropertyName = "Estado";
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            // 
            // Ciudad
            // 
            this.Ciudad.DataPropertyName = "Ciudad";
            this.Ciudad.HeaderText = "Ciudad";
            this.Ciudad.Name = "Ciudad";
            // 
            // Pais
            // 
            this.Pais.DataPropertyName = "Pais";
            this.Pais.HeaderText = "País";
            this.Pais.Name = "Pais";
            // 
            // Correo
            // 
            this.Correo.DataPropertyName = "Correo";
            this.Correo.HeaderText = "Correo";
            this.Correo.Name = "Correo";
            this.Correo.Width = 200;
            // 
            // Creo
            // 
            this.Creo.DataPropertyName = "Creo";
            this.Creo.HeaderText = "Creo";
            this.Creo.Name = "Creo";
            this.Creo.Width = 65;
            // 
            // FechaNuevo
            // 
            this.FechaNuevo.DataPropertyName = "FechaNuevo";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Format = "dd MMM yy";
            this.FechaNuevo.DefaultCellStyle = dataGridViewCellStyle1;
            this.FechaNuevo.HeaderText = "Fec. Sist.";
            this.FechaNuevo.Name = "FechaNuevo";
            this.FechaNuevo.Width = 70;
            // 
            // TContribuyente
            // 
            this.TContribuyente.Dock = System.Windows.Forms.DockStyle.Top;
            this.TContribuyente.Etiqueta = "";
            this.TContribuyente.Location = new System.Drawing.Point(0, 0);
            this.TContribuyente.Name = "TContribuyente";
            this.TContribuyente.ShowActualizar = true;
            this.TContribuyente.ShowCerrar = true;
            this.TContribuyente.ShowEditar = false;
            this.TContribuyente.ShowGuardar = false;
            this.TContribuyente.ShowHerramientas = true;
            this.TContribuyente.ShowImprimir = false;
            this.TContribuyente.ShowNuevo = false;
            this.TContribuyente.ShowRemover = true;
            this.TContribuyente.Size = new System.Drawing.Size(1313, 25);
            this.TContribuyente.TabIndex = 3;
            this.TContribuyente.ButtonNuevo_Click += new System.EventHandler<System.EventArgs>(this.TContribuyente_Nuevo_Click);
            this.TContribuyente.ButtonEditar_Click += new System.EventHandler<System.EventArgs>(this.TContribuyente_Editar_Click);
            this.TContribuyente.ButtonRemover_Click += new System.EventHandler<System.EventArgs>(this.TContribuyente_Remover_Click);
            this.TContribuyente.ButtonActualizar_Click += new System.EventHandler<System.EventArgs>(this.TContribuyente_Actualizar_Click);
            this.TContribuyente.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TContribuyente_Cerrar_Click);
            // 
            // ContribuyenteCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1313, 450);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.TContribuyente);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ContribuyenteCatalogoForm";
            this.Text = "Contribuyentes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ContribuyenteCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView GridData;
        private Common.Forms.ToolBarStandarControl TContribuyente;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn RazonSocial;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telefono;
        private System.Windows.Forms.DataGridViewTextBoxColumn Calle;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoExterior;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoInterior;
        private System.Windows.Forms.DataGridViewTextBoxColumn Colonia;
        private System.Windows.Forms.DataGridViewTextBoxColumn Municipio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ciudad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pais;
        private System.Windows.Forms.DataGridViewTextBoxColumn Correo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Creo;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaNuevo;
    }
}