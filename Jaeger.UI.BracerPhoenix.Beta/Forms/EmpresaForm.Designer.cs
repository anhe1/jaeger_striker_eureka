﻿namespace Jaeger.UI.Forms {
    partial class EmpresaForm {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.domicilioFiscal = new System.Windows.Forms.TextBox();
            this.lblDomicilioFiscal = new System.Windows.Forms.Label();
            this.correo = new System.Windows.Forms.TextBox();
            this.lblCorreo = new System.Windows.Forms.Label();
            this.lblTelefono = new System.Windows.Forms.Label();
            this.regimenFiscal = new System.Windows.Forms.ComboBox();
            this.lblRegimenFiscal = new System.Windows.Forms.Label();
            this.razonSocial = new System.Windows.Forms.TextBox();
            this.lblRazonSocial = new System.Windows.Forms.Label();
            this.RFC = new System.Windows.Forms.TextBox();
            this.lblRFC = new System.Windows.Forms.Label();
            this.telefono = new System.Windows.Forms.TextBox();
            this.nombreComercial = new System.Windows.Forms.TextBox();
            this.lblNombreComercial = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnExaminar = new System.Windows.Forms.Button();
            this.txtRepositorio = new System.Windows.Forms.TextBox();
            this.lblRepositorio = new System.Windows.Forms.Label();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lblInformacion = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // domicilioFiscal
            // 
            this.domicilioFiscal.Location = new System.Drawing.Point(374, 125);
            this.domicilioFiscal.MaxLength = 5;
            this.domicilioFiscal.Name = "domicilioFiscal";
            this.domicilioFiscal.Size = new System.Drawing.Size(79, 20);
            this.domicilioFiscal.TabIndex = 38;
            this.domicilioFiscal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblDomicilioFiscal
            // 
            this.lblDomicilioFiscal.AutoSize = true;
            this.lblDomicilioFiscal.Location = new System.Drawing.Point(372, 109);
            this.lblDomicilioFiscal.Name = "lblDomicilioFiscal";
            this.lblDomicilioFiscal.Size = new System.Drawing.Size(82, 13);
            this.lblDomicilioFiscal.TabIndex = 37;
            this.lblDomicilioFiscal.Text = "Domicilio Fiscal:";
            // 
            // correo
            // 
            this.correo.Location = new System.Drawing.Point(15, 169);
            this.correo.MaxLength = 255;
            this.correo.Name = "correo";
            this.correo.Size = new System.Drawing.Size(274, 20);
            this.correo.TabIndex = 36;
            // 
            // lblCorreo
            // 
            this.lblCorreo.AutoSize = true;
            this.lblCorreo.Location = new System.Drawing.Point(15, 153);
            this.lblCorreo.Name = "lblCorreo";
            this.lblCorreo.Size = new System.Drawing.Size(41, 13);
            this.lblCorreo.TabIndex = 35;
            this.lblCorreo.Text = "Correo:";
            // 
            // lblTelefono
            // 
            this.lblTelefono.AutoSize = true;
            this.lblTelefono.Location = new System.Drawing.Point(290, 153);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(52, 13);
            this.lblTelefono.TabIndex = 33;
            this.lblTelefono.Text = "Telefono:";
            // 
            // regimenFiscal
            // 
            this.regimenFiscal.FormattingEnabled = true;
            this.regimenFiscal.Location = new System.Drawing.Point(15, 125);
            this.regimenFiscal.Name = "regimenFiscal";
            this.regimenFiscal.Size = new System.Drawing.Size(353, 21);
            this.regimenFiscal.TabIndex = 32;
            // 
            // lblRegimenFiscal
            // 
            this.lblRegimenFiscal.AutoSize = true;
            this.lblRegimenFiscal.Location = new System.Drawing.Point(15, 109);
            this.lblRegimenFiscal.Name = "lblRegimenFiscal";
            this.lblRegimenFiscal.Size = new System.Drawing.Size(82, 13);
            this.lblRegimenFiscal.TabIndex = 31;
            this.lblRegimenFiscal.Text = "Régimen Fiscal:";
            // 
            // razonSocial
            // 
            this.razonSocial.Location = new System.Drawing.Point(121, 36);
            this.razonSocial.MaxLength = 255;
            this.razonSocial.Name = "razonSocial";
            this.razonSocial.Size = new System.Drawing.Size(332, 20);
            this.razonSocial.TabIndex = 30;
            // 
            // lblRazonSocial
            // 
            this.lblRazonSocial.AutoSize = true;
            this.lblRazonSocial.Location = new System.Drawing.Point(123, 20);
            this.lblRazonSocial.Name = "lblRazonSocial";
            this.lblRazonSocial.Size = new System.Drawing.Size(126, 13);
            this.lblRazonSocial.TabIndex = 29;
            this.lblRazonSocial.Text = "Nombre ó Razon Social:*";
            // 
            // RFC
            // 
            this.RFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.RFC.Location = new System.Drawing.Point(15, 36);
            this.RFC.MaxLength = 14;
            this.RFC.Name = "RFC";
            this.RFC.Size = new System.Drawing.Size(100, 20);
            this.RFC.TabIndex = 22;
            this.RFC.Validated += new System.EventHandler(this.RFC_Validated);
            // 
            // lblRFC
            // 
            this.lblRFC.AutoSize = true;
            this.lblRFC.Location = new System.Drawing.Point(15, 20);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(38, 13);
            this.lblRFC.TabIndex = 20;
            this.lblRFC.Text = "RFC: *";
            // 
            // telefono
            // 
            this.telefono.Location = new System.Drawing.Point(292, 169);
            this.telefono.MaxLength = 25;
            this.telefono.Name = "telefono";
            this.telefono.Size = new System.Drawing.Size(161, 20);
            this.telefono.TabIndex = 34;
            // 
            // nombreComercial
            // 
            this.nombreComercial.Location = new System.Drawing.Point(15, 86);
            this.nombreComercial.MaxLength = 255;
            this.nombreComercial.Name = "nombreComercial";
            this.nombreComercial.Size = new System.Drawing.Size(438, 20);
            this.nombreComercial.TabIndex = 40;
            // 
            // lblNombreComercial
            // 
            this.lblNombreComercial.AutoSize = true;
            this.lblNombreComercial.Location = new System.Drawing.Point(15, 70);
            this.lblNombreComercial.Name = "lblNombreComercial";
            this.lblNombreComercial.Size = new System.Drawing.Size(96, 13);
            this.lblNombreComercial.TabIndex = 39;
            this.lblNombreComercial.Text = "Nombre Comercial:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnExaminar);
            this.groupBox1.Controls.Add(this.txtRepositorio);
            this.groupBox1.Controls.Add(this.lblRepositorio);
            this.groupBox1.Controls.Add(this.RFC);
            this.groupBox1.Controls.Add(this.lblRFC);
            this.groupBox1.Controls.Add(this.nombreComercial);
            this.groupBox1.Controls.Add(this.lblNombreComercial);
            this.groupBox1.Controls.Add(this.domicilioFiscal);
            this.groupBox1.Controls.Add(this.lblRazonSocial);
            this.groupBox1.Controls.Add(this.lblDomicilioFiscal);
            this.groupBox1.Controls.Add(this.razonSocial);
            this.groupBox1.Controls.Add(this.correo);
            this.groupBox1.Controls.Add(this.lblRegimenFiscal);
            this.groupBox1.Controls.Add(this.lblCorreo);
            this.groupBox1.Controls.Add(this.regimenFiscal);
            this.groupBox1.Controls.Add(this.telefono);
            this.groupBox1.Controls.Add(this.lblTelefono);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(469, 232);
            this.groupBox1.TabIndex = 42;
            this.groupBox1.TabStop = false;
            // 
            // btnExaminar
            // 
            this.btnExaminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExaminar.Location = new System.Drawing.Point(378, 199);
            this.btnExaminar.Name = "btnExaminar";
            this.btnExaminar.Size = new System.Drawing.Size(75, 23);
            this.btnExaminar.TabIndex = 47;
            this.btnExaminar.Text = "Examinar";
            this.btnExaminar.UseVisualStyleBackColor = true;
            this.btnExaminar.Click += new System.EventHandler(this.btnExaminar_Click);
            // 
            // txtRepositorio
            // 
            this.txtRepositorio.Location = new System.Drawing.Point(80, 202);
            this.txtRepositorio.Name = "txtRepositorio";
            this.txtRepositorio.Size = new System.Drawing.Size(292, 20);
            this.txtRepositorio.TabIndex = 42;
            // 
            // lblRepositorio
            // 
            this.lblRepositorio.AutoSize = true;
            this.lblRepositorio.Location = new System.Drawing.Point(15, 206);
            this.lblRepositorio.Name = "lblRepositorio";
            this.lblRepositorio.Size = new System.Drawing.Size(63, 13);
            this.lblRepositorio.TabIndex = 41;
            this.lblRepositorio.Text = "Repositorio:";
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(469, 50);
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // lblInformacion
            // 
            this.lblInformacion.AutoSize = true;
            this.lblInformacion.BackColor = System.Drawing.Color.White;
            this.lblInformacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInformacion.Location = new System.Drawing.Point(12, 17);
            this.lblInformacion.Name = "lblInformacion";
            this.lblInformacion.Size = new System.Drawing.Size(238, 16);
            this.lblInformacion.TabIndex = 44;
            this.lblInformacion.Text = "Administración de Contribuyentes";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.Location = new System.Drawing.Point(303, 288);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 45;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.Guardar_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar.Location = new System.Drawing.Point(384, 288);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(75, 23);
            this.btnCerrar.TabIndex = 46;
            this.btnCerrar.Text = "Cerrar";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.Cerrar_Click);
            // 
            // EmpresaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 321);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.lblInformacion);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmpresaForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Contribuyente";
            this.Load += new System.EventHandler(this.ContribuyenteForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblDomicilioFiscal;
        private System.Windows.Forms.Label lblCorreo;
        private System.Windows.Forms.Label lblTelefono;
        private System.Windows.Forms.Label lblRegimenFiscal;
        private System.Windows.Forms.Label lblRazonSocial;
        private System.Windows.Forms.Label lblRFC;
        internal System.Windows.Forms.TextBox domicilioFiscal;
        internal System.Windows.Forms.TextBox correo;
        internal System.Windows.Forms.ComboBox regimenFiscal;
        internal System.Windows.Forms.TextBox razonSocial;
        internal System.Windows.Forms.TextBox RFC;
        internal System.Windows.Forms.TextBox telefono;
        internal System.Windows.Forms.TextBox nombreComercial;
        private System.Windows.Forms.Label lblNombreComercial;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label lblInformacion;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnExaminar;
        internal System.Windows.Forms.TextBox txtRepositorio;
        private System.Windows.Forms.Label lblRepositorio;
    }
}
