﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Comprobantes {
    public class ComprobantesRecibidosForm : ComprobantesFiscalesForm {
        public ComprobantesRecibidosForm(UIMenuElement menuElement) {
            this.Text = "Emisión: Facturacion";
        }

        public override void Consultar() {
            this._DataSource = this.service.GetList(this.TComprobante.GetMes(), this.TComprobante.GetEjercicio(), "Todos", "Recibidos");
        }
    }
}
