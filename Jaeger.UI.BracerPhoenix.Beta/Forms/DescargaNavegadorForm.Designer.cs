﻿namespace Jaeger.UI.Forms {
    partial class DescargaNavegadorForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DescargaNavegadorForm));
            this.Navegador = new System.Windows.Forms.ToolStrip();
            this.Back = new System.Windows.Forms.ToolStripButton();
            this.Next = new System.Windows.Forms.ToolStripButton();
            this.TRefresh = new System.Windows.Forms.ToolStripButton();
            this.Url = new System.Windows.Forms.ToolStripTextBox();
            this.Go = new System.Windows.Forms.ToolStripButton();
            this.Separador = new System.Windows.Forms.ToolStripSeparator();
            this.TTools = new System.Windows.Forms.ToolStripDropDownButton();
            this.TDownload = new System.Windows.Forms.ToolStripButton();
            this.TClose = new System.Windows.Forms.ToolStripButton();
            this.Status = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.Progress = new System.Windows.Forms.ToolStripProgressBar();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.TOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.Navegador.SuspendLayout();
            this.Status.SuspendLayout();
            this.SuspendLayout();
            // 
            // Navegador
            // 
            this.Navegador.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Back,
            this.Next,
            this.TRefresh,
            this.Url,
            this.Go,
            this.Separador,
            this.TTools,
            this.TDownload,
            this.TClose});
            this.Navegador.Location = new System.Drawing.Point(0, 0);
            this.Navegador.Name = "Navegador";
            this.Navegador.Size = new System.Drawing.Size(800, 25);
            this.Navegador.TabIndex = 0;
            this.Navegador.Text = "Navegador";
            // 
            // Back
            // 
            this.Back.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Back.Image = global::Jaeger.UI.Properties.Resources.back_to_16px;
            this.Back.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(23, 22);
            this.Back.Text = "Back";
            // 
            // Next
            // 
            this.Next.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Next.Image = global::Jaeger.UI.Properties.Resources.next_page_16px;
            this.Next.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Next.Name = "Next";
            this.Next.Size = new System.Drawing.Size(23, 22);
            this.Next.Text = "Next";
            // 
            // TRefresh
            // 
            this.TRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TRefresh.Image = global::Jaeger.UI.Properties.Resources.refresh_16px;
            this.TRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TRefresh.Name = "TRefresh";
            this.TRefresh.Size = new System.Drawing.Size(23, 22);
            this.TRefresh.Text = "Refresh";
            // 
            // Url
            // 
            this.Url.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Url.Name = "Url";
            this.Url.Size = new System.Drawing.Size(400, 25);
            // 
            // Go
            // 
            this.Go.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Go.Image = global::Jaeger.UI.Properties.Resources.play_16;
            this.Go.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Go.Name = "Go";
            this.Go.Size = new System.Drawing.Size(23, 22);
            this.Go.Text = "toolStripButton1";
            // 
            // Separador
            // 
            this.Separador.Name = "Separador";
            this.Separador.Size = new System.Drawing.Size(6, 25);
            // 
            // TTools
            // 
            this.TTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TOptions});
            this.TTools.Image = global::Jaeger.UI.Properties.Resources.toolbox_16px;
            this.TTools.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TTools.Name = "TTools";
            this.TTools.Size = new System.Drawing.Size(107, 22);
            this.TTools.Text = "Herramientas";
            // 
            // TDownload
            // 
            this.TDownload.Image = global::Jaeger.UI.Properties.Resources.download_16px;
            this.TDownload.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TDownload.Name = "TDownload";
            this.TDownload.Size = new System.Drawing.Size(79, 22);
            this.TDownload.Text = "Descargar";
            this.TDownload.Click += new System.EventHandler(this.TDownload_Click);
            // 
            // TClose
            // 
            this.TClose.Image = global::Jaeger.UI.Properties.Resources.close_window_16px;
            this.TClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TClose.Name = "TClose";
            this.TClose.Size = new System.Drawing.Size(59, 22);
            this.TClose.Text = "Cerrar";
            // 
            // Status
            // 
            this.Status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.Progress});
            this.Status.Location = new System.Drawing.Point(0, 428);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(800, 22);
            this.Status.TabIndex = 1;
            this.Status.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(16, 17);
            this.lblStatus.Text = "...";
            // 
            // Progress
            // 
            this.Progress.Name = "Progress";
            this.Progress.Size = new System.Drawing.Size(100, 16);
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(0, 25);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.ScriptErrorsSuppressed = true;
            this.webBrowser.Size = new System.Drawing.Size(800, 403);
            this.webBrowser.TabIndex = 2;
            this.webBrowser.Url = new System.Uri("https://cfdiau.sat.gob.mx/nidp/app/login?id=SATUPCFDiCon&sid=0&option=credential&" +
        "sid=0", System.UriKind.Absolute);
            this.webBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser_DocumentCompleted);
            this.webBrowser.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.webBrowser_Navigated);
            this.webBrowser.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.webBrowser_Navigating);
            // 
            // TOptions
            // 
            this.TOptions.Name = "TOptions";
            this.TOptions.Size = new System.Drawing.Size(180, 22);
            this.TOptions.Text = "Opciones";
            // 
            // DescargaNavegadorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.webBrowser);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.Navegador);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DescargaNavegadorForm";
            this.Text = "Descarga por Navegador";
            this.Load += new System.EventHandler(this.DescargaNavegadorForm_Load);
            this.Navegador.ResumeLayout(false);
            this.Navegador.PerformLayout();
            this.Status.ResumeLayout(false);
            this.Status.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip Navegador;
        private System.Windows.Forms.StatusStrip Status;
        private System.Windows.Forms.ToolStripButton Back;
        private System.Windows.Forms.ToolStripButton Next;
        private System.Windows.Forms.ToolStripButton TRefresh;
        private System.Windows.Forms.ToolStripTextBox Url;
        private System.Windows.Forms.ToolStripButton Go;
        private System.Windows.Forms.ToolStripSeparator Separador;
        private System.Windows.Forms.ToolStripDropDownButton TTools;
        private System.Windows.Forms.ToolStripButton TDownload;
        private System.Windows.Forms.ToolStripButton TClose;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ToolStripProgressBar Progress;
        private System.Windows.Forms.WebBrowser webBrowser;
        private System.Windows.Forms.ToolStripMenuItem TOptions;
    }
}