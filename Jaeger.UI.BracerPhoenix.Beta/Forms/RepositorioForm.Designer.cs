﻿namespace Jaeger.UI.Forms.Repositorio {
    partial class RepositorioForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepositorioForm));
            this.TRepositorio = new System.Windows.Forms.ToolStrip();
            this.LabelComprobante = new System.Windows.Forms.ToolStripLabel();
            this.Comprobante = new System.Windows.Forms.ToolStripComboBox();
            this.LabelTipo = new System.Windows.Forms.ToolStripLabel();
            this.Tipo = new System.Windows.Forms.ToolStripComboBox();
            this.LabelPeriodo = new System.Windows.Forms.ToolStripLabel();
            this.Periodo = new System.Windows.Forms.ToolStripComboBox();
            this.LabelEjercicio = new System.Windows.Forms.ToolStripLabel();
            this.Ejercicio = new System.Windows.Forms.ToolStripTextBox();
            this.Actualizar = new System.Windows.Forms.ToolStripButton();
            this.Imprimir = new System.Windows.Forms.ToolStripButton();
            this.OpcionesSAT = new System.Windows.Forms.ToolStripDropDownButton();
            this.TDescargaSAT = new System.Windows.Forms.ToolStripMenuItem();
            this.TDescargaSATefiel = new System.Windows.Forms.ToolStripMenuItem();
            this.TDescargaNavegado = new System.Windows.Forms.ToolStripMenuItem();
            this.Herramientas = new System.Windows.Forms.ToolStripDropDownButton();
            this.Repositorio = new System.Windows.Forms.ToolStripMenuItem();
            this.Crear = new System.Windows.Forms.ToolStripMenuItem();
            this.Importar = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportarComprobantes = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportarArchivo = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportarCarpeta = new System.Windows.Forms.ToolStripMenuItem();
            this.Certificado = new System.Windows.Forms.ToolStripMenuItem();
            this.Serializar = new System.Windows.Forms.ToolStripMenuItem();
            this.Informacion = new System.Windows.Forms.ToolStripMenuItem();
            this.Cerrar = new System.Windows.Forms.ToolStripButton();
            this.dataGrid = new System.Windows.Forms.DataGridView();
            this.Efecto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmisorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Emisor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Receptor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaEmision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaCerfificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetencionISR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetencionIVA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetencionIEPS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrasladoIVA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrasladoIEPS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descuento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusProcesoCancelar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaStatusCancela = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Result = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PathXML = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PathPDF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.XmlContentB64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PdfAcuseB64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PdfAcuseFinalB64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comprobado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaValidacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuArchivo = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuArchivoXML = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuVer = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuEstadoSAT = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuSerializar = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuImprimir = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.TRepositorio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // TRepositorio
            // 
            this.TRepositorio.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LabelComprobante,
            this.Comprobante,
            this.LabelTipo,
            this.Tipo,
            this.LabelPeriodo,
            this.Periodo,
            this.LabelEjercicio,
            this.Ejercicio,
            this.Actualizar,
            this.Imprimir,
            this.OpcionesSAT,
            this.Herramientas,
            this.Cerrar});
            this.TRepositorio.Location = new System.Drawing.Point(0, 0);
            this.TRepositorio.Name = "TRepositorio";
            this.TRepositorio.Size = new System.Drawing.Size(1145, 25);
            this.TRepositorio.TabIndex = 0;
            this.TRepositorio.Text = "toolStrip1";
            // 
            // LabelComprobante
            // 
            this.LabelComprobante.Name = "LabelComprobante";
            this.LabelComprobante.Size = new System.Drawing.Size(89, 22);
            this.LabelComprobante.Text = "Comprobantes:";
            // 
            // Comprobante
            // 
            this.Comprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Comprobante.Name = "Comprobante";
            this.Comprobante.Size = new System.Drawing.Size(75, 25);
            // 
            // LabelTipo
            // 
            this.LabelTipo.Name = "LabelTipo";
            this.LabelTipo.Size = new System.Drawing.Size(33, 22);
            this.LabelTipo.Text = "Tipo:";
            // 
            // Tipo
            // 
            this.Tipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Tipo.Name = "Tipo";
            this.Tipo.Size = new System.Drawing.Size(75, 25);
            // 
            // LabelPeriodo
            // 
            this.LabelPeriodo.Name = "LabelPeriodo";
            this.LabelPeriodo.Size = new System.Drawing.Size(51, 22);
            this.LabelPeriodo.Text = "Período:";
            // 
            // Periodo
            // 
            this.Periodo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Periodo.Name = "Periodo";
            this.Periodo.Size = new System.Drawing.Size(79, 25);
            // 
            // LabelEjercicio
            // 
            this.LabelEjercicio.Name = "LabelEjercicio";
            this.LabelEjercicio.Size = new System.Drawing.Size(54, 22);
            this.LabelEjercicio.Text = "Ejercicio:";
            // 
            // Ejercicio
            // 
            this.Ejercicio.MaxLength = 4;
            this.Ejercicio.Name = "Ejercicio";
            this.Ejercicio.Size = new System.Drawing.Size(50, 25);
            this.Ejercicio.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Actualizar
            // 
            this.Actualizar.Image = global::Jaeger.UI.Properties.Resources.refresh_16px;
            this.Actualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Size = new System.Drawing.Size(79, 22);
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.Click += new System.EventHandler(this.TActualizar_Click);
            // 
            // Imprimir
            // 
            this.Imprimir.Image = global::Jaeger.UI.Properties.Resources.print_16px;
            this.Imprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Size = new System.Drawing.Size(73, 22);
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.Click += new System.EventHandler(this.TImprimir_Click);
            // 
            // OpcionesSAT
            // 
            this.OpcionesSAT.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OpcionesSAT.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TDescargaSAT,
            this.TDescargaSATefiel,
            this.TDescargaNavegado});
            this.OpcionesSAT.Image = global::Jaeger.UI.Properties.Resources.sat_logo_x16;
            this.OpcionesSAT.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OpcionesSAT.Name = "OpcionesSAT";
            this.OpcionesSAT.Size = new System.Drawing.Size(29, 22);
            this.OpcionesSAT.Text = "SAT";
            // 
            // TDescargaSAT
            // 
            this.TDescargaSAT.Name = "TDescargaSAT";
            this.TDescargaSAT.Size = new System.Drawing.Size(202, 22);
            this.TDescargaSAT.Text = "Descarga Masiva";
            this.TDescargaSAT.Click += new System.EventHandler(this.TDescargaSAT_Click);
            // 
            // TDescargaSATefiel
            // 
            this.TDescargaSATefiel.Name = "TDescargaSATefiel";
            this.TDescargaSATefiel.Size = new System.Drawing.Size(202, 22);
            this.TDescargaSATefiel.Text = "Descarga Asistida";
            this.TDescargaSATefiel.Visible = false;
            this.TDescargaSATefiel.Click += new System.EventHandler(this.TDescargaSATefiel_Click);
            // 
            // TDescargaNavegado
            // 
            this.TDescargaNavegado.Name = "TDescargaNavegado";
            this.TDescargaNavegado.Size = new System.Drawing.Size(202, 22);
            this.TDescargaNavegado.Text = "Descarga por navegador";
            this.TDescargaNavegado.Visible = false;
            this.TDescargaNavegado.Click += new System.EventHandler(this.TDescargaNavegado_Click);
            // 
            // Herramientas
            // 
            this.Herramientas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Repositorio,
            this.Certificado,
            this.Serializar,
            this.Informacion});
            this.Herramientas.Image = global::Jaeger.UI.Properties.Resources.toolbox_16px;
            this.Herramientas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Size = new System.Drawing.Size(107, 22);
            this.Herramientas.Text = "Herramientas";
            // 
            // Repositorio
            // 
            this.Repositorio.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Crear,
            this.Importar,
            this.ImportarComprobantes});
            this.Repositorio.Name = "Repositorio";
            this.Repositorio.Size = new System.Drawing.Size(139, 22);
            this.Repositorio.Text = "Repositorio";
            // 
            // Crear
            // 
            this.Crear.Image = global::Jaeger.UI.Properties.Resources.add_database_16px;
            this.Crear.Name = "Crear";
            this.Crear.Size = new System.Drawing.Size(200, 22);
            this.Crear.Text = "Crear";
            this.Crear.Click += new System.EventHandler(this.TCrearRepositorio_Click);
            // 
            // Importar
            // 
            this.Importar.Name = "Importar";
            this.Importar.Size = new System.Drawing.Size(200, 22);
            this.Importar.Text = "Importar";
            // 
            // ImportarComprobantes
            // 
            this.ImportarComprobantes.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ImportarArchivo,
            this.ImportarCarpeta});
            this.ImportarComprobantes.Image = global::Jaeger.UI.Properties.Resources.xml_16px;
            this.ImportarComprobantes.Name = "ImportarComprobantes";
            this.ImportarComprobantes.Size = new System.Drawing.Size(200, 22);
            this.ImportarComprobantes.Text = "Importar comprobantes";
            // 
            // ImportarArchivo
            // 
            this.ImportarArchivo.Name = "ImportarArchivo";
            this.ImportarArchivo.Size = new System.Drawing.Size(115, 22);
            this.ImportarArchivo.Text = "Archivo";
            this.ImportarArchivo.Click += new System.EventHandler(this.TImportarArchivo_Click);
            // 
            // ImportarCarpeta
            // 
            this.ImportarCarpeta.Name = "ImportarCarpeta";
            this.ImportarCarpeta.Size = new System.Drawing.Size(115, 22);
            this.ImportarCarpeta.Text = "Carpeta";
            this.ImportarCarpeta.Click += new System.EventHandler(this.TImportarCarpeta_Click);
            // 
            // Certificado
            // 
            this.Certificado.Name = "Certificado";
            this.Certificado.Size = new System.Drawing.Size(139, 22);
            this.Certificado.Text = "Certificado";
            this.Certificado.Click += new System.EventHandler(this.TCertificado_Click);
            // 
            // Serializar
            // 
            this.Serializar.Name = "Serializar";
            this.Serializar.Size = new System.Drawing.Size(139, 22);
            this.Serializar.Text = "Serializar";
            this.Serializar.Click += new System.EventHandler(this.TSerializar_Click);
            // 
            // Informacion
            // 
            this.Informacion.Name = "Informacion";
            this.Informacion.Size = new System.Drawing.Size(139, 22);
            this.Informacion.Text = "Información";
            this.Informacion.Click += new System.EventHandler(this.TInformacion_Click);
            // 
            // Cerrar
            // 
            this.Cerrar.Image = global::Jaeger.UI.Properties.Resources.close_window_16px;
            this.Cerrar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(59, 22);
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.Click += new System.EventHandler(this.TCerrar_Click);
            // 
            // dataGrid
            // 
            this.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Efecto,
            this.Estado,
            this.EmisorRFC,
            this.Emisor,
            this.ReceptorRFC,
            this.Receptor,
            this.IdDocumento,
            this.FechaEmision,
            this.FechaCerfificacion,
            this.RetencionISR,
            this.RetencionIVA,
            this.RetencionIEPS,
            this.TrasladoIVA,
            this.TrasladoIEPS,
            this.SubTotal,
            this.Descuento,
            this.Total,
            this.StatusProcesoCancelar,
            this.FechaStatusCancela,
            this.Result,
            this.PathXML,
            this.PathPDF,
            this.XmlContentB64,
            this.PdfAcuseB64,
            this.PdfAcuseFinalB64,
            this.Comprobado,
            this.FechaValidacion});
            this.dataGrid.ContextMenuStrip = this.contextMenu;
            this.dataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGrid.Location = new System.Drawing.Point(0, 25);
            this.dataGrid.Name = "dataGrid";
            this.dataGrid.Size = new System.Drawing.Size(1145, 425);
            this.dataGrid.TabIndex = 1;
            this.dataGrid.RowContextMenuStripNeeded += new System.Windows.Forms.DataGridViewRowContextMenuStripNeededEventHandler(this.DataGrid_RowContextMenuStripNeeded);
            // 
            // Efecto
            // 
            this.Efecto.DataPropertyName = "Efecto";
            this.Efecto.HeaderText = "Tipo";
            this.Efecto.Name = "Efecto";
            this.Efecto.Width = 50;
            // 
            // Estado
            // 
            this.Estado.DataPropertyName = "Estado";
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            this.Estado.Width = 65;
            // 
            // EmisorRFC
            // 
            this.EmisorRFC.DataPropertyName = "EmisorRFC";
            this.EmisorRFC.HeaderText = "Emisor RFC";
            this.EmisorRFC.Name = "EmisorRFC";
            // 
            // Emisor
            // 
            this.Emisor.DataPropertyName = "Emisor";
            this.Emisor.HeaderText = "Emisor";
            this.Emisor.Name = "Emisor";
            this.Emisor.Width = 200;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.DataPropertyName = "ReceptorRFC";
            this.ReceptorRFC.HeaderText = "Receptor RFC";
            this.ReceptorRFC.Name = "ReceptorRFC";
            // 
            // Receptor
            // 
            this.Receptor.DataPropertyName = "Receptor";
            this.Receptor.HeaderText = "Receptor";
            this.Receptor.Name = "Receptor";
            this.Receptor.Width = 200;
            // 
            // IdDocumento
            // 
            this.IdDocumento.DataPropertyName = "IdDocumento";
            this.IdDocumento.HeaderText = "IdDocumento (UUID)";
            this.IdDocumento.MaxInputLength = 36;
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.Width = 215;
            // 
            // FechaEmision
            // 
            this.FechaEmision.DataPropertyName = "FechaEmision";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Format = "dd MMM yy";
            dataGridViewCellStyle1.NullValue = null;
            this.FechaEmision.DefaultCellStyle = dataGridViewCellStyle1;
            this.FechaEmision.HeaderText = "Fecha Emisión";
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.Width = 75;
            // 
            // FechaCerfificacion
            // 
            this.FechaCerfificacion.DataPropertyName = "FechaCerfificacion";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "dd MMM yy";
            this.FechaCerfificacion.DefaultCellStyle = dataGridViewCellStyle2;
            this.FechaCerfificacion.HeaderText = "Fecha Cerfificación";
            this.FechaCerfificacion.Name = "FechaCerfificacion";
            this.FechaCerfificacion.Width = 75;
            // 
            // RetencionISR
            // 
            this.RetencionISR.DataPropertyName = "RetencionISR";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            this.RetencionISR.DefaultCellStyle = dataGridViewCellStyle3;
            this.RetencionISR.HeaderText = "Ret. ISR";
            this.RetencionISR.Name = "RetencionISR";
            this.RetencionISR.Visible = false;
            this.RetencionISR.Width = 75;
            // 
            // RetencionIVA
            // 
            this.RetencionIVA.DataPropertyName = "RetencionIVA";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            this.RetencionIVA.DefaultCellStyle = dataGridViewCellStyle4;
            this.RetencionIVA.HeaderText = "Ret. IVA";
            this.RetencionIVA.Name = "RetencionIVA";
            this.RetencionIVA.ReadOnly = true;
            this.RetencionIVA.Visible = false;
            this.RetencionIVA.Width = 75;
            // 
            // RetencionIEPS
            // 
            this.RetencionIEPS.DataPropertyName = "RetencionIEPS";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            this.RetencionIEPS.DefaultCellStyle = dataGridViewCellStyle5;
            this.RetencionIEPS.HeaderText = "Ret. IEPS";
            this.RetencionIEPS.Name = "RetencionIEPS";
            this.RetencionIEPS.ReadOnly = true;
            this.RetencionIEPS.Visible = false;
            this.RetencionIEPS.Width = 75;
            // 
            // TrasladoIVA
            // 
            this.TrasladoIVA.DataPropertyName = "TrasladoIVA";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            this.TrasladoIVA.DefaultCellStyle = dataGridViewCellStyle6;
            this.TrasladoIVA.HeaderText = "Tras. IVA";
            this.TrasladoIVA.Name = "TrasladoIVA";
            this.TrasladoIVA.ReadOnly = true;
            this.TrasladoIVA.Visible = false;
            this.TrasladoIVA.Width = 75;
            // 
            // TrasladoIEPS
            // 
            this.TrasladoIEPS.DataPropertyName = "TrasladoIEPS";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            this.TrasladoIEPS.DefaultCellStyle = dataGridViewCellStyle7;
            this.TrasladoIEPS.HeaderText = "Tras. IEPS";
            this.TrasladoIEPS.Name = "TrasladoIEPS";
            this.TrasladoIEPS.ReadOnly = true;
            this.TrasladoIEPS.Visible = false;
            this.TrasladoIEPS.Width = 75;
            // 
            // SubTotal
            // 
            this.SubTotal.DataPropertyName = "SubTotal";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            this.SubTotal.DefaultCellStyle = dataGridViewCellStyle8;
            this.SubTotal.HeaderText = "SubTotal";
            this.SubTotal.Name = "SubTotal";
            this.SubTotal.ReadOnly = true;
            this.SubTotal.Visible = false;
            // 
            // Descuento
            // 
            this.Descuento.DataPropertyName = "Descuento";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N2";
            this.Descuento.DefaultCellStyle = dataGridViewCellStyle9;
            this.Descuento.HeaderText = "Descuento";
            this.Descuento.Name = "Descuento";
            this.Descuento.ReadOnly = true;
            this.Descuento.Visible = false;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N2";
            dataGridViewCellStyle10.NullValue = null;
            this.Total.DefaultCellStyle = dataGridViewCellStyle10;
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.Width = 85;
            // 
            // StatusProcesoCancelar
            // 
            this.StatusProcesoCancelar.DataPropertyName = "StatusProcesoCancelar";
            this.StatusProcesoCancelar.HeaderText = "Status Proceso Cancelar";
            this.StatusProcesoCancelar.Name = "StatusProcesoCancelar";
            // 
            // FechaStatusCancela
            // 
            this.FechaStatusCancela.DataPropertyName = "FechaStatusCancela";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.Format = "dd MMM yy";
            this.FechaStatusCancela.DefaultCellStyle = dataGridViewCellStyle11;
            this.FechaStatusCancela.HeaderText = "Fecha Status Cancela";
            this.FechaStatusCancela.Name = "FechaStatusCancela";
            this.FechaStatusCancela.Width = 75;
            // 
            // Result
            // 
            this.Result.DataPropertyName = "Result";
            this.Result.HeaderText = "Result";
            this.Result.Name = "Result";
            // 
            // PathXML
            // 
            this.PathXML.DataPropertyName = "PathXML";
            this.PathXML.HeaderText = "PathXML";
            this.PathXML.Name = "PathXML";
            this.PathXML.Visible = false;
            // 
            // PathPDF
            // 
            this.PathPDF.DataPropertyName = "PathPDF";
            this.PathPDF.HeaderText = "PathPDF";
            this.PathPDF.Name = "PathPDF";
            this.PathPDF.Visible = false;
            // 
            // XmlContentB64
            // 
            this.XmlContentB64.DataPropertyName = "XmlContentB64";
            this.XmlContentB64.HeaderText = "XmlContentB64";
            this.XmlContentB64.Name = "XmlContentB64";
            this.XmlContentB64.Visible = false;
            // 
            // PdfAcuseB64
            // 
            this.PdfAcuseB64.DataPropertyName = "PdfAcuseB64";
            this.PdfAcuseB64.HeaderText = "PdfAcuseB64";
            this.PdfAcuseB64.Name = "PdfAcuseB64";
            this.PdfAcuseB64.Visible = false;
            // 
            // PdfAcuseFinalB64
            // 
            this.PdfAcuseFinalB64.DataPropertyName = "PdfAcuseFinalB64";
            this.PdfAcuseFinalB64.HeaderText = "PdfAcuseFinalB64";
            this.PdfAcuseFinalB64.Name = "PdfAcuseFinalB64";
            this.PdfAcuseFinalB64.Visible = false;
            // 
            // Comprobado
            // 
            this.Comprobado.DataPropertyName = "ComprobadoText";
            this.Comprobado.HeaderText = "Comprobado";
            this.Comprobado.Name = "Comprobado";
            this.Comprobado.Width = 85;
            // 
            // FechaValidacion
            // 
            this.FechaValidacion.DataPropertyName = "FechaValidacion";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.Format = "dd MMM yy";
            this.FechaValidacion.DefaultCellStyle = dataGridViewCellStyle12;
            this.FechaValidacion.HeaderText = "Fecha Validación";
            this.FechaValidacion.Name = "FechaValidacion";
            this.FechaValidacion.Width = 75;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextMenuArchivo,
            this.contextMenuVer,
            this.contextMenuEstadoSAT,
            this.contextMenuSerializar,
            this.contextMenuImprimir});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(132, 114);
            // 
            // contextMenuArchivo
            // 
            this.contextMenuArchivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextMenuArchivoXML});
            this.contextMenuArchivo.Name = "contextMenuArchivo";
            this.contextMenuArchivo.Size = new System.Drawing.Size(131, 22);
            this.contextMenuArchivo.Text = "Archivos";
            // 
            // contextMenuArchivoXML
            // 
            this.contextMenuArchivoXML.Name = "contextMenuArchivoXML";
            this.contextMenuArchivoXML.Size = new System.Drawing.Size(98, 22);
            this.contextMenuArchivoXML.Text = "XML";
            this.contextMenuArchivoXML.Click += new System.EventHandler(this.ContextMenuArchivoXML_Click);
            // 
            // contextMenuVer
            // 
            this.contextMenuVer.Image = global::Jaeger.UI.Properties.Resources.xml_16px;
            this.contextMenuVer.Name = "contextMenuVer";
            this.contextMenuVer.Size = new System.Drawing.Size(131, 22);
            this.contextMenuVer.Text = "Ver XML";
            this.contextMenuVer.Click += new System.EventHandler(this.ContextMenuVer_Click);
            // 
            // contextMenuEstadoSAT
            // 
            this.contextMenuEstadoSAT.Name = "contextMenuEstadoSAT";
            this.contextMenuEstadoSAT.Size = new System.Drawing.Size(131, 22);
            this.contextMenuEstadoSAT.Text = "Estado SAT";
            this.contextMenuEstadoSAT.Click += new System.EventHandler(this.ContextMenuEstadoSAT_Click);
            // 
            // contextMenuSerializar
            // 
            this.contextMenuSerializar.Name = "contextMenuSerializar";
            this.contextMenuSerializar.Size = new System.Drawing.Size(131, 22);
            this.contextMenuSerializar.Text = "Serializar";
            this.contextMenuSerializar.Click += new System.EventHandler(this.ContextMenuSerializar_Click);
            // 
            // contextMenuImprimir
            // 
            this.contextMenuImprimir.Image = global::Jaeger.UI.Properties.Resources.print_16px;
            this.contextMenuImprimir.Name = "contextMenuImprimir";
            this.contextMenuImprimir.Size = new System.Drawing.Size(131, 22);
            this.contextMenuImprimir.Text = "Imprimir";
            this.contextMenuImprimir.Click += new System.EventHandler(this.ContextMenuImprimir_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "*.XML|*.xml";
            this.openFileDialog1.Multiselect = true;
            this.openFileDialog1.ShowReadOnly = true;
            this.openFileDialog1.Title = "Comprobante XML";
            // 
            // RepositorioForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1145, 450);
            this.Controls.Add(this.dataGrid);
            this.Controls.Add(this.TRepositorio);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RepositorioForm";
            this.Text = "Repositorio";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ComprobantesRepositorioForm_Load);
            this.TRepositorio.ResumeLayout(false);
            this.TRepositorio.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip TRepositorio;
        private System.Windows.Forms.DataGridView dataGrid;
        private System.Windows.Forms.ToolStripButton Actualizar;
        private System.Windows.Forms.ToolStripLabel LabelComprobante;
        private System.Windows.Forms.ToolStripComboBox Comprobante;
        private System.Windows.Forms.ToolStripLabel LabelTipo;
        private System.Windows.Forms.ToolStripComboBox Tipo;
        private System.Windows.Forms.ToolStripLabel LabelPeriodo;
        private System.Windows.Forms.ToolStripComboBox Periodo;
        private System.Windows.Forms.ToolStripLabel LabelEjercicio;
        private System.Windows.Forms.ToolStripTextBox Ejercicio;
        private System.Windows.Forms.ToolStripButton Cerrar;
        private System.Windows.Forms.ToolStripDropDownButton Herramientas;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem contextMenuArchivo;
        private System.Windows.Forms.ToolStripMenuItem contextMenuArchivoXML;
        private System.Windows.Forms.ToolStripMenuItem contextMenuVer;
        private System.Windows.Forms.ToolStripMenuItem contextMenuEstadoSAT;
        private System.Windows.Forms.ToolStripMenuItem Serializar;
        private System.Windows.Forms.ToolStripMenuItem contextMenuSerializar;
        private System.Windows.Forms.ToolStripButton Imprimir;
        private System.Windows.Forms.ToolStripDropDownButton OpcionesSAT;
        private System.Windows.Forms.ToolStripMenuItem contextMenuImprimir;
        private System.Windows.Forms.ToolStripMenuItem Informacion;
        private System.Windows.Forms.ToolStripMenuItem Repositorio;
        private System.Windows.Forms.ToolStripMenuItem Crear;
        private System.Windows.Forms.ToolStripMenuItem Importar;
        private System.Windows.Forms.ToolStripMenuItem ImportarComprobantes;
        private System.Windows.Forms.ToolStripMenuItem ImportarArchivo;
        private System.Windows.Forms.ToolStripMenuItem ImportarCarpeta;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ToolStripMenuItem Certificado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Efecto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmisorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Emisor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Receptor;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaEmision;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaCerfificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetencionISR;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetencionIVA;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetencionIEPS;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrasladoIVA;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrasladoIEPS;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descuento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusProcesoCancelar;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaStatusCancela;
        private System.Windows.Forms.DataGridViewTextBoxColumn Result;
        private System.Windows.Forms.DataGridViewTextBoxColumn PathXML;
        private System.Windows.Forms.DataGridViewTextBoxColumn PathPDF;
        private System.Windows.Forms.DataGridViewTextBoxColumn XmlContentB64;
        private System.Windows.Forms.DataGridViewTextBoxColumn PdfAcuseB64;
        private System.Windows.Forms.DataGridViewTextBoxColumn PdfAcuseFinalB64;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comprobado;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaValidacion;
        public System.Windows.Forms.ToolStripMenuItem TDescargaSAT;
        public System.Windows.Forms.ToolStripMenuItem TDescargaSATefiel;
        private System.Windows.Forms.ToolStripMenuItem TDescargaNavegado;
    }
}