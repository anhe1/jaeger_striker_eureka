﻿using System;
using System.IO;
using System.Windows.Forms;
using Jaeger.Aplication.Repositorio.Contracts;
using Jaeger.Aplication.Repositorio.Services;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.UI.Forms {
    public partial class DescargaMasivaFIEL : Form {
        protected IConsultaWebService service;
        private SolicitudModel solicitud;

        public DescargaMasivaFIEL() {
            InitializeComponent();
        }

        private void DescargaMasivaFIEL_Load(object sender, EventArgs e) {
            this.service = new ConsultaWebService();
            this.solicitud = new SolicitudModel();
            this.CreateBinding();
            this.FechaInicial.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 1);
            this.FechaFinal.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            this.TipoSolicitud.DataSource = ComunService.GetTipoDescarga();
            this.TipoSolicitud.DisplayMember = "Descripcion";
            this.TipoSolicitud.ValueMember = "Id";
            this.TipoComprobante.DataSource = ComunService.GetSubTipoComprobante();
            this.TipoComprobante.DisplayMember = "Descripcion";
            this.TipoComprobante.ValueMember = "Id";
            this.Password.Text = "cip1805304s5";
            this.SolicitanteRFC.Text = "CIP1805304S5";
        }

        private void PathPFX_Click(object sender, EventArgs e) {
            var openFileDialog = new OpenFileDialog() {
                Title = "Buscar archivo del certificado *.pfx",
                DefaultExt = ".exe",
                Filter = "Archivo PFX (*.PFX)|*.PFX",
                FilterIndex = 1,
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                this.ArchivoPXF.Text = openFileDialog.FileName;
            }
        }

        private void Descargar_Click(object sender, EventArgs e) {
            //if (File.Exists(this.ArchivoPXF.Text)) {
            //    byte[] pfx = File.ReadAllBytes(this.ArchivoPXF.Text);
            //    var _service = new WebServiceSAT(pfx, this.Password.Text, this.SolicitanteRFC.Text);
            //    var _token = _service.GeneraToken();

            //    if (_token) {
            //        var _solicitud = _service.Consulta((TipoDescargaMasivaTerceros)this.solicitud.IdTipoSolicitud, (TipoConsultaEnum)this.solicitud.IdTipo, this.solicitud.FechaInicio, this.solicitud.FechaFinal);
            //        this.Message(_solicitud.Mensaje);
            //        this.Message(_solicitud.IdSolicitud);
            //        this.Message("Verificando solicitud" + _solicitud.IdSolicitud);
            //        this.solicitud.IdSolicitud = _solicitud.IdSolicitud;
            //        this.solicitud.Estatus = _solicitud.CodEstatus;
            //        this.solicitud.Descripcion = _solicitud.Mensaje;
            //        this.service.Save(this.solicitud);
            //        //var _verificar = _service.VerificaPeticion(_solicitud.IdSolicitud);
            //        //if (_verificar.CodigoEstadoSolicitud.Contains("5000")) {
            //        //    this.Message(_verificar.Mensaje);
            //        //    this.Message("Solicitando descarga ...");

            //        //    if (_verificar.IdsPaquetes != null) {
            //        //        foreach (var item in _verificar.IdsPaquetes) {
            //        //            Stream sPaquete = (Stream)null;
            //        //            var _descargar = _service.DescargaPeticion(item, ref sPaquete);
            //        //            if (_descargar.CodEstatus.Contains("5000") || _descargar.CodEstatus.Contains("5010")) {
            //        //                _service._RutaDescarga = @"C:\Jaeger\Jaeger.Temporal";
            //        //                _service.GuardarPaquete(item, sPaquete);
            //        //            } else {
            //        //                MessageBox.Show(_descargar.Mensaje);
            //        //            }
            //        //        }
            //        //    } else {
            //        //        this.Message(_verificar.Mensaje);
            //        //    }
            //        //} else {
            //        //    this.Message(_verificar.Mensaje);
            //        //}
                    
            //    } else {
            //        MessageBox.Show("No fué posible iniciar sesión");
            //    }
            //}
        }

        private void Message(string message) {
            try {
                this.lblStatus.Text = message;
                this.BoxLog.AppendText(string.Concat(message, "\n"));
            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void CreateBinding() {
            this.SolicitanteRFC.DataBindings.Add("Text", this.solicitud, "RFCSolicitante", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TipoComprobante.DataBindings.Add("SelectedValue", this.solicitud, "IdTipo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TipoSolicitud.DataBindings.Add("SelectedValue", this.solicitud, "IdTipoSolicitud", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaInicial.DataBindings.Add("Value", this.solicitud, "FechaInicio", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaFinal.DataBindings.Add("Value", this.solicitud, "FechaFinal", true, DataSourceUpdateMode.OnPropertyChanged);
        }
    }
}
