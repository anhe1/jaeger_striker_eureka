﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Contribuyentes {
    public class ProveedoresCatalogoForm : ContribuyenteCatalogoForm {
        public ProveedoresCatalogoForm(UIMenuElement uIMenuElement) : base(2) {
        }
    }

    public class EmpleadosCatalogoForm : ContribuyenteCatalogoForm {
        public EmpleadosCatalogoForm(UIMenuElement uIMenuElement) : base(3) {

        }
    }
}
