﻿using System;
using System.Reflection;
using System.Windows.Forms;
using Jaeger.Aplication.Repositorio.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms {
    public partial class MainForm : Form {

        public MainForm() {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e) {
            var MenuPermissions = new UIMenuItemPermission(UIPermissionEnum.Invisible);
            ConfiguracionService.Menus = ConfiguracionService.GetMenus();
            MenuPermissions.Load(ConfiguracionService.Menus);
            ConfiguracionService.Kaiju = new Domain.Base.Profile.KaijuLogger();
            ConfiguracionService.Kaiju.Roles.Add(new Rol { Name = "user" });
            UIMenuUtility.SetPermission(this, ConfiguracionService.Kaiju, MenuPermissions);

            ConfiguracionService.Manager = new ManagerService(ConfiguracionService.Contribuyente);
            this.barchivo_salir.Click += this.Barchivo_salir_Click;
            this.barchivo_abrir.Click += this.Barchivo_abrir_Click;
            this.barchivo_crear.Click += this.Barchivo_crear_Click;
            this.Empresa.Text = ConfiguracionService.Contribuyente.RFC;
            this.Inabilitar();

            if (ConfiguracionService.Manager.OpenSession == false) {
                MessageBox.Show(this, "Archivo no válido", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Version.Text = this.GetVersion();
        }

        private string GetVersion() {
            return string.Format("Versión: {0}", Application.ProductVersion);
        }

        private void Barchivo_abrir_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog() { Filter = "*.sqlite|*.sqlite" };
            if (openFile.ShowDialog(this) == DialogResult.OK) {
                if (ConfiguracionService.Manager.Load(openFile.FileName) == false) {
                    MessageBox.Show(this, "Archivo no válido", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                } 
            }
            this.barchivo_abrir.Text = ConfiguracionService.Manager.OpenSession ? "Cerrar" : "Abrir";
            this.Empresa.Text = ConfiguracionService.Contribuyente.RFC;
            this.Inabilitar();
        }

        private void Barchivo_crear_Click(object sender, EventArgs e) {
            CloseAllForms();
        }

        private void Barchivo_salir_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Inabilitar() {
            //this.barchivo_crear.Enabled = !ConfiguracionService.Manager.OpenSession;
            //this.barchivo_empresa.Enabled = ConfiguracionService.Manager.OpenSession;
            //this.barchivo_configuracion.Enabled = ConfiguracionService.Manager.OpenSession;
            //this.memision.Enabled = ConfiguracionService.Manager.OpenSession;
            //this.mrecepcion.Enabled = ConfiguracionService.Manager.OpenSession;
            //this.bherrami_repositorio.Enabled = ConfiguracionService.Manager.OpenSession;
            //this.bherrami_validador.Enabled = ConfiguracionService.Manager.OpenSession;
        }

        public void CloseAllForms() {
            // close every open form
            for (int x = 0; x < Application.OpenForms.Count; x++) {
                if (Application.OpenForms[x] != this)
                    Application.OpenForms[x].Close();
            }
        }

        #region acciones del menu
        private void Menu_Main_Click(object sender, EventArgs e) {
            var _item = (ToolStripMenuItem)sender;
            var _menuElement = ConfiguracionService.GeMenuElement(_item.Name);

            if (_menuElement != null) {
                var localAssembly = this.GetAssembly(_menuElement.Assembly);
                if (localAssembly != null) {
                    try {
                        if (string.IsNullOrEmpty(_menuElement.Assembly)) {
                            Console.WriteLine(string.Format("Jaeger.UI.{0}.", _menuElement.Form));
                            Type type = localAssembly.GetType(string.Format("Jaeger.UI.{0}", _menuElement.Form), true);
                            this.Activar_Form(type, _menuElement);
                        } else {
                            Type type = localAssembly.GetType(string.Format("{0}.{1}", _menuElement.Assembly, _menuElement.Form), true);
                            this.Activar_Form(type, _menuElement);
                        }
                    } catch (Exception ex) {
                        MessageBox.Show(this, ex.Message, "Jaeger.UI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                } else {
                    MessageBox.Show(this, "No se definio ensamblado.", "Jaeger.UI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private Assembly GetAssembly(string assembly) {
            try {
                if (string.IsNullOrEmpty(assembly)) {
                    return Assembly.Load("Jaeger.UI.BracerPhoenix.Beta");
                }
                return Assembly.Load(assembly);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private void Activar_Form(Type type, object sender = null) {
            this.menuMain.SuspendLayout();
            var _form = (Form)Activator.CreateInstance(type, sender);
            if (_form.WindowState == FormWindowState.Maximized) {
                _form.MdiParent = this;
                _form.Size = this.Size;
                _form.Show();
            } else {
                _form.StartPosition = FormStartPosition.CenterParent;
                _form.ShowDialog(this);
            }

            this.menuMain.Visible = false;
            this.menuMain.Visible = true;
            this.menuMain.ResumeLayout();
        }
        #endregion
    }
}
