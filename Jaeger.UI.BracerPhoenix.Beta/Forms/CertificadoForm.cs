﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.Crypto;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms {
    public partial class CertificadoForm : Form {
        public CertificadoForm(UIMenuElement uIMenuElement) {
            InitializeComponent();
        }

        private void CertificadoForm_Load(object sender, EventArgs e) {
            
        }

        private void btnCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void btnCargar_Click(object sender, EventArgs e) {
            var fileOpen = new OpenFileDialog() { Filter = "*.cer|*.CER" };
            if (fileOpen.ShowDialog(this) == DialogResult.OK) {
                this.Tag = fileOpen.FileName;
                this.CargarCertificado();
            }
        }

        private void CargarCertificado() {
            var ruta = (string)this.Tag;
            if (System.IO.File.Exists(ruta)) {
                var cer = new Crypto.Services.CryptoCertificateInfo(ruta);
                this.RFC.Text = cer.RFC;
                this.txtRazonSocial.Text = cer.UserName;
                this.txtNumeroDeSerie.Text = cer.NoSerie;
                this.txtTipoCertificado.Text = cer.TipoCertificado;
                this.txtValidoDesde.Text = cer.Inicio.Value.ToString("D");
                this.txtValidoHasta.Text = cer.Fin.Value.ToString("D");
                this.txtCertificadoB64.Text = cer.CerB64;
            }
        }
    }
}
