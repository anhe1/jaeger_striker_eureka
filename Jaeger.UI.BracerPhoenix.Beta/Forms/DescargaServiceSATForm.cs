﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Jaeger.Aplication.Repositorio;
using Jaeger.Aplication.Repositorio.Contracts;
using Jaeger.Aplication.Repositorio.Services;
using Jaeger.Domain.Repositorio.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.WSSAT;
using Jaeger.WSSAT.Contracts;

namespace Jaeger.UI.Forms {
    public partial class DescargaServiceSATForm : Form {
        protected IConsultaWebService service;
        protected IWebServiceSAT webService;
        private BindingList<SolicitudModel> solicitudes;

        #region formulario
        public DescargaServiceSATForm() {
            InitializeComponent();
        }

        private void DescargaServiceSATForm_Load(object sender, EventArgs e) {
            this.GridSolicitudes.DataGridCommon();

            this.IdTipo.ValueMember = "Id";
            this.IdTipo.DisplayMember = "Descripcion";
            this.IdTipo.DataSource = ComunService.GetSubTipoComprobante();

            this.IdTipoSolicitud.ValueMember = "Id";
            this.IdTipoSolicitud.DisplayMember = "Descripcion";
            this.IdTipoSolicitud.DataSource = ComunService.GetTipoDescarga();
            
            this.TDescarga.Editar.Image = Properties.Resources.update_16px;
            this.TDescarga.Editar.Text = "Verificar";
            this.TDescarga.Remover.Image = Properties.Resources.download_16px;
            this.TDescarga.Remover.Text = "Descargar";
            this.service = new ConsultaWebService();
            byte[] pfx = System.IO.File.ReadAllBytes(@"D:\bitbucket\jaeger_edita_desktop\documentos\CIP1805304S5.pfx");
            this.webService = new WebServiceSAT(pfx, "cip1805304s5", "CIP1805304S5");
        }
        #endregion

        #region barra de herramientas
        private void TDescarga_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TDescarga_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new DescargaMasivaFIEL();
            _nuevo.ShowDialog(this);
        }

        private void TDescarga_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Common.Forms.WaitingForm(this.Consultar)) {
                espera.Text = "Obteniendo información";
                espera.ShowDialog(this);
            }
            this.GridSolicitudes.DataSource = this.solicitudes;
        }

        private void TDescarga_Verificar_Click(object sender, EventArgs e) {
            if (this.GridSolicitudes.CurrentRow != null) {
                var _seleccionado = this.GridSolicitudes.CurrentRow.DataBoundItem as SolicitudModel;
                if (_seleccionado != null) {
                    if (this.webService.GeneraToken()) {
                        var _verificar = this.webService.Verifica(_seleccionado.IdSolicitud);
                        if (_verificar != null) {
                            _seleccionado.Estatus = _verificar.CodEstatus;
                            _seleccionado.TotalCFDIS = _verificar.NumeroCFDIs;
                            _seleccionado.Estatus = _verificar.CodEstatus;
                            if (_verificar.IdsPaquetes != null) {
                                _seleccionado.Verificacion = new VerificacionMetaModel {
                                    CodEstatus = _verificar.CodEstatus,
                                    CodigoEstadoSolicitud = _verificar.CodigoEstadoSolicitud,
                                    EstadoSolicitud = _verificar.EstadoSolicitud,
                                    IdSolicitud = _seleccionado.IdSolicitud,
                                    IdsPaquetes = _verificar.IdsPaquetes.ToList(),
                                    Mensaje = _verificar.Mensaje,
                                    NumeroCFDIs = _verificar.NumeroCFDIs
                                };
                                _seleccionado = this.service.Save(_seleccionado);
                            }
                        }
                    }
                }
            }
        }

        private void TDescarga_Descargar_Click(object sender, EventArgs e) {
            if (this.GridSolicitudes.CurrentRow != null) {
                var _seleccionado = this.GridSolicitudes.CurrentRow.DataBoundItem as SolicitudModel;
                if (_seleccionado != null) { 
                    _seleccionado.FechaProcesado = DateTime.Now;
                    if (_seleccionado.Verificacion != null) {
                        if (this.webService.GeneraToken()) {
                            this.webService.RutaDescarga = @"C:\Jaeger\Jaeger.Temporal\";
                            for (int i = 0; i < _seleccionado.Verificacion.IdsPaquetes.Count; i++) {
                                System.IO.Stream _stream = null;
                                var _respuesta = this.webService.Descarga(_seleccionado.Verificacion.IdsPaquetes[i], ref _stream);
                                if (_respuesta != null) {
                                    if (_stream != null) {
                                        _seleccionado.IsDescargaZIP = true;
                                        _seleccionado.FechaProcesado = DateTime.Now;
                                        this.webService.GuardarPaquete(_seleccionado.Verificacion.IdsPaquetes[i], _stream);
                                        _seleccionado = this.service.Save(_seleccionado);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region metodos privados
        private void Consultar() {
            this.solicitudes = this.service.GetAll();
        }
        #endregion
    }
}
