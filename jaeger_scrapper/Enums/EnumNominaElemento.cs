﻿//' develop: 071120162357
//' purpose: Nomina tipo de elemento guardado en la tabla de percepciones y deducciones
//' elemento1	                    tipo1	            clave1	            concepto1	        gravado1	        exento1
//' 1=Percepcion	                TipoPercepcion	    Clave	            Concepto	        ImporteGravado	    ImporteExento
//' 2=Deduccion	                TipoDeduccion	    Clave	            Concepto		                        Importe
//' 3=HorasExtra		            Dias	            TipoHoras	        HorasExtra	                            ImportePagado
//' 4=Incapacidades			                                            DiasIncapacidad	    TipoIncapacidad	    ImporteMonetario
//' 5=AccionesOTitulos			                                                            ValorMercado	    PrecioAlOtorgarse
//' 6=JubilacionPensionRetiro	    TotalUnaExhibicion	TotalParcialidad    MontoDiario	        IngresoAcumulable	IngresoNoAcumulable
//' 7=SeparacionIndemnizacion	    TotalPagado	        NumAñosServicio	    UltimoSueldoMensOrd	IngresoAcumulable	IngresoNoAcumulable
//' 8=OtrosPagos	                TipoOtroPago	    Clave	            Concepto		                        Importe
//' 9=CompensacionSaldosAFavor    			                            Año	                SaldoAFavor	        RemanenteSalFav
//' 10=Subsidio al empleo
using System;

namespace Jaeger.Layout.Enums
{
    public enum EnumNominaElemento
    {
        None,
        Percepcion,
        Deduccion,
        HorasExtra,
        Incapacidad,
        AccionesOTitulos,
        JubilacionPensionRetiro,
        SeparacionIndemnizacion,
        OtrosPagos,
        CompensacionSaldosAFavor
    }
}
