﻿using System;
using System.Linq;
using FileHelpers;

namespace Jaeger.Layout.Entities.Banamex
{
    [DelimitedRecord(",")]
    public class Impresores
    {
        /// <summary>
        /// obtener o establecer la fecha del movimiento
        /// </summary>
        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
        public DateTime Fecha;

        /// <summary>
        /// obtener o establecer el tipo de movimiento
        /// </summary>
        public string Tipo;

        public string NoCheque;

        public string NoAutorizacion;

        public string RFC;

        public string Beneficiario;

        [FieldQuoted('"', QuoteMode.OptionalForRead, MultilineMode.AllowForRead)]
        public string Concepto;

        public string NoDocto;

        public decimal Deposito;

        public decimal Retiro;
    }
}
