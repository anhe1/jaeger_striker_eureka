using FileHelpers;
using System;

namespace Jaeger.Layout.Entities.Banamex
{
    [IgnoreFirst(4)]
    [DelimitedRecord("|")]
    public class BanamexEstadoCuenta
    {
        /// <summary>
        /// obtener o establecer la fecha del reporte
        /// </summary>
        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
        public System.DateTime? Fecha;

        /// <summary>
        /// obtener o establecer la hora de emisi�n del reporte
        /// </summary>
        public string Hora;

        /// <summary>
        /// obtener o establecer el numero de cliente
        /// </summary>
        public string NoCliente;

        /// <summary>
        /// obtener o establecer el nombre del beneficiario de la cuenta
        /// </summary>
        public string RazonSocial;

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string Field5;

        /// <summary>
        /// obtener o establecer el nombre del usuario que emite el reporte
        /// </summary>
        public string Usuario;

        /// <summary>
        /// obtener o establecer el tipo de cuenta
        /// </summary>
        public string Tipo;

        /// <summary>
        /// obtener o establecer el numero de la sucursal de la cuenta
        /// </summary>
        public string Sucursal;

        /// <summary>
        /// obtener o establecer el numero de la cuenta
        /// </summary>
        public string NoCuenta;

        /// <summary>
        /// obtener o establecer el nombre
        /// </summary>
        public string Nombre;

        /// <summary>
        /// obtener o establecer otra fecha??
        /// </summary>
        public string Fecha2;

        /// <summary>
        /// obtener o establecer fecha 3
        /// </summary>
        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
        public DateTime FechaPeriodo;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field13;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal SaldoAnterior;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field15;

        /// <summary>
        /// obtener o establecer saldo a la fecha del periodo 
        /// </summary>
        public decimal SaldoAl;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal SaldoPromedioPeriodo;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal SaldoPromedioAnual;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int DiasTrascurridosPeriodo;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal Despositos;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int NumDepositos;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int DiasTrascurridosAnual;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int NumRetiros;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal TotalRetiros;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field25;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field26;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field27;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int ChequesGirados;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field29;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int ChequesExentos;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field31;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field32;

        [DelimitedRecord("|")]
        public partial class BanamexEstadoCuentaMovimiento
        {
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime Fecha;

            [FieldConverter(typeof(ConceptoConverter))]
            public EstadoCuentaConcepto Concepto;

            [FieldNullValue(typeof(decimal), "0")]
            public decimal Retiro;

            [FieldNullValue(typeof(decimal), "0")]
            public decimal Deposito;

            [FixedLengthRecord]
            public partial class EstadoCuentaConcepto
            {
                [FieldFixedLength(6)]
                public string Tipo;

                [FieldFixedLength(11)]
                
                public string Field2;
                [FieldFixedLength(41)]
                
                public string Descripcion;
                
                [FieldFixedLength(12)]
                private string noAutorizacionField;

                /// <summary>
                /// obtener o estalecer el numero de autorizacion del movimiento
                /// </summary>
                public string NoAutorizacion
                {
                    get
                    {
                        return this.noAutorizacionField;
                    }
                    set
                    {
                        this.noAutorizacionField = value.Replace("AUT.", "");
                    }
                }
            }

            public partial class ConceptoConverter : ConverterBase
            {
                public override object StringToField(string from)
                {
                    var engine = new FixedFileEngine(typeof(EstadoCuentaConcepto));
                    var result = engine.ReadString(from);
                    if (result != null)
                    {
                        return result[0];
                    }
                    return new EstadoCuentaConcepto();
                }
            }
        }

    }
}