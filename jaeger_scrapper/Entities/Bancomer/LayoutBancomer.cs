﻿using FileHelpers;
using Jaeger.Layout.Entities.Base;

namespace Jaeger.Layout.Entities.Bancomer
{
    [FixedLengthRecord(FixedMode.ExactLength)]
    public class LayoutBancomer : PropertyChangeImplementation
    {
        [FieldFixedLength(9)]
        private string secuenciaField;
        [FieldFixedLength(16)]
        private string rfcBeneficiarioField;
        [FieldFixedLength(2)]
        private string tipoCuentaField;
        [FieldFixedLength(20)]
        private string numeroCuentaField;
        [FieldFixedLength(15)]
        private string importePagarField;
        [FieldFixedLength(40)]
        private string nombreTrabajador;
        [FieldFixedLength(3)]
        private string bancoDestinoField;
        [FieldFixedLength(3)]
        private string plazaDestinoField;

        public LayoutBancomer()
        {
            this.tipoCuentaField = "99";
            this.bancoDestinoField = "001";
            this.plazaDestinoField = "001";
        }

        public string Secuencia
        {
            get { return secuenciaField; }
            set
            {
                secuenciaField = value;
                this.OnPropertyChanged();
            }
        }

        public string RFCBeneficiario
        {
            get { return rfcBeneficiarioField; }
            set
            {
                rfcBeneficiarioField = value;
                this.OnPropertyChanged();
            }
        }

        public string TipoCuenta
        {
            get { return tipoCuentaField; }
            set
            {
                tipoCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        public string NumeroCuenta
        {
            get { return numeroCuentaField; }
            set
            {
                numeroCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        public string ImportePagar
        {
            get { return importePagarField; }
            set
            {
                importePagarField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del trabajador
        /// </summary>
        public string Nombre
        {
            get { return nombreTrabajador; }
            set
            {
                nombreTrabajador = value;
                this.OnPropertyChanged();
            }
        }

        public string BancoDestino
        {
            get { return bancoDestinoField; }
            set
            {
                bancoDestinoField = value;
                this.OnPropertyChanged();
            }
        }

        public string PlazaDestino
        {
            get { return plazaDestinoField; }
            set
            {
                plazaDestinoField = value;
                this.OnPropertyChanged();
            }
        }

        public LayoutBancomer[] Importar(string archivo)
        {
            var engine = new FileHelperEngine<LayoutBancomer>();

            engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;

            LayoutBancomer[] res = engine.ReadFile(archivo);

            if (engine.ErrorManager.ErrorCount > 0)
                engine.ErrorManager.SaveErrors("Errors.txt");

            return res;
        }

        public bool Exportar(LayoutBancomer[] data, string outfile)
        {
            var engine = new FileHelperEngine<LayoutBancomer>();
            engine.WriteFile(outfile, data);
            return true;
        }

    }
}
