﻿using System;
using System.Collections.Generic;
using FileHelpers;

namespace Jaeger.Layout.Entities {

    [DelimitedRecord(",")]
    public class PolizaCOi {
        public PolizaCOi() {
            this.TipoPoliza = "Dr";
            this.Cuentas = new List<PolizaCuentaCOi>();
        }

        /// <summary>
        /// obtener o establecer el tipo de poliza
        /// </summary>
        public string TipoPoliza { get; set; }

        /// <summary>
        /// obtener o establecer el numero de poliza
        /// </summary>
        public int NumPoliza { get; set; }

        /// <summary>
        /// obtener o establecer el concepto
        /// </summary>
        public string Concepto { get; set; }

        /// <summary>
        /// obtener o establecer numero de dia
        /// </summary>
        public string Dia { get; set; }

        /// <summary>
        /// obtener o establecer lista de cuentas contables
        /// </summary>
        [FieldHidden]
        public List<PolizaCuentaCOi> Cuentas { get; set; }
    }

    [DelimitedRecord(",")]
    public partial class PolizaCuentaCOi : ICloneable {
        public PolizaCuentaCOi() {
            this.Comprobantes = new List<PolizaComprobanteCOi>();
            this.InfoPago = new PolizaInfoPago();
        }
        /// <summary>
        /// obtener o establecer numero de poliza (1)
        /// </summary>
        public string Campo1 { get; set; }

        /// <summary>
        /// obtener o establecer la cuenta contable (2)
        /// </summary>
        public string Cuenta { get; set; }

        /// <summary>
        /// obtener o establecer el departamento (3)
        /// </summary>
        public string Departamento { get; set; }

        /// <summary>
        /// obtener o establecer el concepto (4)
        /// </summary>
        public string Concepto { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de cambio (5)
        /// </summary>
        public decimal TipoCambio { get; set; }

        /// <summary>
        /// obtener o establecer el cargo (6)
        /// </summary>
        [FieldConverter(ConverterKind.Decimal, ".")] // The decimal separator is 
        public decimal Cargo { get; set; }

        /// <summary>
        /// obtener o establcer el aboano (7)
        /// </summary>
        [FieldConverter(ConverterKind.Decimal, ".")] // The decimal separator is 
        public decimal Abono { get; set; }

        /// <summary>
        /// obtener o establecer el centro de costos (8)
        /// </summary>
        public string CentroCosto { get; set; }

        /// <summary>
        /// obtener o establecer el protecto (9)
        /// </summary>
        public string Proyecto { get; set; }

        [FieldHidden]
        public List<PolizaComprobanteCOi> Comprobantes { get; set; }

        [FieldHidden]
        public PolizaInfoPago InfoPago { get; set; }

        public object Clone() {
            return this.MemberwiseClone();
        }
    }

    public class PolizaComprobanteCOi {

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        public string Fecha { get; set; }

        /// <summary>
        /// obtener o establecer el registro federal de contribuyentes del emisor 
        /// </summary>
        public string EmisorRFC { get; set; }

        /// <summary>
        /// obtener o establecer el registro federal de contribuyentes del receptor
        /// </summary>
        public string ReceptorRFC { get; set; }

        public string Receptor { get; set; }

        public decimal ISR { get; set; }

        /// <summary>
        /// obtener o establecer el subtotal de comprobante
        /// </summary>
        public decimal SubTotal { get; set; }

        /// <summary>
        /// obtener o establecer el descuento aplicable al comprobante
        /// </summary>
        public decimal Descuento { get; set; }

        public decimal Total { get; set; }
        private string idDocumento;
        /// <summary>
        /// obtener o establecer el folio fiscal del comprobante
        /// </summary>
        public string IdDocumento {
            get {
                if (this.idDocumento != null)
                    return this.idDocumento;
                return string.Empty;
            }
            set { this.idDocumento = value; }
        }
    }

    public class PolizaInfoPago {
        public PolizaInfoPago() {
            this.Beneficiario = null;
        }

        public string FormaPago { get; set; }
        public string NumCheque { get; set; }
        public string Banco { get; set; }
        public string CuentaOrigen { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Monto { get; set; }
        public string Beneficiario { get; set; }
        public string RFC { get; set; }
        public string BancoDestino { get; set; }
        public string CuentaDestino { get; set; }
        public string BancoOrigen { get; set; }
        public string BancoDestinoExtranjero { get; set; }
        public string IdFiscal { get; set; }
    }
}
