﻿using FileHelpers;
using FileHelpers.MasterDetail;
using Jaeger.Nomina.Entities;
using System.ComponentModel;
using System.IO;

namespace Jaeger.Layout.Entities.Nomina
{
    [DelimitedRecord("|")]
    public class LayoutTablaISR
    {
        public string Etiqueta { get; set; }

        public int Periodo { get; set; }

        [DelimitedRecord("|")]
        public partial class ISR
        {
            public ISR()
            {

            }

            public decimal LimiteInferior { get; set; }
            public decimal LimiteSuperior { get; set; }
            public decimal CuotaFija { get; set; }
            public decimal PorcentajeExcedente { get; set; }
        }

        public BindingList<ViewModelTablaImpuestoSobreRenta> Importar(string archivo)
        {
            if (new FileInfo(archivo).Exists)
            {
                BindingList<ViewModelTablaImpuestoSobreRenta> tablas = new BindingList<ViewModelTablaImpuestoSobreRenta>();
                var engine = new MasterDetailEngine<LayoutTablaISR, ISR>(new MasterDetailSelector(this.ExampleSelector));
                MasterDetails<LayoutTablaISR, ISR>[] ressult = engine.ReadFile(archivo);
                for (int i = 0; i < ressult.Length; i++)
                {
                    ViewModelTablaImpuestoSobreRenta tabla = new ViewModelTablaImpuestoSobreRenta();
                    tabla.Etiqueta = ressult[i].Master.Etiqueta.Replace("@", "");
                    tabla.Periodo = ressult[i].Master.Periodo;
                    foreach (ISR item in ressult[i].Details)
                    {
                        tabla.Rangos.Add(new ImpuestoSobreRenta(item.LimiteInferior, item.LimiteSuperior, item.CuotaFija, item.PorcentajeExcedente));
                    }
                    tablas.Add(tabla);
                }
                return tablas;
            }
            return null;
        }

        private RecordAction ExampleSelector(string record)
        {
            if (record[0].ToString().Contains("@"))
                return RecordAction.Master;
            else
                return RecordAction.Detail;
        }
    }
}
