﻿using FileHelpers;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Layout.Entities.Nomina
{
    [IgnoreFirst(1)]
    [DelimitedRecord("|")]
    public sealed class LayOutEmpleado
    {

        private string numeroField;
        private string claveField;
        private string primerApellidoField;
        private string segundoApellidoField;
        private string nombreField;
        private string mAREA;
        private string departamentoField;
        private string puestoField;
        private string salarioBaseField;
        private string salarioDiarioField;
        private string salarioDiarioIntegreadoField;
        private string jornadasTrabajoField;
        private string horasJornadaTrabajoField;
        private string fechaInicioRelacionLaboralField;
        private string fechaTerminoRelacionLaboralField;
        private string statusField;
        private string cuentaBancariaField;
        private string numeroSeguridadSocialField;
        private string curpField;
        private string rfcField;
        private string correoField;

        [DataNames("NUM")]
        public string Num
        {
            get { return numeroField; }
            set { numeroField = value; }
        }

        [DataNames("CLAVE")]
        public string Clave
        {
            get { return claveField; }
            set { claveField = value; }
        }

        [DataNames("APELLIDOPATERNO")]
        public string PrimerApellido
        {
            get { return primerApellidoField; }
            set { primerApellidoField = value; }
        }

        [DataNames("APELLIDOMATERNO")]
        public string SegundoApellido
        {
            get { return segundoApellidoField; }
            set { segundoApellidoField = value; }
        }

        public string Nombres
        {
            get { return nombreField; }
            set { nombreField = value; }
        }

        public string Area
        {
            get { return mAREA; }
            set { mAREA = value; }
        }

        public string Departamento
        {
            get { return departamentoField; }
            set { departamentoField = value; }
        }

        public string Puesto
        {
            get { return puestoField; }
            set { puestoField = value; }
        }

        public decimal SalarioBase
        {
            get { return System.Convert.ToDecimal(this.salarioBaseField); }
            set { this.salarioBaseField = value.ToString(); }
        }

        public decimal SalarioDiario
        {
            get { return System.Convert.ToDecimal(salarioDiarioField); }
            set { salarioDiarioField = value.ToString(); }
        }

        public string SalarioDiarioIntegreado
        {
            get { return this.salarioDiarioIntegreadoField; }
            set { this.salarioDiarioIntegreadoField = value; }
        }

        public string JornadasTrabajo
        {
            get { return this.jornadasTrabajoField; }
            set { this.jornadasTrabajoField = value; }
        }

        public string HorasJornadaTrabajo
        {
            get { return this.horasJornadaTrabajoField; }
            set { this.horasJornadaTrabajoField = value; }
        }

        public string FechaInicioRelLaboral
        {
            get { return fechaInicioRelacionLaboralField; }
            set { fechaInicioRelacionLaboralField = value; }
        }

        public string Fecha_Baja
        {
            get { return fechaTerminoRelacionLaboralField; }
            set { fechaTerminoRelacionLaboralField = value; }
        }

        public string Activo
        {
            get { return statusField; }
            set { statusField = value; }
        }

        public string CuentaBancaria
        {
            get { return cuentaBancariaField; }
            set { cuentaBancariaField = value; }
        }

        public string NumSeguridadSocial
        {
            get { return numeroSeguridadSocialField; }
            set { numeroSeguridadSocialField = value; }
        }

        public string CURP
        {
            get { return curpField; }
            set { curpField = value; }
        }

        public string RFC
        {
            get { return rfcField; }
            set { rfcField = value; }
        }

        public string Correo
        {
            get { return correoField; }
            set { correoField = value; }
        }
    }
}