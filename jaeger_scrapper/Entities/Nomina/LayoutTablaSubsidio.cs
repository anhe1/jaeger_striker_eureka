﻿using FileHelpers.MasterDetail;
using FileHelpers;
using System.ComponentModel;
using Jaeger.Nomina.Entities;
using System.IO;

namespace Jaeger.Layout.Entities.Nomina
{
    [DelimitedRecord("|")]
    public class LayoutTablaSubsidio
    {
        public string Etiqueta { get; set; }

        public int Periodo { get; set; }

        public LayoutTablaSubsidio()
        {

        }

        [DelimitedRecord("|")]
        private partial class LayoutSubsidio
        {
            public LayoutSubsidio()
            {

            }

            public decimal IngresosDesde { get; set; }

            public decimal IngresosHasta { get; set; }

            public decimal Cantidad { get; set; }
        }

        public BindingList<ViewModelTablaSubsidioAlEmpleo> Import(string filename)
        {
            if (new FileInfo(filename).Exists)
            {
                BindingList<ViewModelTablaSubsidioAlEmpleo> tablas = new BindingList<ViewModelTablaSubsidioAlEmpleo>();
                var engine = new MasterDetailEngine<LayoutTablaSubsidio, LayoutSubsidio>(new MasterDetailSelector(ExampleSelector));
                MasterDetails<LayoutTablaSubsidio, LayoutSubsidio>[] ressult = engine.ReadFile(filename);
                for (int i = 0; i < ressult.Length; i++)
                {
                    ViewModelTablaSubsidioAlEmpleo tabla = new ViewModelTablaSubsidioAlEmpleo();
                    tabla.Descripcion = ressult[i].Master.Etiqueta.Replace("@", "");
                    tabla.Periodo = ressult[i].Master.Periodo;
                    foreach (LayoutSubsidio item in ressult[i].Details)
                    {
                        tabla.Rangos.Add(new Subsidio(item.IngresosDesde, item.IngresosHasta, item.Cantidad));
                    }
                    tablas.Add(tabla);
                }
                return tablas;
            }
            return null;
        }

        public static RecordAction ExampleSelector(string record)
        {
            if (record[0].ToString().Contains("@"))
                return RecordAction.Master;
            else
                return RecordAction.Detail;
        }
    }
}
