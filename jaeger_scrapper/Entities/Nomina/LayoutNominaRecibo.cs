﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Jaeger.Layout.Enums;
using Jaeger.Layout.Helpers;

namespace Jaeger.Layout.Entities.Nomina
{
    public class LayoutNominaRecibo : BasePropertyChangeImplementation, IDataErrorInfo
    {
        #region declaraciones

        private string tipoNomina;
        private DateTime? fechaPago;
        private DateTime? fechaInicialPago;
        private DateTime? fechaFinalPago;
        private DateTime? fechaInicioRelLaboral;
        private int numDiasPagado;
        private string numEmpleado;
        private string rfcReceptor;
        private string curpReceptor;
        private string receptor;
        private string correo;
        private string numSeguridadSocial;
        private string antiguedad;
        private string departamento;
        private string puesto;
        private string claveTipoContrato; 
        private string claveTipoJornada;
        private string claveTipoRegimen;
        private string claveRiesgoPuesto;
        private string clavePeriodicidadPago;
        private string sindicalizado;
        private string claveBanco;
        private string cuentaBancaria;
        private string claveEntidadFederativa;
        private decimal salarioBaseCotApor;
        private decimal salarioDiarioIntegrado;
        private decimal subsidioCausado;
        private int diasIncapacidad;
        private string claveTipoIncapacidad;
        private decimal importeMonetario;
        // relacion con CFDI
        private string claveTipoRelacion;
        private string uuidRelacionado;
        // elementos del layout
        private BindingList<LayoutNominaReciboElemento> elementos;

        #endregion

        #region comprobante fiscal

        private string lugarExpedicion;

        public LayoutNominaRecibo()
        {
            this.Elementos = new BindingList<LayoutNominaReciboElemento>();
        }

        /// <summary>
        /// obtener o establecer el código postal del lugar de expedición del comprobante (domicilio de la matriz o de la sucursal).
        /// </summary>
        public string LugarExpedicion
        {
            get
            {
                return this.lugarExpedicion;
            }
            set
            {
                this.lugarExpedicion = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region complemento de nomina

        /// <summary>
        /// obtener o establecer el tipo de nómina, puede ser O = Nómina ordinaria o E = Nómina extraordinaria.
        /// </summary>
        public string TipoNomina
        {
            get
            {
                return this.tipoNomina;
            }
            set
            {
                this.tipoNomina = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la expresión de la fecha efectiva de erogación del gasto. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601
        /// </summary>
        public DateTime? FechaPago
        {
            get
            {
                return this.fechaPago;
            }
            set
            {
                this.fechaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la expresión de la fecha efectiva de erogación del gasto. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601
        /// </summary>
        public DateTime? FechaInicialPago
        {
            get
            {
                return this.fechaInicialPago;
            }
            set
            {
                this.fechaInicialPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la expresión de la fecha efectiva de erogación del gasto. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601
        /// </summary>
        public DateTime? FechaFinalPago
        {
            get
            {
                return this.fechaFinalPago;
            }
            set
            {
                this.fechaFinalPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la expresión del número o la fracción de días pagados.
        /// pattern value="(([1-9][0-9]{0,4})|[0])(.[0-9]{3})?"
        /// </summary>
        public int NumDiasPagados
        {
            get
            {
                return this.numDiasPagado;
            }
            set
            {
                this.numDiasPagado = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region informacion del emisor

        private string emisor;
        private string emisorCURP;
        private string registroPatronal;
        private string rfcPatronOrigen;
        private string orgienRecurso;
        private decimal montoRecursoPropio;

        public string Emisor
        {
            get
            {
                return this.emisor;
            }
            set
            {
                this.emisor = value;
                this.OnPropertyChanged();
            }
        }

        #region EntidadSNCF

        public string OrigenRecurso
        {
            get
            {
                return this.orgienRecurso;
            }
            set
            {
                this.orgienRecurso = value;
                this.OnPropertyChanged();
            }
        }

        public decimal MontoRecursoPropio
        {
            get
            {
                return this.montoRecursoPropio;
            }
            set
            {
                this.montoRecursoPropio = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        /// <summary>
        /// obtener o establecer la CURP del emisor del comprobante de nómina cuando es una persona física.
        /// </summary>
        public string EmisorCURP
        {
            get
            {
                return this.emisorCURP;
            }
            set
            {
                this.emisorCURP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el registro patronal, clave de ramo - pagaduría o la que le asigne la institución de seguridad social al patrón, a 20 posiciones máximo. 
        /// Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// pattern value="[^|]{1,20}
        /// </summary>
        public string RegistroPatronal
        {
            get
            {
                return this.registroPatronal;
            }
            set
            {
                this.registroPatronal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC de la persona que fungió como patrón cuando el pago al trabajador se realice a través de un tercero como vehículo o herramienta de pago
        /// </summary>
        public string RfcPatronOrigen
        {
            get
            {
                return this.rfcPatronOrigen;
            }
            set
            {
                this.rfcPatronOrigen = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region informacion del receptor

        public string Receptor
        {
            get
            {
                return this.receptor;
            }
            set
            {
                this.receptor = value;
                this.OnPropertyChanged();
            }
        }

        public string ReceptorRFC
        {
            get
            {
                return this.rfcReceptor;
            }
            set
            {
                this.rfcReceptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el CURP del receptor del comprobante de nómina
        /// </summary>
        public string ReceptorCURP
        {
            get
            {
                return this.curpReceptor;
            }
            set
            {
                this.curpReceptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número de seguridad social del trabajador. Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// pattern value="[0-9]{1,15}"
        /// </summary>
        public string NumSeguridadSocial
        {
            get
            {
                return this.numSeguridadSocial;
            }
            set
            {
                this.numSeguridadSocial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de inicio de la relación laboral entre el empleador y el empleado. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601. 
        /// Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        public DateTime? FechaInicioRelLaboral
        {
            get
            {
                return this.fechaInicioRelLaboral;
            }
            set
            {
                this.fechaInicioRelLaboral = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número de semanas o el periodo de años, meses y días que el empleado ha mantenido relación laboral con el empleador. 
        /// Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// pattern value="P(([1-9][0-9]{0,3})|0)W|P([1-9][0-9]?Y)?(([1-9]|1[012])M)?(0|[1-9]|[12][0-9]|3[01])D"
        /// </summary>
        public string Antiguedad
        {
            get
            {
                return this.antiguedad;
            }
            set
            {
                this.antiguedad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de contrato que tiene el trabajador
        /// </summary>
        public string TipoContrato
        {
            get
            {
                return this.claveTipoContrato;
            }
            set
            {
                this.claveTipoContrato = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el trabajador está asociado a un sindicato. Si se omite se asume que no está asociado a algún sindicato.
        /// </summary>
        public string Sindicalizado
        {
            get
            {
                return this.sindicalizado;
            }
            set
            {
                this.sindicalizado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de jornada que cubre el trabajador. Se debe ingresar cuando se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        public string TipoJornada
        {
            get
            {
                return this.claveTipoJornada;
            }
            set
            {
                this.claveTipoJornada = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del régimen por el cual se tiene contratado al trabajador.
        /// </summary>
        public string TipoRegimen
        {
            get
            {
                return this.claveTipoRegimen;
            }
            set
            {
                this.claveTipoRegimen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número de empleado de 1 a 15 posiciones.
        /// pattern value="[^|]{1,15}"
        /// </summary>
        public string NumEmpleado
        {
            get
            {
                return this.numEmpleado;
            }
            set
            {
                this.numEmpleado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la expresión del departamento o área a la que pertenece el trabajador.
        /// pattern value="[^|]{1,100}"
        /// </summary>
        public string Departamento
        {
            get
            {
                return this.departamento;
            }
            set
            {
                this.departamento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la expresión del puesto asignado al empleado o actividad que realiza. (opcional)
        /// pattern value="[^|]{1,100}"
        /// </summary>
        public string Puesto
        {
            get
            {
                return this.puesto;
            }
            set
            {
                this.puesto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave conforme a la Clase en que deben inscribirse los patrones, de acuerdo con las actividades que desempeñan sus trabajadores, según lo previsto en el 
        /// artículo 196 del Reglamento en Materia de Afiliación Clasificación de Empresas, Recaudación y Fiscalización, o conforme con la normatividad del Instituto de Seguridad Social 
        /// del trabajador.  Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        public string RiesgoPuesto
        {
            get
            {
                return this.claveRiesgoPuesto;
            }
            set
            {
                this.claveRiesgoPuesto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la forma en que se establece el pago del salario.
        /// </summary>
        public string PeriodicidadPago
        {
            get
            {
                return this.clavePeriodicidadPago;
            }
            set
            {
                this.clavePeriodicidadPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del Banco conforme al catalogo, donde se realiza el deposito de nomina
        /// </summary>
        public string Banco
        {
            get
            {
                return this.claveBanco;
            }
            set
            {
                this.claveBanco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cuenta bancaria a 11 posiciones o número de teléfono celular a 10 posiciones o número de tarjeta de crédito, débito o servicios a 15 ó 16 posiciones 
        /// o la CLABE a 18 posiciones o número de monedero electrónico, donde se realiza el depósito de nómina.
        /// </summary>
        public string CuentaBancaria
        {
            get
            {
                return this.cuentaBancaria;
            }
            set
            {
                this.cuentaBancaria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar la retribución otorgada al trabajador, que se integra por los pagos hechos en efectivo por cuota diaria, gratificaciones, percepciones, alimentación, 
        /// habitación, primas, comisiones, prestaciones en especie y cualquiera otra cantidad o prestación que se entregue al trabajador por su trabajo, sin considerar los conceptos que se 
        /// excluyen de conformidad con el Artículo 27 de la Ley del Seguro Social, o la integración de los pagos conforme la normatividad del Instituto de Seguridad Social del trabajador. 
        /// (Se emplea para pagar las cuotas y aportaciones de Seguridad Social). Se debe ingresar cuando se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        public decimal SalarioBaseCotApor
        {
            get
            {
                return this.salarioBaseCotApor;
            }
            set
            {
                this.salarioBaseCotApor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// el salario que se integra con los pagos hechos en efectivo por cuota diaria, gratificaciones, percepciones, habitación, primas, comisiones, prestaciones en especie y cualquier otra 
        /// cantidad o prestación que se entregue al trabajador por su trabajo, de conformidad con el Art. 84 de la Ley Federal del Trabajo. (Se utiliza para el cálculo de las indemnizaciones). 
        /// Se debe ingresar cuando se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        public decimal SalarioDiarioIntegrado
        {
            get
            {
                return this.salarioDiarioIntegrado;
            }
            set
            {
                this.salarioDiarioIntegrado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la entidad federativa en donde el receptor del recibo prestó el servicio.
        /// </summary>
        public string EntidadFederativa
        {
            get
            {
                return this.claveEntidadFederativa;
            }
            set
            {
                this.claveEntidadFederativa = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region otros pagos

        /// <summary>
        /// obtener o establecer el subsidio causado conforme a la tabla del subsidio para el empleo publicada en el Anexo 8 de la RMF vigente
        /// </summary>
        public decimal SubsidioCausado
        {
            get
            {
                return this.subsidioCausado;
            }
            set
            {
                this.subsidioCausado = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region Nodo condicional para expresar información de las incapacidades

        /// <summary>
        /// obtener o establecer el número de días enteros que el trabajador se incapacitó en el periodo.
        /// </summary>
        public int DiasIncapacidad
        {
            get
            {
                return this.diasIncapacidad;
            }
            set
            {
                this.diasIncapacidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la razón de la incapacidad.
        /// </summary>
        public string ClaveTipoIncapacidad
        {
            get
            {
                return this.claveTipoIncapacidad;
            }
            set
            {
                this.claveTipoIncapacidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estblecer el monto del importe monetario de la incapacidad.
        /// </summary>
        public decimal ImporteMonetario
        {
            get
            {
                return this.importeMonetario;
            }
            set
            {
                this.importeMonetario = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region informacion CFDI: comprobantes relacionados

        /// <summary>
        /// obtener o establecer la clave de la relación que existe entre éste que se esta generando y el o los CFDI previos.
        /// </summary>
        public string ClaveTipoRelacion
        {
            get
            {
                return this.claveTipoRelacion;
            }
            set
            {
                this.claveTipoRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el folio fiscal (UUID) de un CFDI relacionado con el presente comprobante, por ejemplo: Si el CFDI relacionado es un comprobante de traslado que sirve para 
        /// registrar el movimiento de la mercancía. Si este comprobante se usa como nota de crédito o nota de débito del comprobante relacionado. Si este comprobante es una devolución sobre 
        /// el comprobante relacionado. Si éste sustituye a una factura cancelada.
        /// pattern value="[a-f0-9A-F]{8}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{12}"
        /// </summary>
        public string UuidRelacionado
        {
            get
            {
                return this.uuidRelacionado;
            }
            set
            {
                this.uuidRelacionado = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        public string Correo
        {
            get
            {
                return this.correo;
            }
            set
            {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<LayoutNominaReciboElemento> Elementos
        {
            get
            {
                return this.elementos;
            }
            set
            {
                this.elementos = value;
                this.OnPropertyChanged();
            }
        }

        #region metodos
        #endregion

        public string this[string columnName]
        {
            get
            {
                if (columnName == "TipoNomina" && this.TipoNomina != "O" && this.TipoNomina != "E")
                    return this.TipoNominaValidar();
                
                if (columnName == "TipoRegimen" && !this.RegexValido(this.TipoRegimen, "0[0-9]"))
                    return this.TipoRegimenValidar();
                
                if (columnName == "TipoJornada" && !this.RegexValido(this.TipoJornada, "0[0-9]") && this.TipoNomina != "E")
                    return this.TipoJornadaValidar();

                if (columnName == "UuidRelacionado")
                    return this.IdDocumentoValidado();

                if (columnName == "RegistroPatronal")
                    return this.RegistroPatronalValidar();
                return string.Empty;
            }
        }

        [Browsable(false)]
        public string Error
        {
            get
            {
                if ((this.TipoNomina != "O" && this.TipoNomina != "E") || !this.RfcValido() || !this.RegexValido(this.TipoRegimen, "0|[0-9]") || (!this.RegexValido(this.TipoJornada, "0[0-9]") && this.TipoNomina != "E"))
                    return "Es necesario revisar información de esta fila.";
                
                return string.Empty;
            }
        }

        private string TipoNominaValidar()
        {
            if (this.TipoNomina != "O" && this.TipoNomina != "E")
                return global::Jaeger.Layout.Recursos.ResourceLayoutNomina.TipoNomina_Vacio;
            
            if ((this.TipoNomina != "O" ? false : !this.RegexValido(this.PeriodicidadPago, "0[1-9]")))
                return "El valor del atributo tipo de periodicidad no se encuentra entre 01 al 09.";
            
            if ((this.TipoNomina != "E" ? false : this.PeriodicidadPago != "99"))
                return "El valor del atributo tipo de periodicidad no es 99.";
            
            return string.Empty;
        }

        private bool RfcValido()
        {
            if (this.ReceptorRFC != null)
            {
                bool flag;
                flag = (!(new Regex("^[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9A]$")).IsMatch(this.ReceptorRFC) ? false : true);
                return flag;
            }
            return false;
        }

        private string TipoJornadaValidar()
        {
            if (this.TipoJornada != null)
            {
                if (!this.RegexValido(this.TipoJornada, "0[0-9]"))
                {
                    return "El valor del atributo Nomina.Receptor.TipoJornada no cumple con un valor del catálogo c_TipoJornada.";
                }
            }
            return string.Empty;
        }

        private string TipoRegimenValidar()
        {
            if (!this.RegexValido(this.TipoRegimen, "09|[1-9][0-9]"))
            {
                return "El valor del atributo Nomina.Receptor.TipoRegimen no cumple con un valor del catálogo c_TipoRegimen.";
            }
            if ((!this.RegexValido(this.TipoContrato, "0[1-8]") ? false : !this.RegexValido(this.TipoRegimen, "0[2-4]")))
            {
                return "El atributo Nomina.Receptor.TipoRegimen no es 02, 03 ó 04.";
            }
            if ((!this.RegexValido(this.TipoContrato, "09|[1-9][0-9]") ? false : !this.RegexValido(this.TipoRegimen, "0[5-9]|[1-9][0-9]")))
            {
                return "El atributo Nomina.Receptor.TipoRegimen no está entre 05 a 99.";
            }
            return string.Empty;
        }

        private string IdDocumentoValidado()
        {
            if (!this.RegexValido(@"([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,254}", this.UuidRelacionado))
                return "Id Documento no válido!";
            return string.Empty;
        }

        private string RegistroPatronalValidar()
        {
            if (this.RegexValido(this.TipoContrato, "0[1-8]"))
            {
                if (this.RegistroPatronal == null)
                {
                    return "El atributo Nomina.Emisor.RegistroPatronal se debe registrar.";
                }
            }
            else if (this.RegistroPatronal != null)
            {
                return "El atributo Nomina.Emisor.RegistroPatronal  no se debe registrar.";
            }
            if (this.RegistroPatronal != null)
            {
                if (this.NumSeguridadSocial == null)
                {
                    return "El atributo nomina12:Receptor:NumSeguridadSocial debe existir.";
                }
                if (this.FechaInicioRelLaboral == null)
                {
                    return "El atributo nomina12:Receptor:FechaInicioRelLaboral debe existir.";
                }
                if (this.Antiguedad == null)
                {
                    return "El atributo nomina12:Receptor:Antigüedad debe existir.";
                }
                if (this.RiesgoPuesto == null)
                {
                    return "El atributo nomina12:Receptor:RiesgoPuesto debe existir.";
                }
                if (this.SalarioDiarioIntegrado == null)
                {
                    return "El atributo nomina12:Receptor:SalarioDiarioIntegrado debe existir.";
                }
            }
            return string.Empty;
        }

        private bool RegexValido(string valor, string patron)
        {
            if (valor == null)
                return false;
            bool flag = Regex.IsMatch(valor, string.Concat("^", patron, "$"));
            return flag;
        }
    }

    public partial class LayoutNominaReciboElemento : BasePropertyChangeImplementation
    {
        private EnumNominaElemento elemento;
        private string clave;
        private string tipo;
        private string concepto;
        private decimal importeGravado;
        private decimal importeExento;

        public EnumNominaElemento Elemento
        {
            get
            {
                return this.elemento;
            }
            set
            {
                this.elemento = value;
                this.OnPropertyChanged();
            }
        }

        public string Clave
        {
            get
            {
                return this.clave;
            }
            set
            {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        public string Tipo
        {
            get
            {
                return this.tipo;
            }
            set
            {
                this.tipo = value;
                this.OnPropertyChanged();
            }
        }

        public string Concepto
        {
            get
            {
                return this.concepto;
            }
            set
            {
                this.concepto = value;
                this.OnPropertyChanged();
            }
        }

        public decimal ImporteGravado
        {
            get
            {
                return this.importeGravado;
            }
            set
            {
                this.importeGravado = value;
                this.OnPropertyChanged();
            }
        }

        public decimal ImporteExento
        {
            get
            {
                return this.importeExento;
            }
            set
            {
                this.importeExento = value;
                this.OnPropertyChanged();
            }
        }
    }
}
