﻿using System;
using FileHelpers;

namespace Jaeger.Layout.Entities.RF
{
    public class RF
    {
    }

    [DelimitedRecord(",")]
    public partial class Partida1
    {
        public string Campo1 { get; set; }
        public string Cuenta { get; set; }
        public string Campo3 { get; set; }
        public string Concepto { get; set; }
        public string Campo5 { get; set; }
        [FieldConverter(ConverterKind.Decimal, ".")] // The decimal separator is 
        public decimal Cargp { get; set; }
        [FieldConverter(ConverterKind.Decimal, ".")] // The decimal separator is 
        public decimal Abono { get; set; }

    }

    [DelimitedRecord(",")]
    public partial class Partida2: ICloneable
    {
        public Partida2()
        {
            this.Campo1 = "";
            this.Campo2 = "";
            this.Campo3 = "";
            this.Campo4 = "";
            this.Campo5 = "";
            this.Campo6 = "";
            this.Campo7 = "";
            this.Campo8 = "";
            this.Campo9 = "";
        }
        
        public string Campo1 { get; set; }
        public string Campo2 { get; set; }
        public string Campo3 { get; set; }
        public string Campo4 { get; set; }
        public string Campo5 { get; set; }
        public string Campo6 { get; set; }
        public string Campo7 { get; set; }
        public string Campo8 { get; set; }
        public string Campo9 { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
