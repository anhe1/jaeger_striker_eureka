﻿using System;
using FileHelpers;

namespace Jaeger.Layout.Entities.SAT
{
    /// <summary>
    /// clase de descripcion para el archivo metadata del SAT
    /// </summary>
    [IgnoreFirst(1)]
    [DelimitedRecord("~")]
    public sealed class MetadataSAT
    {
        /// <summary>
        /// obtener o establecer el id comprobante fiscal (UUID)
        /// </summary>
        public string IdDocumento { get; set; }

        /// <summary>
        /// obtener o establcer el registro federal de contribuyentes (RFC) del emisor del comprobante 
        /// </summary>
        public string EmisorRFC { get; set; }

        /// <summary>
        /// obtener o establcer el nombre del emisor del comprobante
        /// </summary>
        public string Emisor { get; set; }

        /// <summary>
        /// obtener o establcer el registro federal de contribuyentes (RFC) del receptor del comprobante 
        /// </summary>
        public string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establcer el nombre del receptor del comprobante
        /// </summary>
        public string Receptor { get; set; }

        /// <summary>
        /// obtener o establecer el registro federal de contribuyentes (RFC) del proveedor autorizado de certificacion (PAC)
        /// </summary>
        public string PAC { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        public string FechaEmision { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de certificacion del comprobante
        /// </summary>
        public string FechaCertificacionSat { get; set; }

        /// <summary>
        /// obtener o establecer el monto del comprobante
        /// </summary>
        public string Monto { get; set; }

        /// <summary>
        /// obtener o establecer el efecto o tipo de comprobante
        /// </summary>
        public string EfectoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer el status del comprobante
        /// </summary>
        public string Estatus { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de la cancelacion del comprobante
        /// </summary>
        public string FechaCancelacion { get; set; }

    }
}
