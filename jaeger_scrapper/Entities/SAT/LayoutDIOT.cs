﻿using FileHelpers;

namespace Jaeger.Layout.Entities.SAT
{
    /// <summary>
    /// DIOT 2019
    /// </summary>
    [DelimitedRecord("|")]
    public class LayoutDIOT
    {
        /// <summary>
        /// obtener o establecer tipo de tercero (campo 1)
        /// </summary>
        public string TipoTercero { get; set; }

        /// <summary>
        /// obtener o establecer tipo de operacion (campo 2)
        /// </summary>
        public string TipoOperacion { get; set; }

        /// <summary>
        /// obtener o establecer RFC del proveedor (campo 3)
        /// </summary>
        public string RFC { get; set; }

        /// <summary>
        /// obtener o establecer Número de ID Fiscal (campo 4)
        /// </summary>
        public string NumeroIdFiscal { get; set; }

        /// <summary>
        /// obtener o establecer Nombre del Extranjero (campo 5)
        /// </summary>
        public string NombreDelExtranjero { get; set; }

        /// <summary>
        /// obtener o establecer País de residencia (campo 6)
        /// </summary>
        public string PaisResidencia { get; set; }

        /// <summary>
        /// obtener o establecer Nacionalidad (campo 7)
        /// </summary>
        public string Nacionalidad { get; set; }

        /// <summary>
        /// obtener o establecer Valor de los actos o actividades pagados a la tasa del 15%  ó 16% de IVA
        /// </summary>
        public string Field_008 { get; set; }

        /// <summary>
        /// obtener o establecer Valor de los actos o actividades pagados a la tasa del 15% de IVA
        /// </summary>
        public string Field_009 { get; set; }

        /// <summary>
        /// obtener o establecer Monto del IVA pagado no acreditable a la tasa del 15% ó 16% (correspondiente en la proporcion de las deducciones autorizadas)
        /// </summary>
        public string Field_010 { get; set; }

        /// <summary>
        /// obtener o establecer Valor de los actos o actividades pagados a la tasa del 10%  u 11%de IVA
        /// </summary>
        public string Field_011 { get; set; }

        /// <summary>
        /// obtener o establecer Valor de los actos o actividades pagados a la tasa del 10% de IVA
        /// </summary>
        public string Field_012 { get; set; }

        /// <summary>
        /// obtener o establecer Valor de los actos o actividades pagados sujeto al estimulo de la region fronteriza norte
        /// </summary>
        public string Field_013 { get; set; }

        /// <summary>
        /// obtener o establecer Monto del IVA pagado no acreditable a la tasa del 10% u 11% (correspondiente en la proporcion de las deducciones autorizadas)
        /// </summary>
        public string Field_014 { get; set; }

        /// <summary>
        /// obtener o establecer Monto del IVA pagado no acreditable sujeto al estimulo de la region fronteriza norte (correspondiente en la proporcion de las deducciones autorizadas)
        /// </summary>
        public string Field_015 { get; set; }

        /// <summary>
        /// obtener o establecer Valor de los actos o actividades pagados en la importación de bienes y servicios a la tasa del 15% ó 16%de IVA
        /// </summary>
        public string Field_016 { get; set; }

        /// <summary>
        /// obtener o establecer Monto del IVA pagado no acreditable por la importación a la tasa del 15% ó 16% (correspondiente en la proporcion de las deducciones autorizadas)
        /// </summary>
        public string Field_017 { get; set; }

        /// <summary>
        /// obtener o establecer Valor de los actos o actividades pagados en la importación de bienes y servicios a la tasa del 10% u 11% de IVA
        /// </summary>
        public string Field_018 { get; set; }

        /// <summary>
        /// obtener o establecer Monto del IVA pagado no acreditable por la importación a la tasa del 10% u 11% (Correspondiente en la proporcion de las deducciones autorizadas)
        /// </summary>
        public string Field_019 { get; set; }

        /// <summary>
        /// obtener o establecer Valor de los actos o actividades pagados en la importación de bienes y servicios por los que no se pagará el IVA (Exentos)
        /// </summary>
        public string Field_020 { get; set; }

        /// <summary>
        /// obtener o establecer Valor de los demás actos o actividades pagados a la tasa del 0% de IVA
        /// </summary>
        public string Field_021 { get; set; }

        /// <summary>
        /// obtener o establecer Valor de los actos o actividades pagados por los que no se pagará el IVA (Exentos)
        /// </summary>
        public string Field_022 { get; set; }

        /// <summary>
        /// obtener o establecer IVA Retenido por el contribuyente
        /// </summary>
        public string Field_023 { get; set; }

        /// <summary>
        /// obtener o establecer IVA correspondiente a las devoluciones, descuentos y bonificaciones sobre compras
        /// </summary>
        public string Field_024 { get; set; }

    }
}