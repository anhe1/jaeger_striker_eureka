﻿using System;
using System.Linq;
using System.ComponentModel;
using Jaeger.Layout.Entities.Base;

namespace Jaeger.Layout.Entities
{
    public class BancoEstadoCuenta : PropertyChangeImplementation
    {
        private DateTime? fechaField;
        private string noClienteField;
        private string razonSocialField;
        private string usuarioField;
        private string tipoField;
        private string sucursalField;
        private string noCuentaField;
        private string nombreField;
        private DateTime fechaPeriodoField;
        private decimal saldoAnteriorField;
        private decimal saldoAlField;
        private decimal saldoPromedioPeriodoField;
        private decimal saldoPromedioAnualField;
        private int diasTrascurridosPeriodoField;
        private decimal totalDespositosField;
        private int numDepositosField;
        private int diasTrascurridosAnualField;
        private int numRetirosField;
        private decimal totalRetirosField;
        private int chequesGiradosField;
        private int chequesExentosField;
        private BindingList<BancoEstadoCuentaMovimiento> movimientos;
        private decimal saldoField;

        public BancoEstadoCuenta()
        {
            this.Movimientos = new BindingList<BancoEstadoCuentaMovimiento>() { RaiseListChangedEvents = true };
            this.Movimientos.AddingNew += Movimientos_AddingNew;
            this.Movimientos.ListChanged += Movimientos_ListChanged;
        }

        /// <summary>
        /// obtener o establecer la fecha del reporte
        /// </summary>
        public DateTime? Fecha
        {
            get
            {
                return this.fechaField;
            }
            set
            {
                this.fechaField = value;
            }
        }

        /// <summary>
        /// obtener o establecer el numero de cliente
        /// </summary>
        public string NoCliente
        {
            get
            {
                return this.noClienteField;
            }
            set
            {
                this.noClienteField=value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del beneficiario de la cuenta
        /// </summary>
        public string RazonSocial
        {
            get
            {
                return this.razonSocialField;
            }
            set
            {
                this.razonSocialField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del usuario que emite el reporte
        /// </summary>
        public string Usuario
        {
            get
            {
                return this.usuarioField;
            }
            set
            {
                this.usuarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cuenta
        /// </summary>
        public string Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de la sucursal de la cuenta
        /// </summary>
        public string Sucursal
        {
            get
            {
                return this.sucursalField;
            }
            set
            {
                this.sucursalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de la cuenta
        /// </summary>
        public string NoCuenta
        {
            get
            {
                return this.noCuentaField;
            }
            set
            {
                this.noCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre
        /// </summary>
        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha 3
        /// </summary>
        public DateTime FechaPeriodo
        {
            get
            {
                return this.fechaPeriodoField;
            }
            set
            {
                this.fechaPeriodoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal SaldoAnterior
        {
            get
            {
                return this.saldoAnteriorField;
            }
            set
            {
                this.saldoAnteriorField = value;
                this.saldoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer saldo a la fecha del periodo 
        /// </summary>
        public decimal SaldoAl
        {
            get
            {
                return this.saldoAlField;
            }
            set
            {
                this.saldoAlField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal SaldoPromedioPeriodo
        {
            get
            {
                return this.saldoPromedioPeriodoField;
            }
            set
            {
                this.saldoPromedioPeriodoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal SaldoPromedioAnual
        {
            get
            {
                return this.saldoPromedioAnualField;
            }
            set
            {
                this.saldoPromedioAnualField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int DiasTrascurridosPeriodo
        {
            get
            {
                return this.diasTrascurridosPeriodoField;
            }
            set
            {
                this.diasTrascurridosPeriodoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal TotalDespositos
        {
            get
            {
                return this.totalDespositosField;
            }
            set
            {
                this.totalDespositosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int NumDepositos
        {
            get
            {
                return this.numDepositosField;
            }
            set
            {
                this.numDepositosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int DiasTrascurridosAnual
        {
            get
            {
                return this.diasTrascurridosAnualField;
            }
            set
            {
                this.diasTrascurridosAnualField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int NumRetiros
        {
            get
            {
                return this.numRetirosField;
            }
            set
            {
                this.numRetirosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal TotalRetiros
        {
            get
            {
                return this.totalRetirosField;
            }
            set
            {
                this.totalRetirosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int ChequesGirados
        {
            get
            {
                return this.chequesGiradosField;
            }
            set
            {
                this.chequesGiradosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public int ChequesExentos
        {
            get
            {
                return this.chequesExentosField;
            }
            set
            {
                this.chequesExentosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer lista de movimientos del estado de cuenta bancario
        /// </summary>
        public BindingList<BancoEstadoCuentaMovimiento> Movimientos
        {
            get
            {
                return this.movimientos;
            }
            set
            {
                if (this.Movimientos != null)
                    this.Movimientos.ListChanged -= Movimientos_ListChanged;
                this.movimientos = value;
                if (this.Movimientos != null)
                    this.Movimientos.ListChanged += Movimientos_ListChanged;
                this.OnPropertyChanged();
            }
        }

        private void Movimientos_ListChanged(object sender, ListChangedEventArgs e)
        {
            //if (this.Movimientos != null)
            //    if (this.Movimientos.Count == 1)
            //        this.saldoField = this.SaldoAnterior;

            if (e.ListChangedType == ListChangedType.ItemAdded)
            {
                if (this.Movimientos[e.NewIndex].Deposito > 0)
                    this.saldoField = saldoField + this.Movimientos[e.NewIndex].Deposito;
                if (this.movimientos[e.NewIndex].Retiro > 0)
                    this.saldoField = saldoField - this.Movimientos[e.NewIndex].Retiro;
                this.Movimientos[e.NewIndex].Saldo = this.saldoField;
            }
        }

        private void Movimientos_AddingNew(object sender, AddingNewEventArgs e)
        {
            
        }
    }

    public partial class BancoEstadoCuentaMovimiento : Jaeger.Layout.Entities.Base.PropertyChangeImplementation
    {
        private DateTime fechaField;
        private string tipoField;
        private string descripcionField;
        private string referenciaField;
        private string noAutorizacionField;
        private decimal depositoField;
        private decimal retiroField;
        private decimal saldoField;

        public BancoEstadoCuentaMovimiento()
        {

        }

        public DateTime Fecha
        {
            get
            {
                return this.fechaField;
            }
            set
            {
                this.fechaField = value;
                this.OnPropertyChanged();
            }
        }

        public string Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Referencia
        {
            get
            {
                return this.referenciaField;
            }
            set
            {
                this.referenciaField = value;
                this.OnPropertyChanged();
            }
        }

        public string Descripcion
        {
            get
            {
                return this.descripcionField;
            }
            set
            {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estalecer el numero de autorizacion del movimiento
        /// </summary>
        public string NoAutorizacion
        {
            get
            {
                return this.noAutorizacionField;
            }
            set
            {
                this.noAutorizacionField = value.Replace("AUT.", "");
                this.OnPropertyChanged();
            }
        }

        public decimal Deposito
        {
            get
            {
                return this.depositoField;
            }
            set
            {
                this.depositoField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Retiro
        {
            get
            {
                return this.retiroField;
            }
            set
            {
                this.retiroField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Saldo
        {
            get
            {
                return this.saldoField;
            }
            set
            {
                this.saldoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}