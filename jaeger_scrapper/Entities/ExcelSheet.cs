﻿namespace Jaeger.Layout.Entities
{
    public partial class ExcelSheet
    {
        public ExcelSheet()
        {

        }

        public ExcelSheet(string name)
        {
            this.Name = name;
        }

        public string Name { get; set; }
    }
}
