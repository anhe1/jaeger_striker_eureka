﻿namespace Jaeger.Layout.Entities.ContaE
{

    public class LayoutCatalogoCuentas
    {
        public string Version { get; set; }
        public string RFC { get; set; }
        public string Mes { get; set; }
        public string Anio { get; set; }
    }

    public class LayoutCatalogoCuenta
    {
        /// <summary>
        /// obtener o establecer código asociador de cuentas y subcuentas conforme al catálogo publicado en la página de internet del SAT. Se debe asociar cada cuenta y subcuenta que sea más apropiado de 
        /// acuerdo con la naturaleza y preponderancia de la cuenta o subcuenta.
        /// </summary>
        public string CodAgrup { get; set; }
        /// <summary>
        /// obtener o establecer la clave con que se distingue la cuenta o subcuenta en la contabilidad 
        /// maxLength value="100"
        /// </summary>
        public string NumCta { get; set; }
        /// <summary>
        /// obtener o establecer el nombre de la cuenta o subcuenta maxLength value="400"
        /// </summary>
        public string Desc { get; set; }
        /// <summary>
        /// obtener o establecer atributo opcional en el caso de subcuentas. Sirve para expresar la clave de la cuenta a la que pertenece dicha subcuenta. Se convierte en requerido cuando se cuente con la información.
        /// maxLength value="100"
        /// </summary>
        public string SubCtaDe { get; set; }
        /// <summary>
        /// obtener o establecer atributo requerido para expresar el nivel en el que se encuentra la cuenta o subcuenta en el catálogo.
        /// </summary>
        public string Nivel { get; set; }
        /// <summary>
        /// obtener o establecer atributo requerido para expresar la naturaleza de la cuenta o subcuenta. (D - Deudora, A - Acreedora). ( Activo = D ) ( Pasivo = A ) ( Capital = A ) ( Ingreso = A ) ( Costo = D ) ( Gasto = D ) ( Resultado Integral de Financiamiento = D y/o A ) ( Cuentas de orden = D y/o A ).
        /// </summary>
        public string Natur { get; set; }
    }
}
