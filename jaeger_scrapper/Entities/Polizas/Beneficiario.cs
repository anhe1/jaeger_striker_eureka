﻿using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Services.Mapping;
/// <summary>
///  layout para cargar archivo excel
/// </summary>
namespace Jaeger.Layout.Entities.Polizas {
    public class Beneficiario {

        public Beneficiario() {

        }

        #region datos del benficiario

        /// <summary>
        /// obtener o establecer nombre del beneficiario
        /// </summary>
        [DataNames("Nombre")]
        public string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer RFC del beneficiario
        /// </summary>
        [DataNames("RFC")]
        public string RFC { get; set; }

        /// <summary>
        /// obtener o establecer cuenta contable relacionada al beneficiario
        /// </summary>
        [DataNames("CuentaContable")]
        public string CuentaContable { get; set; }

        /// <summary>
        /// obtener o establecer referencia de pago (opcional)
        /// </summary>
        [DataNames("Referencia")]
        public string Referencia { get; set; }

        /// <summary>
        /// obtener o establecer clave del banco del beneficiario
        /// </summary>
        [DataNames("ClaveBanco")]
        public string ClaveBanco { get; set; }

        /// <summary>
        /// obtener o establecer numero de cuenta bancaria
        /// </summary>
        [DataNames("Numcuenta")]
        public string NumCuenta { get; set; }

        /// <summary>
        /// obtener o establecer cuenta CLABE del beneficiario
        /// </summary>
        [DataNames("CLABE")]
        public string CLABE { get; set; }

        /// <summary>
        /// obtener o establecer clave de forma de pago
        /// </summary>
        [DataNames("ClaveFormaPago")]
        public string ClaveFormaPago { get; set; }

        #endregion

        #region datos del emisor

        /// <summary>
        /// obtener o establecer cuenta del banco origen
        /// </summary>
        [DataNames("CuentaBancoOrigen")]
        public string CuentaBancoOrigen { get; set; }

        /// <summary>
        /// obtener o establecer clave del banco origen
        /// </summary>
        [DataNames("ClaveBancoOrigen")]
        public string ClaveBancoOrigen { get; set; }

        #endregion
    }
}
