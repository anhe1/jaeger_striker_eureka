﻿using System.Collections.Generic;

namespace Jaeger.Layout.Interface
{
    public interface ILayoutContext<T> where T : class, new()
    {
        List<T> Items { get; set; }

        bool Importar(string archivo);

        bool Save(string archivo);
    }
}