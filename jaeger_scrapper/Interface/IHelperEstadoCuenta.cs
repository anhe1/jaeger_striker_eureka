﻿using System;
using System.Linq;
using Jaeger.Layout.Entities;

namespace Jaeger.Layout.Interface
{
    public interface IHelperEstadoCuenta
    {
        BancoEstadoCuenta EstadoCuenta(string archivo);
    }
}
