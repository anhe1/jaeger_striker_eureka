﻿using FileHelpers;
using Jaeger.Layout.Helpers;

namespace Jaeger.Layout.SAT.Catalogos
{
    public class c_TipoFactor : LayoutContext<c_TipoFactor.c_TipoFactorItem>
    {
        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("|")]
        public partial class c_TipoFactorItem
        {
            public string Clave { get; set; }
        }
    }
}
