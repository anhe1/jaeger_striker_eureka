﻿using FileHelpers;
using Newtonsoft.Json;

namespace Jaeger.Layout.SAT.Catalogos
{
    [JsonObject("item")]
    [IgnoreCommentedLines("//", true)]
    [DelimitedRecord("|")]
    public class c_BaseItem
    {
        [JsonProperty("clv", Order = 1)]
        public string Clave { get; set; }

        [JsonProperty("desc", Order = 2)]
        public string Descripcion { get; set; }

    }
}
