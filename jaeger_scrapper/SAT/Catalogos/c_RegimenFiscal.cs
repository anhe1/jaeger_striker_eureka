﻿/// develop: anhe1 170720191141
/// purpose: lauout de carga del catalogo de remines fiscales utilizados en nomina y CFDI
using System;
using Newtonsoft.Json;
using FileHelpers;
using Jaeger.Layout.Helpers;

namespace Jaeger.Layout.SAT.Catalogos
{
    public class c_RegimenFiscal : LayoutContext<c_RegimenFiscal.c_RegimenFiscalItem>
    {

        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("|")]
        public partial class c_RegimenFiscalItem
        {
            [JsonProperty("clv")]
            public string Clave { get; set; }
            [JsonProperty("desc")]
            public string Descripcion { get; set; }
            [JsonProperty("fisica")]
            public string Fisica { get; set; }
            [JsonProperty("moral")]
            public string Moral { get; set; }
            [FieldOptional]
            [JsonProperty("vigi", Order = 5)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime VigenciaIni { get; set; }

            [JsonProperty("vigf", Order = 6)]
            [FieldOptional]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime? VigenciaFin { get; set; }
        }
    }
}
