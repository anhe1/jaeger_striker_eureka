﻿using System;
using FileHelpers;
using Jaeger.Layout.Helpers;
using Newtonsoft.Json;

namespace Jaeger.Layout.SAT.Catalogos
{
    /// <summary>
    /// 
    /// </summary>
    public class c_Articulo69B : LayoutContext<c_Articulo69B.c_Articulo69BItem>
    {
        [IgnoreFirst(2)]
        [IgnoreEmptyLines()]
        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord(",")]
        public class c_Articulo69BItem
        {
            [JsonProperty("id")]
            public int Id { get; set; }

            [JsonProperty("rfc")]
            public string RFC { get; set; }

            /// <summary>
            /// obtener o establecer nombre del contribuyente
            /// </summary>
            [FieldQuoted('"', QuoteMode.OptionalForRead, MultilineMode.AllowForRead)]
            [JsonProperty("rso")]
            public string RazonSocial { get; set; }

            /// <summary>
            /// obtener o establecer situación del contribuyente
            /// </summary>
            [JsonProperty("situacion")]
            public string Situacion { get; set; }

            #region presunto

            /// <summary>
            /// obtener o establecer número y fecha de oficio global de presunción
            /// </summary>
            [JsonProperty("presuntoNoOficioGlobal")]
            public string PresuntoNoOficioGlobal { get; set; }

            /// <summary>
            /// obtener o establecer fecha de publicación página SAT presuntos
            /// </summary>
            [FieldConverter(ConverterKind.DateMultiFormat, "dd/MM/yy", "d/MM/yy", "dd/MM/yyyy")]
            [FieldNullValue(null)]
            [JsonProperty("presuntoFecPubSat")]
            public DateTime? PresuntoFechaPublicacionSAT { get; set; }

            /// <summary>
            /// Número y fecha de oficio global de presunción
            /// </summary>
            [JsonProperty("presuntoNoOficio")]
            public string PresuntoNoOficio { get; set; }

            /// <summary>
            /// obtener o establecer la fecha de publicación DOF presuntos
            /// </summary>
            [FieldConverter(ConverterKind.DateMultiFormat, "dd/MM/yy", "d/MM/yy", "dd/MM/yyyy")]
            [FieldNullValue(null)]
            [JsonProperty("presuntoFecPubDOF")]
            public DateTime? PresuntoPublicacionDOF { get; set; }

            #endregion

            #region desvirtuado

            /// <summary>
            /// obtener o establecer Publicación página SAT desvirtuados
            /// </summary>
            [FieldConverter(ConverterKind.DateMultiFormat, "dd/MM/yy", "d/MM/yy", "dd/MM/yyyy")]
            [FieldNullValue(null)]
            [JsonProperty("desvirtuadoFecPubSat")]
            public DateTime? DesvirtuadoFechaPublicacionSAT { get; set; }

            /// <summary>
            /// obtener o establecer número y fecha de oficio global de contribuyentes que desvirtuaron
            /// </summary>
            [JsonProperty("desvirtuadoNoOficio")]
            public string DesvirtuadoNoOficio { get; set; }

            /// <summary>
            /// obtener o establecer la fecha de publicación DOF desvirtuados
            /// </summary>
            [FieldConverter(ConverterKind.DateMultiFormat, "dd/MM/yy", "d/MM/yy", "dd/MM/yyyy")]
            [FieldNullValue(null)]
            [JsonProperty("desvirtuadoFecPubDOF")]
            public DateTime? DesvirtuadoPublicacionDOF { get; set; }

            #endregion

            #region definitivo

            /// <summary>
            /// obtener o establecer el número y fecha de oficio global de definitivos
            /// </summary>
            [JsonProperty("definitivoNoOficio")]
            public string DefinitivoNoOficio { get; set; }

            /// <summary>
            /// obtener o establecer la fecha de publicación página SAT definitivos
            /// </summary>
            //[FieldConverter(ConverterKind.DateMultiFormat, "dd/MM/yy", "d/MM/yy", "dd/MM/yyyy")]
            //[FieldNullValue(null)]
            [JsonProperty("definitivoFecPubSat")]
            public string DefinitivoFechaPublicacionSAT { get; set; }

            /// <summary>
            /// obtener o establecer la fecha de publicación DOF definitivos
            /// </summary>
            //[FieldConverter(ConverterKind.DateMultiFormat, "dd/MM/yy", "d/MM/yy", "dd/MM/yyyy")]
            //[FieldNullValue(null)]
            [JsonProperty("definitivoFecPubDOF")]
            public string DefinitivoPublicacionDOF { get; set; }

            #endregion

            #region sentencia

            /// <summary>
            /// obtener o establecer el número y fecha de oficio global de sentencia favorable
            /// </summary>
            [JsonProperty("sentenciaNoOficio")]
            public string SentenciaNoOficio { get; set; }

            /// <summary>
            /// obtener o establecer la fecha de publicación página SAT sentencia favorable
            /// </summary>
            //[FieldConverter(ConverterKind.DateMultiFormat, "dd/MM/yy", "d/MM/yy", "dd/MM/yyyy")]
            //[FieldNullValue(null)]
            [JsonProperty("sentenciaFecPubSat")]
            public string SentenciaFechaPublicacionSAT { get; set; }

            /// <summary>
            /// obtener o establecer número y fecha de oficio global de sentencia favorable
            /// </summary>
            [JsonProperty("sentenciaNoOficio2")]
            public string SentenciaNoOficio2 { get; set; }

            /// <summary>
            /// obtener o establecer la fecha de publicación DOF sentencia favorable
            /// </summary>
            //[FieldConverter(ConverterKind.DateMultiFormat, "dd/MM/yy", "d/MM/yy", "dd/MM/yyyy" )]
            //[FieldNullValue(null)]
            //[JsonProperty("sentenciaFecPubDOF")]
            public string SentenciaPublicacionDOF { get; set; }

            #endregion
        }
    }
}
