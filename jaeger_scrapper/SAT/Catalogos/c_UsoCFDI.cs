﻿using FileHelpers;
using System;
using Newtonsoft.Json;
using Jaeger.Layout.Helpers;

namespace Jaeger.Layout.SAT.Catalogos
{
    public class c_UsoCFDI : LayoutContext<c_UsoCFDI.c_UsoCFDIItem>
    {
        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("|")]
        public partial class c_UsoCFDIItem
        {
            [JsonProperty("clv")]
            public string Clave { get; set; }

            [JsonProperty("desc")]
            public string Descripcion { get; set; }

            [JsonProperty("fisica")]
            public string Fisica { get; set; }

            [JsonProperty("moral")]
            public string Moral { get; set; }

            [JsonProperty("vigi")]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            [FieldOptional]
            public DateTime? VigenciaIni { get; set; }

            [JsonProperty("vigf")]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            [FieldOptional]
            public DateTime? VigenciaFin { get; set; }
        }
    }
}
