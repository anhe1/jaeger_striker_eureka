﻿using FileHelpers;
using Newtonsoft.Json;
using System;
using Jaeger.Layout.Helpers;

namespace Jaeger.Layout.SAT.Catalogos
{
    public class c_ClaveProdServ : LayoutContext<c_ClaveProdServ.c_ClaveProdServItem>
    {

        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("|")]
        public partial class c_ClaveProdServItem
        {
            [JsonProperty("clv")]
            public string Clave { get; set; }
            [JsonProperty("desc")]
            public string Descripcion { get; set; }

            [JsonProperty("iva")]
            [FieldOptional]
            public string IncluirIVA { get; set; }
            [JsonProperty("ieps")]
            [FieldOptional]
            public string IncluirIEPS { get; set; }
            [JsonProperty("comp")]
            [FieldNullValue(null)]
            [FieldOptional]
            public string Complemento { get; set; }
            [JsonProperty("vigi")]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            [FieldOptional]
            public DateTime? VigenciaIni { get; set; }
            [JsonProperty("vigf")]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            [FieldOptional]
            public DateTime? VigenciaFin { get; set; }

            /// <summary>
            /// Estímulo Franja Fronteriza
            /// </summary>
            [JsonProperty("est")]
            [FieldNullValue(null)]
            [FieldOptional]
            public string Estimulo { get; set; }
            [JsonProperty("sim")]
            [FieldNullValue(null)]
            [FieldOptional]
            public string Palabras { get; set; }

        }
    }
}
