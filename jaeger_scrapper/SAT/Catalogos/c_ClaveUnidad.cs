﻿using FileHelpers;
using System;
using Newtonsoft.Json;
using Jaeger.Layout.Helpers;

namespace Jaeger.Layout.SAT.Catalogos
{
    public class c_ClaveUnidad : LayoutContext<c_ClaveUnidad.c_ClaveUnidadItem>
    {
        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("|")]
        public class c_ClaveUnidadItem
        {
            [JsonProperty("clv")]
            public string Clave { get; set; }
            [JsonProperty("nom")]
            public string Nombre { get; set; }

            [FieldOptional]
            [JsonProperty("desc")]
            public string Descripción { get; set; }

            [FieldOptional]
            [FieldNullValue(null)]
            [JsonProperty("nota")]
            public string Nota { get; set; }

            [JsonProperty("vigi")]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            [FieldOptional]
            public DateTime? VigenciaIni { get; set; }
            [JsonProperty("vigf")]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            [FieldOptional]
            public DateTime? VigenciaFin { get; set; }

            [FieldOptional]
            [JsonProperty("sim")]
            public string Simbolo { get; set; }

        }
    }
}
