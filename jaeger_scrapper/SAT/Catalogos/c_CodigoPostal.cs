﻿using FileHelpers;
using System;
using Newtonsoft.Json;
using Jaeger.Layout.Helpers;

namespace Jaeger.Layout.SAT.Catalogos
{
    public class c_CodigoPostal : LayoutContext<c_CodigoPostal.c_CodigoPostalItem>
    {
        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("|")]
        public class c_CodigoPostalItem
        {
            [JsonProperty("cod")]
            public string CodigoPostal { get; set; }
            [JsonProperty("est")]
            public string Estado { get; set; }
            [JsonProperty("mun")]
            public string Municipio { get; set; }
            [JsonProperty("loc")]
            public string Localidad { get; set; }
            /// <summary>
            /// Estímulo Franja Fronteriza
            /// </summary>
            [JsonProperty("esti")]
            public string Estimulo { get; set; }

            [JsonProperty("vigi")]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            [FieldOptional]
            public DateTime? VigenciaIni { get; set; }
            [JsonProperty("vigf")]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            [FieldOptional]
            public DateTime? VigenciaFin { get; set; }
        }
    }
}
