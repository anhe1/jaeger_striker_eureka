﻿using FileHelpers;
using System.Collections.Generic;
using System;
using Newtonsoft.Json;
using Jaeger.Layout.Helpers;

namespace Jaeger.Layout.SAT.Catalogos
{
    public class c_TasaOCuota : LayoutContext<c_TasaOCuota.c_TasaOCuotaItem>
    {
        public c_TasaOCuota()
        {
            this.Items = new List<c_TasaOCuotaItem>();
        }

        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("|")]
        public partial class c_TasaOCuotaItem
        {
            [JsonProperty("rng")]
            public string RangoOFijo { get; set; }
            [JsonProperty("min")]
            public decimal ValorMinimo { get; set; }
            [JsonProperty("max")]
            public decimal ValorMaximo { get; set; }
            [JsonProperty("imp")]
            public string Impuesto { get; set; }
            [JsonProperty("fac")]
            public string Factor { get; set; }
            [JsonProperty("tra")]
            public string Traslado { get; set; }
            [JsonProperty("ret")]
            public string Retencion { get; set; }

            [JsonProperty("vigi", Order = 5)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime VigenciaIni { get; set; }

            [JsonProperty("vigf", Order = 6)]
            [FieldOptional]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime? VigenciaFin { get; set; }
        }
    }
}
