﻿using FileHelpers;
using System;
using Jaeger.Layout.Helpers;
using Newtonsoft.Json;

namespace Jaeger.Layout.SAT.Catalogos
{
    public class c_Banco : LayoutContext<c_Banco.c_BancoItem>
    {
        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("|")]
        public partial class c_BancoItem
        {
            /// <summary>
            /// obtener o establecer la clave del banco
            /// </summary>
            [JsonProperty("clv")]
            public string Clave { get; set; }

            /// <summary>
            /// obtener o establecer el nombre corto del banco
            /// </summary>
            [JsonProperty("desc")]
            public string Descripcion { get; set; }

            /// <summary>
            /// Nombre o razón social
            /// </summary>
            [JsonProperty("rso")]
            public string Nombre { get; set; }

            /// <summary>
            /// obtener o establecer la fecha de inicio de vigencia
            /// </summary>
            [JsonProperty("vigi")]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime VigenciaIni { get; set; }

            /// <summary>
            /// obtener o establecer la fecha fin de vigencia
            /// </summary>
            [JsonProperty("vigf")]
            [FieldOptional]
            [FieldNullValue(null)] 
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime? VigenciaFin { get; set; }
        }
    }
}
