﻿using FileHelpers;
using Newtonsoft.Json;
using Jaeger.Layout.Helpers;

namespace Jaeger.Layout.SAT.Catalogos
{
    public class c_Impuesto : LayoutContext<c_Impuesto.c_ImpuestoItem>
    {
        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("|")]
        public partial class c_ImpuestoItem
        {
            [JsonProperty("clv")]
            public string Clave { get; set; }
            [JsonProperty("desc")]
            public string Descripcion { get; set; }
            public string Retencion { get; set; }
            /// <summary>
            /// Local o federal
            /// </summary>
            public string Traslado { get; set; }
            
            public string LocalFederal { get; set; }
            /// <summary>
            /// Entidad en la que aplica
            /// </summary>
            [FieldOptional]
            public string Entidad { get; set; }
        }
    }
}
