﻿using FileHelpers;
using Newtonsoft.Json;
using Jaeger.Layout.Helpers;

namespace Jaeger.Layout.SAT.Catalogos
{
    public class c_Pais : LayoutContext<c_Pais.c_PaisItem>
    {
        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("|")]
        public partial class c_PaisItem
        {
            [JsonProperty("clv")]
            public string Clave { get; set; }

            [JsonProperty("desc")]
            public string Descripcion { get; set; }

            [JsonProperty("frmc")]
            [FieldQuoted('"', QuoteMode.OptionalForRead, MultilineMode.AllowForRead)]
            [FieldOptional]
            public string FormatoCodigoPostal { get; set; }

            [JsonProperty("frmr")]
            [FieldQuoted('"', QuoteMode.OptionalForRead, MultilineMode.AllowForRead)]
            [FieldOptional]
            public string FormatoRegistrIdentidad { get; set; }

            [JsonProperty("valr")]
            [FieldQuoted('"', QuoteMode.OptionalForRead, MultilineMode.AllowForRead)]
            [FieldOptional]
            public string ValidacionRegistro { get; set; }

            [JsonProperty("agrp")]
            [FieldQuoted('"', QuoteMode.OptionalForRead, MultilineMode.AllowForRead)]
            [FieldOptional]
            public string Agrupaciones { get; set; }
        }
    }
}
