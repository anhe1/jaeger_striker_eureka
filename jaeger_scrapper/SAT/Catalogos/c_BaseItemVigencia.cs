﻿using System;
using FileHelpers;
using Newtonsoft.Json;

namespace Jaeger.Layout.SAT.Catalogos
{
    [IgnoreCommentedLines("//", true)]
    [DelimitedRecord("|")]
    public class c_BaseItemVigencia : c_BaseItem
    {
        [JsonProperty("vigi", Order = 5)]
        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
        [FieldNullValue(null)]
        public DateTime? VigenciaIni { get; set; }

        [JsonProperty("vigf", Order = 6)]
        [FieldOptional]
        [FieldNullValue(null)]
        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
        public DateTime? VigenciaFin { get; set; }
    }
}
