﻿using FileHelpers;
using System;
using Newtonsoft.Json;
using Jaeger.Layout.Helpers;

namespace Jaeger.Layout.SAT.Catalogos
{
    public class c_Moneda : LayoutContext<c_Moneda.c_MonedaItem>
    {
        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("|")]
        public class c_MonedaItem
        {
            [JsonProperty("clv")]
            public string Clave { get; set; }

            [JsonProperty("desc")]
            public string Descripcion { get; set; }

            [JsonProperty("dec")]
            public string Decimales { get; set; }

            [JsonProperty("var")]
            [FieldOptional]
            public string Porcentaje { get; set; }

            [JsonProperty("vigi", Order = 5)]
            [FieldOptional]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime VigenciaIni { get; set; }

            [JsonProperty("vigf", Order = 6)]
            [FieldOptional]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime? VigenciaFin { get; set; }
        }
    }
}
