﻿using System;
using FileHelpers;
using Newtonsoft.Json;
using Jaeger.Layout.Helpers;

namespace Jaeger.Layout.SAT.Catalogos
{
    public class c_NumPedimentoAduana : LayoutContext<c_NumPedimentoAduana.c_NumPedimentoAduanaItem>
    {
        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("|")]
        public partial class c_NumPedimentoAduanaItem
        {
            [JsonProperty("clv")]
            public string Clave { get; set; }
            [JsonProperty("pat")]
            public int Patente { get; set; }
            [JsonProperty("eje")]
            public int Ejercicio { get; set; }
            [JsonProperty("can")]
            public string Cantidad { get; set; }
            [JsonProperty("vigi", Order = 5)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime VigenciaIni { get; set; }

            [JsonProperty("vigf", Order = 6)]
            [FieldOptional]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime? VigenciaFin { get; set; }
        }
    }
}
