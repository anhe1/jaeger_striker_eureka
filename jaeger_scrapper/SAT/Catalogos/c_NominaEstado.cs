﻿using FileHelpers;
using Jaeger.Layout.Helpers;
using Newtonsoft.Json;

namespace Jaeger.Layout.SAT.Catalogos
{
    public class c_NominaEstado : LayoutContext<c_NominaEstado.c_EstadoItem>
    {
        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("|")]
        public partial class c_EstadoItem
        {
            [JsonProperty("clv")]
            public string Clave { get; set; }
            [JsonProperty("pais")]
            public string Pais { get; set; }
            [JsonProperty("nom")]
            public string Nombre { get; set; }
        }
    }
}
