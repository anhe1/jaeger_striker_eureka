﻿using FileHelpers;
using System;
using Newtonsoft.Json;
using Jaeger.Layout.Helpers;

namespace Jaeger.Layout.SAT.Catalogos
{
    public class c_FormaPago : LayoutContext<c_FormaPago.c_FormaPagoItem>
    {

        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("\t")]
        public partial class c_FormaPagoItem
        {
            [JsonProperty("clv")]
            public string Clave { get; set; }
            [JsonProperty("desc")]
            public string Descripcion { get; set; }
            [JsonProperty("bancarizado")]
            public string Bancarizado { get; set; }
            [JsonProperty("numOperacion")]
            public string NúmeroOperación { get; set; }
            [JsonProperty("rfcEmisorCtaOrd")]
            public string RFCEmisor { get; set; }
            [JsonProperty("ctaOrdenante")]
            public string CuentaOrdenante { get; set; }

            [JsonProperty("patronCtaOrd")]
            [FieldQuoted('"', QuoteMode.OptionalForRead, MultilineMode.AllowForRead)]
            public string PatronCuentaOrdenante { get; set; }

            [FieldQuoted('"', QuoteMode.OptionalForRead, MultilineMode.AllowForRead)]
            [JsonProperty("rfcEmisotBenef")]
            public string RFCEmisorCuentaBeneficiario { get; set; }

            [FieldQuoted('"', QuoteMode.OptionalForRead, MultilineMode.AllowForRead)]
            [JsonProperty("ctaDelBenef")]
            public string CuentaBenenficiario { get; set; }

            [FieldQuoted('"', QuoteMode.OptionalForRead, MultilineMode.AllowForRead)]
            [JsonProperty("patronCtaBenef")]
            public string PatronCuentaBeneficiaria { get; set; }
            [FieldOptional]
            [JsonProperty("tipoCadPago")]
            public string TipoCadenaPago { get; set; }
            [FieldOptional]
            [JsonProperty("nombreBancoEmisorCtaOrdenante")]
            public string NombreBancoEmisor { get; set; }

            [FieldOptional]
            [JsonProperty("vigi", Order = 5)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime VigenciaIni { get; set; }

            [JsonProperty("vigf", Order = 6)]
            [FieldOptional]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime? VigenciaFin { get; set; }
        }
    }
}
