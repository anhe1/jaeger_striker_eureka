﻿using System;
using FileHelpers;
using Jaeger.Layout.Helpers;
using Newtonsoft.Json;

namespace Jaeger.Layout.SAT.Catalogos
{
    /// <summary>
    /// lista negra del SAT, Cancelados
    /// </summary>
    public class c_Cancelados : LayoutContext<c_Cancelados.c_CanceladosItem>
    {
        [IgnoreFirst(1)]
        [IgnoreEmptyLines()]
        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord(",")]
        public partial class c_CanceladosItem
        {
            [JsonProperty("rfc")]
            public string RFC { get; set; }

            [FieldQuoted('"', QuoteMode.OptionalForRead, MultilineMode.AllowForRead)]
            [JsonProperty("rso")]
            public string RazonSocial { get; set; }

            [JsonProperty("tipo")]
            public string TipoPersona { get; set; }

            [JsonProperty("supuesto")]
            public string Supuesto { get; set; }

            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            [FieldNullValue(null)]
            [JsonProperty("fec1pub")]
            public DateTime? Fecha1PublicacionX { get; set; }

            [FieldOptional()]
            [FieldQuoted('"', QuoteMode.OptionalForRead, MultilineMode.AllowForRead)]
            [FieldNullValue(null)]
            [JsonProperty("monto")]
            public decimal? Monto { get; set; }

            [FieldOptional()]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            [FieldNullValue(null)]
            [JsonProperty("fec2pub")]
            public DateTime? Fecha2PublicacionX { get; set; }

            [FieldOptional()]
            [JsonProperty("entidad")]
            public string Entidad { get; set; }
        }
    }
}
