﻿using FileHelpers;
using System;
using Newtonsoft.Json;
using Jaeger.Layout.Helpers;

namespace Jaeger.Layout.SAT.Catalogos
{
    public class c_PatenteAduanal : LayoutContext<c_PatenteAduanal.c_PatenteAduanalItem>
    {
        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("|")]
        public partial class c_PatenteAduanalItem
        {
            [JsonProperty("clv")]
            public string Clave { get; set; }

            [FieldOptional]
            [JsonProperty("vigi", Order = 5)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime VigenciaIni { get; set; }

            [JsonProperty("vigf", Order = 6)]
            [FieldOptional]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime? VigenciaFin { get; set; }
        }
    }
}
