﻿using System;
using FileHelpers;
using Jaeger.Layout.Helpers;
using Newtonsoft.Json;

namespace Jaeger.Layout.SAT.Catalogos
{
    /// <summary>
    /// lista negra del SAT, Cancelados
    /// </summary>
    public class c_NoLocalizados : LayoutContext<c_NoLocalizados.c_NoLocalizadoItem>
    {
        [IgnoreFirst(2)]
        [IgnoreEmptyLines()]
        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord(",")]
        public partial class c_NoLocalizadoItem
        {
            [JsonProperty("rfc")]
            public string RFC { get; set; }

            [FieldQuoted('"', QuoteMode.OptionalForRead, MultilineMode.AllowForRead)]
            [JsonProperty("rso")]
            public string RazonSocial { get; set; }

            [JsonProperty("tipo")]
            public string TipoPersona { get; set; }

            [JsonProperty("supuesto")]
            public string Supuesto { get; set; }

            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            [FieldNullValue(null)]
            [JsonProperty("fec1pub")]
            public DateTime? Fecha1PublicacionX { get; set; }
        }
    }
}
