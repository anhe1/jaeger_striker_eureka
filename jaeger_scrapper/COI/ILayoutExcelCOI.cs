﻿using System.Collections.Generic;
using Jaeger.Layout.Entities;

namespace Jaeger.Layout.COI {
    public interface ILayoutExcelCOI {
        bool Exportar(IEnumerable<PolizaCOi> poliza, string fileLocalName);
        bool Exportar(PolizaCOi poliza, string fileLocalName);
    }
}
