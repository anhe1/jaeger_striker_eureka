﻿using System;
using System.Collections.Generic;
using System.IO;
using OfficeOpenXml;
using Jaeger.Layout.Entities;
using Jaeger.Layout.Helpers;

namespace Jaeger.Layout.COI {
    /// <summary>
    /// 
    /// </summary>
    public class LayoutExcelCOI : LayoutExcelCommon, ILayoutExcelCOI {
        private readonly int columnaA = 1;
        private readonly int columnaB = 2;
        private readonly int columnaC = 3;
        private readonly int columnaD = 4;
        private readonly int columnaE = 5;
        private readonly int columnaF = 6;
        private readonly int columnaG = 7;
        private readonly int columnaH = 8;
        private readonly int columnaI = 9;
        private readonly int columnaJ = 10;
        private readonly int columnaK = 11;
        private readonly int columnaL = 12;

        private readonly string inicio_cfdi = "INICIO_CFDI";
        private readonly string fin_cfdi = "FIN_CFDI";
        private readonly string inicio_infopago = "INICIO_INFOPAGO";
        private readonly string fin_infopago = "FIN_INFOPAGO";
        private readonly string fin_partidas = "FIN_PARTIDAS";

        public LayoutExcelCOI() {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        }

        public bool Exportar(IEnumerable<PolizaCOi> poliza, string fileLocalName) {
            return false;
        }

        public bool Exportar(PolizaCOi poliza, string fileLocalName) {
            FileInfo newFile = new FileInfo(fileLocalName);
            using (var package = new ExcelPackage(newFile)) {
                // agregar una nueva hoja
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Poliza COI");
                var currentRow = 3;
                // A3 .- tipo de póliza (diario, ingreso, o egreso)
                worksheet.Cells[currentRow, this.columnaA].Value = poliza.TipoPoliza;
                // B3 .- número de póliza (por lo regular se deja en blanco ya que el sistema lo agrega automáticamente y continuando con la última registrada)
                worksheet.Cells[currentRow, this.columnaB].Value = 1;
                // concepto de poliza C3
                worksheet.Cells[currentRow, this.columnaC].Value = poliza.Concepto;
                // D3 .- día (este espacio corresponde a la fecha, pero en el Layout solo se agrega el día)
                worksheet.Cells[currentRow, this.columnaD].Value = 1;
                
                currentRow++;

                // recorrer todas las cuentas
                foreach (var cuenta in poliza.Cuentas) {
                    // B4.- número de cuenta
                    worksheet.Cells[currentRow, this.columnaB].Value = cuenta.Cuenta;
                    // C4 .- departamento (si no se utiliza algún departamento, solo se dejará en cero)
                    worksheet.Cells[currentRow, this.columnaC].Value = cuenta.Departamento;
                    // D4 .- concepto o descripción de la póliza
                    worksheet.Cells[currentRow, this.columnaD].Value = cuenta.Concepto;
                    // E4 .- tipo de cambio
                    worksheet.Cells[currentRow, this.columnaE].Value = cuenta.TipoCambio;
                    // F4 .- cargo
                    worksheet.Cells[currentRow, this.columnaF].Value = cuenta.Cargo;
                    // G4 .- abono
                    worksheet.Cells[currentRow, this.columnaG].Value = cuenta.Abono;
                    // H4 .- centro de costo (si es el caso)
                    worksheet.Cells[currentRow, this.columnaH].Value = cuenta.CentroCosto;
                    // I4 .- proyecto (si es el caso)
                    worksheet.Cells[currentRow, this.columnaI].Value = cuenta.Proyecto;

                    currentRow++;

                    // comprobantes relacionadas a la cuenta
                    if (cuenta.Comprobantes != null) {
                        worksheet.Cells[currentRow, this.columnaC].Value = this.inicio_cfdi;
                        foreach (var c in cuenta.Comprobantes) {
                            // etiqueta de inicio
                            currentRow++;
                            // C .- fecha del comprobante
                            worksheet.Cells[currentRow, this.columnaC].Value = c.Fecha;
                            // F .- RFC del emisor del comprobante
                            worksheet.Cells[currentRow, this.columnaF].Value = c.EmisorRFC;
                            // G .- RFC del receptor del comprobante
                            worksheet.Cells[currentRow, this.columnaG].Value = c.ReceptorRFC;
                            // H .- Total del comprobante
                            worksheet.Cells[currentRow, this.columnaH].Value = c.Total;
                            // I .- Folio fiscal (UUID)
                            worksheet.Cells[currentRow, this.columnaI].Value = c.IdDocumento;
                            currentRow++;
                        }
                       // fin de comprobantes
                        worksheet.Cells[currentRow, this.columnaC].Value = this.fin_cfdi;
                    }
                    currentRow++;

                    if (cuenta.InfoPago != null) {
                        if (cuenta.InfoPago.Beneficiario != null) {
                            worksheet.Cells[currentRow, this.columnaC].Value = this.inicio_infopago;
                            currentRow++;
                            worksheet.Cells[currentRow, this.columnaC].Value = cuenta.InfoPago.Fecha.ToString("MM/dd/yyyy");
                            worksheet.Cells[currentRow, this.columnaD].Value = "12";//cuenta.InfoPago.BancoOrigen;
                            worksheet.Cells[currentRow, this.columnaE].Value = cuenta.InfoPago.CuentaOrigen;
                            worksheet.Cells[currentRow, this.columnaF].Value = cuenta.InfoPago.FormaPago;
                            worksheet.Cells[currentRow, this.columnaG].Value = cuenta.InfoPago.NumCheque;
                            worksheet.Cells[currentRow, this.columnaH].Value = cuenta.InfoPago.Monto;
                            worksheet.Cells[currentRow, this.columnaI].Value = cuenta.InfoPago.RFC;
                            worksheet.Cells[currentRow, this.columnaJ].Value = cuenta.InfoPago.Beneficiario;
                            worksheet.Cells[currentRow, this.columnaK].Value = cuenta.InfoPago.BancoDestino;
                            worksheet.Cells[currentRow, this.columnaL].Value = cuenta.InfoPago.CuentaDestino;
                            currentRow++;
                            worksheet.Cells[currentRow, this.columnaC].Value = this.fin_infopago;
                        }
                    }
                    currentRow++;
                }

                worksheet.Cells[currentRow, this.columnaC].Value = this.fin_partidas;

                // anchos de columnas
                worksheet.Column(this.columnaA).Width = 5;
                worksheet.Column(this.columnaB).Width = 20;
                worksheet.Column(this.columnaC).Width = 10;
                worksheet.Column(this.columnaD).Width = 40;
                worksheet.Column(this.columnaE).Width = 15;
                worksheet.Column(this.columnaF).Width = 15;
                worksheet.Column(this.columnaG).Width = 15;
                worksheet.Column(this.columnaH).Width = 10;
                worksheet.Column(this.columnaI).Width = 40;
                worksheet.Column(this.columnaJ).Width = 10;
                worksheet.Column(this.columnaK).Width = 10;
                worksheet.Column(this.columnaL).Width = 10;

                // establecer algunas propiedades del documento
                package.Workbook.Properties.Title = "Exportación Poliza COI";
                package.Workbook.Properties.Author = "EDITA Valida";
                package.Workbook.Properties.Comments = "Exportación de datos.";
                package.Workbook.Properties.Created = DateTime.Now;
                // propiedades extendidas
                package.Workbook.Properties.Company = "Jaeger Tacit Ronin";
                // establecer algunos valores de propiedad personalizados
                package.Workbook.Properties.SetCustomPropertyValue("Creado por", "Antonio Hernández R.");
                package.Workbook.Properties.SetCustomPropertyValue("AssemblyName", "Tacit Ronin");

                package.Save();
                return false;
            }
        }

        public bool Exportar(PolizaDetail poliza, string fileLocalName) {
            FileInfo newFile = new FileInfo(fileLocalName);
            using (var package = new ExcelPackage(newFile)) {
                // agregar una nueva hoja
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Poliza COI");
                var currentRow = 3;
                // A3 .- tipo de póliza (diario, ingreso, o egreso)
                worksheet.Cells[currentRow, this.columnaA].Value = poliza.TIPO_POLI;
                // B3 .- número de póliza (por lo regular se deja en blanco ya que el sistema lo agrega automáticamente y continuando con la última registrada)
                worksheet.Cells[currentRow, this.columnaB].Value = 1;
                // concepto de poliza C3
                worksheet.Cells[currentRow, this.columnaC].Value = poliza.CONCEP_PO;
                // D3 .- día (este espacio corresponde a la fecha, pero en el Layout solo se agrega el día)
                worksheet.Cells[currentRow, this.columnaD].Value = 1;

                currentRow++;

                // recorrer todas las cuentas
                foreach (var cuenta in poliza.Auxiliar) {
                    // B4.- número de cuenta
                    worksheet.Cells[currentRow, this.columnaB].Value = cuenta.NUM_CTA;
                    // C4 .- departamento (si no se utiliza algún departamento, solo se dejará en cero)
                    worksheet.Cells[currentRow, this.columnaC].Value = cuenta.NUMDEPTO;
                    // D4 .- concepto o descripción de la póliza
                    worksheet.Cells[currentRow, this.columnaD].Value = cuenta.CONCEP_PO;
                    // E4 .- tipo de cambio
                    worksheet.Cells[currentRow, this.columnaE].Value = cuenta.TIPCAMBIO;
                    if (cuenta.DEBE_HABER == "D") {
                        // F4 .- cargo
                        worksheet.Cells[currentRow, this.columnaF].Value = cuenta.MONTOMOV;
                    } else if (cuenta.DEBE_HABER == "H") {
                        // G4 .- abono
                        worksheet.Cells[currentRow, this.columnaG].Value = cuenta.MONTOMOV;
                    }
                    // H4 .- centro de costo (si es el caso)
                    worksheet.Cells[currentRow, this.columnaH].Value = cuenta.CCOSTOS;
                    // I4 .- proyecto (si es el caso)
                    worksheet.Cells[currentRow, this.columnaI].Value = cuenta.CGRUPOS;

                    currentRow++;

                    // comprobantes relacionadas a la cuenta
                    if (cuenta.Comprobantes != null) {
                        worksheet.Cells[currentRow, this.columnaC].Value = this.inicio_cfdi;
                        foreach (var c in cuenta.Comprobantes) {
                            // etiqueta de inicio
                            currentRow++;
                            // C .- fecha del comprobante
                            worksheet.Cells[currentRow, this.columnaC].Value = c.FECHA.Value.ToShortDateString();
                            // F .- RFC del emisor del comprobante
                            worksheet.Cells[currentRow, this.columnaF].Value = c.RFCEMISOR;
                            // G .- RFC del receptor del comprobante
                            worksheet.Cells[currentRow, this.columnaG].Value = c.RFCRECEPTOR;
                            // H .- Total del comprobante
                            worksheet.Cells[currentRow, this.columnaH].Value = c.MONTO;
                            // I .- Folio fiscal (UUID)
                            worksheet.Cells[currentRow, this.columnaI].Value = c.UUIDTIMBRE;
                        }
                        // fin de comprobantes
                        currentRow++;
                        worksheet.Cells[currentRow, this.columnaC].Value = this.fin_cfdi;
                    }

                    if (cuenta.FormaPago != null) {
                        if (cuenta.FormaPago.BENEF != null) {
                            worksheet.Cells[currentRow, this.columnaC].Value = this.inicio_infopago;
                            currentRow++;
                            worksheet.Cells[currentRow, this.columnaC].Value = cuenta.FormaPago.FECHA.ToString("MM/dd/yyyy");
                            worksheet.Cells[currentRow, this.columnaD].Value = cuenta.FormaPago.BANCO;
                            worksheet.Cells[currentRow, this.columnaE].Value = cuenta.FormaPago.CTAORIG;
                            worksheet.Cells[currentRow, this.columnaF].Value = cuenta.FormaPago.ClaveFormaPago;
                            worksheet.Cells[currentRow, this.columnaG].Value = cuenta.FormaPago.NumCheque;
                            worksheet.Cells[currentRow, this.columnaH].Value = cuenta.FormaPago.MONTO;
                            worksheet.Cells[currentRow, this.columnaI].Value = cuenta.FormaPago.RFC;
                            worksheet.Cells[currentRow, this.columnaJ].Value = cuenta.FormaPago.BENEF;
                            worksheet.Cells[currentRow, this.columnaK].Value = cuenta.FormaPago.BANCODEST;
                            worksheet.Cells[currentRow, this.columnaL].Value = cuenta.FormaPago.CTADEST;
                            currentRow++;
                            worksheet.Cells[currentRow, this.columnaC].Value = this.fin_infopago;
                        }
                    }
                    currentRow++;
                }

                worksheet.Cells[currentRow, this.columnaC].Value = this.fin_partidas;

                // anchos de columnas
                worksheet.Column(this.columnaA).Width = 5;
                worksheet.Column(this.columnaB).Width = 20;
                worksheet.Column(this.columnaC).Width = 10;
                worksheet.Column(this.columnaD).Width = 40;
                worksheet.Column(this.columnaE).Width = 15;
                worksheet.Column(this.columnaF).Width = 15;
                worksheet.Column(this.columnaG).Width = 15;
                worksheet.Column(this.columnaH).Width = 10;
                worksheet.Column(this.columnaI).Width = 40;
                worksheet.Column(this.columnaJ).Width = 10;
                worksheet.Column(this.columnaK).Width = 10;
                worksheet.Column(this.columnaL).Width = 10;

                // establecer algunas propiedades del documento
                package.Workbook.Properties.Title = "Exportación Poliza COI";
                package.Workbook.Properties.Author = "EDITA Valida";
                package.Workbook.Properties.Comments = "Exportación de datos.";
                package.Workbook.Properties.Created = DateTime.Now;
                // propiedades extendidas
                package.Workbook.Properties.Company = "Jaeger Tacit Ronin";
                // establecer algunos valores de propiedad personalizados
                package.Workbook.Properties.SetCustomPropertyValue("Creado por", "Antonio Hernández R.");
                package.Workbook.Properties.SetCustomPropertyValue("AssemblyName", "jaeger scrapper");

                package.Save();
                return false;
            }
        }
    }
}
