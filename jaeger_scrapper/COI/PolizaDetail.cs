﻿using System.Collections.Generic;

namespace Jaeger.Layout.COI {
    public class PolizaDetail : Poliza {
        public PolizaDetail() {
            this.Auxiliar = new List<AuxiliarDetail>();
        }

        public List<AuxiliarDetail> Auxiliar { get; set; }
    }
}
