﻿using System;
using Jaeger.Layout.Helpers;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Layout.COI {
    ///<summary>
    ///UUIDTIMBRES
    ///</summary>
    public partial class UUIDTIMBRES : BasePropertyChangeImplementation {
        public UUIDTIMBRES() {
        }

        private int _NUMREG;
        private string _UUIDTIMBRE;
        private decimal? _MONTO;
        private string _SERIE;
        private string _FOLIO;
        private string _RFCEMISOR;
        private string _RFCRECEPTOR;
        private int? _ORDEN;
        private DateTime? _FECHA;
        private int? _TIPOCOMPROBANTE;
        private double? _TIPOCAMBIO;
        private string _VERSIONCFDI;
        private string _MONEDA;

        /// <summary>
        /// obtener o establecer Numero de registro (NUMREG)
        /// </summary>
        [DataNames("NUMREG")]
        public int NUMREG {
            get {
                return this._NUMREG;
            }
            set {
                this._NUMREG = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer UUID del comprobante (UUIDTIMBRE)
        /// Longitud: 36
        /// </summary>
        [DataNames("UUIDTIMBRE")]
        public string UUIDTIMBRE {
            get {
                return this._UUIDTIMBRE;
            }
            set {
                this._UUIDTIMBRE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer MONTO
        /// </summary>
        [DataNames("MONTO")]
        public decimal? MONTO {
            get {
                return this._MONTO;
            }
            set {
                this._MONTO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer SERIE
        /// Longitud: 100
        /// </summary>
        [DataNames("SERIE")]
        public string SERIE {
            get {
                return this._SERIE;
            }
            set {
                this._SERIE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer FOLIO
        /// Longitud: 100
        /// </summary>
        [DataNames("FOLIO")]
        public string FOLIO {
            get {
                return this._FOLIO;
            }
            set {
                this._FOLIO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC Emisor (RFCEMISOR)
        /// Longitud: 30
        /// </summary>
        [DataNames("RFCEMISOR")]
        public string RFCEMISOR {
            get {
                return this._RFCEMISOR;
            }
            set {
                this._RFCEMISOR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC receptor (RFCRECEPTOR)
        /// Longitud: 30
        /// </summary>
        [DataNames("RFCRECEPTOR")]
        public string RFCRECEPTOR {
            get {
                return this._RFCRECEPTOR;
            }
            set {
                this._RFCRECEPTOR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer orden de la partida (ORDEN)
        /// </summary>
        [DataNames("ORDEN")]
        public int? ORDEN {
            get {
                return this._ORDEN;
            }
            set {
                this._ORDEN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer FECHA
        /// </summary>
        [DataNames("FECHA")]
        public DateTime? FECHA {
            get {
                return this._FECHA;
            }
            set {
                this._FECHA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Indica si es CFDi o Extranjero (TIPOCOMPROBANTE)
        /// </summary>
        [DataNames("TIPOCOMPROBANTE")]
        public int? TIPOCOMPROBANTE {
            get {
                return this._TIPOCOMPROBANTE;
            }
            set {
                this._TIPOCOMPROBANTE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de cambio (TIPOCAMBIO)
        /// </summary>
        [DataNames("TIPOCAMBIO")]
        public double? TIPOCAMBIO {
            get {
                return this._TIPOCAMBIO;
            }
            set {
                this._TIPOCAMBIO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer VERSIONCFDI
        /// Longitud: 6
        /// </summary>
        [DataNames("VERSIONCFDI")]
        public string VERSIONCFDI {
            get {
                return this._VERSIONCFDI;
            }
            set {
                this._VERSIONCFDI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer MONEDA
        /// Longitud: 3
        /// </summary>
        [DataNames("MONEDA")]
        public string MONEDA {
            get {
                return this._MONEDA;
            }
            set {
                this._MONEDA = value;
                this.OnPropertyChanged();
            }
        }
    }
}
