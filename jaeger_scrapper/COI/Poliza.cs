﻿using System;
using Jaeger.Layout.Helpers;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Layout.COI {
    ///<summary>
    ///POLIZA
    ///</summary>
    public class Poliza : BasePropertyChangeImplementation {
        private string _TIPO_POLI;
        private string _NUM_POLIZ;
        private int _PERIODO;
        private int _EJERCICIO;
        private DateTime? _FECHA_POL;
        private string _CONCEP_PO;
        private int? _NUM_PART;
        private string _LOGAUDITA;
        private string _CONTABILIZ;
        private int? _NUMPARCUA;
        private int? _TIENEDOCUMENTOS;
        private int? _PROCCONTAB;
        private string _ORIGEN;
        private string _UUID;
        private int? _ESPOLIZAPRIVADA;
        private string _UUIDOP;

        public Poliza() {
        }

        /// <summary>
        /// obtener o establecer Tipo de poliza (TIPO_POLI) length = 2
        /// </summary>
        [DataNames("TIPO_POLI")]
        public string TIPO_POLI {
            get {
                return this._TIPO_POLI;
            }
            set {
                this._TIPO_POLI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de poliza (NUM_POLIZ) length = 5
        /// </summary>
        [DataNames("NUM_POLIZ")]
        public string NUM_POLIZ {
            get {
                return this._NUM_POLIZ;
            }
            set {
                this._NUM_POLIZ = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PERIODO
        /// </summary>
        [DataNames("PERIODO")]
        public int PERIODO {
            get {
                return this._PERIODO;
            }
            set {
                this._PERIODO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer EJERCICIO
        /// </summary>
        [DataNames("EJERCICIO")]
        public int EJERCICIO {
            get {
                return this._EJERCICIO;
            }
            set {
                this._EJERCICIO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de la poliza (FECHA_POL)
        /// </summary>
        [DataNames("FECHA_POL")]
        public DateTime? FECHA_POL {
            get {
                return this._FECHA_POL;
            }
            set {
                this._FECHA_POL = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer concepto (CONCEP_PO)
        /// </summary>
        [DataNames("CONCEP_PO")]
        public string CONCEP_PO {
            get {
                return this._CONCEP_PO;
            }
            set {
                this._CONCEP_PO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de partida (NUM_PART)
        /// </summary>
        [DataNames("NUM_PART")]
        public int? NUM_PART {
            get {
                return this._NUM_PART;
            }
            set {
                this._NUM_PART = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer poliza auditada (LOGAUDITA) length = 1
        /// </summary>
        [DataNames("LOGAUDITA")]
        public string LOGAUDITA {
            get {
                return this._LOGAUDITA;
            }
            set {
                this._LOGAUDITA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer esta contabilizada (CONTABILIZ) length = 1
        /// </summary>
        [DataNames("CONTABILIZ")]
        public string CONTABILIZ {
            get {
                return this._CONTABILIZ;
            }
            set {
                this._CONTABILIZ = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Numero de partida de la cuenta (NUMPARCUA)
        /// </summary>
        [DataNames("NUMPARCUA")]
        public int? NUMPARCUA {
            get {
                return this._NUMPARCUA;
            }
            set {
                this._NUMPARCUA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Tiene documentos asosciados (TIENEDOCUMENTOS)
        /// </summary>
        [DataNames("TIENEDOCUMENTOS")]
        public int? TIENEDOCUMENTOS {
            get {
                return this._TIENEDOCUMENTOS;
            }
            set {
                this._TIENEDOCUMENTOS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Numero de transaccion para la interfaz (PROCCONTAB)
        /// </summary>
        [DataNames("PROCCONTAB")]
        public int? PROCCONTAB {
            get {
                return this._PROCCONTAB;
            }
            set {
                this._PROCCONTAB = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ORIGEN Length = 15
        /// </summary>
        [DataNames("ORIGEN")]
        public string ORIGEN {
            get {
                return this._ORIGEN;
            }
            set {
                this._ORIGEN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer UUID del comprobante (UUID) 
        /// Longitud = 100
        /// </summary>
        [DataNames("UUID")]
        public string UUID {
            get {
                return this._UUID;
            }
            set {
                this._UUID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer espoliza privada (ESPOLIZAPRIVADA)
        /// </summary>
        [DataNames("ESPOLIZAPRIVADA")]
        public int? ESPOLIZAPRIVADA {
            get {
                return this._ESPOLIZAPRIVADA;
            }
            set {
                this._ESPOLIZAPRIVADA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer UUID de la operacion asociada (UUIDOP)
        /// Longitud = 40
        /// </summary>
        [DataNames("UUIDOP")]
        public string UUIDOP {
            get {
                return this._UUIDOP;
            }
            set {
                this._UUIDOP = value;
                this.OnPropertyChanged();
            }
        }
    }
}
