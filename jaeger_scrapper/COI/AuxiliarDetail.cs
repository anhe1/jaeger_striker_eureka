﻿using System.Collections.Generic;

namespace Jaeger.Layout.COI {
    public class AuxiliarDetail : Auxiliar {
        public AuxiliarDetail() {
            this.Comprobantes = new List<ComprobanteFiscal>();
            this.FormaPago = new INFADIPAR();
        }

        public List<ComprobanteFiscal> Comprobantes { get; set; }
        public INFADIPAR FormaPago { get; set; }
    }
}
