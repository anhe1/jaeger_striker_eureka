﻿using System;
using Jaeger.Layout.Helpers;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Layout.COI {
    ///<summary>
    ///AUXILIAR20
    ///</summary>
    public class Auxiliar : BasePropertyChangeImplementation {
        private string _TIPO_POLI;
        private string _NUM_POLIZ;
        private double _NUM_PART;
        private int _PERIODO;
        private int _EJERCICIO;
        private string _NUM_CTA;
        private DateTime? _FECHA_POL;
        private string _CONCEP_PO;
        private string _DEBE_HABER;
        private decimal? _MONTOMOV;
        private int? _NUMDEPTO;
        private double? _TIPCAMBIO;
        private int? _CONTRAPAR;
        private int? _ORDEN;
        private int? _CCOSTOS;
        private int? _CGRUPOS;
        private int? _IDINFADIPAR;
        private int? _IDUUID;

        public Auxiliar() {
            this._TIPCAMBIO = 1;
        }

        /// <summary>
        /// obtener o establecer tipo (TIPO_POLI)
        /// Longitud = 2
        /// </summary>
        [DataNames("TIPO_POLI")]
        public string TIPO_POLI {
            get {
                return this._TIPO_POLI;
            }
            set {
                this._TIPO_POLI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero (NUM_POLIZ)
        /// Longitud = 5
        /// </summary>
        [DataNames("NUM_POLIZ")]
        public string NUM_POLIZ {
            get {
                return this._NUM_POLIZ;
            }
            set {
                this._NUM_POLIZ = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de partida (NUM_PART)
        /// </summary>
        [DataNames("NUM_PART")]
        public double NUM_PART {
            get {
                return this._NUM_PART;
            }
            set {
                this._NUM_PART = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PERIODO
        /// </summary>
        [DataNames("PERIODO")]
        public int PERIODO {
            get {
                return this._PERIODO;
            }
            set {
                this._PERIODO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer EJERCICIO
        /// </summary>
        [DataNames("EJERCICIO")]
        public int EJERCICIO {
            get {
                return this._EJERCICIO;
            }
            set {
                this._EJERCICIO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Numero de la cuenta (NUM_CTA)
        /// Longitud = 21
        /// </summary>
        [DataNames("NUM_CTA")]
        public string NUM_CTA {
            get {
                return this._NUM_CTA;
            }
            set {
                this._NUM_CTA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Fecha de registro (FECHA_POL)
        /// </summary>
        [DataNames("FECHA_POL")]
        public DateTime? FECHA_POL {
            get {
                return this._FECHA_POL;
            }
            set {
                this._FECHA_POL = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Concepto (CONCEP_PO)
        /// Longitud = 120
        /// </summary>
        [DataNames("CONCEP_PO")]
        public string CONCEP_PO {
            get {
                return this._CONCEP_PO;
            }
            set {
                this._CONCEP_PO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Abono o cargo (DEBE_HABER)
        /// Longitud = 1
        /// </summary>
        [DataNames("DEBE_HABER")]
        public string DEBE_HABER {
            get {
                return this._DEBE_HABER;
            }
            set {
                this._DEBE_HABER = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Monto movimiento (MONTOMOV)
        /// </summary>
        [DataNames("MONTOMOV")]
        public decimal? MONTOMOV {
            get {
                return this._MONTOMOV;
            }
            set {
                this._MONTOMOV = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Numero depto (NUMDEPTO)
        /// </summary>
        [DataNames("NUMDEPTO")]
        public int? NUMDEPTO {
            get {
                return this._NUMDEPTO;
            }
            set {
                this._NUMDEPTO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de cambio (TIPCAMBIO)
        /// </summary>
        [DataNames("TIPCAMBIO")]
        public double? TIPCAMBIO {
            get {
                return this._TIPCAMBIO;
            }
            set {
                this._TIPCAMBIO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Contrapartida (CONTRAPAR)
        /// </summary>
        [DataNames("CONTRAPAR")]
        public int? CONTRAPAR {
            get {
                return this._CONTRAPAR;
            }
            set {
                this._CONTRAPAR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Orden de la partida (ORDEN)
        /// </summary>
        [DataNames("ORDEN")]
        public int? ORDEN {
            get {
                return this._ORDEN;
            }
            set {
                this._ORDEN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer centro de costos CCOSTOS
        /// </summary>
        [DataNames("CCOSTOS")]
        public int? CCOSTOS {
            get {
                return this._CCOSTOS;
            }
            set {
                this._CCOSTOS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer CGRUPOS
        /// </summary>
        [DataNames("CGRUPOS")]
        public int? CGRUPOS {
            get {
                return this._CGRUPOS;
            }
            set {
                this._CGRUPOS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Numero de registro de forma de pago (IDINFADIPAR)
        /// </summary>
        [DataNames("IDINFADIPAR")]
        public int? IDINFADIPAR {
            get {
                return this._IDINFADIPAR;
            }
            set {
                this._IDINFADIPAR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Numero de registro de comprobantes (IDUUID)
        /// </summary>
        [DataNames("IDUUID")]
        public int? IDUUID {
            get {
                return this._IDUUID;
            }
            set {
                this._IDUUID = value;
                this.OnPropertyChanged();
            }
        }
    }
}
