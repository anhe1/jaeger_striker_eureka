﻿using System;
using Jaeger.Layout.Helpers;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Layout.COI {
    ///<summary>
    /// Informacion Adicional de la partida (INFADIPAR)
    ///</summary>
    public class INFADIPAR : BasePropertyChangeImplementation {
        private int _NUMREG;
        private string _FRMPAGO;
        private string _NUMCHEQUE;
        private int _BANCO;
        private string _CTAORIG;
        private DateTime _FECHA;
        private decimal? _MONTO;
        private string _BENEF;
        private string _RFC;
        private int _BANCODEST;
        private string _CTADEST;
        private string _BANCOORIGEXT;
        private string _BANCODESTEXT;
        private string _IDFISCAL;

        public INFADIPAR() {
            this.NumCheque = "NA";
        }

        /// <summary>
        /// obtener o establecer numero de registro (NUMREG)
        /// </summary>
        [DataNames("NUMREG")]
        public int NUMREG {
            get { return this._NUMREG; }
            set {
                this._NUMREG = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer forma de pago (FRMPAGO)
        /// </summary>
        [DataNames("FRMPAGO")]
        public string ClaveFormaPago {
            get { return this._FRMPAGO; }
            set {
                this._FRMPAGO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de cheque (NUMCHEQUE)
        /// </summary>
        [DataNames("NUMCHEQUE")]
        public string NumCheque {
            get { return this._NUMCHEQUE; }
            set {
                this._NUMCHEQUE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del banco origen (BANCO)
        /// </summary>
        [DataNames("BANCO")]
        public int BANCO {
            get { return this._BANCO; }
            set {
                this._BANCO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cuenta origen (CTAORIG)
        /// </summary>
        [DataNames("CTAORIG")]
        public string CTAORIG {
            get { return this._CTAORIG; }
            set {
                this._CTAORIG = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer FECHA
        /// </summary>
        [DataNames("FECHA")]
        public DateTime FECHA {
            get { return this._FECHA; }
            set {
                this._FECHA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer MONTO
        /// </summary>
        [DataNames("MONTO")]
        public decimal? MONTO {
            get { return this._MONTO; }
            set {
                this._MONTO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer beneficiario
        /// </summary>
        [DataNames("BENEF")]
        public string BENEF {
            get { return this._BENEF; }
            set {
                this._BENEF = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC
        /// </summary>
        [DataNames("RFC")]
        public string RFC {
            get { return this._RFC; }
            set {
                this._RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del banco destino (BANCODEST)
        /// </summary>
        [DataNames("BANCODEST")]
        public int BANCODEST {
            get { return this._BANCODEST; }
            set {
                this._BANCODEST = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cuenta destino (CTADEST)
        /// </summary>
        [DataNames("CTADEST")]
        public string CTADEST {
            get { return this._CTADEST; }
            set {
                this._CTADEST = value;
                this.OnPropertyChanged();
            }
        }


        /// <summary>
        /// obtener o establecer banco origen extranjero (BANCOORIGEXT)
        /// </summary>
        [DataNames("BANCOORIGEXT")]
        public string BANCOORIGEXT {
            get { return this._BANCOORIGEXT; }
            set {
                this._BANCOORIGEXT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer banco destino extranjeto (BANCODESTEXT) 255
        /// </summary>
        [DataNames("BANCODESTEXT")]
        public string BANCODESTEXT {
            get { return this._BANCODESTEXT; }
            set {
                this._BANCODESTEXT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer identificador fiscal (IDFISCAL) 40
        /// </summary>
        [DataNames("IDFISCAL")]
        public string IDFISCAL {
            get { return this._IDFISCAL; }
            set {
                this._IDFISCAL = value;
                this.OnPropertyChanged();
            }
        }
    }
}
