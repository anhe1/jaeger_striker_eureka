﻿using Jaeger.Layout.Entities.ContaE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Layout.Helpers
{
    public class LayoutExcelContaE : LayoutExcelCommon
    {
        public List<LayoutCatalogoCuenta> Lista { get; set; }

        public void Datos(string hoja)
        {
            this.Lista = new List<LayoutCatalogoCuenta>();
            foreach (System.Data.DataRow item in this.TableCollection[hoja].Rows)
            {
                if (item.ItemArray[0].ToString() != "" | item.ItemArray[1].ToString() != "")
                {
                    var linea = new LayoutCatalogoCuenta();
                    linea.CodAgrup = item.ItemArray[0].ToString();
                    linea.NumCta = item.ItemArray[1].ToString();
                    linea.Desc = item.ItemArray[2].ToString();
                    linea.SubCtaDe = item.ItemArray[3].ToString();
                    linea.Nivel = item.ItemArray[4].ToString();
                    linea.Natur = item.ItemArray[5].ToString();
                    this.Lista.Add(linea);
                }
            }
        }

        public void Crear(string destino)
        {

        }
    }
}
