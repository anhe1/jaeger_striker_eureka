﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
using Jaeger.Layout.Entities.SAT;

namespace Jaeger.Layout.Helpers
{
    public class LayoutDIOT2019
    {
        public LayoutDIOT2019()
        {

        }

        public void Exportar(string archivoSalida, List<LayoutDIOT> lista)
        {
            var engine = new FileHelperEngine(typeof(Entities.SAT.LayoutDIOT));
            engine.WriteFile(archivoSalida, lista.ToArray());
        }
    }
}