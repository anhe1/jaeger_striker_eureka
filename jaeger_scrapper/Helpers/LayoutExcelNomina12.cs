﻿using System.Data;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Jaeger.Layout.Entities.Nomina;

namespace Jaeger.Layout.Helpers
{
    public partial class LayoutExcelNomina12 : LayoutExcelCommon
    {

        public LayoutExcelNomina12()
        {
        }

        public BindingList<LayoutNominaRecibo> Recibos { get; set; }

        public void Reader(string hoja)
        {
            if (!(this.TableCollection == null))
            {
                this.Recibos = new BindingList<LayoutNominaRecibo>();
                DataTable dt = this.TableCollection[hoja];
                if (!(dt == null))
                {
                    foreach (DataRow fila in dt.Rows)
                    {
                        LayoutNominaRecibo item = new LayoutNominaRecibo();
                        foreach (DataColumn columna in fila.Table.Columns)
                        {
                            switch (columna.ColumnName.ToString().ToUpper())
                            {
                                case "NOMBREEMISOR":
                                    item.Emisor = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "RFCEMISOR":
                                    break;
                                case "CURPEMISOR":
                                    item.EmisorCURP = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "REGISTROPATRONAL":
                                    item.RegistroPatronal = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "TIPONOMINA":
                                    item.TipoNomina = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "RFCRECEPTOR":
                                    item.ReceptorRFC = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "FECHAPAGO":
                                    item.FechaPago = DbConvert.ConvertDateTime(fila[columna.ColumnName]);
                                    break;
                                case "FECHAINICIALPAGO":
                                    item.FechaInicialPago = DbConvert.ConvertDateTime(fila[columna.ColumnName]);
                                    break;
                                case "FECHAFINALPAGO":
                                    item.FechaFinalPago = DbConvert.ConvertDateTime(fila[columna.ColumnName]);
                                    break;
                                case "NUMDIASPAGADOS":
                                    item.NumDiasPagados = DbConvert.ConvertInt32(fila[columna.ColumnName]);
                                    break;
                                case "LUGAREXPEDICION":
                                    item.LugarExpedicion = DbConvert.ConvertString(fila[columna.ColumnName]).Trim();
                                    break;
                                case "NOMBRERECEPTOR":
                                    item.Receptor = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "CORREO":
                                    item.Correo = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "CURPRECEPTOR":
                                    item.ReceptorCURP = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "NUMSEGURIDADSOCIAL":
                                    item.NumSeguridadSocial = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "FECHAINICIORELLABORAL":
                                    item.FechaInicioRelLaboral = DbConvert.ConvertDateTime(fila[columna.ColumnName]);
                                    break;
                                case "ANTIGÜEDAD":
                                    item.Antiguedad = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "TIPOCONTRATO":
                                    item.TipoContrato = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "SINDICALIZADO":
                                    item.Sindicalizado = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "TIPOJORNADA":
                                    item.TipoJornada = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "TIPOREGIMEN":
                                    item.TipoRegimen = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "NUMEMPLEADO":
                                    item.NumEmpleado = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "DEPARTAMENTO":
                                    item.Departamento = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "PUESTO":
                                    item.Puesto = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "RIESGOPUESTO":
                                    item.RiesgoPuesto = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "PERIODICIDADPAGO":
                                    item.PeriodicidadPago = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "BANCO":
                                    item.Banco = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "CUENTABANCARIA":
                                    item.CuentaBancaria = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "SALARIOBASECOTAPOR":
                                    item.SalarioBaseCotApor = DbConvert.ConvertDecimal(fila[columna.ColumnName]);
                                    break;
                                case "SALARIODIARIOINTEGRADO":
                                    item.SalarioDiarioIntegrado = DbConvert.ConvertDecimal(fila[columna.ColumnName]);
                                    break;
                                case "CLAVEENTFED":
                                    item.EntidadFederativa = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "SUBSIDIOCAUSADO":
                                    item.SubsidioCausado = DbConvert.ConvertDecimal(fila[columna.ColumnName]);
                                    break;
                                case "DIASINCAPACIDAD":
                                    item.DiasIncapacidad = DbConvert.ConvertInt32(fila[columna.ColumnName]);
                                    break;
                                case "TIPOINCAPACIDAD":
                                    item.ClaveTipoIncapacidad = DbConvert.ConvertString(fila[columna.ColumnName]);
                                    break;
                                case "IMPORTEMONETARIO":
                                    item.ImporteMonetario = DbConvert.ConvertDecimal(fila[columna.ColumnName]);
                                    break;
                                default:
                                    {
                                        if (columna.ColumnName.ToUpper().Contains("ELEMENTO"))
                                        {
                                            var codigo = Regex.Replace(columna.ColumnName, @"[^\d]", "");
                                            LayoutNominaReciboElemento item2 = new LayoutNominaReciboElemento();
                                            string clave1 = string.Concat("clave", codigo);
                                            item2.Clave = DbConvert.ConvertString(fila[clave1]);
                                            item2.Tipo = DbConvert.ConvertString(fila[string.Concat("tipo", codigo)]);
                                            item2.Concepto = DbConvert.ConvertString(fila[string.Concat("CONCEPTO", codigo)]);
                                            item2.ImporteGravado = DbConvert.ConvertDecimal(fila[string.Concat("gravado", codigo)]);
                                            item2.ImporteExento = DbConvert.ConvertDecimal(fila[string.Concat("exento", codigo)]);
                                            //item2.ElementoText = DbConvert.ConvertString(fila[string.Concat("elemento", codigo)]);
                                            if (DbConvert.ConvertString(fila[string.Concat("elemento", codigo)]).Trim() == "1")
                                            {
                                                item2.Elemento = Enums.EnumNominaElemento.Percepcion;
                                            }
                                            else if (DbConvert.ConvertString(fila[string.Concat("elemento", codigo)]).Trim() == "2")
                                            {
                                                item2.Elemento = Enums.EnumNominaElemento.Deduccion;
                                            }
                                            else if (DbConvert.ConvertString(fila[string.Concat("elemento", codigo)]).Trim() == "3")
                                            {
                                                item2.Elemento = Enums.EnumNominaElemento.HorasExtra;
                                            }
                                            else
                                            {
                                                item2.Elemento = Enums.EnumNominaElemento.None;
                                            }

                                            if (item2.Elemento > 0)
                                                item.Elementos.Add(item2);
                                        }

                                        break;
                                    }
                            }
                        }
                        this.Recibos.Add(item);
                    }
                }
            }
        }
    }
}
