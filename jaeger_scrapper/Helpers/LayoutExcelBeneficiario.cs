﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Jaeger.Layout.Entities.Polizas;
using System.IO;
using OfficeOpenXml;
using Jaeger.Layout.Entities;

namespace Jaeger.Layout.Helpers {
    public class LayoutExcelBeneficiario : LayoutExcelCommon {
        public List<Beneficiario> Reader(string hoja) {
            if (!(this.TableCollection == null)) {
                DataTable dt = this.TableCollection[hoja];
                var response = new Jaeger.Domain.Services.Mapping.DataNamesMapper<Beneficiario>();
                return response.Map(dt).ToList();
            }
            return null;
        }
    }
}
