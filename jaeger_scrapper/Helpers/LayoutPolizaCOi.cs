﻿using System;
using System.Linq;
using System.Collections.Generic;
using FileHelpers;
using FileHelpers.MasterDetail;
using Jaeger.Layout.Entities;

namespace Jaeger.Layout.Helpers {
    public class LayoutPolizaCOi {
        public bool Exportar(PolizaCOi[] poliza, string archivo) {
            try {
                var engine = new FileHelperEngine<PolizaCOi>();
                engine.WriteFile(archivo, poliza);
                return true;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool Exportar(PolizaCuentaCOi[] arr, string archivo) {
            try {
                var engine = new FileHelperEngine<PolizaCuentaCOi>();
                engine.WriteFile(archivo, arr);
                return true;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public void Exportar1(PolizaCOi poliza, string archivo) {
            var masterDetEng = new MasterDetailEngine<PolizaCOi, PolizaCuentaCOi>();

            MasterDetails<PolizaCOi, PolizaCuentaCOi> record;
            var records = new List<MasterDetails<PolizaCOi, PolizaCuentaCOi>>();

            record = new MasterDetails<PolizaCOi, PolizaCuentaCOi>();
            records.Add(record);

            record.Master = new PolizaCOi();
            record.Master = poliza;

            record.Details = poliza.Cuentas.ToArray();

            masterDetEng.WriteFile(archivo, records.ToArray());
        }
    }
}
