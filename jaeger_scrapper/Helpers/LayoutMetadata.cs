﻿using Jaeger.Layout.Entities.SAT;
using FileHelpers;

namespace Jaeger.Layout.Helpers
{
    public class LayoutMetadata
    {
        public MetadataSAT[] Importar(string archivo)
        {
            var engine = new FileHelperEngine<MetadataSAT>();

            engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;

            MetadataSAT[] res = engine.ReadFile(archivo);

            if (engine.ErrorManager.ErrorCount > 0)
                engine.ErrorManager.SaveErrors("Errors.txt");

            return res;
        }

        public MetadataSAT[] ImportarText(string archivo)
        {
            var engine = new FileHelperEngine<MetadataSAT>();

            engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;

            MetadataSAT[] res = engine.ReadString(archivo);

            if (engine.ErrorManager.ErrorCount > 0)
                engine.ErrorManager.SaveErrors("Errors.txt");

            return res;
        }
    }
}
