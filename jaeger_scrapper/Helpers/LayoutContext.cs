﻿/// develop: anhe 16072019
/// purpose: LayoutContext, es utilizado para la lectura de un layout de catalogos en formato txt con el siguiente formato
/* 
 // <- indica comentario en el archivo @<- indica el master del archivo es decir los datos generales
 //version|revision|titulo|FechaInicioVigencia|FechaFinVigencia|FechaActualizacion
 @1.0|0|Título del Catálogo|01/01/2019|01/01/2017|01/01/2017
 //Clave|Descripción <- titulos
 Clave|Descripcion <- un registro
 La salida del archivo es un objeto JSON equivalente al requerido para la lectura de los catalogos en produccion, algunos campos estan marcados como opcionales.
*/
using FileHelpers;
using FileHelpers.MasterDetail;
using System.Collections.Generic;
using System.IO;
using System;
using Newtonsoft.Json;
using Jaeger.Layout.Interface;

namespace Jaeger.Layout.Helpers
{
    public class LayoutContext<T> : ILayoutContext<T> where T : class, new()
    {
        [IgnoreCommentedLines("//", true)]
        [DelimitedRecord("|")]
        public partial class LayoutContextBase
        {
            public LayoutContextBase()
            {
                this.Actualizacion = DateTime.Now;
                this.Items = new List<T>();
            }

            /// <summary>
            /// obtener o establecer version del catalogo
            /// </summary>
            [JsonProperty("ver", Order = 1)]
            public string Version { get; set; }

            /// <summary>
            /// obtener o establecer numero de revision del catalogo
            /// </summary>
            [JsonProperty("rev", Order = 2)]
            public string Revision { get; set; }

            /// <summary>
            /// obtener o establecer titulo del catalogo
            /// </summary>
            [JsonProperty("titulo", Order = 3)]
            public string Titulo { get; set; }

            /// <summary>
            /// obtener o establecer fecha de inicio de vigencia del catalogo
            /// </summary>
            [FieldOptional]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            [JsonProperty("vigi", Order = 4)]
            public DateTime? VigenciaIni { get; set; }

            /// <summary>
            /// obtener o establecer fecha de fin de vigencia
            /// </summary>
            [FieldOptional]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            [JsonProperty("vigf", Order = 5)]
            public DateTime? VigenciaFin { get; set; }

            /// <summary>
            /// obtener o establecer fecha de actualización del catalogo
            /// </summary>
            [FieldOptional]
            [FieldNullValue(null)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            [JsonProperty("act", Order = 6)]
            public DateTime? Actualizacion { get; set; }

            [FieldHidden]
            [JsonProperty("items", Order = 7)]
            public List<T> Items { get; set; }

            public string ToJson(Formatting formatting = 0)
            {
                // configuracion json para la serializacion
                var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy" };
                return JsonConvert.SerializeObject(this, formatting, conf);
            }
        }

        /// <summary>
        /// constructor
        /// </summary>
        public LayoutContext()
        {
            this.contextBase = new LayoutContextBase();
            this.Items = new List<T>();
        }

        /// <summary>
        /// 
        /// </summary>
        private LayoutContextBase contextBase { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<T> Items
        {
            get
            {
                return this.contextBase.Items;
            }
            set
            {
                this.contextBase.Items = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="archivo"></param>
        /// <returns></returns>
        public bool Importar(string archivo)
        {
            FileInfo fileExists = new FileInfo(archivo);
            if (fileExists.Exists)
            {
                var engine = new MasterDetailEngine(typeof(LayoutContextBase), typeof(T)) { Encoding = System.Text.Encoding.UTF8 };
                engine.RecordSelector = new MasterDetailSelector(this.Selector);
                var res = engine.ReadFile(fileExists.FullName);
                if (res != null)
                {
                    LayoutContextBase @base = res[0].Master as LayoutContextBase;
                    this.contextBase = @base;
                    this.contextBase.Version = this.contextBase.Version.Replace("@", "");
                    // sino existe fecha de actualización, se coloca la fecha actual
                    if (this.contextBase.Actualizacion == null)
                        this.contextBase.Actualizacion = DateTime.Now;

                    for (int i = 0; i < res[0].Details.Length; i++)
                    {
                        this.Items.Add((T)res[0].Details[i]);
                    }
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="archivo"></param>
        /// <returns>verdadero si el archivo es creado correctamente</returns>
        public bool Save(string archivo)
        {
            using (StreamWriter streamWriter = new StreamWriter(archivo, false))
            {
                streamWriter.Write(this.contextBase.ToJson());
                streamWriter.Close();
                streamWriter.Dispose();
            }
            return true;
        }

        private RecordAction Selector(string record)
        {
            if (record[0].ToString().StartsWith("@"))
                return RecordAction.Master;
            else
                return RecordAction.Detail;
        }

    }
}
