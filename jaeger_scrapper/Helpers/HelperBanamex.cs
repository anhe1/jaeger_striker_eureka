﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using FileHelpers.MasterDetail;
using Jaeger.Layout.Entities;
using Jaeger.Layout.Entities.Banamex;
using Jaeger.Layout.Interface;
using FileHelpers;

namespace Jaeger.Layout.Helpers
{
    public class HelperBanamex : IHelperEstadoCuenta 
    {
        /// <summary>
        /// importar estado de cuenta banamex del reporte general
        /// </summary>
        public BancoEstadoCuenta EstadoCuenta(string archivo)
        {
            var engine = new MasterDetailEngine<BanamexEstadoCuenta, Jaeger.Layout.Entities.Banamex.BanamexEstadoCuenta.BanamexEstadoCuentaMovimiento>(new MasterDetailSelector(this.SelectorMovimiento));
            MasterDetails<BanamexEstadoCuenta, Jaeger.Layout.Entities.Banamex.BanamexEstadoCuenta.BanamexEstadoCuentaMovimiento>[] coleccion = engine.ReadFile(archivo);

            if (coleccion != null)
            {
                BancoEstadoCuenta response = new BancoEstadoCuenta();
                response.Fecha = coleccion[0].Master.Fecha;
                response.ChequesExentos = coleccion[0].Master.ChequesExentos;
                response.ChequesGirados = coleccion[0].Master.ChequesGirados;
                response.TotalDespositos = coleccion[0].Master.Despositos;
                response.DiasTrascurridosAnual = coleccion[0].Master.DiasTrascurridosAnual;
                response.DiasTrascurridosPeriodo = coleccion[0].Master.DiasTrascurridosPeriodo;
                response.FechaPeriodo = coleccion[0].Master.FechaPeriodo;
                response.NoCliente = coleccion[0].Master.NoCliente;
                response.NoCuenta = coleccion[0].Master.NoCuenta;
                response.Nombre = coleccion[0].Master.Nombre;
                response.NumDepositos = coleccion[0].Master.NumDepositos;
                response.NumRetiros = coleccion[0].Master.NumRetiros;
                response.RazonSocial = coleccion[0].Master.RazonSocial;
                response.SaldoAl = coleccion[0].Master.SaldoAl;
                response.SaldoAnterior = coleccion[0].Master.SaldoAnterior;
                response.SaldoPromedioAnual = coleccion[0].Master.SaldoPromedioAnual;
                response.SaldoPromedioPeriodo = coleccion[0].Master.SaldoPromedioPeriodo;
                response.Sucursal = coleccion[0].Master.Sucursal;
                response.Tipo = coleccion[0].Master.Tipo;
                response.TotalRetiros = coleccion[0].Master.TotalRetiros;
                response.Usuario = coleccion[0].Master.Usuario;
                response.Movimientos = new System.ComponentModel.BindingList<BancoEstadoCuentaMovimiento>();
                
                // movimientos
                for (int i = 0; i < coleccion[0].Details.Length; i++)
                {
                    BancoEstadoCuentaMovimiento item = new BancoEstadoCuentaMovimiento();
                    item.Deposito = coleccion[0].Details[i].Deposito;
                    item.Fecha = coleccion[0].Details[i].Fecha;
                    item.Retiro = coleccion[0].Details[i].Retiro;
                    item.Descripcion = coleccion[0].Details[i].Concepto.Descripcion;
                    item.NoAutorizacion = coleccion[0].Details[i].Concepto.NoAutorizacion;
                    item.Tipo = coleccion[0].Details[i].Concepto.Tipo;
                    item.Referencia = coleccion[0].Details[i].Concepto.Field2;
                    response.Movimientos.Add(item);
                }
                return response;
            }

            return null;
        }

        private RecordAction SelectorMovimiento(string record)
        {
            if (record.Length > 200)
                return RecordAction.Master;
            else
                return RecordAction.Detail;
        }

        public bool ExportarCSV(string archivo, List<Impresores> objetos)
        {
            var engine = new FileHelperEngine<Impresores>();

            engine.WriteFile(archivo, objetos);

            return File.Exists(archivo);
        }
    }
}
