﻿using System;
using System.Linq;
using FileHelpers;
using Jaeger.Layout.Entities.Bancomer;

namespace Jaeger.Layout.Helpers
{
    public class HelperBancomer
    {
        public BancomerNomina[] Importar(string archivo)
        {
            var engine = new FileHelperEngine<BancomerNomina>();

            engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;

            BancomerNomina[] res = engine.ReadFile(archivo);

            if (engine.ErrorManager.ErrorCount > 0)
                engine.ErrorManager.SaveErrors("Errors.txt");

            return res;
        }

        public bool Exportar(BancomerNomina[] data, string outfile)
        {
            var engine = new FileHelperEngine<BancomerNomina>();
            engine.WriteFile(outfile, data);
            return true;
        }
    }
}
