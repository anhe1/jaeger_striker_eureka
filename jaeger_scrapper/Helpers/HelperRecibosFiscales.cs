﻿using System;
using FileHelpers;
using Jaeger.Layout.Entities.RF;

namespace Jaeger.Layout.Helpers {
    public class HelperRecibosFiscales
    {
        public bool Exportar(Partida2[] arr, string archivo)
        {
            try
            {
                var engine = new FileHelperEngine<Partida2>();
                engine.WriteFile(archivo, arr);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
