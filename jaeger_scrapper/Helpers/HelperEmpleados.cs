﻿using System;
using System.Linq;
using System.Text;
using FileHelpers;
using Jaeger.Layout.Entities.Nomina;

namespace Jaeger.Layout.Helpers
{
    public class HelperEmpleados
    {
        public HelperEmpleados()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="archivo"></param>
        /// <returns>Array de objetos de LayOutEmpleado</returns>
        public LayOutEmpleado[] Importar(string archivo)
        {
            var engine = new FileHelperEngine(typeof(LayOutEmpleado));
            engine.Encoding = Encoding.UTF8;
            engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;

            var response = engine.ReadFile(archivo);

            if (engine.ErrorManager.ErrorCount > 0)
                engine.ErrorManager.SaveErrors(@"C:\Jaeger\Jaeger.Log\Errors.txt");

            if (response != null)
            {
                return (LayOutEmpleado[])response;
            }

            return null;
        }
    }
}