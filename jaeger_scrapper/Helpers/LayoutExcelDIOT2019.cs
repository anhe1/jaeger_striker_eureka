﻿using Jaeger.Layout.Entities.SAT;
using System.Collections.Generic;

namespace Jaeger.Layout.Helpers
{
    public class LayoutExcelDIOT2019 : LayoutExcelCommon
    {
        public List<LayoutDIOT> Lista { get; set; }

        public void Datos(string hoja)
        {
            this.Lista = new List<LayoutDIOT>();
            foreach (System.Data.DataRow item in this.TableCollection[hoja].Rows)
            {
                if (item.ItemArray[0].ToString() != "" | item.ItemArray[1].ToString() != "")
                {
                    var linea = new LayoutDIOT();
                    linea.TipoTercero = item.ItemArray[0].ToString();
                    linea.TipoOperacion = item.ItemArray[2].ToString();
                    linea.RFC = item.ItemArray[4].ToString();
                    linea.NumeroIdFiscal = item.ItemArray[5].ToString();
                    linea.NombreDelExtranjero = item.ItemArray[6].ToString();
                    linea.PaisResidencia = item.ItemArray[7].ToString();
                    linea.Nacionalidad = item.ItemArray[8].ToString();
                    linea.Field_008 = item.ItemArray[11].ToString().Replace(",", "");
                    linea.Field_009 = item.ItemArray[12].ToString().Replace(",", "");
                    linea.Field_010 = item.ItemArray[13].ToString().Replace(",", "");
                    string temp11 = item.ItemArray[14].ToString().Replace(",", "");
                    if (temp11.Trim() == "0")
                        linea.Field_011 = "";
                    else
                        linea.Field_011 = temp11;
                    linea.Field_012 = item.ItemArray[15].ToString().Replace(",", "");
                    if (linea.Field_012.Trim() == "0")
                        linea.Field_012 = "";
                    linea.Field_013 = item.ItemArray[16].ToString().Replace(",", "");
                    if (linea.Field_013.Trim() == "0")
                        linea.Field_013 = "";
                    linea.Field_014 = item.ItemArray[17].ToString().Replace(",", "");
                    if (linea.Field_014.Trim() == "0")
                        linea.Field_014 = "";
                    linea.Field_015 = item.ItemArray[18].ToString().Replace(",", "");
                    if (linea.Field_015.Trim() == "0")
                        linea.Field_015 = "";
                    linea.Field_016 = item.ItemArray[19].ToString().Replace(",", "");
                    if (linea.Field_016.Trim() == "0")
                        linea.Field_016 = "";
                    linea.Field_017 = item.ItemArray[20].ToString().Replace(",", "");
                    if (linea.Field_017.Trim() == "0")
                        linea.Field_017 = "";
                    linea.Field_018 = item.ItemArray[21].ToString().Replace(",", "");
                    if (linea.Field_018.Trim() == "0")
                        linea.Field_018 = "";
                    linea.Field_019 = item.ItemArray[22].ToString().Replace(",", "");
                    if (linea.Field_019.Trim() == "0")
                        linea.Field_019 = "";
                    linea.Field_020 = item.ItemArray[23].ToString().Replace(",", "");
                    if (linea.Field_020.Trim() == "0")
                        linea.Field_020 = "";
                    linea.Field_021 = item.ItemArray[24].ToString().Replace(",", "");
                    if (linea.Field_021.Trim() == "0")
                        linea.Field_021 = "";
                    linea.Field_022 = item.ItemArray[25].ToString().Replace(",", "");
                    if (linea.Field_022.Trim() == "0")
                        linea.Field_022 = "";
                    linea.Field_023 = item.ItemArray[26].ToString().Replace(",", "");
                    if (linea.Field_023.Trim() == "0")
                        linea.Field_023 = "";
                    linea.Field_024 = item.ItemArray[29].ToString().Replace(",", "");
                    
                    Lista.Add(linea);
                }
            }
        }

        public void Crear(string destino)
        {
            var procesar = new LayoutDIOT2019();
            procesar.Exportar(destino, this.Lista);
        }
    }
}
