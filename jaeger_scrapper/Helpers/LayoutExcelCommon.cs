﻿using System.ComponentModel;
using System.IO;
using System.Data;
using Jaeger.Layout.Entities;
using ExcelDataReader;

namespace Jaeger.Layout.Helpers {
    public class LayoutExcelCommon {
        #region declaraciones

        private FileInfo fileInfo;
        private DataTableCollection tableCollection;

        #endregion

        #region propiedades

        public FileInfo InfoFile { get; set; }

        public string ExcelFile {
            get {
                return this.fileInfo.FullName;
            }
            set {
                this.fileInfo = new FileInfo(value);
            }
        }

        public BindingList<ExcelSheet> Sheets { get; set; }

        public DataTableCollection TableCollection;

        #endregion

        public void Reader() {
            if (this.InfoFile.Exists == false)
                return;

            using (var stream = File.Open(this.InfoFile.FullName, FileMode.Open, FileAccess.Read)) {
                using (IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream)) {
                    DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration() {
                        ConfigureDataTable = (_) => new ExcelDataTableConfiguration() { UseHeaderRow = true }
                    });

                    this.TableCollection = result.Tables;
                    // agregar nombre de las hojas
                    this.Sheets = new BindingList<ExcelSheet>();
                    foreach (DataTable table in this.TableCollection)
                        this.Sheets.Add(new ExcelSheet(table.TableName));
                }
            }
        }
    }
}
