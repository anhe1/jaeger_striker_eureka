﻿using System.ComponentModel;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Aplication.Repositorio.Contracts {
    public interface IComprobanteFiscalService {
        BindingList<ComprobanteFiscalDetailModel> GetList(int month, int year = 0, string efecto = "Todos", string tipo = "Recibidos");

        ComprobanteFiscalDetailModel Serializar(ComprobanteFiscalDetailModel model);

        ComprobanteFiscalDetailModel Save(ComprobanteFiscalDetailModel model);

        ComprobanteFiscalDetailModel Save(string version, string cfd);

        bool UpdateEstado(string idDocumento, string estado);

        void Test(BindingList<ComprobanteFiscalDetailModel> vales);
    }
}
