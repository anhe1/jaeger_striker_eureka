﻿using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Aplication.Repositorio.Contracts {
    public interface IManagerService {
        bool Load(string fileName);

        bool OpenSession { get; set; }

        string FileRepositorio { get; set; }

        IRepositorioService GetRepositorio();

        IComprobanteFiscalService GetComprobantes();

        IContribuyentesService GetContribuyentes();

        EmpresaModel GetEmpresa();

        VersionModel GetVersion();
    }
}
