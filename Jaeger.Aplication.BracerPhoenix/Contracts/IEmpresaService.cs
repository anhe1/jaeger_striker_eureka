﻿using System.ComponentModel;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Aplication.Repositorio.Contracts {
    public interface IEmpresaService {
        EmpresaModel GetEmpresa();
        EmpresaModel Save(EmpresaModel model);

    }
}
