﻿using System.Collections.Generic;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Aplication.Repositorio.Contracts {
    public interface IMetaDataService {
        List<RepositorioMeta> GetComprobanteMETA(string fileName);

        List<RetencionMeta> GetRetencionMETA(string fileName);

        bool SaveExcelAs(string fileSourceCSV, string fileDestinyXLSX);
    }
}
