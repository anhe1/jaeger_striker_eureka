﻿using Jaeger.Domain.Repositorio.Entities;
using System.ComponentModel;

namespace Jaeger.Aplication.Repositorio.Contracts {
    public interface IContribuyentesService {
        BindingList<ContribuyenteModel> GetList(int relacion);
        void Test(string tipo);
    }
}
