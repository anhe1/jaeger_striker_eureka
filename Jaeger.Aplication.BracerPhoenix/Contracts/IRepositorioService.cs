﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Repositorio.Entities;
using Jaeger.Repositorio.Entities;

namespace Jaeger.Aplication.Repositorio.Contracts {
    public interface IRepositorioService {

        bool CreateRepository();

        /// <summary>
        /// obtener lista de tipos de comprobantes
        /// </summary>
        //List<TipoComprobanteModel> GetTipoComprobante();

        BindingList<ComprobanteFiscalExtended> GetList(int month, int year = 0, string efecto = "Todos", string tipo = "Recibidos");

        //void SaveResults(List<DescargaResponse> resultados);

        void Save(List<ComprobanteFiscalExtended> resultados);

        /// <summary>
        /// obtener xml en formato string 
        /// </summary>
        string GetXmlString(ComprobanteFiscalExtended model);

        byte[] GetXmlByte(ComprobanteFiscalExtended model);

        /// <summary>
        /// obtener el estado del comprobante fiscal mediante el servicio SAT
        /// </summary>
        /// <param name="emisor">RFC del emisor del comprobante</param>
        /// <param name="receptor">RFC del receptor del comprobante</param>
        /// <param name="total">total del comprobante</param>
        /// <param name="idDocumento">folio fiscal del comprobante (uuid)</param>
        /// <returns>string que representa el estado del comprobante</returns>
        string EstadoSAT(string emisor, string receptor, decimal total, string idDocumento);

        ComprobanteFiscalExtended Serializar(ComprobanteFiscalExtended model);

        ComprobanteFiscalExtended Reader(string fileXml);

        ComprobanteFiscalExtended Update(ComprobanteFiscalExtended seleccionado);

        #region versiones
        /// <summary>
        /// obtener version del repositorio
        /// </summary>
        VersionModel GetVersion();
        #endregion
    }
}
