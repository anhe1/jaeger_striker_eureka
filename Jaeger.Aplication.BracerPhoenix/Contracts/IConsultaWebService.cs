﻿using System.ComponentModel;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Aplication.Repositorio.Contracts {
    public interface IConsultaWebService {
        SolicitudModel Save(SolicitudModel solicitudModel);

        BindingList<SolicitudModel> GetAll();
    }
}
