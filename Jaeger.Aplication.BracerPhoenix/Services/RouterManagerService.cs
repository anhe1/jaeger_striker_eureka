﻿using System;
using System.Linq;
using System.IO;
using Jaeger.Aplication.Repositorio.ValueObjects;

namespace Jaeger.Aplication.Repositorio.Services {
    public static class RouterManagerService {
        public static string DefaulPath = @"C:\AdminCFD";
        public static string Prefix = "AdminCFD";

        static RouterManagerService() {
            RouterManagerService.DefaulPath = Path.Combine(@"C:\", RouterManagerService.Prefix);
        }

        public static string GetPath(PathEnum path, string fileName = "") {
            var subFolder = Path.Combine(DefaulPath, Enum.GetName(typeof(PathEnum), path));
            var raiz = Path.Combine(DefaulPath, subFolder);

            if (!Directory.Exists(raiz)) {
                try {
                    Directory.CreateDirectory(raiz);
                } catch (Exception ex) {
                    Console.WriteLine(ex.ToString());
                } 
            }

            if (!string.IsNullOrEmpty(fileName)) {
                return Path.Combine(raiz, fileName);
            }
            return raiz;
        }

        public static void CreatePaths() {
            if (!Directory.Exists(DefaulPath)) {
                try {
                    Directory.CreateDirectory(DefaulPath);
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }

            if (Directory.Exists(DefaulPath)) {
                foreach (var item in Enum.GetValues(typeof(PathEnum))) {
                    try {
                        if (!Directory.Exists(GetPath((PathEnum)item))) {
                            Directory.CreateDirectory(GetPath((PathEnum)item));
                        }
                    } catch (Exception ex) {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        public static void CreatePathsFull() {
            var subFolders = Enum.GetNames(typeof(PathEnum)).Select(x => Path.Combine(DefaulPath, x.ToString())).ToList();

            if (!Directory.Exists(DefaulPath)) {
                try {
                    Directory.CreateDirectory(DefaulPath);
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }

            if (Directory.Exists(DefaulPath)) {
                foreach (var folder in subFolders) {
                    try {
                        if (!Directory.Exists(folder)) {
                            Directory.CreateDirectory(folder);
                        }
                    } catch (Exception ex) {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        public static bool Exists(string fileName) {
            return File.Exists(fileName);
        }
    }
}
