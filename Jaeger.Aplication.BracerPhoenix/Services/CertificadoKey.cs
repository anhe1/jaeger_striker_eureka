﻿using Chilkat;
using Jaeger.Domain.Base.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Jaeger.Aplication.Repositorio.Services {
    public class CertificadoKey  {
        private string _path = "";

        private string _password = "";

        private PrivateKey ChilkatKey;

        public CertificadoKey(string path, string password) {
            this._path = path;
            this._password = password;
            this.ChilkatKey = new PrivateKey();
            this.loadKey();
        }

        public string getFolioModulo() {
            XElement xElement = XElement.Parse(this.ChilkatKey.GetXml());
            return xElement.Element("Modulus").Value.ToString();
        }

        public PrivateKey getPrivateKey() {
            return this.ChilkatKey;
        }

        private void loadKey() {
            if (!this.ChilkatKey.LoadPkcs8EncryptedFile(this._path, this._password)) {
                throw new Exception("Contraseña incorrecta");
            }
        }
    }

    public class CertificadoCER  {
        private readonly string path;

        private Cert ChilkatCer;

        private string numeroCerficado = "";

        public CertificadoCER(string path) {
            this.path = path;
            this.ChilkatCer = new Cert();
            this.loadCer();
        }

        public CertificadoCER() {
            this.ChilkatCer = new Cert();
        }

        private string extractRFCSuject() {
            string subjectDN = this.ChilkatCer.SubjectDN;
            string[] strArrays = subjectDN.Split(new char[] { ',' });
            string str = "";
            string[] strArrays1 = strArrays;
            for (int i = 0; i < (int)strArrays1.Length; i++) {
                string str1 = strArrays1[i];
                if (str1.Contains("OID.2.5.4.45")) {
                    str = str1.Trim();
                }
            }
            return str.Substring(13, 13).Trim();
        }

        private string extraerSerialNumber() {
            byte[] bytes = Encoding.ASCII.GetBytes(this.ChilkatCer.SerialNumber);
            Encoding.UTF8.GetString(bytes);
            BigInteger bigInteger = BigInteger.Parse(this.ChilkatCer.SerialNumber, NumberStyles.HexNumber);
            return bigInteger.ToString();
        }

        public Cert getCer() {
            return this.ChilkatCer;
        }

        public CertChain getCertChain() {
            return this.ChilkatCer.GetCertChain();
        }

        public DateTime getFecheInicio() {
            return this.ChilkatCer.ValidFrom;
        }

        public DateTime getFinVigencia() {
            return this.ChilkatCer.ValidTo;
        }

        public string getFolioModulo() {
            XElement xElement = XElement.Parse(this.ChilkatCer.ExportPublicKey().GetXml());
            return xElement.Element("Modulus").Value.ToString();
        }

        public string getNumeroSerie() {
            return this.extraerSerialNumber();
        }

        public string getPEMToBase64() {
            return this.ChilkatCer.GetEncoded();
        }

        public string getSujectRFC() {
            return this.extractRFCSuject();
        }

        public bool isCief() {
            bool flag = false;
            if (this.ChilkatCer.SubjectOU.Length != 0) {
                throw new CertException("El certificado que intentas cargar es una Key, por favor cargar la e.Firma");
            }
            return flag;
        }

        public void isExpirado() {
            if (this.ChilkatCer.Expired) {
                throw new CertException("Certificado ¡EXPIRADO! por favor cargue otro certificado");
            }
        }

        public void loadCer() {
            if (!this.ChilkatCer.LoadFromFile(this.path)) {
                throw new CertException(this.ChilkatCer.LastErrorText);
            }
        }

        public void loadCerPem(string pem) {
            if (!this.ChilkatCer.LoadPem(pem)) {
                throw new CertException(this.ChilkatCer.LastErrorText);
            }
        }
    }

    public class CertException : Exception {
        public CertException() {
        }

        public CertException(string message) : base(message) {
        }

        public CertException(string message, Exception innerException) : base(message, innerException) {
        }
    }

    public class EFirma  {
        private static string EXTENSION_KEY;

        private static string EXTENSION_PFX;

        private static string EXTENSION_CER;

        private string _pathCert;

        private string _pathKey;

        private readonly string _pathCertificadoPFX;

        private readonly string _pathPFX;

        private readonly string _rfcEmpresa;

        private readonly string _password;

        private CertificadoCER _cerfificadoCer;

        private CertificadoKey _certificadoKey;

        static EFirma() {
            EFirma.EXTENSION_KEY = ".key";
            EFirma.EXTENSION_PFX = ".pfx";
            EFirma.EXTENSION_CER = ".cer";
        }

        public EFirma(string pathCert, string pathKey, string password, string pathCertificadoPFX, string pathPFX = null, string rfcEmpresa = null, CertificadoCER cerfificadoCer = null, CertificadoKey certificadoKey = null) {
            this._pathCert = pathCert;
            this._pathKey = pathKey;
            this._pathCertificadoPFX = pathCertificadoPFX;
            this._pathPFX = pathPFX;
            this._rfcEmpresa = rfcEmpresa;
            this._password = password;
            this._cerfificadoCer = cerfificadoCer;
            this._certificadoKey = certificadoKey;
            this._cerfificadoCer = cerfificadoCer;
            this._certificadoKey = certificadoKey;
        }

        private void isPathCert() {
            if ((this._pathCert.Contains(EFirma.EXTENSION_CER) ? false : !this._pathCert.Contains(".CER"))) {
                throw new CertException("El archivo que está tratando de cargar no es un .cer");
            }
        }

        private void isPathKey() {
            if ((this._pathKey.Contains(EFirma.EXTENSION_KEY) ? false : !this._pathKey.Contains(".KEY"))) {
                throw new CertException("El archivo que está tratando de cargar no es un .key");
            }
        }

        private void isPathPFX() {
            if ((this._pathPFX.Contains(EFirma.EXTENSION_PFX) ? false : !this._pathPFX.Contains(".PFX"))) {
                throw new CertException("El archivo que está tratando de cargar no es un .pfx");
            }
        }

        public EmpresaFIELModel load() {
            EmpresaFIELModel empresaFIELModel = new EmpresaFIELModel();
            string str = "";
            CerModel cerModel = null;
            KeyModel keyModel = null;
            string str1 = "";
            string sujectRFC = "";
            string numeroCertificado = "";
            try {
                if (this._password != "") {
                    str = this._password;
                }
                if (!string.IsNullOrWhiteSpace(this._pathPFX)) {
                    this.isPathPFX();
                    Pfx pfx = new Pfx();
                    if (!pfx.LoadPfxFile(this._pathPFX, str)) {
                        throw new Exception("Contraseña incorrecta");
                    }
                    string pem = pfx.ToPem();
                    str1 = this._pathPFX;
                    sujectRFC = this._rfcEmpresa;
                    this._cerfificadoCer = new CertificadoCER();
                    this._cerfificadoCer.loadCerPem(pem);
                    sujectRFC = this._cerfificadoCer.getSujectRFC();
                    this._pathCert = "";
                    this._pathKey = "";
                } else {
                    this.isPathCert();
                    this.isPathKey();
                    this._cerfificadoCer = new CertificadoCER(this._pathCert);
                    this._certificadoKey = new CertificadoKey(this._pathKey, str);
                    cerModel = new CerModel() {
                        FechaInicio = this._cerfificadoCer.getFecheInicio(),
                        FechaFinal = this._cerfificadoCer.getFinVigencia(),
                        Folio = this._cerfificadoCer.getFolioModulo(),
                        NumeroCertificado = this._cerfificadoCer.getNumeroSerie(),
                        RFC = this._cerfificadoCer.getSujectRFC()
                    };
                    keyModel = new KeyModel() {
                        Password = this._password,
                        Folio = this._certificadoKey.getFolioModulo(),
                        NumeroCertificado = ""
                    };
                    str1 = Path.Combine(this._pathCertificadoPFX, string.Concat("Chilkat_", this._cerfificadoCer.getSujectRFC(), ".pfx"));
                    Pfx pfx1 = new Pfx();
                    pfx1.AddCert(this._cerfificadoCer.getCer(), true);
                    pfx1.AddPrivateKey(this._certificadoKey.getPrivateKey(), this._cerfificadoCer.getCertChain());
                    pfx1.ToFile(str, str1);
                    pfx1.ToPem();
                    sujectRFC = cerModel.RFC;
                    numeroCertificado = cerModel.NumeroCertificado;
                }
                empresaFIELModel.PathCerFull = this._pathCert;
                empresaFIELModel.PathKeyFull = this._pathKey;
                empresaFIELModel.Password = this._password;
                empresaFIELModel.Cer = cerModel;
                empresaFIELModel.Key = keyModel;
                empresaFIELModel.NumeroCertificado = numeroCertificado;
                empresaFIELModel.PathPxfFull = str1;
                empresaFIELModel.RFC = sujectRFC;
            } catch (Exception exception) {
                throw new Exception(exception.Message);
            }
            return empresaFIELModel;
        }
    }

    public class EmpresaFIELModel {
        public CerModel Cer {
            get;
            set;
        }

        public string Credencial {
            get;
            set;
        }

        public string IdEmpresaFIEL {
            get;
            set;
        }

        public KeyModel Key {
            get;
            set;
        }

        public string NumeroCertificado {
            get;
            set;
        }

        public string Password {
            get;
            set;
        }

        public string PathCerFull {
            get;
            set;
        }

        public string PathKeyFull {
            get;
            set;
        }

        public string PathPxfFull {
            get;
            set;
        }

        public string RazonSocial {
            get;
            set;
        }

        public string RFC {
            get;
            set;
        }

        public string RFCEmpresa {
            get;
            set;
        }

        public EmpresaFIELModel() {
        }
    }

    public class CerModel {
        public DateTime FechaFinal {
            get;
            set;
        }

        public DateTime FechaInicio {
            get;
            set;
        }

        public string Folio {
            get;
            set;
        }

        public string IdFirma {
            get;
            set;
        }

        public string NumeroCertificado {
            get;
            set;
        }

        public string RFC {
            get;
            set;
        }

        public string UnencryptedPEMCER {
            get;
            set;
        }

        public CerModel() {
        }
    }

    public class KeyModel {
        public string Folio {
            get;
            set;
        }

        public string IdFirma {
            get;
            set;
        }

        public string NumeroCertificado {
            get;
            set;
        }

        public string Password {
            get;
            set;
        }

        public string Pem {
            get;
            set;
        }

        public KeyModel() {
        }
    }
}
