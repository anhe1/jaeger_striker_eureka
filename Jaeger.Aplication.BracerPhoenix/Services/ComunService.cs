﻿using System.Collections.Generic;
using System.IO;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Aplication.Repositorio.Services {
    public static class ComunService {

        /// <summary>
        /// obtener listado de tipos de comprobantes
        /// </summary>
        public static List<SubTipoComprobanteModel> GetSubTipoComprobante() {
            var result = new List<SubTipoComprobanteModel> {
                new SubTipoComprobanteModel(1, "Emitidos"),
                new SubTipoComprobanteModel(2, "Recibidos")
            };
            return result;
        }

        /// <summary>
        /// Tipo de descarga Metadata = 0 | CFDI = 1
        /// </summary>
        public static List<TipoDescargaModel> GetTipoDescarga() {
            return new List<TipoDescargaModel> {
                new TipoDescargaModel(0, "Metadata"),
                new TipoDescargaModel(1, "CFDI")
            };
        }

        /// <summary>
        /// obtener lista de tipos de comprobantes
        /// </summary>
        public static List<TipoComprobanteModel> GetTipoComprobante() {
            var result = new List<TipoComprobanteModel> {
                new TipoComprobanteModel(0, "Todos"),
                new TipoComprobanteModel(1, "Ingreso"),
                new TipoComprobanteModel(2, "Egreso"),
                new TipoComprobanteModel(3, "Traslado"),
                new TipoComprobanteModel(4, "Nómina"),
                new TipoComprobanteModel(5, "Pagos")
            };
            return result;
        }
    }
}
