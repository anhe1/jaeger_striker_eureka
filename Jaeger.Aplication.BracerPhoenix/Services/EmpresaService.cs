﻿using System;
using System.Linq;
using System.ComponentModel;
using Jaeger.Aplication.Repositorio.Contracts;
using Jaeger.Domain.Repositorio.Contracts;
using Jaeger.DataAccess.Repositorio.Repositories;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Aplication.Repositorio.Services {
    public class EmpresaService : IEmpresaService {
        protected ISqlConfiguracionRepository configuracionRepository;

        public EmpresaService() {
            this.configuracionRepository = new SQLiteConfiguracionRepository(new Domain.DataBase.Entities.DataBaseConfiguracion { Database = ConfiguracionService.Manager.FileRepositorio });
        }

        public EmpresaModel GetEmpresa() {
            var datos = this.configuracionRepository.GetCatagoria(Domain.Repositorio.ValueObjects.CategoriaEnum.Empresa);
            var response = new EmpresaModel();

            foreach (var item in datos) {
                if (!string.IsNullOrEmpty(item.Valor)) {
                    switch (item.Parametro) {
                        case "rfc":
                            response.RFC = item.Valor;
                            break;
                        case "razonsocial":
                            response.RazonSocial = item.Valor;
                            break;
                        default:
                            break;
                    }
                }
            }

            return response;
        }

        public EmpresaModel Save(EmpresaModel model) {
            throw new NotImplementedException();
        }
    }
}
