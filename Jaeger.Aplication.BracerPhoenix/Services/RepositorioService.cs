﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.IO;
using System.Xml;
using System.Collections;
using Jaeger.DataAccess.Repositorio.Repositories;
using Jaeger.Domain.Repositorio.Contracts;
using Jaeger.Domain.Repositorio.Entities;
using Jaeger.Repositorio.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Aplication.Repositorio.Contracts;

namespace Jaeger.Aplication.Repositorio.Services {
    public class RepositorioService : IRepositorioService {
        protected ISqlRepositorioRepository repository;
        protected ISqlVersionRepository versionRepository;
        protected SAT.Consulta.Interfaces.IStatus serviceSAT = new SAT.Consulta.Services.Status();

        public RepositorioService(string fileDataBase) {
            this.repository = new SQLiteRepositorioRepository(new DataBaseConfiguracion { Database = fileDataBase });
            this.versionRepository = new SQLiteVersionRepository(new DataBaseConfiguracion { Database = fileDataBase });
        }
        
        public bool CreateRepository() {
            if (this.repository.CreateDB())
                return this.repository.CreateTables();
            return false;
        }

        public BindingList<ComprobanteFiscalExtended> GetList(int month, int year = 0, string efecto = "Todos", string tipo = "Recibidos") {
            return new BindingList<ComprobanteFiscalExtended>(this.repository.GetList(month, year, efecto, tipo).ToList());
        }

        //public void SaveResults(List<DescargaResponse> resultados) {
        //    var l = new List<ComprobanteFiscalExtended>();
        //    foreach (var item in resultados) {
        //        l.Add(new ComprobanteFiscalExtended {
        //            Emisor = item.Emisor,
        //            ComprobadoText = item.ComprobadoText,
        //            Efecto = item.Efecto,
        //            EmisorRFC = item.EmisorRFC,
        //            Estado = item.Estado,
        //            FechaCerfificacion = item.FechaCerfificacion,
        //            FechaEmision = item.FechaEmision,
        //            FechaStatusCancela = item.FechaStatusCancela,
        //            FechaNuevo = item.FechaNuevo,
        //            FechaValidacion = item.FechaValidacion,
        //            IdDocumento = item.IdDocumento,
        //            PACCertifica = item.PACCertifica,
        //            Receptor = item.Receptor,
        //            ReceptorRFC = item.ReceptorRFC,
        //            StatusCancelacion = item.StatusCancelacion,
        //            StatusProcesoCancelar = item.StatusProcesoCancelar,
        //            XmlContentB64 = item.XmlContentB64,
        //            PathXML = item.PathXML,
        //            Tipo = item.Tipo,
        //            Result = item.Result,
        //            Total = item.Total,
        //            PathPDF = item.PathPDF
        //        });
        //    }
        //    this.Save(l);
        //}

        public void Save(List<ComprobanteFiscalExtended> models) {
            this.repository.SaveResults(models);
        }

        public ComprobanteFiscalExtended Update(ComprobanteFiscalExtended item) {
            return this.repository.Update(item);
        }

        public string GetXmlString(ComprobanteFiscalExtended model) {
            if (model != null) {
                if (model.XmlContentB64 != null) {
                    try {
                        var base64EncodedBytes = Convert.FromBase64String(model.XmlContentB64);
                        return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                    } catch (Exception e) {
                        Console.WriteLine(e.Message);
                    }
                }
            }
            return string.Empty;
        }

        public byte[] GetXmlByte(ComprobanteFiscalExtended model) {
            if (model != null) {
                if (model.XmlContentB64 != null) {
                    try {
                        var base64EncodedBytes = Convert.FromBase64String(model.XmlContentB64);
                        return base64EncodedBytes;
                    } catch (Exception e) {
                        Console.WriteLine(e.Message);
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// obtener el estado del comprobante fiscal mediante el servicio SAT
        /// </summary>
        /// <param name="emisor">RFC del emisor del comprobante</param>
        /// <param name="receptor">RFC del receptor del comprobante</param>
        /// <param name="total">total del comprobante</param>
        /// <param name="idDocumento">folio fiscal del comprobante (uuid)</param>
        /// <returns>string que representa el estado del comprobante</returns>
        public string EstadoSAT(string emisor, string receptor, decimal total, string idDocumento) {
            //var response = this.serviceSAT.GetStatus(emisor, receptor, total, idDocumento);
            var response = this.serviceSAT.Execute(
                SAT.Consulta.Request.Create()
                .WithEmisorRFC(emisor)
                .WithReceptorRFC(receptor)
                .WithTotal(total)
                .WithFolioFiscal(idDocumento)
                .Build());

            if (response != null) {
                this.repository.UpdateEstado(idDocumento, response.Estado);
                return response.Estado;
            }

            return "No Disponible!";
        }

        #region versiones
        /// <summary>
        /// obtener version del repositorio
        /// </summary>
        public VersionModel GetVersion() {
            return this.versionRepository.GetById();
        }
        #endregion

        public ComprobanteFiscalExtended Serializar(ComprobanteFiscalExtended model) {
            var encodedString = Convert.FromBase64String(model.XmlContentB64);

            // Put the byte array into a stream and rewind it to the beginning
            MemoryStream ms = new MemoryStream();
            ms.Write(encodedString, 0, encodedString.Length);
            ms.Seek(0, SeekOrigin.Begin);
            ms.Flush();
            ms.Position = 0;
            var _reader = XmlReader.Create(ms);

            try {
                while (_reader.Read()) {
                    try {
                        if (_reader.IsStartElement()) {
                            string name = _reader.Name;
                            if (!(name == null)) {
                                if (name == "cfdi:Comprobante") {
                                    model.Folio = Convert.ToString(_reader["Folio"]);
                                    model.SubTotal = Convert.ToDecimal(_reader["SubTotal"]);
                                    model.Descuento = Convert.ToDecimal(_reader["Descuento"]);
                                } else if (name == "cfdi:Emisor") {
                                } else if (name == "cfdi:Receptor") {
                                } else if (name == "cfdi:Concepto") {
                                } else if (name == "cfdi:Traslado") {
                                } else if (name == "cfdi:Retenido") {
                                } else if (name == "cfdi:Impuestos") {
                                    this.ReaderCfdi33Impuestos(ref model, _reader.ReadOuterXml());
                                } else if (name == "tfd:TimbreFiscalDigital") {
                                } else if (name == "pago10:Pago") {

                                } else if (name == "pago10:DoctoRelacionado") {

                                } else if (name == "cfdi:CfdiRelacionados") {

                                } else if (name == "cfdi:CfdiRelacionado") {

                                } else if (name == "nomina12:Nomina") {

                                } else if (name == "implocal:ImpuestosLocales") {

                                } else if (name == "valesdedespensa:ValesDeDespensa") {

                                } else if (name == "aerolineas:Aerolineas") {

                                }
                            }
                        }
                    } catch (Exception e) {
                        Console.WriteLine(string.Concat("ReaderCFDI33: ", e.Message));
                    }
                }
            } catch (Exception e) {
                Console.WriteLine(string.Concat("ReaderCFDI32: ", e.Message));
            }
            return model;
        }

        public void ReaderCfdi33Impuestos(ref ComprobanteFiscalExtended objeto, string inXml) {
            // 001 - ISR
            // 002 - IVA
            // 003 - IEPS
            objeto.RetencionIEPS = 0;
            objeto.RetencionISR = 0;
            objeto.RetencionIVA = 0;
            objeto.TrasladoIVA = 0;
            objeto.TrasladoIEPS = 0;

            if ((inXml.Contains("TotalImpuestosTrasladados") || inXml.Contains("TotalImpuestosRetenidos") ? true : false)) {
                XmlDocument xmlD = new XmlDocument();
                IEnumerator enumerator = null;
                xmlD.LoadXml(inXml);
                try {
                    enumerator = xmlD.SelectNodes("//*").GetEnumerator();
                    while (enumerator.MoveNext()) {
                        XmlElement current = (XmlElement)enumerator.Current;
                        try {
                            if (current.Name != "cfdi:Traslado") {
                                if (current.Name == "cfdi:Retencion") {
                                    if (current.GetAttribute("Impuesto").ToLower() == "002") {
                                        objeto.RetencionIVA = objeto.RetencionIVA + Convert.ToDecimal(current.GetAttribute("Importe"));
                                    } else if (current.GetAttribute("Impuesto").ToLower() == "001") {
                                        objeto.RetencionISR = objeto.RetencionISR + Convert.ToDecimal(current.GetAttribute("Importe"));
                                    } else if (current.GetAttribute("Impuesto").ToLower() == "003") {
                                        objeto.RetencionIEPS = objeto.RetencionIEPS + Convert.ToDecimal(current.GetAttribute("Importe"));
                                    }
                                }
                            } else if (current.GetAttribute("Impuesto").ToLower() == "002") {
                                objeto.TrasladoIVA = objeto.TrasladoIVA + Convert.ToDecimal(current.GetAttribute("Importe"));
                            } else if (current.GetAttribute("Impuesto").ToLower() == "001") {
                                objeto.TrasladoIEPS = objeto.TrasladoIEPS + Convert.ToDecimal(current.GetAttribute("Importe"));
                            }
                        } catch (Exception ex) {
                            Console.WriteLine(string.Concat("ReaderCFDI33Impuestos: ", ex.Message));
                        }
                    }
                } catch (Exception ex) {
                    Console.WriteLine(string.Concat("ReaderCFDI33Impuestos: ", ex.Message));
                }
            }
        }

        public ComprobanteFiscalExtended Reader(string fileXml) {
            var objeto = new ComprobanteFiscalExtended();
            XmlReader reader = XmlReader.Create(fileXml);

            try {
                while (reader.Read()) {
                    try {
                        if (reader.IsStartElement()) {
                            string name = reader.Name;
                            if (!(name == null)) {
                                if (name == "cfdi:Comprobante") {
                                    objeto.Efecto = TipoComprobante(reader["TipoDeComprobante"]);
                                    objeto.FechaEmision = Convert.ToDateTime(reader["Fecha"]);
                                    objeto.SubTotal = Convert.ToDecimal(reader["SubTotal"]);
                                    objeto.Descuento = Convert.ToDecimal(reader["Descuento"]);
                                    objeto.Total = Convert.ToDecimal(reader["Total"]);
                                } else if (name == "cfdi:Emisor") {
                                    objeto.EmisorRFC = reader["Rfc"];
                                    objeto.Emisor = reader["Nombre"];
                                } else if (name == "cfdi:Receptor") {
                                    objeto.ReceptorRFC = reader["Rfc"];
                                    objeto.Receptor = reader["Nombre"];
                                } else if (name == "cfdi:Concepto") {
                                } else if (name == "cfdi:Traslado") {
                                } else if (name == "cfdi:Retenido") {
                                } else if (name == "cfdi:Impuestos") {
                                    this.ReaderCfdi33Impuestos(ref objeto, reader.ReadOuterXml());
                                } else if (name == "tfd:TimbreFiscalDigital") {
                                    objeto.IdDocumento = reader["UUID"];
                                    objeto.FechaCerfificacion = Convert.ToDateTime(reader["FechaTimbrado"]);
                                    objeto.PACCertifica = reader["RfcProvCertif"];
                                }
                            }
                        }
                    } catch (Exception e) {
                        Console.WriteLine(string.Concat("ReaderCFDI33: ", e.Message));
                        //return this.ReaderSerializer(fileXml);
                    }
                }
            } catch (Exception e) {
                Console.WriteLine(string.Concat("ReaderCFDI32: ", e.Message));
                //return this.ReaderSerializer(fileXml);
            }

            return objeto;
        }

        private static string TipoComprobante(string clave) {
            if (clave.ToUpper().StartsWith("I")) {
                return "Ingreso";
            } else if (clave.ToUpper().StartsWith("E")) {
                return "Egreso";
            } else if (clave.ToUpper().StartsWith("N")) {
                return "Nomina";
            } else if (clave.ToUpper().StartsWith("T")) {
                return "Traslado";
            } else if (clave.ToUpper().StartsWith("P")) {
                return "Pagos";
            }
            return clave;
        }

        /// <summary>
        /// obtener lista de tipos de comprobantes
        /// </summary>
        public static List<TipoComprobanteModel> GetTipoComprobante() {
            var result = new List<TipoComprobanteModel> {
                new TipoComprobanteModel(0, "Todos"),
                new TipoComprobanteModel(1, "Ingreso"),
                new TipoComprobanteModel(2, "Egreso"),
                new TipoComprobanteModel(3, "Traslado"),
                new TipoComprobanteModel(4, "Nómina"),
                new TipoComprobanteModel(5, "Pagos")
            };
            return result;
        }
    }
}
