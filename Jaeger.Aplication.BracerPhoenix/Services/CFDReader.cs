﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Aplication.Repositorio.Beta.Services {
    public static class CFDReader {
        #region reader
        public static ComprobanteFiscalDetailModel Reader(string stringXml, ref ComprobanteFiscalDetailModel _response) {
            var encodedString = Convert.FromBase64String(stringXml);

            // Put the byte array into a stream and rewind it to the beginning
            MemoryStream ms = new MemoryStream();
            ms.Write(encodedString, 0, encodedString.Length);
            ms.Seek(0, SeekOrigin.Begin);
            ms.Flush();
            ms.Position = 0;
            var _reader = XmlReader.Create(ms);
            //var _response = new ComprobanteFiscalDetailModel();
            try {
                while (_reader.Read()) {
                    try {
                        if (_reader.IsStartElement()) {
                            if (_reader.Name != null) {
                                // solo para el tipo elemento
                                if (_reader.NodeType == XmlNodeType.Element) {
                                    // nombre del nodo
                                    string currentName = _reader.Name.ToLower();
                                    if (currentName == "cfdi:comprobante") {
                                        ReaderComprobante(ref _response, ref _reader);
                                    } else if (currentName == "cfdi:informacionglobal") {

                                    } else if (currentName == "cfdi:acuentaterceros") {

                                    } else if (currentName == "cfdi:emisor") {
                                        ReaderEmisor(ref _response, ref _reader);
                                    } else if (currentName == "cfdi:receptor") {
                                        ReaderReceptor(ref _response, ref _reader);
                                    } else if (currentName == "cfdi:concepto") {

                                    } else if (currentName == "cfdi:traslado") {

                                    } else if (currentName == "cfdi:retenido") {

                                    } else if (currentName == "cfdi:impuestos") {
                                        ReaderCfdi33Impuestos(ref _response, _reader.ReadOuterXml());
                                    } else if (currentName == "tfd:timbrefiscaldigital") {
                                        ReaderTimbreFiscal(ref _response, ref _reader);
                                    } else if (currentName == "pago20:pago" | currentName == "pago10:pago") {
                                        ReaderPagos(ref _response, ref _reader);
                                    } else if (currentName == "pago20:doctorelacionado") {

                                    } else if (currentName == "cfdi:cfdirelacionados") {

                                    } else if (currentName == "cfdi:cfdirelacionado") {

                                    } else if (currentName == "nomina12:nomina") {

                                    } else if (currentName == "implocal:impuestoslocales") {

                                    } else if (currentName == "valesdedespensa:valesdedespensa") {

                                    } else if (currentName == "aerolineas:aerolineas") {

                                    }
                                }
                            }
                        }
                    } catch (Exception exp) {
                        Console.WriteLine(string.Concat("CFDReader: ", exp.Message));
                        throw;
                    }
                }
            } catch (Exception exp) {
                Console.WriteLine(string.Concat("CFDReader: ", exp.Message));
                throw;
            }
            return _response;
        }

        /// <summary>
        /// obtener datos generales del comprobante
        /// </summary>
        public static void ReaderComprobante(ref ComprobanteFiscalDetailModel _response, ref XmlReader _reader) {
            for (int attInd = 0; attInd < _reader.AttributeCount; attInd++) {
                _reader.MoveToAttribute(attInd);
                Console.WriteLine(string.Format("{0} {1}", _reader.Name, _reader.Value));
                switch (_reader.Name.ToLower()) {
                    case "version":
                        _response.Version = ReaderString(_reader.Value);
                        break;
                    case "tipodecomprobante":
                        _response.Efecto = TipoComprobante(ReaderString(_reader.Value));
                        break;
                    case "folio":
                        _response.Folio = ReaderString(_reader.Value);
                        break;
                    case "serie":
                        _response.Serie = ReaderString(_reader.Value);
                        break;
                    case "metodopago":
                    case "metododepago":
                        _response.ClaveMetodoPago = ReaderString(_reader.Value);
                        break;
                    case "formapago":
                    case "formadeapgo":
                        _response.ClaveFormaPago = ReaderString(_reader.Value);
                        break;
                    case "condicionesdepago":
                        break;
                    case "confirmacion":
                        break;
                    case "moneda":
                        _response.ClaveMoneda = ReaderString(_reader.Value);
                        break;
                    case "fecha":
                        _response.FechaEmision = ReaderDateTime(_reader.Value);
                        break;
                    case "subtotal":
                        _response.SubTotal = Convert.ToDecimal(_reader.Value);
                        break;
                    case "descuento":
                        _response.Descuento = Convert.ToDecimal(_reader.Value);
                        break;
                    case "motivodescuento":
                        break;
                    case "numctapago":
                        break;
                    case "foliofiscalorig":
                        break;
                    case "seriefoliofiscalorig":
                        break;
                    case "fechafoliofiscalorig":
                        break;
                    case "montofoliofiscalorig":
                        break;
                    case "total":
                        _response.Total = Convert.ToDecimal(_reader.Value);
                        break;
                    case "nocertificado":
                        break;
                    case "sello":
                        break;
                    case "lugarexpedicion":
                        break;
                }
            }
            _reader.MoveToElement();
        }

        /// <summary>
        /// obtener datos del nodo emisor
        /// </summary>
        public static void ReaderEmisor(ref ComprobanteFiscalDetailModel _response, ref XmlReader _reader) {
            for (int attInd = 0; attInd < _reader.AttributeCount; attInd++) {
                _reader.MoveToAttribute(attInd);
                Console.WriteLine(string.Format("{0} {1}", _reader.Name, _reader.Value));
                switch (_reader.Name.ToLower()) {
                    case "nombre":
                        _response.Emisor = ReaderString(_reader.Value);
                        break;
                    case "rfc":
                        _response.EmisorRFC = ReaderString(_reader.Value);
                        break;
                    case "regimenfiscal":
                        _response.ClaveRegimenFiscal = ReaderString(_reader.Value);
                        break;
                }
            }
            _reader.MoveToElement();
        }

        /// <summary>
        /// obtener datos del nodo receptor
        /// </summary>
        public static void ReaderReceptor(ref ComprobanteFiscalDetailModel _response, ref XmlReader _reader) {
            for (int attInd = 0; attInd < _reader.AttributeCount; attInd++) {
                _reader.MoveToAttribute(attInd);
                Console.WriteLine(string.Format("{0} {1}", _reader.Name, _reader.Value));
                switch (_reader.Name.ToLower()) {
                    case "nombre":
                        _response.Receptor = ReaderString(_reader.Value);
                        break;
                    case "rfc":
                        _response.ReceptorRFC = ReaderString(_reader.Value);
                        break;
                    case "usocfdi":
                        _response.ClaveUsoCFDI = ReaderString(_reader.Value);
                        break;
                    case "domiciliofiscalreceptor":
                        _response.ReceptorDomicilioFiscal = ReaderString(_reader.Value);
                        break;
                    case "regimenfiscalreceptor":
                        _response.ClaveRegimenFiscal = ReaderString(_reader.Value);
                        break;
                }
            }
            _reader.MoveToElement();
        }

        /// <summary>
        /// obtener informacion del nodo timbre fiscal para versiones 3.2, 3.3 y 4.0
        /// </summary>
        public static void ReaderTimbreFiscal(ref ComprobanteFiscalDetailModel _response, ref XmlReader _reader) {
            for (int attInd = 0; attInd < _reader.AttributeCount; attInd++) {
                _reader.MoveToAttribute(attInd);
                Console.WriteLine(string.Format("{0} {1}", _reader.Name, _reader.Value));
                switch (_reader.Name.ToLower()) {
                    case "version":
                        break;
                    case "uuid":
                        _response.IdDocumento = ReaderString(_reader.Value);
                        break;
                    case "fechatimbrado":
                        _response.FechaCerfificacion = ReaderDateTime(_reader.Value);
                        break;
                    case "rfcprovcertif":
                        _response.PACCertifica = ReaderString(_reader.Value);
                        break;
                    case "nocertificadosat":
                        //_response.NoCertificadoProvCertif = ReaderString(_reader.Value);
                        break;
                    case "sellocfd":
                        break;
                    case "sellosat":
                        break;
                    case "leyenda":
                        break;
                }
            }
            _reader.MoveToElement();
        }

        public static void ReaderConceptos(ref ComprobanteFiscalDetailModel _response, ref XmlReader _reader) {

        }

        public static void ReaderCfdi33Impuestos(ref ComprobanteFiscalDetailModel objeto, string inXml) {
            // 001 - ISR
            // 002 - IVA
            // 003 - IEPS
            objeto.RetencionIEPS = 0;
            objeto.RetencionISR = 0;
            objeto.RetencionIVA = 0;
            objeto.TrasladoIVA = 0;
            objeto.TrasladoIEPS = 0;

            if ((inXml.Contains("TotalImpuestosTrasladados") || inXml.Contains("TotalImpuestosRetenidos") ? true : false)) {
                XmlDocument xmlD = new XmlDocument();
                IEnumerator enumerator = null;
                xmlD.LoadXml(inXml);
                try {
                    enumerator = xmlD.SelectNodes("//*").GetEnumerator();
                    while (enumerator.MoveNext()) {
                        XmlElement current = (XmlElement)enumerator.Current;
                        try {
                            if (current.Name != "cfdi:Traslado") {
                                if (current.Name == "cfdi:Retencion") {
                                    if (current.GetAttribute("Impuesto").ToLower() == "002") {
                                        objeto.RetencionIVA = objeto.RetencionIVA + Convert.ToDecimal(current.GetAttribute("Importe"));
                                    } else if (current.GetAttribute("Impuesto").ToLower() == "001") {
                                        objeto.RetencionISR = objeto.RetencionISR + Convert.ToDecimal(current.GetAttribute("Importe"));
                                    } else if (current.GetAttribute("Impuesto").ToLower() == "003") {
                                        objeto.RetencionIEPS = objeto.RetencionIEPS + Convert.ToDecimal(current.GetAttribute("Importe"));
                                    }
                                }
                            } else if (current.GetAttribute("Impuesto").ToLower() == "002") {
                                objeto.TrasladoIVA = objeto.TrasladoIVA + Convert.ToDecimal(current.GetAttribute("Importe"));
                            } else if (current.GetAttribute("Impuesto").ToLower() == "001") {
                                objeto.TrasladoIEPS = objeto.TrasladoIEPS + Convert.ToDecimal(current.GetAttribute("Importe"));
                            }
                        } catch (Exception ex) {
                            Console.WriteLine(string.Concat("ReaderCFDI33Impuestos: ", ex.Message));
                        }
                    }
                } catch (Exception ex) {
                    Console.WriteLine(string.Concat("ReaderCFDI33Impuestos: ", ex.Message));
                }
            }
        }

        /// <summary>
        /// obtener informacion del nodo complementos/pagos
        /// </summary>
        public static void ReaderPagos(ref ComprobanteFiscalDetailModel _response, ref XmlReader _reader) {
            Console.WriteLine(_reader.ReadOuterXml());
        }
        #endregion

        #region metodos
        public static bool IsValidXml(string stringXml) {
            var c = (new Regex("cfdi:Comprobante", RegexOptions.Compiled)).Matches(stringXml);
            if (0 < c.Count) {
                foreach (Match item in c) {
                    string str = item.ToString();
                    if (str.ToLower().Contains("cfdi:comprobante")) {
                        return true;
                    }
                }
            }
            return false;
        }

        public static string ExtraeCFDI(string contenidoXml) {
            int posicion1 = contenidoXml.IndexOf("<cfdi:Comprobante");
            int posicion2 = contenidoXml.IndexOf("</cfdi:Comprobante>");
            return contenidoXml.Substring(posicion1, contenidoXml.Length - posicion1);
        }

        /// <summary>
        /// obtener tipo de comprobante de modo texto
        /// </summary>
        public static string TipoComprobante(string clave) {
            if (clave.ToUpper().StartsWith("I")) {
                return "Ingreso";
            } else if (clave.ToUpper().StartsWith("E")) {
                return "Egreso";
            } else if (clave.ToUpper().StartsWith("N")) {
                return "Nomina";
            } else if (clave.ToUpper().StartsWith("T")) {
                return "Traslado";
            } else if (clave.ToUpper().StartsWith("P")) {
                return "Pagos";
            }
            return clave;
        }

        /// <summary>
        /// obtener valor del objeto en modo texto
        /// </summary>
        public static string ReaderString(object valueObject) {
            if (valueObject != DBNull.Value) {
                if (valueObject != null) {
                    if (valueObject.ToString().Trim() != string.Empty) {
                        return valueObject.ToString();
                    }
                }
            }
            return "";
        }

        public static DateTime ReaderDateTime(object valueObject) {
            if (valueObject != DBNull.Value) {
                if (valueObject != null) {
                    if (valueObject.ToString().Trim() != string.Empty) {
                        return DateTime.Parse(valueObject.ToString());
                    }
                }
            }
            return DateTime.MinValue;
        }

        public static string FileToBase64(FileInfo archivo) {
            UTF8Encoding objUtf8WithoutBom = new UTF8Encoding(false);
            try {
                using (FileStream fileStream = new FileStream(archivo.FullName, FileMode.Open, FileAccess.Read)) {
                    using (StreamReader streamReader = new StreamReader(fileStream, objUtf8WithoutBom)) {
                        return Convert.ToBase64String(Encoding.UTF8.GetBytes(streamReader.ReadToEnd()));
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine("HelperCFDReaderCommon:FileToB64: " + ex.Message);
                return "";
            }
        }
    }
    #endregion
}
