﻿using System;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Repositorio.Contracts;
using Jaeger.DataAccess.Repositorio.Repositories;
using Jaeger.Domain.Repositorio.Contracts;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Aplication.Repositorio.Services {
    public class AdministradorService : IAdministradorService {
        private readonly ISqlEmpresaRepository empresaRepository;
        private string Shatterdome = @"C:\AdminCFD\Repositorio\jaeger_shatterdome.sqlite";

        public AdministradorService() {
            this.Shatterdome = RouterManagerService.GetPath(ValueObjects.PathEnum.Repositorio, "jaeger_shatterdome.sqlite");
            this.empresaRepository = new SQLiteEmpresaRepository(new Domain.DataBase.Entities.DataBaseConfiguracion { Database = this.Shatterdome });
        }

        public int GetDemo() {
            var expira = new DateTime(2023, 3, 20);
            TimeSpan difFechas = expira - DateTime.Now;
            return difFechas.Days;
        }

        public bool Existe() {
            if (System.IO.File.Exists(Shatterdome)) {
                return true;
            }
            return false;
        }

        public bool Create() {
            if (!System.IO.Directory.Exists(RouterManagerService.GetPath(ValueObjects.PathEnum.Repositorio))) {
                return false;
            }
            if (this.empresaRepository.CreateDB() == true) {
                return this.empresaRepository.InitTables();
            }
            return false;
        }

        public bool Verificar() {
            return this.empresaRepository.Verificar();
        }

        public EmpresaModel Save(EmpresaModel model) {
            return this.empresaRepository.Save(model);
        }
        public BindingList<EmpresaModel> GetList() {
            return new BindingList<EmpresaModel>(this.empresaRepository.GetList().ToList());
        }
    }
}
