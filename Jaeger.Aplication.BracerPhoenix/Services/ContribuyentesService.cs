﻿using System.Linq;
using System.ComponentModel;
using Jaeger.Aplication.Repositorio.Contracts;
using Jaeger.Domain.Repositorio.Contracts;
using Jaeger.Domain.Repositorio.Entities;
using Jaeger.DataAccess.Repositorio.Repositories;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Aplication.Repositorio.Services {
    public class ContribuyentesService : IContribuyentesService {
        protected ISqlContribuyenteRepository contribuyenteRepository;

        public ContribuyentesService(string fileDataBase) {
            this.contribuyenteRepository = new SQLiteContribuyenteRepository(new DataBaseConfiguracion { Database = fileDataBase });
        }

        public ContribuyenteModel Save(ContribuyenteModel model) {
            return this.contribuyenteRepository.Save(model);
        }

        public BindingList<ContribuyenteModel> GetList(int relacion) {
            return new BindingList<ContribuyenteModel>(this.contribuyenteRepository.GetList(relacion).ToList());
        }

        public void Test(string tipo) {
            this.contribuyenteRepository.Test(tipo);
        }
    }
}
