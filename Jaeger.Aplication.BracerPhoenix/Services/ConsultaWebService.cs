﻿using System.Linq;
using System.ComponentModel;
using Jaeger.DataAccess.Repositorio.Repositories;
using Jaeger.Domain.Repositorio.Contracts;
using Jaeger.Domain.Repositorio.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Aplication.Repositorio.Contracts;

namespace Jaeger.Aplication.Repositorio.Services {
    public class ConsultaWebService : IConsultaWebService {
        protected ISqlConsultaRepository consultaRepository;

        public ConsultaWebService() {
            this.consultaRepository = new SQLiteConsultaRepository(new DataBaseConfiguracion { Database = ConfiguracionService.Manager.FileRepositorio });
        }

        public SolicitudModel Save(SolicitudModel solicitudModel) {
            if (solicitudModel.Verificacion != null) {

            }
            return this.consultaRepository.Save(solicitudModel);
        }

        public BindingList<SolicitudModel> GetAll() {
            return new BindingList<SolicitudModel>(this.consultaRepository.GetAll().ToList());
        }
    }
}
