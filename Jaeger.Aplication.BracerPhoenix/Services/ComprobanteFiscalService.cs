﻿using System;
using System.IO;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using MiniExcelLibs;
using Jaeger.Domain.Repositorio.Contracts;
using Jaeger.Domain.Repositorio.Entities;
using Jaeger.Aplication.Repositorio.Contracts;
using Jaeger.Aplication.Repositorio.Beta.Services;
using Jaeger.DataAccess.Repositorio.Repositories;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Services;

namespace Jaeger.Aplication.Repositorio.Services {
    public class ComprobanteFiscalService : IComprobanteFiscalService {
        protected ISqlComprobanteFiscalRepository comprobanteFiscalRepository;
        protected ISqlContribuyenteRepository contribuyenteRepository;
        protected ComprobanteExtensions Extensions;

        public ComprobanteFiscalService(string fileDataBase) {
            this.comprobanteFiscalRepository = new SQLiteComprobanteFiscalRepository(new DataBaseConfiguracion { Database = fileDataBase });
            this.contribuyenteRepository = new SQLiteContribuyenteRepository(new DataBaseConfiguracion { Database = fileDataBase });
            this.Extensions = new ComprobanteExtensions();
        }

        public BindingList<ComprobanteFiscalDetailModel> GetList(int month, int year = 0, string efecto = "Todos", string tipo = "Recibidos") {
            return new BindingList<ComprobanteFiscalDetailModel>(this.comprobanteFiscalRepository.GetList(month, year, efecto, tipo).ToList());
        }

        public ComprobanteFiscalDetailModel Serializar(ComprobanteFiscalDetailModel model) {
            model = CFDReader.Reader(model.XmlContentB64, ref model);
            return model;
        }

        public ComprobanteFiscalDetailModel Save(ComprobanteFiscalDetailModel model) {
            return this.comprobanteFiscalRepository.Save(model);
        }

        public ComprobanteFiscalDetailModel Save(string version, string cfd) {
            if (version == "3.3") {
                var c1 = Jaeger.CFDI.V33.Comprobante.LoadBase64(cfd);
                var d1 = this.Extensions.Create(c1);
                d1.XmlContentB64 = cfd;
                return this.comprobanteFiscalRepository.Save(d1);
                
            } else {
                var c1 = Jaeger.CFDI.V40.Comprobante.LoadBase64(cfd);
                var d1 = this.Extensions.Create(c1);
                d1.XmlContentB64 = cfd;
                return this.comprobanteFiscalRepository.Save(d1);
                
            }
        }

        public void Test(BindingList<ComprobanteFiscalDetailModel> vales) {
            var path23 = @"C:\Users\anhed\OneDrive\SAT\testing\C6AC15F4-CF06-47A5-BB40-A4E53682AEEB-0000.txt";

            var config = new MiniExcelLibs.Csv.CsvConfiguration() { Seperator = '~' };
            var rows = MiniExcel.Query(path23, true, excelType:ExcelType.CSV, configuration: config).ToList();

            return;
            var path = Path.Combine(@"C:\Users\anhed\Downloads\", $"{Guid.NewGuid().ToString()}.xlsx");
            var path1 = Path.Combine(@"C:\Users\anhed\Downloads", $"{Guid.NewGuid().ToString()}.xlsx");
            var templatePath = @"C:\Users\anhed\Downloads\plantilla.xlsx";
            var d1 = Domain.Base.Services.DbConvert.ConvertToDataTable<ComprobanteFiscalDetailModel>(vales);
            // 2. By Dictionary
            var value = new Dictionary<string, object>() {
                ["comprobante"] = d1
            };
            MiniExcel.SaveAsByTemplate(path, templatePath, value);
            
        }

        public bool UpdateEstado(string idDocumento, string estado) {
            throw new NotImplementedException();
        }
    }
}
