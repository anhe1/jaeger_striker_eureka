﻿using System.Collections.Generic;

namespace Jaeger.Aplication.Repositorio.Services {
    public static class RegimeFiscalService {
        public static List<RegimenFiscal> Regimenes;
        public class RegimenFiscal : Domain.Base.Abstractions.BaseSingleTipoModel {
            public RegimenFiscal() { }

            public RegimenFiscal(string clave, string descripcion) {
                Id = clave;
                Descripcion = descripcion;
            }
        }

        static RegimeFiscalService() {
            Regimenes = new List<RegimenFiscal> {
                new RegimenFiscal("601", "General de Ley Personas Morales"),
                new RegimenFiscal("603", "Personas Morales con Fines no Lucrativos"),
                new RegimenFiscal("605", "Sueldos y Salarios e Ingresos Asimilados a Salarios"),
                new RegimenFiscal("606", "Arrendamiento"),
                new RegimenFiscal("607", "Régimen de Enajenación o Adquisición de Bienes"),
                new RegimenFiscal("608", "Demás ingresos"),
                new RegimenFiscal("610", "Residentes en el Extranjero sin Establecimiento Permanente en México"),
                new RegimenFiscal("611", "Ingresos por Dividendos (socios y accionistas)"),
                new RegimenFiscal("612", "Personas Físicas con Actividades Empresariales y Profesionales"),
                new RegimenFiscal("614", "Ingresos por intereses"),
                new RegimenFiscal("615", "Régimen de los ingresos por obtención de premios"),
                new RegimenFiscal("616", "Sin obligaciones fiscales"),
                new RegimenFiscal("620", "Sociedades Cooperativas de Producción que optan por diferir sus ingresos"),
                new RegimenFiscal("621", "Incorporación Fiscal"),
                new RegimenFiscal("622", "Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras"),
                new RegimenFiscal("623", "Opcional para Grupos de Sociedades"),
                new RegimenFiscal("624", "Coordinados"),
                new RegimenFiscal("625", "Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas"),
                new RegimenFiscal("626", "Régimen Simplificado de Confianza")
            };
        }
    }
}
