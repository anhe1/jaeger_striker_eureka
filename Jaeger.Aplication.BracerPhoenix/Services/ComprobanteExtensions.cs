﻿using System.ComponentModel;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Aplication.Repositorio.Beta.Services {
    public class ComprobanteExtensions {

        public ComprobanteExtensions() {
        }

        #region version 3.3
        public ComprobanteFiscalDetailModel Create(CFDI.V33.Comprobante cfdi) {
            var _comprobante = new ComprobanteFiscalDetailModel();

            _comprobante.Version = cfdi.Version;
            _comprobante.Folio = cfdi.Folio;
            _comprobante.Serie = cfdi.Serie;
            //response.SubTipoInt = (int)cfdi.SubTipo;
            var _receptor = this.Receptor(cfdi.Receptor);
            _comprobante.Receptor = _receptor.RazonSocial;
            _comprobante.ReceptorRFC = _receptor.RFC;
            var _emisor = this.Emisor(cfdi.Emisor);
            _comprobante.Emisor = _emisor.RazonSocial;
            _comprobante.EmisorRFC = _emisor.RFC;
            _comprobante.SubTotal = cfdi.SubTotal;
            _comprobante.Total = cfdi.Total;
            //_comprobante.NoCertificado = cfdi.NoCertificado;
            _comprobante.FechaEmision = cfdi.Fecha;
            _comprobante.ClaveMoneda = cfdi.Moneda;
            //_comprobante.LugarExpedicion = cfdi.LugarExpedicion;
            _comprobante.ClaveUsoCFDI = cfdi.Receptor.UsoCFDI;
            //_comprobante.Conceptos = Conceptos(cfdi.Conceptos);

            _comprobante.Efecto = cfdi.TipoDeComprobante;
            if (cfdi.TipoDeComprobante.ToUpper().StartsWith("I")) {
                _comprobante.Efecto = "Ingreso";
            } else if (cfdi.TipoDeComprobante.ToUpper().StartsWith("E")) {
                _comprobante.Efecto = "Egreso";
            } else if (cfdi.TipoDeComprobante.ToUpper().StartsWith("T")) {
                _comprobante.Efecto = "Traslado";
            } else if (cfdi.TipoDeComprobante.ToUpper().StartsWith("N")) {
                _comprobante.Efecto = "Nomina";
            } else if (cfdi.TipoDeComprobante.ToUpper().StartsWith("P")) {
                _comprobante.Efecto = "Pagos";
            }

            // descuento
            if (cfdi.Descuento > 0) {
                _comprobante.Descuento = cfdi.Descuento;
            }

            // tipo de campbio
            //if (cfdi.TipoCambioSpecified)
                //_comprobante.TipoCambio = cfdi.TipoCambio;

            // forma de pago
            if (cfdi.FormaPagoSpecified)
                _comprobante.ClaveFormaPago = cfdi.FormaPago;

            // condiciones de pago
            //if (cfdi.CondicionesDePago != null) {
            //    _comprobante.CondicionPago = cfdi.CondicionesDePago;
            //}

            // metodo de pago
            if (cfdi.MetodoPagoSpecified)
                _comprobante.ClaveMetodoPago = cfdi.MetodoPago;

            // comprobantes relacionados
            //if (!(cfdi.CfdiRelacionados == null)) {
            //    _comprobante.CfdiRelacionados = this.CfdiRelacionados(cfdi.CfdiRelacionados);
            //} else {
            //    _comprobante.CfdiRelacionados = null;
            //}

            // totales de los impuestos
            _comprobante = this.Impuestos(ref cfdi, _comprobante);

            // complementos
            if (!(cfdi.Complemento == null)) {
                if (cfdi.Complemento.TimbreFiscalDigital != null) {
                    _comprobante.IdDocumento = cfdi.Complemento.TimbreFiscalDigital.UUID;
                    _comprobante.FechaCerfificacion = cfdi.Complemento.TimbreFiscalDigital.FechaTimbrado;
                    _comprobante.PACCertifica = cfdi.Complemento.TimbreFiscalDigital.RfcProvCertif;
                }
            }
                //_comprobante = this.Complementos(ref cfdi, _comprobante);

            return _comprobante;
        }

        private ContribuyenteModel Receptor(CFDI.V33.ComprobanteReceptor receptor) {
            var response = new ContribuyenteModel() {
                RazonSocial = receptor.Nombre,
                //ClaveUsoCFDI = receptor.UsoCFDI,
                //NumRegIdTrib = receptor.NumRegIdTrib,
                RFC = receptor.Rfc,
            };
            //if (receptor.ResidenciaFiscalSpecified)
            //    response.ResidenciaFiscal = receptor.ResidenciaFiscal;
            return response;
        }

        private ContribuyenteModel Emisor(CFDI.V33.ComprobanteEmisor emisor) {
            var response = new ContribuyenteModel() {
                RFC = emisor.Rfc,
                RazonSocial = emisor.Nombre,
                RegimenFiscal = emisor.RegimenFiscal
            };
            return response;
        }

        //private ComprobanteCfdiRelacionadosModel CfdiRelacionados(CFDI.V33.ComprobanteCfdiRelacionados relacionados) {
        //    if (!(relacionados == null)) {
        //        var response = new ComprobanteCfdiRelacionadosModel();
        //        response.TipoRelacion.Clave = relacionados.TipoRelacion;
        //        foreach (var item in relacionados.CfdiRelacionado) {
        //            var newItem = new ComprobanteCfdiRelacionadosCfdiRelacionadoModel {
        //                IdDocumento = item.UUID
        //            };
        //            response.CfdiRelacionado.Add(newItem);
        //        }
        //        return response;
        //    }
        //    return null;
        //}

        private ComprobanteFiscalDetailModel Impuestos(ref CFDI.V33.Comprobante cfdi, ComprobanteFiscalDetailModel response) {
            // impuestos
            if (!(cfdi.Impuestos == null)) {
                if (cfdi.Impuestos.TotalImpuestosRetenidosSpecified) {
                    if (!(cfdi.Impuestos.Retenciones == null)) {
                        foreach (var imp in cfdi.Impuestos.Retenciones) {
                            if (imp.Impuesto == "001") {
                                // retencion ISR
                                response.RetencionISR = response.RetencionISR + imp.Importe;
                            } else if (imp.Impuesto == "002") {
                                // retencion IVA
                                response.RetencionIVA = response.RetencionIVA + imp.Importe;
                            } else if (imp.Impuesto == "003") {
                                // retencion IEPS
                                response.RetencionIEPS = response.RetencionIEPS + imp.Importe;
                            }
                        }
                    }
                }

                if (cfdi.Impuestos.TotalImpuestosTrasladadosSpecified) {
                    if (!(cfdi.Impuestos.Traslados == null)) {
                        foreach (var imp in cfdi.Impuestos.Traslados) {
                            if (imp.Impuesto == "002") {
                                // traslado IVA
                                response.TrasladoIVA = response.TrasladoIVA + imp.Importe;
                            } else if (imp.Impuesto == "003") {
                                // traslado IEPS
                                response.TrasladoIEPS = response.TrasladoIEPS + imp.Importe;
                            }
                        }
                    }
                }
            }
            return response;
        }
        #endregion

        #region version 4.0
        public ComprobanteFiscalDetailModel Create(CFDI.V40.Comprobante cfdi) {
            var _comprobante = new ComprobanteFiscalDetailModel();
            _comprobante.Version = cfdi.Version;
            _comprobante.Folio = cfdi.Folio;
            _comprobante.Serie = cfdi.Serie;
            var _receptor = this.Receptor(cfdi.Receptor);
            _comprobante.Receptor = _receptor.RazonSocial;
            _comprobante.ReceptorRFC = _receptor.RFC;
            var _emisor = this.Emisor(cfdi.Emisor);
            _comprobante.Emisor = _emisor.RazonSocial;
            _comprobante.EmisorRFC = _emisor.RFC;
            _comprobante.SubTotal = cfdi.SubTotal;
            _comprobante.Total = cfdi.Total;
            //_comprobante.NoCertificado = cfdi.NoCertificado;
            _comprobante.FechaEmision = cfdi.Fecha;
            _comprobante.ClaveMoneda = cfdi.Moneda;
            //_comprobante.LugarExpedicion = cfdi.LugarExpedicion;
            _comprobante.ClaveUsoCFDI = cfdi.Receptor.UsoCFDI;
            //_comprobante.Conceptos = Conceptos(cfdi.Conceptos);
            //_comprobante.DomicilioFiscal = cfdi.Receptor.DomicilioFiscalReceptor;

            // identificar tipo de comprobante
            _comprobante.Efecto = cfdi.TipoDeComprobante;
            //if (cfdi.TipoDeComprobante.ToUpper().StartsWith("I")) {
            //    _comprobante.TipoComprobante = CFDITipoComprobanteEnum.Ingreso;
            //} else if (cfdi.TipoDeComprobante.ToUpper().StartsWith("E")) {
            //    _comprobante.TipoComprobante = CFDITipoComprobanteEnum.Egreso;
            //} else if (cfdi.TipoDeComprobante.ToUpper().StartsWith("T")) {
            //    _comprobante.TipoComprobante = CFDITipoComprobanteEnum.Traslado;
            //} else if (cfdi.TipoDeComprobante.ToUpper().StartsWith("N")) {
            //    _comprobante.TipoComprobante = CFDITipoComprobanteEnum.Nomina;
            //} else if (cfdi.TipoDeComprobante.ToUpper().StartsWith("P")) {
            //    _comprobante.TipoComprobante = CFDITipoComprobanteEnum.Pagos;
            //}

            //if (cfdi.Receptor.ResidenciaFiscalSpecified)
            //    _comprobante.ResidenciaFiscal = cfdi.Receptor.ResidenciaFiscal;
            // descuento
            if (cfdi.Descuento > 0) {
                _comprobante.Descuento = cfdi.Descuento;
            }

            // tipo de campbio
            //if (cfdi.TipoCambioSpecified)
            //    _comprobante.TipoCambio = cfdi.TipoCambio;

            // forma de pago
            if (cfdi.FormaPagoSpecified)
                _comprobante.ClaveFormaPago = cfdi.FormaPago;

            // condiciones de pago
            //if (cfdi.CondicionesDePago != null) {
            //    _comprobante.CondicionPago = cfdi.CondicionesDePago;
            //}

            // metodo de pago
            if (cfdi.MetodoPagoSpecified)
                _comprobante.ClaveMetodoPago = cfdi.MetodoPago;

            // comprobantes relacionados
            //if (!(cfdi.CfdiRelacionados == null)) {
            //    _comprobante.CfdiRelacionados = this.CfdiRelacionados(cfdi.CfdiRelacionados);
            //} else {
            //    _comprobante.CfdiRelacionados = null;
            //}

            // objeto de exportacion
            if (cfdi.Exportacion == "01")
                _comprobante.ClaveExportacion = "NA";// CFDIExportacionEnum.No_Aplica.ToString();
            else if (cfdi.Exportacion == "02")
                _comprobante.ClaveExportacion = CFDIExportacionEnum.Definitiva.ToString();
            else if (cfdi.Exportacion == "03")
                _comprobante.ClaveExportacion = CFDIExportacionEnum.Temporal.ToString();

            // informacion global
            //_comprobante.InformacionGlobal = InformacionGlobal(cfdi.InformacionGlobal);

            // totales de los impuestos
            _comprobante = Impuestos(ref cfdi, _comprobante);

            // complementos
            //if (!(cfdi.Complemento == null))
            //    _comprobante = this.Complementos(ref cfdi, _comprobante);

            // validacion
            //if (cfdi.Validation != null) {
            //    response.JValidacion = cfdi.Validation.Json();
            //    response.Result = cfdi.Validation.ValidoText;
            //    response.Estado = cfdi.Validation.ProofStatus;
            //    response.FechaVal = cfdi.Validation.FechaValidacion;
            //    response.FechaEstado = response.FechaVal;
            //}

            return _comprobante;
        }

        private ContribuyenteModel Emisor(CFDI.V40.ComprobanteEmisor emisor) {
            var _emisor = new ContribuyenteModel() {
                RazonSocial = emisor.Nombre,
                RFC = emisor.Rfc,
                RegimenFiscal = emisor.RegimenFiscal
            };
            return _emisor;
        }

        private ContribuyenteModel Receptor(CFDI.V40.ComprobanteReceptor receptor) {
            var _receptor = new ContribuyenteModel {
                RFC = receptor.Rfc,
                RazonSocial = receptor.Nombre,
                RegimenFiscal = receptor.RegimenFiscalReceptor,
                //ResidenciaFiscal = receptor.ResidenciaFiscal,
                //ClaveUsoCFDI = receptor.UsoCFDI,
                CodigoPostal = receptor.DomicilioFiscalReceptor,
                //NumRegIdTrib = receptor.NumRegIdTrib
            };
            return _receptor;
        }

        //private ComprobanteCfdiRelacionadosModel CfdiRelacionados(CFDI.V40.ComprobanteCfdiRelacionados[] cfdiRelacionados) {
        //    if (cfdiRelacionados != null) {
        //        var _response = new ComprobanteCfdiRelacionadosModel();
        //        foreach (var item in cfdiRelacionados) {
        //            _response.TipoRelacion = new ComprobanteCfdiRelacionadoRelacion { Clave = item.TipoRelacion };
        //            _response.CfdiRelacionado = new BindingList<ComprobanteCfdiRelacionadosCfdiRelacionadoModel>();
        //            foreach (var _item in item.CfdiRelacionado) {
        //                _response.CfdiRelacionado.Add(new ComprobanteCfdiRelacionadosCfdiRelacionadoModel { IdDocumento = _item.UUID });
        //            }
        //        }
        //    }

        //    return null;
        //}

        //public static BindingList<ComprobanteConceptoDetailModel> Conceptos(CFDI.V40.ComprobanteConcepto[] conceptos) {
        //    var _response = new BindingList<ComprobanteConceptoDetailModel>();
        //    foreach (var concepto in conceptos) {
        //        var _concepto = new ComprobanteConceptoDetailModel {
        //            Cantidad = concepto.Cantidad,
        //            Descripcion = concepto.Descripcion,
        //            Importe = concepto.Importe,
        //            ValorUnitario = concepto.ValorUnitario,
        //            NoIdentificacion = concepto.NoIdentificacion,
        //            Unidad = concepto.Unidad,
        //            ClaveProdServ = concepto.ClaveProdServ,
        //            ClaveUnidad = concepto.ClaveUnidad,
        //            Activo = true,
        //            Descuento = concepto.Descuento,
        //            ObjetoImp = concepto.ObjetoImp
        //        };

        //        if (!(concepto.Impuestos == null)) {
        //            //impuestos trasladados
        //            if (!(concepto.Impuestos.Traslados == null)) {
        //                foreach (var t in concepto.Impuestos.Traslados) {
        //                    ComprobanteConceptoImpuesto itemT = new ComprobanteConceptoImpuesto();
        //                    itemT.Tipo = TipoImpuestoEnum.Traslado;

        //                    if (t.Impuesto == "002") {
        //                        // traslado IVA-002
        //                        itemT.Impuesto = ImpuestoEnum.IVA;
        //                    } else if (t.Impuesto == "003") {
        //                        //traslado IEPS
        //                        itemT.Impuesto = ImpuestoEnum.IEPS;
        //                    }

        //                    if (t.TipoFactor.ToLower().Contains("tasa")) {
        //                        itemT.TipoFactor = FactorEnum.Tasa;
        //                    } else if (t.TipoFactor.ToLower().Contains("cuota")) {
        //                        itemT.TipoFactor = FactorEnum.Cuota;
        //                    }

        //                    if (t.TasaOCuotaSpecified) {
        //                        itemT.TasaOCuota = t.TasaOCuota;
        //                    }

        //                    if (t.ImporteSpecified) {
        //                        itemT.Importe = t.Importe;
        //                    }

        //                    itemT.Base = t.Base;
        //                    _concepto.Impuestos.Add(itemT);
        //                }
        //            }

        //            // impuestos retenidos
        //            if (!(concepto.Impuestos.Retenciones == null)) {
        //                foreach (var r in concepto.Impuestos.Retenciones) {
        //                    ComprobanteConceptoImpuesto itemR = new ComprobanteConceptoImpuesto();
        //                    itemR.Base = r.Base;
        //                    itemR.Importe = r.Importe;
        //                    itemR.Tipo = TipoImpuestoEnum.Retencion;
        //                    if (r.Impuesto == "002") {
        //                        itemR.Impuesto = ImpuestoEnum.IVA;
        //                    } else if (r.Impuesto == "003") {
        //                        itemR.Impuesto = ImpuestoEnum.IEPS;
        //                    } else if (r.Impuesto == "001") {
        //                        itemR.Impuesto = ImpuestoEnum.ISR;
        //                    }

        //                    if (r.TipoFactor.ToLower().Contains("tasa")) {
        //                        itemR.TipoFactor = FactorEnum.Tasa;
        //                    } else if (r.TipoFactor.ToLower().Contains("cuota")) {
        //                        itemR.TipoFactor = FactorEnum.Cuota;
        //                    } else if (r.TipoFactor.ToLower().Contains("exento")) {
        //                        itemR.TipoFactor = FactorEnum.Exento;
        //                    }
        //                    _concepto.Impuestos.Add(itemR);
        //                }
        //            }
        //        }
        //        _response.Add(_concepto);
        //    }

        //    // impuestos
        //    return _response;
        //}

        /// <summary>
        /// importes de los impuestos totales del comprobante
        /// </summary>
        public static ComprobanteFiscalDetailModel Impuestos(ref CFDI.V40.Comprobante cfdi, ComprobanteFiscalDetailModel comprobante) {
            // impuestos
            if (!(cfdi.Impuestos == null)) {
                if (cfdi.Impuestos.TotalImpuestosRetenidosSpecified) {
                    if (!(cfdi.Impuestos.Retenciones == null)) {
                        foreach (var imp in cfdi.Impuestos.Retenciones) {
                            if (imp.Impuesto == "001") {
                                // retencion ISR
                                comprobante.RetencionISR = comprobante.RetencionISR + imp.Importe;
                            } else if (imp.Impuesto == "002") {
                                // retencion IVA
                                comprobante.RetencionIVA = comprobante.RetencionIVA + imp.Importe;
                            } else if (imp.Impuesto == "003") {
                                // retencion IEPS
                                comprobante.RetencionIEPS = comprobante.RetencionIEPS + imp.Importe;
                            }
                        }
                    }
                }

                if (cfdi.Impuestos.TotalImpuestosTrasladadosSpecified) {
                    if (!(cfdi.Impuestos.Traslados == null)) {
                        foreach (var imp in cfdi.Impuestos.Traslados) {
                            if (imp.Impuesto == "002") {
                                // traslado IVA
                                comprobante.TrasladoIVA = comprobante.TrasladoIVA + imp.Importe;
                            } else if (imp.Impuesto == "003") {
                                // traslado IEPS
                                comprobante.TrasladoIEPS = comprobante.TrasladoIEPS + imp.Importe;
                            }
                        }
                    }
                }
            }
            return comprobante;
        }

        /// <summary>
        /// informacion del comprobante global
        /// </summary>
        //public static ComprobanteInformacionGlobalDetailModel InformacionGlobal(CFDI.V40.ComprobanteInformacionGlobal informacionGlobal) {
        //    if (informacionGlobal != null) {
        //        var _response = new ComprobanteInformacionGlobalDetailModel {
        //            Activo = true,
        //            Anio = informacionGlobal.Año,
        //            ClaveMeses = informacionGlobal.Meses,
        //            Periodicidad = informacionGlobal.Periodicidad
        //        };
        //        return _response;
        //    }
        //    return null;
        //}
        #endregion
    }
}
