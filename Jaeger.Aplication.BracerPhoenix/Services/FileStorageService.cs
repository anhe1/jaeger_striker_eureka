﻿using Jaeger.Aplication.Repositorio.Contracts;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Aplication.Repositorio.Services {
    public class FileStorageService : IFileStorageService {
        protected internal string PathRoot = @"C:\Jaeger";
        public FileStorageService() {

        }

        public void Backup(ComprobanteFiscalDetailModel model) {
            var mes = model.FechaEmision.Month.ToString();
            var year = model.FechaEmision.Year.ToString();
            var day = model.FechaEmision.Day.ToString();
            var carpeta = System.IO.Path.Combine(this.PathRoot, year, mes, day);
            if (!System.IO.Directory.Exists(carpeta)) {
                System.IO.Directory.CreateDirectory(carpeta);
            }
        }
    }
}
