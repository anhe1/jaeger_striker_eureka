﻿using System;
using System.Collections.Generic;
using System.IO;
using Jaeger.Aplication.Repositorio.Contracts;
using MiniExcelLibs;
using MiniExcelLibs.Csv;

namespace Jaeger.Aplication.Repositorio.Services {
    public class MetaDataService : IMetaDataService {
        public MetaDataService() { }

        public List<Domain.Repositorio.Entities.RepositorioMeta> GetComprobanteMETA(string fileName) {
            if (!File.Exists(fileName)) {
                return null;
            }
            var lista = new List<Domain.Repositorio.Entities.RepositorioMeta>();
            var config = new CsvConfiguration() { Seperator = '~' };
            using (var reader = MiniExcel.GetReader(fileName, configuration: config, excelType: ExcelType.CSV, useHeaderRow: true)) {
                while (reader.Read()) {
                    var fila = new Domain.Repositorio.Entities.RepositorioMeta();
                    for (int i = 0; i < reader.FieldCount; i++) {
                        switch (reader.GetName(i).ToLower()) {
                            case "uuid":
                                fila.IdDocumento = reader.GetValue(i).ToString();
                                break;
                            case "rfcemisor":
                                fila.EmisorRFC = reader.GetValue(i).ToString();
                                break;
                            case "nombreemisor":
                                fila.Emisor = reader.GetValue(i).ToString();
                                break;
                            case "rfcreceptor":
                                fila.ReceptorRFC = reader.GetValue(i).ToString();
                                break;
                            case "nombrereceptor":
                                fila.Receptor = reader.GetValue(i).ToString();
                                break;
                            case "rfcpac":
                                fila.RFCProvCertif = reader.GetValue(i).ToString();
                                break;
                            case "fechaemision":
                                fila.FechaEmision = ReaderDateTime(reader.GetValue(i));
                                break;
                            case "fechacertificacionsat":
                                fila.FechaCertificacion = ReaderDateTime(reader.GetValue(i));
                                break;
                            case "monto":
                                fila.Total = Convert.ToDecimal(reader.GetValue(i));
                                break;
                            case "efectocomprobante":
                                fila.EfectoComprobante = Convert.ToChar(reader.GetValue(i));
                                break;
                            case "estatus":
                                fila.IdEstado = Convert.ToInt32((string)reader.GetValue(i));
                                break;
                            case "fechacancelacion":
                                fila.FechaCancela = ReaderDateTime(reader.GetValue(i));
                                break;
                            default: break;
                        }
                    }
                    lista.Add(fila);
                }
            }
            return lista;
        }

        public List<Domain.Repositorio.Entities.RetencionMeta> GetRetencionMETA(string fileName) {
            if (!File.Exists(fileName)) {
                return null;
            }
            var lista = new List<Domain.Repositorio.Entities.RetencionMeta>();
            var config = new CsvConfiguration() { Seperator = '~' };
            using (var reader = MiniExcel.GetReader(fileName, configuration: config, excelType: ExcelType.CSV, useHeaderRow: true)) {
                while (reader.Read()) {
                    var fila = new Domain.Repositorio.Entities.RetencionMeta();
                    for (int i = 0; i < reader.FieldCount; i++) {
                        switch (reader.GetName(i).ToLower()) {
                            case "uuid":
                                fila.IdDocumento = reader.GetValue(i).ToString();
                                break;
                            case "rfcemisor":
                                fila.EmisorRFC = reader.GetValue(i).ToString();
                                break;
                            case "nombreemisor":
                                fila.Emisor = reader.GetValue(i).ToString();
                                break;
                            case "rfcreceptor":
                                fila.ReceptorRFC = reader.GetValue(i).ToString();
                                break;
                            case "nombrereceptor":
                                fila.Receptor = reader.GetValue(i).ToString();
                                break;
                            case "rfcpac":
                                fila.RFCProvCertif = reader.GetValue(i).ToString();
                                break;
                            case "fechaemision":
                                fila.FechaEmision = ReaderDateTime(reader.GetValue(i));
                                break;
                            case "fechacertificacionsat":
                                fila.FechaCertificacion = ReaderDateTime(reader.GetValue(i));
                                break;
                            case "montoop":
                                fila.MontoTotalOperacion = Convert.ToDecimal(reader.GetValue(i));
                                break;
                            case "montoret":
                                fila.MontoTotalRetencion = Convert.ToDecimal(reader.GetValue(i));
                                break;
                            case "estatus":
                                fila.IdEstado = Convert.ToInt32((string)reader.GetValue(i));
                                break;
                            case "fechacancelacion":
                                fila.FechaCancela = ReaderDateTime(reader.GetValue(i));
                                break;
                            default: break;
                        }
                    }
                    lista.Add(fila);
                }
            }
            return lista;
        }

        public bool SaveExcelAs(string fileSourceCSV, string fileDestinyXLSX) {
            if (!File.Exists(fileSourceCSV)) {
                return false;
            }
            var config = new CsvConfiguration() { Seperator = '~' };
            var d1 = MiniExcel.Query(fileSourceCSV, configuration: config, useHeaderRow: true);
            MiniExcel.SaveAs(fileDestinyXLSX, d1, sheetName: "Hoja1", excelType: ExcelType.XLSX, overwriteFile: true);
            
            return true;
        }

        public static DateTime ReaderDateTime(object valueObject) {
            if (valueObject != DBNull.Value) {
                if (valueObject != null) {
                    if (valueObject.ToString().Trim() != string.Empty) {
                        return DateTime.Parse(valueObject.ToString());
                    }
                }
            }
            return DateTime.MinValue;
        }
    }
}
