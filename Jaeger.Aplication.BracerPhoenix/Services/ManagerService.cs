﻿using System.Linq;
using Jaeger.DataAccess.Repositorio.Repositories;
using Jaeger.Domain.Repositorio.Contracts;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Aplication.Repositorio.Contracts;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Aplication.Repositorio.Services {
    public class ManagerService : IManagerService {
        protected ISqlVersionRepository versionRepository;
        protected ISqlConfiguracionRepository configuracionRepository;
        protected IRepositorioService repositorioService;

        public ManagerService() {

        }

        public ManagerService(EmpresaModel empresa) {
            this.Load(empresa.Repositorio);
        }

        public string Message { get; set; }

        public string FileRepositorio { get; set; }

        public DataBaseConfiguracion DataBase { get; set; }

        public bool OpenSession { get; set; }

        public bool Load(string fileName) {
            this.FileRepositorio = fileName;
            if (System.IO.File.Exists(this.FileRepositorio)) {
                this.DataBase = new DataBaseConfiguracion { Database = fileName };
                this.Load();
                //ConfiguracionService.Contribuyente = this.GetEmpresa();
                return this.Verificar();
            } else {
                this.Message = "No se tiene acceso al archivo seleccionado";
            }
            return false;
        }

        private void Load() {
            this.versionRepository = new SQLiteVersionRepository(this.DataBase);
            this.configuracionRepository = new SQLiteConfiguracionRepository(this.DataBase);
        }

        public bool Verificar() {
            // verificar lista de tablas
            this.versionRepository.test();

            //var version = this.GetVersion();
            //if (version == null) {
            //    this.Message = "No se puede verificar la version del repositorio";
            //    this.OpenSession = false;
            //    return false;
            //}
            this.OpenSession = true;
            return true;
        }

        public ISqlVersionRepository GetVersionRepository() {
            return this.versionRepository;
        }

        public ISqlConfiguracionRepository ConfiguracionRepostory() {
            return this.configuracionRepository;
        }

        public IRepositorioService GetRepositorio() {
            return new RepositorioService(this.FileRepositorio);
        }

        public IComprobanteFiscalService GetComprobantes() {
            return new ComprobanteFiscalService(this.FileRepositorio);
        }

        public IContribuyentesService GetContribuyentes() {
            return new ContribuyentesService(this.FileRepositorio);
        }

        public EmpresaModel GetEmpresa() {
            var lista = this.configuracionRepository.GetList().ToList();
            var response = new EmpresaModel();

            foreach (var item in lista) {
                if (!string.IsNullOrEmpty(item.Valor)) {
                    switch (item.Parametro) {
                        case "rfc":
                            response.RFC = item.Valor;
                            break;
                        case "razonsocial":
                            response.RazonSocial = item.Valor;
                            break;
                        default:
                            break;
                    }
                }
            }

            return response;
        }

        #region versiones
        /// <summary>
        /// obtener version del repositorio
        /// </summary>
        public VersionModel GetVersion() {
            if (versionRepository != null) {
                return this.versionRepository.GetById();
            }
            return null;
        }
        #endregion
    }
}
