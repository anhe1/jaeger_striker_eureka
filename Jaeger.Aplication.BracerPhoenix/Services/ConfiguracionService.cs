﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Repositorio.Entities;
using Jaeger.Aplication.Repositorio.Contracts;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Aplication.Repositorio.Services {
    public static class ConfiguracionService {
        public static string dbShatterdome = "jaeger_shatterdome.sqlite";
        public static EmpresaModel Contribuyente;
        public static IManagerService Manager;
        public static List<UIMenuElement> Menus;
        public static Domain.Base.Profile.KaijuLogger Kaiju;

        public static UIMenuElement GeMenuElement(string key) {
            try {
                return Menus.Where(it => it.Name == key.ToLower()).FirstOrDefault();
            } catch (Exception) {
                return new UIMenuElement();
            }
        }

        public static List<UIMenuElement> GeMenuElement(int parentId) {
            try {
                return Menus.Where(it => it.ParentId == parentId).ToList();
            } catch (Exception) {
                return null;
            }
        }

        public static List<UIMenuElement> GetMenus() {
            var response = new List<UIMenuElement> {
                new UIMenuElement { Id = 1, ParentId = 0, Name = "marchivo", Label = "Archivo", Rol = "*" },
                new UIMenuElement { Id = 2, ParentId = 1, Name = "barchivo_abrir", Label = "Abrir" },
                new UIMenuElement { Id = 3, ParentId = 1, Name = "barchivo_crear", Label = "Crear" },
                new UIMenuElement { Id = 4, ParentId = 1, Name = "barchivo_empresa", Label = "Empresa" },
                new UIMenuElement { Id = 5, ParentId = 1, Name = "barchivo_configuracion", Label = "Configuración", Form = "Forms.General.ConfiguracionForm", Rol = "desarrollador,user" },
                new UIMenuElement { Id = 6, ParentId = 1, Name = "barchivo_salir", Label = "Salir", Rol = "*" },

                new UIMenuElement { Id = 7, ParentId = 0, Name = "memision", Label = "Emisión", Rol = "desarrollador,user" },
                new UIMenuElement { Id = 8, ParentId = 7, Name = "bemision_clientes", Label = "Clientes", Form = "Forms.Contribuyentes.ClientesCatalogoForm", Rol = "desarrollador,user" },
                new UIMenuElement { Id = 9, ParentId = 7, Name = "bemision_comprobante", Label = "Facturación", Form = "Forms.Comprobantes.ComprobantesEmitidosForm" , Rol = "desarrollador,user"},
                new UIMenuElement { Id = 10, ParentId = 7, Name = "bemision_retenciones", Label = "Retenciones", Form = "Forms.Retenciones.ComprobanteRetencionesForm" },

                new UIMenuElement { Id = 11, ParentId = 0, Name = "mrecepcion", Label = "Recepción" , Rol = "desarrollador,user"},
                new UIMenuElement { Id = 12, ParentId = 11, Name = "brecepci_contribuyente", Label = "Proveedor", Form = "Forms.Contribuyentes.ProveedoresCatalogoForm" , Rol = "desarrollador,user"},
                new UIMenuElement { Id = 13, ParentId = 11, Name = "brecepci_validador", Label = "Validador", Form = "Forms.Validador.ComprobanteValidadorForm" , Rol = "desarrollador,user"},
                new UIMenuElement { Id = 14, ParentId = 11, Name = "brecepci_comprobante", Label = "Comprobante", Form = "Forms.Comprobantes.ComprobantesRecibidosForm" , Rol = "desarrollador,user"},

                new UIMenuElement { Id = 15, ParentId = 0, Name = "mnomina", Label = "Nómina" , Rol = "desarrollador,user"},
                new UIMenuElement { Id = 16, ParentId = 15, Name = "mnomina_empleados", Label = "Empleados", Form = "Forms.Contribuyentes.EmpleadosCatalogoForm" , Rol = "desarrollador,user"},
                new UIMenuElement { Id = 17, ParentId = 15, Name = "mnomina_recibos", Label = "Recibos", Form = "Forms.Contribuyentes.ComprobantesNominaForm" },


                new UIMenuElement { Id = 18, ParentId = 0, Name = "mherramientas", Label = "Herramientas", Rol = "desarrollador,user" },
                new UIMenuElement { Id = 19, ParentId = 18, Name = "bherrami_repositorio", Label = "Repositorio SAT", Form = "Forms.Repositorio.RepositorioForm", Rol = "desarrollador,user" },
                new UIMenuElement { Id = 20, ParentId = 19, Name = "breposit_descarga1", Label = "Descarga", Form = "Forms.Repositorio.RepositorioForm", Rol = "desarrollador,user", IsEnable = true },
                new UIMenuElement { Id = 21, ParentId = 19, Name = "breposit_descarga2", Label = "Descarga", Form = "Forms.Repositorio.RepositorioForm", Rol = "desarrollador", IsEnable = false },
                new UIMenuElement { Id = 22, ParentId = 19, Name = "breposit_descarga3", Label = "Descarga de terceros", Form = "Forms.Repositorio.RepositorioForm", Rol = "desarrollador" },
                new UIMenuElement { Id = 23, ParentId = 18, Name = "bherrami_validador", Label = "Validador", Form = "Forms.Validador.ComprobanteValidadorForm", Rol = "desarrollador,user" },
                new UIMenuElement { Id = 24, ParentId = 18, Name = "bherrami_validarfc", Label = "Validación de RFC", Form = "Forms.Validador.ValidaRFCForm", Rol = "desarrollador,user" },
                new UIMenuElement { Id = 25, ParentId = 18, Name = "bherrami_valretencion", Label = "Validación de Retención", Form = "Forms.Validador.ValidaRetencionForm", Rol = "desarrollador,user" },
                new UIMenuElement { Id = 26, ParentId = 18, Name = "bherrami_certificado", Label = "Certificado" },
                new UIMenuElement { Id = 27, ParentId = 26, Name = "bherrami_cerinfo", Label = "Información", Form = "Forms.CertificadoForm" },
                new UIMenuElement { Id = 28, ParentId = 26, Name = "bherrami_cerdescarga", Label = "Descarga", Form = "Forms.CertificadoRecuperaForm" },
                new UIMenuElement { Id = 29, ParentId = 26, Name = "bherrami_cerverifica", Label = "Validación", Form = "Forms.CertificadoValidaForm" },
                new UIMenuElement { Id = 30, ParentId = 0, Name = "bherrami_metadata", Label = "Meta DATA SAT", Rol = "desarrollador,user" },
                new UIMenuElement { Id = 31, ParentId = 31, Name = "bherrami_metacomp", Label = "Comprobantes", Form = "Forms.RepositorioMetaForm", Rol = "desarrollador,user" },
                new UIMenuElement { Id = 32, ParentId = 32, Name = "bherrami_metareten", Label = "Retenciones", Form = "Forms.RepositorioMetaRetForm", Rol = "desarrollador,user" },
                new UIMenuElement { Id = 33, ParentId = 18, Name = "bherrami_cedulafis", Label = "Cedula Fiscal", Form = "Forms.CedulaForm", Rol = "desarrollado,user" },


                new UIMenuElement { Id = 91, ParentId = 0, Name = "mventana", Label = "Ventana", Rol = "*" },
                new UIMenuElement { Id = 92, ParentId = 0, Name = "mayuda", Label = "Ayuda", Rol = "*" },
                new UIMenuElement { Id = 93, ParentId = 24, Name = "bayuda_acercade", Label = "Acerca de ...", Rol = "*" }
            };

            return response;
        }

        /// <summary>
        /// obtener lista de meses
        /// </summary>
        public static List<MesModel> GetMeses() {
            return ((MesesEnum[])Enum.GetValues(typeof(MesesEnum))).Select(c => new MesModel((int)c, c.ToString())).ToList();
        }
    }
}
