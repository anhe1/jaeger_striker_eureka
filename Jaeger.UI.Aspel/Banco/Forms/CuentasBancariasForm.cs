﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Aspel.Banco;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Aspel.Banco.Forms {
    public partial class CuentasBancariasForm : Form {
        protected ICuentasBancariasService service;

        public CuentasBancariasForm() {
            InitializeComponent();
        }

        private void CuentasBancariasForm_Load(object sender, EventArgs e) {
            this.service = new CuentasBancariasService();
            this.dataGridView1.DataGridCommon();
        }

        #region barra de herramientas
        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            this.dataGridView1.DataSource = this.service.GetList();
        }

        private void ToolBar_ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion
    }
}
