﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Aspel.Banco;
using Jaeger.Domain.Aspel.Banco.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Aspel.Banco.Forms {
    public partial class MovimientosForm : Form {
        protected IMovimientosService service;
        private BindingList<MOVS01> movimientos;
        private BindingList<FormaPagoModel> formaPagos;
        private BindingList<COMD> comd;

        public MovimientosForm() {
            InitializeComponent();
        }

        private void MovimientosForm_Load(object sender, EventArgs e) {
            this.service = new MovimientosService();
            this.dataGridView1.DataGridCommon();
        }

        #region barra de herramientas
        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Consulta)) {
                espera.Text = "Consultando...";
                espera.ShowDialog(this);
            }
            var comboFormaPago = this.FormaPago as DataGridViewComboBoxColumn;
            comboFormaPago.DataSource = this.formaPagos;
            comboFormaPago.DisplayMember = "Descripcion";
            comboFormaPago.ValueMember = "NUM_REG";
            var comboComd = this.Descripcion as DataGridViewComboBoxColumn;
            comboComd.DataSource = this.comd;
            comboComd.DisplayMember = "CONCEP";
            comboComd.ValueMember = "CVE_CONCEP";
            this.dataGridView1.DataSource = this.movimientos;
        }

        private void ToolBar_ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        private void Consulta() {
            this.movimientos = this.service.GetList();
            this.formaPagos = this.service.GetFormaPago();
            this.comd = this.service.GetComd();
        }
    }
}
