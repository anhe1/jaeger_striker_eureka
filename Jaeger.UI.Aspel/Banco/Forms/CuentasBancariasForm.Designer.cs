﻿namespace Jaeger.UI.Aspel.Banco.Forms {
    partial class CuentasBancariasForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CuentasBancariasForm));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.Clave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumCuenta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Banco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLABE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SigCheque = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiaCorte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Saldo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Clave,
            this.NumCuenta,
            this.Banco,
            this.CLABE,
            this.Estado,
            this.SigCheque,
            this.DiaCorte,
            this.Saldo});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(800, 425);
            this.dataGridView1.TabIndex = 1;
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "Cuentas Bancarias";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = true;
            this.ToolBar.ShowGuardar = false;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.ShowRemover = true;
            this.ToolBar.Size = new System.Drawing.Size(800, 25);
            this.ToolBar.TabIndex = 2;
            this.ToolBar.ButtonActualizar_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonActualizar_Click);
            this.ToolBar.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.ToolBar_ButtonCerrar_Click);
            // 
            // Clave
            // 
            this.Clave.DataPropertyName = "NUM_REG";
            this.Clave.HeaderText = "Clave";
            this.Clave.Name = "Clave";
            this.Clave.Width = 50;
            // 
            // NumCuenta
            // 
            this.NumCuenta.DataPropertyName = "NUM_CTA";
            this.NumCuenta.HeaderText = "Núm. Cuenta";
            this.NumCuenta.Name = "NumCuenta";
            // 
            // Banco
            // 
            this.Banco.DataPropertyName = "BANCO";
            this.Banco.HeaderText = "Banco";
            this.Banco.Name = "Banco";
            this.Banco.Width = 200;
            // 
            // CLABE
            // 
            this.CLABE.DataPropertyName = "CLABE";
            this.CLABE.HeaderText = "CLABE";
            this.CLABE.Name = "CLABE";
            this.CLABE.Width = 150;
            // 
            // Estado
            // 
            this.Estado.DataPropertyName = "STATUS";
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            this.Estado.Width = 50;
            // 
            // SigCheque
            // 
            this.SigCheque.DataPropertyName = "SIG_CHEQUE";
            this.SigCheque.HeaderText = "Sig. Cheque";
            this.SigCheque.Name = "SigCheque";
            this.SigCheque.Width = 75;
            // 
            // DiaCorte
            // 
            this.DiaCorte.DataPropertyName = "DIA_CORTE";
            this.DiaCorte.HeaderText = "Día de corte";
            this.DiaCorte.Name = "DiaCorte";
            this.DiaCorte.Width = 75;
            // 
            // Saldo
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            this.Saldo.DefaultCellStyle = dataGridViewCellStyle1;
            this.Saldo.HeaderText = "Saldo";
            this.Saldo.Name = "Saldo";
            this.Saldo.Width = 75;
            // 
            // CuentasBancariasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.ToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CuentasBancariasForm";
            this.Text = "Cuentas Bancarias";
            this.Load += new System.EventHandler(this.CuentasBancariasForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private Common.Forms.ToolBarStandarControl ToolBar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Clave;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumCuenta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Banco;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLABE;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn SigCheque;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiaCorte;
        private System.Windows.Forms.DataGridViewTextBoxColumn Saldo;
    }
}