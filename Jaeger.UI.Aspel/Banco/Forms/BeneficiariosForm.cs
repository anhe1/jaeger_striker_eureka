﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Aspel.Banco;
using Jaeger.Domain.Aspel.Banco.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Aspel.Banco.Forms {
    public partial class BeneficiariosForm : Form {
        protected IBeneficiarioService service;
        private BindingList<BeneficiarioModel> beneficiarios;

        public BeneficiariosForm() {
            InitializeComponent();
        }

        private void BeneficiariosForm_Load(object sender, EventArgs e) {
            this.service = new BeneficiarioService();
            this.dataGridBeneficiarios.DataGridCommon();
        }

        #region barra de herramientas
        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            using(var espera = new WaitingForm(this.Consultar)) {
                espera.Text = "Consultando información, espere ...";
                espera.ShowDialog(this);
            }
            this.dataGridBeneficiarios.DataSource = this.beneficiarios;
        }

        private void ToolBarButtonEditar_Click(object sender, EventArgs e) {
            if (this.dataGridBeneficiarios.CurrentRow != null) {
                var seleccionado = this.dataGridBeneficiarios.CurrentRow.DataBoundItem as BeneficiarioModel;
                if (seleccionado != null) {
                    using (var editar = new BeneficiarioForm(seleccionado)) {
                        editar.ShowDialog(this);
                    }
                }
            }
        }

        private void ToolBar_ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        private void Consultar() {
            this.beneficiarios = this.service.GetList();
        }
    }
}
