﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Aspel.Banco;
using Jaeger.Domain.Aspel.Banco.Entities;

namespace Jaeger.UI.Aspel.Banco.Forms {
    public partial class BeneficiarioForm : Form {
        protected IBeneficiarioService service;
        private BeneficiarioModel beneficiario;

        public BeneficiarioForm(BeneficiarioModel beneficiario) {
            InitializeComponent();
            this.beneficiario = beneficiario;
        }

        private void BeneficiarioForm_Load(object sender, EventArgs e) {
            this.service = new BeneficiarioService();
            if (this.beneficiario == null)
                this.beneficiario = new BeneficiarioModel();
            this.CreateBinding();
        }

        private void CreateBinding() {
            this.textBoxClave.DataBindings.Clear();
            this.textBoxClave.DataBindings.Add("Text", this.beneficiario, "NUM_REG", true, DataSourceUpdateMode.OnPropertyChanged);
            this.textBoxNombre.DataBindings.Clear();
            this.textBoxNombre.DataBindings.Add("Text", this.beneficiario, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);
            this.textBoxRFC.DataBindings.Clear();
            this.textBoxRFC.DataBindings.Add("Text", this.beneficiario, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.textBoxCtaContable.DataBindings.Clear();
            this.textBoxCtaContable.DataBindings.Add("Text", this.beneficiario, "CTA_CONTAB", true, DataSourceUpdateMode.OnPropertyChanged);

            this.textBoxReferencia.DataBindings.Clear();
            this.textBoxReferencia.DataBindings.Add("Text", this.beneficiario, "REFERENCIA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.textBoxCuenta.DataBindings.Clear();
            this.textBoxCuenta.DataBindings.Add("Text", this.beneficiario, "CUENTA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.textBoxBancoRFC.DataBindings.Clear();
            this.textBoxBancoRFC.DataBindings.Add("Text", this.beneficiario, "RFCBANCO", true, DataSourceUpdateMode.OnPropertyChanged);

            this.textBoxBancoSucursal.DataBindings.Clear();
            this.textBoxBancoSucursal.DataBindings.Add("Text", this.beneficiario, "SUCURSAL", true, DataSourceUpdateMode.OnPropertyChanged);

            this.textBoxCLABE.DataBindings.Clear();
            this.textBoxCLABE.DataBindings.Add("Text", this.beneficiario, "CLABE", true, DataSourceUpdateMode.OnPropertyChanged);

            this.comboBoxBancoClave.DataBindings.Add("Text", this.beneficiario, "BANCO", true, DataSourceUpdateMode.OnPropertyChanged);

            this.textBoxBancoNombre.DataBindings.Clear();
            this.textBoxBancoNombre.DataBindings.Add("Text", this.beneficiario, "BANCODESC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.checkBoxExtranjero.DataBindings.Clear();
            this.checkBoxExtranjero.DataBindings.Add("Checked", this.beneficiario, "ESBANCOEXT", true, DataSourceUpdateMode.OnPropertyChanged);

            this.textBoxInfoGeneral.DataBindings.Clear();
            this.textBoxInfoGeneral.DataBindings.Add("Text", this.beneficiario, "INF_GENERAL", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void Consulta() {

        }

        private void Guardar() {

        }

        private void buttonGuardar_Click(object sender, EventArgs e) {

        }

        private void buttonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
