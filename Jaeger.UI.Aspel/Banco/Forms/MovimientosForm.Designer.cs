﻿namespace Jaeger.UI.Aspel.Banco.Forms {
    partial class MovimientosForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MovimientosForm));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ClaveConcepto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaAplicacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Movimiento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FormaPago = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.NoCheque = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Referencia1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Abono = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cargo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Saldo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBar1CommonControl();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ClaveConcepto,
            this.Fecha,
            this.FechaAplicacion,
            this.Descripcion,
            this.Movimiento,
            this.Estado,
            this.FormaPago,
            this.NoCheque,
            this.Referencia1,
            this.ANombre,
            this.RFC,
            this.Abono,
            this.Cargo,
            this.Saldo});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1941, 425);
            this.dataGridView1.TabIndex = 3;
            // 
            // ClaveConcepto
            // 
            this.ClaveConcepto.DataPropertyName = "CVE_CONCEP";
            this.ClaveConcepto.HeaderText = "Clave concepto";
            this.ClaveConcepto.Name = "ClaveConcepto";
            // 
            // Fecha
            // 
            this.Fecha.DataPropertyName = "FECHA";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Format = "dd MMM yy";
            this.Fecha.DefaultCellStyle = dataGridViewCellStyle1;
            this.Fecha.HeaderText = "Fecha";
            this.Fecha.Name = "Fecha";
            this.Fecha.Width = 85;
            // 
            // FechaAplicacion
            // 
            this.FechaAplicacion.DataPropertyName = "F_COBRO";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "dd MMM yy";
            this.FechaAplicacion.DefaultCellStyle = dataGridViewCellStyle2;
            this.FechaAplicacion.HeaderText = "Fecha de Aplicación";
            this.FechaAplicacion.Name = "FechaAplicacion";
            this.FechaAplicacion.Width = 85;
            // 
            // Descripcion
            // 
            this.Descripcion.DataPropertyName = "CVE_CONCEP";
            this.Descripcion.HeaderText = "Descripción";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Width = 150;
            // 
            // Movimiento
            // 
            this.Movimiento.HeaderText = "Movimiento";
            this.Movimiento.Name = "Movimiento";
            // 
            // Estado
            // 
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            this.Estado.Width = 75;
            // 
            // FormaPago
            // 
            this.FormaPago.DataPropertyName = "FormaPago";
            this.FormaPago.HeaderText = "Forma de pago";
            this.FormaPago.Name = "FormaPago";
            this.FormaPago.Width = 200;
            // 
            // NoCheque
            // 
            this.NoCheque.HeaderText = "Núm. Cheque";
            this.NoCheque.Name = "NoCheque";
            // 
            // Referencia1
            // 
            this.Referencia1.DataPropertyName = "Referencia1";
            this.Referencia1.HeaderText = "Referencia 1";
            this.Referencia1.Name = "Referencia1";
            // 
            // ANombre
            // 
            this.ANombre.DataPropertyName = "Anombrede";
            this.ANombre.HeaderText = "A nombre de";
            this.ANombre.Name = "ANombre";
            this.ANombre.Width = 200;
            // 
            // RFC
            // 
            this.RFC.DataPropertyName = "RFC";
            this.RFC.HeaderText = "RFC";
            this.RFC.Name = "RFC";
            // 
            // Abono
            // 
            this.Abono.HeaderText = "Abono";
            this.Abono.Name = "Abono";
            // 
            // Cargo
            // 
            this.Cargo.HeaderText = "Cargo";
            this.Cargo.Name = "Cargo";
            // 
            // Saldo
            // 
            this.Saldo.HeaderText = "Saldo";
            this.Saldo.Name = "Saldo";
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowCancelar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = true;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.Size = new System.Drawing.Size(1941, 25);
            this.ToolBar.TabIndex = 4;
            this.ToolBar.Actualizar.Click += this.ToolBarButtonActualizar_Click;
            this.ToolBar.Cerrar.Click += this.ToolBar_ButtonCerrar_Click;
            // 
            // MovimientosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1941, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.ToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MovimientosForm";
            this.Text = "Movimientos";
            this.Load += new System.EventHandler(this.MovimientosForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveConcepto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaAplicacion;
        private System.Windows.Forms.DataGridViewComboBoxColumn Descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Movimiento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewComboBoxColumn FormaPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoCheque;
        private System.Windows.Forms.DataGridViewTextBoxColumn Referencia1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Abono;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cargo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Saldo;
        private Common.Forms.ToolBar1CommonControl ToolBar;
    }
}