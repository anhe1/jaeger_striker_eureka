﻿namespace Jaeger.UI.Aspel.Banco.Forms {
    partial class BeneficiarioForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BeneficiarioForm));
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxClave = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxReferencia = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxNombre = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxCuenta = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxRFC = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxBancoRFC = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxCtaContable = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxBancoSucursal = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxTipo = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxBancoClave = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxCLABE = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxBancoNombre = new System.Windows.Forms.TextBox();
            this.checkBoxExtranjero = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxInfoGeneral = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonGuardar = new System.Windows.Forms.Button();
            this.buttonCerrar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Clave:";
            // 
            // textBoxClave
            // 
            this.textBoxClave.Location = new System.Drawing.Point(79, 23);
            this.textBoxClave.Name = "textBoxClave";
            this.textBoxClave.ReadOnly = true;
            this.textBoxClave.Size = new System.Drawing.Size(100, 20);
            this.textBoxClave.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Referencia:";
            // 
            // textBoxReferencia
            // 
            this.textBoxReferencia.Location = new System.Drawing.Point(88, 17);
            this.textBoxReferencia.MaxLength = 20;
            this.textBoxReferencia.Name = "textBoxReferencia";
            this.textBoxReferencia.Size = new System.Drawing.Size(181, 20);
            this.textBoxReferencia.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nombre:";
            // 
            // textBoxNombre
            // 
            this.textBoxNombre.Location = new System.Drawing.Point(79, 49);
            this.textBoxNombre.MaxLength = 60;
            this.textBoxNombre.Name = "textBoxNombre";
            this.textBoxNombre.Size = new System.Drawing.Size(245, 20);
            this.textBoxNombre.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(282, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Cuenta:";
            // 
            // textBoxCuenta
            // 
            this.textBoxCuenta.Location = new System.Drawing.Point(326, 17);
            this.textBoxCuenta.MaxLength = 30;
            this.textBoxCuenta.Name = "textBoxCuenta";
            this.textBoxCuenta.Size = new System.Drawing.Size(161, 20);
            this.textBoxCuenta.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(330, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "RFC:";
            // 
            // textBoxRFC
            // 
            this.textBoxRFC.Location = new System.Drawing.Point(367, 49);
            this.textBoxRFC.MaxLength = 15;
            this.textBoxRFC.Name = "textBoxRFC";
            this.textBoxRFC.Size = new System.Drawing.Size(127, 20);
            this.textBoxRFC.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "RFC del Banco:";
            // 
            // textBoxBancoRFC
            // 
            this.textBoxBancoRFC.Location = new System.Drawing.Point(96, 43);
            this.textBoxBancoRFC.MaxLength = 15;
            this.textBoxBancoRFC.Name = "textBoxBancoRFC";
            this.textBoxBancoRFC.Size = new System.Drawing.Size(173, 20);
            this.textBoxBancoRFC.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Cta. Contable:";
            // 
            // textBoxCtaContable
            // 
            this.textBoxCtaContable.Location = new System.Drawing.Point(96, 75);
            this.textBoxCtaContable.MaxLength = 40;
            this.textBoxCtaContable.Name = "textBoxCtaContable";
            this.textBoxCtaContable.Size = new System.Drawing.Size(152, 20);
            this.textBoxCtaContable.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(282, 47);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Sucursal:";
            // 
            // textBoxBancoSucursal
            // 
            this.textBoxBancoSucursal.Location = new System.Drawing.Point(339, 43);
            this.textBoxBancoSucursal.MaxLength = 30;
            this.textBoxBancoSucursal.Name = "textBoxBancoSucursal";
            this.textBoxBancoSucursal.Size = new System.Drawing.Size(148, 20);
            this.textBoxBancoSucursal.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(331, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Tipo:";
            // 
            // comboBoxTipo
            // 
            this.comboBoxTipo.FormattingEnabled = true;
            this.comboBoxTipo.Location = new System.Drawing.Point(368, 75);
            this.comboBoxTipo.Name = "comboBoxTipo";
            this.comboBoxTipo.Size = new System.Drawing.Size(127, 21);
            this.comboBoxTipo.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(282, 73);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Clave del Banco:";
            // 
            // comboBoxBancoClave
            // 
            this.comboBoxBancoClave.FormattingEnabled = true;
            this.comboBoxBancoClave.Location = new System.Drawing.Point(366, 69);
            this.comboBoxBancoClave.Name = "comboBoxBancoClave";
            this.comboBoxBancoClave.Size = new System.Drawing.Size(121, 21);
            this.comboBoxBancoClave.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 73);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "CLABE:";
            // 
            // textBoxCLABE
            // 
            this.textBoxCLABE.Location = new System.Drawing.Point(58, 69);
            this.textBoxCLABE.MaxLength = 30;
            this.textBoxCLABE.Name = "textBoxCLABE";
            this.textBoxCLABE.Size = new System.Drawing.Size(211, 20);
            this.textBoxCLABE.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 99);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Banco:";
            // 
            // textBoxBancoNombre
            // 
            this.textBoxBancoNombre.Location = new System.Drawing.Point(58, 95);
            this.textBoxBancoNombre.MaxLength = 20;
            this.textBoxBancoNombre.Name = "textBoxBancoNombre";
            this.textBoxBancoNombre.Size = new System.Drawing.Size(211, 20);
            this.textBoxBancoNombre.TabIndex = 1;
            // 
            // checkBoxExtranjero
            // 
            this.checkBoxExtranjero.AutoSize = true;
            this.checkBoxExtranjero.Location = new System.Drawing.Point(285, 97);
            this.checkBoxExtranjero.Name = "checkBoxExtranjero";
            this.checkBoxExtranjero.Size = new System.Drawing.Size(120, 17);
            this.checkBoxExtranjero.TabIndex = 3;
            this.checkBoxExtranjero.Text = "Es banco extranjero";
            this.checkBoxExtranjero.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 128);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(103, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Información general:";
            // 
            // textBoxInfoGeneral
            // 
            this.textBoxInfoGeneral.Location = new System.Drawing.Point(117, 125);
            this.textBoxInfoGeneral.MaxLength = 250;
            this.textBoxInfoGeneral.Name = "textBoxInfoGeneral";
            this.textBoxInfoGeneral.Size = new System.Drawing.Size(370, 20);
            this.textBoxInfoGeneral.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxClave);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboBoxTipo);
            this.groupBox1.Controls.Add(this.textBoxNombre);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBoxRFC);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.textBoxCtaContable);
            this.groupBox1.Location = new System.Drawing.Point(12, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(507, 117);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos del beneficiario";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBoxReferencia);
            this.groupBox2.Controls.Add(this.checkBoxExtranjero);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.comboBoxBancoClave);
            this.groupBox2.Controls.Add(this.textBoxCuenta);
            this.groupBox2.Controls.Add(this.textBoxInfoGeneral);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.textBoxBancoRFC);
            this.groupBox2.Controls.Add(this.textBoxBancoNombre);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.textBoxCLABE);
            this.groupBox2.Controls.Add(this.textBoxBancoSucursal);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(12, 167);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(507, 162);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos del banco";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(536, 38);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.Enabled = false;
            this.buttonGuardar.Location = new System.Drawing.Point(363, 335);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(75, 23);
            this.buttonGuardar.TabIndex = 7;
            this.buttonGuardar.Text = "Guardar";
            this.buttonGuardar.UseVisualStyleBackColor = true;
            this.buttonGuardar.Click += new System.EventHandler(this.buttonGuardar_Click);
            // 
            // buttonCerrar
            // 
            this.buttonCerrar.Location = new System.Drawing.Point(444, 335);
            this.buttonCerrar.Name = "buttonCerrar";
            this.buttonCerrar.Size = new System.Drawing.Size(75, 23);
            this.buttonCerrar.TabIndex = 8;
            this.buttonCerrar.Text = "Cerrar";
            this.buttonCerrar.UseVisualStyleBackColor = true;
            this.buttonCerrar.Click += new System.EventHandler(this.buttonCerrar_Click);
            // 
            // BeneficiarioForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 369);
            this.Controls.Add(this.buttonGuardar);
            this.Controls.Add(this.buttonCerrar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BeneficiarioForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Beneficiario";
            this.Load += new System.EventHandler(this.BeneficiarioForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxClave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxReferencia;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxNombre;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxCuenta;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxRFC;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxBancoRFC;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxCtaContable;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxBancoSucursal;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxTipo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBoxBancoClave;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxCLABE;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxBancoNombre;
        private System.Windows.Forms.CheckBox checkBoxExtranjero;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxInfoGeneral;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonGuardar;
        private System.Windows.Forms.Button buttonCerrar;
    }
}