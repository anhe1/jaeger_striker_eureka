﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.Aplication.Aspel.COI;

namespace Jaeger.UI.Aspel.COI8.Forms {
    public partial class RubrosCatalogoForm : Form {
        protected IRubrosService service;

        public RubrosCatalogoForm() {
            InitializeComponent();
        }

        private void CoiRubrosForm_Load(object sender, EventArgs e) {
            this.service = new RubrosService();
            this.dataGridBeneficiarios.DataSource = this.service.GetList();
        }

        private void ToolBar_ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
