﻿namespace Jaeger.UI.Aspel.COI8.Forms {
    partial class RubrosCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dataGridBeneficiarios = new System.Windows.Forms.DataGridView();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridBeneficiarios)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridBeneficiarios
            // 
            this.dataGridBeneficiarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridBeneficiarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridBeneficiarios.Location = new System.Drawing.Point(0, 25);
            this.dataGridBeneficiarios.Name = "dataGridBeneficiarios";
            this.dataGridBeneficiarios.Size = new System.Drawing.Size(800, 425);
            this.dataGridBeneficiarios.TabIndex = 3;
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "Titulo";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = true;
            this.ToolBar.ShowGuardar = true;
            this.ToolBar.ShowHerramientas = true;
            this.ToolBar.ShowImprimir = true;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.ShowRemover = true;
            this.ToolBar.Size = new System.Drawing.Size(800, 25);
            this.ToolBar.TabIndex = 4;
            this.ToolBar.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.ToolBar_ButtonCerrar_Click);
            // 
            // RubrosCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridBeneficiarios);
            this.Controls.Add(this.ToolBar);
            this.Name = "RubrosCatalogoForm";
            this.Text = "COI-Rubros";
            this.Load += new System.EventHandler(this.CoiRubrosForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridBeneficiarios)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridBeneficiarios;
        private Common.Forms.ToolBarStandarControl ToolBar;
    }
}