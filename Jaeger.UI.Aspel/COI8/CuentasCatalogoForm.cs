﻿using Jaeger.Aplication.Aspel.COI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jaeger.UI.Aspel.COI8.Forms {
    public partial class CuentasCatalogoForm : Form {
        protected ICuentaContableService service;
        public CuentasCatalogoForm() {
            InitializeComponent();
        }

        private void CuentasCatalogoForm_Load(object sender, EventArgs e) {
            this.service = new CuentaContableService();
            this.ToolBar.Actualizar.Click += this.ToolBar_ButtonActualizar_Click;
        }

        private void ToolBar_ButtonActualizar_Click(object sender, EventArgs e) {
            this.dataGridView1.DataSource = this.service.GetList(this.ToolBar.GetEjercicio());
        }
    }
}
