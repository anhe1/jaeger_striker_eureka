﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.Aplication.Aspel.COI;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Aspel.Coi80.Entities;

namespace Jaeger.UI.Aspel.COI8.Forms {
    public partial class PolizasCatalogoForm : Form {
        protected IPolizaService service;
        private BindingList<PolizaDetailModel> polizas;

        public PolizasCatalogoForm() {
            InitializeComponent();
        }

        private void PolizasForm_Load(object sender, EventArgs e) {
            this.service = new PolizaService();
            this.gridPoliza.DataGridCommon();
            this.gridAuxiliar.DataGridCommon();
        }

        private void ToolBar_ButtonActualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.gridAuxiliar.DataMember = "Auxiliares";
            this.gridPoliza.DataSource = this.polizas;
            this.gridAuxiliar.DataSource = this.polizas;
        }

        private void ToolBar_ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consultar() {
            this.polizas = this.service.GetPolizas(this.ToolBar.GetMes(), this.ToolBar.GetEjercicio());
        }
    }
}
