﻿namespace Jaeger.UI.Aspel.COI8.Forms {
    partial class PolizasCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PolizasCatalogoForm));
            this.gridPoliza = new System.Windows.Forms.DataGridView();
            this.Tipo_Poli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Num_Poliz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LogAudita = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha_Pol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Origen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Concep_po = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Contabiliz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridAuxiliar = new System.Windows.Forms.DataGridView();
            this.Num_Cta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Concep_Po1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha_Pol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipCambio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Debe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Haber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBar1CommonControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridPoliza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAuxiliar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridPoliza
            // 
            this.gridPoliza.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPoliza.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Tipo_Poli,
            this.Num_Poliz,
            this.LogAudita,
            this.Fecha_Pol,
            this.Origen,
            this.Concep_po,
            this.Contabiliz});
            this.gridPoliza.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPoliza.Location = new System.Drawing.Point(0, 0);
            this.gridPoliza.Name = "gridPoliza";
            this.gridPoliza.Size = new System.Drawing.Size(974, 160);
            this.gridPoliza.TabIndex = 1;
            // 
            // Tipo_Poli
            // 
            this.Tipo_Poli.DataPropertyName = "Tipo_Poli";
            this.Tipo_Poli.HeaderText = "Tipo";
            this.Tipo_Poli.Name = "Tipo_Poli";
            this.Tipo_Poli.ReadOnly = true;
            this.Tipo_Poli.Width = 50;
            // 
            // Num_Poliz
            // 
            this.Num_Poliz.DataPropertyName = "Numero";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Format = "00000";
            dataGridViewCellStyle1.NullValue = "00000";
            this.Num_Poliz.DefaultCellStyle = dataGridViewCellStyle1;
            this.Num_Poliz.HeaderText = "# Núm.";
            this.Num_Poliz.MaxInputLength = 5;
            this.Num_Poliz.Name = "Num_Poliz";
            this.Num_Poliz.ReadOnly = true;
            this.Num_Poliz.Width = 85;
            // 
            // LogAudita
            // 
            this.LogAudita.DataPropertyName = "LogAudita";
            this.LogAudita.HeaderText = "Auditada";
            this.LogAudita.Name = "LogAudita";
            this.LogAudita.ReadOnly = true;
            this.LogAudita.Width = 50;
            // 
            // Fecha_Pol
            // 
            this.Fecha_Pol.DataPropertyName = "Fecha_Pol";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "dd MMM yy";
            this.Fecha_Pol.DefaultCellStyle = dataGridViewCellStyle2;
            this.Fecha_Pol.HeaderText = "Fecha";
            this.Fecha_Pol.Name = "Fecha_Pol";
            this.Fecha_Pol.ReadOnly = true;
            this.Fecha_Pol.Width = 75;
            // 
            // Origen
            // 
            this.Origen.DataPropertyName = "Origen";
            this.Origen.HeaderText = "Origen";
            this.Origen.Name = "Origen";
            this.Origen.ReadOnly = true;
            this.Origen.Width = 65;
            // 
            // Concep_po
            // 
            this.Concep_po.DataPropertyName = "Concep_po";
            this.Concep_po.HeaderText = "Concepto";
            this.Concep_po.Name = "Concep_po";
            this.Concep_po.ReadOnly = true;
            this.Concep_po.Width = 350;
            // 
            // Contabiliz
            // 
            this.Contabiliz.DataPropertyName = "Contabiliz";
            this.Contabiliz.HeaderText = "Contabilizada";
            this.Contabiliz.Name = "Contabiliz";
            this.Contabiliz.ReadOnly = true;
            this.Contabiliz.Width = 75;
            // 
            // gridAuxiliar
            // 
            this.gridAuxiliar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridAuxiliar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Num_Cta,
            this.Concep_Po1,
            this.Fecha_Pol1,
            this.TipCambio,
            this.Debe,
            this.Haber});
            this.gridAuxiliar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridAuxiliar.Location = new System.Drawing.Point(0, 0);
            this.gridAuxiliar.Name = "gridAuxiliar";
            this.gridAuxiliar.Size = new System.Drawing.Size(974, 326);
            this.gridAuxiliar.TabIndex = 1;
            // 
            // Num_Cta
            // 
            this.Num_Cta.DataPropertyName = "NumeroCuenta";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Num_Cta.DefaultCellStyle = dataGridViewCellStyle3;
            this.Num_Cta.HeaderText = "Núm. Cuenta";
            this.Num_Cta.Name = "Num_Cta";
            this.Num_Cta.ReadOnly = true;
            this.Num_Cta.Width = 155;
            // 
            // Concep_Po1
            // 
            this.Concep_Po1.DataPropertyName = "Concep_Po";
            this.Concep_Po1.HeaderText = "Concepto";
            this.Concep_Po1.Name = "Concep_Po1";
            this.Concep_Po1.ReadOnly = true;
            this.Concep_Po1.Width = 300;
            // 
            // Fecha_Pol1
            // 
            this.Fecha_Pol1.DataPropertyName = "Fecha_Pol";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Format = "dd MMM yy";
            this.Fecha_Pol1.DefaultCellStyle = dataGridViewCellStyle4;
            this.Fecha_Pol1.HeaderText = "Fecha";
            this.Fecha_Pol1.Name = "Fecha_Pol1";
            this.Fecha_Pol1.ReadOnly = true;
            this.Fecha_Pol1.Width = 75;
            // 
            // TipCambio
            // 
            this.TipCambio.DataPropertyName = "TipCambio";
            this.TipCambio.HeaderText = "Tip. Cambio";
            this.TipCambio.Name = "TipCambio";
            this.TipCambio.ReadOnly = true;
            // 
            // Debe
            // 
            this.Debe.DataPropertyName = "Debe";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            this.Debe.DefaultCellStyle = dataGridViewCellStyle5;
            this.Debe.HeaderText = "Debe";
            this.Debe.Name = "Debe";
            this.Debe.ReadOnly = true;
            // 
            // Haber
            // 
            this.Haber.DataPropertyName = "Haber";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            this.Haber.DefaultCellStyle = dataGridViewCellStyle6;
            this.Haber.HeaderText = "Haber";
            this.Haber.Name = "Haber";
            this.Haber.ReadOnly = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gridPoliza);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gridAuxiliar);
            this.splitContainer1.Size = new System.Drawing.Size(974, 490);
            this.splitContainer1.SplitterDistance = 160;
            this.splitContainer1.TabIndex = 3;
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowCancelar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = true;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.Size = new System.Drawing.Size(974, 25);
            this.ToolBar.TabIndex = 2;
            this.ToolBar.Actualizar.Click += this.ToolBar_ButtonActualizar_Click;
            this.ToolBar.Cerrar.Click += this.ToolBar_ButtonCerrar_Click;
            // 
            // PolizasCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 515);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.ToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PolizasCatalogoForm";
            this.Text = "COI-Pólizas";
            this.Load += new System.EventHandler(this.PolizasForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridPoliza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAuxiliar)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView gridPoliza;
        private System.Windows.Forms.DataGridView gridAuxiliar;
        private Common.Forms.ToolBar1CommonControl ToolBar;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipo_Poli;
        private System.Windows.Forms.DataGridViewTextBoxColumn Num_Poliz;
        private System.Windows.Forms.DataGridViewTextBoxColumn LogAudita;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha_Pol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Origen;
        private System.Windows.Forms.DataGridViewTextBoxColumn Concep_po;
        private System.Windows.Forms.DataGridViewTextBoxColumn Contabiliz;
        private System.Windows.Forms.DataGridViewTextBoxColumn Num_Cta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Concep_Po1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha_Pol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipCambio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Debe;
        private System.Windows.Forms.DataGridViewTextBoxColumn Haber;
    }
}