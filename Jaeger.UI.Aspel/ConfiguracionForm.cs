﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Aspel.Base;

namespace Jaeger.UI.Aspel {
    public partial class ConfiguracionForm : Form {
        public ConfiguracionForm() {
            InitializeComponent();
        }

        private void ConfiguracionForm_Load(object sender, EventArgs e) {
            this.propertyGrid1.SelectedObject = ConfigService.Synapsis;
        }

        private void ButtonGuardar_Click(object sender, EventArgs e) {
            ConfigService.Save();
        }

        private void ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void buttonAcercade_Click(object sender, EventArgs e) {
            var _form_acercade = new Aspel.AboutBoxForm();
            _form_acercade.ShowDialog(this);
        }
    }
}
