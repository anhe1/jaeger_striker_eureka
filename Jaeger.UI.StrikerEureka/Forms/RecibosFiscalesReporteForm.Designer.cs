﻿namespace Jaeger.Views
{
    partial class RecibosFiscalesReporteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RecibosFiscalesReporteForm));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.CommandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabelPeriodo = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarButtonPeriodo = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarLabelEjercicio = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarButtonEjercicio = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarButtonActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarButtonExportar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonDescargar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.Waiting = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsSpinnerWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            this.GridData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Waiting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandBar
            // 
            this.CommandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBar.Location = new System.Drawing.Point(0, 0);
            this.CommandBar.Name = "CommandBar";
            this.CommandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement});
            this.CommandBar.Size = new System.Drawing.Size(1147, 38);
            this.CommandBar.TabIndex = 0;
            // 
            // commandBarRowElement
            // 
            this.commandBarRowElement.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement.Name = "commandBarRowElement";
            this.commandBarRowElement.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "commandBarStripElement1";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelPeriodo,
            this.ToolBarButtonPeriodo,
            this.ToolBarLabelEjercicio,
            this.ToolBarButtonEjercicio,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonFiltro,
            this.ToolBarButtonExportar,
            this.ToolBarButtonDescargar,
            this.ToolBarButtonCerrar});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            // 
            // ToolBarLabelPeriodo
            // 
            this.ToolBarLabelPeriodo.DisplayName = "Etiqueta Periodo";
            this.ToolBarLabelPeriodo.Name = "ToolBarLabelPeriodo";
            this.ToolBarLabelPeriodo.Text = "Ejercicio:";
            // 
            // ToolBarButtonPeriodo
            // 
            this.ToolBarButtonPeriodo.DefaultItem = null;
            this.ToolBarButtonPeriodo.DisplayName = "Periodo";
            this.ToolBarButtonPeriodo.DrawImage = false;
            this.ToolBarButtonPeriodo.DrawText = true;
            this.ToolBarButtonPeriodo.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarButtonPeriodo.Image")));
            this.ToolBarButtonPeriodo.MinSize = new System.Drawing.Size(100, 0);
            this.ToolBarButtonPeriodo.Name = "ToolBarButtonPeriodo";
            this.ToolBarButtonPeriodo.Text = "Selecciona";
            // 
            // ToolBarLabelEjercicio
            // 
            this.ToolBarLabelEjercicio.DisplayName = "Etiqueta Ejercicio";
            this.ToolBarLabelEjercicio.Name = "ToolBarLabelEjercicio";
            this.ToolBarLabelEjercicio.Text = "Periodo:";
            // 
            // ToolBarButtonEjercicio
            // 
            this.ToolBarButtonEjercicio.DefaultItem = null;
            this.ToolBarButtonEjercicio.DisplayName = "Ejercicio";
            this.ToolBarButtonEjercicio.DrawImage = false;
            this.ToolBarButtonEjercicio.DrawText = true;
            this.ToolBarButtonEjercicio.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarButtonEjercicio.Image")));
            this.ToolBarButtonEjercicio.MinSize = new System.Drawing.Size(100, 0);
            this.ToolBarButtonEjercicio.Name = "ToolBarButtonEjercicio";
            this.ToolBarButtonEjercicio.Text = "Selecciona";
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.DisplayName = "Actualizar";
            this.ToolBarButtonActualizar.DrawText = true;
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_actualizar;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.ToolBarButtonActualizar_Click);
            // 
            // ToolBarButtonFiltro
            // 
            this.ToolBarButtonFiltro.DisplayName = "commandBarToggleButton1";
            this.ToolBarButtonFiltro.DrawText = true;
            this.ToolBarButtonFiltro.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_filtrar;
            this.ToolBarButtonFiltro.Name = "ToolBarButtonFiltro";
            this.ToolBarButtonFiltro.Text = "Filtrar";
            this.ToolBarButtonFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonFiltro.Click += new System.EventHandler(this.ToolBarButtonFiltro_Click);
            // 
            // ToolBarButtonExportar
            // 
            this.ToolBarButtonExportar.DisplayName = "Exportar";
            this.ToolBarButtonExportar.DrawText = true;
            this.ToolBarButtonExportar.Enabled = false;
            this.ToolBarButtonExportar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_excel;
            this.ToolBarButtonExportar.Name = "ToolBarButtonExportar";
            this.ToolBarButtonExportar.Text = "Exportar";
            this.ToolBarButtonExportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonExportar.Click += new System.EventHandler(this.ToolBarButtonExportar_Click);
            // 
            // ToolBarButtonDescargar
            // 
            this.ToolBarButtonDescargar.DisplayName = "PrePoliza";
            this.ToolBarButtonDescargar.DrawText = true;
            this.ToolBarButtonDescargar.Enabled = false;
            this.ToolBarButtonDescargar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_exportar;
            this.ToolBarButtonDescargar.Name = "ToolBarButtonDescargar";
            this.ToolBarButtonDescargar.Text = "Poliza?";
            this.ToolBarButtonDescargar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonDescargar.ToolTipText = "Crear archivo de salida COI en formato CSV.";
            this.ToolBarButtonDescargar.Click += new System.EventHandler(this.ToolBarButtonDescargar_Click);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "Cerrar";
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_cerrar;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // GridData
            // 
            this.GridData.Controls.Add(this.Waiting);
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 38);
            // 
            // 
            // 
            this.GridData.MasterTemplate.AllowAddNewRow = false;
            gridViewTextBoxColumn1.FieldName = "Estado";
            gridViewTextBoxColumn1.HeaderText = "Estado";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Estado";
            gridViewTextBoxColumn2.FieldName = "Folio";
            gridViewTextBoxColumn2.HeaderText = "Folio";
            gridViewTextBoxColumn2.Name = "Folio";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 80;
            gridViewTextBoxColumn3.FieldName = "Serie";
            gridViewTextBoxColumn3.HeaderText = "Serie";
            gridViewTextBoxColumn3.Name = "Serie";
            gridViewTextBoxColumn3.Width = 85;
            gridViewTextBoxColumn4.FieldName = "EmisorRFC";
            gridViewTextBoxColumn4.HeaderText = "Emisor (RFC)";
            gridViewTextBoxColumn4.IsVisible = false;
            gridViewTextBoxColumn4.Name = "EmisorRFC";
            gridViewTextBoxColumn4.Width = 90;
            gridViewTextBoxColumn5.FieldName = "Emisor";
            gridViewTextBoxColumn5.HeaderText = "Emisor";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "Emisor";
            gridViewTextBoxColumn5.Width = 240;
            gridViewTextBoxColumn6.FieldName = "ReceptorRfc";
            gridViewTextBoxColumn6.HeaderText = "RFC";
            gridViewTextBoxColumn6.Name = "ReceptorRfc";
            gridViewTextBoxColumn6.Width = 90;
            gridViewTextBoxColumn7.FieldName = "Receptor";
            gridViewTextBoxColumn7.HeaderText = "Receptor";
            gridViewTextBoxColumn7.Name = "Receptor";
            gridViewTextBoxColumn7.Width = 240;
            gridViewTextBoxColumn8.FieldName = "Uuid";
            gridViewTextBoxColumn8.HeaderText = "IdDocumento";
            gridViewTextBoxColumn8.Name = "Uuid";
            gridViewTextBoxColumn8.Width = 195;
            gridViewTextBoxColumn9.FieldName = "NoPlan";
            gridViewTextBoxColumn9.HeaderText = "Tipo";
            gridViewTextBoxColumn9.Name = "NoPlan";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn10.FieldName = "NoPlanText";
            gridViewTextBoxColumn10.HeaderText = "Plan";
            gridViewTextBoxColumn10.Name = "NoPlanText";
            gridViewTextBoxColumn11.DataType = typeof(int);
            gridViewTextBoxColumn11.FieldName = "Dia";
            gridViewTextBoxColumn11.HeaderText = "Día";
            gridViewTextBoxColumn11.Name = "Dia";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn12.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn12.FieldName = "FecEmision";
            gridViewTextBoxColumn12.FormatString = "{0:d}";
            gridViewTextBoxColumn12.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn12.Name = "FecEmision";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn12.Width = 85;
            gridViewTextBoxColumn13.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn13.FieldName = "FecTimbre";
            gridViewTextBoxColumn13.FormatString = "{0:d}";
            gridViewTextBoxColumn13.HeaderText = "Fec. Timbre";
            gridViewTextBoxColumn13.Name = "FecTimbre";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn13.Width = 85;
            gridViewTextBoxColumn14.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn14.FieldName = "FecCancela";
            gridViewTextBoxColumn14.FormatString = "{0:d}";
            gridViewTextBoxColumn14.HeaderText = "Fec. Cancela";
            gridViewTextBoxColumn14.Name = "FecCancela";
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn14.Width = 85;
            gridViewTextBoxColumn15.FieldName = "Total";
            gridViewTextBoxColumn15.FormatString = "{0:n}";
            gridViewTextBoxColumn15.HeaderText = "Total";
            gridViewTextBoxColumn15.Name = "Total";
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn15.Width = 85;
            gridViewTextBoxColumn16.FieldName = "TotalPagado";
            gridViewTextBoxColumn16.FormatString = "{0:n}";
            gridViewTextBoxColumn16.HeaderText = "Total Pagado";
            gridViewTextBoxColumn16.Name = "TotalPagado";
            gridViewTextBoxColumn16.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn16.Width = 85;
            gridViewTextBoxColumn17.FieldName = "Consecutivo";
            gridViewTextBoxColumn17.HeaderText = "Consecutivo";
            gridViewTextBoxColumn17.Name = "Consecutivo";
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn17.Width = 85;
            gridViewTextBoxColumn18.FieldName = "Mensualidad";
            gridViewTextBoxColumn18.HeaderText = "Mensualidad";
            gridViewTextBoxColumn18.Name = "Mensualidad";
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn18.Width = 85;
            gridViewTextBoxColumn19.DataType = typeof(double);
            gridViewTextBoxColumn19.FieldName = "SeguroAuto";
            gridViewTextBoxColumn19.FormatString = "{0:n}";
            gridViewTextBoxColumn19.HeaderText = "Seguro de Auto";
            gridViewTextBoxColumn19.Name = "SeguroAuto";
            gridViewTextBoxColumn19.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn19.Width = 85;
            gridViewTextBoxColumn20.DataType = typeof(double);
            gridViewTextBoxColumn20.FieldName = "Otros";
            gridViewTextBoxColumn20.FormatString = "{0:n}";
            gridViewTextBoxColumn20.HeaderText = "Otros";
            gridViewTextBoxColumn20.Name = "Otros";
            gridViewTextBoxColumn20.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn20.Width = 85;
            gridViewTextBoxColumn21.DataType = typeof(double);
            gridViewTextBoxColumn21.FieldName = "CuotaSeguroVida";
            gridViewTextBoxColumn21.FormatString = "{0:n}";
            gridViewTextBoxColumn21.HeaderText = "Cuota Seguro Vida";
            gridViewTextBoxColumn21.Name = "CuotaSeguroVida";
            gridViewTextBoxColumn21.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn21.Width = 85;
            gridViewTextBoxColumn22.DataType = typeof(double);
            gridViewTextBoxColumn22.FieldName = "CuotaIva";
            gridViewTextBoxColumn22.FormatString = "{0:n}";
            gridViewTextBoxColumn22.HeaderText = "Cuota IVA";
            gridViewTextBoxColumn22.Name = "CuotaIva";
            gridViewTextBoxColumn22.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn22.Width = 85;
            gridViewTextBoxColumn23.DataType = typeof(double);
            gridViewTextBoxColumn23.FieldName = "CuotaGastosAdmon";
            gridViewTextBoxColumn23.FormatString = "{0:n}";
            gridViewTextBoxColumn23.HeaderText = "Cuota Gastos Admon.";
            gridViewTextBoxColumn23.Name = "CuotaGastosAdmon";
            gridViewTextBoxColumn23.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn23.Width = 85;
            gridViewTextBoxColumn24.DataType = typeof(double);
            gridViewTextBoxColumn24.FieldName = "CuotaAutomovil";
            gridViewTextBoxColumn24.FormatString = "{0:n}";
            gridViewTextBoxColumn24.HeaderText = "Cuota Automovil";
            gridViewTextBoxColumn24.Name = "CuotaAutomovil";
            gridViewTextBoxColumn24.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn24.Width = 85;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.ShowGroupPanel = false;
            this.GridData.Size = new System.Drawing.Size(1147, 541);
            this.GridData.TabIndex = 1;
            this.GridData.CurrentRowChanged += new Telerik.WinControls.UI.CurrentRowChangedEventHandler(this.GridData_CurrentRowChanged);
            // 
            // Waiting
            // 
            this.Waiting.AssociatedControl = this.GridData;
            this.Waiting.Location = new System.Drawing.Point(408, 218);
            this.Waiting.Name = "Waiting";
            this.Waiting.Size = new System.Drawing.Size(70, 70);
            this.Waiting.TabIndex = 1;
            this.Waiting.Text = "radWaitingBar1";
            this.Waiting.WaitingIndicators.Add(this.dotsSpinnerWaitingBarIndicatorElement1);
            this.Waiting.WaitingSpeed = 100;
            this.Waiting.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsSpinner;
            ((Telerik.WinControls.UI.RadWaitingBarElement)(this.Waiting.GetChildAt(0))).WaitingSpeed = 100;
            ((Telerik.WinControls.UI.WaitingBarContentElement)(this.Waiting.GetChildAt(0).GetChildAt(0))).WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsSpinner;
            ((Telerik.WinControls.UI.WaitingBarSeparatorElement)(this.Waiting.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Dash = false;
            // 
            // dotsSpinnerWaitingBarIndicatorElement1
            // 
            this.dotsSpinnerWaitingBarIndicatorElement1.Name = "dotsSpinnerWaitingBarIndicatorElement1";
            // 
            // RecibosFiscalesReporteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1147, 579);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.CommandBar);
            this.Name = "RecibosFiscalesReporteForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ViewReporteRecibosFiscales";
            this.Load += new System.EventHandler(this.ViewReporteRecibosFiscales_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.GridData.ResumeLayout(false);
            this.GridData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Waiting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar CommandBar;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelPeriodo;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonPeriodo;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelEjercicio;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonEjercicio;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonActualizar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonExportar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCerrar;
        private Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.RadWaitingBar Waiting;
        private Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement dotsSpinnerWaitingBarIndicatorElement1;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltro;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonDescargar;
    }
}
