﻿using System;

namespace Jaeger.Views
{
    public partial class ViewerPdf : Telerik.WinControls.UI.RadForm
    {
        string nombreDeArchivo;

        public ViewerPdf()
        {
            InitializeComponent();
        }

        public ViewerPdf(string archivo)
        {
            InitializeComponent();
            this.nombreDeArchivo = archivo;
        }

        private void ViewerPdf_Load(object sender, EventArgs e)
        {
            this.PdfViewer.LoadDocument(this.nombreDeArchivo);
        }

        private void ViewerPdf_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            this.PdfViewer.UnloadDocument();
            this.Dispose();
        }
    }
}
