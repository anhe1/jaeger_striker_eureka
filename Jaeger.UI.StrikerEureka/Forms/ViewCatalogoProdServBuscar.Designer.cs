﻿namespace Jaeger.Views
{
    partial class ViewCatalogoProdServBuscar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.TxbDescripcion = new Telerik.WinControls.UI.RadTextBox();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.CboTipoProdServs = new Telerik.WinControls.UI.RadSplitButton();
            this.BttnTipoProductos = new Telerik.WinControls.UI.RadMenuItem();
            this.BttnTipoServicios = new Telerik.WinControls.UI.RadMenuItem();
            this.CboDivision = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.RadLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.CboGrupo = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.RadLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.CboClase = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.RadLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radPageView = new Telerik.WinControls.UI.RadPageView();
            this.PageViewBuscarTexto = new Telerik.WinControls.UI.RadPageViewPage();
            this.ButtonBuscar = new Telerik.WinControls.UI.RadButton();
            this.PageViewBuscarClasificacion = new Telerik.WinControls.UI.RadPageViewPage();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox3 = new Telerik.WinControls.UI.RadTextBox();
            this.TxbClaveDeClase = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.CboSubClase = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.ViewPageBuscarPorCatalogo = new Telerik.WinControls.UI.RadPageViewPage();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.txbPorCatalogo = new Telerik.WinControls.UI.RadTextBox();
            this.GridProductoCatalogo = new Telerik.WinControls.UI.RadGridView();
            this.ButtonCerrar = new Telerik.WinControls.UI.RadButton();
            this.ButtonAsignar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbDescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoProdServs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDivision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDivision.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDivision.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboGrupo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboGrupo.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboGrupo.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboClase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboClase.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboClase.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView)).BeginInit();
            this.radPageView.SuspendLayout();
            this.PageViewBuscarTexto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonBuscar)).BeginInit();
            this.PageViewBuscarClasificacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbClaveDeClase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboSubClase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboSubClase.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboSubClase.EditorControl.MasterTemplate)).BeginInit();
            this.ViewPageBuscarPorCatalogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbPorCatalogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridProductoCatalogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridProductoCatalogo.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonAsignar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(-1, 3);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(67, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Descripción:";
            // 
            // TxbDescripcion
            // 
            this.TxbDescripcion.Location = new System.Drawing.Point(72, 2);
            this.TxbDescripcion.Name = "TxbDescripcion";
            this.TxbDescripcion.Size = new System.Drawing.Size(497, 20);
            this.TxbDescripcion.TabIndex = 1;
            // 
            // GridData
            // 
            this.GridData.Location = new System.Drawing.Point(3, 33);
            // 
            // 
            // 
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn1.Width = 85;
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn2.Width = 450;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.GridData.MasterTemplate.EnableGrouping = false;
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(686, 243);
            this.GridData.TabIndex = 2;
            // 
            // CboTipoProdServs
            // 
            this.CboTipoProdServs.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.BttnTipoProductos,
            this.BttnTipoServicios});
            this.CboTipoProdServs.Location = new System.Drawing.Point(64, 3);
            this.CboTipoProdServs.Name = "CboTipoProdServs";
            this.CboTipoProdServs.Size = new System.Drawing.Size(152, 24);
            this.CboTipoProdServs.TabIndex = 20;
            // 
            // BttnTipoProductos
            // 
            this.BttnTipoProductos.Name = "BttnTipoProductos";
            this.BttnTipoProductos.Text = "Productos";
            this.BttnTipoProductos.UseCompatibleTextRendering = false;
            // 
            // BttnTipoServicios
            // 
            this.BttnTipoServicios.Name = "BttnTipoServicios";
            this.BttnTipoServicios.Text = "Servicios";
            this.BttnTipoServicios.UseCompatibleTextRendering = false;
            // 
            // CboDivision
            // 
            this.CboDivision.AutoFilter = true;
            // 
            // CboDivision.NestedRadGridView
            // 
            this.CboDivision.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboDivision.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboDivision.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboDivision.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboDivision.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboDivision.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboDivision.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn3.FieldName = "Clave";
            gridViewTextBoxColumn3.HeaderText = "Clave";
            gridViewTextBoxColumn3.Name = "Clave";
            gridViewTextBoxColumn3.Width = 85;
            gridViewTextBoxColumn4.FieldName = "Descripcion";
            gridViewTextBoxColumn4.HeaderText = "Descripción";
            gridViewTextBoxColumn4.Name = "Name";
            gridViewTextBoxColumn4.Width = 280;
            this.CboDivision.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.CboDivision.EditorControl.MasterTemplate.EnableFiltering = true;
            this.CboDivision.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboDivision.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboDivision.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.CboDivision.EditorControl.Name = "NestedRadGridView";
            this.CboDivision.EditorControl.ReadOnly = true;
            this.CboDivision.EditorControl.ShowGroupPanel = false;
            this.CboDivision.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboDivision.EditorControl.TabIndex = 0;
            this.CboDivision.Location = new System.Drawing.Point(64, 33);
            this.CboDivision.Name = "CboDivision";
            this.CboDivision.Size = new System.Drawing.Size(625, 20);
            this.CboDivision.TabIndex = 21;
            this.CboDivision.TabStop = false;
            // 
            // RadLabel4
            // 
            this.RadLabel4.Location = new System.Drawing.Point(3, 87);
            this.RadLabel4.Name = "RadLabel4";
            this.RadLabel4.Size = new System.Drawing.Size(35, 18);
            this.RadLabel4.TabIndex = 27;
            this.RadLabel4.Text = "Clase:";
            // 
            // CboGrupo
            // 
            // 
            // CboGrupo.NestedRadGridView
            // 
            this.CboGrupo.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboGrupo.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboGrupo.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboGrupo.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboGrupo.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboGrupo.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboGrupo.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn5.FieldName = "Clave";
            gridViewTextBoxColumn5.HeaderText = "Clave";
            gridViewTextBoxColumn5.Name = "Clave";
            gridViewTextBoxColumn5.Width = 85;
            gridViewTextBoxColumn6.FieldName = "Descripcion";
            gridViewTextBoxColumn6.HeaderText = "Descripción";
            gridViewTextBoxColumn6.Name = "Name";
            gridViewTextBoxColumn6.Width = 290;
            this.CboGrupo.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.CboGrupo.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboGrupo.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboGrupo.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.CboGrupo.EditorControl.Name = "NestedRadGridView";
            this.CboGrupo.EditorControl.ReadOnly = true;
            this.CboGrupo.EditorControl.ShowGroupPanel = false;
            this.CboGrupo.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboGrupo.EditorControl.TabIndex = 0;
            this.CboGrupo.Location = new System.Drawing.Point(64, 59);
            this.CboGrupo.Name = "CboGrupo";
            this.CboGrupo.Size = new System.Drawing.Size(625, 20);
            this.CboGrupo.TabIndex = 22;
            this.CboGrupo.TabStop = false;
            // 
            // RadLabel3
            // 
            this.RadLabel3.Location = new System.Drawing.Point(3, 61);
            this.RadLabel3.Name = "RadLabel3";
            this.RadLabel3.Size = new System.Drawing.Size(40, 18);
            this.RadLabel3.TabIndex = 26;
            this.RadLabel3.Text = "Grupo:";
            // 
            // CboClase
            // 
            // 
            // CboClase.NestedRadGridView
            // 
            this.CboClase.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboClase.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboClase.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboClase.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboClase.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboClase.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboClase.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn7.FieldName = "Clave";
            gridViewTextBoxColumn7.HeaderText = "Clave";
            gridViewTextBoxColumn7.Name = "Clave";
            gridViewTextBoxColumn7.Width = 85;
            gridViewTextBoxColumn8.FieldName = "Descripcion";
            gridViewTextBoxColumn8.HeaderText = "Descripción";
            gridViewTextBoxColumn8.Name = "Name";
            gridViewTextBoxColumn8.Width = 290;
            this.CboClase.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.CboClase.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboClase.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboClase.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.CboClase.EditorControl.Name = "NestedRadGridView";
            this.CboClase.EditorControl.ReadOnly = true;
            this.CboClase.EditorControl.ShowGroupPanel = false;
            this.CboClase.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboClase.EditorControl.TabIndex = 0;
            this.CboClase.Location = new System.Drawing.Point(64, 85);
            this.CboClase.Name = "CboClase";
            this.CboClase.Size = new System.Drawing.Size(625, 20);
            this.CboClase.TabIndex = 23;
            this.CboClase.TabStop = false;
            // 
            // RadLabel2
            // 
            this.RadLabel2.Location = new System.Drawing.Point(3, 35);
            this.RadLabel2.Name = "RadLabel2";
            this.RadLabel2.Size = new System.Drawing.Size(48, 18);
            this.RadLabel2.TabIndex = 25;
            this.RadLabel2.Text = "División:";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(3, 9);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(31, 18);
            this.radLabel5.TabIndex = 24;
            this.radLabel5.Text = "Tipo:";
            // 
            // radPageView
            // 
            this.radPageView.Controls.Add(this.PageViewBuscarTexto);
            this.radPageView.Controls.Add(this.PageViewBuscarClasificacion);
            this.radPageView.Controls.Add(this.ViewPageBuscarPorCatalogo);
            this.radPageView.DefaultPage = this.PageViewBuscarTexto;
            this.radPageView.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPageView.ItemSizeMode = ((Telerik.WinControls.UI.PageViewItemSizeMode)((Telerik.WinControls.UI.PageViewItemSizeMode.EqualWidth | Telerik.WinControls.UI.PageViewItemSizeMode.EqualHeight)));
            this.radPageView.Location = new System.Drawing.Point(0, 0);
            this.radPageView.Name = "radPageView";
            this.radPageView.SelectedPage = this.ViewPageBuscarPorCatalogo;
            this.radPageView.Size = new System.Drawing.Size(702, 421);
            this.radPageView.TabIndex = 1;
            this.radPageView.ViewMode = Telerik.WinControls.UI.PageViewMode.Outlook;
            // 
            // PageViewBuscarTexto
            // 
            this.PageViewBuscarTexto.Controls.Add(this.ButtonBuscar);
            this.PageViewBuscarTexto.Controls.Add(this.GridData);
            this.PageViewBuscarTexto.Controls.Add(this.radLabel1);
            this.PageViewBuscarTexto.Controls.Add(this.TxbDescripcion);
            this.PageViewBuscarTexto.ItemSize = new System.Drawing.SizeF(704F, 32F);
            this.PageViewBuscarTexto.Location = new System.Drawing.Point(5, 31);
            this.PageViewBuscarTexto.Name = "PageViewBuscarTexto";
            this.PageViewBuscarTexto.Size = new System.Drawing.Size(692, 248);
            this.PageViewBuscarTexto.Text = "Búsqueda por texto";
            // 
            // ButtonBuscar
            // 
            this.ButtonBuscar.Location = new System.Drawing.Point(575, 3);
            this.ButtonBuscar.Name = "ButtonBuscar";
            this.ButtonBuscar.Size = new System.Drawing.Size(110, 18);
            this.ButtonBuscar.TabIndex = 4;
            this.ButtonBuscar.Text = "Buscar";
            this.ButtonBuscar.Click += new System.EventHandler(this.ButtonBuscar_Click);
            // 
            // PageViewBuscarClasificacion
            // 
            this.PageViewBuscarClasificacion.Controls.Add(this.radLabel8);
            this.PageViewBuscarClasificacion.Controls.Add(this.radLabel7);
            this.PageViewBuscarClasificacion.Controls.Add(this.radTextBox3);
            this.PageViewBuscarClasificacion.Controls.Add(this.TxbClaveDeClase);
            this.PageViewBuscarClasificacion.Controls.Add(this.radLabel6);
            this.PageViewBuscarClasificacion.Controls.Add(this.CboTipoProdServs);
            this.PageViewBuscarClasificacion.Controls.Add(this.CboSubClase);
            this.PageViewBuscarClasificacion.Controls.Add(this.radLabel5);
            this.PageViewBuscarClasificacion.Controls.Add(this.RadLabel2);
            this.PageViewBuscarClasificacion.Controls.Add(this.CboDivision);
            this.PageViewBuscarClasificacion.Controls.Add(this.CboClase);
            this.PageViewBuscarClasificacion.Controls.Add(this.RadLabel4);
            this.PageViewBuscarClasificacion.Controls.Add(this.RadLabel3);
            this.PageViewBuscarClasificacion.Controls.Add(this.CboGrupo);
            this.PageViewBuscarClasificacion.ItemSize = new System.Drawing.SizeF(704F, 32F);
            this.PageViewBuscarClasificacion.Location = new System.Drawing.Point(5, 31);
            this.PageViewBuscarClasificacion.Name = "PageViewBuscarClasificacion";
            this.PageViewBuscarClasificacion.Size = new System.Drawing.Size(692, 248);
            this.PageViewBuscarClasificacion.Text = "Clasificación";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(170, 146);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(64, 18);
            this.radLabel8.TabIndex = 33;
            this.radLabel8.Text = "Descripción";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(64, 146);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(55, 18);
            this.radLabel7.TabIndex = 32;
            this.radLabel7.Text = "Clave SAT";
            // 
            // radTextBox3
            // 
            this.radTextBox3.Location = new System.Drawing.Point(170, 170);
            this.radTextBox3.Name = "radTextBox3";
            this.radTextBox3.Size = new System.Drawing.Size(519, 20);
            this.radTextBox3.TabIndex = 31;
            // 
            // TxbClaveDeClase
            // 
            this.TxbClaveDeClase.Location = new System.Drawing.Point(64, 170);
            this.TxbClaveDeClase.Name = "TxbClaveDeClase";
            this.TxbClaveDeClase.Size = new System.Drawing.Size(100, 20);
            this.TxbClaveDeClase.TabIndex = 30;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(3, 113);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(54, 18);
            this.radLabel6.TabIndex = 29;
            this.radLabel6.Text = "SubClase:";
            // 
            // CboSubClase
            // 
            // 
            // CboSubClase.NestedRadGridView
            // 
            this.CboSubClase.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboSubClase.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboSubClase.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboSubClase.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboSubClase.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboSubClase.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboSubClase.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn9.FieldName = "Clave";
            gridViewTextBoxColumn9.HeaderText = "Clave";
            gridViewTextBoxColumn9.Name = "Clave";
            gridViewTextBoxColumn9.Width = 85;
            gridViewTextBoxColumn10.FieldName = "Descripcion";
            gridViewTextBoxColumn10.HeaderText = "Descripción";
            gridViewTextBoxColumn10.Name = "Name";
            gridViewTextBoxColumn10.Width = 290;
            this.CboSubClase.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.CboSubClase.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboSubClase.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboSubClase.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.CboSubClase.EditorControl.Name = "NestedRadGridView";
            this.CboSubClase.EditorControl.ReadOnly = true;
            this.CboSubClase.EditorControl.ShowGroupPanel = false;
            this.CboSubClase.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboSubClase.EditorControl.TabIndex = 0;
            this.CboSubClase.Location = new System.Drawing.Point(64, 111);
            this.CboSubClase.Name = "CboSubClase";
            this.CboSubClase.Size = new System.Drawing.Size(625, 20);
            this.CboSubClase.TabIndex = 28;
            this.CboSubClase.TabStop = false;
            // 
            // ViewPageBuscarPorCatalogo
            // 
            this.ViewPageBuscarPorCatalogo.Controls.Add(this.radButton1);
            this.ViewPageBuscarPorCatalogo.Controls.Add(this.radLabel9);
            this.ViewPageBuscarPorCatalogo.Controls.Add(this.txbPorCatalogo);
            this.ViewPageBuscarPorCatalogo.Controls.Add(this.GridProductoCatalogo);
            this.ViewPageBuscarPorCatalogo.ItemSize = new System.Drawing.SizeF(704F, 32F);
            this.ViewPageBuscarPorCatalogo.Location = new System.Drawing.Point(5, 31);
            this.ViewPageBuscarPorCatalogo.Name = "ViewPageBuscarPorCatalogo";
            this.ViewPageBuscarPorCatalogo.Size = new System.Drawing.Size(692, 248);
            this.ViewPageBuscarPorCatalogo.Text = "Catálogo";
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(575, 4);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(110, 18);
            this.radButton1.TabIndex = 8;
            this.radButton1.Text = "Buscar";
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(-1, 4);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(67, 18);
            this.radLabel9.TabIndex = 6;
            this.radLabel9.Text = "Descripción:";
            // 
            // txbPorCatalogo
            // 
            this.txbPorCatalogo.Location = new System.Drawing.Point(72, 3);
            this.txbPorCatalogo.Name = "txbPorCatalogo";
            this.txbPorCatalogo.Size = new System.Drawing.Size(497, 20);
            this.txbPorCatalogo.TabIndex = 7;
            this.txbPorCatalogo.TextChanged += new System.EventHandler(this.txbPorCatalogo_TextChanged);
            // 
            // GridProductoCatalogo
            // 
            this.GridProductoCatalogo.Location = new System.Drawing.Point(-1, 28);
            // 
            // 
            // 
            this.GridProductoCatalogo.MasterTemplate.AllowAddNewRow = false;
            this.GridProductoCatalogo.MasterTemplate.AllowDeleteRow = false;
            this.GridProductoCatalogo.MasterTemplate.AllowEditRow = false;
            this.GridProductoCatalogo.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn11.DataType = typeof(int);
            gridViewTextBoxColumn11.FieldName = "IdModelo";
            gridViewTextBoxColumn11.FormatString = "{0:PT00000#}";
            gridViewTextBoxColumn11.HeaderText = "Id";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "IdModelo";
            gridViewTextBoxColumn11.Width = 65;
            gridViewTextBoxColumn12.FieldName = "Tipo";
            gridViewTextBoxColumn12.HeaderText = "Tipo";
            gridViewTextBoxColumn12.Name = "Tipo";
            gridViewTextBoxColumn12.Width = 80;
            gridViewTextBoxColumn13.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn13.HeaderText = "Identificador";
            gridViewTextBoxColumn13.Name = "NoIdentificacion";
            gridViewTextBoxColumn13.Width = 95;
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "A";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewTextBoxColumn14.FieldName = "Descripcion";
            gridViewTextBoxColumn14.HeaderText = "Descripción";
            gridViewTextBoxColumn14.Name = "Modelo";
            gridViewTextBoxColumn14.Width = 350;
            gridViewTextBoxColumn15.FieldName = "Marca";
            gridViewTextBoxColumn15.HeaderText = "Marca";
            gridViewTextBoxColumn15.Name = "Marca";
            gridViewTextBoxColumn15.Width = 200;
            gridViewTextBoxColumn16.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn16.HeaderText = "Clave\r\nProd. Serv.";
            gridViewTextBoxColumn16.Name = "ClaveProdServ";
            gridViewTextBoxColumn16.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn16.Width = 80;
            gridViewComboBoxColumn1.FieldName = "ClaveUnidad";
            gridViewComboBoxColumn1.HeaderText = "Clave\r\nUnidad";
            gridViewComboBoxColumn1.Name = "ClaveUnidad";
            gridViewComboBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewComboBoxColumn1.Width = 80;
            gridViewComboBoxColumn2.FieldName = "Unidad";
            gridViewComboBoxColumn2.HeaderText = "Unidad";
            gridViewComboBoxColumn2.Name = "Unidad";
            gridViewComboBoxColumn2.Width = 75;
            gridViewTextBoxColumn17.DataType = typeof(decimal);
            gridViewTextBoxColumn17.FieldName = "Unitario";
            gridViewTextBoxColumn17.FormatString = "{0:n}";
            gridViewTextBoxColumn17.HeaderText = "Unitario";
            gridViewTextBoxColumn17.Name = "Unitario";
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn17.Width = 85;
            gridViewTextBoxColumn18.FieldName = "CtaPredial";
            gridViewTextBoxColumn18.HeaderText = "Cta. Predial";
            gridViewTextBoxColumn18.Name = "CtaPredial";
            gridViewTextBoxColumn18.Width = 85;
            gridViewTextBoxColumn19.FieldName = "NumRequerimiento";
            gridViewTextBoxColumn19.HeaderText = "Núm Req.";
            gridViewTextBoxColumn19.Name = "NumRequerimiento";
            gridViewTextBoxColumn19.Width = 85;
            gridViewTextBoxColumn20.FieldName = "Creo";
            gridViewTextBoxColumn20.HeaderText = "Creó";
            gridViewTextBoxColumn20.Name = "Creo";
            gridViewTextBoxColumn20.Width = 75;
            gridViewTextBoxColumn21.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn21.FieldName = "FechaNuevo";
            gridViewTextBoxColumn21.FormatString = "{0:d}";
            gridViewTextBoxColumn21.HeaderText = "Fec. Sistema";
            gridViewTextBoxColumn21.Name = "FechaNuevo";
            gridViewTextBoxColumn21.Width = 75;
            this.GridProductoCatalogo.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewComboBoxColumn1,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21});
            this.GridProductoCatalogo.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.GridProductoCatalogo.Name = "GridProductoCatalogo";
            this.GridProductoCatalogo.ShowGroupPanel = false;
            this.GridProductoCatalogo.Size = new System.Drawing.Size(694, 220);
            this.GridProductoCatalogo.TabIndex = 5;
            // 
            // ButtonCerrar
            // 
            this.ButtonCerrar.Location = new System.Drawing.Point(580, 427);
            this.ButtonCerrar.Name = "ButtonCerrar";
            this.ButtonCerrar.Size = new System.Drawing.Size(110, 24);
            this.ButtonCerrar.TabIndex = 2;
            this.ButtonCerrar.Text = "Cerrar";
            this.ButtonCerrar.Click += new System.EventHandler(this.ButtonCerrar_Click);
            // 
            // ButtonAsignar
            // 
            this.ButtonAsignar.Location = new System.Drawing.Point(464, 427);
            this.ButtonAsignar.Name = "ButtonAsignar";
            this.ButtonAsignar.Size = new System.Drawing.Size(110, 24);
            this.ButtonAsignar.TabIndex = 3;
            this.ButtonAsignar.Text = "Asignar";
            this.ButtonAsignar.Click += new System.EventHandler(this.ButtonAsignar_Click);
            // 
            // ViewCatalogoProdServBuscar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 457);
            this.Controls.Add(this.ButtonAsignar);
            this.Controls.Add(this.ButtonCerrar);
            this.Controls.Add(this.radPageView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewCatalogoProdServBuscar";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Buscar Clave de Producto/Servicio SAT";
            this.Load += new System.EventHandler(this.ViewCatalogoProdServBuscar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbDescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoProdServs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDivision.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDivision.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDivision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboGrupo.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboGrupo.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboGrupo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboClase.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboClase.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboClase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView)).EndInit();
            this.radPageView.ResumeLayout(false);
            this.PageViewBuscarTexto.ResumeLayout(false);
            this.PageViewBuscarTexto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonBuscar)).EndInit();
            this.PageViewBuscarClasificacion.ResumeLayout(false);
            this.PageViewBuscarClasificacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbClaveDeClase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboSubClase.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboSubClase.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboSubClase)).EndInit();
            this.ViewPageBuscarPorCatalogo.ResumeLayout(false);
            this.ViewPageBuscarPorCatalogo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbPorCatalogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridProductoCatalogo.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridProductoCatalogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonAsignar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBox TxbDescripcion;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadGridView GridData;
        internal Telerik.WinControls.UI.RadSplitButton CboTipoProdServs;
        internal Telerik.WinControls.UI.RadMenuItem BttnTipoProductos;
        internal Telerik.WinControls.UI.RadMenuItem BttnTipoServicios;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboDivision;
        internal Telerik.WinControls.UI.RadLabel RadLabel4;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboGrupo;
        internal Telerik.WinControls.UI.RadLabel RadLabel3;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboClase;
        internal Telerik.WinControls.UI.RadLabel RadLabel2;
        internal Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadPageView radPageView;
        private Telerik.WinControls.UI.RadPageViewPage PageViewBuscarTexto;
        private Telerik.WinControls.UI.RadPageViewPage PageViewBuscarClasificacion;
        internal Telerik.WinControls.UI.RadLabel radLabel8;
        internal Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox radTextBox3;
        private Telerik.WinControls.UI.RadTextBox TxbClaveDeClase;
        internal Telerik.WinControls.UI.RadLabel radLabel6;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboSubClase;
        private Telerik.WinControls.UI.RadButton ButtonCerrar;
        private Telerik.WinControls.UI.RadButton ButtonAsignar;
        private Telerik.WinControls.UI.RadButton ButtonBuscar;
        private Telerik.WinControls.UI.RadPageViewPage ViewPageBuscarPorCatalogo;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadTextBox txbPorCatalogo;
        private Telerik.WinControls.UI.RadGridView GridProductoCatalogo;
    }
}
