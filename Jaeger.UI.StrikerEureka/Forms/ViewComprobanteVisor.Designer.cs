﻿namespace Jaeger.Views
{
    partial class ViewComprobanteVisor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PageView = new Telerik.WinControls.UI.RadPageView();
            this.ViewPageXML = new Telerik.WinControls.UI.RadPageViewPage();
            this.radTreeView = new Telerik.WinControls.UI.RadTreeView();
            this.ViewPageOrigen = new Telerik.WinControls.UI.RadPageViewPage();
            ((System.ComponentModel.ISupportInitialize)(this.PageView)).BeginInit();
            this.PageView.SuspendLayout();
            this.ViewPageXML.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // PageView
            // 
            this.PageView.Controls.Add(this.ViewPageXML);
            this.PageView.Controls.Add(this.ViewPageOrigen);
            this.PageView.DefaultPage = this.ViewPageXML;
            this.PageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PageView.Location = new System.Drawing.Point(0, 0);
            this.PageView.Name = "PageView";
            this.PageView.SelectedPage = this.ViewPageOrigen;
            this.PageView.Size = new System.Drawing.Size(1268, 670);
            this.PageView.TabIndex = 0;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // ViewPageXML
            // 
            this.ViewPageXML.Controls.Add(this.radTreeView);
            this.ViewPageXML.ItemSize = new System.Drawing.SizeF(38F, 28F);
            this.ViewPageXML.Location = new System.Drawing.Point(10, 37);
            this.ViewPageXML.Name = "ViewPageXML";
            this.ViewPageXML.Size = new System.Drawing.Size(1247, 622);
            this.ViewPageXML.Text = "XML";
            // 
            // radTreeView
            // 
            this.radTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView.Location = new System.Drawing.Point(0, 0);
            this.radTreeView.Name = "radTreeView";
            this.radTreeView.Size = new System.Drawing.Size(1247, 622);
            this.radTreeView.SpacingBetweenNodes = -1;
            this.radTreeView.TabIndex = 0;
            // 
            // ViewPageOrigen
            // 
            this.ViewPageOrigen.ItemSize = new System.Drawing.SizeF(50F, 28F);
            this.ViewPageOrigen.Location = new System.Drawing.Point(10, 37);
            this.ViewPageOrigen.Name = "ViewPageOrigen";
            this.ViewPageOrigen.Size = new System.Drawing.Size(1247, 622);
            this.ViewPageOrigen.Text = "Origen";
            // 
            // ViewComprobanteVisor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1268, 670);
            this.Controls.Add(this.PageView);
            this.Name = "ViewComprobanteVisor";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ViewComprobanteVisor";
            this.Load += new System.EventHandler(this.ViewComprobanteVisor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PageView)).EndInit();
            this.PageView.ResumeLayout(false);
            this.ViewPageXML.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPageView PageView;
        private Telerik.WinControls.UI.RadPageViewPage ViewPageXML;
        private Telerik.WinControls.UI.RadTreeView radTreeView;
        private Telerik.WinControls.UI.RadPageViewPage ViewPageOrigen;
    }
}
