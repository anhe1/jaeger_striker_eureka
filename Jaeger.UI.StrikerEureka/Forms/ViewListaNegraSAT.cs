﻿using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Repositories;
using Jaeger.Helpers;
using System;
using System.Windows.Forms;
using Telerik.WinControls.Enumerations;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views
{
    public partial class ViewListaNegraSAT : Telerik.WinControls.UI.RadForm
    {
        public ViewListaNegraSAT()
        {
            InitializeComponent();
        }

        private void ViewListaNegraSAT_Load(object sender, EventArgs e)
        {
            this.GridData.TelerikGridCommon();
            
        }

        private void ToolBarButtonFiltro_Click(object sender, EventArgs e)
        {
            this.GridData.ShowFilteringRow = this.ToolBarButtonFiltro.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false)
                this.GridData.FilterDescriptors.Clear();
        }

        private void ToolBarButtonCatalogoNoLocalizados_Click(object sender, EventArgs e)
        {
            //OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "*.csv|*.CSV", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
            //if (openFileDialog.ShowDialog() != DialogResult.OK)
            //    return;
            //Jaeger.Layout.SAT.Catalogos.c_NoLocalizados noLocalizados = new Layout.SAT.Catalogos.c_NoLocalizados();
            //noLocalizados.Importar(openFileDialog.FileName);
            //if (noLocalizados.Items.Count >0)
            //    noLocalizados.Save(System.IO.Path.ChangeExtension(openFileDialog.FileName, "json"));
            var cancelados = new NoLocalizadosCatalogo();
            cancelados.Load();
            this.GridData.DataSource = cancelados.Items;
            this.GridData.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
        }

        private void ToolBarButtonCatalogoCancelados_Click(object sender, EventArgs e)
        {
            var cancelados = new CanceladosCatalogo();
            cancelados.Load();
            this.GridData.DataSource = cancelados.Items;
            this.GridData.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
        }

        private void ToolBarButtonImportar_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog() { Filter = "*.TXT|*.txt|*.CSV|*.csv" };
            if (fileDialog.ShowDialog(this) == DialogResult.OK)
            {
                var catalogo = new Articulo69BCatalogo();
                Layout.SAT.Catalogos.c_Articulo69B c_Articulo69B = new Layout.SAT.Catalogos.c_Articulo69B();
                if (c_Articulo69B.Importar(fileDialog.FileName))
                {
                    foreach (var item in c_Articulo69B.Items)
                    {
                        var articulo = new Articulo69B();
                        articulo.Id = item.Id;
                        articulo.Nombre = item.RazonSocial;
                        articulo.RFC = item.RFC;
                        articulo.Situacion = item.Situacion;
                        articulo.Presunto.NoOficioGlobal = item.PresuntoNoOficioGlobal;
                        articulo.Presunto.NoOficio = item.PresuntoNoOficio;
                        articulo.Presunto.FechaPublicacionSAT = item.PresuntoFechaPublicacionSAT;
                        articulo.Presunto.PublicacionDOF = item.PresuntoPublicacionDOF;
                        articulo.Definitivo.FechaPublicacionSAT = item.DefinitivoFechaPublicacionSAT;
                        articulo.Definitivo.NoOficio = item.DefinitivoNoOficio;
                        articulo.Definitivo.PublicacionDOF = item.DefinitivoPublicacionDOF;
                        articulo.Desvirtuado.FechaPublicacionSAT = item.DesvirtuadoFechaPublicacionSAT;
                        articulo.Desvirtuado.NoOficio = item.DesvirtuadoNoOficio;
                        articulo.Desvirtuado.PublicacionDOF = item.DesvirtuadoPublicacionDOF;
                        articulo.Sentencia.FechaPublicacionSAT = item.SentenciaFechaPublicacionSAT;
                        articulo.Sentencia.NoOficio = item.SentenciaNoOficio;
                        articulo.Sentencia.NoOficio2 = item.SentenciaNoOficio2;
                        articulo.Sentencia.PublicacionDOF = item.SentenciaPublicacionDOF;
                        catalogo.Items.Add(articulo);
                    }
                }
                catalogo.Save();
            }
        }
    }
}
