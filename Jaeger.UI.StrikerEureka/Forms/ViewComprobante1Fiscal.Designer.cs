﻿namespace Jaeger.Views
{
    partial class ViewComprobante1Fiscal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject1 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewComprobante1Fiscal));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn38 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition7 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition8 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition9 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn39 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn40 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition10 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn41 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn42 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition11 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn43 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn44 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition12 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn45 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn46 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn47 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn48 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn49 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition13 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn50 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn51 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn52 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition14 = new Telerik.WinControls.UI.TableViewDefinition();
            this.ToolBarCfdiTipoRelacion = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.CommandBarLabel1 = new Telerik.WinControls.UI.CommandBarLabel();
            this.CommandCfdiRelacionado = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarHostItemIncluir = new Telerik.WinControls.UI.CommandBarHostItem();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.CommandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarCfdiBuscar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarCfdiAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarCfdiQuitar = new Telerik.WinControls.UI.CommandBarButton();
            this.CommandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.GridCfdiRelacionados = new Telerik.WinControls.UI.RadGridView();
            this.RadCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.PageViewCfdiRelacionado = new Telerik.WinControls.UI.RadPageViewPage();
            this.ToolBarLabelProducto = new Telerik.WinControls.UI.CommandBarLabel();
            this.CommandConceptos = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarHostConceptos = new Telerik.WinControls.UI.CommandBarHostItem();
            this.ToolBarConceptoAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarConceptoQuitar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarConceptoButtonNuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarConceptoBuscar = new Telerik.WinControls.UI.CommandBarButton();
            this.CommandBarRowElement4 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.RadCommandBar4 = new Telerik.WinControls.UI.RadCommandBar();
            this.PageViewConcepto = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridConceptos = new Telerik.WinControls.UI.RadGridView();
            this.ComprobanteConcepto = new Telerik.WinControls.UI.RadPageView();
            this.PageConceptoAduana = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridConceptoInformacionAduanera = new Telerik.WinControls.UI.RadGridView();
            this.radCommandBar7 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement8 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.CommandConceptoAduana = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarConceptoAduanaAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarConceptoAduanaQuitar = new Telerik.WinControls.UI.CommandBarButton();
            this.CommandConceptoImpuesto = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarConceptoImpuestoAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarConceptoImpuestoQuitar = new Telerik.WinControls.UI.CommandBarButton();
            this.CommandBarRowElement7 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.GridConceptoImpuestos = new Telerik.WinControls.UI.RadGridView();
            this.RadCommandBar6 = new Telerik.WinControls.UI.RadCommandBar();
            this.PageConceptoImpuesto = new Telerik.WinControls.UI.RadPageViewPage();
            this.FechaCertifica = new Telerik.WinControls.UI.RadDateTimePicker();
            this.RadLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.TipoComprobante = new Telerik.WinControls.UI.RadSplitButton();
            this.RadLabel31 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel29 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel28 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel27 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel26 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.CommandConceptoParte = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarConceptoParteBuscar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarConceptoParteAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarConceptoParteQuitar = new Telerik.WinControls.UI.CommandBarButton();
            this.ComprobanteConceptosPartes = new Telerik.WinControls.UI.RadPageView();
            this.PageConceptoParte = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridConceptoParte = new Telerik.WinControls.UI.RadGridView();
            this.RadCommandBar5 = new Telerik.WinControls.UI.RadCommandBar();
            this.CommandBarRowElement6 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.RadCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.CommandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.CommandComprobanteFiscal = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarEmisor = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.Separador = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolLabelStatus = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarStatus = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarNew = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarCopy = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarSave = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarRefresh = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarCreate = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarCancelar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarPdf = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarXml = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarEmail = new Telerik.WinControls.UI.CommandBarButton();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarSeries = new Telerik.WinControls.UI.CommandBarButton();
            this.CommandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolLabelUuid = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarTextBoxIdDocumento = new Telerik.WinControls.UI.CommandBarTextBox();
            this.ToolBarClose = new Telerik.WinControls.UI.CommandBarButton();
            this.RadLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.TxbLugarDeExpedicion = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.CboReceptor = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.TxbReceptorRFC = new Telerik.WinControls.UI.RadTextBox();
            this.CboUsoCfdi = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.TxbNumRegIdTrib = new Telerik.WinControls.UI.RadTextBox();
            this.CboResidenciaFiscal = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CboCondiciones = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CboFormaPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.TxbCuentaPago = new Telerik.WinControls.UI.RadTextBox();
            this.CboMetodoPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.TxbTipoCambio = new Telerik.WinControls.UI.RadTextBox();
            this.CboMoneda = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CboDocumento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CboSerie = new Telerik.WinControls.UI.RadDropDownList();
            this.TxbFolio = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.FechaEmisionField = new Telerik.WinControls.UI.RadDateTimePicker();
            this.PanelGeneral = new Telerik.WinControls.UI.RadPanel();
            this.CboConceptos = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.ChkCfdiRelacionadoIncluir = new Telerik.WinControls.UI.RadCheckBox();
            this.Presicion = new Telerik.WinControls.UI.RadSpinEditor();
            this.PanelDocumento = new Telerik.WinControls.UI.RadPanel();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.PanelTotales = new Telerik.WinControls.UI.RadPanel();
            this.TxbDescuento = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.TxbSubTotal = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.TxbTrasladoIva = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.TxbTrasladoIEPS = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.TxbRetencionIva = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.TxbRetencionISR = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.TxbTotal = new Telerik.WinControls.UI.RadMaskedEditBox();
            ((System.ComponentModel.ISupportInitialize)(this.GridCfdiRelacionados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridCfdiRelacionados.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar2)).BeginInit();
            this.PageViewCfdiRelacionado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar4)).BeginInit();
            this.PageViewConcepto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComprobanteConcepto)).BeginInit();
            this.ComprobanteConcepto.SuspendLayout();
            this.PageConceptoAduana.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoInformacionAduanera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoInformacionAduanera.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoImpuestos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoImpuestos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar6)).BeginInit();
            this.PageConceptoImpuesto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FechaCertifica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoComprobante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComprobanteConceptosPartes)).BeginInit();
            this.ComprobanteConceptosPartes.SuspendLayout();
            this.PageConceptoParte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbLugarDeExpedicion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbReceptorRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCfdi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCfdi.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCfdi.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumRegIdTrib)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCondiciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCondiciones.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCondiciones.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCuentaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMetodoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMetodoPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMetodoPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMoneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMoneda.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMoneda.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDocumento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDocumento.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboSerie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbFolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmisionField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelGeneral)).BeginInit();
            this.PanelGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CboConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboConceptos.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboConceptos.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCfdiRelacionadoIncluir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Presicion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelDocumento)).BeginInit();
            this.PanelDocumento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelTotales)).BeginInit();
            this.PanelTotales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxbDescuento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSubTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTrasladoIva)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTrasladoIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRetencionIva)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRetencionISR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBarCfdiTipoRelacion
            // 
            this.ToolBarCfdiTipoRelacion.DisplayName = "Tipo de Relación";
            this.ToolBarCfdiTipoRelacion.DropDownAnimationEnabled = true;
            this.ToolBarCfdiTipoRelacion.Enabled = false;
            this.ToolBarCfdiTipoRelacion.MaxDropDownItems = 0;
            this.ToolBarCfdiTipoRelacion.MinSize = new System.Drawing.Size(350, 22);
            this.ToolBarCfdiTipoRelacion.Name = "ToolBarCfdiTipoRelacion";
            this.ToolBarCfdiTipoRelacion.Text = "";
            // 
            // CommandBarLabel1
            // 
            this.CommandBarLabel1.DisplayName = "CommandBarLabel1";
            this.CommandBarLabel1.Name = "CommandBarLabel1";
            this.CommandBarLabel1.Text = "Tipo de Relación: ";
            // 
            // CommandCfdiRelacionado
            // 
            this.CommandCfdiRelacionado.DisplayName = "CommandBarStripElement2";
            this.CommandCfdiRelacionado.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarHostItemIncluir,
            this.commandBarSeparator3,
            this.CommandBarLabel1,
            this.ToolBarCfdiTipoRelacion,
            this.CommandBarSeparator1,
            this.ToolBarCfdiBuscar,
            this.ToolBarCfdiAgregar,
            this.ToolBarCfdiQuitar});
            this.CommandCfdiRelacionado.Name = "CommandCfdiRelacionado";
            // 
            // 
            // 
            this.CommandCfdiRelacionado.OverflowButton.Enabled = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.CommandCfdiRelacionado.GetChildAt(2))).Enabled = false;
            // 
            // ToolBarHostItemIncluir
            // 
            this.ToolBarHostItemIncluir.DisplayName = "Incluir";
            this.ToolBarHostItemIncluir.Name = "ToolBarHostItemIncluir";
            this.ToolBarHostItemIncluir.Text = "Incluir";
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisplayName = "commandBarSeparator3";
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // CommandBarSeparator1
            // 
            this.CommandBarSeparator1.DisplayName = "CommandBarSeparator1";
            this.CommandBarSeparator1.Name = "CommandBarSeparator1";
            this.CommandBarSeparator1.UseCompatibleTextRendering = false;
            this.CommandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarCfdiBuscar
            // 
            this.ToolBarCfdiBuscar.DisplayName = "Buscar";
            this.ToolBarCfdiBuscar.DrawText = true;
            this.ToolBarCfdiBuscar.Enabled = false;
            this.ToolBarCfdiBuscar.FlipText = false;
            this.ToolBarCfdiBuscar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_view_file;
            this.ToolBarCfdiBuscar.Name = "ToolBarCfdiBuscar";
            this.ToolBarCfdiBuscar.Text = "Buscar";
            this.ToolBarCfdiBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarCfdiBuscar.Click += new System.EventHandler(this.ToolBarCfdiBuscar_Click);
            // 
            // ToolBarCfdiAgregar
            // 
            this.ToolBarCfdiAgregar.DisplayName = "Agregar";
            this.ToolBarCfdiAgregar.DrawText = true;
            this.ToolBarCfdiAgregar.Enabled = false;
            this.ToolBarCfdiAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_agregar_archivo;
            this.ToolBarCfdiAgregar.Name = "ToolBarCfdiAgregar";
            this.ToolBarCfdiAgregar.Text = "Agregar";
            this.ToolBarCfdiAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarCfdiAgregar.Click += new System.EventHandler(this.ToolBarCfdiAgregar_Click);
            // 
            // ToolBarCfdiQuitar
            // 
            this.ToolBarCfdiQuitar.DisplayName = "Quitar";
            this.ToolBarCfdiQuitar.DrawText = true;
            this.ToolBarCfdiQuitar.Enabled = false;
            this.ToolBarCfdiQuitar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_delete_file;
            this.ToolBarCfdiQuitar.Name = "ToolBarCfdiQuitar";
            this.ToolBarCfdiQuitar.Text = "Quitar";
            this.ToolBarCfdiQuitar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarCfdiQuitar.Click += new System.EventHandler(this.ToolBarCfdiQuitar_Click);
            // 
            // CommandBarRowElement2
            // 
            this.CommandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement2.Name = "CommandBarRowElement2";
            this.CommandBarRowElement2.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.CommandCfdiRelacionado});
            this.CommandBarRowElement2.Text = "";
            // 
            // GridCfdiRelacionados
            // 
            this.GridCfdiRelacionados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridCfdiRelacionados.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.GridCfdiRelacionados.MasterTemplate.AllowAddNewRow = false;
            gridViewTextBoxColumn1.FieldName = "IdDocumento";
            gridViewTextBoxColumn1.HeaderText = "UUID";
            gridViewTextBoxColumn1.Name = "UUID";
            gridViewTextBoxColumn1.Width = 220;
            gridViewTextBoxColumn2.FieldName = "RFC";
            gridViewTextBoxColumn2.HeaderText = "RFC";
            gridViewTextBoxColumn2.Name = "RFC";
            gridViewTextBoxColumn2.Width = 110;
            gridViewTextBoxColumn3.FieldName = "Nombre";
            gridViewTextBoxColumn3.HeaderText = "Identidad Fiscal";
            gridViewTextBoxColumn3.Name = "Nombre";
            gridViewTextBoxColumn3.Width = 250;
            gridViewTextBoxColumn4.FieldName = "Serie";
            gridViewTextBoxColumn4.HeaderText = "Serie";
            gridViewTextBoxColumn4.Name = "Serie";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 75;
            gridViewTextBoxColumn5.FieldName = "Folio";
            gridViewTextBoxColumn5.HeaderText = "Folio";
            gridViewTextBoxColumn5.Name = "Folio";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 75;
            gridViewTextBoxColumn6.DataType = typeof(decimal);
            gridViewTextBoxColumn6.FieldName = "Total";
            gridViewTextBoxColumn6.FormatString = "{0:n}";
            gridViewTextBoxColumn6.HeaderText = "Total";
            gridViewTextBoxColumn6.Name = "Total";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn6.Width = 95;
            gridViewTextBoxColumn7.DataType = typeof(decimal);
            gridViewTextBoxColumn7.FieldName = "ImporteAplicado";
            gridViewTextBoxColumn7.FormatString = "{0:n}";
            gridViewTextBoxColumn7.HeaderText = "Importe";
            gridViewTextBoxColumn7.Name = "ImporteAplicado";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn7.Width = 95;
            this.GridCfdiRelacionados.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.GridCfdiRelacionados.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridCfdiRelacionados.Name = "GridCfdiRelacionados";
            this.GridCfdiRelacionados.ShowGroupPanel = false;
            this.GridCfdiRelacionados.Size = new System.Drawing.Size(1248, 186);
            this.GridCfdiRelacionados.TabIndex = 1;
            // 
            // RadCommandBar2
            // 
            this.RadCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar2.Name = "RadCommandBar2";
            this.RadCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement2});
            this.RadCommandBar2.Size = new System.Drawing.Size(1248, 0);
            this.RadCommandBar2.TabIndex = 0;
            // 
            // PageViewCfdiRelacionado
            // 
            this.PageViewCfdiRelacionado.Controls.Add(this.GridCfdiRelacionados);
            this.PageViewCfdiRelacionado.Controls.Add(this.RadCommandBar2);
            this.PageViewCfdiRelacionado.ItemSize = new System.Drawing.SizeF(104F, 28F);
            this.PageViewCfdiRelacionado.Location = new System.Drawing.Point(10, 37);
            this.PageViewCfdiRelacionado.Name = "PageViewCfdiRelacionado";
            this.PageViewCfdiRelacionado.Size = new System.Drawing.Size(1248, 186);
            this.PageViewCfdiRelacionado.Text = "CFDI Relacionado";
            // 
            // ToolBarLabelProducto
            // 
            this.ToolBarLabelProducto.DisplayName = "CommandBarLabel2";
            this.ToolBarLabelProducto.Name = "ToolBarLabelProducto";
            this.ToolBarLabelProducto.Text = "Prod. Serv.";
            // 
            // CommandConceptos
            // 
            this.CommandConceptos.DisplayName = "Barra de Conceptos";
            this.CommandConceptos.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelProducto,
            this.ToolBarHostConceptos,
            this.ToolBarConceptoAgregar,
            this.ToolBarConceptoQuitar,
            this.ToolBarConceptoButtonNuevo,
            this.ToolBarConceptoBuscar});
            this.CommandConceptos.Name = "CommandConceptos";
            // 
            // 
            // 
            this.CommandConceptos.OverflowButton.Enabled = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.CommandConceptos.GetChildAt(2))).Enabled = false;
            // 
            // ToolBarHostConceptos
            // 
            this.ToolBarHostConceptos.DisplayName = "commandBarHostItem1";
            this.ToolBarHostConceptos.MinSize = new System.Drawing.Size(360, 0);
            this.ToolBarHostConceptos.Name = "ToolBarHostConceptos";
            this.ToolBarHostConceptos.Text = "";
            // 
            // ToolBarConceptoAgregar
            // 
            this.ToolBarConceptoAgregar.DisplayName = "Agregar";
            this.ToolBarConceptoAgregar.DrawText = true;
            this.ToolBarConceptoAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_agregar_archivo;
            this.ToolBarConceptoAgregar.Name = "ToolBarConceptoAgregar";
            this.ToolBarConceptoAgregar.Text = "Agregar";
            this.ToolBarConceptoAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarConceptoAgregar.Click += new System.EventHandler(this.ToolBarConceptoAgregar_Click);
            // 
            // ToolBarConceptoQuitar
            // 
            this.ToolBarConceptoQuitar.DisplayName = "Quitar";
            this.ToolBarConceptoQuitar.DrawText = true;
            this.ToolBarConceptoQuitar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_delete_file;
            this.ToolBarConceptoQuitar.Name = "ToolBarConceptoQuitar";
            this.ToolBarConceptoQuitar.Text = "Quitar";
            this.ToolBarConceptoQuitar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarConceptoQuitar.Click += new System.EventHandler(this.ToolBarConceptoQuitar_Click);
            // 
            // ToolBarConceptoButtonNuevo
            // 
            this.ToolBarConceptoButtonNuevo.DisplayName = "Nuevo";
            this.ToolBarConceptoButtonNuevo.DrawText = true;
            this.ToolBarConceptoButtonNuevo.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_new_file;
            this.ToolBarConceptoButtonNuevo.Name = "ToolBarConceptoButtonNuevo";
            this.ToolBarConceptoButtonNuevo.Text = "Nuevo";
            this.ToolBarConceptoButtonNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarConceptoButtonNuevo.Click += new System.EventHandler(this.ToolBarConceptoButtonNuevo_Click);
            // 
            // ToolBarConceptoBuscar
            // 
            this.ToolBarConceptoBuscar.DisplayName = "Buscar";
            this.ToolBarConceptoBuscar.DrawText = true;
            this.ToolBarConceptoBuscar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_view_file;
            this.ToolBarConceptoBuscar.Name = "ToolBarConceptoBuscar";
            this.ToolBarConceptoBuscar.Text = "Buscar";
            this.ToolBarConceptoBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarConceptoBuscar.Click += new System.EventHandler(this.ToolBarConceptoBuscar_Click);
            // 
            // CommandBarRowElement4
            // 
            this.CommandBarRowElement4.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement4.Name = "CommandBarRowElement4";
            this.CommandBarRowElement4.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.CommandConceptos});
            this.CommandBarRowElement4.Text = "";
            // 
            // RadCommandBar4
            // 
            this.RadCommandBar4.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar4.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar4.Name = "RadCommandBar4";
            this.RadCommandBar4.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement4});
            this.RadCommandBar4.Size = new System.Drawing.Size(1248, 30);
            this.RadCommandBar4.TabIndex = 110;
            // 
            // PageViewConcepto
            // 
            this.PageViewConcepto.Controls.Add(this.GridConceptos);
            this.PageViewConcepto.Controls.Add(this.RadCommandBar4);
            this.PageViewConcepto.ItemSize = new System.Drawing.SizeF(69F, 28F);
            this.PageViewConcepto.Location = new System.Drawing.Point(10, 37);
            this.PageViewConcepto.Name = "PageViewConcepto";
            this.PageViewConcepto.Size = new System.Drawing.Size(1248, 186);
            this.PageViewConcepto.Text = "Conceptos";
            // 
            // GridConceptos
            // 
            this.GridConceptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridConceptos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridConceptos.MasterTemplate.AllowAddNewRow = false;
            this.GridConceptos.MasterTemplate.AllowDeleteRow = false;
            gridViewTextBoxColumn8.DataType = typeof(int);
            gridViewTextBoxColumn8.FieldName = "Id";
            gridViewTextBoxColumn8.HeaderText = "Id";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "Id";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn8.VisibleInColumnChooser = false;
            gridViewTextBoxColumn9.DataType = typeof(int);
            gridViewTextBoxColumn9.FieldName = "SubId";
            gridViewTextBoxColumn9.HeaderText = "SubId";
            gridViewTextBoxColumn9.IsVisible = false;
            gridViewTextBoxColumn9.Name = "SubId";
            gridViewTextBoxColumn9.VisibleInColumnChooser = false;
            conditionalFormattingObject1.ApplyToRow = true;
            conditionalFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.CellFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            conditionalFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.Name = "Activo";
            conditionalFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            conditionalFormattingObject1.RowForeColor = System.Drawing.Color.Gray;
            conditionalFormattingObject1.TValue1 = "False";
            gridViewCheckBoxColumn1.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            gridViewCheckBoxColumn1.FieldName = "IsActive";
            gridViewCheckBoxColumn1.HeaderText = "Activo";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "IsActive";
            gridViewTextBoxColumn10.FieldName = "NumPedido";
            gridViewTextBoxColumn10.HeaderText = "# Pedido";
            gridViewTextBoxColumn10.Name = "NumPedido";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn10.Width = 60;
            gridViewTextBoxColumn11.DataType = typeof(decimal);
            gridViewTextBoxColumn11.FieldName = "Cantidad";
            gridViewTextBoxColumn11.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn11.HeaderText = "Cantidad";
            gridViewTextBoxColumn11.Name = "Cantidad";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn11.Width = 80;
            gridViewTextBoxColumn12.FieldName = "Unidad";
            gridViewTextBoxColumn12.HeaderText = "Unidad";
            gridViewTextBoxColumn12.Name = "Uniadd";
            gridViewTextBoxColumn12.Width = 80;
            gridViewTextBoxColumn13.FieldName = "ClaveUnidad";
            gridViewTextBoxColumn13.HeaderText = "Clv. Unidad";
            gridViewTextBoxColumn13.Name = "ClaveUnidad";
            gridViewTextBoxColumn13.Width = 80;
            gridViewTextBoxColumn14.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn14.HeaderText = "No. Ident.";
            gridViewTextBoxColumn14.Name = "NoIdentificacion";
            gridViewTextBoxColumn14.Width = 90;
            gridViewTextBoxColumn15.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn15.HeaderText = "Clv. Producto";
            gridViewTextBoxColumn15.Name = "ClaveProducto";
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn15.Width = 80;
            gridViewTextBoxColumn16.FieldName = "Descripcion";
            gridViewTextBoxColumn16.HeaderText = "Concepto";
            gridViewTextBoxColumn16.Name = "Descripcion";
            gridViewTextBoxColumn16.Width = 325;
            gridViewTextBoxColumn17.DataType = typeof(decimal);
            gridViewTextBoxColumn17.FieldName = "ValorUnitario";
            gridViewTextBoxColumn17.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn17.HeaderText = "Unitario";
            gridViewTextBoxColumn17.Name = "ValorUnitario";
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn17.Width = 80;
            gridViewTextBoxColumn18.DataType = typeof(decimal);
            gridViewTextBoxColumn18.FieldName = "Descuento";
            gridViewTextBoxColumn18.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn18.HeaderText = "Descuento";
            gridViewTextBoxColumn18.Name = "Descuento";
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn18.Width = 80;
            gridViewTextBoxColumn19.FieldName = "CtaPredial";
            gridViewTextBoxColumn19.HeaderText = "Cta. Predial";
            gridViewTextBoxColumn19.Name = "CtaPredial";
            gridViewTextBoxColumn19.Width = 80;
            gridViewTextBoxColumn20.DataType = typeof(decimal);
            gridViewTextBoxColumn20.EnableExpressionEditor = false;
            gridViewTextBoxColumn20.Expression = "";
            gridViewTextBoxColumn20.FieldName = "Importe";
            gridViewTextBoxColumn20.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn20.HeaderText = "Importe";
            gridViewTextBoxColumn20.Name = "Importe";
            gridViewTextBoxColumn20.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn20.Width = 80;
            this.GridConceptos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20});
            this.GridConceptos.MasterTemplate.EnableGrouping = false;
            this.GridConceptos.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GridConceptos.Name = "GridConceptos";
            this.GridConceptos.Size = new System.Drawing.Size(1248, 156);
            this.GridConceptos.TabIndex = 109;
            // 
            // ComprobanteConcepto
            // 
            this.ComprobanteConcepto.Controls.Add(this.PageViewConcepto);
            this.ComprobanteConcepto.Controls.Add(this.PageViewCfdiRelacionado);
            this.ComprobanteConcepto.DefaultPage = this.PageViewConcepto;
            this.ComprobanteConcepto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComprobanteConcepto.Location = new System.Drawing.Point(0, 0);
            this.ComprobanteConcepto.Name = "ComprobanteConcepto";
            this.ComprobanteConcepto.SelectedPage = this.PageViewConcepto;
            this.ComprobanteConcepto.Size = new System.Drawing.Size(1269, 234);
            this.ComprobanteConcepto.TabIndex = 168;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.ComprobanteConcepto.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // PageConceptoAduana
            // 
            this.PageConceptoAduana.Controls.Add(this.GridConceptoInformacionAduanera);
            this.PageConceptoAduana.Controls.Add(this.radCommandBar7);
            this.PageConceptoAduana.ItemSize = new System.Drawing.SizeF(108F, 28F);
            this.PageConceptoAduana.Location = new System.Drawing.Point(10, 37);
            this.PageConceptoAduana.Name = "PageConceptoAduana";
            this.PageConceptoAduana.Size = new System.Drawing.Size(996, 169);
            this.PageConceptoAduana.Text = "Concepto: Aduana";
            // 
            // GridConceptoInformacionAduanera
            // 
            this.GridConceptoInformacionAduanera.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridConceptoInformacionAduanera.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.GridConceptoInformacionAduanera.MasterTemplate.AllowAddNewRow = false;
            this.GridConceptoInformacionAduanera.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn21.FieldName = "NumeroPedimento";
            gridViewTextBoxColumn21.HeaderText = "Numero de Pedimento";
            gridViewTextBoxColumn21.Name = "NumeroPedimento";
            gridViewTextBoxColumn21.Width = 150;
            this.GridConceptoInformacionAduanera.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn21});
            this.GridConceptoInformacionAduanera.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.GridConceptoInformacionAduanera.Name = "GridConceptoInformacionAduanera";
            this.GridConceptoInformacionAduanera.ShowGroupPanel = false;
            this.GridConceptoInformacionAduanera.Size = new System.Drawing.Size(996, 169);
            this.GridConceptoInformacionAduanera.TabIndex = 1;
            // 
            // radCommandBar7
            // 
            this.radCommandBar7.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar7.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar7.Name = "radCommandBar7";
            this.radCommandBar7.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement8});
            this.radCommandBar7.Size = new System.Drawing.Size(996, 0);
            this.radCommandBar7.TabIndex = 0;
            // 
            // commandBarRowElement8
            // 
            this.commandBarRowElement8.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement8.Name = "commandBarRowElement8";
            this.commandBarRowElement8.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.CommandConceptoAduana});
            // 
            // CommandConceptoAduana
            // 
            this.CommandConceptoAduana.DisplayName = "commandBarStripElement1";
            this.CommandConceptoAduana.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarConceptoAduanaAgregar,
            this.ToolBarConceptoAduanaQuitar});
            this.CommandConceptoAduana.Name = "CommandConceptoAduana";
            // 
            // 
            // 
            this.CommandConceptoAduana.OverflowButton.Enabled = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.CommandConceptoAduana.GetChildAt(2))).Enabled = false;
            // 
            // ToolBarConceptoAduanaAgregar
            // 
            this.ToolBarConceptoAduanaAgregar.DisplayName = "Agregar";
            this.ToolBarConceptoAduanaAgregar.DrawText = true;
            this.ToolBarConceptoAduanaAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_agregar_archivo;
            this.ToolBarConceptoAduanaAgregar.Name = "ToolBarConceptoAduanaAgregar";
            this.ToolBarConceptoAduanaAgregar.Text = "Agregar";
            this.ToolBarConceptoAduanaAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarConceptoAduanaAgregar.Click += new System.EventHandler(this.ToolBarConceptoAduanaAgregar_Click);
            // 
            // ToolBarConceptoAduanaQuitar
            // 
            this.ToolBarConceptoAduanaQuitar.DisplayName = "Quitar";
            this.ToolBarConceptoAduanaQuitar.DrawText = true;
            this.ToolBarConceptoAduanaQuitar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_delete_file;
            this.ToolBarConceptoAduanaQuitar.Name = "ToolBarConceptoAduanaQuitar";
            this.ToolBarConceptoAduanaQuitar.Text = "Quitar";
            this.ToolBarConceptoAduanaQuitar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarConceptoAduanaQuitar.Click += new System.EventHandler(this.ToolBarConceptoAduanaQuitar_Click);
            // 
            // CommandConceptoImpuesto
            // 
            this.CommandConceptoImpuesto.DisplayName = "CommandBarStripElement7";
            this.CommandConceptoImpuesto.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarConceptoImpuestoAgregar,
            this.ToolBarConceptoImpuestoQuitar});
            this.CommandConceptoImpuesto.Name = "CommandConceptoImpuesto";
            // 
            // 
            // 
            this.CommandConceptoImpuesto.OverflowButton.Enabled = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.CommandConceptoImpuesto.GetChildAt(2))).Enabled = false;
            // 
            // ToolBarConceptoImpuestoAgregar
            // 
            this.ToolBarConceptoImpuestoAgregar.DisplayName = "Agregar";
            this.ToolBarConceptoImpuestoAgregar.DrawText = true;
            this.ToolBarConceptoImpuestoAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_agregar_archivo;
            this.ToolBarConceptoImpuestoAgregar.Name = "ToolBarConceptoImpuestoAgregar";
            this.ToolBarConceptoImpuestoAgregar.Text = "Agregar";
            this.ToolBarConceptoImpuestoAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarConceptoImpuestoAgregar.Click += new System.EventHandler(this.ToolBarConceptoImpuestoAgregar_Click);
            // 
            // ToolBarConceptoImpuestoQuitar
            // 
            this.ToolBarConceptoImpuestoQuitar.DisplayName = "Quitar";
            this.ToolBarConceptoImpuestoQuitar.DrawText = true;
            this.ToolBarConceptoImpuestoQuitar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_delete_file;
            this.ToolBarConceptoImpuestoQuitar.Name = "ToolBarConceptoImpuestoQuitar";
            this.ToolBarConceptoImpuestoQuitar.Text = "Quitar";
            this.ToolBarConceptoImpuestoQuitar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarConceptoImpuestoQuitar.Click += new System.EventHandler(this.ToolBarConceptoImpuestoQuitar_Click);
            // 
            // CommandBarRowElement7
            // 
            this.CommandBarRowElement7.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement7.Name = "CommandBarRowElement7";
            this.CommandBarRowElement7.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.CommandConceptoImpuesto});
            this.CommandBarRowElement7.Text = "";
            // 
            // GridConceptoImpuestos
            // 
            this.GridConceptoImpuestos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridConceptoImpuestos.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.GridConceptoImpuestos.MasterTemplate.AllowAddNewRow = false;
            gridViewComboBoxColumn1.FieldName = "Tipo";
            gridViewComboBoxColumn1.HeaderText = "Tipo";
            gridViewComboBoxColumn1.Name = "Tipo";
            gridViewComboBoxColumn1.Width = 80;
            gridViewComboBoxColumn2.FieldName = "Impuesto";
            gridViewComboBoxColumn2.HeaderText = "Impuesto";
            gridViewComboBoxColumn2.Name = "Impuesto";
            gridViewComboBoxColumn2.Width = 80;
            gridViewTextBoxColumn22.DataType = typeof(decimal);
            gridViewTextBoxColumn22.FieldName = "Base";
            gridViewTextBoxColumn22.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn22.HeaderText = "Base";
            gridViewTextBoxColumn22.Name = "Base";
            gridViewTextBoxColumn22.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn22.Width = 80;
            gridViewComboBoxColumn3.FieldName = "TipoFactor";
            gridViewComboBoxColumn3.HeaderText = "Tipo Factor";
            gridViewComboBoxColumn3.Name = "TipoFactor";
            gridViewComboBoxColumn3.Width = 80;
            gridViewTextBoxColumn23.DataType = typeof(decimal);
            gridViewTextBoxColumn23.FieldName = "TasaOCuota";
            gridViewTextBoxColumn23.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn23.HeaderText = "Tasa ó Cuota";
            gridViewTextBoxColumn23.Name = "TasaOCuota";
            gridViewTextBoxColumn23.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn23.Width = 80;
            gridViewTextBoxColumn24.DataType = typeof(decimal);
            gridViewTextBoxColumn24.FieldName = "Importe";
            gridViewTextBoxColumn24.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn24.HeaderText = "Importe";
            gridViewTextBoxColumn24.Name = "Importe";
            gridViewTextBoxColumn24.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn24.Width = 80;
            this.GridConceptoImpuestos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn1,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn22,
            gridViewComboBoxColumn3,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24});
            this.GridConceptoImpuestos.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.GridConceptoImpuestos.Name = "GridConceptoImpuestos";
            this.GridConceptoImpuestos.ShowGroupPanel = false;
            this.GridConceptoImpuestos.Size = new System.Drawing.Size(996, 169);
            this.GridConceptoImpuestos.TabIndex = 1;
            // 
            // RadCommandBar6
            // 
            this.RadCommandBar6.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar6.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar6.Name = "RadCommandBar6";
            this.RadCommandBar6.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement7});
            this.RadCommandBar6.Size = new System.Drawing.Size(996, 0);
            this.RadCommandBar6.TabIndex = 2;
            // 
            // PageConceptoImpuesto
            // 
            this.PageConceptoImpuesto.Controls.Add(this.GridConceptoImpuestos);
            this.PageConceptoImpuesto.Controls.Add(this.RadCommandBar6);
            this.PageConceptoImpuesto.ItemSize = new System.Drawing.SizeF(122F, 28F);
            this.PageConceptoImpuesto.Location = new System.Drawing.Point(10, 37);
            this.PageConceptoImpuesto.Name = "PageConceptoImpuesto";
            this.PageConceptoImpuesto.Size = new System.Drawing.Size(996, 169);
            this.PageConceptoImpuesto.Text = "Concepto: Impuestos";
            // 
            // FechaCertifica
            // 
            this.FechaCertifica.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaCertifica.Location = new System.Drawing.Point(1040, 11);
            this.FechaCertifica.Name = "FechaCertifica";
            this.FechaCertifica.NullText = "--/--/----";
            this.FechaCertifica.ReadOnly = true;
            this.FechaCertifica.Size = new System.Drawing.Size(93, 20);
            this.FechaCertifica.TabIndex = 184;
            this.FechaCertifica.TabStop = false;
            this.FechaCertifica.Text = "05/12/2017";
            this.FechaCertifica.Value = new System.DateTime(2017, 12, 5, 22, 46, 27, 169);
            // 
            // RadLabel2
            // 
            this.RadLabel2.Location = new System.Drawing.Point(931, 11);
            this.RadLabel2.Name = "RadLabel2";
            this.RadLabel2.Size = new System.Drawing.Size(108, 18);
            this.RadLabel2.TabIndex = 185;
            this.RadLabel2.Text = "Fec. de Certificación:";
            // 
            // TipoComprobante
            // 
            this.TipoComprobante.Location = new System.Drawing.Point(1040, 36);
            this.TipoComprobante.Name = "TipoComprobante";
            this.TipoComprobante.Size = new System.Drawing.Size(93, 19);
            this.TipoComprobante.TabIndex = 183;
            this.TipoComprobante.Text = "-----";
            // 
            // RadLabel31
            // 
            this.RadLabel31.Location = new System.Drawing.Point(16, 173);
            this.RadLabel31.Name = "RadLabel31";
            this.RadLabel31.Size = new System.Drawing.Size(31, 18);
            this.RadLabel31.TabIndex = 182;
            this.RadLabel31.Text = "Total";
            // 
            // RadLabel30
            // 
            this.RadLabel30.Location = new System.Drawing.Point(16, 147);
            this.RadLabel30.Name = "RadLabel30";
            this.RadLabel30.Size = new System.Drawing.Size(75, 18);
            this.RadLabel30.TabIndex = 181;
            this.RadLabel30.Text = "Retención ISR";
            // 
            // RadLabel29
            // 
            this.RadLabel29.Location = new System.Drawing.Point(16, 121);
            this.RadLabel29.Name = "RadLabel29";
            this.RadLabel29.Size = new System.Drawing.Size(77, 18);
            this.RadLabel29.TabIndex = 180;
            this.RadLabel29.Text = "Retención IVA";
            // 
            // RadLabel28
            // 
            this.RadLabel28.Location = new System.Drawing.Point(16, 17);
            this.RadLabel28.Name = "RadLabel28";
            this.RadLabel28.Size = new System.Drawing.Size(50, 18);
            this.RadLabel28.TabIndex = 179;
            this.RadLabel28.Text = "SubTotal";
            // 
            // RadLabel27
            // 
            this.RadLabel27.Location = new System.Drawing.Point(16, 95);
            this.RadLabel27.Name = "RadLabel27";
            this.RadLabel27.Size = new System.Drawing.Size(27, 18);
            this.RadLabel27.TabIndex = 178;
            this.RadLabel27.Text = "IEPS";
            // 
            // RadLabel26
            // 
            this.RadLabel26.Location = new System.Drawing.Point(16, 69);
            this.RadLabel26.Name = "RadLabel26";
            this.RadLabel26.Size = new System.Drawing.Size(24, 18);
            this.RadLabel26.TabIndex = 177;
            this.RadLabel26.Text = "IVA";
            // 
            // RadLabel24
            // 
            this.RadLabel24.Location = new System.Drawing.Point(16, 43);
            this.RadLabel24.Name = "RadLabel24";
            this.RadLabel24.Size = new System.Drawing.Size(59, 18);
            this.RadLabel24.TabIndex = 176;
            this.RadLabel24.Text = "Descuento";
            // 
            // CommandConceptoParte
            // 
            this.CommandConceptoParte.DisplayName = "CommandBarStripElement6";
            // 
            // 
            // 
            this.CommandConceptoParte.Grip.Enabled = true;
            this.CommandConceptoParte.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarConceptoParteBuscar,
            this.ToolBarConceptoParteAgregar,
            this.ToolBarConceptoParteQuitar});
            this.CommandConceptoParte.Name = "CommandConceptoParte";
            // 
            // 
            // 
            this.CommandConceptoParte.OverflowButton.Enabled = false;
            ((Telerik.WinControls.UI.RadCommandBarGrip)(this.CommandConceptoParte.GetChildAt(0))).Enabled = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.CommandConceptoParte.GetChildAt(2))).Enabled = false;
            // 
            // ToolBarConceptoParteBuscar
            // 
            this.ToolBarConceptoParteBuscar.DisplayName = "Buscar";
            this.ToolBarConceptoParteBuscar.DrawText = true;
            this.ToolBarConceptoParteBuscar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_view_file;
            this.ToolBarConceptoParteBuscar.Name = "ToolBarConceptoParteBuscar";
            this.ToolBarConceptoParteBuscar.Text = "Buscar";
            this.ToolBarConceptoParteBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarConceptoParteAgregar
            // 
            this.ToolBarConceptoParteAgregar.DisplayName = "Agregar";
            this.ToolBarConceptoParteAgregar.DrawText = true;
            this.ToolBarConceptoParteAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_agregar_archivo;
            this.ToolBarConceptoParteAgregar.Name = "ToolBarConceptoParteAgregar";
            this.ToolBarConceptoParteAgregar.Text = "Agregar";
            this.ToolBarConceptoParteAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarConceptoParteAgregar.Click += new System.EventHandler(this.ToolBarConceptoParteAgregar_Click);
            // 
            // ToolBarConceptoParteQuitar
            // 
            this.ToolBarConceptoParteQuitar.DisplayName = "Quitar";
            this.ToolBarConceptoParteQuitar.DrawText = true;
            this.ToolBarConceptoParteQuitar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_delete_file;
            this.ToolBarConceptoParteQuitar.Name = "ToolBarConceptoParteQuitar";
            this.ToolBarConceptoParteQuitar.Text = "Quitar";
            this.ToolBarConceptoParteQuitar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarConceptoParteQuitar.Click += new System.EventHandler(this.ToolBarConceptoParteQuitar_Click);
            // 
            // ComprobanteConceptosPartes
            // 
            this.ComprobanteConceptosPartes.Controls.Add(this.PageConceptoParte);
            this.ComprobanteConceptosPartes.Controls.Add(this.PageConceptoImpuesto);
            this.ComprobanteConceptosPartes.Controls.Add(this.PageConceptoAduana);
            this.ComprobanteConceptosPartes.DefaultPage = this.PageConceptoParte;
            this.ComprobanteConceptosPartes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComprobanteConceptosPartes.Location = new System.Drawing.Point(0, 0);
            this.ComprobanteConceptosPartes.Name = "ComprobanteConceptosPartes";
            this.ComprobanteConceptosPartes.SelectedPage = this.PageConceptoParte;
            this.ComprobanteConceptosPartes.Size = new System.Drawing.Size(1017, 217);
            this.ComprobanteConceptosPartes.TabIndex = 167;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.ComprobanteConceptosPartes.GetChildAt(0))).ShowItemPinButton = false;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.ComprobanteConceptosPartes.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.ComprobanteConceptosPartes.GetChildAt(0))).ShowItemCloseButton = false;
            // 
            // PageConceptoParte
            // 
            this.PageConceptoParte.Controls.Add(this.GridConceptoParte);
            this.PageConceptoParte.Controls.Add(this.RadCommandBar5);
            this.PageConceptoParte.ItemSize = new System.Drawing.SizeF(96F, 28F);
            this.PageConceptoParte.Location = new System.Drawing.Point(10, 37);
            this.PageConceptoParte.Name = "PageConceptoParte";
            this.PageConceptoParte.Size = new System.Drawing.Size(996, 169);
            this.PageConceptoParte.Text = "Concepto: Parte";
            // 
            // GridConceptoParte
            // 
            this.GridConceptoParte.AutoGenerateHierarchy = true;
            this.GridConceptoParte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridConceptoParte.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridConceptoParte.MasterTemplate.AllowAddNewRow = false;
            this.GridConceptoParte.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn25.DataType = typeof(decimal);
            gridViewTextBoxColumn25.FieldName = "Cantidad";
            gridViewTextBoxColumn25.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn25.HeaderText = "Cantidad";
            gridViewTextBoxColumn25.Name = "Cantidad";
            gridViewTextBoxColumn25.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn25.Width = 80;
            gridViewTextBoxColumn26.FieldName = "Unidad";
            gridViewTextBoxColumn26.HeaderText = "Unidad";
            gridViewTextBoxColumn26.Name = "Unidad";
            gridViewTextBoxColumn26.Width = 80;
            gridViewTextBoxColumn27.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn27.HeaderText = "Clv. Producto";
            gridViewTextBoxColumn27.Name = "ClaveProdServ";
            gridViewTextBoxColumn27.Width = 80;
            gridViewTextBoxColumn28.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn28.HeaderText = "No. Ident.";
            gridViewTextBoxColumn28.Name = "NoIdentificacion";
            gridViewTextBoxColumn28.Width = 90;
            gridViewTextBoxColumn29.FieldName = "Descripcion";
            gridViewTextBoxColumn29.HeaderText = "Descripcion";
            gridViewTextBoxColumn29.Name = "Descripcion";
            gridViewTextBoxColumn29.Width = 250;
            gridViewTextBoxColumn30.DataType = typeof(decimal);
            gridViewTextBoxColumn30.FieldName = "ValorUnitario";
            gridViewTextBoxColumn30.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn30.HeaderText = "Valor \n\rUnitario";
            gridViewTextBoxColumn30.Name = "ValorUnitario";
            gridViewTextBoxColumn30.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn30.Width = 80;
            gridViewTextBoxColumn31.DataType = typeof(decimal);
            gridViewTextBoxColumn31.FieldName = "Importe";
            gridViewTextBoxColumn31.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn31.HeaderText = "Importe";
            gridViewTextBoxColumn31.Name = "Importe";
            gridViewTextBoxColumn31.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn31.Width = 80;
            this.GridConceptoParte.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31});
            this.GridConceptoParte.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.GridConceptoParte.Name = "GridConceptoParte";
            this.GridConceptoParte.ShowGroupPanel = false;
            this.GridConceptoParte.Size = new System.Drawing.Size(996, 139);
            this.GridConceptoParte.TabIndex = 0;
            // 
            // RadCommandBar5
            // 
            this.RadCommandBar5.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar5.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar5.Name = "RadCommandBar5";
            this.RadCommandBar5.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement6});
            this.RadCommandBar5.Size = new System.Drawing.Size(996, 30);
            this.RadCommandBar5.TabIndex = 1;
            // 
            // CommandBarRowElement6
            // 
            this.CommandBarRowElement6.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement6.Name = "CommandBarRowElement6";
            this.CommandBarRowElement6.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.CommandConceptoParte});
            this.CommandBarRowElement6.Text = "";
            // 
            // RadCommandBar1
            // 
            this.RadCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar1.Name = "RadCommandBar1";
            this.RadCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement1});
            this.RadCommandBar1.Size = new System.Drawing.Size(1269, 30);
            this.RadCommandBar1.TabIndex = 166;
            // 
            // CommandBarRowElement1
            // 
            this.CommandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement1.Name = "CommandBarRowElement1";
            this.CommandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.CommandComprobanteFiscal});
            this.CommandBarRowElement1.Text = "";
            // 
            // CommandComprobanteFiscal
            // 
            this.CommandComprobanteFiscal.DisplayName = "Emision de Comprobante";
            this.CommandComprobanteFiscal.EnableFocusBorder = false;
            this.CommandComprobanteFiscal.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarEmisor,
            this.Separador,
            this.ToolLabelStatus,
            this.ToolBarStatus,
            this.ToolBarNew,
            this.ToolBarCopy,
            this.ToolBarSave,
            this.ToolBarRefresh,
            this.ToolBarCreate,
            this.ToolBarCancelar,
            this.ToolBarPdf,
            this.ToolBarXml,
            this.ToolBarEmail,
            this.Separator2,
            this.ToolBarSeries,
            this.CommandBarSeparator2,
            this.ToolLabelUuid,
            this.ToolBarTextBoxIdDocumento,
            this.ToolBarClose});
            this.CommandComprobanteFiscal.Name = "CommandComprobanteFiscal";
            // 
            // 
            // 
            this.CommandComprobanteFiscal.OverflowButton.Enabled = true;
            this.CommandComprobanteFiscal.ShowHorizontalLine = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.CommandComprobanteFiscal.GetChildAt(2))).Enabled = true;
            // 
            // ToolBarEmisor
            // 
            this.ToolBarEmisor.DefaultItem = null;
            this.ToolBarEmisor.DisplayName = "Emisor";
            this.ToolBarEmisor.DrawText = true;
            this.ToolBarEmisor.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_acercadelusuario;
            this.ToolBarEmisor.Name = "ToolBarEmisor";
            this.ToolBarEmisor.Text = "Emisor";
            this.ToolBarEmisor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separador
            // 
            this.Separador.DisplayName = "Separador 2";
            this.Separador.Name = "Separador";
            this.Separador.UseCompatibleTextRendering = false;
            this.Separador.VisibleInOverflowMenu = false;
            this.Separador.VisibleInStrip = false;
            // 
            // ToolLabelStatus
            // 
            this.ToolLabelStatus.DisplayName = "Etiqueta Status";
            this.ToolLabelStatus.Name = "ToolLabelStatus";
            this.ToolLabelStatus.Text = "Status:";
            // 
            // ToolBarStatus
            // 
            this.ToolBarStatus.DefaultItem = null;
            this.ToolBarStatus.DisplayName = "Estado";
            this.ToolBarStatus.DrawImage = false;
            this.ToolBarStatus.DrawText = true;
            this.ToolBarStatus.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarStatus.Image")));
            this.ToolBarStatus.MinSize = new System.Drawing.Size(70, 26);
            this.ToolBarStatus.Name = "ToolBarStatus";
            this.ToolBarStatus.Text = "EnEspera";
            // 
            // ToolBarNew
            // 
            this.ToolBarNew.DisplayName = "Nuevo";
            this.ToolBarNew.DrawText = true;
            this.ToolBarNew.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_new_file;
            this.ToolBarNew.Name = "ToolBarNew";
            this.ToolBarNew.Text = "Nuevo";
            this.ToolBarNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarNew.Click += new System.EventHandler(this.ToolBarNew_Click);
            // 
            // ToolBarCopy
            // 
            this.ToolBarCopy.DisplayName = "Duplicar";
            this.ToolBarCopy.DrawText = true;
            this.ToolBarCopy.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_copiar;
            this.ToolBarCopy.Name = "ToolBarCopy";
            this.ToolBarCopy.Text = "Duplicar";
            this.ToolBarCopy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarCopy.Click += new System.EventHandler(this.ToolBarCopy_Click);
            // 
            // ToolBarSave
            // 
            this.ToolBarSave.DisplayName = "Guardar";
            this.ToolBarSave.DrawText = true;
            this.ToolBarSave.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_guardar_todo;
            this.ToolBarSave.Name = "ToolBarSave";
            this.ToolBarSave.Text = "Guardar";
            this.ToolBarSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarSave.Click += new System.EventHandler(this.ToolBarSave_Click);
            // 
            // ToolBarRefresh
            // 
            this.ToolBarRefresh.DisplayName = "Actualizar";
            this.ToolBarRefresh.DrawText = true;
            this.ToolBarRefresh.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_actualizar;
            this.ToolBarRefresh.Name = "ToolBarRefresh";
            this.ToolBarRefresh.Text = "Actualizar";
            this.ToolBarRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarRefresh.Click += new System.EventHandler(this.ToolBarRefresh_Click);
            // 
            // ToolBarCreate
            // 
            this.ToolBarCreate.DisplayName = "Timbrar";
            this.ToolBarCreate.DrawText = true;
            this.ToolBarCreate.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_aprobado;
            this.ToolBarCreate.Name = "ToolBarCreate";
            this.ToolBarCreate.Text = "Timbrar";
            this.ToolBarCreate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarCreate.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ToolBarCreate.Click += new System.EventHandler(this.ToolBarCreate_Click);
            // 
            // ToolBarCancelar
            // 
            this.ToolBarCancelar.DisplayName = "Cancelar";
            this.ToolBarCancelar.DrawText = true;
            this.ToolBarCancelar.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarCancelar.Image")));
            this.ToolBarCancelar.Name = "ToolBarCancelar";
            this.ToolBarCancelar.Text = "Cancelar";
            this.ToolBarCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarCancelar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ToolBarCancelar.Click += new System.EventHandler(this.ToolBarCancelar_Click);
            // 
            // ToolBarPdf
            // 
            this.ToolBarPdf.DisplayName = "Archivo PDF";
            this.ToolBarPdf.DrawText = true;
            this.ToolBarPdf.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_pdf;
            this.ToolBarPdf.Name = "ToolBarPdf";
            this.ToolBarPdf.Text = "PDF";
            this.ToolBarPdf.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarPdf.Click += new System.EventHandler(this.ToolBarPdf_Click);
            // 
            // ToolBarXml
            // 
            this.ToolBarXml.DisplayName = "Archivo XML";
            this.ToolBarXml.DrawText = true;
            this.ToolBarXml.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_open_document;
            this.ToolBarXml.Name = "ToolBarXml";
            this.ToolBarXml.Text = "XML";
            this.ToolBarXml.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarXml.Click += new System.EventHandler(this.ToolBarXml_Click);
            // 
            // ToolBarEmail
            // 
            this.ToolBarEmail.DisplayName = "Correo";
            this.ToolBarEmail.DrawText = true;
            this.ToolBarEmail.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_email_document;
            this.ToolBarEmail.Name = "ToolBarEmail";
            this.ToolBarEmail.Text = "Envíar";
            this.ToolBarEmail.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separator2
            // 
            this.Separator2.DisplayName = "CommandBarSeparator2";
            this.Separator2.Name = "Separator2";
            this.Separator2.VisibleInOverflowMenu = false;
            this.Separator2.VisibleInStrip = false;
            // 
            // ToolBarSeries
            // 
            this.ToolBarSeries.DisplayName = "Series y Folios";
            this.ToolBarSeries.DrawText = true;
            this.ToolBarSeries.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_concept;
            this.ToolBarSeries.Name = "ToolBarSeries";
            this.ToolBarSeries.Text = "Series y Folios";
            this.ToolBarSeries.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // CommandBarSeparator2
            // 
            this.CommandBarSeparator2.DisplayName = "CommandBarSeparator2";
            this.CommandBarSeparator2.Name = "CommandBarSeparator2";
            this.CommandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // ToolLabelUuid
            // 
            this.ToolLabelUuid.DisplayName = "UUID";
            this.ToolLabelUuid.Name = "ToolLabelUuid";
            this.ToolLabelUuid.Text = "UUID:";
            this.ToolLabelUuid.VisibleInOverflowMenu = false;
            // 
            // ToolBarTextBoxIdDocumento
            // 
            this.ToolBarTextBoxIdDocumento.DisplayName = "IdDocumento";
            this.ToolBarTextBoxIdDocumento.MinSize = new System.Drawing.Size(240, 22);
            this.ToolBarTextBoxIdDocumento.Name = "ToolBarTextBoxIdDocumento";
            this.ToolBarTextBoxIdDocumento.Text = "";
            ((Telerik.WinControls.UI.RadTextBoxElement)(this.ToolBarTextBoxIdDocumento.GetChildAt(0))).Text = "";
            // 
            // ToolBarClose
            // 
            this.ToolBarClose.DisplayName = "Cerrar";
            this.ToolBarClose.DrawText = true;
            this.ToolBarClose.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_cerrar;
            this.ToolBarClose.Name = "ToolBarClose";
            this.ToolBarClose.Text = "Cerrar";
            this.ToolBarClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarClose.Click += new System.EventHandler(this.ToolBarClose_Click);
            // 
            // RadLabel21
            // 
            this.RadLabel21.Location = new System.Drawing.Point(931, 62);
            this.RadLabel21.Name = "RadLabel21";
            this.RadLabel21.Size = new System.Drawing.Size(94, 18);
            this.RadLabel21.TabIndex = 165;
            this.RadLabel21.Text = "Lugar Expedición:";
            // 
            // TxbLugarDeExpedicion
            // 
            this.TxbLugarDeExpedicion.Location = new System.Drawing.Point(1040, 61);
            this.TxbLugarDeExpedicion.Name = "TxbLugarDeExpedicion";
            this.TxbLugarDeExpedicion.NullText = "Codigo";
            this.TxbLugarDeExpedicion.Size = new System.Drawing.Size(93, 20);
            this.TxbLugarDeExpedicion.TabIndex = 164;
            // 
            // RadLabel20
            // 
            this.RadLabel20.Location = new System.Drawing.Point(931, 87);
            this.RadLabel20.Name = "RadLabel20";
            this.RadLabel20.Size = new System.Drawing.Size(88, 18);
            this.RadLabel20.TabIndex = 163;
            this.RadLabel20.Text = "Tipo de Cambio:";
            // 
            // RadLabel19
            // 
            this.RadLabel19.Location = new System.Drawing.Point(648, 62);
            this.RadLabel19.Name = "RadLabel19";
            this.RadLabel19.Size = new System.Drawing.Size(50, 18);
            this.RadLabel19.TabIndex = 162;
            this.RadLabel19.Text = "Moneda:";
            // 
            // RadLabel18
            // 
            this.RadLabel18.Location = new System.Drawing.Point(450, 62);
            this.RadLabel18.Name = "RadLabel18";
            this.RadLabel18.Size = new System.Drawing.Size(44, 18);
            this.RadLabel18.TabIndex = 161;
            this.RadLabel18.Text = "Cuenta:";
            // 
            // RadLabel17
            // 
            this.RadLabel17.Location = new System.Drawing.Point(648, 87);
            this.RadLabel17.Name = "RadLabel17";
            this.RadLabel17.Size = new System.Drawing.Size(70, 18);
            this.RadLabel17.TabIndex = 160;
            this.RadLabel17.Text = "Condiciones:";
            // 
            // RadLabel16
            // 
            this.RadLabel16.Location = new System.Drawing.Point(332, 89);
            this.RadLabel16.Name = "RadLabel16";
            this.RadLabel16.Size = new System.Drawing.Size(84, 18);
            this.RadLabel16.TabIndex = 159;
            this.RadLabel16.Text = "Forma de Pago:";
            // 
            // RadLabel15
            // 
            this.RadLabel15.Location = new System.Drawing.Point(11, 90);
            this.RadLabel15.Name = "RadLabel15";
            this.RadLabel15.Size = new System.Drawing.Size(93, 18);
            this.RadLabel15.TabIndex = 158;
            this.RadLabel15.Text = "Método de Pago:";
            // 
            // RadLabel14
            // 
            this.RadLabel14.Location = new System.Drawing.Point(242, 62);
            this.RadLabel14.Name = "RadLabel14";
            this.RadLabel14.Size = new System.Drawing.Size(85, 18);
            this.RadLabel14.TabIndex = 157;
            this.RadLabel14.Text = "Núm. Reg. Trib.:";
            // 
            // RadLabel13
            // 
            this.RadLabel13.Location = new System.Drawing.Point(12, 62);
            this.RadLabel13.Name = "RadLabel13";
            this.RadLabel13.Size = new System.Drawing.Size(92, 18);
            this.RadLabel13.TabIndex = 156;
            this.RadLabel13.Text = "Residencia Fiscal:";
            // 
            // RadLabel12
            // 
            this.RadLabel12.Location = new System.Drawing.Point(648, 37);
            this.RadLabel12.Name = "RadLabel12";
            this.RadLabel12.Size = new System.Drawing.Size(70, 18);
            this.RadLabel12.TabIndex = 155;
            this.RadLabel12.Text = "Uso de CFDI:";
            // 
            // RadLabel11
            // 
            this.RadLabel11.Location = new System.Drawing.Point(450, 37);
            this.RadLabel11.Name = "RadLabel11";
            this.RadLabel11.Size = new System.Drawing.Size(28, 18);
            this.RadLabel11.TabIndex = 154;
            this.RadLabel11.Text = "RFC:";
            // 
            // RadLabel10
            // 
            this.RadLabel10.Location = new System.Drawing.Point(12, 37);
            this.RadLabel10.Name = "RadLabel10";
            this.RadLabel10.Size = new System.Drawing.Size(54, 18);
            this.RadLabel10.TabIndex = 153;
            this.RadLabel10.Text = "Receptor:";
            // 
            // RadLabel9
            // 
            this.RadLabel9.Location = new System.Drawing.Point(12, 12);
            this.RadLabel9.Name = "RadLabel9";
            this.RadLabel9.Size = new System.Drawing.Size(67, 18);
            this.RadLabel9.TabIndex = 152;
            this.RadLabel9.Text = "Documento:";
            // 
            // RadLabel8
            // 
            this.RadLabel8.Location = new System.Drawing.Point(275, 12);
            this.RadLabel8.Name = "RadLabel8";
            this.RadLabel8.Size = new System.Drawing.Size(33, 18);
            this.RadLabel8.TabIndex = 151;
            this.RadLabel8.Text = "Serie:";
            // 
            // RadLabel7
            // 
            this.RadLabel7.Location = new System.Drawing.Point(450, 12);
            this.RadLabel7.Name = "RadLabel7";
            this.RadLabel7.Size = new System.Drawing.Size(33, 18);
            this.RadLabel7.TabIndex = 150;
            this.RadLabel7.Text = "Folio:";
            // 
            // CboReceptor
            // 
            this.CboReceptor.AutoSizeDropDownHeight = true;
            // 
            // CboReceptor.NestedRadGridView
            // 
            this.CboReceptor.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboReceptor.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboReceptor.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboReceptor.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboReceptor.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboReceptor.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboReceptor.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn32.FieldName = "Id";
            gridViewTextBoxColumn32.HeaderText = "Id";
            gridViewTextBoxColumn32.IsVisible = false;
            gridViewTextBoxColumn32.Name = "Id";
            gridViewTextBoxColumn33.FieldName = "Nombre";
            gridViewTextBoxColumn33.HeaderText = "Nombre";
            gridViewTextBoxColumn33.Name = "Nombre";
            gridViewTextBoxColumn33.Width = 220;
            gridViewTextBoxColumn34.FieldName = "RFC";
            gridViewTextBoxColumn34.HeaderText = "RFC";
            gridViewTextBoxColumn34.Name = "_drctr_rfc";
            gridViewTextBoxColumn35.FieldName = "ResidenciaFiscal";
            gridViewTextBoxColumn35.HeaderText = "Residencia Fiscal";
            gridViewTextBoxColumn35.IsVisible = false;
            gridViewTextBoxColumn35.Name = "_drctr_resfis";
            gridViewTextBoxColumn36.FieldName = "ClaveUsoCFDI";
            gridViewTextBoxColumn36.HeaderText = "Uso CFDI";
            gridViewTextBoxColumn36.Name = "ClaveUsoCFDI";
            this.CboReceptor.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn32,
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34,
            gridViewTextBoxColumn35,
            gridViewTextBoxColumn36});
            this.CboReceptor.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboReceptor.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboReceptor.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.CboReceptor.EditorControl.Name = "NestedRadGridView";
            this.CboReceptor.EditorControl.ReadOnly = true;
            this.CboReceptor.EditorControl.ShowGroupPanel = false;
            this.CboReceptor.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboReceptor.EditorControl.TabIndex = 0;
            this.CboReceptor.Location = new System.Drawing.Point(84, 36);
            this.CboReceptor.Name = "CboReceptor";
            this.CboReceptor.NullText = "Nombre o Razon Social del Receptor";
            this.CboReceptor.Size = new System.Drawing.Size(355, 20);
            this.CboReceptor.TabIndex = 148;
            this.CboReceptor.TabStop = false;
            // 
            // TxbReceptorRFC
            // 
            this.TxbReceptorRFC.Location = new System.Drawing.Point(498, 36);
            this.TxbReceptorRFC.Name = "TxbReceptorRFC";
            this.TxbReceptorRFC.NullText = "Registro Federal";
            this.TxbReceptorRFC.ReadOnly = true;
            this.TxbReceptorRFC.Size = new System.Drawing.Size(135, 20);
            this.TxbReceptorRFC.TabIndex = 149;
            this.TxbReceptorRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CboUsoCfdi
            // 
            // 
            // CboUsoCfdi.NestedRadGridView
            // 
            this.CboUsoCfdi.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboUsoCfdi.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboUsoCfdi.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboUsoCfdi.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboUsoCfdi.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboUsoCfdi.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboUsoCfdi.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn37.FieldName = "Clave";
            gridViewTextBoxColumn37.HeaderText = "Clave";
            gridViewTextBoxColumn37.Name = "Clave";
            gridViewTextBoxColumn38.FieldName = "Descripcion";
            gridViewTextBoxColumn38.HeaderText = "Descripción";
            gridViewTextBoxColumn38.Name = "Descripcion";
            this.CboUsoCfdi.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn37,
            gridViewTextBoxColumn38});
            this.CboUsoCfdi.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboUsoCfdi.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboUsoCfdi.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition7;
            this.CboUsoCfdi.EditorControl.Name = "NestedRadGridView";
            this.CboUsoCfdi.EditorControl.ReadOnly = true;
            this.CboUsoCfdi.EditorControl.ShowGroupPanel = false;
            this.CboUsoCfdi.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboUsoCfdi.EditorControl.TabIndex = 0;
            this.CboUsoCfdi.Location = new System.Drawing.Point(724, 36);
            this.CboUsoCfdi.Name = "CboUsoCfdi";
            this.CboUsoCfdi.NullText = "Uso de CFDI";
            this.CboUsoCfdi.Size = new System.Drawing.Size(187, 20);
            this.CboUsoCfdi.TabIndex = 145;
            this.CboUsoCfdi.TabStop = false;
            // 
            // TxbNumRegIdTrib
            // 
            this.TxbNumRegIdTrib.Location = new System.Drawing.Point(332, 61);
            this.TxbNumRegIdTrib.Name = "TxbNumRegIdTrib";
            this.TxbNumRegIdTrib.NullText = "Núm. Registro Trib.";
            this.TxbNumRegIdTrib.Size = new System.Drawing.Size(107, 20);
            this.TxbNumRegIdTrib.TabIndex = 146;
            // 
            // CboResidenciaFiscal
            // 
            // 
            // CboResidenciaFiscal.NestedRadGridView
            // 
            this.CboResidenciaFiscal.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboResidenciaFiscal.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboResidenciaFiscal.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboResidenciaFiscal.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition8;
            this.CboResidenciaFiscal.EditorControl.Name = "NestedRadGridView";
            this.CboResidenciaFiscal.EditorControl.ReadOnly = true;
            this.CboResidenciaFiscal.EditorControl.ShowGroupPanel = false;
            this.CboResidenciaFiscal.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboResidenciaFiscal.EditorControl.TabIndex = 0;
            this.CboResidenciaFiscal.Location = new System.Drawing.Point(110, 61);
            this.CboResidenciaFiscal.Name = "CboResidenciaFiscal";
            this.CboResidenciaFiscal.NullText = "Residencia Fiscal";
            this.CboResidenciaFiscal.Size = new System.Drawing.Size(125, 20);
            this.CboResidenciaFiscal.TabIndex = 147;
            this.CboResidenciaFiscal.TabStop = false;
            // 
            // CboCondiciones
            // 
            // 
            // CboCondiciones.NestedRadGridView
            // 
            this.CboCondiciones.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboCondiciones.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCondiciones.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboCondiciones.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboCondiciones.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboCondiciones.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboCondiciones.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.CboCondiciones.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboCondiciones.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboCondiciones.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition9;
            this.CboCondiciones.EditorControl.Name = "NestedRadGridView";
            this.CboCondiciones.EditorControl.ReadOnly = true;
            this.CboCondiciones.EditorControl.ShowGroupPanel = false;
            this.CboCondiciones.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboCondiciones.EditorControl.TabIndex = 0;
            this.CboCondiciones.Location = new System.Drawing.Point(724, 87);
            this.CboCondiciones.Name = "CboCondiciones";
            this.CboCondiciones.NullText = "Condiciones";
            this.CboCondiciones.Size = new System.Drawing.Size(187, 20);
            this.CboCondiciones.TabIndex = 144;
            this.CboCondiciones.TabStop = false;
            // 
            // CboFormaPago
            // 
            // 
            // CboFormaPago.NestedRadGridView
            // 
            this.CboFormaPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboFormaPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboFormaPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboFormaPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboFormaPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboFormaPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboFormaPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn39.FieldName = "Clave";
            gridViewTextBoxColumn39.HeaderText = "Clave";
            gridViewTextBoxColumn39.Name = "Clave";
            gridViewTextBoxColumn40.FieldName = "Descripcion";
            gridViewTextBoxColumn40.HeaderText = "Descripción";
            gridViewTextBoxColumn40.Name = "Descripcion";
            this.CboFormaPago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn39,
            gridViewTextBoxColumn40});
            this.CboFormaPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboFormaPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboFormaPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition10;
            this.CboFormaPago.EditorControl.Name = "NestedRadGridView";
            this.CboFormaPago.EditorControl.ReadOnly = true;
            this.CboFormaPago.EditorControl.ShowGroupPanel = false;
            this.CboFormaPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboFormaPago.EditorControl.TabIndex = 0;
            this.CboFormaPago.Location = new System.Drawing.Point(422, 88);
            this.CboFormaPago.Name = "CboFormaPago";
            this.CboFormaPago.NullText = "Forma de pago";
            this.CboFormaPago.Size = new System.Drawing.Size(211, 20);
            this.CboFormaPago.TabIndex = 143;
            this.CboFormaPago.TabStop = false;
            // 
            // TxbCuentaPago
            // 
            this.TxbCuentaPago.Enabled = false;
            this.TxbCuentaPago.Location = new System.Drawing.Point(498, 61);
            this.TxbCuentaPago.Name = "TxbCuentaPago";
            this.TxbCuentaPago.NullText = "Cuenta";
            this.TxbCuentaPago.Size = new System.Drawing.Size(135, 20);
            this.TxbCuentaPago.TabIndex = 142;
            // 
            // CboMetodoPago
            // 
            // 
            // CboMetodoPago.NestedRadGridView
            // 
            this.CboMetodoPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboMetodoPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboMetodoPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboMetodoPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboMetodoPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboMetodoPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboMetodoPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn41.FieldName = "Clave";
            gridViewTextBoxColumn41.HeaderText = "Clave";
            gridViewTextBoxColumn41.Name = "Clave";
            gridViewTextBoxColumn42.FieldName = "Descripcion";
            gridViewTextBoxColumn42.HeaderText = "Descripción";
            gridViewTextBoxColumn42.Name = "Descripcion";
            this.CboMetodoPago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn41,
            gridViewTextBoxColumn42});
            this.CboMetodoPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboMetodoPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboMetodoPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition11;
            this.CboMetodoPago.EditorControl.Name = "NestedRadGridView";
            this.CboMetodoPago.EditorControl.ReadOnly = true;
            this.CboMetodoPago.EditorControl.ShowGroupPanel = false;
            this.CboMetodoPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboMetodoPago.EditorControl.TabIndex = 0;
            this.CboMetodoPago.Location = new System.Drawing.Point(110, 88);
            this.CboMetodoPago.Name = "CboMetodoPago";
            this.CboMetodoPago.NullText = "Método de Pago";
            this.CboMetodoPago.Size = new System.Drawing.Size(217, 20);
            this.CboMetodoPago.TabIndex = 141;
            this.CboMetodoPago.TabStop = false;
            // 
            // TxbTipoCambio
            // 
            this.TxbTipoCambio.Location = new System.Drawing.Point(1040, 87);
            this.TxbTipoCambio.Name = "TxbTipoCambio";
            this.TxbTipoCambio.NullText = "Tipo de Cambio";
            this.TxbTipoCambio.Size = new System.Drawing.Size(93, 20);
            this.TxbTipoCambio.TabIndex = 140;
            // 
            // CboMoneda
            // 
            // 
            // CboMoneda.NestedRadGridView
            // 
            this.CboMoneda.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboMoneda.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboMoneda.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboMoneda.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboMoneda.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboMoneda.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboMoneda.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn43.FieldName = "Clave";
            gridViewTextBoxColumn43.HeaderText = "Clave";
            gridViewTextBoxColumn43.Name = "Clave";
            gridViewTextBoxColumn44.FieldName = "Descripcion";
            gridViewTextBoxColumn44.HeaderText = "Descripción";
            gridViewTextBoxColumn44.Name = "Descripcion";
            this.CboMoneda.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn43,
            gridViewTextBoxColumn44});
            this.CboMoneda.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboMoneda.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboMoneda.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition12;
            this.CboMoneda.EditorControl.Name = "NestedRadGridView";
            this.CboMoneda.EditorControl.ReadOnly = true;
            this.CboMoneda.EditorControl.ShowGroupPanel = false;
            this.CboMoneda.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboMoneda.EditorControl.TabIndex = 0;
            this.CboMoneda.Location = new System.Drawing.Point(724, 61);
            this.CboMoneda.Name = "CboMoneda";
            this.CboMoneda.NullText = "Moneda";
            this.CboMoneda.Size = new System.Drawing.Size(187, 20);
            this.CboMoneda.TabIndex = 139;
            this.CboMoneda.TabStop = false;
            // 
            // CboDocumento
            // 
            // 
            // CboDocumento.NestedRadGridView
            // 
            this.CboDocumento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboDocumento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboDocumento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboDocumento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboDocumento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboDocumento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboDocumento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn45.FieldName = "TipoDeComprobante";
            gridViewTextBoxColumn45.HeaderText = "Comprobante";
            gridViewTextBoxColumn45.Name = "TipoDeComprobante";
            gridViewTextBoxColumn46.FieldName = "Nombre";
            gridViewTextBoxColumn46.HeaderText = "Nombre";
            gridViewTextBoxColumn46.Name = "Nombre";
            gridViewTextBoxColumn47.FieldName = "Serie";
            gridViewTextBoxColumn47.HeaderText = "Serie";
            gridViewTextBoxColumn47.Name = "Serie";
            gridViewTextBoxColumn48.FieldName = "Folio";
            gridViewTextBoxColumn48.HeaderText = "Folio";
            gridViewTextBoxColumn48.Name = "Folio";
            gridViewTextBoxColumn49.FieldName = "Template";
            gridViewTextBoxColumn49.HeaderText = "Template";
            gridViewTextBoxColumn49.IsVisible = false;
            gridViewTextBoxColumn49.Name = "Template";
            this.CboDocumento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn45,
            gridViewTextBoxColumn46,
            gridViewTextBoxColumn47,
            gridViewTextBoxColumn48,
            gridViewTextBoxColumn49});
            this.CboDocumento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboDocumento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboDocumento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition13;
            this.CboDocumento.EditorControl.Name = "NestedRadGridView";
            this.CboDocumento.EditorControl.ReadOnly = true;
            this.CboDocumento.EditorControl.ShowGroupPanel = false;
            this.CboDocumento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboDocumento.EditorControl.TabIndex = 0;
            this.CboDocumento.Location = new System.Drawing.Point(84, 11);
            this.CboDocumento.Name = "CboDocumento";
            this.CboDocumento.NullText = "Documento";
            this.CboDocumento.Size = new System.Drawing.Size(164, 20);
            this.CboDocumento.TabIndex = 138;
            this.CboDocumento.TabStop = false;
            // 
            // CboSerie
            // 
            this.CboSerie.Location = new System.Drawing.Point(314, 11);
            this.CboSerie.Name = "CboSerie";
            this.CboSerie.NullText = "Serie";
            this.CboSerie.Size = new System.Drawing.Size(125, 20);
            this.CboSerie.TabIndex = 137;
            // 
            // TxbFolio
            // 
            this.TxbFolio.Location = new System.Drawing.Point(498, 11);
            this.TxbFolio.Name = "TxbFolio";
            this.TxbFolio.NullText = "Folio";
            this.TxbFolio.Size = new System.Drawing.Size(135, 20);
            this.TxbFolio.TabIndex = 136;
            this.TxbFolio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxbFolio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbFolio_KeyPress);
            // 
            // RadLabel3
            // 
            this.RadLabel3.Location = new System.Drawing.Point(931, 35);
            this.RadLabel3.Name = "RadLabel3";
            this.RadLabel3.Size = new System.Drawing.Size(103, 18);
            this.RadLabel3.TabIndex = 135;
            this.RadLabel3.Text = "Tipo Comprobante:";
            this.RadLabel3.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // RadLabel1
            // 
            this.RadLabel1.Location = new System.Drawing.Point(648, 12);
            this.RadLabel1.Name = "RadLabel1";
            this.RadLabel1.Size = new System.Drawing.Size(85, 18);
            this.RadLabel1.TabIndex = 134;
            this.RadLabel1.Text = "Fec. de Emisión:";
            // 
            // FechaEmisionField
            // 
            this.FechaEmisionField.CustomFormat = "dd/MMMM/yyyy";
            this.FechaEmisionField.Location = new System.Drawing.Point(739, 11);
            this.FechaEmisionField.Name = "FechaEmisionField";
            this.FechaEmisionField.Size = new System.Drawing.Size(172, 20);
            this.FechaEmisionField.TabIndex = 133;
            this.FechaEmisionField.TabStop = false;
            this.FechaEmisionField.Text = "miércoles, 27 de septiembre de 2017";
            this.FechaEmisionField.Value = new System.DateTime(2017, 9, 27, 19, 9, 13, 971);
            // 
            // PanelGeneral
            // 
            this.PanelGeneral.Controls.Add(this.CboConceptos);
            this.PanelGeneral.Controls.Add(this.ChkCfdiRelacionadoIncluir);
            this.PanelGeneral.Controls.Add(this.Presicion);
            this.PanelGeneral.Controls.Add(this.RadLabel9);
            this.PanelGeneral.Controls.Add(this.FechaEmisionField);
            this.PanelGeneral.Controls.Add(this.FechaCertifica);
            this.PanelGeneral.Controls.Add(this.RadLabel1);
            this.PanelGeneral.Controls.Add(this.RadLabel2);
            this.PanelGeneral.Controls.Add(this.RadLabel3);
            this.PanelGeneral.Controls.Add(this.TipoComprobante);
            this.PanelGeneral.Controls.Add(this.TxbFolio);
            this.PanelGeneral.Controls.Add(this.CboSerie);
            this.PanelGeneral.Controls.Add(this.CboDocumento);
            this.PanelGeneral.Controls.Add(this.CboMoneda);
            this.PanelGeneral.Controls.Add(this.TxbTipoCambio);
            this.PanelGeneral.Controls.Add(this.CboMetodoPago);
            this.PanelGeneral.Controls.Add(this.TxbCuentaPago);
            this.PanelGeneral.Controls.Add(this.CboFormaPago);
            this.PanelGeneral.Controls.Add(this.CboCondiciones);
            this.PanelGeneral.Controls.Add(this.CboResidenciaFiscal);
            this.PanelGeneral.Controls.Add(this.TxbNumRegIdTrib);
            this.PanelGeneral.Controls.Add(this.CboUsoCfdi);
            this.PanelGeneral.Controls.Add(this.TxbReceptorRFC);
            this.PanelGeneral.Controls.Add(this.CboReceptor);
            this.PanelGeneral.Controls.Add(this.RadLabel7);
            this.PanelGeneral.Controls.Add(this.RadLabel8);
            this.PanelGeneral.Controls.Add(this.RadLabel10);
            this.PanelGeneral.Controls.Add(this.RadLabel21);
            this.PanelGeneral.Controls.Add(this.RadLabel11);
            this.PanelGeneral.Controls.Add(this.TxbLugarDeExpedicion);
            this.PanelGeneral.Controls.Add(this.RadLabel12);
            this.PanelGeneral.Controls.Add(this.RadLabel20);
            this.PanelGeneral.Controls.Add(this.RadLabel13);
            this.PanelGeneral.Controls.Add(this.RadLabel19);
            this.PanelGeneral.Controls.Add(this.RadLabel14);
            this.PanelGeneral.Controls.Add(this.RadLabel18);
            this.PanelGeneral.Controls.Add(this.RadLabel15);
            this.PanelGeneral.Controls.Add(this.RadLabel17);
            this.PanelGeneral.Controls.Add(this.RadLabel16);
            this.PanelGeneral.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelGeneral.Location = new System.Drawing.Point(0, 30);
            this.PanelGeneral.Name = "PanelGeneral";
            this.PanelGeneral.Size = new System.Drawing.Size(1269, 120);
            this.PanelGeneral.TabIndex = 186;
            // 
            // CboConceptos
            // 
            // 
            // CboConceptos.NestedRadGridView
            // 
            this.CboConceptos.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboConceptos.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboConceptos.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboConceptos.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboConceptos.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboConceptos.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboConceptos.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn50.FieldName = "ClaveUnidad";
            gridViewTextBoxColumn50.HeaderText = "Clv. Unidad";
            gridViewTextBoxColumn50.Name = "ClaveUnidad";
            gridViewTextBoxColumn51.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn51.HeaderText = "Clv. Producto";
            gridViewTextBoxColumn51.Name = "ClaveProdServ";
            gridViewTextBoxColumn51.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn51.Width = 85;
            gridViewTextBoxColumn52.FieldName = "Descripcion";
            gridViewTextBoxColumn52.HeaderText = "Descripción";
            gridViewTextBoxColumn52.Name = "Descripcion";
            gridViewTextBoxColumn52.Width = 220;
            this.CboConceptos.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn50,
            gridViewTextBoxColumn51,
            gridViewTextBoxColumn52});
            this.CboConceptos.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboConceptos.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboConceptos.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition14;
            this.CboConceptos.EditorControl.Name = "NestedRadGridView";
            this.CboConceptos.EditorControl.ReadOnly = true;
            this.CboConceptos.EditorControl.ShowGroupPanel = false;
            this.CboConceptos.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboConceptos.EditorControl.TabIndex = 0;
            this.CboConceptos.Location = new System.Drawing.Point(1157, 12);
            this.CboConceptos.Name = "CboConceptos";
            this.CboConceptos.NullText = "Selecciona";
            this.CboConceptos.Size = new System.Drawing.Size(100, 20);
            this.CboConceptos.TabIndex = 188;
            this.CboConceptos.TabStop = false;
            // 
            // ChkCfdiRelacionadoIncluir
            // 
            this.ChkCfdiRelacionadoIncluir.Location = new System.Drawing.Point(1194, 55);
            this.ChkCfdiRelacionadoIncluir.Name = "ChkCfdiRelacionadoIncluir";
            this.ChkCfdiRelacionadoIncluir.Size = new System.Drawing.Size(51, 18);
            this.ChkCfdiRelacionadoIncluir.TabIndex = 187;
            this.ChkCfdiRelacionadoIncluir.Text = "Incluir";
            this.ChkCfdiRelacionadoIncluir.CheckStateChanged += new System.EventHandler(this.ChkCfdiRelacionadoIncluir_CheckStateChanged);
            // 
            // Presicion
            // 
            this.Presicion.Location = new System.Drawing.Point(1195, 88);
            this.Presicion.Name = "Presicion";
            this.Presicion.Size = new System.Drawing.Size(48, 20);
            this.Presicion.TabIndex = 186;
            this.Presicion.TabStop = false;
            this.Presicion.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Presicion.Visible = false;
            // 
            // PanelDocumento
            // 
            this.PanelDocumento.Controls.Add(this.radSplitContainer1);
            this.PanelDocumento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelDocumento.Location = new System.Drawing.Point(0, 150);
            this.PanelDocumento.Name = "PanelDocumento";
            this.PanelDocumento.Size = new System.Drawing.Size(1269, 455);
            this.PanelDocumento.TabIndex = 187;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1269, 455);
            this.radSplitContainer1.TabIndex = 0;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.ComprobanteConcepto);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1269, 234);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.01884701F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -22);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.ComprobanteConceptosPartes);
            this.splitPanel2.Controls.Add(this.PanelTotales);
            this.splitPanel2.Location = new System.Drawing.Point(0, 238);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1269, 217);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.01884701F);
            this.splitPanel2.SizeInfo.MinimumSize = new System.Drawing.Size(0, 217);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 22);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // PanelTotales
            // 
            this.PanelTotales.Controls.Add(this.TxbTotal);
            this.PanelTotales.Controls.Add(this.TxbRetencionISR);
            this.PanelTotales.Controls.Add(this.TxbDescuento);
            this.PanelTotales.Controls.Add(this.TxbRetencionIva);
            this.PanelTotales.Controls.Add(this.TxbSubTotal);
            this.PanelTotales.Controls.Add(this.TxbTrasladoIEPS);
            this.PanelTotales.Controls.Add(this.RadLabel28);
            this.PanelTotales.Controls.Add(this.TxbTrasladoIva);
            this.PanelTotales.Controls.Add(this.RadLabel31);
            this.PanelTotales.Controls.Add(this.RadLabel24);
            this.PanelTotales.Controls.Add(this.RadLabel27);
            this.PanelTotales.Controls.Add(this.RadLabel30);
            this.PanelTotales.Controls.Add(this.RadLabel26);
            this.PanelTotales.Controls.Add(this.RadLabel29);
            this.PanelTotales.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelTotales.Location = new System.Drawing.Point(1017, 0);
            this.PanelTotales.Name = "PanelTotales";
            this.PanelTotales.Size = new System.Drawing.Size(252, 217);
            this.PanelTotales.TabIndex = 168;
            // 
            // TxbDescuento
            // 
            this.TxbDescuento.Location = new System.Drawing.Point(98, 42);
            this.TxbDescuento.Mask = "n4";
            this.TxbDescuento.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TxbDescuento.Name = "TxbDescuento";
            this.TxbDescuento.NullText = "SubTotal";
            this.TxbDescuento.Size = new System.Drawing.Size(125, 20);
            this.TxbDescuento.TabIndex = 2;
            this.TxbDescuento.TabStop = false;
            this.TxbDescuento.Text = "0.0000";
            this.TxbDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxbSubTotal
            // 
            this.TxbSubTotal.Location = new System.Drawing.Point(98, 16);
            this.TxbSubTotal.Mask = "n4";
            this.TxbSubTotal.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TxbSubTotal.Name = "TxbSubTotal";
            this.TxbSubTotal.NullText = "SubTotal";
            this.TxbSubTotal.Size = new System.Drawing.Size(125, 20);
            this.TxbSubTotal.TabIndex = 1;
            this.TxbSubTotal.TabStop = false;
            this.TxbSubTotal.Text = "0.0000";
            this.TxbSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxbTrasladoIva
            // 
            this.TxbTrasladoIva.Location = new System.Drawing.Point(98, 68);
            this.TxbTrasladoIva.Mask = "n4";
            this.TxbTrasladoIva.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TxbTrasladoIva.Name = "TxbTrasladoIva";
            this.TxbTrasladoIva.NullText = "SubTotal";
            this.TxbTrasladoIva.Size = new System.Drawing.Size(125, 20);
            this.TxbTrasladoIva.TabIndex = 3;
            this.TxbTrasladoIva.TabStop = false;
            this.TxbTrasladoIva.Text = "0.0000";
            this.TxbTrasladoIva.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxbTrasladoIEPS
            // 
            this.TxbTrasladoIEPS.Location = new System.Drawing.Point(98, 94);
            this.TxbTrasladoIEPS.Mask = "n4";
            this.TxbTrasladoIEPS.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TxbTrasladoIEPS.Name = "TxbTrasladoIEPS";
            this.TxbTrasladoIEPS.NullText = "SubTotal";
            this.TxbTrasladoIEPS.Size = new System.Drawing.Size(125, 20);
            this.TxbTrasladoIEPS.TabIndex = 4;
            this.TxbTrasladoIEPS.TabStop = false;
            this.TxbTrasladoIEPS.Text = "0.0000";
            this.TxbTrasladoIEPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxbRetencionIva
            // 
            this.TxbRetencionIva.Location = new System.Drawing.Point(98, 120);
            this.TxbRetencionIva.Mask = "n4";
            this.TxbRetencionIva.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TxbRetencionIva.Name = "TxbRetencionIva";
            this.TxbRetencionIva.NullText = "SubTotal";
            this.TxbRetencionIva.Size = new System.Drawing.Size(125, 20);
            this.TxbRetencionIva.TabIndex = 5;
            this.TxbRetencionIva.TabStop = false;
            this.TxbRetencionIva.Text = "0.0000";
            this.TxbRetencionIva.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxbRetencionISR
            // 
            this.TxbRetencionISR.Location = new System.Drawing.Point(97, 146);
            this.TxbRetencionISR.Mask = "n4";
            this.TxbRetencionISR.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TxbRetencionISR.Name = "TxbRetencionISR";
            this.TxbRetencionISR.NullText = "SubTotal";
            this.TxbRetencionISR.Size = new System.Drawing.Size(125, 20);
            this.TxbRetencionISR.TabIndex = 6;
            this.TxbRetencionISR.TabStop = false;
            this.TxbRetencionISR.Text = "0.0000";
            this.TxbRetencionISR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxbTotal
            // 
            this.TxbTotal.Location = new System.Drawing.Point(97, 172);
            this.TxbTotal.Mask = "n4";
            this.TxbTotal.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TxbTotal.Name = "TxbTotal";
            this.TxbTotal.NullText = "SubTotal";
            this.TxbTotal.Size = new System.Drawing.Size(125, 20);
            this.TxbTotal.TabIndex = 7;
            this.TxbTotal.TabStop = false;
            this.TxbTotal.Text = "0.0000";
            this.TxbTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ViewComprobante1Fiscal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1269, 605);
            this.Controls.Add(this.PanelDocumento);
            this.Controls.Add(this.PanelGeneral);
            this.Controls.Add(this.RadCommandBar1);
            this.Name = "ViewComprobante1Fiscal";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Comprobante1Fiscal";
            this.Load += new System.EventHandler(this.Comprobante1Fiscal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridCfdiRelacionados.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridCfdiRelacionados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar2)).EndInit();
            this.PageViewCfdiRelacionado.ResumeLayout(false);
            this.PageViewCfdiRelacionado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar4)).EndInit();
            this.PageViewConcepto.ResumeLayout(false);
            this.PageViewConcepto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComprobanteConcepto)).EndInit();
            this.ComprobanteConcepto.ResumeLayout(false);
            this.PageConceptoAduana.ResumeLayout(false);
            this.PageConceptoAduana.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoInformacionAduanera.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoInformacionAduanera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoImpuestos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoImpuestos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar6)).EndInit();
            this.PageConceptoImpuesto.ResumeLayout(false);
            this.PageConceptoImpuesto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FechaCertifica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoComprobante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComprobanteConceptosPartes)).EndInit();
            this.ComprobanteConceptosPartes.ResumeLayout(false);
            this.PageConceptoParte.ResumeLayout(false);
            this.PageConceptoParte.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbLugarDeExpedicion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbReceptorRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCfdi.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCfdi.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCfdi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumRegIdTrib)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCondiciones.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCondiciones.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCondiciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCuentaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMetodoPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMetodoPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMetodoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMoneda.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMoneda.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMoneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDocumento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDocumento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboSerie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbFolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmisionField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelGeneral)).EndInit();
            this.PanelGeneral.ResumeLayout(false);
            this.PanelGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CboConceptos.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboConceptos.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCfdiRelacionadoIncluir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Presicion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelDocumento)).EndInit();
            this.PanelDocumento.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PanelTotales)).EndInit();
            this.PanelTotales.ResumeLayout(false);
            this.PanelTotales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxbDescuento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSubTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTrasladoIva)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTrasladoIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRetencionIva)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRetencionISR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Telerik.WinControls.UI.CommandBarButton ToolBarCfdiAgregar;
        internal Telerik.WinControls.UI.CommandBarDropDownList ToolBarCfdiTipoRelacion;
        internal Telerik.WinControls.UI.CommandBarLabel CommandBarLabel1;
        internal Telerik.WinControls.UI.CommandBarStripElement CommandCfdiRelacionado;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator1;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarCfdiQuitar;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement2;
        internal Telerik.WinControls.UI.RadGridView GridCfdiRelacionados;
        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar2;
        internal Telerik.WinControls.UI.RadPageViewPage PageViewCfdiRelacionado;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarConceptoQuitar;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarConceptoAgregar;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarConceptoBuscar;
        internal Telerik.WinControls.UI.CommandBarLabel ToolBarLabelProducto;
        internal Telerik.WinControls.UI.CommandBarStripElement CommandConceptos;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement4;
        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar4;
        internal Telerik.WinControls.UI.RadPageViewPage PageViewConcepto;
        internal Telerik.WinControls.UI.RadGridView GridConceptos;
        internal Telerik.WinControls.UI.RadPageView ComprobanteConcepto;
        internal Telerik.WinControls.UI.RadPageViewPage PageConceptoAduana;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarConceptoImpuestoQuitar;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarConceptoImpuestoAgregar;
        internal Telerik.WinControls.UI.CommandBarStripElement CommandConceptoImpuesto;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement7;
        internal Telerik.WinControls.UI.RadGridView GridConceptoImpuestos;
        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar6;
        internal Telerik.WinControls.UI.RadPageViewPage PageConceptoImpuesto;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarConceptoParteQuitar;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarConceptoParteAgregar;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaCertifica;
        internal Telerik.WinControls.UI.RadLabel RadLabel2;
        internal Telerik.WinControls.UI.RadSplitButton TipoComprobante;
        internal Telerik.WinControls.UI.RadLabel RadLabel31;
        internal Telerik.WinControls.UI.RadLabel RadLabel30;
        internal Telerik.WinControls.UI.RadLabel RadLabel29;
        internal Telerik.WinControls.UI.RadLabel RadLabel28;
        internal Telerik.WinControls.UI.RadLabel RadLabel27;
        internal Telerik.WinControls.UI.RadLabel RadLabel26;
        internal Telerik.WinControls.UI.RadLabel RadLabel24;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarConceptoParteBuscar;
        internal Telerik.WinControls.UI.CommandBarStripElement CommandConceptoParte;
        internal Telerik.WinControls.UI.RadPageView ComprobanteConceptosPartes;
        internal Telerik.WinControls.UI.RadPageViewPage PageConceptoParte;
        internal Telerik.WinControls.UI.RadGridView GridConceptoParte;
        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar5;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement6;
        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar1;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement1;
        internal Telerik.WinControls.UI.CommandBarStripElement CommandComprobanteFiscal;
        internal Telerik.WinControls.UI.CommandBarSplitButton ToolBarEmisor;
        internal Telerik.WinControls.UI.CommandBarSeparator Separador;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarNew;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarCopy;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarSave;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarRefresh;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarClose;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarCreate;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarPdf;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarXml;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarEmail;
        internal Telerik.WinControls.UI.CommandBarSeparator Separator2;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarSeries;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator2;
        internal Telerik.WinControls.UI.CommandBarLabel ToolLabelStatus;
        internal Telerik.WinControls.UI.CommandBarLabel ToolLabelUuid;
        internal Telerik.WinControls.UI.RadLabel RadLabel21;
        internal Telerik.WinControls.UI.RadTextBox TxbLugarDeExpedicion;
        internal Telerik.WinControls.UI.RadLabel RadLabel20;
        internal Telerik.WinControls.UI.RadLabel RadLabel19;
        internal Telerik.WinControls.UI.RadLabel RadLabel18;
        internal Telerik.WinControls.UI.RadLabel RadLabel17;
        internal Telerik.WinControls.UI.RadLabel RadLabel16;
        internal Telerik.WinControls.UI.RadLabel RadLabel15;
        internal Telerik.WinControls.UI.RadLabel RadLabel14;
        internal Telerik.WinControls.UI.RadLabel RadLabel13;
        internal Telerik.WinControls.UI.RadLabel RadLabel12;
        internal Telerik.WinControls.UI.RadLabel RadLabel11;
        internal Telerik.WinControls.UI.RadLabel RadLabel10;
        internal Telerik.WinControls.UI.RadLabel RadLabel9;
        internal Telerik.WinControls.UI.RadLabel RadLabel8;
        internal Telerik.WinControls.UI.RadLabel RadLabel7;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboReceptor;
        internal Telerik.WinControls.UI.RadTextBox TxbReceptorRFC;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboUsoCfdi;
        internal Telerik.WinControls.UI.RadTextBox TxbNumRegIdTrib;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboResidenciaFiscal;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboCondiciones;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboFormaPago;
        internal Telerik.WinControls.UI.RadTextBox TxbCuentaPago;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboMetodoPago;
        internal Telerik.WinControls.UI.RadTextBox TxbTipoCambio;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboMoneda;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboDocumento;
        internal Telerik.WinControls.UI.RadDropDownList CboSerie;
        internal Telerik.WinControls.UI.RadTextBox TxbFolio;
        internal Telerik.WinControls.UI.RadLabel RadLabel3;
        internal Telerik.WinControls.UI.RadLabel RadLabel1;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEmisionField;
        private Telerik.WinControls.UI.CommandBarButton ToolBarCancelar;
        private Telerik.WinControls.UI.RadPanel PanelGeneral;
        private Telerik.WinControls.UI.RadPanel PanelTotales;
        private Telerik.WinControls.UI.RadPanel PanelDocumento;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarStatus;
        private Telerik.WinControls.UI.CommandBarButton ToolBarCfdiBuscar;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadGridView GridConceptoInformacionAduanera;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar7;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement8;
        private Telerik.WinControls.UI.CommandBarStripElement CommandConceptoAduana;
        private Telerik.WinControls.UI.CommandBarButton ToolBarConceptoAduanaAgregar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarConceptoAduanaQuitar;
        private Telerik.WinControls.UI.RadSpinEditor Presicion;
        private Telerik.WinControls.UI.CommandBarHostItem ToolBarHostItemIncluir;
        private Telerik.WinControls.UI.RadCheckBox ChkCfdiRelacionadoIncluir;
        private Telerik.WinControls.UI.CommandBarHostItem ToolBarHostConceptos;
        private Telerik.WinControls.UI.RadMultiColumnComboBox CboConceptos;
        private Telerik.WinControls.UI.CommandBarButton ToolBarConceptoButtonNuevo;
        private Telerik.WinControls.UI.CommandBarTextBox ToolBarTextBoxIdDocumento;
        private Telerik.WinControls.UI.RadMaskedEditBox TxbSubTotal;
        private Telerik.WinControls.UI.RadMaskedEditBox TxbDescuento;
        private Telerik.WinControls.UI.RadMaskedEditBox TxbRetencionISR;
        private Telerik.WinControls.UI.RadMaskedEditBox TxbRetencionIva;
        private Telerik.WinControls.UI.RadMaskedEditBox TxbTrasladoIEPS;
        private Telerik.WinControls.UI.RadMaskedEditBox TxbTrasladoIva;
        private Telerik.WinControls.UI.RadMaskedEditBox TxbTotal;
    }
}
