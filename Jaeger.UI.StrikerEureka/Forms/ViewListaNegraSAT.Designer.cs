﻿namespace Jaeger.Views
{
    partial class ViewListaNegraSAT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarButtonFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarButtonCatalogo = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonCatalogoNoLocalizados = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonCatalogoCancelados = new Telerik.WinControls.UI.RadMenuItem();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.ToolBarButtonHerramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonImportar = new Telerik.WinControls.UI.RadMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(1085, 38);
            this.radCommandBar1.TabIndex = 0;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarButtonFiltro,
            this.ToolBarButtonCatalogo,
            this.ToolBarButtonHerramientas});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // ToolBarButtonFiltro
            // 
            this.ToolBarButtonFiltro.DisplayName = "Filtro";
            this.ToolBarButtonFiltro.DrawText = true;
            this.ToolBarButtonFiltro.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_filtrar;
            this.ToolBarButtonFiltro.Name = "ToolBarButtonFiltro";
            this.ToolBarButtonFiltro.Text = "Filtrar";
            this.ToolBarButtonFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonFiltro.Click += new System.EventHandler(this.ToolBarButtonFiltro_Click);
            // 
            // ToolBarButtonCatalogo
            // 
            this.ToolBarButtonCatalogo.DisplayName = "commandBarDropDownButton1";
            this.ToolBarButtonCatalogo.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_cursos;
            this.ToolBarButtonCatalogo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonCatalogoNoLocalizados,
            this.ToolBarButtonCatalogoCancelados});
            this.ToolBarButtonCatalogo.Name = "ToolBarButtonCatalogo";
            this.ToolBarButtonCatalogo.Text = "Importar";
            this.ToolBarButtonCatalogo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonCatalogoNoLocalizados
            // 
            this.ToolBarButtonCatalogoNoLocalizados.Name = "ToolBarButtonCatalogoNoLocalizados";
            this.ToolBarButtonCatalogoNoLocalizados.Text = "No Localizados";
            this.ToolBarButtonCatalogoNoLocalizados.Click += new System.EventHandler(this.ToolBarButtonCatalogoNoLocalizados_Click);
            // 
            // ToolBarButtonCatalogoCancelados
            // 
            this.ToolBarButtonCatalogoCancelados.Name = "ToolBarButtonCatalogoCancelados";
            this.ToolBarButtonCatalogoCancelados.Text = "Cancelados";
            this.ToolBarButtonCatalogoCancelados.Click += new System.EventHandler(this.ToolBarButtonCatalogoCancelados_Click);
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 38);
            // 
            // 
            // 
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1085, 464);
            this.GridData.TabIndex = 1;
            // 
            // ToolBarButtonHerramientas
            // 
            this.ToolBarButtonHerramientas.DisplayName = "commandBarDropDownButton1";
            this.ToolBarButtonHerramientas.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_caja_herramientas;
            this.ToolBarButtonHerramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonImportar});
            this.ToolBarButtonHerramientas.Name = "ToolBarButtonHerramientas";
            this.ToolBarButtonHerramientas.Text = "Herramientas";
            // 
            // ToolBarButtonImportar
            // 
            this.ToolBarButtonImportar.Name = "ToolBarButtonImportar";
            this.ToolBarButtonImportar.Text = "Importar layout";
            this.ToolBarButtonImportar.Click += new System.EventHandler(this.ToolBarButtonImportar_Click);
            // 
            // ViewListaNegraSAT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1085, 502);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "ViewListaNegraSAT";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ViewListaNegraSAT";
            this.Load += new System.EventHandler(this.ViewListaNegraSAT_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltro;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonCatalogo;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonCatalogoNoLocalizados;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonCatalogoCancelados;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonHerramientas;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonImportar;
    }
}
