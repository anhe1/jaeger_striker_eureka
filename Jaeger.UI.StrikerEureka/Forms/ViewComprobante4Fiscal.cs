﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.Enums;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Helpers;
using Jaeger.Edita.V2;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Edita.V2.CFDI.Entities.Cancel;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Enums;
using Jaeger.Aplication;
using Jaeger.Catalogos.Repositories;
using Jaeger.Catalogos.Entities;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;

namespace Jaeger.Views {
    public partial class ViewComprobante4Fiscal : RadForm {
        private BackgroundWorker preparar;
        private ViewModelComprobante cfdiV33;
        private SqlSugarComprobanteFiscal data;
        private ISqlDirectorio receptor;
        private HelperCertificacion certifica;
        private MetodoPagoCatalogo catalogoMetodoPago = new MetodoPagoCatalogo();
        private FormaPagoCatalogo catalogoFormaPago = new FormaPagoCatalogo();
        private FormaPagoCatalogo catalogoFormaPagoP;
        private MonedaCatalogo catalogoMoneda = new MonedaCatalogo();
        private MonedaCatalogo catalogoMonedaP;
        private UsoCFDICatalogo catalogoUsoCfdi = new UsoCFDICatalogo();
        private PaisesCatalogo catalogoPais = new PaisesCatalogo();
        private RelacionCFDICatalogo catalogoRelacionesCfdi = new RelacionCFDICatalogo();
        private CatalogoConceptosRecientes catalogoRecientes = new CatalogoConceptosRecientes();
        private TipoCadenaPagoCatalogo catalogoTipoCadenaPago;
        private BancosCatalogo catalgoBancos;

        public ViewComprobante4Fiscal(ViewModelComprobante objeto) {
            InitializeComponent();
            this.cfdiV33 = objeto;
        }

        public ViewComprobante4Fiscal(int indice) {
            InitializeComponent();
            this.cfdiV33 = new ViewModelComprobante() { Id = indice };
        }

        private void ViewComprobante4Fiscal_Load(object sender, EventArgs e) {
            // tareas adicionales
            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += PrepararDoWork;
            this.preparar.RunWorkerCompleted += PrepararRunWorkerCompleted;
            this.preparar.RunWorkerAsync();
            this.ToolBarHostItemIncluir.HostedItem = this.ChkCfdiRelacionadoIncluir.ButtonElement;
            this.ToolBarDoctosHostComprobantes.HostedItem = this.CboPagoDoctosRelacionados.MultiColumnComboBoxElement;
            this.ToolBarDoctosHostAutoSuma.HostedItem = this.ChkAutoSuma.ButtonElement;
            this.CboPagoDoctosRelacionados.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.CboPagoDoctosRelacionados.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.CommandComprobanteFiscal.OverflowButton.AddRemoveButtonsMenuItem.Visibility = ElementVisibility.Collapsed;
            this.CommandComprobanteFiscal.OverflowButton.CustomizeButtonMenuItem.Visibility = ElementVisibility.Collapsed;
        }

        #region procedimientos de controles

        /// <summary>
        /// para asegurarnos que digiten solo numeros
        /// </summary>
        private void TxbFolio_KeyPress(object sender, KeyPressEventArgs e) {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else {
                char keyChar = e.KeyChar;
                e.Handled = !(keyChar.ToString() == char.ConvertFromUtf32(8));
            }
        }

        private void TipoComprobante_Click(object sender, EventArgs e) {
            if (sender as RadMenuItem is RadMenuItem) {
                RadMenuItem button = (RadMenuItem)sender;
                this.TipoComprobante.DefaultItem = (RadItem)sender;
                this.TipoComprobante.Text = button.Text;
                this.TipoComprobante.Tag = button.Tag;
                this.cfdiV33.Default();
                this.ToolBarRefresh.PerformClick();
            }
        }

        private void CboUsoCfdi_SelectedValueChanged(object sender, EventArgs e) {
            GridViewRowInfo temporal = CboReceptor.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                var seleccionado = temporal.DataBoundItem as ClaveUsoCFDI;
                if (seleccionado != null) {
                    this.cfdiV33.UsoCfdi = new ClaveUsoCFDI {
                        Clave = seleccionado.Clave,
                        Descripcion = seleccionado.Descripcion
                    };
                }
            }
        }

        private void CboPaymentType_SelectedIndexChanged(object sender, EventArgs e) {
            GridViewRowInfo temporal = CboFormaPago.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                if (temporal.DataBoundItem is ClaveFormaPago este) {
                    this.cfdiV33.FormaPago.Clave = este.Clave;
                    this.cfdiV33.FormaPago.Descripcion = este.Descripcion;
                }
            }
        }

        private void CboPaymentMethod_SelectedIndexChanged(object sender, EventArgs e) {
            GridViewRowInfo temporal = CboMetodoPago.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                if (temporal.DataBoundItem is ClaveMetodoPago seleccionado) {
                    this.cfdiV33.MetodoPago.Clave = seleccionado.Clave;
                    this.cfdiV33.MetodoPago.Descripcion = seleccionado.Descripcion;
                }
            }
        }

        private void CboReceptor_SelectedValueChanged(object sender, EventArgs e) {
            GridViewRowInfo temporal = CboReceptor.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                if (temporal.DataBoundItem is ViewModelContribuyenteDomicilio receptor) {
                    this.cfdiV33.Receptor.Id = receptor.Id;
                    this.cfdiV33.Receptor.RFC = receptor.RFC;
                    this.cfdiV33.Receptor.Nombre = receptor.Nombre;
                    this.cfdiV33.Receptor.ResidenciaFiscal = receptor.RegimenFiscal;
                    this.cfdiV33.Receptor.NumRegIdTrib = receptor.NumRegIdTrib;
                    if (receptor.ClaveUsoCFDI != "")
                        this.CboUsoCfdi.SelectedValue = receptor.ClaveUsoCFDI;
                    else
                        this.CboUsoCfdi.SelectedValue = "P01";
                }
            }
        }

        private void CboReceptor_DropDownOpening(object sender, CancelEventArgs args) {
            if (this.cfdiV33.Editable) {

            }
            else {
                args.Cancel = true;
            }
        }

        private void CboReceptor_DropDownOpened(object sender, EventArgs e) {
            if (this.cfdiV33.Editable) {
                this.CboReceptor.AutoSizeDropDownToBestFit = true;
                this.CboReceptor.DataSource = this.receptor.GetListBy(Edita.V2.Directorio.Enums.EnumRelationType.Cliente);
            }
            else {

            }
        }

        #endregion

        #region barra de herramientas

        private void ToolBarNew_Click(object sender, EventArgs e) {
            this.cfdiV33 = null;
            this.ToolBarRefresh.PerformClick();
        }

        private void ToolBarCopy_Click(object sender, EventArgs e) {
            this.cfdiV33.Clonar();
            this.cfdiV33.Status = "Pendiente";
            this.ToolBarRefresh.PerformClick();
        }

        private void ToolBarSave_Click(object sender, EventArgs e) {
            using (Waiting2Form espera = new Waiting2Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
        }

        private void ToolBarClose_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ToolBarCreate_Click(object sender, EventArgs e) {
            if (ConfigService.Synapsis.ProveedorAutorizado.Certificacion.Production == false) {
                if (RadMessageBox.Show(this, "La configuración actual indica que el timbrado es en modo de prueba, ¿Es correcto?", "Atención", MessageBoxButtons.YesNo) == DialogResult.No)
                    return;
            }

            using (var espera = new Waiting2Form(this.Procesar)) {
                espera.Text = "Procesando ...";
                espera.ShowDialog(this);
            }
            if (this.certifica.Codigo > 0) {
                RadMessageBox.Show(this, this.certifica.Mensaje);
            }
            this.ToolBarPdf.Enabled = HelperValidacion.ValidaUrl(this.cfdiV33.FilePdf);
            this.ToolBarXml.Enabled = HelperValidacion.ValidaUrl(this.cfdiV33.FileXml);
            this.ToolBarCreate.Enabled = !(HelperValidacion.UUID((this.cfdiV33.TimbreFiscal != null ? this.cfdiV33.TimbreFiscal.UUID : "")));
            this.CommandComprobanteFiscal.Enabled = true;
            this.ToolBarRefresh.PerformClick();
        }

        private void ToolBarCancelar_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show(this, "¿Esta seguro de cancelar este comprobante fiscal? (Esta acción no se puede revertir.)", "Información", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                if (ConfigService.Synapsis.ProveedorAutorizado.Cancelacion.Production == false)
                    if (RadMessageBox.Show("La configuración actual indica que la cancelación es en modo de prueba, ¿Es correcto?", "Atención", MessageBoxButtons.YesNo) == DialogResult.No)
                        return;
                if (!(this.cfdiV33.TimbreFiscal == null)) {
                    using (var espera = new Waiting2Form(this.Cancelar)) {
                        espera.Text = "Enviado solicitud...";
                        espera.ShowDialog(this);
                    }
                    if (certifica.Codigo > 0)
                        RadMessageBox.Show(this, certifica.Mensaje);
                    else if (this.cfdiV33.Accuse != null) {
                        this.ToolBarCancelar.Enabled = false;
                        this.ToolBarCancelar.Text = "Acuse";
                    }
                }
            }
        }

        private void ToolBarPdf_Click(object sender, EventArgs e) {
            string keyDownload = JaegerManagerPaths.JaegerPath(EnumPaths.Downloads, String.Concat(this.cfdiV33.KeyName().ToString(), ".pdf"));

            if ((!System.IO.File.Exists(keyDownload))) {
                Util.Helpers.HelperFiles.DownloadFile(this.cfdiV33.FilePdf, keyDownload);
            }

            if ((System.IO.File.Exists(keyDownload))) {
                ViewerPdf vista = new ViewerPdf(keyDownload) { MdiParent = this.ParentForm };
                vista.Show();
            }
        }

        private void ToolBarXml_Click(object sender, EventArgs e) {
            string keyDownload = JaegerManagerPaths.JaegerPath(EnumPaths.Downloads, String.Concat(this.cfdiV33.KeyName().ToString(), ".xml"));
            if ((!System.IO.File.Exists(keyDownload)))
                Util.Helpers.HelperFiles.DownloadFile(this.cfdiV33.FileXml, keyDownload);

            if ((System.IO.File.Exists(keyDownload))) {
                var vista = new ViewComprobanteVisor(keyDownload) { MdiParent = this.ParentForm };
                vista.Show();
            }
        }

        private void ToolBarRefresh_Click(object sender, EventArgs e) {
            // suponemos que es nuevo
            if (this.cfdiV33 == null) {
                this.Text = "Nuevo Comprobante";
                this.cfdiV33 = new ViewModelComprobante {
                    Status = "Pendiente",
                    TipoComprobante = EnumCfdiType.Pagos,
                    SubTipo = EnumCfdiSubType.Emitido
                };
                this.cfdiV33.Emisor.RFC = ConfigService.Synapsis.Empresa.RFC;
                this.cfdiV33.Emisor.Nombre = ConfigService.Synapsis.Empresa.RazonSocial;
                this.cfdiV33.Default();
            } // en el caso de que tengamos un id
            else if (this.cfdiV33.Id > 0) {
                using (var espera = new Waiting2Form(this.Actualizar)) {
                    espera.Text = "Cargando ...";
                    espera.ShowDialog(this);
                    this.Text = string.Concat(this.cfdiV33.Serie, " ", this.cfdiV33.Folio);
                }
            }
            this.CreateBinding();

        }

        #endregion

        #region conceptos comprobante

        private void ToolBarConceptoButtonNuevo_Click(object sender, EventArgs e) {
            if (this.cfdiV33.Conceptos == null) {
                this.cfdiV33.Conceptos = new BindingList<ViewModelComprobanteConcepto>();
                this.GridConceptos.DataSource = this.cfdiV33.Conceptos;
            }
            this.GridConceptos.Rows.AddNew();
            this.GridConceptos.Refresh();
        }

        private void ToolBarConceptoAgregar_Click(object sender, EventArgs e) {
            if (this.cfdiV33.Conceptos == null) {
                this.cfdiV33.Conceptos = new BindingList<ViewModelComprobanteConcepto>();
                this.GridConceptos.DataSource = this.cfdiV33.Conceptos;
            }
            if (this.CboConceptos.SelectedItem != null) {
                GridViewRowInfo rowView = this.CboConceptos.SelectedItem as GridViewRowInfo;
                if (rowView.DataBoundItem is ViewModelComprobanteConcepto c) {
                    this.cfdiV33.Conceptos.Add(c);
                }
            }
        }

        private void ToolBarConceptoQuitar_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show("¿Esta seguro de remover el concepto seleccionado?", "Atención", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                if (this.GridConceptos.CurrentRow != null) {
                    if (DbConvert.ConvertInt32((this.GridConceptos.CurrentRow.Cells["Id"].Value)) <= 0) {
                        this.GridConceptos.Rows.Remove(this.GridConceptos.CurrentRow);
                    }
                    else {
                        this.GridConceptos.CurrentRow.Cells["IsActive"].Value = false;
                        this.GridConceptos.CurrentRow.IsVisible = false;
                    }
                    this.GridConceptos.Refresh();
                }
            }
        }

        private void ToolBarConceptoBuscar_Click(object sender, EventArgs e) {
            ViewCatalogoProdServBuscar p = new Views.ViewCatalogoProdServBuscar() { StartPosition = FormStartPosition.CenterParent };
            ViewModelComprobanteConcepto c = p.ShowForm();
            if (c != null) {
                this.cfdiV33.Conceptos.Add(c);
            }
        }

        #endregion

        #region cfdi relaciondo

        private void ToolBarCfdiBuscar_Click(object sender, EventArgs e) {
            var buscar = new ViewComprobanteBuscar(this.cfdiV33.ReceptorRFC);
            ComprobanteCfdiRelacionadosCfdiRelacionado seleccionado = buscar.ShowForm();
            if (seleccionado != null) {
                if (this.cfdiV33.CfdiRelacionados == null)
                    this.cfdiV33.CfdiRelacionados = new ComprobanteCfdiRelacionados();
                this.GridCfdiRelacionados.DataSource = this.cfdiV33.CfdiRelacionados;
                this.GridCfdiRelacionados.DataMember = "CfdiRelacionado";
                this.cfdiV33.CfdiRelacionados.CfdiRelacionado.Add(seleccionado);
            }
        }

        private void ToolBarCfdiAgregar_Click(object sender, EventArgs e) {
            this.cfdiV33.CfdiRelacionados.CfdiRelacionado.AddNew();
        }

        private void ToolBarCfdiQuitar_Click(object sender, EventArgs e) {
            if (!(this.GridCfdiRelacionados.CurrentRow == null)) {
                this.GridCfdiRelacionados.Rows.Remove(this.GridCfdiRelacionados.CurrentRow);
            }
        }

        private void ChkCfdiRelacionadoIncluir_CheckStateChanged(object sender, EventArgs e) {
            this.ToolBarCfdiTipoRelacion.Enabled = this.ChkCfdiRelacionadoIncluir.Checked;
            this.ToolBarCfdiBuscar.Enabled = this.ChkCfdiRelacionadoIncluir.Checked;
            this.ToolBarCfdiAgregar.Enabled = this.ChkCfdiRelacionadoIncluir.Checked;
            this.ToolBarCfdiQuitar.Enabled = this.ChkCfdiRelacionadoIncluir.Checked;
        }

        private void ToolBarCfdiTipoRelacion_SelectedValueChanged(object sender, Telerik.WinControls.UI.Data.ValueChangedEventArgs e) {
            RadListDataItem temporal = this.ToolBarCfdiTipoRelacion.SelectedItem;
            if (!(temporal == null)) {
                if (temporal.DataBoundItem is ClaveTipoRelacionCFDI seleccionado) {
                    if (this.cfdiV33.CfdiRelacionados == null) {
                        this.cfdiV33.CfdiRelacionados = new ComprobanteCfdiRelacionados();
                    }
                    this.cfdiV33.CfdiRelacionados.TipoRelacion.Clave = seleccionado.Clave;
                    this.cfdiV33.CfdiRelacionados.TipoRelacion.Descripcion = seleccionado.Descripcion;
                }
            }
        }

        #endregion

        #region preparar

        private void PrepararDoWork(object sender, DoWorkEventArgs e) {
            this.GridDoctosRelacionados.TelerikGridCommon();

            // preparar conexion con el servidor
            this.data = new SqlSugarComprobanteFiscal(ConfigService.Synapsis.RDS.Edita);
            this.receptor = new MySqlDirectorio(ConfigService.Synapsis.RDS.Edita);

            foreach (string item in Enum.GetNames(typeof(EnumCfdiType))) {
                RadMenuItem boton = new RadMenuItem { Text = item, Name = item };
                this.TipoComprobante.Items.Add(boton);
                boton.Click += new EventHandler(this.TipoComprobante_Click);
            }

            // cargar catalogos
            this.catalogoUsoCfdi.Load();
            this.catalogoMetodoPago.Load();

            this.catalogoFormaPago.Load();
            this.catalogoMoneda.Load();
            this.catalogoRelacionesCfdi.Load();

            // cargar catalogos
            this.catalogoUsoCfdi.Load();
            this.catalogoMetodoPago = new MetodoPagoCatalogo();
            this.catalogoMetodoPago.Load();
            this.catalogoFormaPago = new FormaPagoCatalogo();
            this.catalogoFormaPago.Load();
            this.catalogoFormaPagoP = new FormaPagoCatalogo();
            this.catalogoFormaPagoP.Load();
            this.catalogoMoneda = new MonedaCatalogo();
            this.catalogoMoneda.Load();
            this.catalogoMonedaP = new MonedaCatalogo();
            this.catalogoMoneda.Load();
            this.catalgoBancos = new BancosCatalogo();
            this.catalgoBancos.Load();
            this.catalogoRelacionesCfdi = new RelacionCFDICatalogo();
            this.catalogoRelacionesCfdi.Load();
            this.catalogoTipoCadenaPago = new TipoCadenaPagoCatalogo();
            this.catalogoTipoCadenaPago.Load();

        }

        private void PrepararRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            // catalogo metodos de pago
            this.CboMetodoPago.DataSource = this.catalogoMetodoPago.Items;
            this.CboMetodoPago.DisplayMember = "Descripcion";
            this.CboMetodoPago.ValueMember = "Clave";
            this.CboMetodoPago.SelectedIndex = -1;
            this.CboMetodoPago.SelectedValue = null;
            this.CboMetodoPago.AutoSizeDropDownToBestFit = true;
            this.CboMetodoPago.SelectedIndexChanged += CboPaymentMethod_SelectedIndexChanged;

            // catalogo formas de pago
            this.CboFormaPago.DataSource = this.catalogoFormaPago.Items;
            this.CboFormaPago.DisplayMember = "Descripcion";
            this.CboFormaPago.ValueMember = "Clave";
            this.CboFormaPago.AutoSizeDropDownToBestFit = true;
            this.CboFormaPago.SelectedIndexChanged += CboPaymentType_SelectedIndexChanged;
            this.CboFormaPago.SelectedIndex = -1;
            this.CboFormaPago.SelectedValue = null;

            // catalogo de monedas
            this.CboMoneda.DataSource = this.catalogoMoneda.Items;
            this.CboMoneda.DisplayMember = "Clave";
            this.CboMoneda.ValueMember = "Clave";
            this.CboMoneda.SelectedIndex = -1;
            this.CboMoneda.SelectedValue = null;
            this.CboMoneda.AutoFilter = true;
            this.CboMoneda.AutoSizeDropDownToBestFit = true;
            CompositeFilterDescriptor composite = new CompositeFilterDescriptor { LogicalOperator = FilterLogicalOperator.Or };
            composite.FilterDescriptors.Add(new FilterDescriptor("Clave", FilterOperator.Contains, ""));
            composite.FilterDescriptors.Add(new FilterDescriptor("Descripcion", FilterOperator.Contains, ""));
            this.CboMoneda.EditorControl.FilterDescriptors.Add(composite);

            // catalogo uso de cfdi
            this.CboUsoCfdi.DataSource = catalogoUsoCfdi.Items;
            this.CboUsoCfdi.DisplayMember = "Descripcion";
            this.CboUsoCfdi.ValueMember = "Clave";
            this.CboUsoCfdi.SelectedIndex = -1;
            this.CboUsoCfdi.SelectedValue = null;
            this.CboUsoCfdi.AutoSizeDropDownToBestFit = true;
            CompositeFilterDescriptor composite2 = new CompositeFilterDescriptor { LogicalOperator = FilterLogicalOperator.Or };
            composite2.FilterDescriptors.Add(new FilterDescriptor("Clave", FilterOperator.Contains, ""));
            composite2.FilterDescriptors.Add(new FilterDescriptor("Descripcion", FilterOperator.Contains, ""));
            this.CboUsoCfdi.EditorControl.FilterDescriptors.Add(composite2);
            this.CboUsoCfdi.AutoFilter = true;
            this.CboUsoCfdi.SelectedIndex = -1;
            this.CboUsoCfdi.SelectedValue = null;
            this.CboUsoCfdi.SelectedValueChanged += CboUsoCfdi_SelectedValueChanged;

            // catalogo relaciones cfdi
            this.ToolBarCfdiTipoRelacion.DataSource = this.catalogoRelacionesCfdi.Items;
            this.ToolBarCfdiTipoRelacion.DisplayMember = "Descripcion";
            this.ToolBarCfdiTipoRelacion.ValueMember = "Clave";
            this.ToolBarCfdiTipoRelacion.SelectedIndex = -1;
            this.ToolBarCfdiTipoRelacion.SelectedValue = null;
            this.ToolBarCfdiTipoRelacion.AutoSize = true;
            this.ToolBarCfdiTipoRelacion.SelectedValueChanged += ToolBarCfdiTipoRelacion_SelectedValueChanged;

            // receptor del comprobante
            this.CboReceptor.DisplayMember = "Nombre";
            this.CboReceptor.ValueMember = "Nombre";
            CompositeFilterDescriptor composite3 = new CompositeFilterDescriptor { LogicalOperator = FilterLogicalOperator.Or };
            composite3.FilterDescriptors.Add(new FilterDescriptor("Nombre", FilterOperator.Contains, ""));
            composite3.FilterDescriptors.Add(new FilterDescriptor("RFC", FilterOperator.Contains, ""));
            this.CboReceptor.EditorControl.FilterDescriptors.Add(new FilterDescriptor("Nombre", FilterOperator.StartsWith, ""));
            this.CboReceptor.AutoFilter = true;
            this.CboReceptor.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.CboReceptor.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.CboReceptor.DropDownOpened += CboReceptor_DropDownOpened;
            this.CboReceptor.DropDownOpening += CboReceptor_DropDownOpening;
            this.CboReceptor.SelectedValueChanged += CboReceptor_SelectedValueChanged;

            this.ToolBarHostConceptos.HostedItem = this.CboConceptos.MultiColumnComboBoxElement;
            this.CboConceptos.AutoFilter = true;
            this.CboConceptos.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.CboConceptos.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.CboConceptos.AutoSizeDropDownToBestFit = true;
            this.CboConceptos.DisplayMember = "Descripcion";
            this.CboConceptos.ValueMember = "ClaveProdServ";
            this.CboConceptos.SelectedIndex = -1;
            this.CboConceptos.SelectedValue = null;
            //this.CboConceptos.DataSource = this.catalogoRecientes.Items;
            this.CboConceptos.DropDownOpened += CboConceptos_DropDownOpened;
            this.CboConceptos.DropDownOpening += CboConceptos_DropDownOpening;
            this.CboConceptos.SelectedValueChanged += CboConceptos_SelectedValueChanged;

            // catalogo de bancos
            this.CboNomBancoOrdExt.DataSource = this.catalgoBancos.Items;
            this.CboNomBancoOrdExt.DisplayMember = "Descripcion";
            this.CboNomBancoOrdExt.ValueMember = "Clave";
            this.CboNomBancoOrdExt.AutoFilter = true;
            this.CboNomBancoOrdExt.AutoSizeDropDownToBestFit = true;

            // complemento de pagos 10
            this.CboFormaPagoP.DataSource = this.catalogoFormaPagoP.Items;
            this.CboFormaPagoP.DisplayMember = "Descripcion";
            this.CboFormaPagoP.ValueMember = "Clave";
            this.CboFormaPagoP.AutoSizeDropDownToBestFit = true;
            this.CboFormaPagoP.SelectedValueChanged += CboFormaPagoP_SelectedValueChanged;

            // catalogo de monedas para el complemento de pagos
            this.CboMonedaP.DataSource = this.catalogoMonedaP.Items;
            this.CboMonedaP.DisplayMember = "Clave";
            this.CboMonedaP.ValueMember = "Clave";
            this.CboMonedaP.AutoFilter = true;
            this.CboMonedaP.AutoSizeDropDownToBestFit = true;

            // catalogo de tipo cadena pago
            this.CboTipoCadenaPago.DataSource = this.catalogoTipoCadenaPago.Items;
            this.CboTipoCadenaPago.DisplayMember = "Clave";
            this.CboTipoCadenaPago.ValueMember = "Clave";
            this.CboTipoCadenaPago.AutoFilter = true;
            this.CboTipoCadenaPago.AutoSizeDropDownToBestFit = true;

            // comprobantes relacionados al pago
            this.CboPagoDoctosRelacionados.DisplayMember = "IdDocumento";
            this.CboPagoDoctosRelacionados.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.CboPagoDoctosRelacionados.AutoFilter = true;

            this.FechaPago.SetToNullValue();
            this.FechaPago.NullableValue = null;

            this.ToolBarRefresh.PerformClick();

            if (this.cfdiV33.Version.Contains("3.3")) {
                this.CboUsoCfdi.DataSource = this.catalogoUsoCfdi.Items;
            }

            if (ConfigService.Synapsis.ProveedorAutorizado.Certificacion.Production == false) {
                this.ToolBarEmisor.ToolTipText = "Actualmente la configuración indica modo de prueba";
            }
        }

        #endregion

        private void CboFormaPagoP_SelectedValueChanged(object sender, EventArgs e) {
            GridViewRowInfo temporal = CboFormaPagoP.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                var este = temporal.DataBoundItem as ClaveFormaPago;
                if (este != null) {
                    this.cfdiV33.ComplementoPagos.Pago[0].FormaDePagoP.Clave = este.Clave;
                    this.cfdiV33.ComplementoPagos.Pago[0].FormaDePagoP.Descripcion = este.Descripcion;
                }
            }
        }

        private void CreateBinding() {
            this.ToolBarTextBoxIdDocumento.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.ToolBarTextBoxIdDocumento.TextBoxElement.TextBoxItem.TextAlign = HorizontalAlignment.Center;
            this.FechaCertifica.SetToNullValue();
            this.FechaCertifica.NullableValue = null;

            this.Presicion.DataBindings.Clear();
            this.Presicion.DataBindings.Add("Value", this.cfdiV33, "PresionDecimal", true, DataSourceUpdateMode.OnPropertyChanged);

            if (this.cfdiV33.TimbreFiscal == null) {
                this.cfdiV33.SubTipo = EnumCfdiSubType.Emitido;
                this.cfdiV33.Emisor.RFC = ConfigService.Synapsis.Empresa.RFC;
                this.cfdiV33.Emisor.Nombre = ConfigService.Synapsis.Empresa.RazonSocial;
                this.cfdiV33.Emisor.RegimenFiscal = ConfigService.Synapsis.Empresa.RegimenFiscal;
                this.cfdiV33.LugarExpedicion = ConfigService.Synapsis.Empresa.DomicilioFiscal.CodigoPostal;
                this.ToolBarTextBoxIdDocumento.Text = "";
                this.ToolBarTextBoxIdDocumento.Visibility = ElementVisibility.Collapsed;
                this.ToolLabelUuid.Visibility = ElementVisibility.Collapsed;
            }
            else {
                this.cfdiV33.Emisor.RegimenFiscal = ConfigService.Synapsis.Empresa.RegimenFiscal;
                this.cfdiV33.LugarExpedicion = ConfigService.Synapsis.Empresa.DomicilioFiscal.CodigoPostal;
                this.ToolBarTextBoxIdDocumento.Text = this.cfdiV33.TimbreFiscal.UUID;
                this.ToolBarTextBoxIdDocumento.Visibility = ElementVisibility.Visible;
                this.ToolLabelUuid.Visibility = ElementVisibility.Visible;
            }

            this.CboSerie.DataBindings.Clear();
            this.CboSerie.ReadOnly = !this.cfdiV33.Editable;

            this.ToolBarStatus.DataBindings.Clear();
            this.ToolBarStatus.DataBindings.Add("Text", this.cfdiV33, "StatusText", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ToolBarEmisor.DataBindings.Clear();
            this.ToolBarEmisor.DataBindings.Add("Text", ConfigService.Synapsis.Empresa, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TipoComprobante.DataBindings.Clear();
            this.TipoComprobante.DataBindings.Add("Text", this.cfdiV33, "TipoComprobanteText", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TipoComprobante.DataBindings.Add("Enabled", this.cfdiV33, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbLugarDeExpedicion.DataBindings.Clear();
            this.TxbLugarDeExpedicion.DataBindings.Add("Text", cfdiV33, "LugarExpedicion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbLugarDeExpedicion.ReadOnly = !this.cfdiV33.Editable;

            this.CboReceptor.DataBindings.Clear();
            this.CboReceptor.DataBindings.Add("Text", cfdiV33.Receptor, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboReceptor.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboReceptor.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.TxbReceptorRFC.DataBindings.Clear();
            this.TxbReceptorRFC.DataBindings.Add("Text", cfdiV33.Receptor, "Rfc", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboUsoCfdi.DataBindings.Clear();
            this.CboUsoCfdi.DataBindings.Add("SelectedValue", cfdiV33.UsoCfdi, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboUsoCfdi.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboUsoCfdi.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.CboResidenciaFiscal.DataBindings.Clear();
            this.CboResidenciaFiscal.DataBindings.Add("Text", cfdiV33.Receptor, "ResidenciaFiscal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboResidenciaFiscal.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboResidenciaFiscal.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.TxbNumRegIdTrib.DataBindings.Clear();
            this.TxbNumRegIdTrib.DataBindings.Add("Text", cfdiV33.Receptor, "NumRegIdTrib", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbNumRegIdTrib.ReadOnly = !this.cfdiV33.Editable;

            this.CboMetodoPago.DataBindings.Clear();
            this.CboMetodoPago.DataBindings.Add("SelectedValue", cfdiV33.MetodoPago, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboMetodoPago.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboMetodoPago.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);
            this.CboMetodoPago.Enabled = false;

            this.CboFormaPago.DataBindings.Clear();
            this.CboFormaPago.DataBindings.Add("SelectedValue", cfdiV33.FormaPago, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboFormaPago.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboFormaPago.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);
            this.CboFormaPago.Enabled = false;

            this.CboCondiciones.DataBindings.Clear();
            this.CboCondiciones.DataBindings.Add("Text", cfdiV33, "CondicionPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboCondiciones.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboCondiciones.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);
            this.CboCondiciones.Enabled = false;

            this.CboMoneda.DataBindings.Clear();
            this.CboMoneda.DataBindings.Add("SelectedValue", cfdiV33.Moneda, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboMoneda.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboMoneda.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);
            this.CboMoneda.Enabled = false;

            this.TxbCuentaPago.DataBindings.Clear();
            this.TxbCuentaPago.DataBindings.Add("Text", cfdiV33, "CtaPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbCuentaPago.ReadOnly = !this.cfdiV33.Editable;

            this.TxbTipoCambio.DataBindings.Clear();
            this.TxbTipoCambio.DataBindings.Add("Text", cfdiV33, "TipoCambio", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbTipoCambio.ReadOnly = !this.cfdiV33.Editable;
            this.TxbTipoCambio.Enabled = false;

            this.FechaEmisionField.DataBindings.Clear();
            this.FechaEmisionField.DataBindings.Add("Value", cfdiV33, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaEmisionField.DateTimePickerElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.FechaEmisionField.DateTimePickerElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.FechaCertifica.DataBindings.Clear();
            this.FechaCertifica.DataBindings.Add("Value", cfdiV33, "FechaCert", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaCertifica.DateTimePickerElement.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.FechaCertifica.DateTimePickerElement.ArrowButton.Visibility = ElementVisibility.Collapsed;

            this.TxbFolio.DataBindings.Clear();
            this.TxbFolio.DataBindings.Add("Text", cfdiV33, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboSerie.DataBindings.Clear();
            this.CboSerie.DataBindings.Add("Text", cfdiV33, "Serie", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ToolBarCfdiTipoRelacion.DataBindings.Clear();
            if (!(this.cfdiV33.CfdiRelacionados == null)) {
                this.ToolBarCfdiTipoRelacion.DataBindings.Add("SelectedValue", cfdiV33.CfdiRelacionados.TipoRelacion, "Clave", false, DataSourceUpdateMode.OnPropertyChanged);
            }

            this.GridConceptos.DataSource = this.cfdiV33.Conceptos;
            this.GridCfdiRelacionados.DataSource = this.cfdiV33.CfdiRelacionados;
            this.GridCfdiRelacionados.DataMember = "CfdiRelacionado";

            // esta timbrado?
            if (this.cfdiV33.TimbreFiscal == null) {
                if (HelperCertificacion.ValidaFechaEmisionMenor72H(this.cfdiV33.FechaEmision, 1) == false) {
                    RadMessageBox.Show(this, "La fecha de emisión esta fuera del rango permitido es necesario actualizar la fecha.", "Información", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    this.FechaEmisionField.MinDate = DateTime.Now.AddDays(-1);
                    this.cfdiV33.FechaEmision = DateTime.Now;
                }
            }

            // barra de herramientas conceptos del comprobante
            this.ToolBarConceptoAgregar.DataBindings.Clear();
            this.ToolBarConceptoAgregar.DataBindings.Add("Enabled", this.cfdiV33, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);
            this.ToolBarConceptoQuitar.DataBindings.Clear();
            this.ToolBarConceptoQuitar.DataBindings.Add("Enabled", this.cfdiV33, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);
            this.ToolBarConceptoBuscar.DataBindings.Clear();
            this.ToolBarConceptoBuscar.DataBindings.Add("Enabled", this.cfdiV33, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);
            this.ToolBarConceptoButtonNuevo.DataBindings.Clear();
            this.ToolBarConceptoButtonNuevo.DataBindings.Add("Enabled", this.cfdiV33, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);
            this.GridConceptos.DataBindings.Clear();
            this.GridConceptos.DataBindings.Add("AllowEditRow", this.cfdiV33, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);

            // cfdi relacionados
            this.CommandCfdiRelacionado.DataBindings.Clear();
            this.CommandCfdiRelacionado.DataBindings.Add("Enabled", this.cfdiV33, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);
            this.ChkCfdiRelacionadoIncluir.DataBindings.Clear();
            this.ChkCfdiRelacionadoIncluir.DataBindings.Add("Enabled", this.cfdiV33, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);
            this.GridCfdiRelacionados.AllowEditRow = this.ChkCfdiRelacionadoIncluir.CheckState == CheckState.Checked;

            this.ToolBarPdf.Enabled = HelperValidacion.ValidaUrl(this.cfdiV33.FilePdf);
            this.ToolBarPdf.Visibility = (this.ToolBarPdf.Enabled ? ElementVisibility.Visible : ElementVisibility.Collapsed);

            this.ToolBarXml.Enabled = HelperValidacion.ValidaUrl(this.cfdiV33.FileXml);
            this.ToolBarXml.Visibility = (this.ToolBarXml.Enabled ? ElementVisibility.Visible : ElementVisibility.Collapsed);

            this.ToolBarEmail.Enabled = this.ToolBarXml.Enabled;

            this.ToolBarCreate.Enabled = (this.cfdiV33.TimbreFiscal == null);
            this.ToolBarCreate.Visibility = (this.ToolBarCreate.Enabled ? ElementVisibility.Visible : ElementVisibility.Collapsed);

            this.ToolBarCancelar.Enabled = this.ToolBarCancelar.Enabled = !(this.cfdiV33.TimbreFiscal == null) && (this.cfdiV33.StatusText != "Cancelado");
            this.ToolBarCancelar.Visibility = (this.ToolBarCreate.Enabled ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.ToolBarCopy.Enabled = !(this.cfdiV33.TimbreFiscal == null);
            this.ToolBarCopy.Visibility = (this.ToolBarCopy.Enabled ? ElementVisibility.Visible : ElementVisibility.Collapsed);

            this.ToolBarSeries.Visibility = (!this.cfdiV33.Editable ? ElementVisibility.Visible : ElementVisibility.Collapsed);

            this.ToolBarSave.Enabled = this.cfdiV33.Editable;

            this.CreateBindingComprobantePago();
        }

        private void CreateBindingComprobantePago() {

            this.ToolBarDoctosButtonAgregar.DataBindings.Clear();
            this.ToolBarDoctosButtonAgregar.DataBindings.Add("Enabled", this.cfdiV33, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ToolBarDoctosButtonQuitar.DataBindings.Clear();
            this.ToolBarDoctosButtonQuitar.DataBindings.Add("Enabled", this.cfdiV33, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ToolBarDoctosHostComprobantes.DataBindings.Clear();
            this.ToolBarDoctosHostComprobantes.DataBindings.Add("Enabled", this.cfdiV33, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ToolBarDoctosHostAutoSuma.DataBindings.Clear();
            this.ToolBarDoctosHostAutoSuma.DataBindings.Add("Enabled", this.cfdiV33, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ToolBarDoctosContador.DataBindings.Clear();
            this.ToolBarDoctosContador.DataBindings.Add("Text", this.cfdiV33.ComplementoPagos.Pago[0], "Contador", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ToolBarDoctosImportePagado.DataBindings.Clear();
            this.ToolBarDoctosImportePagado.DataBindings.Add("Text", this.cfdiV33.ComplementoPagos.Pago[0], "TotalSaldoImportePagado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.GridDoctosRelacionados.DataSource = this.cfdiV33.ComplementoPagos.Pago[0].DoctoRelacionado;
            this.GridDoctosRelacionados.AllowEditRow = this.cfdiV33.Editable;

            this.FechaPago.DataBindings.Clear();
            this.FechaPago.DataBindings.Add("Value", this.cfdiV33.ComplementoPagos.Pago[0], "FechaPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaPago.DateTimePickerElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.FechaPago.DateTimePickerElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.CboFormaPagoP.DataBindings.Clear();
            this.CboFormaPagoP.DataBindings.Add("SelectedValue", this.cfdiV33.ComplementoPagos.Pago[0].FormaDePagoP, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboFormaPagoP.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboFormaPagoP.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.TxbNumOperacion.DataBindings.Clear();
            this.TxbNumOperacion.DataBindings.Add("Text", this.cfdiV33.ComplementoPagos.Pago[0], "NumOperacion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbNumOperacion.ReadOnly = !this.cfdiV33.Editable;

            this.TxbTipoCambioDR.DataBindings.Clear();
            this.TxbTipoCambioDR.DataBindings.Add("Text", this.cfdiV33.ComplementoPagos.Pago[0], "TipoCambioP", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbTipoCambioDR.ReadOnly = !this.cfdiV33.Editable;

            this.TxbMonto.DataBindings.Clear();
            this.TxbMonto.DataBindings.Add("Text", this.cfdiV33.ComplementoPagos.Pago[0], "Monto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbMonto.ReadOnly = !this.cfdiV33.Editable;

            this.CboMonedaP.DataBindings.Clear();
            this.CboMonedaP.DataBindings.Add("Text", this.cfdiV33.ComplementoPagos.Pago[0], "MonedaP", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboMonedaP.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboMonedaP.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.TxbRfcEmisorCtaOrigen.DataBindings.Clear();
            this.TxbRfcEmisorCtaOrigen.DataBindings.Add("Text", this.cfdiV33.ComplementoPagos.Pago[0], "RfcEmisorCtaOrd", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbRfcEmisorCtaOrigen.ReadOnly = !this.cfdiV33.Editable;

            this.CboNomBancoOrdExt.DataBindings.Clear();
            this.CboNomBancoOrdExt.DataBindings.Add("Text", this.cfdiV33.ComplementoPagos.Pago[0], "NomBancoOrdExt", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboNomBancoOrdExt.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboNomBancoOrdExt.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.TxbCtaOrdenante.DataBindings.Clear();
            this.TxbCtaOrdenante.DataBindings.Add("Text", this.cfdiV33.ComplementoPagos.Pago[0], "CtaOrdenante", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbCtaOrdenante.ReadOnly = !this.cfdiV33.Editable;

            this.TxbRfcCtaBeneficiario.DataBindings.Clear();
            this.TxbRfcCtaBeneficiario.DataBindings.Add("Text", this.cfdiV33.ComplementoPagos.Pago[0], "RfcEmisorCtaBen", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbRfcCtaBeneficiario.ReadOnly = !this.cfdiV33.Editable;

            this.TxbCtaBeneficiario.DataBindings.Clear();
            this.TxbCtaBeneficiario.DataBindings.Add("Text", this.cfdiV33.ComplementoPagos.Pago[0], "CtaBeneficiario", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbCtaBeneficiario.ReadOnly = !this.cfdiV33.Editable;

            this.CboTipoCadenaPago.DataBindings.Clear();
            this.CboTipoCadenaPago.DataBindings.Add("Text", this.cfdiV33.ComplementoPagos.Pago[0], "TipoCadPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboTipoCadenaPago.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboTipoCadenaPago.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.TxbCadenaPago.DataBindings.Clear();
            this.TxbCadenaPago.DataBindings.Add("Text", this.cfdiV33.ComplementoPagos.Pago[0], "CadPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbCadenaPago.ReadOnly = !this.cfdiV33.Editable;

            this.TxbCertificadoPago.DataBindings.Clear();
            this.TxbCertificadoPago.DataBindings.Add("Text", this.cfdiV33.ComplementoPagos.Pago[0], "CertPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbCertificadoPago.ReadOnly = !this.cfdiV33.Editable;

            this.TxbSelloPago.DataBindings.Clear();
            this.TxbSelloPago.DataBindings.Add("Text", this.cfdiV33.ComplementoPagos.Pago[0], "SelloPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbSelloPago.ReadOnly = !this.cfdiV33.Editable;

            this.ChkAutoSuma.DataBindings.Clear();
            this.ChkAutoSuma.DataBindings.Add("Checked", this.cfdiV33.ComplementoPagos.Pago[0], "AutoSuma", true, DataSourceUpdateMode.OnPropertyChanged);


        }

        #region conceptos del comprobante

        private void CboConceptos_DropDownOpened(object sender, EventArgs e) {
            if ((this.cfdiV33.Editable)) {
                this.CboConceptos.AutoSizeDropDownToBestFit = true;
                this.catalogoRecientes.Load();
                this.CboConceptos.DataSource = this.catalogoRecientes.Items;
            }
        }

        private void CboConceptos_DropDownOpening(object sender, CancelEventArgs args) {
            args.Cancel = !this.cfdiV33.Editable;
        }

        private void CboConceptos_SelectedValueChanged(object sender, EventArgs e) {

        }

        #endregion

        #region documentos relacionados al complemento de pago

        private void BttnSpei_Click(object sender, EventArgs e) {
            OpenFileDialog dialogoAbrirArchivo = new OpenFileDialog() {
                Title = "Archivo",
                Multiselect = false,
                Filter = "*.xml|*.XML"
            };

            if (dialogoAbrirArchivo.ShowDialog() == DialogResult.OK) {
                // intentamos leer la primera versión
                CFDI.Complemento_SPEI spei = CFDI.Complemento_SPEI.Load(dialogoAbrirArchivo.FileName);
                if (spei == null) {
                    CFDI.SpeiTercero speitercero = CFDI.SpeiTercero.LoadX(dialogoAbrirArchivo.FileName);
                    if (speitercero == null) {
                        RadMessageBox.Show(this, "Archivo SPEI no válido!", "Atención", MessageBoxButtons.OK);
                        return;
                    }
                    this.cfdiV33.ComplementoPagos.Pago[0] = HelperConvertidor.Create(speitercero);
                    this.cfdiV33.ComplementoPagos.Pago[0].AutoSuma = false;
                    this.CreateBindingComprobantePago();
                }
            }
        }

        private void CboPagoDoctosRelacionados_DropDownOpened(object sender, EventArgs e) {
            this.CboPagoDoctosRelacionados.DataSource = this.data.GetListSingleBy(EnumCfdiSubType.Emitido, this.TxbReceptorRFC.Text, "", "PorCobrar", "PPD");
            this.CboPagoDoctosRelacionados.AutoSizeDropDownToBestFit = true;
            this.CboPagoDoctosRelacionados.AutoSizeDropDownHeight = true;
        }

        private void ToolBarDoctosButtonAgregar_Click(object sender, EventArgs e) {
            GridViewRowInfo rowView = this.CboPagoDoctosRelacionados.SelectedItem as GridViewRowInfo;
            if (rowView != null) {
                if (this.cfdiV33.ComplementoPagos.Pago[0].DoctoRelacionado == null) {
                    this.cfdiV33.ComplementoPagos.Pago[0].DoctoRelacionado = new BindingList<ComplementoPagoDoctoRelacionado>();
                    this.GridCfdiRelacionados.DataSource = this.cfdiV33.ComplementoPagos.Pago[0].DoctoRelacionado;
                }
                var objeto = rowView.DataBoundItem as ViewModelComprobanteSingle;
                if (objeto != null) {
                    // evitar posible duplicados
                    ComplementoPagoDoctoRelacionado b = this.cfdiV33.ComplementoPagos.Pago[0].Buscar(objeto.IdDocumento);
                    if (b == null) {
                        ComplementoPagoDoctoRelacionado newItem = new ComplementoPagoDoctoRelacionado();
                        newItem.FechaEmision = objeto.FechaEmision;
                        newItem.Folio = objeto.Folio;
                        newItem.Serie = objeto.Serie;
                        newItem.IdDocumento = objeto.IdDocumento;
                        newItem.Nombre = objeto.Receptor;
                        newItem.RFC = objeto.ReceptorRFC;
                        newItem.MetodoPago = objeto.MetodoPago;
                        newItem.ImpSaldoAnt = objeto.Saldo;
                        newItem.Moneda = objeto.Moneda;
                        newItem.NumParcialidad = objeto.NumParcialidad + 1;
                        newItem.Estado = EnumEdoPagoDoctoRel.Relacionado;
                        this.cfdiV33.ComplementoPagos.Pago[0].DoctoRelacionado.Add(newItem);
                        this.CboPagoDoctosRelacionados.SelectedItem = null;
                        this.GridDoctosRelacionados.DataSource = this.cfdiV33.ComplementoPagos.Pago[0].DoctoRelacionado;
                    }
                }
            }
            this.GridDoctosRelacionados.Refresh();
        }

        private void ToolBarDoctosButtonQuitar_Click(object sender, EventArgs e) {
            if (!(this.GridDoctosRelacionados.CurrentRow == null)) {
                this.GridDoctosRelacionados.Rows.Remove(this.GridDoctosRelacionados.CurrentRow);
            }
        }

        #endregion

        private void Actualizar() {
            this.cfdiV33 = data.GetComprobante(this.cfdiV33.Id);
        }

        private void Guardar() {
            this.cfdiV33.Complementos = new Complementos();
            this.cfdiV33.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.Pagos10, Data = this.cfdiV33.ComplementoPagos.Json() });
            this.data.Save(this.cfdiV33);
        }

        private void Procesar() {
            this.certifica = new HelperCertificacion(ConfigService.Synapsis);
            CFDI.V33.Comprobante response = certifica.Timbrar(this.cfdiV33);
            if (response != null) {
                if (this.certifica.Codigo > 0) {
                    RadMessageBox.Show(this, certifica.Mensaje);
                }
                else {
                    // convertir a pdf
                    HelperPdfPrinter pdf = new HelperPdfPrinter();
                    HelperConvertidor convertidor = new HelperConvertidor();

                    this.cfdiV33.TimbreFiscal = convertidor.TimbreFiscal(response.Complemento.TimbreFiscalDigital);
                    this.cfdiV33.NoCertificado = response.NoCertificado;
                    this.cfdiV33.Version = response.Version;
                    this.cfdiV33.Xml = response.Serialize();
                    response.Save(JaegerManagerPaths.JaegerPath(EnumPaths.Comprobantes, this.cfdiV33.KeyName() + ".Xml"));

                    // almacenar los cambios en la base de datos.
                    this.cfdiV33.Status = "PorCobrar";
                    this.cfdiV33.Estado = "Vigente";
                    this.cfdiV33 = this.data.Save(this.cfdiV33);

                    // guardar en s3
                    Aplication.Comprobantes.IEditaBucketService s3 = new Aplication.Comprobantes.EditaBucketService(ConfigService.Synapsis.Amazon.S3, ConfigService.Synapsis.ProveedorAutorizado.Certificacion.Production);
                    s3.produccion = ConfigService.Synapsis.ProveedorAutorizado.Certificacion.Production;

                    this.cfdiV33.FileXml = s3.Upload(JaegerManagerPaths.JaegerPath(EnumPaths.Comprobantes, this.cfdiV33.KeyName() + ".Xml"), string.Concat(this.cfdiV33.KeyName(), ".Xml"));
                    this.data.UpdateUrlXml(this.cfdiV33.Id, this.cfdiV33.FileXml);

                    // crear representacion impresa
                    pdf.CarpetaComprobantes = JaegerManagerPaths.JaegerPath(EnumPaths.Comprobantes);
                    pdf.CarpetaMedios = JaegerManagerPaths.JaegerPath(EnumPaths.Media);
                    pdf.CarpetaTemporal = JaegerManagerPaths.JaegerPath(EnumPaths.Temporal);
                    pdf.Clave = ConfigService.Synapsis.Empresa.Clave;
                    string archivoPdf = pdf.HtmlToPdf(response, this.cfdiV33.KeyName());
                    if (System.IO.File.Exists(archivoPdf)) {
                        this.cfdiV33.FilePdf = s3.Upload(archivoPdf, String.Concat(this.cfdiV33.KeyName(), ".pdf"));
                        this.data.UpdateUrlPdf(this.cfdiV33.Id, this.cfdiV33.FilePdf);
                    }
                    else {
                        // en caso de que no exista en templete html, creamos la representacion generica
                        ViewReportes generico = new ViewReportes(response);
                        generico.Show();
                    }
                }
            }
            else {
                this.ToolBarPdf.Enabled = HelperValidacion.ValidaUrl(this.cfdiV33.FilePdf);
                this.ToolBarXml.Enabled = HelperValidacion.ValidaUrl(this.cfdiV33.FileXml);
                this.ToolBarCreate.Enabled = true;
            }
        }

        private void Cancelar() {
            // clase de certificación de comprobante
            this.certifica = new HelperCertificacion(ConfigService.Synapsis);
            CancelaCFDResponse response = certifica.Cancelar(this.cfdiV33);
            if (response != null) {
                if (response.CancelaCFDResult != null) {
                    this.cfdiV33.Accuse = response;
                    this.data.Update(this.cfdiV33.TimbreFiscal.UUID, response, ConfigService.Piloto.Clave);
                }
            }
        }
    }
}
