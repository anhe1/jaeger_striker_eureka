﻿using System;
using System.Drawing;
using System.Xml;
using Telerik.WinControls.UI;

namespace Jaeger.Views
{
    public partial class ViewComprobanteVisor : RadForm
    {
        private string strFileXml = "";

        public ViewComprobanteVisor()
        {
            InitializeComponent();
        }

        public ViewComprobanteVisor(string archivo)
        {
            InitializeComponent();
            this.strFileXml = archivo;
        }

        private void ViewComprobanteVisor_Load(object sender, EventArgs e)
        {
            this.Text = System.IO.Path.GetFileName(this.strFileXml);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(this.strFileXml);
            this.radTreeView.Nodes.Clear();

            Helpers.HelperXmlSerializer.FormatXmlNode(xmlDoc.DocumentElement, 0);

            

            RadTreeNode node = this.radTreeView.Nodes.Add(this.strFileXml);
            node.Expanded = true;

            foreach (XmlNode xmlNode in xmlDoc.ChildNodes)
            {
                TreeviewAddNode(xmlNode, node);
            }

            radTreeView.ShowLines = true;
            radTreeView.ShowRootLines = true;
        }

        private void TreeviewAddNode(XmlNode xNode, RadTreeNode oNode)
        {
            RadTreeNode mNode = new RadTreeNode(xNode.Name, true);
            switch (xNode.NodeType)
            {
                case XmlNodeType.Element:
                    mNode.ForeColor = Color.Blue;
                    oNode.Nodes.Add(mNode);
                    break;
                case XmlNodeType.Text:
                    //oNode.Nodes.Add(mNode);
                    break;
                case XmlNodeType.Comment:
                    oNode.Nodes.Add(mNode);
                    break;
                default:
                    if (oNode != null)
                        mNode = oNode;
                    break;
            }

            if (xNode.Attributes != null)
            {
                foreach (XmlAttribute atributo in xNode.Attributes)
                {
                    switch (atributo.NodeType)
                    {
                        case XmlNodeType.Attribute:
                            mNode.Nodes.Add(string.Format("[{0} = {1}]", atributo.Name, atributo.Value));
                            break;
                        case XmlNodeType.ProcessingInstruction:
                            mNode.Text = string.Format("{0} [{1} = {2}]", mNode.Text, atributo.Name, atributo.Value);
                            break;
                        default:
                            break;
                    }
                }
            }

            if (xNode.ChildNodes.Count > 0)
            {
                foreach (XmlNode item in xNode.ChildNodes)
                {
                    this.TreeviewAddNode(item, mNode);
                }
            }
        }
        
    }
}
