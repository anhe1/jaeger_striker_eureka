﻿namespace Jaeger.Views
{
    partial class ViewComprobantesResumen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CommandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabelEjercicio = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarButtonEjercicio = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarButtonActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonExportar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.GridData = new Telerik.WinControls.UI.RadPivotGrid();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            this.GridData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandBar
            // 
            this.CommandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBar.Location = new System.Drawing.Point(0, 0);
            this.CommandBar.Name = "CommandBar";
            this.CommandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement});
            this.CommandBar.Size = new System.Drawing.Size(1211, 63);
            this.CommandBar.TabIndex = 0;
            // 
            // commandBarRowElement
            // 
            this.commandBarRowElement.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement.Name = "commandBarRowElement";
            this.commandBarRowElement.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "commandBarStripElement1";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelEjercicio,
            this.ToolBarButtonEjercicio,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonExportar,
            this.ToolBarButtonCerrar});
            this.ToolBar.Name = "ToolBar";
            // 
            // ToolBarLabelEjercicio
            // 
            this.ToolBarLabelEjercicio.DisplayName = "Etiqueta Ejercicio";
            this.ToolBarLabelEjercicio.DrawImage = false;
            this.ToolBarLabelEjercicio.Name = "ToolBarLabelEjercicio";
            this.ToolBarLabelEjercicio.Text = "Ejercicio:";
            // 
            // ToolBarButtonEjercicio
            // 
            this.ToolBarButtonEjercicio.DefaultItem = null;
            this.ToolBarButtonEjercicio.DisplayName = "Botón Ejercicio";
            this.ToolBarButtonEjercicio.DrawText = true;
            this.ToolBarButtonEjercicio.Image = null;
            this.ToolBarButtonEjercicio.Name = "ToolBarButtonEjercicio";
            this.ToolBarButtonEjercicio.Text = "Selecciona";
            this.ToolBarButtonEjercicio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.DisplayName = "Actualizar";
            this.ToolBarButtonActualizar.DrawText = true;
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_actualizar;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.ToolBarButtonActualizar_Click);
            // 
            // ToolBarButtonExportar
            // 
            this.ToolBarButtonExportar.DisplayName = "Exportar";
            this.ToolBarButtonExportar.DrawText = true;
            this.ToolBarButtonExportar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_excel;
            this.ToolBarButtonExportar.Name = "ToolBarButtonExportar";
            this.ToolBarButtonExportar.Text = "Exportar";
            this.ToolBarButtonExportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonExportar.Click += new System.EventHandler(this.ToolBarButtonExportar_Click);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "Cerrar";
            this.ToolBarButtonCerrar.DrawImage = true;
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_cerrar;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // GridData
            // 
            this.GridData.Controls.Add(this.Espera);
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 63);
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1211, 576);
            this.GridData.TabIndex = 1;
            // 
            // Espera
            // 
            this.Espera.AssociatedControl = this.GridData;
            this.Espera.Location = new System.Drawing.Point(414, 173);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(130, 24);
            this.Espera.TabIndex = 0;
            this.Espera.Text = "radWaitingBar1";
            this.Espera.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 80;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            // 
            // ViewComprobantesResumen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1211, 639);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.CommandBar);
            this.Name = "ViewComprobantesResumen";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ComprobantesResumen";
            this.Load += new System.EventHandler(this.ComprobantesResumen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.GridData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar CommandBar;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.RadPivotGrid GridData;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelEjercicio;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonEjercicio;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonActualizar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonExportar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCerrar;
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;



    }
}
