﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls.UI;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.Enums;
using Jaeger.Edita.V2;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Entities;
using Jaeger.Helpers;
using Jaeger.Util.Helpers;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Views.Contable;
using Jaeger.SAT.Entities;
using Jaeger.Edita.Interfaces;
using System.IO;
using Jaeger.UI.Common.Forms;
using Jaeger.Catalogos.Repositories;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views {
    public partial class ViewComprobantesFiscales : RadForm {
        private string archivoPreferencias;
        private BackgroundWorker preparar;
        private BackgroundWorker consultar;
        private BackgroundWorker reporte1;
        private Aplication.Comprobantes.IEditaBucketService s3;
        private EnumCfdiSubType subTipoComprobante;
        private Status statusAutorizado;
        private ISqlComprobanteFiscal data;
        private FormaPagoCatalogo myFormaDePago33;
        private RadWaitingBarElement waitingElement;
        private Edita.V2.Contable.Entities.ViewModelPrePoliza nuevo;
        private BindingList<ViewModelComprobanteSingleC> datos;
        private List<ViewModelComprobanteSingleC> seleccion = new List<ViewModelComprobanteSingleC>();
        private ViewModelComprobante seleccionado;

        public ViewComprobantesFiscales(EnumCfdiSubType objeto, ref RadWaitingBarElement element) {
            InitializeComponent();
            this.waitingElement = element;
            base.Load += ComprobantesFiscales_Load;

            this.PrintValidacion.Click += PrintValidacion_Click;
            this.PrintComprobante.Click += PrintComprobante_Click;
            this.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
            this.GridData.RowsChanged += GridData_RowsChanged;

            //this.GridData.KeyDown += GridData_KeyDown;
            //this.GridData.ContextMenuOpening += new ContextMenuOpeningEventHandler(this.GridData_ContextMenuOpening);
            this.subTipoComprobante = objeto;
        }

        private void ComprobantesFiscales_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            s3 = new Aplication.Comprobantes.EditaBucketService(ConfigService.Synapsis.Amazon.S3, ConfigService.Synapsis.ProveedorAutorizado.Certificacion.Production);

            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += Preparar_DoWork;
            this.preparar.RunWorkerCompleted += Preparar_RunWorkerCompleted;
            this.preparar.RunWorkerAsync();

            this.consultar = new BackgroundWorker();
            this.consultar.DoWork += ConsultarDoWork;
            this.consultar.RunWorkerCompleted += ConsultarRunWorkerCompleted;

            this.reporte1 = new BackgroundWorker();
            this.reporte1.DoWork += Reporte1_DoWork;
            this.reporte1.RunWorkerCompleted += Reporte1_RunWorkerCompleted;

            this.ToolBar.OverflowButton.CustomizeButtonMenuItem.Visibility = ElementVisibility.Collapsed;
            this.ToolBar.OverflowButton.AddRemoveButtonsMenuItem.Visibility = ElementVisibility.Collapsed;
        }

        #region barra de Herramientas

        private void ToolBarButtonPeriodo_Click(object sender, EventArgs e) {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem) {
                this.ToolBarPeriodo.DefaultItem = button;
                this.ToolBarPeriodo.Text = button.Text;
                this.ToolBarPeriodo.Tag = button.Tag;
                this.ToolBarPeriodo.PerformClick();
                this.ToolBarRefresh.PerformClick();
            }
        }

        private void ToolBarButtonEjercicio_Click(object sender, EventArgs e) {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem) {
                this.ToolBarEjercicio.DefaultItem = button;
                this.ToolBarEjercicio.Text = button.Text;
                this.ToolBarEjercicio.PerformClick();
            }
        }

        private void ToolBarComprobanteNuevo_Click(object sender, EventArgs e) {
            if (this.subTipoComprobante == EnumCfdiSubType.Emitido) {
                ViewComprobante1Fiscal nuevo = new ViewComprobante1Fiscal(null) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante" };
                nuevo.Show();
            }
            else if (this.subTipoComprobante == EnumCfdiSubType.Recibido) {
                MessageBox.Show("No puedes agregar comprobantes desde esta opción, tienes que validar el comprobante");
            }
        }

        private void ToolBarComprobantePago_Click(object sender, System.EventArgs e) {
            var nuevo = new ViewComprobante4Fiscal(null) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante" };
            nuevo.Show();
        }

        private void ToolBarComprobanteLink_Click(object sender, EventArgs e) {
            MessageBox.Show("Esta opción no contiene ninguna descripción ó método");
        }

        private void ToolBarComprobanteClonar_Click(object sender, EventArgs e) {
            if (this.subTipoComprobante == EnumCfdiSubType.Emitido) {
                if (!(this.GridData.CurrentRow == null)) {
                    ViewModelComprobanteSingleC seleccionado = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteSingleC;
                    if (seleccionado != null) {
                        ViewModelComprobante d = data.GetComprobante(seleccionado.Id);
                        if (!(d == null)) {
                            d.Clonar();
                            d.Status = "PorCobrar";
                            if (d.TipoComprobante == EnumCfdiType.Pagos) {
                                var nuevo = new ViewComprobante4Fiscal(d) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante Pagos" };
                                nuevo.Show();
                            }
                            else if (d.TipoComprobante == EnumCfdiType.Ingreso | d.TipoComprobante == EnumCfdiType.Egreso) {
                                var nuevo = new ViewComprobante2Fiscal(d) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante" };
                                nuevo.Show();
                            }
                        }
                    }
                }
            }
        }

        private void ToolBarComprobanteSustituir_Click(object sender, EventArgs e) {
            if (this.subTipoComprobante == EnumCfdiSubType.Emitido) {
                if (!(this.GridData.CurrentRow == null)) {
                    ViewModelComprobanteSingleC seleccionado = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteSingleC;
                    if (seleccionado != null) {
                        ViewModelComprobante d = this.data.GetComprobante(seleccionado.Id);
                        if (!(d == null)) {
                            d.Sustituir();
                            if (d.TipoComprobante == EnumCfdiType.Pagos) {
                                var nuevo = new ViewComprobante4Fiscal(d) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante Pagos" };
                                nuevo.Show();
                            }
                            else if (d.TipoComprobante == EnumCfdiType.Ingreso | d.TipoComprobante == EnumCfdiType.Egreso) {
                                var nuevo = new ViewComprobante2Fiscal(d) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante" };
                                nuevo.Show();
                            }
                        }
                    }
                }
            }
        }

        private void ToolBarEdit_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                ViewModelComprobanteSingleC indice = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteSingleC;
                if (indice != null) {
                    if (this.subTipoComprobante == EnumCfdiSubType.Emitido) {
                        if (indice.TipoComprobante == EnumCfdiType.Pagos) {
                            ViewComprobante4Fiscal objeto = new ViewComprobante4Fiscal(indice.Id) { MdiParent = this.ParentForm };
                            objeto.Show();
                        }
                        else {
                            ViewComprobante1Fiscal objeto = new ViewComprobante1Fiscal(indice.Id) { MdiParent = this.ParentForm };
                            objeto.Show();
                        }
                    }
                    else if (this.subTipoComprobante == EnumCfdiSubType.Recibido) {
                        ViewComprobante2Fiscal objeto = new ViewComprobante2Fiscal((int)indice.Id) { MdiParent = this.ParentForm };
                        objeto.Show();
                    }
                }
            }
        }

        private void ValidateSatSelected_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.QueryStateSAT)) {
                espera.Text = "Consultando Estado SAT";
                espera.ShowDialog(this);
            }
        }

        private void ToolBarRefresh_Click(object sender, EventArgs e) {
            this.Waiting.StartWaiting();
            this.Waiting.Visible = true;
            this.ToolBarRefresh.Enabled = false;
            this.waitingElement.StartWaiting();
            this.waitingElement.Visibility = ElementVisibility.Visible;
            if (this.consultar.IsBusy)
                this.consultar.Dispose();
            this.consultar.RunWorkerAsync();
        }

        private void ToolBarFilter_Click(object sender, EventArgs e) {
            this.GridData.ShowFilteringRow = this.ToolBarFilter.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false)
                this.GridData.FilterDescriptors.Clear();
        }

        private void ToolBarExport_Click(object sender, EventArgs e) {
            this.GridData.RowSourceNeeded -= new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
            TelerikGridExport exportar = new TelerikGridExport(this.GridData) { StartPosition = FormStartPosition.CenterParent };
            exportar.ShowDialog();
            this.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
        }

        private void ToolBarExportReporte1_Click(object sender, EventArgs e) {
            SaveFileDialog saveFileDialog = new SaveFileDialog { Filter = "xlsx|XLSX" };
            saveFileDialog.FileName = string.Concat("Reporte_", this.ToolBarPeriodo.Text, "_", DateTime.Now.ToString("ddmmss"), ".xlsx");
            if (saveFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }
            this.ToolBarButtonExportarExcelReporte1.Tag = saveFileDialog.FileName;
            this.ToolBarRefresh.Enabled = false;
            this.Waiting.StartWaiting();
            this.Waiting.Visible = true;
            this.reporte1.RunWorkerAsync();
        }

        private void PrintValidacion_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                ViewModelComprobanteSingleC objeto = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteSingleC;
                if (objeto != null) {
                    ViewReportes m = new ViewReportes(objeto.Validacion);
                    m.Show();
                }
                else
                    RadMessageBox.Show(this, "No existe validación relacionada a este comprobante fiscal.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
            else
                RadMessageBox.Show(this, "No existe validación relacionada a este comprobante fiscal.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
        }

        private void PrintComprobante_Click(object sender, EventArgs e) {
            if (this.GridData.Rows.Count > 0) {
                try {
                    this.GridData.PrintPreview();
                }
                catch (Exception ex) {
                    RadMessageBox.Show(string.Concat("Error: ", ex.Message));
                }
            }
        }

        private void ToolBarDelete_Click(object sender, EventArgs e) {
            if (!(this.GridData.CurrentRow == null)) {
                if (this.subTipoComprobante == EnumCfdiSubType.Emitido) {
                    if (RadMessageBox.Show(this, "¿Esta seguro de cancelar el comprobante seleccionado?", "Atención", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        RadMessageBox.Show(this, "Pues no se puede!");
                }
                else if (this.subTipoComprobante == EnumCfdiSubType.Recibido) {
                    if (RadMessageBox.Show(this, "¿Esta seguro de remover el comprobante seleccionado?", "Atención", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        RadMessageBox.Show(this, "Pues no se puede!");
                }
            }
        }

        private void PrintLista_Click(object sender, EventArgs e) {
            this.GridData.RowSourceNeeded -= new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
            GridPrintStyle style = new GridPrintStyle();
            RadPrintDocument document = new RadPrintDocument();
            // set another properties
            style.FitWidthMode = PrintFitWidthMode.FitPageWidth;
            style.PrintGrouping = true;
            style.PrintSummaries = true;
            style.PrintHeaderOnEachPage = true;
            style.PrintHierarchy = false;
            this.GridData.PrintStyle = style;
            document.DefaultPageSettings.Margins = new System.Drawing.Printing.Margins(10, 10, 10, 10);
            document.AssociatedObject = this.GridData;
            document.MiddleHeader = "";
            document.MiddleFooter = string.Concat(this.Text, " ", DateTime.Now.ToShortDateString(), " (", ConfigService.Piloto.Clave, ")");

            //'document.RightFooter
            // show print preview dialog
            RadPrintPreviewDialog dialog = new RadPrintPreviewDialog(document);
            dialog.ShowDialog();
            this.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
        }

        private void PrintAcuse_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                ViewModelComprobanteSingleC objeto = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteSingleC;
                if (objeto != null) {
                    ViewModelAccuseCancelacion accuse = this.data.GetAccuse(objeto.IdDocumento);
                    if (accuse != null) {
                        ViewReportes reporte = new ViewReportes(accuse);
                        reporte.Show();
                    }
                    else
                        RadMessageBox.Show(this, "Ocurrio un error al recuperar el acuse de cancelación, por favor reporta el error al Administrador.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
            else
                RadMessageBox.Show(this, "No seleccionaste ningún comprobante!", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
        }

        private void ToolBarButtonPrintEdoCuentaSimple_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                ViewModelComprobanteSingleC viewModel = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteSingleC;
                if (viewModel != null) {
                    Entities.Reportes.EstadoDeCuenta reporte = new Entities.Reportes.EstadoDeCuenta();
                    if (this.subTipoComprobante == EnumCfdiSubType.Recibido)
                        reporte = this.data.ReporteEstadoCuenta(viewModel.EmisorRFC, this.subTipoComprobante, "", (EnumMonthsOfYear)(Enum.Parse(typeof(EnumMonthsOfYear), this.ToolBarPeriodo.Text)), int.Parse(this.ToolBarEjercicio.Text), EnumCfdiDates.FechaEmision);
                    else
                        reporte = this.data.ReporteEstadoCuenta(viewModel.EmisorRFC, this.subTipoComprobante, "", (EnumMonthsOfYear)(Enum.Parse(typeof(EnumMonthsOfYear), this.ToolBarPeriodo.Text)), int.Parse(this.ToolBarEjercicio.Text), EnumCfdiDates.FechaEmision);

                    if (reporte != null) {
                        reporte.Emisor = (this.subTipoComprobante == EnumCfdiSubType.Recibido ? viewModel.Emisor : viewModel.Receptor);
                        ViewReportes nuevo = new ViewReportes(reporte);
                        nuevo.Show();
                    }
                    else
                        RadMessageBox.Show(this, "No se encontro información!", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }

        }

        private void ToolBarLayOut_Click(object sender, EventArgs e) {
            try {
                this.GridData.SaveLayout(this.archivoPreferencias);
            }
            catch (Exception) {
                throw;
            }
        }

        private void ToolBarAutoSum_Click(object sender, EventArgs e) {
            this.GridData.TelerikGridAutoSum(!(this.ToolBarAutoSum.ToggleState == ToggleState.On));
        }

        private void ToolBarRecibo_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                this.seleccion = new List<ViewModelComprobanteSingleC>();

                seleccion.AddRange(this.GridData.SelectedRows.Where(x => x.IsSelected == true).Select(x => x.DataBoundItem as ViewModelComprobanteSingleC));
                int c = seleccion.Where(it => it.Status == "PorCobrar" || it.Status == "PorPagar").Count();
                if (seleccion.Count - c != 0)
                    RadMessageBox.Show(this, "El comprobante seleccionado no cuenta con el status requerido para esta operación.");
                else {
                    using (var espera = new Waiting2Form(this.CrearRecibo)) {
                        espera.Text = "Creando Recibo";
                        espera.ShowDialog(this);
                        if (this.subTipoComprobante == EnumCfdiSubType.Emitido) {
                            ViewReciboCobro nuevo2 = new ViewReciboCobro(nuevo) {
                                Owner = this,
                                StartPosition = FormStartPosition.CenterParent
                            };
                            nuevo2.Show();
                        }
                        else if (this.subTipoComprobante == EnumCfdiSubType.Recibido) {
                            ViewReciboPago nuevo2 = new ViewReciboPago(nuevo) {
                                Owner = this,
                                StartPosition = FormStartPosition.CenterParent
                            };
                            nuevo2.Show();
                        }
                    }
                }
            }
        }

        private void ToolBarButtonEnviar_Click(object sender, EventArgs e) {
            //ComprobantesEnvioCorreo nuevoEnvio = new ComprobantesEnvioCorreo();
            //nuevoEnvio.ShowDialog();
        }

        private void ToolBarButtonProductos_Click(object sender, EventArgs e) {
            var productos = new ViewComprobanteProductos() { MdiParent = this.ParentForm, Text = "Productos y Serv." };
            productos.Show();
        }

        private void ToolBarButtonPDFValidacion_Click(object sender, EventArgs e) {
            if (!(this.GridData.CurrentRow == null)) {
                var folder = new FolderBrowserDialog() { Description = "Selecciona ruta de descarga" };
                if (folder.ShowDialog(this) != DialogResult.OK)
                    return;
                if (Directory.Exists(folder.SelectedPath) == false) {
                    RadMessageBox.Show(this, "No se encontro una ruta de descarga valida!", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                }
                ToolBarButtonPDFValidacion.Tag = folder.SelectedPath;

                this.seleccion = new List<ViewModelComprobanteSingleC>();
                // obtener la seleccion
                seleccion.AddRange(this.GridData.SelectedRows.Where(x => x.IsSelected == true).Select(x => x.DataBoundItem as ViewModelComprobanteSingleC));

                using (var espera = new Waiting2Form(this.PDFValidacion)) {
                    espera.Text = "Procesando ...";
                    espera.ShowDialog(this);
                }
            }
        }

        private void ToolBarClose_Click(object sender, EventArgs e) {
            this.Close();
        }

        #endregion

        #region menu contextual

        private void ContextMenuSeleccion_Click(object sender, EventArgs e) {
            this.ContextMenuSeleccion.IsChecked = !this.ContextMenuSeleccion.IsChecked;
            this.GridData.MultiSelect = this.ContextMenuSeleccion.IsChecked;
        }

        private void ContextMenuCopiar_Click(object sender, EventArgs e) {
            Clipboard.SetDataObject(this.GridData.CurrentCell.Value.ToString());
        }

        private void ContextMenuClonar_Click(object sender, EventArgs e) {
            using (Waiting2Form espera = new Waiting2Form(this.Copiar)) {
                espera.Text = "Espere ...";
                espera.ShowDialog(this);
            }

            if (this.seleccion != null) {
                if (this.seleccionado.TipoComprobante == EnumCfdiType.Pagos) {
                    var nuevo = new ViewComprobante4Fiscal(this.seleccionado) {
                        MdiParent = this.ParentForm,
                        Text = "Nuevo Comprobante Pagos"
                    };
                    nuevo.Show();
                }
                else if (this.seleccionado.TipoComprobante == EnumCfdiType.Ingreso | this.seleccionado.TipoComprobante == EnumCfdiType.Egreso) {
                    var nuevo = new ViewComprobante1Fiscal(this.seleccionado) {
                        MdiParent = this.ParentForm,
                        Text = "Nuevo Comprobante"
                    };
                    nuevo.Show();
                }
            }
        }

        /// <summary>
        /// crear comprobate con complemento de recepcion de pagos
        /// </summary>
        private void ContextMenuCrearPago_Click(object sender, EventArgs e) {
            if (this.subTipoComprobante == EnumCfdiSubType.Emitido)
                if (this.GridData.CurrentRow != null) {
                    this.seleccion = new List<ViewModelComprobanteSingleC>();
                    seleccion.AddRange(this.GridData.SelectedRows.Where(x => x.IsSelected == true).Select(x => x.DataBoundItem as ViewModelComprobanteSingleC));
                    int c = seleccion.Where(it => it.TipoComprobante == EnumCfdiType.Ingreso).Count();
                    if (seleccion.Count - c != 0)
                        RadMessageBox.Show(this, "No es posible crear un Recibo Electrónico de Pago del comprobante seleccionado.", "Información", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    else {
                        if (this.seleccion.Count > 0) {
                            // crear el comprobante y luego la vista
                            ViewModelComprobante cfdiV33 = this.data.CrearReciboElectronicoPago(this.seleccion);
                            cfdiV33.Emisor.RFC = ConfigService.Synapsis.Empresa.RFC;
                            cfdiV33.Emisor.Nombre = ConfigService.Synapsis.Empresa.RazonSocial;
                            ViewComprobante4Fiscal nuevo = new ViewComprobante4Fiscal(cfdiV33) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante" };
                            nuevo.Show();
                        }
                    }
                }
        }

        private void ContextMenuSustituir_Click(object sender, EventArgs e) {
            this.ToolBarComprobanteSustituir.PerformClick();
        }

        private void ContextMenuReciboCobro_Click(object sender, EventArgs e) {
            this.ToolBarReciboCobro.PerformClick();
        }

        private void ContextMenuReciboPago_Click(object sender, EventArgs e) {
            this.ToolBarReciboPago.PerformClick();
        }

        private void ContextMenuCrearNotaCredito_Click(object sender, EventArgs e) {
            //if (this.subTipoComprobante == EnumCfdiSubType.Emitido)
            //{
            //    // comprobamos que no sea un recibo de pago
            //    if (this.GridData.CurrentRow.Cells["TipoComprobante"].Value.ToString() == "Pagos")
            //    {
            //        RadMessageBox.Show(this, "No es posible crear una Nota de Crédito un Recibo Electrónico de Pago.", "Información", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            //        return;
            //    }
            //    // crear el comprobante y luego la vista
            //    Jaeger.CFDI.Entities.Comprobante cfdiV33 = this.data.CrearNotaCredito(this.ComprobantesSeleccionados());
            //    cfdiV33.Emisor.RFC = GlobalVariables.OSynapsis.Customer.RFC;
            //    cfdiV33.Emisor.Nombre = GlobalVariables.OSynapsis.Customer.RazonSocial;
            //    Comprobante1Fiscal nuevo = new Comprobante1Fiscal(cfdiV33) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante" };
            //    nuevo.Show();
            //}
        }

        private void ContextMenuAplicarNotaCredito_Click(object sender, EventArgs e) {
            //if (this.GridData.CurrentRow != null)
            //{
            //    if (this.GridData.CurrentRow.Cells["TipoComprobante"].Value.ToString() != "Egreso")
            //    {
            //        RadMessageBox.Show(this, "No es posible aplicar una Nota de Crédito a este tipo de comprobante.", "Información", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            //        return;
            //    }
            //    ComprobanteNotaCreditoAplicar d = new ComprobanteNotaCreditoAplicar();
            //    d.ShowDialog();
            //}
        }

        private void ContextButtonSubirXml_Click(object sender, EventArgs e) {
            OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "*.xml|*.XML", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
            if (openFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }

            ViewModelComprobanteSingleC single = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteSingleC;
            if (single != null) {
                if (HelperValidacion.ValidaUrl(single.UrlXml))
                    if (RadMessageBox.Show(this, string.Concat("Se reemplazara el archivo actualmente cargado por: ", openFileDialog.FileName, ", esta acción no se puede revertir ¿Esta seguro?"), "Reemplazar archivo", MessageBoxButtons.YesNo, RadMessageIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;
            }
        }

        private void ContextButtonSubirPdf_Click(object sender, EventArgs e) {
            OpenFileDialog openFileDialog = new OpenFileDialog { Filter = "*.pdf|*.PDF", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
            if (openFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }

            ViewModelComprobanteSingleC single = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteSingleC;
            if (single != null) {
                if (RadMessageBox.Show(this, string.Concat("Se reemplazara el archivo actualmente cargado por: ", openFileDialog.FileName, ", esta acción no se puede revertir ¿Esta seguro?"), "Reemplazar archivo", MessageBoxButtons.YesNo, RadMessageIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
            }
        }

        private async void ContextMenuSerializarCFDI_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                //this.ContextMenuSerializarCFDI.Enabled = false;
                //string salida = await SerializarCfdiComplementos(this.GridData.CurrentRow.Cells["_cfdi_url_xml"].Value.ToString());
                //Console.Write(salida);
                //this.ContextMenuSerializarCFDI.Enabled = true;
            }
        }

        #endregion

        #region acciones del grid

        private void GridData_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridHeaderCellElement) {
            }
            else if (e.ContextMenuProvider is GridRowHeaderCellElement) {
            }
            else if (e.ContextMenuProvider is GridFilterCellElement) {
            }
            else if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.GridData.CurrentRow.ViewInfo.ViewTemplate == this.GridData.MasterTemplate) {
                    e.ContextMenu = this.MenuContextual.DropDown;
                }
            }
        }

        private void GridData_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.F5) {
                this.ToolBarRefresh.PerformClick();
            }
        }

        private void GridData_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (e.Column.Name == "Status") {
                if (e.Row.Cells["Status"].Value != null) {
                    if (this.subTipoComprobante == EnumCfdiSubType.Emitido) {
                        if (e.Row.Cells["Status"].Value.ToString() == Enum.GetName(typeof(EnumCfdiStatusIngreso), EnumCfdiStatusIngreso.Cancelado)) {
                            e.Cancel = true;
                        }
                    }
                    else if (this.subTipoComprobante == EnumCfdiSubType.Recibido) {
                        if (e.Row.Cells["Status"].Value.ToString() == Enum.GetName(typeof(EnumCfdiStatusEgreso), EnumCfdiStatusEgreso.Cancelado)) {
                            e.Cancel = true;
                        }
                    }
                    else {
                        e.Cancel = true;
                    }
                }
            }
        }

        private void GridData_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (e.Column.Name == "Status") {
                if (e.Column.Tag == "Actualizar") {
                    e.Column.Tag = "";
                    this.Waiting.StartWaiting();
                    this.Waiting.Visible = true;
                    Application.DoEvents();
                }
            }
        }

        private void GridData_CellValidating(object sender, CellValidatingEventArgs e) {
            if (e.Column == null) {
                return;
            }

            if (e.Column.Name == "Status") {
                if (e.ActiveEditor != null) {
                    if (e.OldValue == e.Value) {
                        e.Cancel = true;
                    }
                    else {
                        if (this.subTipoComprobante == EnumCfdiSubType.Emitido) {
                        }
                        else if (this.subTipoComprobante == EnumCfdiSubType.Recibido) {
                            if (e.Value.ToString() == Enum.GetName(typeof(EnumCfdiStatusEgreso), EnumCfdiStatusEgreso.PorPagar)) {
                                this.GridData.CurrentRow.Tag = e.Value.ToString();
                                using (var espera = new Waiting2Form(this.ActualizaStatus)) {
                                    espera.Text = "Espere ...";
                                    espera.ShowDialog(this);
                                }
                            }
                        }
                        else {
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void GridData_CellFormatting(object sender, CellFormattingEventArgs e) {
            if (e.Column.Name == "UrlXml" || e.Column.Name == "UrlPdf") {
                if (ParseConvert.ConvertString(e.CellElement.Value) != "") {
                    e.CellElement.Image = Iconos.Images[e.Column.Name];
                    e.CellElement.DrawText = false;
                }
                else {
                    e.CellElement.Image = null;
                    e.CellElement.DrawText = true;
                    e.CellElement.Children.Clear();
                }
            }
            else if (e.Column.Name != "Status") {
                // aqui tenemos que agregar las columnas que no debemos limpiar para evitar borrar controles adicionales
                e.CellElement.Image = null;
                e.CellElement.DrawText = true;
                e.CellElement.Children.Clear();
            }
        }

        private void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (!(this.GridData.CurrentRow == null)) {
                if (!(e.Column == null)) {
                    if (e.Column.Name == "UrlXml" || e.Column.Name == "UrlPdf") {
                        ViewModelComprobanteSingleC single = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteSingleC;
                        string liga = "";
                        if (e.Column.Name == "UrlXml")
                            liga = single.UrlXml;
                        else
                            if (e.Column.Name == "UrlPdf")
                            liga = single.UrlPdf;
                        // validar la liga de descarga
                        if (HelperValidacion.ValidaUrl(liga)) {
                            var savefiledialog = new SaveFileDialog {
                                FileName = System.IO.Path.GetFileName(liga)
                            };
                            if (savefiledialog.ShowDialog(this) != DialogResult.OK)
                                return;
                            if (HelperFiles.DownloadFile(liga, savefiledialog.FileName)) {
                                DialogResult dr = RadMessageBox.Show(this, String.Concat("Se descargo correctamente el archivo. ", savefiledialog.FileName, " ¿Quieres abrir el documento?"), "Descarga", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                                if (dr == DialogResult.Yes) {
                                    try {
                                        System.Diagnostics.Process.Start(savefiledialog.FileName);
                                    }
                                    catch (Exception ex) {
                                        string message = string.Format("El archivo no se puede abrir en su sistema.\nError message: {0}", ex.Message);
                                        RadMessageBox.Show(this, message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void GridData_RowsChanged(object sender, GridViewCollectionChangedEventArgs e) {
            this.ToolBarButtonValidar.Enabled = this.GridData.Rows.Count > 0;
            this.ToolBarReciboCobro.Enabled = this.GridData.Rows.Count > 0;
            this.ToolBarReciboPago.Enabled = this.GridData.Rows.Count > 0;
            this.ToolBarComprobanteLink.Enabled = this.GridData.Rows.Count > 0;
            this.ToolBarEdit.Enabled = this.GridData.Rows.Count > 0;
            this.ToolBarDelete.Enabled = this.GridData.Rows.Count > 0;
            this.ToolBarPrint.Enabled = this.GridData.Rows.Count > 0;
        }

        /// <summary>
        /// llenar los datos relacionados al comprobante seleccionado
        /// </summary>
        private void RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.GridConceptos.Caption) {
                // lista de conceptos del comprobante
                ViewModelComprobanteSingleC rowView = e.ParentRow.DataBoundItem as ViewModelComprobanteSingleC;
                if (rowView != null) {
                    BindingList<ViewModelComprobanteConcepto> tabla = data.GetConceptos(rowView.Id);
                    if (tabla != null) {
                        foreach (ViewModelComprobanteConcepto fila in tabla) {
                            GridViewRowInfo row = e.Template.Rows.NewRow();
                            row.Cells["Id"].Value = fila.Id;
                            row.Cells["ClaveUnidad"].Value = fila.ClaveUnidad;
                            row.Cells["Unidad"].Value = fila.Unidad;
                            row.Cells["ClaveProducto"].Value = fila.ClaveProdServ;
                            row.Cells["Cantidad"].Value = fila.Cantidad;
                            row.Cells["Concepto"].Value = fila.Descripcion;
                            row.Cells["Unitario"].Value = fila.ValorUnitario;
                            row.Cells["SubTotal"].Value = fila.SubTotal;
                            row.Cells["NoOrden"].Value = fila.NumPedido;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            }
            else if (e.Template.Caption == this.GridComprobanteCfdiRelacionados.Caption) {
                // tipo de relacion de los comprobantes relacionados
                ViewModelComprobanteSingleC rowView = e.ParentRow.DataBoundItem as ViewModelComprobanteSingleC;
                if (rowView.CfdiRelacionados != null) {
                    ComprobanteCfdiRelacionados temporal = rowView.CfdiRelacionados;
                    if (temporal != null) {
                        GridViewRowInfo row = e.Template.Rows.NewRow();
                        row.Cells["Clave"].Value = temporal.TipoRelacion.Clave;
                        row.Cells["Nombre"].Value = temporal.TipoRelacion.Descripcion;
                        e.SourceCollection.Add(row);
                    }
                }
            }
            else if (e.Template.Caption == this.GridComprobanteCfdiRelacionadosCfdiRelacionado.Caption) {
                // comprobantes relacionados al CFDI
                GridViewHierarchyRowInfo otro = e.ParentRow.Parent as GridViewHierarchyRowInfo;
                ViewModelComprobanteSingleC rowView = otro.DataBoundItem as ViewModelComprobanteSingleC;
                if (rowView != null) {
                    ComprobanteCfdiRelacionados temporal = rowView.CfdiRelacionados;
                    if (temporal != null) {
                        foreach (ComprobanteCfdiRelacionadosCfdiRelacionado item in temporal.CfdiRelacionado) {
                            GridViewRowInfo row = e.Template.Rows.NewRow();
                            row.Cells["UUID"].Value = item.IdDocumento;
                            row.Cells["folio"].Value = item.Folio;
                            row.Cells["serie"].Value = item.Serie;
                            row.Cells["rfc"].Value = item.RFC;
                            row.Cells["nombre"].Value = item.Nombre;
                            row.Cells["total"].Value = item.Total;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            }
            else if (e.Template.Caption == this.GridContable.Caption) {
                // comprobantes de pago o cobro relacionados al comprobante
                ViewModelComprobanteSingleC rowView = e.ParentRow.DataBoundItem as ViewModelComprobanteSingleC;
                System.ComponentModel.BindingList<Jaeger.Edita.V2.Contable.Entities.ViewModelPrePoliza> tabla = data.Recibos(rowView.IdDocumento);
                if (tabla != null) {
                    foreach (Jaeger.Edita.V2.Contable.Entities.ViewModelPrePoliza fila in tabla) {
                        GridViewRowInfo row = e.Template.Rows.NewRow();
                        row.Cells["_cntbl3_folio"].Value = fila.Id;
                        row.Cells["_cntbl3_status"].Value = fila.EstadoText;
                        row.Cells["_cntbl3_fecems"].Value = fila.FechaEmision;
                        row.Cells["_cntbl3_fecdoc"].Value = fila.FechaDocto;
                        row.Cells["_cntbl3c_cargo"].Value = fila.Cargo;
                        row.Cells["_cntbl3c_abono"].Value = fila.Abono;
                        row.Cells["_cntbl3_usr_n"].Value = fila.Creo;
                        row.Cells["NumAutorizacion"].Value = fila.NumAutorizacion;
                        e.SourceCollection.Add(row);
                    }
                }
            }
            else if (e.Template.Caption == this.GridComplementoPagos.Caption) // complemento de pagos
            {
                // informacion del complemento de pagos
                ViewModelComprobanteSingleC rowView = e.ParentRow.DataBoundItem as ViewModelComprobanteSingleC;
                if (!(rowView == null)) {
                    ComplementoPagos complementos = rowView.ComplementoPagos;
                    if (!(complementos == null)) {
                        if (complementos.Pago != null) {
                            foreach (ComplementoPagosPago item in complementos.Pago) {
                                GridViewRowInfo row = e.Template.Rows.NewRow();
                                row.Cells["FecPago"].Value = item.FechaPago;
                                row.Cells["FormaDePago"].Value = item.FormaDePagoP.Clave;
                                row.Cells["Moneda"].Value = item.MonedaP;
                                row.Cells["TipoCambio"].Value = item.TipoCambioP;
                                row.Cells["CtaBeneficiario"].Value = item.CtaBeneficiario;
                                row.Cells["CtaOrdenante"].Value = item.CtaOrdenante;
                                row.Cells["NomBancoOrdExt"].Value = item.NomBancoOrdExt;
                                row.Cells["NumOperacion"].Value = item.NumOperacion;
                                row.Cells["Monto"].Value = item.Monto;
                                row.Cells["TipoCadPago"].Value = item.TipoCadPago;
                                row.Cells["RfcEmisorCtaBen"].Value = item.RfcEmisorCtaBen;
                                row.Cells["RfcEmisorCtaOrd"].Value = item.RfcEmisorCtaOrd;
                                row.Tag = item;
                                //row.Cells["Data"].Value = item.Json();
                                e.SourceCollection.Add(row);
                            }
                        }
                    }
                }
            }
            else if (e.Template.Caption == this.GridComplementoDoctoRelacionado.Caption) // doctos relacionados
            {
                // documentos relacionados en el complemento de pagos 
                ComplementoPagosPago rowView = e.ParentRow.Tag as ComplementoPagosPago;
                if (!(rowView == null)) {
                    ComplementoPagosPago pagos10 = rowView;
                    if (!(pagos10 == null)) {
                        if (pagos10.DoctoRelacionado != null) {
                            foreach (ComplementoPagoDoctoRelacionado item in pagos10.DoctoRelacionado) {
                                GridViewRowInfo row = e.Template.Rows.NewRow();
                                row.Cells["Estado"].Value = item.Estado;
                                row.Cells["IdDocumento"].Value = item.IdDocumento;
                                row.Cells["Moneda"].Value = item.Moneda;
                                row.Cells["TipoCambio"].Value = item.TipoCambio;
                                row.Cells["MetodoDePago"].Value = item.MetodoPago;
                                row.Cells["NumParcialidad"].Value = item.NumParcialidad;
                                row.Cells["ImpSaldoAnterior"].Value = item.ImpSaldoAnt;
                                row.Cells["ImpPagado"].Value = item.ImpPagado;
                                row.Cells["ImpSaldoInsoluto"].Value = item.ImpSaldoInsoluto;
                                e.SourceCollection.Add(row);
                            }
                        }
                    }
                }
            }
            else if (e.Template.Caption == this.GridRecepcionPagos.Caption) {
                ViewModelComprobanteSingleC rowView = e.ParentRow.DataBoundItem as ViewModelComprobanteSingleC;
                List<ComplementoPagoDoctoRelacionado> tabla = this.data.Rep(rowView.IdDocumento).ToList();
                if (tabla != null) {
                    foreach (ComplementoPagoDoctoRelacionado item in tabla) {
                        GridViewRowInfo row = e.Template.Rows.NewRow();
                        row.Cells["IdDocumento"].Value = item.IdDocumento;
                        row.Cells["Folio"].Value = item.Folio;
                        row.Cells["Serie"].Value = item.Serie;
                        row.Cells["FechaEmision"].Value = item.FechaEmision;
                        row.Cells["Emisor"].Value = item.Nombre;
                        row.Cells["EmisorRFC"].Value = item.RFC;
                        row.Cells["SaldoAnterior"].Value = item.ImpSaldoAnt;
                        row.Cells["ImportePagado"].Value = item.ImpPagado;
                        row.Cells["SaldoInsoluto"].Value = item.ImpSaldoInsoluto;
                        row.Cells["Moneda"].Value = item.Moneda;
                        row.Cells["NumParcialidad"].Value = item.NumParcialidad;
                        row.Cells["MetodoPago"].Value = item.MetodoPago;
                        row.Cells["TipoCambio"].Value = item.TipoCambio;
                        e.SourceCollection.Add(row);
                    }
                }
            }
            else if (e.Template.Caption == this.GridNotasCredito.Caption) // Notas de credito relacionadas
            {
                // tipo de relacion de los comprobantes relacionados
                //DataRowView rowView = e.ParentRow.DataBoundItem as DataRowView;
                //List<Jaeger.CFDI.Entities.Base.ComplementoDoctoRelacionado> lista = this.data.Notas(Jaeger.Helpers.DbConvert.ConvertString(rowView.Row["_cfdi_uuid"]));
                //if (lista != null)
                //{
                //    if (lista.Count > 0)
                //    {
                //        foreach (Jaeger.CFDI.Entities.Base.ComplementoDoctoRelacionado item in lista)
                //        {
                //            GridViewRowInfo row = e.Template.Rows.NewRow();
                //            row.Cells["Clave"].Value = item.TipoRelacion.Clave;
                //            row.Cells["Descripcion"].Value = item.TipoRelacion.Descripcion;
                //            row.Cells["Folio"].Value = item.Folio;
                //            row.Cells["Serie"].Value = item.Serie;
                //            row.Cells["IdDocumento"].Value = item.IdDocumento;
                //            row.Cells["RFC"].Value = item.RFC;
                //            row.Cells["Nombre"].Value = item.Nombre;
                //            row.Cells["ImpTotal"].Value = item.Total;
                //            e.SourceCollection.Add(row);
                //        }
                //    }
                //}
            }

            Thread.Sleep(10);
        }

        #endregion

        #region metodos

        // verificar status del comprobante fiscal seleccionado
        private void QueryStateSAT() {
            if (this.GridData.CurrentRow != null) {
                ViewModelComprobanteSingleC o = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteSingleC;
                if (o != null) {
                    SatQueryResult response = HelperServiceQuerySAT.Query(o.EmisorRFC, o.ReceptorRFC, o.Total, o.IdDocumento);
                    if (response.Key != "E") {
                        response.Id = o.Id;
                        o.Estado = response.Status;
                        o.FechaEstado = DateTime.Now;
                        if (!this.data.Update(response)) {
                            Console.WriteLine("Error al consultar!");
                        }
                        this.GridData.Refresh();
                    }
                }
            }
        }

        private void FormatosCondicionales() {
            // se aplica solo para los comprobantes emitidos a partir del 01/09/2018, solo aquellos con metodo de pago PPD
            ExpressionFormattingObject saldo = new ExpressionFormattingObject("Saldo", "ImportesPagos<=0 AND MetodoPago = 'PPD' AND FechaEmision >= '2018/09/01' AND TipoComprobanteText = 'Ingreso'", false) { CellForeColor = System.Drawing.Color.Red };
            ExpressionFormattingObject saldoPagos = new ExpressionFormattingObject("Saldo de Pagos", "SaldoPagos<=0", false) { CellForeColor = System.Drawing.Color.Red };
            // regla para cambiar de color todos los comprobantes con un estado CANCELADO o NO ENCONTRADO
            ExpressionFormattingObject estado = new ExpressionFormattingObject("Estado del Comprobante", "Estado = 'NoEncontrado' OR Estado = 'Cancelado'", true) { RowForeColor = System.Drawing.Color.DarkGray };
            ExpressionFormattingObject vigente = new ExpressionFormattingObject("Vigente", "Estado = 'Vigente'", false) { CellForeColor = System.Drawing.Color.Green };
            this.GridData.Columns["ImportesPagos"].ConditionalFormattingObjectList.Add(saldo);
            this.GridData.Columns["SaldoPagos"].ConditionalFormattingObjectList.Add(saldoPagos);
            this.GridData.Columns["Estado"].ConditionalFormattingObjectList.Add(estado);
            this.GridData.Columns["Estado"].ConditionalFormattingObjectList.Add(vigente);
        }

        /// <summary>
        /// obtener la lista de folios fiscales seleccionados por el usuario
        /// </summary>
        private List<string> ComprobantesSeleccionados() {
            return this.GridData.SelectedRows.Where(x => x.IsSelected == true).Select(x => x.Cells["FolioFiscal"].Value.ToString()).ToList();
        }

        #endregion

        #region preparar

        private void Preparar_DoWork(object sender, DoWorkEventArgs e) {
            if (this.subTipoComprobante == EnumCfdiSubType.Emitido) {
                this.ToolBarReciboPago.Visibility = ElementVisibility.Collapsed;
                this.GridData.Columns["Emisor"].IsVisible = false;
                this.GridData.Columns["FechaUltimoPago"].HeaderText = "Ult. Cobro";
                this.GridData.Columns["FechaEntrega"].HeaderText = "Fecha Revisión";
                GridViewComboBoxColumn combo = this.GridData.Columns["Status"] as GridViewComboBoxColumn;
                this.statusAutorizado = new Status(string.Join(",", Enum.GetNames(typeof(EnumCfdiStatusIngreso))));
                combo.DataSource = this.statusAutorizado.Status1;
                this.GridData.MasterTemplate.Columns["FechaValidacion"].IsVisible = false;
                this.GridData.MasterTemplate.Columns["FechaValidacion"].VisibleInColumnChooser = false;
                this.MenuContextual.Items.Remove(this.ContextMenuReciboPago);
                this.GridRecepcionPagos.Columns["Emisor"].HeaderText = "Receptor";
                this.ToolBarButtonValidar.Items.Remove(this.ToolBarButtonPDFValidacion);
                this.ToolBarButtonValidar.Items.Remove(this.ToolBarButtonValidarSAT);
                this.ToolBarPrint.Items.Remove(this.PrintValidacion);
            }
            else if (this.subTipoComprobante == EnumCfdiSubType.Recibido) {
                this.ToolBarComprobanteNuevo.Visibility = ElementVisibility.Collapsed;
                this.ToolBarReciboCobro.Visibility = ElementVisibility.Collapsed;
                this.ToolBarButtonValidar.Items.Remove(this.ToolBarButtonValidarSAT);
                this.GridData.Columns["Receptor"].IsVisible = false;
                this.GridData.Columns["FechaUltimoPago"].HeaderText = "Ult. Pago";
                this.GridData.Columns["FechaEntrega"].HeaderText = "Fecha Recepción";
                GridViewComboBoxColumn combo = this.GridData.Columns["Status"] as GridViewComboBoxColumn;
                this.statusAutorizado = new Status(string.Join(",", Enum.GetNames(typeof(EnumCfdiStatusEgreso))));
                combo.DataSource = this.statusAutorizado.Status1;
                this.GridData.MasterTemplate.Columns["Cobrado"].HeaderText = "Pagado";
                this.ToolBarNew.Items.Remove(this.ToolBarComprobanteClonar);
                this.ToolBarNew.Items.Remove(this.ToolBarComprobanteNuevo);
                this.ToolBarNew.Items.Remove(this.ToolBarComprobantePago);
                this.ToolBarNew.Items.Remove(this.ToolBarComprobanteSustituir);
                this.MenuContextual.Items.Remove(this.ContextMenuCrearNotaCredito);
                this.MenuContextual.Items.Remove(this.ContextMenuReciboCobro);
                this.MenuContextual.Items.Remove(this.ContextMenuClonar);
                this.MenuContextual.Items.Remove(this.ContextMenuSustituir);
                this.MenuContextual.Items.Remove(this.ContextMenuCrearPago);
                this.ToolBar.Items.Remove(this.ToolBarButtonProductos);

            }

            foreach (string item in Enum.GetNames(typeof(EnumMonthsOfYear))) {
                RadMenuItem oButtonMonth = new RadMenuItem { Text = item };
                this.ToolBarPeriodo.Items.Add(oButtonMonth);
                oButtonMonth.Click += new EventHandler(this.ToolBarButtonPeriodo_Click);
            }

            for (int anio = 2013; anio <= DateTime.Now.Year; anio = checked(anio + 1)) {
                RadMenuItem oButtonYear = new RadMenuItem() { Text = anio.ToString() };
                this.ToolBarEjercicio.Items.Add(oButtonYear);
                oButtonYear.Click += new EventHandler(this.ToolBarButtonEjercicio_Click);
            }

            this.GridData.TelerikGridCommon();
            this.GridData.AllowEditRow = true;
            this.GridConceptos.Standard();
            this.GridFiles.Standard(); 
            this.GridComprobanteCfdiRelacionados.Standard();
            this.GridContable.Standard();
            this.GridComplementoPagos.Standard();
            this.GridComplementoDoctoRelacionado.Standard();
            this.GridComprobanteCfdiRelacionadosCfdiRelacionado.Standard();
            this.GridRecepcionPagos.Standard();
            this.GridNotasCredito.Standard();

            this.GridConceptos.HierarchyDataProvider = new GridViewEventDataProvider(this.GridConceptos);
            this.GridFiles.HierarchyDataProvider = new GridViewEventDataProvider(this.GridFiles);
            this.GridComprobanteCfdiRelacionados.HierarchyDataProvider = new GridViewEventDataProvider(this.GridComprobanteCfdiRelacionados);
            this.GridContable.HierarchyDataProvider = new GridViewEventDataProvider(this.GridContable);
            this.GridComplementoPagos.HierarchyDataProvider = new GridViewEventDataProvider(this.GridComplementoPagos);
            this.GridComplementoDoctoRelacionado.HierarchyDataProvider = new GridViewEventDataProvider(this.GridComplementoDoctoRelacionado);
            this.GridComprobanteCfdiRelacionadosCfdiRelacionado.HierarchyDataProvider = new GridViewEventDataProvider(this.GridComprobanteCfdiRelacionadosCfdiRelacionado);
            this.GridRecepcionPagos.HierarchyDataProvider = new GridViewEventDataProvider(this.GridRecepcionPagos);
            this.GridNotasCredito.HierarchyDataProvider = new GridViewEventDataProvider(this.GridNotasCredito);

            this.GridData.CellBeginEdit += GridData_CellBeginEdit;
            this.GridData.CellEndEdit += GridData_CellEndEdit;
            this.GridData.CellValidating += GridData_CellValidating;
            this.GridData.CellFormatting += GridData_CellFormatting;
            this.FormatosCondicionales();
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.ToolBarPeriodo.Text = Enum.GetName(typeof(EnumMonthsOfYear), DateTime.Now.Month);
            this.ToolBarEjercicio.Text = DateTime.Now.Year.ToString();

            /// preferencias
            this.archivoPreferencias = System.IO.Path.Combine(Application.StartupPath, string.Concat(this.Name, Enum.GetName(typeof(EnumCfdiSubType), this.subTipoComprobante), "_.xml"));
            this.GridData.TelerikGridLayOut(this.archivoPreferencias);
            this.ToolBarEdit.Enabled = false;
            this.ToolBarPrint.Enabled = false;
            this.data = new MySqlComprobanteFiscal(ConfigService.Synapsis.RDS.Edita);
            this.ToolBarRefresh.PerformClick();

            // unicamente para remover los status no autorizados
            if (!(ConfigService.Piloto.Clave.Contains("ANHE"))) {
                this.statusAutorizado.Status1.Remove("Cobrado");
                this.statusAutorizado.Status1.Remove("Pagado");
                this.statusAutorizado.Status1.Remove("Cancelado");
            }

            this.myFormaDePago33 = new FormaPagoCatalogo();
            this.myFormaDePago33.Load();
        }

        #endregion

        #region consulta de datos

        private void ConsultarDoWork(object sender, DoWorkEventArgs e) {
            this.ToolBar.Enabled = false;
            this.datos = this.data.GetListBy(this.subTipoComprobante, "Todos", (EnumMonthsOfYear)(Enum.Parse(typeof(EnumMonthsOfYear), this.ToolBarPeriodo.Text)), int.Parse(this.ToolBarEjercicio.Text));
        }

        private void ConsultarRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.GridData.DataSource = datos;
            this.Waiting.Visible = false;
            this.Waiting.StopWaiting();
            this.waitingElement.StopWaiting();
            this.waitingElement.Visibility = ElementVisibility.Collapsed;
            this.ToolBarRefresh.Enabled = true;
            this.ToolBar.Enabled = true;
            this.consultar.Dispose();
        }

        #endregion

        #region reporte

        private void Reporte1_DoWork(object sender, DoWorkEventArgs e) {
            Entities.Reportes.EstadoCuentaComprobantes data2 = this.data.Reporte(ConfigService.Synapsis.Empresa.RFC, subTipoComprobante, "Todos", (EnumMonthsOfYear)Enum.Parse(typeof(EnumMonthsOfYear), this.ToolBarPeriodo.Text), 2018, EnumCfdiDates.FechaEmision);
            Jaeger.Helpers.HelperExcelExport.RunSample(this.ToolBarButtonExportarExcelReporte1.Tag.ToString(), string.Concat("Datos_", DateTime.Now.ToString("mmss")), data2);
        }

        private void Reporte1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.ToolBarRefresh.Enabled = true;
            //this.ToolStatus.Enabled = true;
            this.Waiting.StopWaiting();
            this.Waiting.Visible = false;

            if (System.IO.File.Exists(this.ToolBarButtonExportarExcelReporte1.Tag.ToString())) {
                DialogResult dr = RadMessageBox.Show(this, String.Concat("Se creo correctamente el archivo. ", this.ToolBarButtonExportarExcelReporte1.Tag.ToString(), " ¿Quieres abrir el documento?"), "Exportar", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                if (dr == DialogResult.Yes) {
                    try {
                        System.Diagnostics.Process.Start(this.ToolBarButtonExportarExcelReporte1.Tag.ToString());
                    }
                    catch (Exception ex) {
                        RadMessageBox.Show(this, string.Concat("El archivo no se puede abrir en su sistema. ", " Error message: ", ex.Message), "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                    }
                }
            }
        }

        #endregion

        #region serializacion del comprobante

        //public async System.Threading.Tasks.Task<string> SerializarCfdiComplementos(string url)
        //{
        //    System.Diagnostics.Stopwatch tiempo = new System.Diagnostics.Stopwatch();
        //    tiempo.Start();
        //    string salida = await System.Threading.Tasks.Task.Run(() =>
        //    {
        //        byte[] xmlBytes = null;
        //        try
        //        {
        //            xmlBytes = Jaeger.Helpers.HelperArchivos.DownloadFile(url);
        //        }
        //        catch (Exception ex)
        //        {
        //            return "Error! al intentar serialiar el comprobante." + ex.Message;
        //        }
        //        if ((!(xmlBytes == null)))
        //        {
        //            CFDI.V33.Comprobante o33 = CFDI.V33.Comprobante.LoadBytes(xmlBytes);
        //            if ((o33 != null))
        //            {
        //                Jaeger.Helpers.HelperConvertidor c = new Jaeger.Helpers.HelperConvertidor();
        //                Jaeger.CFDI.Entities.Comprobante comun = c.Create(o33);

        //                if ((comun != null))
        //                {
        //                    // ' complemento de pagos
        //                    if ((comun.ComplementoPagos != null))
        //                    {
        //                        foreach (Jaeger.CFDI.Entities.ComplementoPagosPago pagoRef in comun.ComplementoPagos.Pago)
        //                        {
        //                            Entities.SAT.ClaveFormaPago forma = this.myFormaDePago33.Search(Regex.Replace(pagoRef.FormaDePagoP.Clave, @"[^\d]", ""));
        //                            if ((forma != null))
        //                                pagoRef.FormaDePagoP.Descripcion = forma.Descripcion;

        //                            if (!(pagoRef.DoctoRelacionado == null))
        //                            {
        //                                foreach (Jaeger.CFDI.Entities.Base.ComplementoPagoDoctoRelacionado doctoRef in pagoRef.DoctoRelacionado)
        //                                {
        //                                    Jaeger.CFDI.Entities.Base.ComprobanteGeneral info = this.data.GetComprobanteGeneral(doctoRef.IdDocumento);
        //                                    if (!(info == null))
        //                                    {
        //                                        doctoRef.Folio = info.Folio;
        //                                        doctoRef.Serie = info.Serie;
        //                                        doctoRef.Nombre = info.Emisor;
        //                                        doctoRef.RFC = info.EmisorRFC;
        //                                        doctoRef.Estado = EnumEdoPagoDoctoRel.Relacionado;
        //                                    }
        //                                    else
        //                                        doctoRef.Estado = EnumEdoPagoDoctoRel.NoEncontrado;
        //                                }
        //                            }
        //                        }
        //                        this.data.Update(comun.ComplementoPagos, comun.UUID);
        //                    }
        //                    else if ((comun.CfdiRelacionados != null))
        //                    {
        //                        // ' CFDI Relacionados al comprobante
        //                        if ((comun.CfdiRelacionados.CfdiRelacionado != null))
        //                        {
        //                            foreach (Jaeger.CFDI.Entities.ComprobanteCfdiRelacionadosCfdiRelacionado relaciondo in comun.CfdiRelacionados.CfdiRelacionado)
        //                            {
        //                                Jaeger.CFDI.Entities.Base.ComprobanteGeneral info = this.data.GetComprobanteGeneral(relaciondo.IdDocumento);
        //                                if ((info != null))
        //                                {
        //                                    relaciondo.FormaPago = info.ClaveFormaPago;
        //                                    relaciondo.Folio = info.Folio;
        //                                    relaciondo.Serie = info.Serie;
        //                                    relaciondo.Nombre = info.Emisor;
        //                                    relaciondo.RFC = info.EmisorRFC;
        //                                    relaciondo.EstadoSAT = info.Estado;
        //                                    relaciondo.FechaEmision = info.FechaEmision;
        //                                    relaciondo.MetodoPago = info.ClaveMetodoPago;
        //                                    relaciondo.Moneda = info.ClaveMoneda;
        //                                    relaciondo.SubTipo = info.SubTipoComprobante;
        //                                    relaciondo.TipoCambio = info.TipoCambio;
        //                                    relaciondo.Total = info.Total;
        //                                    relaciondo.Estado = EnumEdoPagoDoctoRel.Relacionado;
        //                                }
        //                                else
        //                                    relaciondo.Estado = EnumEdoPagoDoctoRel.NoEncontrado;
        //                            }
        //                            this.data.Update(comun.CfdiRelacionados, comun.UUID);
        //                        }
        //                    }
        //                }
        //                return string.Concat("Tarea terminada para: ", comun.UUID, " en : ", tiempo.Elapsed);
        //            }
        //        }
        //        return string.Concat("Sin archivo, en : ", tiempo.ElapsedMilliseconds);
        //    });

        //    return salida;
        //}

        #endregion

        private void Copiar() {
            ViewModelComprobanteSingleC indice = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteSingleC;
            this.seleccionado = this.data.GetComprobante(indice.Id);
            if (this.seleccionado != null) {
                this.seleccionado.Clonar();
                this.seleccionado.Status = "PorCobrar";
            }
        }

        /// <summary>
        /// crear copia de un comprobante creado desde edita
        /// </summary>
        private void CrearCopia_Afasa() {
            MessageBox.Show("Este método esta desabilitado porque solo era para urgencia de afasa");
            //ViewModelComprobanteSingleC indice = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteSingleC;
            //ViewModelComprobante modelComprobante = this.data.GetById(indice.Id);

            //modelComprobante.TimbreFiscal = null;
            //modelComprobante.Id = 0;
            //modelComprobante.Serie = null;
            //modelComprobante.Folio = null;
            //modelComprobante.UUID = null;
            //modelComprobante.NoCertificado = null;
            //modelComprobante.RfcProvCertif = null;
            //modelComprobante.FechaEmision = new DateTime(2019, 8, 31, 23, 0, 0);
            //modelComprobante.FechaNuevo = DateTime.Now;
            //modelComprobante.FechaCert = null;
            //modelComprobante.FileXml = null;
            //modelComprobante.FilePdf = null;
            //modelComprobante.FileAccuse = null;
            //modelComprobante.Status = "PorCobrar";
            //modelComprobante.Estado = "";
            //modelComprobante.FechaEstado = null;
            //modelComprobante.SubId = 10204;
            //modelComprobante.Documento = "factura";
            //modelComprobante.IdAddenda = 1;
            //modelComprobante.IdSerie = 1;
            //modelComprobante.Sync = false;

            //CFDI.V33.Comprobante comprobante = CFDI.V33.Comprobante.LoadXml(modelComprobante.Xml);
            //comprobante.Complemento = null;
            //comprobante.Certificado = null;
            //comprobante.Sello = null;
            //comprobante.NoCertificado = null;
            //comprobante.Serie = null;
            //comprobante.Folio = null;
            //comprobante.Fecha = new DateTime(2019, 8, 31, 23, 0, 0);

            //string xml = comprobante.Serialize();
            ////MessageBox.Show(xml);

            //var sip = TestingZip.Zip(xml);
            //modelComprobante.Xml = System.Convert.ToBase64String(sip);

            //this.data.Save(modelComprobante);
        }

        private void CrearRecibo() {
            SqlSugarContable contable = new SqlSugarContable(ConfigService.Synapsis.RDS.Edita);
            nuevo = contable.CrearRecibo(seleccion);
            nuevo.Creo = ConfigService.Piloto.Clave;
        }

        private void ActualizaStatus() {
            ViewModelComprobanteSingleC singleC = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteSingleC;
            if (singleC != null) {
                if (this.data.Update(singleC.Id, (string)this.GridData.CurrentRow.Tag, ConfigService.Piloto.Clave)) {
                    singleC.FechaMod = DateTime.Now;
                    singleC.FechaEntrega = DateTime.Now;
                    singleC.Modifica = ConfigService.Piloto.Clave;
                }
                this.GridData.CurrentRow.Tag = "";
            }
        }

        private void PDFValidacion() {
            var d = new HelperReport2PDF();
            for (int index = 0; index < this.seleccion.Count; index++) {
                if (this.seleccion[index].Validacion != null)
                    d.Crear(this.seleccion[index].Validacion, (string)ToolBarButtonPDFValidacion.Tag);
            }
        }

        private void ToolBarButtonMetodo1_Click(object sender, EventArgs e) {
            foreach (var item in this.datos) {
                if (HelperValidacion.ValidaUrl(item.UrlXml) == false) {

                }
            }
        }
    }
}
