﻿using System;
using System.ComponentModel;
using Telerik.WinControls.Enumerations;
using Jaeger.Nomina.Helpers;
using Jaeger.Helpers;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views.Nomina
{
    public partial class ViewNominaAguinaldo : Telerik.WinControls.UI.RadForm
    {
        private Jaeger.Nomina.Entities.Configuracion configuracion;
        private SqlSugarNominaConfiguracion conf;
        private SqlSugarEmpleados empleados;
        private SqlSugarSubsidioAlEmpleo subsidioAlEmpleo;
        private SqlSugarTablaImpuestoSobreRenta impuestoSobreRenta;
        private BindingList<NominaCalcularAguinaldo> calcular = new BindingList<NominaCalcularAguinaldo>();
        private HelperNominaCalcular calcular1;

        public ViewNominaAguinaldo()
        {
            InitializeComponent();
        }

        private void NominaAguinaldo_Load(object sender, EventArgs e)
        {
            this.GridData.TelerikGridCommon();
            this.conf = new SqlSugarNominaConfiguracion(ConfigService.Synapsis.RDS.Edita);
            this.empleados = new SqlSugarEmpleados(ConfigService.Synapsis.RDS.Edita);
            this.configuracion = this.conf.LoadConf();
            this.calcular1 = new HelperNominaCalcular(ConfigService.Synapsis.RDS.Edita);
        }

        private void ToolBarButtonCalcular_Click(object sender, EventArgs e)
        {
            this.GridData.DataSource = this.calcular1.CalcularAguinaldo();
        }

        private void ToolBarButtonFiltro_Click(object sender, EventArgs e)
        {
            this.GridData.ShowFilteringRow = this.ToolBarButtonFiltro.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false)
            {
                this.GridData.FilterDescriptors.Clear();
            }
        }

        private void ToolBarButtonExportar_Click(object sender, EventArgs e)
        {
            TelerikGridExport exportar = new TelerikGridExport(this.GridData);
            exportar.ShowDialog();
        }
    }
}
