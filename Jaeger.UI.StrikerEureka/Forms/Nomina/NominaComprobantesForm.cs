﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls.UI;
using Jaeger.Helpers;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2;
using System.Collections.Generic;
using Telerik.WinControls;
using Jaeger.Edita.V2.Nomina.Entities;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;

namespace Jaeger.Views.Nomina {
    public partial class NominaComprobantesForm : RadForm {
        private BackgroundWorker preparar;
        private BackgroundWorker consultar;
        private ISqlComprobanteNomina dataNomina;
        private List<NominaViewSingle> datos;

        public NominaComprobantesForm() {
            InitializeComponent();
        }

        private void NominaComprobantesForm_Load(object sender, EventArgs e) {
            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += Preparar_DoWork;
            this.preparar.RunWorkerCompleted += Preparar_RunWorkerCompleted;
            this.preparar.RunWorkerAsync();

            this.consultar = new BackgroundWorker();
            this.consultar.DoWork += ConsultarDoWork;
            this.consultar.RunWorkerCompleted += ConsultarRunWorkerCompleted;

            this.CheckPorNomina.ToggleStateChanged += CheckPorNomina_ToggleStateChanged;
            this.CheckPorDepartamento.ToggleStateChanged += CheckPorDepartamento_ToggleStateChanged;
            this.CheckPorEmpleado.ToggleStateChanged += CheckPorEmpleado_ToggleStateChanged;
        }

        #region preparar formulario

        private void Preparar_DoWork(object sender, DoWorkEventArgs e) {
            string[] vs = new string[] { "AAM1901216I3", "SOC190205N7A" };
            if (!vs.Contains(ConfigService.Synapsis.Empresa.RFC)) {
                this.ToolBarNominas.Items.Remove(this.ToolBarButtonHerramientas);
            }
            this.dataNomina = new MySqlComprobanteNomina(ConfigService.Synapsis.RDS.Edita);
            this.GridData.TelerikGridCommon();
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.DateFechaInicial.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            this.DateFechaFinal.Value = DateTime.Now;
        }

        #endregion

        #region consultar

        private void ConsultarDoWork(object sender, DoWorkEventArgs e) {
            if (this.CheckPorNomina.Checked) {
                this.datos = this.dataNomina.GetCfdNominas(DbConvert.ConvertInt32(this.CboNominas.SelectedValue), false);
            } else if (this.RadioButtonIdDocumento.IsChecked) {
                this.datos = this.dataNomina.GetCfdUuid(this.TxbIdDocumento.Text, false);
            } else if (this.RadioButtonRangoFechas.IsChecked) {
                this.datos = this.dataNomina.GetCfdNominaBy(EnumCfdiDates.FechaEmision, this.DateFechaInicial.Value, this.DateFechaFinal.Value, this.CboDepartamentos.Text, this.CboEmpleados.Text);
            } else if (this.CheckPorEmpleado.Checked) {
                this.datos = this.dataNomina.GetCfdNominasBy(this.CboEmpleados.Text);
            } else {
                this.datos = this.dataNomina.GetCfdNominasBy("%%");
            }
        }

        private void ConsultarRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.GridData.DataSource = datos;
            this.Espera.Visible = false;
            this.Espera.StopWaiting();
            this.BarraEstadoLabel.Text = "Listo.";
            this.ToolBarButtonActualizar.Enabled = true;
        }

        #endregion

        #region contoles de busqueda

        private void CheckPorNomina_ToggleStateChanged(object sender, StateChangedEventArgs args) {
            if (this.CheckPorNomina.CheckState == System.Windows.Forms.CheckState.Checked) {
                this.CboNominas.DataSource = this.dataNomina.GetNominas();
            } else {
                this.CboNominas.Enabled = false;
            }
        }

        private void CheckPorEmpleado_ToggleStateChanged(object sender, StateChangedEventArgs args) {
            this.CboEmpleados.Enabled = this.CheckPorEmpleado.Checked;
            if (CboDepartamentos.Enabled) {
                this.CboEmpleados.DataSource = this.dataNomina.GetNominaDeptos();
            } else {
                this.CboEmpleados.DataSource = this.dataNomina.GetEmpleados();
                this.CboEmpleados.DisplayMember = "_cfdi_nomr";
                this.CboEmpleados.ValueMember = "_cfdi_nomr";
                this.CboDepartamentos.Text = "";
            }

            if (this.CboEmpleados.Enabled == false) {
                this.CboEmpleados.Text = "";
            }
        }

        private void CheckPorDepartamento_ToggleStateChanged(object sender, StateChangedEventArgs args) {
            this.CboDepartamentos.Enabled = this.CheckPorDepartamento.Checked;
            if (this.CboDepartamentos.Enabled) {
                this.CboDepartamentos.DataSource = this.dataNomina.GetNominaDeptos();
            } else {
                this.CboDepartamentos.Text = "";
            }
        }

        private void ButtonBuscar_Click(object sender, EventArgs e) {
            this.ToolBarButtonActualizar.PerformClick();
        }

        #endregion

        #region barra de herramientas

        private void ToolBarButtonEditar_Click(object sender, EventArgs e) {

        }

        private void ToolBarButtonCancelar_Click(object sender, EventArgs e) {

        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            this.ToolBarButtonActualizar.Enabled = false;
            this.BarraEstadoLabel.Text = "Consultado...";
            this.Espera.Visible = true;
            this.Espera.StartWaiting();
            this.consultar.RunWorkerAsync();
        }

        private void ToolBarButtonFiltro_Click(object sender, EventArgs e) {
            this.GridData.ShowFilteringRow = this.ToolBarButtonFiltro.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false) {
                this.GridData.FilterDescriptors.Clear();
            }
        }

        private void ToolBarButtonExportar_Click(object sender, EventArgs e) {
            var exportar = new TelerikGridExport(this.GridData) { StartPosition = FormStartPosition.CenterParent };
            exportar.ShowDialog(this);
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ToolBarButtonDescargar_Click(object sender, EventArgs e) {
            FolderBrowserDialog folder = new FolderBrowserDialog() { Description = "Selecciona la carpeta de trabajo para" };
            if (folder.ShowDialog() != DialogResult.OK)
                return;
            this.ToolBarButtonDescargar.Tag = folder.SelectedPath;
            using (Waiting2Form espera = new Waiting2Form(this.DescargaBackup)) {
                espera.Text = "Descargando ...";
                espera.ShowDialog();
            }
        }

        private void ToolBarButtonPoliza_Click(object sender, EventArgs e) {
            var openFileDialog = new SaveFileDialog() { Title = "Selecciona destino", FileName = "PrePoliza.csv", Filter = "*.csv|*.CSV", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
            if (openFileDialog.ShowDialog() != DialogResult.OK)
                return;
            this.ToolBarButtonHerramientas.Tag = openFileDialog.FileName;
            using (var espera = new Waiting2Form(this.Poliza1)) {
                espera.Text = "Procesando ...";
                espera.ShowDialog(this);
            }

            if (!File.Exists((string)this.ToolBarButtonHerramientas.Tag))
                RadMessageBox.Show(this, "No es posible crear el archivo de salida. Es posible que el archivo se encuentre bloqueado.", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
        }

        private void ToolBarButtonPoliza2_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog { Title = "Selecciona el archivo de los beneficiarios.", Filter = "*.xlsx|*.XLSX|*.xls|*.XLS" };
            if (openFile.ShowDialog(this) == DialogResult.Cancel)
                return;
            //var d = new Layout.Helpers.LayoutExcelBeneficiario();
            //d.InfoFile = new FileInfo(openFile.FileName);
            //d.Reader();
            var s = new Jaeger.Aplication.Contabilidad.PolizaCOIService();
            //var beneficiarios = d.Reader("Importar");
            //Layout.Entities.PolizaCOi poliza = new Layout.Entities.PolizaCOi();
            //poliza.TipoPoliza = "Eg";
            //poliza.Concepto = "Pago de Incentivos a Vendedores (Asimilables a Sueldos) 18-Noviembre-2020";

            //if (this.datos == null)
            //    return;

            //foreach (var item in this.datos) {
            //    var buscar = beneficiarios.First(it => it.RFC == item.ReceptorRFC);
            //    if (buscar != null) {
            //        var p = new Jaeger.Layout.Entities.PolizaCuentaCOi();
            //        p.Concepto = buscar.Nombre;
            //        p.Cuenta = buscar.CuentaContable;
            //        p.Cargo = item.Total;
            //        p.Abono = 0;
            //        p.InfoPago = null;
            //        p.Comprobantes.Add(new Layout.Entities.PolizaComprobanteCOi {
            //            EmisorRFC = item.EmisorRFC, 
            //            Descuento = item.Descuento,
            //            Fecha = item.FechaPago.ToString(),
            //            IdDocumento = item.IdDocumento,
            //            Receptor = item.ReceptorNombre,
            //            ReceptorRFC = item.ReceptorRFC,
            //            Total = item.Total,
            //            SubTotal = item.SubTotal
            //        });

            //        var p1 = new Jaeger.Layout.Entities.PolizaCuentaCOi();
            //        p1.Concepto = buscar.Nombre;
            //        p1.Cuenta = "1102-0001-0006-0000";
            //        p1.Cargo = 0;
            //        p1.Abono = item.Total;
            //        p1.Comprobantes.Add(new Layout.Entities.PolizaComprobanteCOi {
            //            EmisorRFC = item.EmisorRFC,
            //            Descuento = item.Descuento,
            //            Fecha = item.FechaPago.ToString(),
            //            IdDocumento = item.IdDocumento,
            //            Receptor = item.ReceptorNombre,
            //            ReceptorRFC = item.ReceptorRFC,
            //            Total = item.Total,
            //            SubTotal = item.SubTotal
            //        });
            //        p1.InfoPago = new Layout.Entities.PolizaInfoPago { BancoOrigen = buscar.ClaveBancoOrigen, CuentaOrigen = buscar.CuentaBancoOrigen, FormaPago = buscar.ClaveFormaPago, RFC = buscar.RFC, Beneficiario = buscar.Nombre, Fecha = (DateTime)item.FechaPago, Monto = item.Total, NumCheque = "", CuentaDestino = buscar.NumCuenta, BancoDestino = buscar.ClaveBanco };
            //        poliza.Cuentas.Add(p);
            //        poliza.Cuentas.Add(p1);
            //    }
            //}
            var f = new Layout.COI.LayoutExcelCOI();
            f.Exportar(s.PolizaIncentivos(this.CboNominas.Text, this.datos, openFile.FileName), @"C:\Jaeger\Jaeger.Temporal\polizacoiT.xlsx");
        }

        private void ToolBarButtonValidarEstado_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.QueryStateSAT)) {
                espera.Text = "Consultando estado SAT, esperando al servidor ...";
                espera.ShowDialog(this);
            }
        }

        #endregion

        #region acciones del grid

        private void GridData_Click(object sender, EventArgs e) {

        }

        private void GridData_CellFormatting(object sender, CellFormattingEventArgs e) {
            if (e.Column.Name == "FileXML" || e.Column.Name == "FilePDF") {
                if (DbConvert.ConvertString(e.CellElement.Value) != "") {
                    e.CellElement.Image = Imagenes.Images[e.Column.Name];
                    e.CellElement.DrawText = false;
                } else {
                    e.CellElement.Image = null;
                    e.CellElement.DrawText = true;
                    e.CellElement.Children.Clear();
                }
            } else if (e.Column.Name != "Status") {
                // aqui tenemos que agregar las columnas que no debemos limpiar para evitar borrar controles adicionales
                e.CellElement.Image = null;
                e.CellElement.DrawText = true;
                e.CellElement.Children.Clear();
            }
        }

        #endregion

        private void DescargaBackup() {
            if (this.GridData.Rows.Count > 0) {
                var root = (string)this.ToolBarButtonDescargar.Tag;
                BindingList<Util.Helpers.DescargaElemento> descargas = new BindingList<Util.Helpers.DescargaElemento>();
                foreach (var fila in this.GridData.Rows) {
                    if (DbConvert.ConvertString(fila.Cells["IdDocumento"].Value) != "") {
                        string nuevaCarpeta = root;
                        string depto = DbConvert.ConvertString(fila.Cells["Departamento"].Value);
                        if (depto.Length > 0)
                            nuevaCarpeta = System.IO.Path.Combine(root, depto);

                        if (Directory.Exists(nuevaCarpeta) == false) {
                            try {
                                Directory.CreateDirectory(nuevaCarpeta);
                            } catch (Exception ex) {
                                Console.WriteLine(ex.Message);
                                nuevaCarpeta = root;
                            }
                        }

                        // archivo XML
                        if (HelperValidacion.ValidaUrl(DbConvert.ConvertString(fila.Cells["FileXML"].Value)))
                            descargas.Add(new Util.Helpers.DescargaElemento { URL = DbConvert.ConvertString(fila.Cells["FileXML"].Value), KeyName = Path.Combine(nuevaCarpeta, Path.GetFileName(DbConvert.ConvertString(fila.Cells["FileXML"].Value))) });
                        // archivo PDF
                        if (HelperValidacion.ValidaUrl(DbConvert.ConvertString(fila.Cells["FilePDF"].Value)))
                            descargas.Add(new Util.Helpers.DescargaElemento { URL = DbConvert.ConvertString(fila.Cells["FilePDF"].Value), KeyName = Path.Combine(nuevaCarpeta, Path.GetFileName(DbConvert.ConvertString(fila.Cells["FilePDF"].Value))) });

                    }
                }
                // descarga
                Util.Helpers.DownloadExtended descarga = new Util.Helpers.DownloadExtended();
                descarga.ProcessDownload += Descarga_ProcessDownload;
                descarga.RunDownloadParallelSync(descargas, @"");
            }
        }

        private void Descarga_ProcessDownload(object sender, Util.Helpers.DownloadChanged e) {
            this.BarraEstadoLabel.Text = e.Caption;
        }

        private void Poliza1() {
            var comprobantes = new List<Layout.Entities.PolizaComprobanteCOi>();
            List<Layout.Entities.RF.Partida2> resultado = new List<Layout.Entities.RF.Partida2>();

            foreach (var item in datos)
                comprobantes.Add(new Layout.Entities.PolizaComprobanteCOi { EmisorRFC = item.EmisorRFC, ReceptorRFC = item.ReceptorRFC, Receptor = item.ReceptorNombre, IdDocumento = item.IdDocumento, SubTotal = item.SubTotal, Descuento = item.Descuento, Total = item.Total, Fecha = (item.FechaCertificacion != null ? item.FechaCertificacion.Value.ToString("dd/mm/yyyy") : null) });

            var inicioCFDI = new Layout.Entities.RF.Partida2() { Campo3 = "INICIO_CFDI" };
            var finCFDI = new Layout.Entities.RF.Partida2() { Campo3 = "FIN_CFDI" };
            var finPartidas = new Layout.Entities.RF.Partida2() { Campo2 = "FIN_PARTIDAS" };
            var descripcion = new Layout.Entities.RF.Partida2() { Campo1 = "Dr", Campo2 = "19", Campo3 = "ASIMILADOS A SALARIO 04101 1237" };
            var cuenta1 = new Layout.Entities.RF.Partida2() { Campo2 = "5101-0001-0001-0000", Campo3 = "0", Campo4 = "ASIMILADOS A SALARIO 04101 1237", Campo5 = "1.00", Campo6 = comprobantes.Sum(it => it.SubTotal).ToString(), Campo7 = "0" };

            resultado.Add(descripcion);
            resultado.Add(cuenta1);

            resultado.Add(inicioCFDI);
            foreach (var item in comprobantes) {
                resultado.Add(new Layout.Entities.RF.Partida2 { Campo3 = item.Fecha, Campo6 = item.EmisorRFC, Campo7 = item.ReceptorRFC, Campo8 = item.Total.ToString(), Campo9 = item.IdDocumento });
            }
            resultado.Add(finCFDI);

            foreach (var item in comprobantes) {
                if (ConfigService.Synapsis.Empresa.RFC == "SOC190205N7A") {
                    resultado.Add(new Layout.Entities.RF.Partida2 { Campo2 = "2102-0002-0003-0000", Campo3 = "0", Campo4 = item.Receptor, Campo5 = "1.00", Campo7 = item.Descuento.ToString() });
                } else if (ConfigService.Synapsis.Empresa.RFC == "AAM1901216I3") {
                    resultado.Add(new Layout.Entities.RF.Partida2 { Campo2 = "2102-0002-0003-0000", Campo3 = "0", Campo4 = item.Receptor, Campo5 = "1.00", Campo7 = item.Descuento.ToString() });
                }
                resultado.Add(inicioCFDI);
                resultado.Add(new Layout.Entities.RF.Partida2 { Campo3 = item.Fecha, Campo6 = item.EmisorRFC, Campo7 = item.ReceptorRFC, Campo8 = item.Total.ToString(), Campo9 = item.IdDocumento.Trim() });
                resultado.Add(finCFDI);
            }

            resultado.Add(new Layout.Entities.RF.Partida2 { Campo2 = "2101-0001-0009-0000", Campo3 = "0", Campo4 = "ASIMILADOS A SALARIO 04101 1237", Campo5 = "1.00", Campo7 = comprobantes.Sum(it => it.Total).ToString() });
            resultado.Add(inicioCFDI);
            foreach (var item in comprobantes) {
                resultado.Add(new Layout.Entities.RF.Partida2 { Campo3 = item.Fecha, Campo6 = item.EmisorRFC, Campo7 = item.ReceptorRFC, Campo8 = item.Total.ToString(), Campo9 = item.IdDocumento });
            }
            resultado.Add(finCFDI);

            resultado.Add(finPartidas);
            var prueba = new Layout.Helpers.HelperRecibosFiscales();
            prueba.Exportar(resultado.ToArray(), (string)this.ToolBarButtonHerramientas.Tag);

        }

        private void QueryStateSAT() {
            //if (this.GridData.CurrentRow != null) {
            //    NominaViewSingle o = this.GridData.CurrentRow.DataBoundItem as NominaViewSingle;
            //    if (o != null) {
            //        SatQueryResult response = HelperServiceQuerySAT.Query(o.EmisorRFC, o.ReceptorRFC, o.Total, o.IdDocumento);
            //        if (response.Key != "E") {

            //            this.GridData.Refresh();
            //        }
            //    }
            //}

            for (int i = 0; i < datos.Count; i++) {
                var response = HelperServiceQuerySAT.Query(this.datos[i].EmisorRFC, this.datos[i].ReceptorRFC, this.datos[i].Total, this.datos[i].IdDocumento);
                if (response.Key != "E") {
                    response.Id = this.datos[i].IdComprobante;
                    this.dataNomina.Update(response);
                }
            }
            this.GridData.Refresh();
        }

        private void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (!(this.GridData.CurrentRow == null)) {
                if (!(e.Column == null)) {
                    if (e.Column.Name == "FileXML" || e.Column.Name == "FilePDF") {
                        var single = e.Row.DataBoundItem as NominaViewSingle;
                        if (single != null) {
                            string liga = "";
                            if (e.Column.Name == "FileXML")
                                liga = single.FileXML;
                            else
                                if (e.Column.Name == "FilePDF")
                                liga = single.FilePDF;
                            // validar la liga de descarga
                            if (HelperValidacion.ValidaUrl(liga)) {
                                var savefiledialog = new SaveFileDialog {
                                    FileName = System.IO.Path.GetFileName(liga)
                                };

                                if (savefiledialog.ShowDialog(this) != DialogResult.OK)
                                    return;
                                if (Util.Helpers.HelperFiles.DownloadFile(liga, savefiledialog.FileName)) {
                                    DialogResult dr = RadMessageBox.Show(this, String.Concat("Se descargo correctamente el archivo. ", savefiledialog.FileName, " ¿Quieres abrir el documento?"), "Descarga", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                                    if (dr == DialogResult.Yes) {
                                        try {
                                            System.Diagnostics.Process.Start(savefiledialog.FileName);
                                        } catch (Exception ex) {
                                            string message = string.Format("El archivo no se puede abrir en su sistema.\nError message: {0}", ex.Message);
                                            RadMessageBox.Show(this, message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
