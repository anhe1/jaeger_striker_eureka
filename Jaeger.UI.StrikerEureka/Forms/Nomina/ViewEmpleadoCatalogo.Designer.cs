﻿namespace Jaeger.Views.Nomina
{
    partial class ViewEmpleadoCatalogo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.CommandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.CommandBarRow = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarNew = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonEditar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.CommandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarButtonRefresh = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarButtonRefreshTodos = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarPrint = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarPrintExpediente = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarPrintListado = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarFilter = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarExport = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonHerramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonCrearTabla = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonImportar = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarClose = new Telerik.WinControls.UI.CommandBarButton();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            this.GridData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandBar
            // 
            this.CommandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBar.Location = new System.Drawing.Point(0, 0);
            this.CommandBar.Name = "CommandBar";
            this.CommandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRow});
            this.CommandBar.Size = new System.Drawing.Size(1316, 63);
            this.CommandBar.TabIndex = 10;
            // 
            // CommandBarRow
            // 
            this.CommandBarRow.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRow.Name = "CommandBarRow";
            this.CommandBarRow.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            this.CommandBarRow.Text = "";
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "CommandBarStripElement1";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarNew,
            this.ToolBarButtonEditar,
            this.ToolBarDelete,
            this.CommandBarSeparator1,
            this.ToolBarButtonRefresh,
            this.ToolBarPrint,
            this.ToolBarFilter,
            this.ToolBarExport,
            this.ToolBarButtonHerramientas,
            this.ToolBarClose});
            this.ToolBar.Name = "ToolBar";
            // 
            // ToolBarNew
            // 
            this.ToolBarNew.DisplayName = "Nuevo";
            this.ToolBarNew.DrawText = true;
            this.ToolBarNew.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_add_user_male;
            this.ToolBarNew.Name = "ToolBarNew";
            this.ToolBarNew.Text = "Nuevo";
            this.ToolBarNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarNew.Click += new System.EventHandler(this.ToolBarNew_Click);
            // 
            // ToolBarButtonEditar
            // 
            this.ToolBarButtonEditar.DisplayName = "Editar";
            this.ToolBarButtonEditar.DrawText = true;
            this.ToolBarButtonEditar.Enabled = false;
            this.ToolBarButtonEditar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_edit_user_male;
            this.ToolBarButtonEditar.Name = "ToolBarButtonEditar";
            this.ToolBarButtonEditar.Text = "Editar";
            this.ToolBarButtonEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonEditar.Click += new System.EventHandler(this.ToolBarButtonEditar_Click);
            // 
            // ToolBarDelete
            // 
            this.ToolBarDelete.DisplayName = "Eliminar";
            this.ToolBarDelete.DrawText = true;
            this.ToolBarDelete.Enabled = false;
            this.ToolBarDelete.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_remove_user_male;
            this.ToolBarDelete.Name = "ToolBarDelete";
            this.ToolBarDelete.Text = "Eliminar";
            this.ToolBarDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarDelete.Click += new System.EventHandler(this.ToolBarDelete_Click);
            // 
            // CommandBarSeparator1
            // 
            this.CommandBarSeparator1.DisplayName = "Separador 1";
            this.CommandBarSeparator1.Name = "CommandBarSeparator1";
            this.CommandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarButtonRefresh
            // 
            this.ToolBarButtonRefresh.DefaultItem = null;
            this.ToolBarButtonRefresh.DisplayName = "Actualizar";
            this.ToolBarButtonRefresh.DrawText = true;
            this.ToolBarButtonRefresh.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_actualizar;
            this.ToolBarButtonRefresh.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonRefreshTodos});
            this.ToolBarButtonRefresh.Name = "ToolBarButtonRefresh";
            this.ToolBarButtonRefresh.Text = "Actualizar";
            this.ToolBarButtonRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonRefresh.Click += new System.EventHandler(this.ToolBarRefresh_Click);
            // 
            // ToolBarButtonRefreshTodos
            // 
            this.ToolBarButtonRefreshTodos.Name = "ToolBarButtonRefreshTodos";
            this.ToolBarButtonRefreshTodos.Text = "Todos";
            this.ToolBarButtonRefreshTodos.Click += new System.EventHandler(this.ToolBarButtonRefreshTodos_Click);
            // 
            // ToolBarPrint
            // 
            this.ToolBarPrint.DisplayName = "Imprimir";
            this.ToolBarPrint.DrawText = true;
            this.ToolBarPrint.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_imprimir;
            this.ToolBarPrint.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarPrintExpediente,
            this.ToolBarPrintListado});
            this.ToolBarPrint.Name = "ToolBarPrint";
            this.ToolBarPrint.Text = "Imprimir";
            this.ToolBarPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarPrintExpediente
            // 
            this.ToolBarPrintExpediente.Name = "ToolBarPrintExpediente";
            this.ToolBarPrintExpediente.Text = "Expediente";
            this.ToolBarPrintExpediente.Click += new System.EventHandler(this.ToolBarPrintExpediente_Click);
            // 
            // ToolBarPrintListado
            // 
            this.ToolBarPrintListado.Name = "ToolBarPrintListado";
            this.ToolBarPrintListado.Text = "Listado";
            this.ToolBarPrintListado.Click += new System.EventHandler(this.ToolBarPrintListado_Click);
            // 
            // ToolBarFilter
            // 
            this.ToolBarFilter.DisplayName = "Filtrado";
            this.ToolBarFilter.DrawText = true;
            this.ToolBarFilter.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_filtrar;
            this.ToolBarFilter.Name = "ToolBarFilter";
            this.ToolBarFilter.Text = "Filtro";
            this.ToolBarFilter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarFilter.Click += new System.EventHandler(this.ToolBarFilter_Click);
            // 
            // ToolBarExport
            // 
            this.ToolBarExport.DisplayName = "Exportar";
            this.ToolBarExport.DrawText = true;
            this.ToolBarExport.Enabled = false;
            this.ToolBarExport.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_excel;
            this.ToolBarExport.Name = "ToolBarExport";
            this.ToolBarExport.Text = "Exportar";
            this.ToolBarExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarExport.Click += new System.EventHandler(this.ToolBarExport_Click);
            // 
            // ToolBarButtonHerramientas
            // 
            this.ToolBarButtonHerramientas.DisplayName = "Herramientas";
            this.ToolBarButtonHerramientas.DrawText = true;
            this.ToolBarButtonHerramientas.Enabled = false;
            this.ToolBarButtonHerramientas.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_configuracion;
            this.ToolBarButtonHerramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonCrearTabla,
            this.ToolBarButtonImportar});
            this.ToolBarButtonHerramientas.Name = "ToolBarButtonHerramientas";
            this.ToolBarButtonHerramientas.Text = "Herramientas";
            this.ToolBarButtonHerramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonCrearTabla
            // 
            this.ToolBarButtonCrearTabla.Name = "ToolBarButtonCrearTabla";
            this.ToolBarButtonCrearTabla.Text = "Crear tabla";
            this.ToolBarButtonCrearTabla.Click += new System.EventHandler(this.ToolBarButtonCrear_Click);
            // 
            // ToolBarButtonImportar
            // 
            this.ToolBarButtonImportar.Name = "ToolBarButtonImportar";
            this.ToolBarButtonImportar.Text = "Importar";
            this.ToolBarButtonImportar.Click += new System.EventHandler(this.ToolBarImport_Click);
            // 
            // ToolBarClose
            // 
            this.ToolBarClose.DisplayName = "Cerrar";
            this.ToolBarClose.DrawText = true;
            this.ToolBarClose.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_cerrar;
            this.ToolBarClose.Name = "ToolBarClose";
            this.ToolBarClose.Text = "Cerrar";
            this.ToolBarClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarClose.Click += new System.EventHandler(this.ToolBarClose_Click);
            // 
            // GridData
            // 
            this.GridData.Controls.Add(this.Espera);
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 63);
            // 
            // 
            // 
            this.GridData.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdEmpleado";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "Activo";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewCheckBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "Num";
            gridViewTextBoxColumn2.FormatString = "{0:0000#}";
            gridViewTextBoxColumn2.HeaderText = "Núm. ";
            gridViewTextBoxColumn2.Name = "NumEmpleado";
            gridViewTextBoxColumn3.Expression = "";
            gridViewTextBoxColumn3.FieldName = "Clave";
            gridViewTextBoxColumn3.HeaderText = "Clave";
            gridViewTextBoxColumn3.Name = "Clave";
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn4.FieldName = "PrimerApellido";
            gridViewTextBoxColumn4.HeaderText = "A. Paterno";
            gridViewTextBoxColumn4.Name = "PrimerApellido";
            gridViewTextBoxColumn4.Width = 140;
            gridViewTextBoxColumn5.FieldName = "SegundoApellido";
            gridViewTextBoxColumn5.HeaderText = "A. Materno";
            gridViewTextBoxColumn5.Name = "SegundoApellido";
            gridViewTextBoxColumn5.Width = 140;
            gridViewTextBoxColumn6.FieldName = "Nombre";
            gridViewTextBoxColumn6.HeaderText = "Nombre";
            gridViewTextBoxColumn6.Name = "Nombre";
            gridViewTextBoxColumn6.Width = 140;
            gridViewTextBoxColumn7.FieldName = "RFC";
            gridViewTextBoxColumn7.HeaderText = "RFC";
            gridViewTextBoxColumn7.Name = "rfc";
            gridViewTextBoxColumn7.Width = 115;
            gridViewTextBoxColumn8.FieldName = "CURP";
            gridViewTextBoxColumn8.HeaderText = "CURP";
            gridViewTextBoxColumn8.Name = "Curp";
            gridViewTextBoxColumn8.Width = 135;
            gridViewTextBoxColumn9.FieldName = "Departamento";
            gridViewTextBoxColumn9.HeaderText = "Departamento";
            gridViewTextBoxColumn9.Name = "Departamento";
            gridViewTextBoxColumn9.Width = 180;
            gridViewTextBoxColumn10.FieldName = "Puesto";
            gridViewTextBoxColumn10.HeaderText = "Puesto";
            gridViewTextBoxColumn10.Name = "Puesto";
            gridViewTextBoxColumn10.Width = 120;
            gridViewTextBoxColumn11.FieldName = "Correo";
            gridViewTextBoxColumn11.HeaderText = "Correo";
            gridViewTextBoxColumn11.Name = "Correo";
            gridViewTextBoxColumn11.Width = 145;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1316, 639);
            this.GridData.TabIndex = 11;
            this.GridData.RowsChanged += new Telerik.WinControls.UI.GridViewCollectionChangedEventHandler(this.GridData_RowsChanged);
            // 
            // Espera
            // 
            this.Espera.AssociatedControl = this.GridData;
            this.Espera.Location = new System.Drawing.Point(555, 279);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(130, 24);
            this.Espera.TabIndex = 1;
            this.Espera.Text = "radWaitingBar1";
            this.Espera.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 80;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            ((Telerik.WinControls.UI.RadWaitingBarElement)(this.Espera.GetChildAt(0))).WaitingSpeed = 80;
            ((Telerik.WinControls.UI.WaitingBarContentElement)(this.Espera.GetChildAt(0).GetChildAt(0))).WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            ((Telerik.WinControls.UI.WaitingBarSeparatorElement)(this.Espera.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Dash = false;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            // 
            // ViewEmpleadoCatalogo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1316, 702);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.CommandBar);
            this.Name = "ViewEmpleadoCatalogo";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Recursos Humanos: Expediente";
            this.Load += new System.EventHandler(this.CatalogoEmpleados_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.GridData.ResumeLayout(false);
            this.GridData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Telerik.WinControls.UI.RadCommandBar CommandBar;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRow;
        internal Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarNew;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarButtonEditar;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarDelete;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator1;
        internal Telerik.WinControls.UI.CommandBarToggleButton ToolBarFilter;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarExport;
        internal Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarPrint;
        private Telerik.WinControls.UI.RadMenuItem ToolBarPrintExpediente;
        private Telerik.WinControls.UI.RadMenuItem ToolBarPrintListado;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonRefresh;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonRefreshTodos;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonHerramientas;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonCrearTabla;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonImportar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarClose;
    }
}
