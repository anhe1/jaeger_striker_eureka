﻿using System;
using Jaeger.Nomina.Entities;
using Jaeger.Nomina.Helpers;
using Jaeger.Helpers;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views.Nomina
{
    public partial class NominaAbrir : Telerik.WinControls.UI.RadForm
    {
        private Jaeger.Nomina.Entities.Configuracion configuracion;
        private HelperNominaCalcular nomina;
        private ViewModelNominaPeriodo seleccionada;

        public NominaAbrir()
        {
            InitializeComponent();
        }

        private void NominaAbrir_Load(object sender, EventArgs e)
        {
            this.GridData.TelerikGridCommon();
            this.nomina = new HelperNominaCalcular(ref this.configuracion, ConfigService.Synapsis.RDS.Edita);
            this.seleccionada = null;
            this.ToolBarButtonActualizar.PerformClick();
        }

        private void ToolBarButtonAbrir_Click(object sender, EventArgs e)
        {
            if (this.GridData.CurrentRow != null)
            {
                ViewModelNominaPeriodo t = this.GridData.CurrentRow.DataBoundItem as ViewModelNominaPeriodo;
                if (t != null)
                {
                    seleccionada = t;
                    this.Close();
                }
            }
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e)
        {
            this.GridData.DataSource = this.nomina.GetPeriodos();
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public ViewModelNominaPeriodo FormShow()
        {
            this.ShowDialog();
            if (this.seleccionada != null)
            {
                return this.seleccionada;
            }
            return null;
        }
    }
}
