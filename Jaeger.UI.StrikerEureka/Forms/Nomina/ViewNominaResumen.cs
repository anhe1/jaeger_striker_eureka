﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Edita.V2;
using Jaeger.UI.Common.Forms;
using Jaeger.Edita.Enums;
using Telerik.WinControls.UI;
using Telerik.Pivot.Core;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views.Nomina {
    public partial class ViewNominaResumen : RadForm {
        private SqlSugarComprobanteNomina data;

        public ViewNominaResumen() {
            InitializeComponent();
        }

        private void ViewNominaResumen_Load(object sender, EventArgs e) {
            this.data = new SqlSugarComprobanteNomina(ConfigService.Synapsis.RDS.Edita);
            foreach (string item in Enum.GetNames(typeof(EnumMonthsOfYear))) {
                RadMenuItem oButtonMonth = new RadMenuItem { Text = item };
                this.ToolBarButtonPeriodo.Items.Add(oButtonMonth);
                oButtonMonth.Click += new EventHandler(this.ToolBarButtonPeriodo_Click);
            }

            radSpinEditor1.Maximum = DateTime.Now.Year;
            radSpinEditor1.Value = DateTime.Now.Year;
            this.ToolBarButtonPeriodo.Text = Enum.GetName(typeof(EnumMonthsOfYear), DateTime.Now.Month);
            this.CreateDefault();
        }

        private void ToolBarButtonPeriodo_Click(object sender, EventArgs e) {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem) {
                this.ToolBarButtonPeriodo.DefaultItem = button;
                this.ToolBarButtonPeriodo.Text = button.Text;
                this.ToolBarButtonPeriodo.Tag = button.Tag;
            }
        }

        private void ToolBarButtonEjercicio_Click(object sender, EventArgs e) {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem) {
                //this.ToolBarButtonEjercicio.DefaultItem = button;
                //this.ToolBarButtonEjercicio.Text = button.Text;
                //this.ToolBarButtonEjercicio.PerformClick();
            }
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            using (Waiting2Form espera = new Waiting2Form(this.Consultar)) {
                espera.Text = "Solicitando datos al servidor";
                espera.ShowDialog(this);
            }
        }

        private void ToolBarButtonExportar_Click(object sender, EventArgs e) {
            SaveFileDialog saveFileDialog = new SaveFileDialog() {
                Filter = "Excel ML|*.xlsx",
                Title = "Export to File"
            };
            if (saveFileDialog.ShowDialog() == DialogResult.OK) {
                bool correcto = HelperTelerikExport.RunExportPivotGrid(this.GridData, saveFileDialog.FileName, "Resumen");
                if (correcto) {
                    if (RadMessageBox.Show("Se exporto correctamente. ¿Quieres abrir el documento?", "Exportar", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                        try {
                            System.Diagnostics.Process.Start(saveFileDialog.FileName);
                        }
                        catch (Exception ex) {
                            string message = string.Format("El archivo no se puede abrir en su sistema. Error message: {0}", ex.Message);
                            RadMessageBox.Show(message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                        }
                    }
                    else {
                        RadMessageBox.Show("No fué posible exportar la información al formato solicitado.", "Exportar", MessageBoxButtons.OK, RadMessageIcon.Error);
                    }
                }
            }
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consultar() {
            if (this.radioPorPeriodo.IsChecked)
                this.GridData.DataSource = this.data.GetResumen(int.Parse(this.radSpinEditor1.Text), Edita.V2.CFDI.Enums.EnumCfdiEstado.Todos, (EnumMonthsOfYear)(Enum.Parse(typeof(EnumMonthsOfYear), this.ToolBarButtonPeriodo.Text)));
            else if (this.radioPorNomina.IsChecked)
                this.GridData.DataSource = this.data.GetResumen(int.Parse(this.cboNominas.SelectedValue.ToString()));
        }

        private void CreateDefault() {
            //configuracion de la tabla pivote
            var m1 = new SumAggregateFunction();

            var pConcepto = new PropertyGroupDescription() {
                GroupComparer = new GroupNameComparer(),
                CustomName = "Concepto",
                PropertyName = "Concepto"
            };

            var pNumEmpleado = new PropertyGroupDescription() {
                GroupComparer = new GroupNameComparer(),
                CustomName = "Num. Empleado",
                PropertyName = "NumEmpleado"
            };

            var pIdDocumento = new PropertyGroupDescription() {
                GroupComparer = new GroupNameComparer(),
                CustomName = "Id Documento",
                PropertyName = "iddocumento"
            };

            var pEstado = new PropertyGroupDescription() {
                GroupComparer = new GroupNameComparer(),
                CustomName = "Estado",
                PropertyName = "Estado"
            };

            var sumAggregateImporteGravado = new PropertyAggregateDescription() {
                AggregateFunction = new SumAggregateFunction(),
                CustomName = "Gravado",
                PropertyName = "ImporteGravado",
                StringFormat = "#,###0.00"
            };

            var sumAggregateImporteExento = new PropertyAggregateDescription() {
                AggregateFunction = new SumAggregateFunction(),
                CustomName = "Exento",
                PropertyName = "ImporteExento",
                StringFormat = "#,###0.00"
            };

            this.GridData.ColumnGroupDescriptions.Add(pConcepto);
            this.GridData.RowGroupDescriptions.Add(pEstado);
            this.GridData.RowGroupDescriptions.Add(pNumEmpleado);
            this.GridData.RowGroupDescriptions.Add(pIdDocumento);
            this.GridData.AggregateDescriptions.Add(sumAggregateImporteGravado);
            this.GridData.AggregateDescriptions.Add(sumAggregateImporteExento);
        }

        private void Options_ToggleStateChanged(object sender, StateChangedEventArgs args) {
            if (this.radioPorNomina.IsChecked) {
                this.cboNominas.Enabled = true;
                this.ToolBarButtonPeriodo.Enabled = false;
                this.radSpinEditor1.Enabled = false;
                this.cboNominas.DataSource = this.data.GetNominas();
            }
            else if (this.radioPorPeriodo.IsChecked) {
                this.cboNominas.Enabled = false;
                this.ToolBarButtonPeriodo.Enabled = true;
                this.radSpinEditor1.Enabled = true;
            }
        }

        private void GridData_DataProviderChanged(object sender, DataProviderChangedEventArgs e) {
            this.buttonExportar.Enabled = true;
        }
    }
}
