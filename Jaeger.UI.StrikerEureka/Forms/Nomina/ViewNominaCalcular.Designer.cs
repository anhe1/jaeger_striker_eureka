﻿namespace Jaeger.Views.Nomina
{
    partial class ViewNominaCalcular
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject1 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem1 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem2 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem3 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem4 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem5 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem6 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem7 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem8 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem9 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem10 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem11 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem12 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem13 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem14 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem15 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem16 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem17 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem18 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem19 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem20 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem21 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem22 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem23 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem24 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem25 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem26 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem27 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem28 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem29 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem30 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem31 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem32 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem33 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem34 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem35 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem36 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem37 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn38 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn39 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn40 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn41 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn42 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem38 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.radWaitingBar1 = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            this.GridPercepciones = new Telerik.WinControls.UI.GridViewTemplate();
            this.GridDeducciones = new Telerik.WinControls.UI.GridViewTemplate();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarButtonNomina = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonNominaNuevo = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonNominaAbrir = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonEditar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonEliminar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonCalcular = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarButtonGuardar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonExportar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonHerramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonCrear = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonLayoutBancomer = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonImprimir = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonListado = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonSobreRecibo = new Telerik.WinControls.UI.RadMenuItem();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.TxbValesDespensa = new Telerik.WinControls.UI.RadTextBox();
            this.DtpFechaPago = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.LabelDiasPeriodo = new Telerik.WinControls.UI.RadLabel();
            this.TxbFondoAhorro = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.DtpFechaFinal = new Telerik.WinControls.UI.RadDateTimePicker();
            this.TxbDeducciones = new Telerik.WinControls.UI.RadTextBox();
            this.DtpFechaInicio = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.TxbPercepciones = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.CboTipoPeriodo = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.CboTipoNomina = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.TxbDescripcion = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.CboEstado = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            this.GridData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPercepciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeducciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbValesDespensa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFechaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDiasPeriodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbFondoAhorro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFechaFinal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbDeducciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFechaInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbPercepciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoPeriodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoNomina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbDescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GridData
            // 
            this.GridData.Controls.Add(this.radWaitingBar1);
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(267, 55);
            // 
            // 
            // 
            this.GridData.MasterTemplate.AllowAddNewRow = false;
            gridViewTextBoxColumn1.FieldName = "Numero";
            gridViewTextBoxColumn1.FormatString = "{0:0000}";
            gridViewTextBoxColumn1.HeaderText = "Núm";
            gridViewTextBoxColumn1.IsPinned = true;
            gridViewTextBoxColumn1.Name = "Numero";
            gridViewTextBoxColumn1.PinPosition = Telerik.WinControls.UI.PinnedColumnPosition.Left;
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 40;
            gridViewTextBoxColumn2.FieldName = "Clave";
            gridViewTextBoxColumn2.HeaderText = "Clave";
            gridViewTextBoxColumn2.IsPinned = true;
            gridViewTextBoxColumn2.Name = "Clave";
            gridViewTextBoxColumn2.PinPosition = Telerik.WinControls.UI.PinnedColumnPosition.Left;
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn3.FieldName = "Nombre";
            gridViewTextBoxColumn3.HeaderText = "Nombre";
            gridViewTextBoxColumn3.IsPinned = true;
            gridViewTextBoxColumn3.Name = "Nombre";
            gridViewTextBoxColumn3.PinPosition = Telerik.WinControls.UI.PinnedColumnPosition.Left;
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 210;
            gridViewTextBoxColumn4.FieldName = "JornadasTrabajo";
            gridViewTextBoxColumn4.HeaderText = "J. T.";
            gridViewTextBoxColumn4.Name = "JornadasTrabajo";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 35;
            gridViewTextBoxColumn5.DataType = typeof(int);
            gridViewTextBoxColumn5.FieldName = "Ausencias";
            gridViewTextBoxColumn5.HeaderText = "Aus.";
            gridViewTextBoxColumn5.Name = "Ausencias";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 35;
            gridViewTextBoxColumn6.FieldName = "Incapacidad";
            gridViewTextBoxColumn6.HeaderText = "Inc.";
            gridViewTextBoxColumn6.Name = "Incapacidad";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.Width = 35;
            gridViewCheckBoxColumn1.FieldName = "Puntualidad";
            gridViewCheckBoxColumn1.HeaderText = "P.";
            gridViewCheckBoxColumn1.Name = "Puntualidad";
            gridViewCheckBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewCheckBoxColumn1.Width = 35;
            gridViewTextBoxColumn7.FieldName = "Productividad";
            gridViewTextBoxColumn7.HeaderText = "% Prod.";
            gridViewTextBoxColumn7.Name = "Productividad";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn7.Width = 40;
            gridViewTextBoxColumn8.FieldName = "HorasReloj";
            gridViewTextBoxColumn8.HeaderText = "Horas Reloj";
            gridViewTextBoxColumn8.Name = "HorasReloj";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn8.WrapText = true;
            gridViewTextBoxColumn9.FieldName = "HorasExtra";
            gridViewTextBoxColumn9.HeaderText = "Hrs. Extra";
            gridViewTextBoxColumn9.Name = "HorasExtra";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn9.WrapText = true;
            gridViewTextBoxColumn10.FieldName = "HorasAutorizadas";
            gridViewTextBoxColumn10.HeaderText = "Horas Aut.";
            gridViewTextBoxColumn10.Name = "HorasAutorizadas";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn10.WrapText = true;
            gridViewTextBoxColumn11.FieldName = "HrsCargo";
            gridViewTextBoxColumn11.HeaderText = "HrsCargo";
            gridViewTextBoxColumn11.Name = "HrsCargo";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn11.Width = 65;
            conditionalFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.CellForeColor = System.Drawing.Color.Red;
            conditionalFormattingObject1.ConditionType = Telerik.WinControls.UI.ConditionTypes.NotEqual;
            conditionalFormattingObject1.Name = "NewCondition";
            conditionalFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.RowForeColor = System.Drawing.Color.White;
            conditionalFormattingObject1.TValue1 = "00:00:00";
            gridViewTextBoxColumn12.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            gridViewTextBoxColumn12.FieldName = "HrsAbono";
            gridViewTextBoxColumn12.HeaderText = "HrsAbono";
            gridViewTextBoxColumn12.Name = "HrsAbono";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn12.Width = 65;
            gridViewTextBoxColumn13.FieldName = "DiasVacaciones";
            gridViewTextBoxColumn13.HeaderText = "Dias Vac.";
            gridViewTextBoxColumn13.Name = "DiasVacaciones";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn13.WrapText = true;
            gridViewTextBoxColumn14.DataType = typeof(decimal);
            gridViewTextBoxColumn14.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn14.FieldName = "SalarioDiario";
            gridViewTextBoxColumn14.FormatString = "{0:n}";
            gridViewTextBoxColumn14.HeaderText = "S. D.";
            gridViewTextBoxColumn14.Name = "SalarioDiario";
            gridViewTextBoxColumn14.ReadOnly = true;
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.Width = 60;
            gridViewTextBoxColumn15.DataType = typeof(decimal);
            gridViewTextBoxColumn15.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn15.FieldName = "SalarioDiarioIntegrado";
            gridViewTextBoxColumn15.FormatString = "{0:n}";
            gridViewTextBoxColumn15.HeaderText = "S. D. I.";
            gridViewTextBoxColumn15.Name = "SalarioDiarioIntegrado";
            gridViewTextBoxColumn15.ReadOnly = true;
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn15.Width = 60;
            gridViewTextBoxColumn16.DataType = typeof(decimal);
            gridViewTextBoxColumn16.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn16.FieldName = "SueldosSalarios";
            gridViewTextBoxColumn16.FormatString = "{0:n}";
            gridViewTextBoxColumn16.HeaderText = "Sueldo";
            gridViewTextBoxColumn16.Name = "SueldosSalarios";
            gridViewTextBoxColumn16.ReadOnly = true;
            gridViewTextBoxColumn16.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn16.Width = 75;
            gridViewTextBoxColumn17.DataType = typeof(decimal);
            gridViewTextBoxColumn17.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn17.FieldName = "PercepcionEspecial";
            gridViewTextBoxColumn17.FormatString = "{0:n}";
            gridViewTextBoxColumn17.HeaderText = "P. Especial";
            gridViewTextBoxColumn17.Name = "PercepcionEspecial";
            gridViewTextBoxColumn17.ReadOnly = true;
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn17.Width = 75;
            gridViewTextBoxColumn18.DataType = typeof(decimal);
            gridViewTextBoxColumn18.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn18.FieldName = "DescuentoPresatamo";
            gridViewTextBoxColumn18.FormatString = "{0:n}";
            gridViewTextBoxColumn18.HeaderText = "Dto. Prestamo";
            gridViewTextBoxColumn18.Name = "DescuentoPresatamo";
            gridViewTextBoxColumn18.ReadOnly = true;
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn18.Width = 75;
            gridViewTextBoxColumn19.DataType = typeof(decimal);
            gridViewTextBoxColumn19.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn19.FieldName = "ISR";
            gridViewTextBoxColumn19.FormatString = "{0:n}";
            gridViewTextBoxColumn19.HeaderText = "ISR Causado";
            gridViewTextBoxColumn19.Name = "ISR";
            gridViewTextBoxColumn19.ReadOnly = true;
            gridViewTextBoxColumn19.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn19.Width = 75;
            gridViewTextBoxColumn19.WrapText = true;
            gridViewTextBoxColumn20.DataType = typeof(decimal);
            gridViewTextBoxColumn20.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn20.FieldName = "SubSidio";
            gridViewTextBoxColumn20.FormatString = "{0:n}";
            gridViewTextBoxColumn20.HeaderText = "SubSidio";
            gridViewTextBoxColumn20.Name = "SubSidio";
            gridViewTextBoxColumn20.ReadOnly = true;
            gridViewTextBoxColumn20.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn20.Width = 75;
            gridViewTextBoxColumn21.DataType = typeof(decimal);
            gridViewTextBoxColumn21.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn21.FieldName = "FondoAhorro";
            gridViewTextBoxColumn21.FormatString = "{0:n}";
            gridViewTextBoxColumn21.HeaderText = "F. Ahorro";
            gridViewTextBoxColumn21.Name = "FondoAhorro";
            gridViewTextBoxColumn21.ReadOnly = true;
            gridViewTextBoxColumn21.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn21.Width = 75;
            gridViewTextBoxColumn21.WrapText = true;
            gridViewTextBoxColumn22.DataType = typeof(decimal);
            gridViewTextBoxColumn22.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn22.FieldName = "CuotasIMSS";
            gridViewTextBoxColumn22.FormatString = "{0:n}";
            gridViewTextBoxColumn22.HeaderText = "IMSS Obrero";
            gridViewTextBoxColumn22.Name = "CuotasIMSS";
            gridViewTextBoxColumn22.ReadOnly = true;
            gridViewTextBoxColumn22.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn22.Width = 75;
            gridViewTextBoxColumn22.WrapText = true;
            gridViewTextBoxColumn23.DataType = typeof(decimal);
            gridViewTextBoxColumn23.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn23.FieldName = "CuotasIMSSPatronal";
            gridViewTextBoxColumn23.FormatString = "{0:n}";
            gridViewTextBoxColumn23.HeaderText = "IMSS Patronal";
            gridViewTextBoxColumn23.Name = "CuotasIMSSPatronal";
            gridViewTextBoxColumn23.ReadOnly = true;
            gridViewTextBoxColumn23.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn23.Width = 75;
            gridViewTextBoxColumn23.WrapText = true;
            gridViewTextBoxColumn24.DataType = typeof(decimal);
            gridViewTextBoxColumn24.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn24.FieldName = "TotalPercepcionesGravado";
            gridViewTextBoxColumn24.FormatString = "{0:n}";
            gridViewTextBoxColumn24.HeaderText = "Total Percepciones Gravado";
            gridViewTextBoxColumn24.Name = "TotalPercepcionesGravado";
            gridViewTextBoxColumn24.ReadOnly = true;
            gridViewTextBoxColumn24.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn24.Width = 75;
            gridViewTextBoxColumn25.DataType = typeof(decimal);
            gridViewTextBoxColumn25.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn25.FieldName = "TotalPercepcionesExento";
            gridViewTextBoxColumn25.FormatString = "{0:n}";
            gridViewTextBoxColumn25.HeaderText = "Total Percepciones Exento";
            gridViewTextBoxColumn25.Name = "TotalPercepcionesExento";
            gridViewTextBoxColumn25.ReadOnly = true;
            gridViewTextBoxColumn25.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn25.Width = 75;
            gridViewTextBoxColumn26.DataType = typeof(decimal);
            gridViewTextBoxColumn26.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn26.FieldName = "TotalPercepciones";
            gridViewTextBoxColumn26.FormatString = "{0:n}";
            gridViewTextBoxColumn26.HeaderText = "Total Percepciones";
            gridViewTextBoxColumn26.Name = "TotalPercepciones";
            gridViewTextBoxColumn26.ReadOnly = true;
            gridViewTextBoxColumn26.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn26.Width = 75;
            gridViewTextBoxColumn27.DataType = typeof(decimal);
            gridViewTextBoxColumn27.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn27.FieldName = "TotalDeducciones";
            gridViewTextBoxColumn27.FormatString = "{0:n}";
            gridViewTextBoxColumn27.HeaderText = "Total Deducciones";
            gridViewTextBoxColumn27.Name = "TotalDeducciones";
            gridViewTextBoxColumn27.ReadOnly = true;
            gridViewTextBoxColumn27.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn27.Width = 75;
            gridViewTextBoxColumn28.DataType = typeof(decimal);
            gridViewTextBoxColumn28.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn28.FieldName = "ISRRetener";
            gridViewTextBoxColumn28.FormatString = "{0:n}";
            gridViewTextBoxColumn28.HeaderText = "ISR Retener";
            gridViewTextBoxColumn28.Name = "ISRRetener";
            gridViewTextBoxColumn28.ReadOnly = true;
            gridViewTextBoxColumn28.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn28.Width = 75;
            gridViewTextBoxColumn29.DataType = typeof(decimal);
            gridViewTextBoxColumn29.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn29.FieldName = "SueldoNeto";
            gridViewTextBoxColumn29.FormatString = "{0:n}";
            gridViewTextBoxColumn29.HeaderText = "Sueldo Neto";
            gridViewTextBoxColumn29.Name = "SueldoNeto";
            gridViewTextBoxColumn29.ReadOnly = true;
            gridViewTextBoxColumn29.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn29.Width = 75;
            gridViewTextBoxColumn29.WrapText = true;
            gridViewTextBoxColumn30.DataType = typeof(decimal);
            gridViewTextBoxColumn30.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn30.FieldName = "ValesDespensa";
            gridViewTextBoxColumn30.FormatString = "{0:n}";
            gridViewTextBoxColumn30.HeaderText = "V. Despensa";
            gridViewTextBoxColumn30.Name = "ValesDespensa";
            gridViewTextBoxColumn30.ReadOnly = true;
            gridViewTextBoxColumn30.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn30.Width = 65;
            gridViewTextBoxColumn31.DataType = typeof(decimal);
            gridViewTextBoxColumn31.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn31.FieldName = "Sueldo";
            gridViewTextBoxColumn31.FormatString = "{0:n}";
            gridViewTextBoxColumn31.HeaderText = "Sueldo";
            gridViewTextBoxColumn31.Name = "Sueldo";
            gridViewTextBoxColumn31.ReadOnly = true;
            gridViewTextBoxColumn31.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn31.Width = 75;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31});
            gridViewSummaryItem1.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem1.AggregateExpression = null;
            gridViewSummaryItem1.FormatString = "{0:n}";
            gridViewSummaryItem1.Name = "SueldosSalarios";
            gridViewSummaryItem2.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem2.AggregateExpression = null;
            gridViewSummaryItem2.FormatString = "{0:n}";
            gridViewSummaryItem2.Name = "TotalPercepciones";
            gridViewSummaryItem3.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem3.AggregateExpression = null;
            gridViewSummaryItem3.FormatString = "{0:n}";
            gridViewSummaryItem3.Name = "TotalPercepcionesGravado";
            gridViewSummaryItem4.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem4.AggregateExpression = null;
            gridViewSummaryItem4.FormatString = "{0:n}";
            gridViewSummaryItem4.Name = "TotalPercepcionesExento";
            gridViewSummaryItem5.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem5.AggregateExpression = null;
            gridViewSummaryItem5.FormatString = "{0:n}";
            gridViewSummaryItem5.Name = "TotalDeducciones";
            gridViewSummaryItem6.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem6.AggregateExpression = null;
            gridViewSummaryItem6.FormatString = "{0:n}";
            gridViewSummaryItem6.Name = "PercepcionEspecial";
            gridViewSummaryItem7.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem7.AggregateExpression = null;
            gridViewSummaryItem7.FormatString = "{0:n}";
            gridViewSummaryItem7.Name = "DescuentoPresatamo";
            gridViewSummaryItem8.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem8.AggregateExpression = null;
            gridViewSummaryItem8.FormatString = "{0:n}";
            gridViewSummaryItem8.Name = "DescuentoInfonavit";
            gridViewSummaryItem9.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem9.AggregateExpression = null;
            gridViewSummaryItem9.FormatString = "{0:n}";
            gridViewSummaryItem9.Name = "FondoAhorro";
            gridViewSummaryItem10.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem10.AggregateExpression = null;
            gridViewSummaryItem10.FormatString = "{0:n}";
            gridViewSummaryItem10.Name = "ValesDespensa";
            gridViewSummaryItem11.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem11.AggregateExpression = null;
            gridViewSummaryItem11.FormatString = "{0:n}";
            gridViewSummaryItem11.Name = "SueldoNeto";
            gridViewSummaryItem12.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem12.AggregateExpression = null;
            gridViewSummaryItem12.FormatString = "{0:n}";
            gridViewSummaryItem12.Name = "Sueldo";
            gridViewSummaryItem13.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem13.AggregateExpression = null;
            gridViewSummaryItem13.FormatString = "{0:n}";
            gridViewSummaryItem13.Name = "ISR";
            gridViewSummaryItem14.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem14.AggregateExpression = null;
            gridViewSummaryItem14.FormatString = "{0:n}";
            gridViewSummaryItem14.Name = "SubSidio";
            gridViewSummaryItem15.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem15.AggregateExpression = null;
            gridViewSummaryItem15.FormatString = "{0:n}";
            gridViewSummaryItem15.Name = "ISRRetener";
            gridViewSummaryItem16.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem16.AggregateExpression = null;
            gridViewSummaryItem16.FormatString = "{0:n}";
            gridViewSummaryItem16.Name = "CuotasIMSS";
            gridViewSummaryItem17.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem17.AggregateExpression = null;
            gridViewSummaryItem17.FormatString = "{0:n}";
            gridViewSummaryItem17.Name = "CuotasIMSSPatronal";
            this.GridData.MasterTemplate.SummaryRowsBottom.Add(new Telerik.WinControls.UI.GridViewSummaryRowItem(new Telerik.WinControls.UI.GridViewSummaryItem[] {
                gridViewSummaryItem1,
                gridViewSummaryItem2,
                gridViewSummaryItem3,
                gridViewSummaryItem4,
                gridViewSummaryItem5,
                gridViewSummaryItem6,
                gridViewSummaryItem7,
                gridViewSummaryItem8,
                gridViewSummaryItem9,
                gridViewSummaryItem10,
                gridViewSummaryItem11,
                gridViewSummaryItem12,
                gridViewSummaryItem13,
                gridViewSummaryItem14,
                gridViewSummaryItem15,
                gridViewSummaryItem16,
                gridViewSummaryItem17}));
            gridViewSummaryItem18.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem18.AggregateExpression = null;
            gridViewSummaryItem18.FormatString = "{0:n}";
            gridViewSummaryItem18.Name = "SueldosSalarios";
            gridViewSummaryItem19.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem19.AggregateExpression = null;
            gridViewSummaryItem19.FormatString = "{0:n}";
            gridViewSummaryItem19.Name = "TotalPercepciones";
            gridViewSummaryItem20.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem20.AggregateExpression = null;
            gridViewSummaryItem20.FormatString = "{0:n}";
            gridViewSummaryItem20.Name = "TotalPercepcionesGravado";
            gridViewSummaryItem21.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem21.AggregateExpression = null;
            gridViewSummaryItem21.FormatString = "{0:n}";
            gridViewSummaryItem21.Name = "TotalPercepcionesExento";
            gridViewSummaryItem22.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem22.AggregateExpression = null;
            gridViewSummaryItem22.FormatString = "{0:n}";
            gridViewSummaryItem22.Name = "TotalDeducciones";
            gridViewSummaryItem23.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem23.AggregateExpression = null;
            gridViewSummaryItem23.FormatString = "{0:n}";
            gridViewSummaryItem23.Name = "PercepcionEspecial";
            gridViewSummaryItem24.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem24.AggregateExpression = null;
            gridViewSummaryItem24.FormatString = "{0:n}";
            gridViewSummaryItem24.Name = "DescuentoPresatamo";
            gridViewSummaryItem25.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem25.AggregateExpression = null;
            gridViewSummaryItem25.FormatString = "{0:n}";
            gridViewSummaryItem25.Name = "DescuentoInfonavit";
            gridViewSummaryItem26.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem26.AggregateExpression = null;
            gridViewSummaryItem26.FormatString = "{0:n}";
            gridViewSummaryItem26.Name = "FondoAhorro";
            gridViewSummaryItem27.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem27.AggregateExpression = null;
            gridViewSummaryItem27.FormatString = "{0:n}";
            gridViewSummaryItem27.Name = "ValesDespensa";
            gridViewSummaryItem28.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem28.AggregateExpression = null;
            gridViewSummaryItem28.FormatString = "{0:n}";
            gridViewSummaryItem28.Name = "SueldoNeto";
            gridViewSummaryItem29.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem29.AggregateExpression = null;
            gridViewSummaryItem29.FormatString = "{0:n}";
            gridViewSummaryItem29.Name = "Sueldo";
            gridViewSummaryItem30.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem30.AggregateExpression = null;
            gridViewSummaryItem30.FormatString = "{0:n}";
            gridViewSummaryItem30.Name = "ISR";
            gridViewSummaryItem31.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem31.AggregateExpression = null;
            gridViewSummaryItem31.FormatString = "{0:n}";
            gridViewSummaryItem31.Name = "SubSidio";
            gridViewSummaryItem32.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem32.AggregateExpression = null;
            gridViewSummaryItem32.FormatString = "{0:n}";
            gridViewSummaryItem32.Name = "ISRRetener";
            gridViewSummaryItem33.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem33.AggregateExpression = null;
            gridViewSummaryItem33.FormatString = "{0:n}";
            gridViewSummaryItem33.Name = "CuotasIMSS";
            gridViewSummaryItem34.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem34.AggregateExpression = null;
            gridViewSummaryItem34.FormatString = "{0:n}";
            gridViewSummaryItem34.Name = "CuotasIMSSPatronal";
            gridViewSummaryItem35.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem35.AggregateExpression = null;
            gridViewSummaryItem35.FormatString = "{0:n}";
            gridViewSummaryItem35.Name = "ImpuestoSobreNomina";
            this.GridData.MasterTemplate.SummaryRowsTop.Add(new Telerik.WinControls.UI.GridViewSummaryRowItem(new Telerik.WinControls.UI.GridViewSummaryItem[] {
                gridViewSummaryItem18,
                gridViewSummaryItem19,
                gridViewSummaryItem20,
                gridViewSummaryItem21,
                gridViewSummaryItem22,
                gridViewSummaryItem23,
                gridViewSummaryItem24,
                gridViewSummaryItem25,
                gridViewSummaryItem26,
                gridViewSummaryItem27,
                gridViewSummaryItem28,
                gridViewSummaryItem29,
                gridViewSummaryItem30,
                gridViewSummaryItem31,
                gridViewSummaryItem32,
                gridViewSummaryItem33,
                gridViewSummaryItem34,
                gridViewSummaryItem35}));
            this.GridData.MasterTemplate.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.GridPercepciones,
            this.GridDeducciones});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1028, 563);
            this.GridData.TabIndex = 1;
            // 
            // radWaitingBar1
            // 
            this.radWaitingBar1.AssociatedControl = this.GridData;
            this.radWaitingBar1.Location = new System.Drawing.Point(318, 264);
            this.radWaitingBar1.Name = "radWaitingBar1";
            this.radWaitingBar1.Size = new System.Drawing.Size(130, 24);
            this.radWaitingBar1.TabIndex = 1;
            this.radWaitingBar1.Text = "radWaitingBar1";
            this.radWaitingBar1.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.radWaitingBar1.WaitingSpeed = 80;
            this.radWaitingBar1.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            ((Telerik.WinControls.UI.RadWaitingBarElement)(this.radWaitingBar1.GetChildAt(0))).WaitingSpeed = 80;
            ((Telerik.WinControls.UI.WaitingBarContentElement)(this.radWaitingBar1.GetChildAt(0).GetChildAt(0))).WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            ((Telerik.WinControls.UI.WaitingBarSeparatorElement)(this.radWaitingBar1.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Dash = false;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            // 
            // GridPercepciones
            // 
            this.GridPercepciones.Caption = "Percepciones";
            gridViewTextBoxColumn32.FieldName = "Identificador";
            gridViewTextBoxColumn32.HeaderText = "Ident.";
            gridViewTextBoxColumn32.Name = "Identificador";
            gridViewTextBoxColumn33.FieldName = "Clave";
            gridViewTextBoxColumn33.HeaderText = "Clave";
            gridViewTextBoxColumn33.Name = "Clave";
            gridViewTextBoxColumn34.FieldName = "Tipo";
            gridViewTextBoxColumn34.HeaderText = "Tipo";
            gridViewTextBoxColumn34.Name = "Tipo";
            gridViewTextBoxColumn35.FieldName = "Concepto";
            gridViewTextBoxColumn35.HeaderText = "Concepto";
            gridViewTextBoxColumn35.Name = "Concepto";
            gridViewTextBoxColumn35.Width = 200;
            gridViewTextBoxColumn36.DataType = typeof(decimal);
            gridViewTextBoxColumn36.FieldName = "ImporteGravado";
            gridViewTextBoxColumn36.FormatString = "{0:n}";
            gridViewTextBoxColumn36.HeaderText = "Gravado";
            gridViewTextBoxColumn36.Name = "ImporteGravado";
            gridViewTextBoxColumn36.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn36.Width = 65;
            gridViewTextBoxColumn37.DataType = typeof(decimal);
            gridViewTextBoxColumn37.FieldName = "ImporteExento";
            gridViewTextBoxColumn37.FormatString = "{0:n}";
            gridViewTextBoxColumn37.HeaderText = "Exento";
            gridViewTextBoxColumn37.Name = "ImporteExento";
            gridViewTextBoxColumn37.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn37.Width = 65;
            this.GridPercepciones.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn32,
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34,
            gridViewTextBoxColumn35,
            gridViewTextBoxColumn36,
            gridViewTextBoxColumn37});
            gridViewSummaryItem36.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem36.AggregateExpression = null;
            gridViewSummaryItem36.FormatString = "{0:n}";
            gridViewSummaryItem36.Name = "ImporteGravado";
            gridViewSummaryItem37.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem37.AggregateExpression = null;
            gridViewSummaryItem37.FormatString = "{0:n}";
            gridViewSummaryItem37.Name = "ImporteExento";
            this.GridPercepciones.SummaryRowsBottom.Add(new Telerik.WinControls.UI.GridViewSummaryRowItem(new Telerik.WinControls.UI.GridViewSummaryItem[] {
                gridViewSummaryItem36,
                gridViewSummaryItem37}));
            this.GridPercepciones.ViewDefinition = tableViewDefinition1;
            // 
            // GridDeducciones
            // 
            this.GridDeducciones.Caption = "Deducciones";
            gridViewTextBoxColumn38.FieldName = "Identificador";
            gridViewTextBoxColumn38.HeaderText = "Ident.";
            gridViewTextBoxColumn38.Name = "Identificador";
            gridViewTextBoxColumn39.FieldName = "Clave";
            gridViewTextBoxColumn39.HeaderText = "Clave";
            gridViewTextBoxColumn39.Name = "Clave";
            gridViewTextBoxColumn40.FieldName = "Tipo";
            gridViewTextBoxColumn40.HeaderText = "Tipo";
            gridViewTextBoxColumn40.Name = "Tipo";
            gridViewTextBoxColumn41.FieldName = "Concepto";
            gridViewTextBoxColumn41.HeaderText = "Concepto";
            gridViewTextBoxColumn41.Name = "Concepto";
            gridViewTextBoxColumn41.Width = 200;
            gridViewTextBoxColumn42.DataType = typeof(decimal);
            gridViewTextBoxColumn42.FieldName = "Importe";
            gridViewTextBoxColumn42.FormatString = "{0:n}";
            gridViewTextBoxColumn42.HeaderText = "Importe";
            gridViewTextBoxColumn42.Name = "Importe";
            gridViewTextBoxColumn42.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn42.Width = 75;
            this.GridDeducciones.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn38,
            gridViewTextBoxColumn39,
            gridViewTextBoxColumn40,
            gridViewTextBoxColumn41,
            gridViewTextBoxColumn42});
            gridViewSummaryItem38.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem38.AggregateExpression = null;
            gridViewSummaryItem38.FormatString = "{0:n}";
            gridViewSummaryItem38.Name = "Importe";
            this.GridDeducciones.SummaryRowsBottom.Add(new Telerik.WinControls.UI.GridViewSummaryRowItem(new Telerik.WinControls.UI.GridViewSummaryItem[] {
                gridViewSummaryItem38}));
            this.GridDeducciones.ViewDefinition = tableViewDefinition2;
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 618);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(1295, 26);
            this.radStatusStrip1.SizingGrip = false;
            this.radStatusStrip1.TabIndex = 2;
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(1295, 55);
            this.radCommandBar1.TabIndex = 4;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            this.commandBarRowElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement1.UseCompatibleTextRendering = false;
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarButtonNomina,
            this.ToolBarButtonEditar,
            this.ToolBarButtonEliminar,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonCalcular,
            this.ToolBarButtonFiltro,
            this.ToolBarButtonGuardar,
            this.ToolBarButtonImprimir,
            this.ToolBarButtonExportar,
            this.ToolBarButtonHerramientas,
            this.ToolBarButtonCerrar});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // ToolBarButtonNomina
            // 
            this.ToolBarButtonNomina.DisplayName = "commandBarDropDownButton1";
            this.ToolBarButtonNomina.DrawText = true;
            this.ToolBarButtonNomina.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_select_users;
            this.ToolBarButtonNomina.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonNominaNuevo,
            this.ToolBarButtonNominaAbrir});
            this.ToolBarButtonNomina.Name = "ToolBarButtonNomina";
            this.ToolBarButtonNomina.Text = "Nomina";
            this.ToolBarButtonNomina.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonNominaNuevo
            // 
            this.ToolBarButtonNominaNuevo.Name = "ToolBarButtonNominaNuevo";
            this.ToolBarButtonNominaNuevo.Text = "Nueva nómina";
            this.ToolBarButtonNominaNuevo.Click += new System.EventHandler(this.ToolBarButtonNominaNuevo_Click);
            // 
            // ToolBarButtonNominaAbrir
            // 
            this.ToolBarButtonNominaAbrir.Name = "ToolBarButtonNominaAbrir";
            this.ToolBarButtonNominaAbrir.Text = "Abrir";
            this.ToolBarButtonNominaAbrir.ToolTipText = "Abrir nomina almacenada";
            this.ToolBarButtonNominaAbrir.Click += new System.EventHandler(this.ToolBarButtonNominaAbrir_Click);
            // 
            // ToolBarButtonEditar
            // 
            this.ToolBarButtonEditar.DisplayName = "commandBarButton1";
            this.ToolBarButtonEditar.DrawText = true;
            this.ToolBarButtonEditar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_registro;
            this.ToolBarButtonEditar.Name = "ToolBarButtonEditar";
            this.ToolBarButtonEditar.Text = "Editar";
            this.ToolBarButtonEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonEditar.Click += new System.EventHandler(this.ToolBarButtonEditar_Click);
            // 
            // ToolBarButtonEliminar
            // 
            this.ToolBarButtonEliminar.DisplayName = "commandBarButton1";
            this.ToolBarButtonEliminar.DrawText = true;
            this.ToolBarButtonEliminar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_retire_hombre_usuario;
            this.ToolBarButtonEliminar.Name = "ToolBarButtonEliminar";
            this.ToolBarButtonEliminar.Text = "Eliminar";
            this.ToolBarButtonEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonEliminar.Click += new System.EventHandler(this.ToolBarButtonEliminar_Click);
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.DisplayName = "commandBarButton1";
            this.ToolBarButtonActualizar.DrawText = true;
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_actualizar;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.ToolBarButtonActualizar_Click);
            // 
            // ToolBarButtonCalcular
            // 
            this.ToolBarButtonCalcular.DisplayName = "commandBarButton1";
            this.ToolBarButtonCalcular.DrawText = true;
            this.ToolBarButtonCalcular.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_ciclo_vital;
            this.ToolBarButtonCalcular.Name = "ToolBarButtonCalcular";
            this.ToolBarButtonCalcular.Text = "Calcular";
            this.ToolBarButtonCalcular.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCalcular.Click += new System.EventHandler(this.ToolBarButtonCalcular_Click);
            // 
            // ToolBarButtonFiltro
            // 
            this.ToolBarButtonFiltro.DisplayName = "commandBarToggleButton1";
            this.ToolBarButtonFiltro.DrawText = true;
            this.ToolBarButtonFiltro.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_filtrar;
            this.ToolBarButtonFiltro.Name = "ToolBarButtonFiltro";
            this.ToolBarButtonFiltro.Text = "Filtro";
            this.ToolBarButtonFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonFiltro.Click += new System.EventHandler(this.ToolBarButtonFiltro_Click);
            // 
            // ToolBarButtonGuardar
            // 
            this.ToolBarButtonGuardar.DisplayName = "commandBarButton1";
            this.ToolBarButtonGuardar.DrawText = true;
            this.ToolBarButtonGuardar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_guardar;
            this.ToolBarButtonGuardar.Name = "ToolBarButtonGuardar";
            this.ToolBarButtonGuardar.Text = "Guardar";
            this.ToolBarButtonGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonGuardar.Click += new System.EventHandler(this.ToolBarButtonGuardar_Click);
            // 
            // ToolBarButtonExportar
            // 
            this.ToolBarButtonExportar.DisplayName = "Exportar";
            this.ToolBarButtonExportar.DrawText = true;
            this.ToolBarButtonExportar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_excel;
            this.ToolBarButtonExportar.Name = "ToolBarButtonExportar";
            this.ToolBarButtonExportar.Text = "Exportar";
            this.ToolBarButtonExportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonExportar.Click += new System.EventHandler(this.ToolBarButtonExportar_Click);
            // 
            // ToolBarButtonHerramientas
            // 
            this.ToolBarButtonHerramientas.DisplayName = "Herramientas";
            this.ToolBarButtonHerramientas.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_configuracion;
            this.ToolBarButtonHerramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonCrear,
            this.ToolBarButtonLayoutBancomer});
            this.ToolBarButtonHerramientas.Name = "ToolBarButtonHerramientas";
            this.ToolBarButtonHerramientas.Text = "Herramientas";
            this.ToolBarButtonHerramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonCrear
            // 
            this.ToolBarButtonCrear.Name = "ToolBarButtonCrear";
            this.ToolBarButtonCrear.Text = "Crear tabla";
            this.ToolBarButtonCrear.Click += new System.EventHandler(this.ToolBarButtonCrear_Click);
            // 
            // ToolBarButtonLayoutBancomer
            // 
            this.ToolBarButtonLayoutBancomer.Name = "ToolBarButtonLayoutBancomer";
            this.ToolBarButtonLayoutBancomer.Text = "Bancomer (layout)";
            this.ToolBarButtonLayoutBancomer.Click += new System.EventHandler(this.ToolBarButtonLayoutBancomer_Click);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "commandBarButton1";
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_cerrar;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // ToolBarButtonImprimir
            // 
            this.ToolBarButtonImprimir.DisplayName = "commandBarDropDownButton1";
            this.ToolBarButtonImprimir.DrawText = true;
            this.ToolBarButtonImprimir.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_imprimir;
            this.ToolBarButtonImprimir.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonListado,
            this.ToolBarButtonSobreRecibo});
            this.ToolBarButtonImprimir.Name = "ToolBarButtonImprimir";
            this.ToolBarButtonImprimir.Text = "Imprimir";
            this.ToolBarButtonImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonListado
            // 
            this.ToolBarButtonListado.Name = "ToolBarButtonListado";
            this.ToolBarButtonListado.Text = "Listado";
            this.ToolBarButtonListado.Click += new System.EventHandler(this.ToolBarButtonImprimirListado_Click);
            // 
            // ToolBarButtonSobreRecibo
            // 
            this.ToolBarButtonSobreRecibo.Name = "ToolBarButtonSobreRecibo";
            this.ToolBarButtonSobreRecibo.Text = "SobreRecibo";
            this.ToolBarButtonSobreRecibo.Click += new System.EventHandler(this.ToolBarButtonImprimirSobreRecibo_Click);
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radCollapsiblePanel1.ExpandDirection = Telerik.WinControls.UI.RadDirection.Right;
            this.radCollapsiblePanel1.HorizontalHeaderAlignment = Telerik.WinControls.UI.RadHorizontalAlignment.Right;
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 55);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 30, 150, 470);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel7);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.TxbValesDespensa);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.DtpFechaPago);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel12);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.LabelDiasPeriodo);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.TxbFondoAhorro);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel8);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel11);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.DtpFechaFinal);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.TxbDeducciones);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.DtpFechaInicio);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel10);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel6);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.TxbPercepciones);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel5);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel9);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.CboTipoPeriodo);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel4);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.CboTipoNomina);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel3);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.TxbDescripcion);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel2);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.CboEstado);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel1);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(239, 561);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(267, 563);
            this.radCollapsiblePanel1.TabIndex = 5;
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(6, 309);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(90, 18);
            this.radLabel7.TabIndex = 12;
            this.radLabel7.Text = "Dias del Periodo:";
            // 
            // TxbValesDespensa
            // 
            this.TxbValesDespensa.Location = new System.Drawing.Point(106, 461);
            this.TxbValesDespensa.Name = "TxbValesDespensa";
            this.TxbValesDespensa.Size = new System.Drawing.Size(77, 20);
            this.TxbValesDespensa.TabIndex = 9;
            // 
            // DtpFechaPago
            // 
            this.DtpFechaPago.Location = new System.Drawing.Point(6, 357);
            this.DtpFechaPago.Name = "DtpFechaPago";
            this.DtpFechaPago.Size = new System.Drawing.Size(222, 20);
            this.DtpFechaPago.TabIndex = 10;
            this.DtpFechaPago.TabStop = false;
            this.DtpFechaPago.Text = "martes, 22 de enero de 2019";
            this.DtpFechaPago.Value = new System.DateTime(2019, 1, 22, 22, 26, 13, 706);
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(6, 463);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(86, 18);
            this.radLabel12.TabIndex = 10;
            this.radLabel12.Text = "Vales Despensa:";
            // 
            // LabelDiasPeriodo
            // 
            this.LabelDiasPeriodo.Location = new System.Drawing.Point(102, 309);
            this.LabelDiasPeriodo.Name = "LabelDiasPeriodo";
            this.LabelDiasPeriodo.Size = new System.Drawing.Size(48, 18);
            this.LabelDiasPeriodo.TabIndex = 10;
            this.LabelDiasPeriodo.Text = "Periodo:";
            // 
            // TxbFondoAhorro
            // 
            this.TxbFondoAhorro.Location = new System.Drawing.Point(106, 437);
            this.TxbFondoAhorro.Name = "TxbFondoAhorro";
            this.TxbFondoAhorro.Size = new System.Drawing.Size(77, 20);
            this.TxbFondoAhorro.TabIndex = 7;
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(6, 333);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(79, 18);
            this.radLabel8.TabIndex = 11;
            this.radLabel8.Text = "Fecha de Pago";
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(6, 439);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(94, 18);
            this.radLabel11.TabIndex = 8;
            this.radLabel11.Text = "Fondo de Ahorro:";
            // 
            // DtpFechaFinal
            // 
            this.DtpFechaFinal.Location = new System.Drawing.Point(6, 283);
            this.DtpFechaFinal.Name = "DtpFechaFinal";
            this.DtpFechaFinal.Size = new System.Drawing.Size(222, 20);
            this.DtpFechaFinal.TabIndex = 8;
            this.DtpFechaFinal.TabStop = false;
            this.DtpFechaFinal.Text = "martes, 22 de enero de 2019";
            this.DtpFechaFinal.Value = new System.DateTime(2019, 1, 22, 22, 26, 13, 706);
            // 
            // TxbDeducciones
            // 
            this.TxbDeducciones.Location = new System.Drawing.Point(106, 413);
            this.TxbDeducciones.Name = "TxbDeducciones";
            this.TxbDeducciones.Size = new System.Drawing.Size(77, 20);
            this.TxbDeducciones.TabIndex = 5;
            // 
            // DtpFechaInicio
            // 
            this.DtpFechaInicio.Location = new System.Drawing.Point(6, 233);
            this.DtpFechaInicio.Name = "DtpFechaInicio";
            this.DtpFechaInicio.Size = new System.Drawing.Size(222, 20);
            this.DtpFechaInicio.TabIndex = 1;
            this.DtpFechaInicio.TabStop = false;
            this.DtpFechaInicio.Text = "martes, 22 de enero de 2019";
            this.DtpFechaInicio.Value = new System.DateTime(2019, 1, 22, 22, 26, 13, 706);
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(6, 415);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(84, 18);
            this.radLabel10.TabIndex = 6;
            this.radLabel10.Text = "T. Deducciones:";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(6, 259);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(98, 18);
            this.radLabel6.TabIndex = 9;
            this.radLabel6.Text = "Fecha de Termino:";
            // 
            // TxbPercepciones
            // 
            this.TxbPercepciones.Location = new System.Drawing.Point(106, 389);
            this.TxbPercepciones.Name = "TxbPercepciones";
            this.TxbPercepciones.Size = new System.Drawing.Size(77, 20);
            this.TxbPercepciones.TabIndex = 3;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(6, 209);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(83, 18);
            this.radLabel5.TabIndex = 7;
            this.radLabel5.Text = "Fecha de Inicio:";
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(6, 391);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(86, 18);
            this.radLabel9.TabIndex = 4;
            this.radLabel9.Text = "T. Percepciones:";
            // 
            // CboTipoPeriodo
            // 
            this.CboTipoPeriodo.Location = new System.Drawing.Point(6, 183);
            this.CboTipoPeriodo.Name = "CboTipoPeriodo";
            this.CboTipoPeriodo.NullText = "Selecciona";
            this.CboTipoPeriodo.Size = new System.Drawing.Size(98, 20);
            this.CboTipoPeriodo.TabIndex = 6;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(6, 159);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(48, 18);
            this.radLabel4.TabIndex = 5;
            this.radLabel4.Text = "Periodo:";
            // 
            // CboTipoNomina
            // 
            this.CboTipoNomina.Location = new System.Drawing.Point(6, 133);
            this.CboTipoNomina.Name = "CboTipoNomina";
            this.CboTipoNomina.NullText = "Selecciona";
            this.CboTipoNomina.Size = new System.Drawing.Size(98, 20);
            this.CboTipoNomina.TabIndex = 4;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(6, 109);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(89, 18);
            this.radLabel3.TabIndex = 3;
            this.radLabel3.Text = "Tipo de Nómina:";
            // 
            // TxbDescripcion
            // 
            this.TxbDescripcion.Location = new System.Drawing.Point(6, 83);
            this.TxbDescripcion.Name = "TxbDescripcion";
            this.TxbDescripcion.NullText = "Descripción";
            this.TxbDescripcion.Size = new System.Drawing.Size(222, 20);
            this.TxbDescripcion.TabIndex = 1;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(6, 59);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(67, 18);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "Descripción:";
            // 
            // CboEstado
            // 
            this.CboEstado.Location = new System.Drawing.Point(6, 33);
            this.CboEstado.Name = "CboEstado";
            this.CboEstado.NullText = "Selecciona";
            this.CboEstado.Size = new System.Drawing.Size(98, 20);
            this.CboEstado.TabIndex = 1;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(6, 9);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(40, 18);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Estado";
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.DisplayName = "commandBarStripElement2";
            this.commandBarStripElement2.Name = "commandBarStripElement2";
            // 
            // ViewNominaCalcular
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1295, 644);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.radCollapsiblePanel1);
            this.Controls.Add(this.radCommandBar1);
            this.Controls.Add(this.radStatusStrip1);
            this.Name = "ViewNominaCalcular";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ViewNominaCalcular";
            this.Load += new System.EventHandler(this.NominaCalcular_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.GridData.ResumeLayout(false);
            this.GridData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPercepciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeducciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            this.radCollapsiblePanel1.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            this.radCollapsiblePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbValesDespensa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFechaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDiasPeriodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbFondoAhorro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFechaFinal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbDeducciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFechaInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbPercepciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoPeriodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoNomina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbDescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonNomina;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonEditar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonEliminar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonActualizar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCalcular;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonGuardar;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonNominaNuevo;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox TxbValesDespensa;
        private Telerik.WinControls.UI.RadDateTimePicker DtpFechaPago;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel LabelDiasPeriodo;
        private Telerik.WinControls.UI.RadTextBox TxbFondoAhorro;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadDateTimePicker DtpFechaFinal;
        private Telerik.WinControls.UI.RadTextBox TxbDeducciones;
        private Telerik.WinControls.UI.RadDateTimePicker DtpFechaInicio;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox TxbPercepciones;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadDropDownList CboTipoPeriodo;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadDropDownList CboTipoNomina;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox TxbDescripcion;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList CboEstado;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonNominaAbrir;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCerrar;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar1;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;
        private Telerik.WinControls.UI.GridViewTemplate GridPercepciones;
        private Telerik.WinControls.UI.GridViewTemplate GridDeducciones;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonExportar;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltro;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonHerramientas;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonCrear;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonLayoutBancomer;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonImprimir;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonListado;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonSobreRecibo;
    }
}
