﻿namespace Jaeger.Views.Nomina
{
    partial class ViewDepartamentoCatalogo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn3 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarDepartamento = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarDeptoLabel = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarDeptoSeparator = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarDeptoButtonAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarDeptoButtonEliminar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarDeptoButtonGuardar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarDeptoButtonActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarDeptoButtonHerramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarDeptoButtonherramientasCrear = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarDeptoButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.GridDeptos = new Telerik.WinControls.UI.RadGridView();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.GridPuestos = new Telerik.WinControls.UI.RadGridView();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarPuesto = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarPuestoLabel = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarPuestoSeparador = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarPuestoButtonAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarPuestoButtonEliminar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarPuestoButtonGuardar = new Telerik.WinControls.UI.CommandBarButton();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.GridFunciones = new Telerik.WinControls.UI.RadGridView();
            this.radCommandBar3 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarFuncion = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarFuncionLabel = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarFuncionSeparador = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarFuncionButtonAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarFuncionButtonEliminar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarFuncionButtonGuardar = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeptos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridPuestos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPuestos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridFunciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridFunciones.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(1156, 30);
            this.radCommandBar1.TabIndex = 0;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarDepartamento});
            // 
            // ToolBarDepartamento
            // 
            this.ToolBarDepartamento.DisplayName = "commandBarStripElement1";
            this.ToolBarDepartamento.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarDeptoLabel,
            this.ToolBarDeptoSeparator,
            this.ToolBarDeptoButtonAgregar,
            this.ToolBarDeptoButtonEliminar,
            this.ToolBarDeptoButtonGuardar,
            this.ToolBarDeptoButtonActualizar,
            this.ToolBarDeptoButtonHerramientas,
            this.ToolBarDeptoButtonCerrar});
            this.ToolBarDepartamento.Name = "ToolBarDepartamento";
            // 
            // ToolBarDeptoLabel
            // 
            this.ToolBarDeptoLabel.DisplayName = "Etiqueta";
            this.ToolBarDeptoLabel.Name = "ToolBarDeptoLabel";
            this.ToolBarDeptoLabel.Text = "Departamentos:";
            // 
            // ToolBarDeptoSeparator
            // 
            this.ToolBarDeptoSeparator.DisplayName = "Separador";
            this.ToolBarDeptoSeparator.Name = "ToolBarDeptoSeparator";
            this.ToolBarDeptoSeparator.VisibleInOverflowMenu = false;
            // 
            // ToolBarDeptoButtonAgregar
            // 
            this.ToolBarDeptoButtonAgregar.DisplayName = "Agregar";
            this.ToolBarDeptoButtonAgregar.DrawText = true;
            this.ToolBarDeptoButtonAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_mas;
            this.ToolBarDeptoButtonAgregar.Name = "ToolBarDeptoButtonAgregar";
            this.ToolBarDeptoButtonAgregar.Text = "Agregar";
            this.ToolBarDeptoButtonAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarDeptoButtonAgregar.Click += new System.EventHandler(this.ToolBarDeptoButtonAgregar_Click);
            // 
            // ToolBarDeptoButtonEliminar
            // 
            this.ToolBarDeptoButtonEliminar.DisplayName = "Eliminar";
            this.ToolBarDeptoButtonEliminar.DrawText = true;
            this.ToolBarDeptoButtonEliminar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_menos;
            this.ToolBarDeptoButtonEliminar.Name = "ToolBarDeptoButtonEliminar";
            this.ToolBarDeptoButtonEliminar.Text = "Eliminar";
            this.ToolBarDeptoButtonEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarDeptoButtonEliminar.Click += new System.EventHandler(this.ToolBarDeptoButtonEliminar_Click);
            // 
            // ToolBarDeptoButtonGuardar
            // 
            this.ToolBarDeptoButtonGuardar.DisplayName = "Guardar";
            this.ToolBarDeptoButtonGuardar.DrawText = true;
            this.ToolBarDeptoButtonGuardar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_guardar;
            this.ToolBarDeptoButtonGuardar.Name = "ToolBarDeptoButtonGuardar";
            this.ToolBarDeptoButtonGuardar.Text = "Guardar";
            this.ToolBarDeptoButtonGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarDeptoButtonGuardar.Click += new System.EventHandler(this.ToolBarDeptoButtonGuardar_Click);
            // 
            // ToolBarDeptoButtonActualizar
            // 
            this.ToolBarDeptoButtonActualizar.DisplayName = "Actualizar";
            this.ToolBarDeptoButtonActualizar.DrawText = true;
            this.ToolBarDeptoButtonActualizar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_actualizar;
            this.ToolBarDeptoButtonActualizar.Name = "ToolBarDeptoButtonActualizar";
            this.ToolBarDeptoButtonActualizar.Text = "Actualizar";
            this.ToolBarDeptoButtonActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarDeptoButtonActualizar.Click += new System.EventHandler(this.ToolBarDeptoButtonActualizar_Click);
            // 
            // ToolBarDeptoButtonHerramientas
            // 
            this.ToolBarDeptoButtonHerramientas.DisplayName = "Herramientas";
            this.ToolBarDeptoButtonHerramientas.DrawText = true;
            this.ToolBarDeptoButtonHerramientas.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_caja_herramientas;
            this.ToolBarDeptoButtonHerramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarDeptoButtonherramientasCrear});
            this.ToolBarDeptoButtonHerramientas.Name = "ToolBarDeptoButtonHerramientas";
            this.ToolBarDeptoButtonHerramientas.Text = "Herramientas";
            this.ToolBarDeptoButtonHerramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarDeptoButtonherramientasCrear
            // 
            this.ToolBarDeptoButtonherramientasCrear.Name = "ToolBarDeptoButtonherramientasCrear";
            this.ToolBarDeptoButtonherramientasCrear.Text = "Crear";
            this.ToolBarDeptoButtonherramientasCrear.Click += new System.EventHandler(this.ToolBarDeptoButtonherramientasCrear_Click);
            // 
            // ToolBarDeptoButtonCerrar
            // 
            this.ToolBarDeptoButtonCerrar.DisplayName = "Cerrar";
            this.ToolBarDeptoButtonCerrar.DrawText = true;
            this.ToolBarDeptoButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_cerrar;
            this.ToolBarDeptoButtonCerrar.Name = "ToolBarDeptoButtonCerrar";
            this.ToolBarDeptoButtonCerrar.Text = "Cerrar";
            this.ToolBarDeptoButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarDeptoButtonCerrar.Click += new System.EventHandler(this.ToolBarDeptoButtonCerrar_Click);
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 30);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1156, 573);
            this.radSplitContainer1.TabIndex = 1;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.GridDeptos);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1156, 245);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.06942003F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -39);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // GridDeptos
            // 
            this.GridDeptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridDeptos.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "A";
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewCheckBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.DataType = typeof(int);
            gridViewTextBoxColumn2.FieldName = "Secuencia";
            gridViewTextBoxColumn2.HeaderText = "Sec.";
            gridViewTextBoxColumn2.Name = "Secuencia";
            gridViewComboBoxColumn1.FieldName = "IdClase";
            gridViewComboBoxColumn1.HeaderText = "Area";
            gridViewComboBoxColumn1.Name = "IdClase";
            gridViewComboBoxColumn1.Width = 300;
            gridViewTextBoxColumn3.FieldName = "Nombre";
            gridViewTextBoxColumn3.HeaderText = "Nombre";
            gridViewTextBoxColumn3.Name = "Nombre";
            gridViewTextBoxColumn3.Width = 240;
            gridViewTextBoxColumn4.FieldName = "Responsable";
            gridViewTextBoxColumn4.HeaderText = "Responsable";
            gridViewTextBoxColumn4.Name = "Responsable";
            gridViewTextBoxColumn4.Width = 200;
            gridViewTextBoxColumn5.FieldName = "Descripcion";
            gridViewTextBoxColumn5.HeaderText = "Descripción";
            gridViewTextBoxColumn5.Name = "Descripcion";
            gridViewTextBoxColumn5.Width = 200;
            this.GridDeptos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.GridDeptos.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridDeptos.Name = "GridDeptos";
            this.GridDeptos.Size = new System.Drawing.Size(1156, 245);
            this.GridDeptos.TabIndex = 0;
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radSplitContainer2);
            this.splitPanel2.Location = new System.Drawing.Point(0, 249);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1156, 324);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.06942003F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 39);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel3);
            this.radSplitContainer2.Controls.Add(this.splitPanel4);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(1156, 324);
            this.radSplitContainer2.TabIndex = 0;
            this.radSplitContainer2.TabStop = false;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.GridPuestos);
            this.splitPanel3.Controls.Add(this.radCommandBar2);
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel3.Size = new System.Drawing.Size(576, 324);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // GridPuestos
            // 
            this.GridPuestos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridPuestos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridPuestos.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn6.DataType = typeof(int);
            gridViewTextBoxColumn6.FieldName = "Id";
            gridViewTextBoxColumn6.HeaderText = "Id";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "Id";
            gridViewTextBoxColumn6.VisibleInColumnChooser = false;
            gridViewTextBoxColumn7.DataType = typeof(int);
            gridViewTextBoxColumn7.FieldName = "IdDepto";
            gridViewTextBoxColumn7.HeaderText = "SubId";
            gridViewTextBoxColumn7.IsVisible = false;
            gridViewTextBoxColumn7.Name = "IdDepto";
            gridViewTextBoxColumn7.VisibleInColumnChooser = false;
            gridViewCheckBoxColumn2.FieldName = "Activo";
            gridViewCheckBoxColumn2.HeaderText = "A";
            gridViewCheckBoxColumn2.Name = "Activo";
            gridViewTextBoxColumn8.DataType = typeof(int);
            gridViewTextBoxColumn8.FieldName = "Secuencia";
            gridViewTextBoxColumn8.HeaderText = "Sec";
            gridViewTextBoxColumn8.Name = "Secuencia";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn9.FieldName = "Descripcion";
            gridViewTextBoxColumn9.HeaderText = "Descripción";
            gridViewTextBoxColumn9.Name = "Descripcion";
            gridViewTextBoxColumn9.Width = 300;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "Salario";
            gridViewTextBoxColumn10.FormatString = "{0:n}";
            gridViewTextBoxColumn10.HeaderText = "Salario Base";
            gridViewTextBoxColumn10.Name = "Salario";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 85;
            gridViewTextBoxColumn11.FieldName = "Creo";
            gridViewTextBoxColumn11.HeaderText = "Creó";
            gridViewTextBoxColumn11.Name = "Creo";
            gridViewTextBoxColumn11.ReadOnly = true;
            gridViewTextBoxColumn11.Width = 85;
            gridViewTextBoxColumn12.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn12.FieldName = "FechaNuevo";
            gridViewTextBoxColumn12.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn12.Name = "FechaNuevo";
            gridViewTextBoxColumn12.ReadOnly = true;
            gridViewTextBoxColumn12.Width = 85;
            this.GridPuestos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewCheckBoxColumn2,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12});
            this.GridPuestos.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GridPuestos.Name = "GridPuestos";
            this.GridPuestos.Size = new System.Drawing.Size(576, 294);
            this.GridPuestos.TabIndex = 1;
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement2});
            this.radCommandBar2.Size = new System.Drawing.Size(576, 30);
            this.radCommandBar2.TabIndex = 0;
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Name = "commandBarRowElement2";
            this.commandBarRowElement2.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarPuesto});
            // 
            // ToolBarPuesto
            // 
            this.ToolBarPuesto.DisplayName = "commandBarStripElement1";
            this.ToolBarPuesto.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarPuestoLabel,
            this.ToolBarPuestoSeparador,
            this.ToolBarPuestoButtonAgregar,
            this.ToolBarPuestoButtonEliminar,
            this.ToolBarPuestoButtonGuardar});
            this.ToolBarPuesto.Name = "ToolBarPuesto";
            // 
            // ToolBarPuestoLabel
            // 
            this.ToolBarPuestoLabel.DisplayName = "commandBarLabel1";
            this.ToolBarPuestoLabel.Name = "ToolBarPuestoLabel";
            this.ToolBarPuestoLabel.Text = "Puestos:";
            // 
            // ToolBarPuestoSeparador
            // 
            this.ToolBarPuestoSeparador.DisplayName = "Separador";
            this.ToolBarPuestoSeparador.Name = "ToolBarPuestoSeparador";
            this.ToolBarPuestoSeparador.VisibleInOverflowMenu = false;
            // 
            // ToolBarPuestoButtonAgregar
            // 
            this.ToolBarPuestoButtonAgregar.DisplayName = "commandBarButton1";
            this.ToolBarPuestoButtonAgregar.DrawText = true;
            this.ToolBarPuestoButtonAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_mas;
            this.ToolBarPuestoButtonAgregar.Name = "ToolBarPuestoButtonAgregar";
            this.ToolBarPuestoButtonAgregar.Text = "Agregar";
            this.ToolBarPuestoButtonAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarPuestoButtonAgregar.Click += new System.EventHandler(this.ToolBarPuestoButtonAgregar_Click);
            // 
            // ToolBarPuestoButtonEliminar
            // 
            this.ToolBarPuestoButtonEliminar.DisplayName = "commandBarButton1";
            this.ToolBarPuestoButtonEliminar.DrawText = true;
            this.ToolBarPuestoButtonEliminar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_menos;
            this.ToolBarPuestoButtonEliminar.Name = "ToolBarPuestoButtonEliminar";
            this.ToolBarPuestoButtonEliminar.Text = "Eliminar";
            this.ToolBarPuestoButtonEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarPuestoButtonEliminar.Click += new System.EventHandler(this.ToolBarPuestoButtonEliminar_Click);
            // 
            // ToolBarPuestoButtonGuardar
            // 
            this.ToolBarPuestoButtonGuardar.DisplayName = "commandBarButton1";
            this.ToolBarPuestoButtonGuardar.DrawText = true;
            this.ToolBarPuestoButtonGuardar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_guardar;
            this.ToolBarPuestoButtonGuardar.Name = "ToolBarPuestoButtonGuardar";
            this.ToolBarPuestoButtonGuardar.Text = "Guardar";
            this.ToolBarPuestoButtonGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarPuestoButtonGuardar.Click += new System.EventHandler(this.ToolBarPuestoButtonGuardar_Click);
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.GridFunciones);
            this.splitPanel4.Controls.Add(this.radCommandBar3);
            this.splitPanel4.Location = new System.Drawing.Point(580, 0);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(576, 324);
            this.splitPanel4.TabIndex = 1;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // GridFunciones
            // 
            this.GridFunciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridFunciones.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridFunciones.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn13.DataType = typeof(int);
            gridViewTextBoxColumn13.FieldName = "Id";
            gridViewTextBoxColumn13.HeaderText = "Id";
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "Id";
            gridViewTextBoxColumn13.VisibleInColumnChooser = false;
            gridViewTextBoxColumn14.DataType = typeof(int);
            gridViewTextBoxColumn14.FieldName = "IdPuesto";
            gridViewTextBoxColumn14.HeaderText = "IdPuesto";
            gridViewTextBoxColumn14.IsVisible = false;
            gridViewTextBoxColumn14.Name = "IdPuesto";
            gridViewTextBoxColumn14.VisibleInColumnChooser = false;
            gridViewTextBoxColumn15.DataType = typeof(int);
            gridViewTextBoxColumn15.FieldName = "Secuencia";
            gridViewTextBoxColumn15.HeaderText = "Sec.";
            gridViewTextBoxColumn15.Name = "Secuencia";
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn15.VisibleInColumnChooser = false;
            gridViewCheckBoxColumn3.FieldName = "Activo";
            gridViewCheckBoxColumn3.HeaderText = "A";
            gridViewCheckBoxColumn3.Name = "Activo";
            gridViewTextBoxColumn16.FieldName = "Descripcion";
            gridViewTextBoxColumn16.HeaderText = "Descripción";
            gridViewTextBoxColumn16.Name = "Descripcion";
            gridViewTextBoxColumn16.Width = 250;
            gridViewTextBoxColumn17.FieldName = "Creo";
            gridViewTextBoxColumn17.HeaderText = "Creó";
            gridViewTextBoxColumn17.Name = "Creo";
            gridViewTextBoxColumn17.Width = 85;
            gridViewTextBoxColumn18.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn18.FieldName = "FechaNuevo";
            gridViewTextBoxColumn18.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn18.Name = "FechaNuevo";
            gridViewTextBoxColumn18.Width = 85;
            this.GridFunciones.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewCheckBoxColumn3,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18});
            this.GridFunciones.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.GridFunciones.Name = "GridFunciones";
            this.GridFunciones.Size = new System.Drawing.Size(576, 294);
            this.GridFunciones.TabIndex = 1;
            // 
            // radCommandBar3
            // 
            this.radCommandBar3.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar3.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar3.Name = "radCommandBar3";
            this.radCommandBar3.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar3.Size = new System.Drawing.Size(576, 30);
            this.radCommandBar3.TabIndex = 0;
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.Name = "commandBarRowElement3";
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarFuncion});
            // 
            // ToolBarFuncion
            // 
            this.ToolBarFuncion.DisplayName = "commandBarStripElement1";
            this.ToolBarFuncion.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarFuncionLabel,
            this.ToolBarFuncionSeparador,
            this.ToolBarFuncionButtonAgregar,
            this.ToolBarFuncionButtonEliminar,
            this.ToolBarFuncionButtonGuardar});
            this.ToolBarFuncion.Name = "ToolBarFuncion";
            // 
            // ToolBarFuncionLabel
            // 
            this.ToolBarFuncionLabel.DisplayName = "Etiqueta";
            this.ToolBarFuncionLabel.Name = "ToolBarFuncionLabel";
            this.ToolBarFuncionLabel.Text = "Funciones:";
            // 
            // ToolBarFuncionSeparador
            // 
            this.ToolBarFuncionSeparador.DisplayName = "Separador";
            this.ToolBarFuncionSeparador.Name = "ToolBarFuncionSeparador";
            this.ToolBarFuncionSeparador.VisibleInOverflowMenu = false;
            // 
            // ToolBarFuncionButtonAgregar
            // 
            this.ToolBarFuncionButtonAgregar.DisplayName = "Agregar";
            this.ToolBarFuncionButtonAgregar.DrawText = true;
            this.ToolBarFuncionButtonAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_mas;
            this.ToolBarFuncionButtonAgregar.Name = "ToolBarFuncionButtonAgregar";
            this.ToolBarFuncionButtonAgregar.Text = "Agregar";
            this.ToolBarFuncionButtonAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarFuncionButtonAgregar.Click += new System.EventHandler(this.ToolBarFuncionButtonAgregar_Click);
            // 
            // ToolBarFuncionButtonEliminar
            // 
            this.ToolBarFuncionButtonEliminar.DisplayName = "Eliminar";
            this.ToolBarFuncionButtonEliminar.DrawText = true;
            this.ToolBarFuncionButtonEliminar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_menos;
            this.ToolBarFuncionButtonEliminar.Name = "ToolBarFuncionButtonEliminar";
            this.ToolBarFuncionButtonEliminar.Text = "Eliminar";
            this.ToolBarFuncionButtonEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarFuncionButtonEliminar.Click += new System.EventHandler(this.ToolBarFuncionButtonEliminar_Click);
            // 
            // ToolBarFuncionButtonGuardar
            // 
            this.ToolBarFuncionButtonGuardar.DisplayName = "Guardar";
            this.ToolBarFuncionButtonGuardar.DrawText = true;
            this.ToolBarFuncionButtonGuardar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_guardar;
            this.ToolBarFuncionButtonGuardar.Name = "ToolBarFuncionButtonGuardar";
            this.ToolBarFuncionButtonGuardar.Text = "Guardar";
            this.ToolBarFuncionButtonGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarFuncionButtonGuardar.Click += new System.EventHandler(this.ToolBarFuncionButtonGuardar_Click);
            // 
            // ViewDepartamentoCatalogo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1156, 603);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "ViewDepartamentoCatalogo";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ViewDepartamentoCatalogo";
            this.Load += new System.EventHandler(this.ViewDepartamentoCatalogo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridDeptos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            this.splitPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridPuestos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPuestos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            this.splitPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridFunciones.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridFunciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBarDepartamento;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarDeptoLabel;
        private Telerik.WinControls.UI.CommandBarSeparator ToolBarDeptoSeparator;
        private Telerik.WinControls.UI.CommandBarButton ToolBarDeptoButtonAgregar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarDeptoButtonEliminar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarDeptoButtonGuardar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarDeptoButtonActualizar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarDeptoButtonCerrar;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadGridView GridDeptos;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.RadGridView GridPuestos;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBarPuesto;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarPuestoLabel;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.RadGridView GridFunciones;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar3;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBarFuncion;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarFuncionLabel;
        private Telerik.WinControls.UI.CommandBarButton ToolBarPuestoButtonAgregar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarPuestoButtonEliminar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarPuestoButtonGuardar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarFuncionButtonAgregar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarFuncionButtonEliminar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarFuncionButtonGuardar;
        private Telerik.WinControls.UI.CommandBarSeparator ToolBarPuestoSeparador;
        private Telerik.WinControls.UI.CommandBarSeparator ToolBarFuncionSeparador;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarDeptoButtonHerramientas;
        private Telerik.WinControls.UI.RadMenuItem ToolBarDeptoButtonherramientasCrear;
    }
}
