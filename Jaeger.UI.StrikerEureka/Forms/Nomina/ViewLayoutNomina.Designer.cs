﻿namespace Jaeger.Views
{
    partial class ViewLayoutNomina
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewLayoutNomina));
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.dotsSpinnerWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement();
            this.BarraEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.BarraEstadoLabel = new Telerik.WinControls.UI.RadLabelElement();
            this.Imagenes = new System.Windows.Forms.ImageList(this.components);
            this.ToolBarNominas = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabelArchivo = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarTextArchivo = new Telerik.WinControls.UI.CommandBarTextBox();
            this.ToolBarButtonAbrir = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarButtonCorreo = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonCorreoTodos = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonCorreoSeleccionado = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.BarRowElementNominas = new Telerik.WinControls.UI.CommandBarRowElement();
            this.CommandBarNominas = new Telerik.WinControls.UI.RadCommandBar();
            this.ToolBarButtonCargar = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            this.GridData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBarNominas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Espera
            // 
            this.Espera.AssociatedControl = this.GridData;
            this.Espera.Location = new System.Drawing.Point(373, 204);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(70, 70);
            this.Espera.TabIndex = 5;
            this.Espera.Text = "radWaitingBar1";
            this.Espera.WaitingIndicators.Add(this.dotsSpinnerWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 100;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsSpinner;
            ((Telerik.WinControls.UI.RadWaitingBarElement)(this.Espera.GetChildAt(0))).WaitingSpeed = 100;
            ((Telerik.WinControls.UI.WaitingBarContentElement)(this.Espera.GetChildAt(0).GetChildAt(0))).WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsSpinner;
            ((Telerik.WinControls.UI.WaitingBarSeparatorElement)(this.Espera.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Dash = false;
            // 
            // GridData
            // 
            this.GridData.Controls.Add(this.Espera);
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 38);
            // 
            // 
            // 
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1086, 632);
            this.GridData.TabIndex = 8;
            // 
            // dotsSpinnerWaitingBarIndicatorElement1
            // 
            this.dotsSpinnerWaitingBarIndicatorElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.dotsSpinnerWaitingBarIndicatorElement1.Name = "dotsSpinnerWaitingBarIndicatorElement1";
            this.dotsSpinnerWaitingBarIndicatorElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.dotsSpinnerWaitingBarIndicatorElement1.UseCompatibleTextRendering = false;
            // 
            // BarraEstado
            // 
            this.BarraEstado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.BarraEstadoLabel});
            this.BarraEstado.Location = new System.Drawing.Point(0, 670);
            this.BarraEstado.Name = "BarraEstado";
            this.BarraEstado.Size = new System.Drawing.Size(1086, 26);
            this.BarraEstado.TabIndex = 7;
            // 
            // BarraEstadoLabel
            // 
            this.BarraEstadoLabel.Name = "BarraEstadoLabel";
            this.BarraEstado.SetSpring(this.BarraEstadoLabel, false);
            this.BarraEstadoLabel.Text = "Listo.";
            this.BarraEstadoLabel.TextWrap = true;
            this.BarraEstadoLabel.UseCompatibleTextRendering = false;
            // 
            // Imagenes
            // 
            this.Imagenes.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Imagenes.ImageStream")));
            this.Imagenes.TransparentColor = System.Drawing.Color.Transparent;
            this.Imagenes.Images.SetKeyName(0, "accept_button.png");
            this.Imagenes.Images.SetKeyName(1, "PDF");
            this.Imagenes.Images.SetKeyName(2, "XML");
            // 
            // ToolBarNominas
            // 
            this.ToolBarNominas.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarNominas.DisplayName = "commandBarStripElement1";
            this.ToolBarNominas.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelArchivo,
            this.ToolBarTextArchivo,
            this.ToolBarButtonAbrir,
            this.ToolBarButtonCargar,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonFiltro,
            this.ToolBarButtonCorreo,
            this.ToolBarButtonCerrar});
            this.ToolBarNominas.Name = "ToolBarNominas";
            this.ToolBarNominas.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarNominas.UseCompatibleTextRendering = false;
            // 
            // ToolBarLabelArchivo
            // 
            this.ToolBarLabelArchivo.DisplayName = "commandBarLabel1";
            this.ToolBarLabelArchivo.Name = "ToolBarLabelArchivo";
            this.ToolBarLabelArchivo.Text = "Descripción:";
            // 
            // ToolBarTextArchivo
            // 
            this.ToolBarTextArchivo.DisplayName = "Examinar";
            this.ToolBarTextArchivo.MinSize = new System.Drawing.Size(260, 0);
            this.ToolBarTextArchivo.Name = "ToolBarTextArchivo";
            this.ToolBarTextArchivo.NullText = "Seleccione";
            this.ToolBarTextArchivo.Text = "";
            // 
            // ToolBarButtonAbrir
            // 
            this.ToolBarButtonAbrir.DisplayName = "commandBarButton1";
            this.ToolBarButtonAbrir.DrawText = true;
            this.ToolBarButtonAbrir.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_importar_csv1;
            this.ToolBarButtonAbrir.Name = "ToolBarButtonAbrir";
            this.ToolBarButtonAbrir.Text = "Abrir";
            this.ToolBarButtonAbrir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonAbrir.Click += new System.EventHandler(this.ToolBarButtonAbrir_Click);
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarButtonActualizar.DisplayName = "Actualizar";
            this.ToolBarButtonActualizar.DrawText = true;
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_actualizar;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonActualizar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarButtonActualizar.UseCompatibleTextRendering = false;
            // 
            // ToolBarButtonFiltro
            // 
            this.ToolBarButtonFiltro.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarButtonFiltro.DisplayName = "commandBarToggleButton1";
            this.ToolBarButtonFiltro.DrawText = true;
            this.ToolBarButtonFiltro.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_filtrar;
            this.ToolBarButtonFiltro.Name = "ToolBarButtonFiltro";
            this.ToolBarButtonFiltro.Text = "Filtro";
            this.ToolBarButtonFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonFiltro.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarButtonFiltro.UseCompatibleTextRendering = false;
            // 
            // ToolBarButtonCorreo
            // 
            this.ToolBarButtonCorreo.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarButtonCorreo.DisplayName = "commandBarDropDownButton1";
            this.ToolBarButtonCorreo.DrawText = true;
            this.ToolBarButtonCorreo.Image = global::Jaeger.UI.Properties.Resources.icons8_x30_enviar;
            this.ToolBarButtonCorreo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonCorreoTodos,
            this.ToolBarButtonCorreoSeleccionado});
            this.ToolBarButtonCorreo.Name = "ToolBarButtonCorreo";
            this.ToolBarButtonCorreo.Text = "Correo";
            this.ToolBarButtonCorreo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCorreo.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarButtonCorreo.UseCompatibleTextRendering = false;
            // 
            // ToolBarButtonCorreoTodos
            // 
            this.ToolBarButtonCorreoTodos.Name = "ToolBarButtonCorreoTodos";
            this.ToolBarButtonCorreoTodos.Text = "Todos";
            this.ToolBarButtonCorreoTodos.UseCompatibleTextRendering = false;
            // 
            // ToolBarButtonCorreoSeleccionado
            // 
            this.ToolBarButtonCorreoSeleccionado.Name = "ToolBarButtonCorreoSeleccionado";
            this.ToolBarButtonCorreoSeleccionado.Text = "Seleccionado";
            this.ToolBarButtonCorreoSeleccionado.UseCompatibleTextRendering = false;
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "commandBarButton1";
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_cerrar;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // BarRowElementNominas
            // 
            this.BarRowElementNominas.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.BarRowElementNominas.MinSize = new System.Drawing.Size(25, 25);
            this.BarRowElementNominas.Name = "BarRowElementNominas";
            this.BarRowElementNominas.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarNominas});
            this.BarRowElementNominas.Text = "";
            this.BarRowElementNominas.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.BarRowElementNominas.UseCompatibleTextRendering = false;
            // 
            // CommandBarNominas
            // 
            this.CommandBarNominas.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBarNominas.Location = new System.Drawing.Point(0, 0);
            this.CommandBarNominas.Name = "CommandBarNominas";
            this.CommandBarNominas.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.BarRowElementNominas});
            this.CommandBarNominas.Size = new System.Drawing.Size(1086, 38);
            this.CommandBarNominas.TabIndex = 6;
            // 
            // ToolBarButtonCargar
            // 
            this.ToolBarButtonCargar.DisplayName = "commandBarButton1";
            this.ToolBarButtonCargar.DrawText = true;
            this.ToolBarButtonCargar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_guardar;
            this.ToolBarButtonCargar.Name = "ToolBarButtonCargar";
            this.ToolBarButtonCargar.Text = "Cargar";
            this.ToolBarButtonCargar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCargar.Click += new System.EventHandler(this.ToolBarButtonCargar_Click);
            // 
            // ViewLayoutNomina
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1086, 696);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.BarraEstado);
            this.Controls.Add(this.CommandBarNominas);
            this.Name = "ViewLayoutNomina";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ViewLayoutNomina";
            this.Load += new System.EventHandler(this.ViewLayoutNomina_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.GridData.ResumeLayout(false);
            this.GridData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBarNominas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement dotsSpinnerWaitingBarIndicatorElement1;
        private Telerik.WinControls.UI.RadStatusStrip BarraEstado;
        private Telerik.WinControls.UI.RadLabelElement BarraEstadoLabel;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonCorreoSeleccionado;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonCorreoTodos;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonCorreo;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltro;
        internal System.Windows.Forms.ImageList Imagenes;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBarNominas;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonActualizar;
        private Telerik.WinControls.UI.CommandBarRowElement BarRowElementNominas;
        private Telerik.WinControls.UI.RadCommandBar CommandBarNominas;
        private Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCerrar;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelArchivo;
        private Telerik.WinControls.UI.CommandBarTextBox ToolBarTextArchivo;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonAbrir;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCargar;
    }
}
