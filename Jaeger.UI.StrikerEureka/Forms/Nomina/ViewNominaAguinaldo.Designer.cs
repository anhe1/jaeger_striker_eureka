﻿namespace Jaeger.Views.Nomina
{
    partial class ViewNominaAguinaldo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject1 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject2 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarButtonCalcular = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonExportar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(1066, 63);
            this.radCommandBar1.TabIndex = 0;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarButtonCalcular,
            this.ToolBarButtonExportar,
            this.ToolBarButtonFiltro});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // ToolBarButtonCalcular
            // 
            this.ToolBarButtonCalcular.DisplayName = "commandBarButton1";
            this.ToolBarButtonCalcular.DrawText = true;
            this.ToolBarButtonCalcular.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_ciclo_vital;
            this.ToolBarButtonCalcular.Name = "ToolBarButtonCalcular";
            this.ToolBarButtonCalcular.Text = "Calcular";
            this.ToolBarButtonCalcular.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCalcular.Click += new System.EventHandler(this.ToolBarButtonCalcular_Click);
            // 
            // ToolBarButtonExportar
            // 
            this.ToolBarButtonExportar.DisplayName = "commandBarButton1";
            this.ToolBarButtonExportar.DrawText = true;
            this.ToolBarButtonExportar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_excel;
            this.ToolBarButtonExportar.Name = "ToolBarButtonExportar";
            this.ToolBarButtonExportar.Text = "Exportar";
            this.ToolBarButtonExportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonExportar.Click += new System.EventHandler(this.ToolBarButtonExportar_Click);
            // 
            // ToolBarButtonFiltro
            // 
            this.ToolBarButtonFiltro.DisplayName = "Filtro";
            this.ToolBarButtonFiltro.DrawText = true;
            this.ToolBarButtonFiltro.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_filtrar;
            this.ToolBarButtonFiltro.Name = "ToolBarButtonFiltro";
            this.ToolBarButtonFiltro.Text = "Filtro";
            this.ToolBarButtonFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonFiltro.Click += new System.EventHandler(this.ToolBarButtonFiltro_Click);
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 63);
            // 
            // 
            // 
            gridViewTextBoxColumn1.FieldName = "Num";
            gridViewTextBoxColumn1.HeaderText = "Núm";
            gridViewTextBoxColumn1.Name = "Num";
            gridViewTextBoxColumn2.FieldName = "Clave";
            gridViewTextBoxColumn2.HeaderText = "Clave";
            gridViewTextBoxColumn2.Name = "Clave";
            gridViewTextBoxColumn2.Width = 60;
            gridViewTextBoxColumn3.FieldName = "Nombre";
            gridViewTextBoxColumn3.HeaderText = "Nombre";
            gridViewTextBoxColumn3.Name = "Nombre";
            gridViewTextBoxColumn3.Width = 220;
            gridViewTextBoxColumn4.FieldName = "DiasLaborados";
            gridViewTextBoxColumn4.HeaderText = "Dias Laborados";
            gridViewTextBoxColumn4.Name = "DiasLaborados";
            gridViewTextBoxColumn4.WrapText = true;
            gridViewTextBoxColumn5.DataType = typeof(decimal);
            gridViewTextBoxColumn5.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn5.FieldName = "SalarioDiario";
            gridViewTextBoxColumn5.FormatString = "{0:n}";
            gridViewTextBoxColumn5.HeaderText = "Salario Diario";
            gridViewTextBoxColumn5.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn5.Name = "SalarioDiario";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn5.Width = 60;
            gridViewTextBoxColumn5.WrapText = true;
            gridViewTextBoxColumn6.DataType = typeof(decimal);
            gridViewTextBoxColumn6.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn6.FieldName = "SalarioMensual";
            gridViewTextBoxColumn6.FormatString = "{0:n}";
            gridViewTextBoxColumn6.HeaderText = "Salario Mensual";
            gridViewTextBoxColumn6.Name = "SalarioMensual";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn6.Width = 60;
            gridViewTextBoxColumn6.WrapText = true;
            gridViewTextBoxColumn7.DataType = typeof(decimal);
            gridViewTextBoxColumn7.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn7.FieldName = "Aguinaldo";
            gridViewTextBoxColumn7.FormatString = "{0:n}";
            gridViewTextBoxColumn7.HeaderText = "Aguinaldo";
            gridViewTextBoxColumn7.Name = "Aguinaldo";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn7.Width = 75;
            gridViewTextBoxColumn7.WrapText = true;
            gridViewTextBoxColumn8.DataType = typeof(decimal);
            gridViewTextBoxColumn8.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn8.FieldName = "BaseGravable";
            gridViewTextBoxColumn8.FormatString = "{0:n}";
            gridViewTextBoxColumn8.HeaderText = "Base Gravable";
            gridViewTextBoxColumn8.Name = "BaseGravable";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn8.Width = 75;
            gridViewTextBoxColumn8.WrapText = true;
            gridViewTextBoxColumn9.DataType = typeof(decimal);
            gridViewTextBoxColumn9.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn9.FieldName = "BaseExento";
            gridViewTextBoxColumn9.FormatString = "{0:n}";
            gridViewTextBoxColumn9.HeaderText = "Base Exento";
            gridViewTextBoxColumn9.Name = "BaseExento";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn9.Width = 75;
            gridViewTextBoxColumn9.WrapText = true;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn10.FieldName = "BaseGravada";
            gridViewTextBoxColumn10.FormatString = "{0:n}";
            gridViewTextBoxColumn10.HeaderText = "Base Gravada";
            gridViewTextBoxColumn10.Name = "BaseGravada";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 75;
            gridViewTextBoxColumn10.WrapText = true;
            gridViewTextBoxColumn11.DataType = typeof(decimal);
            gridViewTextBoxColumn11.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn11.FieldName = "ISR";
            gridViewTextBoxColumn11.FormatString = "{0:n}";
            gridViewTextBoxColumn11.HeaderText = "ISR Ag + Sld";
            gridViewTextBoxColumn11.Name = "ISR";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn11.Width = 75;
            gridViewTextBoxColumn11.WrapText = true;
            gridViewTextBoxColumn12.DataType = typeof(decimal);
            gridViewTextBoxColumn12.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn12.FieldName = "Subsidio";
            gridViewTextBoxColumn12.FormatString = "{0:n}";
            gridViewTextBoxColumn12.HeaderText = "Subsidio";
            gridViewTextBoxColumn12.Name = "Subsidio";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn12.Width = 75;
            gridViewTextBoxColumn12.WrapText = true;
            conditionalFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.CellForeColor = System.Drawing.Color.Red;
            conditionalFormattingObject1.ConditionType = Telerik.WinControls.UI.ConditionTypes.Less;
            conditionalFormattingObject1.Name = "Valor Negativo ISR Salario (+Aguinaldo)";
            conditionalFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.TValue1 = "0";
            gridViewTextBoxColumn13.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            gridViewTextBoxColumn13.DataType = typeof(decimal);
            gridViewTextBoxColumn13.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn13.FieldName = "ISRSalarioMasAguinaldo";
            gridViewTextBoxColumn13.FormatString = "{0:n}";
            gridViewTextBoxColumn13.HeaderText = "ISR Salario Mas Aguinaldo";
            gridViewTextBoxColumn13.Name = "ISRSalarioMasAguinaldo";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn13.Width = 75;
            gridViewTextBoxColumn13.WrapText = true;
            conditionalFormattingObject2.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.CellForeColor = System.Drawing.Color.Red;
            conditionalFormattingObject2.ConditionType = Telerik.WinControls.UI.ConditionTypes.Less;
            conditionalFormattingObject2.Name = "Valor Negativo ISR Salario";
            conditionalFormattingObject2.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.TValue1 = "0";
            gridViewTextBoxColumn14.ConditionalFormattingObjectList.Add(conditionalFormattingObject2);
            gridViewTextBoxColumn14.DataType = typeof(decimal);
            gridViewTextBoxColumn14.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn14.FieldName = "ISRSueldo";
            gridViewTextBoxColumn14.FormatString = "{0:n}";
            gridViewTextBoxColumn14.HeaderText = "ISR Salario";
            gridViewTextBoxColumn14.Name = "ISRSueldo";
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.Width = 75;
            gridViewTextBoxColumn14.WrapText = true;
            gridViewTextBoxColumn15.DataType = typeof(decimal);
            gridViewTextBoxColumn15.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn15.FieldName = "ISRAguinaldo";
            gridViewTextBoxColumn15.FormatString = "{0:n}";
            gridViewTextBoxColumn15.HeaderText = "ISR Aguinaldo";
            gridViewTextBoxColumn15.Name = "ISRAguinaldo";
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn15.Width = 75;
            gridViewTextBoxColumn15.WrapText = true;
            gridViewTextBoxColumn16.DataType = typeof(decimal);
            gridViewTextBoxColumn16.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn16.FieldName = "AguinaldoPagar";
            gridViewTextBoxColumn16.FormatString = "{0:n}";
            gridViewTextBoxColumn16.HeaderText = "Gratificación";
            gridViewTextBoxColumn16.Name = "AguinaldoPagar";
            gridViewTextBoxColumn16.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn16.Width = 75;
            gridViewTextBoxColumn16.WrapText = true;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1066, 476);
            this.GridData.TabIndex = 1;
            // 
            // viewNominaAguinaldo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1066, 539);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "NominaAguinaldo";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Aguinaldo";
            this.Load += new System.EventHandler(this.NominaAguinaldo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCalcular;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonExportar;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltro;
    }
}
