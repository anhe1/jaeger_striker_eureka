namespace Jaeger.Views.Nomina
{
    partial class ViewEmpleadoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn3 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCommandColumn gridViewCommandColumn3 = new Telerik.WinControls.UI.GridViewCommandColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            this.ButtonRFC = new Telerik.WinControls.UI.RadButton();
            this.label24 = new Telerik.WinControls.UI.RadLabel();
            this.TxtCuentaBancaria = new Telerik.WinControls.UI.RadTextBox();
            this.label23 = new Telerik.WinControls.UI.RadLabel();
            this.CboBancos = new Telerik.WinControls.UI.RadDropDownList();
            this.label22 = new Telerik.WinControls.UI.RadLabel();
            this.dtFechaTerminoRelacion = new Telerik.WinControls.UI.RadDateTimePicker();
            this.label21 = new Telerik.WinControls.UI.RadLabel();
            this.TxbNumSeguridadSocial = new Telerik.WinControls.UI.RadTextBox();
            this.label20 = new Telerik.WinControls.UI.RadLabel();
            this.CboRegimenFiscal = new Telerik.WinControls.UI.RadDropDownList();
            this.label19 = new Telerik.WinControls.UI.RadLabel();
            this.CboTipoJornada = new Telerik.WinControls.UI.RadDropDownList();
            this.label18 = new Telerik.WinControls.UI.RadLabel();
            this.CboTipoContrato = new Telerik.WinControls.UI.RadDropDownList();
            this.label17 = new Telerik.WinControls.UI.RadLabel();
            this.label16 = new Telerik.WinControls.UI.RadLabel();
            this.CboRiesgoPuesto = new Telerik.WinControls.UI.RadDropDownList();
            this.label15 = new Telerik.WinControls.UI.RadLabel();
            this.label14 = new Telerik.WinControls.UI.RadLabel();
            this.label13 = new Telerik.WinControls.UI.RadLabel();
            this.CboPuesto = new Telerik.WinControls.UI.RadDropDownList();
            this.CboDepartamento = new Telerik.WinControls.UI.RadDropDownList();
            this.label12 = new Telerik.WinControls.UI.RadLabel();
            this.label11 = new Telerik.WinControls.UI.RadLabel();
            this.CboGenero = new Telerik.WinControls.UI.RadDropDownList();
            this.DtFechaInicioRel = new Telerik.WinControls.UI.RadDateTimePicker();
            this.DtFechaNacimiento = new Telerik.WinControls.UI.RadDateTimePicker();
            this.label10 = new Telerik.WinControls.UI.RadLabel();
            this.label9 = new Telerik.WinControls.UI.RadLabel();
            this.label8 = new Telerik.WinControls.UI.RadLabel();
            this.label7 = new Telerik.WinControls.UI.RadLabel();
            this.label6 = new Telerik.WinControls.UI.RadLabel();
            this.label5 = new Telerik.WinControls.UI.RadLabel();
            this.label4 = new Telerik.WinControls.UI.RadLabel();
            this.label3 = new Telerik.WinControls.UI.RadLabel();
            this.label2 = new Telerik.WinControls.UI.RadLabel();
            this.label1 = new Telerik.WinControls.UI.RadLabel();
            this.TxbSitio = new Telerik.WinControls.UI.RadTextBox();
            this.TxbCorreo = new Telerik.WinControls.UI.RadTextBox();
            this.TxbTelefono = new Telerik.WinControls.UI.RadTextBox();
            this.TxbSegundoApellido = new Telerik.WinControls.UI.RadTextBox();
            this.TxbPrimerApellido = new Telerik.WinControls.UI.RadTextBox();
            this.TxbNombre = new Telerik.WinControls.UI.RadTextBox();
            this.TxbRFC = new Telerik.WinControls.UI.RadTextBox();
            this.TxbCURP = new Telerik.WinControls.UI.RadTextBox();
            this.TxbClave = new Telerik.WinControls.UI.RadTextBox();
            this.TxbNum = new Telerik.WinControls.UI.RadTextBox();
            this.label25 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.switchActivo = new Telerik.WinControls.UI.RadToggleSwitch();
            this.CboPerioricidadPago = new Telerik.WinControls.UI.RadDropDownList();
            this.label26 = new Telerik.WinControls.UI.RadLabel();
            this.TxbSalarioBase = new Telerik.WinControls.UI.RadTextBox();
            this.TxbSalarioDiario = new Telerik.WinControls.UI.RadTextBox();
            this.label31 = new Telerik.WinControls.UI.RadLabel();
            this.label32 = new Telerik.WinControls.UI.RadLabel();
            this.label27 = new Telerik.WinControls.UI.RadLabel();
            this.TxtSalarioDiarioIntegrado = new Telerik.WinControls.UI.RadTextBox();
            this.comboBox3 = new Telerik.WinControls.UI.RadDropDownList();
            this.label35 = new Telerik.WinControls.UI.RadLabel();
            this.TxtCuentaValesDespensa = new Telerik.WinControls.UI.RadTextBox();
            this.label33 = new Telerik.WinControls.UI.RadLabel();
            this.TxtHorasJornada = new Telerik.WinControls.UI.RadTextBox();
            this.label34 = new Telerik.WinControls.UI.RadLabel();
            this.TxtJornadasTrabajo = new Telerik.WinControls.UI.RadTextBox();
            this.label30 = new Telerik.WinControls.UI.RadLabel();
            this.TxbAfore = new Telerik.WinControls.UI.RadTextBox();
            this.label29 = new Telerik.WinControls.UI.RadLabel();
            this.TxbNumFonacot = new Telerik.WinControls.UI.RadTextBox();
            this.label28 = new Telerik.WinControls.UI.RadLabel();
            this.CboFormaPago = new Telerik.WinControls.UI.RadDropDownList();
            this.label36 = new Telerik.WinControls.UI.RadLabel();
            this.TxbPremiosPorPuntualidad = new Telerik.WinControls.UI.RadTextBox();
            this.label37 = new Telerik.WinControls.UI.RadLabel();
            this.TxbBecasTrabajadores = new Telerik.WinControls.UI.RadTextBox();
            this.label38 = new Telerik.WinControls.UI.RadLabel();
            this.TxbOtrosPremiosAsistencia = new Telerik.WinControls.UI.RadTextBox();
            this.label39 = new Telerik.WinControls.UI.RadLabel();
            this.TxbAyudaArticulosEscolares = new Telerik.WinControls.UI.RadTextBox();
            this.label40 = new Telerik.WinControls.UI.RadLabel();
            this.TxbAyudaTransporte = new Telerik.WinControls.UI.RadTextBox();
            this.tabControl = new Telerik.WinControls.UI.RadPageView();
            this.pageGeneral = new Telerik.WinControls.UI.RadPageViewPage();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.TxbTelefono2 = new Telerik.WinControls.UI.RadTextBox();
            this.TxbUMF = new Telerik.WinControls.UI.RadTextBox();
            this.label49 = new Telerik.WinControls.UI.RadLabel();
            this.CboTipoEmpleado = new Telerik.WinControls.UI.RadDropDownList();
            this.label45 = new Telerik.WinControls.UI.RadLabel();
            this.label43 = new Telerik.WinControls.UI.RadLabel();
            this.textBox2 = new Telerik.WinControls.UI.RadTextBox();
            this.label44 = new Telerik.WinControls.UI.RadLabel();
            this.CboEntidadFedNacimiento = new Telerik.WinControls.UI.RadDropDownList();
            this.pagePrestacion = new Telerik.WinControls.UI.RadPageViewPage();
            this.label42 = new Telerik.WinControls.UI.RadLabel();
            this.CboTablaProductividad = new Telerik.WinControls.UI.RadDropDownList();
            this.label41 = new Telerik.WinControls.UI.RadLabel();
            this.TxbPagoCreditoInfonavit = new Telerik.WinControls.UI.RadTextBox();
            this.pagePago = new Telerik.WinControls.UI.RadPageViewPage();
            this.CboTipoCuenta = new Telerik.WinControls.UI.RadDropDownList();
            this.label48 = new Telerik.WinControls.UI.RadLabel();
            this.label47 = new Telerik.WinControls.UI.RadLabel();
            this.TxbBancoSucursal = new Telerik.WinControls.UI.RadTextBox();
            this.label46 = new Telerik.WinControls.UI.RadLabel();
            this.TxbCuentaCLABE = new Telerik.WinControls.UI.RadTextBox();
            this.pageContrato = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridContratos = new Telerik.WinControls.UI.RadGridView();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarButtonContratoNuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonContratoEliminar = new Telerik.WinControls.UI.CommandBarButton();
            this.pageDocumento = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridDocumentos = new Telerik.WinControls.UI.RadGridView();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarButtonDocumentosAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonDocumentosEditar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonDocumentosEliminar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonDocumentosDescargar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarcommandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarEmpleado = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabelActivo = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarHostActivo = new Telerik.WinControls.UI.CommandBarHostItem();
            this.ToolBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarButtonNuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonGuardar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonImprimir = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBar = new Telerik.WinControls.UI.RadCommandBar();
            this.Encabezado = new System.Windows.Forms.PictureBox();
            this.StatusBar = new Telerik.WinControls.UI.RadStatusStrip();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBarElement();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCuentaBancaria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboBancos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFechaTerminoRelacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumSeguridadSocial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboRegimenFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoJornada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoContrato)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboRiesgoPuesto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboPuesto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDepartamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboGenero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtFechaInicioRel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtFechaNacimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSitio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCorreo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTelefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSegundoApellido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbPrimerApellido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbClave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox)).BeginInit();
            this.groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.switchActivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboPerioricidadPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSalarioBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSalarioDiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalarioDiarioIntegrado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCuentaValesDespensa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHorasJornada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJornadasTrabajo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbAfore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumFonacot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbPremiosPorPuntualidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbBecasTrabajadores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbOtrosPremiosAsistencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbAyudaArticulosEscolares)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbAyudaTransporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.pageGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTelefono2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbUMF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoEmpleado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEntidadFedNacimiento)).BeginInit();
            this.pagePrestacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.label42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTablaProductividad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbPagoCreditoInfonavit)).BeginInit();
            this.pagePago.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoCuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbBancoSucursal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCuentaCLABE)).BeginInit();
            this.pageContrato.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridContratos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridContratos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            this.pageDocumento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDocumentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDocumentos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToolBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonRFC
            // 
            this.ButtonRFC.Location = new System.Drawing.Point(138, 192);
            this.ButtonRFC.Name = "ButtonRFC";
            this.ButtonRFC.Size = new System.Drawing.Size(38, 23);
            this.ButtonRFC.TabIndex = 101;
            this.ButtonRFC.Text = "RFC";
            this.ButtonRFC.Click += new System.EventHandler(this.ButtonRFC_Click);
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(10, 110);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(116, 18);
            this.label24.TabIndex = 100;
            this.label24.Text = "Núm. Cuenta / Tarjeta";
            // 
            // TxtCuentaBancaria
            // 
            this.TxtCuentaBancaria.Location = new System.Drawing.Point(10, 134);
            this.TxtCuentaBancaria.MaxLength = 20;
            this.TxtCuentaBancaria.Name = "TxtCuentaBancaria";
            this.TxtCuentaBancaria.Size = new System.Drawing.Size(125, 20);
            this.TxtCuentaBancaria.TabIndex = 99;
            this.TxtCuentaBancaria.Text = "123456789012345678";
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(10, 62);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(39, 18);
            this.label23.TabIndex = 98;
            this.label23.Text = "Banco:";
            // 
            // CboBancos
            // 
            this.CboBancos.Location = new System.Drawing.Point(10, 86);
            this.CboBancos.Name = "CboBancos";
            this.CboBancos.Size = new System.Drawing.Size(434, 20);
            this.CboBancos.TabIndex = 97;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(499, 218);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(171, 18);
            this.label22.TabIndex = 95;
            this.label22.Text = "Fec. Termino de Relación Laboral";
            // 
            // dtFechaTerminoRelacion
            // 
            this.dtFechaTerminoRelacion.Location = new System.Drawing.Point(502, 236);
            this.dtFechaTerminoRelacion.Name = "dtFechaTerminoRelacion";
            this.dtFechaTerminoRelacion.Size = new System.Drawing.Size(168, 20);
            this.dtFechaTerminoRelacion.TabIndex = 94;
            this.dtFechaTerminoRelacion.TabStop = false;
            this.dtFechaTerminoRelacion.Text = "lunes, 21 de enero de 2019";
            this.dtFechaTerminoRelacion.Value = new System.DateTime(2019, 1, 21, 13, 51, 43, 93);
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(270, 92);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(122, 18);
            this.label21.TabIndex = 93;
            this.label21.Text = "Núm. Seguridad Social:";
            // 
            // TxbNumSeguridadSocial
            // 
            this.TxbNumSeguridadSocial.Location = new System.Drawing.Point(270, 110);
            this.TxbNumSeguridadSocial.Name = "TxbNumSeguridadSocial";
            this.TxbNumSeguridadSocial.Size = new System.Drawing.Size(125, 20);
            this.TxbNumSeguridadSocial.TabIndex = 92;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(5, 50);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(94, 18);
            this.label20.TabIndex = 91;
            this.label20.Text = "Tipo de Regimen:";
            // 
            // CboRegimenFiscal
            // 
            this.CboRegimenFiscal.Location = new System.Drawing.Point(8, 68);
            this.CboRegimenFiscal.Name = "CboRegimenFiscal";
            this.CboRegimenFiscal.Size = new System.Drawing.Size(256, 20);
            this.CboRegimenFiscal.TabIndex = 90;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(499, 50);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(86, 18);
            this.label19.TabIndex = 89;
            this.label19.Text = "Tipo de Jornada";
            // 
            // CboTipoJornada
            // 
            this.CboTipoJornada.Location = new System.Drawing.Point(502, 68);
            this.CboTipoJornada.Name = "CboTipoJornada";
            this.CboTipoJornada.Size = new System.Drawing.Size(168, 20);
            this.CboTipoJornada.TabIndex = 88;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(6, 133);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(93, 18);
            this.label18.TabIndex = 87;
            this.label18.Text = "Tipo de Contrato:";
            // 
            // CboTipoContrato
            // 
            this.CboTipoContrato.Location = new System.Drawing.Point(9, 151);
            this.CboTipoContrato.Name = "CboTipoContrato";
            this.CboTipoContrato.Size = new System.Drawing.Size(369, 20);
            this.CboTipoContrato.TabIndex = 86;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(6, 91);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(112, 18);
            this.label17.TabIndex = 85;
            this.label17.Text = "Perioricidad de Pago:";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(267, 50);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(95, 18);
            this.label16.TabIndex = 83;
            this.label16.Text = "Riesgo de Puesto:";
            // 
            // CboRiesgoPuesto
            // 
            this.CboRiesgoPuesto.Location = new System.Drawing.Point(270, 68);
            this.CboRiesgoPuesto.Name = "CboRiesgoPuesto";
            this.CboRiesgoPuesto.Size = new System.Drawing.Size(226, 20);
            this.CboRiesgoPuesto.TabIndex = 82;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(313, 5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(166, 18);
            this.label15.TabIndex = 81;
            this.label15.Text = "Fecha Inicio de Relación Laboral";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(215, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 18);
            this.label14.TabIndex = 80;
            this.label14.Text = "Puesto:";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(6, 8);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 18);
            this.label13.TabIndex = 79;
            this.label13.Text = "Departamento:";
            // 
            // CboPuesto
            // 
            this.CboPuesto.Location = new System.Drawing.Point(218, 26);
            this.CboPuesto.Name = "CboPuesto";
            this.CboPuesto.Size = new System.Drawing.Size(278, 20);
            this.CboPuesto.TabIndex = 78;
            // 
            // CboDepartamento
            // 
            this.CboDepartamento.Location = new System.Drawing.Point(9, 26);
            this.CboDepartamento.Name = "CboDepartamento";
            this.CboDepartamento.Size = new System.Drawing.Size(200, 20);
            this.CboDepartamento.TabIndex = 77;
            this.CboDepartamento.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CboDepartamento_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(136, 133);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(114, 18);
            this.label12.TabIndex = 76;
            this.label12.Text = "Fecha de Nacimiento:";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(6, 133);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 18);
            this.label11.TabIndex = 75;
            this.label11.Text = "Genero:";
            // 
            // CboGenero
            // 
            this.CboGenero.Location = new System.Drawing.Point(9, 151);
            this.CboGenero.Name = "CboGenero";
            this.CboGenero.Size = new System.Drawing.Size(125, 20);
            this.CboGenero.TabIndex = 74;
            // 
            // DtFechaInicioRel
            // 
            this.DtFechaInicioRel.Location = new System.Drawing.Point(313, 27);
            this.DtFechaInicioRel.Name = "DtFechaInicioRel";
            this.DtFechaInicioRel.Size = new System.Drawing.Size(184, 20);
            this.DtFechaInicioRel.TabIndex = 73;
            this.DtFechaInicioRel.TabStop = false;
            this.DtFechaInicioRel.Text = "lunes, 21 de enero de 2019";
            this.DtFechaInicioRel.Value = new System.DateTime(2019, 1, 21, 13, 51, 43, 167);
            // 
            // DtFechaNacimiento
            // 
            this.DtFechaNacimiento.Location = new System.Drawing.Point(139, 151);
            this.DtFechaNacimiento.Name = "DtFechaNacimiento";
            this.DtFechaNacimiento.Size = new System.Drawing.Size(175, 20);
            this.DtFechaNacimiento.TabIndex = 72;
            this.DtFechaNacimiento.TabStop = false;
            this.DtFechaNacimiento.Text = "lunes, 21 de enero de 2019";
            this.DtFechaNacimiento.Value = new System.DateTime(2019, 1, 21, 13, 51, 43, 174);
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(270, 218);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 18);
            this.label10.TabIndex = 71;
            this.label10.Text = "Sitio:";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(7, 218);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 18);
            this.label9.TabIndex = 70;
            this.label9.Text = "Correo:";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(320, 175);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 18);
            this.label8.TabIndex = 69;
            this.label8.Text = "Telefono 1:";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(335, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 18);
            this.label7.TabIndex = 68;
            this.label7.Text = "Apellido Materno:";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(169, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 18);
            this.label6.TabIndex = 67;
            this.label6.Text = "Apellido Paterno:";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(6, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 18);
            this.label5.TabIndex = 66;
            this.label5.Text = "Nombre(s):";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(6, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 18);
            this.label4.TabIndex = 65;
            this.label4.Text = "RFC (F7):";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(179, 175);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 18);
            this.label3.TabIndex = 64;
            this.label3.Text = "CURP (F7):";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(106, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 18);
            this.label2.TabIndex = 63;
            this.label2.Text = "Clave:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 18);
            this.label1.TabIndex = 62;
            this.label1.Text = "Núm.:";
            // 
            // TxbSitio
            // 
            this.TxbSitio.Location = new System.Drawing.Point(270, 236);
            this.TxbSitio.Name = "TxbSitio";
            this.TxbSitio.Size = new System.Drawing.Size(226, 20);
            this.TxbSitio.TabIndex = 61;
            // 
            // TxbCorreo
            // 
            this.TxbCorreo.Location = new System.Drawing.Point(9, 236);
            this.TxbCorreo.Name = "TxbCorreo";
            this.TxbCorreo.Size = new System.Drawing.Size(256, 20);
            this.TxbCorreo.TabIndex = 60;
            // 
            // TxbTelefono
            // 
            this.TxbTelefono.Location = new System.Drawing.Point(320, 193);
            this.TxbTelefono.Name = "TxbTelefono";
            this.TxbTelefono.Size = new System.Drawing.Size(176, 20);
            this.TxbTelefono.TabIndex = 59;
            // 
            // TxbSegundoApellido
            // 
            this.TxbSegundoApellido.Location = new System.Drawing.Point(335, 68);
            this.TxbSegundoApellido.Name = "TxbSegundoApellido";
            this.TxbSegundoApellido.Size = new System.Drawing.Size(162, 20);
            this.TxbSegundoApellido.TabIndex = 58;
            // 
            // TxbPrimerApellido
            // 
            this.TxbPrimerApellido.Location = new System.Drawing.Point(172, 68);
            this.TxbPrimerApellido.Name = "TxbPrimerApellido";
            this.TxbPrimerApellido.Size = new System.Drawing.Size(157, 20);
            this.TxbPrimerApellido.TabIndex = 57;
            // 
            // TxbNombre
            // 
            this.TxbNombre.Location = new System.Drawing.Point(9, 68);
            this.TxbNombre.Name = "TxbNombre";
            this.TxbNombre.Size = new System.Drawing.Size(157, 20);
            this.TxbNombre.TabIndex = 56;
            // 
            // TxbRFC
            // 
            this.TxbRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxbRFC.Location = new System.Drawing.Point(9, 193);
            this.TxbRFC.MaxLength = 16;
            this.TxbRFC.Name = "TxbRFC";
            this.TxbRFC.Size = new System.Drawing.Size(125, 20);
            this.TxbRFC.TabIndex = 55;
            this.TxbRFC.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxbRFC_KeyDown);
            // 
            // TxbCURP
            // 
            this.TxbCURP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxbCURP.Location = new System.Drawing.Point(182, 193);
            this.TxbCURP.MaxLength = 18;
            this.TxbCURP.Name = "TxbCURP";
            this.TxbCURP.Size = new System.Drawing.Size(132, 20);
            this.TxbCURP.TabIndex = 54;
            this.TxbCURP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxbCURP_KeyDown);
            // 
            // TxbClave
            // 
            this.TxbClave.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxbClave.Location = new System.Drawing.Point(149, 18);
            this.TxbClave.Name = "TxbClave";
            this.TxbClave.Size = new System.Drawing.Size(64, 20);
            this.TxbClave.TabIndex = 53;
            // 
            // TxbNum
            // 
            this.TxbNum.Location = new System.Drawing.Point(47, 18);
            this.TxbNum.Name = "TxbNum";
            this.TxbNum.ReadOnly = true;
            this.TxbNum.Size = new System.Drawing.Size(53, 20);
            this.TxbNum.TabIndex = 52;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(46, 12);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(195, 18);
            this.label25.TabIndex = 104;
            this.label25.Text = "Información del Empleado Registrado";
            // 
            // groupBox
            // 
            this.groupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox.Controls.Add(this.switchActivo);
            this.groupBox.Controls.Add(this.CboPerioricidadPago);
            this.groupBox.Controls.Add(this.label1);
            this.groupBox.Controls.Add(this.TxbNum);
            this.groupBox.Controls.Add(this.TxbClave);
            this.groupBox.Controls.Add(this.label26);
            this.groupBox.Controls.Add(this.TxbNombre);
            this.groupBox.Controls.Add(this.TxbPrimerApellido);
            this.groupBox.Controls.Add(this.TxbSalarioBase);
            this.groupBox.Controls.Add(this.TxbSegundoApellido);
            this.groupBox.Controls.Add(this.label2);
            this.groupBox.Controls.Add(this.label5);
            this.groupBox.Controls.Add(this.TxbSalarioDiario);
            this.groupBox.Controls.Add(this.label6);
            this.groupBox.Controls.Add(this.label7);
            this.groupBox.Controls.Add(this.label31);
            this.groupBox.Controls.Add(this.label32);
            this.groupBox.Controls.Add(this.label27);
            this.groupBox.Controls.Add(this.TxtSalarioDiarioIntegrado);
            this.groupBox.Controls.Add(this.CboTipoContrato);
            this.groupBox.Controls.Add(this.comboBox3);
            this.groupBox.Controls.Add(this.DtFechaInicioRel);
            this.groupBox.Controls.Add(this.label18);
            this.groupBox.Controls.Add(this.label15);
            this.groupBox.Controls.Add(this.label17);
            this.groupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox.Enabled = false;
            this.groupBox.HeaderText = "";
            this.groupBox.Location = new System.Drawing.Point(0, 63);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(701, 184);
            this.groupBox.TabIndex = 106;
            this.groupBox.TabStop = false;
            // 
            // switchActivo
            // 
            this.switchActivo.Location = new System.Drawing.Point(580, 151);
            this.switchActivo.Name = "switchActivo";
            this.switchActivo.Size = new System.Drawing.Size(50, 20);
            this.switchActivo.TabIndex = 110;
            // 
            // CboPerioricidadPago
            // 
            this.CboPerioricidadPago.Location = new System.Drawing.Point(9, 111);
            this.CboPerioricidadPago.Name = "CboPerioricidadPago";
            this.CboPerioricidadPago.Size = new System.Drawing.Size(157, 20);
            this.CboPerioricidadPago.TabIndex = 109;
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(384, 133);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(68, 18);
            this.label26.TabIndex = 104;
            this.label26.Text = "Salario Base:";
            // 
            // TxbSalarioBase
            // 
            this.TxbSalarioBase.Location = new System.Drawing.Point(384, 151);
            this.TxbSalarioBase.Name = "TxbSalarioBase";
            this.TxbSalarioBase.Size = new System.Drawing.Size(113, 20);
            this.TxbSalarioBase.TabIndex = 102;
            this.TxbSalarioBase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxbSalarioDiario
            // 
            this.TxbSalarioDiario.Location = new System.Drawing.Point(172, 110);
            this.TxbSalarioDiario.Name = "TxbSalarioDiario";
            this.TxbSalarioDiario.Size = new System.Drawing.Size(100, 20);
            this.TxbSalarioDiario.TabIndex = 103;
            this.TxbSalarioDiario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label31
            // 
            this.label31.Location = new System.Drawing.Point(275, 91);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(98, 18);
            this.label31.TabIndex = 108;
            this.label31.Text = "S. Base Cotización:";
            // 
            // label32
            // 
            this.label32.Location = new System.Drawing.Point(381, 90);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(100, 18);
            this.label32.TabIndex = 108;
            this.label32.Text = "Base de Cotización";
            // 
            // label27
            // 
            this.label27.Location = new System.Drawing.Point(169, 92);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(75, 18);
            this.label27.TabIndex = 105;
            this.label27.Text = "Salario Diario:";
            // 
            // TxtSalarioDiarioIntegrado
            // 
            this.TxtSalarioDiarioIntegrado.Location = new System.Drawing.Point(278, 109);
            this.TxtSalarioDiarioIntegrado.Name = "TxtSalarioDiarioIntegrado";
            this.TxtSalarioDiarioIntegrado.Size = new System.Drawing.Size(100, 20);
            this.TxtSalarioDiarioIntegrado.TabIndex = 107;
            this.TxtSalarioDiarioIntegrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // comboBox3
            // 
            this.comboBox3.Location = new System.Drawing.Point(384, 108);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(113, 20);
            this.comboBox3.TabIndex = 107;
            // 
            // label35
            // 
            this.label35.Location = new System.Drawing.Point(10, 160);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(121, 18);
            this.label35.TabIndex = 115;
            this.label35.Text = "Tarjeta Vales Despensa";
            // 
            // TxtCuentaValesDespensa
            // 
            this.TxtCuentaValesDespensa.Location = new System.Drawing.Point(10, 184);
            this.TxtCuentaValesDespensa.MaxLength = 18;
            this.TxtCuentaValesDespensa.Name = "TxtCuentaValesDespensa";
            this.TxtCuentaValesDespensa.Size = new System.Drawing.Size(125, 20);
            this.TxtCuentaValesDespensa.TabIndex = 114;
            // 
            // label33
            // 
            this.label33.Location = new System.Drawing.Point(104, 46);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(105, 18);
            this.label33.TabIndex = 113;
            this.label33.Text = "Horas de la Jornada";
            // 
            // TxtHorasJornada
            // 
            this.TxtHorasJornada.Location = new System.Drawing.Point(15, 43);
            this.TxtHorasJornada.Name = "TxtHorasJornada";
            this.TxtHorasJornada.Size = new System.Drawing.Size(83, 20);
            this.TxtHorasJornada.TabIndex = 111;
            this.TxtHorasJornada.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label34
            // 
            this.label34.Location = new System.Drawing.Point(104, 20);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(106, 18);
            this.label34.TabIndex = 112;
            this.label34.Text = "Jornadas de Trabajo";
            // 
            // TxtJornadasTrabajo
            // 
            this.TxtJornadasTrabajo.Location = new System.Drawing.Point(15, 17);
            this.TxtJornadasTrabajo.Name = "TxtJornadasTrabajo";
            this.TxtJornadasTrabajo.Size = new System.Drawing.Size(83, 20);
            this.TxtJornadasTrabajo.TabIndex = 110;
            this.TxtJornadasTrabajo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label30
            // 
            this.label30.Location = new System.Drawing.Point(137, 92);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(34, 18);
            this.label30.TabIndex = 108;
            this.label30.Text = "Afore";
            // 
            // TxbAfore
            // 
            this.TxbAfore.Location = new System.Drawing.Point(140, 110);
            this.TxbAfore.Name = "TxbAfore";
            this.TxbAfore.Size = new System.Drawing.Size(125, 20);
            this.TxbAfore.TabIndex = 107;
            // 
            // label29
            // 
            this.label29.Location = new System.Drawing.Point(6, 92);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(88, 18);
            this.label29.TabIndex = 109;
            this.label29.Text = "Núm. FONACOT";
            // 
            // TxbNumFonacot
            // 
            this.TxbNumFonacot.Location = new System.Drawing.Point(9, 110);
            this.TxbNumFonacot.Name = "TxbNumFonacot";
            this.TxbNumFonacot.Size = new System.Drawing.Size(125, 20);
            this.TxbNumFonacot.TabIndex = 108;
            // 
            // label28
            // 
            this.label28.Location = new System.Drawing.Point(10, 14);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(93, 18);
            this.label28.TabIndex = 107;
            this.label28.Text = "Método de Pago:";
            // 
            // CboFormaPago
            // 
            this.CboFormaPago.Location = new System.Drawing.Point(10, 38);
            this.CboFormaPago.Name = "CboFormaPago";
            this.CboFormaPago.Size = new System.Drawing.Size(200, 20);
            this.CboFormaPago.TabIndex = 106;
            // 
            // label36
            // 
            this.label36.Location = new System.Drawing.Point(104, 72);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(130, 18);
            this.label36.TabIndex = 114;
            this.label36.Text = "Premios por puntualidad";
            // 
            // TxbPremiosPorPuntualidad
            // 
            this.TxbPremiosPorPuntualidad.Location = new System.Drawing.Point(15, 69);
            this.TxbPremiosPorPuntualidad.Name = "TxbPremiosPorPuntualidad";
            this.TxbPremiosPorPuntualidad.Size = new System.Drawing.Size(83, 20);
            this.TxbPremiosPorPuntualidad.TabIndex = 113;
            this.TxbPremiosPorPuntualidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label37
            // 
            this.label37.Location = new System.Drawing.Point(104, 98);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(171, 18);
            this.label37.TabIndex = 116;
            this.label37.Text = "Becas para trabajadores y/o hijos";
            // 
            // TxbBecasTrabajadores
            // 
            this.TxbBecasTrabajadores.Location = new System.Drawing.Point(15, 95);
            this.TxbBecasTrabajadores.Name = "TxbBecasTrabajadores";
            this.TxbBecasTrabajadores.Size = new System.Drawing.Size(83, 20);
            this.TxbBecasTrabajadores.TabIndex = 115;
            this.TxbBecasTrabajadores.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(104, 124);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(153, 18);
            this.label38.TabIndex = 118;
            this.label38.Text = "Otros; Premios por Asistencia";
            // 
            // TxbOtrosPremiosAsistencia
            // 
            this.TxbOtrosPremiosAsistencia.Location = new System.Drawing.Point(15, 121);
            this.TxbOtrosPremiosAsistencia.Name = "TxbOtrosPremiosAsistencia";
            this.TxbOtrosPremiosAsistencia.Size = new System.Drawing.Size(83, 20);
            this.TxbOtrosPremiosAsistencia.TabIndex = 117;
            this.TxbOtrosPremiosAsistencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label39
            // 
            this.label39.Location = new System.Drawing.Point(104, 150);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(162, 18);
            this.label39.TabIndex = 120;
            this.label39.Text = "Ayuda para artículos escolares\r\n";
            // 
            // TxbAyudaArticulosEscolares
            // 
            this.TxbAyudaArticulosEscolares.Location = new System.Drawing.Point(15, 147);
            this.TxbAyudaArticulosEscolares.Name = "TxbAyudaArticulosEscolares";
            this.TxbAyudaArticulosEscolares.Size = new System.Drawing.Size(83, 20);
            this.TxbAyudaArticulosEscolares.TabIndex = 119;
            this.TxbAyudaArticulosEscolares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label40
            // 
            this.label40.Location = new System.Drawing.Point(104, 176);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(118, 18);
            this.label40.TabIndex = 122;
            this.label40.Text = "Ayuda para transporte";
            // 
            // TxbAyudaTransporte
            // 
            this.TxbAyudaTransporte.Location = new System.Drawing.Point(15, 173);
            this.TxbAyudaTransporte.Name = "TxbAyudaTransporte";
            this.TxbAyudaTransporte.Size = new System.Drawing.Size(83, 20);
            this.TxbAyudaTransporte.TabIndex = 121;
            this.TxbAyudaTransporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.pageGeneral);
            this.tabControl.Controls.Add(this.pagePrestacion);
            this.tabControl.Controls.Add(this.pagePago);
            this.tabControl.Controls.Add(this.pageContrato);
            this.tabControl.Controls.Add(this.pageDocumento);
            this.tabControl.DefaultPage = this.pageGeneral;
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl.Enabled = false;
            this.tabControl.Location = new System.Drawing.Point(0, 247);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedPage = this.pageGeneral;
            this.tabControl.Size = new System.Drawing.Size(701, 316);
            this.tabControl.TabIndex = 123;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.tabControl.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // pageGeneral
            // 
            this.pageGeneral.Controls.Add(this.radLabel1);
            this.pageGeneral.Controls.Add(this.TxbTelefono2);
            this.pageGeneral.Controls.Add(this.TxbUMF);
            this.pageGeneral.Controls.Add(this.label49);
            this.pageGeneral.Controls.Add(this.label8);
            this.pageGeneral.Controls.Add(this.CboTipoEmpleado);
            this.pageGeneral.Controls.Add(this.label45);
            this.pageGeneral.Controls.Add(this.label43);
            this.pageGeneral.Controls.Add(this.textBox2);
            this.pageGeneral.Controls.Add(this.label13);
            this.pageGeneral.Controls.Add(this.label44);
            this.pageGeneral.Controls.Add(this.CboDepartamento);
            this.pageGeneral.Controls.Add(this.CboEntidadFedNacimiento);
            this.pageGeneral.Controls.Add(this.label14);
            this.pageGeneral.Controls.Add(this.label22);
            this.pageGeneral.Controls.Add(this.label30);
            this.pageGeneral.Controls.Add(this.CboPuesto);
            this.pageGeneral.Controls.Add(this.TxbCURP);
            this.pageGeneral.Controls.Add(this.CboRegimenFiscal);
            this.pageGeneral.Controls.Add(this.dtFechaTerminoRelacion);
            this.pageGeneral.Controls.Add(this.label20);
            this.pageGeneral.Controls.Add(this.CboRiesgoPuesto);
            this.pageGeneral.Controls.Add(this.label10);
            this.pageGeneral.Controls.Add(this.TxbNumSeguridadSocial);
            this.pageGeneral.Controls.Add(this.label16);
            this.pageGeneral.Controls.Add(this.label9);
            this.pageGeneral.Controls.Add(this.label19);
            this.pageGeneral.Controls.Add(this.TxbAfore);
            this.pageGeneral.Controls.Add(this.CboTipoJornada);
            this.pageGeneral.Controls.Add(this.TxbSitio);
            this.pageGeneral.Controls.Add(this.label29);
            this.pageGeneral.Controls.Add(this.CboGenero);
            this.pageGeneral.Controls.Add(this.TxbNumFonacot);
            this.pageGeneral.Controls.Add(this.TxbRFC);
            this.pageGeneral.Controls.Add(this.label21);
            this.pageGeneral.Controls.Add(this.TxbTelefono);
            this.pageGeneral.Controls.Add(this.label11);
            this.pageGeneral.Controls.Add(this.label3);
            this.pageGeneral.Controls.Add(this.TxbCorreo);
            this.pageGeneral.Controls.Add(this.ButtonRFC);
            this.pageGeneral.Controls.Add(this.DtFechaNacimiento);
            this.pageGeneral.Controls.Add(this.label4);
            this.pageGeneral.Controls.Add(this.label12);
            this.pageGeneral.ItemSize = new System.Drawing.SizeF(55F, 28F);
            this.pageGeneral.Location = new System.Drawing.Point(10, 37);
            this.pageGeneral.Name = "pageGeneral";
            this.pageGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.pageGeneral.Size = new System.Drawing.Size(680, 268);
            this.pageGeneral.Text = "General";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(502, 175);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(62, 18);
            this.radLabel1.TabIndex = 132;
            this.radLabel1.Text = "Telefono 2:";
            // 
            // TxbTelefono2
            // 
            this.TxbTelefono2.Location = new System.Drawing.Point(502, 193);
            this.TxbTelefono2.Name = "TxbTelefono2";
            this.TxbTelefono2.Size = new System.Drawing.Size(168, 20);
            this.TxbTelefono2.TabIndex = 131;
            // 
            // TxbUMF
            // 
            this.TxbUMF.Location = new System.Drawing.Point(401, 110);
            this.TxbUMF.Name = "TxbUMF";
            this.TxbUMF.Size = new System.Drawing.Size(125, 20);
            this.TxbUMF.TabIndex = 129;
            // 
            // label49
            // 
            this.label49.Location = new System.Drawing.Point(401, 92);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(124, 18);
            this.label49.TabIndex = 130;
            this.label49.Text = "Unidad Medica Familiar";
            // 
            // CboTipoEmpleado
            // 
            this.CboTipoEmpleado.Location = new System.Drawing.Point(502, 26);
            this.CboTipoEmpleado.Name = "CboTipoEmpleado";
            this.CboTipoEmpleado.Size = new System.Drawing.Size(168, 20);
            this.CboTipoEmpleado.TabIndex = 124;
            // 
            // label45
            // 
            this.label45.Location = new System.Drawing.Point(502, 133);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(118, 18);
            this.label45.TabIndex = 128;
            this.label45.Text = "Ciudad de Nacimiento";
            // 
            // label43
            // 
            this.label43.Location = new System.Drawing.Point(499, 8);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(99, 18);
            this.label43.TabIndex = 125;
            this.label43.Text = "Tipo de empleado:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(502, 151);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(168, 20);
            this.textBox2.TabIndex = 127;
            // 
            // label44
            // 
            this.label44.Location = new System.Drawing.Point(320, 133);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(144, 18);
            this.label44.TabIndex = 126;
            this.label44.Text = "Entidad Fed. de Nacimiento";
            // 
            // CboEntidadFedNacimiento
            // 
            this.CboEntidadFedNacimiento.Location = new System.Drawing.Point(320, 151);
            this.CboEntidadFedNacimiento.Name = "CboEntidadFedNacimiento";
            this.CboEntidadFedNacimiento.Size = new System.Drawing.Size(176, 20);
            this.CboEntidadFedNacimiento.TabIndex = 125;
            // 
            // pagePrestacion
            // 
            this.pagePrestacion.Controls.Add(this.label42);
            this.pagePrestacion.Controls.Add(this.CboTablaProductividad);
            this.pagePrestacion.Controls.Add(this.label41);
            this.pagePrestacion.Controls.Add(this.label33);
            this.pagePrestacion.Controls.Add(this.label34);
            this.pagePrestacion.Controls.Add(this.TxtJornadasTrabajo);
            this.pagePrestacion.Controls.Add(this.label36);
            this.pagePrestacion.Controls.Add(this.TxbPremiosPorPuntualidad);
            this.pagePrestacion.Controls.Add(this.label37);
            this.pagePrestacion.Controls.Add(this.TxbBecasTrabajadores);
            this.pagePrestacion.Controls.Add(this.TxbAyudaArticulosEscolares);
            this.pagePrestacion.Controls.Add(this.label40);
            this.pagePrestacion.Controls.Add(this.TxbPagoCreditoInfonavit);
            this.pagePrestacion.Controls.Add(this.TxtHorasJornada);
            this.pagePrestacion.Controls.Add(this.TxbOtrosPremiosAsistencia);
            this.pagePrestacion.Controls.Add(this.label39);
            this.pagePrestacion.Controls.Add(this.TxbAyudaTransporte);
            this.pagePrestacion.Controls.Add(this.label38);
            this.pagePrestacion.ItemSize = new System.Drawing.SizeF(79F, 28F);
            this.pagePrestacion.Location = new System.Drawing.Point(10, 37);
            this.pagePrestacion.Name = "pagePrestacion";
            this.pagePrestacion.Padding = new System.Windows.Forms.Padding(3);
            this.pagePrestacion.Size = new System.Drawing.Size(680, 268);
            this.pagePrestacion.Text = "Prestaciones";
            // 
            // label42
            // 
            this.label42.Location = new System.Drawing.Point(142, 202);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(151, 18);
            this.label42.TabIndex = 127;
            this.label42.Text = "Premio Productividad (Tabla)";
            // 
            // CboTablaProductividad
            // 
            this.CboTablaProductividad.Location = new System.Drawing.Point(15, 199);
            this.CboTablaProductividad.Name = "CboTablaProductividad";
            this.CboTablaProductividad.Size = new System.Drawing.Size(121, 20);
            this.CboTablaProductividad.TabIndex = 126;
            // 
            // label41
            // 
            this.label41.Location = new System.Drawing.Point(422, 20);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(150, 18);
            this.label41.TabIndex = 125;
            this.label41.Text = "Pago por crédito de vivienda";
            // 
            // TxbPagoCreditoInfonavit
            // 
            this.TxbPagoCreditoInfonavit.Location = new System.Drawing.Point(333, 17);
            this.TxbPagoCreditoInfonavit.Name = "TxbPagoCreditoInfonavit";
            this.TxbPagoCreditoInfonavit.Size = new System.Drawing.Size(83, 20);
            this.TxbPagoCreditoInfonavit.TabIndex = 124;
            this.TxbPagoCreditoInfonavit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // pagePago
            // 
            this.pagePago.Controls.Add(this.label35);
            this.pagePago.Controls.Add(this.CboFormaPago);
            this.pagePago.Controls.Add(this.CboTipoCuenta);
            this.pagePago.Controls.Add(this.label48);
            this.pagePago.Controls.Add(this.label47);
            this.pagePago.Controls.Add(this.TxbBancoSucursal);
            this.pagePago.Controls.Add(this.label46);
            this.pagePago.Controls.Add(this.TxbCuentaCLABE);
            this.pagePago.Controls.Add(this.label24);
            this.pagePago.Controls.Add(this.TxtCuentaBancaria);
            this.pagePago.Controls.Add(this.TxtCuentaValesDespensa);
            this.pagePago.Controls.Add(this.label23);
            this.pagePago.Controls.Add(this.CboBancos);
            this.pagePago.Controls.Add(this.label28);
            this.pagePago.ItemSize = new System.Drawing.SizeF(42F, 28F);
            this.pagePago.Location = new System.Drawing.Point(10, 37);
            this.pagePago.Name = "pagePago";
            this.pagePago.Size = new System.Drawing.Size(680, 268);
            this.pagePago.Text = "Pago";
            // 
            // CboTipoCuenta
            // 
            this.CboTipoCuenta.Location = new System.Drawing.Point(453, 86);
            this.CboTipoCuenta.Name = "CboTipoCuenta";
            this.CboTipoCuenta.Size = new System.Drawing.Size(105, 20);
            this.CboTipoCuenta.TabIndex = 120;
            // 
            // label48
            // 
            this.label48.Location = new System.Drawing.Point(450, 62);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(82, 18);
            this.label48.TabIndex = 121;
            this.label48.Text = "Tipo de Cuenta";
            // 
            // label47
            // 
            this.label47.Location = new System.Drawing.Point(272, 110);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(47, 18);
            this.label47.TabIndex = 119;
            this.label47.Text = "Sucursal";
            // 
            // TxbBancoSucursal
            // 
            this.TxbBancoSucursal.Location = new System.Drawing.Point(272, 134);
            this.TxbBancoSucursal.MaxLength = 20;
            this.TxbBancoSucursal.Name = "TxbBancoSucursal";
            this.TxbBancoSucursal.Size = new System.Drawing.Size(125, 20);
            this.TxbBancoSucursal.TabIndex = 118;
            // 
            // label46
            // 
            this.label46.Location = new System.Drawing.Point(141, 110);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(102, 18);
            this.label46.TabIndex = 117;
            this.label46.Text = "Clave Interbancaria";
            // 
            // TxbCuentaCLABE
            // 
            this.TxbCuentaCLABE.Location = new System.Drawing.Point(141, 134);
            this.TxbCuentaCLABE.MaxLength = 18;
            this.TxbCuentaCLABE.Name = "TxbCuentaCLABE";
            this.TxbCuentaCLABE.Size = new System.Drawing.Size(125, 20);
            this.TxbCuentaCLABE.TabIndex = 116;
            // 
            // pageContrato
            // 
            this.pageContrato.Controls.Add(this.gridContratos);
            this.pageContrato.Controls.Add(this.radCommandBar1);
            this.pageContrato.ItemSize = new System.Drawing.SizeF(65F, 28F);
            this.pageContrato.Location = new System.Drawing.Point(10, 37);
            this.pageContrato.Name = "pageContrato";
            this.pageContrato.Size = new System.Drawing.Size(680, 268);
            this.pageContrato.Text = "Contratos";
            // 
            // gridContratos
            // 
            this.gridContratos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridContratos.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridContratos.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.gridContratos.Name = "gridContratos";
            this.gridContratos.Size = new System.Drawing.Size(680, 268);
            this.gridContratos.TabIndex = 1;
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(680, 0);
            this.radCommandBar1.TabIndex = 0;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarButtonContratoNuevo,
            this.ToolBarButtonContratoEliminar});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // ToolBarButtonContratoNuevo
            // 
            this.ToolBarButtonContratoNuevo.DisplayName = "commandBarButton1";
            this.ToolBarButtonContratoNuevo.DrawText = true;
            this.ToolBarButtonContratoNuevo.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_new_file;
            this.ToolBarButtonContratoNuevo.Name = "ToolBarButtonContratoNuevo";
            this.ToolBarButtonContratoNuevo.Text = "Nuevo";
            this.ToolBarButtonContratoNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonContratoEliminar
            // 
            this.ToolBarButtonContratoEliminar.DisplayName = "commandBarButton1";
            this.ToolBarButtonContratoEliminar.DrawText = true;
            this.ToolBarButtonContratoEliminar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_delete_file;
            this.ToolBarButtonContratoEliminar.Name = "ToolBarButtonContratoEliminar";
            this.ToolBarButtonContratoEliminar.Text = "Eliminar";
            this.ToolBarButtonContratoEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // pageDocumento
            // 
            this.pageDocumento.Controls.Add(this.gridDocumentos);
            this.pageDocumento.Controls.Add(this.radCommandBar2);
            this.pageDocumento.ItemSize = new System.Drawing.SizeF(80F, 28F);
            this.pageDocumento.Location = new System.Drawing.Point(10, 37);
            this.pageDocumento.Name = "pageDocumento";
            this.pageDocumento.Size = new System.Drawing.Size(680, 268);
            this.pageDocumento.Text = "Documentos";
            // 
            // gridDocumentos
            // 
            this.gridDocumentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDocumentos.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            gridViewCheckBoxColumn3.FieldName = "Activo";
            gridViewCheckBoxColumn3.HeaderText = "A";
            gridViewCheckBoxColumn3.IsVisible = false;
            gridViewCheckBoxColumn3.Name = "Activo";
            gridViewCheckBoxColumn3.VisibleInColumnChooser = false;
            gridViewTextBoxColumn5.FieldName = "Descripcion";
            gridViewTextBoxColumn5.HeaderText = "Descripción";
            gridViewTextBoxColumn5.Name = "Descripcion";
            gridViewTextBoxColumn5.Width = 350;
            gridViewTextBoxColumn6.FieldName = "Tipo";
            gridViewTextBoxColumn6.HeaderText = "Tipo";
            gridViewTextBoxColumn6.Name = "Tipo";
            gridViewTextBoxColumn6.Width = 160;
            gridViewCommandColumn3.FieldName = "Url";
            gridViewCommandColumn3.HeaderText = "Descargar";
            gridViewCommandColumn3.Name = "Url";
            gridViewCommandColumn3.Width = 85;
            this.gridDocumentos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn3,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewCommandColumn3});
            this.gridDocumentos.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.gridDocumentos.Name = "gridDocumentos";
            this.gridDocumentos.Size = new System.Drawing.Size(680, 268);
            this.gridDocumentos.TabIndex = 1;
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement2});
            this.radCommandBar2.Size = new System.Drawing.Size(680, 0);
            this.radCommandBar2.TabIndex = 0;
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Name = "commandBarRowElement2";
            this.commandBarRowElement2.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.DisplayName = "commandBarStripElement2";
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarButtonDocumentosAgregar,
            this.ToolBarButtonDocumentosEditar,
            this.ToolBarButtonDocumentosEliminar,
            this.ToolBarButtonDocumentosDescargar});
            this.commandBarStripElement2.Name = "commandBarStripElement2";
            // 
            // ToolBarButtonDocumentosAgregar
            // 
            this.ToolBarButtonDocumentosAgregar.DisplayName = "commandBarButton1";
            this.ToolBarButtonDocumentosAgregar.DrawText = true;
            this.ToolBarButtonDocumentosAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_agregar_archivo;
            this.ToolBarButtonDocumentosAgregar.Name = "ToolBarButtonDocumentosAgregar";
            this.ToolBarButtonDocumentosAgregar.Text = "Agregar";
            this.ToolBarButtonDocumentosAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonDocumentosEditar
            // 
            this.ToolBarButtonDocumentosEditar.DisplayName = "commandBarButton1";
            this.ToolBarButtonDocumentosEditar.DrawText = true;
            this.ToolBarButtonDocumentosEditar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_edit_file;
            this.ToolBarButtonDocumentosEditar.Name = "ToolBarButtonDocumentosEditar";
            this.ToolBarButtonDocumentosEditar.Text = "Editar";
            this.ToolBarButtonDocumentosEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonDocumentosEliminar
            // 
            this.ToolBarButtonDocumentosEliminar.DisplayName = "commandBarButton2";
            this.ToolBarButtonDocumentosEliminar.DrawText = true;
            this.ToolBarButtonDocumentosEliminar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_delete_file;
            this.ToolBarButtonDocumentosEliminar.Name = "ToolBarButtonDocumentosEliminar";
            this.ToolBarButtonDocumentosEliminar.Text = "Eliminar";
            this.ToolBarButtonDocumentosEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonDocumentosDescargar
            // 
            this.ToolBarButtonDocumentosDescargar.DisplayName = "commandBarButton3";
            this.ToolBarButtonDocumentosDescargar.DrawText = true;
            this.ToolBarButtonDocumentosDescargar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_descargar_desde_la_nube;
            this.ToolBarButtonDocumentosDescargar.Name = "ToolBarButtonDocumentosDescargar";
            this.ToolBarButtonDocumentosDescargar.Text = "Descargar";
            this.ToolBarButtonDocumentosDescargar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarcommandBarRowElement1
            // 
            this.ToolBarcommandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.ToolBarcommandBarRowElement1.Name = "ToolBarcommandBarRowElement1";
            this.ToolBarcommandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarEmpleado});
            this.ToolBarcommandBarRowElement1.Text = "";
            // 
            // ToolBarEmpleado
            // 
            this.ToolBarEmpleado.DisplayName = "ToolBarcommandBarStripElement1";
            this.ToolBarEmpleado.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelActivo,
            this.ToolBarHostActivo,
            this.ToolBarSeparator1,
            this.ToolBarButtonNuevo,
            this.ToolBarButtonGuardar,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonImprimir,
            this.ToolBarButtonCerrar});
            this.ToolBarEmpleado.Name = "ToolBarEmpleado";
            // 
            // ToolBarLabelActivo
            // 
            this.ToolBarLabelActivo.DisplayName = "Etiqueta: Activo";
            this.ToolBarLabelActivo.Name = "ToolBarLabelActivo";
            this.ToolBarLabelActivo.Text = "Activo:";
            // 
            // ToolBarHostActivo
            // 
            this.ToolBarHostActivo.DisplayName = "Activo";
            this.ToolBarHostActivo.Enabled = false;
            this.ToolBarHostActivo.MinSize = new System.Drawing.Size(50, 0);
            this.ToolBarHostActivo.Name = "ToolBarHostActivo";
            this.ToolBarHostActivo.Text = "commandBarHostItem1";
            // 
            // ToolBarSeparator1
            // 
            this.ToolBarSeparator1.DisplayName = "Separador 1";
            this.ToolBarSeparator1.Name = "ToolBarSeparator1";
            this.ToolBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarButtonNuevo
            // 
            this.ToolBarButtonNuevo.DisplayName = "Nuevo";
            this.ToolBarButtonNuevo.DrawText = true;
            this.ToolBarButtonNuevo.Enabled = false;
            this.ToolBarButtonNuevo.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_agregar_usuario;
            this.ToolBarButtonNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonNuevo.Name = "ToolBarButtonNuevo";
            this.ToolBarButtonNuevo.Text = "Nuevo";
            this.ToolBarButtonNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonNuevo.Click += new System.EventHandler(this.ToolBarButtonNuevo_Click);
            // 
            // ToolBarButtonGuardar
            // 
            this.ToolBarButtonGuardar.DisplayName = "Guardar";
            this.ToolBarButtonGuardar.DrawText = true;
            this.ToolBarButtonGuardar.Enabled = false;
            this.ToolBarButtonGuardar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_guardar;
            this.ToolBarButtonGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonGuardar.Name = "ToolBarButtonGuardar";
            this.ToolBarButtonGuardar.Text = "Guardar";
            this.ToolBarButtonGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonGuardar.Click += new System.EventHandler(this.ToolBarButtonGuardar_Click);
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.DisplayName = "Actualizar";
            this.ToolBarButtonActualizar.DrawText = true;
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_actualizar;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.ToolBarButtonActualizar_Click);
            // 
            // ToolBarButtonImprimir
            // 
            this.ToolBarButtonImprimir.DisplayName = "Imprimir";
            this.ToolBarButtonImprimir.DrawText = true;
            this.ToolBarButtonImprimir.Enabled = false;
            this.ToolBarButtonImprimir.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_imprimir;
            this.ToolBarButtonImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonImprimir.Name = "ToolBarButtonImprimir";
            this.ToolBarButtonImprimir.Text = "Imprimir";
            this.ToolBarButtonImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonImprimir.Click += new System.EventHandler(this.ToolBarButtonImprimir_Click);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "Cerrar";
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_cerrar;
            this.ToolBarButtonCerrar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Location = new System.Drawing.Point(0, 33);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.ToolBarcommandBarRowElement1});
            this.ToolBar.Size = new System.Drawing.Size(701, 30);
            this.ToolBar.TabIndex = 124;
            // 
            // Encabezado
            // 
            this.Encabezado.BackColor = System.Drawing.Color.White;
            this.Encabezado.Dock = System.Windows.Forms.DockStyle.Top;
            this.Encabezado.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_administrador;
            this.Encabezado.Location = new System.Drawing.Point(0, 0);
            this.Encabezado.Name = "Encabezado";
            this.Encabezado.Size = new System.Drawing.Size(701, 33);
            this.Encabezado.TabIndex = 102;
            this.Encabezado.TabStop = false;
            // 
            // StatusBar
            // 
            this.StatusBar.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Espera});
            this.StatusBar.Location = new System.Drawing.Point(0, 561);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Size = new System.Drawing.Size(701, 26);
            this.StatusBar.SizingGrip = false;
            this.StatusBar.TabIndex = 125;
            // 
            // Espera
            // 
            this.Espera.Name = "Espera";
            this.StatusBar.SetSpring(this.Espera, false);
            this.Espera.Text = "...";
            this.Espera.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // ViewEmpleadoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 587);
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.ToolBar);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.Encabezado);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewEmpleadoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Empleado";
            this.Load += new System.EventHandler(this.EmpleadoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCuentaBancaria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboBancos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFechaTerminoRelacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumSeguridadSocial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboRegimenFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoJornada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoContrato)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboRiesgoPuesto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboPuesto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDepartamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboGenero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtFechaInicioRel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtFechaNacimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSitio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCorreo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTelefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSegundoApellido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbPrimerApellido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbClave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox)).EndInit();
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.switchActivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboPerioricidadPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSalarioBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSalarioDiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalarioDiarioIntegrado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCuentaValesDespensa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHorasJornada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJornadasTrabajo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbAfore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumFonacot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbPremiosPorPuntualidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbBecasTrabajadores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbOtrosPremiosAsistencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbAyudaArticulosEscolares)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbAyudaTransporte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.pageGeneral.ResumeLayout(false);
            this.pageGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTelefono2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbUMF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoEmpleado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEntidadFedNacimiento)).EndInit();
            this.pagePrestacion.ResumeLayout(false);
            this.pagePrestacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.label42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTablaProductividad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbPagoCreditoInfonavit)).EndInit();
            this.pagePago.ResumeLayout(false);
            this.pagePago.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoCuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbBancoSucursal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCuentaCLABE)).EndInit();
            this.pageContrato.ResumeLayout(false);
            this.pageContrato.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridContratos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridContratos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.pageDocumento.ResumeLayout(false);
            this.pageDocumento.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDocumentos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDocumentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToolBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadButton ButtonRFC;
        private Telerik.WinControls.UI.RadLabel label24;
        private Telerik.WinControls.UI.RadTextBox TxtCuentaBancaria;
        private Telerik.WinControls.UI.RadLabel label23;
        private Telerik.WinControls.UI.RadDropDownList CboBancos;
        private Telerik.WinControls.UI.RadLabel label22;
        private Telerik.WinControls.UI.RadDateTimePicker dtFechaTerminoRelacion;
        private Telerik.WinControls.UI.RadLabel label21;
        private Telerik.WinControls.UI.RadTextBox TxbNumSeguridadSocial;
        private Telerik.WinControls.UI.RadLabel label20;
        private Telerik.WinControls.UI.RadDropDownList CboRegimenFiscal;
        private Telerik.WinControls.UI.RadLabel label19;
        private Telerik.WinControls.UI.RadDropDownList CboTipoJornada;
        private Telerik.WinControls.UI.RadLabel label18;
        private Telerik.WinControls.UI.RadDropDownList CboTipoContrato;
        private Telerik.WinControls.UI.RadLabel label17;
        private Telerik.WinControls.UI.RadLabel label16;
        private Telerik.WinControls.UI.RadDropDownList CboRiesgoPuesto;
        private Telerik.WinControls.UI.RadLabel label15;
        private Telerik.WinControls.UI.RadLabel label14;
        private Telerik.WinControls.UI.RadLabel label13;
        private Telerik.WinControls.UI.RadDropDownList CboPuesto;
        private Telerik.WinControls.UI.RadDropDownList CboDepartamento;
        private Telerik.WinControls.UI.RadLabel label12;
        private Telerik.WinControls.UI.RadLabel label11;
        private Telerik.WinControls.UI.RadDropDownList CboGenero;
        private Telerik.WinControls.UI.RadDateTimePicker DtFechaInicioRel;
        private Telerik.WinControls.UI.RadDateTimePicker DtFechaNacimiento;
        private Telerik.WinControls.UI.RadLabel label10;
        private Telerik.WinControls.UI.RadLabel label9;
        private Telerik.WinControls.UI.RadLabel label8;
        private Telerik.WinControls.UI.RadLabel label7;
        private Telerik.WinControls.UI.RadLabel label6;
        private Telerik.WinControls.UI.RadLabel label5;
        private Telerik.WinControls.UI.RadLabel label4;
        private Telerik.WinControls.UI.RadLabel label3;
        private Telerik.WinControls.UI.RadLabel label2;
        private Telerik.WinControls.UI.RadLabel label1;
        private Telerik.WinControls.UI.RadTextBox TxbSitio;
        private Telerik.WinControls.UI.RadTextBox TxbCorreo;
        private Telerik.WinControls.UI.RadTextBox TxbTelefono;
        private Telerik.WinControls.UI.RadTextBox TxbSegundoApellido;
        private Telerik.WinControls.UI.RadTextBox TxbPrimerApellido;
        private Telerik.WinControls.UI.RadTextBox TxbNombre;
        private Telerik.WinControls.UI.RadTextBox TxbRFC;
        private Telerik.WinControls.UI.RadTextBox TxbCURP;
        private Telerik.WinControls.UI.RadTextBox TxbClave;
        private Telerik.WinControls.UI.RadTextBox TxbNum;
        private System.Windows.Forms.PictureBox Encabezado;
        private Telerik.WinControls.UI.RadLabel label25;
        private Telerik.WinControls.UI.RadGroupBox groupBox;
        private Telerik.WinControls.UI.RadTextBox TxbSalarioDiario;
        private Telerik.WinControls.UI.RadTextBox TxbSalarioBase;
        private Telerik.WinControls.UI.RadLabel label27;
        private Telerik.WinControls.UI.RadLabel label26;
        private Telerik.WinControls.UI.RadLabel label32;
        private Telerik.WinControls.UI.RadLabel label31;
        private Telerik.WinControls.UI.RadDropDownList comboBox3;
        private Telerik.WinControls.UI.RadTextBox TxtSalarioDiarioIntegrado;
        private Telerik.WinControls.UI.RadLabel label30;
        private Telerik.WinControls.UI.RadTextBox TxbAfore;
        private Telerik.WinControls.UI.RadLabel label29;
        private Telerik.WinControls.UI.RadTextBox TxbNumFonacot;
        private Telerik.WinControls.UI.RadLabel label28;
        private Telerik.WinControls.UI.RadDropDownList CboFormaPago;
        private Telerik.WinControls.UI.RadLabel label35;
        private Telerik.WinControls.UI.RadTextBox TxtCuentaValesDespensa;
        private Telerik.WinControls.UI.RadLabel label33;
        private Telerik.WinControls.UI.RadTextBox TxtHorasJornada;
        private Telerik.WinControls.UI.RadLabel label34;
        private Telerik.WinControls.UI.RadTextBox TxtJornadasTrabajo;
        private Telerik.WinControls.UI.RadLabel label36;
        private Telerik.WinControls.UI.RadTextBox TxbPremiosPorPuntualidad;
        private Telerik.WinControls.UI.RadLabel label37;
        private Telerik.WinControls.UI.RadTextBox TxbBecasTrabajadores;
        private Telerik.WinControls.UI.RadLabel label38;
        private Telerik.WinControls.UI.RadTextBox TxbOtrosPremiosAsistencia;
        private Telerik.WinControls.UI.RadLabel label39;
        private Telerik.WinControls.UI.RadTextBox TxbAyudaArticulosEscolares;
        private Telerik.WinControls.UI.RadLabel label40;
        private Telerik.WinControls.UI.RadTextBox TxbAyudaTransporte;
        private Telerik.WinControls.UI.RadPageView tabControl;
        private Telerik.WinControls.UI.RadPageViewPage pageGeneral;
        private Telerik.WinControls.UI.RadPageViewPage pagePrestacion;
        private Telerik.WinControls.UI.RadLabel label41;
        private Telerik.WinControls.UI.RadTextBox TxbPagoCreditoInfonavit;
        private Telerik.WinControls.UI.RadPageViewPage pagePago;
        private Telerik.WinControls.UI.RadLabel label42;
        private Telerik.WinControls.UI.RadDropDownList CboTablaProductividad;
        private Telerik.WinControls.UI.RadDropDownList CboTipoEmpleado;
        private Telerik.WinControls.UI.RadLabel label43;
        private Telerik.WinControls.UI.RadLabel label44;
        private Telerik.WinControls.UI.RadDropDownList CboEntidadFedNacimiento;
        private Telerik.WinControls.UI.RadLabel label45;
        private Telerik.WinControls.UI.RadTextBox textBox2;
        private Telerik.WinControls.UI.RadDropDownList CboTipoCuenta;
        private Telerik.WinControls.UI.RadLabel label48;
        private Telerik.WinControls.UI.RadLabel label47;
        private Telerik.WinControls.UI.RadTextBox TxbBancoSucursal;
        private Telerik.WinControls.UI.RadLabel label46;
        private Telerik.WinControls.UI.RadTextBox TxbCuentaCLABE;
        private Telerik.WinControls.UI.RadTextBox TxbUMF;
        private Telerik.WinControls.UI.RadLabel label49;
        private Telerik.WinControls.UI.CommandBarRowElement ToolBarcommandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBarEmpleado;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonNuevo;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonGuardar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonImprimir;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCerrar;
        private Telerik.WinControls.UI.RadCommandBar ToolBar;
        private Telerik.WinControls.UI.RadDropDownList CboPerioricidadPago;
        private Telerik.WinControls.UI.RadPageViewPage pageContrato;
        private Telerik.WinControls.UI.RadGridView gridContratos;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonContratoNuevo;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonContratoEliminar;
        private Telerik.WinControls.UI.RadPageViewPage pageDocumento;
        private Telerik.WinControls.UI.RadGridView gridDocumentos;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonDocumentosAgregar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonDocumentosEliminar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonDocumentosDescargar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonDocumentosEditar;
        private Telerik.WinControls.UI.RadStatusStrip StatusBar;
        private Telerik.WinControls.UI.RadWaitingBarElement Espera;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonActualizar;
        private Telerik.WinControls.UI.RadToggleSwitch switchActivo;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox TxbTelefono2;
        private Telerik.WinControls.UI.CommandBarHostItem ToolBarHostActivo;
        private Telerik.WinControls.UI.CommandBarSeparator ToolBarSeparator1;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelActivo;
    }
}