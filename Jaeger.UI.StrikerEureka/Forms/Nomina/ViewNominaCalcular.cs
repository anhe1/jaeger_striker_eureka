﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Enumerations;
using Jaeger.Nomina.Helpers;
using Jaeger.Nomina.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views.Nomina
{
    public partial class ViewNominaCalcular : RadForm
    {
        private HelperNominaCalcular nomina;
        private SqlSugarNominaConfiguracion data;
        private Configuracion configuracion;
        private BackgroundWorker calcular;

        public ViewNominaCalcular()
        {
            InitializeComponent();
        }

        private void NominaCalcular_Load(object sender, EventArgs e)
        {
            this.calcular = new BackgroundWorker();
            this.calcular.DoWork += Calcular_DoWork;
            this.calcular.RunWorkerCompleted += Calcular_RunWorkerCompleted;
            this.GridData.TelerikGridCommon();
            this.GridData.AllowEditRow = true;
            this.GridPercepciones.Standard();
            this.GridPercepciones.HierarchyDataProvider = new GridViewEventDataProvider(this.GridPercepciones);
            this.GridDeducciones.Standard();
            this.GridDeducciones.HierarchyDataProvider = new GridViewEventDataProvider(this.GridDeducciones);
            this.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.GridData_RowSourceNeeded);
            this.data= new SqlSugarNominaConfiguracion(ConfigService.Synapsis.RDS.Edita);
            this.configuracion = this.data.LoadConf();
            this.nomina = new HelperNominaCalcular(ref this.configuracion, ConfigService.Synapsis.RDS.Edita);
            this.GridData.DataBindings.Add("DataSource", this.nomina.Periodo, "Empleados", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboEstado.DataSource = Enum.GetValues(typeof(ViewModelNominaControl.EnumStatusNomina))
            .Cast<Enum>()
            .Select(value => new
            {
                (Attribute.GetCustomAttribute(value.GetType().GetField(value.ToString()), typeof(DescriptionAttribute)) as DescriptionAttribute).Description,
                value
            })
            .OrderBy(item => item.value)
            .ToList();
            CboEstado.DisplayMember = "Description";
            CboEstado.ValueMember = "value";
        }

        private void ToolBarButtonNominaAbrir_Click(object sender, EventArgs e)
        {
            Jaeger.Views.Nomina.NominaAbrir abrir = new Jaeger.Views.Nomina.NominaAbrir() { StartPosition = FormStartPosition.CenterParent };
            ViewModelNominaPeriodo t = abrir.FormShow();
            if (t != null)
            {
                this.nomina.CargarNomina(t.Id);
            }
            if (this.nomina.Periodo.Estado == ViewModelNominaControl.EnumStatusNomina.Autorizada)
            {
                this.ToolBarButtonCalcular.Enabled = false;
                this.ToolBarButtonGuardar.Enabled = false;
            }
            else
            {
                this.ToolBarButtonGuardar.Enabled = true;
                this.ToolBarButtonCalcular.Enabled = true;
            }
            this.CrearBinding();
        }

        private void ToolBarButtonNominaNuevo_Click(object sender, EventArgs e)
        {
            this.GridData.DataSource = null;
            using (Waiting2Form espera = new Waiting2Form(this.nomina.NuevaNomina))
            {
                espera.ShowDialog(this);
                this.nomina.Periodo.Creo = ConfigService.Piloto.Clave;
                this.CrearBinding();
            }
        }

        private void ToolBarButtonEditar_Click(object sender, EventArgs e)
        {

        }

        private void ToolBarButtonEliminar_Click(object sender, EventArgs e)
        {

        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e)
        {

        }

        private void ToolBarButtonCalcular_Click(object sender, EventArgs e)
        {
            if (this.GridData.Rows.Count > 0)
            {
                using (Waiting2Form espera = new Waiting2Form(this.nomina.Calcular))
                {
                    espera.ShowDialog(this);
                    this.GridData.DataSource = null;
                    this.GridData.DataSource = this.nomina.Periodo.Empleados;
                }
                
            }
            else
            {
                RadMessageBox.Show("Debes tener al menos un trabajador listado para calular la nómina", "Error", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        private void ToolBarButtonGuardar_Click(object sender, EventArgs e)
        {
            if (this.nomina.Periodo.Id != 0)
            {
                this.nomina.Periodo.Modifica = ConfigService.Piloto.Clave;
            }
            using (Waiting2Form espera = new Waiting2Form(this.nomina.Guardar))
            {
                espera.ShowDialog(this);
            }
        }

        private void ToolBarButtonImprimirListado_Click(object sender, EventArgs e)
        {
            if (this.GridData.RowCount > 0)
            {
                //Jaeger.Main.Reportes reporte = new Reportes(this.nomina.Periodo);
                //reporte.Show();
            }
        }

        private void ToolBarButtonImprimirSobreRecibo_Click(object sender, EventArgs e)
        {
            if (this.GridData.CurrentRow != null)
            {
                Jaeger.Nomina.Entities.ViewModelNominaPeriodo em = this.GridData.CurrentRow.DataBoundItem as Jaeger.Nomina.Entities.ViewModelNominaPeriodo;
                if (em != null)
                {
                    //Jaeger.Nomina.Entities.EmpleadoSobreRecibo temporal = this.nomina.SobreRecibo(em.Id);
                    //Jaeger.Main.Reportes reporte = new Reportes(temporal);
                    //reporte.Show();
                }
            }
        }

        private void ToolBarButtonCrear_Click(object sender, EventArgs e)
        {
            if (RadMessageBox.Show("¿Esta seguro de crear la tabla? Esto acción eliminara toda la información almacenada,", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Exclamation) == System.Windows.Forms.DialogResult.Yes)
            {
                this.nomina.CrearTabla();
            }
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CrearBinding()
        {
            this.TxbDescripcion.DataBindings.Clear();
            this.TxbDescripcion.DataBindings.Add("Text", this.nomina.Periodo, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.DtpFechaInicio.DataBindings.Clear();
            this.DtpFechaInicio.DataBindings.Add("Value", this.nomina.Periodo, "FechaInicio", true, DataSourceUpdateMode.OnPropertyChanged);
            this.DtpFechaFinal.DataBindings.Clear();
            this.DtpFechaFinal.DataBindings.Add("Value", this.nomina.Periodo, "FechaFin", true, DataSourceUpdateMode.OnPropertyChanged);
            this.DtpFechaPago.DataBindings.Clear();
            this.DtpFechaPago.DataBindings.Add("Value", this.nomina.Periodo, "FechaPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.LabelDiasPeriodo.DataBindings.Clear();
            this.LabelDiasPeriodo.DataBindings.Add("Text", this.nomina.Periodo, "DiasPeriodo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbPercepciones.DataBindings.Clear();
            this.TxbPercepciones.DataBindings.Add("Text", this.nomina.Periodo, "TotalPercepciones", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbDeducciones.DataBindings.Clear();
            this.TxbDeducciones.DataBindings.Add("Text", this.nomina.Periodo, "TotalDeducciones", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbFondoAhorro.DataBindings.Clear();
            this.TxbFondoAhorro.DataBindings.Add("Text", this.nomina.Periodo, "TotalFondoAhorro", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbValesDespensa.DataBindings.Clear();
            this.TxbValesDespensa.DataBindings.Add("Text", this.nomina.Periodo, "TotalValesDespensa", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboEstado.DataBindings.Clear();
            this.CboEstado.DataBindings.Add("SelectedValue", this.nomina.Periodo, "Estado", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e)
        {
            if (e.Template.Caption == this.GridPercepciones.Caption)
            {
                ViewModelNominaPeriodo rowView = e.ParentRow.DataBoundItem as ViewModelNominaPeriodo;
                if (rowView != null)
                {
                    if (rowView.Percepciones != null)
                    {
                        foreach (Percepcion item in rowView.Percepciones)
                        {
                            GridViewRowInfo row = e.Template.Rows.NewRow();
                            row.Cells["Identificador"].Value = item.Identificador;
                            row.Cells["Clave"].Value = item.Clave;
                            row.Cells["Tipo"].Value = item.Tipo;
                            row.Cells["Concepto"].Value = item.Concepto;
                            row.Cells["ImporteExento"].Value = item.ImporteExento;
                            row.Cells["ImporteGravado"].Value = item.ImporteGravado;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            }
            else if (e.Template.Caption == this.GridDeducciones.Caption)
            {
                ViewModelNominaPeriodo rowView = e.ParentRow.DataBoundItem as ViewModelNominaPeriodo;
                if (rowView != null)
                {
                    if (rowView.Percepciones != null)
                    {
                        foreach (Deduccion item in rowView.Deducciones)
                        {
                            GridViewRowInfo row = e.Template.Rows.NewRow();
                            row.Cells["Identificador"].Value = item.Identificador;
                            row.Cells["Clave"].Value = item.Clave;
                            row.Cells["Tipo"].Value = item.Tipo;
                            row.Cells["Concepto"].Value = item.Concepto;
                            row.Cells["Importe"].Value = item.Importe;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            }
        }

        private void Calcular_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.CrearBinding();
            this.GridData.DataSource = this.nomina.Periodo.Empleados;
            this.radWaitingBar1.Visible = false;
            this.radWaitingBar1.StopWaiting();
        }

        private void Calcular_DoWork(object sender, DoWorkEventArgs e)
        {
            this.nomina.Calcular();
        }

        private void ToolBarButtonFiltro_Click(object sender, EventArgs e)
        {
            this.GridData.ShowFilteringRow = this.ToolBarButtonFiltro.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false)
            {
                this.GridData.FilterDescriptors.Clear();
            }
        }

        private void ToolBarButtonExportar_Click(object sender, EventArgs e)
        {
            TelerikGridExport exportar = new TelerikGridExport(this.GridData);
            exportar.ShowDialog();
        }

        private void ToolBarButtonLayoutBancomer_Click(object sender, EventArgs e)
        {
            if (RadMessageBox.Show(this, "Para este layout solo se incluiran los empleados que tengan asignado cuentas de Bancomer, los empleados que no correspondan seran ignorados. ¿Deseas continuar?", "Atencion", MessageBoxButtons.YesNo, RadMessageIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                SaveFileDialog savefiledialog = new SaveFileDialog() { Filter = "*.txt|*.txt" };
                savefiledialog.FileName = this.nomina.Periodo.NombreLayout();
                if (savefiledialog.ShowDialog() == DialogResult.OK)
                {
                    this.nomina.CrearLayoutBancomer(this.nomina.Periodo.Id, savefiledialog.FileName);
                }
            }
        }
    }
}
