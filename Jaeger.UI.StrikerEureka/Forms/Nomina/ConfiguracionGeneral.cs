﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Nomina.Helpers;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Views.Nomina
{
    public partial class ConfiguracionGeneral : Telerik.WinControls.UI.RadForm
    {
        private SqlSugarNominaConfiguracion data;
        private Jaeger.Nomina.Entities.Configuracion conf;
        public ConfiguracionGeneral()
        {
            InitializeComponent();
        }

        private void ConfiguracionGeneral_Load(object sender, EventArgs e)
        {
            this.data = new SqlSugarNominaConfiguracion(ConfigService.Synapsis.RDS.Edita);
            this.conf = this.data.LoadConf();
            if (this.conf == null)
            {
                if (RadMessageBox.Show("La configuración no extiste, ¿quieres crearla?", "Atención", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    this.conf = new Jaeger.Nomina.Entities.Configuracion();
                    if (this.data.Create(this.conf) == true)
                    {
                        RadMessageBox.Show(this, "La configuración inicial guardo correctamente!");
                    }
                }
                else
                {
                    this.Close();
                }
            }
            this.radPropertyGrid1.SelectedObject = this.conf;
        }

        private void ToolBarButtonGuardar_Click(object sender, EventArgs e)
        {
            this.data.Save(this.conf);
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
