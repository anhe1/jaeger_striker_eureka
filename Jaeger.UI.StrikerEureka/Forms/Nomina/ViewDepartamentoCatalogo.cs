﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Helpers;
using Jaeger.Nomina.Helpers;
using Jaeger.Nomina.Entities;
using Telerik.WinControls.UI;
using Jaeger.Edita.V2;
using Jaeger.Edita.V2.Almacen.Entities;
using Jaeger.Nomina.Interface;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;

namespace Jaeger.Views.Nomina
{
    public partial class ViewDepartamentoCatalogo : Telerik.WinControls.UI.RadForm
    {
        private ISqlDepartamento data;
        private SqlSugarClasificacionG dataClase;
        private BindingList<ViewModelDepartamento> datos;
        private BindingList<ViewModelClase> clases;
        private GridViewComboBoxColumn comboProductos;

        public ViewDepartamentoCatalogo()
        {
            InitializeComponent();
        }

        private void ViewDepartamentoCatalogo_Load(object sender, EventArgs e)
        {
            this.GridDeptos.TelerikGridCommon();
            this.GridPuestos.TelerikGridCommon();
            this.GridFunciones.TelerikGridCommon();
            this.GridDeptos.AllowEditRow = true;
            this.GridPuestos.AllowEditRow = true;
            this.GridFunciones.AllowEditRow = true;
            this.data = new SqlSugarDepartamento(ConfigService.Synapsis.RDS.Edita);
            this.dataClase = new SqlSugarClasificacionG(ConfigService.Synapsis.RDS.Edita);
            this.comboProductos = this.GridDeptos.Columns["IdClase"] as GridViewComboBoxColumn;
            this.comboProductos.DisplayMember = "Resume";
            this.comboProductos.ValueMember = "Id";
            this.comboProductos.AutoSizeMode = BestFitColumnMode.AllCells;
        }

        #region barra de herramientas departamentos

        private void ToolBarDeptoButtonAgregar_Click(object sender, EventArgs e)
        {
            this.datos.Add(new ViewModelDepartamento { FechaNuevo = DateTime.Now, Creo = ConfigService.Piloto.Clave });
        }

        private void ToolBarDeptoButtonEliminar_Click(object sender, EventArgs e)
        {
            if (this.GridDeptos.CurrentRow != null)
            {
                if (RadMessageBox.Show(this, "¿Esta seguro de eliminar?", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                var seleccionado = this.GridDeptos.CurrentRow.DataBoundItem as ViewModelDepartamento;
                if (seleccionado != null)
                {
                    if (seleccionado.Id == 0)
                        this.datos.Remove(seleccionado);
                }
            }
        }

        private void ToolBarDeptoButtonGuardar_Click(object sender, EventArgs e)
        {
            if (this.GridDeptos.CurrentRow != null)
            {
                var seleccionado = this.GridDeptos.CurrentRow.DataBoundItem as ViewModelDepartamento;
                if (seleccionado != null)
                    this.data.Save(seleccionado);
            }
        }

        private void ToolBarDeptoButtonActualizar_Click(object sender, EventArgs e)
        {
            using (var espera = new Waiting2Form(this.Consultar))
            {
                espera.Text = "Espera...";
                espera.ShowDialog(this);
            }
            this.comboProductos.DataSource = this.clases;
            this.GridDeptos.DataSource = this.datos;
            this.GridPuestos.DataSource = this.datos;
            this.GridPuestos.DataMember = "Puestos";
            this.GridFunciones.DataSource = this.datos;
            this.GridFunciones.DataMember = "Puestos.Funciones";
        }

        private void ToolBarDeptoButtonCerrar_Click(object sender, EventArgs e)
        {

        }

        private void ToolBarDeptoButtonherramientasCrear_Click(object sender, EventArgs e)
        {
            this.data.Create();
        }

        #endregion

        #region puestos

        private void ToolBarPuestoButtonAgregar_Click(object sender, EventArgs e)
        {
            if (this.GridDeptos.CurrentRow != null)
            {
                var seleccionado = this.GridDeptos.CurrentRow.DataBoundItem as ViewModelDepartamento;
                if (seleccionado != null)
                {
                    if (seleccionado.Id == 0)
                        RadMessageBox.Show(this, "Almacena primero el departamento");
                    else 
                        seleccionado.Puestos.Add(new ViewModelDeptoPuesto { IdDepto = seleccionado.Id, Creo = ConfigService.Piloto.Clave });
                }
            }
        }

        private void ToolBarPuestoButtonEliminar_Click(object sender, EventArgs e)
        {

        }

        private void ToolBarPuestoButtonGuardar_Click(object sender, EventArgs e)
        {
            if (this.GridPuestos.CurrentRow != null)
            {
                var seleccionado = this.GridPuestos.CurrentRow.DataBoundItem as ViewModelDeptoPuesto;
                if (seleccionado != null)
                    this.data.Puestos.Save(seleccionado);
            }
        }

        #endregion

        private void Consultar()
        {
            this.clases = new BindingList<ViewModelClase>(this.dataClase.GetListBy());
            this.datos = this.data.GetListBy();
        }

        #region funciones

        private void ToolBarFuncionButtonAgregar_Click(object sender, EventArgs e)
        {
            if (this.GridPuestos.CurrentRow != null)
            {
                var seleccionado = this.GridPuestos.CurrentRow.DataBoundItem as ViewModelDeptoPuesto;
                if (seleccionado.Id == 0)
                    RadMessageBox.Show(this, "Almacena primero el departamento");
                else
                    seleccionado.Funciones.Add(new ViewModelDeptoFuncion { IdPuesto = seleccionado.Id, Creo = ConfigService.Piloto.Clave, FechaNuevo = DateTime.Now });
            }
        }

        private void ToolBarFuncionButtonEliminar_Click(object sender, EventArgs e)
        {

        }

        private void ToolBarFuncionButtonGuardar_Click(object sender, EventArgs e)
        {
            if (this.GridFunciones.CurrentRow != null)
            {
                var seleccionado = this.GridFunciones.CurrentRow.DataBoundItem as ViewModelDeptoFuncion;
                if (seleccionado != null)
                    this.data.Puestos.Funciones.Save(seleccionado);
            }
        }

        #endregion
    }
}
