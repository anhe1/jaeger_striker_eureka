using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Jaeger.Nomina.Enums;
using Jaeger.Nomina.Entities;
using Jaeger.Nomina.Helpers;
using Jaeger.Helpers;
using Jaeger.Nomina.Interface;
using Jaeger.UI.Common.Forms;
using Jaeger.Catalogos.Repositories;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views.Nomina
{
    public partial class ViewEmpleadoForm : Telerik.WinControls.UI.RadForm
    {
        private BackgroundWorker preparar;
        private Jaeger.Nomina.Entities.ViewModelEmpleado empleado;
        private TipoContratoCatalogo tipoContrato;
        private PeriodicidadPagoCatalogo perioricidadPago;
        private BancosCatalogo bancos;
        private CatalogoTipoRegimen regimenesFiscales;
        private RiesgoPuestoCatalogo riesgoPuesto;
        private TipoJornadaCatalogo tipoJornada;
        private SqlSugarEmpleados catalogo;
        private ISqlDepartamento deptos;
        private SqlSugarTablaProductividad productividad;

        public ViewEmpleadoForm(Jaeger.Nomina.Entities.ViewModelEmpleado empleado = null)
        {
            InitializeComponent();
            this.empleado = empleado;
        }

        private void EmpleadoForm_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            ToolBarHostActivo.HostedItem = this.switchActivo.ToggleSwitchElement;
            this.gridContratos.TelerikGridCommon();
            this.gridDocumentos.TelerikGridCommon();
            this.Espera.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.Espera.StartWaiting();
            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += PrepararDoWork;
            this.preparar.RunWorkerCompleted += PrepararRunWorkerCompleted;
            this.preparar.RunWorkerAsync();
            
        }

        private void ButtonRFC_Click(object sender, EventArgs e)
        {
            string rfc = this.empleado.CalcularRFC();
            if (rfc == "")
            {
                this.label4.Text = "RFC: Error!";
            }
            else
            {
                this.label4.Text = "RFC:";
            }
        }

        private void TxbCURP_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F7)
            {
                if (this.empleado.FechaNacimiento == null)
                {
                    MessageBox.Show("Por favor asigna una fecha de nacimiento valida!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (this.empleado.EntidadFederativaNacimiento == 0)
                {
                    MessageBox.Show("Es necesario indicar la Entidad Federativa de Nacimiento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                HelperCURP t = new HelperCURP(this.empleado.Nombre, this.empleado.PrimerApellido, this.empleado.SegundoApellido, this.empleado.Genero, this.empleado.FechaNacimiento.Value, this.empleado.EntidadFederativaNacimiento);
                this.empleado.CURP = t.CURP;
            }
        }

        private void TxbRFC_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F7)
            {
                if (this.empleado.FechaNacimiento == null)
                {
                    MessageBox.Show("Por favor asigna una fecha de nacimiento valida!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (this.empleado.EntidadFederativaNacimiento == 0)
                {
                    MessageBox.Show("Es necesario indicar la Entidad Federativa de Nacimiento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                this.empleado.CalcularRFC();
            }
        }

        private void ToolBarButtonNuevo_Click(object sender, EventArgs e)
        {
            this.empleado = null;
        }

        private void ToolBarButtonGuardar_Click(object sender, EventArgs e)
        {
            if (this.empleado.Num == 0)
            {
                this.empleado.Creo = ConfigService.Piloto.Clave;
                this.empleado.FechaNuevo = DateTime.Now;
            }
            else
            {
                this.empleado.Modifica = ConfigService.Piloto.Clave;
                this.empleado.FechaModifica = DateTime.Now;
            }

            using (var espera = new Waiting2Form(this.Guardar))
            {
                espera.Text = "Guardando...";
                espera.ShowDialog(this);
            }
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e)
        {
            if (this.empleado == null)
                this.empleado = new ViewModelEmpleado();
            this.CreateBinding();
        }

        private void ToolBarButtonImprimir_Click(object sender, EventArgs e)
        {
            //Jaeger.Views.Reportes reporte = new Reportes(this.empleado);
            //reporte.Show();
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CboDepartamento_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (this.CboDepartamento.SelectedItem == null)
                return;
            var seleccionado = this.CboDepartamento.SelectedItem.DataBoundItem as ViewModelDepartamento;
            if (seleccionado != null)
            {
                this.CboPuesto.DataSource = this.deptos.Puestos.GetListBy(seleccionado.Id);
            }
        }

        private void PrepararDoWork(object sender, DoWorkEventArgs e)
        {
            this.catalogo = new SqlSugarEmpleados(ConfigService.Synapsis.RDS.Edita);
            this.productividad = new SqlSugarTablaProductividad(ConfigService.Synapsis.RDS.Edita);
            this.deptos = new SqlSugarDepartamento(ConfigService.Synapsis.RDS.Edita);
            this.tipoContrato = new TipoContratoCatalogo();
            this.tipoContrato.Load();

            this.perioricidadPago = new PeriodicidadPagoCatalogo();
            this.perioricidadPago.Load();
            this.bancos = new BancosCatalogo();
            this.bancos.Load();
            this.regimenesFiscales = new CatalogoTipoRegimen();
            this.regimenesFiscales.Load();
            this.riesgoPuesto = new RiesgoPuestoCatalogo();
            this.riesgoPuesto.Load();
            this.tipoJornada = new TipoJornadaCatalogo();
            this.tipoJornada.Load();

            this.CboTipoContrato.DisplayMember = "Descripcion";
            this.CboTipoContrato.ValueMember = "Clave";

            this.CboPerioricidadPago.DisplayMember = "Descripcion";
            this.CboPerioricidadPago.ValueMember = "Clave";

            this.CboBancos.DisplayMember = "Descripcion";
            this.CboBancos.ValueMember = "Clave";

            this.CboRegimenFiscal.DisplayMember = "Descripcion";
            this.CboRegimenFiscal.ValueMember = "Clave";

            this.CboRiesgoPuesto.DisplayMember = "Descripcion";
            this.CboRiesgoPuesto.ValueMember = "Clave";

            this.CboTipoJornada.DisplayMember = "Descripcion";
            this.CboTipoJornada.ValueMember = "Clave";

            this.CboTablaProductividad.DisplayMember = "Descripcion";
            this.CboTablaProductividad.ValueMember = "Id";

            this.CboEntidadFedNacimiento.DisplayMember = "Description";
            this.CboEntidadFedNacimiento.ValueMember = "Value";
            this.CboEntidadFedNacimiento.DataSource = Enum.GetValues(typeof(EnumEstado)).Cast<Enum>()
            .Select(value => new
            {
                (Attribute.GetCustomAttribute(value.GetType().GetField(value.ToString()), typeof(DescriptionAttribute)) as DescriptionAttribute).Description,
                value
            })
            .OrderBy(item => item.value)
            .ToList();

            this.CboGenero.DisplayMember = "Description";
            this.CboGenero.ValueMember = "Value";
            this.CboGenero.DataSource = Enum.GetValues(typeof(EnumGenero)).Cast<Enum>()
            .Select(value => new
            {
                (Attribute.GetCustomAttribute(value.GetType().GetField(value.ToString()), typeof(DescriptionAttribute)) as DescriptionAttribute).Description,
                value
            })
            .OrderBy(item => item.value)
            .ToList();

            this.CboFormaPago.DisplayMember = "Description";
            this.CboFormaPago.ValueMember = "Value";
            this.CboFormaPago.DataSource = Enum.GetValues(typeof(EnumFormaPago)).Cast<Enum>()
            .Select(value => new
            {
                (Attribute.GetCustomAttribute(value.GetType().GetField(value.ToString()), typeof(DescriptionAttribute)) as DescriptionAttribute).Description,
                value
            })
            .OrderBy(item => item.value)
            .ToList();

            this.CboTipoCuenta.DisplayMember = "Description";
            this.CboTipoCuenta.ValueMember = "Value";
            this.CboTipoCuenta.DataSource = Enum.GetValues(typeof(EnumTipoCuentaBancaria)).Cast<Enum>()
            .Select(value => new
            {
                (Attribute.GetCustomAttribute(value.GetType().GetField(value.ToString()), typeof(DescriptionAttribute)) as DescriptionAttribute).Description,
                value
            })
            .OrderBy(item => item.value)
            .ToList();

            this.CboDepartamento.DisplayMember = "Nombre";
            this.CboDepartamento.ValueMember = "Id";

            this.CboPuesto.DisplayMember = "Descripcion";
            this.CboPuesto.ValueMember = "Id";

            this.DtFechaNacimiento.SetToNullValue();
            this.dtFechaTerminoRelacion.SetToNullValue();
            this.CboTablaProductividad.DataSource = this.productividad.GetTablas();
            this.CboDepartamento.DataSource = this.deptos.GetList();
        }

        private void PrepararRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.CboTipoContrato.DataSource = this.tipoContrato.Items;
            this.CboPerioricidadPago.DataSource = this.perioricidadPago.Items;
            this.CboBancos.DataSource = this.bancos.Items;
            this.CboRegimenFiscal.DataSource = this.regimenesFiscales.Items;
            this.CboRiesgoPuesto.DataSource = this.riesgoPuesto.Items;
            this.CboTipoJornada.DataSource = this.tipoJornada.Items;
            //this.CreateBinding();
            this.groupBox.Enabled = true;
            this.tabControl.Enabled = true;
            this.Espera.StopWaiting();
            this.Espera.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.ToolBarButtonActualizar.PerformClick();
            
        }

        private void CreateBinding()
        {
            this.TxbNum.DataBindings.Clear();
            this.TxbNum.DataBindings.Add("Text", this.empleado, "Num", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbClave.DataBindings.Clear();
            this.TxbClave.DataBindings.Add("Text", this.empleado, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.TxbRFC.DataBindings.Clear();
            this.TxbRFC.DataBindings.Add("Text", this.empleado, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.TxbCURP.DataBindings.Clear();
            this.TxbCURP.DataBindings.Add("Text", this.empleado, "CURP", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.TxbNombre.DataBindings.Clear();
            this.TxbNombre.DataBindings.Add("Text", this.empleado, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.TxbPrimerApellido.DataBindings.Clear();
            this.TxbPrimerApellido.DataBindings.Add("Text", this.empleado, "PrimerApellido", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.TxbSegundoApellido.DataBindings.Clear();
            this.TxbSegundoApellido.DataBindings.Add("Text", this.empleado, "SegundoApellido", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.TxbTelefono.DataBindings.Clear();
            this.TxbTelefono.DataBindings.Add("Text", this.empleado, "Telefono", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbTelefono2.DataBindings.Clear();
            this.TxbTelefono2.DataBindings.Add("Text", this.empleado, "Telefono2", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.TxbCorreo.DataBindings.Clear();
            this.TxbCorreo.DataBindings.Add("Text", this.empleado, "Correo", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.TxbSitio.DataBindings.Clear();
            this.TxbSitio.DataBindings.Add("Text", this.empleado, "Sitio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboPerioricidadPago.DataBindings.Clear();
            this.CboPerioricidadPago.DataBindings.Add("SelectedValue", this.empleado, "PeriodicidadPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbSalarioBase.DataBindings.Clear();
            this.TxbSalarioBase.DataBindings.Add("Text", this.empleado, "SalarioBase", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbSalarioDiario.DataBindings.Clear();
            this.TxbSalarioDiario.DataBindings.Add("Text", this.empleado, "SalarioDiario", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbNumFonacot.DataBindings.Clear();
            this.TxbNumFonacot.DataBindings.Add("Text", this.empleado, "NumFonacot", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbAfore.DataBindings.Clear();
            this.TxbAfore.DataBindings.Add("Text", this.empleado, "Afore", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbNumSeguridadSocial.DataBindings.Clear();
            this.TxbNumSeguridadSocial.DataBindings.Add("Text", this.empleado, "NumSeguridadSocial", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbUMF.DataBindings.Clear();
            this.TxbUMF.DataBindings.Add("Text", this.empleado, "UMF", true, DataSourceUpdateMode.OnPropertyChanged);

            this.DtFechaNacimiento.DataBindings.Clear();
            this.DtFechaNacimiento.DataBindings.Add("Value", this.empleado, "FechaNacimiento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.DtFechaInicioRel.DataBindings.Clear();
            this.DtFechaInicioRel.DataBindings.Add("Value", this.empleado, "FechaInicioRelLaboral", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboTipoContrato.DataBindings.Clear();
            this.CboTipoContrato.DataBindings.Add("SelectedValue", this.empleado, "TipoContrato", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.CboRiesgoPuesto.DataBindings.Clear();
            this.CboRiesgoPuesto.DataBindings.Add("SelectedValue", this.empleado, "RiesgoPuesto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboDepartamento.DataBindings.Clear();
            this.CboDepartamento.DataBindings.Add("Text", this.empleado, "Departamento", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboDepartamento.DataBindings.Add("SelectedValue", this.empleado, "IdDepto", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.CboPuesto.DataBindings.Clear();
            this.CboPuesto.DataBindings.Add("Text", this.empleado, "Puesto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboBancos.DataBindings.Clear();
            this.CboBancos.DataBindings.Add("SelectedValue", this.empleado, "ClaveBanco", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxtCuentaBancaria.DataBindings.Clear();
            this.TxtCuentaBancaria.DataBindings.Add("Text", this.empleado, "CuentaBancaria", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboFormaPago.DataBindings.Clear();
            this.CboFormaPago.DataBindings.Add("SelectedValue", this.empleado, "MetodoPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbCuentaCLABE.DataBindings.Clear();
            this.TxbCuentaCLABE.DataBindings.Add("Text", this.empleado, "CLABE", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbBancoSucursal.DataBindings.Clear();
            this.TxbBancoSucursal.DataBindings.Add("Text", this.empleado, "Sucursal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboTipoCuenta.DataBindings.Clear();
            this.CboTipoCuenta.DataBindings.Add("SelectedValue", this.empleado, "TipoCuenta", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxtCuentaValesDespensa.DataBindings.Clear();
            this.TxtCuentaValesDespensa.DataBindings.Add("Text", this.empleado, "CuentaTarjetaValesDespensa", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxtJornadasTrabajo.DataBindings.Clear();
            this.TxtJornadasTrabajo.DataBindings.Add("Text", this.empleado, "JornadasTrabajo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxtHorasJornada.DataBindings.Clear();
            this.TxtHorasJornada.DataBindings.Add("Text", this.empleado, "HorasJornadaTrabajo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxtSalarioDiarioIntegrado.DataBindings.Clear();
            this.TxtSalarioDiarioIntegrado.DataBindings.Add("Text", this.empleado, "SalarioDiarioIntegrado", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.CboTipoJornada.DataBindings.Clear();
            this.CboTipoJornada.DataBindings.Add("SelectedValue", this.empleado, "TipoJornada", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboRegimenFiscal.DataBindings.Clear();
            this.CboRegimenFiscal.DataBindings.Add("SelectedValue", this.empleado, "TipoRegimen", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboGenero.DataBindings.Clear();
            this.CboGenero.DataBindings.Add("SelectedValue", this.empleado, "Genero", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboEntidadFedNacimiento.DataBindings.Clear();
            this.CboEntidadFedNacimiento.DataBindings.Add("SelectedValue", this.empleado, "EntidadFederativaNacimiento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbAyudaTransporte.DataBindings.Clear();
            this.TxbAyudaTransporte.DataBindings.Add("Text", this.empleado, "AyudaTransporte", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbBecasTrabajadores.DataBindings.Clear();
            this.TxbBecasTrabajadores.DataBindings.Add("Text", this.empleado, "Becas", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.TxbOtrosPremiosAsistencia.DataBindings.Clear();
            this.TxbOtrosPremiosAsistencia.DataBindings.Add("Text", this.empleado, "PremiosAsistencia", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbPagoCreditoInfonavit.DataBindings.Clear();
            this.TxbPagoCreditoInfonavit.DataBindings.Add("Text", this.empleado, "CreditoVivienda", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbPremiosPorPuntualidad.DataBindings.Clear();
            this.TxbPremiosPorPuntualidad.DataBindings.Add("Text", this.empleado, "PremiosPuntualidad", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbAyudaArticulosEscolares.DataBindings.Clear();
            this.TxbAyudaArticulosEscolares.DataBindings.Add("Text", this.empleado, "AyudaArtEscolares", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboTablaProductividad.DataBindings.Clear();
            this.CboTablaProductividad.DataBindings.Add("SelectedValue", this.empleado, "ProductividadId", true, DataSourceUpdateMode.OnPropertyChanged);

            this.switchActivo.DataBindings.Clear();
            this.switchActivo.DataBindings.Add("Value", this.empleado, "IsActive", true, DataSourceUpdateMode.OnPropertyChanged);

            this.dtFechaTerminoRelacion.Enabled = !this.empleado.IsActive;
            this.TxbClave.Enabled = !(this.empleado.Num > 0);

            this.gridDocumentos.DataSource = this.empleado.Documentos;
            this.ToolBarHostActivo.Enabled = this.empleado.Id > 0;
            this.ToolBarButtonGuardar.Enabled = true;
            this.ToolBarButtonImprimir.Enabled = this.empleado.Id > 0;
        }

        private void Guardar()
        {
            this.empleado = this.catalogo.Save(this.empleado);
            this.CreateBinding();
        }

    }
}
