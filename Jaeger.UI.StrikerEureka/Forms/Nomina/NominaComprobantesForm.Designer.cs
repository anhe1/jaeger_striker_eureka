﻿namespace Jaeger.Views.Nomina
{
    partial class NominaComprobantesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn38 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn39 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NominaComprobantesForm));
            this.PanelIzquierdo = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.GrupoBusqueda = new Telerik.WinControls.UI.RadGroupBox();
            this.ButtonBuscar = new Telerik.WinControls.UI.RadButton();
            this.CboEmpleados = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CheckPorEmpleado = new Telerik.WinControls.UI.RadCheckBox();
            this.CboDepartamentos = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CheckPorDepartamento = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.DateFechaFinal = new Telerik.WinControls.UI.RadDateTimePicker();
            this.DateFechaInicial = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.ButtonSplitFechas = new Telerik.WinControls.UI.RadSplitButton();
            this.RadioButtonRangoFechas = new Telerik.WinControls.UI.RadRadioButton();
            this.TxbIdDocumento = new Telerik.WinControls.UI.RadTextBox();
            this.RadioButtonIdDocumento = new Telerik.WinControls.UI.RadRadioButton();
            this.CboNominas = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CheckPorNomina = new Telerik.WinControls.UI.RadCheckBox();
            this.CommandBarNominas = new Telerik.WinControls.UI.RadCommandBar();
            this.BarRowElementNominas = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarNominas = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarButtonNuevo = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonEditar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonCancelar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarButtonExportar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonCorreo = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonCorreoTodos = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonCorreoSeleccionado = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonDescargar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonHerramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonPoliza1 = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonValidar = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonValidarEstado = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.BarraEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.BarraEstadoLabel = new Telerik.WinControls.UI.RadLabelElement();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsSpinnerWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement();
            this.Imagenes = new System.Windows.Forms.ImageList(this.components);
            this.ToolBarButtonPoliza2 = new Telerik.WinControls.UI.RadMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.PanelIzquierdo)).BeginInit();
            this.PanelIzquierdo.PanelContainer.SuspendLayout();
            this.PanelIzquierdo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrupoBusqueda)).BeginInit();
            this.GrupoBusqueda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmpleados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmpleados.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmpleados.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPorEmpleado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDepartamentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDepartamentos.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDepartamentos.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPorDepartamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateFechaFinal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateFechaInicial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonSplitFechas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadioButtonRangoFechas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbIdDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadioButtonIdDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboNominas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboNominas.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboNominas.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPorNomina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBarNominas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            this.GridData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelIzquierdo
            // 
            this.PanelIzquierdo.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelIzquierdo.ExpandDirection = Telerik.WinControls.UI.RadDirection.Right;
            this.PanelIzquierdo.Location = new System.Drawing.Point(0, 0);
            this.PanelIzquierdo.Name = "PanelIzquierdo";
            this.PanelIzquierdo.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 150, 648);
            // 
            // PanelIzquierdo.PanelContainer
            // 
            this.PanelIzquierdo.PanelContainer.Controls.Add(this.GrupoBusqueda);
            this.PanelIzquierdo.PanelContainer.Size = new System.Drawing.Size(347, 646);
            this.PanelIzquierdo.Size = new System.Drawing.Size(375, 648);
            this.PanelIzquierdo.TabIndex = 0;
            // 
            // GrupoBusqueda
            // 
            this.GrupoBusqueda.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GrupoBusqueda.Controls.Add(this.ButtonBuscar);
            this.GrupoBusqueda.Controls.Add(this.CboEmpleados);
            this.GrupoBusqueda.Controls.Add(this.CheckPorEmpleado);
            this.GrupoBusqueda.Controls.Add(this.CboDepartamentos);
            this.GrupoBusqueda.Controls.Add(this.CheckPorDepartamento);
            this.GrupoBusqueda.Controls.Add(this.radLabel3);
            this.GrupoBusqueda.Controls.Add(this.radLabel2);
            this.GrupoBusqueda.Controls.Add(this.DateFechaFinal);
            this.GrupoBusqueda.Controls.Add(this.DateFechaInicial);
            this.GrupoBusqueda.Controls.Add(this.radLabel1);
            this.GrupoBusqueda.Controls.Add(this.ButtonSplitFechas);
            this.GrupoBusqueda.Controls.Add(this.RadioButtonRangoFechas);
            this.GrupoBusqueda.Controls.Add(this.TxbIdDocumento);
            this.GrupoBusqueda.Controls.Add(this.RadioButtonIdDocumento);
            this.GrupoBusqueda.Controls.Add(this.CboNominas);
            this.GrupoBusqueda.Controls.Add(this.CheckPorNomina);
            this.GrupoBusqueda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GrupoBusqueda.HeaderText = "Busqueda";
            this.GrupoBusqueda.Location = new System.Drawing.Point(0, 0);
            this.GrupoBusqueda.Name = "GrupoBusqueda";
            this.GrupoBusqueda.Size = new System.Drawing.Size(347, 646);
            this.GrupoBusqueda.TabIndex = 0;
            this.GrupoBusqueda.Text = "Busqueda";
            // 
            // ButtonBuscar
            // 
            this.ButtonBuscar.Location = new System.Drawing.Point(210, 382);
            this.ButtonBuscar.Name = "ButtonBuscar";
            this.ButtonBuscar.Size = new System.Drawing.Size(110, 24);
            this.ButtonBuscar.TabIndex = 15;
            this.ButtonBuscar.Text = "Buscar";
            this.ButtonBuscar.Click += new System.EventHandler(this.ButtonBuscar_Click);
            // 
            // CboEmpleados
            // 
            // 
            // CboEmpleados.NestedRadGridView
            // 
            this.CboEmpleados.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboEmpleados.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboEmpleados.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboEmpleados.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboEmpleados.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboEmpleados.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboEmpleados.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.CboEmpleados.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboEmpleados.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboEmpleados.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.CboEmpleados.EditorControl.Name = "NestedRadGridView";
            this.CboEmpleados.EditorControl.ReadOnly = true;
            this.CboEmpleados.EditorControl.ShowGroupPanel = false;
            this.CboEmpleados.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboEmpleados.EditorControl.TabIndex = 0;
            this.CboEmpleados.Location = new System.Drawing.Point(14, 340);
            this.CboEmpleados.Name = "CboEmpleados";
            this.CboEmpleados.NullText = "Empleado";
            this.CboEmpleados.Size = new System.Drawing.Size(306, 20);
            this.CboEmpleados.TabIndex = 14;
            this.CboEmpleados.TabStop = false;
            // 
            // CheckPorEmpleado
            // 
            this.CheckPorEmpleado.Location = new System.Drawing.Point(14, 316);
            this.CheckPorEmpleado.Name = "CheckPorEmpleado";
            this.CheckPorEmpleado.Size = new System.Drawing.Size(90, 18);
            this.CheckPorEmpleado.TabIndex = 13;
            this.CheckPorEmpleado.Text = "Por Empleado";
            // 
            // CboDepartamentos
            // 
            // 
            // CboDepartamentos.NestedRadGridView
            // 
            this.CboDepartamentos.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboDepartamentos.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboDepartamentos.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboDepartamentos.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboDepartamentos.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboDepartamentos.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboDepartamentos.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.CboDepartamentos.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboDepartamentos.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboDepartamentos.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.CboDepartamentos.EditorControl.Name = "NestedRadGridView";
            this.CboDepartamentos.EditorControl.ReadOnly = true;
            this.CboDepartamentos.EditorControl.ShowGroupPanel = false;
            this.CboDepartamentos.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboDepartamentos.EditorControl.TabIndex = 0;
            this.CboDepartamentos.Location = new System.Drawing.Point(14, 290);
            this.CboDepartamentos.Name = "CboDepartamentos";
            this.CboDepartamentos.NullText = "Departamento";
            this.CboDepartamentos.Size = new System.Drawing.Size(306, 20);
            this.CboDepartamentos.TabIndex = 12;
            this.CboDepartamentos.TabStop = false;
            // 
            // CheckPorDepartamento
            // 
            this.CheckPorDepartamento.Location = new System.Drawing.Point(14, 266);
            this.CheckPorDepartamento.Name = "CheckPorDepartamento";
            this.CheckPorDepartamento.Size = new System.Drawing.Size(113, 18);
            this.CheckPorDepartamento.TabIndex = 11;
            this.CheckPorDepartamento.Text = "Por Departamento";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(14, 232);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(19, 18);
            this.radLabel3.TabIndex = 10;
            this.radLabel3.Text = "Al:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(14, 204);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(25, 18);
            this.radLabel2.TabIndex = 9;
            this.radLabel2.Text = "Del:";
            // 
            // DateFechaFinal
            // 
            this.DateFechaFinal.Location = new System.Drawing.Point(45, 230);
            this.DateFechaFinal.Name = "DateFechaFinal";
            this.DateFechaFinal.Size = new System.Drawing.Size(208, 20);
            this.DateFechaFinal.TabIndex = 8;
            this.DateFechaFinal.TabStop = false;
            this.DateFechaFinal.Text = "jueves, 1 de noviembre de 2018";
            this.DateFechaFinal.Value = new System.DateTime(2018, 11, 1, 22, 10, 26, 820);
            // 
            // DateFechaInicial
            // 
            this.DateFechaInicial.Location = new System.Drawing.Point(45, 204);
            this.DateFechaInicial.Name = "DateFechaInicial";
            this.DateFechaInicial.Size = new System.Drawing.Size(208, 20);
            this.DateFechaInicial.TabIndex = 7;
            this.DateFechaInicial.TabStop = false;
            this.DateFechaInicial.Text = "jueves, 1 de noviembre de 2018";
            this.DateFechaInicial.Value = new System.DateTime(2018, 11, 1, 22, 10, 26, 820);
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(14, 174);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(73, 18);
            this.radLabel1.TabIndex = 6;
            this.radLabel1.Text = "Por Fecha de:";
            // 
            // ButtonSplitFechas
            // 
            this.ButtonSplitFechas.Location = new System.Drawing.Point(112, 174);
            this.ButtonSplitFechas.Name = "ButtonSplitFechas";
            this.ButtonSplitFechas.Size = new System.Drawing.Size(110, 24);
            this.ButtonSplitFechas.TabIndex = 5;
            this.ButtonSplitFechas.Text = "Selecciona";
            // 
            // RadioButtonRangoFechas
            // 
            this.RadioButtonRangoFechas.Location = new System.Drawing.Point(14, 150);
            this.RadioButtonRangoFechas.Name = "RadioButtonRangoFechas";
            this.RadioButtonRangoFechas.Size = new System.Drawing.Size(120, 18);
            this.RadioButtonRangoFechas.TabIndex = 4;
            this.RadioButtonRangoFechas.Text = "Por rango de fechas";
            // 
            // TxbIdDocumento
            // 
            this.TxbIdDocumento.Location = new System.Drawing.Point(14, 114);
            this.TxbIdDocumento.MaxLength = 36;
            this.TxbIdDocumento.Name = "TxbIdDocumento";
            this.TxbIdDocumento.NullText = "UUID";
            this.TxbIdDocumento.Size = new System.Drawing.Size(306, 20);
            this.TxbIdDocumento.TabIndex = 3;
            // 
            // RadioButtonIdDocumento
            // 
            this.RadioButtonIdDocumento.Location = new System.Drawing.Point(14, 90);
            this.RadioButtonIdDocumento.Name = "RadioButtonIdDocumento";
            this.RadioButtonIdDocumento.Size = new System.Drawing.Size(131, 18);
            this.RadioButtonIdDocumento.TabIndex = 2;
            this.RadioButtonIdDocumento.Text = "Por Folio Fiscal (UUID)";
            // 
            // CboNominas
            // 
            // 
            // CboNominas.NestedRadGridView
            // 
            this.CboNominas.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboNominas.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboNominas.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboNominas.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboNominas.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboNominas.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboNominas.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "_nmnctrl_id";
            gridViewTextBoxColumn1.HeaderText = "Núm.";
            gridViewTextBoxColumn1.Name = "NoControl";
            gridViewTextBoxColumn2.FieldName = "_nmnctrl_ttl";
            gridViewTextBoxColumn2.HeaderText = "Nombre";
            gridViewTextBoxColumn2.Name = "Nombre";
            gridViewTextBoxColumn2.Width = 260;
            gridViewTextBoxColumn3.FieldName = "_nmnctrl_tp";
            gridViewTextBoxColumn3.HeaderText = "Tipo";
            gridViewTextBoxColumn3.Name = "ClaveTipoNomina";
            gridViewTextBoxColumn4.FieldName = "_nmnctrl_fch";
            gridViewTextBoxColumn4.FormatString = "{0:d}";
            gridViewTextBoxColumn4.HeaderText = "Fecha";
            gridViewTextBoxColumn4.Name = "FechaNomina";
            gridViewTextBoxColumn4.Width = 85;
            this.CboNominas.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.CboNominas.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboNominas.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboNominas.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.CboNominas.EditorControl.Name = "NestedRadGridView";
            this.CboNominas.EditorControl.ReadOnly = true;
            this.CboNominas.EditorControl.ShowGroupPanel = false;
            this.CboNominas.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboNominas.EditorControl.TabIndex = 0;
            this.CboNominas.Location = new System.Drawing.Point(14, 53);
            this.CboNominas.Name = "CboNominas";
            this.CboNominas.NullText = "Selecciona";
            this.CboNominas.Size = new System.Drawing.Size(306, 20);
            this.CboNominas.TabIndex = 1;
            this.CboNominas.TabStop = false;
            this.CboNominas.ValueMember = "_nmnctrl_id";
            // 
            // CheckPorNomina
            // 
            this.CheckPorNomina.Location = new System.Drawing.Point(14, 29);
            this.CheckPorNomina.Name = "CheckPorNomina";
            this.CheckPorNomina.Size = new System.Drawing.Size(80, 18);
            this.CheckPorNomina.TabIndex = 0;
            this.CheckPorNomina.Text = "Por Nómina";
            // 
            // CommandBarNominas
            // 
            this.CommandBarNominas.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBarNominas.Location = new System.Drawing.Point(375, 0);
            this.CommandBarNominas.Name = "CommandBarNominas";
            this.CommandBarNominas.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.BarRowElementNominas});
            this.CommandBarNominas.Size = new System.Drawing.Size(920, 38);
            this.CommandBarNominas.TabIndex = 1;
            // 
            // BarRowElementNominas
            // 
            this.BarRowElementNominas.MinSize = new System.Drawing.Size(25, 25);
            this.BarRowElementNominas.Name = "BarRowElementNominas";
            this.BarRowElementNominas.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarNominas});
            // 
            // ToolBarNominas
            // 
            this.ToolBarNominas.DisplayName = "commandBarStripElement1";
            this.ToolBarNominas.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarButtonNuevo,
            this.ToolBarButtonEditar,
            this.ToolBarButtonCancelar,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonFiltro,
            this.ToolBarButtonExportar,
            this.ToolBarButtonCorreo,
            this.ToolBarButtonDescargar,
            this.ToolBarButtonHerramientas,
            this.ToolBarButtonValidar,
            this.ToolBarButtonCerrar});
            this.ToolBarNominas.Name = "ToolBarNominas";
            // 
            // ToolBarButtonNuevo
            // 
            this.ToolBarButtonNuevo.DisplayName = "Nuevo";
            this.ToolBarButtonNuevo.DrawText = true;
            this.ToolBarButtonNuevo.Enabled = false;
            this.ToolBarButtonNuevo.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_agregar_archivo;
            this.ToolBarButtonNuevo.Name = "ToolBarButtonNuevo";
            this.ToolBarButtonNuevo.Text = "Nuevo";
            this.ToolBarButtonNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonEditar
            // 
            this.ToolBarButtonEditar.DisplayName = "Editar";
            this.ToolBarButtonEditar.DrawText = true;
            this.ToolBarButtonEditar.Enabled = false;
            this.ToolBarButtonEditar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_edit_file;
            this.ToolBarButtonEditar.Name = "ToolBarButtonEditar";
            this.ToolBarButtonEditar.Text = "Editar";
            this.ToolBarButtonEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonEditar.Click += new System.EventHandler(this.ToolBarButtonEditar_Click);
            // 
            // ToolBarButtonCancelar
            // 
            this.ToolBarButtonCancelar.DisplayName = "Cancelar";
            this.ToolBarButtonCancelar.DrawText = true;
            this.ToolBarButtonCancelar.Enabled = false;
            this.ToolBarButtonCancelar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_delete_file;
            this.ToolBarButtonCancelar.Name = "ToolBarButtonCancelar";
            this.ToolBarButtonCancelar.Text = "Cancelar";
            this.ToolBarButtonCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCancelar.Click += new System.EventHandler(this.ToolBarButtonCancelar_Click);
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.DisplayName = "Actualizar";
            this.ToolBarButtonActualizar.DrawText = true;
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_actualizar;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.ToolBarButtonActualizar_Click);
            // 
            // ToolBarButtonFiltro
            // 
            this.ToolBarButtonFiltro.DisplayName = "commandBarToggleButton1";
            this.ToolBarButtonFiltro.DrawText = true;
            this.ToolBarButtonFiltro.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_filtrar;
            this.ToolBarButtonFiltro.Name = "ToolBarButtonFiltro";
            this.ToolBarButtonFiltro.Text = "Filtro";
            this.ToolBarButtonFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonFiltro.Click += new System.EventHandler(this.ToolBarButtonFiltro_Click);
            // 
            // ToolBarButtonExportar
            // 
            this.ToolBarButtonExportar.DisplayName = "Exportar";
            this.ToolBarButtonExportar.DrawText = true;
            this.ToolBarButtonExportar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_excel;
            this.ToolBarButtonExportar.Name = "ToolBarButtonExportar";
            this.ToolBarButtonExportar.Text = "Exportar";
            this.ToolBarButtonExportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonExportar.Click += new System.EventHandler(this.ToolBarButtonExportar_Click);
            // 
            // ToolBarButtonCorreo
            // 
            this.ToolBarButtonCorreo.DisplayName = "commandBarDropDownButton1";
            this.ToolBarButtonCorreo.DrawText = true;
            this.ToolBarButtonCorreo.Enabled = false;
            this.ToolBarButtonCorreo.Image = global::Jaeger.UI.Properties.Resources.icons8_x30_enviar;
            this.ToolBarButtonCorreo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonCorreoTodos,
            this.ToolBarButtonCorreoSeleccionado});
            this.ToolBarButtonCorreo.Name = "ToolBarButtonCorreo";
            this.ToolBarButtonCorreo.Text = "Correo";
            this.ToolBarButtonCorreo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonCorreoTodos
            // 
            this.ToolBarButtonCorreoTodos.Name = "ToolBarButtonCorreoTodos";
            this.ToolBarButtonCorreoTodos.Text = "Todos";
            // 
            // ToolBarButtonCorreoSeleccionado
            // 
            this.ToolBarButtonCorreoSeleccionado.Name = "ToolBarButtonCorreoSeleccionado";
            this.ToolBarButtonCorreoSeleccionado.Text = "Seleccionado";
            // 
            // ToolBarButtonDescargar
            // 
            this.ToolBarButtonDescargar.DisplayName = "Descargar";
            this.ToolBarButtonDescargar.DrawText = true;
            this.ToolBarButtonDescargar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_descargar_desde_nube;
            this.ToolBarButtonDescargar.Name = "ToolBarButtonDescargar";
            this.ToolBarButtonDescargar.Text = "Descargar";
            this.ToolBarButtonDescargar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonDescargar.Click += new System.EventHandler(this.ToolBarButtonDescargar_Click);
            // 
            // ToolBarButtonHerramientas
            // 
            this.ToolBarButtonHerramientas.DisplayName = "commandBarButton1";
            this.ToolBarButtonHerramientas.DrawImage = true;
            this.ToolBarButtonHerramientas.DrawText = false;
            this.ToolBarButtonHerramientas.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_caja_herramientas;
            this.ToolBarButtonHerramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonPoliza1,
            this.ToolBarButtonPoliza2});
            this.ToolBarButtonHerramientas.Name = "ToolBarButtonHerramientas";
            this.ToolBarButtonHerramientas.Text = "Herramientas";
            // 
            // ToolBarButtonPoliza1
            // 
            this.ToolBarButtonPoliza1.Name = "ToolBarButtonPoliza1";
            this.ToolBarButtonPoliza1.Text = "Poliza 1";
            this.ToolBarButtonPoliza1.Click += new System.EventHandler(this.ToolBarButtonPoliza_Click);
            // 
            // ToolBarButtonValidar
            // 
            this.ToolBarButtonValidar.DisplayName = "Herramientas";
            this.ToolBarButtonValidar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_seguridad_comprobado;
            this.ToolBarButtonValidar.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonValidarEstado});
            this.ToolBarButtonValidar.Name = "ToolBarButtonValidar";
            this.ToolBarButtonValidar.Text = "Herramientas";
            this.ToolBarButtonValidar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonValidarEstado
            // 
            this.ToolBarButtonValidarEstado.Name = "ToolBarButtonValidarEstado";
            this.ToolBarButtonValidarEstado.Text = "Consulta Estado SAT";
            this.ToolBarButtonValidarEstado.ToolTipText = "Consultar el estado del comprobante";
            this.ToolBarButtonValidarEstado.Click += new System.EventHandler(this.ToolBarButtonValidarEstado_Click);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "Cerrar";
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_cerrar;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // BarraEstado
            // 
            this.BarraEstado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.BarraEstadoLabel});
            this.BarraEstado.Location = new System.Drawing.Point(375, 622);
            this.BarraEstado.Name = "BarraEstado";
            this.BarraEstado.Size = new System.Drawing.Size(920, 26);
            this.BarraEstado.TabIndex = 2;
            // 
            // BarraEstadoLabel
            // 
            this.BarraEstadoLabel.Name = "BarraEstadoLabel";
            this.BarraEstado.SetSpring(this.BarraEstadoLabel, false);
            this.BarraEstadoLabel.Text = "Listo.";
            this.BarraEstadoLabel.TextWrap = true;
            // 
            // GridData
            // 
            this.GridData.Controls.Add(this.Espera);
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(375, 38);
            // 
            // 
            // 
            gridViewTextBoxColumn5.DataType = typeof(int);
            gridViewTextBoxColumn5.FieldName = "IdNomina";
            gridViewTextBoxColumn5.HeaderText = "Id";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "IdNomina";
            gridViewTextBoxColumn5.VisibleInColumnChooser = false;
            gridViewTextBoxColumn6.DataType = typeof(int);
            gridViewTextBoxColumn6.FieldName = "IdDirectorio";
            gridViewTextBoxColumn6.HeaderText = "IdDirectorio";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "IdDirectorio";
            gridViewTextBoxColumn6.VisibleInColumnChooser = false;
            gridViewTextBoxColumn7.DataType = typeof(int);
            gridViewTextBoxColumn7.FieldName = "IdComprobante";
            gridViewTextBoxColumn7.HeaderText = "IdComprobante";
            gridViewTextBoxColumn7.IsVisible = false;
            gridViewTextBoxColumn7.Name = "IdComprobante";
            gridViewTextBoxColumn7.VisibleInColumnChooser = false;
            gridViewTextBoxColumn8.FieldName = "ClaveTipoRegimen";
            gridViewTextBoxColumn8.HeaderText = "Tipo Reg.";
            gridViewTextBoxColumn8.Name = "ClaveTipoRegimen";
            gridViewTextBoxColumn9.FieldName = "NoEmpleado";
            gridViewTextBoxColumn9.HeaderText = "Núm. Empleado";
            gridViewTextBoxColumn9.Name = "NoEmpleado";
            gridViewTextBoxColumn10.FieldName = "ReceptorNombre";
            gridViewTextBoxColumn10.HeaderText = "Empleado";
            gridViewTextBoxColumn10.Name = "Empleado";
            gridViewTextBoxColumn10.Width = 250;
            gridViewTextBoxColumn11.FieldName = "Departamento";
            gridViewTextBoxColumn11.HeaderText = "Departamento";
            gridViewTextBoxColumn11.Name = "Departamento";
            gridViewTextBoxColumn11.Width = 150;
            gridViewTextBoxColumn12.FieldName = "Puesto";
            gridViewTextBoxColumn12.HeaderText = "Puesto";
            gridViewTextBoxColumn12.Name = "Puesto";
            gridViewTextBoxColumn12.Width = 150;
            gridViewTextBoxColumn13.FieldName = "ClaveRiesgoPuesto";
            gridViewTextBoxColumn13.HeaderText = "Riesgo de Puesto";
            gridViewTextBoxColumn13.Name = "ClaveRiesgoPuesto";
            gridViewTextBoxColumn14.FieldName = "ClaveTipoContrato";
            gridViewTextBoxColumn14.HeaderText = "Tipo de Contrato";
            gridViewTextBoxColumn14.Name = "ClaveTipoContrato";
            gridViewTextBoxColumn15.FieldName = "ClaveTipoJornada";
            gridViewTextBoxColumn15.HeaderText = "Tipo Jornada";
            gridViewTextBoxColumn15.Name = "ClaveTipoJornada";
            gridViewTextBoxColumn16.FieldName = "ClavePeriricidadPago";
            gridViewTextBoxColumn16.HeaderText = "Perioricidad de Pago";
            gridViewTextBoxColumn16.Name = "ClavePeriricidadPago";
            gridViewTextBoxColumn17.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn17.FieldName = "FechaPago";
            gridViewTextBoxColumn17.FormatString = "{0:d}";
            gridViewTextBoxColumn17.HeaderText = "Fec. Pago";
            gridViewTextBoxColumn17.Name = "FechaPago";
            gridViewTextBoxColumn17.Width = 85;
            gridViewTextBoxColumn18.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn18.FieldName = "FechaInicialPago";
            gridViewTextBoxColumn18.FormatString = "{0:d}";
            gridViewTextBoxColumn18.HeaderText = "Fec. Ini. Pago";
            gridViewTextBoxColumn18.Name = "FechaInicialPago";
            gridViewTextBoxColumn18.Width = 85;
            gridViewTextBoxColumn19.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn19.FieldName = "FechaFinalPago";
            gridViewTextBoxColumn19.FormatString = "{0:d}";
            gridViewTextBoxColumn19.HeaderText = "Fec. Final Pago";
            gridViewTextBoxColumn19.Name = "FechaFinalPago";
            gridViewTextBoxColumn19.Width = 85;
            gridViewTextBoxColumn20.FieldName = "Antiguedad";
            gridViewTextBoxColumn20.HeaderText = "Antiguedad";
            gridViewTextBoxColumn20.Name = "Antiguedad";
            gridViewTextBoxColumn21.FieldName = "NumDiasPagados";
            gridViewTextBoxColumn21.HeaderText = "Días Pagados";
            gridViewTextBoxColumn21.Name = "DiasPagados";
            gridViewTextBoxColumn22.DataType = typeof(decimal);
            gridViewTextBoxColumn22.FieldName = "SalarioBaseCotApor";
            gridViewTextBoxColumn22.FormatString = "{0:n}";
            gridViewTextBoxColumn22.HeaderText = "Salario Base de Cotización";
            gridViewTextBoxColumn22.Name = "SalarioBaseCotizacion";
            gridViewTextBoxColumn22.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn23.DataType = typeof(decimal);
            gridViewTextBoxColumn23.FieldName = "SalarioDiarioIntegrado";
            gridViewTextBoxColumn23.FormatString = "{0:n}";
            gridViewTextBoxColumn23.HeaderText = "Salario Diario Integrado";
            gridViewTextBoxColumn23.Name = "SalarioDiarioIntegrado";
            gridViewTextBoxColumn23.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn24.DataType = typeof(decimal);
            gridViewTextBoxColumn24.FieldName = "PercepcionTotalGravado";
            gridViewTextBoxColumn24.FormatString = "{0:n}";
            gridViewTextBoxColumn24.HeaderText = "P. Total Gravado";
            gridViewTextBoxColumn24.Name = "PercepcionTotalGravado";
            gridViewTextBoxColumn24.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn25.DataType = typeof(decimal);
            gridViewTextBoxColumn25.FieldName = "PercepcionTotalExento";
            gridViewTextBoxColumn25.FormatString = "{0:n}";
            gridViewTextBoxColumn25.HeaderText = "P. Total Exento";
            gridViewTextBoxColumn25.Name = "PercepcionTotalExento";
            gridViewTextBoxColumn25.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn26.DataType = typeof(decimal);
            gridViewTextBoxColumn26.FieldName = "DeduccionTotalGravado";
            gridViewTextBoxColumn26.FormatString = "{0:n}";
            gridViewTextBoxColumn26.HeaderText = "D. Total Gravado";
            gridViewTextBoxColumn26.Name = "DeduccionTotalGravado";
            gridViewTextBoxColumn26.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn27.DataType = typeof(decimal);
            gridViewTextBoxColumn27.FieldName = "DeduccionTotalExento";
            gridViewTextBoxColumn27.FormatString = "{0:n}";
            gridViewTextBoxColumn27.HeaderText = "D. Total Exento";
            gridViewTextBoxColumn27.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn27.Name = "DeduccionTotalExento";
            gridViewTextBoxColumn27.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn28.DataType = typeof(decimal);
            gridViewTextBoxColumn28.FieldName = "DescuentoIncapacidad";
            gridViewTextBoxColumn28.FormatString = "{0:n}";
            gridViewTextBoxColumn28.HeaderText = "Descuento por Incapacidad";
            gridViewTextBoxColumn28.Name = "DescuentoIncapacidad";
            gridViewTextBoxColumn28.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn29.DataType = typeof(decimal);
            gridViewTextBoxColumn29.FieldName = "HorasExtra";
            gridViewTextBoxColumn29.FormatString = "{0:n}";
            gridViewTextBoxColumn29.HeaderText = "Horas Extra";
            gridViewTextBoxColumn29.Name = "HorasExtra";
            gridViewTextBoxColumn29.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn30.DataType = typeof(decimal);
            gridViewTextBoxColumn30.FieldName = "Total";
            gridViewTextBoxColumn30.FormatString = "{0:n}";
            gridViewTextBoxColumn30.HeaderText = "Total";
            gridViewTextBoxColumn30.Name = "Total";
            gridViewTextBoxColumn30.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn31.FieldName = "FechaInicioRelLaboral";
            gridViewTextBoxColumn31.HeaderText = "Fec. Relacion";
            gridViewTextBoxColumn31.Name = "FechaInicioRelLaboral";
            gridViewTextBoxColumn32.FieldName = "ClaveTipoNomina";
            gridViewTextBoxColumn32.HeaderText = "T. Nómina";
            gridViewTextBoxColumn32.Name = "ClaveTipoNomina";
            gridViewTextBoxColumn33.FieldName = "IdDocumento";
            gridViewTextBoxColumn33.HeaderText = "UUID";
            gridViewTextBoxColumn33.Name = "IdDocumento";
            gridViewTextBoxColumn33.Width = 240;
            gridViewTextBoxColumn34.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn34.HeaderText = "RFC";
            gridViewTextBoxColumn34.Name = "ReceptorRFC";
            gridViewTextBoxColumn34.Width = 85;
            gridViewTextBoxColumn35.FieldName = "EstadoSAT";
            gridViewTextBoxColumn35.HeaderText = "Estado SAT";
            gridViewTextBoxColumn35.Name = "EstadoSAT";
            gridViewTextBoxColumn36.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn36.FieldName = "FechaEstadoSAT";
            gridViewTextBoxColumn36.FormatString = "{0:d}";
            gridViewTextBoxColumn36.HeaderText = "Fec. Estado";
            gridViewTextBoxColumn36.Name = "FechaEstadoSAT";
            gridViewTextBoxColumn36.Width = 85;
            gridViewTextBoxColumn37.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn37.FieldName = "FechaCertificacion";
            gridViewTextBoxColumn37.FormatString = "{0:d}";
            gridViewTextBoxColumn37.HeaderText = "Fec. Certificación";
            gridViewTextBoxColumn37.Name = "FechaCertifiacion";
            gridViewTextBoxColumn37.Width = 85;
            gridViewTextBoxColumn38.FieldName = "FileXML";
            gridViewTextBoxColumn38.HeaderText = "XML";
            gridViewTextBoxColumn38.Name = "FileXML";
            gridViewTextBoxColumn38.Width = 40;
            gridViewTextBoxColumn39.FieldName = "FilePDF";
            gridViewTextBoxColumn39.HeaderText = "PDF";
            gridViewTextBoxColumn39.Name = "FilePDF";
            gridViewTextBoxColumn39.Width = 40;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32,
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34,
            gridViewTextBoxColumn35,
            gridViewTextBoxColumn36,
            gridViewTextBoxColumn37,
            gridViewTextBoxColumn38,
            gridViewTextBoxColumn39});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(920, 584);
            this.GridData.TabIndex = 3;
            this.GridData.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.GridData_CellFormatting);
            this.GridData.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridData_CellDoubleClick);
            this.GridData.Click += new System.EventHandler(this.GridData_Click);
            // 
            // Espera
            // 
            this.Espera.AssociatedControl = this.GridData;
            this.Espera.Location = new System.Drawing.Point(378, 247);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(70, 70);
            this.Espera.TabIndex = 1;
            this.Espera.Text = "radWaitingBar1";
            this.Espera.WaitingIndicators.Add(this.dotsSpinnerWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 100;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsSpinner;
            // 
            // dotsSpinnerWaitingBarIndicatorElement1
            // 
            this.dotsSpinnerWaitingBarIndicatorElement1.Name = "dotsSpinnerWaitingBarIndicatorElement1";
            // 
            // Imagenes
            // 
            this.Imagenes.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Imagenes.ImageStream")));
            this.Imagenes.TransparentColor = System.Drawing.Color.Transparent;
            this.Imagenes.Images.SetKeyName(0, "accept_button.png");
            this.Imagenes.Images.SetKeyName(1, "FilePDF");
            this.Imagenes.Images.SetKeyName(2, "FileXML");
            // 
            // ToolBarButtonPoliza2
            // 
            this.ToolBarButtonPoliza2.Name = "ToolBarButtonPoliza2";
            this.ToolBarButtonPoliza2.Text = "Poliza 2";
            this.ToolBarButtonPoliza2.Click += new System.EventHandler(this.ToolBarButtonPoliza2_Click);
            // 
            // NominaComprobantesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1295, 648);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.BarraEstado);
            this.Controls.Add(this.CommandBarNominas);
            this.Controls.Add(this.PanelIzquierdo);
            this.Name = "NominaComprobantesForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Recursos Humanos: Comprobantes";
            this.Load += new System.EventHandler(this.NominaComprobantesForm_Load);
            this.PanelIzquierdo.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PanelIzquierdo)).EndInit();
            this.PanelIzquierdo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GrupoBusqueda)).EndInit();
            this.GrupoBusqueda.ResumeLayout(false);
            this.GrupoBusqueda.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmpleados.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmpleados.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmpleados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPorEmpleado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDepartamentos.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDepartamentos.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDepartamentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPorDepartamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateFechaFinal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateFechaInicial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonSplitFechas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadioButtonRangoFechas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbIdDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadioButtonIdDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboNominas.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboNominas.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboNominas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPorNomina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBarNominas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.GridData.ResumeLayout(false);
            this.GridData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCollapsiblePanel PanelIzquierdo;
        private Telerik.WinControls.UI.RadCommandBar CommandBarNominas;
        private Telerik.WinControls.UI.CommandBarRowElement BarRowElementNominas;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBarNominas;
        private Telerik.WinControls.UI.RadStatusStrip BarraEstado;
        private Telerik.WinControls.UI.RadLabelElement BarraEstadoLabel;
        private Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.RadGroupBox GrupoBusqueda;
        private Telerik.WinControls.UI.RadButton ButtonBuscar;
        private Telerik.WinControls.UI.RadMultiColumnComboBox CboEmpleados;
        private Telerik.WinControls.UI.RadCheckBox CheckPorEmpleado;
        private Telerik.WinControls.UI.RadMultiColumnComboBox CboDepartamentos;
        private Telerik.WinControls.UI.RadCheckBox CheckPorDepartamento;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDateTimePicker DateFechaFinal;
        private Telerik.WinControls.UI.RadDateTimePicker DateFechaInicial;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadSplitButton ButtonSplitFechas;
        private Telerik.WinControls.UI.RadRadioButton RadioButtonRangoFechas;
        private Telerik.WinControls.UI.RadTextBox TxbIdDocumento;
        private Telerik.WinControls.UI.RadRadioButton RadioButtonIdDocumento;
        private Telerik.WinControls.UI.RadMultiColumnComboBox CboNominas;
        private Telerik.WinControls.UI.RadCheckBox CheckPorNomina;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonActualizar;
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement dotsSpinnerWaitingBarIndicatorElement1;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonNuevo;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonEditar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCancelar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonExportar;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonCorreo;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonCorreoTodos;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonCorreoSeleccionado;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltro;
        internal System.Windows.Forms.ImageList Imagenes;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCerrar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonDescargar;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonHerramientas;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonValidar;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonValidarEstado;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonPoliza1;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonPoliza2;
    }
}
