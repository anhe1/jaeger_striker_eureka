﻿using System;
using System.Windows.Forms;
using Jaeger.Helpers;
using Jaeger.Edita.V2.Nomina.Entities;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views
{
    public partial class ViewLayoutNomina : Telerik.WinControls.UI.RadForm
    {
        private LayoutExcelNomina12 prueba;
        private ViewModelControlNominaSingle control;

        public ViewLayoutNomina()
        {
            InitializeComponent();
        }

        private void ViewLayoutNomina_Load(object sender, EventArgs e)
        {
            this.prueba = new LayoutExcelNomina12();
            this.control = new ViewModelControlNominaSingle();
            this.GridData.TelerikGridCommon();

            this.ToolBarTextArchivo.DataBindings.Clear();
            this.ToolBarTextArchivo.DataBindings.Add("Text", this.control, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);

        }

        private void ToolBarButtonAbrir_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog()
            {
                Title = "Abrir Comprobante Fiscal",
                InitialDirectory = "",
                Filter = "*.xlsx|*.XLSX"
            };

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                this.prueba.ExcelFile = openFile.FileName;
                this.control.ArchivoExcel = System.IO.Path.GetFileName(openFile.FileName);
                this.GridData.DataSource = this.prueba.LoadNomina("");
                this.GridData.AutoGenerateHierarchy = true;
                this.GridData.BestFitColumns();
            }
        }

        private void ToolBarButtonCargar_Click(object sender, EventArgs e)
        {

        }
    }
}
