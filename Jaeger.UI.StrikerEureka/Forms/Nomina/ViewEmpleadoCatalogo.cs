﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Collections.Generic;
using Jaeger.Nomina.Entities;
using Jaeger.Nomina.Helpers;
using Jaeger.Helpers;
using Jaeger.Layout.Helpers;
using Jaeger.Layout.Entities.Nomina;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;

namespace Jaeger.Views.Nomina
{
    public partial class ViewEmpleadoCatalogo : RadForm
    {
        private bool todos = true;
        private BackgroundWorker consultar;
        private SqlSugarEmpleados data;

        public ViewEmpleadoCatalogo()
        {
            InitializeComponent();
        }

        private void CatalogoEmpleados_Load(object sender, EventArgs e)
        {
            this.GridData.TelerikGridCommon();
            ExpressionFormattingObject activos = new ExpressionFormattingObject("Activos", "Activo = false", true) { RowForeColor = Color.Red };
            this.GridData.Columns["Activo"].ConditionalFormattingObjectList.Add(activos);
            this.data = new SqlSugarEmpleados(ConfigService.Synapsis.RDS.Edita);
            this.consultar = new BackgroundWorker();
            this.consultar.DoWork += ConsultarDoWork;
            this.consultar.RunWorkerCompleted += ConsultarRunWorkerCompleted;
        }

        private void ToolBarRefresh_Click(object sender, EventArgs e)
        {
            this.todos = true;
            this.Espera.Visible = true;
            this.Espera.StartWaiting();
            this.GridData.DataSource = null;
            this.consultar.RunWorkerAsync();
        }

        private void ToolBarButtonRefreshTodos_Click(object sender, EventArgs e)
        {
            this.todos = false;
            this.Espera.Visible = true;
            this.Espera.StartWaiting();
            this.GridData.DataSource = null;
            this.consultar.RunWorkerAsync();
        }

        private void ToolBarNew_Click(object sender, EventArgs e)
        {
            Jaeger.Views.Nomina.ViewEmpleadoForm editar = new Jaeger.Views.Nomina.ViewEmpleadoForm(null) { StartPosition = FormStartPosition.CenterParent };
            editar.ShowDialog();
        }

        private void ToolBarButtonEditar_Click(object sender, EventArgs e)
        {
            if (!(this.GridData.CurrentRow == null))
            {
                using (var espera = new Waiting2Form(this.Consultar))
                {
                    espera.Text = "Consultando";
                    espera.ShowDialog(this);
                }

                ViewModelEmpleado empleado = (ViewModelEmpleado)this.ToolBarButtonEditar.Tag as ViewModelEmpleado;
                if (empleado != null)
                {
                    Jaeger.Views.Nomina.ViewEmpleadoForm editar = new Jaeger.Views.Nomina.ViewEmpleadoForm(empleado) { StartPosition = FormStartPosition.CenterParent };
                    editar.ShowDialog();
                }
            }
        }

        private void ToolBarDelete_Click(object sender, EventArgs e)
        {
            RadMessageBox.Show(this, "No tienes permiso para dar de baja al empleado!", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
        }

        private void ToolBarFilter_Click(object sender, EventArgs e)
        {
            this.GridData.ShowFilteringRow = this.ToolBarFilter.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false)
            {
                this.GridData.FilterDescriptors.Clear();
            }
        }

        private void ToolBarExport_Click(object sender, EventArgs e)
        {
            TelerikGridExport exportar = new TelerikGridExport(this.GridData);
            exportar.ShowDialog();
        }

        private void ToolBarImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog() { Filter = "*.txt|*.TXT", InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) };
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                HelperEmpleados layout = new HelperEmpleados();
                LayOutEmpleado[] resultado = layout.Importar(fileDialog.FileName);
                if (resultado == null)
                {
                    RadMessageBox.Show(this, "Ocurrio un error al procesar el layout de catálogo de empleados, por favor revise el formato y vuelva a intentar.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
                else
                {
                    List<ViewModelEmpleado> datos = new List<ViewModelEmpleado>();
                    foreach (LayOutEmpleado item in resultado)
                    {
                        ViewModelEmpleado nvoEmpleado = new ViewModelEmpleado();
                        nvoEmpleado.Num = Helpers.DbConvert.ConvertInt32(item.Num);
                        nvoEmpleado.Nombre = item.Nombres;
                        nvoEmpleado.NumSeguridadSocial = item.NumSeguridadSocial;
                        nvoEmpleado.PrimerApellido = item.PrimerApellido;
                        nvoEmpleado.SegundoApellido = item.SegundoApellido;
                        nvoEmpleado.RFC = item.RFC;
                        nvoEmpleado.CURP = item.CURP;
                        nvoEmpleado.Clave = item.Clave;
                        nvoEmpleado.Puesto = item.Puesto;
                        nvoEmpleado.Departamento = item.Departamento;
                        nvoEmpleado.Correo = item.Correo;
                        nvoEmpleado.FechaInicioRelLaboral = Helpers.DbConvert.ConvertDateTime(item.FechaInicioRelLaboral);
                        nvoEmpleado.SalarioDiario = Helpers.DbConvert.ConvertDecimal(item.SalarioDiario);
                        nvoEmpleado.SalarioDiarioIntegrado = Helpers.DbConvert.ConvertDecimal(item.SalarioDiarioIntegreado);
                        nvoEmpleado.SalarioBase = Helpers.DbConvert.ConvertDecimal(item.SalarioBase);
                        nvoEmpleado.CuentaBancaria = item.CuentaBancaria;
                        nvoEmpleado.JornadasTrabajo = Helpers.DbConvert.ConvertInt32(item.JornadasTrabajo);
                        nvoEmpleado.HorasJornadaTrabajo = Helpers.DbConvert.ConvertDecimal(item.HorasJornadaTrabajo);
                        if (item.Activo == "A")
                        {
                            nvoEmpleado.IsActive = true;
                        }
                        else if (item.Activo == "Z")
                        {
                            nvoEmpleado.IsActive = false;
                        }
                        datos.Add(nvoEmpleado);
                    }
                    if (datos.Count > 0)
                    {
                        data.Insert(datos.ToArray());
                    }
                }
                this.ToolBarButtonRefresh.PerformClick();
            }
        }

        private void ToolBarButtonCrear_Click(object sender, EventArgs e)
        {
            if (RadMessageBox.Show(this, "¿Esta seguro de crear la tabla? Esto acción eliminara toda la información almacenada,", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Exclamation) == System.Windows.Forms.DialogResult.Yes)
            {
                this.data.CreateTable(); 
            }
        }

        private void ToolBarPrintExpediente_Click(object sender, EventArgs e)
        {
            if (this.GridData.CurrentRow != null)
            {
                Jaeger.Nomina.Entities.ViewModelEmpleado empleado = this.GridData.CurrentRow.DataBoundItem as Jaeger.Nomina.Entities.ViewModelEmpleado;
                if (empleado != null)
                {
                    Jaeger.Views.ViewReportes reporte = new ViewReportes(empleado);
                    reporte.Show();
                }
                else
                {
                    RadMessageBox.Show(this, "No fue posible recuperar el expediente seleccionado");
                }
            }
        }

        private void ToolBarPrintListado_Click(object sender, EventArgs e)
        {
            //Jaeger.Views.Reportes reporte = new Reportes(this.data.GetList(true));
            //reporte.Show();
        }

        private void ToolBarClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ConsultarDoWork(object sender, DoWorkEventArgs e)
        {
            this.data.GetList(this.todos);
        }

        private void ConsultarRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.GridData.DataSource = this.data.GetList();
            this.Espera.Visible = false;
            this.Espera.StopWaiting();
            this.ToolBarButtonRefresh.Enabled = true;
        }

        private void GridData_RowsChanged(object sender, GridViewCollectionChangedEventArgs e)
        {
            this.ToolBarButtonEditar.Enabled = this.GridData.Rows.Count > 0;
            this.ToolBarExport.Enabled = this.GridData.Rows.Count > 0;
        }

        private void Consultar()
        {
            if (!(this.GridData.CurrentRow == null))
            {
                ViewModelEmpleado empleado = this.GridData.CurrentRow.DataBoundItem as ViewModelEmpleado;
                if (empleado != null)
                    this.ToolBarButtonEditar.Tag = this.data.GetById(empleado.Id);
            }
            else
            {
                RadMessageBox.Show(this, "No haz seleccionado a un empleado!", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }
    }
}
