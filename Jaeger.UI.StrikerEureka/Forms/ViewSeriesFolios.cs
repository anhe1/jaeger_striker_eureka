﻿using System;
using Jaeger.Edita.V2;
using Jaeger.Aplication;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Views
{
    public partial class ViewSeriesFolios : Telerik.WinControls.UI.RadForm
    {
        private SqlSugarSeries data;

        public ViewSeriesFolios()
        {
            InitializeComponent();
        }

        private void ViewSeriesFolios_Load(object sender, EventArgs e)
        {
            this.data = new SqlSugarSeries(ConfigService.Synapsis.RDS.Edita);
            this.GridData.DataSource = this.data.GetList();
            //Edita.V2.Empresa.Entities.FormatoSerie serie = new Edita.V2.Empresa.Entities.FormatoSerie();
            //serie.Objetos = new List<Edita.V2.Empresa.Entities.FormatoSerieObjeto>();
            //serie.Objetos.Add(new Edita.V2.Empresa.Entities.FormatoSerieObjeto { name = "name" });
            //MessageBox.Show(Newtonsoft.Json.JsonConvert.SerializeObject(serie));
        }

        private void GridData_CurrentRowChanged(object sender, Telerik.WinControls.UI.CurrentRowChangedEventArgs e)
        {
            if (this.GridData.CurrentRow!= null)
            {
                Edita.V2.Empresa.Entities.ViewModelSerieFolio serieFolio = this.GridData.CurrentRow.DataBoundItem as Edita.V2.Empresa.Entities.ViewModelSerieFolio;
                if (serieFolio != null)
                {
                    Edita.V2.Empresa.Entities.FormatoSerie objeto = Newtonsoft.Json.JsonConvert.DeserializeObject<Edita.V2.Empresa.Entities.FormatoSerie>(serieFolio.Formato);
                    if (objeto != null)
                        this.radPropertyGrid1.SelectedObject = objeto;
                }
            }
        }
    }
}
