﻿using Jaeger.Views;
using Telerik.WinControls.UI;

namespace Jaeger.Views
{
    partial class ComprobantesValidadorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCommandColumn gridViewCommandColumn1 = new Telerik.WinControls.UI.GridViewCommandColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCommandColumn gridViewCommandColumn2 = new Telerik.WinControls.UI.GridViewCommandColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition7 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn38 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn39 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn40 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn41 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn42 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn43 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn44 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn45 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn46 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn47 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn48 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn49 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn50 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn51 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn52 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn53 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn54 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn55 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn56 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn57 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn58 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn59 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn60 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn61 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn62 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn63 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn64 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn65 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn66 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn67 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn68 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn69 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn70 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn71 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn72 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn73 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn74 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn75 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn76 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn77 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn78 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComprobantesValidadorForm));
            this.CommandBarValidador = new Telerik.WinControls.UI.RadCommandBar();
            this.CommandBarRowElement = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarValidador = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabelCarpeta = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarHostItemCboCarpeta = new Telerik.WinControls.UI.CommandBarHostItem();
            this.ToolBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarButtonCarpeta = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarButtonCarpetaIncluirSubCarpetas = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonCarpetaInlcuirArchivosZIP = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarMenuAbrirArchivoZIP = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarMenuCrearArchivoZIP = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonCopiarA = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonArchivoLog = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonDetenerBusqueda = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarButtonAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonRemover = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarButtonLimpiar = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarToggleFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarButtonValidar = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarButtonValidarTodos = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonValidarSeleccionado = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonBackup = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonBakcupAuto = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonBakcupTodos = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonBakcupSeleccionado = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonPago = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonPDF = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonPDFAsignar = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonPDFBuscar = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonPDFValidacion = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonImprimir = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonImprimirPDF = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonImprimirValidacion = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonExportar = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ExportarNormal = new Telerik.WinControls.UI.RadMenuItem();
            this.ExportarTemplete = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonHerramientas = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarButtonConfiguracion = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonGuardarVista = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonMostrarAviso = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.BarraEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.BarraEstadoLabel = new Telerik.WinControls.UI.RadLabelElement();
            this.CboCarpetas = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.rotatingRingsWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.RotatingRingsWaitingBarIndicatorElement();
            this.GridConceptos = new Telerik.WinControls.UI.GridViewTemplate();
            this.GridCFDIRelacionado = new Telerik.WinControls.UI.GridViewTemplate();
            this.GridViewCFDIRelacionadoDocumento = new Telerik.WinControls.UI.GridViewTemplate();
            this.GridComplementoPagos = new Telerik.WinControls.UI.GridViewTemplate();
            this.GridViewComplementoPagoDoctosRelacionados = new Telerik.WinControls.UI.GridViewTemplate();
            this.Iconos = new System.Windows.Forms.ImageList(this.components);
            this.ContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.ContextMenuCopiar = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuValidar = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuEstadoSAT = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuArchivos = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextArchivoPDFBuscar = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextArchivoPDF = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextArchivoAcusePDF = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextArchivoAcuseXML = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuReciboPago = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonVersion69B = new Telerik.WinControls.UI.RadMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBarValidador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCarpetas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCarpetas.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCarpetas.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            this.GridData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridCFDIRelacionado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewCFDIRelacionadoDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComplementoPagos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewComplementoPagoDoctosRelacionados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandBarValidador
            // 
            this.CommandBarValidador.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBarValidador.Location = new System.Drawing.Point(0, 0);
            this.CommandBarValidador.Name = "CommandBarValidador";
            this.CommandBarValidador.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement});
            this.CommandBarValidador.Size = new System.Drawing.Size(1430, 38);
            this.CommandBarValidador.TabIndex = 0;
            // 
            // CommandBarRowElement
            // 
            this.CommandBarRowElement.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement.Name = "CommandBarRowElement";
            this.CommandBarRowElement.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarValidador});
            // 
            // ToolBarValidador
            // 
            this.ToolBarValidador.DisplayName = "commandBarStripElement1";
            this.ToolBarValidador.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelCarpeta,
            this.ToolBarHostItemCboCarpeta,
            this.ToolBarSeparator1,
            this.ToolBarButtonCarpeta,
            this.ToolBarButtonDetenerBusqueda,
            this.commandBarSeparator1,
            this.ToolBarButtonAgregar,
            this.ToolBarButtonRemover,
            this.ToolBarButtonActualizar,
            this.ToolBarToggleFiltro,
            this.ToolBarButtonValidar,
            this.ToolBarButtonBackup,
            this.ToolBarButtonPago,
            this.ToolBarButtonPDF,
            this.ToolBarButtonImprimir,
            this.ToolBarButtonExportar,
            this.ToolBarButtonHerramientas,
            this.ToolBarButtonCerrar});
            this.ToolBarValidador.Name = "ToolBarValidador";
            this.ToolBarValidador.StretchHorizontally = true;
            // 
            // ToolBarLabelCarpeta
            // 
            this.ToolBarLabelCarpeta.DisplayName = "Carpeta";
            this.ToolBarLabelCarpeta.Name = "ToolBarLabelCarpeta";
            this.ToolBarLabelCarpeta.Text = "Carpeta:";
            this.ToolBarLabelCarpeta.VisibleInOverflowMenu = false;
            // 
            // ToolBarHostItemCboCarpeta
            // 
            this.ToolBarHostItemCboCarpeta.DisplayName = "Carpeta";
            this.ToolBarHostItemCboCarpeta.MaxSize = new System.Drawing.Size(0, 0);
            this.ToolBarHostItemCboCarpeta.MinSize = new System.Drawing.Size(260, 0);
            this.ToolBarHostItemCboCarpeta.Name = "ToolBarHostItemCboCarpeta";
            this.ToolBarHostItemCboCarpeta.Text = "Carpeta";
            // 
            // ToolBarSeparator1
            // 
            this.ToolBarSeparator1.DisplayName = "commandBarSeparator1";
            this.ToolBarSeparator1.Name = "ToolBarSeparator1";
            this.ToolBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarButtonCarpeta
            // 
            this.ToolBarButtonCarpeta.DefaultItem = null;
            this.ToolBarButtonCarpeta.DisplayName = "Carpeta";
            this.ToolBarButtonCarpeta.Enabled = false;
            this.ToolBarButtonCarpeta.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_abrir_carpeta;
            this.ToolBarButtonCarpeta.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonCarpetaIncluirSubCarpetas,
            this.ToolBarButtonCarpetaInlcuirArchivosZIP,
            this.ToolBarMenuAbrirArchivoZIP,
            this.ToolBarMenuCrearArchivoZIP,
            this.ToolBarButtonCopiarA,
            this.ToolBarButtonArchivoLog});
            this.ToolBarButtonCarpeta.Name = "ToolBarButtonCarpeta";
            this.ToolBarButtonCarpeta.Text = "Carpeta";
            this.ToolBarButtonCarpeta.Click += new System.EventHandler(this.ToolBarButtonCarpeta_Click);
            // 
            // ToolBarButtonCarpetaIncluirSubCarpetas
            // 
            this.ToolBarButtonCarpetaIncluirSubCarpetas.CheckOnClick = true;
            this.ToolBarButtonCarpetaIncluirSubCarpetas.IsChecked = true;
            this.ToolBarButtonCarpetaIncluirSubCarpetas.Name = "ToolBarButtonCarpetaIncluirSubCarpetas";
            this.ToolBarButtonCarpetaIncluirSubCarpetas.Text = "Incluir SubCarpetas";
            this.ToolBarButtonCarpetaIncluirSubCarpetas.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // ToolBarButtonCarpetaInlcuirArchivosZIP
            // 
            this.ToolBarButtonCarpetaInlcuirArchivosZIP.CheckOnClick = true;
            this.ToolBarButtonCarpetaInlcuirArchivosZIP.Name = "ToolBarButtonCarpetaInlcuirArchivosZIP";
            this.ToolBarButtonCarpetaInlcuirArchivosZIP.Text = "Incluir Archivos ZIP";
            // 
            // ToolBarMenuAbrirArchivoZIP
            // 
            this.ToolBarMenuAbrirArchivoZIP.Name = "ToolBarMenuAbrirArchivoZIP";
            this.ToolBarMenuAbrirArchivoZIP.Text = "Abrir archivo ZIP";
            this.ToolBarMenuAbrirArchivoZIP.Click += new System.EventHandler(this.ToolBarMenuAbrirArchivoZIP_Click);
            // 
            // ToolBarMenuCrearArchivoZIP
            // 
            this.ToolBarMenuCrearArchivoZIP.Name = "ToolBarMenuCrearArchivoZIP";
            this.ToolBarMenuCrearArchivoZIP.Text = "Crear archivo ZIP";
            this.ToolBarMenuCrearArchivoZIP.Click += new System.EventHandler(this.ToolBarMenuCrearArchivoZIP_Click);
            // 
            // ToolBarButtonCopiarA
            // 
            this.ToolBarButtonCopiarA.Name = "ToolBarButtonCopiarA";
            this.ToolBarButtonCopiarA.Text = "Copiar todo a ..";
            this.ToolBarButtonCopiarA.Click += new System.EventHandler(this.ToolBarButtonCopiarA_Click);
            // 
            // ToolBarButtonArchivoLog
            // 
            this.ToolBarButtonArchivoLog.Name = "ToolBarButtonArchivoLog";
            this.ToolBarButtonArchivoLog.Text = "Archivo Log";
            this.ToolBarButtonArchivoLog.Click += new System.EventHandler(this.ToolBarButtonArchivoLog_Click);
            // 
            // ToolBarButtonDetenerBusqueda
            // 
            this.ToolBarButtonDetenerBusqueda.DisplayName = "Detener";
            this.ToolBarButtonDetenerBusqueda.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_cancelar;
            this.ToolBarButtonDetenerBusqueda.Name = "ToolBarButtonDetenerBusqueda";
            this.ToolBarButtonDetenerBusqueda.Text = "Detener Busqueda";
            this.ToolBarButtonDetenerBusqueda.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ToolBarButtonDetenerBusqueda.Click += new System.EventHandler(this.ToolBarButtonDetenerBusqueda_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarButtonAgregar
            // 
            this.ToolBarButtonAgregar.DisplayName = "Agregar";
            this.ToolBarButtonAgregar.DrawText = true;
            this.ToolBarButtonAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_agregar_archivo;
            this.ToolBarButtonAgregar.Name = "ToolBarButtonAgregar";
            this.ToolBarButtonAgregar.Text = "Agregar";
            this.ToolBarButtonAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonAgregar.Click += new System.EventHandler(this.ToolBarButtonAgregar_Click);
            // 
            // ToolBarButtonRemover
            // 
            this.ToolBarButtonRemover.DefaultItem = null;
            this.ToolBarButtonRemover.DisplayName = "Remover";
            this.ToolBarButtonRemover.DrawText = true;
            this.ToolBarButtonRemover.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_delete_file;
            this.ToolBarButtonRemover.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonLimpiar});
            this.ToolBarButtonRemover.Name = "ToolBarButtonRemover";
            this.ToolBarButtonRemover.Text = "Remover";
            this.ToolBarButtonRemover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonLimpiar
            // 
            this.ToolBarButtonLimpiar.Name = "ToolBarButtonLimpiar";
            this.ToolBarButtonLimpiar.Text = "Todo";
            this.ToolBarButtonLimpiar.Click += new System.EventHandler(this.ToolBarButtonLimpiar_Click);
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.DisplayName = "Actualizar";
            this.ToolBarButtonActualizar.DrawText = true;
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_actualizar;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.ToolBarButtonActualizar_Click);
            // 
            // ToolBarToggleFiltro
            // 
            this.ToolBarToggleFiltro.DisplayName = "Filtro";
            this.ToolBarToggleFiltro.DrawText = true;
            this.ToolBarToggleFiltro.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_filtrar;
            this.ToolBarToggleFiltro.Name = "ToolBarToggleFiltro";
            this.ToolBarToggleFiltro.Text = "Filtro";
            this.ToolBarToggleFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarToggleFiltro.Click += new System.EventHandler(this.ToolBarToggleFiltro_Click);
            // 
            // ToolBarButtonValidar
            // 
            this.ToolBarButtonValidar.DefaultItem = null;
            this.ToolBarButtonValidar.DisplayName = "Validar";
            this.ToolBarButtonValidar.DrawText = true;
            this.ToolBarButtonValidar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_seguridad_comprobado;
            this.ToolBarButtonValidar.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonValidarTodos,
            this.ToolBarButtonValidarSeleccionado});
            this.ToolBarButtonValidar.Name = "ToolBarButtonValidar";
            this.ToolBarButtonValidar.Text = "Validar";
            this.ToolBarButtonValidar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonValidar.Click += new System.EventHandler(this.ToolBarButtonValidarTodos_Click);
            // 
            // ToolBarButtonValidarTodos
            // 
            this.ToolBarButtonValidarTodos.Name = "ToolBarButtonValidarTodos";
            this.ToolBarButtonValidarTodos.Text = "Todos";
            this.ToolBarButtonValidarTodos.Click += new System.EventHandler(this.ToolBarButtonValidarTodos_Click);
            // 
            // ToolBarButtonValidarSeleccionado
            // 
            this.ToolBarButtonValidarSeleccionado.Name = "ToolBarButtonValidarSeleccionado";
            this.ToolBarButtonValidarSeleccionado.Text = "Seleccionado";
            this.ToolBarButtonValidarSeleccionado.Click += new System.EventHandler(this.ToolBarButtonValidarSeleccionado_Click);
            // 
            // ToolBarButtonBackup
            // 
            this.ToolBarButtonBackup.DisplayName = "Backup";
            this.ToolBarButtonBackup.DrawText = true;
            this.ToolBarButtonBackup.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_protección_de_datos;
            this.ToolBarButtonBackup.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonBakcupAuto,
            this.ToolBarButtonBakcupTodos,
            this.ToolBarButtonBakcupSeleccionado});
            this.ToolBarButtonBackup.Name = "ToolBarButtonBackup";
            this.ToolBarButtonBackup.Text = "Backup";
            this.ToolBarButtonBackup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonBakcupAuto
            // 
            this.ToolBarButtonBakcupAuto.CheckOnClick = true;
            this.ToolBarButtonBakcupAuto.IsChecked = true;
            this.ToolBarButtonBakcupAuto.Name = "ToolBarButtonBakcupAuto";
            this.ToolBarButtonBakcupAuto.Text = "Automático";
            this.ToolBarButtonBakcupAuto.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.ToolBarButtonBakcupAuto.Click += new System.EventHandler(this.ToolBarButtonBakcupAuto_Click);
            // 
            // ToolBarButtonBakcupTodos
            // 
            this.ToolBarButtonBakcupTodos.Name = "ToolBarButtonBakcupTodos";
            this.ToolBarButtonBakcupTodos.Text = "Todos";
            this.ToolBarButtonBakcupTodos.Click += new System.EventHandler(this.ToolBarButtonBakcupTodos_Click);
            // 
            // ToolBarButtonBakcupSeleccionado
            // 
            this.ToolBarButtonBakcupSeleccionado.Name = "ToolBarButtonBakcupSeleccionado";
            this.ToolBarButtonBakcupSeleccionado.Text = "Seleccionado";
            this.ToolBarButtonBakcupSeleccionado.Click += new System.EventHandler(this.ToolBarButtonBakcupSeleccionado_Click);
            // 
            // ToolBarButtonPago
            // 
            this.ToolBarButtonPago.DisplayName = "Pago";
            this.ToolBarButtonPago.DrawText = true;
            this.ToolBarButtonPago.Enabled = false;
            this.ToolBarButtonPago.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_chequera;
            this.ToolBarButtonPago.Name = "ToolBarButtonPago";
            this.ToolBarButtonPago.Text = "Pago";
            this.ToolBarButtonPago.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonPago.Click += new System.EventHandler(this.ToolBarButtonPago_Click);
            // 
            // ToolBarButtonPDF
            // 
            this.ToolBarButtonPDF.DisplayName = "Asignar PDF";
            this.ToolBarButtonPDF.DrawText = true;
            this.ToolBarButtonPDF.Enabled = false;
            this.ToolBarButtonPDF.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_pdf;
            this.ToolBarButtonPDF.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonPDFAsignar,
            this.ToolBarButtonPDFBuscar,
            this.ToolBarButtonPDFValidacion});
            this.ToolBarButtonPDF.Name = "ToolBarButtonPDF";
            this.ToolBarButtonPDF.Text = "PDF";
            this.ToolBarButtonPDF.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonPDFAsignar
            // 
            this.ToolBarButtonPDFAsignar.Name = "ToolBarButtonPDFAsignar";
            this.ToolBarButtonPDFAsignar.Text = "Asignar";
            this.ToolBarButtonPDFAsignar.Click += new System.EventHandler(this.ToolBarButtonPDFAsignar_Click);
            // 
            // ToolBarButtonPDFBuscar
            // 
            this.ToolBarButtonPDFBuscar.Name = "ToolBarButtonPDFBuscar";
            this.ToolBarButtonPDFBuscar.Text = "Buscar";
            this.ToolBarButtonPDFBuscar.Click += new System.EventHandler(this.ToolBarButtonPDFBuscar_Click);
            // 
            // ToolBarButtonPDFValidacion
            // 
            this.ToolBarButtonPDFValidacion.Name = "ToolBarButtonPDFValidacion";
            this.ToolBarButtonPDFValidacion.Text = "Descargar PDF\'s de validación";
            this.ToolBarButtonPDFValidacion.ToolTipText = "Descargar todos los reportes de validación en formato PDF";
            this.ToolBarButtonPDFValidacion.Click += new System.EventHandler(this.ToolBarButtonPDFValidacion_Click);
            // 
            // ToolBarButtonImprimir
            // 
            this.ToolBarButtonImprimir.DisplayName = "Imprimir";
            this.ToolBarButtonImprimir.DrawText = true;
            this.ToolBarButtonImprimir.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_imprimir;
            this.ToolBarButtonImprimir.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonImprimirPDF,
            this.ToolBarButtonImprimirValidacion});
            this.ToolBarButtonImprimir.Name = "ToolBarButtonImprimir";
            this.ToolBarButtonImprimir.Text = "Imprimir";
            this.ToolBarButtonImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonImprimirPDF
            // 
            this.ToolBarButtonImprimirPDF.Name = "ToolBarButtonImprimirPDF";
            this.ToolBarButtonImprimirPDF.Text = "PDF";
            this.ToolBarButtonImprimirPDF.Click += new System.EventHandler(this.ToolBarButtonImprimirPDF_Click);
            // 
            // ToolBarButtonImprimirValidacion
            // 
            this.ToolBarButtonImprimirValidacion.Name = "ToolBarButtonImprimirValidacion";
            this.ToolBarButtonImprimirValidacion.Text = "Validación";
            this.ToolBarButtonImprimirValidacion.Click += new System.EventHandler(this.ToolBarImprimirValidacion_Click);
            // 
            // ToolBarButtonExportar
            // 
            this.ToolBarButtonExportar.DisplayName = "Exportar";
            this.ToolBarButtonExportar.DrawText = true;
            this.ToolBarButtonExportar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_excel;
            this.ToolBarButtonExportar.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ExportarNormal,
            this.ExportarTemplete});
            this.ToolBarButtonExportar.Name = "ToolBarButtonExportar";
            this.ToolBarButtonExportar.Text = "Exportar";
            this.ToolBarButtonExportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonExportar.ToolTipText = "Exportar resultados en formato EXCEL";
            // 
            // ExportarNormal
            // 
            this.ExportarNormal.Name = "ExportarNormal";
            this.ExportarNormal.Text = "Normal";
            this.ExportarNormal.Click += new System.EventHandler(this.ToolBarButtonExportar_Click);
            // 
            // ExportarTemplete
            // 
            this.ExportarTemplete.Name = "ExportarTemplete";
            this.ExportarTemplete.Text = "Exportar por templete";
            this.ExportarTemplete.Click += new System.EventHandler(this.ExportarTemplete_Click);
            // 
            // ToolBarButtonHerramientas
            // 
            this.ToolBarButtonHerramientas.DefaultItem = null;
            this.ToolBarButtonHerramientas.DisplayName = "Configuración";
            this.ToolBarButtonHerramientas.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_configuracion;
            this.ToolBarButtonHerramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonConfiguracion,
            this.ToolBarButtonGuardarVista,
            this.ToolBarButtonMostrarAviso,
            this.ToolBarButtonVersion69B});
            this.ToolBarButtonHerramientas.Name = "ToolBarButtonHerramientas";
            this.ToolBarButtonHerramientas.Text = "Configuración";
            this.ToolBarButtonHerramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonConfiguracion
            // 
            this.ToolBarButtonConfiguracion.Name = "ToolBarButtonConfiguracion";
            this.ToolBarButtonConfiguracion.Text = "Configuración";
            this.ToolBarButtonConfiguracion.Click += new System.EventHandler(this.ToolBarButtonConfiguracion_Click);
            // 
            // ToolBarButtonGuardarVista
            // 
            this.ToolBarButtonGuardarVista.Name = "ToolBarButtonGuardarVista";
            this.ToolBarButtonGuardarVista.Text = "Guardar vista";
            this.ToolBarButtonGuardarVista.Click += new System.EventHandler(this.ToolBarButtonGuardarVista_Click);
            // 
            // ToolBarButtonMostrarAviso
            // 
            this.ToolBarButtonMostrarAviso.Name = "ToolBarButtonMostrarAviso";
            this.ToolBarButtonMostrarAviso.Text = "Mostrar avisos";
            this.ToolBarButtonMostrarAviso.Click += new System.EventHandler(this.ToolBarButtonMostrarAviso_Click);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "Cerrar";
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_cerrar;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // BarraEstado
            // 
            this.BarraEstado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.BarraEstadoLabel});
            this.BarraEstado.Location = new System.Drawing.Point(0, 619);
            this.BarraEstado.Name = "BarraEstado";
            this.BarraEstado.Size = new System.Drawing.Size(1430, 26);
            this.BarraEstado.SizingGrip = false;
            this.BarraEstado.TabIndex = 1;
            // 
            // BarraEstadoLabel
            // 
            this.BarraEstadoLabel.Name = "BarraEstadoLabel";
            this.BarraEstado.SetSpring(this.BarraEstadoLabel, false);
            this.BarraEstadoLabel.Text = "Listo!";
            this.BarraEstadoLabel.TextWrap = true;
            // 
            // CboCarpetas
            // 
            // 
            // CboCarpetas.NestedRadGridView
            // 
            this.CboCarpetas.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboCarpetas.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCarpetas.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboCarpetas.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboCarpetas.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboCarpetas.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboCarpetas.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Carpeta";
            gridViewTextBoxColumn1.HeaderText = "Carpeta";
            gridViewTextBoxColumn1.Name = "Carpeta";
            gridViewTextBoxColumn1.Width = 200;
            gridViewCommandColumn1.FieldName = "Accion";
            gridViewCommandColumn1.HeaderText = "Acción";
            gridViewCommandColumn1.Name = "Remover";
            gridViewCommandColumn1.Width = 85;
            this.CboCarpetas.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewCommandColumn1});
            this.CboCarpetas.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboCarpetas.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboCarpetas.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.CboCarpetas.EditorControl.Name = "NestedRadGridView";
            this.CboCarpetas.EditorControl.ReadOnly = true;
            this.CboCarpetas.EditorControl.ShowGroupPanel = false;
            this.CboCarpetas.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboCarpetas.EditorControl.TabIndex = 0;
            this.CboCarpetas.EditorControl.CommandCellClick += new Telerik.WinControls.UI.CommandCellClickEventHandler(this.CboCarpetas_CommandCellClick);
            this.CboCarpetas.Location = new System.Drawing.Point(72, 135);
            this.CboCarpetas.Name = "CboCarpetas";
            this.CboCarpetas.NullText = "Selecciona";
            this.CboCarpetas.Size = new System.Drawing.Size(239, 20);
            this.CboCarpetas.TabIndex = 2;
            this.CboCarpetas.TabStop = false;
            this.CboCarpetas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboCarpetas_KeyDown);
            // 
            // GridData
            // 
            this.GridData.Controls.Add(this.CboCarpetas);
            this.GridData.Controls.Add(this.Espera);
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 38);
            // 
            // 
            // 
            gridViewTextBoxColumn2.DataType = typeof(int);
            gridViewTextBoxColumn2.FieldName = "Id";
            gridViewTextBoxColumn2.HeaderText = "Id";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "Id";
            gridViewTextBoxColumn2.VisibleInColumnChooser = false;
            gridViewTextBoxColumn3.FieldName = "TipoComprobante";
            gridViewTextBoxColumn3.HeaderText = "Tipo Comprobante";
            gridViewTextBoxColumn3.Name = "TipoComprobante";
            gridViewTextBoxColumn4.FieldName = "Version";
            gridViewTextBoxColumn4.HeaderText = "Versión";
            gridViewTextBoxColumn4.IsVisible = false;
            gridViewTextBoxColumn4.Name = "Version";
            gridViewTextBoxColumn5.FieldName = "MetaData";
            gridViewTextBoxColumn5.HeaderText = "Doc.";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "MetaData";
            gridViewTextBoxColumn6.FieldName = "Folio";
            gridViewTextBoxColumn6.HeaderText = "Folio";
            gridViewTextBoxColumn6.Name = "Folio";
            gridViewTextBoxColumn7.FieldName = "Serie";
            gridViewTextBoxColumn7.HeaderText = "Serie";
            gridViewTextBoxColumn7.Name = "Serie";
            gridViewTextBoxColumn8.FieldName = "EmisorRFC";
            gridViewTextBoxColumn8.HeaderText = "Emisor (RFC)";
            gridViewTextBoxColumn8.Name = "EmisorRFC";
            gridViewTextBoxColumn8.Width = 85;
            gridViewTextBoxColumn9.FieldName = "Emisor";
            gridViewTextBoxColumn9.HeaderText = "Emisor";
            gridViewTextBoxColumn9.Name = "Emisor";
            gridViewTextBoxColumn9.Width = 220;
            gridViewTextBoxColumn10.FieldName = "EmisorRegimen";
            gridViewTextBoxColumn10.HeaderText = "Emisor Reg.";
            gridViewTextBoxColumn10.MaxLength = 3;
            gridViewTextBoxColumn10.Name = "EmisorRegimen";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn11.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn11.HeaderText = "Receptor (RFC)";
            gridViewTextBoxColumn11.Name = "ReceptorRFC";
            gridViewTextBoxColumn11.Width = 85;
            gridViewTextBoxColumn12.FieldName = "Receptor";
            gridViewTextBoxColumn12.HeaderText = "Receptor";
            gridViewTextBoxColumn12.Name = "Receptor";
            gridViewTextBoxColumn12.Width = 220;
            gridViewTextBoxColumn13.FieldName = "ReceptorRegimen";
            gridViewTextBoxColumn13.HeaderText = "Receptor Reg.";
            gridViewTextBoxColumn13.Name = "ReceptorRegimen";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn14.FieldName = "DomicilioFiscal";
            gridViewTextBoxColumn14.HeaderText = "Receptor Dom.";
            gridViewTextBoxColumn14.Name = "DomicilioFiscal";
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn1.FieldName = "Fecha";
            gridViewDateTimeColumn1.FormatString = "{0:d}";
            gridViewDateTimeColumn1.HeaderText = "Fecha";
            gridViewDateTimeColumn1.Name = "Fecha";
            gridViewDateTimeColumn1.Width = 80;
            gridViewDateTimeColumn2.FieldName = "FechaCertificacion";
            gridViewDateTimeColumn2.FormatString = "{0:d}";
            gridViewDateTimeColumn2.HeaderText = "Fec. Certifica";
            gridViewDateTimeColumn2.Name = "FechaCertificacion";
            gridViewDateTimeColumn2.Width = 85;
            gridViewDateTimeColumn2.WrapText = true;
            gridViewTextBoxColumn15.FieldName = "IdDocumento";
            gridViewTextBoxColumn15.HeaderText = "UUID";
            gridViewTextBoxColumn15.Name = "IdDocumento";
            gridViewTextBoxColumn15.Width = 240;
            gridViewTextBoxColumn16.FieldName = "NoCertificado";
            gridViewTextBoxColumn16.HeaderText = "No. Certificado";
            gridViewTextBoxColumn16.Name = "NoCertificado";
            gridViewTextBoxColumn16.Width = 95;
            gridViewTextBoxColumn17.FieldName = "RFCProvCertif";
            gridViewTextBoxColumn17.HeaderText = "Prov. Certif. (RFC)";
            gridViewTextBoxColumn17.Name = "RFCProvCertif";
            gridViewTextBoxColumn17.Width = 90;
            gridViewTextBoxColumn18.FieldName = "EstadoSAT";
            gridViewTextBoxColumn18.HeaderText = "Estado SAT";
            gridViewTextBoxColumn18.Name = "EstadoSAT";
            gridViewTextBoxColumn18.Width = 65;
            gridViewTextBoxColumn19.FieldName = "LugarExpedicion";
            gridViewTextBoxColumn19.HeaderText = "Expedición";
            gridViewTextBoxColumn19.Name = "LugarExpedicion";
            gridViewTextBoxColumn20.FieldName = "MetodoPago";
            gridViewTextBoxColumn20.HeaderText = "Método Pago";
            gridViewTextBoxColumn20.Name = "MetodoPago";
            gridViewTextBoxColumn21.FieldName = "FormaPago";
            gridViewTextBoxColumn21.HeaderText = "Forma de Pago";
            gridViewTextBoxColumn21.Name = "FormaPago";
            gridViewTextBoxColumn21.WrapText = true;
            gridViewTextBoxColumn22.FieldName = "UsoCFDI";
            gridViewTextBoxColumn22.HeaderText = "Uso CFDI";
            gridViewTextBoxColumn22.Name = "UsoCFDI";
            gridViewTextBoxColumn23.FieldName = "MetodoVsForma";
            gridViewTextBoxColumn23.HeaderText = "Metodo\r\nVs Forma";
            gridViewTextBoxColumn23.Name = "MetodoVsForma";
            gridViewTextBoxColumn24.DataType = typeof(decimal);
            gridViewTextBoxColumn24.FieldName = "SubTotal";
            gridViewTextBoxColumn24.FormatString = "{0:n}";
            gridViewTextBoxColumn24.HeaderText = "SubTotal";
            gridViewTextBoxColumn24.Name = "SubTotal";
            gridViewTextBoxColumn24.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn24.Width = 85;
            gridViewTextBoxColumn25.DataType = typeof(decimal);
            gridViewTextBoxColumn25.FieldName = "Descuento";
            gridViewTextBoxColumn25.FormatString = "{0:n}";
            gridViewTextBoxColumn25.HeaderText = "Descuento";
            gridViewTextBoxColumn25.Name = "Descuento";
            gridViewTextBoxColumn25.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn25.Width = 85;
            gridViewTextBoxColumn26.DataType = typeof(decimal);
            gridViewTextBoxColumn26.FieldName = "TotalRetencionISR";
            gridViewTextBoxColumn26.FormatString = "{0:n}";
            gridViewTextBoxColumn26.HeaderText = "ISR Ret.";
            gridViewTextBoxColumn26.Name = "TotalRetencionISR";
            gridViewTextBoxColumn26.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn26.Width = 75;
            gridViewTextBoxColumn27.DataType = typeof(decimal);
            gridViewTextBoxColumn27.FieldName = "TotalRetencionIVA";
            gridViewTextBoxColumn27.FormatString = "{0:n}";
            gridViewTextBoxColumn27.HeaderText = "IVA Ret.";
            gridViewTextBoxColumn27.Name = "TotalRetencionIVA";
            gridViewTextBoxColumn27.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn27.Width = 75;
            gridViewTextBoxColumn28.DataType = typeof(decimal);
            gridViewTextBoxColumn28.FieldName = "TotalRetencionIEPS";
            gridViewTextBoxColumn28.FormatString = "{0:n}";
            gridViewTextBoxColumn28.HeaderText = "IEPS Ret.";
            gridViewTextBoxColumn28.Name = "TotalRetencionIEPS";
            gridViewTextBoxColumn28.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn28.Width = 75;
            gridViewTextBoxColumn29.DataType = typeof(decimal);
            gridViewTextBoxColumn29.FieldName = "TotalTrasladoIVA";
            gridViewTextBoxColumn29.FormatString = "{0:n}";
            gridViewTextBoxColumn29.HeaderText = "IVA Tras.";
            gridViewTextBoxColumn29.Name = "TotalTrasladoIVA";
            gridViewTextBoxColumn29.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn29.Width = 75;
            gridViewTextBoxColumn30.DataType = typeof(decimal);
            gridViewTextBoxColumn30.FieldName = "TotalTrasladoIEPS";
            gridViewTextBoxColumn30.FormatString = "{0:n}";
            gridViewTextBoxColumn30.HeaderText = "IEPS Tras.";
            gridViewTextBoxColumn30.Name = "TotalTrasladoIEPS";
            gridViewTextBoxColumn30.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn30.Width = 75;
            gridViewTextBoxColumn31.DataType = typeof(decimal);
            gridViewTextBoxColumn31.FieldName = "Total";
            gridViewTextBoxColumn31.FormatString = "{0:n}";
            gridViewTextBoxColumn31.HeaderText = "Total";
            gridViewTextBoxColumn31.Name = "Total";
            gridViewTextBoxColumn31.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn31.Width = 85;
            gridViewTextBoxColumn32.FieldName = "XML";
            gridViewTextBoxColumn32.HeaderText = "XML";
            gridViewTextBoxColumn32.Name = "XML";
            gridViewTextBoxColumn32.Width = 30;
            gridViewTextBoxColumn33.FieldName = "PDF";
            gridViewTextBoxColumn33.HeaderText = "PDF";
            gridViewTextBoxColumn33.Name = "PDF";
            gridViewTextBoxColumn33.Width = 30;
            gridViewTextBoxColumn34.FieldName = "ClaveUnidad";
            gridViewTextBoxColumn34.HeaderText = "Clave Unidad";
            gridViewTextBoxColumn34.Name = "ClaveUnidad";
            gridViewTextBoxColumn34.Width = 150;
            gridViewTextBoxColumn35.FieldName = "ClaveConceptos";
            gridViewTextBoxColumn35.HeaderText = "Clave Conceptos";
            gridViewTextBoxColumn35.Name = "ClaveConceptos";
            gridViewTextBoxColumn35.Width = 250;
            gridViewTextBoxColumn36.FieldName = "Situacion";
            gridViewTextBoxColumn36.HeaderText = "Situación";
            gridViewTextBoxColumn36.Name = "Situacion";
            gridViewTextBoxColumn36.Width = 85;
            gridViewCommandColumn2.FieldName = "Resultado";
            gridViewCommandColumn2.HeaderText = "Resultado";
            gridViewCommandColumn2.Name = "Resultado";
            gridViewCommandColumn2.Width = 85;
            gridViewTextBoxColumn37.FieldName = "Registrado";
            gridViewTextBoxColumn37.HeaderText = "Registrado";
            gridViewTextBoxColumn37.IsVisible = false;
            gridViewTextBoxColumn37.Name = "Registrado";
            gridViewTextBoxColumn37.VisibleInColumnChooser = false;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewDateTimeColumn1,
            gridViewDateTimeColumn2,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32,
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34,
            gridViewTextBoxColumn35,
            gridViewTextBoxColumn36,
            gridViewCommandColumn2,
            gridViewTextBoxColumn37});
            this.GridData.MasterTemplate.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.GridConceptos,
            this.GridCFDIRelacionado,
            this.GridComplementoPagos});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition7;
            this.GridData.Name = "GridData";
            this.GridData.ShowGroupPanel = false;
            this.GridData.Size = new System.Drawing.Size(1430, 581);
            this.GridData.TabIndex = 4;
            this.GridData.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.GridData_CellFormatting);
            this.GridData.RowSourceNeeded += new Telerik.WinControls.UI.GridViewRowSourceNeededEventHandler(this.GridData_RowSourceNeeded);
            this.GridData.RowsChanged += new Telerik.WinControls.UI.GridViewCollectionChangedEventHandler(this.GridData_RowsChanged);
            this.GridData.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridData_CellDoubleClick);
            this.GridData.CommandCellClick += new Telerik.WinControls.UI.CommandCellClickEventHandler(this.GridData_CommandCellClick);
            this.GridData.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.GridData_ContextMenuOpening);
            // 
            // Espera
            // 
            this.Espera.AssociatedControl = this.GridData;
            this.Espera.Location = new System.Drawing.Point(562, 235);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(70, 70);
            this.Espera.TabIndex = 1;
            this.Espera.Text = "Espera";
            this.Espera.WaitingIndicators.Add(this.rotatingRingsWaitingBarIndicatorElement1);
            this.Espera.WaitingStep = 7;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.RotatingRings;
            ((Telerik.WinControls.UI.RadWaitingBarElement)(this.Espera.GetChildAt(0))).WaitingStep = 7;
            ((Telerik.WinControls.UI.WaitingBarContentElement)(this.Espera.GetChildAt(0).GetChildAt(0))).WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.RotatingRings;
            ((Telerik.WinControls.UI.WaitingBarSeparatorElement)(this.Espera.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Dash = false;
            // 
            // rotatingRingsWaitingBarIndicatorElement1
            // 
            this.rotatingRingsWaitingBarIndicatorElement1.Name = "rotatingRingsWaitingBarIndicatorElement1";
            // 
            // GridConceptos
            // 
            this.GridConceptos.Caption = "Conceptos";
            gridViewTextBoxColumn38.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn38.HeaderText = "ClaveProdServ";
            gridViewTextBoxColumn38.Name = "ClaveProdServ";
            gridViewTextBoxColumn38.Width = 65;
            gridViewTextBoxColumn39.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn39.HeaderText = "NoIdentificacion";
            gridViewTextBoxColumn39.Name = "NoIdentificacion";
            gridViewTextBoxColumn39.Width = 65;
            gridViewTextBoxColumn40.DataType = typeof(decimal);
            gridViewTextBoxColumn40.FieldName = "Cantidad";
            gridViewTextBoxColumn40.FormatString = "{0:n}";
            gridViewTextBoxColumn40.HeaderText = "Cantidad";
            gridViewTextBoxColumn40.Name = "Cantidad";
            gridViewTextBoxColumn40.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn40.Width = 65;
            gridViewTextBoxColumn41.FieldName = "ClaveUnidad";
            gridViewTextBoxColumn41.HeaderText = "Clave Unidad";
            gridViewTextBoxColumn41.Name = "ClaveUnidad";
            gridViewTextBoxColumn41.Width = 65;
            gridViewTextBoxColumn42.FieldName = "Unidad";
            gridViewTextBoxColumn42.HeaderText = "Unidad";
            gridViewTextBoxColumn42.Name = "Unidad";
            gridViewTextBoxColumn42.Width = 65;
            gridViewTextBoxColumn43.FieldName = "Descripcion";
            gridViewTextBoxColumn43.HeaderText = "Descripción";
            gridViewTextBoxColumn43.Name = "Descripcion";
            gridViewTextBoxColumn43.Width = 280;
            gridViewTextBoxColumn44.DataType = typeof(decimal);
            gridViewTextBoxColumn44.FieldName = "ValorUnitario";
            gridViewTextBoxColumn44.FormatString = "{0:n}";
            gridViewTextBoxColumn44.HeaderText = "Valor Unitario";
            gridViewTextBoxColumn44.Name = "ValorUnitario";
            gridViewTextBoxColumn44.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn44.Width = 85;
            gridViewTextBoxColumn45.DataType = typeof(decimal);
            gridViewTextBoxColumn45.FieldName = "Importe";
            gridViewTextBoxColumn45.FormatString = "{0:n}";
            gridViewTextBoxColumn45.HeaderText = "Importe";
            gridViewTextBoxColumn45.Name = "Importe";
            gridViewTextBoxColumn45.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn45.Width = 85;
            gridViewTextBoxColumn46.DataType = typeof(decimal);
            gridViewTextBoxColumn46.FieldName = "Descuento";
            gridViewTextBoxColumn46.FormatString = "{0:n}";
            gridViewTextBoxColumn46.HeaderText = "Descuento";
            gridViewTextBoxColumn46.Name = "Descuento";
            gridViewTextBoxColumn46.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn46.Width = 85;
            this.GridConceptos.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn38,
            gridViewTextBoxColumn39,
            gridViewTextBoxColumn40,
            gridViewTextBoxColumn41,
            gridViewTextBoxColumn42,
            gridViewTextBoxColumn43,
            gridViewTextBoxColumn44,
            gridViewTextBoxColumn45,
            gridViewTextBoxColumn46});
            this.GridConceptos.ViewDefinition = tableViewDefinition2;
            // 
            // GridCFDIRelacionado
            // 
            this.GridCFDIRelacionado.Caption = "CFDI Relacionado";
            gridViewTextBoxColumn47.FieldName = "Clave";
            gridViewTextBoxColumn47.HeaderText = "Clave";
            gridViewTextBoxColumn47.Name = "Clave";
            gridViewTextBoxColumn47.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn48.FieldName = "Name";
            gridViewTextBoxColumn48.HeaderText = "Descripción";
            gridViewTextBoxColumn48.Name = "Name";
            gridViewTextBoxColumn48.Width = 300;
            this.GridCFDIRelacionado.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn47,
            gridViewTextBoxColumn48});
            this.GridCFDIRelacionado.NewRowText = "";
            this.GridCFDIRelacionado.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.GridViewCFDIRelacionadoDocumento});
            this.GridCFDIRelacionado.ViewDefinition = tableViewDefinition4;
            // 
            // GridViewCFDIRelacionadoDocumento
            // 
            gridViewTextBoxColumn49.FieldName = "IdDocumento";
            gridViewTextBoxColumn49.HeaderText = "UUID";
            gridViewTextBoxColumn49.Name = "IdDocumento";
            gridViewTextBoxColumn49.Width = 230;
            gridViewTextBoxColumn50.FieldName = "Folio";
            gridViewTextBoxColumn50.HeaderText = "Folio";
            gridViewTextBoxColumn50.Name = "Folio";
            gridViewTextBoxColumn51.FieldName = "Serie";
            gridViewTextBoxColumn51.HeaderText = "Serie";
            gridViewTextBoxColumn51.Name = "Serie";
            gridViewTextBoxColumn52.FieldName = "EmisorRFC";
            gridViewTextBoxColumn52.HeaderText = "Emisor (RFC)";
            gridViewTextBoxColumn52.Name = "EmisorRFC";
            gridViewTextBoxColumn52.Width = 85;
            gridViewTextBoxColumn53.FieldName = "Emisor";
            gridViewTextBoxColumn53.HeaderText = "Emisor";
            gridViewTextBoxColumn53.Name = "Emisor";
            gridViewTextBoxColumn53.Width = 220;
            gridViewTextBoxColumn54.DataType = typeof(decimal);
            gridViewTextBoxColumn54.FieldName = "Total";
            gridViewTextBoxColumn54.HeaderText = "Total";
            gridViewTextBoxColumn54.Name = "Total";
            gridViewTextBoxColumn54.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn54.Width = 85;
            this.GridViewCFDIRelacionadoDocumento.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn49,
            gridViewTextBoxColumn50,
            gridViewTextBoxColumn51,
            gridViewTextBoxColumn52,
            gridViewTextBoxColumn53,
            gridViewTextBoxColumn54});
            this.GridViewCFDIRelacionadoDocumento.ViewDefinition = tableViewDefinition3;
            // 
            // GridComplementoPagos
            // 
            this.GridComplementoPagos.Caption = "Complemento Pagos";
            gridViewTextBoxColumn55.FieldName = "FecPago";
            gridViewTextBoxColumn55.FormatString = "{0:dd-MM-yy}";
            gridViewTextBoxColumn55.HeaderText = "Fec. Pago";
            gridViewTextBoxColumn55.Name = "FecPago";
            gridViewTextBoxColumn55.Width = 75;
            gridViewTextBoxColumn56.FieldName = "FormaDePago";
            gridViewTextBoxColumn56.HeaderText = "Forma de Pago";
            gridViewTextBoxColumn56.Name = "FormaDePago";
            gridViewTextBoxColumn56.Width = 125;
            gridViewTextBoxColumn57.FieldName = "Moneda";
            gridViewTextBoxColumn57.HeaderText = "Moneda";
            gridViewTextBoxColumn57.Name = "Moneda";
            gridViewTextBoxColumn57.Width = 65;
            gridViewTextBoxColumn58.FieldName = "TipoCambio";
            gridViewTextBoxColumn58.HeaderText = "Tipo de Cambio";
            gridViewTextBoxColumn58.Name = "TipoCambio";
            gridViewTextBoxColumn58.Width = 75;
            gridViewTextBoxColumn59.DataType = typeof(decimal);
            gridViewTextBoxColumn59.FieldName = "Monto";
            gridViewTextBoxColumn59.FormatString = "{0:n}";
            gridViewTextBoxColumn59.HeaderText = "Monto";
            gridViewTextBoxColumn59.Name = "Monto";
            gridViewTextBoxColumn59.Width = 85;
            gridViewTextBoxColumn60.FieldName = "NumOperacion";
            gridViewTextBoxColumn60.HeaderText = "Núm. Operacion";
            gridViewTextBoxColumn60.Name = "NumOperacion";
            gridViewTextBoxColumn61.FieldName = "RfcEmisorCtaOrd";
            gridViewTextBoxColumn61.HeaderText = "RFC Emisor Cta. Ord.";
            gridViewTextBoxColumn61.Name = "RfcEmisorCtaOrd";
            gridViewTextBoxColumn61.Width = 95;
            gridViewTextBoxColumn62.FieldName = "NomBancoOrdExt";
            gridViewTextBoxColumn62.HeaderText = "Nom. Banco. OrdExt.";
            gridViewTextBoxColumn62.Name = "NomBancoOrdExt";
            gridViewTextBoxColumn62.Width = 120;
            gridViewTextBoxColumn63.FieldName = "CtaOrdenante";
            gridViewTextBoxColumn63.HeaderText = "Cta. Ordenante";
            gridViewTextBoxColumn63.Name = "CtaOrdenante";
            gridViewTextBoxColumn63.Width = 120;
            gridViewTextBoxColumn64.FieldName = "RfcEmisorCtaBen";
            gridViewTextBoxColumn64.HeaderText = "RFC Emisor Cta. Ben.";
            gridViewTextBoxColumn64.Name = "RfcEmisorCtaBen";
            gridViewTextBoxColumn64.Width = 110;
            gridViewTextBoxColumn65.FieldName = "CtaBeneficiario";
            gridViewTextBoxColumn65.HeaderText = "Cta. Beneficiario";
            gridViewTextBoxColumn65.Name = "CtaBeneficiario";
            gridViewTextBoxColumn65.Width = 95;
            gridViewTextBoxColumn66.FieldName = "TipoCadPago";
            gridViewTextBoxColumn66.HeaderText = "Tipo Cad. Pago";
            gridViewTextBoxColumn66.Name = "TipoCadPago";
            gridViewTextBoxColumn66.Width = 75;
            gridViewTextBoxColumn67.HeaderText = "Data";
            gridViewTextBoxColumn67.IsVisible = false;
            gridViewTextBoxColumn67.Name = "Data";
            gridViewTextBoxColumn67.VisibleInColumnChooser = false;
            this.GridComplementoPagos.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn55,
            gridViewTextBoxColumn56,
            gridViewTextBoxColumn57,
            gridViewTextBoxColumn58,
            gridViewTextBoxColumn59,
            gridViewTextBoxColumn60,
            gridViewTextBoxColumn61,
            gridViewTextBoxColumn62,
            gridViewTextBoxColumn63,
            gridViewTextBoxColumn64,
            gridViewTextBoxColumn65,
            gridViewTextBoxColumn66,
            gridViewTextBoxColumn67});
            this.GridComplementoPagos.NewRowText = "";
            this.GridComplementoPagos.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.GridViewComplementoPagoDoctosRelacionados});
            this.GridComplementoPagos.ViewDefinition = tableViewDefinition6;
            // 
            // GridViewComplementoPagoDoctosRelacionados
            // 
            this.GridViewComplementoPagoDoctosRelacionados.Caption = "Doctos. Relacionados";
            gridViewTextBoxColumn68.FieldName = "Folio";
            gridViewTextBoxColumn68.HeaderText = "Folio";
            gridViewTextBoxColumn68.Name = "Folio";
            gridViewTextBoxColumn69.FieldName = "Serie";
            gridViewTextBoxColumn69.HeaderText = "Serie";
            gridViewTextBoxColumn69.Name = "Serie";
            gridViewTextBoxColumn70.FieldName = "IdDocumento";
            gridViewTextBoxColumn70.HeaderText = "IdDocumento";
            gridViewTextBoxColumn70.Name = "IdDocumento";
            gridViewTextBoxColumn70.Width = 240;
            gridViewTextBoxColumn71.FieldName = "Moneda";
            gridViewTextBoxColumn71.HeaderText = "Moneda";
            gridViewTextBoxColumn71.Name = "Moneda";
            gridViewTextBoxColumn72.FieldName = "TipoCambio";
            gridViewTextBoxColumn72.HeaderText = "Tipo de Cambio";
            gridViewTextBoxColumn72.Name = "TipoCambio";
            gridViewTextBoxColumn73.HeaderText = "Metodo de Pago";
            gridViewTextBoxColumn73.Name = "column10";
            gridViewTextBoxColumn74.FieldName = "NumParcialidad";
            gridViewTextBoxColumn74.HeaderText = "Núm. Parcialidad";
            gridViewTextBoxColumn74.Name = "NumParcialidad";
            gridViewTextBoxColumn74.Width = 65;
            gridViewTextBoxColumn75.DataType = typeof(decimal);
            gridViewTextBoxColumn75.FieldName = "ImpSaldoAnterior";
            gridViewTextBoxColumn75.FormatString = "{0:n}";
            gridViewTextBoxColumn75.HeaderText = "Saldo Anterior";
            gridViewTextBoxColumn75.Name = "ImpSaldoAnterior";
            gridViewTextBoxColumn75.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn75.Width = 90;
            gridViewTextBoxColumn76.DataType = typeof(decimal);
            gridViewTextBoxColumn76.FieldName = "ImpPagado";
            gridViewTextBoxColumn76.HeaderText = "Pagado";
            gridViewTextBoxColumn76.Name = "ImpPagado";
            gridViewTextBoxColumn76.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn76.Width = 90;
            gridViewTextBoxColumn77.DataType = typeof(decimal);
            gridViewTextBoxColumn77.FieldName = "ImpSaldoInsoluto";
            gridViewTextBoxColumn77.HeaderText = "Saldo Insoluto";
            gridViewTextBoxColumn77.Name = "ImpSaldoInsoluto";
            gridViewTextBoxColumn77.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn77.Width = 90;
            gridViewTextBoxColumn78.FieldName = "Estado";
            gridViewTextBoxColumn78.HeaderText = "Estado";
            gridViewTextBoxColumn78.Name = "Estado";
            gridViewTextBoxColumn78.Width = 95;
            this.GridViewComplementoPagoDoctosRelacionados.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn68,
            gridViewTextBoxColumn69,
            gridViewTextBoxColumn70,
            gridViewTextBoxColumn71,
            gridViewTextBoxColumn72,
            gridViewTextBoxColumn73,
            gridViewTextBoxColumn74,
            gridViewTextBoxColumn75,
            gridViewTextBoxColumn76,
            gridViewTextBoxColumn77,
            gridViewTextBoxColumn78});
            this.GridViewComplementoPagoDoctosRelacionados.NewRowText = "";
            this.GridViewComplementoPagoDoctosRelacionados.ViewDefinition = tableViewDefinition5;
            // 
            // Iconos
            // 
            this.Iconos.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Iconos.ImageStream")));
            this.Iconos.TransparentColor = System.Drawing.Color.Transparent;
            this.Iconos.Images.SetKeyName(0, "XML");
            this.Iconos.Images.SetKeyName(1, "PDF");
            // 
            // ContextMenu
            // 
            this.ContextMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ContextMenuCopiar,
            this.ContextMenuValidar,
            this.ContextMenuEstadoSAT,
            this.ContextMenuArchivos,
            this.ContextMenuReciboPago});
            // 
            // ContextMenuCopiar
            // 
            this.ContextMenuCopiar.Name = "ContextMenuCopiar";
            this.ContextMenuCopiar.Text = "Copiar";
            this.ContextMenuCopiar.Click += new System.EventHandler(this.ContextMenuCopiar_Click);
            // 
            // ContextMenuValidar
            // 
            this.ContextMenuValidar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_seguridad_comprobado;
            this.ContextMenuValidar.Name = "ContextMenuValidar";
            this.ContextMenuValidar.Text = "Validar";
            this.ContextMenuValidar.Click += new System.EventHandler(this.ToolBarButtonValidarSeleccionado_Click);
            // 
            // ContextMenuEstadoSAT
            // 
            this.ContextMenuEstadoSAT.Name = "ContextMenuEstadoSAT";
            this.ContextMenuEstadoSAT.Text = "Estado SAT";
            this.ContextMenuEstadoSAT.Click += new System.EventHandler(this.ContextMenuEstadoSAT_Click);
            // 
            // ContextMenuArchivos
            // 
            this.ContextMenuArchivos.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ContextArchivoPDFBuscar,
            this.ContextArchivoPDF,
            this.ContextArchivoAcusePDF,
            this.ContextArchivoAcuseXML});
            this.ContextMenuArchivos.Name = "ContextMenuArchivos";
            this.ContextMenuArchivos.Text = "Archivos";
            // 
            // ContextArchivoPDFBuscar
            // 
            this.ContextArchivoPDFBuscar.Name = "ContextArchivoPDFBuscar";
            this.ContextArchivoPDFBuscar.Text = "Buscar PDF";
            this.ContextArchivoPDFBuscar.Click += new System.EventHandler(this.ContextArchivoPDFBuscar_Click);
            // 
            // ContextArchivoPDF
            // 
            this.ContextArchivoPDF.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_pdf;
            this.ContextArchivoPDF.Name = "ContextArchivoPDF";
            this.ContextArchivoPDF.Text = "Adjuntar PDF";
            this.ContextArchivoPDF.Click += new System.EventHandler(this.ContextArchivoPDF_Click);
            // 
            // ContextArchivoAcusePDF
            // 
            this.ContextArchivoAcusePDF.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_pdf;
            this.ContextArchivoAcusePDF.Name = "ContextArchivoAcusePDF";
            this.ContextArchivoAcusePDF.Text = "Adjuntar Acuse PDF";
            this.ContextArchivoAcusePDF.Click += new System.EventHandler(this.ContextArchivoAcusePDF_Click);
            // 
            // ContextArchivoAcuseXML
            // 
            this.ContextArchivoAcuseXML.Name = "ContextArchivoAcuseXML";
            this.ContextArchivoAcuseXML.Text = "Adjuntar Acuse XML";
            this.ContextArchivoAcuseXML.Click += new System.EventHandler(this.ContextArchivoAcuseXML_Click);
            // 
            // ContextMenuReciboPago
            // 
            this.ContextMenuReciboPago.Name = "ContextMenuReciboPago";
            this.ContextMenuReciboPago.Text = "Crear Recibo de Pago";
            this.ContextMenuReciboPago.Click += new System.EventHandler(this.ContextMenuReciboPago_Click);
            // 
            // ToolBarButtonVersion69B
            // 
            this.ToolBarButtonVersion69B.Name = "ToolBarButtonVersion69B";
            this.ToolBarButtonVersion69B.Text = "Versión: Art. 69-B";
            this.ToolBarButtonVersion69B.Click += new System.EventHandler(this.ToolBarButtonVersion69B_Click);
            // 
            // ComprobantesValidadorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1430, 645);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.BarraEstado);
            this.Controls.Add(this.CommandBarValidador);
            this.Name = "ComprobantesValidadorForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ComprobantesValidador";
            this.Load += new System.EventHandler(this.ComprobantesValidador_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CommandBarValidador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCarpetas.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCarpetas.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCarpetas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.GridData.ResumeLayout(false);
            this.GridData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridCFDIRelacionado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewCFDIRelacionadoDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComplementoPagos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewComplementoPagoDoctosRelacionados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar CommandBarValidador;
        private Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBarValidador;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelCarpeta;
        private Telerik.WinControls.UI.CommandBarHostItem ToolBarHostItemCboCarpeta;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonDetenerBusqueda;
        private Telerik.WinControls.UI.RadStatusStrip BarraEstado;
        private Telerik.WinControls.UI.RadLabelElement BarraEstadoLabel;
        private Telerik.WinControls.UI.RadMultiColumnComboBox CboCarpetas;
        private Telerik.WinControls.UI.CommandBarSeparator ToolBarSeparator1;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonAgregar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonActualizar;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonPDF;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonImprimir;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonExportar;
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.RotatingRingsWaitingBarIndicatorElement rotatingRingsWaitingBarIndicatorElement1;
        private Telerik.WinControls.UI.GridViewTemplate GridConceptos;
        private Telerik.WinControls.UI.GridViewTemplate GridCFDIRelacionado;
        private Telerik.WinControls.UI.GridViewTemplate GridComplementoPagos;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarToggleFiltro;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private GridViewTemplate GridViewCFDIRelacionadoDocumento;
        private GridViewTemplate GridViewComplementoPagoDoctosRelacionados;
        private System.Windows.Forms.ImageList Iconos;
        private RadContextMenu ContextMenu;
        private RadMenuItem ContextMenuValidar;
        private RadMenuItem ContextMenuEstadoSAT;
        private RadMenuItem ContextMenuArchivos;
        private RadMenuItem ContextArchivoPDF;
        private RadMenuItem ContextArchivoAcuseXML;
        private RadMenuItem ContextArchivoAcusePDF;
        private RadMenuItem ContextMenuCopiar;
        private RadGridView GridData;
        private CommandBarSplitButton ToolBarButtonCarpeta;
        private RadMenuItem ToolBarButtonCarpetaIncluirSubCarpetas;
        private RadMenuItem ToolBarButtonCarpetaInlcuirArchivosZIP;
        private RadMenuItem ContextArchivoPDFBuscar;
        private CommandBarButton ToolBarButtonCerrar;
        private RadMenuItem ToolBarButtonPDFAsignar;
        private RadMenuItem ToolBarButtonPDFBuscar;
        private RadMenuItem ToolBarButtonImprimirValidacion;
        private RadMenuItem ToolBarButtonImprimirPDF;
        private RadMenuItem ToolBarButtonArchivoLog;
        private RadMenuItem ToolBarMenuAbrirArchivoZIP;
        private RadMenuItem ToolBarMenuCrearArchivoZIP;
        private RadMenuItem ToolBarButtonCopiarA;
        private CommandBarSplitButton ToolBarButtonRemover;
        private CommandBarDropDownButton ToolBarButtonBackup;
        private RadMenuItem ToolBarButtonBakcupAuto;
        private RadMenuItem ToolBarButtonBakcupTodos;
        private RadMenuItem ToolBarButtonBakcupSeleccionado;
        private CommandBarSplitButton ToolBarButtonHerramientas;
        private RadMenuItem ToolBarButtonLimpiar;
        private RadMenuItem ToolBarButtonGuardarVista;
        private RadMenuItem ToolBarButtonMostrarAviso;
        private CommandBarSplitButton ToolBarButtonValidar;
        private RadMenuItem ToolBarButtonValidarSeleccionado;
        private RadMenuItem ToolBarButtonValidarTodos;
        private CommandBarButton ToolBarButtonPago;
        private RadMenuItem ContextMenuReciboPago;
        private RadMenuItem ToolBarButtonConfiguracion;
        private RadMenuItem ToolBarButtonPDFValidacion;
        private RadMenuItem ExportarNormal;
        private RadMenuItem ExportarTemplete;
        private RadMenuItem ToolBarButtonVersion69B;
    }
}
