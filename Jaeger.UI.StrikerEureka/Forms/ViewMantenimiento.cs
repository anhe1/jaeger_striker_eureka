﻿using System;
using Jaeger.Edita.Helpers;
using Jaeger.Helpers;
using Jaeger.Edita.V2;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views
{
    public partial class ViewMantenimiento : Telerik.WinControls.UI.RadForm
    {
        private DbMaintenance maintenance;

        public ViewMantenimiento()
        {
            InitializeComponent();
        }

        private void ViewMantenimiento_Load(object sender, EventArgs e)
        {
            this.maintenance = new DbMaintenance(ConfigService.Synapsis.RDS.Edita);
            this.radPropertyGrid1.SelectedObject = ConfigService.Synapsis.RDS.Edita;
            this.radGridView1.TelerikGridCommon();
            this.radGridView1.DataSource = maintenance.GetTablesInfo();
        }

        private void ButtonCrear_Click(object sender, EventArgs e)
        {
            this.maintenance.CreateClass();
        }

        private void ToolBarButtonCrearClase_Click(object sender, EventArgs e)
        {
            if (this.radGridView1.CurrentRow != null)
            {
                var tabla = this.radGridView1.CurrentRow.DataBoundItem as DbMaintenance.Tabla;
                if (tabla != null)
                    this.maintenance.CreateClass(tabla.Nombre);
            }
        }

        private void ToolBarButtonCrearTabla_Click(object sender, EventArgs e)
        {
            var crea = new SqlSugarClasificacionG(ConfigService.Synapsis.RDS.Edita);
            crea.CreateTable();
        }
    }
}
