﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.ComponentModel;
using System.Diagnostics;
using Telerik.WinControls;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls.UI;
using Jaeger.Entities;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Validador.V3;
using Jaeger.Helpers;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2.Contable.Enums;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2.Empresa.Entities;
using Jaeger.Edita.V2;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views {
    public partial class ComprobantesValidadorForm : RadForm {
        private Edita.V2.Validador.Entities.Configuracion configuracionLocal;
        private ISqlConfiguracion data;
        private ViewModelEmpresaConfiguracion configuracion;
        private string fileLayOut = Path.Combine(Application.StartupPath, "view_comprobante_validador_layout.xml");
        private Stopwatch stopwatch = new Stopwatch();
        private BackgroundWorker preparar;
        private HelperValidador ayuda;
        private CarpetasRecientes carpetas = new CarpetasRecientes();
        private Jaeger.UI.Helpers.MiniExcelTemplete _ExcelTemplete;

        public ComprobantesValidadorForm() {
            InitializeComponent();

            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += Preparar_DoWork;
            this.preparar.RunWorkerCompleted += Preparar_RunWorkerCompleted;
        }

        private void ComprobantesValidador_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this._ExcelTemplete = new UI.Helpers.MiniExcelTemplete();
            this.ToolBarHostItemCboCarpeta.HostedItem = this.CboCarpetas.MultiColumnComboBoxElement;
            this.ToolBarButtonBakcupAuto.DataBindings.Clear();
            this.CboCarpetas.KeyDown += CboCarpetas_KeyDown;
            this.preparar.RunWorkerAsync();
        }

        private void CboCarpetas_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter)
                this.ToolBarButtonActualizar.PerformClick();
        }

        #region menu contextual 

        private void ContextArchivoPDFBuscar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                int index = this.GridData.Rows.IndexOf(this.GridData.CurrentRow);
                string busqueda = Validador.V3.Administrador.BuscarPDF(this.ayuda.Documentos[index].ArchivoXML.FileInfo, this.ayuda.Documentos[index].IdDocumento);
                if (File.Exists(busqueda) == false) {
                    if (RadMessageBox.Show(this, string.Concat("No se enconto ninguna conincidencia para el folio fiscal: ", this.ayuda.Documentos[index].IdDocumento, " ¿Quieres buscarlo manualmente?"), "Información", MessageBoxButtons.YesNo, RadMessageIcon.Info) == DialogResult.Yes) {
                        OpenFileDialog openfile = new OpenFileDialog() {
                            Title = "Buscar representación impresa",
                            InitialDirectory = "",
                            Filter = "*.pdf|*.PDF"
                        };
                        if (openfile.ShowDialog() == DialogResult.OK) {
                            this.ayuda.Documentos[index].ArchivoPDF = Validador.V3.CFDReaderCommon.Load(new FileInfo(openfile.FileName));
                        }
                    }
                }
            }
        }

        private void ContextMenuValidar_Click(object sender, EventArgs e) {
            //this.ToolBarButtonValidarSeleccionado.PerformClick();
        }

        private void ContextMenuEstadoSAT_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                int index = this.GridData.Rows.IndexOf(this.GridData.CurrentRow);
                this.ayuda.ExecuteEstadoSAT(index);
            }
        }

        private void ContextMenuCopiar_Click(object sender, EventArgs e) {
            Clipboard.SetDataObject(this.GridData.CurrentCell.Value.ToString());
        }

        private void ContextArchivoPDF_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                int index = this.GridData.Rows.IndexOf(this.GridData.CurrentRow);
                if (this.ayuda.Documentos[index].ArchivoPDF != null) {
                    if (File.Exists(this.ayuda.Documentos[index].ArchivoPDF.FullFileName)) {
                        if (RadMessageBox.Show(string.Concat("Actualmente este comprobante tiene asignado un representación impresa del archivo: ",
                                                            this.ayuda.Documentos[index].ArchivoPDF.FullFileName,
                                                            ", aún así quieres asignar un nuevo archivo?"), "Informacion", MessageBoxButtons.YesNo, RadMessageIcon.Info) == System.Windows.Forms.DialogResult.No) {
                            return;
                        }
                    }
                }

                OpenFileDialog openFile = new OpenFileDialog {
                    Title = "Buscar representación impresa",
                    InitialDirectory = Path.GetDirectoryName(this.ayuda.Documentos[index].ArchivoXML.FullFileName),
                    Filter = "*.pdf|*.PDF"
                };
                if (openFile.ShowDialog() == DialogResult.OK) {
                    if (File.Exists(openFile.FileName)) {
                        this.ayuda.Documentos[index].ArchivoXML = Jaeger.Validador.V3.CFDReaderCommon.Load(new FileInfo(openFile.FileName));
                    }
                    else {
                        RadMessageBox.Show(this, "No se tiene acceso al archivo!", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    }
                }
            }
        }

        private void ContextArchivoAcuseXML_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                int index = this.GridData.Rows.IndexOf(this.GridData.CurrentRow);
                if (this.ayuda.Documentos[index].AcuseXML != null) {
                    if (File.Exists(this.ayuda.Documentos[index].AcuseXML.FullFileName)) {
                        if (RadMessageBox.Show(string.Concat("Actualmente este comprobante tiene asignado un archivo: ",
                                                            this.ayuda.Documentos[index].AcuseXML.FullFileName,
                                                            ", aún así quieres asignar un nuevo archivo?"), "Informacion", MessageBoxButtons.YesNo, RadMessageIcon.Info) == System.Windows.Forms.DialogResult.No) {
                            return;
                        }
                    }
                }

                OpenFileDialog openFile = new OpenFileDialog {
                    Title = "Buscar Acuse de Cancelación XML",
                    InitialDirectory = Path.GetDirectoryName(this.ayuda.Documentos[index].ArchivoXML.FullFileName),
                    Filter = "*.xml|*.XML"
                };
                if (openFile.ShowDialog() == DialogResult.OK) {
                    if (File.Exists(openFile.FileName)) {
                        this.ayuda.Documentos[index].AcuseXML = Jaeger.Validador.V3.CFDReaderCommon.Load(new FileInfo(openFile.FileName));
                    }
                    else {
                        RadMessageBox.Show(this, "No se tiene acceso al archivo!", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    }
                }
            }
        }

        private void ContextArchivoAcusePDF_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                int index = this.GridData.Rows.IndexOf(this.GridData.CurrentRow);
                if (this.ayuda.Documentos[index].AcusePDF != null) {
                    if (File.Exists(this.ayuda.Documentos[index].AcusePDF.FullFileName)) {
                        if (RadMessageBox.Show(string.Concat("Actualmente este comprobante tiene asignado un archivo: ",
                                                            this.ayuda.Documentos[index].AcusePDF.FullFileName,
                                                            ", aún así quieres asignar un nuevo archivo?"), "Informacion", MessageBoxButtons.YesNo, RadMessageIcon.Info) == System.Windows.Forms.DialogResult.No) {
                            return;
                        }
                    }
                }

                OpenFileDialog openFile = new OpenFileDialog {
                    Title = "Buscar Representación del Acuse de Cancelación",
                    InitialDirectory = Path.GetDirectoryName(this.ayuda.Documentos[index].ArchivoXML.FullFileName),
                    Filter = "*.pdf|*.PDF"
                };
                if (openFile.ShowDialog() == DialogResult.OK) {
                    if (File.Exists(openFile.FileName)) {
                        this.ayuda.Documentos[index].AcusePDF = Validador.V3.CFDReaderCommon.Load(new FileInfo(openFile.FileName));
                    }
                    else {
                        RadMessageBox.Show(this, "No se tiene acceso al archivo!", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    }
                }
            }
        }

        #endregion

        #region barra de herramientas

        private void ToolBarButtonCarpeta_Click(object sender, EventArgs e) {
            FolderBrowserDialog folder = new FolderBrowserDialog();
            if (folder.ShowDialog() != DialogResult.OK) {
                return;
            }
            this.CboCarpetas.Text = folder.SelectedPath;
            this.carpetas.Add(new CarpetaItem { Carpeta = folder.SelectedPath.ToString() });
            this.ToolBarButtonActualizar.PerformClick();
            this.carpetas.Save();
        }

        private void CboCarpetas_CommandCellClick(object sender, GridViewCellEventArgs e) {
            if (e.Column.Name == "Remover") {
                this.carpetas.Delete(e.RowIndex);
                this.carpetas.Save();
                this.CboCarpetas.DataSource = null;
                this.CboCarpetas.DataSource = this.carpetas.Items;
            }
        }

        private void ToolBarButtonDetenerBusqueda_Click(object sender, EventArgs e) {

        }

        private void ToolBarButtonAgregar_Click(object sender, EventArgs e) {
            OpenFileDialog openFile = new OpenFileDialog() { Filter = "*.xml|*.XML" };
            if (openFile.ShowDialog() == DialogResult.OK) {
                this.ToolBarButtonAgregar.Enabled = false;
                this.ayuda.Agregar(new FileInfo(openFile.FileName));
                this.ToolBarButtonAgregar.Enabled = true;
                this.GridData.DataSource = this.ayuda.Documentos;
                this.GridData.Refresh();
            }
        }

        private void ToolBarButtonRemover_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                if (RadMessageBox.Show(this, "¿Esta seguro de remover el comprobante? Esta acción no elimina el archivo.", "Atención", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                    int index = this.GridData.Rows.IndexOf(this.GridData.CurrentRow);
                    this.ayuda.Remover(index);
                }
            }
        }

        private void ToolBarButtonLimpiar_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show(this, "¿Esta seguro?", "Atención", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                this.ayuda.Documentos.Clear();
            }
        }

        private void ToolBarToggleFiltro_Click(object sender, EventArgs e) {
            this.GridData.ShowFilteringRow = this.ToolBarToggleFiltro.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false)
                this.GridData.FilterDescriptors.Clear();
        }

        private void ToolBarButtonExportar_Click(object sender, EventArgs e) {
            TelerikGridExport exportar = new TelerikGridExport(this.GridData);
            exportar.ShowDialog();
        }

        private void ToolBarButtonConfiguracion_Click(object sender, EventArgs e) {
            using (var conf = new ViewComprobantesValidadorConfigurar()) {
                conf.StartPosition = FormStartPosition.CenterParent;
                conf.Text = "Configuración";
                conf.ShowDialog(this);
            }
        }

        private void ToolBarButtonValidarSeleccionado_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                int index = this.GridData.Rows.IndexOf(this.GridData.CurrentRow);
                MessageBox.Show("No implementado");
                //this.ayuda.ExecuteValidacion(index);
            }
        }

        private void ToolBarButtonValidarTodos_Click(object sender, EventArgs e) {
            this.Espera.StartWaiting();
            this.Espera.Visible = true;
            this.ToolBarValidador.Enabled = false;
            this.GridData.DataSource = null;
            this.ayuda.ValidacionChanged += this.Validacion_Proceso;
            this.ayuda.ValidacionCompleted += this.Validacion_Terminada;
            this.ayuda.BackupChanged += this.Backup_Changed;
            this.ayuda.BackupCompleted += this.Backup_Completed;
            this.ayuda.ExecuteValidacion();
        }

        private void ToolBarButtonBakcupAuto_Click(object sender, EventArgs e) {
            if (this.ToolBarButtonBakcupAuto.ToggleState == ToggleState.Off) {
                if (RadMessageBox.Show(this, "¿Esta seguro de desactivar la opción de respaldo automático de Documentos?", "Atención", MessageBoxButtons.YesNo) == DialogResult.No)
                    this.ToolBarButtonBakcupAuto.ToggleState = ToggleState.On;

                if (this.ToolBarButtonBakcupAuto.ToggleState == ToggleState.On)
                    this.ToolBarButtonBackup.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_protección_de_datos;
                else
                    this.ToolBarButtonBackup.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_copia_seguridad;
            }
        }

        private void ToolBarButtonBakcupAuto_ToggleStateChanged(object sender, StateChangedEventArgs args) {
            this.ayuda.AutoBackup = (args.ToggleState == ToggleState.On);
        }

        private void ToolBarButtonBakcupTodos_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Crear)) {
                espera.Text = "Creando ...";
                espera.ShowDialog(this);
            }
        }

        private void ToolBarButtonBakcupSeleccionado_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                this.ayuda.ExecuteRegistra(this.GridData.Rows.IndexOf(this.GridData.CurrentRow));
                this.ayuda.ExecuteBackUp(this.GridData.Rows.IndexOf(this.GridData.CurrentRow));
            }
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ToolBarButtonPDFAsignar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                int index = this.GridData.Rows.IndexOf(this.GridData.CurrentRow);
                if (this.ayuda.Documentos[index].ArchivoPDF != null) {
                    if (File.Exists(this.ayuda.Documentos[index].ArchivoPDF.FullFileName)) {
                        if (RadMessageBox.Show(this, string.Concat("Actualmente este comprobante tiene asignado un representación impresa del archivo: ",
                                                            this.ayuda.Documentos[index].ArchivoPDF.FileName,
                                                            ", aún así quieres asignar un nuevo archivo?"), "Informacion", MessageBoxButtons.YesNo, RadMessageIcon.Info) == System.Windows.Forms.DialogResult.No) {
                            return;
                        }
                    }
                }

                OpenFileDialog openFile = new OpenFileDialog() {
                    Title = "Buscar representación impresa",
                    InitialDirectory = Path.GetDirectoryName(this.ayuda.Documentos[index].ArchivoXML.FileName),
                    Filter = "*.pdf|*.PDF"
                };

                if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    if (File.Exists(openFile.FileName))
                        this.ayuda.Documentos[index].ArchivoPDF = CFDReaderCommon.Load(new FileInfo(openFile.FileName));
                }
            }
        }

        private void ToolBarButtonPDFBuscar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.BuscarPDF)) {
                espera.Text = "Buscando ...";
                espera.ShowDialog(this);
            }
        }

        private void ToolBarImprimirValidacion_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                int index = this.GridData.Rows.IndexOf(this.GridData.CurrentRow);
                if (this.ayuda.Documentos[index].Resultado == Enums.EnumValidateResult.Valido | this.ayuda.Documentos[index].Resultado == Enums.EnumValidateResult.NoValido) {
                    ViewReportes reporte = new ViewReportes(this.ayuda.Documentos[index].Validacion);
                    reporte.Show();
                }
            }
        }

        private void ToolBarButtonImprimirPDF_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                int index = this.GridData.Rows.IndexOf(this.GridData.CurrentRow);
                if (this.ayuda.Documentos[index].ArchivoPDF != null) {
                    if (File.Exists(this.ayuda.Documentos[index].ArchivoPDF.FullFileName)) {
                        ViewerPdf pdf = new ViewerPdf(this.ayuda.Documentos[index].ArchivoPDF.FullFileName);
                        pdf.Show();
                    }
                }
            }
        }

        private async void ToolBarButtonArchivoLog_Click(object sender, EventArgs e) {
            OpenFileDialog openFile = new OpenFileDialog() {
                Title = "Buscar representación impresa",
                Filter = "*.zip|*.ZIP|*.log|*.LOG|*.json|*.JSON"
            };

            if (openFile.ShowDialog() == DialogResult.OK) {
                await ayuda.ExecuteLog(openFile.FileName);
                this.GridData.DataSource = this.ayuda.Documentos;
            }
        }

        private async void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            this.Espera.StartWaiting();
            this.Espera.Visible = true;
            this.ToolBarValidador.Enabled = false;
            this.GridData.TableElement.BeginUpdate();
            this.GridData.DataSource = null;
            this.ayuda.SearchFiles += new EventHandler<Validador.V3.Entities.ProcessChanged>(this.Busqueda_Proceso);
            await this.ayuda.ExecuteBusqueda(this.CboCarpetas.Text, this.ToolBarButtonCarpetaIncluirSubCarpetas.IsChecked);
            this.ToolBarValidador.Enabled = true;
            this.GridData.DataSource = this.ayuda.Documentos;
            this.GridData.TableElement.EndUpdate();
            this.Espera.StopWaiting();

            if (this.ayuda.DocumentosError.Count > 0 && this.ayuda.MostrarAvisos) {

            }
        }

        private async void ToolBarButtonCopiarA_Click(object sender, EventArgs e) {
            FolderBrowserDialog folder = new FolderBrowserDialog() { Description = "Copiar a ..." };
            if (!(folder.ShowDialog() == DialogResult.OK))
                return;
            this.Espera.StartWaiting();
            this.Espera.Visible = true;
            this.ToolBarValidador.Enabled = false;
            await this.ayuda.ExecuteCopiarA(folder.SelectedPath);
            this.ToolBarValidador.Enabled = true;
            this.Espera.StopWaiting();
        }

        private async void ToolBarMenuAbrirArchivoZIP_Click(object sender, EventArgs e) {
            OpenFileDialog dialogo = new OpenFileDialog {
                Title = "Abrir archivo ZIP",
                Filter = "*.zip|*.ZIP"
            };

            if (dialogo.ShowDialog() == DialogResult.OK) {
                this.Espera.StartWaiting();
                this.Espera.Visible = true;
                this.ToolBarValidador.Enabled = false;
                string temp = await this.ayuda.ExecuteOpenZIP(dialogo.FileName, "");
                if (Directory.Exists(temp)) {
                    this.CboCarpetas.Text = temp;
                    this.ToolBarButtonActualizar.PerformClick();
                }
                this.ToolBarValidador.Enabled = true;
                this.Espera.StopWaiting();
            }
        }

        private async void ToolBarMenuCrearArchivoZIP_Click(object sender, EventArgs e) {
            SaveFileDialog dialogo = new SaveFileDialog();
            dialogo.Filter = "Zip File | *.zip";
            dialogo.FileName = "Validacion.zip";
            if (dialogo.ShowDialog() == DialogResult.OK) {
                this.Espera.StartWaiting();
                this.Espera.Visible = true;
                this.ToolBarValidador.Enabled = false;
                await this.ayuda.ExecuteCreateZIP(dialogo.FileName);
                this.ToolBarValidador.Enabled = true;
                this.Espera.StopWaiting();
            }
        }

        private void ToolBarButtonMostrarAviso_Click(object sender, EventArgs e) {
            this.ayuda.MostrarAvisos = this.ToolBarButtonMostrarAviso.IsChecked;
        }

        private void ToolBarButtonGuardarVista_Click(object sender, EventArgs e) {
            try {
                this.GridData.SaveLayout(this.fileLayOut);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        private void ToolBarButtonPago_Click(object sender, EventArgs e) {
            if (this.ayuda.Documentos.Count <= 0)
                return;
            Edita.V2.Contable.Entities.ViewModelPrePoliza prepoliza = new Edita.V2.Contable.Entities.ViewModelPrePoliza() { Tipo = EnumPolizaTipo.Egreso, Creo = ConfigService.Piloto.Clave, NoIndet = "", Emisor = new Edita.V2.Contable.Entities.PrePolizaCuenta { Nombre = "---" }, Receptor = new Edita.V2.Contable.Entities.PrePolizaCuenta { Nombre = "---" }, Concepto = "Pago", FechaPago = DateTime.Now };
            foreach (var item in this.ayuda.Documentos) {
                prepoliza.Comprobantes.Add(new Edita.V2.Contable.Entities.ViewModelPrePolizaComprobante(item));
            }
            var recibo = new Contable.ViewRecibo2Pago(prepoliza);
            recibo.StartPosition = FormStartPosition.CenterParent;
            recibo.ShowDialog();
        }

        private void ContextMenuReciboPago_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var newItem = this.GridData.CurrentRow.DataBoundItem as Documento;
                if (newItem != null) {
                    if (newItem.Validacion != null) {
                        Edita.V2.Contable.Entities.ViewModelPrePoliza prepoliza = new Edita.V2.Contable.Entities.ViewModelPrePoliza() { ReceptorRFC = newItem.EmisorRFC, ReceptorBeneficiario = newItem.Emisor, Tipo = EnumPolizaTipo.Egreso, Creo = ConfigService.Piloto.Clave, NoIndet = "" };
                        prepoliza.Comprobantes.Add(new Edita.V2.Contable.Entities.ViewModelPrePolizaComprobante(newItem));
                        var recibo = new Contable.ViewRecibo2Pago(prepoliza);
                        recibo.StartPosition = FormStartPosition.CenterParent;
                        recibo.ShowDialog();
                    }
                }
            }
        }

        private void ToolBarButtonPDFValidacion_Click(object sender, EventArgs e) {
            var folder = new FolderBrowserDialog() { Description = "Selecciona ruta de descarga" };
            if (folder.ShowDialog(this) != DialogResult.OK)
                return;
            if (Directory.Exists(folder.SelectedPath) == false) {
                RadMessageBox.Show(this, "No se encontro una ruta de descarga valida!", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }

            ToolBarButtonPDFValidacion.Tag = folder.SelectedPath;

            using (var espera = new Waiting2Form(this.PDFValidacion)) {
                espera.Text = "Procesando ...";
                espera.ShowDialog(this);
            }
        }

        #endregion

        #region acciones del grid

        private void GridFormatosCondicionales() {
            ExpressionFormattingObject emisorReceptor = new ExpressionFormattingObject("Receptor", string.Format("EmisorRFC NOT = '{0}' AND ReceptorRFC NOT = '{1}'", ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RFC), true) { RowBackColor = Color.LightYellow };
            ExpressionFormattingObject registro = new ExpressionFormattingObject("Registrado", "Registrado = true", true) { RowForeColor = Color.Green };
            this.GridData.Columns["EmisorRFC"].ConditionalFormattingObjectList.Add(emisorReceptor);
            this.GridData.Columns["Registrado"].ConditionalFormattingObjectList.Add(registro);
        }

        private void GridData_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridDataCellElement)
                if (this.GridData.CurrentRow.ViewInfo.ViewTemplate == this.GridData.MasterTemplate)
                    e.ContextMenu = this.ContextMenu.DropDown;
        }

        private void GridData_CommandCellClick(object sender, GridViewCellEventArgs e) {
            if (this.GridData.CurrentRow != null) {
                int index = this.GridData.Rows.IndexOf(this.GridData.CurrentRow);
                if (this.ayuda.Documentos[index].Resultado == Enums.EnumValidateResult.EnEspera)
                    this.ToolBarButtonValidarSeleccionado.PerformClick();
                else if (this.ayuda.Documentos[index].Resultado == Enums.EnumValidateResult.Valido | this.ayuda.Documentos[index].Resultado == Enums.EnumValidateResult.NoValido) {
                    var reporte = new ViewReportes(this.ayuda.Documentos[index].Validacion);
                    reporte.Show();
                }
                else if (this.ayuda.Documentos[index].Resultado == Enums.EnumValidateResult.Error) {
                }
            }
        }

        private void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            Documento fila = e.ParentRow.DataBoundItem as Documento;

            if (e.Template.Caption == this.GridConceptos.Caption) {   // grid de conceptos
                foreach (Entities.Basico.Concepto item in fila.Conceptos) {
                    GridViewRowInfo row = e.Template.Rows.NewRow();
                    row.Cells["ClaveProdServ"].Value = item.ClaveProdServ;
                    row.Cells["NoIdentificacion"].Value = item.NoIdentificacion;
                    row.Cells["Cantidad"].Value = item.Cantidad;
                    row.Cells["ClaveUnidad"].Value = item.ClaveUnidad;
                    row.Cells["Unidad"].Value = item.Unidad;
                    row.Cells["Descripcion"].Value = item.Descripcion;
                    row.Cells["ValorUnitario"].Value = item.ValorUnitario;
                    row.Cells["Importe"].Value = item.Importe;
                    row.Cells["Descuento"].Value = item.Descuento;
                    e.SourceCollection.Add(row);
                }
            }
            else if (e.Template.Caption == this.GridCFDIRelacionado.Caption) {   // grid de CFDI Relacionados al comprobante (clave de tipo de relacion)
                if (fila.CFDIRelacionado != null) {
                    GridViewRowInfo row = e.Template.Rows.NewRow();
                    row.Cells["Clave"].Value = fila.CFDIRelacionado.TipoRelacion.Clave;
                    row.Cells["Name"].Value = fila.CFDIRelacionado.TipoRelacion.Descripcion;
                    e.SourceCollection.Add(row);
                }
            }
            else if (e.Template.Caption == this.GridViewCFDIRelacionadoDocumento.Caption) {   // grid de documentos relacionados 
                GridViewHierarchyRowInfo otro = e.ParentRow.Parent as GridViewHierarchyRowInfo;
                Documento t1 = otro.DataBoundItem as Documento;
                if (t1 != null) {
                    if (t1.CFDIRelacionado != null) {
                        if (t1.CFDIRelacionado.CfdiRelacionado != null) {
                            foreach (ComprobanteCfdiRelacionadosCfdiRelacionado r in t1.CFDIRelacionado.CfdiRelacionado) {
                                GridViewRowInfo row = e.Template.Rows.NewRow();
                                row.Cells["IdDocumento"].Value = r.IdDocumento;
                                row.Cells["Folio"].Value = r.Folio;
                                row.Cells["Serie"].Value = r.Serie;
                                row.Cells["EmisorRfc"].Value = r.RFC;
                                row.Cells["Emisor"].Value = r.Nombre;
                                row.Cells["Total"].Value = r.Total;
                                e.SourceCollection.Add(row);
                            }
                        }
                    }
                }
            }
            else if (e.Template.Caption == this.GridComplementoPagos.Caption) {   // grid de Complemento de Pagos
                if (fila.ComplementoPago != null) {
                    foreach (ComplementoPagosPago item in fila.ComplementoPago.Pago) {
                        GridViewRowInfo row = e.Template.Rows.NewRow();
                        row.Cells["FecPago"].Value = item.FechaPago;
                        row.Cells["FormaDePago"].Value = String.Concat(item.FormaDePagoP.Clave, " ", item.FormaDePagoP.Descripcion);
                        row.Cells["Moneda"].Value = item.MonedaP;
                        row.Cells["TipoCambio"].Value = item.TipoCambioP;
                        row.Cells["CtaBeneficiario"].Value = item.CtaBeneficiario;
                        row.Cells["CtaOrdenante"].Value = item.CtaOrdenante;
                        row.Cells["NomBancoOrdExt"].Value = item.NomBancoOrdExt;
                        row.Cells["NumOperacion"].Value = item.NumOperacion;
                        row.Cells["Monto"].Value = item.Monto;
                        row.Cells["TipoCadPago"].Value = item.TipoCadPago;
                        row.Cells["RfcEmisorCtaBen"].Value = item.RfcEmisorCtaBen;
                        row.Cells["RfcEmisorCtaOrd"].Value = item.RfcEmisorCtaOrd;
                        row.Cells["Data"].Value = item.Json();
                        e.SourceCollection.Add(row);
                    }
                }
            }
            else if (e.Template.Caption == this.GridViewComplementoPagoDoctosRelacionados.Caption) {
                GridViewRowInfo rowView = e.ParentRow;
                if (rowView != null) {
                    ComplementoPagosPago pago = ComplementoPagosPago.Json(rowView.Cells["Data"].Value.ToString());
                    if (pago != null) {
                        if (pago.DoctoRelacionado != null) {
                            foreach (ComplementoPagoDoctoRelacionado doctoRel in pago.DoctoRelacionado) {
                                GridViewRowInfo row = e.Template.Rows.NewRow();
                                row.Cells["Folio"].Value = doctoRel.Folio;
                                row.Cells["Serie"].Value = doctoRel.Serie;
                                row.Cells["Estado"].Value = doctoRel.Estado;
                                row.Cells["IdDocumento"].Value = doctoRel.IdDocumento;
                                row.Cells["Moneda"].Value = doctoRel.Moneda;
                                row.Cells["TipoCambio"].Value = doctoRel.TipoCambio;
                                //row.Cells["MetodoPago"].Value = doctoRel.MetodoPago;
                                row.Cells["NumParcialidad"].Value = doctoRel.NumParcialidad;
                                row.Cells["ImpSaldoAnterior"].Value = doctoRel.ImpSaldoAnt;
                                row.Cells["ImpPagado"].Value = doctoRel.ImpPagado;
                                row.Cells["ImpSaldoInsoluto"].Value = doctoRel.ImpSaldoInsoluto;
                                e.SourceCollection.Add(row);
                            }
                        }
                    }
                }
            }
        }

        private void GridData_CellFormatting(object sender, CellFormattingEventArgs e) {
            if (e.Column.Name == "XML" || e.Column.Name == "PDF") {
                if (e.CellElement.Value.ToString() != "") {
                    if (e.Column.Name == "XML") {
                        e.CellElement.Image = Iconos.Images["XML"];
                        e.CellElement.DrawText = false;
                    }
                    else if (e.Column.Name == "PDF") {
                        e.CellElement.Image = Iconos.Images["PDF"];
                        e.CellElement.DrawText = false;
                    }
                }
            }
            else {
                e.CellElement.Image = null;
                e.CellElement.DrawText = true;
                if (e.Column.Name != "Resultado") {
                    if (e.CellElement.Children.Count > 0)
                        e.CellElement.Children.Clear();
                }
            }
        }

        private void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (e.Column.Name == "XML") {
                if (this.ayuda.Documentos[e.RowIndex].ArchivoXML.FileInfo.Exists) {

                }
            }
            else if (e.Column.Name == "PDF") {
                if (this.ayuda.Documentos[e.RowIndex].ArchivoPDF.FileInfo.Exists) {
                    ViewerPdf pdf = new ViewerPdf(this.ayuda.Documentos[e.RowIndex].ArchivoPDF.FullFileName);
                    pdf.Show();
                }
            }
        }

        private void GridData_RowsChanged(object sender, GridViewCollectionChangedEventArgs e) {
            this.ToolBarButtonRemover.Enabled = this.GridData.RowCount > 0;
            this.ToolBarButtonPDF.Enabled = this.GridData.RowCount > 0;
            this.ToolBarButtonValidar.Enabled = this.GridData.RowCount > 0;
            this.ToolBarButtonLimpiar.Enabled = this.GridData.RowCount > 0;
            this.ToolBarButtonPago.Enabled = this.GridData.RowCount > 0;
            this.ToolBarMenuCrearArchivoZIP.Enabled = this.GridData.RowCount > 0;
        }

        #endregion

        #region preparar formulario

        private void Preparar_DoWork(object sender, DoWorkEventArgs e) {
            this.data = new SqlSugarConfiguracion(ConfigService.Synapsis.RDS.Edita);
            // rutas resientes
            this.carpetas.Load();
            // configuracion del grid
            this.GridData.TelerikGridCommon();
            this.GridConceptos.Standard();
            this.GridConceptos.HierarchyDataProvider = new GridViewEventDataProvider(this.GridConceptos);
            this.GridCFDIRelacionado.Standard();
            this.GridCFDIRelacionado.HierarchyDataProvider = new GridViewEventDataProvider(this.GridCFDIRelacionado);
            this.GridViewCFDIRelacionadoDocumento.Standard();
            this.GridViewCFDIRelacionadoDocumento.HierarchyDataProvider = new GridViewEventDataProvider(this.GridViewCFDIRelacionadoDocumento);
            this.GridComplementoPagos.Standard();
            this.GridComplementoPagos.HierarchyDataProvider = new GridViewEventDataProvider(this.GridComplementoPagos);
            this.GridViewComplementoPagoDoctosRelacionados.Standard();
            this.GridViewComplementoPagoDoctosRelacionados.HierarchyDataProvider = new GridViewEventDataProvider(this.GridViewComplementoPagoDoctosRelacionados);

            this.configuracion = this.data.GetByKey(SqlSugarConfiguracion.KeyConfiguracion.valida6);

            if (this.configuracion == null)
                this.configuracionLocal = new Edita.V2.Validador.Entities.Configuracion();
            else
                this.configuracionLocal = Edita.V2.Validador.Entities.Configuracion.Json(this.configuracion.Data);
            this.ayuda = new HelperValidador(ConfigService.Synapsis, this.configuracionLocal);
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.CboCarpetas.DataSource = carpetas.Items;
            this.CboCarpetas.AutoSizeDropDownToBestFit = true;
            this.ToolBarButtonBakcupAuto.DataBindings.Add("IsChecked", this.ayuda, "AutoBackup", true, DataSourceUpdateMode.OnPropertyChanged);
            this.ToolBarButtonBakcupAuto.ToggleStateChanged += ToolBarButtonBakcupAuto_ToggleStateChanged;
            this.GridFormatosCondicionales();
            this.ToolBarButtonCarpeta.Enabled = true;
        }

        #endregion

        private void Busqueda_Proceso(object sender, Jaeger.Validador.V3.Entities.ProcessChanged e) {
            this.BarraEstadoLabel.Text = e.Caption;
        }

        private void Validacion_Proceso(object sender, Jaeger.Validador.V3.Entities.ProcessChanged e) {
            this.BarraEstadoLabel.Text = e.Caption;
        }

        private void Validacion_Terminada(object sender, Jaeger.Validador.V3.Entities.ProcessCompleted e) {
            this.ayuda.Save();
            this.Espera.StopWaiting();
            this.GridData.DataSource = this.ayuda.Documentos;
            this.ToolBarValidador.Enabled = true;

        }

        public void Backup_Changed(object sender, Jaeger.Validador.V3.Entities.ProcessChanged e) {
            this.BarraEstadoLabel.Text = e.Caption;
        }

        public void Backup_Completed(object sender, Jaeger.Validador.V3.Entities.ProcessCompleted e) {
            this.ayuda.Save();
            this.BarraEstadoLabel.Text = e.Caption;
            this.Espera.StopWaiting();
            this.ToolBarValidador.Enabled = true;
            this.GridData.TableElement.BeginUpdate();
            this.GridData.DataSource = this.ayuda.Documentos;
            this.GridData.TableElement.EndUpdate();
        }

        private void BuscarPDF() {
            if (this.GridData.CurrentRow != null) {
                int index = this.GridData.Rows.IndexOf(this.GridData.CurrentRow);
                if (index > -1) {
                    if (this.ayuda.Documentos[index].ArchivoPDF != null) {
                        if (File.Exists(this.ayuda.Documentos[index].ArchivoPDF.FullFileName)) {
                            if (RadMessageBox.Show(this, string.Concat("Actualmente este comprobante tiene asignado un representación impresa del archivo: ",
                                                            this.ayuda.Documentos[index].ArchivoPDF.FileName,
                                                            ", aún así quieres asignar un nuevo archivo?"), "Informacion", MessageBoxButtons.YesNo, RadMessageIcon.Info) == System.Windows.Forms.DialogResult.No) {
                                return;
                            }
                        }

                        string archivo = this.ayuda.Buscar(this.ayuda.Documentos[index].ArchivoPDF.FullFileName, this.ayuda.Documentos[index].IdDocumento);
                        if (File.Exists(archivo))
                            this.ayuda.Documentos[index].ArchivoPDF = CFDReaderCommon.Load(new FileInfo(archivo));
                    }
                    else {
                        string archivo = this.ayuda.Buscar(this.ayuda.Documentos[index].ArchivoXML.FullFileName, this.ayuda.Documentos[index].IdDocumento);
                        if (File.Exists(archivo))
                            this.ayuda.Documentos[index].ArchivoPDF = CFDReaderCommon.Load(new FileInfo(archivo));
                    }
                }
            }
        }

        /// <summary>
        /// convertir el reporte de validacion en formato PDF
        /// </summary>
        private void PDFValidacion() {
            for (int index = 0; index < this.ayuda.Documentos.Count; index++) {
                var d = new HelperReport2PDF();
                if (this.ayuda.Documentos[index].Resultado != Enums.EnumValidateResult.EnEspera)
                    if (this.ayuda.Documentos[index].Validacion != null)
                        d.Crear(this.ayuda.Documentos[index].Validacion, (string)ToolBarButtonPDFValidacion.Tag);
            }
        }

        private void Crear() {
            for (int i = 0; i < this.ayuda.Documentos.Count; i++) {
                this.ayuda.ExecuteRegistra(i);
                this.ayuda.ExecuteBackUp(i);
            }
        }

        private void ExportarTemplete_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog() { Filter = "*.xlsx|*xlsx", DefaultExt = "xlsx", AddExtension = true, Title = "Selecciona un templete EXCEL" };
            if (openFile.ShowDialog() == DialogResult.OK) {
                if (!File.Exists(openFile.FileName)) {
                    RadMessageBox.Show(this, "El archivo seleccionado no existe", "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                } else {
                    if (Util.Helpers.HelperFiles.IsFileInUse(openFile.FileName)) {
                        RadMessageBox.Show(this, "El archivo seleccionado actualmente se encuentra en uso, no es posible utilizar el templete.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                        return;
                    }
                }
                var saveFile = new SaveFileDialog() { Filter = "*.xlsx|*xlsx", DefaultExt = "xlsx", AddExtension = true, Title = "Nombre del archivo de exportación", FileName = "Reporte_Validacion_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") };
                if (saveFile.ShowDialog() == DialogResult.OK) {
                    //if (Util.Helpers.HelperFiles.IsFileInUse(saveFile.FileName)) {
                    //    RadMessageBox.Show(this, "El archivo seleccionado actualmente se encuentra en uso.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                    //    return;
                    //}
                    this._ExcelTemplete.ExportarExcel(saveFile.FileName, this.ayuda.Documentos, openFile.FileName);
                }
            }
        }

        private void ToolBarButtonVersion69B_Click(object sender, EventArgs e) {
            RadMessageBox.Show(this,
                "Validador de comprobantes: " + this.ayuda.Version + Environment.NewLine +
                "Catálogo Art. 69-B: Información actualizada al 16 de Octubre de 2024." + Environment.NewLine, "Información", MessageBoxButtons.OK, RadMessageIcon.Info);
            //RadMessageBox.Show(this, 
            //    "Validador de comprobantes: " + this.ayuda.Version + Environment.NewLine + 
            //    "Catálogo Art. 69-B: " + ValidadorComprobantes.articulo69B.catalogo.Revision + Environment.NewLine, "Información", MessageBoxButtons.OK, RadMessageIcon.Info);
        }
    }
}
