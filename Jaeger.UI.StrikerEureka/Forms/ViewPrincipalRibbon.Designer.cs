﻿namespace Jaeger.Views
{
    partial class ViewPrincipalRibbon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewPrincipalRibbon));
            this.StatusStrip = new Telerik.WinControls.UI.RadStatusStrip();
            this.SplitButton = new Telerik.WinControls.UI.RadSplitButtonElement();
            this.Waiting = new Telerik.WinControls.UI.RadWaitingBarElement();
            this.RadDock = new Telerik.WinControls.UI.Docking.RadDock();
            this.documentContainer2 = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.radDesktopAlert1 = new Telerik.WinControls.UI.RadDesktopAlert(this.components);
            this.office2010BlackTheme = new Telerik.WinControls.Themes.Office2010BlackTheme();
            this.office2010BlueTheme = new Telerik.WinControls.Themes.Office2010BlueTheme();
            this.office2010SilverTheme = new Telerik.WinControls.Themes.Office2010SilverTheme();
            this.RibbonBar = new Telerik.WinControls.UI.RadRibbonBar();
            this.Tab_Administracion = new Telerik.WinControls.UI.RibbonTab();
            this.Administracion_Grupo_Emision = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.Administracion_Grupo_Emision_Button_Clientes = new Telerik.WinControls.UI.RadButtonElement();
            this.Administracion_Grupo_Emision_Button_Comprobante = new Telerik.WinControls.UI.RadButtonElement();
            this.Administracion_Grupo_Emision_Button_Cobros = new Telerik.WinControls.UI.RadButtonElement();
            this.Administracion_Grupo_Emision_Button_Reportes = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.Grupo_Cliente_Button_EstadoCuenta = new Telerik.WinControls.UI.RadMenuItem();
            this.Grupo_Cliente_Button_EstadoCredito = new Telerik.WinControls.UI.RadMenuItem();
            this.Grupo_Cliente_Button_ReciboFiscal = new Telerik.WinControls.UI.RadMenuItem();
            this.Grupo_Cliente_Button_Resumen = new Telerik.WinControls.UI.RadMenuItem();
            this.Administracion_Grupo_Recepcion = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.Grupo_Recepcion_Button_Expediente = new Telerik.WinControls.UI.RadButtonElement();
            this.Grupo_Recepcion_Button_Validacion = new Telerik.WinControls.UI.RadButtonElement();
            this.Grupo_Recepcion_Button_Comprobante = new Telerik.WinControls.UI.RadButtonElement();
            this.Grupo_Recepcion_Button_Pago = new Telerik.WinControls.UI.RadButtonElement();
            this.Grupo_Recepcion_Button_Reportes = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.Grupo_Recepcion_Button_EstadoCuenta = new Telerik.WinControls.UI.RadMenuItem();
            this.Grupo_Recepcion_Button_Resumen = new Telerik.WinControls.UI.RadMenuItem();
            this.Administracion_Grupo_Nomina = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.Grupo_Nominas_Button_Expediente = new Telerik.WinControls.UI.RadButtonElement();
            this.Grupo_Nominas_Button_Nominas = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.Grupo_Nominas_Button_Calcular = new Telerik.WinControls.UI.RadMenuItem();
            this.Grupo_Nominas_Button_Consulta = new Telerik.WinControls.UI.RadMenuItem();
            this.Grupo_Nominas_Button_ConsultaAguinaldo = new Telerik.WinControls.UI.RadMenuItem();
            this.Grupo_Nominas_Button_Resumen = new Telerik.WinControls.UI.RadMenuItem();
            this.Grupo_Nominas_Button_Layout = new Telerik.WinControls.UI.RadMenuItem();
            this.Grupo_Nominas_Button_Group = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.Grupo_Nominas_Button_Departamentos = new Telerik.WinControls.UI.RadButtonElement();
            this.Grupo_Nominas_Button_Catalogos = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.Grupo_Nominas_Button_Configuracion = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.Administracion_Grupo_ContableE = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.Administracion_Grupo_ContableE_Button_Plantillas = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.Administracion_Grupo_ContableE_Button_LayoutDIOT2019 = new Telerik.WinControls.UI.RadMenuItem();
            this.Administracion_Grupo_ContableE_Button_Convertir = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.Administracion_Grupo_ContableE_Button_DIOT = new Telerik.WinControls.UI.RadMenuItem();
            this.Administracion_Grupo_ContableE_Button_Layout = new Telerik.WinControls.UI.RadMenuItem();
            this.Administracion_Grupo_ContableE_Button_Firmar = new Telerik.WinControls.UI.RadButtonElement();
            this.Tab_Herramientas = new Telerik.WinControls.UI.RibbonTab();
            this.Herramientas_Grupo_Configuracion = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.Herramientas_Grupo_Configuracion_General = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.Herramientas_Grupo_Configuracion_Button_Empresa = new Telerik.WinControls.UI.RadMenuItem();
            this.Herramientas_Grupo_Configuracion_Button_Avanzado = new Telerik.WinControls.UI.RadMenuItem();
            this.Herramientas_Grupo_Configuracion_Button_Usuario = new Telerik.WinControls.UI.RadMenuItem();
            this.Herramientas_Grupo_Configuracion_Button_Perfile = new Telerik.WinControls.UI.RadMenuItem();
            this.Herramientas_Grupo_Configuracion_Button_ArchivoINI = new Telerik.WinControls.UI.RadMenuItem();
            this.Herramientas_Grupo_Configuracion_Button_Mantenimiento = new Telerik.WinControls.UI.RadMenuItem();
            this.Herramientas_Grupo_Configuracion_Button_SeriesFolios = new Telerik.WinControls.UI.RadButtonElement();
            this.Herramientas_Grupo_Herramientas = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Asistida = new Telerik.WinControls.UI.RadMenuItem();
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Repositorio = new Telerik.WinControls.UI.RadMenuItem();
            this.Herramientas_Grupo_Herramientas_Button_Backup = new Telerik.WinControls.UI.RadButtonElement();
            this.Herramientas_Grupo_Herramientas_Button_Mantenimiento = new Telerik.WinControls.UI.RadButtonElement();
            this.Herramientas_Grupo_ListaNegra = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.Herramientas_Grupo_ListaNegra_Button_Listas = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.Herramientas_Grupo_ListaNegra_Button_Cancelados = new Telerik.WinControls.UI.RadMenuItem();
            this.Herramientas_Grupo_Temas = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.Office2010Black = new Telerik.WinControls.UI.RadButtonElement();
            this.Office2010Blue = new Telerik.WinControls.UI.RadButtonElement();
            this.Office2010Silver = new Telerik.WinControls.UI.RadButtonElement();
            ((System.ComponentModel.ISupportInitialize)(this.StatusStrip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDock)).BeginInit();
            this.RadDock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // StatusStrip
            // 
            this.StatusStrip.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.SplitButton,
            this.Waiting});
            this.StatusStrip.Location = new System.Drawing.Point(0, 720);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(1182, 28);
            this.StatusStrip.SizingGrip = false;
            this.StatusStrip.TabIndex = 1;
            // 
            // SplitButton
            // 
            this.SplitButton.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.SplitButton.DefaultItem = null;
            this.SplitButton.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.SplitButton.ExpandArrowButton = false;
            this.SplitButton.Image = null;
            this.SplitButton.Name = "SplitButton";
            this.StatusStrip.SetSpring(this.SplitButton, false);
            this.SplitButton.Text = "XXXXXXXXXXXXXX";
            this.SplitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            // 
            // Waiting
            // 
            this.Waiting.Name = "Waiting";
            this.StatusStrip.SetSpring(this.Waiting, false);
            this.Waiting.Text = "";
            this.Waiting.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // RadDock
            // 
            this.RadDock.AutoDetectMdiChildren = true;
            this.RadDock.Controls.Add(this.documentContainer2);
            this.RadDock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadDock.IsCleanUpTarget = true;
            this.RadDock.Location = new System.Drawing.Point(0, 167);
            this.RadDock.MainDocumentContainer = this.documentContainer2;
            this.RadDock.Name = "RadDock";
            // 
            // 
            // 
            this.RadDock.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.RadDock.Size = new System.Drawing.Size(1182, 553);
            this.RadDock.TabIndex = 4;
            this.RadDock.TabStop = false;
            // 
            // documentContainer2
            // 
            this.documentContainer2.Name = "documentContainer2";
            // 
            // 
            // 
            this.documentContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.documentContainer2.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            // 
            // RibbonBar
            // 
            this.RibbonBar.CommandTabs.AddRange(new Telerik.WinControls.RadItem[] {
            this.Tab_Administracion,
            this.Tab_Herramientas});
            // 
            // 
            // 
            this.RibbonBar.ExitButton.Text = "Exit";
            this.RibbonBar.ExitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.RibbonBar.Location = new System.Drawing.Point(0, 0);
            this.RibbonBar.Name = "RibbonBar";
            // 
            // 
            // 
            this.RibbonBar.OptionsButton.Text = "Options";
            this.RibbonBar.OptionsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // 
            // 
            this.RibbonBar.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.RibbonBar.Size = new System.Drawing.Size(1182, 167);
            this.RibbonBar.StartButtonImage = ((System.Drawing.Image)(resources.GetObject("RibbonBar.StartButtonImage")));
            this.RibbonBar.TabIndex = 0;
            this.RibbonBar.Text = "ViewPrincipalRibbon";
            // 
            // Tab_Administracion
            // 
            this.Tab_Administracion.IsSelected = true;
            this.Tab_Administracion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Administracion_Grupo_Emision,
            this.Administracion_Grupo_Recepcion,
            this.Administracion_Grupo_Nomina,
            this.Administracion_Grupo_ContableE});
            this.Tab_Administracion.Name = "Tab_Administracion";
            this.Tab_Administracion.Text = "Administración";
            this.Tab_Administracion.UseMnemonic = false;
            // 
            // Administracion_Grupo_Emision
            // 
            this.Administracion_Grupo_Emision.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Administracion_Grupo_Emision_Button_Clientes,
            this.Administracion_Grupo_Emision_Button_Comprobante,
            this.Administracion_Grupo_Emision_Button_Cobros,
            this.Administracion_Grupo_Emision_Button_Reportes});
            this.Administracion_Grupo_Emision.Name = "Administracion_Grupo_Emision";
            this.Administracion_Grupo_Emision.Text = "Emisión";
            // 
            // Administracion_Grupo_Emision_Button_Clientes
            // 
            this.Administracion_Grupo_Emision_Button_Clientes.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_conference_call;
            this.Administracion_Grupo_Emision_Button_Clientes.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Administracion_Grupo_Emision_Button_Clientes.Name = "Administracion_Grupo_Emision_Button_Clientes";
            this.Administracion_Grupo_Emision_Button_Clientes.Text = "Clientes";
            this.Administracion_Grupo_Emision_Button_Clientes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Administracion_Grupo_Emision_Button_Clientes.ToolTipText = "Catálogo de clientes";
            this.Administracion_Grupo_Emision_Button_Clientes.Click += new System.EventHandler(this.Administracion_Grupo_Emision_Button_Clientes_Click);
            // 
            // Administracion_Grupo_Emision_Button_Comprobante
            // 
            this.Administracion_Grupo_Emision_Button_Comprobante.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_factura;
            this.Administracion_Grupo_Emision_Button_Comprobante.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Administracion_Grupo_Emision_Button_Comprobante.Name = "Administracion_Grupo_Emision_Button_Comprobante";
            this.Administracion_Grupo_Emision_Button_Comprobante.SmallImage = global::Jaeger.UI.Properties.Resources.icons8_x16_factura;
            this.Administracion_Grupo_Emision_Button_Comprobante.Text = "Facturación";
            this.Administracion_Grupo_Emision_Button_Comprobante.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Administracion_Grupo_Emision_Button_Comprobante.Click += new System.EventHandler(this.Administracion_Grupo_Emision_Button_Comprobante_Click);
            // 
            // Administracion_Grupo_Emision_Button_Cobros
            // 
            this.Administracion_Grupo_Emision_Button_Cobros.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_cash_register;
            this.Administracion_Grupo_Emision_Button_Cobros.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Administracion_Grupo_Emision_Button_Cobros.Name = "Administracion_Grupo_Emision_Button_Cobros";
            this.Administracion_Grupo_Emision_Button_Cobros.Text = "Cobros";
            this.Administracion_Grupo_Emision_Button_Cobros.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Administracion_Grupo_Emision_Button_Cobros.Click += new System.EventHandler(this.Administracion_Grupo_Emision_Button_Cobros_Click);
            // 
            // Administracion_Grupo_Emision_Button_Reportes
            // 
            this.Administracion_Grupo_Emision_Button_Reportes.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.Administracion_Grupo_Emision_Button_Reportes.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.Administracion_Grupo_Emision_Button_Reportes.ExpandArrowButton = false;
            this.Administracion_Grupo_Emision_Button_Reportes.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_check;
            this.Administracion_Grupo_Emision_Button_Reportes.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Administracion_Grupo_Emision_Button_Reportes.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Grupo_Cliente_Button_EstadoCuenta,
            this.Grupo_Cliente_Button_EstadoCredito,
            this.Grupo_Cliente_Button_ReciboFiscal,
            this.Grupo_Cliente_Button_Resumen});
            this.Administracion_Grupo_Emision_Button_Reportes.Name = "Administracion_Grupo_Emision_Button_Reportes";
            this.Administracion_Grupo_Emision_Button_Reportes.Text = "Reportes";
            this.Administracion_Grupo_Emision_Button_Reportes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // Grupo_Cliente_Button_EstadoCuenta
            // 
            this.Grupo_Cliente_Button_EstadoCuenta.Name = "Grupo_Cliente_Button_EstadoCuenta";
            this.Grupo_Cliente_Button_EstadoCuenta.Text = "Estado de Cuenta";
            this.Grupo_Cliente_Button_EstadoCuenta.Click += new System.EventHandler(this.Grupo_Cliente_Button_EstadoCuenta_Click);
            // 
            // Grupo_Cliente_Button_EstadoCredito
            // 
            this.Grupo_Cliente_Button_EstadoCredito.Name = "Grupo_Cliente_Button_EstadoCredito";
            this.Grupo_Cliente_Button_EstadoCredito.Text = "Creditos";
            this.Grupo_Cliente_Button_EstadoCredito.Click += new System.EventHandler(this.Grupo_Cliente_Button_EstadoCredito_Click);
            // 
            // Grupo_Cliente_Button_ReciboFiscal
            // 
            this.Grupo_Cliente_Button_ReciboFiscal.Name = "Grupo_Cliente_Button_ReciboFiscal";
            this.Grupo_Cliente_Button_ReciboFiscal.Text = "Recibos Fiscales";
            this.Grupo_Cliente_Button_ReciboFiscal.Click += new System.EventHandler(this.Grupo_Cliente_Button_ReciboFiscal_Click);
            // 
            // Grupo_Cliente_Button_Resumen
            // 
            this.Grupo_Cliente_Button_Resumen.Name = "Grupo_Cliente_Button_Resumen";
            this.Grupo_Cliente_Button_Resumen.Text = "Resumen";
            this.Grupo_Cliente_Button_Resumen.Click += new System.EventHandler(this.Grupo_Cliente_Button_Resumen_Click);
            // 
            // Administracion_Grupo_Recepcion
            // 
            this.Administracion_Grupo_Recepcion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Grupo_Recepcion_Button_Expediente,
            this.Grupo_Recepcion_Button_Validacion,
            this.Grupo_Recepcion_Button_Comprobante,
            this.Grupo_Recepcion_Button_Pago,
            this.Grupo_Recepcion_Button_Reportes});
            this.Administracion_Grupo_Recepcion.Name = "Administracion_Grupo_Recepcion";
            this.Administracion_Grupo_Recepcion.Text = "Recepción";
            // 
            // Grupo_Recepcion_Button_Expediente
            // 
            this.Grupo_Recepcion_Button_Expediente.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_conference_call;
            this.Grupo_Recepcion_Button_Expediente.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Grupo_Recepcion_Button_Expediente.Name = "Grupo_Recepcion_Button_Expediente";
            this.Grupo_Recepcion_Button_Expediente.Text = "Proveedores";
            this.Grupo_Recepcion_Button_Expediente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Grupo_Recepcion_Button_Expediente.ToolTipText = "Catálogo de proveedores";
            this.Grupo_Recepcion_Button_Expediente.Click += new System.EventHandler(this.Grupo_Recepcion_Button_Expediente_Click);
            // 
            // Grupo_Recepcion_Button_Validacion
            // 
            this.Grupo_Recepcion_Button_Validacion.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_seguridad_comprobado;
            this.Grupo_Recepcion_Button_Validacion.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Grupo_Recepcion_Button_Validacion.Name = "Grupo_Recepcion_Button_Validacion";
            this.Grupo_Recepcion_Button_Validacion.Text = "Validar";
            this.Grupo_Recepcion_Button_Validacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Grupo_Recepcion_Button_Validacion.ToolTipText = "Validador de comprobantes fiscales";
            this.Grupo_Recepcion_Button_Validacion.Click += new System.EventHandler(this.Grupo_Recepcion_Button_Validacion_Click);
            // 
            // Grupo_Recepcion_Button_Comprobante
            // 
            this.Grupo_Recepcion_Button_Comprobante.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_factura;
            this.Grupo_Recepcion_Button_Comprobante.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Grupo_Recepcion_Button_Comprobante.Name = "Grupo_Recepcion_Button_Comprobante";
            this.Grupo_Recepcion_Button_Comprobante.Text = "Facturación";
            this.Grupo_Recepcion_Button_Comprobante.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Grupo_Recepcion_Button_Comprobante.Click += new System.EventHandler(this.Grupo_Recepcion_Button_Comprobante_Click);
            // 
            // Grupo_Recepcion_Button_Pago
            // 
            this.Grupo_Recepcion_Button_Pago.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_chequera;
            this.Grupo_Recepcion_Button_Pago.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Grupo_Recepcion_Button_Pago.Name = "Grupo_Recepcion_Button_Pago";
            this.Grupo_Recepcion_Button_Pago.Text = "Pagos";
            this.Grupo_Recepcion_Button_Pago.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Grupo_Recepcion_Button_Pago.Click += new System.EventHandler(this.Grupo_Recepcion_Button_Pago_Click);
            // 
            // Grupo_Recepcion_Button_Reportes
            // 
            this.Grupo_Recepcion_Button_Reportes.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.Grupo_Recepcion_Button_Reportes.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.Grupo_Recepcion_Button_Reportes.ExpandArrowButton = false;
            this.Grupo_Recepcion_Button_Reportes.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_check;
            this.Grupo_Recepcion_Button_Reportes.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Grupo_Recepcion_Button_Reportes.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Grupo_Recepcion_Button_EstadoCuenta,
            this.Grupo_Recepcion_Button_Resumen});
            this.Grupo_Recepcion_Button_Reportes.Name = "Grupo_Recepcion_Button_Reportes";
            this.Grupo_Recepcion_Button_Reportes.Text = "Reportes";
            this.Grupo_Recepcion_Button_Reportes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // Grupo_Recepcion_Button_EstadoCuenta
            // 
            this.Grupo_Recepcion_Button_EstadoCuenta.Name = "Grupo_Recepcion_Button_EstadoCuenta";
            this.Grupo_Recepcion_Button_EstadoCuenta.Text = "Estado de Cuenta";
            this.Grupo_Recepcion_Button_EstadoCuenta.Click += new System.EventHandler(this.Grupo_Recepcion_Button_EstadoCuenta_Click);
            // 
            // Grupo_Recepcion_Button_Resumen
            // 
            this.Grupo_Recepcion_Button_Resumen.Name = "Grupo_Recepcion_Button_Resumen";
            this.Grupo_Recepcion_Button_Resumen.Text = "Resumen";
            this.Grupo_Recepcion_Button_Resumen.Click += new System.EventHandler(this.Grupo_Recepcion_Button_Resumen_Click);
            // 
            // Administracion_Grupo_Nomina
            // 
            this.Administracion_Grupo_Nomina.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Grupo_Nominas_Button_Expediente,
            this.Grupo_Nominas_Button_Nominas,
            this.Grupo_Nominas_Button_Group});
            this.Administracion_Grupo_Nomina.Name = "Administracion_Grupo_Nomina";
            this.Administracion_Grupo_Nomina.Text = "Nóminas";
            // 
            // Grupo_Nominas_Button_Expediente
            // 
            this.Grupo_Nominas_Button_Expediente.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_user_group_man_woman;
            this.Grupo_Nominas_Button_Expediente.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Grupo_Nominas_Button_Expediente.Name = "Grupo_Nominas_Button_Expediente";
            this.Grupo_Nominas_Button_Expediente.Text = "Empleados";
            this.Grupo_Nominas_Button_Expediente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Grupo_Nominas_Button_Expediente.ToolTipText = "Catálogo de Empleados";
            this.Grupo_Nominas_Button_Expediente.Click += new System.EventHandler(this.Grupo_Nominas_Button_Expediente_Click);
            // 
            // Grupo_Nominas_Button_Nominas
            // 
            this.Grupo_Nominas_Button_Nominas.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.Grupo_Nominas_Button_Nominas.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.Grupo_Nominas_Button_Nominas.ExpandArrowButton = false;
            this.Grupo_Nominas_Button_Nominas.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_resume;
            this.Grupo_Nominas_Button_Nominas.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Grupo_Nominas_Button_Nominas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Grupo_Nominas_Button_Calcular,
            this.Grupo_Nominas_Button_Consulta,
            this.Grupo_Nominas_Button_ConsultaAguinaldo,
            this.Grupo_Nominas_Button_Resumen,
            this.Grupo_Nominas_Button_Layout});
            this.Grupo_Nominas_Button_Nominas.Name = "Grupo_Nominas_Button_Nominas";
            this.Grupo_Nominas_Button_Nominas.Text = "Nóminas";
            this.Grupo_Nominas_Button_Nominas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // Grupo_Nominas_Button_Calcular
            // 
            this.Grupo_Nominas_Button_Calcular.Name = "Grupo_Nominas_Button_Calcular";
            this.Grupo_Nominas_Button_Calcular.Text = "Calcular";
            this.Grupo_Nominas_Button_Calcular.Click += new System.EventHandler(this.Grupo_Nominas_Button_Calcular_Click);
            // 
            // Grupo_Nominas_Button_Consulta
            // 
            this.Grupo_Nominas_Button_Consulta.Name = "Grupo_Nominas_Button_Consulta";
            this.Grupo_Nominas_Button_Consulta.Text = "Consulta";
            this.Grupo_Nominas_Button_Consulta.Click += new System.EventHandler(this.Grupo_Nominas_Button_Consulta_Click);
            // 
            // Grupo_Nominas_Button_ConsultaAguinaldo
            // 
            this.Grupo_Nominas_Button_ConsultaAguinaldo.Name = "Grupo_Nominas_Button_ConsultaAguinaldo";
            this.Grupo_Nominas_Button_ConsultaAguinaldo.Text = "Aguinaldo";
            this.Grupo_Nominas_Button_ConsultaAguinaldo.Click += new System.EventHandler(this.Grupo_Nominas_Button_ConsultaAguinaldo_Click);
            // 
            // Grupo_Nominas_Button_Resumen
            // 
            this.Grupo_Nominas_Button_Resumen.Name = "Grupo_Nominas_Button_Resumen";
            this.Grupo_Nominas_Button_Resumen.Text = "Resumen";
            this.Grupo_Nominas_Button_Resumen.Click += new System.EventHandler(this.Grupo_Nominas_Button_Resumen_Click);
            // 
            // Grupo_Nominas_Button_Layout
            // 
            this.Grupo_Nominas_Button_Layout.Name = "Grupo_Nominas_Button_Layout";
            this.Grupo_Nominas_Button_Layout.Text = "Layout";
            this.Grupo_Nominas_Button_Layout.Click += new System.EventHandler(this.Grupo_Nominas_Button_Layout_Click);
            // 
            // Grupo_Nominas_Button_Group
            // 
            this.Grupo_Nominas_Button_Group.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Grupo_Nominas_Button_Departamentos,
            this.Grupo_Nominas_Button_Catalogos,
            this.Grupo_Nominas_Button_Configuracion});
            this.Grupo_Nominas_Button_Group.Name = "Grupo_Nominas_Button_Group";
            this.Grupo_Nominas_Button_Group.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.Grupo_Nominas_Button_Group.Padding = new System.Windows.Forms.Padding(2);
            this.Grupo_Nominas_Button_Group.Text = "Configuraciones";
            // 
            // Grupo_Nominas_Button_Departamentos
            // 
            this.Grupo_Nominas_Button_Departamentos.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_select_users;
            this.Grupo_Nominas_Button_Departamentos.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Grupo_Nominas_Button_Departamentos.Name = "Grupo_Nominas_Button_Departamentos";
            this.Grupo_Nominas_Button_Departamentos.Padding = new System.Windows.Forms.Padding(2);
            this.Grupo_Nominas_Button_Departamentos.ShowBorder = false;
            this.Grupo_Nominas_Button_Departamentos.Text = "Departamentos";
            this.Grupo_Nominas_Button_Departamentos.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.Grupo_Nominas_Button_Departamentos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Grupo_Nominas_Button_Catalogos
            // 
            this.Grupo_Nominas_Button_Catalogos.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.Grupo_Nominas_Button_Catalogos.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.Grupo_Nominas_Button_Catalogos.ExpandArrowButton = false;
            this.Grupo_Nominas_Button_Catalogos.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_encuadernador;
            this.Grupo_Nominas_Button_Catalogos.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Grupo_Nominas_Button_Catalogos.Name = "Grupo_Nominas_Button_Catalogos";
            this.Grupo_Nominas_Button_Catalogos.Padding = new System.Windows.Forms.Padding(2);
            this.Grupo_Nominas_Button_Catalogos.Text = "Catálogos";
            this.Grupo_Nominas_Button_Catalogos.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.Grupo_Nominas_Button_Catalogos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Grupo_Nominas_Button_Configuracion
            // 
            this.Grupo_Nominas_Button_Configuracion.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.Grupo_Nominas_Button_Configuracion.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.Grupo_Nominas_Button_Configuracion.ExpandArrowButton = false;
            this.Grupo_Nominas_Button_Configuracion.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_configuracion;
            this.Grupo_Nominas_Button_Configuracion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Grupo_Nominas_Button_Configuracion.Name = "Grupo_Nominas_Button_Configuracion";
            this.Grupo_Nominas_Button_Configuracion.Padding = new System.Windows.Forms.Padding(2);
            this.Grupo_Nominas_Button_Configuracion.Text = "Configuración";
            this.Grupo_Nominas_Button_Configuracion.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.Grupo_Nominas_Button_Configuracion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Administracion_Grupo_ContableE
            // 
            this.Administracion_Grupo_ContableE.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Administracion_Grupo_ContableE_Button_Plantillas,
            this.Administracion_Grupo_ContableE_Button_Convertir,
            this.Administracion_Grupo_ContableE_Button_Firmar});
            this.Administracion_Grupo_ContableE.Name = "Administracion_Grupo_ContableE";
            this.Administracion_Grupo_ContableE.Text = "Contabilidad Electrónica";
            // 
            // Administracion_Grupo_ContableE_Button_Plantillas
            // 
            this.Administracion_Grupo_ContableE_Button_Plantillas.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.Administracion_Grupo_ContableE_Button_Plantillas.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.Administracion_Grupo_ContableE_Button_Plantillas.ExpandArrowButton = false;
            this.Administracion_Grupo_ContableE_Button_Plantillas.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_excel;
            this.Administracion_Grupo_ContableE_Button_Plantillas.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Administracion_Grupo_ContableE_Button_Plantillas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Administracion_Grupo_ContableE_Button_LayoutDIOT2019});
            this.Administracion_Grupo_ContableE_Button_Plantillas.Name = "Administracion_Grupo_ContableE_Button_Plantillas";
            this.Administracion_Grupo_ContableE_Button_Plantillas.Text = "Plantillas";
            this.Administracion_Grupo_ContableE_Button_Plantillas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // Administracion_Grupo_ContableE_Button_LayoutDIOT2019
            // 
            this.Administracion_Grupo_ContableE_Button_LayoutDIOT2019.Name = "Administracion_Grupo_ContableE_Button_LayoutDIOT2019";
            this.Administracion_Grupo_ContableE_Button_LayoutDIOT2019.Text = "DIOT 2019";
            this.Administracion_Grupo_ContableE_Button_LayoutDIOT2019.Click += new System.EventHandler(this.Administracion_Grupo_ContableE_Button_LayoutDIOT2019_Click);
            // 
            // Administracion_Grupo_ContableE_Button_Convertir
            // 
            this.Administracion_Grupo_ContableE_Button_Convertir.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.Administracion_Grupo_ContableE_Button_Convertir.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.Administracion_Grupo_ContableE_Button_Convertir.ExpandArrowButton = false;
            this.Administracion_Grupo_ContableE_Button_Convertir.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_txt;
            this.Administracion_Grupo_ContableE_Button_Convertir.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Administracion_Grupo_ContableE_Button_Convertir.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Administracion_Grupo_ContableE_Button_DIOT,
            this.Administracion_Grupo_ContableE_Button_Layout});
            this.Administracion_Grupo_ContableE_Button_Convertir.Name = "Administracion_Grupo_ContableE_Button_Convertir";
            this.Administracion_Grupo_ContableE_Button_Convertir.Text = "Convertir";
            this.Administracion_Grupo_ContableE_Button_Convertir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // Administracion_Grupo_ContableE_Button_DIOT
            // 
            this.Administracion_Grupo_ContableE_Button_DIOT.Name = "Administracion_Grupo_ContableE_Button_DIOT";
            this.Administracion_Grupo_ContableE_Button_DIOT.Text = "Excel - DIOT";
            this.Administracion_Grupo_ContableE_Button_DIOT.ToolTipText = "Convertir plantilla DIOT-Excel a archivo TXT";
            this.Administracion_Grupo_ContableE_Button_DIOT.Click += new System.EventHandler(this.Administracion_Grupo_ContableE_Button_DIOT_Click);
            // 
            // Administracion_Grupo_ContableE_Button_Layout
            // 
            this.Administracion_Grupo_ContableE_Button_Layout.Name = "Administracion_Grupo_ContableE_Button_Layout";
            this.Administracion_Grupo_ContableE_Button_Layout.Text = "Convertor Plantilla ContaE";
            this.Administracion_Grupo_ContableE_Button_Layout.Click += new System.EventHandler(this.Administracion_Grupo_ContableE_Button_Layout_Click);
            // 
            // Administracion_Grupo_ContableE_Button_Firmar
            // 
            this.Administracion_Grupo_ContableE_Button_Firmar.Enabled = false;
            this.Administracion_Grupo_ContableE_Button_Firmar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_diploma1;
            this.Administracion_Grupo_ContableE_Button_Firmar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Administracion_Grupo_ContableE_Button_Firmar.Name = "Administracion_Grupo_ContableE_Button_Firmar";
            this.Administracion_Grupo_ContableE_Button_Firmar.Text = "Firmar";
            this.Administracion_Grupo_ContableE_Button_Firmar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // Tab_Herramientas
            // 
            this.Tab_Herramientas.IsSelected = false;
            this.Tab_Herramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Herramientas_Grupo_Configuracion,
            this.Herramientas_Grupo_Herramientas,
            this.Herramientas_Grupo_ListaNegra,
            this.Herramientas_Grupo_Temas});
            this.Tab_Herramientas.Name = "Tab_Herramientas";
            this.Tab_Herramientas.Text = "Herramientas";
            this.Tab_Herramientas.UseMnemonic = false;
            // 
            // Herramientas_Grupo_Configuracion
            // 
            this.Herramientas_Grupo_Configuracion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Herramientas_Grupo_Configuracion_General,
            this.Herramientas_Grupo_Configuracion_Button_SeriesFolios});
            this.Herramientas_Grupo_Configuracion.Name = "Herramientas_Grupo_Configuracion";
            this.Herramientas_Grupo_Configuracion.Text = "Configuración";
            // 
            // Herramientas_Grupo_Configuracion_General
            // 
            this.Herramientas_Grupo_Configuracion_General.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.Herramientas_Grupo_Configuracion_General.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.Herramientas_Grupo_Configuracion_General.ExpandArrowButton = false;
            this.Herramientas_Grupo_Configuracion_General.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_organización;
            this.Herramientas_Grupo_Configuracion_General.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Herramientas_Grupo_Configuracion_General.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Herramientas_Grupo_Configuracion_Button_Empresa,
            this.Herramientas_Grupo_Configuracion_Button_Avanzado,
            this.Herramientas_Grupo_Configuracion_Button_Usuario,
            this.Herramientas_Grupo_Configuracion_Button_Perfile,
            this.Herramientas_Grupo_Configuracion_Button_ArchivoINI,
            this.Herramientas_Grupo_Configuracion_Button_Mantenimiento});
            this.Herramientas_Grupo_Configuracion_General.Name = "Herramientas_Grupo_Configuracion_General";
            this.Herramientas_Grupo_Configuracion_General.Text = "Empresa";
            this.Herramientas_Grupo_Configuracion_General.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // Herramientas_Grupo_Configuracion_Button_Empresa
            // 
            this.Herramientas_Grupo_Configuracion_Button_Empresa.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_organizacion;
            this.Herramientas_Grupo_Configuracion_Button_Empresa.Name = "Herramientas_Grupo_Configuracion_Button_Empresa";
            this.Herramientas_Grupo_Configuracion_Button_Empresa.Text = "General";
            // 
            // Herramientas_Grupo_Configuracion_Button_Avanzado
            // 
            this.Herramientas_Grupo_Configuracion_Button_Avanzado.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_configuracion;
            this.Herramientas_Grupo_Configuracion_Button_Avanzado.Name = "Herramientas_Grupo_Configuracion_Button_Avanzado";
            this.Herramientas_Grupo_Configuracion_Button_Avanzado.Text = "Avanzado";
            this.Herramientas_Grupo_Configuracion_Button_Avanzado.Click += new System.EventHandler(this.Herramientas_Grupo_Configuracion_Button_Avanzado_Click);
            // 
            // Herramientas_Grupo_Configuracion_Button_Usuario
            // 
            this.Herramientas_Grupo_Configuracion_Button_Usuario.Name = "Herramientas_Grupo_Configuracion_Button_Usuario";
            this.Herramientas_Grupo_Configuracion_Button_Usuario.Text = "Usuarios";
            this.Herramientas_Grupo_Configuracion_Button_Usuario.Click += new System.EventHandler(this.Herramientas_Grupo_Configuracion_Button_Usuario_Click);
            // 
            // Herramientas_Grupo_Configuracion_Button_Perfile
            // 
            this.Herramientas_Grupo_Configuracion_Button_Perfile.Name = "Herramientas_Grupo_Configuracion_Button_Perfile";
            this.Herramientas_Grupo_Configuracion_Button_Perfile.Text = "Perfiles";
            this.Herramientas_Grupo_Configuracion_Button_Perfile.Click += new System.EventHandler(this.Herramientas_Grupo_Configuracion_Button_Perfiles_Click);
            // 
            // Herramientas_Grupo_Configuracion_Button_ArchivoINI
            // 
            this.Herramientas_Grupo_Configuracion_Button_ArchivoINI.Name = "Herramientas_Grupo_Configuracion_Button_ArchivoINI";
            this.Herramientas_Grupo_Configuracion_Button_ArchivoINI.Text = "Archivo INI";
            this.Herramientas_Grupo_Configuracion_Button_ArchivoINI.Click += new System.EventHandler(this.Herramientas_Grupo_Configuracion_Button_Config_Click);
            // 
            // Herramientas_Grupo_Configuracion_Button_Mantenimiento
            // 
            this.Herramientas_Grupo_Configuracion_Button_Mantenimiento.Name = "Herramientas_Grupo_Configuracion_Button_Mantenimiento";
            this.Herramientas_Grupo_Configuracion_Button_Mantenimiento.Text = "Mantenimiento";
            this.Herramientas_Grupo_Configuracion_Button_Mantenimiento.Click += new System.EventHandler(this.Herramientas_Grupo_Configuracion_Button_Mantenimiento_Click);
            // 
            // Herramientas_Grupo_Configuracion_Button_SeriesFolios
            // 
            this.Herramientas_Grupo_Configuracion_Button_SeriesFolios.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_contador;
            this.Herramientas_Grupo_Configuracion_Button_SeriesFolios.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Herramientas_Grupo_Configuracion_Button_SeriesFolios.Name = "Herramientas_Grupo_Configuracion_Button_SeriesFolios";
            this.Herramientas_Grupo_Configuracion_Button_SeriesFolios.Text = "Series y Folios";
            this.Herramientas_Grupo_Configuracion_Button_SeriesFolios.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Herramientas_Grupo_Configuracion_Button_SeriesFolios.ToolTipText = "Configuración de Series y Folios";
            this.Herramientas_Grupo_Configuracion_Button_SeriesFolios.Click += new System.EventHandler(this.Herramientas_Grupo_Configuracion_Button_SeriesFolios_Click);
            // 
            // Herramientas_Grupo_Herramientas
            // 
            this.Herramientas_Grupo_Herramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva,
            this.Herramientas_Grupo_Herramientas_Button_Backup,
            this.Herramientas_Grupo_Herramientas_Button_Mantenimiento});
            this.Herramientas_Grupo_Herramientas.Name = "Herramientas_Grupo_Herramientas";
            this.Herramientas_Grupo_Herramientas.Text = "Herramientas";
            // 
            // Herramientas_Grupo_Herramientas_Button_DescargaMasiva
            // 
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva.ExpandArrowButton = false;
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_descargar_desde_nube;
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Asistida,
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Repositorio});
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva.Name = "Herramientas_Grupo_Herramientas_Button_DescargaMasiva";
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva.Text = "Descarga\r\nMasiva";
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Asistida
            // 
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Asistida.Name = "Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Asistida";
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Asistida.Text = "Descarga Asistida";
            // 
            // Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Repositorio
            // 
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Repositorio.Name = "Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Repositorio";
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Repositorio.Text = "Repositorio";
            this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Repositorio.Click += new System.EventHandler(this.Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Repositorio_Click);
            // 
            // Herramientas_Grupo_Herramientas_Button_Backup
            // 
            this.Herramientas_Grupo_Herramientas_Button_Backup.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_network_drive;
            this.Herramientas_Grupo_Herramientas_Button_Backup.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Herramientas_Grupo_Herramientas_Button_Backup.Name = "Herramientas_Grupo_Herramientas_Button_Backup";
            this.Herramientas_Grupo_Herramientas_Button_Backup.Text = "Backup";
            this.Herramientas_Grupo_Herramientas_Button_Backup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Herramientas_Grupo_Herramientas_Button_Backup.Click += new System.EventHandler(this.Herramientas_Grupo_Herramientas_Button_Backup_Click);
            // 
            // Herramientas_Grupo_Herramientas_Button_Mantenimiento
            // 
            this.Herramientas_Grupo_Herramientas_Button_Mantenimiento.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_mantenimiento;
            this.Herramientas_Grupo_Herramientas_Button_Mantenimiento.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Herramientas_Grupo_Herramientas_Button_Mantenimiento.Name = "Herramientas_Grupo_Herramientas_Button_Mantenimiento";
            this.Herramientas_Grupo_Herramientas_Button_Mantenimiento.Text = "Mantenimiento";
            this.Herramientas_Grupo_Herramientas_Button_Mantenimiento.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Herramientas_Grupo_Herramientas_Button_Mantenimiento.Click += new System.EventHandler(this.Herramientas_Grupo_Herramientas_Button_Mantenimiento_Click);
            // 
            // Herramientas_Grupo_ListaNegra
            // 
            this.Herramientas_Grupo_ListaNegra.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Herramientas_Grupo_ListaNegra_Button_Listas});
            this.Herramientas_Grupo_ListaNegra.Name = "Herramientas_Grupo_ListaNegra";
            this.Herramientas_Grupo_ListaNegra.Text = "Lista Negra";
            // 
            // Herramientas_Grupo_ListaNegra_Button_Listas
            // 
            this.Herramientas_Grupo_ListaNegra_Button_Listas.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.Herramientas_Grupo_ListaNegra_Button_Listas.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.Herramientas_Grupo_ListaNegra_Button_Listas.ExpandArrowButton = false;
            this.Herramientas_Grupo_ListaNegra_Button_Listas.Image = global::Jaeger.UI.Properties.Resources.icons8_x30_escudo_de_restricción;
            this.Herramientas_Grupo_ListaNegra_Button_Listas.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Herramientas_Grupo_ListaNegra_Button_Listas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Herramientas_Grupo_ListaNegra_Button_Cancelados});
            this.Herramientas_Grupo_ListaNegra_Button_Listas.Name = "Herramientas_Grupo_ListaNegra_Button_Listas";
            this.Herramientas_Grupo_ListaNegra_Button_Listas.Text = "Listas";
            this.Herramientas_Grupo_ListaNegra_Button_Listas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // Herramientas_Grupo_ListaNegra_Button_Cancelados
            // 
            this.Herramientas_Grupo_ListaNegra_Button_Cancelados.Name = "Herramientas_Grupo_ListaNegra_Button_Cancelados";
            this.Herramientas_Grupo_ListaNegra_Button_Cancelados.Text = "Cancelados";
            this.Herramientas_Grupo_ListaNegra_Button_Cancelados.Click += new System.EventHandler(this.Herramientas_Grupo_ListaNegra_Button_Cancelados_Click);
            // 
            // Herramientas_Grupo_Temas
            // 
            this.Herramientas_Grupo_Temas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Office2010Black,
            this.Office2010Blue,
            this.Office2010Silver});
            this.Herramientas_Grupo_Temas.Name = "Herramientas_Grupo_Temas";
            this.Herramientas_Grupo_Temas.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.Herramientas_Grupo_Temas.Text = "Office 2010 Themes";
            // 
            // Office2010Black
            // 
            this.Office2010Black.Image = ((System.Drawing.Image)(resources.GetObject("Office2010Black.Image")));
            this.Office2010Black.Name = "Office2010Black";
            this.Office2010Black.Padding = new System.Windows.Forms.Padding(2);
            this.Office2010Black.Text = "Office 2010 Black";
            this.Office2010Black.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Office2010Black.TextOrientation = System.Windows.Forms.Orientation.Horizontal;
            this.Office2010Black.Click += new System.EventHandler(this.Office2010Black_Click);
            // 
            // Office2010Blue
            // 
            this.Office2010Blue.Image = ((System.Drawing.Image)(resources.GetObject("Office2010Blue.Image")));
            this.Office2010Blue.Name = "Office2010Blue";
            this.Office2010Blue.Padding = new System.Windows.Forms.Padding(2);
            this.Office2010Blue.Text = "Office 2010 Blue";
            this.Office2010Blue.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Office2010Blue.Click += new System.EventHandler(this.Office2010Blue_Click);
            // 
            // Office2010Silver
            // 
            this.Office2010Silver.Image = ((System.Drawing.Image)(resources.GetObject("Office2010Silver.Image")));
            this.Office2010Silver.Name = "Office2010Silver";
            this.Office2010Silver.Padding = new System.Windows.Forms.Padding(2);
            this.Office2010Silver.Text = "Office 2010 Silver";
            this.Office2010Silver.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Office2010Silver.Click += new System.EventHandler(this.Office2010Silver_Click);
            // 
            // ViewPrincipalRibbon
            // 
            this.AllowAero = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 748);
            this.Controls.Add(this.RadDock);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.RibbonBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = null;
            this.Name = "ViewPrincipalRibbon";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ViewPrincipalRibbon";
            this.ThemeName = "Office2010Black";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ViewPrincipalRibbon_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StatusStrip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDock)).EndInit();
            this.RadDock.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadRibbonBar RibbonBar;
        private Telerik.WinControls.UI.RadStatusStrip StatusStrip;
        private Telerik.WinControls.UI.RadSplitButtonElement SplitButton;
        private Telerik.WinControls.UI.RadWaitingBarElement Waiting;
        private Telerik.WinControls.UI.RibbonTab Tab_Administracion;
        private Telerik.WinControls.UI.RibbonTab Tab_Herramientas;
        private Telerik.WinControls.UI.RadRibbonBarGroup Administracion_Grupo_Emision;
        private Telerik.WinControls.UI.RadButtonElement Administracion_Grupo_Emision_Button_Clientes;
        private Telerik.WinControls.UI.RadButtonElement Administracion_Grupo_Emision_Button_Comprobante;
        private Telerik.WinControls.UI.RadDropDownButtonElement Administracion_Grupo_Emision_Button_Reportes;
        internal Telerik.WinControls.UI.Docking.RadDock RadDock;
        internal Telerik.WinControls.UI.Docking.DocumentContainer documentContainer2;
        private Telerik.WinControls.UI.RadMenuItem Grupo_Cliente_Button_EstadoCuenta;
        private Telerik.WinControls.UI.RadMenuItem Grupo_Cliente_Button_EstadoCredito;
        private Telerik.WinControls.UI.RadMenuItem Grupo_Cliente_Button_Resumen;
        private Telerik.WinControls.UI.RadRibbonBarGroup Administracion_Grupo_Recepcion;
        private Telerik.WinControls.UI.RadButtonElement Grupo_Recepcion_Button_Expediente;
        private Telerik.WinControls.UI.RadButtonElement Grupo_Recepcion_Button_Validacion;
        private Telerik.WinControls.UI.RadButtonElement Grupo_Recepcion_Button_Comprobante;
        private Telerik.WinControls.UI.RadButtonElement Grupo_Recepcion_Button_Pago;
        private Telerik.WinControls.UI.RadRibbonBarGroup Administracion_Grupo_Nomina;
        private Telerik.WinControls.UI.RadButtonElement Grupo_Nominas_Button_Expediente;
        private Telerik.WinControls.UI.RadDropDownButtonElement Grupo_Nominas_Button_Nominas;
        private Telerik.WinControls.UI.RadRibbonBarGroup Herramientas_Grupo_Herramientas;
        private Telerik.WinControls.UI.RadDropDownButtonElement Herramientas_Grupo_Herramientas_Button_DescargaMasiva;
        private Telerik.WinControls.UI.RadButtonElement Herramientas_Grupo_Herramientas_Button_Backup;
        private Telerik.WinControls.UI.RadMenuItem Grupo_Nominas_Button_Consulta;
        private Telerik.WinControls.UI.RadMenuItem Grupo_Nominas_Button_ConsultaAguinaldo;
        private Telerik.WinControls.UI.RadMenuItem Grupo_Nominas_Button_Calcular;
        private Telerik.WinControls.UI.RadMenuItem Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Asistida;
        private Telerik.WinControls.UI.RadMenuItem Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Repositorio;
        private Telerik.WinControls.UI.RadMenuItem Grupo_Nominas_Button_Layout;
        private Telerik.WinControls.UI.RadRibbonBarGroup Herramientas_Grupo_Configuracion;
        private Telerik.WinControls.UI.RadButtonElement Herramientas_Grupo_Configuracion_Button_SeriesFolios;
        private Telerik.WinControls.UI.RadButtonElement Administracion_Grupo_Emision_Button_Cobros;
        private Telerik.WinControls.UI.RadDropDownButtonElement Grupo_Recepcion_Button_Reportes;
        private Telerik.WinControls.UI.RadMenuItem Grupo_Recepcion_Button_Resumen;
        private Telerik.WinControls.UI.RadMenuItem Grupo_Recepcion_Button_EstadoCuenta;
        private Telerik.WinControls.UI.RadRibbonBarGroup Administracion_Grupo_ContableE;
        private Telerik.WinControls.UI.RadDropDownButtonElement Administracion_Grupo_ContableE_Button_Plantillas;
        private Telerik.WinControls.UI.RadButtonElement Administracion_Grupo_ContableE_Button_Firmar;
        private Telerik.WinControls.UI.RadDesktopAlert radDesktopAlert1;
        private Telerik.WinControls.UI.RadMenuItem Grupo_Cliente_Button_ReciboFiscal;
        private Telerik.WinControls.UI.RadMenuItem Grupo_Nominas_Button_Resumen;
        private Telerik.WinControls.UI.RadDropDownButtonElement Herramientas_Grupo_Configuracion_General;
        private Telerik.WinControls.UI.RadMenuItem Herramientas_Grupo_Configuracion_Button_Empresa;
        private Telerik.WinControls.UI.RadMenuItem Herramientas_Grupo_Configuracion_Button_Avanzado;
        private Telerik.WinControls.UI.RadMenuItem Herramientas_Grupo_Configuracion_Button_Usuario;
        private Telerik.WinControls.UI.RadMenuItem Herramientas_Grupo_Configuracion_Button_Perfile;
        private Telerik.WinControls.UI.RadMenuItem Herramientas_Grupo_Configuracion_Button_ArchivoINI;
        private Telerik.WinControls.UI.RadMenuItem Herramientas_Grupo_Configuracion_Button_Mantenimiento;
        private Telerik.WinControls.UI.RadDropDownButtonElement Administracion_Grupo_ContableE_Button_Convertir;
        private Telerik.WinControls.UI.RadMenuItem Administracion_Grupo_ContableE_Button_DIOT;
        private Telerik.WinControls.UI.RadRibbonBarGroup Herramientas_Grupo_Temas;
        private Telerik.WinControls.UI.RadButtonElement Office2010Black;
        private Telerik.WinControls.UI.RadButtonElement Office2010Blue;
        private Telerik.WinControls.UI.RadButtonElement Office2010Silver;
        private Telerik.WinControls.Themes.Office2010BlackTheme office2010BlackTheme;
        private Telerik.WinControls.Themes.Office2010BlueTheme office2010BlueTheme;
        private Telerik.WinControls.Themes.Office2010SilverTheme office2010SilverTheme;
        private Telerik.WinControls.UI.RadMenuItem Administracion_Grupo_ContableE_Button_LayoutDIOT2019;
        private Telerik.WinControls.UI.RadMenuItem Administracion_Grupo_ContableE_Button_Layout;
        private Telerik.WinControls.UI.RadButtonElement Herramientas_Grupo_Herramientas_Button_Mantenimiento;
        private Telerik.WinControls.UI.RadRibbonBarGroup Herramientas_Grupo_ListaNegra;
        private Telerik.WinControls.UI.RadDropDownButtonElement Herramientas_Grupo_ListaNegra_Button_Listas;
        private Telerik.WinControls.UI.RadMenuItem Herramientas_Grupo_ListaNegra_Button_Cancelados;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup Grupo_Nominas_Button_Group;
        private Telerik.WinControls.UI.RadButtonElement Grupo_Nominas_Button_Departamentos;
        private Telerik.WinControls.UI.RadDropDownButtonElement Grupo_Nominas_Button_Catalogos;
        private Telerik.WinControls.UI.RadDropDownButtonElement Grupo_Nominas_Button_Configuracion;
    }
}
