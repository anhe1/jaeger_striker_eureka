﻿using System;
using System.ComponentModel;
using System.Data;
using Telerik.WinControls.UI;
using Jaeger.Edita.V2;
using Jaeger.Helpers;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Aplication;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views.Directorio
{
    public partial class ViewEstadoCredito : RadForm
    {
        private DataTable datos;
        private BackgroundWorker preparar;
        private BackgroundWorker consulta;
        private SqlSugarComprobanteFiscal data;
        private SqlSugarDirectorio directorio;
        private EnumCfdiSubType subtipo;

        public ViewEstadoCredito(EnumCfdiSubType subtipo)
        {
            InitializeComponent();
            this.ToolBarHostItem.HostedItem = this.CboReceptor.MultiColumnComboBoxElement;
            this.subtipo = subtipo;
        }

        private void ViewEstadoCredito_Load(object sender, EventArgs e)
        {
            this.GridData.TelerikGridCommon();
            this.consulta = new BackgroundWorker();
            this.consulta.DoWork += Consulta_DoWork;
            this.consulta.RunWorkerCompleted += Consulta_RunWorkerCompleted;

            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += PrepararDoWork;
            this.preparar.RunWorkerCompleted += PrepararRunWorkerCompleted;

            this.ToolBar.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.preparar.RunWorkerAsync();
            this.Espera.Visible = true;
            this.Espera.StartWaiting();
        }

        private void PrepararDoWork(object sender, DoWorkEventArgs e)
        {
            this.directorio = new SqlSugarDirectorio(ConfigService.Synapsis.RDS.Edita);
            this.data = new SqlSugarComprobanteFiscal(ConfigService.Synapsis.RDS.Edita);

            for (int anio = 2013; anio <= DateTime.Now.Year; anio = checked(anio + 1))
            {
                RadMenuItem oButtonYear = new RadMenuItem() { Text = anio.ToString() };
                this.ToolBarButtonEjercicio.Items.Add(oButtonYear);
                oButtonYear.Click += new EventHandler(this.ToolBarButtonEjercicio_Click);
            }

            if (this.subtipo == EnumCfdiSubType.Emitido)
            {
                foreach (string item in Enum.GetNames(typeof(EnumCfdiStatusIngreso)))
                {
                    RadMenuItem estado = new RadMenuItem() { Text = item };
                    this.ToolBarButtonStatus.Items.Add(estado);
                    estado.Click += new EventHandler(this.ToolBarButtonEstado_Click);
                }
                this.CboReceptor.DataSource = this.directorio.GetListBy(Jaeger.Edita.V2.Directorio.Enums.EnumRelationType.Cliente);
            }
            else
            {
                foreach (string item in Enum.GetNames(typeof(EnumCfdiStatusEgreso)))
                {
                    RadMenuItem estado = new RadMenuItem() { Text = item };
                    this.ToolBarButtonStatus.Items.Add(estado);
                    estado.Click += new EventHandler(this.ToolBarButtonEstado_Click);
                }
                this.CboReceptor.DataSource = this.directorio.GetListBy(Jaeger.Edita.V2.Directorio.Enums.EnumRelationType.Proveedor);
            }
        }
 
         private void PrepararRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (this.subtipo == EnumCfdiSubType.Emitido)
                this.ChartView.Title = "Facturación vs Cobranza";
            else if (this.subtipo == EnumCfdiSubType.Recibido)
                this.ChartView.Title = "Facturación vs Pagos";
            this.ToolBarButtonEjercicio.Text = DateTime.Now.Year.ToString();
            this.ToolBar.Enabled = true;
            this.CboReceptor.Enabled = true;
            this.Espera.Visible = false;
            this.Espera.StopWaiting();
        }

        private void Consulta_DoWork(object sender, DoWorkEventArgs e)
        {
            
            if (this.CboReceptor.SelectedItem != null)
            {
                GridViewRowInfo seleccionado = this.CboReceptor.SelectedItem as GridViewRowInfo;
                if (seleccionado != null)
                {
                    ViewModelContribuyenteDomicilio d = seleccionado.DataBoundItem as ViewModelContribuyenteDomicilio;
                    this.datos = this.data.Prueba3(DbConvert.ConvertInt32(this.ToolBarButtonEjercicio.Text), this.subtipo, this.ToolBarButtonStatus.Text, d.RFC);
                }
            }
        }

        private void Consulta_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Espera.Visible = false;
            this.Espera.StopWaiting();
            this.GridData.DataSource = this.datos;

            BarSeries lineSeria = new BarSeries();
            LineSeries lineSeria1 = new LineSeries();
            lineSeria.ValueMember = "TotalFacturado";
            lineSeria.CategoryMember = "Mes";
            lineSeria.LegendTitle = "Facturado";
            lineSeria.LabelFormat = "{0:n}";
            lineSeria1.ValueMember = "TotalCobrado";
            lineSeria1.CategoryMember = "Mes";
            if (this.subtipo == EnumCfdiSubType.Emitido)
                lineSeria1.LegendTitle = "Cobrado";
            if (this.subtipo == EnumCfdiSubType.Recibido)
                lineSeria1.LegendTitle = "Pagado";
            lineSeria1.LabelFormat = "{0:n}";
            lineSeria.ShowLabels = true;

            this.ChartView.Axes.Purge();
            this.ChartView.Series.Clear();
            this.ChartView.Invalidate();
            this.ChartView.Series.Add(lineSeria);
            this.ChartView.Series.Add(lineSeria1);
            this.ChartView.ShowLegend = true;
            this.ChartView.ShowTitle = true;
            
            lineSeria.DataSource = this.datos;
            lineSeria1.DataSource = this.datos;
            this.ChartView.Refresh();
        }

        private void ToolBarButtonEjercicio_Click(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                this.ToolBarButtonEjercicio.DefaultItem = button;
                this.ToolBarButtonEjercicio.Text = button.Text;
                this.ToolBarButtonEjercicio.PerformClick();
            }
        }

        private void ToolBarButtonEstado_Click(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                this.ToolBarButtonStatus.DefaultItem = button;
                this.ToolBarButtonStatus.Text = button.Text;
            }
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e)
        {
            this.Espera.Visible = true;
            this.Espera.StartWaiting();
            this.consulta.RunWorkerAsync();
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
