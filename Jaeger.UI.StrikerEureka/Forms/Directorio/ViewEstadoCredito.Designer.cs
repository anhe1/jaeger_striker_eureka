﻿using Jaeger.Views.Directorio;

namespace Jaeger.Views.Directorio
{
    partial class ViewEstadoCredito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewEstadoCredito));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.CartesianArea cartesianArea1 = new Telerik.WinControls.UI.CartesianArea();
            this.CommandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarHostItem = new Telerik.WinControls.UI.CommandBarHostItem();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarLabelEjercicio = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarButtonEjercicio = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarLabelStatus = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarButtonStatus = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarButtonActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.CboReceptor = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            this.ChartView = new Telerik.WinControls.UI.RadChartView();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.ToolBarButtonExportar = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).BeginInit();
            this.CommandBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            this.GridData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandBar
            // 
            this.CommandBar.Controls.Add(this.Espera);
            this.CommandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBar.Location = new System.Drawing.Point(0, 0);
            this.CommandBar.Name = "CommandBar";
            this.CommandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement});
            this.CommandBar.Size = new System.Drawing.Size(1161, 63);
            this.CommandBar.TabIndex = 0;
            // 
            // commandBarRowElement
            // 
            this.commandBarRowElement.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement.Name = "commandBarRowElement";
            this.commandBarRowElement.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "commandBarStripElement1";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarHostItem,
            this.commandBarSeparator1,
            this.ToolBarLabelEjercicio,
            this.ToolBarButtonEjercicio,
            this.ToolBarLabelStatus,
            this.ToolBarButtonStatus,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonExportar,
            this.ToolBarButtonCerrar});
            this.ToolBar.Name = "ToolBar";
            // 
            // ToolBarHostItem
            // 
            this.ToolBarHostItem.DisplayName = "Nombre";
            this.ToolBarHostItem.MinSize = new System.Drawing.Size(355, 20);
            this.ToolBarHostItem.Name = "ToolBarHostItem";
            this.ToolBarHostItem.Text = "";
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarLabelEjercicio
            // 
            this.ToolBarLabelEjercicio.Name = "ToolBarLabelEjercicio";
            this.ToolBarLabelEjercicio.Text = "Ejercicio:";
            // 
            // ToolBarButtonEjercicio
            // 
            this.ToolBarButtonEjercicio.DefaultItem = null;
            this.ToolBarButtonEjercicio.DisplayName = "Ejercicio";
            this.ToolBarButtonEjercicio.DrawImage = false;
            this.ToolBarButtonEjercicio.DrawText = true;
            this.ToolBarButtonEjercicio.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarButtonEjercicio.Image")));
            this.ToolBarButtonEjercicio.MinSize = new System.Drawing.Size(78, 34);
            this.ToolBarButtonEjercicio.Name = "ToolBarButtonEjercicio";
            this.ToolBarButtonEjercicio.Text = "Selecciona";
            // 
            // ToolBarLabelStatus
            // 
            this.ToolBarLabelStatus.DisplayName = "Status";
            this.ToolBarLabelStatus.Name = "ToolBarLabelStatus";
            this.ToolBarLabelStatus.Text = "Status:";
            // 
            // ToolBarButtonStatus
            // 
            this.ToolBarButtonStatus.DefaultItem = null;
            this.ToolBarButtonStatus.DisplayName = "Status";
            this.ToolBarButtonStatus.DrawImage = false;
            this.ToolBarButtonStatus.DrawText = true;
            this.ToolBarButtonStatus.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarButtonStatus.Image")));
            this.ToolBarButtonStatus.MinSize = new System.Drawing.Size(78, 34);
            this.ToolBarButtonStatus.Name = "ToolBarButtonStatus";
            this.ToolBarButtonStatus.Text = "Todos";
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.DisplayName = "Actualizar";
            this.ToolBarButtonActualizar.DrawText = true;
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_actualizar;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.ToolBarButtonActualizar_Click);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "Cerrar";
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_cerrar;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // GridData
            // 
            this.GridData.Controls.Add(this.CboReceptor);
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            gridViewTextBoxColumn4.FieldName = "RFC";
            gridViewTextBoxColumn4.HeaderText = "RFC";
            gridViewTextBoxColumn4.Name = "RFC";
            gridViewTextBoxColumn4.Width = 80;
            gridViewTextBoxColumn5.FieldName = "Mes";
            gridViewTextBoxColumn5.HeaderText = "Mes";
            gridViewTextBoxColumn5.Name = "Mes";
            gridViewTextBoxColumn5.Width = 75;
            gridViewTextBoxColumn6.FieldName = "DiasCobro";
            gridViewTextBoxColumn6.FormatString = "{0:n}";
            gridViewTextBoxColumn6.HeaderText = "Días Cobro";
            gridViewTextBoxColumn6.Name = "DiasCobro";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn7.FieldName = "TotalComprobantes";
            gridViewTextBoxColumn7.FormatString = "{0:n}";
            gridViewTextBoxColumn7.HeaderText = "Comp.";
            gridViewTextBoxColumn7.Name = "TotalComprobantes";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn8.DataType = typeof(decimal);
            gridViewTextBoxColumn8.FieldName = "TotalFacturado";
            gridViewTextBoxColumn8.FormatString = "{0:n}";
            gridViewTextBoxColumn8.HeaderText = "Facturado";
            gridViewTextBoxColumn8.Name = "TotalFacturado";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn8.Width = 85;
            gridViewTextBoxColumn9.DataType = typeof(decimal);
            gridViewTextBoxColumn9.FieldName = "TotalCobrado";
            gridViewTextBoxColumn9.FormatString = "{0:n}";
            gridViewTextBoxColumn9.HeaderText = "Cobrado";
            gridViewTextBoxColumn9.Name = "TotalCobrado";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn9.Width = 85;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "PromedioFacturado";
            gridViewTextBoxColumn10.FormatString = "{0:n}";
            gridViewTextBoxColumn10.HeaderText = "Prom. Facturado";
            gridViewTextBoxColumn10.Name = "PromedioFacturado";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 85;
            gridViewTextBoxColumn11.DataType = typeof(decimal);
            gridViewTextBoxColumn11.FieldName = "PromedioCobrado";
            gridViewTextBoxColumn11.FormatString = "{0:n}";
            gridViewTextBoxColumn11.HeaderText = "Prom. Cobrado";
            gridViewTextBoxColumn11.Name = "PromedioCobrado";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn11.Width = 85;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1161, 291);
            this.GridData.TabIndex = 1;
            // 
            // CboReceptor
            // 
            this.CboReceptor.AutoSizeDropDownHeight = true;
            // 
            // CboReceptor.NestedRadGridView
            // 
            this.CboReceptor.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboReceptor.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboReceptor.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboReceptor.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboReceptor.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboReceptor.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboReceptor.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn2.FieldName = "Nombre";
            gridViewTextBoxColumn2.HeaderText = "Nombre";
            gridViewTextBoxColumn2.Name = "Nombre";
            gridViewTextBoxColumn2.Width = 220;
            gridViewTextBoxColumn3.FieldName = "RFC";
            gridViewTextBoxColumn3.HeaderText = "RFC";
            gridViewTextBoxColumn3.Name = "RFC";
            gridViewTextBoxColumn3.Width = 80;
            this.CboReceptor.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.CboReceptor.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboReceptor.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboReceptor.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.CboReceptor.EditorControl.Name = "NestedRadGridView";
            this.CboReceptor.EditorControl.ReadOnly = true;
            this.CboReceptor.EditorControl.ShowGroupPanel = false;
            this.CboReceptor.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboReceptor.EditorControl.TabIndex = 0;
            this.CboReceptor.Location = new System.Drawing.Point(12, 93);
            this.CboReceptor.Name = "CboReceptor";
            this.CboReceptor.NullText = "Nombre o Razon Social del Receptor";
            this.CboReceptor.Size = new System.Drawing.Size(355, 20);
            this.CboReceptor.TabIndex = 150;
            this.CboReceptor.TabStop = false;
            this.CboReceptor.ValueMember = "Id";
            // 
            // Espera
            // 
            this.Espera.Location = new System.Drawing.Point(919, 3);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(130, 24);
            this.Espera.TabIndex = 1;
            this.Espera.Text = "radWaitingBar1";
            this.Espera.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 80;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            // 
            // ChartView
            // 
            this.ChartView.AreaDesign = cartesianArea1;
            this.ChartView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChartView.Location = new System.Drawing.Point(0, 0);
            this.ChartView.Name = "ChartView";
            this.ChartView.ShowGrid = false;
            this.ChartView.ShowPanZoom = true;
            this.ChartView.ShowToolTip = true;
            this.ChartView.ShowTrackBall = true;
            this.ChartView.Size = new System.Drawing.Size(1161, 291);
            this.ChartView.TabIndex = 2;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 63);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1161, 586);
            this.radSplitContainer1.TabIndex = 3;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.ChartView);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1161, 291);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.GridData);
            this.splitPanel2.Location = new System.Drawing.Point(0, 295);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1161, 291);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // ToolBarButtonExportar
            // 
            this.ToolBarButtonExportar.DisplayName = "Exportar";
            this.ToolBarButtonExportar.DrawText = true;
            this.ToolBarButtonExportar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_excel;
            this.ToolBarButtonExportar.Name = "ToolBarButtonExportar";
            this.ToolBarButtonExportar.Text = "Exportar";
            this.ToolBarButtonExportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ViewEstadoCredito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1161, 649);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.CommandBar);
            this.Name = "ViewEstadoCredito";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ViewEstadoCredito";
            this.Load += new System.EventHandler(this.ViewEstadoCredito_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).EndInit();
            this.CommandBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.GridData.ResumeLayout(false);
            this.GridData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar CommandBar;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonActualizar;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelEjercicio;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonEjercicio;
        private Telerik.WinControls.UI.RadChartView ChartView;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.CommandBarHostItem ToolBarHostItem;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboReceptor;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCerrar;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelStatus;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonStatus;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonExportar;
    }
}
