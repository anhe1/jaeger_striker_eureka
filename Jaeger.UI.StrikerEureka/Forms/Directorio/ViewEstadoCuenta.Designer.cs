﻿using Jaeger.Views.Directorio;

namespace Jaeger.Views.Directorio
{
    partial class ViewEstadoCuenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewEstadoCuenta));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarHostNombre = new Telerik.WinControls.UI.CommandBarHostItem();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarLabelPeriodo = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarButtonPeriodo = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarLabelEjercicio = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarButtonEjercicio = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarButtonActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarButtonExportar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsSpinnerWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement();
            this.CboReceptor = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            this.GridData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(985, 63);
            this.radCommandBar1.TabIndex = 0;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarHostNombre,
            this.commandBarSeparator1,
            this.ToolBarLabelPeriodo,
            this.ToolBarButtonPeriodo,
            this.ToolBarLabelEjercicio,
            this.ToolBarButtonEjercicio,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonFiltro,
            this.ToolBarButtonExportar,
            this.ToolBarButtonCerrar});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // ToolBarHostNombre
            // 
            this.ToolBarHostNombre.DisplayName = "Contribuyente";
            this.ToolBarHostNombre.MinSize = new System.Drawing.Size(355, 20);
            this.ToolBarHostNombre.Name = "ToolBarHostNombre";
            this.ToolBarHostNombre.Text = "Nombre";
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarLabelPeriodo
            // 
            this.ToolBarLabelPeriodo.DisplayName = "commandBarLabel1";
            this.ToolBarLabelPeriodo.Name = "ToolBarLabelPeriodo";
            this.ToolBarLabelPeriodo.Text = "Período:";
            // 
            // ToolBarButtonPeriodo
            // 
            this.ToolBarButtonPeriodo.DefaultItem = null;
            this.ToolBarButtonPeriodo.DisplayName = "commandBarSplitButton1";
            this.ToolBarButtonPeriodo.DrawImage = false;
            this.ToolBarButtonPeriodo.DrawText = true;
            this.ToolBarButtonPeriodo.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarButtonPeriodo.Image")));
            this.ToolBarButtonPeriodo.Name = "ToolBarButtonPeriodo";
            this.ToolBarButtonPeriodo.Text = "Selecciona";
            // 
            // ToolBarLabelEjercicio
            // 
            this.ToolBarLabelEjercicio.DisplayName = "commandBarLabel2";
            this.ToolBarLabelEjercicio.Name = "ToolBarLabelEjercicio";
            this.ToolBarLabelEjercicio.Text = "Ejercicio:";
            // 
            // ToolBarButtonEjercicio
            // 
            this.ToolBarButtonEjercicio.DefaultItem = null;
            this.ToolBarButtonEjercicio.DisplayName = "commandBarSplitButton1";
            this.ToolBarButtonEjercicio.DrawImage = false;
            this.ToolBarButtonEjercicio.DrawText = true;
            this.ToolBarButtonEjercicio.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarButtonEjercicio.Image")));
            this.ToolBarButtonEjercicio.Name = "ToolBarButtonEjercicio";
            this.ToolBarButtonEjercicio.Text = "Selecciona";
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.DisplayName = "Actualizar";
            this.ToolBarButtonActualizar.DrawText = true;
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_actualizar;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.ToolBarButtonActualizar_Click);
            // 
            // ToolBarButtonFiltro
            // 
            this.ToolBarButtonFiltro.DisplayName = "commandBarToggleButton1";
            this.ToolBarButtonFiltro.DrawText = true;
            this.ToolBarButtonFiltro.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_filtrar;
            this.ToolBarButtonFiltro.Name = "ToolBarButtonFiltro";
            this.ToolBarButtonFiltro.Text = "Filtro";
            this.ToolBarButtonFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonFiltro.Click += new System.EventHandler(this.ToolBarButtonFiltro_Click);
            // 
            // ToolBarButtonExportar
            // 
            this.ToolBarButtonExportar.DisplayName = "commandBarButton1";
            this.ToolBarButtonExportar.DrawText = true;
            this.ToolBarButtonExportar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_excel;
            this.ToolBarButtonExportar.Name = "ToolBarButtonExportar";
            this.ToolBarButtonExportar.Text = "Exportar";
            this.ToolBarButtonExportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonExportar.Click += new System.EventHandler(this.ToolBarButtonExportar_Click);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "commandBarButton1";
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_cerrar;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // GridData
            // 
            this.GridData.Controls.Add(this.Espera);
            this.GridData.Controls.Add(this.CboReceptor);
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 63);
            // 
            // 
            // 
            gridViewTextBoxColumn4.FieldName = "RFC";
            gridViewTextBoxColumn4.HeaderText = "RFC";
            gridViewTextBoxColumn4.Name = "RFC";
            gridViewTextBoxColumn4.Width = 85;
            gridViewTextBoxColumn5.HeaderText = "Tipo";
            gridViewTextBoxColumn5.Name = "Tipo";
            gridViewTextBoxColumn6.FieldName = "Receptor";
            gridViewTextBoxColumn6.HeaderText = "Receptor";
            gridViewTextBoxColumn6.Name = "Receptor";
            gridViewTextBoxColumn6.Width = 220;
            gridViewTextBoxColumn7.FieldName = "Status";
            gridViewTextBoxColumn7.HeaderText = "Status";
            gridViewTextBoxColumn7.Name = "Status";
            gridViewTextBoxColumn7.Width = 75;
            gridViewTextBoxColumn8.FieldName = "IdDocumento";
            gridViewTextBoxColumn8.HeaderText = "IdDocumento";
            gridViewTextBoxColumn8.Name = "IdDocumento";
            gridViewTextBoxColumn8.Width = 220;
            gridViewTextBoxColumn9.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn9.FieldName = "FechaEmision";
            gridViewTextBoxColumn9.FormatString = "{0:d}";
            gridViewTextBoxColumn9.HeaderText = "FechaEmision";
            gridViewTextBoxColumn9.Name = "FechaEmision";
            gridViewTextBoxColumn9.Width = 75;
            gridViewTextBoxColumn10.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn10.FieldName = "FechaCobro";
            gridViewTextBoxColumn10.FormatString = "{0:d}";
            gridViewTextBoxColumn10.HeaderText = "FechaCobro";
            gridViewTextBoxColumn10.Name = "FechaCobro";
            gridViewTextBoxColumn10.Width = 75;
            gridViewTextBoxColumn11.DataType = typeof(decimal);
            gridViewTextBoxColumn11.FieldName = "Total";
            gridViewTextBoxColumn11.FormatString = "{0:n}";
            gridViewTextBoxColumn11.HeaderText = "Total";
            gridViewTextBoxColumn11.Name = "Total";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn11.Width = 85;
            gridViewTextBoxColumn12.DataType = typeof(decimal);
            gridViewTextBoxColumn12.FieldName = "Descuento";
            gridViewTextBoxColumn12.FormatString = "{0:n}";
            gridViewTextBoxColumn12.HeaderText = "Descuento";
            gridViewTextBoxColumn12.Name = "Descuento";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn12.Width = 85;
            gridViewTextBoxColumn13.DataType = typeof(decimal);
            gridViewTextBoxColumn13.FieldName = "Acumulado";
            gridViewTextBoxColumn13.FormatString = "{0:n}";
            gridViewTextBoxColumn13.HeaderText = "Acumulado";
            gridViewTextBoxColumn13.Name = "Acumulado";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn13.Width = 85;
            gridViewTextBoxColumn14.FieldName = "Saldo";
            gridViewTextBoxColumn14.HeaderText = "Saldo";
            gridViewTextBoxColumn14.Name = "Saldo";
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.Width = 85;
            gridViewTextBoxColumn15.DataType = typeof(int);
            gridViewTextBoxColumn15.FieldName = "DiasCobranza";
            gridViewTextBoxColumn15.HeaderText = "DiasCobranza";
            gridViewTextBoxColumn15.Name = "DiasCobranza";
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(985, 472);
            this.GridData.TabIndex = 1;
            // 
            // Espera
            // 
            this.Espera.AssociatedControl = this.GridData;
            this.Espera.Location = new System.Drawing.Point(425, 190);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(70, 70);
            this.Espera.TabIndex = 150;
            this.Espera.Text = "radWaitingBar1";
            this.Espera.WaitingIndicators.Add(this.dotsSpinnerWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 100;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsSpinner;
            // 
            // dotsSpinnerWaitingBarIndicatorElement1
            // 
            this.dotsSpinnerWaitingBarIndicatorElement1.Name = "dotsSpinnerWaitingBarIndicatorElement1";
            // 
            // CboReceptor
            // 
            this.CboReceptor.AutoSizeDropDownHeight = true;
            // 
            // CboReceptor.NestedRadGridView
            // 
            this.CboReceptor.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboReceptor.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboReceptor.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboReceptor.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboReceptor.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboReceptor.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboReceptor.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn2.FieldName = "Nombre";
            gridViewTextBoxColumn2.HeaderText = "Nombre";
            gridViewTextBoxColumn2.Name = "Nombre";
            gridViewTextBoxColumn2.Width = 220;
            gridViewTextBoxColumn3.FieldName = "RFC";
            gridViewTextBoxColumn3.HeaderText = "RFC";
            gridViewTextBoxColumn3.Name = "RFC";
            gridViewTextBoxColumn3.Width = 80;
            this.CboReceptor.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.CboReceptor.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboReceptor.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboReceptor.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.CboReceptor.EditorControl.Name = "NestedRadGridView";
            this.CboReceptor.EditorControl.ReadOnly = true;
            this.CboReceptor.EditorControl.ShowGroupPanel = false;
            this.CboReceptor.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboReceptor.EditorControl.TabIndex = 0;
            this.CboReceptor.Location = new System.Drawing.Point(12, 101);
            this.CboReceptor.Name = "CboReceptor";
            this.CboReceptor.NullText = "Nombre o Razon Social del Receptor";
            this.CboReceptor.Size = new System.Drawing.Size(355, 20);
            this.CboReceptor.TabIndex = 149;
            this.CboReceptor.TabStop = false;
            this.CboReceptor.ValueMember = "Id";
            // 
            // ViewEstadoCuenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(985, 535);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "ViewEstadoCuenta";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ViewReporteCredito";
            this.Load += new System.EventHandler(this.ViewReporteCredito_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.GridData.ResumeLayout(false);
            this.GridData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.CommandBarHostItem ToolBarHostNombre;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboReceptor;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonActualizar;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelPeriodo;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonPeriodo;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelEjercicio;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonEjercicio;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltro;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonExportar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCerrar;
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement dotsSpinnerWaitingBarIndicatorElement1;
    }
}
