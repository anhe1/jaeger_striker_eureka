﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Directorio {
    public class ClientesForm : Jaeger.UI.Contribuyentes.Forms.ClientesCatalogoForm {
        public ClientesForm(UIMenuElement menuElement) : base(menuElement) {
            this.service = new Jaeger.Aplication.Contribuyentes.Services.DirectorioService();
        }
    }

    public class ProveedoresForm : Jaeger.UI.Contribuyentes.Forms.ClientesCatalogoForm {
        public ProveedoresForm(UIMenuElement menuElement) : base(menuElement) {
            this.service = new Jaeger.Aplication.Contribuyentes.Services.DirectorioService();
        }
    }
}
