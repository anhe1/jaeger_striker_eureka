﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.Enums;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Helpers;
using Telerik.WinControls.UI;
using Telerik.WinControls.Enumerations;
using Jaeger.Aplication;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views.Directorio
{
    public partial class ViewEstadoCuenta : Telerik.WinControls.UI.RadForm
    {
        private EnumCfdiSubType subTipoComprobante;
        private BackgroundWorker preparar;
        private BackgroundWorker consulta;
        private SqlSugarDirectorio directorio;
        private SqlSugarComprobanteFiscal data;
        private List<ViewModelEstadoCuenta> datos;

        public ViewEstadoCuenta(EnumCfdiSubType subtipo)
        {
            InitializeComponent();
            this.ToolBarHostNombre.HostedItem = this.CboReceptor.MultiColumnComboBoxElement;
            this.subTipoComprobante = subtipo;
        }

        private void ViewReporteCredito_Load(object sender, EventArgs e)
        {
            this.GridData.TelerikGridCommon();

            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += PrepararDoWork;
            this.preparar.RunWorkerCompleted += PrepararRunWorkerCompleted;

            this.consulta = new BackgroundWorker();
            this.consulta.DoWork += Consulta_DoWork;
            this.consulta.RunWorkerCompleted += Consulta_RunWorkerCompleted;
            this.preparar.RunWorkerAsync();

            this.Espera.Visible = true;
            this.Espera.StartWaiting();
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e)
        {
            this.Espera.Visible = true;
            this.Espera.StartWaiting();
            this.consulta.RunWorkerAsync();
        }

        private void ToolBarButtonFiltro_Click(object sender, EventArgs e)
        {
            this.GridData.ShowFilteringRow = this.ToolBarButtonFiltro.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false)
            {
                this.GridData.FilterDescriptors.Clear();
            }
        }

        private void ToolBarButtonExportar_Click(object sender, EventArgs e)
        {
            TelerikGridExport exportar = new TelerikGridExport(this.GridData);
            exportar.ShowDialog();
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ToolSplitButtonPeriodo_Click(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                this.ToolBarButtonPeriodo.DefaultItem = button;
                this.ToolBarButtonPeriodo.Text = button.Text;
                this.ToolBarButtonPeriodo.Tag = button.Tag;
                this.ToolBarButtonPeriodo.PerformClick();
                this.ToolBarButtonPeriodo.PerformClick();
            }
        }

        private void ToolSplitButtonEjercicio_Click(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                this.ToolBarButtonEjercicio.DefaultItem = button;
                this.ToolBarButtonEjercicio.Text = button.Text;
                this.ToolBarButtonEjercicio.PerformClick();
            }
        }

        private void PrepararDoWork(object sender, DoWorkEventArgs e)
        {
            this.directorio = new SqlSugarDirectorio(ConfigService.Synapsis.RDS.Edita);
            this.data = new SqlSugarComprobanteFiscal(ConfigService.Synapsis.RDS.Edita);
            this.CboReceptor.AutoSizeDropDownToBestFit = true;
            if (this.subTipoComprobante == EnumCfdiSubType.Emitido)
            {
                this.CboReceptor.DataSource = this.directorio.GetListBy(Jaeger.Edita.V2.Directorio.Enums.EnumRelationType.Cliente);
            }
            else
            {
                this.CboReceptor.DataSource = this.directorio.GetListBy(Jaeger.Edita.V2.Directorio.Enums.EnumRelationType.Proveedor);
            }

            foreach (string item in Enum.GetNames(typeof(EnumMonthsOfYear)))
            {
                RadMenuItem oButtonMonth = new RadMenuItem { Text = item };
                this.ToolBarButtonPeriodo.Items.Add(oButtonMonth);
                oButtonMonth.Click += new EventHandler(this.ToolSplitButtonPeriodo_Click);
            }

            for (int anio = 2013; anio <= DateTime.Now.Year; anio = checked(anio + 1))
            {
                RadMenuItem oButtonYear = new RadMenuItem() { Text = anio.ToString() };
                this.ToolBarButtonEjercicio.Items.Add(oButtonYear);
                oButtonYear.Click += new EventHandler(this.ToolSplitButtonEjercicio_Click);
            }
        }
 
        private void PrepararRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.ToolBarButtonPeriodo.Text = Enum.GetName(typeof(EnumMonthsOfYear), DateTime.Now.Month);
            this.ToolBarButtonEjercicio.Text = DateTime.Now.Year.ToString();
            this.Espera.Visible = false;
            this.Espera.StopWaiting();
        }

        private void Consulta_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            if (this.CboReceptor.SelectedItem != null)
            {
                Telerik.WinControls.UI.GridViewRowInfo seleccionado = this.CboReceptor.SelectedItem as GridViewRowInfo;
                if (seleccionado != null)
                {
                    ViewModelContribuyenteDomicilio d = seleccionado.DataBoundItem as ViewModelContribuyenteDomicilio;
                    this.datos = this.data.Prueba(this.subTipoComprobante, d.RFC, DbConvert.ConvertInt32(this.ToolBarButtonEjercicio.Text), (EnumMonthsOfYear)(Enum.Parse(typeof(EnumMonthsOfYear), this.ToolBarButtonPeriodo.Text)));
                }
            }
        }

        private void Consulta_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            this.GridData.DataSource = this.datos;
            this.Espera.Visible = false;
            this.Espera.StopWaiting();
        }

    }
}
