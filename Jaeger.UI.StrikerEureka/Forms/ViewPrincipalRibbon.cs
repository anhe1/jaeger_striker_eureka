﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls;
using Telerik.WinControls.UI.Localization;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Helpers;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Kaiju.Contracts;
using Jaeger.Aplication.Kaiju.Services;

namespace Jaeger.Views {
    public partial class ViewPrincipalRibbon : Telerik.WinControls.UI.RadRibbonForm {
        private BackgroundWorker descargarLogo;

        public ViewPrincipalRibbon() {
            InitializeComponent();
        }

        private void ViewPrincipalRibbon_Load(object sender, EventArgs e) {
            this.descargarLogo = new BackgroundWorker();
            this.descargarLogo.DoWork += DescargarLogo_DoWork;
            this.descargarLogo.RunWorkerAsync();
            RadGridLocalizationProvider.CurrentProvider = new LocalizationProviderCustomGrid();
            this.Text = string.Format("{0} [{1}]", ConfigService.Synapsis.Empresa.RazonSocial, ConfigService.Synapsis.Empresa.RFC);
            this.SplitButton.Items.Add(new RadMenuItem() { Name = "Usuario", Text = ConfigService.Piloto.Clave + " | " + ConfigService.Piloto.Clave, Image = UI.Properties.Resources.icons8_x16_ciclo_vital });
            this.SplitButton.Items.Add(new RadMenuItem() { Name = "Empresa", Text = ConfigService.Synapsis.Empresa.RFC });
            this.SplitButton.Items.Add(new RadMenuItem() { Name = "Version", Text = "Versión: " + Application.ProductVersion.ToString() });
            this.SplitButton.Text = ConfigService.Synapsis.Empresa.RFC;
            if (ConfigService.Synapsis.RDS.Edita.Database.Contains("test")) {
                this.radDesktopAlert1.PopupAnimationDirection = RadDirection.Up;
                this.radDesktopAlert1.CaptionText = "Atención";
                this.radDesktopAlert1.ContentText = "Se inicio en modo de prueba.";
                this.radDesktopAlert1.Show();
            }
             
            if (!(ConfigService.Piloto.Clave.ToLower().Contains("anhe1"))) {
                this.Herramientas_Grupo_Herramientas.Items.Remove(Herramientas_Grupo_Herramientas_Button_Mantenimiento);
                this.Herramientas_Grupo_Configuracion.Items.Remove(Herramientas_Grupo_Configuracion_Button_SeriesFolios);
                this.Herramientas_Grupo_Configuracion_General.Items.Remove(Herramientas_Grupo_Configuracion_Button_Usuario);
                this.Herramientas_Grupo_Configuracion_General.Items.Remove(Herramientas_Grupo_Configuracion_Button_Perfile);
                this.Herramientas_Grupo_Configuracion_General.Items.Remove(this.Herramientas_Grupo_Configuracion_Button_ArchivoINI);
                this.Herramientas_Grupo_Configuracion_General.Items.Remove(Herramientas_Grupo_Configuracion_Button_Avanzado);
                this.Herramientas_Grupo_Configuracion_General.Items.Remove(Herramientas_Grupo_Configuracion_Button_Mantenimiento);
                Grupo_Nominas_Button_Configuracion.Enabled = false;
                Grupo_Nominas_Button_Expediente.Enabled = false;
                Grupo_Nominas_Button_Calcular.Enabled = false;
                Grupo_Nominas_Button_ConsultaAguinaldo.Enabled = false;
                Grupo_Nominas_Button_Layout.Enabled = false;
                Grupo_Nominas_Button_Catalogos.Enabled = false;
            }
            ProfileService d0 = new ProfileService();
            d0.CreateProfile(ConfigService.Piloto);
            d0.CreateMenus(ConfigService.Piloto.Id);
            if ((!(ConfigService.Piloto.Roles[0] as Rol).Name.ToLower().Contains("administrador") && !(ConfigService.Piloto.Roles[0] as Rol).Name.ToLower().Contains("desarrollador"))) {
                Tab_Administracion.Items.Remove(this.Administracion_Grupo_Nomina);
                // Tab_Administracion.Items.Remove(this.Administracion_Grupo_ContableE);
                //Tab_Herramientas.Items.Remove(this.Herramientas_Grupo_Configuracion);
            }
        }

        private void Administracion_Grupo_Emision_Button_Clientes_Click(object sender, EventArgs e) {
            var clientes = new Jaeger.UI.Forms.Directorio.ClientesForm(new UIMenuElement()) {
                MdiParent = this,
                WindowState = FormWindowState.Maximized,
                Text = "Clientes: Expediente"
            };
            clientes.Show();
            this.RadDock.ActivateMdiChild(clientes);
        }

        private void Administracion_Grupo_Emision_Button_Comprobante_Click(object sender, EventArgs e) {
            var comprobantes = new ViewComprobantesFiscales(EnumCfdiSubType.Emitido, ref this.Waiting) {
                MdiParent = this,
                WindowState = FormWindowState.Maximized,
                Text = "Clientes: Facturación"
            };
            comprobantes.Show();
            this.RadDock.ActivateMdiChild(comprobantes);
        }

        private void Administracion_Grupo_Emision_Button_Cobros_Click(object sender, EventArgs e) {
            Contable.ViewPrePolizas cobros = new Contable.ViewPrePolizas(Edita.V2.Contable.Enums.EnumPolizaTipo.Ingreso) {
                MdiParent = this,
                WindowState = FormWindowState.Maximized,
                Text = "Clientes: Cobros"
            };
            cobros.Show();
            this.RadDock.ActivateMdiChild(cobros);
        }

        private void Grupo_Cliente_Button_EstadoCuenta_Click(object sender, EventArgs e) {
            Directorio.ViewEstadoCuenta estado = new Directorio.ViewEstadoCuenta(EnumCfdiSubType.Emitido) {
                MdiParent = this,
                WindowState = FormWindowState.Maximized,
                Text = "Estado de Cuenta"
            };
            estado.Show();
            this.RadDock.ActivateMdiChild(estado);
        }

        private void Grupo_Cliente_Button_EstadoCredito_Click(object sender, EventArgs e) {
            Directorio.ViewEstadoCredito estado = new Directorio.ViewEstadoCredito(EnumCfdiSubType.Emitido) {
                MdiParent = this,
                WindowState = FormWindowState.Maximized,
                Text = "Clientes: Crédito"
            };
            estado.Show();
            this.RadDock.ActivateMdiChild(estado);
        }

        private void Grupo_Cliente_Button_ReciboFiscal_Click(object sender, EventArgs e) {
            var reporte1 = new RecibosFiscalesReporteForm(ref this.Waiting) {
                MdiParent = this,
                WindowState = FormWindowState.Maximized,
                Text = "Reportes: Recibos Fiscales"
            };
            reporte1.Show();
            this.RadDock.ActivateMdiChild(reporte1);
        }

        private void Grupo_Cliente_Button_Resumen_Click(object sender, EventArgs e) {
            ViewComprobantesResumen estado = new ViewComprobantesResumen(EnumCfdiSubType.Emitido) {
                MdiParent = this,
                WindowState = FormWindowState.Maximized,
                Text = "Reportes: Facturación Clientes"
            };
            estado.Show();
            this.RadDock.ActivateMdiChild(estado);
        }

        private void Grupo_Recepcion_Button_Expediente_Click(object sender, EventArgs e) {
            var proveedores = new Jaeger.UI.Forms.Directorio.ProveedoresForm(new UIMenuElement()) {
                MdiParent = this,
                WindowState = FormWindowState.Maximized,
                Text = "Proveedores: Expediente"
            };
            proveedores.Show();
            this.RadDock.ActivateMdiChild(proveedores);
        }

        private void Grupo_Recepcion_Button_Validacion_Click(object sender, EventArgs e) {
            ComprobantesValidadorForm validador = new ComprobantesValidadorForm() { Text = "Recepción: Validador", MdiParent = this };
            validador.Show();
            this.RadDock.ActivateMdiChild(validador);
        }

        private void Grupo_Recepcion_Button_Comprobante_Click(object sender, EventArgs e) {
            ViewComprobantesFiscales comprobantes = new ViewComprobantesFiscales(EnumCfdiSubType.Recibido, ref this.Waiting) {
                MdiParent = this,
                WindowState = FormWindowState.Maximized,
                Text = "Recepción: Facturación"
            };
            comprobantes.Show();
            this.RadDock.ActivateMdiChild(comprobantes);
        }

        private void Grupo_Recepcion_Button_Pago_Click(object sender, EventArgs e) {
            Contable.ViewPrePolizas pagos = new Contable.ViewPrePolizas(Edita.V2.Contable.Enums.EnumPolizaTipo.Egreso) {
                MdiParent = this,
                WindowState = FormWindowState.Maximized,
                Text = "Proveedores: Pagos"
            };
            pagos.Show();
            this.RadDock.ActivateMdiChild(pagos);
        }

        private void Grupo_Recepcion_Button_Resumen_Click(object sender, EventArgs e) {
            ViewComprobantesResumen resumen = new ViewComprobantesResumen(EnumCfdiSubType.Recibido) {
                MdiParent = this,
                WindowState = FormWindowState.Maximized,
                Text = "Proveedores: Resumen"
            };
            resumen.Show();
            this.RadDock.ActivateMdiChild(resumen);
        }

        private void Grupo_Recepcion_Button_EstadoCuenta_Click(object sender, EventArgs e) {
            Directorio.ViewEstadoCredito estado = new Directorio.ViewEstadoCredito(EnumCfdiSubType.Recibido) {
                MdiParent = this,
                WindowState = FormWindowState.Maximized,
                Text = "Proveedor: Crédito"
            };
            estado.Show();
            this.RadDock.ActivateMdiChild(estado);
        }

        private void Grupo_Nominas_Button_Expediente_Click(object sender, EventArgs e) {
            Nomina.ViewEmpleadoCatalogo catalogoEmpleados = new Nomina.ViewEmpleadoCatalogo();
            catalogoEmpleados.MdiParent = this;
            catalogoEmpleados.Show();
            this.RadDock.ActivateMdiChild(catalogoEmpleados);
        }

        private void Grupo_Nominas_Button_ConsultaAguinaldo_Click(object sender, EventArgs e) {
            Nomina.ViewNominaAguinaldo aguinaldo = new Nomina.ViewNominaAguinaldo() { MdiParent = this };
            aguinaldo.Show();
            this.RadDock.ActivateMdiChild(aguinaldo);
        }

        private void Grupo_Nominas_Button_Departamentos_Click(object sender, EventArgs e) {
            var departamentos = new Nomina.ViewDepartamentoCatalogo() { MdiParent = this };
            departamentos.Show();
            this.RadDock.ActivateMdiChild(departamentos);
        }

        private void Grupo_Nominas_Button_Layout_Click(object sender, EventArgs e) {
            Views.ViewLayoutNomina viewLayoutNomina = new ViewLayoutNomina() { MdiParent = this };
            viewLayoutNomina.Show();
            this.RadDock.ActivateMdiChild(viewLayoutNomina);
        }

        private void Grupo_Nominas_Button_CrearTablas_Click(object sender, EventArgs e) {
            Edita.V2.SqlSugarComprobanteFiscal sql = new Edita.V2.SqlSugarComprobanteFiscal(ConfigService.Synapsis.RDS.Edita);
            sql.Prueba(EnumCfdiSubType.Emitido, "", 2019, Edita.Enums.EnumMonthsOfYear.None);
        }

        private void Grupo_Nominas_Button_Consulta_Click(object sender, EventArgs e) {
            Nomina.NominaComprobantesForm comprobantesNomina = new Nomina.NominaComprobantesForm() { MdiParent = this };
            comprobantesNomina.Show();
            this.RadDock.ActivateMdiChild(comprobantesNomina);
        }

        private void Grupo_Nominas_Button_Mantenimineto_Click(object sender, EventArgs e) {
            Nomina.ConfiguracionGeneral configuracionGeneral = new Nomina.ConfiguracionGeneral() { MdiParent = this };
            configuracionGeneral.Show();
            this.RadDock.ActivateMdiChild(configuracionGeneral);
        }

        private void Grupo_Nominas_Button_Calcular_Click(object sender, EventArgs e) {
            Nomina.ViewNominaCalcular nominaCalcular = new Nomina.ViewNominaCalcular() { MdiParent = this };
            nominaCalcular.Show();
            this.RadDock.ActivateMdiChild(nominaCalcular);
        }

        private void Grupo_Nominas_Button_Resumen_Click(object sender, EventArgs e) {
            Nomina.ViewNominaResumen resumen = new Nomina.ViewNominaResumen() { MdiParent = this };
            resumen.Show();
            this.RadDock.ActivateMdiChild(resumen);
        }

        private void Herramientas_Grupo_Herramientas_Button_DescargaMasiva_Repositorio_Click(object sender, EventArgs e) {
            var viewComprobantesRepositorio = new Jaeger.Views.Descarga.ViewComprobantesRepositorio() { MdiParent = this };
            viewComprobantesRepositorio.Show();
            this.RadDock.ActivateMdiChild(viewComprobantesRepositorio);
        }

        private void Herramientas_Grupo_Configuracion_Button_Avanzado_Click(object sender, EventArgs e) {
            ViewConfiguracion conf = new ViewConfiguracion() { Text = "Configuración", StartPosition = FormStartPosition.CenterParent };
            conf.ShowDialog(this);
        }

        private void Herramientas_Grupo_Configuracion_Button_Config_Click(object sender, EventArgs e) {
            //ViewConfigSynapsis conf = new ViewConfigSynapsis() { Text = "Configuración", StartPosition = FormStartPosition.CenterParent };
            //conf.ShowDialog(this);
        }

        private void Herramientas_Grupo_Configuracion_Button_Mantenimiento_Click(object sender, EventArgs e) {
            ViewMantenimiento viewMantenimiento = new ViewMantenimiento() { StartPosition = FormStartPosition.CenterParent };
            viewMantenimiento.ShowDialog();
        }

        private void Herramientas_Grupo_Configuracion_Button_Perfiles_Click(object sender, EventArgs e) {
            //ViewPerfiles perfiles = new ViewPerfiles { Text = "Empresa: Perfiles de Usuario", MdiParent = this };
            //perfiles.Show();
            //this.RadDock.ActivateMdiChild(perfiles);
        }

        private void Herramientas_Grupo_Configuracion_Button_Usuario_Click(object sender, EventArgs e) {
            //ViewUsuarios usuarios = new ViewUsuarios { MdiParent = this, Text = "Empresa: Usuarios" };
            //usuarios.Show();
        }

        private void Herramientas_Grupo_Configuracion_Button_SeriesFolios_Click(object sender, EventArgs e) {
            ViewSeriesFolios viewSeriesFolios = new ViewSeriesFolios() { StartPosition = FormStartPosition.CenterParent };
            viewSeriesFolios.ShowDialog(this);
        }

        private void Herramientas_Grupo_Herramientas_Button_Backup_Click(object sender, EventArgs e) {
            ViewComprobantesBackup backup = new ViewComprobantesBackup() { StartPosition = FormStartPosition.CenterParent };
            backup.ShowDialog(this);
        }

        private void Administracion_Grupo_ContableE_Button_LayoutDIOT2019_Click(object sender, EventArgs e) {
            //LayoutDIOT2019
            var saveFile = new SaveFileDialog() { Filter = "*.xlsx|*.XLSX", FileName = "LayoutDIOT2019.xlsx" };
            if (saveFile.ShowDialog() == DialogResult.OK) {
                var layouts = new Jaeger.Util.Helpers.EmbeddedResources("jaeger_striker_eureka");
                var salida = layouts.GetAsBytes("Jaeger.Layouts.LayoutDIOT2019.xlsx");
                if (salida != null)
                    Jaeger.Util.Helpers.HelperFiles.WriteFileByte(salida, saveFile.FileName);
            }
        }

        private void Administracion_Grupo_ContableE_Button_DIOT_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog() { Filter = "*.xlsx|*.XLSX" };
            if (openFile.ShowDialog() == DialogResult.OK) {
                var saveFile = new SaveFileDialog() { Filter = "*.txt|*.TXT" };
                if (saveFile.ShowDialog() == DialogResult.OK) {
                    var testing = new Layout.Helpers.LayoutExcelDIOT2019();
                    testing.InfoFile = new System.IO.FileInfo(openFile.FileName);
                    testing.Reader();
                    testing.Datos(testing.Sheets[0].Name);
                    testing.Crear(saveFile.FileName);
                    if (System.IO.File.Exists(saveFile.FileName))
                        RadMessageBox.Show(this, "Se creo el archivo: " + saveFile.FileName, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
                    else
                        RadMessageBox.Show(this, "Ocurrio un error al procesar el archivo, por favor verifique archivo layout." + openFile.FileName, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
                }
            }
        }

        private void Office2010Black_Click(object sender, EventArgs e) {
            ThemeResolutionService.ApplicationThemeName = this.office2010BlackTheme.ThemeName;
            UI.Properties.Settings.Default.Theme = this.office2010BlackTheme.ThemeName;
            UI.Properties.Settings.Default.Save();
        }

        private void Office2010Blue_Click(object sender, EventArgs e) {
            ThemeResolutionService.ApplicationThemeName = this.office2010BlueTheme.ThemeName;
            UI.Properties.Settings.Default.Theme = this.office2010BlueTheme.ThemeName;
            UI.Properties.Settings.Default.Save();
        }

        private void Office2010Silver_Click(object sender, EventArgs e) {
            ThemeResolutionService.ApplicationThemeName = this.office2010SilverTheme.ThemeName;
            UI.Properties.Settings.Default.Theme = this.office2010SilverTheme.ThemeName;
            UI.Properties.Settings.Default.Save();
        }

        private void Administracion_Grupo_ContableE_Button_Layout_Click(object sender, EventArgs e) {
            var layout = new Contable.ViewLayoutContableE() { StartPosition = FormStartPosition.CenterParent };
            layout.ShowDialog(this);
        }

        private void Herramientas_Grupo_Herramientas_Button_Mantenimiento_Click(object sender, EventArgs e) {
            var mantto = new ViewMantenimiento() { StartPosition = FormStartPosition.CenterParent };
            mantto.ShowDialog(this);
        }

        private void Herramientas_Grupo_ListaNegra_Button_Cancelados_Click(object sender, EventArgs e) {
            var lista = new ViewListaNegraSAT() { MdiParent = this };
            lista.Show();
            this.RadDock.ActivateMdiChild(lista);
        }

        private void DescargarLogo_DoWork(object sender, DoWorkEventArgs e) {
            string logo = string.Format("https://s3.amazonaws.com/{0}/Conf/logo-{1}.png", ConfigService.Synapsis.Empresa.RFC.ToLower(), ConfigService.Synapsis.Empresa.Clave);
            string archivo = JaegerManagerPaths.JaegerPath(Edita.Enums.EnumPaths.Media, string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            Util.Helpers.HelperFiles.DownloadFile(logo, archivo);
        }
    }
}
