﻿namespace Jaeger.Views
{
    partial class ViewComprobantesValidadorConfigurar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.switchEstadoSAT = new Telerik.WinControls.UI.RadToggleSwitch();
            this.switchLocalizarPDF = new Telerik.WinControls.UI.RadToggleSwitch();
            this.switchEstricto = new Telerik.WinControls.UI.RadToggleSwitch();
            this.switchModoParalelo = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.txbLugarExpedicion = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.switchRenombrarOrigen = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.buttonGuardar = new Telerik.WinControls.UI.RadButton();
            this.buttonCerrar = new Telerik.WinControls.UI.RadButton();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.switchArticulo69B = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.switchClaveProductoServicio = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.switchClaveUnidad = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.switchDescargaCertificados = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.switchMetodoPago = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.switchFormaPago = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.switchUsoCFDI = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radGroupBox4 = new Telerik.WinControls.UI.RadGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.switchEstadoSAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchLocalizarPDF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchEstricto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchModoParalelo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbLugarExpedicion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchRenombrarOrigen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonGuardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.switchArticulo69B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchClaveProductoServicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchClaveUnidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchDescargaCertificados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchMetodoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchFormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchUsoCFDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).BeginInit();
            this.radGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // switchEstadoSAT
            // 
            this.switchEstadoSAT.Location = new System.Drawing.Point(246, 49);
            this.switchEstadoSAT.Name = "switchEstadoSAT";
            this.switchEstadoSAT.Size = new System.Drawing.Size(50, 20);
            this.switchEstadoSAT.TabIndex = 0;
            // 
            // switchLocalizarPDF
            // 
            this.switchLocalizarPDF.Location = new System.Drawing.Point(246, 75);
            this.switchLocalizarPDF.Name = "switchLocalizarPDF";
            this.switchLocalizarPDF.Size = new System.Drawing.Size(50, 20);
            this.switchLocalizarPDF.TabIndex = 1;
            // 
            // switchEstricto
            // 
            this.switchEstricto.Location = new System.Drawing.Point(246, 101);
            this.switchEstricto.Name = "switchEstricto";
            this.switchEstricto.Size = new System.Drawing.Size(50, 20);
            this.switchEstricto.TabIndex = 2;
            // 
            // switchModoParalelo
            // 
            this.switchModoParalelo.Location = new System.Drawing.Point(246, 127);
            this.switchModoParalelo.Name = "switchModoParalelo";
            this.switchModoParalelo.Size = new System.Drawing.Size(50, 20);
            this.switchModoParalelo.TabIndex = 3;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(11, 49);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(209, 18);
            this.radLabel1.TabIndex = 4;
            this.radLabel1.Text = "Verificar el estado del comprobante SAT.";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(11, 75);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(110, 18);
            this.radLabel2.TabIndex = 5;
            this.radLabel2.Text = "Buscar y asignar PDF";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(11, 101);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(122, 18);
            this.radLabel3.TabIndex = 6;
            this.radLabel3.Text = "Validación XSD estricto";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(11, 127);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(80, 18);
            this.radLabel4.TabIndex = 7;
            this.radLabel4.Text = "Modo paralelo";
            // 
            // txbLugarExpedicion
            // 
            this.txbLugarExpedicion.Location = new System.Drawing.Point(11, 70);
            this.txbLugarExpedicion.Name = "txbLugarExpedicion";
            this.txbLugarExpedicion.ShowClearButton = true;
            this.txbLugarExpedicion.Size = new System.Drawing.Size(277, 20);
            this.txbLugarExpedicion.TabIndex = 8;
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Location = new System.Drawing.Point(11, 21);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(208, 79);
            this.radLabel5.TabIndex = 9;
            this.radLabel5.Text = "Validar si el contribuyente se ubica en el listado SAT presunción de llevar a cab" +
    "o operaciones inexistentes a través de la emisión de facturas o comprobantes fis" +
    "cales.";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(11, 153);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(177, 18);
            this.radLabel6.TabIndex = 11;
            this.radLabel6.Text = "Renombrar los archivos de origen.";
            // 
            // switchRenombrarOrigen
            // 
            this.switchRenombrarOrigen.Location = new System.Drawing.Point(246, 153);
            this.switchRenombrarOrigen.Name = "switchRenombrarOrigen";
            this.switchRenombrarOrigen.Size = new System.Drawing.Size(50, 20);
            this.switchRenombrarOrigen.TabIndex = 10;
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Location = new System.Drawing.Point(11, 17);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(277, 47);
            this.radLabel7.TabIndex = 12;
            this.radLabel7.Text = "Expresión regular para la comprobación del codigo postal ó lugar de expedición, p" +
    "ara versión CFDI 3.2";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radLabel7);
            this.radGroupBox1.Controls.Add(this.txbLugarExpedicion);
            this.radGroupBox1.HeaderText = "Lugar de Expedición";
            this.radGroupBox1.Location = new System.Drawing.Point(324, 201);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(311, 107);
            this.radGroupBox1.TabIndex = 13;
            this.radGroupBox1.Text = "Lugar de Expedición";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.radLabel1);
            this.radGroupBox2.Controls.Add(this.switchEstadoSAT);
            this.radGroupBox2.Controls.Add(this.radLabel6);
            this.radGroupBox2.Controls.Add(this.switchLocalizarPDF);
            this.radGroupBox2.Controls.Add(this.switchRenombrarOrigen);
            this.radGroupBox2.Controls.Add(this.switchEstricto);
            this.radGroupBox2.Controls.Add(this.radLabel4);
            this.radGroupBox2.Controls.Add(this.switchModoParalelo);
            this.radGroupBox2.Controls.Add(this.radLabel3);
            this.radGroupBox2.Controls.Add(this.radLabel2);
            this.radGroupBox2.HeaderText = "General";
            this.radGroupBox2.Location = new System.Drawing.Point(12, 12);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(306, 185);
            this.radGroupBox2.TabIndex = 14;
            this.radGroupBox2.Text = "General";
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.Location = new System.Drawing.Point(653, 12);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(110, 24);
            this.buttonGuardar.TabIndex = 15;
            this.buttonGuardar.Text = "Guardar";
            this.buttonGuardar.Click += new System.EventHandler(this.buttonGuardar_Click);
            // 
            // buttonCerrar
            // 
            this.buttonCerrar.Location = new System.Drawing.Point(653, 42);
            this.buttonCerrar.Name = "buttonCerrar";
            this.buttonCerrar.Size = new System.Drawing.Size(110, 24);
            this.buttonCerrar.TabIndex = 16;
            this.buttonCerrar.Text = "Cerrar";
            this.buttonCerrar.Click += new System.EventHandler(this.buttonCerrar_Click);
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.Controls.Add(this.switchArticulo69B);
            this.radGroupBox3.Controls.Add(this.radLabel5);
            this.radGroupBox3.HeaderText = "Artículo 69B";
            this.radGroupBox3.Location = new System.Drawing.Point(12, 201);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Size = new System.Drawing.Size(306, 107);
            this.radGroupBox3.TabIndex = 19;
            this.radGroupBox3.Text = "Artículo 69B";
            // 
            // switchArticulo69B
            // 
            this.switchArticulo69B.Location = new System.Drawing.Point(246, 21);
            this.switchArticulo69B.Name = "switchArticulo69B";
            this.switchArticulo69B.Size = new System.Drawing.Size(50, 20);
            this.switchArticulo69B.TabIndex = 11;
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(14, 23);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(163, 18);
            this.radLabel8.TabIndex = 21;
            this.radLabel8.Text = "Verificar clave SAT de concepto";
            // 
            // switchClaveProductoServicio
            // 
            this.switchClaveProductoServicio.Location = new System.Drawing.Point(249, 23);
            this.switchClaveProductoServicio.Name = "switchClaveProductoServicio";
            this.switchClaveProductoServicio.Size = new System.Drawing.Size(50, 20);
            this.switchClaveProductoServicio.TabIndex = 20;
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(14, 49);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(151, 18);
            this.radLabel9.TabIndex = 23;
            this.radLabel9.Text = "Verificar clave SAT de unidad";
            // 
            // switchClaveUnidad
            // 
            this.switchClaveUnidad.Location = new System.Drawing.Point(249, 49);
            this.switchClaveUnidad.Name = "switchClaveUnidad";
            this.switchClaveUnidad.Size = new System.Drawing.Size(50, 20);
            this.switchClaveUnidad.TabIndex = 22;
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(14, 75);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(116, 18);
            this.radLabel10.TabIndex = 25;
            this.radLabel10.Text = "Descargar certificados";
            // 
            // switchDescargaCertificados
            // 
            this.switchDescargaCertificados.Location = new System.Drawing.Point(249, 75);
            this.switchDescargaCertificados.Name = "switchDescargaCertificados";
            this.switchDescargaCertificados.Size = new System.Drawing.Size(50, 20);
            this.switchDescargaCertificados.TabIndex = 24;
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(14, 101);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(128, 18);
            this.radLabel11.TabIndex = 27;
            this.radLabel11.Text = "Validar método de pago";
            // 
            // switchMetodoPago
            // 
            this.switchMetodoPago.Location = new System.Drawing.Point(249, 101);
            this.switchMetodoPago.Name = "switchMetodoPago";
            this.switchMetodoPago.Size = new System.Drawing.Size(50, 20);
            this.switchMetodoPago.TabIndex = 26;
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(14, 127);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(118, 18);
            this.radLabel12.TabIndex = 29;
            this.radLabel12.Text = "Validar forma de Pago";
            // 
            // switchFormaPago
            // 
            this.switchFormaPago.Location = new System.Drawing.Point(249, 127);
            this.switchFormaPago.Name = "switchFormaPago";
            this.switchFormaPago.Size = new System.Drawing.Size(50, 20);
            this.switchFormaPago.TabIndex = 28;
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(14, 153);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(148, 18);
            this.radLabel13.TabIndex = 31;
            this.radLabel13.Text = "Valdiar clave de uso de CFDI";
            // 
            // switchUsoCFDI
            // 
            this.switchUsoCFDI.Location = new System.Drawing.Point(249, 153);
            this.switchUsoCFDI.Name = "switchUsoCFDI";
            this.switchUsoCFDI.Size = new System.Drawing.Size(50, 20);
            this.switchUsoCFDI.TabIndex = 30;
            // 
            // radGroupBox4
            // 
            this.radGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox4.Controls.Add(this.radLabel8);
            this.radGroupBox4.Controls.Add(this.radLabel13);
            this.radGroupBox4.Controls.Add(this.switchClaveProductoServicio);
            this.radGroupBox4.Controls.Add(this.switchUsoCFDI);
            this.radGroupBox4.Controls.Add(this.switchClaveUnidad);
            this.radGroupBox4.Controls.Add(this.radLabel12);
            this.radGroupBox4.Controls.Add(this.radLabel9);
            this.radGroupBox4.Controls.Add(this.switchFormaPago);
            this.radGroupBox4.Controls.Add(this.switchDescargaCertificados);
            this.radGroupBox4.Controls.Add(this.radLabel11);
            this.radGroupBox4.Controls.Add(this.radLabel10);
            this.radGroupBox4.Controls.Add(this.switchMetodoPago);
            this.radGroupBox4.HeaderText = "Validaciones adicionales";
            this.radGroupBox4.Location = new System.Drawing.Point(324, 12);
            this.radGroupBox4.Name = "radGroupBox4";
            this.radGroupBox4.Size = new System.Drawing.Size(311, 185);
            this.radGroupBox4.TabIndex = 32;
            this.radGroupBox4.Text = "Validaciones adicionales";
            // 
            // ViewComprobantesValidadorConfigurar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 319);
            this.Controls.Add(this.radGroupBox4);
            this.Controls.Add(this.radGroupBox3);
            this.Controls.Add(this.buttonCerrar);
            this.Controls.Add(this.buttonGuardar);
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.radGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewComprobantesValidadorConfigurar";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Configuración:";
            this.Load += new System.EventHandler(this.ViewComprobantesValidadorConfigurar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.switchEstadoSAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchLocalizarPDF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchEstricto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchModoParalelo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbLugarExpedicion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchRenombrarOrigen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonGuardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.switchArticulo69B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchClaveProductoServicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchClaveUnidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchDescargaCertificados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchMetodoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchFormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchUsoCFDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).EndInit();
            this.radGroupBox4.ResumeLayout(false);
            this.radGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadToggleSwitch switchEstadoSAT;
        private Telerik.WinControls.UI.RadToggleSwitch switchLocalizarPDF;
        private Telerik.WinControls.UI.RadToggleSwitch switchEstricto;
        private Telerik.WinControls.UI.RadToggleSwitch switchModoParalelo;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox txbLugarExpedicion;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadToggleSwitch switchRenombrarOrigen;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadButton buttonGuardar;
        private Telerik.WinControls.UI.RadButton buttonCerrar;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Telerik.WinControls.UI.RadToggleSwitch switchArticulo69B;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadToggleSwitch switchClaveProductoServicio;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadToggleSwitch switchClaveUnidad;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadToggleSwitch switchDescargaCertificados;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadToggleSwitch switchMetodoPago;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadToggleSwitch switchFormaPago;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadToggleSwitch switchUsoCFDI;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox4;
    }
}
