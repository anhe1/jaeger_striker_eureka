﻿namespace Jaeger.Views
{
    partial class ViewComprobanteBuscar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabelBuscar = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarTextBoxBuscar = new Telerik.WinControls.UI.CommandBarTextBox();
            this.ToolBarButtonBuscar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonSeleccionar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(734, 30);
            this.radCommandBar1.TabIndex = 0;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "commandBarStripElement1";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelBuscar,
            this.ToolBarTextBoxBuscar,
            this.ToolBarButtonBuscar,
            this.ToolBarButtonSeleccionar,
            this.ToolBarButtonFiltro,
            this.ToolBarButtonCerrar});
            this.ToolBar.Name = "ToolBar";
            // 
            // 
            // 
            this.ToolBar.OverflowButton.Enabled = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.ToolBar.GetChildAt(2))).Enabled = false;
            // 
            // ToolBarLabelBuscar
            // 
            this.ToolBarLabelBuscar.DisplayName = "Folio";
            this.ToolBarLabelBuscar.Name = "ToolBarLabelBuscar";
            this.ToolBarLabelBuscar.Text = "Folio:";
            // 
            // ToolBarTextBoxBuscar
            // 
            this.ToolBarTextBoxBuscar.DisplayName = "commandBarTextBox1";
            this.ToolBarTextBoxBuscar.MinSize = new System.Drawing.Size(85, 22);
            this.ToolBarTextBoxBuscar.Name = "ToolBarTextBoxBuscar";
            this.ToolBarTextBoxBuscar.NullText = "Folio";
            this.ToolBarTextBoxBuscar.Text = "";
            // 
            // ToolBarButtonBuscar
            // 
            this.ToolBarButtonBuscar.DisplayName = "Buscar";
            this.ToolBarButtonBuscar.DrawText = true;
            this.ToolBarButtonBuscar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_busqueda;
            this.ToolBarButtonBuscar.Name = "ToolBarButtonBuscar";
            this.ToolBarButtonBuscar.Text = "Buscar";
            this.ToolBarButtonBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonBuscar.Click += new System.EventHandler(this.ToolBarButtonBuscar_Click);
            // 
            // ToolBarButtonSeleccionar
            // 
            this.ToolBarButtonSeleccionar.DisplayName = "commandBarButton1";
            this.ToolBarButtonSeleccionar.DrawText = true;
            this.ToolBarButtonSeleccionar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_mas;
            this.ToolBarButtonSeleccionar.Name = "ToolBarButtonSeleccionar";
            this.ToolBarButtonSeleccionar.Text = "Seleccionar";
            this.ToolBarButtonSeleccionar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonSeleccionar.Click += new System.EventHandler(this.ToolBarButtonSeleccionar_Click);
            // 
            // ToolBarButtonFiltro
            // 
            this.ToolBarButtonFiltro.DisplayName = "Filtro";
            this.ToolBarButtonFiltro.DrawText = true;
            this.ToolBarButtonFiltro.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_filtrar;
            this.ToolBarButtonFiltro.Name = "ToolBarButtonFiltro";
            this.ToolBarButtonFiltro.Text = "Filtro";
            this.ToolBarButtonFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonFiltro.Click += new System.EventHandler(this.ToolBarButtonFiltro_Click);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "Cerrar";
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_cerrar;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "TipoComprobante";
            gridViewTextBoxColumn2.HeaderText = "Tipo";
            gridViewTextBoxColumn2.Name = "TipoComprobante";
            gridViewTextBoxColumn3.FieldName = "Folio";
            gridViewTextBoxColumn3.HeaderText = "Folio";
            gridViewTextBoxColumn3.Name = "Folio";
            gridViewTextBoxColumn4.FieldName = "Serie";
            gridViewTextBoxColumn4.HeaderText = "Serie";
            gridViewTextBoxColumn4.Name = "Serie";
            gridViewTextBoxColumn5.FieldName = "Status";
            gridViewTextBoxColumn5.HeaderText = "Status";
            gridViewTextBoxColumn5.Name = "Status";
            gridViewTextBoxColumn5.Width = 85;
            gridViewTextBoxColumn6.FieldName = "Receptor";
            gridViewTextBoxColumn6.HeaderText = "Receptor";
            gridViewTextBoxColumn6.Name = "Receptor";
            gridViewTextBoxColumn6.Width = 200;
            gridViewTextBoxColumn7.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn7.HeaderText = "RFC";
            gridViewTextBoxColumn7.Name = "ReceptorRFC";
            gridViewTextBoxColumn7.Width = 90;
            gridViewTextBoxColumn8.FieldName = "IdDocumento";
            gridViewTextBoxColumn8.HeaderText = "IdDocumento";
            gridViewTextBoxColumn8.Name = "IdDocumento";
            gridViewTextBoxColumn8.Width = 190;
            gridViewTextBoxColumn9.FieldName = "FechaEmision";
            gridViewTextBoxColumn9.FormatString = "{0:d}";
            gridViewTextBoxColumn9.HeaderText = "Fecha Emision";
            gridViewTextBoxColumn9.Name = "FechaEmision";
            gridViewTextBoxColumn9.Width = 85;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "Total";
            gridViewTextBoxColumn10.FormatString = "{0:n}";
            gridViewTextBoxColumn10.HeaderText = "Total";
            gridViewTextBoxColumn10.Name = "Total";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 85;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.ShowGroupPanel = false;
            this.GridData.Size = new System.Drawing.Size(734, 240);
            this.GridData.TabIndex = 1;
            // 
            // ViewComprobanteBuscar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 270);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.radCommandBar1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewComprobanteBuscar";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Buscar";
            this.Load += new System.EventHandler(this.ViewComprobanteBuscar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelBuscar;
        private Telerik.WinControls.UI.CommandBarTextBox ToolBarTextBoxBuscar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonBuscar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonSeleccionar;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltro;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCerrar;
        private Telerik.WinControls.UI.RadGridView GridData;
    }
}
