﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using Jaeger.Edita.V2.Almacen.Entities;
using Microsoft.Reporting.WinForms;
using Jaeger.Edita.Enums;
using Jaeger.Edita.V2.Contable.Entities;
using Jaeger.Edita.V2.Contable.Enums;
using Jaeger.Enums;
using Jaeger.Helpers;
using Jaeger.Util.Helpers;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Entities.Reportes;
using Jaeger.Aplication;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Views
{
    public partial class ViewReportes : Telerik.WinControls.UI.RadForm
    {
        private HelperConvertidor convertidor = new HelperConvertidor();
        private EmbeddedResources horizon = new EmbeddedResources("jaeger_horizon_brave");
        private EnumReporteTipo eTypeReport;
        private ViewModelPrePoliza prepoliza;
        private ValidateResponse validateResponse;
        private ViewModelAccuseCancelacion acuse33;
        private EstadoDeCuenta estadoDeCuenta;
        //private ViewModelOrdenCompra ordenCompra;
        private CFDI.V33.Comprobante comprobante33;
        private Jaeger.Nomina.Entities.ViewModelEmpleado empleado;
        //private Jaeger.Edita.V2.Almacen.Entities.ViewModelValeAlmacen valeAlmacen;

        private byte[] logo;

        public ViewReportes()
        {
            InitializeComponent();
        }

        public ViewReportes(ViewModelPrePoliza objeto)
        {
            InitializeComponent();
            if (objeto.Tipo == EnumPolizaTipo.Ingreso)
                eTypeReport = EnumReporteTipo.ReciboDeCobro;
            else
                eTypeReport = EnumReporteTipo.ReciboDePago;
            this.prepoliza = objeto;
        }

        public ViewReportes(ValidateResponse validate)
        {
            InitializeComponent();
            this.eTypeReport = EnumReporteTipo.Validation;
            this.validateResponse = validate;
        }

        public ViewReportes(ViewModelAccuseCancelacion acuse)
        {
            InitializeComponent();
            this.eTypeReport = EnumReporteTipo.Accuse;
            this.acuse33 = acuse;
        }

        public ViewReportes(CFDI.V33.Comprobante comprobante)
        {
            InitializeComponent();
            this.eTypeReport = EnumReporteTipo.Cfdiv33;
            this.comprobante33 = comprobante;
        }

        public ViewReportes(EstadoDeCuenta estadoDeCuenta)
        {
            InitializeComponent();
            this.eTypeReport = EnumReporteTipo.EstadoCuenta;
            this.estadoDeCuenta = estadoDeCuenta;
        }

        //public ViewReportes(ViewModelOrdenCompra ordenCompra)
        //{
        //    InitializeComponent();
        //    this.eTypeReport = EnumReporteTipo.OrdenCompra;
        //    this.ordenCompra = ordenCompra;
        //}

        public ViewReportes(Jaeger.Nomina.Entities.ViewModelEmpleado objeto)
        {
            InitializeComponent();
            this.eTypeReport = EnumReporteTipo.ExpedienteEmpleado;
            this.empleado = objeto;
        }

        //public ViewReportes(Jaeger.Edita.V2.Almacen.Entities.ViewModelValeAlmacen objeto)
        //{
        //    InitializeComponent();
        //    if (objeto.Efecto == Edita.V2.Almacen.Enums.EnumMovimientoTipo.Entrada)
        //        this.eTypeReport = EnumReporteTipo.ValeEntrada;
        //    else if (objeto.Efecto == Edita.V2.Almacen.Enums.EnumMovimientoTipo.Salida)
        //        this.eTypeReport = EnumReporteTipo.ValeSalida;
        //    this.valeAlmacen = objeto;
        //}

        private void ViewReportes_Load(object sender, EventArgs e)
        {
            // cargar informacion del logotipo
            string pathLogo = Path.Combine(JaegerManagerPaths.JaegerPath(EnumPaths.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            if (File.Exists(pathLogo))
                logo = HelperFiles.ReadFileByte(pathLogo);
            else
                logo = horizon.GetAsBytes("Jaeger.Reportes.logo-default.png");

            // tipo de reporte
            if (eTypeReport == EnumReporteTipo.ReciboDeCobro)
                this.CrearRecibo();
            else if (eTypeReport == EnumReporteTipo.ReciboDePago)
                this.CrearRecibo();
            else if (this.eTypeReport == EnumReporteTipo.Validation)
                this.CrearValidacion();
            else if (this.eTypeReport == EnumReporteTipo.Accuse)
                this.CrearAcuse();
            else if (this.eTypeReport == EnumReporteTipo.Cfdiv33)
                this.CrearComprobante33();
            else if (this.eTypeReport == EnumReporteTipo.EstadoCuenta)
                this.CreateEstadoDeCuenta();
            //else if (this.eTypeReport == EnumReporteTipo.OrdenCompra)
            //    this.CrearOrdenCompra();
            else if (this.eTypeReport == EnumReporteTipo.ExpedienteEmpleado)
                this.CreateEmpleadoExpediente();
            //if ((this.eTypeReport == EnumReporteTipo.ValeEntrada) | (this.eTypeReport == EnumReporteTipo.ValeSalida))
            //    this.CreateValeAlmacen();
        }

        private void CrearRecibo()
        {
            if (this.eTypeReport == EnumReporteTipo.ReciboDeCobro)
            {
                this.ReportViewer.LocalReport.LoadReportDefinition(this.horizon.GetStream("Jaeger.Reportes.ReciboCobrov20.rdlc"));
                this.ReportViewer.LocalReport.DisplayName = "Recibo de Cobro";
                this.prepoliza.TotalLetra = HelperNumeroALetras.NumeroALetras(DbConvert.ConvertDouble(this.prepoliza.Cargo), 1);
            }
            else
            {
                this.ReportViewer.LocalReport.LoadReportDefinition(this.horizon.GetStream("Jaeger.Reportes.ReciboPagov20.rdlc"));
                this.ReportViewer.LocalReport.DisplayName = "Recibo de Pago";
                this.prepoliza.TotalLetra = HelperNumeroALetras.NumeroALetras(DbConvert.ConvertDouble(this.prepoliza.Abono), 1);
            }

            this.prepoliza.LogoTipo = logo;
            string[] qr = { "&folio=", this.prepoliza.Folio.ToString(), "&rfce=", this.prepoliza.Emisor.Rfc, "&rfcr=", this.prepoliza.Receptor.Rfc, "&fecha=", this.prepoliza.FechaEmision.ToShortDateString(), "&total=", this.prepoliza.Cargo.ToString() };
            this.prepoliza.Cbb = HelperQRCodes.GetQRBytes(qr);

            List<ViewModelPrePoliza> lista = new List<ViewModelPrePoliza>() { this.prepoliza };
            List<PrePolizaCuenta> lista2 = new List<PrePolizaCuenta>() { this.prepoliza.Emisor };
            List<PrePolizaFormaPago> lista3 = new List<PrePolizaFormaPago>() { this.prepoliza.FormaDePago };

            if (this.prepoliza.Comprobantes == null)
                this.prepoliza.Comprobantes = new BindingList<ViewModelPrePolizaComprobante>();

            this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("PrePoliza", (IEnumerable)lista));
            this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("Emisor", (IEnumerable)lista2));
            this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("Receptor", (IEnumerable)new List<PrePolizaCuenta>() { this.prepoliza.Receptor }));
            this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("FormaPago", (IEnumerable)lista3));
            this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("Comprobantes", (IEnumerable)this.prepoliza.Comprobantes));

            this.Finalizar();
        }

        private void CrearValidacion()
        {
            List<ValidateResponse> lista = new List<ValidateResponse>() { this.validateResponse };
            this.ReportViewer.LocalReport.DisplayName = "Reporte de Validación de Comprobantes";
            this.ReportViewer.LocalReport.LoadReportDefinition(horizon.GetStream("Jaeger.Reportes.ReporteValidacion.rdlc"));
            this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ValidationResponse", (IEnumerable)lista));
            this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("Validations", (IEnumerable)this.validateResponse.Validations));
            this.ReportViewer.RefreshReport();
            this.Finalizar();
        }

        private void CrearAcuse()
        {
            List<ViewModelAccuseCancelacion> lista = new List<ViewModelAccuseCancelacion>();
            this.ReportViewer.LocalReport.DisplayName = "Acuse de cancelación de Comprobantes";
            this.ReportViewer.LocalReport.LoadReportDefinition(horizon.GetStream("Jaeger.Reportes.AccuseCancelacion33.rdlc"));
            this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("AccuseCancelacion33", (IEnumerable)lista));
            this.ReportViewer.RefreshReport();
            this.Finalizar();
        }

        private void CrearComprobante33()
        {
            List<Entities.Basico.Comprobante> comprobantes = new List<Entities.Basico.Comprobante>();
            comprobantes.Add(HelperConvertidor.ComprobanteToBasic(this.comprobante33));
            this.ReportViewer.LocalReport.DisplayName = "CFDI ver. 3.3";
            this.ReportViewer.LocalReport.LoadReportDefinition(horizon.GetStream("Jaeger.Reportes.Cfdiv33.rdlc"));
            this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("Comprobante", comprobantes));
            this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("Conceptos", HelperConvertidor.ComprobanteToBasic(this.comprobante33.Conceptos)));
            this.ReportViewer.RefreshReport();
            this.Finalizar();
        }

        private void CreateEstadoDeCuenta()
        {
            List<EstadoDeCuenta> lista = new List<EstadoDeCuenta>() { this.estadoDeCuenta };
            this.ReportViewer.LocalReport.DisplayName = "Estado de Cuenta";
            this.ReportViewer.LocalReport.LoadReportDefinition(horizon.GetStream("Jaeger.Reportes.EstadoDeCuenta.rdlc"));
            this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("EstadoDeCuenta", (IEnumerable)lista));
            this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("Comprobantes", (IEnumerable)this.estadoDeCuenta.Items));
            this.ReportViewer.RefreshReport();
            this.ReportViewer.SetDisplayMode(DisplayMode.PrintLayout);
            this.ReportViewer.ZoomMode = ZoomMode.Percent;
            this.ReportViewer.ZoomPercent = 100;
        }

        //private void CrearOrdenCompra()
        //{
        //    var config = new OrdenCompraConfig();

        //    config.Concepto = config.Concepto.Replace("@folio", this.ordenCompra.Id.ToString("#0000"));

        //    List<ViewModelOrdenCompra> lista = new List<ViewModelOrdenCompra>() { this.ordenCompra };
        //    this.ReportViewer.LocalReport.DisplayName = "Orden de Compra No. " + this.ordenCompra.Id.ToString("#0000");
        //    this.ReportViewer.LocalReport.LoadReportDefinition(horizon.GetStream("Jaeger.Reportes.ReporteOrdenCompra.rdlc"));
        //    this.ReportViewer.LocalReport.SetParameters(new ReportParameter("EntregarEn", ConfigService.Synapsis.Empresa.DomicilioFiscal.ToString()));
        //    this.ReportViewer.LocalReport.SetParameters(new ReportParameter("Concepto", config.Concepto));
        //    this.ReportViewer.LocalReport.SetParameters(new ReportParameter("LeyendaPiePagina", config.LeyendaPiePagina));
        //    this.ReportViewer.LocalReport.SetParameters(new ReportParameter("Logo", System.Convert.ToBase64String(logo)));
        //    this.ReportViewer.LocalReport.SetParameters(new ReportParameter("RFC", ConfigService.Synapsis.Empresa.RFC));
        //    this.ReportViewer.LocalReport.SetParameters(new ReportParameter("Empresa", ConfigService.Synapsis.Empresa.RazonSocial));
        //    this.ReportViewer.LocalReport.SetParameters(new ReportParameter("ImagenQR", HelperQRCodes.GetQRBase64(new string[] { "||OC|", this.ordenCompra.Version, "|",this.ordenCompra.Id.ToString(), "|", this.ordenCompra.FechaEmision.ToShortDateString(), "||" })));
        //    this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("Orden", lista));
        //    this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("Conceptos", this.ordenCompra.Conceptos));
        //    this.Finalizar();
        //}

        private void CreateEmpleadoExpediente()
        {
            List<Jaeger.Nomina.Entities.ViewModelEmpleado> lista = new List<Jaeger.Nomina.Entities.ViewModelEmpleado>() { this.empleado };
            this.horizon = new EmbeddedResources("jaeger_saber_athena");
            this.ReportViewer.LocalReport.DisplayName = "Empleado";
            this.ReportViewer.LocalReport.LoadReportDefinition(horizon.GetStream("Jaeger.Nomina.Reportes.Expediente.rdlc"));
            this.ReportViewer.LocalReport.SetParameters(new ReportParameter("Logo", Convert.ToBase64String(logo)));
            this.ReportViewer.LocalReport.SetParameters(new ReportParameter("Empresa", ConfigService.Synapsis.Empresa.RazonSocial));
            this.ReportViewer.LocalReport.SetParameters(new ReportParameter("RFC", ConfigService.Synapsis.Empresa.RFC));
            this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("Expediente", lista));
            this.Finalizar();
        }

        //private void CreateValeAlmacen()
        //{
        //    var config = new ValeAlmacenConfig();
            
        //    var lista = new List<Jaeger.Edita.V2.Almacen.Entities.ViewModelValeAlmacen>() { this.valeAlmacen };
        //    this.ReportViewer.LocalReport.DisplayName = "Orden de Compra No. " + this.valeAlmacen.Folio.ToString("#0000");
        //    this.ReportViewer.LocalReport.LoadReportDefinition(horizon.GetStream("Jaeger.Reportes.ReporteValeEntradav20.rdlc"));
        //    this.ReportViewer.LocalReport.SetParameters(new ReportParameter("Logo", System.Convert.ToBase64String(logo)));
        //    this.ReportViewer.LocalReport.SetParameters(new ReportParameter("RFC", ConfigService.Synapsis.Empresa.RFC));
        //    this.ReportViewer.LocalReport.SetParameters(new ReportParameter("Empresa", ConfigService.Synapsis.Empresa.RazonSocial));
        //    this.ReportViewer.LocalReport.SetParameters(new ReportParameter("ImagenQR", HelperQRCodes.GetQRBase64(new string[] { "||VL|", this.valeAlmacen.Version, "|", this.valeAlmacen.Folio.ToString(), "|", this.valeAlmacen.FechaEmision.ToShortDateString(), "||" })));
        //    this.ReportViewer.LocalReport.SetParameters(new ReportParameter("LeyendaPiePagina", config.LeyendaPiePagina));
        //    this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ValeAlmacen", lista));
        //    this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("Conceptos", this.valeAlmacen.Conceptos));
        //    this.Finalizar();
        //}

        private void Finalizar()
        {
            this.ReportViewer.RefreshReport();
            this.ReportViewer.SetDisplayMode(DisplayMode.PrintLayout);
            this.ReportViewer.ZoomMode = ZoomMode.Percent;
            this.ReportViewer.ZoomPercent = 100;
            this.ReportViewer = null;
        }
    }
}
