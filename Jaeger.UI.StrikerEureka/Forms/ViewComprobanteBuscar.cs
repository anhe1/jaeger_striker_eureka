﻿using System;
using Jaeger.Helpers;
using Jaeger.Edita.V2;
using Telerik.WinControls.Enumerations;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views
{
    public partial class ViewComprobanteBuscar : Telerik.WinControls.UI.RadForm
    {
        private SqlSugarComprobanteFiscal data;
        private string porRFC;
        private ViewModelComprobanteSingle seleccionado;

        public ViewComprobanteBuscar(string rfc)
        {
            InitializeComponent();
            this.porRFC = rfc;
        }

        private void ViewComprobanteBuscar_Load(object sender, EventArgs e)
        {
            this.data = new SqlSugarComprobanteFiscal(ConfigService.Synapsis.RDS.Edita);
            this.GridData.TelerikGridCommon();
        }

        private void ToolBarButtonBuscar_Click(object sender, EventArgs e)
        {
            this.GridData.DataSource = this.data.GetListSingleBy(Edita.V2.CFDI.Enums.EnumCfdiSubType.Emitido, this.porRFC, this.ToolBarTextBoxBuscar.Text);
        }

        private void ToolBarButtonSeleccionar_Click(object sender, EventArgs e)
        {
            if (this.GridData.CurrentRow != null)
            {
                this.seleccionado = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteSingle;
                this.ToolBarButtonCerrar.PerformClick();
            }
        }

        private void ToolBarButtonFiltro_Click(object sender, EventArgs e)
        {
            this.GridData.ShowFilteringRow = this.ToolBarButtonFiltro.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false)
                this.GridData.FilterDescriptors.Clear();
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public ComprobanteCfdiRelacionadosCfdiRelacionado ShowForm()
        {
            this.ShowDialog();
            if (this.seleccionado != null)
            {
                ComprobanteCfdiRelacionadosCfdiRelacionado relacionado = new ComprobanteCfdiRelacionadosCfdiRelacionado();
                relacionado.Folio = this.seleccionado.Folio;
                relacionado.FechaEmision = this.seleccionado.FechaEmision;
                relacionado.EstadoSAT = this.seleccionado.Estado;
                relacionado.FormaPago = this.seleccionado.FormaPago;
                relacionado.IdDocumento = this.seleccionado.IdDocumento;
                relacionado.MetodoPago = this.seleccionado.MetodoPago;
                relacionado.Moneda = this.seleccionado.Moneda;
                relacionado.Nombre = this.seleccionado.Receptor;
                relacionado.RFC = this.seleccionado.ReceptorRFC;
                relacionado.Serie = this.seleccionado.Serie;
                relacionado.SubTipo = this.seleccionado.SubTipo;
                relacionado.TipoCambio = this.seleccionado.TipoCambio;
                relacionado.Total = this.seleccionado.Total;
                return relacionado;
            }
            return null;
        }

        public ComplementoPagoDoctoRelacionado PagoDoctoRelacionado()
        {
            this.ShowDialog();
            if (this.seleccionado != null)
            {
                ComplementoPagoDoctoRelacionado relacionado = new ComplementoPagoDoctoRelacionado();
                relacionado.Folio = this.seleccionado.Folio;
                relacionado.FechaEmision = this.seleccionado.FechaEmision;
                relacionado.EstadoSAT = this.seleccionado.Estado;
                relacionado.FormaPago = this.seleccionado.FormaPago;
                relacionado.IdDocumento = this.seleccionado.IdDocumento;
                relacionado.MetodoPago = this.seleccionado.MetodoPago;
                relacionado.Moneda = this.seleccionado.Moneda;
                relacionado.Nombre = this.seleccionado.Receptor;
                relacionado.RFC = this.seleccionado.ReceptorRFC;
                relacionado.Serie = this.seleccionado.Serie;
                relacionado.SubTipo = this.seleccionado.SubTipo;
                relacionado.TipoCambio = this.seleccionado.TipoCambio;
                relacionado.Total = this.seleccionado.Total;
                relacionado.ImpSaldoAnt = this.seleccionado.Saldo;
                return relacionado;
            }
            return null;
        }
    }
}
