﻿namespace Jaeger.Views
{
    partial class ViewComprobanteProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.ToolBarButtonAgregar = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarNuevoValeEntrada = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarNuevoProducto = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarNuevo = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarNuevoValeSalida = new Telerik.WinControls.UI.RadMenuItem();
            this.MenuContextual = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.ToolBarButtonCopiar = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonDuplicar = new Telerik.WinControls.UI.RadMenuItem();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsSpinnerWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement();
            this.Separador = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarButtonNuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonEditar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonEliminar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonFiltrar = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarButtonExportar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            this.GridData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBarButtonAgregar
            // 
            this.ToolBarButtonAgregar.Name = "ToolBarButtonAgregar";
            this.ToolBarButtonAgregar.Text = "Agregar ubicación";
            this.ToolBarButtonAgregar.UseCompatibleTextRendering = false;
            // 
            // ToolBarNuevoValeEntrada
            // 
            this.ToolBarNuevoValeEntrada.Name = "ToolBarNuevoValeEntrada";
            this.ToolBarNuevoValeEntrada.Text = "Vale de Entrada";
            this.ToolBarNuevoValeEntrada.UseCompatibleTextRendering = false;
            // 
            // ToolBarNuevoProducto
            // 
            this.ToolBarNuevoProducto.Name = "ToolBarNuevoProducto";
            this.ToolBarNuevoProducto.Text = "Producto";
            this.ToolBarNuevoProducto.UseCompatibleTextRendering = false;
            // 
            // ToolBarNuevo
            // 
            this.ToolBarNuevo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarNuevoProducto,
            this.ToolBarNuevoValeEntrada,
            this.ToolBarNuevoValeSalida});
            this.ToolBarNuevo.Name = "ToolBarNuevo";
            this.ToolBarNuevo.Text = "Nuevo";
            this.ToolBarNuevo.UseCompatibleTextRendering = false;
            // 
            // ToolBarNuevoValeSalida
            // 
            this.ToolBarNuevoValeSalida.Name = "ToolBarNuevoValeSalida";
            this.ToolBarNuevoValeSalida.Text = "Vale de Salida";
            this.ToolBarNuevoValeSalida.UseCompatibleTextRendering = false;
            // 
            // MenuContextual
            // 
            this.MenuContextual.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonCopiar,
            this.ToolBarButtonDuplicar,
            this.ToolBarNuevo,
            this.ToolBarButtonAgregar});
            // 
            // ToolBarButtonCopiar
            // 
            this.ToolBarButtonCopiar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_copiar_portapapeles;
            this.ToolBarButtonCopiar.Name = "ToolBarButtonCopiar";
            this.ToolBarButtonCopiar.Text = "Copiar";
            this.ToolBarButtonCopiar.UseCompatibleTextRendering = false;
            // 
            // ToolBarButtonDuplicar
            // 
            this.ToolBarButtonDuplicar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_copiar;
            this.ToolBarButtonDuplicar.Name = "ToolBarButtonDuplicar";
            this.ToolBarButtonDuplicar.Text = "Duplicar";
            this.ToolBarButtonDuplicar.UseCompatibleTextRendering = false;
            // 
            // GridData
            // 
            this.GridData.Controls.Add(this.Espera);
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 63);
            // 
            // 
            // 
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdModelo";
            gridViewTextBoxColumn1.FormatString = "{0:PT00000#}";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdModelo";
            gridViewTextBoxColumn1.Width = 65;
            gridViewTextBoxColumn2.FieldName = "Tipo";
            gridViewTextBoxColumn2.HeaderText = "Tipo";
            gridViewTextBoxColumn2.Name = "Tipo";
            gridViewTextBoxColumn2.Width = 80;
            gridViewTextBoxColumn3.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn3.HeaderText = "Identificador";
            gridViewTextBoxColumn3.Name = "NoIdentificacion";
            gridViewTextBoxColumn3.Width = 95;
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "A";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewTextBoxColumn4.FieldName = "Descripcion";
            gridViewTextBoxColumn4.HeaderText = "Descripción";
            gridViewTextBoxColumn4.Name = "Modelo";
            gridViewTextBoxColumn4.Width = 350;
            gridViewTextBoxColumn5.FieldName = "Marca";
            gridViewTextBoxColumn5.HeaderText = "Marca";
            gridViewTextBoxColumn5.Name = "Marca";
            gridViewTextBoxColumn5.Width = 200;
            gridViewTextBoxColumn6.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn6.HeaderText = "Clave\r\nProd. Serv.";
            gridViewTextBoxColumn6.Name = "ClaveProdServ";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.Width = 80;
            gridViewComboBoxColumn1.FieldName = "ClaveUnidad";
            gridViewComboBoxColumn1.HeaderText = "Clave\r\nUnidad";
            gridViewComboBoxColumn1.Name = "ClaveUnidad";
            gridViewComboBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewComboBoxColumn1.Width = 80;
            gridViewComboBoxColumn2.FieldName = "Unidad";
            gridViewComboBoxColumn2.HeaderText = "Unidad";
            gridViewComboBoxColumn2.Name = "Unidad";
            gridViewComboBoxColumn2.Width = 75;
            gridViewTextBoxColumn7.DataType = typeof(decimal);
            gridViewTextBoxColumn7.FieldName = "Unitario";
            gridViewTextBoxColumn7.FormatString = "{0:n}";
            gridViewTextBoxColumn7.HeaderText = "Unitario";
            gridViewTextBoxColumn7.Name = "Unitario";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn7.Width = 85;
            gridViewTextBoxColumn8.FieldName = "CtaPredial";
            gridViewTextBoxColumn8.HeaderText = "Cta. Predial";
            gridViewTextBoxColumn8.Name = "CtaPredial";
            gridViewTextBoxColumn8.Width = 85;
            gridViewTextBoxColumn9.FieldName = "NumRequerimiento";
            gridViewTextBoxColumn9.HeaderText = "Núm Req.";
            gridViewTextBoxColumn9.Name = "NumRequerimiento";
            gridViewTextBoxColumn9.Width = 85;
            gridViewTextBoxColumn10.FieldName = "Creo";
            gridViewTextBoxColumn10.HeaderText = "Creó";
            gridViewTextBoxColumn10.Name = "Creo";
            gridViewTextBoxColumn10.Width = 75;
            gridViewTextBoxColumn11.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn11.FieldName = "FechaNuevo";
            gridViewTextBoxColumn11.FormatString = "{0:d}";
            gridViewTextBoxColumn11.HeaderText = "Fec. Sistema";
            gridViewTextBoxColumn11.Name = "FechaNuevo";
            gridViewTextBoxColumn11.Width = 75;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewComboBoxColumn1,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(962, 495);
            this.GridData.TabIndex = 4;
            this.GridData.RowsChanged += new Telerik.WinControls.UI.GridViewCollectionChangedEventHandler(this.GridData_RowsChanged);
            // 
            // Espera
            // 
            this.Espera.AssociatedControl = this.GridData;
            this.Espera.Location = new System.Drawing.Point(392, 191);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(70, 70);
            this.Espera.TabIndex = 1;
            this.Espera.Text = "radWaitingBar1";
            this.Espera.Visible = false;
            this.Espera.WaitingIndicators.Add(this.dotsSpinnerWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 100;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsSpinner;
            // 
            // dotsSpinnerWaitingBarIndicatorElement1
            // 
            this.dotsSpinnerWaitingBarIndicatorElement1.Name = "dotsSpinnerWaitingBarIndicatorElement1";
            // 
            // Separador
            // 
            this.Separador.DisplayName = "Separador";
            this.Separador.Name = "Separador";
            this.Separador.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separador.VisibleInOverflowMenu = false;
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "Barra de Herramientas";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarButtonNuevo,
            this.ToolBarButtonEditar,
            this.ToolBarButtonEliminar,
            this.Separador,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonFiltrar,
            this.ToolBarButtonExportar,
            this.ToolBarButtonCerrar});
            this.ToolBar.Name = "ToolBar";
            // 
            // ToolBarButtonNuevo
            // 
            this.ToolBarButtonNuevo.DisplayName = "Nuevo";
            this.ToolBarButtonNuevo.DrawText = true;
            this.ToolBarButtonNuevo.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_archivo_nuevo;
            this.ToolBarButtonNuevo.Name = "ToolBarButtonNuevo";
            this.ToolBarButtonNuevo.Text = "Nuevo";
            this.ToolBarButtonNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonNuevo.Click += new System.EventHandler(this.ToolBarButtonNuevo_Click);
            // 
            // ToolBarButtonEditar
            // 
            this.ToolBarButtonEditar.DisplayName = "Editar";
            this.ToolBarButtonEditar.DrawText = true;
            this.ToolBarButtonEditar.Enabled = false;
            this.ToolBarButtonEditar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_edit_file;
            this.ToolBarButtonEditar.Name = "ToolBarButtonEditar";
            this.ToolBarButtonEditar.Text = "Editar";
            this.ToolBarButtonEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonEditar.Click += new System.EventHandler(this.ToolBarButtonEditar_Click);
            // 
            // ToolBarButtonEliminar
            // 
            this.ToolBarButtonEliminar.DisplayName = "Eliminar";
            this.ToolBarButtonEliminar.DrawText = true;
            this.ToolBarButtonEliminar.Enabled = false;
            this.ToolBarButtonEliminar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_delete_file;
            this.ToolBarButtonEliminar.Name = "ToolBarButtonEliminar";
            this.ToolBarButtonEliminar.Text = "Eliminar";
            this.ToolBarButtonEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonEliminar.Click += new System.EventHandler(this.ToolBarButtonEliminar_Click);
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.DisplayName = "Actualizar";
            this.ToolBarButtonActualizar.DrawText = true;
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_actualizar;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.ToolBarButtonActualizar_Click);
            // 
            // ToolBarButtonFiltrar
            // 
            this.ToolBarButtonFiltrar.DisplayName = "Filtrar";
            this.ToolBarButtonFiltrar.DrawText = true;
            this.ToolBarButtonFiltrar.Enabled = false;
            this.ToolBarButtonFiltrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_filtrar;
            this.ToolBarButtonFiltrar.Name = "ToolBarButtonFiltrar";
            this.ToolBarButtonFiltrar.Text = "Filtrar";
            this.ToolBarButtonFiltrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonFiltrar.Click += new System.EventHandler(this.ToolBarButtonFiltrar_Click);
            // 
            // ToolBarButtonExportar
            // 
            this.ToolBarButtonExportar.DisplayName = "Exportar";
            this.ToolBarButtonExportar.DrawText = true;
            this.ToolBarButtonExportar.Enabled = false;
            this.ToolBarButtonExportar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_excel;
            this.ToolBarButtonExportar.Name = "ToolBarButtonExportar";
            this.ToolBarButtonExportar.Text = "Exportar";
            this.ToolBarButtonExportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonExportar.Click += new System.EventHandler(this.ToolBarButtonExportar_Click);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "Cerrar";
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_cerrar;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            this.commandBarRowElement1.Text = "";
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(962, 63);
            this.radCommandBar1.TabIndex = 3;
            // 
            // ViewComprobanteProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 558);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "ViewComprobanteProductos";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ViewComprobanteProductos";
            this.Load += new System.EventHandler(this.ViewComprobanteProductos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.GridData.ResumeLayout(false);
            this.GridData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonAgregar;
        private Telerik.WinControls.UI.RadMenuItem ToolBarNuevoValeEntrada;
        private Telerik.WinControls.UI.RadMenuItem ToolBarNuevoProducto;
        private Telerik.WinControls.UI.RadMenuItem ToolBarNuevo;
        private Telerik.WinControls.UI.RadMenuItem ToolBarNuevoValeSalida;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonDuplicar;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonCopiar;
        private Telerik.WinControls.UI.RadContextMenu MenuContextual;
        private Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCerrar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonExportar;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltrar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonActualizar;
        private Telerik.WinControls.UI.CommandBarSeparator Separador;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonEliminar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonEditar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonNuevo;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement dotsSpinnerWaitingBarIndicatorElement1;
    }
}
