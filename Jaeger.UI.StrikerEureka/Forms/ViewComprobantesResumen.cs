﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.Pivot.Core;
using Telerik.Pivot.Core.Aggregates;
using Telerik.WinControls.UI;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2;
using System.Collections.Generic;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Views
{
    public partial class ViewComprobantesResumen : RadForm
    {
        private BackgroundWorker preparar;
        private BackgroundWorker consulta;
        private LocalDataSourceProvider dataProvider;
        private SqlSugarComprobanteFiscal data;
        private EnumCfdiSubType comprobanteTipo;
        private List<ViewModelEstadoCuenta> datos;

        public ViewComprobantesResumen(EnumCfdiSubType objType)
        {
            InitializeComponent();
            this.comprobanteTipo = objType;
        }

        private void ComprobantesResumen_Load(object sender, EventArgs e)
        {
            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += Preparar_DoWork;
            this.preparar.RunWorkerCompleted += Preparar_RunWorkerCompleted;

            this.consulta = new BackgroundWorker();
            this.consulta.DoWork += Consulta_DoWork;
            this.consulta.RunWorkerCompleted += Consulta_RunWorkerCompleted;

            this.preparar.RunWorkerAsync();   
        }
 
        private void Preparar_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int anio = 2013; anio <= DateTime.Now.Year; anio = checked(anio + 1))
            {
                RadMenuItem oButtonYear = new RadMenuItem() { Text = anio.ToString() };
                this.ToolBarButtonEjercicio.Items.Add(oButtonYear);
                oButtonYear.Click += new EventHandler(this.ToolBarButtonEjercicio_Click);
            }

            this.dataProvider = new LocalDataSourceProvider();
            this.dataProvider.Culture = new System.Globalization.CultureInfo("es-MX");
            this.dataProvider.AggregateDescriptions.Add(new PropertyAggregateDescription() { PropertyName = "Total", AggregateFunction = AggregateFunctions.Sum });
            this.dataProvider.AggregateDescriptions.Add(new PropertyAggregateDescription() { PropertyName = "Acumulado", AggregateFunction = AggregateFunctions.Sum });
            this.dataProvider.ColumnGroupDescriptions.Add(new DateTimeGroupDescription() { PropertyName = "FechaEmision", CustomName = "Año", Step = DateTimeStep.Year, GroupComparer = new GroupNameComparer() });
            this.dataProvider.ColumnGroupDescriptions.Add(new DateTimeGroupDescription() { PropertyName = "FechaEmision", CustomName = "Mes", Step = DateTimeStep.Month, GroupComparer = new GroupNameComparer() });
            this.dataProvider.RowGroupDescriptions.Add(new PropertyGroupDescription() { PropertyName = "Receptor", GroupComparer = new GroupNameComparer() });
            this.data = new SqlSugarComprobanteFiscal(ConfigService.Synapsis.RDS.Edita);
        }

        private void ToolBarButtonEjercicio_Click(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                this.ToolBarButtonEjercicio.DefaultItem = button;
                this.ToolBarButtonEjercicio.Text = button.Text;
                this.ToolBarButtonEjercicio.PerformClick();
            }
        }
 
        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.ToolBarButtonEjercicio.Text = DateTime.Now.Year.ToString();
        }

        private void Consulta_DoWork(object sender, DoWorkEventArgs e)
        {
            this.datos = data.Resumen(this.comprobanteTipo, Helpers.DbConvert.ConvertInt32(this.ToolBarButtonEjercicio.Text), Edita.Enums.EnumMonthsOfYear.None);
        }

        private void Consulta_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.dataProvider.ItemsSource = this.datos;
            this.GridData.DataProvider = this.dataProvider;
            this.Espera.StopWaiting();
            this.Espera.Visible = false;
        }

        #region barra de herramientas

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e)
        {
            this.Espera.StartWaiting();
            this.Espera.Visible = true;
            this.consulta.RunWorkerAsync();
        }

        private void ToolBarButtonExportar_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Excel XLSX|*.xlsx";
            saveFileDialog1.Title = "Export to File";
            saveFileDialog1.ShowDialog();
            if (saveFileDialog1.FileName != "")
            {
                UI.Common.Services.HelperTelerikExport.RunExportPivotGrid(GridData, saveFileDialog1.FileName, "Resumen");
                try
                {
                    System.Diagnostics.Process.Start(saveFileDialog1.FileName);
                }
                finally
                {
                }
            }
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

    }
}
