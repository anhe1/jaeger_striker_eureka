﻿namespace Jaeger.Views
{
    partial class ViewComprobantesBackup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewComprobantesBackup));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.ChkCrearLog = new Telerik.WinControls.UI.RadCheckBox();
            this.DotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            this.CommandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarLog = new Telerik.WinControls.UI.CommandBarHostItem();
            this.CommandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.CommandBarLabel2 = new Telerik.WinControls.UI.CommandBarLabel();
            this.CommandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarLabelTipoComprobante = new Telerik.WinControls.UI.CommandBarLabel();
            this.CommandBarDescarga = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarTipoComprobante = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ButtonComprobanteEmitido = new Telerik.WinControls.UI.RadMenuItem();
            this.ButtonComprobanteRecibido = new Telerik.WinControls.UI.RadMenuItem();
            this.ButtonComprobanteNomina = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarPorFecha = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ButtonFechaEmision = new Telerik.WinControls.UI.RadMenuItem();
            this.ButtonFechaTimbre = new Telerik.WinControls.UI.RadMenuItem();
            this.ButtonFechaValidacion = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.CommandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.RadCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.ButtonDescarga = new Telerik.WinControls.UI.RadButton();
            this.ChkNormaliza = new Telerik.WinControls.UI.RadCheckBox();
            this.RadLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.TxbPath = new Telerik.WinControls.UI.RadTextBox();
            this.DotsSpinnerWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.LabelResult = new Telerik.WinControls.UI.RadLabelElement();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.BarraEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.radCheckBox1 = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCrearLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonDescarga)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNormaliza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbPath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkCrearLog
            // 
            this.ChkCrearLog.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkCrearLog.Location = new System.Drawing.Point(616, 36);
            this.ChkCrearLog.Name = "ChkCrearLog";
            this.ChkCrearLog.Size = new System.Drawing.Size(68, 18);
            this.ChkCrearLog.TabIndex = 22;
            this.ChkCrearLog.Text = "Crear Log";
            this.ChkCrearLog.TextWrap = true;
            this.ChkCrearLog.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // DotsLineWaitingBarIndicatorElement1
            // 
            this.DotsLineWaitingBarIndicatorElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.DotsLineWaitingBarIndicatorElement1.Name = "DotsLineWaitingBarIndicatorElement1";
            this.DotsLineWaitingBarIndicatorElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.DotsLineWaitingBarIndicatorElement1.UseCompatibleTextRendering = false;
            // 
            // CommandBarSeparator3
            // 
            this.CommandBarSeparator3.DisplayName = "CommandBarSeparator3";
            this.CommandBarSeparator3.Name = "CommandBarSeparator3";
            this.CommandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // ToolBarLog
            // 
            this.ToolBarLog.DisplayName = "CommandBarHostItem1";
            this.ToolBarLog.Name = "ToolBarLog";
            this.ToolBarLog.Text = "";
            // 
            // CommandBarSeparator2
            // 
            this.CommandBarSeparator2.DisplayName = "CommandBarSeparator2";
            this.CommandBarSeparator2.Name = "CommandBarSeparator2";
            this.CommandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // CommandBarLabel2
            // 
            this.CommandBarLabel2.DisplayName = "CommandBarLabel2";
            this.CommandBarLabel2.Name = "CommandBarLabel2";
            this.CommandBarLabel2.Text = "Obtener por fecha:";
            // 
            // CommandBarSeparator1
            // 
            this.CommandBarSeparator1.DisplayName = "CommandBarSeparator1";
            this.CommandBarSeparator1.Name = "CommandBarSeparator1";
            this.CommandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarLabelTipoComprobante
            // 
            this.ToolBarLabelTipoComprobante.DisplayName = "CommandBarLabel1";
            this.ToolBarLabelTipoComprobante.Name = "ToolBarLabelTipoComprobante";
            this.ToolBarLabelTipoComprobante.Text = "Tipo de Comprobante:";
            // 
            // CommandBarDescarga
            // 
            this.CommandBarDescarga.DisplayName = "CommandBarStripElement1";
            this.CommandBarDescarga.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelTipoComprobante,
            this.ToolBarTipoComprobante,
            this.CommandBarSeparator1,
            this.CommandBarLabel2,
            this.ToolBarPorFecha,
            this.CommandBarSeparator2,
            this.ToolBarLog,
            this.CommandBarSeparator3,
            this.ToolBarCerrar});
            this.CommandBarDescarga.Name = "CommandBarDescarga";
            // 
            // ToolBarTipoComprobante
            // 
            this.ToolBarTipoComprobante.DefaultItem = null;
            this.ToolBarTipoComprobante.DisplayName = "CommandBarSplitButton1";
            this.ToolBarTipoComprobante.DrawImage = false;
            this.ToolBarTipoComprobante.DrawText = true;
            this.ToolBarTipoComprobante.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarTipoComprobante.Image")));
            this.ToolBarTipoComprobante.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ButtonComprobanteEmitido,
            this.ButtonComprobanteRecibido,
            this.ButtonComprobanteNomina});
            this.ToolBarTipoComprobante.MinSize = new System.Drawing.Size(82, 26);
            this.ToolBarTipoComprobante.Name = "ToolBarTipoComprobante";
            this.ToolBarTipoComprobante.Text = "Selecciona";
            // 
            // ButtonComprobanteEmitido
            // 
            this.ButtonComprobanteEmitido.Name = "ButtonComprobanteEmitido";
            this.ButtonComprobanteEmitido.Tag = "Emitido";
            this.ButtonComprobanteEmitido.Text = "Emitido";
            this.ButtonComprobanteEmitido.Click += new System.EventHandler(this.ButtonTipoComprobante_Click);
            // 
            // ButtonComprobanteRecibido
            // 
            this.ButtonComprobanteRecibido.Name = "ButtonComprobanteRecibido";
            this.ButtonComprobanteRecibido.Tag = "Recibido";
            this.ButtonComprobanteRecibido.Text = "Recibido";
            this.ButtonComprobanteRecibido.Click += new System.EventHandler(this.ButtonTipoComprobante_Click);
            // 
            // ButtonComprobanteNomina
            // 
            this.ButtonComprobanteNomina.Name = "ButtonComprobanteNomina";
            this.ButtonComprobanteNomina.Tag = "Nomina";
            this.ButtonComprobanteNomina.Text = "Nómina";
            this.ButtonComprobanteNomina.Click += new System.EventHandler(this.ButtonTipoComprobante_Click);
            // 
            // ToolBarPorFecha
            // 
            this.ToolBarPorFecha.DefaultItem = null;
            this.ToolBarPorFecha.DisplayName = "CommandBarSplitButton2";
            this.ToolBarPorFecha.DrawImage = false;
            this.ToolBarPorFecha.DrawText = true;
            this.ToolBarPorFecha.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarPorFecha.Image")));
            this.ToolBarPorFecha.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ButtonFechaEmision,
            this.ButtonFechaTimbre,
            this.ButtonFechaValidacion});
            this.ToolBarPorFecha.MinSize = new System.Drawing.Size(120, 26);
            this.ToolBarPorFecha.Name = "ToolBarPorFecha";
            this.ToolBarPorFecha.Text = "Selecciona";
            // 
            // ButtonFechaEmision
            // 
            this.ButtonFechaEmision.Name = "ButtonFechaEmision";
            this.ButtonFechaEmision.Tag = "FechaEmision";
            this.ButtonFechaEmision.Text = "Fecha Emisión";
            this.ButtonFechaEmision.UseCompatibleTextRendering = false;
            this.ButtonFechaEmision.Click += new System.EventHandler(this.ButtonPorFechaDe_Click);
            // 
            // ButtonFechaTimbre
            // 
            this.ButtonFechaTimbre.Name = "ButtonFechaTimbre";
            this.ButtonFechaTimbre.Tag = "FechaTimbre";
            this.ButtonFechaTimbre.Text = "Fecha de Timbre";
            this.ButtonFechaTimbre.UseCompatibleTextRendering = false;
            this.ButtonFechaTimbre.Click += new System.EventHandler(this.ButtonPorFechaDe_Click);
            // 
            // ButtonFechaValidacion
            // 
            this.ButtonFechaValidacion.Name = "ButtonFechaValidacion";
            this.ButtonFechaValidacion.Tag = "FechaValidacion";
            this.ButtonFechaValidacion.Text = "Fecha Validación";
            this.ButtonFechaValidacion.UseCompatibleTextRendering = false;
            this.ButtonFechaValidacion.Click += new System.EventHandler(this.ButtonPorFechaDe_Click);
            // 
            // ToolBarCerrar
            // 
            this.ToolBarCerrar.DisplayName = "CommandBarButton1";
            this.ToolBarCerrar.DrawText = true;
            this.ToolBarCerrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_cerrar;
            this.ToolBarCerrar.Name = "ToolBarCerrar";
            this.ToolBarCerrar.Text = "Cerrar";
            this.ToolBarCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarCerrar.Click += new System.EventHandler(this.ToolBarCerrar_Click);
            // 
            // CommandBarRowElement1
            // 
            this.CommandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement1.Name = "CommandBarRowElement1";
            this.CommandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.CommandBarDescarga});
            this.CommandBarRowElement1.Text = "";
            // 
            // RadCommandBar1
            // 
            this.RadCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar1.Name = "RadCommandBar1";
            this.RadCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement1});
            this.RadCommandBar1.Size = new System.Drawing.Size(599, 30);
            this.RadCommandBar1.TabIndex = 21;
            // 
            // ButtonDescarga
            // 
            this.ButtonDescarga.Location = new System.Drawing.Point(462, 473);
            this.ButtonDescarga.Name = "ButtonDescarga";
            this.ButtonDescarga.Size = new System.Drawing.Size(137, 24);
            this.ButtonDescarga.TabIndex = 20;
            this.ButtonDescarga.Text = "Descargar";
            this.ButtonDescarga.Click += new System.EventHandler(this.ButtonDescarga_Click);
            // 
            // ChkNormaliza
            // 
            this.ChkNormaliza.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkNormaliza.Location = new System.Drawing.Point(82, 476);
            this.ChkNormaliza.Name = "ChkNormaliza";
            this.ChkNormaliza.Size = new System.Drawing.Size(183, 18);
            this.ChkNormaliza.TabIndex = 19;
            this.ChkNormaliza.Text = "Normalizar nombres de carpetas";
            this.ChkNormaliza.TextWrap = true;
            this.ChkNormaliza.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // RadLabel3
            // 
            this.RadLabel3.Location = new System.Drawing.Point(12, 448);
            this.RadLabel3.Name = "RadLabel3";
            this.RadLabel3.Size = new System.Drawing.Size(64, 18);
            this.RadLabel3.TabIndex = 18;
            this.RadLabel3.Text = "Guardar en:";
            // 
            // TxbPath
            // 
            this.TxbPath.Location = new System.Drawing.Point(82, 447);
            this.TxbPath.Name = "TxbPath";
            this.TxbPath.NullText = "Selecciona ...";
            this.TxbPath.Size = new System.Drawing.Size(517, 20);
            this.TxbPath.TabIndex = 17;
            // 
            // DotsSpinnerWaitingBarIndicatorElement1
            // 
            this.DotsSpinnerWaitingBarIndicatorElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.DotsSpinnerWaitingBarIndicatorElement1.Name = "DotsSpinnerWaitingBarIndicatorElement1";
            this.DotsSpinnerWaitingBarIndicatorElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.DotsSpinnerWaitingBarIndicatorElement1.UseCompatibleTextRendering = false;
            // 
            // Espera
            // 
            this.Espera.AssociatedControl = this.GridData;
            this.Espera.Location = new System.Drawing.Point(246, 140);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(70, 70);
            this.Espera.TabIndex = 23;
            this.Espera.Text = "RadWaitingBar1";
            this.Espera.WaitingIndicators.Add(this.DotsSpinnerWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 100;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsSpinner;
            ((Telerik.WinControls.UI.RadWaitingBarElement)(this.Espera.GetChildAt(0))).WaitingSpeed = 100;
            ((Telerik.WinControls.UI.WaitingBarContentElement)(this.Espera.GetChildAt(0).GetChildAt(0))).WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsSpinner;
            ((Telerik.WinControls.UI.WaitingBarSeparatorElement)(this.Espera.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Dash = false;
            // 
            // GridData
            // 
            this.GridData.Location = new System.Drawing.Point(0, 36);
            // 
            // 
            // 
            this.GridData.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.FieldName = "Index";
            gridViewTextBoxColumn1.HeaderText = "Index";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Index";
            gridViewTextBoxColumn2.FieldName = "Parent";
            gridViewTextBoxColumn2.HeaderText = "Parent";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "Parent";
            gridViewTextBoxColumn3.FieldName = "Name";
            gridViewTextBoxColumn3.HeaderText = "Año / Mes";
            gridViewTextBoxColumn3.Name = "Name";
            gridViewTextBoxColumn4.FieldName = "Type";
            gridViewTextBoxColumn4.HeaderText = "Tipo";
            gridViewTextBoxColumn4.Name = "Type";
            gridViewTextBoxColumn5.FieldName = "Count";
            gridViewTextBoxColumn5.FormatString = "{0:#,###0} elemento(s)";
            gridViewTextBoxColumn5.HeaderText = "Cantidad";
            gridViewTextBoxColumn5.Name = "Count";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn6.FieldName = "Year";
            gridViewTextBoxColumn6.HeaderText = "Año";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "Year";
            gridViewTextBoxColumn7.FieldName = "Month";
            gridViewTextBoxColumn7.HeaderText = "Mes";
            gridViewTextBoxColumn7.IsVisible = false;
            gridViewTextBoxColumn7.Name = "Month";
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.ShowGroupPanel = false;
            this.GridData.Size = new System.Drawing.Size(599, 406);
            this.GridData.TabIndex = 16;
            this.GridData.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.GridData_CellFormatting);
            this.GridData.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.GridData_ViewCellFormatting);
            this.GridData.RowsChanged += new Telerik.WinControls.UI.GridViewCollectionChangedEventHandler(this.GridData_RowsChanged);
            // 
            // LabelResult
            // 
            this.LabelResult.Name = "LabelResult";
            this.BarraEstado.SetSpring(this.LabelResult, false);
            this.LabelResult.Text = "";
            this.LabelResult.TextWrap = true;
            this.LabelResult.UseCompatibleTextRendering = false;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "Open Folder.png");
            this.ImageList1.Images.SetKeyName(1, "Live Folder.png");
            this.ImageList1.Images.SetKeyName(2, "Document.png");
            // 
            // BarraEstado
            // 
            this.BarraEstado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.LabelResult});
            this.BarraEstado.Location = new System.Drawing.Point(0, 507);
            this.BarraEstado.Name = "BarraEstado";
            this.BarraEstado.Size = new System.Drawing.Size(599, 26);
            this.BarraEstado.SizingGrip = false;
            this.BarraEstado.TabIndex = 15;
            // 
            // radCheckBox1
            // 
            this.radCheckBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox1.Location = new System.Drawing.Point(285, 476);
            this.radCheckBox1.Name = "radCheckBox1";
            this.radCheckBox1.Size = new System.Drawing.Size(130, 18);
            this.radCheckBox1.TabIndex = 24;
            this.radCheckBox1.Text = "Método no sincrónico";
            this.radCheckBox1.TextWrap = true;
            this.radCheckBox1.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // ViewComprobantesBackup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 533);
            this.Controls.Add(this.radCheckBox1);
            this.Controls.Add(this.ChkCrearLog);
            this.Controls.Add(this.RadCommandBar1);
            this.Controls.Add(this.ButtonDescarga);
            this.Controls.Add(this.ChkNormaliza);
            this.Controls.Add(this.RadLabel3);
            this.Controls.Add(this.TxbPath);
            this.Controls.Add(this.Espera);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.BarraEstado);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewComprobantesBackup";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Backup de Comprobantes";
            this.Load += new System.EventHandler(this.ViewComprobantesBackup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ChkCrearLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonDescarga)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNormaliza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbPath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Telerik.WinControls.UI.RadCheckBox ChkCrearLog;
        internal Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement DotsLineWaitingBarIndicatorElement1;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarCerrar;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator3;
        internal Telerik.WinControls.UI.CommandBarHostItem ToolBarLog;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator2;
        internal Telerik.WinControls.UI.RadMenuItem ButtonFechaValidacion;
        internal Telerik.WinControls.UI.RadMenuItem ButtonFechaTimbre;
        internal Telerik.WinControls.UI.RadMenuItem ButtonFechaEmision;
        internal Telerik.WinControls.UI.CommandBarSplitButton ToolBarPorFecha;
        internal Telerik.WinControls.UI.CommandBarLabel CommandBarLabel2;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator1;
        internal Telerik.WinControls.UI.RadMenuItem ButtonComprobanteNomina;
        internal Telerik.WinControls.UI.RadMenuItem ButtonComprobanteRecibido;
        internal Telerik.WinControls.UI.RadMenuItem ButtonComprobanteEmitido;
        internal Telerik.WinControls.UI.CommandBarSplitButton ToolBarTipoComprobante;
        internal Telerik.WinControls.UI.CommandBarLabel ToolBarLabelTipoComprobante;
        internal Telerik.WinControls.UI.CommandBarStripElement CommandBarDescarga;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement1;
        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar1;
        internal Telerik.WinControls.UI.RadButton ButtonDescarga;
        internal Telerik.WinControls.UI.RadCheckBox ChkNormaliza;
        internal Telerik.WinControls.UI.RadLabel RadLabel3;
        internal Telerik.WinControls.UI.RadTextBox TxbPath;
        internal Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement DotsSpinnerWaitingBarIndicatorElement1;
        internal Telerik.WinControls.UI.RadWaitingBar Espera;
        internal Telerik.WinControls.UI.RadGridView GridData;
        internal Telerik.WinControls.UI.RadLabelElement LabelResult;
        internal Telerik.WinControls.UI.RadStatusStrip BarraEstado;
        internal System.Windows.Forms.ImageList ImageList1;
        internal Telerik.WinControls.UI.RadCheckBox radCheckBox1;
    }
}
