﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.Enums;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Helpers;
using Jaeger.Edita.V2;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.Catalogos.Repositories;
using Jaeger.Catalogos.Entities;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Views
{
    public partial class ViewComprobante2Fiscal : RadForm
    {
        
        private BackgroundWorker preparar;
        private ViewModelComprobante cfdiV33;
        private SqlSugarComprobanteFiscal data;
        private ISqlDirectorio receptor;
        private MetodoPagoCatalogo catalogoMetodoPago = new MetodoPagoCatalogo();
        private FormaPagoCatalogo catalogoFormaPago = new FormaPagoCatalogo();
        private MonedaCatalogo catalogoMoneda = new MonedaCatalogo();
        private UsoCFDICatalogo catalogoUsoCfdi = new UsoCFDICatalogo();
        private RelacionCFDICatalogo catalogoRelacionesCfdi = new RelacionCFDICatalogo();
        private CatalogoConceptosRecientes catalogoRecientes = new CatalogoConceptosRecientes();

        public ViewComprobante2Fiscal(ViewModelComprobante objeto)
        {
            InitializeComponent();
            this.cfdiV33 = objeto;
        }

        public ViewComprobante2Fiscal(int indice)
        {
            InitializeComponent();
            this.cfdiV33 = new ViewModelComprobante() { Id = indice };
        }

        private void Comprobante1Fiscal_Load(object sender, EventArgs e)
        {
            // tareas adicionales
            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += PrepararDoWork;
            this.preparar.RunWorkerCompleted += PrepararRunWorkerCompleted;
            this.preparar.RunWorkerAsync();
            this.ToolBarHostItemIncluir.HostedItem = this.ChkCfdiRelacionadoIncluir.ButtonElement;
            this.CommandComprobanteFiscal.OverflowButton.AddRemoveButtonsMenuItem.Visibility = ElementVisibility.Collapsed;
            this.CommandComprobanteFiscal.OverflowButton.CustomizeButtonMenuItem.Visibility = ElementVisibility.Collapsed;
        }

        #region procedimientos de controles

        private void TipoComprobante_Click(object sender, EventArgs e)
        {
            if (sender as RadMenuItem is RadMenuItem)
            {
                RadMenuItem button = (RadMenuItem)sender;
                this.TipoComprobante.DefaultItem = (RadItem)sender;
                this.TipoComprobante.Text = button.Text;
                this.TipoComprobante.Tag = button.Tag;
                this.ToolBarRefresh.PerformClick();
            }
        }

        private void CboUsoCfdi_SelectedValueChanged(object sender, EventArgs e)
        {
            GridViewRowInfo temporal = CboReceptor.SelectedItem as GridViewRowInfo;
            if (!(temporal == null))
            {
                var seleccionado = temporal.DataBoundItem as ClaveUsoCFDI;
                if (seleccionado != null)
                {
                    this.cfdiV33.UsoCfdi = new ClaveUsoCFDI
                    {
                        Clave = seleccionado.Clave,
                        Descripcion = seleccionado.Descripcion
                    };
                }
            }
        }

        private void CboPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRowInfo temporal = CboFormaPago.SelectedItem as GridViewRowInfo;
            if (!(temporal == null))
            {
                if (temporal.DataBoundItem is ClaveFormaPago este)
                {
                    this.cfdiV33.FormaPago.Clave = este.Clave;
                    this.cfdiV33.FormaPago.Descripcion = este.Descripcion;
                }
            }
        }

        private void CboPaymentMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRowInfo temporal = CboMetodoPago.SelectedItem as GridViewRowInfo;
            if (!(temporal == null))
            {
                if (temporal.DataBoundItem is ClaveMetodoPago seleccionado)
                {
                    this.cfdiV33.MetodoPago.Clave = seleccionado.Clave;
                    this.cfdiV33.MetodoPago.Descripcion = seleccionado.Descripcion;
                }
            }
        }

        private void CboReceptor_SelectedValueChanged(object sender, EventArgs e)
        {
            GridViewRowInfo temporal = CboReceptor.SelectedItem as GridViewRowInfo;
            if (!(temporal == null))
            {
                if (temporal.DataBoundItem is ViewModelContribuyenteDomicilio receptor)
                {
                    this.cfdiV33.Receptor.Id = receptor.Id;
                    this.cfdiV33.Receptor.RFC = receptor.RFC;
                    this.cfdiV33.Receptor.Nombre = receptor.Nombre;
                    this.cfdiV33.Receptor.ResidenciaFiscal = receptor.RegimenFiscal;
                    this.cfdiV33.Receptor.NumRegIdTrib = receptor.NumRegIdTrib;
                    if (receptor.ClaveUsoCFDI != "")
                        this.CboUsoCfdi.SelectedValue = receptor.ClaveUsoCFDI;
                    else
                        this.CboUsoCfdi.SelectedValue = "P01";
                }
            }
        }

        private void CboReceptor_DropDownOpening(object sender, CancelEventArgs args)
        {
            if (this.cfdiV33.Editable)
            {

            }
            else
            {
                args.Cancel = true;
            }
        }

        private void CboReceptor_DropDownOpened(object sender, EventArgs e)
        {
            if (this.cfdiV33.Editable)
            {
                this.CboReceptor.AutoSizeDropDownToBestFit = true;
                this.CboReceptor.DataSource = this.receptor.GetListBy(Edita.V2.Directorio.Enums.EnumRelationType.Cliente);
            }
            else
            {

            }
        }

        #endregion

        #region barra de herramientas

        private void ToolBarSave_Click(object sender, EventArgs e)
        {
            using(Waiting2Form espera = new Waiting2Form(this.Guardar))
            {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
        }

        private void ToolBarClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ToolBarCancelar_Click(object sender, EventArgs e)
        {
            if (RadMessageBox.Show(this, "¿Esta seguro de cancelar este comprobante fiscal? (Esta acción no se puede revertir.)", "Información", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                
            }
        }

        private void ToolBarPdf_Click(object sender, EventArgs e)
        {
            string keyDownload = JaegerManagerPaths.JaegerPath(EnumPaths.Downloads, String.Concat(this.cfdiV33.KeyName().ToString(), ".pdf"));

            if ((!System.IO.File.Exists(keyDownload)))
            {
                Util.Helpers.HelperFiles.DownloadFile(this.cfdiV33.FilePdf, keyDownload);
            }

            if ((System.IO.File.Exists(keyDownload)))
            {
                ViewerPdf vista = new ViewerPdf(keyDownload) { MdiParent = this.ParentForm };
                vista.Show();
            }
        }

        private void ToolBarXml_Click(object sender, EventArgs e)
        {
            string keyDownload = JaegerManagerPaths.JaegerPath(EnumPaths.Downloads, String.Concat(this.cfdiV33.KeyName().ToString(), ".xml"));
            if ((!System.IO.File.Exists(keyDownload)))
                Util.Helpers.HelperFiles.DownloadFile(this.cfdiV33.FileXml, keyDownload);

            if ((System.IO.File.Exists(keyDownload)))
            {
                var vista = new ViewComprobanteVisor(keyDownload) { MdiParent = this.ParentForm };
                vista.Show();
            }
        }

        private void ToolBarRefresh_Click(object sender, EventArgs e)
        {
            // suponemos que es nuevo
            if (this.cfdiV33 == null)
            {
                this.Text = "Nuevo Comprobante";
                this.cfdiV33 = new ViewModelComprobante
                {
                    Status = "Pendiente",
                    TipoComprobante = EnumCfdiType.Ingreso,
                    SubTipo = EnumCfdiSubType.Emitido
                };
                this.cfdiV33.Emisor.RFC = ConfigService.Synapsis.Empresa.RFC;
                this.cfdiV33.Emisor.Nombre = ConfigService.Synapsis.Empresa.RazonSocial;
                this.cfdiV33.Default();
            } // en el caso de que tengamos un id
            else if (this.cfdiV33.Id > 0)
            {
                using(var espera = new Waiting2Form(this.Actualizar))
                {
                    espera.Text = "Cargando ...";
                    espera.ShowDialog(this);
                    this.Text = string.Concat(this.cfdiV33.Serie, " ", this.cfdiV33.Folio);
                }
            }
            this.CreateBinding();
            
        }

        #endregion

        #region conceptos comprobante

        private void ToolBarCfdiBuscar_Click(object sender, EventArgs e)
        {
            var buscar = new ViewComprobanteBuscar(this.cfdiV33.ReceptorRFC);
            ComprobanteCfdiRelacionadosCfdiRelacionado seleccionado = buscar.ShowForm();
            if (seleccionado != null)
            {
                if (this.cfdiV33.CfdiRelacionados == null)
                    this.cfdiV33.CfdiRelacionados = new ComprobanteCfdiRelacionados();
                this.GridCfdiRelacionados.DataSource = this.cfdiV33.CfdiRelacionados;
                this.GridCfdiRelacionados.DataMember = "CfdiRelacionado";
                this.cfdiV33.CfdiRelacionados.CfdiRelacionado.Add(seleccionado);
            }
        }

        private void ToolBarCfdiAgregar_Click(object sender, EventArgs e)
        {
            this.cfdiV33.CfdiRelacionados.CfdiRelacionado.AddNew();
        }

        private void ToolBarCfdiQuitar_Click(object sender, EventArgs e)
        {
            if (!(this.GridCfdiRelacionados.CurrentRow == null))
            {
                this.GridCfdiRelacionados.Rows.Remove(this.GridCfdiRelacionados.CurrentRow);
            }
        }

        private void ChkCfdiRelacionadoIncluir_CheckStateChanged(object sender, EventArgs e)
        {
            this.ToolBarCfdiTipoRelacion.Enabled = this.ChkCfdiRelacionadoIncluir.Checked;
            this.ToolBarCfdiBuscar.Enabled = this.ChkCfdiRelacionadoIncluir.Checked;
            this.ToolBarCfdiAgregar.Enabled = this.ChkCfdiRelacionadoIncluir.Checked;
            this.ToolBarCfdiQuitar.Enabled = this.ChkCfdiRelacionadoIncluir.Checked;
        }

        private void ToolBarCfdiTipoRelacion_SelectedValueChanged(object sender, Telerik.WinControls.UI.Data.ValueChangedEventArgs e)
        {
            RadListDataItem temporal = this.ToolBarCfdiTipoRelacion.SelectedItem;
            if (!(temporal == null))
            {
                if (temporal.DataBoundItem is ClaveTipoRelacionCFDI seleccionado)
                {
                    if (this.cfdiV33.CfdiRelacionados == null)
                    {
                        this.cfdiV33.CfdiRelacionados = new ComprobanteCfdiRelacionados();
                    }
                    this.cfdiV33.CfdiRelacionados.TipoRelacion.Clave = seleccionado.Clave;
                    this.cfdiV33.CfdiRelacionados.TipoRelacion.Descripcion = seleccionado.Descripcion;
                }
            }
        }

        #endregion

        #region preparar

        private void PrepararDoWork(object sender, DoWorkEventArgs e)
        {
            // preparar conexion con el servidor
            this.data = new SqlSugarComprobanteFiscal(ConfigService.Synapsis.RDS.Edita);
            this.receptor = new MySqlDirectorio(ConfigService.Synapsis.RDS.Edita);

            foreach (string item in Enum.GetNames(typeof(EnumCfdiType)))
            {
                RadMenuItem boton = new RadMenuItem { Text = item, Name = item };
                this.TipoComprobante.Items.Add(boton);
                boton.Click += new EventHandler(this.TipoComprobante_Click);
            }

            // cargar catalogos
            this.catalogoUsoCfdi.Load();
            this.catalogoMetodoPago.Load();
            
            this.catalogoFormaPago.Load();
            this.catalogoMoneda.Load();
            this.catalogoRelacionesCfdi.Load();
        }

        private void PrepararRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // catalogo metodos de pago
            this.CboMetodoPago.DataSource = this.catalogoMetodoPago.Items;
            this.CboMetodoPago.DisplayMember = "Descripcion";
            this.CboMetodoPago.ValueMember = "Clave";
            this.CboMetodoPago.SelectedIndex = -1;
            this.CboMetodoPago.SelectedValue = null;
            this.CboMetodoPago.AutoSizeDropDownToBestFit = true;
            this.CboMetodoPago.SelectedIndexChanged += CboPaymentMethod_SelectedIndexChanged;

            // catalogo formas de pago
            this.CboFormaPago.DataSource = this.catalogoFormaPago.Items;
            this.CboFormaPago.DisplayMember = "Descripcion";
            this.CboFormaPago.ValueMember = "Clave";
            this.CboFormaPago.AutoSizeDropDownToBestFit = true;
            this.CboFormaPago.SelectedIndexChanged += CboPaymentType_SelectedIndexChanged;
            this.CboFormaPago.SelectedIndex = -1;
            this.CboFormaPago.SelectedValue = null;

            // catalogo de monedas
            this.CboMoneda.DataSource = this.catalogoMoneda.Items;
            this.CboMoneda.DisplayMember = "Clave";
            this.CboMoneda.ValueMember = "Clave";
            this.CboMoneda.SelectedIndex = -1;
            this.CboMoneda.SelectedValue = null;
            this.CboMoneda.AutoFilter = true;
            this.CboMoneda.AutoSizeDropDownToBestFit = true;
            CompositeFilterDescriptor composite = new CompositeFilterDescriptor { LogicalOperator = FilterLogicalOperator.Or };
            composite.FilterDescriptors.Add(new FilterDescriptor("Clave", FilterOperator.Contains, ""));
            composite.FilterDescriptors.Add(new FilterDescriptor("Descripcion", FilterOperator.Contains, ""));
            this.CboMoneda.EditorControl.FilterDescriptors.Add(composite);

            // catalogo uso de cfdi
            this.CboUsoCfdi.DataSource = catalogoUsoCfdi.Items;
            this.CboUsoCfdi.DisplayMember = "Descripcion";
            this.CboUsoCfdi.ValueMember = "Clave";
            this.CboUsoCfdi.SelectedIndex = -1;
            this.CboUsoCfdi.SelectedValue = null;
            this.CboUsoCfdi.AutoSizeDropDownToBestFit = true;
            CompositeFilterDescriptor composite2 = new CompositeFilterDescriptor { LogicalOperator = FilterLogicalOperator.Or };
            composite2.FilterDescriptors.Add(new FilterDescriptor("Clave", FilterOperator.Contains, ""));
            composite2.FilterDescriptors.Add(new FilterDescriptor("Descripcion", FilterOperator.Contains, ""));
            this.CboUsoCfdi.EditorControl.FilterDescriptors.Add(composite2);
            this.CboUsoCfdi.AutoFilter = true;
            this.CboUsoCfdi.SelectedIndex = -1;
            this.CboUsoCfdi.SelectedValue = null;
            this.CboUsoCfdi.SelectedValueChanged += CboUsoCfdi_SelectedValueChanged;

            // catalogo relaciones cfdi
            this.ToolBarCfdiTipoRelacion.DataSource = this.catalogoRelacionesCfdi.Items;
            this.ToolBarCfdiTipoRelacion.DisplayMember = "Descripcion";
            this.ToolBarCfdiTipoRelacion.ValueMember = "Clave";
            this.ToolBarCfdiTipoRelacion.SelectedIndex = -1;
            this.ToolBarCfdiTipoRelacion.SelectedValue = null;
            this.ToolBarCfdiTipoRelacion.AutoSize = true;
            this.ToolBarCfdiTipoRelacion.SelectedValueChanged += ToolBarCfdiTipoRelacion_SelectedValueChanged;

            // receptor del comprobante
            this.CboReceptor.DisplayMember = "Nombre";
            this.CboReceptor.ValueMember = "Nombre";
            CompositeFilterDescriptor composite3 = new CompositeFilterDescriptor { LogicalOperator = FilterLogicalOperator.Or };
            composite3.FilterDescriptors.Add(new FilterDescriptor("Nombre", FilterOperator.Contains, ""));
            composite3.FilterDescriptors.Add(new FilterDescriptor("RFC", FilterOperator.Contains, ""));
            this.CboReceptor.EditorControl.FilterDescriptors.Add(new FilterDescriptor("Nombre", FilterOperator.StartsWith, ""));
            this.CboReceptor.AutoFilter = true;
            this.CboReceptor.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.CboReceptor.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.CboReceptor.DropDownOpened += CboReceptor_DropDownOpened;
            this.CboReceptor.DropDownOpening += CboReceptor_DropDownOpening;
            this.CboReceptor.SelectedValueChanged += CboReceptor_SelectedValueChanged;

            // impuestos de los conceptos
            GridViewComboBoxColumn comboTipo = (GridViewComboBoxColumn)this.GridConceptoImpuestos.MasterTemplate.Columns["Tipo"];
            comboTipo.DataSource = Enum.GetNames(typeof(Enums.EnumTipoImpuesto));
            GridViewComboBoxColumn comboImpuesto = (GridViewComboBoxColumn)this.GridConceptoImpuestos.MasterTemplate.Columns["Impuesto"];
            comboImpuesto.DataSource = Enum.GetNames(typeof(Enums.EnumImpuesto));
            GridViewComboBoxColumn comboFactor = (GridViewComboBoxColumn)this.GridConceptoImpuestos.MasterTemplate.Columns["TipoFactor"];
            comboFactor.DataSource = Enum.GetNames(typeof(Enums.EnumFactor));

            this.ToolBarRefresh.PerformClick();

            if (this.cfdiV33.Version.Contains("3.3"))
            {
                this.CboUsoCfdi.DataSource = this.catalogoUsoCfdi.Items;
            }

            if (ConfigService.Synapsis.ProveedorAutorizado.Certificacion.Production == false)
            {
                this.ToolBarEmisor.ToolTipText = "Actualmente la configuración indica modo de prueba";
            }
        }

        #endregion

        private void CreateBinding()
        {
            this.ToolBarTextBoxIdDocumento.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.ToolBarTextBoxIdDocumento.TextBoxElement.TextBoxItem.TextAlign = HorizontalAlignment.Center;
            this.FechaCertifica.SetToNullValue();
            this.FechaCertifica.NullableValue = null;

            this.Presicion.DataBindings.Clear();
            this.Presicion.DataBindings.Add("Value", this.cfdiV33, "PresionDecimal", true, DataSourceUpdateMode.OnPropertyChanged);

            if (this.cfdiV33.TimbreFiscal == null)
            {
                this.cfdiV33.SubTipo = EnumCfdiSubType.Emitido;
                this.cfdiV33.Emisor.RFC = ConfigService.Synapsis.Empresa.RFC;
                this.cfdiV33.Emisor.Nombre = ConfigService.Synapsis.Empresa.RazonSocial;
                this.cfdiV33.Emisor.RegimenFiscal = ConfigService.Synapsis.Empresa.RegimenFiscal;
                this.cfdiV33.LugarExpedicion = ConfigService.Synapsis.Empresa.DomicilioFiscal.CodigoPostal;
                this.ToolBarTextBoxIdDocumento.Text = "";
                this.ToolBarTextBoxIdDocumento.Visibility = ElementVisibility.Collapsed;
                this.ToolLabelUuid.Visibility = ElementVisibility.Collapsed;
            }
            else
            {
                this.cfdiV33.Emisor.RegimenFiscal = ConfigService.Synapsis.Empresa.RegimenFiscal;
                this.cfdiV33.LugarExpedicion = ConfigService.Synapsis.Empresa.DomicilioFiscal.CodigoPostal;
                this.ToolBarTextBoxIdDocumento.Text = this.cfdiV33.TimbreFiscal.UUID;
                this.ToolBarTextBoxIdDocumento.Visibility = ElementVisibility.Visible;
                this.ToolLabelUuid.Visibility = ElementVisibility.Visible;
            }

            this.CboSerie.DataBindings.Clear();
            this.CboSerie.ReadOnly = !this.cfdiV33.Editable;

            this.ToolBarStatus.DataBindings.Clear();
            this.ToolBarStatus.DataBindings.Add("Text", this.cfdiV33, "StatusText", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ToolBarEmisor.DataBindings.Clear();
            this.ToolBarEmisor.DataBindings.Add("Text", ConfigService.Synapsis.Empresa, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TipoComprobante.DataBindings.Clear();
            this.TipoComprobante.DataBindings.Add("Text", this.cfdiV33, "TipoComprobanteText", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TipoComprobante.DataBindings.Add("Enabled", this.cfdiV33, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbLugarDeExpedicion.DataBindings.Clear();
            this.TxbLugarDeExpedicion.DataBindings.Add("Text", cfdiV33, "LugarExpedicion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbLugarDeExpedicion.ReadOnly = !this.cfdiV33.Editable;

            this.CboReceptor.DataBindings.Clear();
            this.CboReceptor.DataBindings.Add("Text", cfdiV33.Emisor, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboReceptor.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboReceptor.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.TxbReceptorRFC.DataBindings.Clear();
            this.TxbReceptorRFC.DataBindings.Add("Text", cfdiV33.Emisor, "Rfc", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboUsoCfdi.DataBindings.Clear();
            this.CboUsoCfdi.DataBindings.Add("SelectedValue", cfdiV33.UsoCfdi, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboUsoCfdi.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboUsoCfdi.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.CboResidenciaFiscal.DataBindings.Clear();
            this.CboResidenciaFiscal.DataBindings.Add("Text", cfdiV33.Emisor, "ResidenciaFiscal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboResidenciaFiscal.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboResidenciaFiscal.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.TxbNumRegIdTrib.DataBindings.Clear();
            this.TxbNumRegIdTrib.DataBindings.Add("Text", cfdiV33.Emisor, "NumRegIdTrib", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbNumRegIdTrib.ReadOnly = !this.cfdiV33.Editable;

            this.CboMetodoPago.DataBindings.Clear();
            this.CboMetodoPago.DataBindings.Add("SelectedValue", cfdiV33.MetodoPago, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboMetodoPago.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboMetodoPago.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.CboFormaPago.DataBindings.Clear();
            this.CboFormaPago.DataBindings.Add("SelectedValue", cfdiV33.FormaPago, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboFormaPago.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboFormaPago.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.CboCondiciones.DataBindings.Clear();
            this.CboCondiciones.DataBindings.Add("Text", cfdiV33, "CondicionPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboCondiciones.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboCondiciones.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.CboMoneda.DataBindings.Clear();
            this.CboMoneda.DataBindings.Add("SelectedValue", cfdiV33.Moneda, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboMoneda.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.CboMoneda.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.TxbCuentaPago.DataBindings.Clear();
            this.TxbCuentaPago.DataBindings.Add("Text", cfdiV33, "CtaPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbCuentaPago.ReadOnly=!this.cfdiV33.Editable;

            this.TxbTipoCambio.DataBindings.Clear();
            this.TxbTipoCambio.DataBindings.Add("Text", cfdiV33, "TipoCambio", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbTipoCambio.ReadOnly = !this.cfdiV33.Editable;

            this.FechaEmisionField.DataBindings.Clear();
            this.FechaEmisionField.DataBindings.Add("Value", cfdiV33, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaEmisionField.DateTimePickerElement.TextBoxElement.TextBoxItem.ReadOnly = !this.cfdiV33.Editable;
            this.FechaEmisionField.DateTimePickerElement.ArrowButton.Visibility = (this.cfdiV33.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.FechaCertifica.DataBindings.Clear();
            this.FechaCertifica.DataBindings.Add("Value", cfdiV33, "FechaCert", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaCertifica.DateTimePickerElement.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.FechaCertifica.DateTimePickerElement.ArrowButton.Visibility = ElementVisibility.Collapsed;

            this.TxbSubTotal.DataBindings.Clear();
            this.TxbSubTotal.DataBindings.Add("Text", cfdiV33, "SubTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbDescuento.DataBindings.Clear();
            this.TxbDescuento.DataBindings.Add("Text", cfdiV33, "Descuento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbTotal.DataBindings.Clear();
            this.TxbTotal.DataBindings.Add("Text", cfdiV33, "Total", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbTrasladoIva.DataBindings.Clear();
            this.TxbTrasladoIva.DataBindings.Add("Text", cfdiV33, "TrasladoIva", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbTrasladoIEPS.DataBindings.Clear();
            this.TxbTrasladoIEPS.DataBindings.Add("Text", cfdiV33, "TrasladoIeps", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbRetencionIva.DataBindings.Clear();
            this.TxbRetencionIva.DataBindings.Add("Text", cfdiV33, "RetencionIva", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbRetencionISR.DataBindings.Clear();
            this.TxbRetencionISR.DataBindings.Add("Text", cfdiV33, "RetencionIsr", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbFolio.DataBindings.Clear();
            this.TxbFolio.DataBindings.Add("Text", cfdiV33, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboSerie.DataBindings.Clear();
            this.CboSerie.DataBindings.Add("Text", cfdiV33, "Serie", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ToolBarCfdiTipoRelacion.DataBindings.Clear();
            if (!(this.cfdiV33.CfdiRelacionados == null))
            {
                this.ToolBarCfdiTipoRelacion.DataBindings.Add("SelectedValue", cfdiV33.CfdiRelacionados.TipoRelacion, "Clave", false, DataSourceUpdateMode.OnPropertyChanged);
            }

            this.GridConceptos.DataSource = this.cfdiV33.Conceptos;
            this.GridConceptoParte.AutoGenerateHierarchy = true;
            this.GridConceptoParte.DataSource = this.cfdiV33.Conceptos;
            this.GridConceptoParte.DataMember = "Parte";
            this.GridConceptoImpuestos.DataSource = this.cfdiV33.Conceptos;
            this.GridConceptoImpuestos.DataMember = "Impuestos";
            this.GridConceptoInformacionAduanera.DataSource = this.cfdiV33.Conceptos;
            this.GridConceptoInformacionAduanera.DataMember = "InformacionAduanera";

            this.GridCfdiRelacionados.DataSource = this.cfdiV33.CfdiRelacionados;
            this.GridCfdiRelacionados.DataMember = "CfdiRelacionado";

            // esta timbrado?
            if (this.cfdiV33.TimbreFiscal == null)
            {
                if (HelperCertificacion.ValidaFechaEmisionMenor72H(this.cfdiV33.FechaEmision, 1) == false)
                {
                    RadMessageBox.Show(this, "La fecha de emisión esta fuera del rango permitido es necesario actualizar la fecha.", "Información", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    this.FechaEmisionField.MinDate = DateTime.Now.AddDays(-1);
                    this.cfdiV33.FechaEmision = DateTime.Now;
                }
            }

            // cfdi relacionados
            this.CommandCfdiRelacionado.DataBindings.Clear();
            this.CommandCfdiRelacionado.DataBindings.Add("Enabled", this.cfdiV33, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);
            this.ChkCfdiRelacionadoIncluir.DataBindings.Clear();
            this.ChkCfdiRelacionadoIncluir.DataBindings.Add("Enabled", this.cfdiV33, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);
            this.GridCfdiRelacionados.AllowEditRow = this.ChkCfdiRelacionadoIncluir.CheckState == CheckState.Checked;

            this.ToolBarPdf.Enabled = HelperValidacion.ValidaUrl(this.cfdiV33.FilePdf);
            this.ToolBarPdf.Visibility = (this.ToolBarPdf.Enabled ? ElementVisibility.Visible : ElementVisibility.Collapsed);

            this.ToolBarXml.Enabled = HelperValidacion.ValidaUrl(this.cfdiV33.FileXml);
            this.ToolBarXml.Visibility = (this.ToolBarXml.Enabled ? ElementVisibility.Visible : ElementVisibility.Collapsed);

            this.ToolBarEmail.Enabled = this.ToolBarXml.Enabled;

            this.ToolBarCancelar.Enabled = this.ToolBarCancelar.Enabled = !(this.cfdiV33.TimbreFiscal == null) && (this.cfdiV33.StatusText != "Cancelado");

            this.ToolBarSave.Enabled = this.cfdiV33.Editable;

        }

        private void Actualizar()
        {
            this.cfdiV33 = data.GetComprobante(this.cfdiV33.Id);
        }

        private void Guardar()
        {
            this.data.Save(this.cfdiV33);
        }

    }
}
