﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Base.Services;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2;
using Jaeger.Edita.V2.Empresa.Entities;
using Jaeger.UI.Common.Forms;

namespace Jaeger.Views
{
    public partial class ViewComprobantesValidadorConfigurar : Telerik.WinControls.UI.RadForm
    {
        private Edita.V2.Validador.Entities.Configuracion configuracionLocal;
        private ISqlConfiguracion data;
        private ViewModelEmpresaConfiguracion configuracion;

        public ViewComprobantesValidadorConfigurar()
        {
            InitializeComponent();
        }

        private void ViewComprobantesValidadorConfigurar_Load(object sender, EventArgs e)
        {
            this.data = new SqlSugarConfiguracion(ConfigService.Synapsis.RDS.Edita);
            this.configuracion = this.data.GetByKey(SqlSugarConfiguracion.KeyConfiguracion.valida6);
            if (this.configuracion == null)
            {
                this.configuracion = new ViewModelEmpresaConfiguracion();
                this.configuracion.Key = SqlSugarConfiguracion.KeyConfiguracion.valida6.ToString();
                this.configuracion.Data = new Edita.V2.Validador.Entities.Configuracion().Json();
            }
            this.configuracionLocal = Edita.V2.Validador.Entities.Configuracion.Json(this.configuracion.Data);
            if (this.configuracionLocal == null)
                this.configuracionLocal = new Edita.V2.Validador.Entities.Configuracion();
            this.CreateBinding();
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            using (var espera = new Waiting2Form(this.Guardar))
            {
                espera.Text = "Guardando, espere un momento ...";
                espera.ShowDialog(this);
            }
        }

        private void buttonCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Guardar()
        {
            this.configuracion.Data = this.configuracionLocal.Json();
            this.configuracion = this.data.Save(this.configuracion);
        }

        private void CreateBinding()
        {
            this.switchModoParalelo.DataBindings.Clear();
            this.switchModoParalelo.DataBindings.Add("Value", this.configuracionLocal, "ModoParalelo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.switchEstricto.DataBindings.Clear();
            this.switchEstricto.DataBindings.Add("Value", this.configuracionLocal, "Estricto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.switchEstadoSAT.DataBindings.Clear();
            this.switchEstadoSAT.DataBindings.Add("Value", this.configuracionLocal, "EstadoSAT", true, DataSourceUpdateMode.OnPropertyChanged);
            this.switchLocalizarPDF.DataBindings.Clear();
            this.switchLocalizarPDF.DataBindings.Add("Value", this.configuracionLocal, "LocalizarPDF", true, DataSourceUpdateMode.OnPropertyChanged);
            this.switchRenombrarOrigen.DataBindings.Clear();
            this.switchRenombrarOrigen.DataBindings.Add("Value", this.configuracionLocal, "RenombrarOrigen", true, DataSourceUpdateMode.OnPropertyChanged);
            this.switchArticulo69B.DataBindings.Clear();
            this.switchArticulo69B.DataBindings.Add("Value", this.configuracionLocal, "Articulo69B", true, DataSourceUpdateMode.OnPropertyChanged);
            this.switchClaveProductoServicio.DataBindings.Clear();
            this.switchClaveProductoServicio.DataBindings.Add("Value", this.configuracionLocal, "ClaveProductoServicio", true, DataSourceUpdateMode.OnPropertyChanged);
            this.switchClaveUnidad.DataBindings.Clear();
            this.switchClaveUnidad.DataBindings.Add("Value", this.configuracionLocal, "ClaveUnidad", true, DataSourceUpdateMode.OnPropertyChanged);
            this.switchDescargaCertificados.DataBindings.Clear();
            this.switchDescargaCertificados.DataBindings.Add("Value", this.configuracionLocal, "DescargaCertificados", true, DataSourceUpdateMode.OnPropertyChanged);
            this.switchMetodoPago.DataBindings.Clear();
            this.switchMetodoPago.DataBindings.Add("Value", this.configuracionLocal, "MetodoPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.switchFormaPago.DataBindings.Clear();
            this.switchFormaPago.DataBindings.Add("Value", this.configuracionLocal, "FormaPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.switchUsoCFDI.DataBindings.Clear();
            this.switchUsoCFDI.DataBindings.Add("Value", this.configuracionLocal, "ClaveUsoCFDI", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbLugarExpedicion.DataBindings.Clear();
            this.txbLugarExpedicion.DataBindings.Add("Text", this.configuracionLocal, "LugarExpedicion", true, DataSourceUpdateMode.OnPropertyChanged);
        }
    }
}
