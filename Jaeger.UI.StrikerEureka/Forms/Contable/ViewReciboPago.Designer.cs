﻿namespace Jaeger.Views.Contable
{
    partial class ViewReciboPago
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewReciboPago));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn47 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn3 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn48 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn2 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn49 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn50 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn51 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn52 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn53 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn54 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn55 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn56 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCalculatorColumn gridViewCalculatorColumn3 = new Telerik.WinControls.UI.GridViewCalculatorColumn();
            Telerik.WinControls.UI.GridViewCalculatorColumn gridViewCalculatorColumn4 = new Telerik.WinControls.UI.GridViewCalculatorColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn57 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition9 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn58 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn59 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn60 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCommandColumn gridViewCommandColumn2 = new Telerik.WinControls.UI.GridViewCommandColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition10 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn61 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn62 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn4 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn63 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn64 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn65 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn66 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn67 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn68 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn69 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn70 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn71 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn72 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn73 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn74 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition11 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn75 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn76 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn77 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn78 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn79 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition12 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn80 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn81 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition13 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn82 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn83 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn84 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition14 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn85 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn86 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn87 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn88 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition15 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn89 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn90 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn91 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn92 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition16 = new Telerik.WinControls.UI.TableViewDefinition();
            this.RadCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.CommandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.CommandRecibo = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarEmisor = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.Separador = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarLabelNoIdent = new Telerik.WinControls.UI.CommandBarLabel();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolLabelStatus = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolSplitStatus = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarNew = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarCopy = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarCreate = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarPrint = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarSave = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarRefresh = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarDescargar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarLayout = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarBancomerPagosInterbancarios = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarBanamexPagosInterbancarios = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarBanamexPagosInterbancariosClabe = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarClose = new Telerik.WinControls.UI.CommandBarButton();
            this.PageView = new Telerik.WinControls.UI.RadPageView();
            this.PageViewComprobante = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridComprobantes = new Telerik.WinControls.UI.RadGridView();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarComprobantes = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarComprobantesNuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarComprobantesAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarComprobantesQuitar = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarTipoDocumento = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarDocumentoRemision = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarDocumentoFactura = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarDocumentos = new Telerik.WinControls.UI.CommandBarHostItem();
            this.PageViewDoctosRelacionados = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridDoctosRelacionados = new Telerik.WinControls.UI.RadGridView();
            this.radCommandBar3 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarDoctosButtonAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarDoctosButtonQuitar = new Telerik.WinControls.UI.CommandBarButton();
            this.CboComprobantes = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.TxbEmisorRfc = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.CboEmisorCuenta = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.TxbEmisorClabe = new Telerik.WinControls.UI.RadTextBox();
            this.TxbEmisorSucursal = new Telerik.WinControls.UI.RadTextBox();
            this.TxbEmisorBanco = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.FechaDePago = new Telerik.WinControls.UI.RadDateTimePicker();
            this.FechaDocumento = new Telerik.WinControls.UI.RadDateTimePicker();
            this.TxbNumDocto = new Telerik.WinControls.UI.RadTextBox();
            this.CboFormaDePago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.RadLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.TxbReceptorRfc = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.CboReceptorBanco = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.TxbReceptorClabe = new Telerik.WinControls.UI.RadTextBox();
            this.TxbReceptorSucursal = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.TxbNotas = new Telerik.WinControls.UI.RadTextBox();
            this.TxbConcepto = new Telerik.WinControls.UI.RadTextBox();
            this.TxbNumAuto = new Telerik.WinControls.UI.RadTextBox();
            this.TxbReferencia = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.TxbCargo = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.LabelCargo = new Telerik.WinControls.UI.RadLabel();
            this.TxbAbono = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.LabelAbono = new Telerik.WinControls.UI.RadLabel();
            this.ChkPorJustificar = new Telerik.WinControls.UI.RadCheckBox();
            this.ChkParaAbono = new Telerik.WinControls.UI.RadCheckBox();
            this.FechaPoliza = new Telerik.WinControls.UI.RadDateTimePicker();
            this.TxbEmisorNumeroCta = new Telerik.WinControls.UI.RadTextBox();
            this.CboReceptorCuenta = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.PanelGeneral = new Telerik.WinControls.UI.RadPanel();
            this.CboReceptor = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.ChkPagoNomina = new Telerik.WinControls.UI.RadCheckBox();
            this.TxbRefNumerica = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.ChkMultiPago = new Telerik.WinControls.UI.RadCheckBox();
            this.PanelDocumentos = new Telerik.WinControls.UI.RadPanel();
            this.BarraEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.LabeStatus = new Telerik.WinControls.UI.RadLabelElement();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PageView)).BeginInit();
            this.PageView.SuspendLayout();
            this.PageViewComprobante.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobantes.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            this.PageViewDoctosRelacionados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDoctosRelacionados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDoctosRelacionados.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboComprobantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboComprobantes.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboComprobantes.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorRfc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmisorCuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmisorCuenta.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmisorCuenta.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorClabe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorSucursal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaDePago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumDocto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaDePago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaDePago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaDePago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbReceptorRfc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptorBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptorBanco.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptorBanco.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbReceptorClabe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbReceptorSucursal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNotas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbConcepto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumAuto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbReferencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCargo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelCargo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbAbono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelAbono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPorJustificar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkParaAbono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaPoliza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorNumeroCta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptorCuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptorCuenta.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptorCuenta.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelGeneral)).BeginInit();
            this.PanelGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPagoNomina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRefNumerica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkMultiPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelDocumentos)).BeginInit();
            this.PanelDocumentos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadCommandBar1
            // 
            this.RadCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar1.Name = "RadCommandBar1";
            this.RadCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement1});
            this.RadCommandBar1.Size = new System.Drawing.Size(962, 30);
            this.RadCommandBar1.TabIndex = 220;
            // 
            // CommandBarRowElement1
            // 
            this.CommandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement1.Name = "CommandBarRowElement1";
            this.CommandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.CommandRecibo});
            this.CommandBarRowElement1.Text = "";
            this.CommandBarRowElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // CommandRecibo
            // 
            this.CommandRecibo.DisplayName = "Emision de Comprobante";
            this.CommandRecibo.EnableFocusBorder = false;
            this.CommandRecibo.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarEmisor,
            this.Separador,
            this.ToolBarLabelNoIdent,
            this.commandBarSeparator2,
            this.ToolLabelStatus,
            this.ToolSplitStatus,
            this.Separator2,
            this.ToolBarNew,
            this.ToolBarCopy,
            this.ToolBarCreate,
            this.ToolBarPrint,
            this.ToolBarSave,
            this.ToolBarRefresh,
            this.ToolBarDescargar,
            this.ToolBarLayout,
            this.ToolBarClose});
            this.CommandRecibo.Name = "CommandRecibo";
            this.CommandRecibo.ShowHorizontalLine = false;
            // 
            // ToolBarEmisor
            // 
            this.ToolBarEmisor.DefaultItem = null;
            this.ToolBarEmisor.DisplayName = "Emisor";
            this.ToolBarEmisor.DrawText = true;
            this.ToolBarEmisor.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_acercadelusuario;
            this.ToolBarEmisor.Name = "ToolBarEmisor";
            this.ToolBarEmisor.Text = "Emisor";
            this.ToolBarEmisor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separador
            // 
            this.Separador.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separador.DisplayName = "Separador 2";
            this.Separador.MinSize = new System.Drawing.Size(2, 0);
            this.Separador.Name = "Separador";
            this.Separador.UseCompatibleTextRendering = false;
            this.Separador.VisibleInOverflowMenu = false;
            // 
            // ToolBarLabelNoIdent
            // 
            this.ToolBarLabelNoIdent.DisplayName = "NoIdent";
            this.ToolBarLabelNoIdent.MinSize = new System.Drawing.Size(59, 26);
            this.ToolBarLabelNoIdent.Name = "ToolBarLabelNoIdent";
            this.ToolBarLabelNoIdent.Text = "T00000000";
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisplayName = "commandBarSeparator2";
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // ToolLabelStatus
            // 
            this.ToolLabelStatus.DisplayName = "Etiqueta Status";
            this.ToolLabelStatus.DrawImage = false;
            this.ToolLabelStatus.Name = "ToolLabelStatus";
            this.ToolLabelStatus.Text = "Status:";
            // 
            // ToolSplitStatus
            // 
            this.ToolSplitStatus.DefaultItem = null;
            this.ToolSplitStatus.DisplayName = "Status";
            this.ToolSplitStatus.DrawImage = false;
            this.ToolSplitStatus.DrawText = true;
            this.ToolSplitStatus.Image = ((System.Drawing.Image)(resources.GetObject("ToolSplitStatus.Image")));
            this.ToolSplitStatus.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolSplitStatus.Name = "ToolSplitStatus";
            this.ToolSplitStatus.Text = "Listo";
            this.ToolSplitStatus.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // Separator2
            // 
            this.Separator2.DisplayName = "CommandBarSeparator2";
            this.Separator2.MinSize = new System.Drawing.Size(2, 0);
            this.Separator2.Name = "Separator2";
            this.Separator2.VisibleInOverflowMenu = false;
            // 
            // ToolBarNew
            // 
            this.ToolBarNew.DisplayName = "Nuevo";
            this.ToolBarNew.DrawText = true;
            this.ToolBarNew.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_new_file;
            this.ToolBarNew.Name = "ToolBarNew";
            this.ToolBarNew.Text = "Nuevo";
            this.ToolBarNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarCopy
            // 
            this.ToolBarCopy.DisplayName = "Duplicar";
            this.ToolBarCopy.DrawText = true;
            this.ToolBarCopy.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_copiar;
            this.ToolBarCopy.Name = "ToolBarCopy";
            this.ToolBarCopy.Text = "Duplicar";
            this.ToolBarCopy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarCreate
            // 
            this.ToolBarCreate.DisplayName = "Comprobante de Pago";
            this.ToolBarCreate.DrawText = true;
            this.ToolBarCreate.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_factura;
            this.ToolBarCreate.Name = "ToolBarCreate";
            this.ToolBarCreate.Text = "Comprobante de Pago";
            this.ToolBarCreate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarCreate.Click += new System.EventHandler(this.ToolBarCreate_Click);
            // 
            // ToolBarPrint
            // 
            this.ToolBarPrint.DisplayName = "Imprimir";
            this.ToolBarPrint.DrawImage = true;
            this.ToolBarPrint.DrawText = true;
            this.ToolBarPrint.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_imprimir;
            this.ToolBarPrint.Name = "ToolBarPrint";
            this.ToolBarPrint.Text = "Imprimir";
            this.ToolBarPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarPrint.Click += new System.EventHandler(this.ToolBarPrint_Click);
            // 
            // ToolBarSave
            // 
            this.ToolBarSave.DisplayName = "Guardar";
            this.ToolBarSave.DrawText = true;
            this.ToolBarSave.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_guardar_todo;
            this.ToolBarSave.Name = "ToolBarSave";
            this.ToolBarSave.Text = "Guardar";
            this.ToolBarSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarSave.Click += new System.EventHandler(this.ToolBarSave_Click);
            // 
            // ToolBarRefresh
            // 
            this.ToolBarRefresh.DisplayName = "Actualizar";
            this.ToolBarRefresh.DrawText = true;
            this.ToolBarRefresh.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_actualizar;
            this.ToolBarRefresh.Name = "ToolBarRefresh";
            this.ToolBarRefresh.Text = "Actualizar";
            this.ToolBarRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarRefresh.Click += new System.EventHandler(this.ToolBarRefresh_Click);
            // 
            // ToolBarDescargar
            // 
            this.ToolBarDescargar.DisplayName = "Descargar";
            this.ToolBarDescargar.DrawText = true;
            this.ToolBarDescargar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_descargar_desde_la_nube;
            this.ToolBarDescargar.Name = "ToolBarDescargar";
            this.ToolBarDescargar.Text = "Descargar";
            this.ToolBarDescargar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarDescargar.ToolTipText = "Descargar los comprobantes adjuntos";
            this.ToolBarDescargar.Click += new System.EventHandler(this.ToolBarDescargar_Click);
            // 
            // ToolBarLayout
            // 
            this.ToolBarLayout.DisplayName = "Layout";
            this.ToolBarLayout.DrawText = true;
            this.ToolBarLayout.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_archivo_binario;
            this.ToolBarLayout.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarBancomerPagosInterbancarios,
            this.ToolBarBanamexPagosInterbancarios,
            this.ToolBarBanamexPagosInterbancariosClabe});
            this.ToolBarLayout.Name = "ToolBarLayout";
            this.ToolBarLayout.Text = "Layout";
            this.ToolBarLayout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarBancomerPagosInterbancarios
            // 
            this.ToolBarBancomerPagosInterbancarios.Name = "ToolBarBancomerPagosInterbancarios";
            this.ToolBarBancomerPagosInterbancarios.Text = "Bancomer Traspasos y/o Pagos Interbancarios";
            this.ToolBarBancomerPagosInterbancarios.Click += new System.EventHandler(this.ToolBarBancomerPagosInterbancarios_Click);
            // 
            // ToolBarBanamexPagosInterbancarios
            // 
            this.ToolBarBanamexPagosInterbancarios.Name = "ToolBarBanamexPagosInterbancarios";
            this.ToolBarBanamexPagosInterbancarios.Text = "Banamex Interbancario Layout C";
            this.ToolBarBanamexPagosInterbancarios.ToolTipText = "Layout \"C\" formatos para importación (TEF/TEF INTELAR)";
            this.ToolBarBanamexPagosInterbancarios.Click += new System.EventHandler(this.ToolBarBanamexPagosInterbancarios_Click);
            // 
            // ToolBarBanamexPagosInterbancariosClabe
            // 
            this.ToolBarBanamexPagosInterbancariosClabe.Name = "ToolBarBanamexPagosInterbancariosClabe";
            this.ToolBarBanamexPagosInterbancariosClabe.Text = "Banamex Interbancario Layout C (CLABE)";
            this.ToolBarBanamexPagosInterbancariosClabe.Click += new System.EventHandler(this.ToolBarBanamexPagosInterbancariosClabe_Click);
            // 
            // ToolBarClose
            // 
            this.ToolBarClose.DisplayName = "Cerrar";
            this.ToolBarClose.DrawText = true;
            this.ToolBarClose.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_cerrar;
            this.ToolBarClose.Name = "ToolBarClose";
            this.ToolBarClose.Text = "Cerrar";
            this.ToolBarClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarClose.Click += new System.EventHandler(this.ToolBarClose_Click);
            // 
            // PageView
            // 
            this.PageView.Controls.Add(this.PageViewComprobante);
            this.PageView.Controls.Add(this.PageViewDoctosRelacionados);
            this.PageView.DefaultPage = this.PageViewComprobante;
            this.PageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PageView.Location = new System.Drawing.Point(0, 0);
            this.PageView.Name = "PageView";
            this.PageView.SelectedPage = this.PageViewComprobante;
            this.PageView.Size = new System.Drawing.Size(962, 301);
            this.PageView.TabIndex = 260;
            this.PageView.TabStop = false;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // PageViewComprobante
            // 
            this.PageViewComprobante.Controls.Add(this.GridComprobantes);
            this.PageViewComprobante.Controls.Add(this.radCommandBar2);
            this.PageViewComprobante.ItemSize = new System.Drawing.SizeF(90F, 28F);
            this.PageViewComprobante.Location = new System.Drawing.Point(10, 37);
            this.PageViewComprobante.Name = "PageViewComprobante";
            this.PageViewComprobante.Size = new System.Drawing.Size(941, 253);
            this.PageViewComprobante.Text = "Comprobantes";
            // 
            // GridComprobantes
            // 
            this.GridComprobantes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridComprobantes.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridComprobantes.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.GridComprobantes.MasterTemplate.AllowAddNewRow = false;
            this.GridComprobantes.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn47.FieldName = "Id";
            gridViewTextBoxColumn47.HeaderText = "Index";
            gridViewTextBoxColumn47.IsVisible = false;
            gridViewTextBoxColumn47.Name = "Id";
            gridViewTextBoxColumn47.ReadOnly = true;
            gridViewTextBoxColumn47.VisibleInColumnChooser = false;
            gridViewCheckBoxColumn3.FieldName = "IsActive";
            gridViewCheckBoxColumn3.HeaderText = "IsActive";
            gridViewCheckBoxColumn3.IsVisible = false;
            gridViewCheckBoxColumn3.Name = "IsActive";
            gridViewCheckBoxColumn3.ReadOnly = true;
            gridViewCheckBoxColumn3.VisibleInColumnChooser = false;
            gridViewTextBoxColumn48.FieldName = "TipoComprobante";
            gridViewTextBoxColumn48.HeaderText = "Tipo";
            gridViewTextBoxColumn48.IsVisible = false;
            gridViewTextBoxColumn48.Name = "TipoComprobante";
            gridViewTextBoxColumn48.ReadOnly = true;
            gridViewTextBoxColumn48.VisibleInColumnChooser = false;
            gridViewTextBoxColumn48.Width = 65;
            gridViewMultiComboBoxColumn2.FieldName = "Folio";
            gridViewMultiComboBoxColumn2.HeaderText = "Folio";
            gridViewMultiComboBoxColumn2.Name = "Folio";
            gridViewMultiComboBoxColumn2.ReadOnly = true;
            gridViewMultiComboBoxColumn2.Width = 80;
            gridViewTextBoxColumn49.FieldName = "Serie";
            gridViewTextBoxColumn49.HeaderText = "Serie";
            gridViewTextBoxColumn49.Name = "Serie";
            gridViewTextBoxColumn49.ReadOnly = true;
            gridViewTextBoxColumn50.FieldName = "EmisorRfc";
            gridViewTextBoxColumn50.HeaderText = "RFC";
            gridViewTextBoxColumn50.IsVisible = false;
            gridViewTextBoxColumn50.Name = "RfcEmisor";
            gridViewTextBoxColumn50.ReadOnly = true;
            gridViewTextBoxColumn51.FieldName = "Emisor";
            gridViewTextBoxColumn51.HeaderText = "Identidad Fiscal";
            gridViewTextBoxColumn51.Name = "Emisor";
            gridViewTextBoxColumn51.ReadOnly = true;
            gridViewTextBoxColumn51.Width = 200;
            gridViewTextBoxColumn52.FieldName = "Uuid";
            gridViewTextBoxColumn52.HeaderText = "UUID";
            gridViewTextBoxColumn52.Name = "Uuid";
            gridViewTextBoxColumn52.ReadOnly = true;
            gridViewTextBoxColumn52.Width = 200;
            gridViewTextBoxColumn53.FieldName = "SubTotal";
            gridViewTextBoxColumn53.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn53.HeaderText = "SubTotal";
            gridViewTextBoxColumn53.IsVisible = false;
            gridViewTextBoxColumn53.Name = "SubTotal";
            gridViewTextBoxColumn53.ReadOnly = true;
            gridViewTextBoxColumn53.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn53.Width = 80;
            gridViewTextBoxColumn54.DataType = typeof(decimal);
            gridViewTextBoxColumn54.FieldName = "TrasladoIva";
            gridViewTextBoxColumn54.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn54.HeaderText = "Iva";
            gridViewTextBoxColumn54.IsVisible = false;
            gridViewTextBoxColumn54.Name = "Iva";
            gridViewTextBoxColumn54.ReadOnly = true;
            gridViewTextBoxColumn54.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn54.Width = 80;
            gridViewTextBoxColumn55.DataType = typeof(decimal);
            gridViewTextBoxColumn55.FieldName = "Total";
            gridViewTextBoxColumn55.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn55.HeaderText = "Total";
            gridViewTextBoxColumn55.Name = "Total";
            gridViewTextBoxColumn55.ReadOnly = true;
            gridViewTextBoxColumn55.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn55.Width = 85;
            gridViewTextBoxColumn56.DataType = typeof(decimal);
            gridViewTextBoxColumn56.FieldName = "Acumulado";
            gridViewTextBoxColumn56.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn56.HeaderText = "Acumulado";
            gridViewTextBoxColumn56.Name = "Acumulado";
            gridViewTextBoxColumn56.ReadOnly = true;
            gridViewTextBoxColumn56.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn56.Width = 85;
            gridViewCalculatorColumn3.FieldName = "Cargo";
            gridViewCalculatorColumn3.FormatString = "{0:#,###0.00}";
            gridViewCalculatorColumn3.HeaderText = "Cargo";
            gridViewCalculatorColumn3.Name = "Cargo";
            gridViewCalculatorColumn3.Width = 85;
            gridViewCalculatorColumn4.FieldName = "Abono";
            gridViewCalculatorColumn4.FormatString = "{0:#,###0.00}";
            gridViewCalculatorColumn4.HeaderText = "Abono";
            gridViewCalculatorColumn4.Name = "Abono";
            gridViewCalculatorColumn4.Width = 85;
            gridViewTextBoxColumn57.FieldName = "Estado";
            gridViewTextBoxColumn57.HeaderText = "Estado";
            gridViewTextBoxColumn57.Name = "Estado";
            gridViewTextBoxColumn57.ReadOnly = true;
            gridViewTextBoxColumn57.Width = 80;
            this.GridComprobantes.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn47,
            gridViewCheckBoxColumn3,
            gridViewTextBoxColumn48,
            gridViewMultiComboBoxColumn2,
            gridViewTextBoxColumn49,
            gridViewTextBoxColumn50,
            gridViewTextBoxColumn51,
            gridViewTextBoxColumn52,
            gridViewTextBoxColumn53,
            gridViewTextBoxColumn54,
            gridViewTextBoxColumn55,
            gridViewTextBoxColumn56,
            gridViewCalculatorColumn3,
            gridViewCalculatorColumn4,
            gridViewTextBoxColumn57});
            this.GridComprobantes.MasterTemplate.EnableGrouping = false;
            this.GridComprobantes.MasterTemplate.ViewDefinition = tableViewDefinition9;
            this.GridComprobantes.Name = "GridComprobantes";
            this.GridComprobantes.NewRowEnterKeyMode = Telerik.WinControls.UI.RadGridViewNewRowEnterKeyMode.EnterMovesToNextCell;
            this.GridComprobantes.ShowGroupPanel = false;
            this.GridComprobantes.Size = new System.Drawing.Size(941, 223);
            this.GridComprobantes.TabIndex = 0;
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement2});
            this.radCommandBar2.Size = new System.Drawing.Size(941, 30);
            this.radCommandBar2.TabIndex = 1;
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Name = "commandBarRowElement2";
            this.commandBarRowElement2.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarComprobantes});
            this.commandBarRowElement2.Text = "";
            // 
            // ToolBarComprobantes
            // 
            this.ToolBarComprobantes.DisplayName = "commandBarStripElement1";
            // 
            // 
            // 
            this.ToolBarComprobantes.Grip.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ToolBarComprobantes.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarComprobantesNuevo,
            this.ToolBarComprobantesAgregar,
            this.ToolBarComprobantesQuitar,
            this.commandBarSeparator1,
            this.ToolBarTipoDocumento,
            this.ToolBarDocumentos});
            this.ToolBarComprobantes.Name = "ToolBarComprobantes";
            ((Telerik.WinControls.UI.RadCommandBarGrip)(this.ToolBarComprobantes.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // ToolBarComprobantesNuevo
            // 
            this.ToolBarComprobantesNuevo.DisplayName = "commandBarButton1";
            this.ToolBarComprobantesNuevo.DrawText = true;
            this.ToolBarComprobantesNuevo.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_new_file;
            this.ToolBarComprobantesNuevo.Name = "ToolBarComprobantesNuevo";
            this.ToolBarComprobantesNuevo.Text = "Nuevo";
            this.ToolBarComprobantesNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarComprobantesAgregar
            // 
            this.ToolBarComprobantesAgregar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarComprobantesAgregar.DisplayName = "commandBarButton2";
            this.ToolBarComprobantesAgregar.DrawText = true;
            this.ToolBarComprobantesAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_agregar_archivo;
            this.ToolBarComprobantesAgregar.Name = "ToolBarComprobantesAgregar";
            this.ToolBarComprobantesAgregar.Text = "Agregar";
            this.ToolBarComprobantesAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarComprobantesAgregar.Click += new System.EventHandler(this.ToolBarComprobantesAgregar_Click);
            // 
            // ToolBarComprobantesQuitar
            // 
            this.ToolBarComprobantesQuitar.DisplayName = "commandBarButton3";
            this.ToolBarComprobantesQuitar.DrawText = true;
            this.ToolBarComprobantesQuitar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_delete_file;
            this.ToolBarComprobantesQuitar.Name = "ToolBarComprobantesQuitar";
            this.ToolBarComprobantesQuitar.Text = "Quitar";
            this.ToolBarComprobantesQuitar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarComprobantesQuitar.Click += new System.EventHandler(this.ToolBarComprobantesQuitar_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarTipoDocumento
            // 
            this.ToolBarTipoDocumento.DefaultItem = null;
            this.ToolBarTipoDocumento.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarTipoDocumento.DisplayName = "Documento";
            this.ToolBarTipoDocumento.DrawImage = false;
            this.ToolBarTipoDocumento.DrawText = true;
            this.ToolBarTipoDocumento.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarTipoDocumento.Image")));
            this.ToolBarTipoDocumento.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarDocumentoRemision,
            this.ToolBarDocumentoFactura});
            this.ToolBarTipoDocumento.Name = "ToolBarTipoDocumento";
            this.ToolBarTipoDocumento.Text = "Documento";
            // 
            // ToolBarDocumentoRemision
            // 
            this.ToolBarDocumentoRemision.Name = "ToolBarDocumentoRemision";
            this.ToolBarDocumentoRemision.Text = "Remisión";
            this.ToolBarDocumentoRemision.UseCompatibleTextRendering = false;
            this.ToolBarDocumentoRemision.Click += new System.EventHandler(this.TipoDocumento_Click);
            // 
            // ToolBarDocumentoFactura
            // 
            this.ToolBarDocumentoFactura.Name = "ToolBarDocumentoFactura";
            this.ToolBarDocumentoFactura.Text = "Factura";
            this.ToolBarDocumentoFactura.UseCompatibleTextRendering = false;
            this.ToolBarDocumentoFactura.Click += new System.EventHandler(this.TipoDocumento_Click);
            // 
            // ToolBarDocumentos
            // 
            this.ToolBarDocumentos.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarDocumentos.DisplayName = "Comprobantes";
            this.ToolBarDocumentos.MinSize = new System.Drawing.Size(600, 20);
            this.ToolBarDocumentos.Name = "ToolBarDocumentos";
            this.ToolBarDocumentos.Text = "Comprobantes";
            this.ToolBarDocumentos.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarDocumentos.UseCompatibleTextRendering = false;
            // 
            // PageViewDoctosRelacionados
            // 
            this.PageViewDoctosRelacionados.Controls.Add(this.GridDoctosRelacionados);
            this.PageViewDoctosRelacionados.Controls.Add(this.radCommandBar3);
            this.PageViewDoctosRelacionados.ItemSize = new System.Drawing.SizeF(123F, 28F);
            this.PageViewDoctosRelacionados.Location = new System.Drawing.Point(10, 37);
            this.PageViewDoctosRelacionados.Name = "PageViewDoctosRelacionados";
            this.PageViewDoctosRelacionados.Size = new System.Drawing.Size(939, 282);
            this.PageViewDoctosRelacionados.Text = "Doctos. Relacionados";
            // 
            // GridDoctosRelacionados
            // 
            this.GridDoctosRelacionados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridDoctosRelacionados.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            gridViewTextBoxColumn58.FieldName = "Titulo";
            gridViewTextBoxColumn58.HeaderText = "Título";
            gridViewTextBoxColumn58.Name = "Titulo";
            gridViewTextBoxColumn58.Width = 120;
            gridViewTextBoxColumn59.FieldName = "Descripcion";
            gridViewTextBoxColumn59.HeaderText = "Descripción";
            gridViewTextBoxColumn59.Name = "Descripcion";
            gridViewTextBoxColumn59.Width = 220;
            gridViewTextBoxColumn60.FieldName = "Content";
            gridViewTextBoxColumn60.HeaderText = "Tipo";
            gridViewTextBoxColumn60.Name = "Tipo";
            gridViewTextBoxColumn60.Width = 90;
            gridViewCommandColumn2.FieldName = "Descargar";
            gridViewCommandColumn2.HeaderText = "Documento";
            gridViewCommandColumn2.Name = "Descargar";
            gridViewCommandColumn2.Width = 85;
            this.GridDoctosRelacionados.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn58,
            gridViewTextBoxColumn59,
            gridViewTextBoxColumn60,
            gridViewCommandColumn2});
            this.GridDoctosRelacionados.MasterTemplate.ViewDefinition = tableViewDefinition10;
            this.GridDoctosRelacionados.Name = "GridDoctosRelacionados";
            this.GridDoctosRelacionados.Size = new System.Drawing.Size(939, 282);
            this.GridDoctosRelacionados.TabIndex = 1;
            // 
            // radCommandBar3
            // 
            this.radCommandBar3.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar3.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar3.Name = "radCommandBar3";
            this.radCommandBar3.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar3.Size = new System.Drawing.Size(939, 0);
            this.radCommandBar3.TabIndex = 0;
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.Name = "commandBarRowElement3";
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarDoctosButtonAgregar,
            this.ToolBarDoctosButtonQuitar});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // ToolBarDoctosButtonAgregar
            // 
            this.ToolBarDoctosButtonAgregar.DisplayName = "commandBarButton1";
            this.ToolBarDoctosButtonAgregar.DrawText = true;
            this.ToolBarDoctosButtonAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_agregar_archivo;
            this.ToolBarDoctosButtonAgregar.Name = "ToolBarDoctosButtonAgregar";
            this.ToolBarDoctosButtonAgregar.Text = "Agregar";
            this.ToolBarDoctosButtonAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarDoctosButtonAgregar.Click += new System.EventHandler(this.ToolBarDoctosButtonAgregar_Click);
            // 
            // ToolBarDoctosButtonQuitar
            // 
            this.ToolBarDoctosButtonQuitar.DisplayName = "commandBarButton2";
            this.ToolBarDoctosButtonQuitar.DrawText = true;
            this.ToolBarDoctosButtonQuitar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_delete_file;
            this.ToolBarDoctosButtonQuitar.Name = "ToolBarDoctosButtonQuitar";
            this.ToolBarDoctosButtonQuitar.Text = "Eliminar";
            this.ToolBarDoctosButtonQuitar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarDoctosButtonQuitar.Click += new System.EventHandler(this.ToolBarDoctosButtonQuitar_Click);
            // 
            // CboComprobantes
            // 
            // 
            // CboComprobantes.NestedRadGridView
            // 
            this.CboComprobantes.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboComprobantes.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboComprobantes.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboComprobantes.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboComprobantes.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboComprobantes.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboComprobantes.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn61.FieldName = "Id";
            gridViewTextBoxColumn61.HeaderText = "Id";
            gridViewTextBoxColumn61.IsVisible = false;
            gridViewTextBoxColumn61.Name = "Id";
            gridViewTextBoxColumn61.VisibleInColumnChooser = false;
            gridViewTextBoxColumn62.FieldName = "SubId";
            gridViewTextBoxColumn62.HeaderText = "SubId";
            gridViewTextBoxColumn62.IsVisible = false;
            gridViewTextBoxColumn62.Name = "SubId";
            gridViewTextBoxColumn62.VisibleInColumnChooser = false;
            gridViewCheckBoxColumn4.FieldName = "IsActive";
            gridViewCheckBoxColumn4.HeaderText = "IsActive";
            gridViewCheckBoxColumn4.IsVisible = false;
            gridViewCheckBoxColumn4.Name = "IsActive";
            gridViewCheckBoxColumn4.VisibleInColumnChooser = false;
            gridViewTextBoxColumn63.FieldName = "Status";
            gridViewTextBoxColumn63.HeaderText = "Status";
            gridViewTextBoxColumn63.Name = "Status";
            gridViewTextBoxColumn64.FieldName = "Folio";
            gridViewTextBoxColumn64.HeaderText = "Folio";
            gridViewTextBoxColumn64.Name = "Folio";
            gridViewTextBoxColumn65.FieldName = "Serie";
            gridViewTextBoxColumn65.HeaderText = "Serie";
            gridViewTextBoxColumn65.Name = "Serie";
            gridViewTextBoxColumn66.FieldName = "Uuid";
            gridViewTextBoxColumn66.HeaderText = "UUID";
            gridViewTextBoxColumn66.Name = "Uuid";
            gridViewTextBoxColumn67.FieldName = "EmisorRfc";
            gridViewTextBoxColumn67.HeaderText = "Emisor (RFC)";
            gridViewTextBoxColumn67.Name = "RfcEmisor";
            gridViewTextBoxColumn68.FieldName = "Emisor";
            gridViewTextBoxColumn68.HeaderText = "Emisor";
            gridViewTextBoxColumn68.Name = "Emisor";
            gridViewTextBoxColumn69.FieldName = "ReceptorRfc";
            gridViewTextBoxColumn69.HeaderText = "Receptor (RFC)";
            gridViewTextBoxColumn69.IsVisible = false;
            gridViewTextBoxColumn69.Name = "RfcReceptor";
            gridViewTextBoxColumn70.FieldName = "Receptor";
            gridViewTextBoxColumn70.HeaderText = "Receptor";
            gridViewTextBoxColumn70.IsVisible = false;
            gridViewTextBoxColumn70.Name = "Receptor";
            gridViewTextBoxColumn71.FieldName = "Estado";
            gridViewTextBoxColumn71.HeaderText = "Estado";
            gridViewTextBoxColumn71.Name = "Estado";
            gridViewTextBoxColumn72.DataType = typeof(double);
            gridViewTextBoxColumn72.FieldName = "Total";
            gridViewTextBoxColumn72.HeaderText = "Total";
            gridViewTextBoxColumn72.Name = "Total";
            gridViewTextBoxColumn72.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn73.FieldName = "Acumulado";
            gridViewTextBoxColumn73.HeaderText = "Acumulado";
            gridViewTextBoxColumn73.Name = "Acumulado";
            gridViewTextBoxColumn74.FieldName = "TipoComprobante";
            gridViewTextBoxColumn74.HeaderText = "TipoComprobante";
            gridViewTextBoxColumn74.IsVisible = false;
            gridViewTextBoxColumn74.Name = "TipoComprobante";
            this.CboComprobantes.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn61,
            gridViewTextBoxColumn62,
            gridViewCheckBoxColumn4,
            gridViewTextBoxColumn63,
            gridViewTextBoxColumn64,
            gridViewTextBoxColumn65,
            gridViewTextBoxColumn66,
            gridViewTextBoxColumn67,
            gridViewTextBoxColumn68,
            gridViewTextBoxColumn69,
            gridViewTextBoxColumn70,
            gridViewTextBoxColumn71,
            gridViewTextBoxColumn72,
            gridViewTextBoxColumn73,
            gridViewTextBoxColumn74});
            this.CboComprobantes.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboComprobantes.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboComprobantes.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition11;
            this.CboComprobantes.EditorControl.Name = "NestedRadGridView";
            this.CboComprobantes.EditorControl.ReadOnly = true;
            this.CboComprobantes.EditorControl.ShowGroupPanel = false;
            this.CboComprobantes.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboComprobantes.EditorControl.TabIndex = 0;
            this.CboComprobantes.Location = new System.Drawing.Point(1132, 171);
            this.CboComprobantes.Name = "CboComprobantes";
            this.CboComprobantes.Size = new System.Drawing.Size(43, 20);
            this.CboComprobantes.TabIndex = 2;
            this.CboComprobantes.TabStop = false;
            // 
            // TxbEmisorRfc
            // 
            this.TxbEmisorRfc.Location = new System.Drawing.Point(549, 7);
            this.TxbEmisorRfc.Name = "TxbEmisorRfc";
            this.TxbEmisorRfc.NullText = "RFC";
            this.TxbEmisorRfc.ReadOnly = true;
            this.TxbEmisorRfc.Size = new System.Drawing.Size(104, 20);
            this.TxbEmisorRfc.TabIndex = 282;
            this.TxbEmisorRfc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel22
            // 
            this.radLabel22.Location = new System.Drawing.Point(515, 7);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(28, 18);
            this.radLabel22.TabIndex = 281;
            this.radLabel22.Text = "RFC:";
            // 
            // CboEmisorCuenta
            // 
            // 
            // CboEmisorCuenta.NestedRadGridView
            // 
            this.CboEmisorCuenta.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboEmisorCuenta.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboEmisorCuenta.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboEmisorCuenta.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboEmisorCuenta.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboEmisorCuenta.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboEmisorCuenta.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn75.FieldName = "Banco";
            gridViewTextBoxColumn75.HeaderText = "Banco";
            gridViewTextBoxColumn75.Name = "Banco";
            gridViewTextBoxColumn76.FieldName = "Beneficiario";
            gridViewTextBoxColumn76.HeaderText = "Beneficiario";
            gridViewTextBoxColumn76.Name = "Beneficiario";
            gridViewTextBoxColumn77.FieldName = "Alias";
            gridViewTextBoxColumn77.HeaderText = "Alias";
            gridViewTextBoxColumn77.Name = "Alias";
            gridViewTextBoxColumn78.FieldName = "NumeroDeCuenta";
            gridViewTextBoxColumn78.HeaderText = "Cuenta";
            gridViewTextBoxColumn78.Name = "NumeroDeCuenta";
            gridViewTextBoxColumn79.FieldName = "Clave";
            gridViewTextBoxColumn79.HeaderText = "Clave";
            gridViewTextBoxColumn79.Name = "Clave";
            this.CboEmisorCuenta.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn75,
            gridViewTextBoxColumn76,
            gridViewTextBoxColumn77,
            gridViewTextBoxColumn78,
            gridViewTextBoxColumn79});
            this.CboEmisorCuenta.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboEmisorCuenta.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboEmisorCuenta.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition12;
            this.CboEmisorCuenta.EditorControl.Name = "NestedRadGridView";
            this.CboEmisorCuenta.EditorControl.ReadOnly = true;
            this.CboEmisorCuenta.EditorControl.ShowGroupPanel = false;
            this.CboEmisorCuenta.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboEmisorCuenta.EditorControl.TabIndex = 0;
            this.CboEmisorCuenta.Location = new System.Drawing.Point(92, 7);
            this.CboEmisorCuenta.Name = "CboEmisorCuenta";
            this.CboEmisorCuenta.NullText = "Selecciona";
            this.CboEmisorCuenta.Size = new System.Drawing.Size(417, 20);
            this.CboEmisorCuenta.TabIndex = 280;
            this.CboEmisorCuenta.TabStop = false;
            // 
            // TxbEmisorClabe
            // 
            this.TxbEmisorClabe.Location = new System.Drawing.Point(699, 32);
            this.TxbEmisorClabe.Name = "TxbEmisorClabe";
            this.TxbEmisorClabe.NullText = "Clabe";
            this.TxbEmisorClabe.ReadOnly = true;
            this.TxbEmisorClabe.Size = new System.Drawing.Size(110, 20);
            this.TxbEmisorClabe.TabIndex = 279;
            this.TxbEmisorClabe.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxbEmisorSucursal
            // 
            this.TxbEmisorSucursal.Location = new System.Drawing.Point(549, 32);
            this.TxbEmisorSucursal.Name = "TxbEmisorSucursal";
            this.TxbEmisorSucursal.NullText = "Sucursal";
            this.TxbEmisorSucursal.ReadOnly = true;
            this.TxbEmisorSucursal.Size = new System.Drawing.Size(104, 20);
            this.TxbEmisorSucursal.TabIndex = 278;
            this.TxbEmisorSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxbEmisorBanco
            // 
            this.TxbEmisorBanco.Location = new System.Drawing.Point(243, 32);
            this.TxbEmisorBanco.Name = "TxbEmisorBanco";
            this.TxbEmisorBanco.NullText = "Banco";
            this.TxbEmisorBanco.ReadOnly = true;
            this.TxbEmisorBanco.Size = new System.Drawing.Size(251, 20);
            this.TxbEmisorBanco.TabIndex = 277;
            // 
            // RadLabel15
            // 
            this.RadLabel15.Location = new System.Drawing.Point(659, 33);
            this.RadLabel15.Name = "RadLabel15";
            this.RadLabel15.Size = new System.Drawing.Size(37, 18);
            this.RadLabel15.TabIndex = 275;
            this.RadLabel15.Text = "Clabe:";
            // 
            // RadLabel16
            // 
            this.RadLabel16.Location = new System.Drawing.Point(498, 32);
            this.RadLabel16.Name = "RadLabel16";
            this.RadLabel16.Size = new System.Drawing.Size(50, 18);
            this.RadLabel16.TabIndex = 274;
            this.RadLabel16.Text = "Sucursal:";
            // 
            // RadLabel14
            // 
            this.RadLabel14.Location = new System.Drawing.Point(198, 32);
            this.RadLabel14.Name = "RadLabel14";
            this.RadLabel14.Size = new System.Drawing.Size(39, 18);
            this.RadLabel14.TabIndex = 273;
            this.RadLabel14.Text = "Banco:";
            // 
            // RadLabel13
            // 
            this.RadLabel13.Location = new System.Drawing.Point(7, 33);
            this.RadLabel13.Name = "RadLabel13";
            this.RadLabel13.Size = new System.Drawing.Size(74, 18);
            this.RadLabel13.TabIndex = 272;
            this.RadLabel13.Text = "Núm. Cuenta:";
            // 
            // RadLabel11
            // 
            this.RadLabel11.Location = new System.Drawing.Point(7, 8);
            this.RadLabel11.Name = "RadLabel11";
            this.RadLabel11.Size = new System.Drawing.Size(65, 18);
            this.RadLabel11.TabIndex = 271;
            this.RadLabel11.Text = "Cta. Origen:";
            // 
            // FechaDePago
            // 
            this.FechaDePago.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaDePago.Location = new System.Drawing.Point(714, 57);
            this.FechaDePago.Name = "FechaDePago";
            this.FechaDePago.Size = new System.Drawing.Size(95, 20);
            this.FechaDePago.TabIndex = 290;
            this.FechaDePago.TabStop = false;
            this.FechaDePago.Text = "15/12/2016";
            this.FechaDePago.Value = new System.DateTime(2016, 12, 15, 20, 4, 51, 874);
            // 
            // FechaDocumento
            // 
            this.FechaDocumento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaDocumento.Location = new System.Drawing.Point(556, 57);
            this.FechaDocumento.Name = "FechaDocumento";
            this.FechaDocumento.Size = new System.Drawing.Size(85, 20);
            this.FechaDocumento.TabIndex = 289;
            this.FechaDocumento.TabStop = false;
            this.FechaDocumento.Text = "15/12/2016";
            this.FechaDocumento.Value = new System.DateTime(2016, 12, 15, 20, 4, 51, 874);
            // 
            // TxbNumDocto
            // 
            this.TxbNumDocto.Location = new System.Drawing.Point(359, 57);
            this.TxbNumDocto.Name = "TxbNumDocto";
            this.TxbNumDocto.NullText = "Folio";
            this.TxbNumDocto.Size = new System.Drawing.Size(99, 20);
            this.TxbNumDocto.TabIndex = 288;
            this.TxbNumDocto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CboFormaDePago
            // 
            this.CboFormaDePago.AutoSizeDropDownToBestFit = true;
            // 
            // CboFormaDePago.NestedRadGridView
            // 
            this.CboFormaDePago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboFormaDePago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboFormaDePago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboFormaDePago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboFormaDePago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboFormaDePago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboFormaDePago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn80.FieldName = "Clave";
            gridViewTextBoxColumn80.HeaderText = "Clave";
            gridViewTextBoxColumn80.Name = "Clave";
            gridViewTextBoxColumn81.FieldName = "Descripcion";
            gridViewTextBoxColumn81.HeaderText = "Método";
            gridViewTextBoxColumn81.Name = "Descripcion";
            gridViewTextBoxColumn81.Width = 80;
            this.CboFormaDePago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn80,
            gridViewTextBoxColumn81});
            this.CboFormaDePago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboFormaDePago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboFormaDePago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition13;
            this.CboFormaDePago.EditorControl.Name = "NestedRadGridView";
            this.CboFormaDePago.EditorControl.ReadOnly = true;
            this.CboFormaDePago.EditorControl.ShowGroupPanel = false;
            this.CboFormaDePago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboFormaDePago.EditorControl.TabIndex = 0;
            this.CboFormaDePago.Location = new System.Drawing.Point(92, 57);
            this.CboFormaDePago.Name = "CboFormaDePago";
            this.CboFormaDePago.NullText = "Forma de pago";
            this.CboFormaDePago.Size = new System.Drawing.Size(222, 20);
            this.CboFormaDePago.TabIndex = 287;
            this.CboFormaDePago.TabStop = false;
            // 
            // RadLabel9
            // 
            this.RadLabel9.Location = new System.Drawing.Point(646, 57);
            this.RadLabel9.Name = "RadLabel9";
            this.RadLabel9.Size = new System.Drawing.Size(66, 18);
            this.RadLabel9.TabIndex = 286;
            this.RadLabel9.Text = "Fecha Pago:";
            // 
            // RadLabel8
            // 
            this.RadLabel8.Location = new System.Drawing.Point(479, 58);
            this.RadLabel8.Name = "RadLabel8";
            this.RadLabel8.Size = new System.Drawing.Size(71, 18);
            this.RadLabel8.TabIndex = 285;
            this.RadLabel8.Text = "Fecha Docto:";
            // 
            // RadLabel7
            // 
            this.RadLabel7.Location = new System.Drawing.Point(320, 60);
            this.RadLabel7.Name = "RadLabel7";
            this.RadLabel7.Size = new System.Drawing.Size(33, 18);
            this.RadLabel7.TabIndex = 284;
            this.RadLabel7.Text = "Folio:";
            // 
            // RadLabel2
            // 
            this.RadLabel2.Location = new System.Drawing.Point(7, 58);
            this.RadLabel2.Name = "RadLabel2";
            this.RadLabel2.Size = new System.Drawing.Size(85, 18);
            this.RadLabel2.TabIndex = 283;
            this.RadLabel2.Text = "Forma de pago:";
            // 
            // TxbReceptorRfc
            // 
            this.TxbReceptorRfc.Location = new System.Drawing.Point(549, 82);
            this.TxbReceptorRfc.Name = "TxbReceptorRfc";
            this.TxbReceptorRfc.NullText = "RFC";
            this.TxbReceptorRfc.ReadOnly = true;
            this.TxbReceptorRfc.Size = new System.Drawing.Size(104, 20);
            this.TxbReceptorRfc.TabIndex = 295;
            this.TxbReceptorRfc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // RadLabel10
            // 
            this.RadLabel10.Location = new System.Drawing.Point(515, 83);
            this.RadLabel10.Name = "RadLabel10";
            this.RadLabel10.Size = new System.Drawing.Size(28, 18);
            this.RadLabel10.TabIndex = 294;
            this.RadLabel10.Text = "RFC:";
            // 
            // RadLabel1
            // 
            this.RadLabel1.Location = new System.Drawing.Point(7, 83);
            this.RadLabel1.Name = "RadLabel1";
            this.RadLabel1.Size = new System.Drawing.Size(67, 18);
            this.RadLabel1.TabIndex = 292;
            this.RadLabel1.Text = "Beneficiario:";
            // 
            // CboReceptorBanco
            // 
            // 
            // CboReceptorBanco.NestedRadGridView
            // 
            this.CboReceptorBanco.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboReceptorBanco.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboReceptorBanco.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboReceptorBanco.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboReceptorBanco.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboReceptorBanco.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboReceptorBanco.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn82.FieldName = "Clave";
            gridViewTextBoxColumn82.HeaderText = "Clave";
            gridViewTextBoxColumn82.Name = "Clave";
            gridViewTextBoxColumn83.FieldName = "Descripcion";
            gridViewTextBoxColumn83.HeaderText = "Nombre Corto";
            gridViewTextBoxColumn83.Name = "Descripcion";
            gridViewTextBoxColumn83.Width = 140;
            gridViewTextBoxColumn84.FieldName = "RazonSocial";
            gridViewTextBoxColumn84.HeaderText = "Institución";
            gridViewTextBoxColumn84.Name = "RazonSocial";
            gridViewTextBoxColumn84.Width = 220;
            this.CboReceptorBanco.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn82,
            gridViewTextBoxColumn83,
            gridViewTextBoxColumn84});
            this.CboReceptorBanco.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboReceptorBanco.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboReceptorBanco.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition14;
            this.CboReceptorBanco.EditorControl.Name = "NestedRadGridView";
            this.CboReceptorBanco.EditorControl.ReadOnly = true;
            this.CboReceptorBanco.EditorControl.ShowGroupPanel = false;
            this.CboReceptorBanco.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboReceptorBanco.EditorControl.TabIndex = 0;
            this.CboReceptorBanco.Location = new System.Drawing.Point(243, 107);
            this.CboReceptorBanco.Name = "CboReceptorBanco";
            this.CboReceptorBanco.NullText = "Catálogo de Bancos";
            this.CboReceptorBanco.Size = new System.Drawing.Size(251, 20);
            this.CboReceptorBanco.TabIndex = 303;
            this.CboReceptorBanco.TabStop = false;
            // 
            // TxbReceptorClabe
            // 
            this.TxbReceptorClabe.Location = new System.Drawing.Point(699, 107);
            this.TxbReceptorClabe.Name = "TxbReceptorClabe";
            this.TxbReceptorClabe.NullText = "Clabe";
            this.TxbReceptorClabe.Size = new System.Drawing.Size(110, 20);
            this.TxbReceptorClabe.TabIndex = 302;
            this.TxbReceptorClabe.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxbReceptorSucursal
            // 
            this.TxbReceptorSucursal.Location = new System.Drawing.Point(549, 107);
            this.TxbReceptorSucursal.Name = "TxbReceptorSucursal";
            this.TxbReceptorSucursal.NullText = "Sucursal";
            this.TxbReceptorSucursal.Size = new System.Drawing.Size(104, 20);
            this.TxbReceptorSucursal.TabIndex = 301;
            this.TxbReceptorSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // RadLabel3
            // 
            this.RadLabel3.Location = new System.Drawing.Point(198, 108);
            this.RadLabel3.Name = "RadLabel3";
            this.RadLabel3.Size = new System.Drawing.Size(39, 18);
            this.RadLabel3.TabIndex = 296;
            this.RadLabel3.Text = "Banco:";
            // 
            // RadLabel6
            // 
            this.RadLabel6.Location = new System.Drawing.Point(7, 108);
            this.RadLabel6.Name = "RadLabel6";
            this.RadLabel6.Size = new System.Drawing.Size(74, 18);
            this.RadLabel6.TabIndex = 299;
            this.RadLabel6.Text = "Núm. Cuenta:";
            // 
            // RadLabel5
            // 
            this.RadLabel5.Location = new System.Drawing.Point(659, 108);
            this.RadLabel5.Name = "RadLabel5";
            this.RadLabel5.Size = new System.Drawing.Size(37, 18);
            this.RadLabel5.TabIndex = 298;
            this.RadLabel5.Text = "Clabe:";
            // 
            // RadLabel4
            // 
            this.RadLabel4.Location = new System.Drawing.Point(500, 108);
            this.RadLabel4.Name = "RadLabel4";
            this.RadLabel4.Size = new System.Drawing.Size(50, 18);
            this.RadLabel4.TabIndex = 297;
            this.RadLabel4.Text = "Sucursal:";
            // 
            // TxbNotas
            // 
            this.TxbNotas.Location = new System.Drawing.Point(92, 182);
            this.TxbNotas.Name = "TxbNotas";
            this.TxbNotas.NullText = "Observaciones";
            this.TxbNotas.Size = new System.Drawing.Size(523, 20);
            this.TxbNotas.TabIndex = 311;
            // 
            // TxbConcepto
            // 
            this.TxbConcepto.Location = new System.Drawing.Point(92, 157);
            this.TxbConcepto.Name = "TxbConcepto";
            this.TxbConcepto.NullText = "Concepto";
            this.TxbConcepto.Size = new System.Drawing.Size(523, 20);
            this.TxbConcepto.TabIndex = 310;
            // 
            // TxbNumAuto
            // 
            this.TxbNumAuto.Location = new System.Drawing.Point(572, 132);
            this.TxbNumAuto.Name = "TxbNumAuto";
            this.TxbNumAuto.NullText = "Núm. Autorización";
            this.TxbNumAuto.Size = new System.Drawing.Size(238, 20);
            this.TxbNumAuto.TabIndex = 309;
            // 
            // TxbReferencia
            // 
            this.TxbReferencia.Location = new System.Drawing.Point(92, 132);
            this.TxbReferencia.Name = "TxbReferencia";
            this.TxbReferencia.NullText = "Referencia Alfanumérica";
            this.TxbReferencia.Size = new System.Drawing.Size(145, 20);
            this.TxbReferencia.TabIndex = 308;
            // 
            // RadLabel21
            // 
            this.RadLabel21.Location = new System.Drawing.Point(7, 183);
            this.RadLabel21.Name = "RadLabel21";
            this.RadLabel21.Size = new System.Drawing.Size(81, 18);
            this.RadLabel21.TabIndex = 307;
            this.RadLabel21.Text = "Observaciones:";
            // 
            // RadLabel18
            // 
            this.RadLabel18.Location = new System.Drawing.Point(7, 158);
            this.RadLabel18.Name = "RadLabel18";
            this.RadLabel18.Size = new System.Drawing.Size(57, 18);
            this.RadLabel18.TabIndex = 306;
            this.RadLabel18.Text = "Concepto:";
            // 
            // RadLabel19
            // 
            this.RadLabel19.Location = new System.Drawing.Point(464, 133);
            this.RadLabel19.Name = "RadLabel19";
            this.RadLabel19.Size = new System.Drawing.Size(102, 18);
            this.RadLabel19.TabIndex = 305;
            this.RadLabel19.Text = "Núm. Autorización:";
            // 
            // RadLabel17
            // 
            this.RadLabel17.Location = new System.Drawing.Point(7, 133);
            this.RadLabel17.Name = "RadLabel17";
            this.RadLabel17.Size = new System.Drawing.Size(61, 18);
            this.RadLabel17.TabIndex = 304;
            this.RadLabel17.Text = "Referencia:";
            // 
            // TxbCargo
            // 
            this.TxbCargo.Location = new System.Drawing.Point(820, 157);
            this.TxbCargo.Mask = "c";
            this.TxbCargo.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TxbCargo.Name = "TxbCargo";
            this.TxbCargo.NullText = "Importe";
            this.TxbCargo.Size = new System.Drawing.Size(112, 20);
            this.TxbCargo.TabIndex = 315;
            this.TxbCargo.TabStop = false;
            this.TxbCargo.Text = "$0.00";
            this.TxbCargo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LabelCargo
            // 
            this.LabelCargo.Location = new System.Drawing.Point(775, 158);
            this.LabelCargo.Name = "LabelCargo";
            this.LabelCargo.Size = new System.Drawing.Size(39, 18);
            this.LabelCargo.TabIndex = 314;
            this.LabelCargo.Text = "Cargo:";
            // 
            // TxbAbono
            // 
            this.TxbAbono.Location = new System.Drawing.Point(820, 182);
            this.TxbAbono.Mask = "c";
            this.TxbAbono.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TxbAbono.Name = "TxbAbono";
            this.TxbAbono.NullText = "Importe";
            this.TxbAbono.Size = new System.Drawing.Size(112, 20);
            this.TxbAbono.TabIndex = 313;
            this.TxbAbono.TabStop = false;
            this.TxbAbono.Text = "$0.00";
            this.TxbAbono.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LabelAbono
            // 
            this.LabelAbono.Location = new System.Drawing.Point(775, 183);
            this.LabelAbono.Name = "LabelAbono";
            this.LabelAbono.Size = new System.Drawing.Size(42, 18);
            this.LabelAbono.TabIndex = 312;
            this.LabelAbono.Text = "Abono:";
            // 
            // ChkPorJustificar
            // 
            this.ChkPorJustificar.Location = new System.Drawing.Point(821, 84);
            this.ChkPorJustificar.Name = "ChkPorJustificar";
            this.ChkPorJustificar.Size = new System.Drawing.Size(111, 18);
            this.ChkPorJustificar.TabIndex = 317;
            this.ChkPorJustificar.Text = "Pago por justificar";
            // 
            // ChkParaAbono
            // 
            this.ChkParaAbono.Location = new System.Drawing.Point(820, 111);
            this.ChkParaAbono.Name = "ChkParaAbono";
            this.ChkParaAbono.Size = new System.Drawing.Size(131, 33);
            this.ChkParaAbono.TabIndex = 316;
            this.ChkParaAbono.Text = "Para abono a cuenta \r\ndel beneficiario";
            // 
            // FechaPoliza
            // 
            this.FechaPoliza.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaPoliza.Location = new System.Drawing.Point(842, 7);
            this.FechaPoliza.Name = "FechaPoliza";
            this.FechaPoliza.Size = new System.Drawing.Size(90, 20);
            this.FechaPoliza.TabIndex = 319;
            this.FechaPoliza.TabStop = false;
            this.FechaPoliza.Text = "15/12/2016";
            this.FechaPoliza.Value = new System.DateTime(2016, 12, 15, 20, 4, 51, 874);
            // 
            // TxbEmisorNumeroCta
            // 
            this.TxbEmisorNumeroCta.Location = new System.Drawing.Point(92, 32);
            this.TxbEmisorNumeroCta.Name = "TxbEmisorNumeroCta";
            this.TxbEmisorNumeroCta.NullText = "Núm. Cuenta";
            this.TxbEmisorNumeroCta.Size = new System.Drawing.Size(100, 20);
            this.TxbEmisorNumeroCta.TabIndex = 320;
            // 
            // CboReceptorCuenta
            // 
            this.CboReceptorCuenta.AutoSizeDropDownHeight = true;
            this.CboReceptorCuenta.AutoSizeDropDownToBestFit = true;
            // 
            // CboReceptorCuenta.NestedRadGridView
            // 
            this.CboReceptorCuenta.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboReceptorCuenta.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboReceptorCuenta.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboReceptorCuenta.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboReceptorCuenta.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboReceptorCuenta.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboReceptorCuenta.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn85.FieldName = "NumeroDeCuenta";
            gridViewTextBoxColumn85.HeaderText = "Numero de Cuenta";
            gridViewTextBoxColumn85.Name = "NumeroDeCuenta";
            gridViewTextBoxColumn86.FieldName = "InsitucionBancaria";
            gridViewTextBoxColumn86.HeaderText = "Banco";
            gridViewTextBoxColumn86.Name = "Banco";
            gridViewTextBoxColumn87.FieldName = "Clabe";
            gridViewTextBoxColumn87.HeaderText = "CLABE";
            gridViewTextBoxColumn87.Name = "Clabe";
            gridViewTextBoxColumn87.Width = 90;
            gridViewTextBoxColumn88.FieldName = "Alias";
            gridViewTextBoxColumn88.HeaderText = "Alias";
            gridViewTextBoxColumn88.Name = "Alias";
            this.CboReceptorCuenta.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn85,
            gridViewTextBoxColumn86,
            gridViewTextBoxColumn87,
            gridViewTextBoxColumn88});
            this.CboReceptorCuenta.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboReceptorCuenta.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboReceptorCuenta.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition15;
            this.CboReceptorCuenta.EditorControl.Name = "NestedRadGridView";
            this.CboReceptorCuenta.EditorControl.ReadOnly = true;
            this.CboReceptorCuenta.EditorControl.ShowGroupPanel = false;
            this.CboReceptorCuenta.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboReceptorCuenta.EditorControl.TabIndex = 0;
            this.CboReceptorCuenta.Location = new System.Drawing.Point(92, 107);
            this.CboReceptorCuenta.Name = "CboReceptorCuenta";
            this.CboReceptorCuenta.NullText = "Selecciona";
            this.CboReceptorCuenta.Size = new System.Drawing.Size(100, 20);
            this.CboReceptorCuenta.TabIndex = 321;
            this.CboReceptorCuenta.TabStop = false;
            // 
            // PanelGeneral
            // 
            this.PanelGeneral.Controls.Add(this.CboReceptor);
            this.PanelGeneral.Controls.Add(this.ChkPagoNomina);
            this.PanelGeneral.Controls.Add(this.TxbRefNumerica);
            this.PanelGeneral.Controls.Add(this.radLabel12);
            this.PanelGeneral.Controls.Add(this.CboComprobantes);
            this.PanelGeneral.Controls.Add(this.ChkMultiPago);
            this.PanelGeneral.Controls.Add(this.RadLabel11);
            this.PanelGeneral.Controls.Add(this.CboReceptorCuenta);
            this.PanelGeneral.Controls.Add(this.RadLabel13);
            this.PanelGeneral.Controls.Add(this.TxbEmisorNumeroCta);
            this.PanelGeneral.Controls.Add(this.RadLabel14);
            this.PanelGeneral.Controls.Add(this.FechaPoliza);
            this.PanelGeneral.Controls.Add(this.RadLabel16);
            this.PanelGeneral.Controls.Add(this.RadLabel15);
            this.PanelGeneral.Controls.Add(this.ChkPorJustificar);
            this.PanelGeneral.Controls.Add(this.TxbEmisorBanco);
            this.PanelGeneral.Controls.Add(this.ChkParaAbono);
            this.PanelGeneral.Controls.Add(this.TxbEmisorSucursal);
            this.PanelGeneral.Controls.Add(this.TxbCargo);
            this.PanelGeneral.Controls.Add(this.TxbEmisorClabe);
            this.PanelGeneral.Controls.Add(this.LabelCargo);
            this.PanelGeneral.Controls.Add(this.CboEmisorCuenta);
            this.PanelGeneral.Controls.Add(this.TxbAbono);
            this.PanelGeneral.Controls.Add(this.radLabel22);
            this.PanelGeneral.Controls.Add(this.LabelAbono);
            this.PanelGeneral.Controls.Add(this.TxbEmisorRfc);
            this.PanelGeneral.Controls.Add(this.TxbNotas);
            this.PanelGeneral.Controls.Add(this.RadLabel2);
            this.PanelGeneral.Controls.Add(this.TxbConcepto);
            this.PanelGeneral.Controls.Add(this.RadLabel7);
            this.PanelGeneral.Controls.Add(this.TxbNumAuto);
            this.PanelGeneral.Controls.Add(this.RadLabel8);
            this.PanelGeneral.Controls.Add(this.TxbReferencia);
            this.PanelGeneral.Controls.Add(this.RadLabel9);
            this.PanelGeneral.Controls.Add(this.RadLabel21);
            this.PanelGeneral.Controls.Add(this.CboFormaDePago);
            this.PanelGeneral.Controls.Add(this.RadLabel18);
            this.PanelGeneral.Controls.Add(this.TxbNumDocto);
            this.PanelGeneral.Controls.Add(this.RadLabel19);
            this.PanelGeneral.Controls.Add(this.FechaDocumento);
            this.PanelGeneral.Controls.Add(this.RadLabel17);
            this.PanelGeneral.Controls.Add(this.FechaDePago);
            this.PanelGeneral.Controls.Add(this.CboReceptorBanco);
            this.PanelGeneral.Controls.Add(this.TxbReceptorClabe);
            this.PanelGeneral.Controls.Add(this.RadLabel1);
            this.PanelGeneral.Controls.Add(this.TxbReceptorSucursal);
            this.PanelGeneral.Controls.Add(this.RadLabel3);
            this.PanelGeneral.Controls.Add(this.RadLabel10);
            this.PanelGeneral.Controls.Add(this.RadLabel6);
            this.PanelGeneral.Controls.Add(this.TxbReceptorRfc);
            this.PanelGeneral.Controls.Add(this.RadLabel5);
            this.PanelGeneral.Controls.Add(this.RadLabel4);
            this.PanelGeneral.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelGeneral.Location = new System.Drawing.Point(0, 30);
            this.PanelGeneral.Name = "PanelGeneral";
            this.PanelGeneral.Size = new System.Drawing.Size(962, 213);
            this.PanelGeneral.TabIndex = 322;
            // 
            // CboReceptor
            // 
            // 
            // CboReceptor.NestedRadGridView
            // 
            this.CboReceptor.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboReceptor.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboReceptor.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboReceptor.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboReceptor.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboReceptor.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboReceptor.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn89.FieldName = "Id";
            gridViewTextBoxColumn89.HeaderText = "Id";
            gridViewTextBoxColumn89.IsVisible = false;
            gridViewTextBoxColumn89.Name = "Id";
            gridViewTextBoxColumn90.FieldName = "Nombre";
            gridViewTextBoxColumn90.HeaderText = "Identidad Fiscal";
            gridViewTextBoxColumn90.Name = "Nombre";
            gridViewTextBoxColumn90.Width = 200;
            gridViewTextBoxColumn91.FieldName = "RFC";
            gridViewTextBoxColumn91.HeaderText = "RFC";
            gridViewTextBoxColumn91.Name = "RFC";
            gridViewTextBoxColumn91.Width = 85;
            gridViewTextBoxColumn92.FieldName = "Clave";
            gridViewTextBoxColumn92.HeaderText = "Clave";
            gridViewTextBoxColumn92.Name = "Clave";
            this.CboReceptor.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn89,
            gridViewTextBoxColumn90,
            gridViewTextBoxColumn91,
            gridViewTextBoxColumn92});
            this.CboReceptor.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboReceptor.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboReceptor.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition16;
            this.CboReceptor.EditorControl.Name = "NestedRadGridView";
            this.CboReceptor.EditorControl.ReadOnly = true;
            this.CboReceptor.EditorControl.ShowGroupPanel = false;
            this.CboReceptor.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboReceptor.EditorControl.TabIndex = 0;
            this.CboReceptor.Location = new System.Drawing.Point(92, 82);
            this.CboReceptor.Name = "CboReceptor";
            this.CboReceptor.NullText = "Beneficiario";
            this.CboReceptor.Size = new System.Drawing.Size(417, 20);
            this.CboReceptor.TabIndex = 346;
            this.CboReceptor.TabStop = false;
            // 
            // ChkPagoNomina
            // 
            this.ChkPagoNomina.Location = new System.Drawing.Point(821, 35);
            this.ChkPagoNomina.Name = "ChkPagoNomina";
            this.ChkPagoNomina.Size = new System.Drawing.Size(88, 18);
            this.ChkPagoNomina.TabIndex = 325;
            this.ChkPagoNomina.Text = "Pago Nómina";
            // 
            // TxbRefNumerica
            // 
            this.TxbRefNumerica.Location = new System.Drawing.Point(328, 132);
            this.TxbRefNumerica.MaxLength = 7;
            this.TxbRefNumerica.Name = "TxbRefNumerica";
            this.TxbRefNumerica.NullText = "Ref. Númerica";
            this.TxbRefNumerica.Size = new System.Drawing.Size(130, 20);
            this.TxbRefNumerica.TabIndex = 324;
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(243, 133);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(79, 18);
            this.radLabel12.TabIndex = 323;
            this.radLabel12.Text = "Ref. Númerica:";
            // 
            // ChkMultiPago
            // 
            this.ChkMultiPago.Location = new System.Drawing.Point(821, 59);
            this.ChkMultiPago.Name = "ChkMultiPago";
            this.ChkMultiPago.Size = new System.Drawing.Size(72, 18);
            this.ChkMultiPago.TabIndex = 322;
            this.ChkMultiPago.Text = "Multipago";
            this.ChkMultiPago.CheckStateChanged += new System.EventHandler(this.ChkMultiPago_CheckStateChanged);
            // 
            // PanelDocumentos
            // 
            this.PanelDocumentos.Controls.Add(this.PageView);
            this.PanelDocumentos.Controls.Add(this.BarraEstado);
            this.PanelDocumentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelDocumentos.Location = new System.Drawing.Point(0, 243);
            this.PanelDocumentos.Name = "PanelDocumentos";
            this.PanelDocumentos.Size = new System.Drawing.Size(962, 327);
            this.PanelDocumentos.TabIndex = 323;
            // 
            // BarraEstado
            // 
            this.BarraEstado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.LabeStatus});
            this.BarraEstado.Location = new System.Drawing.Point(0, 301);
            this.BarraEstado.Name = "BarraEstado";
            this.BarraEstado.Size = new System.Drawing.Size(962, 26);
            this.BarraEstado.SizingGrip = false;
            this.BarraEstado.TabIndex = 261;
            // 
            // LabeStatus
            // 
            this.LabeStatus.Name = "LabeStatus";
            this.BarraEstado.SetSpring(this.LabeStatus, false);
            this.LabeStatus.Text = "...";
            this.LabeStatus.TextWrap = true;
            // 
            // ViewReciboPago
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 570);
            this.Controls.Add(this.PanelDocumentos);
            this.Controls.Add(this.PanelGeneral);
            this.Controls.Add(this.RadCommandBar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewReciboPago";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Recibo";
            this.Load += new System.EventHandler(this.ReciboDePago_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PageView)).EndInit();
            this.PageView.ResumeLayout(false);
            this.PageViewComprobante.ResumeLayout(false);
            this.PageViewComprobante.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobantes.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            this.PageViewDoctosRelacionados.ResumeLayout(false);
            this.PageViewDoctosRelacionados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDoctosRelacionados.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDoctosRelacionados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboComprobantes.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboComprobantes.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboComprobantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorRfc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmisorCuenta.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmisorCuenta.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmisorCuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorClabe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorSucursal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorBanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaDePago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumDocto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaDePago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaDePago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaDePago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbReceptorRfc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptorBanco.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptorBanco.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptorBanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbReceptorClabe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbReceptorSucursal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNotas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbConcepto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumAuto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbReferencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCargo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelCargo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbAbono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelAbono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPorJustificar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkParaAbono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaPoliza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorNumeroCta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptorCuenta.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptorCuenta.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptorCuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelGeneral)).EndInit();
            this.PanelGeneral.ResumeLayout(false);
            this.PanelGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPagoNomina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRefNumerica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkMultiPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelDocumentos)).EndInit();
            this.PanelDocumentos.ResumeLayout(false);
            this.PanelDocumentos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar1;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement1;
        internal Telerik.WinControls.UI.CommandBarStripElement CommandRecibo;
        internal Telerik.WinControls.UI.CommandBarSplitButton ToolBarEmisor;
        internal Telerik.WinControls.UI.CommandBarSeparator Separador;
        internal Telerik.WinControls.UI.CommandBarLabel ToolLabelStatus;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolSplitStatus;
        internal Telerik.WinControls.UI.CommandBarSeparator Separator2;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarNew;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarCopy;
        private Telerik.WinControls.UI.CommandBarButton ToolBarCreate;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarSave;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarRefresh;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarClose;
        internal Telerik.WinControls.UI.RadPageView PageView;
        internal Telerik.WinControls.UI.RadPageViewPage PageViewComprobante;
        internal Telerik.WinControls.UI.RadGridView GridComprobantes;
        private Telerik.WinControls.UI.RadMultiColumnComboBox CboComprobantes;
        internal Telerik.WinControls.UI.RadTextBox TxbEmisorRfc;
        internal Telerik.WinControls.UI.RadLabel radLabel22;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboEmisorCuenta;
        internal Telerik.WinControls.UI.RadTextBox TxbEmisorClabe;
        internal Telerik.WinControls.UI.RadTextBox TxbEmisorSucursal;
        internal Telerik.WinControls.UI.RadTextBox TxbEmisorBanco;
        internal Telerik.WinControls.UI.RadLabel RadLabel15;
        internal Telerik.WinControls.UI.RadLabel RadLabel16;
        internal Telerik.WinControls.UI.RadLabel RadLabel14;
        internal Telerik.WinControls.UI.RadLabel RadLabel13;
        internal Telerik.WinControls.UI.RadLabel RadLabel11;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaDePago;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaDocumento;
        internal Telerik.WinControls.UI.RadTextBox TxbNumDocto;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboFormaDePago;
        internal Telerik.WinControls.UI.RadLabel RadLabel9;
        internal Telerik.WinControls.UI.RadLabel RadLabel8;
        internal Telerik.WinControls.UI.RadLabel RadLabel7;
        internal Telerik.WinControls.UI.RadLabel RadLabel2;
        internal Telerik.WinControls.UI.RadTextBox TxbReceptorRfc;
        internal Telerik.WinControls.UI.RadLabel RadLabel10;
        internal Telerik.WinControls.UI.RadLabel RadLabel1;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboReceptorBanco;
        internal Telerik.WinControls.UI.RadTextBox TxbReceptorClabe;
        internal Telerik.WinControls.UI.RadTextBox TxbReceptorSucursal;
        internal Telerik.WinControls.UI.RadLabel RadLabel3;
        internal Telerik.WinControls.UI.RadLabel RadLabel6;
        internal Telerik.WinControls.UI.RadLabel RadLabel5;
        internal Telerik.WinControls.UI.RadLabel RadLabel4;
        internal Telerik.WinControls.UI.RadTextBox TxbNotas;
        internal Telerik.WinControls.UI.RadTextBox TxbConcepto;
        internal Telerik.WinControls.UI.RadTextBox TxbNumAuto;
        internal Telerik.WinControls.UI.RadTextBox TxbReferencia;
        internal Telerik.WinControls.UI.RadLabel RadLabel21;
        internal Telerik.WinControls.UI.RadLabel RadLabel18;
        internal Telerik.WinControls.UI.RadLabel RadLabel19;
        internal Telerik.WinControls.UI.RadLabel RadLabel17;
        internal Telerik.WinControls.UI.RadMaskedEditBox TxbCargo;
        internal Telerik.WinControls.UI.RadLabel LabelCargo;
        internal Telerik.WinControls.UI.RadMaskedEditBox TxbAbono;
        internal Telerik.WinControls.UI.RadLabel LabelAbono;
        internal Telerik.WinControls.UI.RadCheckBox ChkPorJustificar;
        internal Telerik.WinControls.UI.RadCheckBox ChkParaAbono;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaPoliza;
        private Telerik.WinControls.UI.RadTextBox TxbEmisorNumeroCta;
        private Telerik.WinControls.UI.RadMultiColumnComboBox CboReceptorCuenta;
        private Telerik.WinControls.UI.CommandBarButton ToolBarPrint;
        private Telerik.WinControls.UI.RadPanel PanelGeneral;
        private Telerik.WinControls.UI.RadPanel PanelDocumentos;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarLayout;
        private Telerik.WinControls.UI.RadMenuItem ToolBarBancomerPagosInterbancarios;
        internal Telerik.WinControls.UI.RadCheckBox ChkMultiPago;
        private Telerik.WinControls.UI.RadMenuItem ToolBarBanamexPagosInterbancarios;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBarComprobantes;
        private Telerik.WinControls.UI.CommandBarButton ToolBarComprobantesNuevo;
        private Telerik.WinControls.UI.CommandBarButton ToolBarComprobantesAgregar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarComprobantesQuitar;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarTipoDocumento;
        private Telerik.WinControls.UI.RadMenuItem ToolBarDocumentoRemision;
        private Telerik.WinControls.UI.RadMenuItem ToolBarDocumentoFactura;
        private Telerik.WinControls.UI.CommandBarHostItem ToolBarDocumentos;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadMenuItem ToolBarBanamexPagosInterbancariosClabe;
        internal Telerik.WinControls.UI.RadTextBox TxbRefNumerica;
        internal Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelNoIdent;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadPageViewPage PageViewDoctosRelacionados;
        private Telerik.WinControls.UI.RadGridView GridDoctosRelacionados;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar3;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton ToolBarDoctosButtonAgregar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarDoctosButtonQuitar;
        internal Telerik.WinControls.UI.RadCheckBox ChkPagoNomina;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboReceptor;
        private Telerik.WinControls.UI.CommandBarButton ToolBarDescargar;
        private Telerik.WinControls.UI.RadStatusStrip BarraEstado;
        private Telerik.WinControls.UI.RadLabelElement LabeStatus;
    }
}
