﻿namespace Jaeger.Views.Contable
{
    partial class ViewLayoutContableE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.txbPlantilla = new Telerik.WinControls.UI.RadTextBox();
            this.txbRFC = new Telerik.WinControls.UI.RadTextBox();
            this.txbEjercicio = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.cboPeriodo = new Telerik.WinControls.UI.RadDropDownList();
            this.cboClave = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.txbNumOrden = new Telerik.WinControls.UI.RadTextBox();
            this.txbNumTramite = new Telerik.WinControls.UI.RadTextBox();
            this.cboTipoSolicitud = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.cboTipoEnvio = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.dtpFecha = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.PictureBox2 = new System.Windows.Forms.PictureBox();
            this.ButtonCrear = new Telerik.WinControls.UI.RadButton();
            this.ButtonCerrar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbPlantilla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbEjercicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPeriodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboClave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboClave.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboClave.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbNumOrden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbNumTramite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipoSolicitud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipoSolicitud.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipoSolicitud.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipoEnvio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipoEnvio.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipoEnvio.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCrear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(18, 32);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(45, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Plantilla";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(207, 57);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(47, 18);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "Ejercicio";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(260, 57);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(45, 18);
            this.radLabel3.TabIndex = 2;
            this.radLabel3.Text = "Período";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(362, 57);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(33, 18);
            this.radLabel4.TabIndex = 3;
            this.radLabel4.Text = "Clave";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(18, 82);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(25, 18);
            this.radLabel5.TabIndex = 4;
            this.radLabel5.Text = "RFC";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(18, 107);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(68, 18);
            this.radLabel6.TabIndex = 5;
            this.radLabel6.Text = "Núm. Orden";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(282, 107);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(74, 18);
            this.radLabel7.TabIndex = 6;
            this.radLabel7.Text = "Núm. Tramite";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(18, 133);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(90, 18);
            this.radLabel8.TabIndex = 7;
            this.radLabel8.Text = "Tipo de Solicitud";
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(282, 133);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(74, 18);
            this.radLabel9.TabIndex = 8;
            this.radLabel9.Text = "Tipo de Envío";
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(18, 159);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(109, 18);
            this.radLabel10.TabIndex = 9;
            this.radLabel10.Text = "Fec. de Modificación";
            // 
            // txbPlantilla
            // 
            this.txbPlantilla.Location = new System.Drawing.Point(69, 31);
            this.txbPlantilla.Name = "txbPlantilla";
            this.txbPlantilla.NullText = "Rutal del archivo XLSX";
            this.txbPlantilla.ShowClearButton = true;
            this.txbPlantilla.Size = new System.Drawing.Size(461, 20);
            this.txbPlantilla.TabIndex = 10;
            // 
            // txbRFC
            // 
            this.txbRFC.Location = new System.Drawing.Point(69, 81);
            this.txbRFC.Name = "txbRFC";
            this.txbRFC.NullText = "RFC";
            this.txbRFC.ShowClearButton = true;
            this.txbRFC.Size = new System.Drawing.Size(119, 20);
            this.txbRFC.TabIndex = 11;
            this.txbRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbEjercicio
            // 
            this.txbEjercicio.Location = new System.Drawing.Point(211, 81);
            this.txbEjercicio.Name = "txbEjercicio";
            this.txbEjercicio.NullText = "0000";
            this.txbEjercicio.Size = new System.Drawing.Size(43, 20);
            this.txbEjercicio.TabIndex = 12;
            this.txbEjercicio.TabStop = false;
            this.txbEjercicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cboPeriodo
            // 
            this.cboPeriodo.Location = new System.Drawing.Point(260, 81);
            this.cboPeriodo.Name = "cboPeriodo";
            this.cboPeriodo.NullText = "Período";
            this.cboPeriodo.Size = new System.Drawing.Size(96, 20);
            this.cboPeriodo.TabIndex = 13;
            // 
            // cboClave
            // 
            this.cboClave.AutoSizeDropDownToBestFit = true;
            this.cboClave.DisplayMember = "Descripcion";
            // 
            // cboClave.NestedRadGridView
            // 
            this.cboClave.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cboClave.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboClave.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboClave.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cboClave.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cboClave.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cboClave.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn7.FieldName = "Clave";
            gridViewTextBoxColumn7.HeaderText = "Clave";
            gridViewTextBoxColumn7.Name = "Clave";
            gridViewTextBoxColumn7.Width = 40;
            gridViewTextBoxColumn8.FieldName = "Descripcion";
            gridViewTextBoxColumn8.HeaderText = "Descripción";
            gridViewTextBoxColumn8.Name = "Descripcion";
            gridViewTextBoxColumn8.Width = 120;
            this.cboClave.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.cboClave.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cboClave.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cboClave.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.cboClave.EditorControl.Name = "NestedRadGridView";
            this.cboClave.EditorControl.ReadOnly = true;
            this.cboClave.EditorControl.ShowGroupPanel = false;
            this.cboClave.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cboClave.EditorControl.TabIndex = 0;
            this.cboClave.Location = new System.Drawing.Point(362, 81);
            this.cboClave.Name = "cboClave";
            this.cboClave.NullText = "Clave";
            this.cboClave.Size = new System.Drawing.Size(168, 20);
            this.cboClave.TabIndex = 14;
            this.cboClave.TabStop = false;
            this.cboClave.ValueMember = "Clave";
            // 
            // txbNumOrden
            // 
            this.txbNumOrden.Location = new System.Drawing.Point(114, 106);
            this.txbNumOrden.Name = "txbNumOrden";
            this.txbNumOrden.NullText = "Núm. Orden";
            this.txbNumOrden.ShowClearButton = true;
            this.txbNumOrden.Size = new System.Drawing.Size(162, 20);
            this.txbNumOrden.TabIndex = 15;
            // 
            // txbNumTramite
            // 
            this.txbNumTramite.Location = new System.Drawing.Point(362, 106);
            this.txbNumTramite.Name = "txbNumTramite";
            this.txbNumTramite.NullText = "Núm. Tramite";
            this.txbNumTramite.ShowClearButton = true;
            this.txbNumTramite.Size = new System.Drawing.Size(168, 20);
            this.txbNumTramite.TabIndex = 16;
            // 
            // cboTipoSolicitud
            // 
            this.cboTipoSolicitud.AutoSizeDropDownToBestFit = true;
            this.cboTipoSolicitud.DisplayMember = "Descripcion";
            // 
            // cboTipoSolicitud.NestedRadGridView
            // 
            this.cboTipoSolicitud.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cboTipoSolicitud.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTipoSolicitud.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboTipoSolicitud.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cboTipoSolicitud.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cboTipoSolicitud.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cboTipoSolicitud.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn9.FieldName = "Clave";
            gridViewTextBoxColumn9.HeaderText = "Clave";
            gridViewTextBoxColumn9.Name = "Clave";
            gridViewTextBoxColumn9.Width = 40;
            gridViewTextBoxColumn10.FieldName = "Descripcion";
            gridViewTextBoxColumn10.HeaderText = "Descripción";
            gridViewTextBoxColumn10.Name = "Descripcion";
            gridViewTextBoxColumn10.Width = 120;
            this.cboTipoSolicitud.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.cboTipoSolicitud.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cboTipoSolicitud.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cboTipoSolicitud.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.cboTipoSolicitud.EditorControl.Name = "NestedRadGridView";
            this.cboTipoSolicitud.EditorControl.ReadOnly = true;
            this.cboTipoSolicitud.EditorControl.ShowGroupPanel = false;
            this.cboTipoSolicitud.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cboTipoSolicitud.EditorControl.TabIndex = 0;
            this.cboTipoSolicitud.Location = new System.Drawing.Point(114, 132);
            this.cboTipoSolicitud.Name = "cboTipoSolicitud";
            this.cboTipoSolicitud.NullText = "Tipo de Solicitud";
            this.cboTipoSolicitud.Size = new System.Drawing.Size(162, 20);
            this.cboTipoSolicitud.TabIndex = 17;
            this.cboTipoSolicitud.TabStop = false;
            this.cboTipoSolicitud.ValueMember = "Clave";
            // 
            // cboTipoEnvio
            // 
            this.cboTipoEnvio.AutoSizeDropDownToBestFit = true;
            this.cboTipoEnvio.DisplayMember = "Descripcion";
            // 
            // cboTipoEnvio.NestedRadGridView
            // 
            this.cboTipoEnvio.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cboTipoEnvio.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTipoEnvio.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboTipoEnvio.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cboTipoEnvio.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cboTipoEnvio.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cboTipoEnvio.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn11.FieldName = "Clave";
            gridViewTextBoxColumn11.HeaderText = "Clave";
            gridViewTextBoxColumn11.Name = "Clave";
            gridViewTextBoxColumn11.Width = 40;
            gridViewTextBoxColumn12.FieldName = "Descripcion";
            gridViewTextBoxColumn12.HeaderText = "Descripción";
            gridViewTextBoxColumn12.Name = "Descripcion";
            gridViewTextBoxColumn12.Width = 120;
            this.cboTipoEnvio.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12});
            this.cboTipoEnvio.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cboTipoEnvio.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cboTipoEnvio.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.cboTipoEnvio.EditorControl.Name = "NestedRadGridView";
            this.cboTipoEnvio.EditorControl.ReadOnly = true;
            this.cboTipoEnvio.EditorControl.ShowGroupPanel = false;
            this.cboTipoEnvio.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cboTipoEnvio.EditorControl.TabIndex = 0;
            this.cboTipoEnvio.Location = new System.Drawing.Point(362, 132);
            this.cboTipoEnvio.Name = "cboTipoEnvio";
            this.cboTipoEnvio.NullText = "Tipo de Envío";
            this.cboTipoEnvio.Size = new System.Drawing.Size(168, 20);
            this.cboTipoEnvio.TabIndex = 18;
            this.cboTipoEnvio.TabStop = false;
            this.cboTipoEnvio.ValueMember = "Clave";
            // 
            // dtpFecha
            // 
            this.dtpFecha.Location = new System.Drawing.Point(133, 158);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(223, 20);
            this.dtpFecha.TabIndex = 19;
            this.dtpFecha.TabStop = false;
            this.dtpFecha.Text = "lunes, 4 de noviembre de 2019";
            this.dtpFecha.Value = new System.DateTime(2019, 11, 4, 23, 26, 41, 541);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.txbPlantilla);
            this.radGroupBox1.Controls.Add(this.dtpFecha);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.cboTipoEnvio);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.cboTipoSolicitud);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.txbNumTramite);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.txbNumOrden);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.cboClave);
            this.radGroupBox1.Controls.Add(this.radLabel6);
            this.radGroupBox1.Controls.Add(this.cboPeriodo);
            this.radGroupBox1.Controls.Add(this.radLabel7);
            this.radGroupBox1.Controls.Add(this.txbEjercicio);
            this.radGroupBox1.Controls.Add(this.radLabel8);
            this.radGroupBox1.Controls.Add(this.txbRFC);
            this.radGroupBox1.Controls.Add(this.radLabel9);
            this.radGroupBox1.Controls.Add(this.radLabel10);
            this.radGroupBox1.HeaderText = "Datos del layout";
            this.radGroupBox1.Location = new System.Drawing.Point(12, 86);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(550, 198);
            this.radGroupBox1.TabIndex = 20;
            this.radGroupBox1.Text = "Datos del layout";
            // 
            // PictureBox2
            // 
            this.PictureBox2.BackColor = System.Drawing.Color.White;
            this.PictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.PictureBox2.Location = new System.Drawing.Point(0, 0);
            this.PictureBox2.Name = "PictureBox2";
            this.PictureBox2.Size = new System.Drawing.Size(575, 80);
            this.PictureBox2.TabIndex = 31;
            this.PictureBox2.TabStop = false;
            // 
            // ButtonCrear
            // 
            this.ButtonCrear.Location = new System.Drawing.Point(336, 290);
            this.ButtonCrear.Name = "ButtonCrear";
            this.ButtonCrear.Size = new System.Drawing.Size(110, 24);
            this.ButtonCrear.TabIndex = 32;
            this.ButtonCrear.Text = "Convertir";
            this.ButtonCrear.Click += new System.EventHandler(this.ButtonCrear_Click);
            // 
            // ButtonCerrar
            // 
            this.ButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_cerrar;
            this.ButtonCerrar.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.ButtonCerrar.Location = new System.Drawing.Point(452, 290);
            this.ButtonCerrar.Name = "ButtonCerrar";
            this.ButtonCerrar.Size = new System.Drawing.Size(110, 24);
            this.ButtonCerrar.TabIndex = 33;
            this.ButtonCerrar.Text = "Cerrar";
            this.ButtonCerrar.Click += new System.EventHandler(this.ButtonCerrar_Click);
            // 
            // ViewLayoutContableE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 320);
            this.Controls.Add(this.ButtonCerrar);
            this.Controls.Add(this.ButtonCrear);
            this.Controls.Add(this.PictureBox2);
            this.Controls.Add(this.radGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewLayoutContableE";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Convertir Layout";
            this.Load += new System.EventHandler(this.ViewContableELayout_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbPlantilla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbEjercicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPeriodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboClave.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboClave.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboClave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbNumOrden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbNumTramite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipoSolicitud.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipoSolicitud.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipoSolicitud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipoEnvio.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipoEnvio.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipoEnvio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCrear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadTextBox txbPlantilla;
        private Telerik.WinControls.UI.RadTextBox txbRFC;
        private Telerik.WinControls.UI.RadMaskedEditBox txbEjercicio;
        private Telerik.WinControls.UI.RadDropDownList cboPeriodo;
        private Telerik.WinControls.UI.RadMultiColumnComboBox cboClave;
        private Telerik.WinControls.UI.RadTextBox txbNumOrden;
        private Telerik.WinControls.UI.RadTextBox txbNumTramite;
        private Telerik.WinControls.UI.RadMultiColumnComboBox cboTipoSolicitud;
        private Telerik.WinControls.UI.RadMultiColumnComboBox cboTipoEnvio;
        private Telerik.WinControls.UI.RadDateTimePicker dtpFecha;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        internal System.Windows.Forms.PictureBox PictureBox2;
        private Telerik.WinControls.UI.RadButton ButtonCrear;
        private Telerik.WinControls.UI.RadButton ButtonCerrar;
    }
}
