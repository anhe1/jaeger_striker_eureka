﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Helpers;
using Jaeger.SAT.Entities;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Edita.V2;
using Jaeger.Edita.V2.Almacen.Enums;
using Jaeger.Edita.V2.Contable.Entities;
using Jaeger.Edita.V2.Contable.Enums;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Aplication;
using Jaeger.Catalogos.Repositories;
using Jaeger.Catalogos.Entities;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Forms;

namespace Jaeger.Views.Contable
{
    public partial class ViewReciboCobro : RadForm
    {
        private BackgroundWorker preparar;

        private BackgroundWorker descarga;
        private SqlSugarContable data;
        private SqlSugarDirectorio cliente;
        private ViewModelPrePoliza recibo;
        private FormaPagoCatalogo catalogoFormaPago = new FormaPagoCatalogo();
        private BancosCatalogo catalogoBancos = new BancosCatalogo();

        /// <summary>
        /// constructor
        /// </summary>
        public ViewReciboCobro(ViewModelPrePoliza nuevo)
        {
            InitializeComponent();
            this.recibo = nuevo;
            this.Load += ReciboDeCobro_Load;
            this.ToolBarRecibo.OverflowButton.Visibility = ElementVisibility.Collapsed;
            this.ToolBarComprobantes.OverflowButton.Visibility = ElementVisibility.Collapsed;
        }

        public ViewReciboCobro(int index, string noIdent)
        {
            InitializeComponent();
            this.recibo = new ViewModelPrePoliza { Id = 0, NoIndet = noIdent };
            this.Load += ReciboDeCobro_Load;
            this.ToolBarRecibo.OverflowButton.Visibility = ElementVisibility.Collapsed;
            this.ToolBarComprobantes.OverflowButton.Visibility = ElementVisibility.Collapsed;
        }

        private void ReciboDeCobro_Load(object sender, EventArgs e)
        {
            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += PrepararDoWork;
            this.preparar.RunWorkerCompleted += PrepararRunWorkerCompleted;
            this.preparar.RunWorkerAsync();
        }

        #region procedimientos de los controles

        private void CboReceptor_DropDownOpened(object sender, EventArgs e)
        {
            this.CboReceptor.AutoSizeDropDownToBestFit = true;
            this.CboReceptor.DataSource = this.cliente.GetListBy(Jaeger.Edita.V2.Directorio.Enums.EnumRelationType.Cliente);
        }

        private void CboReceptor_SelectedValueChanged(object sender, EventArgs e)
        {
            GridViewRowInfo temporal = CboReceptor.SelectedItem as GridViewRowInfo;
            if (!(temporal == null))
            {
                ViewModelContribuyenteDomicilio receptor = temporal.DataBoundItem as ViewModelContribuyenteDomicilio;
                if (receptor != null)
                {
                    this.recibo.Receptor.Id = receptor.Id;
                    this.recibo.Receptor.Rfc = receptor.RFC;
                    this.recibo.Receptor.Nombre = receptor.Nombre;
                    this.recibo.Receptor.Clave = receptor.Clave;
                }
            }
        }

        private void CboReceptorCuenta_DropDownOpened(object sender, EventArgs e)
        {
            this.CboReceptorCuenta.AutoSizeDropDownToBestFit = true;
            this.CboReceptorCuenta.DataSource = this.cliente.Bancos.GetList((int)this.recibo.Receptor.Id, false);
        }

        private void CboReceptorCuenta_SelectedValueChanged(object sender, EventArgs e)
        {
            Telerik.WinControls.UI.GridViewRowInfo temporal = this.CboReceptorCuenta.SelectedItem as Telerik.WinControls.UI.GridViewRowInfo;
            if (temporal != null)
            {
                ViewModelCuentaBancaria o = temporal.DataBoundItem as ViewModelCuentaBancaria;
                if (o != null)
                {
                    this.recibo.Receptor.Clabe = o.Clabe;
                    this.recibo.Receptor.Banco = o.InsitucionBancaria;
                    this.recibo.Receptor.Codigo = o.Clave;
                    this.recibo.Receptor.NumCta = o.NumeroDeCuenta;
                    this.recibo.Receptor.Sucursal = o.Sucursal;
                    this.recibo.Receptor.Nombres = o.Nombre;
                    this.recibo.Receptor.PrimerApellido = o.PrimerApellido;
                    this.recibo.Receptor.SegundoApellido = o.SegundoApellido;
                    this.recibo.Receptor.RefAlfaNumerica = o.RefAlfanumerica;
                    this.recibo.Receptor.RefNumerica = o.RefNumerica;
                }
            }
        }

        private void CboCatBancos_SelectedValueChanged(object sender, EventArgs e)
        {
            Telerik.WinControls.UI.GridViewRowInfo temporal = CboReceptorBanco.SelectedItem as Telerik.WinControls.UI.GridViewRowInfo;
            if (!(temporal == null))
            {
                ClaveBanco banco = temporal.DataBoundItem as ClaveBanco;
                if (banco != null)
                {
                    this.recibo.Receptor.Codigo = banco.Clave;
                    this.recibo.Receptor.Banco = banco.Descripcion;
                }
            }
        }
        
        private void CboFormaDePago_SelectedValueChanged(object sender, EventArgs e)
        {
            Telerik.WinControls.UI.GridViewRowInfo temporal = CboFormaDePago.SelectedItem as Telerik.WinControls.UI.GridViewRowInfo;
            if (!(temporal == null))
            {
                ClaveFormaPago forma = temporal.DataBoundItem as ClaveFormaPago;
                if (forma != null)
                {
                    this.recibo.FormaDePago.Clave = forma.Clave;
                    this.recibo.FormaDePago.Descripcion = forma.Descripcion;
                }
            }
        }

        private void CboCtaDestino_SelectedValueChanged(object sender, EventArgs e)
        {
            Telerik.WinControls.UI.GridViewRowInfo temporal = this.CboEmisorCuenta.SelectedItem as Telerik.WinControls.UI.GridViewRowInfo;
            if (temporal != null)
            {
                ViewModelBancoCuenta o = temporal.DataBoundItem as ViewModelBancoCuenta;
                if (o != null)
                {
                    this.recibo.Emisor.Clabe = o.Clabe;
                    this.recibo.Emisor.Codigo = o.Clave;
                    this.recibo.Emisor.Rfc = o.RFC;
                    this.recibo.Emisor.Nombre = o.Beneficiario;
                    this.recibo.Emisor.NumCta = o.NumeroDeCuenta;
                    this.recibo.Emisor.Banco = o.Banco;
                    this.recibo.Emisor.Sucursal = o.Sucursal;
                }
            }
        }

        private void CboComprobantes_DropDownOpened(object sender, EventArgs e)
        {
            if (this.ToolBarTipoDocumento.Text == this.ToolBarDocumentoFactura.Text)
            {
                this.CboComprobantes.AutoSizeDropDownToBestFit = true;
                this.CboComprobantes.AutoSizeDropDownHeight = true;
                this.CboComprobantes.DataSource = this.data.GetComprobantes(EnumCfdiSubType.Emitido, ConfigService.Synapsis.Empresa.RFC, this.TxbReceptorRfc.Text, "PorCobrar", "", "Pagos");
            }
            else if (this.ToolBarTipoDocumento.Text == this.ToolBarDocumentoRemision.Text)
            {
                this.CboComprobantes.AutoSizeDropDownToBestFit = true;
                this.CboComprobantes.AutoSizeDropDownHeight = true;
                //this.CboComprobantes.DataSource = this.data.GetComprobantes((int)this.recibo.Receptor.Id, ConfigService.Synapsis.Empresa.Clave, EnumRemisionFiscalStatus.PorCobrar);
            }
        }

        #endregion

        #region barra de herramientas

        private void ToolSplitButtonStatusClick(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                this.ToolSplitStatus.DefaultItem = button;
                this.ToolSplitStatus.Text = button.Text;
                this.ToolSplitStatus.PerformClick();
                this.ToolBarRefresh.PerformClick();
            }
        }

        private void TipoDocumento_Click(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                this.ToolBarTipoDocumento.DefaultItem = button;
                this.ToolBarTipoDocumento.Text = button.Text;
            }
        }

        private void ToolBarStatusClick(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                this.ToolSplitStatus.DefaultItem = button;
                this.ToolSplitStatus.Text = button.Text;
                this.ToolSplitStatus.Tag = button.Tag;
                this.ToolSplitStatus.PerformClick();

                if (this.recibo.Estado == EnumPrePolizaStatus.Cancelado)
                {
                    if (RadMessageBox.Show(this, "¿Esta seguro de cancelar este recibo? (Esta acción no se puede revertir.)", "Información", MessageBoxButtons.YesNo, Telerik.WinControls.RadMessageIcon.Info) == DialogResult.Yes)
                    {

                    }
                }
            }
        }

        private void ToolBarClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ToolBarComprobantesAgregar_Click(object sender, EventArgs e)
        {
            GridViewRowInfo rowView = this.CboComprobantes.SelectedItem as GridViewRowInfo;
            if (rowView != null)
            {
                ViewModelPrePolizaComprobante c = rowView.DataBoundItem as ViewModelPrePolizaComprobante;
                if (c != null)
                {
                    c.Abono = c.Total - c.Acumulado;
                    if (this.recibo.Buscar(c.UUID) == null)
                    {
                        c.Creo = ConfigService.Piloto.Clave;
                        recibo.Comprobantes.Add(c);
                    }
                    else
                    {
                        Telerik.WinControls.RadMessageBox.Show("Este comprobante fiscal ya se encuentra en la lista.", "Atención", MessageBoxButtons.OK);
                    }
                }
            }
        }

        private void ToolBarComprobantesQuitar_Click(object sender, EventArgs e)
        {
            if (Telerik.WinControls.RadMessageBox.Show("¿Esta seguro de remover el comprobante seleccionado?", "Atención", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (this.GridComprobantes.CurrentRow != null)
                {
                    if (Jaeger.Helpers.DbConvert.ConvertInt32(this.GridComprobantes.CurrentRow.Cells["Id"].Value) <= 0)
                    {
                        this.GridComprobantes.Rows.Remove(this.GridComprobantes.CurrentRow);
                    }
                    else
                    {
                        this.GridComprobantes.CurrentRow.Cells["IsActive"].Value = false;
                    }
                    this.GridComprobantes.Refresh();
                }
            }
        }

        private void ToolBarSave_Click(object sender, EventArgs e)
        {
            if (this.recibo.Receptor.Rfc == null || this.recibo.Receptor.Nombre == null) 
            {
                MessageBox.Show("Necesitas agregar a un receptor del comprobante de pago.");
                return;
            }

            if (this.recibo.FormaDePago.Clave == null || this.recibo.FormaDePago.Descripcion == null)
            {
                MessageBox.Show("Indica una forma de pago válida para esta operación!");
                return;
            }

            if (this.recibo.Receptor.Banco == null || this.recibo.Receptor.Codigo == null)
            {
                MessageBox.Show("Indica el banco de la operación.");
                return;
            }

            if (this.recibo.Concepto == null)
            {
                MessageBox.Show("Indica un concepto de la operación.");
                return;
            }

            if (this.recibo.Emisor.NumCta == null)
            {
                MessageBox.Show("Indica una cuenta destino para esta operación.");
                return;
            }
            
            this.recibo = this.data.Save(this.recibo);
            this.CreateBinding();
        }

        private void ToolBarButtonDescargar_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// crear comprobante de recibo electronico de pago
        /// </summary>
        private void ToolBarCreate_Click(object sender, EventArgs e)
        {
            if (this.recibo.Id > 0)
            {
                //SqlSugarComprobanteFiscal data22 = new SqlSugarComprobanteFiscal(ConfigService.Synapsis.RDS.Edita);
                //Comprobante comprobantePago = data22.CrearReciboElectronicoPago(this.recibo);
                //ViewModelComprobante4Fiscal nuevo = new ViewModelComprobante4Fiscal(comprobantePago) { MdiParent = this.ParentForm };
                //nuevo.Show();
            }
        }

        private void ToolBarPrint_Click(object sender, EventArgs e)
        {
            if (this.recibo.Id > 0)
            {
                ViewReportes reporte = new ViewReportes(this.recibo);
                reporte.Show();
            }
        }

        private void ToolBarRefresh_Click(object sender, EventArgs e)
        {
            // en caso de que sea nuevo
            if (this.recibo == null)
            {
                this.recibo = new ViewModelPrePoliza();
                this.recibo.Tipo = EnumPolizaTipo.Ingreso;
                this.recibo.Creo = ConfigService.Piloto.Clave;
                this.recibo.FechaNuevo = DateTime.Now;
                this.Text = "Recibo: Nuevo";
            }
            else if (this.recibo.NoIndet != "")
            {
                using (Waiting2Form espera = new Waiting2Form(this.Consultar))
                {
                    espera.Text = "Cargando ...";
                    espera.ShowDialog(this);
                }
            }
            this.CreateBinding();
        }

        private void ToolBarCopy_Click(object sender, EventArgs e)
        {

        }

        private void TxbNotas_TextChanged(object sender, EventArgs e)
        {

        }

        #endregion

        #region preparar

        private void PrepararDoWork(object sender, DoWorkEventArgs e)
        {
            foreach (string item in Enum.GetNames(typeof(EnumPrePolizaStatus)))
            {
                RadMenuItem oButtonMonth = new RadMenuItem { Text = item };
                this.ToolSplitStatus.Items.Add(oButtonMonth);
                oButtonMonth.Click += new EventHandler(this.ToolBarStatusClick);
            }

            this.cliente = new SqlSugarDirectorio(ConfigService.Synapsis.RDS.Edita);
            this.data = new SqlSugarContable(ConfigService.Synapsis.RDS.Edita);

            // catalogos
            this.catalogoFormaPago.Load();

            this.catalogoBancos.Load();
        }

        private void PrepararRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // receptor del documento
            this.CboReceptor.DisplayMember = "Nombre";
            this.CboReceptor.ValueMember = "Nombre";
            CompositeFilterDescriptor composite3 = new CompositeFilterDescriptor { LogicalOperator = FilterLogicalOperator.Or };
            composite3.FilterDescriptors.Add(new FilterDescriptor("Nombre", FilterOperator.Contains, ""));
            composite3.FilterDescriptors.Add(new FilterDescriptor("RFC", FilterOperator.Contains, ""));
            this.CboReceptor.EditorControl.FilterDescriptors.Add(new FilterDescriptor("Nombre", FilterOperator.StartsWith, ""));
            this.CboReceptor.AutoFilter = true;
            this.CboReceptor.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.CboReceptor.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.CboReceptor.DropDownOpened += CboReceptor_DropDownOpened;
            this.CboReceptor.SelectedValueChanged += CboReceptor_SelectedValueChanged;

            this.CboReceptorCuenta.DisplayMember = "NumeroDeCuenta";
            this.CboReceptorCuenta.ValueMember = "NumeroDeCuenta";
            this.CboReceptorCuenta.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.CboReceptorCuenta.DropDownOpened += CboReceptorCuenta_DropDownOpened;
            this.CboReceptorCuenta.SelectedValueChanged += CboReceptorCuenta_SelectedValueChanged;


            this.CboFormaDePago.DataSource = this.catalogoFormaPago.Items;
            this.CboFormaDePago.DisplayMember = "Descripcion";
            this.CboFormaDePago.ValueMember = "Clave";
            this.CboFormaDePago.AutoSizeDropDownToBestFit = true;
            this.CboFormaDePago.SelectedValueChanged += CboFormaDePago_SelectedValueChanged;

            // catalogo de bancos

            this.CboReceptorBanco.DisplayMember = "Descripcion";
            this.CboReceptorBanco.ValueMember = "Clave";
            this.CboReceptorBanco.DataSource = this.catalogoBancos.Items;
            this.CboReceptorBanco.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.CboReceptorBanco.AutoSizeDropDownToBestFit = true;
            this.CboReceptorBanco.SelectedValueChanged += CboCatBancos_SelectedValueChanged;

            // comprobantes
            this.CboComprobantes.DisplayMember = "Uuid";
            this.CboComprobantes.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.CboComprobantes.AutoFilter = true;
            this.CboComprobantes.AutoSizeDropDownToBestFit = true;
            this.CboComprobantes.DropDownOpened += CboComprobantes_DropDownOpened;

            this.CboEmisorCuenta.AutoSizeDropDownToBestFit = true;
            this.CboEmisorCuenta.DisplayMember = "Beneficiario";
            this.CboEmisorCuenta.DataSource = this.data.Banco.GetCuentasDeBanco();
            this.CboEmisorCuenta.SelectedValueChanged += CboCtaDestino_SelectedValueChanged;

            this.FechaDePago.NullText = "Selecciona";
            this.FechaDePago.DateTimePickerElement.Calendar.FocusedDate = DateTime.Now;
            this.FechaDePago.SetToNullValue();

            this.CboReceptor.SelectedItem = null;
            this.CboReceptor.SelectedIndex = -1;
            this.CboFormaDePago.SelectedItem = null;
            this.CboReceptorBanco.SelectedItem = null;

            this.CboReceptor.SelectedItem = null;
            this.CboFormaDePago.SelectedItem = null;
            this.CboReceptorBanco.SelectedItem = null;

            this.CboReceptor.SelectedIndex = -1;
            this.CboEmisorCuenta.SelectedIndex = -1;

            // desaparecer algunos elementos
            this.TxbAbono.Visible = false;
            this.LabelAbono.Visible = false;
            this.GridComprobantes.Columns["Cargo"].IsVisible = false;
            this.ToolBarDocumentos.HostedItem = this.CboComprobantes.MultiColumnComboBoxElement;
            this.ToolBarRefresh.PerformClick();

        }

        #endregion

        #region metodos

        private void CreateBinding()
        {
            // datos de la cuenta del emisor
            this.ToolBarEmisor.DataBindings.Clear();
            this.ToolBarEmisor.DataBindings.Add("Text", ConfigService.Synapsis.Empresa, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboEmisorCuenta.DataBindings.Clear();
            this.CboEmisorCuenta.DataBindings.Add("Text", this.recibo.Emisor, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboEmisorCuenta.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.CboEmisorCuenta.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.recibo.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.TxbEmisorSucursal.DataBindings.Clear();
            this.TxbEmisorSucursal.DataBindings.Add("Text", this.recibo.Emisor, "Sucursal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbEmisorClabe.DataBindings.Clear();
            this.TxbEmisorClabe.DataBindings.Add("Text", this.recibo.Emisor, "Clabe", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbEmisorNumeroCta.DataBindings.Clear();
            this.TxbEmisorNumeroCta.DataBindings.Add("Text", this.recibo.Emisor, "NumCta", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbEmisorNumeroCta.DataBindings.Add("Enabled", this.recibo, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbEmisorBanco.DataBindings.Clear();
            this.TxbEmisorBanco.DataBindings.Add("Text", this.recibo.Emisor, "Banco", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbEmisorRfc.DataBindings.Clear();
            this.TxbEmisorRfc.DataBindings.Add("Text", this.recibo.Emisor, "Rfc", true, DataSourceUpdateMode.OnPropertyChanged);

            // datos del receptor
            this.TxbReceptorSucursal.DataBindings.Clear();
            this.TxbReceptorSucursal.DataBindings.Add("Text", this.recibo.Receptor, "Sucursal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbReceptorSucursal.ReadOnly = !(this.recibo.Editable);

            this.TxbReceptorClabe.DataBindings.Clear();
            this.TxbReceptorClabe.DataBindings.Add("Text", this.recibo.Receptor, "Clabe", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbReceptorClabe.ReadOnly = !(this.recibo.Editable);

            this.CboReceptorCuenta.DataBindings.Clear();
            this.CboReceptorCuenta.DataBindings.Add("Text", this.recibo.Receptor, "NumCta", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboReceptorCuenta.EditorControl.ReadOnly = (this.recibo.Id > 0);
            this.CboReceptorCuenta.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.CboReceptorCuenta.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.recibo.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.CboReceptorBanco.DataBindings.Clear();
            this.CboReceptorBanco.DataBindings.Add("SelectedValue", this.recibo.Receptor, "Codigo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboReceptorBanco.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.CboReceptorBanco.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.recibo.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.TxbReceptorRfc.DataBindings.Clear();
            this.TxbReceptorRfc.DataBindings.Add("Text", this.recibo.Receptor, "Rfc", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboReceptor.DataBindings.Clear();
            this.CboReceptor.DataBindings.Add("Text", this.recibo.Receptor, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboReceptor.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.CboReceptor.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.recibo.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            // datos generales
            this.ToolSplitStatus.DataBindings.Clear();
            this.ToolSplitStatus.DataBindings.Add("Text", this.recibo, "EstadoText", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ToolBarLabelNoIdent.DataBindings.Clear();
            this.ToolBarLabelNoIdent.DataBindings.Add("Text", this.recibo, "NoIndet", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbReferencia.DataBindings.Clear();
            this.TxbReferencia.DataBindings.Add("Text", this.recibo, "Referencia", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbReferencia.ReadOnly = !(this.recibo.Editable);

            this.TxbRefNumerica.DataBindings.Clear();
            this.TxbRefNumerica.DataBindings.Add("Text", this.recibo, "ReferenciaNumerica", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbRefNumerica.ReadOnly = !(this.recibo.Editable);

            this.TxbNumAuto.DataBindings.Clear();
            this.TxbNumAuto.DataBindings.Add("Text", this.recibo, "NumAutorizacion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbNumAuto.ReadOnly = !(this.recibo.Editable);

            this.TxbConcepto.DataBindings.Clear();
            this.TxbConcepto.DataBindings.Add("Text", this.recibo, "Concepto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbConcepto.ReadOnly = !(this.recibo.Editable);

            this.TxbNotas.DataBindings.Clear();
            this.TxbNotas.DataBindings.Add("Text", this.recibo, "Notas", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbNotas.ReadOnly = !(this.recibo.Editable);

            this.CboFormaDePago.DataBindings.Clear();
            this.CboFormaDePago.DataBindings.Add("SelectedValue", this.recibo.FormaDePago, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboFormaDePago.Enabled = (this.recibo.Editable);

            this.TxbNumDocto.DataBindings.Clear();
            this.TxbNumDocto.DataBindings.Add("Text", this.recibo, "NumDocto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbNumDocto.ReadOnly = !(this.recibo.Editable);

            this.ChkParaAbono.DataBindings.Clear();
            this.ChkParaAbono.DataBindings.Add("Checked", this.recibo, "ParaAbono", true, DataSourceUpdateMode.OnPropertyChanged);
            this.ChkParaAbono.ReadOnly = !(this.recibo.Editable);

            this.ChkPorJustificar.DataBindings.Clear();
            this.ChkPorJustificar.DataBindings.Add("Checked", this.recibo, "PorJustificar", true, DataSourceUpdateMode.OnPropertyChanged);
            this.ChkPorJustificar.ReadOnly = !(this.recibo.Editable);

            this.FechaDocumento.DataBindings.Clear();
            this.FechaDocumento.DataBindings.Add("Value", this.recibo, "FechaDocto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaDocumento.ReadOnly = !(this.recibo.Editable);
            this.FechaDocumento.DateTimePickerElement.ArrowButton.Visibility = (!(this.recibo.Editable) ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.FechaPoliza.DataBindings.Clear();
            this.FechaPoliza.DataBindings.Add("Value", this.recibo, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaPoliza.ReadOnly = !(this.recibo.Editable);
            this.FechaPoliza.DateTimePickerElement.ArrowButton.Visibility = (!(this.recibo.Editable) ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.FechaDePago.DataBindings.Clear();
            this.FechaDePago.DataBindings.Add("Value", this.recibo, "FechaPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaDePago.ReadOnly = !(this.recibo.Editable);
            this.FechaDePago.DateTimePickerElement.ArrowButton.Visibility = (!(this.recibo.Editable) ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.TxbCargo.DataBindings.Clear();
            this.TxbCargo.DataBindings.Add("Text", this.recibo, "Cargo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbCargo.ReadOnly = !(this.recibo.Editable);

            this.TxbAbono.DataBindings.Clear();
            this.TxbAbono.DataBindings.Add("Text", this.recibo, "Abono", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbAbono.ReadOnly = !(this.recibo.Editable);

            this.GridComprobantes.DataSource = this.recibo.Comprobantes;
            this.GridComprobantes.AllowEditRow = this.recibo.Editable;
            this.ChkMultiPago.ReadOnly = !this.recibo.Editable;

            this.CboComprobantes.Visible = !(this.recibo.Editable);
            this.ToolBarComprobantes.Enabled = (this.recibo.Editable);
            this.ToolBarCreate.Enabled = this.recibo.Id > 0;
            this.ToolBarButtonDescargar.Enabled = this.recibo.Comprobantes.Count > 0;

            if (this.recibo.PorJustificar)
            {
                this.ChkPorJustificar.Enabled = false;
                this.CboComprobantes.Visible = true;
                this.ToolBarComprobantes.Enabled = true;
            }

            if (this.recibo.Id > 0)
            {
                this.Text = string.Concat("Recibo: ", this.recibo.NoIndet);
                this.recibo.Modifica = ConfigService.Piloto.Clave;
            }
            else
            {
                this.Text = "Recibo Nuevo";
            }
        }

        private void FormatosCondicionales()
        {
            // regla para cambiar de color todos los comprobantes con un estado CANCELADO o NO ENCONTRADO
            ExpressionFormattingObject estado = new ExpressionFormattingObject("Estado del Comprobante", "Estado = 'NoEncontrado' OR Estado = 'Cancelado'", true) { RowForeColor = System.Drawing.Color.DarkGray };
            ExpressionFormattingObject vigente = new ExpressionFormattingObject("Vigente", "Estado = 'Vigente'", false) { CellForeColor = System.Drawing.Color.Green };
            this.GridComprobantes.Columns["Estado"].ConditionalFormattingObjectList.Add(estado);
            this.GridComprobantes.Columns["Estado"].ConditionalFormattingObjectList.Add(vigente);
        }

        #endregion

        private void Consultar()
        {
            this.recibo = this.data.GetPrePoliza(this.recibo.NoIndet);
        }

        private void Guardar()
        {
            this.recibo = this.data.Save(this.recibo);
        }

    }
}
