﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Edita.Enums;
using Jaeger.Edita.V2;
using Jaeger.Edita.V2.Contable.Entities;
using Jaeger.Edita.V2.Contable.Enums;
using Jaeger.Entities;
using Jaeger.Helpers;
using Jaeger.Edita.Interfaces;
using Jaeger.Aplication;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views.Contable
{
    public partial class ViewPrePolizas : RadForm
    {
        private EnumPolizaTipo tipo;
        private ISqlContable data;
        private BackgroundWorker preparar;
        private BackgroundWorker consultar;
        private BackgroundWorker aplicar;
        private BindingList<ViewModelPrePoliza> datos;
        private Status statusAutorizado;
        

        public ViewPrePolizas(EnumPolizaTipo t)
        {
            InitializeComponent();
            this.tipo = t;
        }

        private void PrePolizas_Load(object sender, EventArgs e)
        {
            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += Preparar_DoWork;
            this.preparar.RunWorkerCompleted += Preparar_RunWorkerCompleted;
            this.preparar.RunWorkerAsync();
            
            this.consultar = new BackgroundWorker();
            this.consultar.DoWork += ConsultarDoWork;
            this.consultar.RunWorkerCompleted += ConsultarRunWorkerCompleted;
            
            this.aplicar = new BackgroundWorker();
            this.aplicar.DoWork += Aplicar_DoWork;
            this.aplicar.RunWorkerCompleted += Aplicar_RunWorkerCompleted;
        }

        private void ContextMenuCopiar_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(this.GridData.CurrentCell.Value.ToString());
        }

        private void ContextMenuClonar_Click(object sender, EventArgs e)
        {
            if (this.GridData.CurrentRow != null)
            {
                ViewModelPrePoliza c = this.GridData.CurrentRow.DataBoundItem as ViewModelPrePoliza;
                if (c != null)
                {
                    ViewModelPrePoliza clon = this.data.GetPrePoliza(c.NoIndet);
                    if (clon != null)
                    {
                        clon.Clonar();
                        if (tipo == EnumPolizaTipo.Ingreso)
                        {
                            ViewReciboCobro objeto = new ViewReciboCobro(clon) { MdiParent = this.ParentForm, StartPosition = FormStartPosition.CenterParent };
                            objeto.Show();
                        }
                        else if (tipo == EnumPolizaTipo.Egreso)
                        {
                            var objeto = new ViewRecibo2Pago(clon) { MdiParent = this.ParentForm, StartPosition = FormStartPosition.CenterParent };
                            objeto.Show();
                        }
                    }
                }
            }
        }

        private void ContextMenuSustituir_Click(object sender, EventArgs e)
        {
            if (this.GridData.CurrentRow != null)
            {
                ViewModelPrePoliza c = this.GridData.CurrentRow.DataBoundItem as ViewModelPrePoliza;
                if (c != null)
                {
                    ViewModelPrePoliza clon = this.data.GetPrePoliza(c.NoIndet);
                    clon.Creo = ConfigService.Piloto.Clave;
                    if (clon != null)
                    {
                        clon.Sustituir();
                        if (tipo == EnumPolizaTipo.Ingreso)
                        {
                            ViewReciboCobro objeto = new ViewReciboCobro(clon) { MdiParent = this.ParentForm, StartPosition = System.Windows.Forms.FormStartPosition.CenterParent };
                            objeto.Show();
                        }
                        else if (tipo == EnumPolizaTipo.Egreso)
                        {
                            var objeto = new ViewRecibo2Pago(clon) { MdiParent = this.ParentForm, StartPosition = System.Windows.Forms.FormStartPosition.CenterParent };
                            objeto.Show();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// crear comprobante recibo electronico de pago, este metodo tambien esta el los recibos de cobro
        /// </summary>
        private void ContextMenuCrearPago_Click(object sender, EventArgs e)
        {
            //if (this.GridData.CurrentRow != null)
            //{
            //    ViewModelPrePoliza c = this.GridData.CurrentRow.DataBoundItem as ViewModelPrePoliza;
            //    if (c != null)
            //    {
            //        ViewModelPrePoliza recibo = this.data.GetPrePoliza(c.NoIndet);
            //        if (tipo == EnumPolizaTipo.Ingreso)
            //        {
            //            HelperComprobanteFiscal db = new HelperComprobanteFiscal(this.data.Settings);
            //            Jaeger.CFDI.Entities.Comprobante comprobantePago = db.CrearReciboElectronicoPago(recibo);
            //            ViewComprobante4Fiscal nuevo = new ViewComprobante4Fiscal(comprobantePago) { MdiParent = this.ParentForm };
            //            nuevo.Show();
            //        }
            //        else if (tipo == EnumPolizaTipo.Egreso)
            //        {
                    
            //        }
            //    }
            //}
        }

        #region barra de herramientas

        private void ToolSplitButtonMonthClick(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                this.ToolBarSplitPeriodo.DefaultItem = button;
                this.ToolBarSplitPeriodo.Text = button.Text;
                this.ToolBarSplitPeriodo.Tag = button.Tag;
                this.ToolBarSplitPeriodo.PerformClick();
                this.ToolBarRefresh.PerformClick();
            }
        }

        private void ToolSplitButtonYearClick(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                this.ToolBarSplitEjercicio.DefaultItem = button;
                this.ToolBarSplitEjercicio.Text = button.Text;
                this.ToolBarSplitEjercicio.PerformClick();
            }
        }

        private void ToolBarNew_Click(object sender, EventArgs e)
        {
            if (tipo == EnumPolizaTipo.Ingreso)
            {
                ViewReciboCobro nuevoRecibo = new ViewReciboCobro(null)
                {
                    Text = "Recibo de Cobro",
                    StartPosition = FormStartPosition.CenterParent
                };
                nuevoRecibo.ShowDialog();
            }
            else if (tipo == EnumPolizaTipo.Egreso)
            {
                var nuevoRecibo = new ViewRecibo2Pago(null)
                {
                    Text = "Recibo de Pago",
                    StartPosition = FormStartPosition.CenterParent
                };
                nuevoRecibo.ShowDialog();
            }
        }

        private void ToolBarNewReciboMultipago_Click(object sender, EventArgs e)
        {
            //ReciboDePagoT nuevoRecibo = new ReciboDePagoT(null)
            //{
            //    Text = "Recibo de Multipago",
            //    StartPosition = FormStartPosition.CenterParent
            //};
            //nuevoRecibo.ShowDialog();
        }

        private void ToolBarEdit_Click(object sender, EventArgs e)
        {
            if (this.GridData.CurrentRow != null)
            {
                ViewModelPrePoliza c = this.GridData.CurrentRow.DataBoundItem as ViewModelPrePoliza;
                if (c != null)
                {
                    if (tipo == EnumPolizaTipo.Ingreso)
                    {
                        ViewReciboCobro objeto = new ViewReciboCobro(c.Id, c.NoIndet) { MdiParent = this.ParentForm };
                        objeto.Show();
                    }
                    else if (tipo == EnumPolizaTipo.Egreso)
                    {
                        var objeto = new ViewRecibo2Pago(c.Id, c.NoIndet) { MdiParent = this.ParentForm };
                        objeto.Show();
                    }
                }
            }
        }

        private void ToolBarDelete_Click(object sender, EventArgs e)
        {
            if (this.GridData.CurrentRow != null)
            {
                ViewModelPrePoliza c = this.GridData.CurrentRow.DataBoundItem as ViewModelPrePoliza;
                if (c != null)
                {
                    if (RadMessageBox.Show(this,"¿Esta seguro de cancelar este recibo? (Esta acción no se puede revertir.)", "Información", MessageBoxButtons.YesNo, Telerik.WinControls.RadMessageIcon.Info) == DialogResult.Yes)
                    {
                        if (this.data.AplicarStatus(c.NoIndet, c.EstadoText, ConfigService.Piloto.Clave))
                        {
                            this.data.Aplicar(c.NoIndet);
                        }
                    }
                }
            }
        }

        private void ToolBarRefresh_Click(object sender, EventArgs e)
        {
            this.ToolBarRefresh.Enabled = false;
            this.Espera.StartWaiting();
            this.Espera.Visible = true;
            
            this.consultar.RunWorkerAsync();
        }

        private void ToolBarFilter_Click(object sender, EventArgs e)
        {
            this.GridData.ShowFilteringRow = this.ToolBarFilter.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false)
            {
                this.GridData.FilterDescriptors.Clear();
            }
        }

        private void ToolBarAutoSum_Click(object sender, EventArgs e)
        {
            this.GridData.TelerikGridAutoSum(!(this.ToolBarAutoSum.ToggleState == ToggleState.On));
        }

        private void ToolBarExport_Click(object sender, EventArgs e)
        {
            TelerikGridExport exportar = new TelerikGridExport(this.GridData);
            exportar.ShowDialog();
        }

        private void PrintComprobante_Click(object sender, EventArgs e)
        {
            if (this.GridData.CurrentRow != null)
            {
                if (this.GridData.CurrentRow.HierarchyLevel == 0)
                {
                    ViewModelPrePoliza temporal = this.GridData.CurrentRow.DataBoundItem as ViewModelPrePoliza;
                    temporal = this.data.GetPrePoliza(temporal.NoIndet);
                    if (temporal != null)
                    {
                        ViewReportes reporte = new ViewReportes(temporal);
                        reporte.Show();
                    }
                }
            }
        }

        private void PrintLista_Click(object sender, EventArgs e)
        {
            GridPrintStyle style = new GridPrintStyle();
            RadPrintDocument document = new RadPrintDocument();
            // set another properties
            style.FitWidthMode = PrintFitWidthMode.FitPageWidth;
            style.PrintGrouping = true;
            style.PrintSummaries = true;
            style.PrintHeaderOnEachPage = true;

            document.DefaultPageSettings.Margins = new System.Drawing.Printing.Margins(10, 10, 10, 10);
            document.AssociatedObject = this.GridData;
            document.MiddleHeader = "";
            document.MiddleFooter = string.Concat(this.Text, " ", DateTime.Now.ToShortDateString(), " (", ConfigService.Piloto.Clave, ")");

            //'document.RightFooter
            // show print preview dialog
            RadPrintPreviewDialog dialog = new RadPrintPreviewDialog(document);
            dialog.ShowDialog();
        }

        private void ToolBarClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region acciones del grid

        private void GridData_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e)
        {
            if (e.ContextMenuProvider is GridHeaderCellElement)
            {

            }
            else if (e.ContextMenuProvider is GridRowHeaderCellElement)
            {

            }
            else if (e.ContextMenuProvider is GridFilterCellElement)
            {

            }
            else if (e.ContextMenuProvider is GridDataCellElement)
            {
                if (this.GridData.CurrentRow.ViewInfo.ViewTemplate == this.GridData.MasterTemplate)
                {
                    e.ContextMenu = this.MenuContextual.DropDown;
                }
            }
        }

        private void GridData_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (!(e.Row is GridViewFilteringRowInfo))
            {
                
                if (e.Column.Name == "Estado")
                {
                    if (e.Row.Cells["Estado"].Value.ToString() == Enum.GetName(typeof(EnumPrePolizaStatus), EnumPrePolizaStatus.Cancelado))
                    {
                        e.Cancel = true;
                    }
                }
            }

        }

        private void GridData_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            if (!(e.Row is GridViewFilteringRowInfo))
            {
                if (e.Column.Name == "Estado")
                {
                    if ((string)e.Column.Tag == "Actualizar")
                    {
                        e.Column.Tag = "";
                        this.Espera.StartWaiting();
                        this.Espera.Visible = true;
                        this.aplicar.RunWorkerAsync();
                        Application.DoEvents();
                    }
                }
            }
        }

        private void GridData_CellValidating(object sender, CellValidatingEventArgs e)
        {
            if (!(e.Row is GridViewFilteringRowInfo))
            {
                if (e.Column.Name == "Estado")
                {
                    if (e.ActiveEditor != null)
                    {
                        if (e.OldValue == e.Value)
                        {
                            e.Cancel = true;
                        }
                        else
                        {
                            if ((string)e.Value == Enum.GetName(typeof(EnumPrePolizaStatus), EnumPrePolizaStatus.Cancelado))
                            {
                                if (RadMessageBox.Show(this, "¿Esta seguro de cancelar este comprobante?", "Información", MessageBoxButtons.YesNo, Telerik.WinControls.RadMessageIcon.Info) == DialogResult.No)
                                {
                                    e.Cancel = true; 
                                    return;
                                }
                            }
                            else if ((string)e.Value.ToString() == Enum.GetName(typeof(EnumPrePolizaStatus), EnumPrePolizaStatus.Aplicado))
                            {
                                e.Column.Tag = "Actualizar";
                                e.Cancel = false;
                                return;
                            }
                            else if ((string)e.Value == Enum.GetName(typeof(EnumPrePolizaStatus), EnumPrePolizaStatus.NoAplicado))
                            {
                                return;
                            }
                        }
                    }
                }
            }
        }

        private void RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e)
        {
            if (e.Template.Caption == this.GridComprobantes.Caption)
            {
                ViewModelPrePoliza rowView = e.ParentRow.DataBoundItem as ViewModelPrePoliza;

                if (rowView != null)
                {
                    if (rowView.Comprobantes != null)
                    {
                        rowView.Comprobantes = this.data.GetComprobantes(rowView.NoIndet);
                        foreach (ViewModelPrePolizaComprobante item in rowView.Comprobantes)
                        {
                            GridViewRowInfo row = e.Template.Rows.NewRow();
                            row.Cells["TipoComprobante"].Value = item.TipoText;
                            row.Cells["Folio"].Value = item.Folio;
                            row.Cells["Serie"].Value = item.Serie;
                            row.Cells["Uuid"].Value = item.UUID;
                            row.Cells["FechaEmision"].Value = item.FechaEmision;
                            row.Cells["RfcEmisor"].Value = item.EmisorRFC;
                            row.Cells["Emisor"].Value = item.Emisor;
                            row.Cells["RfcReceptor"].Value = item.ReceptorRFC;
                            row.Cells["Receptor"].Value = item.Receptor;
                            row.Cells["Total"].Value = item.Total;
                            row.Cells["Acumulado"].Value = item.Acumulado;
                            row.Cells["Cargo"].Value = item.Cargo;
                            row.Cells["Abono"].Value = item.Abono;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            }
        }

        #endregion

        #region preparar formulario

        private void Preparar_DoWork(object sender, DoWorkEventArgs e)
        {
            foreach (string item in Enum.GetNames(typeof(EnumMonthsOfYear)))
            {
                RadMenuItem oButtonMonth = new RadMenuItem { Text = item };
                this.ToolBarSplitPeriodo.Items.Add(oButtonMonth);
                oButtonMonth.Click += new EventHandler(this.ToolSplitButtonMonthClick);
            }

            for (int anio = 2013; anio <= DateTime.Now.Year; anio = checked(anio + 1))
            {
                RadMenuItem oButtonYear = new RadMenuItem() { Text = anio.ToString() };
                this.ToolBarSplitEjercicio.Items.Add(oButtonYear);
                oButtonYear.Click += new EventHandler(this.ToolSplitButtonYearClick);
            }

            this.ToolBarSplitPeriodo.Text = Enum.GetName(typeof(EnumMonthsOfYear), DateTime.Now.Month);
            this.ToolBarSplitEjercicio.Text = DateTime.Now.Year.ToString();

            this.GridData.TelerikGridCommon();
            this.GridComprobantes.Standard();
            this.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
            this.GridComprobantes.HierarchyDataProvider = new GridViewEventDataProvider(this.GridComprobantes);
            this.GridData.CellBeginEdit += GridData_CellBeginEdit;
            this.GridData.CellEndEdit += GridData_CellEndEdit;
            this.GridData.CellValidating += GridData_CellValidating;
            this.GridData.ContextMenuOpening += GridData_ContextMenuOpening;

            // actualizar por tipo de documento
            if (tipo == EnumPolizaTipo.Ingreso)
            {
                GridViewComboBoxColumn combo = this.GridData.Columns["Estado"] as GridViewComboBoxColumn;
                this.statusAutorizado = new Status(string.Join(",", Enum.GetNames(typeof(EnumPrePolizaStatus))));
                combo.DataSource = this.statusAutorizado.Status1;
                combo.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                combo.DropDownStyle = RadDropDownStyle.DropDown;
                this.ToolBarNewReciboMultipago.Visibility = ElementVisibility.Collapsed;
                this.GridComprobantes.Columns["Emisor"].IsVisible = false;
                this.GridComprobantes.Columns["Receptor"].Width = this.GridComprobantes.Columns["Receptor"].Width + this.GridComprobantes.Columns["Emisor"].Width;
            }
            else if (tipo == EnumPolizaTipo.Egreso)
            {
                GridViewComboBoxColumn combo = this.GridData.Columns["Estado"] as GridViewComboBoxColumn;
                this.statusAutorizado = new Status(string.Join(",", Enum.GetNames(typeof(EnumPrePolizaStatus))));
                combo.DataSource = this.statusAutorizado.Status1;
                this.GridComprobantes.Columns["Receptor"].IsVisible = false;
                this.GridComprobantes.Columns["Emisor"].Width = this.GridComprobantes.Columns["Emisor"].Width + this.GridComprobantes.Columns["Receptor"].Width;
            }
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.GridData.AllowEditRow = true;
            // desaparecer algunos elementos
            this.ToolBarAcciones.Grip.Visibility = ElementVisibility.Collapsed;
            this.ToolBarAcciones.OverflowButton.Visibility = ElementVisibility.Collapsed;
            this.ToolBarRefresh.PerformClick();
        }

        #endregion

        #region consulta de datos

        private void ConsultarDoWork(object sender, DoWorkEventArgs e)
        {
            Application.DoEvents();
            this.data = new SqlSugarContable(ConfigService.Synapsis.RDS.Edita);
            this.datos = this.data.GetPrePolizas(tipo, (EnumMonthsOfYear)(Enum.Parse(typeof(EnumMonthsOfYear), this.ToolBarSplitPeriodo.Text)), int.Parse(this.ToolBarSplitEjercicio.Text));
        }

        private void ConsultarRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Espera.Visible = false;
            this.Espera.StopWaiting();
            this.GridData.DataSource = datos;
            this.ToolBarRefresh.Enabled = true;
        }

        #endregion

        #region aplicar status

        private void Aplicar_DoWork(object sender, DoWorkEventArgs e)
        {
            Application.DoEvents();
            this.data = new SqlSugarContable(ConfigService.Synapsis.RDS.Edita);
            ViewModelPrePoliza c = this.GridData.CurrentRow.DataBoundItem as ViewModelPrePoliza;
            if (c != null)
            {
                if (this.data.AplicarStatus(c.NoIndet, c.EstadoText, ConfigService.Piloto.Clave))
                {
                    this.data.Aplicar(c.NoIndet);
                }
            }
        }

        private void Aplicar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Espera.Visible = false;
            this.Espera.StopWaiting();
        }

        #endregion
    }
}
