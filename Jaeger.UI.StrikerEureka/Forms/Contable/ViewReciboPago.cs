﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.Data;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.Contable.Entities;
using Jaeger.Edita.V2.Contable.Enums;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Edita.V2;
using Jaeger.Helpers;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Catalogos.Repositories;
using Jaeger.Catalogos.Entities;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;

namespace Jaeger.Views.Contable
{
    public partial class ViewReciboPago : RadForm
    {
        private BackgroundWorker preparar;
        private BackgroundWorker procesar;
        private BackgroundWorker descarga;
        private ViewModelPrePoliza recibo;
        private SqlSugarContable data;
        private SqlSugarDirectorio proveedor;
        private FormaPagoCatalogo catalogoFormaPago = new FormaPagoCatalogo();
        private BancosCatalogo catalogoBancos = new BancosCatalogo();

        public ViewReciboPago(ViewModelPrePoliza nuevo)
        {
            InitializeComponent();
            this.recibo = nuevo;
            this.CommandRecibo.OverflowButton.Visibility = ElementVisibility.Collapsed;
            this.ToolBarComprobantes.OverflowButton.Visibility = ElementVisibility.Collapsed;
        }

        public ViewReciboPago(int index, string noIdent)
        {
            InitializeComponent();
            this.recibo = new ViewModelPrePoliza { Id = 0, NoIndet = noIdent };
            this.CommandRecibo.OverflowButton.Visibility = ElementVisibility.Collapsed;
            this.ToolBarComprobantes.OverflowButton.Visibility = ElementVisibility.Collapsed;
        }

        private void ReciboDePago_Load(object sender, EventArgs e)
        {
            if (this.recibo == null)
            {
                this.CommandRecibo.Items.Remove(this.ToolBarCopy);
                this.CommandRecibo.Items.Remove(this.ToolBarCreate);
            }
            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += PrepararDoWork;
            this.preparar.RunWorkerCompleted += PrepararRunWorkerCompleted;
            this.procesar = new BackgroundWorker();
            this.procesar.DoWork += ProcesarDoWork;
            this.procesar.RunWorkerCompleted += ProcesarRunWorkerCompleted;

            this.preparar.RunWorkerAsync();

        }

        #region procedimientos de los controles

        private void CboReceptor_DropDownOpened(object sender, EventArgs e)
        {
            this.CboReceptor.AutoSizeDropDownToBestFit = true;
            if (this.ChkPagoNomina.Checked)
            {
                this.CboReceptor.DataSource = this.proveedor.GetListBy(Jaeger.Edita.V2.Directorio.Enums.EnumRelationType.Empleado);
            }
            else
            {
                this.CboReceptor.DataSource = this.proveedor.GetListBy(Jaeger.Edita.V2.Directorio.Enums.EnumRelationType.Proveedor);
            }
        }

        private void CboReceptor_SelectedValueChanged(object sender, EventArgs e)
        {
            Telerik.WinControls.UI.GridViewRowInfo temporal = CboReceptor.SelectedItem as Telerik.WinControls.UI.GridViewRowInfo;
            if (!(temporal == null))
            {
                ViewModelContribuyenteDomicilio receptor = temporal.DataBoundItem as ViewModelContribuyenteDomicilio;
                if (receptor != null)
                {
                    this.recibo.Receptor.Id = receptor.Id;
                    this.recibo.Receptor.Rfc = receptor.RFC;
                    this.recibo.Receptor.Nombre = receptor.Nombre;
                    this.recibo.Receptor.Clave = receptor.Clave;
                }
            }
        }

        private void CboReceptorCuenta_DropDownOpened(object sender, EventArgs e)
        {
            this.CboReceptorCuenta.AutoSizeDropDownToBestFit = true;
            this.CboReceptorCuenta.DataSource = this.proveedor.Bancos.GetList((int)this.recibo.Receptor.Id, false);
        }

        private void CboReceptorCuenta_SelectedValueChanged(object sender, EventArgs e)
        {
            Telerik.WinControls.UI.GridViewRowInfo temporal = this.CboReceptorCuenta.SelectedItem as Telerik.WinControls.UI.GridViewRowInfo;
            if (temporal != null)
            {
                ViewModelCuentaBancaria o = temporal.DataBoundItem as ViewModelCuentaBancaria;
                if (o != null)
                {
                    this.recibo.Receptor.Clabe = o.Clabe;
                    this.recibo.Receptor.Banco = o.InsitucionBancaria;
                    this.recibo.Receptor.Codigo = o.Clave;
                    this.recibo.Receptor.NumCta = o.NumeroDeCuenta;
                    this.recibo.Receptor.Sucursal = o.Sucursal;
                    this.recibo.Receptor.Nombres = o.Nombre;
                    this.recibo.Receptor.PrimerApellido = o.PrimerApellido;
                    this.recibo.Receptor.SegundoApellido = o.SegundoApellido;
                    this.recibo.Receptor.RefAlfaNumerica = o.RefAlfanumerica;
                    this.recibo.Receptor.RefNumerica = o.RefNumerica;
                }
            }
        }

        private void CboCatBancos_SelectedValueChanged(object sender, EventArgs e)
        {
            Telerik.WinControls.UI.GridViewRowInfo temporal = CboReceptorBanco.SelectedItem as Telerik.WinControls.UI.GridViewRowInfo;
            if (!(temporal == null))
            {
                ClaveBanco banco = temporal.DataBoundItem as ClaveBanco;
                if (banco != null)
                {
                    this.recibo.Receptor.Codigo = banco.Clave;
                    this.recibo.Receptor.Banco = banco.Descripcion;
                }
            }
        }

        private void CboFormaDePago_SelectedValueChanged(object sender, EventArgs e)
        {
            Telerik.WinControls.UI.GridViewRowInfo temporal = CboFormaDePago.SelectedItem as Telerik.WinControls.UI.GridViewRowInfo;
            if (!(temporal == null))
            {
                ClaveFormaPago forma = temporal.DataBoundItem as ClaveFormaPago;
                if (forma != null)
                {
                    this.recibo.FormaDePago.Clave = forma.Clave;
                    this.recibo.FormaDePago.Descripcion = forma.Descripcion;
                }
            }
        }

        private void CboCtaOrigen_SelectedValueChanged(object sender, EventArgs e)
        {
            Telerik.WinControls.UI.GridViewRowInfo temporal = this.CboEmisorCuenta.SelectedItem as Telerik.WinControls.UI.GridViewRowInfo;
            if (temporal != null)
            {
                ViewModelBancoCuenta o = temporal.DataBoundItem as ViewModelBancoCuenta;
                if (o != null)
                {
                    this.recibo.Emisor.Clabe = o.Clabe;
                    this.recibo.Emisor.Codigo = o.Clave;
                    this.recibo.Emisor.Rfc = o.RFC;
                    this.recibo.Emisor.Nombre = o.Beneficiario;
                    this.recibo.Emisor.NumCta = o.NumeroDeCuenta;
                    this.recibo.Emisor.Banco = o.Banco;
                    this.recibo.Emisor.Sucursal = o.Sucursal;
                }
            }
        }

        private void CboComprobantes_DropDownOpened(object sender, EventArgs e)
        {
            if (this.ToolBarTipoDocumento.Text == this.ToolBarDocumentoFactura.Text)
            {
                this.CboComprobantes.AutoSizeDropDownToBestFit = true;
                this.CboComprobantes.AutoSizeDropDownHeight = true;
                if (this.ChkMultiPago.Checked == false & !this.ChkPagoNomina.Checked)
                {
                    this.CboComprobantes.DataSource = this.data.GetComprobantes(EnumCfdiSubType.Recibido, ConfigService.Synapsis.Empresa.RFC, this.TxbReceptorRfc.Text, "PorPagar");
                }
                else if (this.ChkPagoNomina.Checked)
                {
                    this.CboComprobantes.DataSource = this.data.GetComprobantes(EnumCfdiSubType.Emitido, ConfigService.Synapsis.Empresa.RFC, this.TxbReceptorRfc.Text, "Importado");
                }
                else
                {
                    this.CboComprobantes.DataSource = this.data.GetComprobantes(EnumCfdiSubType.Recibido, ConfigService.Synapsis.Empresa.RFC, "%%", "PorPagar");
                }
            }
            else if (this.ToolBarTipoDocumento.Text == this.ToolBarDocumentoRemision.Text)
            {

            }
        }

        private void ToolSplitButtonStatusClick(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                this.ToolSplitStatus.DefaultItem = button;
                this.ToolSplitStatus.Text = button.Text;
                this.ToolSplitStatus.PerformClick();
                this.ToolBarRefresh.PerformClick();
            }
        }

        private void TipoDocumento_Click(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                this.ToolBarTipoDocumento.DefaultItem = button;
                this.ToolBarTipoDocumento.Text = button.Text;
            }
        }

        private void ChkMultiPago_CheckStateChanged(object sender, System.EventArgs e)
        {
            if (this.ChkMultiPago.Checked)
            {
                this.recibo.Receptor = new PrePolizaCuenta();
                this.CboReceptor.Text = "Multipago";
            }
        }

        #endregion

        #region barra de herramientas

        private void ToolBarSave_Click(object sender, EventArgs e)
        {
            if (this.FechaPoliza.Value > DateTime.Now)
            {
                RadMessageBox.Show(this, "La fecha del recibo no puede ser superior a la fecha actual", "Atención");
                this.FechaPoliza.Value = DateTime.Now;
                return;
            }

            if (this.recibo.Emisor.Nombre == null)
            {
                RadMessageBox.Show(this, "Selecciona una cuenta de Origen!", "Atención");
                return;
            }

            if (this.recibo.Receptor.Nombre == null)
            {
                RadMessageBox.Show(this, "Selecciona un beneficiario para el documento!", "Atención");
                return;
            }

            if (this.recibo.Abono <= 0)
            {
                RadMessageBox.Show(this, "Indica el monto pagado.", "Atención");
                return;
            }

            if (this.recibo.FormaDePago.Clave == "02" & Convert.ToInt32(this.recibo.NumDocto) == 0)
            {
                RadMessageBox.Show(this, "Para esta forma de pago es necesario que indiques el número del documento.", "Atención");
                return;
            }

            if (this.recibo.FechaPago == null)
            {
                RadMessageBox.Show(this, "Por favor indica una fecha de pago.", "Atención");
                return;
            }

            if (this.recibo.Concepto == null)
            {
                RadMessageBox.Show("Indica un concepto de la operación.");
                return;
            }

            if (this.recibo.Comprobantes.Count == 0 && this.recibo.PorJustificar == false)
            {
                if (RadMessageBox.Show(this, "No se agregaron comprobantes a este pago, es correcto?", "Atención", MessageBoxButtons.YesNo) == DialogResult.No)
                    return;
                else
                    this.recibo.PorJustificar = true;
            }
            else
            {
                decimal total = this.recibo.Comprobantes.Where((ViewModelPrePolizaComprobante p) => p.IsActive == true).Sum((ViewModelPrePolizaComprobante p) => p.Cargo);
                if (!(this.recibo.Abono == total))
                {
                    RadMessageBox.Show("Los montos no coinciden para este recibo por comprobar!", "Atención");
                    return;
                }
                else
                    this.recibo.PorJustificar = false;
            }

            using (var espera = new Waiting2Form(this.Guardar))
            {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
                this.ToolBarRefresh.PerformClick();
            }
        }

        private void ToolBarRefresh_Click(object sender, EventArgs e)
        {
            // en caso de que sea nuevo
            if (this.recibo == null)
            {
                this.recibo = new ViewModelPrePoliza();
                this.recibo.Tipo = EnumPolizaTipo.Egreso;
                this.recibo.Creo = ConfigService.Piloto.Clave;
                this.CommandRecibo.Items.Remove(this.ToolBarCopy);
                this.CommandRecibo.Items.Remove(this.ToolBarCreate);
            }
            else if (this.recibo.NoIndet != "")
            {
                using (Waiting2Form espera = new Waiting2Form(this.Consultar))
                {
                    espera.Text = "Cargando ...";
                    espera.ShowDialog(this);
                }
            }
            else
            {
                this.CommandRecibo.Items.Remove(this.ToolBarCopy);
                this.CommandRecibo.Items.Remove(this.ToolBarCreate);
            }
            this.CreateBinding();
        }

        private void ToolBarPrint_Click(object sender, EventArgs e)
        {
            if (this.recibo.Id > 0)
            {
                ViewReportes reporte = new ViewReportes(this.recibo);
                reporte.Show();
            }
        }

        private void ToolBarCreate_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "Comprobante (*.xml)|*.xml" };
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {

            }
        }

        private void ToolBarClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ToolBarComprobantesAgregar_Click(object sender, EventArgs e)
        {
            GridViewRowInfo rowView = this.CboComprobantes.SelectedItem as GridViewRowInfo;
            if (rowView != null)
            {
                ViewModelPrePolizaComprobante c = rowView.DataBoundItem as ViewModelPrePolizaComprobante;
                if (c != null)
                {
                    if (c.TipoComprobante == EnumCfdiType.Ingreso | c.TipoComprobanteText.StartsWith("I"))
                    {
                        if (this.recibo.PorJustificar == false)
                            c.Cargo = c.Total - c.Acumulado;
                    }
                    else
                    {
                        c.Cargo = 0;
                    }
                    if (this.recibo.Buscar(c.UUID) == null)
                    {
                        recibo.Comprobantes.Add(c);
                    }
                    else
                    {
                        Telerik.WinControls.RadMessageBox.Show(this, "Este comprobante fiscal ya se encuentra en la lista.", "Atención", MessageBoxButtons.OK);
                    }
                }
                this.GridComprobantes.Refresh();
                this.CboComprobantes.SelectedItem = null;
            }
        }

        private void ToolBarComprobantesQuitar_Click(object sender, EventArgs e)
        {
            if (Telerik.WinControls.RadMessageBox.Show(this, "¿Esta seguro de remover el comprobante seleccionado?", "Atención", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (this.GridComprobantes.CurrentRow != null)
                {
                    if (Jaeger.Helpers.DbConvert.ConvertInt32(this.GridComprobantes.CurrentRow.Cells["Id"].Value) <= 0)
                    {
                        this.GridComprobantes.Rows.Remove(this.GridComprobantes.CurrentRow);
                    }
                    else
                    {
                        this.GridComprobantes.CurrentRow.Cells["IsActive"].Value = false;
                    }
                    this.GridComprobantes.Refresh();
                }
            }
        }

        private void ToolBarBancomerPagosInterbancarios_Click(object sender, EventArgs e)
        {
            //Jaeger.Contable.Entities.PrePolizaMultipago otro = new PrePolizaMultipago();
            //otro.Emisor = this.recibo.Emisor;
            //PrePolizaCuentaMultipago primero = new PrePolizaCuentaMultipago();
            //primero.Banco = "BANAMEX";
            //primero.Nombres = "Comida";
            //PrePolizaComprobante segundo = new PrePolizaComprobante();
            //segundo.Folio = "1";
            //segundo.Serie = "FAC";
            //primero.Comprobantes.Add(segundo);
            //otro.Receptor.Add(primero);
            //Jaeger.Main.Contable.ReciboDePagoT r = new ReciboDePagoT(otro) { MdiParent = this.ParentForm };
            //r.Show();
        }

        private void ToolBarBanamexPagosInterbancarios_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog() { Filter = "Text (*.txt)|*.txt", FileName = string.Concat(this.recibo.NoIndet, ".txt") };
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Jaeger.Entities.Layout.BanamexLayoutC d = new Entities.Layout.BanamexLayoutC();
                Jaeger.Util.Helpers.HelperFiles.WriteFileText(saveFileDialog.FileName, d.Create(this.recibo, false));
            }
        }

        private void ToolBarBanamexPagosInterbancariosClabe_Click(object sender, EventArgs e)
        {
            //SaveFileDialog saveFileDialog = new SaveFileDialog() { Filter = "Text (*.txt)|*.txt", FileName = string.Concat(this.recibo.NoIndet, ".txt") };
            //if (saveFileDialog.ShowDialog() == DialogResult.OK)
            //{
            //    Jaeger.Edita.V2.Old.HelperBancoLayout d = new Jaeger.Edita.V2.Old.HelperBancoLayout(ConfigService.Synapsis.RDS.Edita);
            //    Jaeger.Util.Helpers.HelperFiles.WriteFileText(saveFileDialog.FileName, d.Create(this.recibo, true));
            //}
        }

        #endregion

        #region preparar

        private void PrepararDoWork(object sender, DoWorkEventArgs e)
        {
            // datos
            this.proveedor = new SqlSugarDirectorio(ConfigService.Synapsis.RDS.Edita);
            this.data = new SqlSugarContable(ConfigService.Synapsis.RDS.Edita);

            // status
            foreach (string item in Enum.GetNames(typeof(EnumPrePolizaStatus)))
            {
                RadMenuItem oButtonStatus = new RadMenuItem { Text = item };
                this.ToolSplitStatus.Items.Add(oButtonStatus);
                oButtonStatus.Click += ToolSplitButtonStatusClick;
            }

            // cargar catalogos
            this.catalogoFormaPago = new FormaPagoCatalogo();
            this.catalogoFormaPago.Load();

            this.catalogoBancos.Load();
        }

        private void PrepararRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // receptor del documento
            this.CboReceptor.DisplayMember = "Nombre";
            this.CboReceptor.ValueMember = "Nombre";
            CompositeFilterDescriptor composite3 = new CompositeFilterDescriptor { LogicalOperator = FilterLogicalOperator.Or };
            composite3.FilterDescriptors.Add(new FilterDescriptor("Nombre", FilterOperator.Contains, ""));
            composite3.FilterDescriptors.Add(new FilterDescriptor("RFC", FilterOperator.Contains, ""));
            this.CboReceptor.EditorControl.FilterDescriptors.Add(new FilterDescriptor("Nombre", FilterOperator.Contains, ""));
            this.CboReceptor.AutoFilter = true;
            this.CboReceptor.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.CboReceptor.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.CboReceptor.DropDownOpened += CboReceptor_DropDownOpened;
            this.CboReceptor.SelectedValueChanged += CboReceptor_SelectedValueChanged;

            this.CboFormaDePago.DataSource = this.catalogoFormaPago.Items;
            this.CboFormaDePago.DisplayMember = "Descripcion";
            this.CboFormaDePago.ValueMember = "Clave";
            this.CboFormaDePago.AutoSizeDropDownToBestFit = true;
            this.CboFormaDePago.SelectedValueChanged += CboFormaDePago_SelectedValueChanged;

            // catalogo de bancos

            this.CboReceptorBanco.DisplayMember = "Descripcion";
            this.CboReceptorBanco.ValueMember = "Clave";
            this.CboReceptorBanco.DataSource = this.catalogoBancos.Items;
            this.CboReceptorBanco.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.CboReceptorBanco.AutoSizeDropDownToBestFit = true;
            this.CboReceptorBanco.SelectedValueChanged += CboCatBancos_SelectedValueChanged;

            this.CboReceptorCuenta.DisplayMember = "NumeroDeCuenta";
            this.CboReceptorCuenta.ValueMember = "NumeroDeCuenta";
            this.CboReceptorCuenta.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.CboReceptorCuenta.DropDownOpened += CboReceptorCuenta_DropDownOpened;
            this.CboReceptorCuenta.SelectedValueChanged += CboReceptorCuenta_SelectedValueChanged;

            // comprobantes
            this.CboComprobantes.DisplayMember = "Uuid";
            this.CboComprobantes.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.CboComprobantes.AutoFilter = true;
            this.CboComprobantes.DropDownOpened += CboComprobantes_DropDownOpened;

            this.FechaDePago.NullText = "Selecciona";
            this.FechaDePago.DateTimePickerElement.Calendar.FocusedDate = DateTime.Now;
            this.FechaDePago.SetToNullValue();

            this.CboEmisorCuenta.AutoSizeDropDownToBestFit = true;
            this.CboEmisorCuenta.DisplayMember = "Beneficiario";
            this.CboEmisorCuenta.DataSource = this.data.Banco.GetCuentasDeBanco();
            this.CboEmisorCuenta.SelectedValueChanged += CboCtaOrigen_SelectedValueChanged;

            this.CboReceptor.SelectedIndex = -1;
            this.CboEmisorCuenta.SelectedIndex = -1;
            this.CboReceptor.SelectedItem = null;
            this.CboFormaDePago.SelectedItem = null;
            this.CboReceptorBanco.SelectedItem = null;

            // desaparecer algunos elementos
            this.CommandRecibo.Grip.Visibility = ElementVisibility.Collapsed;
            this.LabelCargo.Visible = false;
            this.TxbCargo.Visible = false;
            this.GridComprobantes.Columns["Abono"].IsVisible = false;
            this.ToolBarDocumentos.HostedItem = this.CboComprobantes.MultiColumnComboBoxElement;

            this.GridDoctosRelacionados.TelerikGridCommon();
            // formatos condicionales
            this.FormatosCondicionales();
            // refrescar la vista
            this.ToolBarRefresh.PerformClick();
        }

        private void CboReceptor_DropDownOpening(object sender, CancelEventArgs args)
        {
            args.Cancel = !this.recibo.Editable;
        }

        private void EditorControl_CurrentRowChanging(object sender, CurrentRowChangingEventArgs e)
        {
        }

        #endregion

        #region procesar

        private void ProcesarDoWork(object sender, DoWorkEventArgs e)
        {
            this.recibo = this.data.Save(this.recibo);
        }

        private void ProcesarRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.ToolBarRefresh.PerformClick();
        }
        #endregion

        private void CreateBinding()
        {
            // datos de la cuenta del emisor
            this.ToolBarEmisor.DataBindings.Clear();
            this.ToolBarEmisor.DataBindings.Add("Text", ConfigService.Synapsis.Empresa, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboEmisorCuenta.DataBindings.Clear();
            this.CboEmisorCuenta.DataBindings.Add("Text", this.recibo.Emisor, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboEmisorCuenta.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.CboEmisorCuenta.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.recibo.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.TxbEmisorSucursal.DataBindings.Clear();
            this.TxbEmisorSucursal.DataBindings.Add("Text", this.recibo.Emisor, "Sucursal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbEmisorClabe.DataBindings.Clear();
            this.TxbEmisorClabe.DataBindings.Add("Text", this.recibo.Emisor, "Clabe", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbEmisorNumeroCta.DataBindings.Clear();
            this.TxbEmisorNumeroCta.DataBindings.Add("Text", this.recibo.Emisor, "NumCta", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbEmisorNumeroCta.DataBindings.Add("Enabled", this.recibo, "Editable", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbEmisorBanco.DataBindings.Clear();
            this.TxbEmisorBanco.DataBindings.Add("Text", this.recibo.Emisor, "Banco", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbEmisorRfc.DataBindings.Clear();
            this.TxbEmisorRfc.DataBindings.Add("Text", this.recibo.Emisor, "Rfc", true, DataSourceUpdateMode.OnPropertyChanged);

            // datos del receptor
            this.TxbReceptorSucursal.DataBindings.Clear();
            this.TxbReceptorSucursal.DataBindings.Add("Text", this.recibo.Receptor, "Sucursal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbReceptorSucursal.ReadOnly = !(this.recibo.Editable);

            this.TxbReceptorClabe.DataBindings.Clear();
            this.TxbReceptorClabe.DataBindings.Add("Text", this.recibo.Receptor, "Clabe", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbReceptorClabe.ReadOnly = !(this.recibo.Editable);

            this.CboReceptorCuenta.DataBindings.Clear();
            this.CboReceptorCuenta.DataBindings.Add("Text", this.recibo.Receptor, "NumCta", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboReceptorCuenta.EditorControl.ReadOnly = (this.recibo.Id > 0);
            this.CboReceptorCuenta.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.CboReceptorCuenta.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.recibo.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.CboReceptorBanco.DataBindings.Clear();
            this.CboReceptorBanco.DataBindings.Add("SelectedValue", this.recibo.Receptor, "Codigo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboReceptorBanco.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.CboReceptorBanco.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.recibo.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.TxbReceptorRfc.DataBindings.Clear();
            this.TxbReceptorRfc.DataBindings.Add("Text", this.recibo.Receptor, "Rfc", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboReceptor.DataBindings.Clear();
            this.CboReceptor.DataBindings.Add("Text", this.recibo.Receptor, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);
            //this.CboReceptor.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.CboReceptor.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.recibo.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);


            // datos generales
            this.ToolSplitStatus.DataBindings.Clear();
            this.ToolSplitStatus.DataBindings.Add("Text", this.recibo, "EstadoText", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ToolBarLabelNoIdent.DataBindings.Clear();
            this.ToolBarLabelNoIdent.DataBindings.Add("Text", this.recibo, "NoIndet", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbReferencia.DataBindings.Clear();
            this.TxbReferencia.DataBindings.Add("Text", this.recibo, "Referencia", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbReferencia.ReadOnly = !(this.recibo.Editable);

            this.TxbRefNumerica.DataBindings.Clear();
            this.TxbRefNumerica.DataBindings.Add("Text", this.recibo, "ReferenciaNumerica", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbRefNumerica.ReadOnly = !(this.recibo.Editable);

            this.TxbNumAuto.DataBindings.Clear();
            this.TxbNumAuto.DataBindings.Add("Text", this.recibo, "NumAutorizacion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbNumAuto.ReadOnly = !(this.recibo.Editable);

            this.TxbConcepto.DataBindings.Clear();
            this.TxbConcepto.DataBindings.Add("Text", this.recibo, "Concepto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbConcepto.ReadOnly = !(this.recibo.Editable);

            this.TxbNotas.DataBindings.Clear();
            this.TxbNotas.DataBindings.Add("Text", this.recibo, "Notas", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbNotas.ReadOnly = !(this.recibo.Editable);

            this.CboFormaDePago.DataBindings.Clear();
            this.CboFormaDePago.DataBindings.Add("SelectedValue", this.recibo.FormaDePago, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboFormaDePago.Enabled = (this.recibo.Editable);

            this.TxbNumDocto.DataBindings.Clear();
            this.TxbNumDocto.DataBindings.Add("Text", this.recibo, "NumDocto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbNumDocto.ReadOnly = !(this.recibo.Editable);

            this.ChkParaAbono.DataBindings.Clear();
            this.ChkParaAbono.DataBindings.Add("Checked", this.recibo, "ParaAbono", true, DataSourceUpdateMode.OnPropertyChanged);
            this.ChkParaAbono.ReadOnly = !(this.recibo.Editable);

            this.ChkPorJustificar.DataBindings.Clear();
            this.ChkPorJustificar.DataBindings.Add("Checked", this.recibo, "PorJustificar", true, DataSourceUpdateMode.OnPropertyChanged);
            this.ChkPorJustificar.ReadOnly = !(this.recibo.Editable);

            this.FechaDocumento.DataBindings.Clear();
            this.FechaDocumento.DataBindings.Add("Value", this.recibo, "FechaDocto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaDocumento.ReadOnly = !(this.recibo.Editable);
            this.FechaDocumento.DateTimePickerElement.ArrowButton.Visibility = (!(this.recibo.Editable) ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.FechaPoliza.DataBindings.Clear();
            this.FechaPoliza.DataBindings.Add("Value", this.recibo, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaPoliza.ReadOnly = !(this.recibo.Editable);
            this.FechaPoliza.DateTimePickerElement.ArrowButton.Visibility = (!(this.recibo.Editable) ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.FechaDePago.DataBindings.Clear();
            this.FechaDePago.DataBindings.Add("Value", this.recibo, "FechaPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaDePago.ReadOnly = !(this.recibo.Editable);
            this.FechaDePago.DateTimePickerElement.ArrowButton.Visibility = (!(this.recibo.Editable) ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.TxbCargo.DataBindings.Clear();
            this.TxbCargo.DataBindings.Add("Text", this.recibo, "Cargo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbCargo.ReadOnly = !(this.recibo.Editable);

            this.TxbAbono.DataBindings.Clear();
            this.TxbAbono.DataBindings.Add("Text", this.recibo, "Abono", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbAbono.ReadOnly = !(this.recibo.Editable);

            this.GridComprobantes.DataSource = this.recibo.Comprobantes;
            this.GridComprobantes.AllowEditRow = this.recibo.Editable;
            this.GridDoctosRelacionados.DataSource = this.recibo.Doctos;

            this.ChkMultiPago.ReadOnly = !this.recibo.Editable;

            //this.CboComprobantes.Visible = !(this.recibo.Editable);
            //this.ToolBarComprobantes.Enabled = !(this.recibo.PorJustificar);

            this.ToolBarLayout.Visibility = (this.recibo.Id > 0 ? ElementVisibility.Visible : ElementVisibility.Collapsed);
            this.ToolBarLayout.VisibleInOverflowMenu = false;

            if (this.recibo.PorJustificar)
            {
                this.ChkPorJustificar.Enabled = false;
                this.CboComprobantes.Visible = true;
                this.ToolBarComprobantes.Enabled = true;
                this.GridComprobantes.AllowEditRow = true;
            }
            else if (this.recibo.Id > 0)
            {
                this.ToolBarComprobantes.Enabled = false;
                this.CboComprobantes.Visible = false;
                this.GridComprobantes.AllowEditRow = false;
            }

            if (this.recibo.Id > 0)
            {
                this.Text = string.Concat("Recibo: ", this.recibo.NoIndet);
            }
            else
            {
                this.Text = "Recibo Nuevo";
            }
            this.Inabilita();
        }

        private void FormatosCondicionales()
        {
            // regla para cambiar de color todos los comprobantes con un estado CANCELADO o NO ENCONTRADO
            ExpressionFormattingObject estado = new ExpressionFormattingObject("Estado del Comprobante", "Estado = 'NoEncontrado' OR Estado = 'Cancelado'", true) { RowForeColor = System.Drawing.Color.DarkGray };
            ExpressionFormattingObject vigente = new ExpressionFormattingObject("Vigente", "Estado = 'Vigente'", false) { CellForeColor = System.Drawing.Color.Green };
            this.GridComprobantes.Columns["Estado"].ConditionalFormattingObjectList.Add(estado);
            this.GridComprobantes.Columns["Estado"].ConditionalFormattingObjectList.Add(vigente);
        }

        private void ToolBarDoctosButtonAgregar_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "*.*|*.*", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
            if (openFileDialog.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            System.IO.FileInfo n = new System.IO.FileInfo(openFileDialog.FileName);
            if (n.Exists)
            {
                Jaeger.Edita.Helpers.FileContentTypes fContent = new Jaeger.Edita.Helpers.FileContentTypes();

                PrePolizaDoctosRelacionados n1 = new PrePolizaDoctosRelacionados();
                n1.Titulo = n.Name;
                n1.Content = fContent.FileContentType(n.FullName);
                if (this.recibo.Doctos == null)
                {
                    this.recibo.Doctos = new BindingList<PrePolizaDoctosRelacionados>();
                    this.GridDoctosRelacionados.DataSource = this.recibo.Doctos;
                }
                this.recibo.Doctos.Add(n1);
            }
        }

        private void ToolBarDoctosButtonQuitar_Click(object sender, EventArgs e)
        {

        }

        private void Inabilita()
        {
            this.ChkMultiPago.Enabled = !(this.recibo.Id > 0);
            this.ChkParaAbono.Enabled = !(this.recibo.Id > 0);
            this.ChkPorJustificar.Enabled = !(this.recibo.Id > 0);
            this.ChkPagoNomina.Enabled = !(this.recibo.Id > 0);

        }

        private void Consultar()
        {
            this.recibo = this.data.GetPrePoliza(this.recibo.NoIndet);
        }

        private void Guardar()
        {
            this.recibo = this.data.Save(this.recibo);
        }

        private void ToolBarDescargar_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folder = new FolderBrowserDialog();
            if (folder.ShowDialog() != DialogResult.OK)
                return;
            this.Tag = folder.SelectedPath;

            using (var espera = new Waiting2Form(this.Descargar))
            {
                espera.Text = "Descargando ...";
                espera.ShowDialog(this);
            }

            this.LabeStatus.Text = "...";
        }

        private void Descargar()
        {
            SqlSugarComprobanteFiscal comprobanteFiscal = new SqlSugarComprobanteFiscal(ConfigService.Synapsis.RDS.Edita);
            BindingList<ViewModelComprobanteSingle> singles = comprobanteFiscal.GetListIn(this.recibo.Comprobantes.Select(it => it.UUID).ToArray());

            if (singles != null)
            {
                var descargas = new BindingList<Util.Helpers.DescargaElemento>();
                foreach (var item in singles)
                {
                    var xml = new Util.Helpers.DescargaElemento { KeyName = System.IO.Path.GetFileName(item.UrlXml), URL = item.UrlXml };
                    descargas.Add(xml);
                    var pdf = new Util.Helpers.DescargaElemento { KeyName = System.IO.Path.GetFileName(item.UrlPdf), URL = item.UrlPdf };
                    descargas.Add(pdf);
                }

                Util.Helpers.DownloadExtended descarga = new Util.Helpers.DownloadExtended();
                descarga.ProcessDownload += Descarga_ProcessDownload;
                descarga.RunDownloadParallelSync(descargas, (string)this.Tag);
            }
        }

        private void Descarga_ProcessDownload(object sender, Util.Helpers.DownloadChanged e)
        {
            this.LabeStatus.Text = e.Caption;
        }

    }
}
