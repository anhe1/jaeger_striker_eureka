﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Helpers;

namespace Jaeger.Views.Contable
{
    public partial class ViewLayoutContableE : Telerik.WinControls.UI.RadForm
    {
        private HelperConvertidorContableE convertidor;

        public ViewLayoutContableE()
        {
            InitializeComponent();
        }

        private void ViewContableELayout_Load(object sender, EventArgs e)
        {
            this.convertidor = new HelperConvertidorContableE();
            this.cboClave.DataSource = HelperConvertidorContableE.Claves();
            this.cboTipoEnvio.DataSource = HelperConvertidorContableE.TiposDeEnvio();
            this.cboTipoSolicitud.DataSource = HelperConvertidorContableE.TiposDeSolicitud();
        }

        private void ButtonCrear_Click(object sender, EventArgs e)
        {

        }

        private void ButtonCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
