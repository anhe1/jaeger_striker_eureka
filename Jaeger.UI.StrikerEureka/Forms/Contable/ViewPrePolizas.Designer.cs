﻿namespace Jaeger.Views.Contable
{
    partial class ViewPrePolizas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewPrePolizas));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject1 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn38 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn39 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn40 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn41 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn42 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn43 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn44 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn45 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.PrintComprobante = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarAttach = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.AttachAdd = new Telerik.WinControls.UI.RadMenuItem();
            this.AttachDownload = new Telerik.WinControls.UI.RadMenuItem();
            this.AttachDelete = new Telerik.WinControls.UI.RadMenuItem();
            this.CommandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.CommandBarRow = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarAcciones = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabelEjercicio = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarSplitEjercicio = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarLabelPeriodo = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarSplitPeriodo = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.CommandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarNew = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarReciboNuevo = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarNewReciboMultipago = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarEdit = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarRefresh = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarFilter = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarAutoSum = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarExport = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarPrint = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.PrintLista = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarClose = new Telerik.WinControls.UI.CommandBarButton();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsSpinnerWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement();
            this.GridComprobantes = new Telerik.WinControls.UI.GridViewTemplate();
            this.Imagenes = new System.Windows.Forms.ImageList(this.components);
            this.MenuContextual = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.ContextMenuCopiar = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuClonar = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuCrearPago = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuSustituir = new Telerik.WinControls.UI.RadMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            this.GridData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // PrintComprobante
            // 
            this.PrintComprobante.Name = "PrintComprobante";
            this.PrintComprobante.Text = "Comprobante";
            this.PrintComprobante.UseCompatibleTextRendering = false;
            this.PrintComprobante.Click += new System.EventHandler(this.PrintComprobante_Click);
            // 
            // ToolBarAttach
            // 
            this.ToolBarAttach.DefaultItem = null;
            this.ToolBarAttach.DisplayName = "CommandBarSplitButton1";
            this.ToolBarAttach.DrawText = true;
            this.ToolBarAttach.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_adjuntar;
            this.ToolBarAttach.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.AttachAdd,
            this.AttachDownload,
            this.AttachDelete});
            this.ToolBarAttach.Name = "ToolBarAttach";
            this.ToolBarAttach.Text = "Adjuntar";
            this.ToolBarAttach.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarAttach.ToolTipText = "Archivos adjuntos";
            // 
            // AttachAdd
            // 
            this.AttachAdd.Name = "AttachAdd";
            this.AttachAdd.Text = "Agregar";
            // 
            // AttachDownload
            // 
            this.AttachDownload.Name = "AttachDownload";
            this.AttachDownload.Text = "Descargar";
            // 
            // AttachDelete
            // 
            this.AttachDelete.Name = "AttachDelete";
            this.AttachDelete.Text = "Eliminar";
            // 
            // CommandBar
            // 
            this.CommandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBar.Location = new System.Drawing.Point(0, 0);
            this.CommandBar.Name = "CommandBar";
            this.CommandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRow});
            this.CommandBar.Size = new System.Drawing.Size(1346, 63);
            this.CommandBar.TabIndex = 15;
            // 
            // CommandBarRow
            // 
            this.CommandBarRow.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRow.Name = "CommandBarRow";
            this.CommandBarRow.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarAcciones});
            this.CommandBarRow.Text = "";
            // 
            // ToolBarAcciones
            // 
            this.ToolBarAcciones.DisplayName = "CommandBarStripElement1";
            this.ToolBarAcciones.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelEjercicio,
            this.ToolBarSplitEjercicio,
            this.ToolBarLabelPeriodo,
            this.ToolBarSplitPeriodo,
            this.CommandBarSeparator1,
            this.ToolBarNew,
            this.ToolBarEdit,
            this.ToolBarDelete,
            this.commandBarSeparator2,
            this.ToolBarRefresh,
            this.ToolBarFilter,
            this.ToolBarAutoSum,
            this.ToolBarExport,
            this.ToolBarPrint,
            this.ToolBarAttach,
            this.ToolBarClose});
            this.ToolBarAcciones.Name = "ToolBarAcciones";
            // 
            // ToolBarLabelEjercicio
            // 
            this.ToolBarLabelEjercicio.DisplayName = "Etiqueta Ejercicio";
            this.ToolBarLabelEjercicio.Name = "ToolBarLabelEjercicio";
            this.ToolBarLabelEjercicio.Text = "Ejercicio:";
            // 
            // ToolBarSplitEjercicio
            // 
            this.ToolBarSplitEjercicio.DefaultItem = null;
            this.ToolBarSplitEjercicio.DisplayName = "commandBarSplitButton1";
            this.ToolBarSplitEjercicio.DrawImage = false;
            this.ToolBarSplitEjercicio.DrawText = true;
            this.ToolBarSplitEjercicio.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarSplitEjercicio.Image")));
            this.ToolBarSplitEjercicio.Name = "ToolBarSplitEjercicio";
            this.ToolBarSplitEjercicio.Text = "Selecciona";
            // 
            // ToolBarLabelPeriodo
            // 
            this.ToolBarLabelPeriodo.DisplayName = "Periodo";
            this.ToolBarLabelPeriodo.Name = "ToolBarLabelPeriodo";
            this.ToolBarLabelPeriodo.Text = "Periodo:";
            // 
            // ToolBarSplitPeriodo
            // 
            this.ToolBarSplitPeriodo.DefaultItem = null;
            this.ToolBarSplitPeriodo.DisplayName = "commandBarSplitButton2";
            this.ToolBarSplitPeriodo.DrawImage = false;
            this.ToolBarSplitPeriodo.DrawText = true;
            this.ToolBarSplitPeriodo.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarSplitPeriodo.Image")));
            this.ToolBarSplitPeriodo.MinSize = new System.Drawing.Size(90, 34);
            this.ToolBarSplitPeriodo.Name = "ToolBarSplitPeriodo";
            this.ToolBarSplitPeriodo.Text = "Selecciona";
            // 
            // CommandBarSeparator1
            // 
            this.CommandBarSeparator1.DisplayName = "Separador 1";
            this.CommandBarSeparator1.Name = "CommandBarSeparator1";
            this.CommandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarNew
            // 
            this.ToolBarNew.DefaultItem = null;
            this.ToolBarNew.DisplayName = "commandBarSplitButton1";
            this.ToolBarNew.DrawText = true;
            this.ToolBarNew.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_agregar_archivo;
            this.ToolBarNew.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarReciboNuevo,
            this.ToolBarNewReciboMultipago});
            this.ToolBarNew.Name = "ToolBarNew";
            this.ToolBarNew.Text = "Nuevo";
            this.ToolBarNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarReciboNuevo
            // 
            this.ToolBarReciboNuevo.Name = "ToolBarReciboNuevo";
            this.ToolBarReciboNuevo.Text = "Recibo";
            this.ToolBarReciboNuevo.Click += new System.EventHandler(this.ToolBarNew_Click);
            // 
            // ToolBarNewReciboMultipago
            // 
            this.ToolBarNewReciboMultipago.Name = "ToolBarNewReciboMultipago";
            this.ToolBarNewReciboMultipago.Text = "Recibo Multipago";
            this.ToolBarNewReciboMultipago.Click += new System.EventHandler(this.ToolBarNewReciboMultipago_Click);
            // 
            // ToolBarEdit
            // 
            this.ToolBarEdit.DisplayName = "Editar";
            this.ToolBarEdit.DrawText = true;
            this.ToolBarEdit.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_edit_file;
            this.ToolBarEdit.Name = "ToolBarEdit";
            this.ToolBarEdit.Text = "Editar";
            this.ToolBarEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarEdit.ToolTipText = "Editar el objeto seleccionado";
            this.ToolBarEdit.Click += new System.EventHandler(this.ToolBarEdit_Click);
            // 
            // ToolBarDelete
            // 
            this.ToolBarDelete.DisplayName = "Eliminar";
            this.ToolBarDelete.DrawText = true;
            this.ToolBarDelete.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_delete_file;
            this.ToolBarDelete.Name = "ToolBarDelete";
            this.ToolBarDelete.Text = "Cancelar";
            this.ToolBarDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarDelete.Click += new System.EventHandler(this.ToolBarDelete_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisplayName = "commandBarSeparator2";
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // ToolBarRefresh
            // 
            this.ToolBarRefresh.DisplayName = "Actualizar";
            this.ToolBarRefresh.DrawText = true;
            this.ToolBarRefresh.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_actualizar;
            this.ToolBarRefresh.Name = "ToolBarRefresh";
            this.ToolBarRefresh.Text = "Actualizar";
            this.ToolBarRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarRefresh.Click += new System.EventHandler(this.ToolBarRefresh_Click);
            // 
            // ToolBarFilter
            // 
            this.ToolBarFilter.DisplayName = "Filtrado";
            this.ToolBarFilter.DrawText = true;
            this.ToolBarFilter.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_filtrar;
            this.ToolBarFilter.Name = "ToolBarFilter";
            this.ToolBarFilter.Text = "Filtro";
            this.ToolBarFilter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarFilter.Click += new System.EventHandler(this.ToolBarFilter_Click);
            // 
            // ToolBarAutoSum
            // 
            this.ToolBarAutoSum.DisplayName = "ToolBarAutoSum";
            this.ToolBarAutoSum.DrawText = true;
            this.ToolBarAutoSum.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_auto_flash;
            this.ToolBarAutoSum.Name = "ToolBarAutoSum";
            this.ToolBarAutoSum.Text = "AutoSuma";
            this.ToolBarAutoSum.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarAutoSum.Click += new System.EventHandler(this.ToolBarAutoSum_Click);
            // 
            // ToolBarExport
            // 
            this.ToolBarExport.DisplayName = "Exportar";
            this.ToolBarExport.DrawText = true;
            this.ToolBarExport.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_excel;
            this.ToolBarExport.Name = "ToolBarExport";
            this.ToolBarExport.Text = "Exportar";
            this.ToolBarExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarExport.Click += new System.EventHandler(this.ToolBarExport_Click);
            // 
            // ToolBarPrint
            // 
            this.ToolBarPrint.DefaultItem = null;
            this.ToolBarPrint.DisplayName = "Imprimir";
            this.ToolBarPrint.DrawText = true;
            this.ToolBarPrint.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_imprimir;
            this.ToolBarPrint.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.PrintComprobante,
            this.PrintLista});
            this.ToolBarPrint.Name = "ToolBarPrint";
            this.ToolBarPrint.Text = "Imprimir";
            this.ToolBarPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarPrint.ToolTipText = "Opciones de impresión";
            // 
            // PrintLista
            // 
            this.PrintLista.Name = "PrintLista";
            this.PrintLista.Text = "Listado";
            this.PrintLista.Click += new System.EventHandler(this.PrintLista_Click);
            // 
            // ToolBarClose
            // 
            this.ToolBarClose.DisplayName = "commandBarButton1";
            this.ToolBarClose.DrawText = true;
            this.ToolBarClose.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_cerrar;
            this.ToolBarClose.Name = "ToolBarClose";
            this.ToolBarClose.Text = "Cerrar";
            this.ToolBarClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarClose.ToolTipText = "Cerrar esta pestaña";
            this.ToolBarClose.Click += new System.EventHandler(this.ToolBarClose_Click);
            // 
            // GridData
            // 
            this.GridData.Controls.Add(this.Espera);
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 63);
            // 
            // 
            // 
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Index";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "IsActive";
            gridViewTextBoxColumn2.HeaderText = "Activo";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "IsActive";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.VisibleInColumnChooser = false;
            gridViewTextBoxColumn3.FieldName = "NoIndet";
            gridViewTextBoxColumn3.HeaderText = "NumUnIdenPol";
            gridViewTextBoxColumn3.Name = "NoIdent";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 85;
            gridViewComboBoxColumn1.FieldName = "EstadoText";
            gridViewComboBoxColumn1.HeaderText = "Estado";
            gridViewComboBoxColumn1.Name = "Estado";
            gridViewComboBoxColumn1.Width = 80;
            gridViewTextBoxColumn4.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn4.FieldName = "FechaDocto";
            gridViewTextBoxColumn4.FormatString = "{0:dd-MMM-yy}";
            gridViewTextBoxColumn4.HeaderText = "Fec. Docto.";
            gridViewTextBoxColumn4.Name = "FechaDocto";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 75;
            gridViewTextBoxColumn5.FieldName = "TipoText";
            gridViewTextBoxColumn5.HeaderText = "Tipo";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "Tipo";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn6.FieldName = "EmisorRFC";
            gridViewTextBoxColumn6.HeaderText = "Propio";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "EmisorRfc";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn7.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn7.HeaderText = "RFC";
            gridViewTextBoxColumn7.IsVisible = false;
            gridViewTextBoxColumn7.Name = "ReceptorRfc";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn8.DataType = typeof(int);
            gridViewTextBoxColumn8.FieldName = "Folio";
            gridViewTextBoxColumn8.FormatString = "{0:000#}";
            gridViewTextBoxColumn8.HeaderText = "Folio";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "Folio";
            gridViewTextBoxColumn8.ReadOnly = true;
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn9.FieldName = "ReceptorBeneficiario";
            gridViewTextBoxColumn9.HeaderText = "Beneficiario";
            gridViewTextBoxColumn9.Name = "Beneficiario";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.Width = 250;
            gridViewTextBoxColumn10.FieldName = "Concepto";
            gridViewTextBoxColumn10.HeaderText = "Concepto";
            gridViewTextBoxColumn10.Name = "Concepto";
            gridViewTextBoxColumn10.ReadOnly = true;
            gridViewTextBoxColumn10.Width = 165;
            gridViewTextBoxColumn11.FieldName = "FormaDePagoText";
            gridViewTextBoxColumn11.HeaderText = "Clave";
            gridViewTextBoxColumn11.Name = "FormaPagoClave";
            gridViewTextBoxColumn11.ReadOnly = true;
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn11.Width = 35;
            gridViewTextBoxColumn12.DataType = typeof(int);
            gridViewTextBoxColumn12.FieldName = "NumDocto";
            gridViewTextBoxColumn12.FormatString = "{0:000000#}";
            gridViewTextBoxColumn12.HeaderText = "No. Docto.";
            gridViewTextBoxColumn12.Name = "NumDocto";
            gridViewTextBoxColumn12.ReadOnly = true;
            gridViewTextBoxColumn12.Width = 80;
            gridViewTextBoxColumn13.FieldName = "ReceptorCLABE";
            gridViewTextBoxColumn13.HeaderText = "CLABE";
            gridViewTextBoxColumn13.Name = "ReceptorCLABE";
            gridViewTextBoxColumn13.ReadOnly = true;
            gridViewTextBoxColumn13.Width = 90;
            gridViewTextBoxColumn14.FieldName = "EmisorNumCta";
            gridViewTextBoxColumn14.HeaderText = "No. Cuenta";
            gridViewTextBoxColumn14.Name = "EmisorNumCta";
            gridViewTextBoxColumn14.Width = 75;
            gridViewTextBoxColumn15.FieldName = "ReceptorSucursal";
            gridViewTextBoxColumn15.HeaderText = "Sucursal";
            gridViewTextBoxColumn15.Name = "ReceptorSucursal";
            gridViewTextBoxColumn15.ReadOnly = true;
            gridViewTextBoxColumn15.Width = 80;
            gridViewTextBoxColumn16.FieldName = "Referencia";
            gridViewTextBoxColumn16.HeaderText = "Referencia";
            gridViewTextBoxColumn16.Name = "Referencia";
            gridViewTextBoxColumn16.ReadOnly = true;
            gridViewTextBoxColumn16.Width = 80;
            gridViewTextBoxColumn17.FieldName = "NumAutorizacion";
            gridViewTextBoxColumn17.HeaderText = "Autorización";
            gridViewTextBoxColumn17.Name = "NumAutorizacion";
            gridViewTextBoxColumn17.ReadOnly = true;
            gridViewTextBoxColumn17.Width = 80;
            gridViewTextBoxColumn18.DataType = typeof(decimal);
            gridViewTextBoxColumn18.FieldName = "Abono";
            gridViewTextBoxColumn18.FormatString = "{0:n}";
            gridViewTextBoxColumn18.HeaderText = "Abono";
            gridViewTextBoxColumn18.Name = "Abono";
            gridViewTextBoxColumn18.ReadOnly = true;
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn18.Width = 85;
            gridViewTextBoxColumn19.DataType = typeof(decimal);
            gridViewTextBoxColumn19.FieldName = "Cargo";
            gridViewTextBoxColumn19.FormatString = "{0:n}";
            gridViewTextBoxColumn19.HeaderText = "Cargo";
            gridViewTextBoxColumn19.Name = "Cargo";
            gridViewTextBoxColumn19.ReadOnly = true;
            gridViewTextBoxColumn19.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn19.Width = 85;
            gridViewTextBoxColumn20.FieldName = "FechaVence";
            gridViewTextBoxColumn20.HeaderText = "Fec. Vence";
            gridViewTextBoxColumn20.Name = "FechaVence";
            gridViewTextBoxColumn20.ReadOnly = true;
            gridViewTextBoxColumn20.Width = 85;
            gridViewTextBoxColumn21.FieldName = "FechaPago";
            gridViewTextBoxColumn21.HeaderText = "Cobrado / Pagado";
            gridViewTextBoxColumn21.IsVisible = false;
            gridViewTextBoxColumn21.Name = "FechaPago";
            gridViewTextBoxColumn21.ReadOnly = true;
            gridViewTextBoxColumn22.FieldName = "FechaBoveda";
            gridViewTextBoxColumn22.HeaderText = "Fec. Liberación";
            gridViewTextBoxColumn22.IsVisible = false;
            gridViewTextBoxColumn22.Name = "FechaBoveda";
            gridViewTextBoxColumn22.ReadOnly = true;
            gridViewTextBoxColumn22.Width = 85;
            gridViewTextBoxColumn23.FieldName = "FechaCancela";
            gridViewTextBoxColumn23.HeaderText = "Fec. Cancela";
            gridViewTextBoxColumn23.IsVisible = false;
            gridViewTextBoxColumn23.Name = "FechaCancela";
            gridViewTextBoxColumn23.ReadOnly = true;
            gridViewTextBoxColumn23.Width = 85;
            gridViewTextBoxColumn24.HeaderText = "Fec. Auto";
            gridViewTextBoxColumn24.IsVisible = false;
            gridViewTextBoxColumn24.Name = "FechaAutoriza";
            gridViewTextBoxColumn24.ReadOnly = true;
            gridViewTextBoxColumn24.Width = 85;
            gridViewTextBoxColumn25.FieldName = "Autoriza";
            gridViewTextBoxColumn25.HeaderText = "Autoriza";
            gridViewTextBoxColumn25.IsVisible = false;
            gridViewTextBoxColumn25.Name = "Autoriza";
            gridViewTextBoxColumn25.ReadOnly = true;
            gridViewTextBoxColumn26.FieldName = "Cancela";
            gridViewTextBoxColumn26.HeaderText = "Canceló";
            gridViewTextBoxColumn26.Name = "Cancela";
            gridViewTextBoxColumn26.ReadOnly = true;
            gridViewTextBoxColumn27.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn27.FieldName = "FechaEmision";
            gridViewTextBoxColumn27.FormatString = "{0:dd-MMM-yy hh:mm tt}";
            gridViewTextBoxColumn27.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn27.Name = "FechaEmision";
            gridViewTextBoxColumn27.ReadOnly = true;
            gridViewTextBoxColumn27.Width = 85;
            gridViewTextBoxColumn28.FieldName = "Creo";
            gridViewTextBoxColumn28.HeaderText = "Creó";
            gridViewTextBoxColumn28.Name = "Creo";
            gridViewTextBoxColumn28.ReadOnly = true;
            gridViewTextBoxColumn29.FieldName = "Modifica";
            gridViewTextBoxColumn29.HeaderText = "Mod.";
            gridViewTextBoxColumn29.IsVisible = false;
            gridViewTextBoxColumn29.Name = "Modifica";
            gridViewTextBoxColumn29.ReadOnly = true;
            gridViewTextBoxColumn30.FieldName = "FechaMod";
            gridViewTextBoxColumn30.HeaderText = "Fec. Mod.";
            gridViewTextBoxColumn30.IsVisible = false;
            gridViewTextBoxColumn30.Name = "FechaModifica";
            gridViewTextBoxColumn30.ReadOnly = true;
            gridViewTextBoxColumn30.Width = 85;
            gridViewTextBoxColumn31.FieldName = "JPrepoliza";
            gridViewTextBoxColumn31.HeaderText = "_cntbl3_json";
            gridViewTextBoxColumn31.IsVisible = false;
            gridViewTextBoxColumn31.Name = "JPrepoliza";
            gridViewTextBoxColumn31.ReadOnly = true;
            gridViewTextBoxColumn31.VisibleInColumnChooser = false;
            expressionFormattingObject1.ApplyToRow = true;
            expressionFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.Expression = "PorJustificar = 1";
            expressionFormattingObject1.Name = "Por Justificar";
            expressionFormattingObject1.RowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            expressionFormattingObject1.RowForeColor = System.Drawing.Color.Empty;
            gridViewTextBoxColumn32.ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            gridViewTextBoxColumn32.DataType = typeof(int);
            gridViewTextBoxColumn32.FieldName = "PorJustificar";
            gridViewTextBoxColumn32.HeaderText = "PorJustificar";
            gridViewTextBoxColumn32.IsVisible = false;
            gridViewTextBoxColumn32.Name = "PorJustificar";
            gridViewTextBoxColumn32.VisibleInColumnChooser = false;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32});
            this.GridData.MasterTemplate.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.GridComprobantes});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1346, 600);
            this.GridData.TabIndex = 16;
            // 
            // Espera
            // 
            this.Espera.AssociatedControl = this.GridData;
            this.Espera.Location = new System.Drawing.Point(541, 249);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(70, 70);
            this.Espera.TabIndex = 17;
            this.Espera.Text = "radWaitingBar1";
            this.Espera.WaitingIndicators.Add(this.dotsSpinnerWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 100;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsSpinner;
            // 
            // dotsSpinnerWaitingBarIndicatorElement1
            // 
            this.dotsSpinnerWaitingBarIndicatorElement1.Name = "dotsSpinnerWaitingBarIndicatorElement1";
            // 
            // GridComprobantes
            // 
            this.GridComprobantes.Caption = "Comprobantes";
            gridViewTextBoxColumn33.FieldName = "TipoComprobante";
            gridViewTextBoxColumn33.HeaderText = "Tipo";
            gridViewTextBoxColumn33.Name = "TipoComprobante";
            gridViewTextBoxColumn34.FieldName = "Folio";
            gridViewTextBoxColumn34.HeaderText = "Folio";
            gridViewTextBoxColumn34.Name = "Folio";
            gridViewTextBoxColumn35.FieldName = "Serie";
            gridViewTextBoxColumn35.HeaderText = "Serie";
            gridViewTextBoxColumn35.Name = "Serie";
            gridViewTextBoxColumn36.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn36.FieldName = "FechaEmision";
            gridViewTextBoxColumn36.FormatString = "{0:dd-MMM-yy}";
            gridViewTextBoxColumn36.HeaderText = "Fecha Emisión";
            gridViewTextBoxColumn36.Name = "FechaEmision";
            gridViewTextBoxColumn36.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn36.Width = 75;
            gridViewTextBoxColumn37.FieldName = "Uuid";
            gridViewTextBoxColumn37.HeaderText = "UUID";
            gridViewTextBoxColumn37.Name = "Uuid";
            gridViewTextBoxColumn37.Width = 185;
            gridViewTextBoxColumn38.FieldName = "RfcEmisor";
            gridViewTextBoxColumn38.HeaderText = "Emisor (RFC)";
            gridViewTextBoxColumn38.Name = "RfcEmisor";
            gridViewTextBoxColumn38.Width = 75;
            gridViewTextBoxColumn39.FieldName = "Emisor";
            gridViewTextBoxColumn39.HeaderText = "Emisor";
            gridViewTextBoxColumn39.Name = "Emisor";
            gridViewTextBoxColumn39.Width = 180;
            gridViewTextBoxColumn40.FieldName = "RfcReceptor";
            gridViewTextBoxColumn40.HeaderText = "Receptor (RFC)";
            gridViewTextBoxColumn40.Name = "RfcReceptor";
            gridViewTextBoxColumn40.Width = 75;
            gridViewTextBoxColumn41.FieldName = "Receptor";
            gridViewTextBoxColumn41.HeaderText = "Receptor";
            gridViewTextBoxColumn41.Name = "Receptor";
            gridViewTextBoxColumn41.Width = 180;
            gridViewTextBoxColumn42.DataType = typeof(decimal);
            gridViewTextBoxColumn42.FieldName = "Total";
            gridViewTextBoxColumn42.FormatString = "{0:n}";
            gridViewTextBoxColumn42.HeaderText = "Total";
            gridViewTextBoxColumn42.Name = "Total";
            gridViewTextBoxColumn42.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn42.Width = 85;
            gridViewTextBoxColumn43.DataType = typeof(decimal);
            gridViewTextBoxColumn43.FieldName = "Acumulado";
            gridViewTextBoxColumn43.FormatString = "{0:n}";
            gridViewTextBoxColumn43.HeaderText = "Acumulado";
            gridViewTextBoxColumn43.Name = "Acumulado";
            gridViewTextBoxColumn43.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn43.Width = 85;
            gridViewTextBoxColumn44.DataType = typeof(decimal);
            gridViewTextBoxColumn44.FieldName = "Abono";
            gridViewTextBoxColumn44.FormatString = "{0:n}";
            gridViewTextBoxColumn44.HeaderText = "Abono";
            gridViewTextBoxColumn44.Name = "Abono";
            gridViewTextBoxColumn44.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn44.Width = 85;
            gridViewTextBoxColumn45.DataType = typeof(decimal);
            gridViewTextBoxColumn45.FieldName = "Cargo";
            gridViewTextBoxColumn45.FormatString = "{0:n}";
            gridViewTextBoxColumn45.HeaderText = "Cargo";
            gridViewTextBoxColumn45.Name = "Cargo";
            gridViewTextBoxColumn45.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn45.Width = 85;
            this.GridComprobantes.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34,
            gridViewTextBoxColumn35,
            gridViewTextBoxColumn36,
            gridViewTextBoxColumn37,
            gridViewTextBoxColumn38,
            gridViewTextBoxColumn39,
            gridViewTextBoxColumn40,
            gridViewTextBoxColumn41,
            gridViewTextBoxColumn42,
            gridViewTextBoxColumn43,
            gridViewTextBoxColumn44,
            gridViewTextBoxColumn45});
            this.GridComprobantes.ViewDefinition = tableViewDefinition1;
            // 
            // Imagenes
            // 
            this.Imagenes.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Imagenes.ImageStream")));
            this.Imagenes.TransparentColor = System.Drawing.Color.Transparent;
            this.Imagenes.Images.SetKeyName(0, "accept_button.png");
            this.Imagenes.Images.SetKeyName(1, "_cfdi_url_pdf");
            this.Imagenes.Images.SetKeyName(2, "_cfdi_url_xml");
            // 
            // MenuContextual
            // 
            this.MenuContextual.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ContextMenuCopiar,
            this.ContextMenuClonar,
            this.ContextMenuCrearPago,
            this.ContextMenuSustituir});
            // 
            // ContextMenuCopiar
            // 
            this.ContextMenuCopiar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_copiar_portapapeles;
            this.ContextMenuCopiar.Name = "ContextMenuCopiar";
            this.ContextMenuCopiar.Text = "Copiar";
            this.ContextMenuCopiar.Click += new System.EventHandler(this.ContextMenuCopiar_Click);
            // 
            // ContextMenuClonar
            // 
            this.ContextMenuClonar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_copiar;
            this.ContextMenuClonar.Name = "ContextMenuClonar";
            this.ContextMenuClonar.Text = "Clonar Comprobante";
            this.ContextMenuClonar.Click += new System.EventHandler(this.ContextMenuClonar_Click);
            // 
            // ContextMenuCrearPago
            // 
            this.ContextMenuCrearPago.Name = "ContextMenuCrearPago";
            this.ContextMenuCrearPago.Text = "Crear Recibo Electrónico de Pago";
            this.ContextMenuCrearPago.Click += new System.EventHandler(this.ContextMenuCrearPago_Click);
            // 
            // ContextMenuSustituir
            // 
            this.ContextMenuSustituir.Name = "ContextMenuSustituir";
            this.ContextMenuSustituir.Text = "Sustituir Comprobante";
            this.ContextMenuSustituir.Click += new System.EventHandler(this.ContextMenuSustituir_Click);
            // 
            // ViewPrePolizas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1346, 663);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.CommandBar);
            this.Name = "ViewPrePolizas";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "PrePolizas";
            this.Load += new System.EventHandler(this.PrePolizas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.GridData.ResumeLayout(false);
            this.GridData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadMenuItem PrintComprobante;
        internal Telerik.WinControls.UI.CommandBarSplitButton ToolBarAttach;
        private Telerik.WinControls.UI.RadMenuItem AttachAdd;
        private Telerik.WinControls.UI.RadMenuItem AttachDownload;
        private Telerik.WinControls.UI.RadMenuItem AttachDelete;
        internal Telerik.WinControls.UI.RadCommandBar CommandBar;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRow;
        internal Telerik.WinControls.UI.CommandBarStripElement ToolBarAcciones;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarEdit;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarDelete;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator1;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarRefresh;
        internal Telerik.WinControls.UI.CommandBarToggleButton ToolBarFilter;
        internal Telerik.WinControls.UI.CommandBarToggleButton ToolBarAutoSum;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarExport;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarPrint;
        internal Telerik.WinControls.UI.RadGridView GridData;
        internal System.Windows.Forms.ImageList Imagenes;
        private Telerik.WinControls.UI.GridViewTemplate GridComprobantes;
        private Telerik.WinControls.UI.RadMenuItem PrintLista;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarNew;
        private Telerik.WinControls.UI.RadMenuItem ToolBarNewReciboMultipago;
        private Telerik.WinControls.UI.RadMenuItem ToolBarReciboNuevo;
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement dotsSpinnerWaitingBarIndicatorElement1;
        private Telerik.WinControls.UI.RadContextMenu MenuContextual;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuCopiar;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuClonar;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuCrearPago;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuSustituir;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelEjercicio;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarSplitEjercicio;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelPeriodo;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarSplitPeriodo;
        private Telerik.WinControls.UI.CommandBarButton ToolBarClose;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
    }
}
