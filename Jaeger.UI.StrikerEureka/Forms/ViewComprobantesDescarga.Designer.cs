﻿namespace Jaeger.Views
{
    partial class ViewComprobantesDescarga
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewComprobantesDescarga));
            this.DescargaTipoEmitidas = new Telerik.WinControls.UI.RadMenuItem();
            this.DtFechaInicial = new Telerik.WinControls.UI.RadDateTimePicker();
            this.RadLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.DtFechaFinal = new Telerik.WinControls.UI.RadDateTimePicker();
            this.RadLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.CboComplemento = new Telerik.WinControls.UI.RadDropDownList();
            this.RadLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.CboStatus = new Telerik.WinControls.UI.RadDropDownList();
            this.RadLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.TxbFiltrarRFC = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.TxbCarpetaDescarga = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.GroupUsuario = new Telerik.WinControls.UI.RadGroupBox();
            this.TxbCaptcha = new Telerik.WinControls.UI.RadTextBox();
            this.ButtonRefresh = new Telerik.WinControls.UI.RadButton();
            this.ButtonDescargar = new Telerik.WinControls.UI.RadButton();
            this.lbRFC = new Telerik.WinControls.UI.RadLabel();
            this.TxbConsola = new System.Windows.Forms.TextBox();
            this.lbCIEC = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.PicCaptcha = new System.Windows.Forms.PictureBox();
            this.TxbRFC = new Telerik.WinControls.UI.RadTextBox();
            this.TxbClaveCiec = new Telerik.WinControls.UI.RadTextBox();
            this.BarraEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.LabelStatus = new Telerik.WinControls.UI.RadLabelElement();
            this.StatusBarProgreso = new Telerik.WinControls.UI.RadProgressBarElement();
            this.DescargaTipoRecibidas = new Telerik.WinControls.UI.RadMenuItem();
            this.FlowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.GroupConsulta = new Telerik.WinControls.UI.RadGroupBox();
            this.RadLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadCheckBox1 = new Telerik.WinControls.UI.RadCheckBox();
            this.ButtonDescargaTipo = new Telerik.WinControls.UI.RadSplitButton();
            this.DescargaTipoTodas = new Telerik.WinControls.UI.RadMenuItem();
            this.TxbStatus = new System.Windows.Forms.TextBox();
            this.ButtonExplorerCarpeta = new Telerik.WinControls.UI.RadButton();
            this.PageView = new Telerik.WinControls.UI.RadPageView();
            this.PageNavegador = new Telerik.WinControls.UI.RadPageViewPage();
            this.WebSAT = new System.Windows.Forms.WebBrowser();
            this.RadCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.CommandBarRowElementNavegador = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarNavegador = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarNavegadorButtonRegresa = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarNavegadorButtonAvanza = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarNavegadorButtonRecargar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarNavegadorTextBoxUrl = new Telerik.WinControls.UI.CommandBarTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.DtFechaInicial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtFechaFinal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboComplemento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbFiltrarRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCarpetaDescarga)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupUsuario)).BeginInit();
            this.GroupUsuario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCaptcha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonDescargar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCIEC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCaptcha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbClaveCiec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).BeginInit();
            this.FlowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GroupConsulta)).BeginInit();
            this.GroupConsulta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonDescargaTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonExplorerCarpeta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PageView)).BeginInit();
            this.PageView.SuspendLayout();
            this.PageNavegador.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // DescargaTipoEmitidas
            // 
            this.DescargaTipoEmitidas.Name = "DescargaTipoEmitidas";
            this.DescargaTipoEmitidas.Text = "Emitidos";
            this.DescargaTipoEmitidas.UseCompatibleTextRendering = false;
            // 
            // DtFechaInicial
            // 
            this.DtFechaInicial.Location = new System.Drawing.Point(71, 52);
            this.DtFechaInicial.Name = "DtFechaInicial";
            this.DtFechaInicial.Size = new System.Drawing.Size(179, 20);
            this.DtFechaInicial.TabIndex = 1;
            this.DtFechaInicial.TabStop = false;
            this.DtFechaInicial.Text = "lunes, 26 de noviembre de 2018";
            this.DtFechaInicial.Value = new System.DateTime(2018, 11, 26, 14, 42, 33, 335);
            // 
            // RadLabel7
            // 
            this.RadLabel7.Location = new System.Drawing.Point(10, 207);
            this.RadLabel7.Name = "RadLabel7";
            this.RadLabel7.Size = new System.Drawing.Size(64, 18);
            this.RadLabel7.TabIndex = 92;
            this.RadLabel7.Text = "Guardar en:";
            // 
            // DtFechaFinal
            // 
            this.DtFechaFinal.Location = new System.Drawing.Point(71, 78);
            this.DtFechaFinal.Name = "DtFechaFinal";
            this.DtFechaFinal.Size = new System.Drawing.Size(179, 20);
            this.DtFechaFinal.TabIndex = 2;
            this.DtFechaFinal.TabStop = false;
            this.DtFechaFinal.Text = "lunes, 26 de noviembre de 2018";
            this.DtFechaFinal.Value = new System.DateTime(2018, 11, 26, 14, 42, 33, 335);
            // 
            // RadLabel6
            // 
            this.RadLabel6.Location = new System.Drawing.Point(10, 181);
            this.RadLabel6.Name = "RadLabel6";
            this.RadLabel6.Size = new System.Drawing.Size(62, 18);
            this.RadLabel6.TabIndex = 91;
            this.RadLabel6.Text = "RFC Emisor";
            // 
            // CboComplemento
            // 
            this.CboComplemento.Location = new System.Drawing.Point(10, 154);
            this.CboComplemento.Name = "CboComplemento";
            this.CboComplemento.NullText = "Selecciona";
            this.CboComplemento.Size = new System.Drawing.Size(240, 20);
            this.CboComplemento.TabIndex = 19;
            // 
            // RadLabel5
            // 
            this.RadLabel5.Location = new System.Drawing.Point(10, 130);
            this.RadLabel5.Name = "RadLabel5";
            this.RadLabel5.Size = new System.Drawing.Size(197, 18);
            this.RadLabel5.TabIndex = 90;
            this.RadLabel5.Text = "Tipo de Comprobante (Complemento)";
            // 
            // CboStatus
            // 
            this.CboStatus.Location = new System.Drawing.Point(146, 104);
            this.CboStatus.Name = "CboStatus";
            this.CboStatus.NullText = "Selecciona";
            this.CboStatus.Size = new System.Drawing.Size(104, 20);
            this.CboStatus.TabIndex = 20;
            // 
            // RadLabel4
            // 
            this.RadLabel4.Location = new System.Drawing.Point(10, 104);
            this.RadLabel4.Name = "RadLabel4";
            this.RadLabel4.Size = new System.Drawing.Size(130, 18);
            this.RadLabel4.TabIndex = 89;
            this.RadLabel4.Text = "Estado del Comprobante";
            // 
            // TxbFiltrarRFC
            // 
            this.TxbFiltrarRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxbFiltrarRFC.Location = new System.Drawing.Point(78, 180);
            this.TxbFiltrarRFC.MaxLength = 14;
            this.TxbFiltrarRFC.Name = "TxbFiltrarRFC";
            this.TxbFiltrarRFC.NullText = "Filtrar por RFC";
            this.TxbFiltrarRFC.Size = new System.Drawing.Size(172, 20);
            this.TxbFiltrarRFC.TabIndex = 21;
            this.TxbFiltrarRFC.ThemeName = "Office2007Silver";
            // 
            // RadLabel3
            // 
            this.RadLabel3.Location = new System.Drawing.Point(10, 79);
            this.RadLabel3.Name = "RadLabel3";
            this.RadLabel3.Size = new System.Drawing.Size(52, 18);
            this.RadLabel3.TabIndex = 88;
            this.RadLabel3.Text = "Fec. Final";
            // 
            // TxbCarpetaDescarga
            // 
            this.TxbCarpetaDescarga.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TxbCarpetaDescarga.ForeColor = System.Drawing.Color.Black;
            this.TxbCarpetaDescarga.Location = new System.Drawing.Point(77, 206);
            this.TxbCarpetaDescarga.Name = "TxbCarpetaDescarga";
            this.TxbCarpetaDescarga.Size = new System.Drawing.Size(128, 20);
            this.TxbCarpetaDescarga.TabIndex = 75;
            this.TxbCarpetaDescarga.TabStop = false;
            this.TxbCarpetaDescarga.ThemeName = "Office2007Silver";
            // 
            // RadLabel2
            // 
            this.RadLabel2.Location = new System.Drawing.Point(10, 53);
            this.RadLabel2.Name = "RadLabel2";
            this.RadLabel2.Size = new System.Drawing.Size(57, 18);
            this.RadLabel2.TabIndex = 87;
            this.RadLabel2.Text = "Fec. Inicial";
            // 
            // GroupUsuario
            // 
            this.GroupUsuario.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GroupUsuario.Controls.Add(this.TxbCaptcha);
            this.GroupUsuario.Controls.Add(this.ButtonRefresh);
            this.GroupUsuario.Controls.Add(this.ButtonDescargar);
            this.GroupUsuario.Controls.Add(this.lbRFC);
            this.GroupUsuario.Controls.Add(this.TxbConsola);
            this.GroupUsuario.Controls.Add(this.lbCIEC);
            this.GroupUsuario.Controls.Add(this.RadLabel8);
            this.GroupUsuario.Controls.Add(this.PicCaptcha);
            this.GroupUsuario.Controls.Add(this.TxbRFC);
            this.GroupUsuario.Controls.Add(this.TxbClaveCiec);
            this.GroupUsuario.HeaderText = "Usuario";
            this.GroupUsuario.Location = new System.Drawing.Point(272, 3);
            this.GroupUsuario.Name = "GroupUsuario";
            this.GroupUsuario.Size = new System.Drawing.Size(263, 260);
            this.GroupUsuario.TabIndex = 1;
            this.GroupUsuario.Text = "Usuario";
            // 
            // TxbCaptcha
            // 
            this.TxbCaptcha.Location = new System.Drawing.Point(60, 158);
            this.TxbCaptcha.Name = "TxbCaptcha";
            this.TxbCaptcha.NullText = "CAPTCHA";
            this.TxbCaptcha.Size = new System.Drawing.Size(81, 20);
            this.TxbCaptcha.TabIndex = 82;
            this.TxbCaptcha.ThemeName = "Office2007Silver";
            // 
            // ButtonRefresh
            // 
            this.ButtonRefresh.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonRefresh.Location = new System.Drawing.Point(214, 100);
            this.ButtonRefresh.Name = "ButtonRefresh";
            this.ButtonRefresh.Size = new System.Drawing.Size(25, 24);
            this.ButtonRefresh.TabIndex = 1;
            // 
            // ButtonDescargar
            // 
            this.ButtonDescargar.Location = new System.Drawing.Point(161, 156);
            this.ButtonDescargar.Name = "ButtonDescargar";
            this.ButtonDescargar.Size = new System.Drawing.Size(88, 24);
            this.ButtonDescargar.TabIndex = 83;
            this.ButtonDescargar.Text = "Descargar";
            // 
            // lbRFC
            // 
            this.lbRFC.Location = new System.Drawing.Point(5, 21);
            this.lbRFC.Name = "lbRFC";
            this.lbRFC.Size = new System.Drawing.Size(28, 18);
            this.lbRFC.TabIndex = 77;
            this.lbRFC.Text = "RFC:";
            // 
            // TxbConsola
            // 
            this.TxbConsola.Location = new System.Drawing.Point(5, 201);
            this.TxbConsola.Multiline = true;
            this.TxbConsola.Name = "TxbConsola";
            this.TxbConsola.Size = new System.Drawing.Size(242, 49);
            this.TxbConsola.TabIndex = 85;
            // 
            // lbCIEC
            // 
            this.lbCIEC.Location = new System.Drawing.Point(5, 45);
            this.lbCIEC.Name = "lbCIEC";
            this.lbCIEC.Size = new System.Drawing.Size(61, 18);
            this.lbCIEC.TabIndex = 78;
            this.lbCIEC.Text = "Clave CIEC:";
            // 
            // RadLabel8
            // 
            this.RadLabel8.Location = new System.Drawing.Point(8, 159);
            this.RadLabel8.Name = "RadLabel8";
            this.RadLabel8.Size = new System.Drawing.Size(47, 18);
            this.RadLabel8.TabIndex = 93;
            this.RadLabel8.Text = "Captcha";
            // 
            // PicCaptcha
            // 
            this.PicCaptcha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCaptcha.Location = new System.Drawing.Point(10, 70);
            this.PicCaptcha.Name = "PicCaptcha";
            this.PicCaptcha.Size = new System.Drawing.Size(239, 80);
            this.PicCaptcha.TabIndex = 81;
            this.PicCaptcha.TabStop = false;
            // 
            // TxbRFC
            // 
            this.TxbRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxbRFC.Location = new System.Drawing.Point(73, 20);
            this.TxbRFC.MaxLength = 14;
            this.TxbRFC.Name = "TxbRFC";
            this.TxbRFC.NullText = "RFC";
            this.TxbRFC.Size = new System.Drawing.Size(165, 20);
            this.TxbRFC.TabIndex = 79;
            this.TxbRFC.ThemeName = "Office2007Silver";
            // 
            // TxbClaveCiec
            // 
            this.TxbClaveCiec.Location = new System.Drawing.Point(73, 44);
            this.TxbClaveCiec.Name = "TxbClaveCiec";
            this.TxbClaveCiec.NullText = "Contraseña";
            this.TxbClaveCiec.PasswordChar = '*';
            this.TxbClaveCiec.Size = new System.Drawing.Size(165, 20);
            this.TxbClaveCiec.TabIndex = 80;
            this.TxbClaveCiec.ThemeName = "TelerikMetroBlue";
            // 
            // BarraEstado
            // 
            this.BarraEstado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.LabelStatus,
            this.StatusBarProgreso});
            this.BarraEstado.Location = new System.Drawing.Point(0, 268);
            this.BarraEstado.Name = "BarraEstado";
            this.BarraEstado.Size = new System.Drawing.Size(544, 26);
            this.BarraEstado.TabIndex = 7;
            // 
            // LabelStatus
            // 
            this.LabelStatus.Name = "LabelStatus";
            this.BarraEstado.SetSpring(this.LabelStatus, false);
            this.LabelStatus.Text = "...";
            this.LabelStatus.TextWrap = true;
            this.LabelStatus.UseCompatibleTextRendering = false;
            // 
            // StatusBarProgreso
            // 
            this.StatusBarProgreso.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.StatusBarProgreso.Name = "StatusBarProgreso";
            this.StatusBarProgreso.SeparatorColor1 = System.Drawing.Color.White;
            this.StatusBarProgreso.SeparatorColor2 = System.Drawing.Color.White;
            this.StatusBarProgreso.SeparatorColor3 = System.Drawing.Color.White;
            this.StatusBarProgreso.SeparatorColor4 = System.Drawing.Color.White;
            this.StatusBarProgreso.SeparatorGradientAngle = 0;
            this.StatusBarProgreso.SeparatorGradientPercentage1 = 0.4F;
            this.StatusBarProgreso.SeparatorGradientPercentage2 = 0.6F;
            this.StatusBarProgreso.SeparatorNumberOfColors = 2;
            this.BarraEstado.SetSpring(this.StatusBarProgreso, false);
            this.StatusBarProgreso.StepWidth = 14;
            this.StatusBarProgreso.SweepAngle = 90;
            this.StatusBarProgreso.Text = "";
            this.StatusBarProgreso.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.StatusBarProgreso.UseCompatibleTextRendering = false;
            this.StatusBarProgreso.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // DescargaTipoRecibidas
            // 
            this.DescargaTipoRecibidas.Name = "DescargaTipoRecibidas";
            this.DescargaTipoRecibidas.Text = "Recibidos";
            this.DescargaTipoRecibidas.UseCompatibleTextRendering = false;
            // 
            // FlowLayoutPanel1
            // 
            this.FlowLayoutPanel1.Controls.Add(this.GroupConsulta);
            this.FlowLayoutPanel1.Controls.Add(this.GroupUsuario);
            this.FlowLayoutPanel1.Controls.Add(this.PageView);
            this.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FlowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.FlowLayoutPanel1.Name = "FlowLayoutPanel1";
            this.FlowLayoutPanel1.Size = new System.Drawing.Size(544, 294);
            this.FlowLayoutPanel1.TabIndex = 8;
            // 
            // GroupConsulta
            // 
            this.GroupConsulta.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GroupConsulta.Controls.Add(this.RadLabel1);
            this.GroupConsulta.Controls.Add(this.RadCheckBox1);
            this.GroupConsulta.Controls.Add(this.ButtonDescargaTipo);
            this.GroupConsulta.Controls.Add(this.DtFechaInicial);
            this.GroupConsulta.Controls.Add(this.RadLabel7);
            this.GroupConsulta.Controls.Add(this.DtFechaFinal);
            this.GroupConsulta.Controls.Add(this.RadLabel6);
            this.GroupConsulta.Controls.Add(this.TxbStatus);
            this.GroupConsulta.Controls.Add(this.CboComplemento);
            this.GroupConsulta.Controls.Add(this.RadLabel5);
            this.GroupConsulta.Controls.Add(this.CboStatus);
            this.GroupConsulta.Controls.Add(this.RadLabel4);
            this.GroupConsulta.Controls.Add(this.TxbFiltrarRFC);
            this.GroupConsulta.Controls.Add(this.RadLabel3);
            this.GroupConsulta.Controls.Add(this.TxbCarpetaDescarga);
            this.GroupConsulta.Controls.Add(this.RadLabel2);
            this.GroupConsulta.Controls.Add(this.ButtonExplorerCarpeta);
            this.GroupConsulta.HeaderText = "Consulta";
            this.GroupConsulta.Location = new System.Drawing.Point(3, 3);
            this.GroupConsulta.Name = "GroupConsulta";
            this.GroupConsulta.Size = new System.Drawing.Size(263, 260);
            this.GroupConsulta.TabIndex = 1;
            this.GroupConsulta.Text = "Consulta";
            // 
            // RadLabel1
            // 
            this.RadLabel1.Location = new System.Drawing.Point(10, 25);
            this.RadLabel1.Name = "RadLabel1";
            this.RadLabel1.Size = new System.Drawing.Size(83, 18);
            this.RadLabel1.TabIndex = 86;
            this.RadLabel1.Text = "Comprobantes:";
            // 
            // RadCheckBox1
            // 
            this.RadCheckBox1.Enabled = false;
            this.RadCheckBox1.Location = new System.Drawing.Point(10, 232);
            this.RadCheckBox1.Name = "RadCheckBox1";
            this.RadCheckBox1.Size = new System.Drawing.Size(91, 18);
            this.RadCheckBox1.TabIndex = 94;
            this.RadCheckBox1.Text = "Solo consultar";
            // 
            // ButtonDescargaTipo
            // 
            this.ButtonDescargaTipo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.DescargaTipoTodas,
            this.DescargaTipoEmitidas,
            this.DescargaTipoRecibidas});
            this.ButtonDescargaTipo.Location = new System.Drawing.Point(97, 22);
            this.ButtonDescargaTipo.Name = "ButtonDescargaTipo";
            this.ButtonDescargaTipo.Size = new System.Drawing.Size(110, 24);
            this.ButtonDescargaTipo.TabIndex = 0;
            this.ButtonDescargaTipo.Text = "Selecciona";
            // 
            // DescargaTipoTodas
            // 
            this.DescargaTipoTodas.Name = "DescargaTipoTodas";
            this.DescargaTipoTodas.Text = "Todas";
            this.DescargaTipoTodas.UseCompatibleTextRendering = false;
            // 
            // TxbStatus
            // 
            this.TxbStatus.Location = new System.Drawing.Point(198, 232);
            this.TxbStatus.Name = "TxbStatus";
            this.TxbStatus.Size = new System.Drawing.Size(52, 20);
            this.TxbStatus.TabIndex = 1;
            this.TxbStatus.Visible = false;
            // 
            // ButtonExplorerCarpeta
            // 
            this.ButtonExplorerCarpeta.Image = ((System.Drawing.Image)(resources.GetObject("ButtonExplorerCarpeta.Image")));
            this.ButtonExplorerCarpeta.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonExplorerCarpeta.Location = new System.Drawing.Point(209, 205);
            this.ButtonExplorerCarpeta.Name = "ButtonExplorerCarpeta";
            this.ButtonExplorerCarpeta.Size = new System.Drawing.Size(41, 20);
            this.ButtonExplorerCarpeta.TabIndex = 76;
            this.ButtonExplorerCarpeta.ThemeName = "Office2007Silver";
            // 
            // PageView
            // 
            this.PageView.Controls.Add(this.PageNavegador);
            this.PageView.Location = new System.Drawing.Point(3, 269);
            this.PageView.Name = "PageView";
            this.PageView.SelectedPage = this.PageNavegador;
            this.PageView.Size = new System.Drawing.Size(1212, 800);
            this.PageView.TabIndex = 0;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // PageNavegador
            // 
            this.PageNavegador.Controls.Add(this.WebSAT);
            this.PageNavegador.Controls.Add(this.RadCommandBar2);
            this.PageNavegador.ItemSize = new System.Drawing.SizeF(71F, 28F);
            this.PageNavegador.Location = new System.Drawing.Point(10, 37);
            this.PageNavegador.Name = "PageNavegador";
            this.PageNavegador.Size = new System.Drawing.Size(1191, 752);
            this.PageNavegador.Text = "Navegador";
            // 
            // WebSAT
            // 
            this.WebSAT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WebSAT.Location = new System.Drawing.Point(0, 30);
            this.WebSAT.MinimumSize = new System.Drawing.Size(20, 20);
            this.WebSAT.Name = "WebSAT";
            this.WebSAT.ScriptErrorsSuppressed = true;
            this.WebSAT.Size = new System.Drawing.Size(1191, 722);
            this.WebSAT.TabIndex = 1;
            this.WebSAT.Url = new System.Uri("https://cfdiau.sat.gob.mx/nidp/app/login?id=SATUPCFDiCon&sid=0&option=credential&" +
        "sid=0", System.UriKind.Absolute);
            // 
            // RadCommandBar2
            // 
            this.RadCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar2.Name = "RadCommandBar2";
            this.RadCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElementNavegador});
            this.RadCommandBar2.Size = new System.Drawing.Size(1191, 30);
            this.RadCommandBar2.TabIndex = 0;
            // 
            // CommandBarRowElementNavegador
            // 
            this.CommandBarRowElementNavegador.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.CommandBarRowElementNavegador.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElementNavegador.Name = "CommandBarRowElementNavegador";
            this.CommandBarRowElementNavegador.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarNavegador});
            this.CommandBarRowElementNavegador.Text = "";
            this.CommandBarRowElementNavegador.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.CommandBarRowElementNavegador.UseCompatibleTextRendering = false;
            // 
            // ToolBarNavegador
            // 
            this.ToolBarNavegador.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarNavegador.DisplayName = "CommandBarStripElement1";
            this.ToolBarNavegador.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarNavegadorButtonRegresa,
            this.ToolBarNavegadorButtonAvanza,
            this.ToolBarNavegadorButtonRecargar,
            this.ToolBarNavegadorTextBoxUrl});
            this.ToolBarNavegador.Name = "ToolBarNavegador";
            this.ToolBarNavegador.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarNavegador.UseCompatibleTextRendering = false;
            // 
            // ToolBarNavegadorButtonRegresa
            // 
            this.ToolBarNavegadorButtonRegresa.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarNavegadorButtonRegresa.DisplayName = "Regresa";
            this.ToolBarNavegadorButtonRegresa.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarNavegadorButtonRegresa.Image")));
            this.ToolBarNavegadorButtonRegresa.Name = "ToolBarNavegadorButtonRegresa";
            this.ToolBarNavegadorButtonRegresa.Text = "Regresa";
            this.ToolBarNavegadorButtonRegresa.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarNavegadorButtonRegresa.UseCompatibleTextRendering = false;
            // 
            // ToolBarNavegadorButtonAvanza
            // 
            this.ToolBarNavegadorButtonAvanza.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarNavegadorButtonAvanza.DisplayName = "Avanza";
            this.ToolBarNavegadorButtonAvanza.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarNavegadorButtonAvanza.Image")));
            this.ToolBarNavegadorButtonAvanza.Name = "ToolBarNavegadorButtonAvanza";
            this.ToolBarNavegadorButtonAvanza.Text = "Avanza";
            this.ToolBarNavegadorButtonAvanza.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarNavegadorButtonAvanza.UseCompatibleTextRendering = false;
            // 
            // ToolBarNavegadorButtonRecargar
            // 
            this.ToolBarNavegadorButtonRecargar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarNavegadorButtonRecargar.DisplayName = "Recargar";
            this.ToolBarNavegadorButtonRecargar.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarNavegadorButtonRecargar.Image")));
            this.ToolBarNavegadorButtonRecargar.Name = "ToolBarNavegadorButtonRecargar";
            this.ToolBarNavegadorButtonRecargar.Text = "Recargar";
            this.ToolBarNavegadorButtonRecargar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarNavegadorButtonRecargar.UseCompatibleTextRendering = false;
            // 
            // ToolBarNavegadorTextBoxUrl
            // 
            this.ToolBarNavegadorTextBoxUrl.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarNavegadorTextBoxUrl.DisplayName = "Direccion";
            this.ToolBarNavegadorTextBoxUrl.MinSize = new System.Drawing.Size(500, 22);
            this.ToolBarNavegadorTextBoxUrl.Name = "ToolBarNavegadorTextBoxUrl";
            this.ToolBarNavegadorTextBoxUrl.Text = "";
            this.ToolBarNavegadorTextBoxUrl.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarNavegadorTextBoxUrl.UseCompatibleTextRendering = false;
            // 
            // ViewComprobantesDescarga
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 294);
            this.Controls.Add(this.BarraEstado);
            this.Controls.Add(this.FlowLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ViewComprobantesDescarga";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ViewComprobantesDescarga";
            ((System.ComponentModel.ISupportInitialize)(this.DtFechaInicial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtFechaFinal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboComplemento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbFiltrarRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCarpetaDescarga)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupUsuario)).EndInit();
            this.GroupUsuario.ResumeLayout(false);
            this.GroupUsuario.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCaptcha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonDescargar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCIEC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCaptcha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbClaveCiec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).EndInit();
            this.FlowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GroupConsulta)).EndInit();
            this.GroupConsulta.ResumeLayout(false);
            this.GroupConsulta.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonDescargaTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonExplorerCarpeta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PageView)).EndInit();
            this.PageView.ResumeLayout(false);
            this.PageNavegador.ResumeLayout(false);
            this.PageNavegador.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Telerik.WinControls.UI.RadMenuItem DescargaTipoEmitidas;
        internal Telerik.WinControls.UI.RadDateTimePicker DtFechaInicial;
        internal Telerik.WinControls.UI.RadLabel RadLabel7;
        internal Telerik.WinControls.UI.RadDateTimePicker DtFechaFinal;
        internal Telerik.WinControls.UI.RadLabel RadLabel6;
        internal Telerik.WinControls.UI.RadDropDownList CboComplemento;
        internal Telerik.WinControls.UI.RadLabel RadLabel5;
        internal Telerik.WinControls.UI.RadDropDownList CboStatus;
        internal Telerik.WinControls.UI.RadLabel RadLabel4;
        internal Telerik.WinControls.UI.RadTextBox TxbFiltrarRFC;
        internal Telerik.WinControls.UI.RadLabel RadLabel3;
        private Telerik.WinControls.UI.RadTextBox TxbCarpetaDescarga;
        internal Telerik.WinControls.UI.RadLabel RadLabel2;
        internal Telerik.WinControls.UI.RadButton ButtonExplorerCarpeta;
        internal Telerik.WinControls.UI.RadGroupBox GroupUsuario;
        internal Telerik.WinControls.UI.RadTextBox TxbCaptcha;
        internal Telerik.WinControls.UI.RadButton ButtonRefresh;
        internal Telerik.WinControls.UI.RadButton ButtonDescargar;
        internal Telerik.WinControls.UI.RadLabel lbRFC;
        internal System.Windows.Forms.TextBox TxbConsola;
        internal Telerik.WinControls.UI.RadLabel lbCIEC;
        internal Telerik.WinControls.UI.RadLabel RadLabel8;
        internal System.Windows.Forms.PictureBox PicCaptcha;
        internal Telerik.WinControls.UI.RadTextBox TxbRFC;
        internal Telerik.WinControls.UI.RadTextBox TxbClaveCiec;
        internal Telerik.WinControls.UI.RadStatusStrip BarraEstado;
        internal Telerik.WinControls.UI.RadLabelElement LabelStatus;
        internal Telerik.WinControls.UI.RadProgressBarElement StatusBarProgreso;
        internal Telerik.WinControls.UI.RadMenuItem DescargaTipoRecibidas;
        internal System.Windows.Forms.FlowLayoutPanel FlowLayoutPanel1;
        internal Telerik.WinControls.UI.RadGroupBox GroupConsulta;
        internal Telerik.WinControls.UI.RadLabel RadLabel1;
        internal Telerik.WinControls.UI.RadCheckBox RadCheckBox1;
        internal Telerik.WinControls.UI.RadSplitButton ButtonDescargaTipo;
        internal Telerik.WinControls.UI.RadMenuItem DescargaTipoTodas;
        internal System.Windows.Forms.TextBox TxbStatus;
        internal Telerik.WinControls.UI.RadPageView PageView;
        internal Telerik.WinControls.UI.RadPageViewPage PageNavegador;
        internal System.Windows.Forms.WebBrowser WebSAT;
        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar2;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElementNavegador;
        internal Telerik.WinControls.UI.CommandBarStripElement ToolBarNavegador;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarNavegadorButtonRegresa;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarNavegadorButtonAvanza;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarNavegadorButtonRecargar;
        internal Telerik.WinControls.UI.CommandBarTextBox ToolBarNavegadorTextBoxUrl;
    }
}
