﻿namespace Jaeger.Views
{
    partial class ViewConfiguracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.PropertyConf = new Telerik.WinControls.UI.RadPropertyGrid();
            this.ButtonCerrar = new Telerik.WinControls.UI.RadButton();
            this.ButtonGuardar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertyConf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonGuardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 12);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(152, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Configuración de la empresa:";
            // 
            // PropertyConf
            // 
            this.PropertyConf.Location = new System.Drawing.Point(12, 36);
            this.PropertyConf.Name = "PropertyConf";
            this.PropertyConf.Size = new System.Drawing.Size(516, 419);
            this.PropertyConf.TabIndex = 1;
            // 
            // ButtonCerrar
            // 
            this.ButtonCerrar.Location = new System.Drawing.Point(418, 461);
            this.ButtonCerrar.Name = "ButtonCerrar";
            this.ButtonCerrar.Size = new System.Drawing.Size(110, 24);
            this.ButtonCerrar.TabIndex = 2;
            this.ButtonCerrar.Text = "Cerrar";
            this.ButtonCerrar.Click += new System.EventHandler(this.ButtonCerrar_Click);
            // 
            // ButtonGuardar
            // 
            this.ButtonGuardar.Enabled = false;
            this.ButtonGuardar.Location = new System.Drawing.Point(302, 461);
            this.ButtonGuardar.Name = "ButtonGuardar";
            this.ButtonGuardar.Size = new System.Drawing.Size(110, 24);
            this.ButtonGuardar.TabIndex = 3;
            this.ButtonGuardar.Text = "Guardar";
            this.ButtonGuardar.Click += new System.EventHandler(this.ButtonGuardar_Click);
            // 
            // ViewConfiguracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 494);
            this.Controls.Add(this.ButtonGuardar);
            this.Controls.Add(this.ButtonCerrar);
            this.Controls.Add(this.PropertyConf);
            this.Controls.Add(this.radLabel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewConfiguracion";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Configuración";
            this.Load += new System.EventHandler(this.ViewConfiguracion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertyConf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonGuardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadPropertyGrid PropertyConf;
        private Telerik.WinControls.UI.RadButton ButtonCerrar;
        private Telerik.WinControls.UI.RadButton ButtonGuardar;
    }
}
