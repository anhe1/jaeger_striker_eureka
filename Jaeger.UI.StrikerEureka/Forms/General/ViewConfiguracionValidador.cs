﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2;
using Jaeger.Edita.V2.Empresa.Entities;
using System;
using Jaeger.UI.Common.Forms;

namespace Jaeger.Views.General
{
    public partial class ViewConfiguracionValidador : Telerik.WinControls.UI.RadForm
    {
        private Edita.V2.Validador.Entities.Configuracion configuracion;
        private ViewModelEmpresaConfiguracion registro;
        private ISqlConfiguracion data;

        public ViewConfiguracionValidador()
        {
            InitializeComponent();
        }

        private void ViewConfiguracionValidador_Load(object sender, EventArgs e)
        {
            this.data = new SqlSugarConfiguracion(ConfigService.Synapsis.RDS.Edita);
            this.ToolBarButtonActualizar.PerformClick();
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e)
        {
            using (var espera = new Waiting2Form(this.Consultar))
            {
                espera.Text = "Consultando ...";
                espera.ShowDialog();
            }
            this.propertyConfiguracion.SelectedObject = this.configuracion;
        }

        private void ToolBarButtonGuardar_Click(object sender, EventArgs e)
        {
            using (var espera = new Waiting2Form(this.Guardar))
            {
                espera.Text = "Guardando ...";
                espera.ShowDialog();
            }
            this.propertyConfiguracion.SelectedObject = this.configuracion;
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Consultar()
        {
            this.registro = this.data.GetByKey("validador");
            if (this.registro == null)
                this.configuracion = new Edita.V2.Validador.Entities.Configuracion();
            else
                this.configuracion = Edita.V2.Validador.Entities.Configuracion.Json(this.registro.Data);
        }

        private void Guardar()
        {
            this.registro.Data = this.configuracion.Json();
            this.data.Save(this.registro);
        }

        private void Crear()
        {

        }
    }
}
