﻿using System;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Views
{
    public partial class ViewConfiguracion : Telerik.WinControls.UI.RadForm
    {
        public ViewConfiguracion()
        {
            InitializeComponent();
        }

        private void ViewConfiguracion_Load(object sender, EventArgs e)
        {
            this.radLabel1.Text = string.Format(this.radLabel1.Text, ConfigService.Synapsis.Empresa.RFC);
            this.PropertyConf.SelectedObject = ConfigService.Synapsis;
        }

        private void ButtonCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ButtonGuardar_Click(object sender, EventArgs e)
        {

        }
    }
}
