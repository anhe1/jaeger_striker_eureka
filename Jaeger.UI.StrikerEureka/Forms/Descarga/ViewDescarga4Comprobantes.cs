﻿using System;
using System.Windows.Forms;
using Jaeger.Repositorio.Interface;
using Jaeger.Repositorio.V3.Entities;
using Jaeger.Repositorio.V3.Helpers;
using Jaeger.Repositorio.Helpers;
using Jaeger.Edita.Enums;
using Jaeger.Helpers;
using Telerik.WinControls.UI;
using Jaeger.Repositorio.V3.Enums;
using Jaeger.Aplication;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Views.Descarga
{
    public partial class ViewDescarga4Comprobantes : RadForm
    {
        private IDescargaMasiva descarga;
        private DateTime fechaInicial;
        private SQLiteRepositorio db;
        private Configuracion configuracion;
        private string currentUrl;
        private string tipoDeDescarga;

        public ViewDescarga4Comprobantes()
        {
            InitializeComponent();
            this.Size = new System.Drawing.Size(549, 323);
        }

        public ViewDescarga4Comprobantes(DateTime fecha, string tipo)
        {
            InitializeComponent();
            this.fechaInicial = fecha;
            this.tipoDeDescarga = tipo;
            this.FormClosed += ViewDescarga4Comprobantes_FormClosed;
        }

        private void ViewDescarga4Comprobantes_FormClosed(object sender, FormClosedEventArgs e)
        {
            descarga = null;
            this.WebSAT.Dispose();
        }

        private void ViewDescarga4Comprobantes_Load(object sender, EventArgs e)
        {
            this.LabelStatus.Text = "Esperando ...";
            this.ButtonDescargar.Enabled = false;

            this.configuracion = new Configuracion();
            this.configuracion.FechaInicial = this.fechaInicial;

            if (this.fechaInicial.Year == DateTime.Now.Year && this.fechaInicial.Month == DateTime.Now.Month)
                this.configuracion.FechaFinal = DateTime.Now;
            else
                this.configuracion.FechaFinal = new DateTime(fechaInicial.Year, fechaInicial.Month, 1).AddMonths(1).AddDays(-1);

            this.configuracion.TipoText = this.tipoDeDescarga;
            this.CreateBinding();

            this.CboComplemento.DataSource = new BindingSource(DescargaXmlSATComun.TipoComplementos(), null);
            this.CboComplemento.DisplayMember = "Value";
            this.CboComplemento.ValueMember = "Key";

            this.configuracion.RFC = ConfigService.Synapsis.Empresa.RFC;

            this.configuracion.CarpetaDescarga = JaegerManagerPaths.JaegerPath(EnumPaths.Repositorio);

            this.db = new SQLiteRepositorio(new Domain.DataBase.Entities.DataBaseConfiguracion { Database = JaegerManagerPaths.JaegerPath(EnumPaths.Repositorio, ConfigService.Synapsis.Empresa.RFC.ToLower() + ".sqlite") });

            if (ConfigService.Piloto.Clave.Contains("ANHE"))
            {
                this.MaximizeBox = true;
                this.MinimizeBox = true;
                this.FormBorderStyle = FormBorderStyle.Sizable;
            }
        }

        private void ButtonDescargaTipo_Click(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                this.ButtonDescargaTipo.DefaultItem = button;
                this.ButtonDescargaTipo.Text = button.Text;
            }
        }

        private void ButtonExplorerCarpeta_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
                folderBrowserDialog.SelectedPath = this.configuracion.CarpetaDescarga;
                folderBrowserDialog.ShowDialog();
                this.configuracion.CarpetaDescarga = folderBrowserDialog.SelectedPath;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void ButtonDescargar_Click(object sender, EventArgs e)
        {
            if ((this.configuracion.Tipo == EnumDescargaTipo.Recibidos))
                this.descarga = new DescargaXml2SAT(this.TxbConsola, this.TxbStatus);
            else if ((this.configuracion.Tipo == EnumDescargaTipo.Emitidos))
                this.descarga = new DescargaXml4SAT(this.TxbConsola, this.TxbStatus);
            else if ((this.configuracion.Tipo == EnumDescargaTipo.Todas))
                this.descarga = new DescargaXml3SAT(this.TxbConsola, this.TxbStatus);

            this.descarga.Configuracion = this.configuracion;
            this.GroupConsulta.Enabled = false;
            this.GroupUsuario.Enabled = false;
            this.ButtonDescargar.Enabled = false;
            this.TxbCaptcha.Enabled = false;
            this.PicCaptcha.Enabled = false;

            this.descarga.StartProcess += Iniciar_Proceso;
            this.descarga.ProgressChanged += Proceso_Changed;
            this.descarga.CompletedProcess += Proceso_Termiando;

            StatusBarProgreso.Minimum = 0;
            StatusBarProgreso.Maximum = 100;
            this.descarga.Consultar(this.WebSAT, this.configuracion);
        }

        private void ButtonRefresh_Click(object sender, EventArgs e)
        {
            this.WebSAT.Url = new Uri("https://cfdiau.sat.gob.mx/nidp/app/login?id=SATUPCFDiCon&sid=0&option=credential&sid=0");
        }

        private void ToolBarNavegadorButtonRecargar_Click(object sender, EventArgs e)
        {
            this.WebSAT.Refresh();
        }

        private void TxbStatus_TextChanged(object sender, EventArgs e)
        {
            this.LabelStatus.Text = this.TxbStatus.Text;
            Application.DoEvents();
        }

        private void WebSat_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            this.ToolBarNavegadorTextBoxUrl.Text = e.Url.ToString();
            if ((this.WebSAT != null))
            {
                HtmlElement htmlElementCapchaImgage = this.WebSAT.Document.GetElementById("IDPLogin");
                if ((htmlElementCapchaImgage != null))
                {
                    Application.DoEvents();
                    HtmlElementCollection elemetsimages = this.WebSAT.Document.GetElementsByTagName("img");

                    foreach (HtmlElement image in elemetsimages)
                    {
                        image.SetAttribute("id", "capchaimage");
                        mshtml.HTMLWindow2 w2 = (mshtml.HTMLWindow2)WebSAT.Document.Window.DomWindow;
                        w2.execScript("var ctrlRange = document.body.createControlRange();ctrlRange.add(document.getElementById('capchaimage'));ctrlRange.execCommand('Copy');", "javascript");
                        this.PicCaptcha.Image = Clipboard.GetImage();
                    }
                }

                while (true)
                {
                    Application.DoEvents();
                    if ((!(this.WebSAT.IsBusy)))
                    {
                        this.LabelStatus.Text = "Listo";
                        this.ButtonDescargar.Enabled = true;
                        break;
                    }
                }

                this.currentUrl = this.WebSAT.Url.ToString();
            }
        }

        private void Iniciar_Proceso(object sender, DescargaMasivaStartProcess e)
        {
        }

        private void Proceso_Termiando(object sender, DescargaMasivaCompletedProcess e)
        {
            this.descarga.Reset();
            this.db.Comprobantes = this.descarga.Resultados;
            this.db.SaveResults();
            this.WebSAT.Dispose();
            this.Close();
        }

        private void Proceso_Changed(object sender, DescargaMasivaProgreso e)
        {
            if (e.Completado > 0)
                this.StatusBarProgreso.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            else if ((e.Completado == 100))
                this.StatusBarProgreso.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.StatusBarProgreso.Value1 = e.Completado;
        }


        private void CreateBinding()
        {
            this.TxbRFC.DataBindings.Clear();
            this.TxbRFC.DataBindings.Add("Text", this.configuracion, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbClaveCiec.DataBindings.Clear();
            this.TxbClaveCiec.DataBindings.Add("Text", this.configuracion, "CIEC", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbCaptcha.DataBindings.Clear();
            this.TxbCaptcha.DataBindings.Add("Text", this.configuracion, "Captcha", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxbCarpetaDescarga.DataBindings.Clear();
            this.TxbCarpetaDescarga.DataBindings.Add("Text", this.configuracion, "CarpetaDescarga", true, DataSourceUpdateMode.OnPropertyChanged);
            this.DtFechaInicial.DataBindings.Clear();
            this.DtFechaInicial.DataBindings.Add("Value", this.configuracion, "FechaInicial", true, DataSourceUpdateMode.OnPropertyChanged);
            this.DtFechaFinal.DataBindings.Clear();
            this.DtFechaFinal.DataBindings.Add("Value", this.configuracion, "FechaFinal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboStatus.DataBindings.Clear();
            this.CboStatus.DataBindings.Add("Text", this.configuracion, "Status", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboComplemento.DataBindings.Clear();
            this.CboComplemento.DataBindings.Add("Text", this.configuracion, "Complemento", true, DataSourceUpdateMode.OnPropertyChanged);
            this.ButtonDescargaTipo.DataBindings.Clear();
            this.ButtonDescargaTipo.DataBindings.Add("Text", this.configuracion, "TipoText", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void ToolBarNavegadorButtonAvanza_Click(object sender, EventArgs e)
        {
            this.WebSAT.Url = new Uri(ToolBarNavegadorTextBoxUrl.Text);
        }
    }
}
