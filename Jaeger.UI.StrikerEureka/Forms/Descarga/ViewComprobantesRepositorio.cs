﻿ using System;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Edita.Enums;
using Jaeger.Repositorio.V3.Entities;
using Jaeger.Helpers;
using Jaeger.Repositorio.Helpers;
using Jaeger.Repositorio.V3.Enums;
using Jaeger.Edita.V2;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.SAT.Entities;
using System.Xml;
using System.Collections;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;

namespace Jaeger.Views.Descarga
{
    public partial class ViewComprobantesRepositorio : RadForm
    {
        private BackgroundWorker preparar;
        private BackgroundWorker consulta;
        private SQLiteRepositorio data;
        private SqlSugarComprobanteFiscal comprobantes;
        private BindingList<ViewModelDescargaResponse> datos;
        private string baseDatos;
        public ViewComprobantesRepositorio()
        {
            InitializeComponent();
            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += Preparar_DoWork;
            this.preparar.RunWorkerCompleted += Preparar_RunWorkerCompleted;
            this.consulta = new BackgroundWorker();
            this.consulta.DoWork += Consulta_DoWork;
            this.consulta.RunWorkerCompleted += Consulta_RunWorkerCompleted;
        }

        private void ViewComprobantesRepositorio_Load(object sender, EventArgs e)
        {
            this.baseDatos = JaegerManagerPaths.JaegerPath(EnumPaths.Repositorio, ConfigService.Synapsis.Empresa.RFC.ToLower() + ".sqlite");
            this.data = new SQLiteRepositorio(new Domain.DataBase.Entities.DataBaseConfiguracion { Database = this.baseDatos });

            if (File.Exists( this.baseDatos) == false)
            {
                RadMessageBox.Show(this, "Actualmente no existe el repositorio para la empresa actual, utilice la opción crear en la barra de herramientas.", "Error!", MessageBoxButtons.OK, RadMessageIcon.Error);
            } else {
                var d = this.data.GetVersion();
                if (d == null) {
                    MessageBox.Show(this, "Esta utilizando una versión anterior del repositorio, es necesario actualizar la versión.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            this.comprobantes = new SqlSugarComprobanteFiscal(ConfigService.Synapsis.RDS.Edita);
            this.GridData.TelerikGridCommon();
            this.preparar.RunWorkerAsync();
        }

        #region barra de herramientas

        private void ToolBarButtonTipo_Click(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                ToolBarComprobante.DefaultItem = button;
            }
        }

        private void ToolBarButtonPeriodo_Click(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                this.ToolBarButtonPeriodo.DefaultItem = button;
                this.ToolBarButtonPeriodo.Text = button.Text;
                this.ToolBarButtonPeriodo.Tag = button.Tag;
                this.ToolBarButtonPeriodo.PerformClick();
                this.ToolBarButtonActualizar.PerformClick();
            }
        }

        private void ToolBarButtonEjecicio_Click(object sender, EventArgs e)
        {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem)
            {
                this.ToolBarButtonEjercicio.DefaultItem = button;
                this.ToolBarButtonEjercicio.Text = button.Text;
                this.ToolBarButtonEjercicio.PerformClick();
            }
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e)
        {
            this.ToolBarButtonActualizar.Enabled = false;
            this.WaitingBar.StartWaiting();
            this.WaitingBar.Visible = true;
            this.consulta.RunWorkerAsync();
        }

        private void ToolBarButtonSATDescarga_Click(object sender, EventArgs e)
        {
            var d = this.data.GetVersion();
            if (d == null) {
                MessageBox.Show(this, "Es necesario actualiza el repositorio, utiliza la opción Crear desde las herramientas.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            } else {
                var descargam = new ViewDescarga4Comprobantes(new DateTime(int.Parse(ToolBarButtonEjercicio.Text), (int)Enum.Parse(typeof(EnumMonthsOfYear), this.ToolBarButtonPeriodo.Text.ToString()), 1), this.ToolBarComprobante.Text) { StartPosition = FormStartPosition.CenterParent };
                descargam.ShowDialog();
                this.ToolBarButtonActualizar.PerformClick();
            }
        }

        private void ToolBarButtonFiltro_Click(object sender, EventArgs e)
        {
            this.GridData.ShowFilteringRow = !(ToolBarButtonFiltro.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
            if (this.GridData.ShowFilteringRow == false)
                this.GridData.FilterDescriptors.Clear();
        }

        private void ToolBarButtonExportar_Click(object sender, EventArgs e)
        {
            TelerikGridExport exportar = new TelerikGridExport(this.GridData);
            exportar.ShowDialog(this);
        }

        private void ToolBarButtonImprimirComprobante_Click(object sender, EventArgs e)
        {
            if (this.GridData.CurrentRow != null)
            {
                var comprobante = this.GridData.CurrentRow.DataBoundItem as ViewModelDescargaResponse;
                if (comprobante != null)
                {
                    FileInfo pdf = new FileInfo(Path.Combine(comprobante.PathXML, comprobante.KeyNamePdf()));
                    if (pdf.Exists)
                    {
                        ViewerPdf vista = new ViewerPdf(pdf.FullName) { MdiParent = this.ParentForm, Text = comprobante.KeyNamePdf() };
                        vista.Show();
                    }
                    else
                    {
                        var comprobante1 = CFDI.V33.Comprobante.Load(Path.Combine(comprobante.PathXML, comprobante.KeyNameXml()));
                        if (comprobante1 == null)
                        {
                            if (HelperValidacion.IsBase64String(comprobante.XmlContentB64))
                                comprobante1 = CFDI.V33.Comprobante.LoadBytes(System.Text.Encoding.UTF8.GetBytes(comprobante.XmlContentB64));
                        }
                        if (comprobante1 == null)
                            RadMessageBox.Show(this, "No existe documento relacionado!", "Información", MessageBoxButtons.OK, RadMessageIcon.Info);
                        else
                        {
                            ViewReportes viewReportes = new ViewReportes(comprobante1) { MdiParent = this.ParentForm, Text = comprobante.KeyNamePdf() };
                            viewReportes.Show();
                        }
                    }
                }
            }
        }

        private void ToolBarButtonHerramientasCrear_Click(object sender, EventArgs e) {
            if (File.Exists(this.baseDatos) == false) {
                if (RadMessageBox.Show(this, "A continuación se creara el repositorio: " + this.baseDatos + " ¿Esta de acuerdo?", "Atención!", MessageBoxButtons.YesNo, RadMessageIcon.Exclamation) == DialogResult.Yes)
                    if (this.data.CreateDB()) {
                        this.data.CreateTables();
                        RadMessageBox.Show(this, "Se creo correctamente el repositorio para la empresa actual, por favor cierra la ventana y comienza de nuevo.", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    } else {
                        RadMessageBox.Show(this, "No fue posible crear el repositorio, por favor consulte al Adminsitrador.", "Error!", MessageBoxButtons.OK, RadMessageIcon.Error);
                    }
                return;
            } else {
                var d = this.data.GetVersion();
                if (d == null) {
                    if (RadMessageBox.Show(this, "Es necesario actualiza el repositorio, ¿Quieres continuar?", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Error) == DialogResult.Yes) {
                        this.data.CreateTables();
                    }
                }
            }
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        private void Preparar_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 2013; i <= DateTime.Now.Year; i++)
            {
                RadMenuItem oButtonYear = new RadMenuItem() { Text = i.ToString() };
                this.ToolBarButtonEjercicio.Items.Add(oButtonYear);
                oButtonYear.Click += new EventHandler(this.ToolBarButtonEjecicio_Click);
            }

            foreach (string item in Enum.GetNames(typeof(EnumMonthsOfYear)))
            {
                RadMenuItem oButtonMonth = new RadMenuItem { Text = item };
                this.ToolBarButtonPeriodo.Items.Add(oButtonMonth);
                oButtonMonth.Click += new EventHandler(this.ToolBarButtonPeriodo_Click);
            }
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.ToolBarButtonPeriodo.Text = Enum.GetName(typeof(EnumMonthsOfYear), DateTime.Now.Month);
            this.ToolBarButtonEjercicio.Text = DateTime.Now.Year.ToString();
        }

        private void Consulta_DoWork(object sender, DoWorkEventArgs e)
        {
            this.datos = this.data.GetList((EnumMonthsOfYear)(Enum.Parse(typeof(EnumMonthsOfYear), this.ToolBarButtonPeriodo.Text)), int.Parse(this.ToolBarButtonEjercicio.Text), this.ToolBarButtonTipo.Text, this.ToolBarComprobante.Text);
        }

        private void Consulta_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.GridData.DataSource = this.datos;
            this.WaitingBar.Visible = false;
            this.WaitingBar.StopWaiting();
            this.ToolBarButtonActualizar.Enabled = true;
        }

        #region acciones del grid

        private void GridData_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (e.Column.Name == "XmlContentB64")
            {
                if (DbConvert.ConvertString(e.CellElement.Value) != "")
                {
                    e.CellElement.DrawText = false;
                    e.CellElement.Image = Imagenes.Images["_cfdi_url_xml"];
                }
                else
                {
                    e.CellElement.Image = null;
                    e.CellElement.Children.Clear();
                }
            }
            else if (e.Column.Name == "PdfAcuseFinalB64")
            {
                if (DbConvert.ConvertString(e.CellElement.Value) != "")
                {
                    e.CellElement.Image = Imagenes.Images["_cfdi_url_pdf"];
                    e.CellElement.DrawText = false;
                }
                else
                {
                    e.CellElement.Image = null;
                    e.CellElement.Children.Clear();
                }
            }
            else if ((e.Column.Name == "Comprobado"))
            {
                if (DbConvert.ConvertString(e.CellElement.Value) == Enum.GetName(typeof(EnumDescargaConsilia), EnumDescargaConsilia.SinVerificar))
                {
                    e.CellElement.Image = Imagenes.Images["sincomprobar"];
                    e.CellElement.DrawText = false;
                }
                else if (DbConvert.ConvertString(e.CellElement.Value) == Enum.GetName(typeof(EnumDescargaConsilia), EnumDescargaConsilia.Verificado))
                {
                    e.CellElement.Image = Imagenes.Images["comprobado"];
                    e.CellElement.DrawText = false;
                }
                else
                {
                    e.CellElement.Image = Imagenes.Images["noencontrado"];
                    e.CellElement.DrawText = false;
                }
            }
            else if ((e.Column.Name == "AcusePdf"))
            {
                if (DbConvert.ConvertString(e.CellElement.Value) != "")
                {
                    e.CellElement.Image = Imagenes.Images["_cfdi_url_pdf"];
                    e.CellElement.DrawText = false;
                }
                else
                {
                    e.CellElement.Image = null;
                    e.CellElement.Children.Clear();
                }
            }
            else if ((e.Column.Name != "Status" | e.Column.Name != "AcusePdf" | e.Column.Name != "AcuseXml"))
            {
                e.CellElement.Image = null;
                e.CellElement.DrawText = true;
                e.CellElement.Children.Clear();
            }
        }

        #endregion

        private void ToolBarButtonConsiliar_Todo_Click(object sender, EventArgs e)
        {
            if (this.datos.Count > 0) {
                using (var espera = new Waiting2Form(this.Consiliar))
                {
                    espera.Text = "Procesando ...";
                    espera.ShowDialog(this);
                }
            } else {
                RadMessageBox.Show(this, "No existen documentos para procesar.", "Información", MessageBoxButtons.OK, RadMessageIcon.Info);
            }
        }

        /// <summary>
        /// crear consiliación de los comprobantes listados
        /// </summary>
        private void Consiliar()
        {
            // se obtiene la lista de los comprobantes registrados en el mes y año seleccionado
            BindingList<ViewModelComprobanteSingle> datos2 = this.comprobantes.GetListSingleBy(Edita.V2.CFDI.Enums.EnumCfdiSubType.Recibido, "Todos", (EnumMonthsOfYear)(Enum.Parse(typeof(EnumMonthsOfYear), this.ToolBarButtonPeriodo.Text)), int.Parse(this.ToolBarButtonEjercicio.Text));
            var lista = datos2.Select(x => x.IdDocumento).ToArray();
            if (datos2.Count > 0)
            {
                //obtener las diferencias entre el repositorio y los comprobantes almacenados en edita
                BindingList<ViewModelDescargaResponse> datos3 = new BindingList<ViewModelDescargaResponse>(datos.Where(it => lista.Contains(it.IdDocumento)).ToList());
                foreach (var item in datos3)
                {
                    var index = this.datos.IndexOf(item);
                    this.datos[index].Comprobado = EnumDescargaConsilia.Verificado;
                    this.datos[index].FechaValidacion = datos2.Where(x => x.IdDocumento == item.IdDocumento).Select(x => x.FechaValidacion).First();
                }
            }
        }

        private void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            string[] array = new string[] { "XmlContentB64", "PdfAcuseB64", "PdfAcuseFinalB64" };

            if (array.Contains(e.Column.Name))
            {
                //comprobamos el contenido de la celda como base64
                string b64 = e.Value.ToString();
                if (HelperValidacion.IsBase64String(b64))
                {
                    SaveFileDialog savefiledialog = new SaveFileDialog();
                    var seleccionado = e.Row.DataBoundItem as ViewModelDescargaResponse;
                    if (seleccionado != null)
                        if (e.Column.Name == "XmlContentB64")
                            savefiledialog.FileName = seleccionado.KeyNameXml();
                        else if (e.Column.Name == "PdfAcuseB64")
                            savefiledialog.FileName = seleccionado.KeyNamePdf();
                        else if (e.Column.Name == "PdfAcuseFinalB64")
                            savefiledialog.FileName = seleccionado.KeyNameAcuseFinal();

                    if (savefiledialog.ShowDialog() == DialogResult.OK)
                        Util.Helpers.HelperFiles.WriteFileB64(b64, savefiledialog.FileName);
                }
                else
                {
                    RadMessageBox.Show(this, "No existe documento relacionado!", "Información", MessageBoxButtons.OK, RadMessageIcon.Info);
                }
            }
        }

        private void ToolBarButtonSATConsulta_Click(object sender, EventArgs e)
        {
            if (this.GridData.CurrentRow != null)
            {
                using (var espera = new Waiting2Form(this.ConsultarEstadoSAT))
                {
                    espera.Text = "Consultando ...";
                    espera.ShowDialog(this);
                }
            }
        }

        private void ToolBarButtonMetadataSAT_Click(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog() { Title = "Selecciona archivo metadata SAT.", Filter = "*.txt|*.TXT" };
            if (openFile.ShowDialog() != DialogResult.OK)
                return;
            this.ToolBarButtonMetadataSAT.Tag = openFile.FileName;
        }

        /// <summary>
        /// 
        /// </summary>
        private void ConsultarEstadoSAT()
        {
            var seleccionado = this.GridData.CurrentRow.DataBoundItem as ViewModelDescargaResponse;
            if (seleccionado != null)
            {
                SatQueryResult response = HelperServiceQuerySAT.Query(seleccionado.EmisorRFC, seleccionado.ReceptorRFC, seleccionado.Total, seleccionado.IdDocumento);
                if (!(response.Key == "E"))
                {
                    seleccionado.Estado = response.Status;
                }
            }
        }

        private void ProcesarMetadata()
        {
            var layout = new Layout.Helpers.LayoutMetadata();
            var contenido = Util.Helpers.HelperFiles.ReadFileText((string)this.ToolBarButtonMetadataSAT.Tag);
            var response = layout.ImportarText(contenido);
        }

        private void ToolBarButtonSerializar_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show(this, "Este proceso puede durar algunos minutos, ¿Esta seguro de continuar?", "Atención", MessageBoxButtons.YesNo,RadMessageIcon.Question,MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                using (var espera = new Waiting2Form(this.Serializar1)) {
                    espera.Text = "Procesando documentos ...";
                    espera.ShowDialog(this);
                }
            }
        }

        private void Serializar1() {
            for (int i = 0; i < this.datos.Count; i++) {
                if (HelperValidacion.IsBase64String(this.datos[i].XmlContentB64)) {
                    this.datos[i] = this.Serializar(this.datos[i]);
                    this.data.Update(this.datos[i]);
                }
            }
        }

        public ViewModelDescargaResponse Serializar(ViewModelDescargaResponse model) {
            ////var stringXml = this.GetXmlString(model);
            ////string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
            ////if (stringXml.StartsWith(_byteOrderMarkUtf8)) {
            ////    var lastIndexOfUtf8 = _byteOrderMarkUtf8.Length - 1;
            ////    stringXml = stringXml.Remove(0, lastIndexOfUtf8);
            ////}

            //var _reader = XmlReader.Create((new StringReader(stringXml));

            var encodedString = Convert.FromBase64String(model.XmlContentB64);
            // Encode the XML string in a UTF-8 byte array
            //byte[] encodedString = Encoding.UTF8.GetBytes(texto);

            // Put the byte array into a stream and rewind it to the beginning
            MemoryStream ms = new MemoryStream();
            ms.Write(encodedString, 0, encodedString.Length);
            ms.Seek(0, SeekOrigin.Begin);
            ms.Flush();
            ms.Position = 0;
            StreamReader sr = new StreamReader(ms);
            var _reader = XmlReader.Create(ms);

            try {
                while (_reader.Read()) {
                    try {
                        if (_reader.IsStartElement()) {
                            string name = _reader.Name;
                            if (!(name == null)) {
                                if (name == "cfdi:Comprobante") {
                                    model.SubTotal = Convert.ToDecimal(_reader["SubTotal"]);
                                    model.Descuento = Convert.ToDecimal(_reader["Descuento"]);
                                } else if (name == "cfdi:Emisor") {
                                } else if (name == "cfdi:Receptor") {
                                } else if (name == "cfdi:Concepto") {
                                } else if (name == "cfdi:Traslado") {
                                } else if (name == "cfdi:Retenido") {
                                } else if (name == "cfdi:Impuestos") {
                                    this.ReaderCfdi33Impuestos(ref model, _reader.ReadOuterXml());
                                } else if (name == "tfd:TimbreFiscalDigital") {
                                } else if (name == "pago10:Pago") {

                                } else if (name == "pago10:DoctoRelacionado") {

                                } else if (name == "cfdi:CfdiRelacionados") {

                                } else if (name == "cfdi:CfdiRelacionado") {

                                } else if (name == "nomina12:Nomina") {

                                } else if (name == "implocal:ImpuestosLocales") {

                                } else if (name == "valesdedespensa:ValesDeDespensa") {

                                } else if (name == "aerolineas:Aerolineas") {

                                }
                            }
                        }
                    } catch (Exception e) {
                        Console.WriteLine(string.Concat("ReaderCFDI33: ", e.Message));
                    }
                }
            } catch (Exception e) {
                Console.WriteLine(string.Concat("ReaderCFDI32: ", e.Message));
            }
            return model;
        }

        public void ReaderCfdi33Impuestos(ref ViewModelDescargaResponse objeto, string inXml) {
            // 001 - ISR
            // 002 - IVA
            // 003 - IEPS
            objeto.RetencionIEPS = 0;
            objeto.RetencionISR = 0;
            objeto.RetencionIVA = 0;
            objeto.TrasladoIVA = 0;
            objeto.TrasladoIEPS = 0;

            if ((inXml.Contains("TotalImpuestosTrasladados") || inXml.Contains("TotalImpuestosRetenidos") ? true : false)) {
                XmlDocument xmlD = new XmlDocument();
                IEnumerator enumerator = null;
                xmlD.LoadXml(inXml);
                try {
                    enumerator = xmlD.SelectNodes("//*").GetEnumerator();
                    while (enumerator.MoveNext()) {
                        XmlElement current = (XmlElement)enumerator.Current;
                        try {
                            if (current.Name != "cfdi:Traslado") {
                                if (current.Name == "cfdi:Retencion") {
                                    if (current.GetAttribute("Impuesto").ToLower() == "002") {
                                        objeto.RetencionIVA = objeto.RetencionIVA + Convert.ToDecimal(current.GetAttribute("Importe"));
                                    } else if (current.GetAttribute("Impuesto").ToLower() == "001") {
                                        objeto.RetencionISR = objeto.RetencionISR + Convert.ToDecimal(current.GetAttribute("Importe"));
                                    } else if (current.GetAttribute("Impuesto").ToLower() == "003") {
                                        objeto.RetencionIEPS = objeto.RetencionIEPS + Convert.ToDecimal(current.GetAttribute("Importe"));
                                    }
                                }
                            } else if (current.GetAttribute("Impuesto").ToLower() == "002") {
                                objeto.TrasladoIVA = objeto.TrasladoIVA + Convert.ToDecimal(current.GetAttribute("Importe"));
                            } else if (current.GetAttribute("Impuesto").ToLower() == "001") {
                                objeto.TrasladoIEPS = objeto.TrasladoIEPS + Convert.ToDecimal(current.GetAttribute("Importe"));
                            }
                        } catch (Exception ex) {
                            Console.WriteLine(string.Concat("ReaderCFDI33Impuestos: ", ex.Message));
                        }
                    }
                } catch (Exception ex) {
                    Console.WriteLine(string.Concat("ReaderCFDI33Impuestos: ", ex.Message));
                }
            }
        }
    }
}
