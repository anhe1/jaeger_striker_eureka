﻿using Jaeger.Views.Descarga;

namespace Jaeger.Views.Descarga
{
    partial class ViewComprobantesRepositorio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewComprobantesRepositorio));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.CommandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.BarRowElement = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabelComprobante = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarComprobante = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.TipoComprobanteRecibidos = new Telerik.WinControls.UI.RadMenuItem();
            this.TipoComprobanteEmitidos = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarLabelTipo = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarButtonTipo = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.TipoTodos = new Telerik.WinControls.UI.RadMenuItem();
            this.TipoIngreso = new Telerik.WinControls.UI.RadMenuItem();
            this.TipoEgreso = new Telerik.WinControls.UI.RadMenuItem();
            this.TipoTraslado = new Telerik.WinControls.UI.RadMenuItem();
            this.TipoNomina = new Telerik.WinControls.UI.RadMenuItem();
            this.TipoPagos = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarLabelPeriodo = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarButtonPeriodo = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarLabelEjercicio = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarButtonEjercicio = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarButtonActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarButtonConsiliar = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonConsiliar_Todo = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonConsiliar_Seleccionado = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonExportar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonImprimir = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonImprimirComprobante = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonValidar = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarButtonSAT = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarButtonSATDescarga = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonSATConsulta = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonMetadataSAT = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonHerramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonHerramientasCrear = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonSerializar = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.WaitingBar = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            this.Imagenes = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            this.GridData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WaitingBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandBar
            // 
            this.CommandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBar.Location = new System.Drawing.Point(0, 0);
            this.CommandBar.Name = "CommandBar";
            this.CommandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.BarRowElement});
            this.CommandBar.Size = new System.Drawing.Size(1315, 40);
            this.CommandBar.TabIndex = 0;
            // 
            // BarRowElement
            // 
            this.BarRowElement.MinSize = new System.Drawing.Size(25, 25);
            this.BarRowElement.Name = "BarRowElement";
            this.BarRowElement.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "commandBarStripElement1";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelComprobante,
            this.ToolBarComprobante,
            this.ToolBarLabelTipo,
            this.ToolBarButtonTipo,
            this.ToolBarLabelPeriodo,
            this.ToolBarButtonPeriodo,
            this.ToolBarLabelEjercicio,
            this.ToolBarButtonEjercicio,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonFiltro,
            this.ToolBarButtonConsiliar,
            this.ToolBarButtonExportar,
            this.ToolBarButtonImprimir,
            this.ToolBarButtonValidar,
            this.ToolBarButtonSAT,
            this.ToolBarButtonHerramientas,
            this.ToolBarButtonCerrar});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            // 
            // ToolBarLabelComprobante
            // 
            this.ToolBarLabelComprobante.DisplayName = "Etiqueta Comprobante";
            this.ToolBarLabelComprobante.Name = "ToolBarLabelComprobante";
            this.ToolBarLabelComprobante.Text = "Comprobantes:";
            // 
            // ToolBarComprobante
            // 
            this.ToolBarComprobante.DefaultItem = this.TipoComprobanteRecibidos;
            this.ToolBarComprobante.DisplayName = "Tipo de Comprobante";
            this.ToolBarComprobante.DrawImage = false;
            this.ToolBarComprobante.DrawText = true;
            this.ToolBarComprobante.Image = null;
            this.ToolBarComprobante.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.TipoComprobanteEmitidos,
            this.TipoComprobanteRecibidos});
            this.ToolBarComprobante.MinSize = new System.Drawing.Size(78, 0);
            this.ToolBarComprobante.Name = "ToolBarComprobante";
            this.ToolBarComprobante.Text = "Recibidos";
            // 
            // TipoComprobanteRecibidos
            // 
            this.TipoComprobanteRecibidos.Name = "TipoComprobanteRecibidos";
            this.TipoComprobanteRecibidos.Text = "Recibidos";
            this.TipoComprobanteRecibidos.Click += new System.EventHandler(this.ToolBarButtonTipo_Click);
            // 
            // TipoComprobanteEmitidos
            // 
            this.TipoComprobanteEmitidos.Name = "TipoComprobanteEmitidos";
            this.TipoComprobanteEmitidos.Text = "Emitidos";
            this.TipoComprobanteEmitidos.Click += new System.EventHandler(this.ToolBarButtonTipo_Click);
            // 
            // ToolBarLabelTipo
            // 
            this.ToolBarLabelTipo.DisplayName = "Tipo";
            this.ToolBarLabelTipo.DrawImage = true;
            this.ToolBarLabelTipo.Name = "ToolBarLabelTipo";
            this.ToolBarLabelTipo.Text = "Tipo:";
            // 
            // ToolBarButtonTipo
            // 
            this.ToolBarButtonTipo.DefaultItem = this.TipoTodos;
            this.ToolBarButtonTipo.DisplayName = "Tipo";
            this.ToolBarButtonTipo.DrawImage = false;
            this.ToolBarButtonTipo.DrawText = true;
            this.ToolBarButtonTipo.Image = null;
            this.ToolBarButtonTipo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.TipoIngreso,
            this.TipoEgreso,
            this.TipoTraslado,
            this.TipoNomina,
            this.TipoPagos,
            this.TipoTodos});
            this.ToolBarButtonTipo.MinSize = new System.Drawing.Size(78, 0);
            this.ToolBarButtonTipo.Name = "ToolBarButtonTipo";
            this.ToolBarButtonTipo.Text = "Todos";
            // 
            // TipoTodos
            // 
            this.TipoTodos.Name = "TipoTodos";
            this.TipoTodos.Text = "Todos";
            // 
            // TipoIngreso
            // 
            this.TipoIngreso.Name = "TipoIngreso";
            this.TipoIngreso.Text = "Ingreso";
            // 
            // TipoEgreso
            // 
            this.TipoEgreso.Name = "TipoEgreso";
            this.TipoEgreso.Text = "Egreso";
            // 
            // TipoTraslado
            // 
            this.TipoTraslado.Name = "TipoTraslado";
            this.TipoTraslado.Text = "Traslado";
            // 
            // TipoNomina
            // 
            this.TipoNomina.Name = "TipoNomina";
            this.TipoNomina.Text = "Nómina";
            // 
            // TipoPagos
            // 
            this.TipoPagos.Name = "TipoPagos";
            this.TipoPagos.Text = "Pago";
            // 
            // ToolBarLabelPeriodo
            // 
            this.ToolBarLabelPeriodo.DisplayName = "Período";
            this.ToolBarLabelPeriodo.Name = "ToolBarLabelPeriodo";
            this.ToolBarLabelPeriodo.Text = "Período";
            // 
            // ToolBarButtonPeriodo
            // 
            this.ToolBarButtonPeriodo.DefaultItem = null;
            this.ToolBarButtonPeriodo.DisplayName = "Período";
            this.ToolBarButtonPeriodo.DrawImage = false;
            this.ToolBarButtonPeriodo.DrawText = true;
            this.ToolBarButtonPeriodo.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarButtonPeriodo.Image")));
            this.ToolBarButtonPeriodo.MinSize = new System.Drawing.Size(78, 0);
            this.ToolBarButtonPeriodo.Name = "ToolBarButtonPeriodo";
            this.ToolBarButtonPeriodo.Text = "Selecciona";
            // 
            // ToolBarLabelEjercicio
            // 
            this.ToolBarLabelEjercicio.DisplayName = "Etiqueta: Ejercicio";
            this.ToolBarLabelEjercicio.Name = "ToolBarLabelEjercicio";
            this.ToolBarLabelEjercicio.Text = "Ejercicio:";
            // 
            // ToolBarButtonEjercicio
            // 
            this.ToolBarButtonEjercicio.DefaultItem = null;
            this.ToolBarButtonEjercicio.DisplayName = "Ejercicio";
            this.ToolBarButtonEjercicio.DrawImage = false;
            this.ToolBarButtonEjercicio.DrawText = true;
            this.ToolBarButtonEjercicio.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarButtonEjercicio.Image")));
            this.ToolBarButtonEjercicio.Name = "ToolBarButtonEjercicio";
            this.ToolBarButtonEjercicio.Text = "0000";
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.DisplayName = "Actualizar";
            this.ToolBarButtonActualizar.DrawText = true;
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_actualizar;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.ToolBarButtonActualizar_Click);
            // 
            // ToolBarButtonFiltro
            // 
            this.ToolBarButtonFiltro.DisplayName = "Filtro";
            this.ToolBarButtonFiltro.DrawText = true;
            this.ToolBarButtonFiltro.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_filtrar;
            this.ToolBarButtonFiltro.Name = "ToolBarButtonFiltro";
            this.ToolBarButtonFiltro.Text = "Filtro";
            this.ToolBarButtonFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonFiltro.Click += new System.EventHandler(this.ToolBarButtonFiltro_Click);
            // 
            // ToolBarButtonConsiliar
            // 
            this.ToolBarButtonConsiliar.DisplayName = "Consiliar";
            this.ToolBarButtonConsiliar.DrawText = true;
            this.ToolBarButtonConsiliar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_importar_csv;
            this.ToolBarButtonConsiliar.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonConsiliar_Todo,
            this.ToolBarButtonConsiliar_Seleccionado});
            this.ToolBarButtonConsiliar.Name = "ToolBarButtonConsiliar";
            this.ToolBarButtonConsiliar.Text = "Conciliar";
            this.ToolBarButtonConsiliar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonConsiliar_Todo
            // 
            this.ToolBarButtonConsiliar_Todo.Name = "ToolBarButtonConsiliar_Todo";
            this.ToolBarButtonConsiliar_Todo.Text = "Todos";
            this.ToolBarButtonConsiliar_Todo.Click += new System.EventHandler(this.ToolBarButtonConsiliar_Todo_Click);
            // 
            // ToolBarButtonConsiliar_Seleccionado
            // 
            this.ToolBarButtonConsiliar_Seleccionado.Name = "ToolBarButtonConsiliar_Seleccionado";
            this.ToolBarButtonConsiliar_Seleccionado.Text = "Seleccionado";
            // 
            // ToolBarButtonExportar
            // 
            this.ToolBarButtonExportar.DisplayName = "Exportar";
            this.ToolBarButtonExportar.DrawText = true;
            this.ToolBarButtonExportar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_excel;
            this.ToolBarButtonExportar.Name = "ToolBarButtonExportar";
            this.ToolBarButtonExportar.Text = "Exportar";
            this.ToolBarButtonExportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonExportar.Click += new System.EventHandler(this.ToolBarButtonExportar_Click);
            // 
            // ToolBarButtonImprimir
            // 
            this.ToolBarButtonImprimir.DisplayName = "Imprimir";
            this.ToolBarButtonImprimir.DrawText = true;
            this.ToolBarButtonImprimir.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_imprimir;
            this.ToolBarButtonImprimir.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonImprimirComprobante});
            this.ToolBarButtonImprimir.Name = "ToolBarButtonImprimir";
            this.ToolBarButtonImprimir.Text = "Imprimir";
            this.ToolBarButtonImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonImprimirComprobante
            // 
            this.ToolBarButtonImprimirComprobante.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_pdf;
            this.ToolBarButtonImprimirComprobante.Name = "ToolBarButtonImprimirComprobante";
            this.ToolBarButtonImprimirComprobante.Text = "Comprobante";
            this.ToolBarButtonImprimirComprobante.Click += new System.EventHandler(this.ToolBarButtonImprimirComprobante_Click);
            // 
            // ToolBarButtonValidar
            // 
            this.ToolBarButtonValidar.DefaultItem = null;
            this.ToolBarButtonValidar.DisplayName = "Validar";
            this.ToolBarButtonValidar.DrawText = true;
            this.ToolBarButtonValidar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_seguridad_comprobado;
            this.ToolBarButtonValidar.Name = "ToolBarButtonValidar";
            this.ToolBarButtonValidar.Text = "Validar";
            this.ToolBarButtonValidar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonSAT
            // 
            this.ToolBarButtonSAT.DefaultItem = null;
            this.ToolBarButtonSAT.DisplayName = "SAT";
            this.ToolBarButtonSAT.DrawText = false;
            this.ToolBarButtonSAT.FlipText = false;
            this.ToolBarButtonSAT.Image = global::Jaeger.UI.Properties.Resources.sat;
            this.ToolBarButtonSAT.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonSATDescarga,
            this.ToolBarButtonSATConsulta,
            this.ToolBarButtonMetadataSAT});
            this.ToolBarButtonSAT.Name = "ToolBarButtonSAT";
            this.ToolBarButtonSAT.Text = "SAT";
            this.ToolBarButtonSAT.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonSATDescarga
            // 
            this.ToolBarButtonSATDescarga.Name = "ToolBarButtonSATDescarga";
            this.ToolBarButtonSATDescarga.Text = "Descarga Masiva";
            this.ToolBarButtonSATDescarga.Click += new System.EventHandler(this.ToolBarButtonSATDescarga_Click);
            // 
            // ToolBarButtonSATConsulta
            // 
            this.ToolBarButtonSATConsulta.Name = "ToolBarButtonSATConsulta";
            this.ToolBarButtonSATConsulta.Text = "Consultar Estado SAT";
            this.ToolBarButtonSATConsulta.ToolTipText = "Consultar el estado del comprobante con el servicio SAT";
            this.ToolBarButtonSATConsulta.Click += new System.EventHandler(this.ToolBarButtonSATConsulta_Click);
            // 
            // ToolBarButtonMetadataSAT
            // 
            this.ToolBarButtonMetadataSAT.Name = "ToolBarButtonMetadataSAT";
            this.ToolBarButtonMetadataSAT.Text = "Importar archivo metadata";
            this.ToolBarButtonMetadataSAT.Click += new System.EventHandler(this.ToolBarButtonMetadataSAT_Click);
            // 
            // ToolBarButtonHerramientas
            // 
            this.ToolBarButtonHerramientas.DisplayName = "Herramientas";
            this.ToolBarButtonHerramientas.DrawText = true;
            this.ToolBarButtonHerramientas.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_trabajo;
            this.ToolBarButtonHerramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonHerramientasCrear,
            this.ToolBarButtonSerializar});
            this.ToolBarButtonHerramientas.Name = "ToolBarButtonHerramientas";
            this.ToolBarButtonHerramientas.Text = "Herramientas";
            this.ToolBarButtonHerramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonHerramientasCrear
            // 
            this.ToolBarButtonHerramientasCrear.Name = "ToolBarButtonHerramientasCrear";
            this.ToolBarButtonHerramientasCrear.Text = "Crear repositorio";
            this.ToolBarButtonHerramientasCrear.Click += new System.EventHandler(this.ToolBarButtonHerramientasCrear_Click);
            // 
            // ToolBarButtonSerializar
            // 
            this.ToolBarButtonSerializar.Name = "ToolBarButtonSerializar";
            this.ToolBarButtonSerializar.Text = "Serializar";
            this.ToolBarButtonSerializar.Click += new System.EventHandler(this.ToolBarButtonSerializar_Click);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "Cerrar";
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_cerrar;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // GridData
            // 
            this.GridData.Controls.Add(this.WaitingBar);
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 40);
            // 
            // 
            // 
            gridViewTextBoxColumn1.FieldName = "Efecto";
            gridViewTextBoxColumn1.HeaderText = "Efecto";
            gridViewTextBoxColumn1.Name = "Efecto";
            gridViewTextBoxColumn1.Width = 85;
            gridViewTextBoxColumn2.FieldName = "Estado";
            gridViewTextBoxColumn2.HeaderText = "Estado";
            gridViewTextBoxColumn2.Name = "Estado";
            gridViewTextBoxColumn3.FieldName = "EmisorRFC";
            gridViewTextBoxColumn3.HeaderText = "Emisor RFC";
            gridViewTextBoxColumn3.Name = "EmisorRFC";
            gridViewTextBoxColumn3.Width = 90;
            gridViewTextBoxColumn4.FieldName = "Emisor";
            gridViewTextBoxColumn4.HeaderText = "Nombre o Razón Social del Emisor";
            gridViewTextBoxColumn4.Name = "Emisor";
            gridViewTextBoxColumn4.Width = 220;
            gridViewTextBoxColumn5.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn5.HeaderText = "Receptor RFC";
            gridViewTextBoxColumn5.Name = "ReceptorRFC";
            gridViewTextBoxColumn5.Width = 90;
            gridViewTextBoxColumn6.FieldName = "Receptor";
            gridViewTextBoxColumn6.HeaderText = "Nombre o Razón Social del Receptor";
            gridViewTextBoxColumn6.Name = "Receptor";
            gridViewTextBoxColumn6.Width = 220;
            gridViewTextBoxColumn7.FieldName = "IdDocumento";
            gridViewTextBoxColumn7.HeaderText = "UUID";
            gridViewTextBoxColumn7.Name = "IdDocumento";
            gridViewTextBoxColumn7.Width = 230;
            gridViewTextBoxColumn8.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn8.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.ShortDate;
            gridViewTextBoxColumn8.FieldName = "FechaEmision";
            gridViewTextBoxColumn8.FormatString = "{0:d}";
            gridViewTextBoxColumn8.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn8.Name = "FechaEmision";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn8.Width = 75;
            gridViewTextBoxColumn9.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn9.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.ShortDate;
            gridViewTextBoxColumn9.FieldName = "FechaCerfificacion";
            gridViewTextBoxColumn9.FormatString = "{0:d}";
            gridViewTextBoxColumn9.HeaderText = "Fec. Cerfificación";
            gridViewTextBoxColumn9.Name = "FechaCerfificacion";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn9.Width = 75;
            gridViewTextBoxColumn9.WrapText = true;
            gridViewTextBoxColumn10.FieldName = "PACCertifica";
            gridViewTextBoxColumn10.HeaderText = "PAC";
            gridViewTextBoxColumn10.Name = "PACCertifica";
            gridViewTextBoxColumn10.Width = 90;
            gridViewTextBoxColumn11.DataType = typeof(decimal);
            gridViewTextBoxColumn11.FieldName = "RetencionISR";
            gridViewTextBoxColumn11.FormatString = "{0:n2}";
            gridViewTextBoxColumn11.HeaderText = "RetencionISR";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "RetencionISR";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn11.Width = 75;
            gridViewTextBoxColumn12.DataType = typeof(decimal);
            gridViewTextBoxColumn12.FieldName = "RetencionIVA";
            gridViewTextBoxColumn12.FormatString = "{0:n2}";
            gridViewTextBoxColumn12.HeaderText = "RetencionIVA";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "RetencionIVA";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn12.Width = 75;
            gridViewTextBoxColumn13.DataType = typeof(decimal);
            gridViewTextBoxColumn13.FieldName = "RetencionIEPS";
            gridViewTextBoxColumn13.FormatString = "{0:n2}";
            gridViewTextBoxColumn13.HeaderText = "RetencionIEPS";
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "RetencionIEPS";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn13.Width = 75;
            gridViewTextBoxColumn14.DataType = typeof(decimal);
            gridViewTextBoxColumn14.FieldName = "TrasladoIVA";
            gridViewTextBoxColumn14.FormatString = "{0:n2}";
            gridViewTextBoxColumn14.HeaderText = "TrasladoIVA";
            gridViewTextBoxColumn14.IsVisible = false;
            gridViewTextBoxColumn14.Name = "TrasladoIVA";
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.Width = 75;
            gridViewTextBoxColumn15.DataType = typeof(decimal);
            gridViewTextBoxColumn15.FieldName = "TrasladoIEPS";
            gridViewTextBoxColumn15.FormatString = "{0:n2}";
            gridViewTextBoxColumn15.HeaderText = "TrasladoIEPS";
            gridViewTextBoxColumn15.IsVisible = false;
            gridViewTextBoxColumn15.Name = "TrasladoIEPS";
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn15.Width = 75;
            gridViewTextBoxColumn16.DataType = typeof(decimal);
            gridViewTextBoxColumn16.FieldName = "SubTotal";
            gridViewTextBoxColumn16.FormatString = "{0:n2}";
            gridViewTextBoxColumn16.HeaderText = "SubTotal";
            gridViewTextBoxColumn16.IsVisible = false;
            gridViewTextBoxColumn16.Name = "SubTotal";
            gridViewTextBoxColumn16.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn16.Width = 75;
            gridViewTextBoxColumn17.DataType = typeof(decimal);
            gridViewTextBoxColumn17.FieldName = "Descuento";
            gridViewTextBoxColumn17.FormatString = "{0:n2}";
            gridViewTextBoxColumn17.HeaderText = "Descuento";
            gridViewTextBoxColumn17.IsVisible = false;
            gridViewTextBoxColumn17.Name = "Descuento";
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn17.Width = 75;
            gridViewTextBoxColumn18.DataType = typeof(decimal);
            gridViewTextBoxColumn18.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Currency;
            gridViewTextBoxColumn18.FieldName = "Total";
            gridViewTextBoxColumn18.FormatString = "{0:n}";
            gridViewTextBoxColumn18.HeaderText = "Total";
            gridViewTextBoxColumn18.Name = "Total";
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn18.Width = 85;
            gridViewTextBoxColumn19.FieldName = "StatusCancelacion";
            gridViewTextBoxColumn19.HeaderText = "Estatus de cancelación";
            gridViewTextBoxColumn19.Name = "StatusCancelacion";
            gridViewTextBoxColumn19.Width = 120;
            gridViewTextBoxColumn20.FieldName = "StatusProcesoCancelar";
            gridViewTextBoxColumn20.HeaderText = "Proceso Cancelar";
            gridViewTextBoxColumn20.Name = "StatusProcesoCancelar";
            gridViewTextBoxColumn20.Width = 120;
            gridViewTextBoxColumn21.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn21.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.ShortDate;
            gridViewTextBoxColumn21.FieldName = "FechaStatusCancela";
            gridViewTextBoxColumn21.FormatString = "{0:d}";
            gridViewTextBoxColumn21.HeaderText = "Fec. Status Cancela";
            gridViewTextBoxColumn21.Name = "FechaStatusCancela";
            gridViewTextBoxColumn21.Width = 75;
            gridViewTextBoxColumn21.WrapText = true;
            gridViewTextBoxColumn22.FieldName = "Result";
            gridViewTextBoxColumn22.HeaderText = "Resultado";
            gridViewTextBoxColumn22.IsVisible = false;
            gridViewTextBoxColumn22.Name = "Result";
            gridViewTextBoxColumn22.Width = 65;
            gridViewTextBoxColumn23.FieldName = "PathXML";
            gridViewTextBoxColumn23.HeaderText = "PathXML";
            gridViewTextBoxColumn23.IsVisible = false;
            gridViewTextBoxColumn23.Name = "PathXML";
            gridViewTextBoxColumn24.FieldName = "PathPDF";
            gridViewTextBoxColumn24.HeaderText = "PathPDF";
            gridViewTextBoxColumn24.IsVisible = false;
            gridViewTextBoxColumn24.Name = "PathPDF";
            gridViewTextBoxColumn25.FieldName = "XmlContentB64";
            gridViewTextBoxColumn25.HeaderText = "XML";
            gridViewTextBoxColumn25.Name = "XmlContentB64";
            gridViewTextBoxColumn25.Width = 35;
            gridViewTextBoxColumn26.FieldName = "PdfAcuseB64";
            gridViewTextBoxColumn26.HeaderText = "Acuse";
            gridViewTextBoxColumn26.Name = "PdfAcuseB64";
            gridViewTextBoxColumn26.Width = 35;
            gridViewTextBoxColumn27.FieldName = "PdfAcuseFinalB64";
            gridViewTextBoxColumn27.HeaderText = "Acuse Final";
            gridViewTextBoxColumn27.Name = "PdfAcuseFinalB64";
            gridViewTextBoxColumn27.Width = 35;
            gridViewTextBoxColumn28.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.None;
            gridViewTextBoxColumn28.FieldName = "ComprobadoText";
            gridViewTextBoxColumn28.HeaderText = "C";
            gridViewTextBoxColumn28.Name = "Comprobado";
            gridViewTextBoxColumn28.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn28.Width = 35;
            gridViewTextBoxColumn29.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn29.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.ShortDate;
            gridViewTextBoxColumn29.FieldName = "FechaValidacion";
            gridViewTextBoxColumn29.FormatString = "{0:d}";
            gridViewTextBoxColumn29.HeaderText = "Fecha Validación";
            gridViewTextBoxColumn29.Name = "FechaValidacion";
            gridViewTextBoxColumn29.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn29.Width = 75;
            gridViewTextBoxColumn29.WrapText = true;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1315, 688);
            this.GridData.TabIndex = 3;
            this.GridData.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.GridData_CellFormatting);
            this.GridData.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridData_CellDoubleClick);
            // 
            // WaitingBar
            // 
            this.WaitingBar.AssociatedControl = this.GridData;
            this.WaitingBar.Location = new System.Drawing.Point(486, 260);
            this.WaitingBar.Name = "WaitingBar";
            this.WaitingBar.Size = new System.Drawing.Size(130, 24);
            this.WaitingBar.TabIndex = 1;
            this.WaitingBar.Text = "radWaitingBar1";
            this.WaitingBar.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.WaitingBar.WaitingSpeed = 80;
            this.WaitingBar.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            // 
            // Imagenes
            // 
            this.Imagenes.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Imagenes.ImageStream")));
            this.Imagenes.TransparentColor = System.Drawing.Color.Transparent;
            this.Imagenes.Images.SetKeyName(0, "_cfdi_url_pdf");
            this.Imagenes.Images.SetKeyName(1, "comprobado");
            this.Imagenes.Images.SetKeyName(2, "noencontrado");
            this.Imagenes.Images.SetKeyName(3, "sincomprobar");
            this.Imagenes.Images.SetKeyName(4, "_cfdi_url_xml");
            // 
            // ViewComprobantesRepositorio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1315, 728);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.CommandBar);
            this.Name = "ViewComprobantesRepositorio";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Herramientas: Repositorio";
            this.Load += new System.EventHandler(this.ViewComprobantesRepositorio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.GridData.ResumeLayout(false);
            this.GridData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WaitingBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar CommandBar;
        private Telerik.WinControls.UI.CommandBarRowElement BarRowElement;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelComprobante;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarComprobante;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelTipo;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonTipo;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelPeriodo;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonPeriodo;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelEjercicio;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonEjercicio;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonActualizar;
        internal Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.RadWaitingBar WaitingBar;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;
        private Telerik.WinControls.UI.RadMenuItem TipoComprobanteRecibidos;
        private Telerik.WinControls.UI.RadMenuItem TipoComprobanteEmitidos;
        private Telerik.WinControls.UI.RadMenuItem TipoTodos;
        private Telerik.WinControls.UI.RadMenuItem TipoIngreso;
        private Telerik.WinControls.UI.RadMenuItem TipoEgreso;
        private Telerik.WinControls.UI.RadMenuItem TipoTraslado;
        private Telerik.WinControls.UI.RadMenuItem TipoNomina;
        private Telerik.WinControls.UI.RadMenuItem TipoPagos;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltro;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonConsiliar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonExportar;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonImprimir;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonValidar;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonSAT;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonSATDescarga;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCerrar;
        internal System.Windows.Forms.ImageList Imagenes;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonImprimirComprobante;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonConsiliar_Todo;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonConsiliar_Seleccionado;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonSATConsulta;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonHerramientas;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonHerramientasCrear;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonMetadataSAT;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonSerializar;
    }
}
