﻿namespace Jaeger.Views
{
    partial class ViewMantenimiento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radPropertyGrid1 = new Telerik.WinControls.UI.RadPropertyGrid();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarButtonCrearClase = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonCrearTabla = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.radPropertyGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPropertyGrid1
            // 
            this.radPropertyGrid1.Location = new System.Drawing.Point(498, 60);
            this.radPropertyGrid1.Name = "radPropertyGrid1";
            this.radPropertyGrid1.Size = new System.Drawing.Size(304, 254);
            this.radPropertyGrid1.TabIndex = 0;
            this.radPropertyGrid1.ToolbarVisible = true;
            // 
            // radGridView1
            // 
            this.radGridView1.Location = new System.Drawing.Point(3, 36);
            // 
            // 
            // 
            gridViewTextBoxColumn3.FieldName = "Nombre";
            gridViewTextBoxColumn3.HeaderText = "Nombre";
            gridViewTextBoxColumn3.Name = "Nombre";
            gridViewTextBoxColumn3.Width = 120;
            gridViewTextBoxColumn4.FieldName = "Descripcion";
            gridViewTextBoxColumn4.HeaderText = "Descripción";
            gridViewTextBoxColumn4.Name = "Descripcion";
            gridViewTextBoxColumn4.Width = 350;
            this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.radGridView1.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.Size = new System.Drawing.Size(489, 278);
            this.radGridView1.TabIndex = 2;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(498, 36);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(113, 18);
            this.radLabel1.TabIndex = 3;
            this.radLabel1.Text = "Datos de la conexión:";
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(811, 65);
            this.radCommandBar1.TabIndex = 4;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "Herramientas";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarButtonCrearClase,
            this.ToolBarButtonCrearTabla});
            this.ToolBar.Name = "ToolBar";
            // 
            // ToolBarButtonCrearClase
            // 
            this.ToolBarButtonCrearClase.DisplayName = "commandBarButton1";
            this.ToolBarButtonCrearClase.DrawText = true;
            this.ToolBarButtonCrearClase.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_archivo_binario;
            this.ToolBarButtonCrearClase.Name = "ToolBarButtonCrearClase";
            this.ToolBarButtonCrearClase.Text = "Clase";
            this.ToolBarButtonCrearClase.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ToolBarButtonCrearClase.Click += new System.EventHandler(this.ToolBarButtonCrearClase_Click);
            // 
            // ToolBarButtonCrearTabla
            // 
            this.ToolBarButtonCrearTabla.DisplayName = "commandBarButton1";
            this.ToolBarButtonCrearTabla.DrawText = true;
            this.ToolBarButtonCrearTabla.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_archivo_binario;
            this.ToolBarButtonCrearTabla.Name = "ToolBarButtonCrearTabla";
            this.ToolBarButtonCrearTabla.Text = "Crear Tabla";
            this.ToolBarButtonCrearTabla.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ToolBarButtonCrearTabla.Click += new System.EventHandler(this.ToolBarButtonCrearTabla_Click);
            // 
            // ViewMantenimiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 320);
            this.Controls.Add(this.radCommandBar1);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radGridView1);
            this.Controls.Add(this.radPropertyGrid1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewMantenimiento";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.Text = "ViewMantenimiento";
            this.Load += new System.EventHandler(this.ViewMantenimiento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPropertyGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadPropertyGrid radPropertyGrid1;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCrearClase;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCrearTabla;
    }
}
