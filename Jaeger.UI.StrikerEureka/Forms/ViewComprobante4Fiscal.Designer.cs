﻿namespace Jaeger.Views
{
    partial class ViewComprobante4Fiscal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewComprobante4Fiscal));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition7 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition8 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject1 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition9 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition10 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn38 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn39 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn40 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn41 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition11 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn42 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn43 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition12 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn44 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn45 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn46 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition13 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn47 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn48 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition14 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn49 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn50 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition15 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn51 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn52 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn53 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn54 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn55 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn56 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn57 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn58 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn59 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn60 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn61 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn62 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn63 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn64 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition16 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn65 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn66 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn67 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn68 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn69 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn70 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn1 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn71 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn2 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn72 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn73 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn74 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn75 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn76 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition17 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn77 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn78 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn79 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition18 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn4 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn5 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn80 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn6 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn81 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn82 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition19 = new Telerik.WinControls.UI.TableViewDefinition();
            this.RadCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.CommandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.CommandComprobanteFiscal = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarEmisor = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.Separador = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolLabelStatus = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarStatus = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarNew = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarCopy = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarSave = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarRefresh = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarCreate = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarCancelar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarPdf = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarXml = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarEmail = new Telerik.WinControls.UI.CommandBarButton();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarSeries = new Telerik.WinControls.UI.CommandBarButton();
            this.CommandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolLabelUuid = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarTextBoxIdDocumento = new Telerik.WinControls.UI.CommandBarTextBox();
            this.ToolBarClose = new Telerik.WinControls.UI.CommandBarButton();
            this.PanelGeneral = new Telerik.WinControls.UI.RadPanel();
            this.ComprobanteConcepto = new Telerik.WinControls.UI.RadPageView();
            this.PageViewComprobante = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.Presicion = new Telerik.WinControls.UI.RadSpinEditor();
            this.RadLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.FechaEmisionField = new Telerik.WinControls.UI.RadDateTimePicker();
            this.FechaCertifica = new Telerik.WinControls.UI.RadDateTimePicker();
            this.RadLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.TipoComprobante = new Telerik.WinControls.UI.RadSplitButton();
            this.TxbFolio = new Telerik.WinControls.UI.RadTextBox();
            this.CboSerie = new Telerik.WinControls.UI.RadDropDownList();
            this.CboDocumento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CboMoneda = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.TxbTipoCambio = new Telerik.WinControls.UI.RadTextBox();
            this.CboMetodoPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.TxbCuentaPago = new Telerik.WinControls.UI.RadTextBox();
            this.CboFormaPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CboCondiciones = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CboResidenciaFiscal = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.TxbNumRegIdTrib = new Telerik.WinControls.UI.RadTextBox();
            this.CboUsoCfdi = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.TxbReceptorRFC = new Telerik.WinControls.UI.RadTextBox();
            this.CboReceptor = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.RadLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.TxbLugarDeExpedicion = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.PageViewConcepto = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridConceptos = new Telerik.WinControls.UI.RadGridView();
            this.RadCommandBar4 = new Telerik.WinControls.UI.RadCommandBar();
            this.CboConceptos = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CommandBarRowElement4 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.CommandConceptos = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabelProducto = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarHostConceptos = new Telerik.WinControls.UI.CommandBarHostItem();
            this.ToolBarConceptoAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarConceptoQuitar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarConceptoButtonNuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarConceptoBuscar = new Telerik.WinControls.UI.CommandBarButton();
            this.PageViewCfdiRelacionado = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridCfdiRelacionados = new Telerik.WinControls.UI.RadGridView();
            this.ChkCfdiRelacionadoIncluir = new Telerik.WinControls.UI.RadCheckBox();
            this.RadCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.CommandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.CommandCfdiRelacionado = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarHostItemIncluir = new Telerik.WinControls.UI.CommandBarHostItem();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.CommandBarLabel1 = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarCfdiTipoRelacion = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.CommandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarCfdiBuscar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarCfdiAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarCfdiQuitar = new Telerik.WinControls.UI.CommandBarButton();
            this.PanelPago = new Telerik.WinControls.UI.RadPanel();
            this.TxbMonto = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.ChkAutoSuma = new Telerik.WinControls.UI.RadCheckBox();
            this.ButtonSPEI = new Telerik.WinControls.UI.RadButton();
            this.CboFormaPagoP = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CboNomBancoOrdExt = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.RadLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.TxbSelloPago = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.TxbCadenaPago = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.TxbCertificadoPago = new Telerik.WinControls.UI.RadTextBox();
            this.CboTipoCadenaPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.RadLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.TxbCtaBeneficiario = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.TxbRfcCtaBeneficiario = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel25 = new Telerik.WinControls.UI.RadLabel();
            this.TxbCtaOrdenante = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel26 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel27 = new Telerik.WinControls.UI.RadLabel();
            this.TxbRfcEmisorCtaOrigen = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel28 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel29 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.TxbNumOperacion = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel31 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel32 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel33 = new Telerik.WinControls.UI.RadLabel();
            this.TxbTipoCambioDR = new Telerik.WinControls.UI.RadTextBox();
            this.CboMonedaP = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.FechaPago = new Telerik.WinControls.UI.RadDateTimePicker();
            this.CboPagoDoctosRelacionados = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Contenedor = new Telerik.WinControls.UI.RadSplitContainer();
            this.PanelDoctosRealcionados = new Telerik.WinControls.UI.SplitPanel();
            this.GridDoctosRelacionados = new Telerik.WinControls.UI.RadGridView();
            this.radCommandBar3 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarDoctosRelacionados = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarDoctosLabel = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarDoctosHostComprobantes = new Telerik.WinControls.UI.CommandBarHostItem();
            this.ToolBarDoctosButtonAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarDoctosButtonQuitar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarDoctosSeparador1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarDoctosLabelContador = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarDoctosContador = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarDoctosSeparador2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarDoctosImportesPagadosLabel = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarDoctosImportePagado = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarDoctosSeparador3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarDoctosHostAutoSuma = new Telerik.WinControls.UI.CommandBarHostItem();
            this.PanelImpuestos = new Telerik.WinControls.UI.SplitPanel();
            this.radPageView2 = new Telerik.WinControls.UI.RadPageView();
            this.PageImpuestosTralados = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridImpuestosTraslados = new Telerik.WinControls.UI.RadGridView();
            this.radCommandBar5 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement5 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarImpuestosTraslados = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarImpuestosTrasladoBuscar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarImpuestosTrasladoAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarImpuestosTrasladoQuitar = new Telerik.WinControls.UI.CommandBarButton();
            this.PageImpuestosRetenidos = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridImpuestosRetenidos = new Telerik.WinControls.UI.RadGridView();
            this.radCommandBar6 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement6 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarImpuestosRetenidos = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarImpuestosRetenidoBuscar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarImpuestosRetenidoAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarImpuestosRetenidoQuitar = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelGeneral)).BeginInit();
            this.PanelGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComprobanteConcepto)).BeginInit();
            this.ComprobanteConcepto.SuspendLayout();
            this.PageViewComprobante.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Presicion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmisionField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaCertifica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoComprobante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbFolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboSerie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDocumento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDocumento.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMoneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMoneda.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMoneda.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMetodoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMetodoPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMetodoPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCuentaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCondiciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCondiciones.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCondiciones.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumRegIdTrib)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCfdi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCfdi.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCfdi.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbReceptorRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbLugarDeExpedicion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel16)).BeginInit();
            this.PageViewConcepto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar4)).BeginInit();
            this.RadCommandBar4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CboConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboConceptos.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboConceptos.EditorControl.MasterTemplate)).BeginInit();
            this.PageViewCfdiRelacionado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCfdiRelacionados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridCfdiRelacionados.MasterTemplate)).BeginInit();
            this.GridCfdiRelacionados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCfdiRelacionadoIncluir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelPago)).BeginInit();
            this.PanelPago.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxbMonto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAutoSuma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonSPEI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPagoP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPagoP.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPagoP.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboNomBancoOrdExt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboNomBancoOrdExt.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboNomBancoOrdExt.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSelloPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCadenaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCertificadoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoCadenaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoCadenaPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoCadenaPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCtaBeneficiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRfcCtaBeneficiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCtaOrdenante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRfcEmisorCtaOrigen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumOperacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTipoCambioDR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMonedaP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMonedaP.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMonedaP.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboPagoDoctosRelacionados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboPagoDoctosRelacionados.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboPagoDoctosRelacionados.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Contenedor)).BeginInit();
            this.Contenedor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelDoctosRealcionados)).BeginInit();
            this.PanelDoctosRealcionados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDoctosRelacionados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDoctosRelacionados.MasterTemplate)).BeginInit();
            this.GridDoctosRelacionados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelImpuestos)).BeginInit();
            this.PanelImpuestos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView2)).BeginInit();
            this.radPageView2.SuspendLayout();
            this.PageImpuestosTralados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridImpuestosTraslados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridImpuestosTraslados.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar5)).BeginInit();
            this.PageImpuestosRetenidos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridImpuestosRetenidos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridImpuestosRetenidos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadCommandBar1
            // 
            this.RadCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar1.Name = "RadCommandBar1";
            this.RadCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement1});
            this.RadCommandBar1.Size = new System.Drawing.Size(1430, 55);
            this.RadCommandBar1.TabIndex = 0;
            // 
            // CommandBarRowElement1
            // 
            this.CommandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement1.Name = "CommandBarRowElement1";
            this.CommandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.CommandComprobanteFiscal});
            this.CommandBarRowElement1.Text = "";
            // 
            // CommandComprobanteFiscal
            // 
            this.CommandComprobanteFiscal.DisplayName = "Emision de Comprobante";
            this.CommandComprobanteFiscal.EnableFocusBorder = false;
            this.CommandComprobanteFiscal.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarEmisor,
            this.Separador,
            this.ToolLabelStatus,
            this.ToolBarStatus,
            this.ToolBarNew,
            this.ToolBarCopy,
            this.ToolBarSave,
            this.ToolBarRefresh,
            this.ToolBarCreate,
            this.ToolBarCancelar,
            this.ToolBarPdf,
            this.ToolBarXml,
            this.ToolBarEmail,
            this.Separator2,
            this.ToolBarSeries,
            this.CommandBarSeparator2,
            this.ToolLabelUuid,
            this.ToolBarTextBoxIdDocumento,
            this.ToolBarClose});
            this.CommandComprobanteFiscal.Name = "CommandComprobanteFiscal";
            // 
            // 
            // 
            this.CommandComprobanteFiscal.OverflowButton.Enabled = true;
            this.CommandComprobanteFiscal.ShowHorizontalLine = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.CommandComprobanteFiscal.GetChildAt(2))).Enabled = true;
            // 
            // ToolBarEmisor
            // 
            this.ToolBarEmisor.DefaultItem = null;
            this.ToolBarEmisor.DisplayName = "Emisor";
            this.ToolBarEmisor.DrawText = true;
            this.ToolBarEmisor.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_acercadelusuario;
            this.ToolBarEmisor.Name = "ToolBarEmisor";
            this.ToolBarEmisor.Text = "Emisor";
            this.ToolBarEmisor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separador
            // 
            this.Separador.DisplayName = "Separador 2";
            this.Separador.Name = "Separador";
            this.Separador.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separador.VisibleInOverflowMenu = false;
            this.Separador.VisibleInStrip = false;
            // 
            // ToolLabelStatus
            // 
            this.ToolLabelStatus.DisplayName = "Etiqueta Status";
            this.ToolLabelStatus.Name = "ToolLabelStatus";
            this.ToolLabelStatus.Text = "Status:";
            // 
            // ToolBarStatus
            // 
            this.ToolBarStatus.DefaultItem = null;
            this.ToolBarStatus.DisplayName = "Estado";
            this.ToolBarStatus.DrawImage = false;
            this.ToolBarStatus.DrawText = true;
            this.ToolBarStatus.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarStatus.Image")));
            this.ToolBarStatus.MinSize = new System.Drawing.Size(70, 26);
            this.ToolBarStatus.Name = "ToolBarStatus";
            this.ToolBarStatus.Text = "EnEspera";
            // 
            // ToolBarNew
            // 
            this.ToolBarNew.DisplayName = "Nuevo";
            this.ToolBarNew.DrawText = true;
            this.ToolBarNew.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_new_file;
            this.ToolBarNew.Name = "ToolBarNew";
            this.ToolBarNew.Text = "Nuevo";
            this.ToolBarNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarNew.Click += new System.EventHandler(this.ToolBarNew_Click);
            // 
            // ToolBarCopy
            // 
            this.ToolBarCopy.DisplayName = "Duplicar";
            this.ToolBarCopy.DrawText = true;
            this.ToolBarCopy.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_copiar;
            this.ToolBarCopy.Name = "ToolBarCopy";
            this.ToolBarCopy.Text = "Duplicar";
            this.ToolBarCopy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarCopy.Click += new System.EventHandler(this.ToolBarCopy_Click);
            // 
            // ToolBarSave
            // 
            this.ToolBarSave.DisplayName = "Guardar";
            this.ToolBarSave.DrawText = true;
            this.ToolBarSave.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_guardar_todo;
            this.ToolBarSave.Name = "ToolBarSave";
            this.ToolBarSave.Text = "Guardar";
            this.ToolBarSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarSave.Click += new System.EventHandler(this.ToolBarSave_Click);
            // 
            // ToolBarRefresh
            // 
            this.ToolBarRefresh.DisplayName = "Actualizar";
            this.ToolBarRefresh.DrawText = true;
            this.ToolBarRefresh.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_actualizar;
            this.ToolBarRefresh.Name = "ToolBarRefresh";
            this.ToolBarRefresh.Text = "Actualizar";
            this.ToolBarRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarRefresh.Click += new System.EventHandler(this.ToolBarRefresh_Click);
            // 
            // ToolBarCreate
            // 
            this.ToolBarCreate.DisplayName = "Timbrar";
            this.ToolBarCreate.DrawText = true;
            this.ToolBarCreate.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_aprobado;
            this.ToolBarCreate.Name = "ToolBarCreate";
            this.ToolBarCreate.Text = "Timbrar";
            this.ToolBarCreate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarCreate.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ToolBarCreate.Click += new System.EventHandler(this.ToolBarCreate_Click);
            // 
            // ToolBarCancelar
            // 
            this.ToolBarCancelar.DisplayName = "Cancelar";
            this.ToolBarCancelar.DrawText = true;
            this.ToolBarCancelar.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarCancelar.Image")));
            this.ToolBarCancelar.Name = "ToolBarCancelar";
            this.ToolBarCancelar.Text = "Cancelar";
            this.ToolBarCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarCancelar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ToolBarCancelar.Click += new System.EventHandler(this.ToolBarCancelar_Click);
            // 
            // ToolBarPdf
            // 
            this.ToolBarPdf.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarPdf.DisplayName = "Archivo PDF";
            this.ToolBarPdf.DrawText = true;
            this.ToolBarPdf.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_pdf;
            this.ToolBarPdf.Name = "ToolBarPdf";
            this.ToolBarPdf.Text = "PDF";
            this.ToolBarPdf.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarPdf.Click += new System.EventHandler(this.ToolBarPdf_Click);
            // 
            // ToolBarXml
            // 
            this.ToolBarXml.DisplayName = "Archivo XML";
            this.ToolBarXml.DrawText = true;
            this.ToolBarXml.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_open_document;
            this.ToolBarXml.Name = "ToolBarXml";
            this.ToolBarXml.Text = "XML";
            this.ToolBarXml.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarXml.Click += new System.EventHandler(this.ToolBarXml_Click);
            // 
            // ToolBarEmail
            // 
            this.ToolBarEmail.DisplayName = "Correo";
            this.ToolBarEmail.DrawText = true;
            this.ToolBarEmail.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_email_document;
            this.ToolBarEmail.Name = "ToolBarEmail";
            this.ToolBarEmail.Text = "Envíar";
            this.ToolBarEmail.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separator2
            // 
            this.Separator2.DisplayName = "CommandBarSeparator2";
            this.Separator2.Name = "Separator2";
            this.Separator2.VisibleInOverflowMenu = false;
            this.Separator2.VisibleInStrip = false;
            // 
            // ToolBarSeries
            // 
            this.ToolBarSeries.DisplayName = "Series y Folios";
            this.ToolBarSeries.DrawText = true;
            this.ToolBarSeries.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_concept;
            this.ToolBarSeries.Name = "ToolBarSeries";
            this.ToolBarSeries.Text = "Series y Folios";
            this.ToolBarSeries.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // CommandBarSeparator2
            // 
            this.CommandBarSeparator2.DisplayName = "CommandBarSeparator2";
            this.CommandBarSeparator2.Name = "CommandBarSeparator2";
            this.CommandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // ToolLabelUuid
            // 
            this.ToolLabelUuid.DisplayName = "UUID";
            this.ToolLabelUuid.Name = "ToolLabelUuid";
            this.ToolLabelUuid.Text = "UUID:";
            this.ToolLabelUuid.VisibleInOverflowMenu = false;
            // 
            // ToolBarTextBoxIdDocumento
            // 
            this.ToolBarTextBoxIdDocumento.DisplayName = "IdDocumento";
            this.ToolBarTextBoxIdDocumento.MinSize = new System.Drawing.Size(240, 22);
            this.ToolBarTextBoxIdDocumento.Name = "ToolBarTextBoxIdDocumento";
            this.ToolBarTextBoxIdDocumento.Text = "";
            // 
            // ToolBarClose
            // 
            this.ToolBarClose.DisplayName = "Cerrar";
            this.ToolBarClose.DrawText = true;
            this.ToolBarClose.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_cerrar;
            this.ToolBarClose.Name = "ToolBarClose";
            this.ToolBarClose.Text = "Cerrar";
            this.ToolBarClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarClose.Click += new System.EventHandler(this.ToolBarClose_Click);
            // 
            // PanelGeneral
            // 
            this.PanelGeneral.Controls.Add(this.ComprobanteConcepto);
            this.PanelGeneral.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelGeneral.Location = new System.Drawing.Point(0, 55);
            this.PanelGeneral.Name = "PanelGeneral";
            this.PanelGeneral.Size = new System.Drawing.Size(1430, 170);
            this.PanelGeneral.TabIndex = 168;
            // 
            // ComprobanteConcepto
            // 
            this.ComprobanteConcepto.Controls.Add(this.PageViewComprobante);
            this.ComprobanteConcepto.Controls.Add(this.PageViewConcepto);
            this.ComprobanteConcepto.Controls.Add(this.PageViewCfdiRelacionado);
            this.ComprobanteConcepto.DefaultPage = this.PageViewComprobante;
            this.ComprobanteConcepto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComprobanteConcepto.Location = new System.Drawing.Point(0, 0);
            this.ComprobanteConcepto.Name = "ComprobanteConcepto";
            this.ComprobanteConcepto.SelectedPage = this.PageViewComprobante;
            this.ComprobanteConcepto.Size = new System.Drawing.Size(1430, 170);
            this.ComprobanteConcepto.TabIndex = 0;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.ComprobanteConcepto.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // PageViewComprobante
            // 
            this.PageViewComprobante.Controls.Add(this.radPanel1);
            this.PageViewComprobante.ItemSize = new System.Drawing.SizeF(85F, 28F);
            this.PageViewComprobante.Location = new System.Drawing.Point(10, 37);
            this.PageViewComprobante.Name = "PageViewComprobante";
            this.PageViewComprobante.Size = new System.Drawing.Size(1409, 122);
            this.PageViewComprobante.Text = "Comprobante";
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.Presicion);
            this.radPanel1.Controls.Add(this.RadLabel9);
            this.radPanel1.Controls.Add(this.FechaEmisionField);
            this.radPanel1.Controls.Add(this.FechaCertifica);
            this.radPanel1.Controls.Add(this.RadLabel1);
            this.radPanel1.Controls.Add(this.RadLabel2);
            this.radPanel1.Controls.Add(this.RadLabel3);
            this.radPanel1.Controls.Add(this.TipoComprobante);
            this.radPanel1.Controls.Add(this.TxbFolio);
            this.radPanel1.Controls.Add(this.CboSerie);
            this.radPanel1.Controls.Add(this.CboDocumento);
            this.radPanel1.Controls.Add(this.CboMoneda);
            this.radPanel1.Controls.Add(this.TxbTipoCambio);
            this.radPanel1.Controls.Add(this.CboMetodoPago);
            this.radPanel1.Controls.Add(this.TxbCuentaPago);
            this.radPanel1.Controls.Add(this.CboFormaPago);
            this.radPanel1.Controls.Add(this.CboCondiciones);
            this.radPanel1.Controls.Add(this.CboResidenciaFiscal);
            this.radPanel1.Controls.Add(this.TxbNumRegIdTrib);
            this.radPanel1.Controls.Add(this.CboUsoCfdi);
            this.radPanel1.Controls.Add(this.TxbReceptorRFC);
            this.radPanel1.Controls.Add(this.CboReceptor);
            this.radPanel1.Controls.Add(this.RadLabel7);
            this.radPanel1.Controls.Add(this.RadLabel8);
            this.radPanel1.Controls.Add(this.RadLabel10);
            this.radPanel1.Controls.Add(this.RadLabel21);
            this.radPanel1.Controls.Add(this.RadLabel11);
            this.radPanel1.Controls.Add(this.TxbLugarDeExpedicion);
            this.radPanel1.Controls.Add(this.RadLabel12);
            this.radPanel1.Controls.Add(this.RadLabel20);
            this.radPanel1.Controls.Add(this.RadLabel13);
            this.radPanel1.Controls.Add(this.RadLabel19);
            this.radPanel1.Controls.Add(this.RadLabel14);
            this.radPanel1.Controls.Add(this.RadLabel18);
            this.radPanel1.Controls.Add(this.RadLabel15);
            this.radPanel1.Controls.Add(this.RadLabel17);
            this.radPanel1.Controls.Add(this.RadLabel16);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(1409, 122);
            this.radPanel1.TabIndex = 187;
            // 
            // Presicion
            // 
            this.Presicion.Location = new System.Drawing.Point(1195, 88);
            this.Presicion.Name = "Presicion";
            this.Presicion.Size = new System.Drawing.Size(48, 20);
            this.Presicion.TabIndex = 186;
            this.Presicion.TabStop = false;
            this.Presicion.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Presicion.Visible = false;
            // 
            // RadLabel9
            // 
            this.RadLabel9.Location = new System.Drawing.Point(12, 12);
            this.RadLabel9.Name = "RadLabel9";
            this.RadLabel9.Size = new System.Drawing.Size(67, 18);
            this.RadLabel9.TabIndex = 152;
            this.RadLabel9.Text = "Documento:";
            // 
            // FechaEmisionField
            // 
            this.FechaEmisionField.CustomFormat = "dd/MMMM/yyyy";
            this.FechaEmisionField.Location = new System.Drawing.Point(739, 11);
            this.FechaEmisionField.Name = "FechaEmisionField";
            this.FechaEmisionField.Size = new System.Drawing.Size(172, 20);
            this.FechaEmisionField.TabIndex = 3;
            this.FechaEmisionField.TabStop = false;
            this.FechaEmisionField.Text = "miércoles, 27 de septiembre de 2017";
            this.FechaEmisionField.Value = new System.DateTime(2017, 9, 27, 19, 9, 13, 971);
            // 
            // FechaCertifica
            // 
            this.FechaCertifica.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaCertifica.Location = new System.Drawing.Point(1040, 11);
            this.FechaCertifica.Name = "FechaCertifica";
            this.FechaCertifica.NullText = "--/--/----";
            this.FechaCertifica.ReadOnly = true;
            this.FechaCertifica.Size = new System.Drawing.Size(93, 20);
            this.FechaCertifica.TabIndex = 14;
            this.FechaCertifica.TabStop = false;
            this.FechaCertifica.Text = "05/12/2017";
            this.FechaCertifica.Value = new System.DateTime(2017, 12, 5, 22, 46, 27, 169);
            // 
            // RadLabel1
            // 
            this.RadLabel1.Location = new System.Drawing.Point(648, 12);
            this.RadLabel1.Name = "RadLabel1";
            this.RadLabel1.Size = new System.Drawing.Size(85, 18);
            this.RadLabel1.TabIndex = 134;
            this.RadLabel1.Text = "Fec. de Emisión:";
            // 
            // RadLabel2
            // 
            this.RadLabel2.Location = new System.Drawing.Point(931, 11);
            this.RadLabel2.Name = "RadLabel2";
            this.RadLabel2.Size = new System.Drawing.Size(108, 18);
            this.RadLabel2.TabIndex = 185;
            this.RadLabel2.Text = "Fec. de Certificación:";
            // 
            // RadLabel3
            // 
            this.RadLabel3.Location = new System.Drawing.Point(931, 35);
            this.RadLabel3.Name = "RadLabel3";
            this.RadLabel3.Size = new System.Drawing.Size(103, 18);
            this.RadLabel3.TabIndex = 135;
            this.RadLabel3.Text = "Tipo Comprobante:";
            this.RadLabel3.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // TipoComprobante
            // 
            this.TipoComprobante.Location = new System.Drawing.Point(1040, 36);
            this.TipoComprobante.Name = "TipoComprobante";
            this.TipoComprobante.Size = new System.Drawing.Size(93, 19);
            this.TipoComprobante.TabIndex = 15;
            this.TipoComprobante.Text = "-----";
            // 
            // TxbFolio
            // 
            this.TxbFolio.Location = new System.Drawing.Point(498, 11);
            this.TxbFolio.Name = "TxbFolio";
            this.TxbFolio.NullText = "Folio";
            this.TxbFolio.Size = new System.Drawing.Size(135, 20);
            this.TxbFolio.TabIndex = 2;
            this.TxbFolio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxbFolio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbFolio_KeyPress);
            // 
            // CboSerie
            // 
            this.CboSerie.Location = new System.Drawing.Point(314, 11);
            this.CboSerie.Name = "CboSerie";
            this.CboSerie.NullText = "Serie";
            this.CboSerie.Size = new System.Drawing.Size(125, 20);
            this.CboSerie.TabIndex = 1;
            // 
            // CboDocumento
            // 
            // 
            // CboDocumento.NestedRadGridView
            // 
            this.CboDocumento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboDocumento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboDocumento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboDocumento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboDocumento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboDocumento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboDocumento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "TipoDeComprobante";
            gridViewTextBoxColumn1.HeaderText = "Comprobante";
            gridViewTextBoxColumn1.Name = "TipoDeComprobante";
            gridViewTextBoxColumn2.FieldName = "Nombre";
            gridViewTextBoxColumn2.HeaderText = "Nombre";
            gridViewTextBoxColumn2.Name = "Nombre";
            gridViewTextBoxColumn3.FieldName = "Serie";
            gridViewTextBoxColumn3.HeaderText = "Serie";
            gridViewTextBoxColumn3.Name = "Serie";
            gridViewTextBoxColumn4.FieldName = "Folio";
            gridViewTextBoxColumn4.HeaderText = "Folio";
            gridViewTextBoxColumn4.Name = "Folio";
            gridViewTextBoxColumn5.FieldName = "Template";
            gridViewTextBoxColumn5.HeaderText = "Template";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "Template";
            this.CboDocumento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.CboDocumento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboDocumento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboDocumento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.CboDocumento.EditorControl.Name = "NestedRadGridView";
            this.CboDocumento.EditorControl.ReadOnly = true;
            this.CboDocumento.EditorControl.ShowGroupPanel = false;
            this.CboDocumento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboDocumento.EditorControl.TabIndex = 0;
            this.CboDocumento.Location = new System.Drawing.Point(84, 11);
            this.CboDocumento.Name = "CboDocumento";
            this.CboDocumento.NullText = "Documento";
            this.CboDocumento.Size = new System.Drawing.Size(164, 20);
            this.CboDocumento.TabIndex = 0;
            this.CboDocumento.TabStop = false;
            // 
            // CboMoneda
            // 
            // 
            // CboMoneda.NestedRadGridView
            // 
            this.CboMoneda.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboMoneda.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboMoneda.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboMoneda.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboMoneda.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboMoneda.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboMoneda.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn6.FieldName = "Clave";
            gridViewTextBoxColumn6.HeaderText = "Clave";
            gridViewTextBoxColumn6.Name = "Clave";
            gridViewTextBoxColumn7.FieldName = "Descripcion";
            gridViewTextBoxColumn7.HeaderText = "Descripción";
            gridViewTextBoxColumn7.Name = "Descripcion";
            this.CboMoneda.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.CboMoneda.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboMoneda.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboMoneda.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.CboMoneda.EditorControl.Name = "NestedRadGridView";
            this.CboMoneda.EditorControl.ReadOnly = true;
            this.CboMoneda.EditorControl.ShowGroupPanel = false;
            this.CboMoneda.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboMoneda.EditorControl.TabIndex = 0;
            this.CboMoneda.Location = new System.Drawing.Point(724, 61);
            this.CboMoneda.Name = "CboMoneda";
            this.CboMoneda.NullText = "Moneda";
            this.CboMoneda.Size = new System.Drawing.Size(187, 20);
            this.CboMoneda.TabIndex = 10;
            this.CboMoneda.TabStop = false;
            // 
            // TxbTipoCambio
            // 
            this.TxbTipoCambio.Location = new System.Drawing.Point(1040, 87);
            this.TxbTipoCambio.Name = "TxbTipoCambio";
            this.TxbTipoCambio.NullText = "Tipo de Cambio";
            this.TxbTipoCambio.Size = new System.Drawing.Size(93, 20);
            this.TxbTipoCambio.TabIndex = 17;
            // 
            // CboMetodoPago
            // 
            // 
            // CboMetodoPago.NestedRadGridView
            // 
            this.CboMetodoPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboMetodoPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboMetodoPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboMetodoPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboMetodoPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboMetodoPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboMetodoPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn8.FieldName = "Clave";
            gridViewTextBoxColumn8.HeaderText = "Clave";
            gridViewTextBoxColumn8.Name = "Clave";
            gridViewTextBoxColumn9.FieldName = "Descripcion";
            gridViewTextBoxColumn9.HeaderText = "Descripción";
            gridViewTextBoxColumn9.Name = "Descripcion";
            this.CboMetodoPago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9});
            this.CboMetodoPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboMetodoPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboMetodoPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.CboMetodoPago.EditorControl.Name = "NestedRadGridView";
            this.CboMetodoPago.EditorControl.ReadOnly = true;
            this.CboMetodoPago.EditorControl.ShowGroupPanel = false;
            this.CboMetodoPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboMetodoPago.EditorControl.TabIndex = 0;
            this.CboMetodoPago.Location = new System.Drawing.Point(110, 88);
            this.CboMetodoPago.Name = "CboMetodoPago";
            this.CboMetodoPago.NullText = "Método de Pago";
            this.CboMetodoPago.Size = new System.Drawing.Size(217, 20);
            this.CboMetodoPago.TabIndex = 11;
            this.CboMetodoPago.TabStop = false;
            // 
            // TxbCuentaPago
            // 
            this.TxbCuentaPago.Enabled = false;
            this.TxbCuentaPago.Location = new System.Drawing.Point(498, 61);
            this.TxbCuentaPago.Name = "TxbCuentaPago";
            this.TxbCuentaPago.NullText = "Cuenta";
            this.TxbCuentaPago.Size = new System.Drawing.Size(135, 20);
            this.TxbCuentaPago.TabIndex = 9;
            // 
            // CboFormaPago
            // 
            // 
            // CboFormaPago.NestedRadGridView
            // 
            this.CboFormaPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboFormaPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboFormaPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboFormaPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboFormaPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboFormaPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboFormaPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn10.FieldName = "Clave";
            gridViewTextBoxColumn10.HeaderText = "Clave";
            gridViewTextBoxColumn10.Name = "Clave";
            gridViewTextBoxColumn11.FieldName = "Descripcion";
            gridViewTextBoxColumn11.HeaderText = "Descripción";
            gridViewTextBoxColumn11.Name = "Descripcion";
            this.CboFormaPago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11});
            this.CboFormaPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboFormaPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboFormaPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.CboFormaPago.EditorControl.Name = "NestedRadGridView";
            this.CboFormaPago.EditorControl.ReadOnly = true;
            this.CboFormaPago.EditorControl.ShowGroupPanel = false;
            this.CboFormaPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboFormaPago.EditorControl.TabIndex = 0;
            this.CboFormaPago.Location = new System.Drawing.Point(422, 88);
            this.CboFormaPago.Name = "CboFormaPago";
            this.CboFormaPago.NullText = "Forma de pago";
            this.CboFormaPago.Size = new System.Drawing.Size(211, 20);
            this.CboFormaPago.TabIndex = 12;
            this.CboFormaPago.TabStop = false;
            // 
            // CboCondiciones
            // 
            // 
            // CboCondiciones.NestedRadGridView
            // 
            this.CboCondiciones.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboCondiciones.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboCondiciones.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboCondiciones.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboCondiciones.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboCondiciones.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboCondiciones.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.CboCondiciones.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboCondiciones.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboCondiciones.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.CboCondiciones.EditorControl.Name = "NestedRadGridView";
            this.CboCondiciones.EditorControl.ReadOnly = true;
            this.CboCondiciones.EditorControl.ShowGroupPanel = false;
            this.CboCondiciones.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboCondiciones.EditorControl.TabIndex = 0;
            this.CboCondiciones.Location = new System.Drawing.Point(724, 87);
            this.CboCondiciones.Name = "CboCondiciones";
            this.CboCondiciones.NullText = "Condiciones";
            this.CboCondiciones.Size = new System.Drawing.Size(187, 20);
            this.CboCondiciones.TabIndex = 13;
            this.CboCondiciones.TabStop = false;
            // 
            // CboResidenciaFiscal
            // 
            // 
            // CboResidenciaFiscal.NestedRadGridView
            // 
            this.CboResidenciaFiscal.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboResidenciaFiscal.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboResidenciaFiscal.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboResidenciaFiscal.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.CboResidenciaFiscal.EditorControl.Name = "NestedRadGridView";
            this.CboResidenciaFiscal.EditorControl.ReadOnly = true;
            this.CboResidenciaFiscal.EditorControl.ShowGroupPanel = false;
            this.CboResidenciaFiscal.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboResidenciaFiscal.EditorControl.TabIndex = 0;
            this.CboResidenciaFiscal.Location = new System.Drawing.Point(110, 61);
            this.CboResidenciaFiscal.Name = "CboResidenciaFiscal";
            this.CboResidenciaFiscal.NullText = "Residencia Fiscal";
            this.CboResidenciaFiscal.Size = new System.Drawing.Size(125, 20);
            this.CboResidenciaFiscal.TabIndex = 7;
            this.CboResidenciaFiscal.TabStop = false;
            // 
            // TxbNumRegIdTrib
            // 
            this.TxbNumRegIdTrib.Location = new System.Drawing.Point(332, 61);
            this.TxbNumRegIdTrib.Name = "TxbNumRegIdTrib";
            this.TxbNumRegIdTrib.NullText = "Núm. Registro Trib.";
            this.TxbNumRegIdTrib.Size = new System.Drawing.Size(107, 20);
            this.TxbNumRegIdTrib.TabIndex = 8;
            // 
            // CboUsoCfdi
            // 
            // 
            // CboUsoCfdi.NestedRadGridView
            // 
            this.CboUsoCfdi.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboUsoCfdi.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboUsoCfdi.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboUsoCfdi.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboUsoCfdi.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboUsoCfdi.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboUsoCfdi.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn12.FieldName = "Clave";
            gridViewTextBoxColumn12.HeaderText = "Clave";
            gridViewTextBoxColumn12.Name = "Clave";
            gridViewTextBoxColumn13.FieldName = "Descripcion";
            gridViewTextBoxColumn13.HeaderText = "Descripción";
            gridViewTextBoxColumn13.Name = "Descripcion";
            this.CboUsoCfdi.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13});
            this.CboUsoCfdi.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboUsoCfdi.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboUsoCfdi.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition7;
            this.CboUsoCfdi.EditorControl.Name = "NestedRadGridView";
            this.CboUsoCfdi.EditorControl.ReadOnly = true;
            this.CboUsoCfdi.EditorControl.ShowGroupPanel = false;
            this.CboUsoCfdi.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboUsoCfdi.EditorControl.TabIndex = 0;
            this.CboUsoCfdi.Location = new System.Drawing.Point(724, 36);
            this.CboUsoCfdi.Name = "CboUsoCfdi";
            this.CboUsoCfdi.NullText = "Uso de CFDI";
            this.CboUsoCfdi.Size = new System.Drawing.Size(187, 20);
            this.CboUsoCfdi.TabIndex = 6;
            this.CboUsoCfdi.TabStop = false;
            // 
            // TxbReceptorRFC
            // 
            this.TxbReceptorRFC.Location = new System.Drawing.Point(498, 36);
            this.TxbReceptorRFC.Name = "TxbReceptorRFC";
            this.TxbReceptorRFC.NullText = "Registro Federal";
            this.TxbReceptorRFC.ReadOnly = true;
            this.TxbReceptorRFC.Size = new System.Drawing.Size(135, 20);
            this.TxbReceptorRFC.TabIndex = 5;
            this.TxbReceptorRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CboReceptor
            // 
            this.CboReceptor.AutoSizeDropDownHeight = true;
            // 
            // CboReceptor.NestedRadGridView
            // 
            this.CboReceptor.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboReceptor.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboReceptor.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboReceptor.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboReceptor.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboReceptor.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboReceptor.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn14.FieldName = "Id";
            gridViewTextBoxColumn14.HeaderText = "Id";
            gridViewTextBoxColumn14.IsVisible = false;
            gridViewTextBoxColumn14.Name = "Id";
            gridViewTextBoxColumn15.FieldName = "Nombre";
            gridViewTextBoxColumn15.HeaderText = "Nombre";
            gridViewTextBoxColumn15.Name = "Nombre";
            gridViewTextBoxColumn15.Width = 220;
            gridViewTextBoxColumn16.FieldName = "RFC";
            gridViewTextBoxColumn16.HeaderText = "RFC";
            gridViewTextBoxColumn16.Name = "_drctr_rfc";
            gridViewTextBoxColumn17.FieldName = "ResidenciaFiscal";
            gridViewTextBoxColumn17.HeaderText = "Residencia Fiscal";
            gridViewTextBoxColumn17.IsVisible = false;
            gridViewTextBoxColumn17.Name = "_drctr_resfis";
            gridViewTextBoxColumn18.FieldName = "ClaveUsoCFDI";
            gridViewTextBoxColumn18.HeaderText = "Uso CFDI";
            gridViewTextBoxColumn18.Name = "ClaveUsoCFDI";
            this.CboReceptor.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18});
            this.CboReceptor.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboReceptor.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboReceptor.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition8;
            this.CboReceptor.EditorControl.Name = "NestedRadGridView";
            this.CboReceptor.EditorControl.ReadOnly = true;
            this.CboReceptor.EditorControl.ShowGroupPanel = false;
            this.CboReceptor.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboReceptor.EditorControl.TabIndex = 0;
            this.CboReceptor.Location = new System.Drawing.Point(84, 36);
            this.CboReceptor.Name = "CboReceptor";
            this.CboReceptor.NullText = "Nombre o Razon Social del Receptor";
            this.CboReceptor.Size = new System.Drawing.Size(355, 20);
            this.CboReceptor.TabIndex = 4;
            this.CboReceptor.TabStop = false;
            // 
            // RadLabel7
            // 
            this.RadLabel7.Location = new System.Drawing.Point(450, 12);
            this.RadLabel7.Name = "RadLabel7";
            this.RadLabel7.Size = new System.Drawing.Size(33, 18);
            this.RadLabel7.TabIndex = 150;
            this.RadLabel7.Text = "Folio:";
            // 
            // RadLabel8
            // 
            this.RadLabel8.Location = new System.Drawing.Point(275, 12);
            this.RadLabel8.Name = "RadLabel8";
            this.RadLabel8.Size = new System.Drawing.Size(33, 18);
            this.RadLabel8.TabIndex = 151;
            this.RadLabel8.Text = "Serie:";
            // 
            // RadLabel10
            // 
            this.RadLabel10.Location = new System.Drawing.Point(12, 37);
            this.RadLabel10.Name = "RadLabel10";
            this.RadLabel10.Size = new System.Drawing.Size(54, 18);
            this.RadLabel10.TabIndex = 153;
            this.RadLabel10.Text = "Receptor:";
            // 
            // RadLabel21
            // 
            this.RadLabel21.Location = new System.Drawing.Point(931, 62);
            this.RadLabel21.Name = "RadLabel21";
            this.RadLabel21.Size = new System.Drawing.Size(94, 18);
            this.RadLabel21.TabIndex = 165;
            this.RadLabel21.Text = "Lugar Expedición:";
            // 
            // RadLabel11
            // 
            this.RadLabel11.Location = new System.Drawing.Point(450, 37);
            this.RadLabel11.Name = "RadLabel11";
            this.RadLabel11.Size = new System.Drawing.Size(28, 18);
            this.RadLabel11.TabIndex = 154;
            this.RadLabel11.Text = "RFC:";
            // 
            // TxbLugarDeExpedicion
            // 
            this.TxbLugarDeExpedicion.Location = new System.Drawing.Point(1040, 61);
            this.TxbLugarDeExpedicion.Name = "TxbLugarDeExpedicion";
            this.TxbLugarDeExpedicion.NullText = "Codigo";
            this.TxbLugarDeExpedicion.Size = new System.Drawing.Size(93, 20);
            this.TxbLugarDeExpedicion.TabIndex = 16;
            // 
            // RadLabel12
            // 
            this.RadLabel12.Location = new System.Drawing.Point(648, 37);
            this.RadLabel12.Name = "RadLabel12";
            this.RadLabel12.Size = new System.Drawing.Size(70, 18);
            this.RadLabel12.TabIndex = 155;
            this.RadLabel12.Text = "Uso de CFDI:";
            // 
            // RadLabel20
            // 
            this.RadLabel20.Location = new System.Drawing.Point(931, 87);
            this.RadLabel20.Name = "RadLabel20";
            this.RadLabel20.Size = new System.Drawing.Size(88, 18);
            this.RadLabel20.TabIndex = 163;
            this.RadLabel20.Text = "Tipo de Cambio:";
            // 
            // RadLabel13
            // 
            this.RadLabel13.Location = new System.Drawing.Point(12, 62);
            this.RadLabel13.Name = "RadLabel13";
            this.RadLabel13.Size = new System.Drawing.Size(92, 18);
            this.RadLabel13.TabIndex = 156;
            this.RadLabel13.Text = "Residencia Fiscal:";
            // 
            // RadLabel19
            // 
            this.RadLabel19.Location = new System.Drawing.Point(648, 62);
            this.RadLabel19.Name = "RadLabel19";
            this.RadLabel19.Size = new System.Drawing.Size(50, 18);
            this.RadLabel19.TabIndex = 162;
            this.RadLabel19.Text = "Moneda:";
            // 
            // RadLabel14
            // 
            this.RadLabel14.Location = new System.Drawing.Point(242, 62);
            this.RadLabel14.Name = "RadLabel14";
            this.RadLabel14.Size = new System.Drawing.Size(85, 18);
            this.RadLabel14.TabIndex = 157;
            this.RadLabel14.Text = "Núm. Reg. Trib.:";
            // 
            // RadLabel18
            // 
            this.RadLabel18.Location = new System.Drawing.Point(450, 62);
            this.RadLabel18.Name = "RadLabel18";
            this.RadLabel18.Size = new System.Drawing.Size(44, 18);
            this.RadLabel18.TabIndex = 161;
            this.RadLabel18.Text = "Cuenta:";
            // 
            // RadLabel15
            // 
            this.RadLabel15.Location = new System.Drawing.Point(11, 90);
            this.RadLabel15.Name = "RadLabel15";
            this.RadLabel15.Size = new System.Drawing.Size(93, 18);
            this.RadLabel15.TabIndex = 158;
            this.RadLabel15.Text = "Método de Pago:";
            // 
            // RadLabel17
            // 
            this.RadLabel17.Location = new System.Drawing.Point(648, 87);
            this.RadLabel17.Name = "RadLabel17";
            this.RadLabel17.Size = new System.Drawing.Size(70, 18);
            this.RadLabel17.TabIndex = 160;
            this.RadLabel17.Text = "Condiciones:";
            // 
            // RadLabel16
            // 
            this.RadLabel16.Location = new System.Drawing.Point(332, 89);
            this.RadLabel16.Name = "RadLabel16";
            this.RadLabel16.Size = new System.Drawing.Size(84, 18);
            this.RadLabel16.TabIndex = 159;
            this.RadLabel16.Text = "Forma de Pago:";
            // 
            // PageViewConcepto
            // 
            this.PageViewConcepto.Controls.Add(this.GridConceptos);
            this.PageViewConcepto.Controls.Add(this.RadCommandBar4);
            this.PageViewConcepto.ItemSize = new System.Drawing.SizeF(69F, 28F);
            this.PageViewConcepto.Location = new System.Drawing.Point(10, 37);
            this.PageViewConcepto.Name = "PageViewConcepto";
            this.PageViewConcepto.Size = new System.Drawing.Size(1409, 122);
            this.PageViewConcepto.Text = "Conceptos";
            // 
            // GridConceptos
            // 
            this.GridConceptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridConceptos.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.GridConceptos.MasterTemplate.AllowAddNewRow = false;
            this.GridConceptos.MasterTemplate.AllowDeleteRow = false;
            gridViewTextBoxColumn19.DataType = typeof(int);
            gridViewTextBoxColumn19.FieldName = "Id";
            gridViewTextBoxColumn19.HeaderText = "Id";
            gridViewTextBoxColumn19.IsVisible = false;
            gridViewTextBoxColumn19.Name = "Id";
            gridViewTextBoxColumn19.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn19.VisibleInColumnChooser = false;
            gridViewTextBoxColumn20.DataType = typeof(int);
            gridViewTextBoxColumn20.FieldName = "SubId";
            gridViewTextBoxColumn20.HeaderText = "SubId";
            gridViewTextBoxColumn20.IsVisible = false;
            gridViewTextBoxColumn20.Name = "SubId";
            gridViewTextBoxColumn20.VisibleInColumnChooser = false;
            conditionalFormattingObject1.ApplyToRow = true;
            conditionalFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.CellFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            conditionalFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.Name = "Activo";
            conditionalFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            conditionalFormattingObject1.RowForeColor = System.Drawing.Color.Gray;
            conditionalFormattingObject1.TValue1 = "False";
            gridViewCheckBoxColumn1.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            gridViewCheckBoxColumn1.FieldName = "IsActive";
            gridViewCheckBoxColumn1.HeaderText = "Activo";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "IsActive";
            gridViewTextBoxColumn21.FieldName = "NumPedido";
            gridViewTextBoxColumn21.HeaderText = "# Pedido";
            gridViewTextBoxColumn21.Name = "NumPedido";
            gridViewTextBoxColumn21.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn21.Width = 60;
            gridViewTextBoxColumn22.DataType = typeof(decimal);
            gridViewTextBoxColumn22.FieldName = "Cantidad";
            gridViewTextBoxColumn22.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn22.HeaderText = "Cantidad";
            gridViewTextBoxColumn22.Name = "Cantidad";
            gridViewTextBoxColumn22.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn22.Width = 80;
            gridViewTextBoxColumn23.FieldName = "Unidad";
            gridViewTextBoxColumn23.HeaderText = "Unidad";
            gridViewTextBoxColumn23.Name = "Uniadd";
            gridViewTextBoxColumn23.Width = 80;
            gridViewTextBoxColumn24.FieldName = "ClaveUnidad";
            gridViewTextBoxColumn24.HeaderText = "Clv. Unidad";
            gridViewTextBoxColumn24.Name = "ClaveUnidad";
            gridViewTextBoxColumn24.Width = 80;
            gridViewTextBoxColumn25.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn25.HeaderText = "No. Ident.";
            gridViewTextBoxColumn25.Name = "NoIdentificacion";
            gridViewTextBoxColumn25.Width = 90;
            gridViewTextBoxColumn26.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn26.HeaderText = "Clv. Producto";
            gridViewTextBoxColumn26.Name = "ClaveProducto";
            gridViewTextBoxColumn26.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn26.Width = 80;
            gridViewTextBoxColumn27.FieldName = "Descripcion";
            gridViewTextBoxColumn27.HeaderText = "Concepto";
            gridViewTextBoxColumn27.Name = "Descripcion";
            gridViewTextBoxColumn27.Width = 325;
            gridViewTextBoxColumn28.DataType = typeof(decimal);
            gridViewTextBoxColumn28.FieldName = "ValorUnitario";
            gridViewTextBoxColumn28.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn28.HeaderText = "Unitario";
            gridViewTextBoxColumn28.Name = "ValorUnitario";
            gridViewTextBoxColumn28.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn28.Width = 80;
            gridViewTextBoxColumn29.DataType = typeof(decimal);
            gridViewTextBoxColumn29.FieldName = "Descuento";
            gridViewTextBoxColumn29.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn29.HeaderText = "Descuento";
            gridViewTextBoxColumn29.Name = "Descuento";
            gridViewTextBoxColumn29.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn29.Width = 80;
            gridViewTextBoxColumn30.FieldName = "CtaPredial";
            gridViewTextBoxColumn30.HeaderText = "Cta. Predial";
            gridViewTextBoxColumn30.Name = "CtaPredial";
            gridViewTextBoxColumn30.Width = 80;
            gridViewTextBoxColumn31.DataType = typeof(decimal);
            gridViewTextBoxColumn31.EnableExpressionEditor = false;
            gridViewTextBoxColumn31.Expression = "";
            gridViewTextBoxColumn31.FieldName = "Importe";
            gridViewTextBoxColumn31.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn31.HeaderText = "Importe";
            gridViewTextBoxColumn31.Name = "Importe";
            gridViewTextBoxColumn31.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn31.Width = 80;
            this.GridConceptos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31});
            this.GridConceptos.MasterTemplate.EnableGrouping = false;
            this.GridConceptos.MasterTemplate.ViewDefinition = tableViewDefinition9;
            this.GridConceptos.Name = "GridConceptos";
            this.GridConceptos.Size = new System.Drawing.Size(1409, 122);
            this.GridConceptos.TabIndex = 109;
            // 
            // RadCommandBar4
            // 
            this.RadCommandBar4.Controls.Add(this.CboConceptos);
            this.RadCommandBar4.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar4.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar4.Name = "RadCommandBar4";
            this.RadCommandBar4.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement4});
            this.RadCommandBar4.Size = new System.Drawing.Size(1409, 0);
            this.RadCommandBar4.TabIndex = 110;
            // 
            // CboConceptos
            // 
            // 
            // CboConceptos.NestedRadGridView
            // 
            this.CboConceptos.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboConceptos.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboConceptos.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboConceptos.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboConceptos.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboConceptos.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboConceptos.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn32.FieldName = "ClaveUnidad";
            gridViewTextBoxColumn32.HeaderText = "Clv. Unidad";
            gridViewTextBoxColumn32.Name = "ClaveUnidad";
            gridViewTextBoxColumn33.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn33.HeaderText = "Clv. Producto";
            gridViewTextBoxColumn33.Name = "ClaveProdServ";
            gridViewTextBoxColumn33.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn33.Width = 85;
            gridViewTextBoxColumn34.FieldName = "Descripcion";
            gridViewTextBoxColumn34.HeaderText = "Descripción";
            gridViewTextBoxColumn34.Name = "Descripcion";
            gridViewTextBoxColumn34.Width = 220;
            this.CboConceptos.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn32,
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34});
            this.CboConceptos.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboConceptos.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboConceptos.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition10;
            this.CboConceptos.EditorControl.Name = "NestedRadGridView";
            this.CboConceptos.EditorControl.ReadOnly = true;
            this.CboConceptos.EditorControl.ShowGroupPanel = false;
            this.CboConceptos.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboConceptos.EditorControl.TabIndex = 0;
            this.CboConceptos.Location = new System.Drawing.Point(943, 7);
            this.CboConceptos.Name = "CboConceptos";
            this.CboConceptos.NullText = "Selecciona";
            this.CboConceptos.Size = new System.Drawing.Size(100, 20);
            this.CboConceptos.TabIndex = 188;
            this.CboConceptos.TabStop = false;
            // 
            // CommandBarRowElement4
            // 
            this.CommandBarRowElement4.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement4.Name = "CommandBarRowElement4";
            this.CommandBarRowElement4.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.CommandConceptos});
            this.CommandBarRowElement4.Text = "";
            // 
            // CommandConceptos
            // 
            this.CommandConceptos.DisplayName = "Barra de Conceptos";
            this.CommandConceptos.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelProducto,
            this.ToolBarHostConceptos,
            this.ToolBarConceptoAgregar,
            this.ToolBarConceptoQuitar,
            this.ToolBarConceptoButtonNuevo,
            this.ToolBarConceptoBuscar});
            this.CommandConceptos.Name = "CommandConceptos";
            // 
            // 
            // 
            this.CommandConceptos.OverflowButton.Enabled = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.CommandConceptos.GetChildAt(2))).Enabled = false;
            // 
            // ToolBarLabelProducto
            // 
            this.ToolBarLabelProducto.DisplayName = "CommandBarLabel2";
            this.ToolBarLabelProducto.Name = "ToolBarLabelProducto";
            this.ToolBarLabelProducto.Text = "Prod. Serv.";
            // 
            // ToolBarHostConceptos
            // 
            this.ToolBarHostConceptos.DisplayName = "commandBarHostItem1";
            this.ToolBarHostConceptos.MinSize = new System.Drawing.Size(360, 0);
            this.ToolBarHostConceptos.Name = "ToolBarHostConceptos";
            this.ToolBarHostConceptos.Text = "";
            // 
            // ToolBarConceptoAgregar
            // 
            this.ToolBarConceptoAgregar.DisplayName = "Agregar";
            this.ToolBarConceptoAgregar.DrawText = true;
            this.ToolBarConceptoAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_agregar_archivo;
            this.ToolBarConceptoAgregar.Name = "ToolBarConceptoAgregar";
            this.ToolBarConceptoAgregar.Text = "Agregar";
            this.ToolBarConceptoAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarConceptoAgregar.Click += new System.EventHandler(this.ToolBarConceptoAgregar_Click);
            // 
            // ToolBarConceptoQuitar
            // 
            this.ToolBarConceptoQuitar.DisplayName = "Quitar";
            this.ToolBarConceptoQuitar.DrawText = true;
            this.ToolBarConceptoQuitar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_delete_file;
            this.ToolBarConceptoQuitar.Name = "ToolBarConceptoQuitar";
            this.ToolBarConceptoQuitar.Text = "Quitar";
            this.ToolBarConceptoQuitar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarConceptoQuitar.Click += new System.EventHandler(this.ToolBarConceptoQuitar_Click);
            // 
            // ToolBarConceptoButtonNuevo
            // 
            this.ToolBarConceptoButtonNuevo.DisplayName = "Nuevo";
            this.ToolBarConceptoButtonNuevo.DrawText = true;
            this.ToolBarConceptoButtonNuevo.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_new_file;
            this.ToolBarConceptoButtonNuevo.Name = "ToolBarConceptoButtonNuevo";
            this.ToolBarConceptoButtonNuevo.Text = "Nuevo";
            this.ToolBarConceptoButtonNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarConceptoButtonNuevo.Click += new System.EventHandler(this.ToolBarConceptoButtonNuevo_Click);
            // 
            // ToolBarConceptoBuscar
            // 
            this.ToolBarConceptoBuscar.DisplayName = "Buscar";
            this.ToolBarConceptoBuscar.DrawText = true;
            this.ToolBarConceptoBuscar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_view_file;
            this.ToolBarConceptoBuscar.Name = "ToolBarConceptoBuscar";
            this.ToolBarConceptoBuscar.Text = "Buscar";
            this.ToolBarConceptoBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarConceptoBuscar.Click += new System.EventHandler(this.ToolBarConceptoBuscar_Click);
            // 
            // PageViewCfdiRelacionado
            // 
            this.PageViewCfdiRelacionado.Controls.Add(this.GridCfdiRelacionados);
            this.PageViewCfdiRelacionado.Controls.Add(this.RadCommandBar2);
            this.PageViewCfdiRelacionado.ItemSize = new System.Drawing.SizeF(104F, 28F);
            this.PageViewCfdiRelacionado.Location = new System.Drawing.Point(10, 37);
            this.PageViewCfdiRelacionado.Name = "PageViewCfdiRelacionado";
            this.PageViewCfdiRelacionado.Size = new System.Drawing.Size(1409, 122);
            this.PageViewCfdiRelacionado.Text = "CFDI Relacionado";
            // 
            // GridCfdiRelacionados
            // 
            this.GridCfdiRelacionados.Controls.Add(this.ChkCfdiRelacionadoIncluir);
            this.GridCfdiRelacionados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridCfdiRelacionados.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.GridCfdiRelacionados.MasterTemplate.AllowAddNewRow = false;
            gridViewTextBoxColumn35.FieldName = "IdDocumento";
            gridViewTextBoxColumn35.HeaderText = "UUID";
            gridViewTextBoxColumn35.Name = "UUID";
            gridViewTextBoxColumn35.Width = 220;
            gridViewTextBoxColumn36.FieldName = "RFC";
            gridViewTextBoxColumn36.HeaderText = "RFC";
            gridViewTextBoxColumn36.Name = "RFC";
            gridViewTextBoxColumn36.Width = 110;
            gridViewTextBoxColumn37.FieldName = "Nombre";
            gridViewTextBoxColumn37.HeaderText = "Identidad Fiscal";
            gridViewTextBoxColumn37.Name = "Nombre";
            gridViewTextBoxColumn37.Width = 250;
            gridViewTextBoxColumn38.FieldName = "Serie";
            gridViewTextBoxColumn38.HeaderText = "Serie";
            gridViewTextBoxColumn38.Name = "Serie";
            gridViewTextBoxColumn38.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn38.Width = 75;
            gridViewTextBoxColumn39.FieldName = "Folio";
            gridViewTextBoxColumn39.HeaderText = "Folio";
            gridViewTextBoxColumn39.Name = "Folio";
            gridViewTextBoxColumn39.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn39.Width = 75;
            gridViewTextBoxColumn40.DataType = typeof(decimal);
            gridViewTextBoxColumn40.FieldName = "Total";
            gridViewTextBoxColumn40.FormatString = "{0:n}";
            gridViewTextBoxColumn40.HeaderText = "Total";
            gridViewTextBoxColumn40.Name = "Total";
            gridViewTextBoxColumn40.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn40.Width = 95;
            gridViewTextBoxColumn41.DataType = typeof(decimal);
            gridViewTextBoxColumn41.FieldName = "ImporteAplicado";
            gridViewTextBoxColumn41.FormatString = "{0:n}";
            gridViewTextBoxColumn41.HeaderText = "Importe";
            gridViewTextBoxColumn41.Name = "ImporteAplicado";
            gridViewTextBoxColumn41.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn41.Width = 95;
            this.GridCfdiRelacionados.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn35,
            gridViewTextBoxColumn36,
            gridViewTextBoxColumn37,
            gridViewTextBoxColumn38,
            gridViewTextBoxColumn39,
            gridViewTextBoxColumn40,
            gridViewTextBoxColumn41});
            this.GridCfdiRelacionados.MasterTemplate.ViewDefinition = tableViewDefinition11;
            this.GridCfdiRelacionados.Name = "GridCfdiRelacionados";
            this.GridCfdiRelacionados.ShowGroupPanel = false;
            this.GridCfdiRelacionados.Size = new System.Drawing.Size(1409, 122);
            this.GridCfdiRelacionados.TabIndex = 1;
            // 
            // ChkCfdiRelacionadoIncluir
            // 
            this.ChkCfdiRelacionadoIncluir.Location = new System.Drawing.Point(1121, 38);
            this.ChkCfdiRelacionadoIncluir.Name = "ChkCfdiRelacionadoIncluir";
            this.ChkCfdiRelacionadoIncluir.Size = new System.Drawing.Size(51, 18);
            this.ChkCfdiRelacionadoIncluir.TabIndex = 187;
            this.ChkCfdiRelacionadoIncluir.Text = "Incluir";
            this.ChkCfdiRelacionadoIncluir.CheckStateChanged += new System.EventHandler(this.ChkCfdiRelacionadoIncluir_CheckStateChanged);
            // 
            // RadCommandBar2
            // 
            this.RadCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar2.Name = "RadCommandBar2";
            this.RadCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement2});
            this.RadCommandBar2.Size = new System.Drawing.Size(1409, 0);
            this.RadCommandBar2.TabIndex = 0;
            // 
            // CommandBarRowElement2
            // 
            this.CommandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement2.Name = "CommandBarRowElement2";
            this.CommandBarRowElement2.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.CommandCfdiRelacionado});
            this.CommandBarRowElement2.Text = "";
            // 
            // CommandCfdiRelacionado
            // 
            this.CommandCfdiRelacionado.DisplayName = "CommandBarStripElement2";
            this.CommandCfdiRelacionado.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarHostItemIncluir,
            this.commandBarSeparator3,
            this.CommandBarLabel1,
            this.ToolBarCfdiTipoRelacion,
            this.CommandBarSeparator1,
            this.ToolBarCfdiBuscar,
            this.ToolBarCfdiAgregar,
            this.ToolBarCfdiQuitar});
            this.CommandCfdiRelacionado.Name = "CommandCfdiRelacionado";
            // 
            // 
            // 
            this.CommandCfdiRelacionado.OverflowButton.Enabled = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.CommandCfdiRelacionado.GetChildAt(2))).Enabled = false;
            // 
            // ToolBarHostItemIncluir
            // 
            this.ToolBarHostItemIncluir.DisplayName = "Incluir";
            this.ToolBarHostItemIncluir.Name = "ToolBarHostItemIncluir";
            this.ToolBarHostItemIncluir.Text = "Incluir";
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisplayName = "commandBarSeparator3";
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // CommandBarLabel1
            // 
            this.CommandBarLabel1.DisplayName = "CommandBarLabel1";
            this.CommandBarLabel1.Name = "CommandBarLabel1";
            this.CommandBarLabel1.Text = "Tipo de Relación: ";
            // 
            // ToolBarCfdiTipoRelacion
            // 
            this.ToolBarCfdiTipoRelacion.DisplayName = "Tipo de Relación";
            this.ToolBarCfdiTipoRelacion.DropDownAnimationEnabled = true;
            this.ToolBarCfdiTipoRelacion.Enabled = false;
            this.ToolBarCfdiTipoRelacion.MaxDropDownItems = 0;
            this.ToolBarCfdiTipoRelacion.MinSize = new System.Drawing.Size(350, 22);
            this.ToolBarCfdiTipoRelacion.Name = "ToolBarCfdiTipoRelacion";
            this.ToolBarCfdiTipoRelacion.Text = "";
            // 
            // CommandBarSeparator1
            // 
            this.CommandBarSeparator1.DisplayName = "CommandBarSeparator1";
            this.CommandBarSeparator1.Name = "CommandBarSeparator1";
            this.CommandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarCfdiBuscar
            // 
            this.ToolBarCfdiBuscar.DisplayName = "Buscar";
            this.ToolBarCfdiBuscar.DrawText = true;
            this.ToolBarCfdiBuscar.Enabled = false;
            this.ToolBarCfdiBuscar.FlipText = false;
            this.ToolBarCfdiBuscar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_view_file;
            this.ToolBarCfdiBuscar.Name = "ToolBarCfdiBuscar";
            this.ToolBarCfdiBuscar.Text = "Buscar";
            this.ToolBarCfdiBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarCfdiBuscar.Click += new System.EventHandler(this.ToolBarCfdiBuscar_Click);
            // 
            // ToolBarCfdiAgregar
            // 
            this.ToolBarCfdiAgregar.DisplayName = "Agregar";
            this.ToolBarCfdiAgregar.DrawText = true;
            this.ToolBarCfdiAgregar.Enabled = false;
            this.ToolBarCfdiAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_agregar_archivo;
            this.ToolBarCfdiAgregar.Name = "ToolBarCfdiAgregar";
            this.ToolBarCfdiAgregar.Text = "Agregar";
            this.ToolBarCfdiAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarCfdiAgregar.Click += new System.EventHandler(this.ToolBarCfdiAgregar_Click);
            // 
            // ToolBarCfdiQuitar
            // 
            this.ToolBarCfdiQuitar.DisplayName = "Quitar";
            this.ToolBarCfdiQuitar.DrawText = true;
            this.ToolBarCfdiQuitar.Enabled = false;
            this.ToolBarCfdiQuitar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_delete_file;
            this.ToolBarCfdiQuitar.Name = "ToolBarCfdiQuitar";
            this.ToolBarCfdiQuitar.Text = "Quitar";
            this.ToolBarCfdiQuitar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarCfdiQuitar.Click += new System.EventHandler(this.ToolBarCfdiQuitar_Click);
            // 
            // PanelPago
            // 
            this.PanelPago.Controls.Add(this.TxbMonto);
            this.PanelPago.Controls.Add(this.ChkAutoSuma);
            this.PanelPago.Controls.Add(this.ButtonSPEI);
            this.PanelPago.Controls.Add(this.CboFormaPagoP);
            this.PanelPago.Controls.Add(this.CboNomBancoOrdExt);
            this.PanelPago.Controls.Add(this.RadLabel4);
            this.PanelPago.Controls.Add(this.TxbSelloPago);
            this.PanelPago.Controls.Add(this.RadLabel5);
            this.PanelPago.Controls.Add(this.TxbCadenaPago);
            this.PanelPago.Controls.Add(this.RadLabel6);
            this.PanelPago.Controls.Add(this.TxbCertificadoPago);
            this.PanelPago.Controls.Add(this.CboTipoCadenaPago);
            this.PanelPago.Controls.Add(this.RadLabel22);
            this.PanelPago.Controls.Add(this.RadLabel23);
            this.PanelPago.Controls.Add(this.TxbCtaBeneficiario);
            this.PanelPago.Controls.Add(this.RadLabel24);
            this.PanelPago.Controls.Add(this.TxbRfcCtaBeneficiario);
            this.PanelPago.Controls.Add(this.RadLabel25);
            this.PanelPago.Controls.Add(this.TxbCtaOrdenante);
            this.PanelPago.Controls.Add(this.RadLabel26);
            this.PanelPago.Controls.Add(this.RadLabel27);
            this.PanelPago.Controls.Add(this.TxbRfcEmisorCtaOrigen);
            this.PanelPago.Controls.Add(this.RadLabel28);
            this.PanelPago.Controls.Add(this.RadLabel29);
            this.PanelPago.Controls.Add(this.RadLabel30);
            this.PanelPago.Controls.Add(this.TxbNumOperacion);
            this.PanelPago.Controls.Add(this.RadLabel31);
            this.PanelPago.Controls.Add(this.RadLabel32);
            this.PanelPago.Controls.Add(this.RadLabel33);
            this.PanelPago.Controls.Add(this.TxbTipoCambioDR);
            this.PanelPago.Controls.Add(this.CboMonedaP);
            this.PanelPago.Controls.Add(this.FechaPago);
            this.PanelPago.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelPago.Location = new System.Drawing.Point(0, 225);
            this.PanelPago.Name = "PanelPago";
            this.PanelPago.Size = new System.Drawing.Size(1430, 135);
            this.PanelPago.TabIndex = 171;
            // 
            // TxbMonto
            // 
            this.TxbMonto.Location = new System.Drawing.Point(1078, 11);
            this.TxbMonto.Mask = "c2";
            this.TxbMonto.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TxbMonto.Name = "TxbMonto";
            this.TxbMonto.Size = new System.Drawing.Size(104, 20);
            this.TxbMonto.TabIndex = 196;
            this.TxbMonto.TabStop = false;
            this.TxbMonto.Text = "$0.00";
            this.TxbMonto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ChkAutoSuma
            // 
            this.ChkAutoSuma.Location = new System.Drawing.Point(1209, 36);
            this.ChkAutoSuma.Name = "ChkAutoSuma";
            this.ChkAutoSuma.Size = new System.Drawing.Size(72, 18);
            this.ChkAutoSuma.TabIndex = 193;
            this.ChkAutoSuma.Text = "AutoSuma";
            // 
            // ButtonSPEI
            // 
            this.ButtonSPEI.Location = new System.Drawing.Point(1083, 60);
            this.ButtonSPEI.Name = "ButtonSPEI";
            this.ButtonSPEI.Size = new System.Drawing.Size(99, 20);
            this.ButtonSPEI.TabIndex = 191;
            this.ButtonSPEI.Text = "SPEI";
            this.ButtonSPEI.Click += new System.EventHandler(this.BttnSpei_Click);
            // 
            // CboFormaPagoP
            // 
            // 
            // CboFormaPagoP.NestedRadGridView
            // 
            this.CboFormaPagoP.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboFormaPagoP.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboFormaPagoP.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboFormaPagoP.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboFormaPagoP.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboFormaPagoP.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboFormaPagoP.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn42.FieldName = "Clave";
            gridViewTextBoxColumn42.HeaderText = "Codigo";
            gridViewTextBoxColumn42.Name = "Code";
            gridViewTextBoxColumn43.FieldName = "Descripcion";
            gridViewTextBoxColumn43.HeaderText = "Descripción";
            gridViewTextBoxColumn43.Name = "Name";
            this.CboFormaPagoP.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn42,
            gridViewTextBoxColumn43});
            this.CboFormaPagoP.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboFormaPagoP.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboFormaPagoP.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition12;
            this.CboFormaPagoP.EditorControl.Name = "NestedRadGridView";
            this.CboFormaPagoP.EditorControl.ReadOnly = true;
            this.CboFormaPagoP.EditorControl.ShowGroupPanel = false;
            this.CboFormaPagoP.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboFormaPagoP.EditorControl.TabIndex = 0;
            this.CboFormaPagoP.Location = new System.Drawing.Point(336, 10);
            this.CboFormaPagoP.Name = "CboFormaPagoP";
            this.CboFormaPagoP.NullText = "Forma de pago";
            this.CboFormaPagoP.Size = new System.Drawing.Size(294, 20);
            this.CboFormaPagoP.TabIndex = 190;
            this.CboFormaPagoP.TabStop = false;
            // 
            // CboNomBancoOrdExt
            // 
            // 
            // CboNomBancoOrdExt.NestedRadGridView
            // 
            this.CboNomBancoOrdExt.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboNomBancoOrdExt.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboNomBancoOrdExt.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboNomBancoOrdExt.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboNomBancoOrdExt.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboNomBancoOrdExt.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboNomBancoOrdExt.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn44.FieldName = "RazonSocial";
            gridViewTextBoxColumn44.HeaderText = "Razon Social";
            gridViewTextBoxColumn44.Name = "RazonSocial";
            gridViewTextBoxColumn44.Width = 220;
            gridViewTextBoxColumn45.FieldName = "Clave";
            gridViewTextBoxColumn45.HeaderText = "Clave";
            gridViewTextBoxColumn45.Name = "Clave";
            gridViewTextBoxColumn46.FieldName = "Descripcion";
            gridViewTextBoxColumn46.HeaderText = "Descripción";
            gridViewTextBoxColumn46.Name = "Descripcion";
            gridViewTextBoxColumn46.Width = 165;
            this.CboNomBancoOrdExt.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn44,
            gridViewTextBoxColumn45,
            gridViewTextBoxColumn46});
            this.CboNomBancoOrdExt.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboNomBancoOrdExt.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboNomBancoOrdExt.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition13;
            this.CboNomBancoOrdExt.EditorControl.Name = "NestedRadGridView";
            this.CboNomBancoOrdExt.EditorControl.ReadOnly = true;
            this.CboNomBancoOrdExt.EditorControl.ShowGroupPanel = false;
            this.CboNomBancoOrdExt.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboNomBancoOrdExt.EditorControl.TabIndex = 0;
            this.CboNomBancoOrdExt.Location = new System.Drawing.Point(500, 34);
            this.CboNomBancoOrdExt.Name = "CboNomBancoOrdExt";
            this.CboNomBancoOrdExt.NullText = "Nombre del Banco";
            this.CboNomBancoOrdExt.Size = new System.Drawing.Size(238, 20);
            this.CboNomBancoOrdExt.TabIndex = 189;
            this.CboNomBancoOrdExt.TabStop = false;
            // 
            // RadLabel4
            // 
            this.RadLabel4.Location = new System.Drawing.Point(488, 111);
            this.RadLabel4.Name = "RadLabel4";
            this.RadLabel4.Size = new System.Drawing.Size(77, 18);
            this.RadLabel4.TabIndex = 188;
            this.RadLabel4.Text = "Sello de pago:";
            // 
            // TxbSelloPago
            // 
            this.TxbSelloPago.Location = new System.Drawing.Point(571, 110);
            this.TxbSelloPago.Name = "TxbSelloPago";
            this.TxbSelloPago.NullText = "Sello de pago:";
            this.TxbSelloPago.Size = new System.Drawing.Size(611, 20);
            this.TxbSelloPago.TabIndex = 187;
            // 
            // RadLabel5
            // 
            this.RadLabel5.Location = new System.Drawing.Point(314, 84);
            this.RadLabel5.Name = "RadLabel5";
            this.RadLabel5.Size = new System.Drawing.Size(91, 18);
            this.RadLabel5.TabIndex = 186;
            this.RadLabel5.Text = "Cadena de pago:";
            // 
            // TxbCadenaPago
            // 
            this.TxbCadenaPago.Location = new System.Drawing.Point(426, 83);
            this.TxbCadenaPago.Name = "TxbCadenaPago";
            this.TxbCadenaPago.NullText = "Cadena de pago";
            this.TxbCadenaPago.Size = new System.Drawing.Size(756, 20);
            this.TxbCadenaPago.TabIndex = 185;
            // 
            // RadLabel6
            // 
            this.RadLabel6.Location = new System.Drawing.Point(12, 110);
            this.RadLabel6.Name = "RadLabel6";
            this.RadLabel6.Size = new System.Drawing.Size(107, 18);
            this.RadLabel6.TabIndex = 184;
            this.RadLabel6.Text = "Certificado de pago:";
            // 
            // TxbCertificadoPago
            // 
            this.TxbCertificadoPago.Location = new System.Drawing.Point(132, 109);
            this.TxbCertificadoPago.Name = "TxbCertificadoPago";
            this.TxbCertificadoPago.NullText = "Certificado de pago";
            this.TxbCertificadoPago.Size = new System.Drawing.Size(342, 20);
            this.TxbCertificadoPago.TabIndex = 183;
            // 
            // CboTipoCadenaPago
            // 
            // 
            // CboTipoCadenaPago.NestedRadGridView
            // 
            this.CboTipoCadenaPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboTipoCadenaPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboTipoCadenaPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboTipoCadenaPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboTipoCadenaPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboTipoCadenaPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboTipoCadenaPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn47.FieldName = "Clave";
            gridViewTextBoxColumn47.HeaderText = "Clave";
            gridViewTextBoxColumn47.Name = "Clave";
            gridViewTextBoxColumn48.FieldName = "Descripcion";
            gridViewTextBoxColumn48.HeaderText = "Descripción";
            gridViewTextBoxColumn48.Name = "Descripcion";
            this.CboTipoCadenaPago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn47,
            gridViewTextBoxColumn48});
            this.CboTipoCadenaPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboTipoCadenaPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboTipoCadenaPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition14;
            this.CboTipoCadenaPago.EditorControl.Name = "NestedRadGridView";
            this.CboTipoCadenaPago.EditorControl.ReadOnly = true;
            this.CboTipoCadenaPago.EditorControl.ShowGroupPanel = false;
            this.CboTipoCadenaPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboTipoCadenaPago.EditorControl.TabIndex = 0;
            this.CboTipoCadenaPago.Location = new System.Drawing.Point(132, 84);
            this.CboTipoCadenaPago.Name = "CboTipoCadenaPago";
            this.CboTipoCadenaPago.NullText = "Cadena de Pago";
            this.CboTipoCadenaPago.Size = new System.Drawing.Size(173, 20);
            this.CboTipoCadenaPago.TabIndex = 182;
            this.CboTipoCadenaPago.TabStop = false;
            // 
            // RadLabel22
            // 
            this.RadLabel22.Location = new System.Drawing.Point(12, 84);
            this.RadLabel22.Name = "RadLabel22";
            this.RadLabel22.Size = new System.Drawing.Size(114, 18);
            this.RadLabel22.TabIndex = 181;
            this.RadLabel22.Text = "Tipo cadena de pago:";
            // 
            // RadLabel23
            // 
            this.RadLabel23.Location = new System.Drawing.Point(314, 60);
            this.RadLabel23.Name = "RadLabel23";
            this.RadLabel23.Size = new System.Drawing.Size(105, 18);
            this.RadLabel23.TabIndex = 180;
            this.RadLabel23.Text = "Cuenta beneficiario:";
            // 
            // TxbCtaBeneficiario
            // 
            this.TxbCtaBeneficiario.Location = new System.Drawing.Point(426, 60);
            this.TxbCtaBeneficiario.MaxLength = 50;
            this.TxbCtaBeneficiario.Name = "TxbCtaBeneficiario";
            this.TxbCtaBeneficiario.NullText = "Cuenta beneficiario";
            this.TxbCtaBeneficiario.Size = new System.Drawing.Size(164, 20);
            this.TxbCtaBeneficiario.TabIndex = 179;
            // 
            // RadLabel24
            // 
            this.RadLabel24.Location = new System.Drawing.Point(12, 60);
            this.RadLabel24.Name = "RadLabel24";
            this.RadLabel24.Size = new System.Drawing.Size(163, 18);
            this.RadLabel24.TabIndex = 178;
            this.RadLabel24.Text = "RFC Emisor Cuenta Beneficiaria:";
            // 
            // TxbRfcCtaBeneficiario
            // 
            this.TxbRfcCtaBeneficiario.Location = new System.Drawing.Point(181, 60);
            this.TxbRfcCtaBeneficiario.MaxLength = 13;
            this.TxbRfcCtaBeneficiario.Name = "TxbRfcCtaBeneficiario";
            this.TxbRfcCtaBeneficiario.NullText = "RFC emisor de cuenta origen:";
            this.TxbRfcCtaBeneficiario.Size = new System.Drawing.Size(124, 20);
            this.TxbRfcCtaBeneficiario.TabIndex = 177;
            // 
            // RadLabel25
            // 
            this.RadLabel25.Location = new System.Drawing.Point(744, 36);
            this.RadLabel25.Name = "RadLabel25";
            this.RadLabel25.Size = new System.Drawing.Size(99, 18);
            this.RadLabel25.TabIndex = 176;
            this.RadLabel25.Text = "Cuenta ordenante:";
            // 
            // TxbCtaOrdenante
            // 
            this.TxbCtaOrdenante.Location = new System.Drawing.Point(849, 35);
            this.TxbCtaOrdenante.MaxLength = 50;
            this.TxbCtaOrdenante.Name = "TxbCtaOrdenante";
            this.TxbCtaOrdenante.NullText = "Cuenta ordenante";
            this.TxbCtaOrdenante.Size = new System.Drawing.Size(179, 20);
            this.TxbCtaOrdenante.TabIndex = 175;
            // 
            // RadLabel26
            // 
            this.RadLabel26.Location = new System.Drawing.Point(314, 36);
            this.RadLabel26.Name = "RadLabel26";
            this.RadLabel26.Size = new System.Drawing.Size(180, 18);
            this.RadLabel26.TabIndex = 174;
            this.RadLabel26.Text = "Nombre del Banco Ord. Extranjero:";
            // 
            // RadLabel27
            // 
            this.RadLabel27.Location = new System.Drawing.Point(12, 36);
            this.RadLabel27.Name = "RadLabel27";
            this.RadLabel27.Size = new System.Drawing.Size(156, 18);
            this.RadLabel27.TabIndex = 173;
            this.RadLabel27.Text = "RFC Emisor de Cuenta Origen:";
            // 
            // TxbRfcEmisorCtaOrigen
            // 
            this.TxbRfcEmisorCtaOrigen.Location = new System.Drawing.Point(181, 35);
            this.TxbRfcEmisorCtaOrigen.MaxLength = 13;
            this.TxbRfcEmisorCtaOrigen.Name = "TxbRfcEmisorCtaOrigen";
            this.TxbRfcEmisorCtaOrigen.NullText = "RFC emisor de cuenta origen:";
            this.TxbRfcEmisorCtaOrigen.Size = new System.Drawing.Size(124, 20);
            this.TxbRfcEmisorCtaOrigen.TabIndex = 172;
            // 
            // RadLabel28
            // 
            this.RadLabel28.Location = new System.Drawing.Point(636, 11);
            this.RadLabel28.Name = "RadLabel28";
            this.RadLabel28.Size = new System.Drawing.Size(106, 18);
            this.RadLabel28.TabIndex = 171;
            this.RadLabel28.Text = "Núm. de Operación:";
            // 
            // RadLabel29
            // 
            this.RadLabel29.Location = new System.Drawing.Point(1031, 12);
            this.RadLabel29.Name = "RadLabel29";
            this.RadLabel29.Size = new System.Drawing.Size(42, 18);
            this.RadLabel29.TabIndex = 170;
            this.RadLabel29.Text = "Monto:";
            // 
            // RadLabel30
            // 
            this.RadLabel30.Location = new System.Drawing.Point(12, 11);
            this.RadLabel30.Name = "RadLabel30";
            this.RadLabel30.Size = new System.Drawing.Size(82, 18);
            this.RadLabel30.TabIndex = 169;
            this.RadLabel30.Text = "Fecha de Pago:";
            // 
            // TxbNumOperacion
            // 
            this.TxbNumOperacion.Location = new System.Drawing.Point(744, 10);
            this.TxbNumOperacion.Name = "TxbNumOperacion";
            this.TxbNumOperacion.NullText = "Número de Operación";
            this.TxbNumOperacion.Size = new System.Drawing.Size(145, 20);
            this.TxbNumOperacion.TabIndex = 168;
            // 
            // RadLabel31
            // 
            this.RadLabel31.Location = new System.Drawing.Point(1031, 35);
            this.RadLabel31.Name = "RadLabel31";
            this.RadLabel31.Size = new System.Drawing.Size(50, 18);
            this.RadLabel31.TabIndex = 167;
            this.RadLabel31.Text = "Moneda:";
            // 
            // RadLabel32
            // 
            this.RadLabel32.Location = new System.Drawing.Point(246, 11);
            this.RadLabel32.Name = "RadLabel32";
            this.RadLabel32.Size = new System.Drawing.Size(84, 18);
            this.RadLabel32.TabIndex = 166;
            this.RadLabel32.Text = "Forma de Pago:";
            // 
            // RadLabel33
            // 
            this.RadLabel33.Location = new System.Drawing.Point(895, 12);
            this.RadLabel33.Name = "RadLabel33";
            this.RadLabel33.Size = new System.Drawing.Size(88, 18);
            this.RadLabel33.TabIndex = 165;
            this.RadLabel33.Text = "Tipo de Cambio:";
            // 
            // TxbTipoCambioDR
            // 
            this.TxbTipoCambioDR.Location = new System.Drawing.Point(989, 11);
            this.TxbTipoCambioDR.Name = "TxbTipoCambioDR";
            this.TxbTipoCambioDR.NullText = "Tipo de Cambio";
            this.TxbTipoCambioDR.Size = new System.Drawing.Size(39, 20);
            this.TxbTipoCambioDR.TabIndex = 164;
            this.TxbTipoCambioDR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CboMonedaP
            // 
            // 
            // CboMonedaP.NestedRadGridView
            // 
            this.CboMonedaP.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboMonedaP.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboMonedaP.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboMonedaP.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboMonedaP.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboMonedaP.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboMonedaP.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn49.FieldName = "Clave";
            gridViewTextBoxColumn49.HeaderText = "Clave";
            gridViewTextBoxColumn49.Name = "Clave";
            gridViewTextBoxColumn50.FieldName = "Descripcion";
            gridViewTextBoxColumn50.HeaderText = "Descripción";
            gridViewTextBoxColumn50.Name = "Descripcion";
            this.CboMonedaP.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn49,
            gridViewTextBoxColumn50});
            this.CboMonedaP.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboMonedaP.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboMonedaP.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition15;
            this.CboMonedaP.EditorControl.Name = "NestedRadGridView";
            this.CboMonedaP.EditorControl.ReadOnly = true;
            this.CboMonedaP.EditorControl.ShowGroupPanel = false;
            this.CboMonedaP.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboMonedaP.EditorControl.TabIndex = 0;
            this.CboMonedaP.Location = new System.Drawing.Point(1083, 34);
            this.CboMonedaP.Name = "CboMonedaP";
            this.CboMonedaP.NullText = "Moneda";
            this.CboMonedaP.Size = new System.Drawing.Size(99, 20);
            this.CboMonedaP.TabIndex = 163;
            this.CboMonedaP.TabStop = false;
            // 
            // FechaPago
            // 
            this.FechaPago.CustomFormat = "dd/MM/yyyy hh:mm tt";
            this.FechaPago.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FechaPago.Location = new System.Drawing.Point(101, 10);
            this.FechaPago.Name = "FechaPago";
            this.FechaPago.NullText = "--/--/---- --:-- -- --";
            this.FechaPago.Size = new System.Drawing.Size(137, 20);
            this.FechaPago.TabIndex = 162;
            this.FechaPago.TabStop = false;
            this.FechaPago.Text = "06/10/2017 12:18 a. m.";
            this.FechaPago.Value = new System.DateTime(2017, 10, 6, 0, 18, 34, 554);
            // 
            // CboPagoDoctosRelacionados
            // 
            // 
            // CboPagoDoctosRelacionados.NestedRadGridView
            // 
            this.CboPagoDoctosRelacionados.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboPagoDoctosRelacionados.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboPagoDoctosRelacionados.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboPagoDoctosRelacionados.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboPagoDoctosRelacionados.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboPagoDoctosRelacionados.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboPagoDoctosRelacionados.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn51.FieldName = "Id";
            gridViewTextBoxColumn51.HeaderText = "Id";
            gridViewTextBoxColumn51.IsVisible = false;
            gridViewTextBoxColumn51.Name = "Id";
            gridViewTextBoxColumn51.VisibleInColumnChooser = false;
            gridViewTextBoxColumn52.FieldName = "SubId";
            gridViewTextBoxColumn52.HeaderText = "SubId";
            gridViewTextBoxColumn52.IsVisible = false;
            gridViewTextBoxColumn52.Name = "SubId";
            gridViewTextBoxColumn52.VisibleInColumnChooser = false;
            gridViewCheckBoxColumn2.FieldName = "IsActive";
            gridViewCheckBoxColumn2.HeaderText = "IsActive";
            gridViewCheckBoxColumn2.IsVisible = false;
            gridViewCheckBoxColumn2.Name = "IsActive";
            gridViewCheckBoxColumn2.VisibleInColumnChooser = false;
            gridViewTextBoxColumn53.FieldName = "Status";
            gridViewTextBoxColumn53.HeaderText = "Status";
            gridViewTextBoxColumn53.IsVisible = false;
            gridViewTextBoxColumn53.Name = "Status";
            gridViewTextBoxColumn54.FieldName = "Folio";
            gridViewTextBoxColumn54.HeaderText = "Folio";
            gridViewTextBoxColumn54.Name = "Folio";
            gridViewTextBoxColumn55.FieldName = "Serie";
            gridViewTextBoxColumn55.HeaderText = "Serie";
            gridViewTextBoxColumn55.Name = "Serie";
            gridViewTextBoxColumn56.FieldName = "IdDocumento";
            gridViewTextBoxColumn56.HeaderText = "UUID";
            gridViewTextBoxColumn56.Name = "IdDocumento";
            gridViewTextBoxColumn57.FieldName = "EmisorRfc";
            gridViewTextBoxColumn57.HeaderText = "Emisor (RFC)";
            gridViewTextBoxColumn57.Name = "RfcEmisor";
            gridViewTextBoxColumn58.FieldName = "Emisor";
            gridViewTextBoxColumn58.HeaderText = "Emisor";
            gridViewTextBoxColumn58.IsVisible = false;
            gridViewTextBoxColumn58.Name = "Emisor";
            gridViewTextBoxColumn59.FieldName = "ReceptorRfc";
            gridViewTextBoxColumn59.HeaderText = "Receptor (RFC)";
            gridViewTextBoxColumn59.Name = "RfcReceptor";
            gridViewTextBoxColumn60.FieldName = "Receptor";
            gridViewTextBoxColumn60.HeaderText = "Receptor";
            gridViewTextBoxColumn60.Name = "Receptor";
            gridViewTextBoxColumn61.FieldName = "Estado";
            gridViewTextBoxColumn61.HeaderText = "Estado";
            gridViewTextBoxColumn61.IsVisible = false;
            gridViewTextBoxColumn61.Name = "Estado";
            gridViewTextBoxColumn62.FieldName = "NumParcialidad";
            gridViewTextBoxColumn62.HeaderText = "Núm. Par.";
            gridViewTextBoxColumn62.Name = "NumParcialidad";
            gridViewTextBoxColumn63.DataType = typeof(decimal);
            gridViewTextBoxColumn63.FieldName = "Total";
            gridViewTextBoxColumn63.FormatString = "{0:$#,###0.00}";
            gridViewTextBoxColumn63.HeaderText = "Total";
            gridViewTextBoxColumn63.Name = "Total";
            gridViewTextBoxColumn63.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn64.DataType = typeof(decimal);
            gridViewTextBoxColumn64.FieldName = "Acumulado";
            gridViewTextBoxColumn64.FormatString = "{0:$#,###0.00}";
            gridViewTextBoxColumn64.HeaderText = "Acumulado";
            gridViewTextBoxColumn64.Name = "Acumulado";
            gridViewTextBoxColumn64.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.CboPagoDoctosRelacionados.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn51,
            gridViewTextBoxColumn52,
            gridViewCheckBoxColumn2,
            gridViewTextBoxColumn53,
            gridViewTextBoxColumn54,
            gridViewTextBoxColumn55,
            gridViewTextBoxColumn56,
            gridViewTextBoxColumn57,
            gridViewTextBoxColumn58,
            gridViewTextBoxColumn59,
            gridViewTextBoxColumn60,
            gridViewTextBoxColumn61,
            gridViewTextBoxColumn62,
            gridViewTextBoxColumn63,
            gridViewTextBoxColumn64});
            this.CboPagoDoctosRelacionados.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboPagoDoctosRelacionados.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboPagoDoctosRelacionados.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition16;
            this.CboPagoDoctosRelacionados.EditorControl.Name = "NestedRadGridView";
            this.CboPagoDoctosRelacionados.EditorControl.ReadOnly = true;
            this.CboPagoDoctosRelacionados.EditorControl.ShowGroupPanel = false;
            this.CboPagoDoctosRelacionados.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboPagoDoctosRelacionados.EditorControl.TabIndex = 0;
            this.CboPagoDoctosRelacionados.Location = new System.Drawing.Point(833, 103);
            this.CboPagoDoctosRelacionados.Name = "CboPagoDoctosRelacionados";
            this.CboPagoDoctosRelacionados.NullText = "Selecciona";
            this.CboPagoDoctosRelacionados.Size = new System.Drawing.Size(72, 20);
            this.CboPagoDoctosRelacionados.TabIndex = 192;
            this.CboPagoDoctosRelacionados.TabStop = false;
            this.CboPagoDoctosRelacionados.DropDownOpened += new System.EventHandler(this.CboPagoDoctosRelacionados_DropDownOpened);
            // 
            // Contenedor
            // 
            this.Contenedor.Controls.Add(this.PanelDoctosRealcionados);
            this.Contenedor.Controls.Add(this.PanelImpuestos);
            this.Contenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Contenedor.Location = new System.Drawing.Point(0, 360);
            this.Contenedor.Name = "Contenedor";
            this.Contenedor.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.Contenedor.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.Contenedor.Size = new System.Drawing.Size(1430, 453);
            this.Contenedor.TabIndex = 172;
            this.Contenedor.TabStop = false;
            // 
            // PanelDoctosRealcionados
            // 
            this.PanelDoctosRealcionados.Controls.Add(this.GridDoctosRelacionados);
            this.PanelDoctosRealcionados.Controls.Add(this.radCommandBar3);
            this.PanelDoctosRealcionados.Location = new System.Drawing.Point(0, 0);
            this.PanelDoctosRealcionados.Name = "PanelDoctosRealcionados";
            // 
            // 
            // 
            this.PanelDoctosRealcionados.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.PanelDoctosRealcionados.Size = new System.Drawing.Size(1430, 252);
            this.PanelDoctosRealcionados.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.06109726F);
            this.PanelDoctosRealcionados.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 25);
            this.PanelDoctosRealcionados.TabIndex = 0;
            this.PanelDoctosRealcionados.TabStop = false;
            // 
            // GridDoctosRelacionados
            // 
            this.GridDoctosRelacionados.Controls.Add(this.CboPagoDoctosRelacionados);
            this.GridDoctosRelacionados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridDoctosRelacionados.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn65.FieldName = "IdDocumento";
            gridViewTextBoxColumn65.HeaderText = "IdDocumento";
            gridViewTextBoxColumn65.Name = "IdDocumento";
            gridViewTextBoxColumn65.Width = 230;
            gridViewTextBoxColumn66.FieldName = "Serie";
            gridViewTextBoxColumn66.HeaderText = "Serie";
            gridViewTextBoxColumn66.Name = "Serie";
            gridViewTextBoxColumn66.Width = 65;
            gridViewTextBoxColumn67.FieldName = "Folio";
            gridViewTextBoxColumn67.HeaderText = "Folio";
            gridViewTextBoxColumn67.Name = "Folio";
            gridViewTextBoxColumn67.Width = 65;
            gridViewTextBoxColumn68.FieldName = "RFC";
            gridViewTextBoxColumn68.HeaderText = "RFC";
            gridViewTextBoxColumn68.Name = "RfcReceptor";
            gridViewTextBoxColumn68.Width = 85;
            gridViewTextBoxColumn69.FieldName = "Nombre";
            gridViewTextBoxColumn69.HeaderText = "Receptor";
            gridViewTextBoxColumn69.Name = "Receptor";
            gridViewTextBoxColumn69.Width = 200;
            gridViewTextBoxColumn70.FieldName = "FechaEmision";
            gridViewTextBoxColumn70.HeaderText = "Fecha \r\nEmision";
            gridViewTextBoxColumn70.Name = "FechaEmision";
            gridViewTextBoxColumn70.Width = 85;
            gridViewMultiComboBoxColumn1.FieldName = "Moneda";
            gridViewMultiComboBoxColumn1.HeaderText = "Moneda";
            gridViewMultiComboBoxColumn1.Name = "MonedaDR";
            gridViewMultiComboBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewMultiComboBoxColumn1.Width = 65;
            gridViewTextBoxColumn71.FieldName = "TipoCambio";
            gridViewTextBoxColumn71.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn71.HeaderText = "Tipo \r\nCambio";
            gridViewTextBoxColumn71.Name = "TipoCambioDR";
            gridViewTextBoxColumn71.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewMultiComboBoxColumn2.FieldName = "MetodoPago";
            gridViewMultiComboBoxColumn2.HeaderText = "Método \r\nde Pago";
            gridViewMultiComboBoxColumn2.Name = "MetodoDePagoDR";
            gridViewMultiComboBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewMultiComboBoxColumn2.Width = 80;
            gridViewTextBoxColumn72.FieldName = "NumParcialidad";
            gridViewTextBoxColumn72.HeaderText = "Núm. \r\n Parcialidad";
            gridViewTextBoxColumn72.Name = "NumParcialidad";
            gridViewTextBoxColumn72.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn73.FieldName = "ImpSaldoAnt";
            gridViewTextBoxColumn73.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn73.HeaderText = "Imp. Saldo \r\nAnterior";
            gridViewTextBoxColumn73.Name = "ImpSaldoAnt";
            gridViewTextBoxColumn73.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn73.Width = 75;
            gridViewTextBoxColumn74.FieldName = "ImpPagado";
            gridViewTextBoxColumn74.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn74.HeaderText = "Importe \r\n Pagado";
            gridViewTextBoxColumn74.Name = "ImpPagado";
            gridViewTextBoxColumn74.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn74.Width = 75;
            gridViewTextBoxColumn75.FieldName = "ImpSaldoInsoluto";
            gridViewTextBoxColumn75.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn75.HeaderText = "Imp. Saldo\r\nInsoluto";
            gridViewTextBoxColumn75.Name = "ImpSaldoInsoluto";
            gridViewTextBoxColumn75.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn75.Width = 75;
            gridViewTextBoxColumn76.FieldName = "Estado";
            gridViewTextBoxColumn76.HeaderText = "Estado";
            gridViewTextBoxColumn76.Name = "Estado";
            gridViewTextBoxColumn76.Width = 65;
            this.GridDoctosRelacionados.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn65,
            gridViewTextBoxColumn66,
            gridViewTextBoxColumn67,
            gridViewTextBoxColumn68,
            gridViewTextBoxColumn69,
            gridViewTextBoxColumn70,
            gridViewMultiComboBoxColumn1,
            gridViewTextBoxColumn71,
            gridViewMultiComboBoxColumn2,
            gridViewTextBoxColumn72,
            gridViewTextBoxColumn73,
            gridViewTextBoxColumn74,
            gridViewTextBoxColumn75,
            gridViewTextBoxColumn76});
            this.GridDoctosRelacionados.MasterTemplate.EnableGrouping = false;
            this.GridDoctosRelacionados.MasterTemplate.ViewDefinition = tableViewDefinition17;
            this.GridDoctosRelacionados.Name = "GridDoctosRelacionados";
            this.GridDoctosRelacionados.Size = new System.Drawing.Size(1430, 222);
            this.GridDoctosRelacionados.TabIndex = 1;
            // 
            // radCommandBar3
            // 
            this.radCommandBar3.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar3.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar3.Name = "radCommandBar3";
            this.radCommandBar3.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar3.Size = new System.Drawing.Size(1430, 30);
            this.radCommandBar3.TabIndex = 0;
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.Name = "commandBarRowElement2";
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarDoctosRelacionados});
            this.commandBarRowElement3.Text = "";
            // 
            // ToolBarDoctosRelacionados
            // 
            this.ToolBarDoctosRelacionados.DisplayName = "commandBarStripElement1";
            this.ToolBarDoctosRelacionados.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarDoctosLabel,
            this.ToolBarDoctosHostComprobantes,
            this.ToolBarDoctosButtonAgregar,
            this.ToolBarDoctosButtonQuitar,
            this.ToolBarDoctosSeparador1,
            this.ToolBarDoctosLabelContador,
            this.ToolBarDoctosContador,
            this.ToolBarDoctosSeparador2,
            this.ToolBarDoctosImportesPagadosLabel,
            this.ToolBarDoctosImportePagado,
            this.ToolBarDoctosSeparador3,
            this.ToolBarDoctosHostAutoSuma});
            this.ToolBarDoctosRelacionados.Name = "ToolBarDoctosRelacionados";
            // 
            // 
            // 
            this.ToolBarDoctosRelacionados.OverflowButton.Enabled = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.ToolBarDoctosRelacionados.GetChildAt(2))).Enabled = false;
            // 
            // ToolBarDoctosLabel
            // 
            this.ToolBarDoctosLabel.DisplayName = "commandBarLabel1";
            this.ToolBarDoctosLabel.Name = "ToolBarDoctosLabel";
            this.ToolBarDoctosLabel.Text = "Comprobante:";
            // 
            // ToolBarDoctosHostComprobantes
            // 
            this.ToolBarDoctosHostComprobantes.DisplayName = "Comprobantes";
            this.ToolBarDoctosHostComprobantes.MinSize = new System.Drawing.Size(420, 0);
            this.ToolBarDoctosHostComprobantes.Name = "ToolBarDoctosHostComprobantes";
            this.ToolBarDoctosHostComprobantes.Text = "Comprobantes";
            // 
            // ToolBarDoctosButtonAgregar
            // 
            this.ToolBarDoctosButtonAgregar.DisplayName = "Agregar";
            this.ToolBarDoctosButtonAgregar.DrawText = true;
            this.ToolBarDoctosButtonAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_agregar_archivo;
            this.ToolBarDoctosButtonAgregar.Name = "ToolBarDoctosButtonAgregar";
            this.ToolBarDoctosButtonAgregar.Text = "Agregar";
            this.ToolBarDoctosButtonAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarDoctosButtonAgregar.Click += new System.EventHandler(this.ToolBarDoctosButtonAgregar_Click);
            // 
            // ToolBarDoctosButtonQuitar
            // 
            this.ToolBarDoctosButtonQuitar.DisplayName = "Quitar";
            this.ToolBarDoctosButtonQuitar.DrawText = true;
            this.ToolBarDoctosButtonQuitar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_delete_file;
            this.ToolBarDoctosButtonQuitar.Name = "ToolBarDoctosButtonQuitar";
            this.ToolBarDoctosButtonQuitar.Text = "Quitar";
            this.ToolBarDoctosButtonQuitar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarDoctosButtonQuitar.Click += new System.EventHandler(this.ToolBarDoctosButtonQuitar_Click);
            // 
            // ToolBarDoctosSeparador1
            // 
            this.ToolBarDoctosSeparador1.DisplayName = "commandBarSeparator1";
            this.ToolBarDoctosSeparador1.Name = "ToolBarDoctosSeparador1";
            this.ToolBarDoctosSeparador1.VisibleInOverflowMenu = false;
            // 
            // ToolBarDoctosLabelContador
            // 
            this.ToolBarDoctosLabelContador.DisplayName = "commandBarLabel1";
            this.ToolBarDoctosLabelContador.Name = "ToolBarDoctosLabelContador";
            this.ToolBarDoctosLabelContador.Text = "Doc. Agregados:";
            // 
            // ToolBarDoctosContador
            // 
            this.ToolBarDoctosContador.Name = "ToolBarDoctosContador";
            this.ToolBarDoctosContador.Text = "";
            // 
            // ToolBarDoctosSeparador2
            // 
            this.ToolBarDoctosSeparador2.DisplayName = "commandBarSeparator3";
            this.ToolBarDoctosSeparador2.Name = "ToolBarDoctosSeparador2";
            this.ToolBarDoctosSeparador2.VisibleInOverflowMenu = false;
            // 
            // ToolBarDoctosImportesPagadosLabel
            // 
            this.ToolBarDoctosImportesPagadosLabel.DisplayName = "commandBarLabel2";
            this.ToolBarDoctosImportesPagadosLabel.Name = "ToolBarDoctosImportesPagadosLabel";
            this.ToolBarDoctosImportesPagadosLabel.Text = "Importes Pagados:";
            // 
            // ToolBarDoctosImportePagado
            // 
            this.ToolBarDoctosImportePagado.DisplayName = "commandBarLabel1";
            this.ToolBarDoctosImportePagado.Name = "ToolBarDoctosImportePagado";
            this.ToolBarDoctosImportePagado.Text = "";
            // 
            // ToolBarDoctosSeparador3
            // 
            this.ToolBarDoctosSeparador3.DisplayName = "commandBarSeparator4";
            this.ToolBarDoctosSeparador3.Name = "ToolBarDoctosSeparador3";
            this.ToolBarDoctosSeparador3.VisibleInOverflowMenu = false;
            // 
            // ToolBarDoctosHostAutoSuma
            // 
            this.ToolBarDoctosHostAutoSuma.DisplayName = "commandBarHostItem1";
            this.ToolBarDoctosHostAutoSuma.Name = "ToolBarDoctosHostAutoSuma";
            this.ToolBarDoctosHostAutoSuma.Text = "Autosuma";
            // 
            // PanelImpuestos
            // 
            this.PanelImpuestos.Controls.Add(this.radPageView2);
            this.PanelImpuestos.Location = new System.Drawing.Point(0, 256);
            this.PanelImpuestos.Name = "PanelImpuestos";
            // 
            // 
            // 
            this.PanelImpuestos.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.PanelImpuestos.Size = new System.Drawing.Size(1430, 197);
            this.PanelImpuestos.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.06109726F);
            this.PanelImpuestos.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -25);
            this.PanelImpuestos.TabIndex = 1;
            this.PanelImpuestos.TabStop = false;
            // 
            // radPageView2
            // 
            this.radPageView2.Controls.Add(this.PageImpuestosTralados);
            this.radPageView2.Controls.Add(this.PageImpuestosRetenidos);
            this.radPageView2.DefaultPage = this.PageImpuestosTralados;
            this.radPageView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView2.Location = new System.Drawing.Point(0, 0);
            this.radPageView2.Name = "radPageView2";
            this.radPageView2.SelectedPage = this.PageImpuestosTralados;
            this.radPageView2.Size = new System.Drawing.Size(1430, 197);
            this.radPageView2.TabIndex = 0;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView2.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // PageImpuestosTralados
            // 
            this.PageImpuestosTralados.Controls.Add(this.GridImpuestosTraslados);
            this.PageImpuestosTralados.Controls.Add(this.radCommandBar5);
            this.PageImpuestosTralados.ItemSize = new System.Drawing.SizeF(133F, 28F);
            this.PageImpuestosTralados.Location = new System.Drawing.Point(10, 37);
            this.PageImpuestosTralados.Name = "PageImpuestosTralados";
            this.PageImpuestosTralados.Size = new System.Drawing.Size(1409, 149);
            this.PageImpuestosTralados.Text = "Impuestos: Trasladados";
            // 
            // GridImpuestosTraslados
            // 
            this.GridImpuestosTraslados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridImpuestosTraslados.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridImpuestosTraslados.MasterTemplate.AllowAddNewRow = false;
            gridViewComboBoxColumn1.FieldName = "Tipo";
            gridViewComboBoxColumn1.HeaderText = "Tipo";
            gridViewComboBoxColumn1.Name = "Tipo";
            gridViewComboBoxColumn1.Width = 80;
            gridViewComboBoxColumn2.FieldName = "Impuesto";
            gridViewComboBoxColumn2.HeaderText = "Impuesto";
            gridViewComboBoxColumn2.Name = "Impuesto";
            gridViewComboBoxColumn2.Width = 80;
            gridViewTextBoxColumn77.DataType = typeof(decimal);
            gridViewTextBoxColumn77.FieldName = "Base";
            gridViewTextBoxColumn77.HeaderText = "Base";
            gridViewTextBoxColumn77.Name = "Base";
            gridViewTextBoxColumn77.Width = 80;
            gridViewComboBoxColumn3.FieldName = "TipoFactor";
            gridViewComboBoxColumn3.HeaderText = "Tipo Factor";
            gridViewComboBoxColumn3.Name = "TipoFactor";
            gridViewComboBoxColumn3.Width = 80;
            gridViewTextBoxColumn78.DataType = typeof(decimal);
            gridViewTextBoxColumn78.FieldName = "TasaOCuota";
            gridViewTextBoxColumn78.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn78.HeaderText = "Tasa ó Cuota";
            gridViewTextBoxColumn78.Name = "TasaOCuota";
            gridViewTextBoxColumn78.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn78.Width = 80;
            gridViewTextBoxColumn79.DataType = typeof(decimal);
            gridViewTextBoxColumn79.FieldName = "Importe";
            gridViewTextBoxColumn79.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn79.HeaderText = "Importe";
            gridViewTextBoxColumn79.Name = "Importe";
            gridViewTextBoxColumn79.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn79.Width = 80;
            this.GridImpuestosTraslados.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn1,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn77,
            gridViewComboBoxColumn3,
            gridViewTextBoxColumn78,
            gridViewTextBoxColumn79});
            this.GridImpuestosTraslados.MasterTemplate.ViewDefinition = tableViewDefinition18;
            this.GridImpuestosTraslados.Name = "GridImpuestosTraslados";
            this.GridImpuestosTraslados.ShowGroupPanel = false;
            this.GridImpuestosTraslados.Size = new System.Drawing.Size(1409, 119);
            this.GridImpuestosTraslados.TabIndex = 2;
            // 
            // radCommandBar5
            // 
            this.radCommandBar5.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar5.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar5.Name = "radCommandBar5";
            this.radCommandBar5.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement5});
            this.radCommandBar5.Size = new System.Drawing.Size(1409, 30);
            this.radCommandBar5.TabIndex = 0;
            // 
            // commandBarRowElement5
            // 
            this.commandBarRowElement5.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement5.Name = "commandBarRowElement3";
            this.commandBarRowElement5.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarImpuestosTraslados});
            this.commandBarRowElement5.Text = "";
            // 
            // ToolBarImpuestosTraslados
            // 
            this.ToolBarImpuestosTraslados.DisplayName = "commandBarStripElement2";
            this.ToolBarImpuestosTraslados.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarImpuestosTrasladoBuscar,
            this.ToolBarImpuestosTrasladoAgregar,
            this.ToolBarImpuestosTrasladoQuitar});
            this.ToolBarImpuestosTraslados.Name = "ToolBarImpuestosTraslados";
            // 
            // 
            // 
            this.ToolBarImpuestosTraslados.OverflowButton.Enabled = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.ToolBarImpuestosTraslados.GetChildAt(2))).Enabled = false;
            // 
            // ToolBarImpuestosTrasladoBuscar
            // 
            this.ToolBarImpuestosTrasladoBuscar.DisplayName = "Buscar";
            this.ToolBarImpuestosTrasladoBuscar.DrawText = true;
            this.ToolBarImpuestosTrasladoBuscar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_view_file;
            this.ToolBarImpuestosTrasladoBuscar.Name = "ToolBarImpuestosTrasladoBuscar";
            this.ToolBarImpuestosTrasladoBuscar.Text = "Buscar";
            this.ToolBarImpuestosTrasladoBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarImpuestosTrasladoAgregar
            // 
            this.ToolBarImpuestosTrasladoAgregar.DisplayName = "Agregar";
            this.ToolBarImpuestosTrasladoAgregar.DrawText = true;
            this.ToolBarImpuestosTrasladoAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_agregar_archivo;
            this.ToolBarImpuestosTrasladoAgregar.Name = "ToolBarImpuestosTrasladoAgregar";
            this.ToolBarImpuestosTrasladoAgregar.Text = "Agregar";
            this.ToolBarImpuestosTrasladoAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarImpuestosTrasladoQuitar
            // 
            this.ToolBarImpuestosTrasladoQuitar.DisplayName = "Quitar";
            this.ToolBarImpuestosTrasladoQuitar.DrawText = true;
            this.ToolBarImpuestosTrasladoQuitar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_delete_file;
            this.ToolBarImpuestosTrasladoQuitar.Name = "ToolBarImpuestosTrasladoQuitar";
            this.ToolBarImpuestosTrasladoQuitar.Text = "Quitar";
            this.ToolBarImpuestosTrasladoQuitar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // PageImpuestosRetenidos
            // 
            this.PageImpuestosRetenidos.Controls.Add(this.GridImpuestosRetenidos);
            this.PageImpuestosRetenidos.Controls.Add(this.radCommandBar6);
            this.PageImpuestosRetenidos.ItemSize = new System.Drawing.SizeF(123F, 28F);
            this.PageImpuestosRetenidos.Location = new System.Drawing.Point(10, 37);
            this.PageImpuestosRetenidos.Name = "PageImpuestosRetenidos";
            this.PageImpuestosRetenidos.Size = new System.Drawing.Size(1409, 160);
            this.PageImpuestosRetenidos.Text = "Impuestos: Retenidos";
            // 
            // GridImpuestosRetenidos
            // 
            this.GridImpuestosRetenidos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridImpuestosRetenidos.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.GridImpuestosRetenidos.MasterTemplate.AllowAddNewRow = false;
            gridViewComboBoxColumn4.FieldName = "Tipo";
            gridViewComboBoxColumn4.HeaderText = "Tipo";
            gridViewComboBoxColumn4.Name = "Tipo";
            gridViewComboBoxColumn4.Width = 80;
            gridViewComboBoxColumn5.FieldName = "Impuesto";
            gridViewComboBoxColumn5.HeaderText = "Impuesto";
            gridViewComboBoxColumn5.Name = "Impuesto";
            gridViewComboBoxColumn5.Width = 80;
            gridViewTextBoxColumn80.DataType = typeof(decimal);
            gridViewTextBoxColumn80.FieldName = "Base";
            gridViewTextBoxColumn80.HeaderText = "Base";
            gridViewTextBoxColumn80.Name = "Base";
            gridViewTextBoxColumn80.Width = 80;
            gridViewComboBoxColumn6.FieldName = "TipoFactor";
            gridViewComboBoxColumn6.HeaderText = "Tipo Factor";
            gridViewComboBoxColumn6.Name = "TipoFactor";
            gridViewComboBoxColumn6.Width = 80;
            gridViewTextBoxColumn81.DataType = typeof(decimal);
            gridViewTextBoxColumn81.FieldName = "TasaOCuota";
            gridViewTextBoxColumn81.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn81.HeaderText = "Tasa ó Cuota";
            gridViewTextBoxColumn81.Name = "TasaOCuota";
            gridViewTextBoxColumn81.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn81.Width = 80;
            gridViewTextBoxColumn82.DataType = typeof(decimal);
            gridViewTextBoxColumn82.FieldName = "Importe";
            gridViewTextBoxColumn82.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn82.HeaderText = "Importe";
            gridViewTextBoxColumn82.Name = "Importe";
            gridViewTextBoxColumn82.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn82.Width = 80;
            this.GridImpuestosRetenidos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn4,
            gridViewComboBoxColumn5,
            gridViewTextBoxColumn80,
            gridViewComboBoxColumn6,
            gridViewTextBoxColumn81,
            gridViewTextBoxColumn82});
            this.GridImpuestosRetenidos.MasterTemplate.ViewDefinition = tableViewDefinition19;
            this.GridImpuestosRetenidos.Name = "GridImpuestosRetenidos";
            this.GridImpuestosRetenidos.ShowGroupPanel = false;
            this.GridImpuestosRetenidos.Size = new System.Drawing.Size(1409, 160);
            this.GridImpuestosRetenidos.TabIndex = 3;
            // 
            // radCommandBar6
            // 
            this.radCommandBar6.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar6.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar6.Name = "radCommandBar6";
            this.radCommandBar6.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement6});
            this.radCommandBar6.Size = new System.Drawing.Size(1409, 0);
            this.radCommandBar6.TabIndex = 0;
            // 
            // commandBarRowElement6
            // 
            this.commandBarRowElement6.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement6.Name = "commandBarRowElement4";
            this.commandBarRowElement6.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarImpuestosRetenidos});
            this.commandBarRowElement6.Text = "";
            // 
            // ToolBarImpuestosRetenidos
            // 
            this.ToolBarImpuestosRetenidos.DisplayName = "commandBarStripElement3";
            this.ToolBarImpuestosRetenidos.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarImpuestosRetenidoBuscar,
            this.ToolBarImpuestosRetenidoAgregar,
            this.ToolBarImpuestosRetenidoQuitar});
            this.ToolBarImpuestosRetenidos.Name = "ToolBarImpuestosRetenidos";
            // 
            // 
            // 
            this.ToolBarImpuestosRetenidos.OverflowButton.Enabled = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.ToolBarImpuestosRetenidos.GetChildAt(2))).Enabled = false;
            // 
            // ToolBarImpuestosRetenidoBuscar
            // 
            this.ToolBarImpuestosRetenidoBuscar.DisplayName = "Buscar";
            this.ToolBarImpuestosRetenidoBuscar.DrawText = true;
            this.ToolBarImpuestosRetenidoBuscar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_view_file;
            this.ToolBarImpuestosRetenidoBuscar.Name = "ToolBarImpuestosRetenidoBuscar";
            this.ToolBarImpuestosRetenidoBuscar.Text = "Buscar";
            this.ToolBarImpuestosRetenidoBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarImpuestosRetenidoAgregar
            // 
            this.ToolBarImpuestosRetenidoAgregar.DisplayName = "Agregar";
            this.ToolBarImpuestosRetenidoAgregar.DrawText = true;
            this.ToolBarImpuestosRetenidoAgregar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_agregar_archivo;
            this.ToolBarImpuestosRetenidoAgregar.Name = "ToolBarImpuestosRetenidoAgregar";
            this.ToolBarImpuestosRetenidoAgregar.Text = "Agregar";
            this.ToolBarImpuestosRetenidoAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarImpuestosRetenidoQuitar
            // 
            this.ToolBarImpuestosRetenidoQuitar.DisplayName = "Quitar";
            this.ToolBarImpuestosRetenidoQuitar.DrawText = true;
            this.ToolBarImpuestosRetenidoQuitar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_delete_file;
            this.ToolBarImpuestosRetenidoQuitar.Name = "ToolBarImpuestosRetenidoQuitar";
            this.ToolBarImpuestosRetenidoQuitar.Text = "Quitar";
            this.ToolBarImpuestosRetenidoQuitar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ViewComprobante4Fiscal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1430, 813);
            this.Controls.Add(this.Contenedor);
            this.Controls.Add(this.PanelPago);
            this.Controls.Add(this.PanelGeneral);
            this.Controls.Add(this.RadCommandBar1);
            this.Name = "ViewComprobante4Fiscal";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ViewComprobante4Fiscal";
            this.Load += new System.EventHandler(this.ViewComprobante4Fiscal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelGeneral)).EndInit();
            this.PanelGeneral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ComprobanteConcepto)).EndInit();
            this.ComprobanteConcepto.ResumeLayout(false);
            this.PageViewComprobante.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Presicion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmisionField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaCertifica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoComprobante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbFolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboSerie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDocumento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDocumento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMoneda.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMoneda.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMoneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMetodoPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMetodoPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMetodoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCuentaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCondiciones.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCondiciones.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboCondiciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumRegIdTrib)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCfdi.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCfdi.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCfdi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbReceptorRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboReceptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbLugarDeExpedicion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel16)).EndInit();
            this.PageViewConcepto.ResumeLayout(false);
            this.PageViewConcepto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar4)).EndInit();
            this.RadCommandBar4.ResumeLayout(false);
            this.RadCommandBar4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CboConceptos.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboConceptos.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboConceptos)).EndInit();
            this.PageViewCfdiRelacionado.ResumeLayout(false);
            this.PageViewCfdiRelacionado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCfdiRelacionados.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridCfdiRelacionados)).EndInit();
            this.GridCfdiRelacionados.ResumeLayout(false);
            this.GridCfdiRelacionados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCfdiRelacionadoIncluir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelPago)).EndInit();
            this.PanelPago.ResumeLayout(false);
            this.PanelPago.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxbMonto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAutoSuma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonSPEI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPagoP.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPagoP.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboFormaPagoP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboNomBancoOrdExt.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboNomBancoOrdExt.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboNomBancoOrdExt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSelloPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCadenaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCertificadoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoCadenaPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoCadenaPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoCadenaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCtaBeneficiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRfcCtaBeneficiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCtaOrdenante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRfcEmisorCtaOrigen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumOperacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTipoCambioDR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMonedaP.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMonedaP.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboMonedaP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboPagoDoctosRelacionados.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboPagoDoctosRelacionados.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboPagoDoctosRelacionados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Contenedor)).EndInit();
            this.Contenedor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PanelDoctosRealcionados)).EndInit();
            this.PanelDoctosRealcionados.ResumeLayout(false);
            this.PanelDoctosRealcionados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDoctosRelacionados.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDoctosRelacionados)).EndInit();
            this.GridDoctosRelacionados.ResumeLayout(false);
            this.GridDoctosRelacionados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelImpuestos)).EndInit();
            this.PanelImpuestos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPageView2)).EndInit();
            this.radPageView2.ResumeLayout(false);
            this.PageImpuestosTralados.ResumeLayout(false);
            this.PageImpuestosTralados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridImpuestosTraslados.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridImpuestosTraslados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar5)).EndInit();
            this.PageImpuestosRetenidos.ResumeLayout(false);
            this.PageImpuestosRetenidos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridImpuestosRetenidos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridImpuestosRetenidos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar1;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement1;
        internal Telerik.WinControls.UI.CommandBarStripElement CommandComprobanteFiscal;
        internal Telerik.WinControls.UI.CommandBarSplitButton ToolBarEmisor;
        internal Telerik.WinControls.UI.CommandBarSeparator Separador;
        internal Telerik.WinControls.UI.CommandBarLabel ToolLabelStatus;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarStatus;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarNew;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarCopy;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarSave;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarRefresh;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarCreate;
        private Telerik.WinControls.UI.CommandBarButton ToolBarCancelar;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarPdf;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarXml;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarEmail;
        internal Telerik.WinControls.UI.CommandBarSeparator Separator2;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarSeries;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator2;
        internal Telerik.WinControls.UI.CommandBarLabel ToolLabelUuid;
        private Telerik.WinControls.UI.CommandBarTextBox ToolBarTextBoxIdDocumento;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarClose;
        private Telerik.WinControls.UI.RadPanel PanelGeneral;
        internal Telerik.WinControls.UI.RadPageView ComprobanteConcepto;
        internal Telerik.WinControls.UI.RadPageViewPage PageViewConcepto;
        internal Telerik.WinControls.UI.RadGridView GridConceptos;
        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar4;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement4;
        internal Telerik.WinControls.UI.CommandBarStripElement CommandConceptos;
        internal Telerik.WinControls.UI.CommandBarLabel ToolBarLabelProducto;
        private Telerik.WinControls.UI.CommandBarHostItem ToolBarHostConceptos;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarConceptoAgregar;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarConceptoQuitar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarConceptoButtonNuevo;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarConceptoBuscar;
        internal Telerik.WinControls.UI.RadPageViewPage PageViewCfdiRelacionado;
        internal Telerik.WinControls.UI.RadGridView GridCfdiRelacionados;
        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar2;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement2;
        internal Telerik.WinControls.UI.CommandBarStripElement CommandCfdiRelacionado;
        private Telerik.WinControls.UI.CommandBarHostItem ToolBarHostItemIncluir;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        internal Telerik.WinControls.UI.CommandBarLabel CommandBarLabel1;
        internal Telerik.WinControls.UI.CommandBarDropDownList ToolBarCfdiTipoRelacion;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarButton ToolBarCfdiBuscar;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarCfdiAgregar;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarCfdiQuitar;
        private Telerik.WinControls.UI.RadPageViewPage PageViewComprobante;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadMultiColumnComboBox CboConceptos;
        private Telerik.WinControls.UI.RadCheckBox ChkCfdiRelacionadoIncluir;
        private Telerik.WinControls.UI.RadSpinEditor Presicion;
        internal Telerik.WinControls.UI.RadLabel RadLabel9;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEmisionField;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaCertifica;
        internal Telerik.WinControls.UI.RadLabel RadLabel1;
        internal Telerik.WinControls.UI.RadLabel RadLabel2;
        internal Telerik.WinControls.UI.RadLabel RadLabel3;
        internal Telerik.WinControls.UI.RadSplitButton TipoComprobante;
        internal Telerik.WinControls.UI.RadTextBox TxbFolio;
        internal Telerik.WinControls.UI.RadDropDownList CboSerie;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboDocumento;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboMoneda;
        internal Telerik.WinControls.UI.RadTextBox TxbTipoCambio;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboMetodoPago;
        internal Telerik.WinControls.UI.RadTextBox TxbCuentaPago;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboFormaPago;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboCondiciones;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboResidenciaFiscal;
        internal Telerik.WinControls.UI.RadTextBox TxbNumRegIdTrib;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboUsoCfdi;
        internal Telerik.WinControls.UI.RadTextBox TxbReceptorRFC;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboReceptor;
        internal Telerik.WinControls.UI.RadLabel RadLabel7;
        internal Telerik.WinControls.UI.RadLabel RadLabel8;
        internal Telerik.WinControls.UI.RadLabel RadLabel10;
        internal Telerik.WinControls.UI.RadLabel RadLabel21;
        internal Telerik.WinControls.UI.RadLabel RadLabel11;
        internal Telerik.WinControls.UI.RadTextBox TxbLugarDeExpedicion;
        internal Telerik.WinControls.UI.RadLabel RadLabel12;
        internal Telerik.WinControls.UI.RadLabel RadLabel20;
        internal Telerik.WinControls.UI.RadLabel RadLabel13;
        internal Telerik.WinControls.UI.RadLabel RadLabel19;
        internal Telerik.WinControls.UI.RadLabel RadLabel14;
        internal Telerik.WinControls.UI.RadLabel RadLabel18;
        internal Telerik.WinControls.UI.RadLabel RadLabel15;
        internal Telerik.WinControls.UI.RadLabel RadLabel17;
        internal Telerik.WinControls.UI.RadLabel RadLabel16;
        private Telerik.WinControls.UI.RadPanel PanelPago;
        private Telerik.WinControls.UI.RadMaskedEditBox TxbMonto;
        private Telerik.WinControls.UI.RadCheckBox ChkAutoSuma;
        private Telerik.WinControls.UI.RadMultiColumnComboBox CboPagoDoctosRelacionados;
        private Telerik.WinControls.UI.RadButton ButtonSPEI;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboFormaPagoP;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboNomBancoOrdExt;
        internal Telerik.WinControls.UI.RadLabel RadLabel4;
        internal Telerik.WinControls.UI.RadTextBox TxbSelloPago;
        internal Telerik.WinControls.UI.RadLabel RadLabel5;
        internal Telerik.WinControls.UI.RadTextBox TxbCadenaPago;
        internal Telerik.WinControls.UI.RadLabel RadLabel6;
        internal Telerik.WinControls.UI.RadTextBox TxbCertificadoPago;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboTipoCadenaPago;
        internal Telerik.WinControls.UI.RadLabel RadLabel22;
        internal Telerik.WinControls.UI.RadLabel RadLabel23;
        internal Telerik.WinControls.UI.RadTextBox TxbCtaBeneficiario;
        internal Telerik.WinControls.UI.RadLabel RadLabel24;
        internal Telerik.WinControls.UI.RadTextBox TxbRfcCtaBeneficiario;
        internal Telerik.WinControls.UI.RadLabel RadLabel25;
        internal Telerik.WinControls.UI.RadTextBox TxbCtaOrdenante;
        internal Telerik.WinControls.UI.RadLabel RadLabel26;
        internal Telerik.WinControls.UI.RadLabel RadLabel27;
        internal Telerik.WinControls.UI.RadTextBox TxbRfcEmisorCtaOrigen;
        internal Telerik.WinControls.UI.RadLabel RadLabel28;
        internal Telerik.WinControls.UI.RadLabel RadLabel29;
        internal Telerik.WinControls.UI.RadLabel RadLabel30;
        internal Telerik.WinControls.UI.RadTextBox TxbNumOperacion;
        internal Telerik.WinControls.UI.RadLabel RadLabel31;
        internal Telerik.WinControls.UI.RadLabel RadLabel32;
        internal Telerik.WinControls.UI.RadLabel RadLabel33;
        internal Telerik.WinControls.UI.RadTextBox TxbTipoCambioDR;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboMonedaP;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaPago;
        private Telerik.WinControls.UI.RadSplitContainer Contenedor;
        private Telerik.WinControls.UI.SplitPanel PanelDoctosRealcionados;
        private Telerik.WinControls.UI.RadGridView GridDoctosRelacionados;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar3;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBarDoctosRelacionados;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarDoctosLabel;
        private Telerik.WinControls.UI.CommandBarHostItem ToolBarDoctosHostComprobantes;
        private Telerik.WinControls.UI.CommandBarButton ToolBarDoctosButtonAgregar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarDoctosButtonQuitar;
        private Telerik.WinControls.UI.CommandBarSeparator ToolBarDoctosSeparador1;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarDoctosLabelContador;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarDoctosContador;
        private Telerik.WinControls.UI.CommandBarSeparator ToolBarDoctosSeparador2;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarDoctosImportesPagadosLabel;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarDoctosImportePagado;
        private Telerik.WinControls.UI.CommandBarSeparator ToolBarDoctosSeparador3;
        private Telerik.WinControls.UI.CommandBarHostItem ToolBarDoctosHostAutoSuma;
        private Telerik.WinControls.UI.SplitPanel PanelImpuestos;
        private Telerik.WinControls.UI.RadPageView radPageView2;
        private Telerik.WinControls.UI.RadPageViewPage PageImpuestosTralados;
        internal Telerik.WinControls.UI.RadGridView GridImpuestosTraslados;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar5;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement5;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBarImpuestosTraslados;
        private Telerik.WinControls.UI.CommandBarButton ToolBarImpuestosTrasladoBuscar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarImpuestosTrasladoAgregar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarImpuestosTrasladoQuitar;
        private Telerik.WinControls.UI.RadPageViewPage PageImpuestosRetenidos;
        internal Telerik.WinControls.UI.RadGridView GridImpuestosRetenidos;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar6;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement6;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBarImpuestosRetenidos;
        private Telerik.WinControls.UI.CommandBarButton ToolBarImpuestosRetenidoBuscar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarImpuestosRetenidoAgregar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarImpuestosRetenidoQuitar;
    }
}
