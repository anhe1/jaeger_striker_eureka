﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Helpers;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.Enums;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2;
using Jaeger.Aplication.Comprobantes;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;

namespace Jaeger.Views {
    public partial class ViewComprobantesBackup : RadForm {
        private BackgroundWorker consulta;
        private SqlSugarComprobanteFiscal back = new SqlSugarComprobanteFiscal(ConfigService.Synapsis.RDS.Edita);
        private RadButtonElement bttnSelectPath = new RadButtonElement();
        private List<ViewModelBackup> datos = new List<ViewModelBackup>();
        private List<ViewModelBackupComprobante> datosDescarga = new List<ViewModelBackupComprobante>();
        private bool showCellBorders = false;
        private DashStyle lineStyle = DashStyle.Dot;
        private Color lineColor = Color.Black;
        private EnumCfdiSubType subtipo;

        public ViewComprobantesBackup() {
            InitializeComponent();
        }

        private void ViewComprobantesBackup_Load(object sender, EventArgs e) {
            this.GridData.AllowAddNewRow = false;
            this.GridData.TableElement.RowHeight = 40;
            this.GridData.ShowGroupPanel = false;
            this.GridData.ReadOnly = true;
            this.GridData.MasterTemplate.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            this.GridData.Relations.AddSelfReference(this.GridData.MasterTemplate, "Index", "Parent");
            this.GridData.TableElement.ShowSelfReferenceLines = true;
            this.GridData.EnableSorting = false;

            this.ToolBarLog.HostedItem = this.ChkCrearLog.ButtonElement;

            this.TxbPath.CrearBoton(bttnSelectPath, "Seleccione");
            this.bttnSelectPath.Click += new EventHandler(this.BttnSelectPath_Click);
            this.ButtonDescarga.Enabled = false;

            this.TxbPath.Enabled = false;
            this.CommandBarDescarga.OverflowButton.Visibility = ElementVisibility.Collapsed;
            this.consulta = new BackgroundWorker { WorkerSupportsCancellation = true };
            this.consulta.DoWork += new DoWorkEventHandler(this.Consulta_DoWork);
            this.consulta.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.Consulta_RunWorkerCompleted);

            if (!(ConfigService.Piloto.Roles[0] as Rol).Name.Contains("administrador")) {
                this.ToolBarTipoComprobante.Items.Remove(this.ButtonComprobanteNomina);
            }

        }

        private void ButtonTipoComprobante_Click(object sender, EventArgs e) {
            RadMenuItem button = sender as RadMenuItem;
            if ((button is RadMenuItem))
                this.ToolBarTipoComprobante.DefaultItem = (RadMenuItem)sender;
            else
                return;

            if (button.Tag.ToString() == "Nomina") {
                this.ButtonFechaTimbre.Visibility = ElementVisibility.Collapsed;
                this.ButtonFechaValidacion.Visibility = ElementVisibility.Collapsed;
                this.subtipo = EnumCfdiSubType.Nomina;
            }
            else if (button.Tag.ToString() == "Recibido") {
                this.ButtonFechaTimbre.Visibility = ElementVisibility.Visible;
                this.ButtonFechaValidacion.Visibility = ElementVisibility.Visible;
                this.subtipo = EnumCfdiSubType.Recibido;
            }
            else if (button.Tag.ToString() == "Emitido") {
                this.ButtonFechaTimbre.Visibility = ElementVisibility.Visible;
                this.ButtonFechaValidacion.Visibility = ElementVisibility.Collapsed;
                this.subtipo = EnumCfdiSubType.Emitido;
            }

            if ((this.ToolBarPorFecha.Text == "Selecciona"))
                return;
            this.RadCommandBar1.Enabled = false;
            this.Espera.StartWaiting();
            this.consulta.RunWorkerAsync();
        }

        private void ButtonPorFechaDe_Click(object sender, EventArgs e) {
            RadMenuItem button = sender as RadMenuItem;
            if ((button is RadMenuItem)) {
                this.ToolBarPorFecha.DefaultItem = (RadMenuItem)sender;
                this.ToolBarPorFecha.Tag = button.Tag;
            }
            else
                return;

            if ((this.ToolBarTipoComprobante.Text == "Selecciona"))
                return;
            this.RadCommandBar1.Enabled = false;
            this.Espera.StartWaiting();
            this.consulta.RunWorkerAsync();
        }

        private void BttnSelectPath_Click(object sender, EventArgs e) {
            this.TxbPath.Text = Extensions.DialogFolderOpen(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
        }

        private void ButtonDescarga_Click(object sender, EventArgs e) {
            using (Waiting2Form espera = new Waiting2Form(this.DescargaBackup)) {
                espera.Text = "Descargando ...";
                espera.ShowDialog();
            }
        }

        private void ToolBarCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consulta_DoWork(object sender, DoWorkEventArgs e) {
            SqlSugarComprobanteFiscal sql = new SqlSugarComprobanteFiscal(ConfigService.Synapsis.RDS.Edita);
            this.datos = sql.GetInfoBackUp(this.subtipo, (EnumCfdiDates)Enum.Parse(typeof(EnumCfdiDates), ToolBarPorFecha.Tag.ToString()));
        }

        private void Consulta_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.GridData.DataSource = this.datos;
            this.Espera.StopWaiting();
            this.RadCommandBar1.Enabled = true;
        }

        private void Descargar_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            this.LabelResult.Text = string.Concat("Procesando: ", e.ProgressPercentage.ToString(), " elementos");
        }

        private void GridData_CellFormatting(object sender, CellFormattingEventArgs e) {
            GridDataCellElement dataCell = e.CellElement as GridDataCellElement;

            if (dataCell.ColumnInfo.Name == "Name") {
                GridViewDataRowInfo dataRow = dataCell.RowInfo as GridViewDataRowInfo;
                dataCell.ImageAlignment = ContentAlignment.MiddleLeft;
                if (dataRow == null)
                    return;
                string valueType = Convert.ToString(dataRow.Cells["Type"].Value).ToUpperInvariant();

                if (valueType.Contains("FILE") || valueType.Contains("DOCUMENT"))
                    dataCell.Image = this.ImageList1.Images[2];
                else if (dataRow.IsExpanded)
                    dataCell.Image = this.ImageList1.Images[1];
                else
                    dataCell.Image = this.ImageList1.Images[0];

                dataCell.TextImageRelation = TextImageRelation.ImageBeforeText;
            }
            else {
                dataCell.ResetValue(LightVisualElement.ImageProperty, ValueResetFlags.Local);
                dataCell.ResetValue(LightVisualElement.ImageAlignmentProperty, ValueResetFlags.Local);
                dataCell.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                dataCell.ResetValue(LightVisualElement.ImageLayoutProperty, ValueResetFlags.Local);
            }
        }

        private void GridData_ViewCellFormatting(object sender, CellFormattingEventArgs e) {
            if (e.Row is GridViewDataRowInfo) {
                if (showCellBorders)
                    e.CellElement.ResetValue(GridDataCellElement.DrawBorderProperty, ValueResetFlags.Local);
                else
                    e.CellElement.DrawBorder = false;

                GridDataCellElement cell = e.CellElement as GridDataCellElement;
                if (cell != null && cell.SelfReferenceLayout != null) {
                    foreach (RadElement element in cell.SelfReferenceLayout.StackLayoutElement.Children) {
                        GridLinkItem linkItem = element as GridLinkItem;
                        if (linkItem != null) {
                            linkItem.LineStyle = lineStyle;
                            linkItem.ForeColor = lineColor;
                        }
                        GridExpanderItem expanderItem = element as GridExpanderItem;
                        if (expanderItem != null) {
                            expanderItem.LinkLineStyle = lineStyle;
                            expanderItem.LinkLineColor = lineColor;
                        }
                    }
                }
            }
        }

        private void GridData_RowsChanged(object sender, GridViewCollectionChangedEventArgs e) {
            this.ButtonDescarga.Enabled = this.datos.Count > 0;
            this.bttnSelectPath.Enabled = this.datos.Count > 0;
            this.TxbPath.Enabled = this.datos.Count > 0;
        }

        private void DescargaBackup() {
            if (this.GridData.CurrentRow != null) {
                ViewModelBackup modelBackup = this.GridData.CurrentRow.DataBoundItem as ViewModelBackup;
                if (modelBackup != null)
                    this.datosDescarga = this.back.GetBackupFiles(this.subtipo, modelBackup.Year, modelBackup.Month, (EnumCfdiDates)Enum.Parse(typeof(EnumCfdiDates), this.ToolBarPorFecha.Tag.ToString()));
                if (this.datosDescarga != null) {
                    string[] arrayMeses = Enum.GetNames(typeof(EnumMonthsOfYear));
                    string rutaDescarga = this.TxbPath.Text;
                    string nuevaCarpeta = this.TxbPath.Text;

                    BindingList<Util.Helpers.DescargaElemento> descargas = new BindingList<Util.Helpers.DescargaElemento>();
                    foreach (var item in this.datosDescarga) {
                        string nivel = (item.Nombre.Trim().Length > 0 ? item.Nombre : item.RFC);

                        if ((item.Categoria != null))
                            nivel = string.Concat(item.Categoria, @"\", nivel);

                        if ((item.Nomina != null))
                            nivel = string.Concat(item.Nomina, @"\", nivel);

                        nivel = string.Concat(Enum.GetName(typeof(EnumCfdiSubType), item.SubTipo), @"\", nivel);

                        // ' si la ruta de la nueva carpeta no existe, entonces la creamos
                        nuevaCarpeta = Path.Combine(rutaDescarga, item.Ejercicio.ToString(), arrayMeses[item.Periodo], nivel);
                        if ((Directory.Exists(nuevaCarpeta) == false)) {
                            try {
                                Directory.CreateDirectory(nuevaCarpeta);
                            }
                            catch (Exception ex) {
                                Console.WriteLine(ex.Message);
                                nuevaCarpeta = rutaDescarga;
                            }
                        }

                        if (HelperValidacion.ValidaUrl(item.UrlXml))
                            descargas.Add(new Util.Helpers.DescargaElemento { URL = item.UrlXml, KeyName = Path.Combine(nuevaCarpeta, Path.GetFileName(item.UrlXml)), Contenido = item._cfdi_save });
                        else
                            descargas.Add(new Util.Helpers.DescargaElemento { URL = item.KeyName, KeyName = Path.Combine(nuevaCarpeta, Path.GetFileName(item.KeyName) + ".xml"), Contenido = item._cfdi_save });
                        if (HelperValidacion.ValidaUrl(item.UrlPdf))
                            descargas.Add(new Util.Helpers.DescargaElemento { URL = item.UrlPdf, KeyName = Path.Combine(nuevaCarpeta, Path.GetFileName(item.UrlPdf)) });

                    }

                    if (this.radCheckBox1.Checked) {
                        IEditaBucketService dwn = new Aplication.Comprobantes.EditaBucketService(ConfigService.Synapsis.Amazon.S3, ConfigService.Synapsis.ProveedorAutorizado.Certificacion.Production);
                        foreach (var item in descargas) {
                            if (HelperValidacion.ValidaUrl(item.URL) == true) {
                                bool result = dwn.Download(item.KeyName, item.URL);
                                if (result == false) {
                                    if (item.URL.ToLower().EndsWith(".xml")) {
                                        HelperFile.WriteFileText(item.KeyName, item.Contenido);
                                    }
                                }
                            }
                            else {
                                if (item.KeyName.ToLower().EndsWith(".xml")) {
                                    if (item.Contenido != null)
                                        HelperFile.WriteFileText(item.KeyName, item.Contenido);
                                }
                            }
                        }
                    }
                    else {
                        // descarga
                        Util.Helpers.DownloadExtended descarga = new Util.Helpers.DownloadExtended();
                        descarga.ProcessDownload += Descarga_ProcessDownload;
                        descarga.RunDownloadParallelSync(descargas, @"");
                    }
                }
            }
        }

        private void Descarga_ProcessDownload(object sender, Util.Helpers.DownloadChanged e) {
            this.LabelResult.Text = e.Caption;
        }
    }
}
