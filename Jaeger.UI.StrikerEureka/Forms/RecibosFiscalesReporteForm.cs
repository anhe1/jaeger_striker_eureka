﻿using System;
using System.Linq;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Telerik.WinControls.Enumerations;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2;
using Jaeger.Edita.Enums;
using Telerik.WinControls;
using System.Collections.Generic;
using System.Windows.Forms;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views {
    public partial class RecibosFiscalesReporteForm : RadForm {
        private SqlSugarComprobanteFiscal data;
        private BindingList<ViewModelReciboFiscal> datos;
        private BackgroundWorker consultar;
        private RadWaitingBarElement waitingElement;

        public RecibosFiscalesReporteForm(ref RadWaitingBarElement element) {
            InitializeComponent();
            this.waitingElement = element;
        }

        private void ViewReporteRecibosFiscales_Load(object sender, EventArgs e) {
            this.GridData.TelerikGridCommon();
            this.GridData.MasterTemplate.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            this.data = new SqlSugarComprobanteFiscal(ConfigService.Synapsis.RDS.Edita);

            foreach (string item in Enum.GetNames(typeof(EnumMonthsOfYear))) {
                RadMenuItem oButtonMonth = new RadMenuItem { Text = item };
                this.ToolBarButtonPeriodo.Items.Add(oButtonMonth);
                oButtonMonth.Click += new EventHandler(this.ToolBarButtonPeriodo_Click);
            }

            for (int anio = 2013; anio <= DateTime.Now.Year; anio = checked(anio + 1)) {
                RadMenuItem oButtonYear = new RadMenuItem() { Text = anio.ToString() };
                this.ToolBarButtonEjercicio.Items.Add(oButtonYear);
                oButtonYear.Click += new EventHandler(this.ToolBarButtonEjercicio_Click);
            }

            this.ToolBar.OverflowButton.CustomizeButtonMenuItem.Visibility = ElementVisibility.Collapsed;
            this.ToolBar.OverflowButton.AddRemoveButtonsMenuItem.Visibility = ElementVisibility.Collapsed;

            this.ToolBarButtonEjercicio.Text = DateTime.Now.Year.ToString();
            this.ToolBarButtonPeriodo.Text = Enum.GetName(typeof(EnumMonthsOfYear), DateTime.Now.Month);

            this.consultar = new BackgroundWorker();
            this.consultar.DoWork += ConsultarDoWork;
            this.consultar.RunWorkerCompleted += ConsultarRunWorkerCompleted;
        }

        private void ToolBarButtonPeriodo_Click(object sender, EventArgs e) {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem) {
                this.ToolBarButtonPeriodo.DefaultItem = button;
                this.ToolBarButtonPeriodo.Text = button.Text;
                this.ToolBarButtonPeriodo.Tag = button.Tag;
                this.ToolBarButtonPeriodo.PerformClick();
                this.ToolBarButtonPeriodo.PerformClick();
            }
        }

        private void ToolBarButtonEjercicio_Click(object sender, EventArgs e) {
            RadMenuItem button = sender as RadMenuItem;
            if (button is RadMenuItem) {
                this.ToolBarButtonEjercicio.DefaultItem = button;
                this.ToolBarButtonEjercicio.Text = button.Text;
                this.ToolBarButtonEjercicio.PerformClick();
            }
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            this.Waiting.StartWaiting();
            this.Waiting.Visible = true;
            this.ToolBar.Enabled = false;
            this.waitingElement.StartWaiting();
            this.waitingElement.Visibility = ElementVisibility.Visible;
            if (this.consultar.IsBusy)
                this.consultar.Dispose();
            this.consultar.RunWorkerAsync();
        }

        private void ToolBarButtonExportar_Click(object sender, EventArgs e) {
            TelerikGridExport exportar = new TelerikGridExport(this.GridData);
            exportar.ShowDialog();
        }

        private void ToolBarButtonFiltro_Click(object sender, EventArgs e) {
            this.GridData.ShowFilteringRow = this.ToolBarButtonFiltro.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false)
                this.GridData.FilterDescriptors.Clear();
        }

        private void ToolBarButtonDescargar_Click(object sender, EventArgs e) {
            SaveFileDialog openFileDialog = new SaveFileDialog() { FileName = "PrePoliza.csv", Filter = "*.csv|*.CSV", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
            if (openFileDialog.ShowDialog() != DialogResult.OK)
                return;
            this.ToolBarButtonDescargar.Tag = openFileDialog.FileName;
            this.Reporte();
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        #region consulta de datos

        private void ConsultarDoWork(object sender, DoWorkEventArgs e) {
            this.datos = this.data.GetRecibosFiscales((int)(Enum.Parse(typeof(EnumMonthsOfYear), this.ToolBarButtonPeriodo.Text)), int.Parse(this.ToolBarButtonEjercicio.Text), 0, ConfigService.Synapsis.Empresa.RFC);
        }

        private void ConsultarRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.GridData.DataSource = datos;
            this.Waiting.Visible = false;
            this.Waiting.StopWaiting();
            this.ToolBar.Enabled = true;
            this.waitingElement.StopWaiting();
            this.waitingElement.Visibility = ElementVisibility.Collapsed;
            this.consultar.Dispose();
        }

        #endregion

        private void GridData_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e) {
            this.ToolBarButtonExportar.Enabled = this.GridData.Rows.Count > 0;
            this.ToolBarButtonDescargar.Enabled = this.GridData.Rows.Count > 0;
        }

        private List<List<Layout.Entities.RF.Partida2>> ConfiguraCuentas() {
            List<List<Layout.Entities.RF.Partida2>> cuentas = new List<List<Layout.Entities.RF.Partida2>>();
            // plan cero
            var planCero = new List<Layout.Entities.RF.Partida2>()
            {
                new Layout.Entities.RF.Partida2(){ Campo2 = "--------------------"},
                new Layout.Entities.RF.Partida2(){ Campo2 = "--------------------"},
                new Layout.Entities.RF.Partida2(){ Campo2 = "--------------------"},
                new Layout.Entities.RF.Partida2(){ Campo2 = "--------------------"},
                new Layout.Entities.RF.Partida2(){ Campo2 = "--------------------"},
            };
            cuentas.Add(planCero);

            // plan flexible
            var planFlexible = new List<Layout.Entities.RF.Partida2>()
            {
                new Layout.Entities.RF.Partida2(){ Campo2 = "1105-0001-0001-0040", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "1105-0001-0002-0040", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "5102-0001-0040-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2110-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2105-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2105-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" }
            };
            cuentas.Add(planFlexible);

            // a la medida
            var planAlaMedida = new List<Layout.Entities.RF.Partida2>()
            {
                new Layout.Entities.RF.Partida2(){ Campo2 = "1105-0001-0001-0041", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "1105-0001-0002-0041", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "5102-0001-0041-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2110-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
            };
            cuentas.Add(planAlaMedida);

            // a la medida
            var planEconomico = new List<Layout.Entities.RF.Partida2>()
            {
                new Layout.Entities.RF.Partida2(){ Campo2 = "1105-0001-0001-0039", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "1105-0001-0002-0039", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "5102-0001-0039-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2110-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
            };
            cuentas.Add(planEconomico);

            // plan clasico n.r.
            var planClasico = new List<Layout.Entities.RF.Partida2>()
            {
                new Layout.Entities.RF.Partida2(){ Campo2 = "1105-0001-0001-0038", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "1105-0001-0002-0038", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "5102-0001-0038-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2110-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
            };
            cuentas.Add(planClasico);

            // plan auto clasico
            var planAutoClasico = new List<Layout.Entities.RF.Partida2>()
            {
                new Layout.Entities.RF.Partida2(){ Campo2 = "1105-0001-0001-0042", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "1105-0001-0002-0042", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "5102-0001-0042-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2110-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
            };
            cuentas.Add(planAutoClasico);

            // plan estrena ya
            var planEstrenaYa = new List<Layout.Entities.RF.Partida2>()
            {
                new Layout.Entities.RF.Partida2(){ Campo2 = "1105-0001-0001-0043", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "1105-0001-0002-0043", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "5102-0001-0043-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2110-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
            };
            cuentas.Add(planEstrenaYa);

            // plan afasa 3
            var planAfasa3 = new List<Layout.Entities.RF.Partida2>()
            {
                new Layout.Entities.RF.Partida2(){ Campo2 = "1105-0001-0001-0044", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "1105-0001-0002-0043", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "5102-0001-0043-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2110-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
            };
            cuentas.Add(planAfasa3);

            // afasa rapido 60 A
            var planAfasaRapido = new List<Layout.Entities.RF.Partida2>() {
                new Layout.Entities.RF.Partida2(){ Campo2 = "1104-0002-0001-0001", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "4101-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2104-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2105-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2105-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" }
            };
            cuentas.Add(planAfasaRapido);

            // afasa comodo 60 C
            var planAfasaComodo = new List<Layout.Entities.RF.Partida2>() {
                new Layout.Entities.RF.Partida2(){ Campo2 = "1104-0002-0001-0002", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "4101-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2104-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2105-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" }
            };
            cuentas.Add(planAfasaComodo);

            // afa flexible 60 B
            var planAfasaFlexible60B = new List<Layout.Entities.RF.Partida2>() {
                new Layout.Entities.RF.Partida2(){ Campo2 = "1104-0002-0001-0003", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "4101-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2104-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Layout.Entities.RF.Partida2(){ Campo2 = "2105-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" }
            };
            cuentas.Add(planAfasaFlexible60B);

            return cuentas;
        }

        private void Reporte() {
            var inicioCFDI = new Layout.Entities.RF.Partida2() { Campo3 = "INICIO_CFDI" };
            var finCFDI = new Layout.Entities.RF.Partida2() { Campo3 = "FIN_CFDI" };

            List<List<Layout.Entities.RF.Partida2>> Cuentas1 = this.ConfiguraCuentas();
            List<Layout.Entities.RF.Partida2> resultado = new List<Layout.Entities.RF.Partida2>();
            var _seleccionados = this.GridData.ChildRows.Select(it => it.DataBoundItem as ViewModelReciboFiscal).ToList();
            for (int i = 0; i < 32; i++) {
                List<ViewModelReciboFiscal> datos2 = _seleccionados.Where(it => it.Dia == i).OrderBy(it => it.NoPlan).Select(it => it as ViewModelReciboFiscal).ToList();
                if (datos2 != null) {
                    if (datos2.Count > 0) {
                        List<int> minimo = datos2.Select(it => Convert.ToInt32(it.Folio)).ToList();
                        var descripcion = new Layout.Entities.RF.Partida2() {
                            Campo2 = i.ToString(),
                            Campo1 = "Dr",
                            Campo3 = string.Format("RFISC-{0} A {1} SIST. VARIOS ({2})", minimo.Min<int>().ToString(), minimo.Max<int>().ToString(), datos2[0].FecEmision.Value.ToString("dd/MM/yy"))
                        };

                        resultado.Add(descripcion);
                        string Campo3 = descripcion.Campo3;
                        descripcion = null;
                        for (int plan = 0; plan < 11; plan++) {
                            List<Layout.Entities.RF.Partida2> tipo1 = datos2.Where(it => it.NoPlan == plan).OrderBy(it => Convert.ToInt32(it.Folio)).Select(it => new Jaeger.Layout.Entities.RF.Partida2 {
                                Campo3 = it.FecEmision.Value.ToString("dd/MM/yy"),
                                Campo6 = it.EmisorRFC,
                                Campo7 = it.ReceptorRfc,
                                Campo8 = it.Total.ToString(),
                                Campo9 = it.Uuid
                            }).ToList();


                            if (tipo1.Count > 0) {
                                decimal iva = datos2.Where(it => it.NoPlan == plan).Sum(it => it.Iva);
                                decimal total = datos2.Where(it => it.NoPlan == plan).Sum(it => it.SubTotal);

                                Layout.Entities.RF.Partida2 uno = (Layout.Entities.RF.Partida2)Cuentas1[plan][0].Clone();
                                uno.Campo4 = Campo3;
                                uno.Campo6 = (iva + total).ToString();
                                uno.Campo7 = "0";
                                resultado.Add(uno);
                                resultado.Add(inicioCFDI);
                                resultado.AddRange(tipo1);
                                resultado.Add(finCFDI);

                                Layout.Entities.RF.Partida2 dos = (Layout.Entities.RF.Partida2)Cuentas1[plan][1].Clone();
                                resultado.Add(dos);
                                dos.Campo4 = Campo3;
                                dos.Campo6 = "0";
                                dos.Campo7 = total.ToString();
                                resultado.Add(inicioCFDI);
                                resultado.AddRange(tipo1);
                                resultado.Add(finCFDI);

                                Layout.Entities.RF.Partida2 tres = (Layout.Entities.RF.Partida2)Cuentas1[plan][2].Clone();
                                tres.Campo4 = Campo3;
                                tres.Campo6 = "0";
                                tres.Campo7 = iva.ToString();
                                resultado.Add(tres);
                                resultado.Add(inicioCFDI);
                                resultado.AddRange(tipo1);
                                resultado.Add(finCFDI);

                                Layout.Entities.RF.Partida2 cuatro = (Layout.Entities.RF.Partida2)Cuentas1[plan][3].Clone();
                                cuatro.Campo4 = Campo3;
                                cuatro.Campo6 = iva.ToString();
                                cuatro.Campo7 = "0";
                                resultado.Add(cuatro);
                                resultado.Add(inicioCFDI);
                                resultado.AddRange(tipo1);
                                resultado.Add(finCFDI);

                                if (Cuentas1[plan].Count > 4) {
                                    Layout.Entities.RF.Partida2 cinco = (Layout.Entities.RF.Partida2)Cuentas1[plan][4].Clone();
                                    cinco.Campo4 = Campo3;
                                    cinco.Campo6 = "0";
                                    cinco.Campo7 = iva.ToString();
                                    resultado.Add(cinco);
                                    resultado.Add(inicioCFDI);
                                    resultado.AddRange(tipo1);
                                    resultado.Add(finCFDI);
                                }
                            }
                        }
                    }
                }
            }
            var prueba = new Layout.Helpers.HelperRecibosFiscales();
            if (prueba.Exportar(resultado.ToArray(), (string)this.ToolBarButtonDescargar.Tag) == false)
                RadMessageBox.Show(this, "No es posible crear el archivo de salida. Es posible que el archivo se encuentre bloqueado.", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
        }
    }
}
