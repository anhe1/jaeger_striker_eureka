﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Helpers;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;
using Jaeger.Edita.V2;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Catalogos.Repositories;
using Jaeger.Catalogos.Entities;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views
{
    public partial class ViewCatalogoProdServBuscar : RadForm
    {
        //private SQLiteCatalogoProdServs catalogo = new SQLiteCatalogoProdServs(new Jaeger.Edita.V2.Empresa.Entities.DataBase { Base = @"C:\Jaeger\Jaeger.Catalogos\Catalogos.sqlite" });
        private ProdServsCatalogo catalogo = new ProdServsCatalogo();
        private CatalogoConceptosRecientes catalogoRecientes = new CatalogoConceptosRecientes();
        private ViewModelComprobanteConcepto seleccionado;
        private SqlSugarModelos data;

        public ViewCatalogoProdServBuscar()
        {
            InitializeComponent();
        }

        private void ViewCatalogoProdServBuscar_Load(object sender, EventArgs e)
        {
            this.GridData.TelerikGridCommon();
            this.data = new SqlSugarModelos(ConfigService.Synapsis.RDS.Edita);
            this.CboDivision.DisplayMember = "Descripcion";
            this.CboDivision.ValueMember = "Clave";
            this.CboDivision.EditorControl.FilterDescriptors.Add(new FilterDescriptor("Name", FilterOperator.Contains, String.Empty));
            this.CboDivision.AutoSizeDropDownToBestFit = true;
            this.CboDivision.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.CboDivision.DropDownStyle = RadDropDownStyle.DropDown;
            this.CboDivision.SelectedValueChanged += CboDivision_SelectedIndexChanged;

            this.CboGrupo.DisplayMember = "Descripcion";
            this.CboGrupo.ValueMember = "Clave";
            this.CboGrupo.AutoSizeDropDownToBestFit = true;
            this.CboGrupo.AutoFilter = true;
            this.CboGrupo.EditorControl.FilterDescriptors.Add(new FilterDescriptor("Nombre", FilterOperator.StartsWith, String.Empty));
            this.CboGrupo.DropDownStyle = RadDropDownStyle.DropDown;
            this.CboGrupo.SelectedValueChanged += CboGrupo_SelectedValueChanged;

            this.CboClase.DisplayMember = "Descripcion";
            this.CboClase.ValueMember = "Clave";
            this.CboClase.AutoSizeDropDownToBestFit = true;
            this.CboClase.AutoFilter = true;
            this.CboClase.EditorControl.FilterDescriptors.Add(new FilterDescriptor("Nombre", FilterOperator.StartsWith, String.Empty));
            this.CboClase.DropDownStyle = RadDropDownStyle.DropDown;
            this.CboClase.SelectedValueChanged += CboClase_SelectedValueChanged;

            this.TxbClaveDeClase.DataBindings.Add("Text", this.CboClase, "SelectedValue");

            this.BttnTipoProductos.Click += new EventHandler(this.SetDefaultItem);
            this.BttnTipoServicios.Click += new EventHandler(this.SetDefaultItem);

            this.TxbDescripcion.TextChanged += TxbDescripcion_TextChanged;

            this.catalogo.Load();
        }

        private void TxbDescripcion_TextChanged(object sender, EventArgs e)
        {
            if (this.TxbDescripcion.Text.Length > 5)
            {
                //this.GridData.DataSource = this.catalogo.GetSearch(this.TxbDescripcion.Text);
                //this.GridData.DataSource = this.catalogo.Search(this.TxbDescripcion.Text);
                this.GridData.DataSource = this.catalogo.Productos(this.TxbDescripcion.Text);
            }
        }

        private void txbPorCatalogo_TextChanged(object sender, EventArgs e)
        {
            if (this.txbPorCatalogo.Text.Length > 4)
                this.GridProductoCatalogo.DataSource = this.data.GetListConceptos(Edita.V2.Almacen.Enums.EnumAlmacen.PT, this.txbPorCatalogo.Text);
        }

        private void SetDefaultItem(object sender, EventArgs e)
        {
            RadMenuItem item = sender as RadMenuItem;
            if (item != null)
            {
                this.CboTipoProdServs.DefaultItem = item;
                this.CboTipoProdServs.ImageIndex = item.ImageIndex;
                this.CboTipoProdServs.Text = item.Text;
                if (item.Name == this.BttnTipoProductos.Name)
                {
                    //this.CboDivision.DataSource = this.catalogo.GetListProductos();
                    this.CboDivision.DataSource = this.catalogo.Productos();
                }
                else if (item.Name == this.BttnTipoServicios.Name)
                {
                    //this.CboDivision.DataSource = this.catalogo.GetListServicios();
                    this.CboDivision.DataSource = this.catalogo.Servicios();
                }
            }
        }

        private void CboDivision_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.CboDivision.SelectedItem != null)
            {
                GridViewRowInfo temporal = this.CboDivision.SelectedItem as GridViewRowInfo;
                if (temporal != null)
                {
                    var item = temporal.DataBoundItem as ClaveProdServ;
                    if (item != null)
                        this.CboGrupo.DataSource = this.catalogo.Grupo(item.Clave);
                    //if (item != null)
                    //    this.CboGrupo.DataSource = this.catalogo.GetListGrupo(item.Clave);
                }
            }
        }

        private void CboGrupo_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.CboGrupo.SelectedItem != null)
            {
                //this.CboClase.DataSource = this.catalogo.GetListClase(this.CboGrupo.SelectedValue.ToString());
                this.CboClase.DataSource = this.catalogo.Clases(this.CboGrupo.SelectedValue.ToString());
            }
        }

        private void CboClase_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.CboClase.SelectedItem != null)
            {
                //this.CboSubClase.DataSource = this.catalogo.GetList(this.CboClase.SelectedValue.ToString());
                this.CboSubClase.DataSource = this.catalogo.Clases(this.CboClase.SelectedValue.ToString());
            }
        }

        private void ButtonBuscar_Click(object sender, EventArgs e)
        {
            //this.GridData.DataSource = this.catalogo.GetSearch(this.TxbDescripcion.Text);
        }

        private void ButtonCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ButtonAsignar_Click(object sender, EventArgs e)
        {
            if (this.radPageView.SelectedPage == this.ViewPageBuscarPorCatalogo)
            {
                var producto1 = this.GridProductoCatalogo.CurrentRow.DataBoundItem as ViewModelComprobanteModelo;
                this.seleccionado = new ViewModelComprobanteConcepto();
                this.seleccionado.ClaveProdServ = (producto1.ClaveProdServ != null ? producto1.ClaveProdServ : "");
                this.seleccionado.ClaveUnidad = (producto1.ClaveUnidad != null ? producto1.ClaveUnidad : "");
                this.seleccionado.CtaPredial = (producto1.CtaPredial != null ? producto1.CtaPredial : "");
                this.seleccionado.Descripcion = producto1.Descripcion;
                this.seleccionado.NoIdentificacion = (producto1.NoIdentificacion != null ? producto1.NoIdentificacion : "");
                this.seleccionado.Unidad = (producto1.Unidad != null ? producto1.Unidad : "");
                this.seleccionado.ValorUnitario = producto1.Unitario;
                this.seleccionado.Impuestos = new System.ComponentModel.BindingList<ComprobanteConceptoImpuesto>();
                // impuestos trasladados
                if (producto1.FactorTrasladoIVA != null)
                {
                    if (producto1.FactorTrasladoIVA == "Tasa")
                        this.seleccionado.Impuestos.Add(new ComprobanteConceptoImpuesto { Impuesto = Enums.EnumImpuesto.IVA, TipoFactor = Enums.EnumFactor.Tasa, Tipo = Enums.EnumTipoImpuesto.Traslado, TasaOCuota = decimal.Parse(producto1.ValorTrasladoIVA.ToString()) });
                    if (producto1.FactorTrasladoIVA == "Exento")
                        this.seleccionado.Impuestos.Add(new ComprobanteConceptoImpuesto { Impuesto = Enums.EnumImpuesto.IVA, TipoFactor = Enums.EnumFactor.Exento, Tipo = Enums.EnumTipoImpuesto.Traslado, TasaOCuota = 0 });
                }
                if (producto1.FactorTrasladoIEPS != null)
                {
                    if (producto1.FactorTrasladoIEPS == "Tasa")
                        this.seleccionado.Impuestos.Add(new ComprobanteConceptoImpuesto { Impuesto = Enums.EnumImpuesto.IEPS, TipoFactor = Enums.EnumFactor.Tasa, Tipo = Enums.EnumTipoImpuesto.Traslado, TasaOCuota = decimal.Parse(producto1.ValorTrasladoIEPS.ToString()) });
                    if (producto1.FactorTrasladoIEPS == "Cuota")
                        this.seleccionado.Impuestos.Add(new ComprobanteConceptoImpuesto { Impuesto = Enums.EnumImpuesto.IEPS, TipoFactor = Enums.EnumFactor.Cuota, Tipo = Enums.EnumTipoImpuesto.Traslado, TasaOCuota = decimal.Parse(producto1.ValorTrasladoIEPS.ToString()) });
                }
                // impuestos retenidos
                if (producto1.ValorRetencionIVA != null)
                    this.seleccionado.Impuestos.Add(new ComprobanteConceptoImpuesto { Impuesto = Enums.EnumImpuesto.IVA, TipoFactor = Enums.EnumFactor.Tasa, Tipo = Enums.EnumTipoImpuesto.Retencion, TasaOCuota = decimal.Parse(producto1.ValorRetencionIVA.ToString()) });

                if (producto1.ValorRetecionISR != null)
                    this.seleccionado.Impuestos.Add(new ComprobanteConceptoImpuesto { Impuesto = Enums.EnumImpuesto.ISR, TipoFactor = Enums.EnumFactor.Tasa, Tipo = Enums.EnumTipoImpuesto.Retencion, TasaOCuota = decimal.Parse(producto1.ValorRetecionISR.ToString()) });

                if (producto1.FactorRetencionIEPS != null)
                {
                    if (producto1.FactorRetencionIEPS == "Tasa")
                        this.seleccionado.Impuestos.Add(new ComprobanteConceptoImpuesto { Impuesto = Enums.EnumImpuesto.IEPS, TipoFactor = Enums.EnumFactor.Tasa, Tipo = Enums.EnumTipoImpuesto.Retencion, TasaOCuota = decimal.Parse(producto1.ValorRetencionIEPS.ToString()) });
                    if (producto1.FactorRetencionIEPS == "Cuota")
                        this.seleccionado.Impuestos.Add(new ComprobanteConceptoImpuesto { Impuesto = Enums.EnumImpuesto.IEPS, TipoFactor = Enums.EnumFactor.Cuota, Tipo = Enums.EnumTipoImpuesto.Retencion, TasaOCuota = decimal.Parse(producto1.ValorRetencionIEPS.ToString()) });
                }
            }
            else if (this.radPageView.SelectedPage == this.PageViewBuscarTexto)
            {
                var producto2 = this.GridData.CurrentRow.DataBoundItem as ClaveProdServ;
                this.seleccionado = new ViewModelComprobanteConcepto();
                this.seleccionado.ClaveProdServ = producto2.Clave;
                this.seleccionado.Descripcion = producto2.Descripcion;
            }
            else
            {
                this.seleccionado = null;
            }
            this.Close();
        }

        public ViewModelComprobanteConcepto ShowForm()
        {
            this.ShowDialog();
            if (seleccionado == null)
                return null;
            return this.seleccionado;
        }

        
    }
}
