﻿using Jaeger.Views;

namespace Jaeger.Views
{
    partial class ViewComprobantesFiscales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewComprobantesFiscales));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn3 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn4 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn5 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn6 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn7 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn8 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn9 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition10 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn38 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn39 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn3 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn40 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn41 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn42 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn43 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn44 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn45 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn46 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn47 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn48 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn49 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn50 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn51 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn52 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn53 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn54 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn55 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn56 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn57 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn58 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn59 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn60 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn61 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn62 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewTextBoxColumn63 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn64 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn65 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn66 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn67 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn68 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewTextBoxColumn69 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn70 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn71 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn72 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn73 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn74 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn75 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn76 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn77 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn78 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn79 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn80 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn81 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition7 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn82 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn83 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn84 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn85 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn86 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn87 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn88 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn89 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn90 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn91 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn92 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn93 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn94 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn95 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn96 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn97 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn98 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn99 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn100 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn101 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn102 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn103 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition8 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn104 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn105 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn106 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn107 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn108 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn109 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn110 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn111 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition9 = new Telerik.WinControls.UI.TableViewDefinition();
            this.CommandBarRow = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabelPeriodo = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarPeriodo = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarLabelEjercicio = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarEjercicio = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.CommandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarNew = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarComprobanteNuevo = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarComprobantePago = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarComprobanteClonar = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarComprobanteSustituir = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarComprobanteLink = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarReciboCobro = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarReciboPago = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarEdit = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonEnviar = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarRefresh = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarFilter = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarAutoSum = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarButtonExport = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonExportarExcel = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonExportarExcelReporte1 = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarPrint = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.PrintAcuse = new Telerik.WinControls.UI.RadMenuItem();
            this.PrintComprobante = new Telerik.WinControls.UI.RadMenuItem();
            this.PrintValidacion = new Telerik.WinControls.UI.RadMenuItem();
            this.PrintLista = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonPrintEdoCuentaSimple = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonValidar = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarButtonValidarEstado = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonValidarSAT = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonPDFValidacion = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonProductos = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarLayOut = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarClose = new Telerik.WinControls.UI.CommandBarButton();
            this.CommandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.GridConceptos = new Telerik.WinControls.UI.GridViewTemplate();
            this.GridFiles = new Telerik.WinControls.UI.GridViewTemplate();
            this.GridComprobanteCfdiRelacionados = new Telerik.WinControls.UI.GridViewTemplate();
            this.GridComprobanteCfdiRelacionadosCfdiRelacionado = new Telerik.WinControls.UI.GridViewTemplate();
            this.GridContable = new Telerik.WinControls.UI.GridViewTemplate();
            this.GridComplementoPagos = new Telerik.WinControls.UI.GridViewTemplate();
            this.GridComplementoDoctoRelacionado = new Telerik.WinControls.UI.GridViewTemplate();
            this.GridRecepcionPagos = new Telerik.WinControls.UI.GridViewTemplate();
            this.GridNotasCredito = new Telerik.WinControls.UI.GridViewTemplate();
            this.MenuContextual = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.ContextMenuSeleccion = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuCopiar = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuClonar = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuSustituir = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuCrearPago = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuCrearNotaCredito = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuAplicarNotaCredito = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuReciboCobro = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuReciboPago = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuDoctoRelacionado = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuSerializarCFDI = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuCargarArchivo = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextButtonSubirXML = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextButtonSubirPDF = new Telerik.WinControls.UI.RadMenuItem();
            this.Waiting = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsSpinnerWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement();
            this.Iconos = new System.Windows.Forms.ImageList(this.components);
            this.ToolBarButtonHerramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonMetodo1 = new Telerik.WinControls.UI.RadMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobanteCfdiRelacionados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobanteCfdiRelacionadosCfdiRelacionado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridContable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComplementoPagos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComplementoDoctoRelacionado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridRecepcionPagos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridNotasCredito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Waiting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandBarRow
            // 
            this.CommandBarRow.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRow.Name = "CommandBarRow";
            this.CommandBarRow.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            this.CommandBarRow.Text = "";
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "Herramientas";
            // 
            // 
            // 
            this.ToolBar.Grip.Enabled = true;
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelPeriodo,
            this.ToolBarPeriodo,
            this.ToolBarLabelEjercicio,
            this.ToolBarEjercicio,
            this.CommandBarSeparator1,
            this.ToolBarNew,
            this.ToolBarEdit,
            this.ToolBarDelete,
            this.ToolBarButtonEnviar,
            this.commandBarSeparator2,
            this.ToolBarRefresh,
            this.ToolBarFilter,
            this.ToolBarAutoSum,
            this.ToolBarButtonExport,
            this.ToolBarPrint,
            this.ToolBarButtonValidar,
            this.ToolBarButtonProductos,
            this.ToolBarLayOut,
            this.ToolBarButtonHerramientas,
            this.ToolBarClose});
            this.ToolBar.Name = "ToolBar";
            // 
            // 
            // 
            this.ToolBar.OverflowButton.Enabled = true;
            ((Telerik.WinControls.UI.RadCommandBarGrip)(this.ToolBar.GetChildAt(0))).Enabled = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.ToolBar.GetChildAt(2))).Enabled = true;
            // 
            // ToolBarLabelPeriodo
            // 
            this.ToolBarLabelPeriodo.DisplayName = "commandBarLabel1";
            this.ToolBarLabelPeriodo.Name = "ToolBarLabelPeriodo";
            this.ToolBarLabelPeriodo.Text = "Periodo:";
            // 
            // ToolBarPeriodo
            // 
            this.ToolBarPeriodo.DefaultItem = null;
            this.ToolBarPeriodo.DisplayName = "Periodo";
            this.ToolBarPeriodo.DrawImage = false;
            this.ToolBarPeriodo.DrawText = true;
            this.ToolBarPeriodo.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarPeriodo.Image")));
            this.ToolBarPeriodo.MinSize = new System.Drawing.Size(72, 36);
            this.ToolBarPeriodo.Name = "ToolBarPeriodo";
            this.ToolBarPeriodo.Text = "Todos";
            // 
            // ToolBarLabelEjercicio
            // 
            this.ToolBarLabelEjercicio.DisplayName = "Ejercicio";
            this.ToolBarLabelEjercicio.Name = "ToolBarLabelEjercicio";
            this.ToolBarLabelEjercicio.Text = "Ejercicio:";
            // 
            // ToolBarEjercicio
            // 
            this.ToolBarEjercicio.DefaultItem = null;
            this.ToolBarEjercicio.DisplayName = "Ejercicio";
            this.ToolBarEjercicio.DrawImage = false;
            this.ToolBarEjercicio.DrawText = true;
            this.ToolBarEjercicio.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarEjercicio.Image")));
            this.ToolBarEjercicio.Name = "ToolBarEjercicio";
            this.ToolBarEjercicio.Text = "0000";
            // 
            // CommandBarSeparator1
            // 
            this.CommandBarSeparator1.DisplayName = "Separador 1";
            this.CommandBarSeparator1.Name = "CommandBarSeparator1";
            this.CommandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarNew
            // 
            this.ToolBarNew.DisplayName = "Nuevo";
            this.ToolBarNew.DrawText = true;
            this.ToolBarNew.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_agregar_archivo;
            this.ToolBarNew.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarComprobanteNuevo,
            this.ToolBarComprobantePago,
            this.ToolBarComprobanteClonar,
            this.ToolBarComprobanteSustituir,
            this.ToolBarComprobanteLink,
            this.ToolBarReciboCobro,
            this.ToolBarReciboPago});
            this.ToolBarNew.Name = "ToolBarNew";
            this.ToolBarNew.Text = "Nuevo";
            this.ToolBarNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarComprobanteNuevo
            // 
            this.ToolBarComprobanteNuevo.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_factura;
            this.ToolBarComprobanteNuevo.Name = "ToolBarComprobanteNuevo";
            this.ToolBarComprobanteNuevo.Text = "Comprobante";
            this.ToolBarComprobanteNuevo.Click += new System.EventHandler(this.ToolBarComprobanteNuevo_Click);
            // 
            // ToolBarComprobantePago
            // 
            this.ToolBarComprobantePago.Name = "ToolBarComprobantePago";
            this.ToolBarComprobantePago.Text = "Recibo Electrónico de Pago";
            this.ToolBarComprobantePago.Click += new System.EventHandler(this.ToolBarComprobantePago_Click);
            // 
            // ToolBarComprobanteClonar
            // 
            this.ToolBarComprobanteClonar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_copiar;
            this.ToolBarComprobanteClonar.Name = "ToolBarComprobanteClonar";
            this.ToolBarComprobanteClonar.Text = "Clonar Comprobante";
            this.ToolBarComprobanteClonar.Click += new System.EventHandler(this.ToolBarComprobanteClonar_Click);
            // 
            // ToolBarComprobanteSustituir
            // 
            this.ToolBarComprobanteSustituir.Name = "ToolBarComprobanteSustituir";
            this.ToolBarComprobanteSustituir.Text = "Sustituir Comprobante";
            this.ToolBarComprobanteSustituir.Click += new System.EventHandler(this.ToolBarComprobanteSustituir_Click);
            // 
            // ToolBarComprobanteLink
            // 
            this.ToolBarComprobanteLink.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_archivo_de_symlink;
            this.ToolBarComprobanteLink.Name = "ToolBarComprobanteLink";
            this.ToolBarComprobanteLink.Text = "Relación";
            this.ToolBarComprobanteLink.Click += new System.EventHandler(this.ToolBarComprobanteLink_Click);
            // 
            // ToolBarReciboCobro
            // 
            this.ToolBarReciboCobro.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_cash_register;
            this.ToolBarReciboCobro.Name = "ToolBarReciboCobro";
            this.ToolBarReciboCobro.Text = "Recibo de Cobro";
            this.ToolBarReciboCobro.Click += new System.EventHandler(this.ToolBarRecibo_Click);
            // 
            // ToolBarReciboPago
            // 
            this.ToolBarReciboPago.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_actualizar;
            this.ToolBarReciboPago.Name = "ToolBarReciboPago";
            this.ToolBarReciboPago.Text = "Recibo de Pago";
            this.ToolBarReciboPago.Click += new System.EventHandler(this.ToolBarRecibo_Click);
            // 
            // ToolBarEdit
            // 
            this.ToolBarEdit.DisplayName = "Editar";
            this.ToolBarEdit.DrawText = true;
            this.ToolBarEdit.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_edit_file;
            this.ToolBarEdit.Name = "ToolBarEdit";
            this.ToolBarEdit.Text = "Editar";
            this.ToolBarEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarEdit.Click += new System.EventHandler(this.ToolBarEdit_Click);
            // 
            // ToolBarDelete
            // 
            this.ToolBarDelete.DisplayName = "Eliminar";
            this.ToolBarDelete.DrawText = true;
            this.ToolBarDelete.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_delete_file;
            this.ToolBarDelete.Name = "ToolBarDelete";
            this.ToolBarDelete.Text = "Cancelar";
            this.ToolBarDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarDelete.Click += new System.EventHandler(this.ToolBarDelete_Click);
            // 
            // ToolBarButtonEnviar
            // 
            this.ToolBarButtonEnviar.DisplayName = "Envíar";
            this.ToolBarButtonEnviar.DrawText = true;
            this.ToolBarButtonEnviar.Image = global::Jaeger.UI.Properties.Resources.icons8_x30_enviar;
            this.ToolBarButtonEnviar.Name = "ToolBarButtonEnviar";
            this.ToolBarButtonEnviar.Text = "Envíar";
            this.ToolBarButtonEnviar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonEnviar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ToolBarButtonEnviar.Click += new System.EventHandler(this.ToolBarButtonEnviar_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisplayName = "commandBarSeparator2";
            this.commandBarSeparator2.DrawText = false;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.commandBarSeparator2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // ToolBarRefresh
            // 
            this.ToolBarRefresh.DisplayName = "Actualizar";
            this.ToolBarRefresh.DrawText = true;
            this.ToolBarRefresh.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_actualizar;
            this.ToolBarRefresh.Name = "ToolBarRefresh";
            this.ToolBarRefresh.Text = "Actualizar";
            this.ToolBarRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarRefresh.Click += new System.EventHandler(this.ToolBarRefresh_Click);
            // 
            // ToolBarFilter
            // 
            this.ToolBarFilter.DisplayName = "Filtrado";
            this.ToolBarFilter.DrawText = true;
            this.ToolBarFilter.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_filtrar;
            this.ToolBarFilter.Name = "ToolBarFilter";
            this.ToolBarFilter.Text = "Filtro";
            this.ToolBarFilter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarFilter.Click += new System.EventHandler(this.ToolBarFilter_Click);
            // 
            // ToolBarAutoSum
            // 
            this.ToolBarAutoSum.DisplayName = "ToolBarAutoSum";
            this.ToolBarAutoSum.DrawText = true;
            this.ToolBarAutoSum.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_auto_flash;
            this.ToolBarAutoSum.Name = "ToolBarAutoSum";
            this.ToolBarAutoSum.Text = "AutoSuma";
            this.ToolBarAutoSum.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarAutoSum.Click += new System.EventHandler(this.ToolBarAutoSum_Click);
            // 
            // ToolBarButtonExport
            // 
            this.ToolBarButtonExport.DisplayName = "Exportar";
            this.ToolBarButtonExport.DrawText = true;
            this.ToolBarButtonExport.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_excel;
            this.ToolBarButtonExport.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonExportarExcel,
            this.ToolBarButtonExportarExcelReporte1});
            this.ToolBarButtonExport.Name = "ToolBarButtonExport";
            this.ToolBarButtonExport.Text = "Exportar";
            this.ToolBarButtonExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonExportarExcel
            // 
            this.ToolBarButtonExportarExcel.Name = "ToolBarButtonExportarExcel";
            this.ToolBarButtonExportarExcel.Text = "Exportar a Excel";
            this.ToolBarButtonExportarExcel.Click += new System.EventHandler(this.ToolBarExport_Click);
            // 
            // ToolBarButtonExportarExcelReporte1
            // 
            this.ToolBarButtonExportarExcelReporte1.Name = "ToolBarButtonExportarExcelReporte1";
            this.ToolBarButtonExportarExcelReporte1.Text = "Reporte Ingreso vs Recepción Pagos";
            this.ToolBarButtonExportarExcelReporte1.Click += new System.EventHandler(this.ToolBarExportReporte1_Click);
            // 
            // ToolBarPrint
            // 
            this.ToolBarPrint.DefaultItem = null;
            this.ToolBarPrint.DisplayName = "Imprimir";
            this.ToolBarPrint.DrawText = true;
            this.ToolBarPrint.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_imprimir;
            this.ToolBarPrint.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.PrintAcuse,
            this.PrintComprobante,
            this.PrintValidacion,
            this.PrintLista,
            this.ToolBarButtonPrintEdoCuentaSimple});
            this.ToolBarPrint.Name = "ToolBarPrint";
            this.ToolBarPrint.Text = "Imprimir";
            this.ToolBarPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarPrint.ToolTipText = "Imprimir documento relacionado";
            // 
            // PrintAcuse
            // 
            this.PrintAcuse.Name = "PrintAcuse";
            this.PrintAcuse.Text = "Acuse";
            this.PrintAcuse.Click += new System.EventHandler(this.PrintAcuse_Click);
            // 
            // PrintComprobante
            // 
            this.PrintComprobante.Name = "PrintComprobante";
            this.PrintComprobante.Text = "Comprobante";
            // 
            // PrintValidacion
            // 
            this.PrintValidacion.Name = "PrintValidacion";
            this.PrintValidacion.Text = "Validación";
            // 
            // PrintLista
            // 
            this.PrintLista.Name = "PrintLista";
            this.PrintLista.Text = "Listado";
            this.PrintLista.Click += new System.EventHandler(this.PrintLista_Click);
            // 
            // ToolBarButtonPrintEdoCuentaSimple
            // 
            this.ToolBarButtonPrintEdoCuentaSimple.Name = "ToolBarButtonPrintEdoCuentaSimple";
            this.ToolBarButtonPrintEdoCuentaSimple.Text = "Estado de Cuenta";
            this.ToolBarButtonPrintEdoCuentaSimple.Click += new System.EventHandler(this.ToolBarButtonPrintEdoCuentaSimple_Click);
            // 
            // ToolBarButtonValidar
            // 
            this.ToolBarButtonValidar.DefaultItem = null;
            this.ToolBarButtonValidar.DisplayName = "Validar";
            this.ToolBarButtonValidar.DrawText = false;
            this.ToolBarButtonValidar.Enabled = false;
            this.ToolBarButtonValidar.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_seguridad_comprobado;
            this.ToolBarButtonValidar.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonValidarEstado,
            this.ToolBarButtonValidarSAT,
            this.ToolBarButtonPDFValidacion});
            this.ToolBarButtonValidar.Name = "ToolBarButtonValidar";
            this.ToolBarButtonValidar.Text = "Validar";
            this.ToolBarButtonValidar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonValidarEstado
            // 
            this.ToolBarButtonValidarEstado.Name = "ToolBarButtonValidarEstado";
            this.ToolBarButtonValidarEstado.Text = "Consulta Estado SAT";
            this.ToolBarButtonValidarEstado.ToolTipText = "Consultar el estado del comprobante";
            this.ToolBarButtonValidarEstado.Click += new System.EventHandler(this.ValidateSatSelected_Click);
            // 
            // ToolBarButtonValidarSAT
            // 
            this.ToolBarButtonValidarSAT.Name = "ToolBarButtonValidarSAT";
            this.ToolBarButtonValidarSAT.Text = "Validar";
            this.ToolBarButtonValidarSAT.ToolTipText = "Valdiar comprobante seleccionado";
            this.ToolBarButtonValidarSAT.Click += new System.EventHandler(this.ValidateSatSelected_Click);
            // 
            // ToolBarButtonPDFValidacion
            // 
            this.ToolBarButtonPDFValidacion.Name = "ToolBarButtonPDFValidacion";
            this.ToolBarButtonPDFValidacion.Text = "Descargar PDF de validación";
            this.ToolBarButtonPDFValidacion.ToolTipText = "Descargar reporte de validación en formato PDF";
            this.ToolBarButtonPDFValidacion.Click += new System.EventHandler(this.ToolBarButtonPDFValidacion_Click);
            // 
            // ToolBarButtonProductos
            // 
            this.ToolBarButtonProductos.DisplayName = "Productos";
            this.ToolBarButtonProductos.DrawText = true;
            this.ToolBarButtonProductos.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_producto;
            this.ToolBarButtonProductos.Name = "ToolBarButtonProductos";
            this.ToolBarButtonProductos.Text = "Productos";
            this.ToolBarButtonProductos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonProductos.Click += new System.EventHandler(this.ToolBarButtonProductos_Click);
            // 
            // ToolBarLayOut
            // 
            this.ToolBarLayOut.DisplayName = "Preferencias";
            this.ToolBarLayOut.DrawText = false;
            this.ToolBarLayOut.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_vista_personalizada;
            this.ToolBarLayOut.Name = "ToolBarLayOut";
            this.ToolBarLayOut.Text = "Preferencias";
            this.ToolBarLayOut.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarLayOut.Click += new System.EventHandler(this.ToolBarLayOut_Click);
            // 
            // ToolBarClose
            // 
            this.ToolBarClose.DisplayName = "Cerrar";
            this.ToolBarClose.DrawText = true;
            this.ToolBarClose.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_cerrar;
            this.ToolBarClose.Name = "ToolBarClose";
            this.ToolBarClose.Text = "Cerrar";
            this.ToolBarClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarClose.Click += new System.EventHandler(this.ToolBarClose_Click);
            // 
            // CommandBar
            // 
            this.CommandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBar.Location = new System.Drawing.Point(0, 0);
            this.CommandBar.Name = "CommandBar";
            this.CommandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRow});
            this.CommandBar.Size = new System.Drawing.Size(1297, 40);
            this.CommandBar.TabIndex = 13;
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 40);
            // 
            // 
            // 
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "Activo";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewCheckBoxColumn1.ReadOnly = true;
            gridViewCheckBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.DataType = typeof(int);
            gridViewTextBoxColumn2.FieldName = "TipoComprobante";
            gridViewTextBoxColumn2.HeaderText = "Tipo";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "TipoComprobante";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewComboBoxColumn1.FieldName = "Status";
            gridViewComboBoxColumn1.HeaderText = "Status";
            gridViewComboBoxColumn1.Name = "Status";
            gridViewComboBoxColumn1.Width = 80;
            gridViewTextBoxColumn3.FieldName = "Folio";
            gridViewTextBoxColumn3.HeaderText = "Folio";
            gridViewTextBoxColumn3.Name = "Folio";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 65;
            gridViewTextBoxColumn4.FieldName = "Serie";
            gridViewTextBoxColumn4.HeaderText = "Serie";
            gridViewTextBoxColumn4.Name = "Serie";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.Width = 65;
            gridViewTextBoxColumn5.FieldName = "EmisorRFC";
            gridViewTextBoxColumn5.HeaderText = "RFC Emisor";
            gridViewTextBoxColumn5.Name = "EmisorRFC";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.Width = 90;
            gridViewTextBoxColumn6.FieldName = "Emisor";
            gridViewTextBoxColumn6.HeaderText = "Nombre del Emisor";
            gridViewTextBoxColumn6.Name = "Emisor";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.Width = 180;
            gridViewTextBoxColumn7.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn7.HeaderText = "RFC Receptor";
            gridViewTextBoxColumn7.Name = "ReceptorRFC";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn7.Width = 90;
            gridViewTextBoxColumn8.FieldName = "Receptor";
            gridViewTextBoxColumn8.HeaderText = "Nombre del Receptor";
            gridViewTextBoxColumn8.Name = "Receptor";
            gridViewTextBoxColumn8.ReadOnly = true;
            gridViewTextBoxColumn8.Width = 220;
            gridViewTextBoxColumn9.FieldName = "IdDocumento";
            gridViewTextBoxColumn9.HeaderText = "Folios Fiscal (uuid)";
            gridViewTextBoxColumn9.Name = "IdDocumento";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.Width = 180;
            gridViewTextBoxColumn10.FieldName = "TipoComprobanteText";
            gridViewTextBoxColumn10.HeaderText = "Tipo Comprobante";
            gridViewTextBoxColumn10.Name = "TipoComprobanteText";
            gridViewTextBoxColumn10.ReadOnly = true;
            gridViewTextBoxColumn10.WrapText = true;
            gridViewDateTimeColumn1.FieldName = "FechaEmision";
            gridViewDateTimeColumn1.FormatString = "{0:d}";
            gridViewDateTimeColumn1.HeaderText = "Fecha de Emisión";
            gridViewDateTimeColumn1.Name = "FechaEmision";
            gridViewDateTimeColumn1.ReadOnly = true;
            gridViewDateTimeColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn1.Width = 70;
            gridViewDateTimeColumn1.WrapText = true;
            gridViewDateTimeColumn2.FieldName = "FechaTimbrado";
            gridViewDateTimeColumn2.FormatString = "{0:d}";
            gridViewDateTimeColumn2.HeaderText = "Fecha de Certificación";
            gridViewDateTimeColumn2.Name = "FechaTimbrado";
            gridViewDateTimeColumn2.ReadOnly = true;
            gridViewDateTimeColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn2.Width = 70;
            gridViewDateTimeColumn2.WrapText = true;
            gridViewDateTimeColumn3.FieldName = "FechaCancela";
            gridViewDateTimeColumn3.FormatString = "{0:d}";
            gridViewDateTimeColumn3.HeaderText = "Fecha de Cancelación";
            gridViewDateTimeColumn3.Name = "FechaCancela";
            gridViewDateTimeColumn3.ReadOnly = true;
            gridViewDateTimeColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn3.Width = 70;
            gridViewDateTimeColumn3.WrapText = true;
            gridViewDateTimeColumn4.FieldName = "FechaValidacion";
            gridViewDateTimeColumn4.FormatString = "{0:d}";
            gridViewDateTimeColumn4.HeaderText = "Fecha de Validación";
            gridViewDateTimeColumn4.Name = "FechaValidacion";
            gridViewDateTimeColumn4.ReadOnly = true;
            gridViewDateTimeColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn4.Width = 70;
            gridViewDateTimeColumn4.WrapText = true;
            gridViewDateTimeColumn5.FieldName = "FechaEntrega";
            gridViewDateTimeColumn5.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            gridViewDateTimeColumn5.FormatString = "{0:d}";
            gridViewDateTimeColumn5.HeaderText = "Fecha de Entrega";
            gridViewDateTimeColumn5.Name = "FechaEntrega";
            gridViewDateTimeColumn5.ReadOnly = true;
            gridViewDateTimeColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn5.Width = 70;
            gridViewDateTimeColumn5.WrapText = true;
            gridViewDateTimeColumn6.FieldName = "FechaUltimoPago";
            gridViewDateTimeColumn6.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            gridViewDateTimeColumn6.FormatString = "{0:d}";
            gridViewDateTimeColumn6.HeaderText = "Ult. Pago";
            gridViewDateTimeColumn6.Name = "FechaUltimoPago";
            gridViewDateTimeColumn6.ReadOnly = true;
            gridViewDateTimeColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn6.Width = 70;
            gridViewDateTimeColumn7.FieldName = "FechaRecepcionPago";
            gridViewDateTimeColumn7.FormatString = "{0:d}";
            gridViewDateTimeColumn7.HeaderText = "C. Fecha Pago";
            gridViewDateTimeColumn7.Name = "FechaRecepcionPago";
            gridViewDateTimeColumn7.Width = 70;
            gridViewTextBoxColumn11.FieldName = "NumParcialidad";
            gridViewTextBoxColumn11.HeaderText = "Núm. Par.";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "NumParcialidad";
            gridViewTextBoxColumn11.WrapText = true;
            gridViewTextBoxColumn12.FieldName = "MetodoPago";
            gridViewTextBoxColumn12.HeaderText = "Método de Pago";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "MetodoPago";
            gridViewTextBoxColumn12.ReadOnly = true;
            gridViewTextBoxColumn12.Width = 75;
            gridViewTextBoxColumn12.WrapText = true;
            gridViewTextBoxColumn13.FieldName = "FormaPago";
            gridViewTextBoxColumn13.HeaderText = "Forma de Pago";
            gridViewTextBoxColumn13.Name = "FormaPago";
            gridViewTextBoxColumn14.DataType = typeof(decimal);
            gridViewTextBoxColumn14.FieldName = "SubTotal";
            gridViewTextBoxColumn14.FormatString = "{0:n}";
            gridViewTextBoxColumn14.HeaderText = "SubTotal";
            gridViewTextBoxColumn14.IsVisible = false;
            gridViewTextBoxColumn14.Name = "SubTotal";
            gridViewTextBoxColumn14.ReadOnly = true;
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.Width = 85;
            gridViewTextBoxColumn15.DataType = typeof(decimal);
            gridViewTextBoxColumn15.FieldName = "TrasladoIVA";
            gridViewTextBoxColumn15.FormatString = "{0:n}";
            gridViewTextBoxColumn15.HeaderText = "Iva";
            gridViewTextBoxColumn15.IsVisible = false;
            gridViewTextBoxColumn15.Name = "TrasladoIVA";
            gridViewTextBoxColumn15.ReadOnly = true;
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn15.Width = 85;
            gridViewTextBoxColumn16.DataType = typeof(decimal);
            gridViewTextBoxColumn16.FieldName = "Total";
            gridViewTextBoxColumn16.FormatString = "{0:n}";
            gridViewTextBoxColumn16.HeaderText = "Total";
            gridViewTextBoxColumn16.Name = "Total";
            gridViewTextBoxColumn16.ReadOnly = true;
            gridViewTextBoxColumn16.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn16.Width = 85;
            gridViewTextBoxColumn17.DataType = typeof(decimal);
            gridViewTextBoxColumn17.FieldName = "Descuento";
            gridViewTextBoxColumn17.FormatString = "{0:n}";
            gridViewTextBoxColumn17.HeaderText = "Descuento";
            gridViewTextBoxColumn17.Name = "Descuento";
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn17.Width = 95;
            gridViewTextBoxColumn18.DataType = typeof(double);
            gridViewTextBoxColumn18.FieldName = "Acumulado";
            gridViewTextBoxColumn18.FormatString = "{0:n}";
            gridViewTextBoxColumn18.HeaderText = "Cobrado";
            gridViewTextBoxColumn18.Name = "Cobrado";
            gridViewTextBoxColumn18.ReadOnly = true;
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn18.Width = 85;
            gridViewTextBoxColumn19.DataType = typeof(decimal);
            gridViewTextBoxColumn19.FieldName = "Saldo";
            gridViewTextBoxColumn19.FormatString = "{0:n}";
            gridViewTextBoxColumn19.HeaderText = "Saldo";
            gridViewTextBoxColumn19.Name = "Saldo";
            gridViewTextBoxColumn19.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn19.Width = 85;
            gridViewTextBoxColumn20.FieldName = "ImportesPagos";
            gridViewTextBoxColumn20.FormatString = "{0:n}";
            gridViewTextBoxColumn20.HeaderText = "C. Pagos";
            gridViewTextBoxColumn20.Name = "ImportesPagos";
            gridViewTextBoxColumn20.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn20.Width = 85;
            gridViewTextBoxColumn21.FieldName = "SaldoPagos";
            gridViewTextBoxColumn21.FormatString = "{0:n}";
            gridViewTextBoxColumn21.HeaderText = "C. Saldo";
            gridViewTextBoxColumn21.Name = "SaldoPagos";
            gridViewTextBoxColumn21.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn21.Width = 85;
            gridViewTextBoxColumn22.FieldName = "Estado";
            gridViewTextBoxColumn22.HeaderText = "Estado";
            gridViewTextBoxColumn22.Name = "Estado";
            gridViewTextBoxColumn22.Width = 75;
            gridViewTextBoxColumn23.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn23.FieldName = "FechaEstado";
            gridViewTextBoxColumn23.FormatString = "{0:d}";
            gridViewTextBoxColumn23.HeaderText = "Fecha del Estado";
            gridViewTextBoxColumn23.IsVisible = false;
            gridViewTextBoxColumn23.Name = "FechaEstado";
            gridViewTextBoxColumn23.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn23.Width = 70;
            gridViewTextBoxColumn23.WrapText = true;
            gridViewDateTimeColumn8.FieldName = "FechaNuevo";
            gridViewDateTimeColumn8.FormatString = "{0:d}";
            gridViewDateTimeColumn8.HeaderText = "Fecha de Sistema";
            gridViewDateTimeColumn8.IsVisible = false;
            gridViewDateTimeColumn8.Name = "FechaNuevo";
            gridViewDateTimeColumn8.ReadOnly = true;
            gridViewDateTimeColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn8.Width = 70;
            gridViewTextBoxColumn24.FieldName = "Creo";
            gridViewTextBoxColumn24.HeaderText = "Creó";
            gridViewTextBoxColumn24.IsVisible = false;
            gridViewTextBoxColumn24.Name = "Creo";
            gridViewTextBoxColumn24.ReadOnly = true;
            gridViewTextBoxColumn24.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn24.Width = 75;
            gridViewTextBoxColumn25.FieldName = "Modifica";
            gridViewTextBoxColumn25.HeaderText = "Mod.";
            gridViewTextBoxColumn25.IsVisible = false;
            gridViewTextBoxColumn25.Name = "Modifica";
            gridViewTextBoxColumn25.ReadOnly = true;
            gridViewTextBoxColumn25.Width = 75;
            gridViewDateTimeColumn9.FieldName = "FechaMod";
            gridViewDateTimeColumn9.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            gridViewDateTimeColumn9.FormatString = "{0:d}";
            gridViewDateTimeColumn9.HeaderText = "Fecha de Modificación";
            gridViewDateTimeColumn9.IsVisible = false;
            gridViewDateTimeColumn9.Name = "FechaMod";
            gridViewDateTimeColumn9.ReadOnly = true;
            gridViewDateTimeColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn9.Width = 70;
            gridViewDateTimeColumn9.WrapText = true;
            gridViewTextBoxColumn26.FieldName = "UrlXml";
            gridViewTextBoxColumn26.HeaderText = "XML";
            gridViewTextBoxColumn26.Name = "UrlXml";
            gridViewTextBoxColumn26.ReadOnly = true;
            gridViewTextBoxColumn26.Width = 30;
            gridViewTextBoxColumn27.FieldName = "UrlPdf";
            gridViewTextBoxColumn27.HeaderText = "PDF";
            gridViewTextBoxColumn27.Name = "UrlPdf";
            gridViewTextBoxColumn27.ReadOnly = true;
            gridViewTextBoxColumn27.Width = 30;
            gridViewTextBoxColumn28.FieldName = "Moneda";
            gridViewTextBoxColumn28.HeaderText = "Moneda";
            gridViewTextBoxColumn28.IsVisible = false;
            gridViewTextBoxColumn28.Name = "Moneda";
            gridViewTextBoxColumn29.FieldName = "Nota";
            gridViewTextBoxColumn29.HeaderText = "Nota";
            gridViewTextBoxColumn29.Name = "Nota";
            gridViewTextBoxColumn29.Width = 100;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewDateTimeColumn1,
            gridViewDateTimeColumn2,
            gridViewDateTimeColumn3,
            gridViewDateTimeColumn4,
            gridViewDateTimeColumn5,
            gridViewDateTimeColumn6,
            gridViewDateTimeColumn7,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewDateTimeColumn8,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewDateTimeColumn9,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29});
            this.GridData.MasterTemplate.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.GridConceptos,
            this.GridFiles,
            this.GridComprobanteCfdiRelacionados,
            this.GridContable,
            this.GridComplementoPagos,
            this.GridRecepcionPagos,
            this.GridNotasCredito});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition10;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1297, 608);
            this.GridData.TabIndex = 14;
            this.GridData.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridData_CellDoubleClick);
            this.GridData.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.GridData_ContextMenuOpening);
            this.GridData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridData_KeyDown);
            // 
            // GridConceptos
            // 
            this.GridConceptos.Caption = "Conceptos";
            gridViewTextBoxColumn30.DataType = typeof(int);
            gridViewTextBoxColumn30.FieldName = "_cfdcnp_id";
            gridViewTextBoxColumn30.HeaderText = "id";
            gridViewTextBoxColumn30.IsVisible = false;
            gridViewTextBoxColumn30.Name = "Id";
            gridViewTextBoxColumn30.VisibleInColumnChooser = false;
            gridViewCheckBoxColumn2.FieldName = "_cfdcnp_a";
            gridViewCheckBoxColumn2.HeaderText = "Activo";
            gridViewCheckBoxColumn2.IsVisible = false;
            gridViewCheckBoxColumn2.Name = "Activo";
            gridViewCheckBoxColumn2.VisibleInColumnChooser = false;
            gridViewTextBoxColumn31.DataType = typeof(int);
            gridViewTextBoxColumn31.FieldName = "_cfdcnp_cfds_id";
            gridViewTextBoxColumn31.HeaderText = "_cfdcnp_cfds_id";
            gridViewTextBoxColumn31.IsVisible = false;
            gridViewTextBoxColumn31.Name = "_cfdcnp_cfds_id";
            gridViewTextBoxColumn31.VisibleInColumnChooser = false;
            gridViewTextBoxColumn32.DataType = typeof(double);
            gridViewTextBoxColumn32.FieldName = "_cfdcnp_cntdd";
            gridViewTextBoxColumn32.FormatString = "{0:n}";
            gridViewTextBoxColumn32.HeaderText = "Cantidad";
            gridViewTextBoxColumn32.Name = "Cantidad";
            gridViewTextBoxColumn32.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn32.Width = 70;
            gridViewTextBoxColumn33.FieldName = "_cfdcnp_clvund";
            gridViewTextBoxColumn33.HeaderText = "Clv. Unidad";
            gridViewTextBoxColumn33.Name = "ClaveUnidad";
            gridViewTextBoxColumn33.Width = 75;
            gridViewComboBoxColumn2.FieldName = "_cfdcnp_undd";
            gridViewComboBoxColumn2.HeaderText = "Unidad";
            gridViewComboBoxColumn2.Name = "Unidad";
            gridViewComboBoxColumn2.Width = 65;
            gridViewTextBoxColumn34.FieldName = "_cfdcnp_pdd_id";
            gridViewTextBoxColumn34.HeaderText = "No. Orden";
            gridViewTextBoxColumn34.Name = "NoOrden";
            gridViewTextBoxColumn34.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn34.Width = 75;
            gridViewTextBoxColumn35.FieldName = "_cfdcnp_clvprds";
            gridViewTextBoxColumn35.HeaderText = "Clv. Prod.";
            gridViewTextBoxColumn35.Name = "ClaveProducto";
            gridViewTextBoxColumn35.Width = 75;
            gridViewTextBoxColumn36.FieldName = "_cfdcnp_cncpt";
            gridViewTextBoxColumn36.HeaderText = "Concepto";
            gridViewTextBoxColumn36.Name = "Concepto";
            gridViewTextBoxColumn36.Width = 285;
            gridViewTextBoxColumn37.DataType = typeof(double);
            gridViewTextBoxColumn37.FieldName = "_cfdcnp_untr";
            gridViewTextBoxColumn37.FormatString = "{0:n}";
            gridViewTextBoxColumn37.HeaderText = "Unitario";
            gridViewTextBoxColumn37.Name = "Unitario";
            gridViewTextBoxColumn37.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn37.Width = 75;
            gridViewTextBoxColumn38.DataType = typeof(double);
            gridViewTextBoxColumn38.FieldName = "_cfdcnp_sbttl";
            gridViewTextBoxColumn38.FormatString = "{0:n}";
            gridViewTextBoxColumn38.HeaderText = "SubTotal";
            gridViewTextBoxColumn38.Name = "SubTotal";
            gridViewTextBoxColumn38.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn38.Width = 75;
            this.GridConceptos.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn30,
            gridViewCheckBoxColumn2,
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32,
            gridViewTextBoxColumn33,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn34,
            gridViewTextBoxColumn35,
            gridViewTextBoxColumn36,
            gridViewTextBoxColumn37,
            gridViewTextBoxColumn38});
            this.GridConceptos.ViewDefinition = tableViewDefinition1;
            // 
            // GridFiles
            // 
            this.GridFiles.Caption = "Adjuntos";
            gridViewTextBoxColumn39.FieldName = "_bckt_id";
            gridViewTextBoxColumn39.HeaderText = "id";
            gridViewTextBoxColumn39.IsVisible = false;
            gridViewTextBoxColumn39.Name = "_bckt_id";
            gridViewTextBoxColumn39.VisibleInColumnChooser = false;
            gridViewCheckBoxColumn3.FieldName = "_bckt_a";
            gridViewCheckBoxColumn3.HeaderText = "Activo";
            gridViewCheckBoxColumn3.IsVisible = false;
            gridViewCheckBoxColumn3.Name = "_bckt_a";
            gridViewCheckBoxColumn3.VisibleInColumnChooser = false;
            gridViewTextBoxColumn40.FieldName = "_bckt_doc";
            gridViewTextBoxColumn40.HeaderText = "doc";
            gridViewTextBoxColumn40.IsVisible = false;
            gridViewTextBoxColumn40.Name = "_bckt_doc";
            gridViewTextBoxColumn40.VisibleInColumnChooser = false;
            gridViewTextBoxColumn41.FieldName = "_bckt_bucket";
            gridViewTextBoxColumn41.HeaderText = "Bucket";
            gridViewTextBoxColumn41.IsVisible = false;
            gridViewTextBoxColumn41.Name = "_bckt_bucket";
            gridViewTextBoxColumn42.FieldName = "_bckt_keyname";
            gridViewTextBoxColumn42.HeaderText = "keyname";
            gridViewTextBoxColumn42.IsVisible = false;
            gridViewTextBoxColumn42.Name = "_bckt_keyname";
            gridViewTextBoxColumn42.VisibleInColumnChooser = false;
            gridViewTextBoxColumn43.FieldName = "_bckt_uuid";
            gridViewTextBoxColumn43.HeaderText = "uuid";
            gridViewTextBoxColumn43.IsVisible = false;
            gridViewTextBoxColumn43.Name = "_bckt_uuid";
            gridViewTextBoxColumn43.VisibleInColumnChooser = false;
            gridViewTextBoxColumn43.Width = 75;
            gridViewTextBoxColumn44.FieldName = "_bckt_file";
            gridViewTextBoxColumn44.HeaderText = "Nombre del archivo";
            gridViewTextBoxColumn44.Name = "_bckt_file";
            gridViewTextBoxColumn44.Width = 240;
            gridViewTextBoxColumn45.FieldName = "_bckt_ext";
            gridViewTextBoxColumn45.HeaderText = "Extensión";
            gridViewTextBoxColumn45.IsVisible = false;
            gridViewTextBoxColumn45.Name = "_bckt_ext";
            gridViewTextBoxColumn45.VisibleInColumnChooser = false;
            gridViewTextBoxColumn46.FieldName = "_bckt_nom";
            gridViewTextBoxColumn46.HeaderText = "Descripción";
            gridViewTextBoxColumn46.Name = "_bckt_nom";
            gridViewTextBoxColumn46.Width = 70;
            gridViewTextBoxColumn47.FieldName = "_bckt_type";
            gridViewTextBoxColumn47.HeaderText = "Tipo de archivo";
            gridViewTextBoxColumn47.Name = "_bckt_type";
            gridViewTextBoxColumn47.Width = 75;
            gridViewTextBoxColumn48.DataType = typeof(double);
            gridViewTextBoxColumn48.FieldName = "_bckt_size";
            gridViewTextBoxColumn48.FormatString = "{0:n}";
            gridViewTextBoxColumn48.HeaderText = "Tamaño";
            gridViewTextBoxColumn48.Name = "_bckt_size";
            gridViewTextBoxColumn48.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn49.FieldName = "_bckt_url";
            gridViewTextBoxColumn49.HeaderText = "url";
            gridViewTextBoxColumn49.IsVisible = false;
            gridViewTextBoxColumn49.Name = "_bckt_url";
            gridViewTextBoxColumn49.VisibleInColumnChooser = false;
            gridViewTextBoxColumn50.FieldName = "_bckt_fn";
            gridViewTextBoxColumn50.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn50.Name = "_bckt_fn";
            gridViewTextBoxColumn51.FieldName = "_bckt_rel_id";
            gridViewTextBoxColumn51.HeaderText = "_bckt_rel_id";
            gridViewTextBoxColumn51.IsVisible = false;
            gridViewTextBoxColumn51.Name = "_bckt_rel_id";
            gridViewTextBoxColumn51.VisibleInColumnChooser = false;
            gridViewTextBoxColumn52.FieldName = "_bckt_usr_n";
            gridViewTextBoxColumn52.HeaderText = "Creó";
            gridViewTextBoxColumn52.Name = "_bckt_usr_n";
            this.GridFiles.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn39,
            gridViewCheckBoxColumn3,
            gridViewTextBoxColumn40,
            gridViewTextBoxColumn41,
            gridViewTextBoxColumn42,
            gridViewTextBoxColumn43,
            gridViewTextBoxColumn44,
            gridViewTextBoxColumn45,
            gridViewTextBoxColumn46,
            gridViewTextBoxColumn47,
            gridViewTextBoxColumn48,
            gridViewTextBoxColumn49,
            gridViewTextBoxColumn50,
            gridViewTextBoxColumn51,
            gridViewTextBoxColumn52});
            this.GridFiles.ViewDefinition = tableViewDefinition2;
            // 
            // GridComprobanteCfdiRelacionados
            // 
            this.GridComprobanteCfdiRelacionados.Caption = "CFDI Relacionado";
            gridViewTextBoxColumn53.FieldName = "Clave";
            gridViewTextBoxColumn53.HeaderText = "Clave";
            gridViewTextBoxColumn53.Name = "Clave";
            gridViewTextBoxColumn54.FieldName = "Nombre";
            gridViewTextBoxColumn54.HeaderText = "Descripción";
            gridViewTextBoxColumn54.Name = "Nombre";
            gridViewTextBoxColumn54.Width = 250;
            this.GridComprobanteCfdiRelacionados.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn53,
            gridViewTextBoxColumn54});
            this.GridComprobanteCfdiRelacionados.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.GridComprobanteCfdiRelacionadosCfdiRelacionado});
            this.GridComprobanteCfdiRelacionados.ViewDefinition = tableViewDefinition4;
            // 
            // GridComprobanteCfdiRelacionadosCfdiRelacionado
            // 
            gridViewTextBoxColumn55.FieldName = "UUID";
            gridViewTextBoxColumn55.HeaderText = "UUID";
            gridViewTextBoxColumn55.Name = "UUID";
            gridViewTextBoxColumn55.Width = 225;
            gridViewTextBoxColumn56.FieldName = "RFC";
            gridViewTextBoxColumn56.HeaderText = "RFC";
            gridViewTextBoxColumn56.Name = "RFC";
            gridViewTextBoxColumn56.Width = 85;
            gridViewTextBoxColumn57.FieldName = "Nombre";
            gridViewTextBoxColumn57.HeaderText = "Emisor";
            gridViewTextBoxColumn57.Name = "Nombre";
            gridViewTextBoxColumn57.Width = 200;
            gridViewTextBoxColumn58.FieldName = "Serie";
            gridViewTextBoxColumn58.HeaderText = "Serie";
            gridViewTextBoxColumn58.Name = "Serie";
            gridViewTextBoxColumn58.Width = 85;
            gridViewTextBoxColumn59.FieldName = "Folio";
            gridViewTextBoxColumn59.HeaderText = "Folio";
            gridViewTextBoxColumn59.Name = "Folio";
            gridViewTextBoxColumn59.Width = 85;
            gridViewTextBoxColumn60.FieldName = "Total";
            gridViewTextBoxColumn60.FormatString = "{0:n}";
            gridViewTextBoxColumn60.HeaderText = "Total";
            gridViewTextBoxColumn60.Name = "Total";
            gridViewTextBoxColumn60.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn60.Width = 90;
            this.GridComprobanteCfdiRelacionadosCfdiRelacionado.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn55,
            gridViewTextBoxColumn56,
            gridViewTextBoxColumn57,
            gridViewTextBoxColumn58,
            gridViewTextBoxColumn59,
            gridViewTextBoxColumn60});
            this.GridComprobanteCfdiRelacionadosCfdiRelacionado.ViewDefinition = tableViewDefinition3;
            // 
            // GridContable
            // 
            this.GridContable.Caption = "Pagos / Cobros";
            gridViewTextBoxColumn61.DataType = typeof(int);
            gridViewTextBoxColumn61.FieldName = "_cntbl3_folio";
            gridViewTextBoxColumn61.FormatString = "{0:000#}";
            gridViewTextBoxColumn61.HeaderText = "Folio";
            gridViewTextBoxColumn61.Name = "_cntbl3_folio";
            gridViewTextBoxColumn61.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn62.FieldName = "_cntbl3_status";
            gridViewTextBoxColumn62.HeaderText = "Status";
            gridViewTextBoxColumn62.Name = "_cntbl3_status";
            gridViewTextBoxColumn62.Width = 80;
            gridViewTextBoxColumn63.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn63.FieldName = "_cntbl3_fecems";
            gridViewTextBoxColumn63.FormatString = "{0:d}";
            gridViewTextBoxColumn63.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn63.Name = "_cntbl3_fecems";
            gridViewTextBoxColumn63.Width = 75;
            gridViewTextBoxColumn64.FieldName = "_cntbl3_fecdoc";
            gridViewTextBoxColumn64.FormatString = "{0:d}";
            gridViewTextBoxColumn64.HeaderText = "Fec. Cobro";
            gridViewTextBoxColumn64.Name = "_cntbl3_fecdoc";
            gridViewTextBoxColumn64.Width = 75;
            gridViewTextBoxColumn65.FieldName = "NumAutorizacion";
            gridViewTextBoxColumn65.HeaderText = "NumAutorización";
            gridViewTextBoxColumn65.Name = "NumAutorizacion";
            gridViewTextBoxColumn66.DataType = typeof(double);
            gridViewTextBoxColumn66.FieldName = "_cntbl3c_cargo";
            gridViewTextBoxColumn66.FormatString = "{0:n}";
            gridViewTextBoxColumn66.HeaderText = "Cargo";
            gridViewTextBoxColumn66.Name = "_cntbl3c_cargo";
            gridViewTextBoxColumn66.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn66.Width = 75;
            gridViewTextBoxColumn67.DataType = typeof(double);
            gridViewTextBoxColumn67.FieldName = "_cntbl3c_abono";
            gridViewTextBoxColumn67.FormatString = "{0:n}";
            gridViewTextBoxColumn67.HeaderText = "Abono";
            gridViewTextBoxColumn67.Name = "_cntbl3c_abono";
            gridViewTextBoxColumn67.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn67.Width = 75;
            gridViewTextBoxColumn68.FieldName = "_cntbl3_usr_n";
            gridViewTextBoxColumn68.HeaderText = "Creó";
            gridViewTextBoxColumn68.Name = "_cntbl3_usr_n";
            this.GridContable.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn61,
            gridViewTextBoxColumn62,
            gridViewTextBoxColumn63,
            gridViewTextBoxColumn64,
            gridViewTextBoxColumn65,
            gridViewTextBoxColumn66,
            gridViewTextBoxColumn67,
            gridViewTextBoxColumn68});
            this.GridContable.ViewDefinition = tableViewDefinition5;
            // 
            // GridComplementoPagos
            // 
            this.GridComplementoPagos.Caption = "Complemento Pagos";
            gridViewTextBoxColumn69.FieldName = "FecPago";
            gridViewTextBoxColumn69.FormatString = "{0:d}";
            gridViewTextBoxColumn69.HeaderText = "Fec. Pago";
            gridViewTextBoxColumn69.Name = "FecPago";
            gridViewTextBoxColumn69.Width = 85;
            gridViewTextBoxColumn70.FieldName = "FormaDePago";
            gridViewTextBoxColumn70.HeaderText = "Forma de Pago";
            gridViewTextBoxColumn70.Name = "FormaDePago";
            gridViewTextBoxColumn71.FieldName = "Moneda";
            gridViewTextBoxColumn71.HeaderText = "Moneda";
            gridViewTextBoxColumn71.Name = "Moneda";
            gridViewTextBoxColumn71.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn72.FieldName = "TipoCambio";
            gridViewTextBoxColumn72.FormatString = "{0:n}";
            gridViewTextBoxColumn72.HeaderText = "Tipo de Cambio";
            gridViewTextBoxColumn72.Name = "TipoCambio";
            gridViewTextBoxColumn72.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn73.FieldName = "Monto";
            gridViewTextBoxColumn73.FormatString = "{0:n}";
            gridViewTextBoxColumn73.HeaderText = "Monto";
            gridViewTextBoxColumn73.Name = "Monto";
            gridViewTextBoxColumn73.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn73.Width = 85;
            gridViewTextBoxColumn74.FieldName = "NumOperacion";
            gridViewTextBoxColumn74.HeaderText = "Num. Operación";
            gridViewTextBoxColumn74.Name = "NumOperacion";
            gridViewTextBoxColumn74.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn74.Width = 80;
            gridViewTextBoxColumn75.FieldName = "RfcEmisorCtaOrd";
            gridViewTextBoxColumn75.HeaderText = "RFC Emisor Cta. Ord.";
            gridViewTextBoxColumn75.Name = "RfcEmisorCtaOrd";
            gridViewTextBoxColumn76.FieldName = "NomBancoOrdExt";
            gridViewTextBoxColumn76.HeaderText = "Nom. Banco Ord. Ext.";
            gridViewTextBoxColumn76.Name = "NomBancoOrdExt";
            gridViewTextBoxColumn76.Width = 80;
            gridViewTextBoxColumn77.FieldName = "CtaOrdenante";
            gridViewTextBoxColumn77.HeaderText = "Cta. Ordenante";
            gridViewTextBoxColumn77.Name = "CtaOrdenante";
            gridViewTextBoxColumn77.Width = 80;
            gridViewTextBoxColumn78.FieldName = "RfcEmisorCtaBen";
            gridViewTextBoxColumn78.HeaderText = "RFC Emisor Cta. Ben.";
            gridViewTextBoxColumn78.Name = "RfcEmisorCtaBen";
            gridViewTextBoxColumn79.FieldName = "CtaBeneficiario";
            gridViewTextBoxColumn79.HeaderText = "Cta. Beneficiario";
            gridViewTextBoxColumn79.Name = "CtaBeneficiario";
            gridViewTextBoxColumn79.Width = 80;
            gridViewTextBoxColumn80.FieldName = "TipoCadPago";
            gridViewTextBoxColumn80.HeaderText = "Tipo Cad. Pago";
            gridViewTextBoxColumn80.Name = "TipoCadPago";
            gridViewTextBoxColumn81.HeaderText = "data";
            gridViewTextBoxColumn81.IsVisible = false;
            gridViewTextBoxColumn81.Name = "data";
            gridViewTextBoxColumn81.VisibleInColumnChooser = false;
            this.GridComplementoPagos.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn69,
            gridViewTextBoxColumn70,
            gridViewTextBoxColumn71,
            gridViewTextBoxColumn72,
            gridViewTextBoxColumn73,
            gridViewTextBoxColumn74,
            gridViewTextBoxColumn75,
            gridViewTextBoxColumn76,
            gridViewTextBoxColumn77,
            gridViewTextBoxColumn78,
            gridViewTextBoxColumn79,
            gridViewTextBoxColumn80,
            gridViewTextBoxColumn81});
            this.GridComplementoPagos.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.GridComplementoDoctoRelacionado});
            this.GridComplementoPagos.ViewDefinition = tableViewDefinition7;
            // 
            // GridComplementoDoctoRelacionado
            // 
            this.GridComplementoDoctoRelacionado.Caption = "GridComplementoDoctoRelacionado";
            gridViewTextBoxColumn82.FieldName = "IdDocumento";
            gridViewTextBoxColumn82.HeaderText = "IdDocumento";
            gridViewTextBoxColumn82.Name = "IdDocumento";
            gridViewTextBoxColumn82.Width = 225;
            gridViewTextBoxColumn83.FieldName = "Moneda";
            gridViewTextBoxColumn83.HeaderText = "Moneda";
            gridViewTextBoxColumn83.Name = "Moneda";
            gridViewTextBoxColumn83.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn84.FieldName = "TipoCambio";
            gridViewTextBoxColumn84.HeaderText = "Tipo de Cambio";
            gridViewTextBoxColumn84.Name = "TipoCambio";
            gridViewTextBoxColumn84.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn85.FieldName = "MetodoDePago";
            gridViewTextBoxColumn85.HeaderText = "Metodo de Pago";
            gridViewTextBoxColumn85.Name = "MetodoDePago";
            gridViewTextBoxColumn86.FieldName = "NumParcialidad";
            gridViewTextBoxColumn86.HeaderText = "Num. Parcialidad";
            gridViewTextBoxColumn86.Name = "NumParcialidad";
            gridViewTextBoxColumn86.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn87.FieldName = "ImpSaldoAnterior";
            gridViewTextBoxColumn87.FormatString = "{0:n}";
            gridViewTextBoxColumn87.HeaderText = "Saldo Anterior";
            gridViewTextBoxColumn87.Name = "ImpSaldoAnterior";
            gridViewTextBoxColumn87.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn87.Width = 85;
            gridViewTextBoxColumn88.FieldName = "ImpPagado";
            gridViewTextBoxColumn88.FormatString = "{0:n}";
            gridViewTextBoxColumn88.HeaderText = "Imp. Pagado";
            gridViewTextBoxColumn88.Name = "ImpPagado";
            gridViewTextBoxColumn88.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn88.Width = 85;
            gridViewTextBoxColumn89.FieldName = "ImpSaldoInsoluto";
            gridViewTextBoxColumn89.FormatString = "{0:n}";
            gridViewTextBoxColumn89.HeaderText = "Saldo Insoluto";
            gridViewTextBoxColumn89.Name = "ImpSaldoInsoluto";
            gridViewTextBoxColumn89.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn89.Width = 85;
            gridViewTextBoxColumn90.FieldName = "Estado";
            gridViewTextBoxColumn90.HeaderText = "Estado";
            gridViewTextBoxColumn90.Name = "Estado";
            gridViewTextBoxColumn90.Width = 85;
            this.GridComplementoDoctoRelacionado.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn82,
            gridViewTextBoxColumn83,
            gridViewTextBoxColumn84,
            gridViewTextBoxColumn85,
            gridViewTextBoxColumn86,
            gridViewTextBoxColumn87,
            gridViewTextBoxColumn88,
            gridViewTextBoxColumn89,
            gridViewTextBoxColumn90});
            this.GridComplementoDoctoRelacionado.ViewDefinition = tableViewDefinition6;
            // 
            // GridRecepcionPagos
            // 
            this.GridRecepcionPagos.Caption = "Pagos Relacionados";
            gridViewTextBoxColumn91.FieldName = "Folio";
            gridViewTextBoxColumn91.HeaderText = "Folio";
            gridViewTextBoxColumn91.Name = "Folio";
            gridViewTextBoxColumn92.FieldName = "Serie";
            gridViewTextBoxColumn92.HeaderText = "Serie";
            gridViewTextBoxColumn92.Name = "Serie";
            gridViewTextBoxColumn93.FieldName = "Emisor";
            gridViewTextBoxColumn93.HeaderText = "Emisor";
            gridViewTextBoxColumn93.Name = "Emisor";
            gridViewTextBoxColumn93.Width = 220;
            gridViewTextBoxColumn94.FieldName = "EmisorRFC";
            gridViewTextBoxColumn94.HeaderText = "RFC";
            gridViewTextBoxColumn94.Name = "EmisorRFC";
            gridViewTextBoxColumn94.Width = 85;
            gridViewTextBoxColumn95.FieldName = "IdDocumento";
            gridViewTextBoxColumn95.HeaderText = "IdDocumento";
            gridViewTextBoxColumn95.Name = "IdDocumento";
            gridViewTextBoxColumn95.Width = 220;
            gridViewTextBoxColumn96.FieldName = "FechaEmision";
            gridViewTextBoxColumn96.HeaderText = "Fecha Emisión";
            gridViewTextBoxColumn96.Name = "FechaEmision";
            gridViewTextBoxColumn96.Width = 85;
            gridViewTextBoxColumn97.FieldName = "Moneda";
            gridViewTextBoxColumn97.HeaderText = "Moneda";
            gridViewTextBoxColumn97.Name = "Moneda";
            gridViewTextBoxColumn98.DataType = typeof(decimal);
            gridViewTextBoxColumn98.FieldName = "TipoCambio";
            gridViewTextBoxColumn98.HeaderText = "Tipo de Cambio";
            gridViewTextBoxColumn98.Name = "TipoCambio";
            gridViewTextBoxColumn98.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn99.FieldName = "MetodoPago";
            gridViewTextBoxColumn99.HeaderText = "Método de Pago";
            gridViewTextBoxColumn99.Name = "MetodoPago";
            gridViewTextBoxColumn100.DataType = typeof(int);
            gridViewTextBoxColumn100.FieldName = "NumParcialidad";
            gridViewTextBoxColumn100.HeaderText = "Núm. Parcialidad";
            gridViewTextBoxColumn100.Name = "NumParcialidad";
            gridViewTextBoxColumn100.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn101.DataType = typeof(decimal);
            gridViewTextBoxColumn101.FieldName = "SaldoAnterior";
            gridViewTextBoxColumn101.FormatString = "{0:n}";
            gridViewTextBoxColumn101.HeaderText = "Saldo Anterior";
            gridViewTextBoxColumn101.Name = "SaldoAnterior";
            gridViewTextBoxColumn101.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn101.Width = 85;
            gridViewTextBoxColumn102.DataType = typeof(decimal);
            gridViewTextBoxColumn102.FieldName = "ImportePagado";
            gridViewTextBoxColumn102.FormatString = "{0:n}";
            gridViewTextBoxColumn102.HeaderText = "Importe Pagado";
            gridViewTextBoxColumn102.Name = "ImportePagado";
            gridViewTextBoxColumn102.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn102.Width = 85;
            gridViewTextBoxColumn103.DataType = typeof(decimal);
            gridViewTextBoxColumn103.FieldName = "SaldoInsoluto";
            gridViewTextBoxColumn103.FormatString = "{0:n}";
            gridViewTextBoxColumn103.HeaderText = "Saldo Insoluto";
            gridViewTextBoxColumn103.Name = "SaldoInsoluto";
            gridViewTextBoxColumn103.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn103.Width = 85;
            this.GridRecepcionPagos.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn91,
            gridViewTextBoxColumn92,
            gridViewTextBoxColumn93,
            gridViewTextBoxColumn94,
            gridViewTextBoxColumn95,
            gridViewTextBoxColumn96,
            gridViewTextBoxColumn97,
            gridViewTextBoxColumn98,
            gridViewTextBoxColumn99,
            gridViewTextBoxColumn100,
            gridViewTextBoxColumn101,
            gridViewTextBoxColumn102,
            gridViewTextBoxColumn103});
            this.GridRecepcionPagos.ViewDefinition = tableViewDefinition8;
            // 
            // GridNotasCredito
            // 
            this.GridNotasCredito.Caption = "Notas de Crédito";
            gridViewTextBoxColumn104.FieldName = "Clave";
            gridViewTextBoxColumn104.HeaderText = "Clave";
            gridViewTextBoxColumn104.Name = "Clave";
            gridViewTextBoxColumn104.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn105.FieldName = "Descripcion";
            gridViewTextBoxColumn105.HeaderText = "Descripción";
            gridViewTextBoxColumn105.Name = "Descripcion";
            gridViewTextBoxColumn105.Width = 310;
            gridViewTextBoxColumn106.FieldName = "Folio";
            gridViewTextBoxColumn106.HeaderText = "Folio";
            gridViewTextBoxColumn106.Name = "Folio";
            gridViewTextBoxColumn107.FieldName = "Serie";
            gridViewTextBoxColumn107.HeaderText = "Serie";
            gridViewTextBoxColumn107.Name = "Serie";
            gridViewTextBoxColumn108.FieldName = "IdDocumento";
            gridViewTextBoxColumn108.HeaderText = "UUID";
            gridViewTextBoxColumn108.Name = "IdDocumento";
            gridViewTextBoxColumn108.Width = 220;
            gridViewTextBoxColumn109.FieldName = "RFC";
            gridViewTextBoxColumn109.HeaderText = "RFC";
            gridViewTextBoxColumn109.Name = "RFC";
            gridViewTextBoxColumn109.Width = 85;
            gridViewTextBoxColumn110.FieldName = "Nombre";
            gridViewTextBoxColumn110.HeaderText = "Identidad Fiscal";
            gridViewTextBoxColumn110.Name = "Nombre";
            gridViewTextBoxColumn110.Width = 250;
            gridViewTextBoxColumn111.DataType = typeof(decimal);
            gridViewTextBoxColumn111.FormatString = "{0:n}";
            gridViewTextBoxColumn111.HeaderText = "Total";
            gridViewTextBoxColumn111.Name = "ImpTotal";
            gridViewTextBoxColumn111.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn111.Width = 85;
            this.GridNotasCredito.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn104,
            gridViewTextBoxColumn105,
            gridViewTextBoxColumn106,
            gridViewTextBoxColumn107,
            gridViewTextBoxColumn108,
            gridViewTextBoxColumn109,
            gridViewTextBoxColumn110,
            gridViewTextBoxColumn111});
            this.GridNotasCredito.ViewDefinition = tableViewDefinition9;
            // 
            // MenuContextual
            // 
            this.MenuContextual.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ContextMenuSeleccion,
            this.ContextMenuCopiar,
            this.ContextMenuClonar,
            this.ContextMenuSustituir,
            this.ContextMenuCrearPago,
            this.ContextMenuCrearNotaCredito,
            this.ContextMenuAplicarNotaCredito,
            this.ContextMenuReciboCobro,
            this.ContextMenuReciboPago,
            this.ContextMenuDoctoRelacionado,
            this.ContextMenuSerializarCFDI,
            this.ContextMenuCargarArchivo});
            // 
            // ContextMenuSeleccion
            // 
            this.ContextMenuSeleccion.Name = "ContextMenuSeleccion";
            this.ContextMenuSeleccion.Text = "Selección múltiple";
            this.ContextMenuSeleccion.Click += new System.EventHandler(this.ContextMenuSeleccion_Click);
            // 
            // ContextMenuCopiar
            // 
            this.ContextMenuCopiar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_copiar_portapapeles;
            this.ContextMenuCopiar.Name = "ContextMenuCopiar";
            this.ContextMenuCopiar.Text = "Copiar";
            this.ContextMenuCopiar.Click += new System.EventHandler(this.ContextMenuCopiar_Click);
            // 
            // ContextMenuClonar
            // 
            this.ContextMenuClonar.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_copiar;
            this.ContextMenuClonar.Name = "ContextMenuClonar";
            this.ContextMenuClonar.Text = "Clonar Comprobante";
            this.ContextMenuClonar.Click += new System.EventHandler(this.ContextMenuClonar_Click);
            // 
            // ContextMenuSustituir
            // 
            this.ContextMenuSustituir.Name = "ContextMenuSustituir";
            this.ContextMenuSustituir.Text = "Sustituir Comprobante";
            this.ContextMenuSustituir.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ContextMenuSustituir.Click += new System.EventHandler(this.ContextMenuSustituir_Click);
            // 
            // ContextMenuCrearPago
            // 
            this.ContextMenuCrearPago.Name = "ContextMenuCrearPago";
            this.ContextMenuCrearPago.Text = "Crear Recibo Electrónico de Pago";
            this.ContextMenuCrearPago.Click += new System.EventHandler(this.ContextMenuCrearPago_Click);
            // 
            // ContextMenuCrearNotaCredito
            // 
            this.ContextMenuCrearNotaCredito.Name = "ContextMenuCrearNotaCredito";
            this.ContextMenuCrearNotaCredito.Text = "Crear Nota de Crédito";
            this.ContextMenuCrearNotaCredito.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ContextMenuCrearNotaCredito.Click += new System.EventHandler(this.ContextMenuCrearNotaCredito_Click);
            // 
            // ContextMenuAplicarNotaCredito
            // 
            this.ContextMenuAplicarNotaCredito.Name = "ContextMenuAplicarNotaCredito";
            this.ContextMenuAplicarNotaCredito.Text = "Aplicar Nota de Crédito";
            this.ContextMenuAplicarNotaCredito.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ContextMenuAplicarNotaCredito.Click += new System.EventHandler(this.ContextMenuAplicarNotaCredito_Click);
            // 
            // ContextMenuReciboCobro
            // 
            this.ContextMenuReciboCobro.Name = "ContextMenuReciboCobro";
            this.ContextMenuReciboCobro.Text = "Crear Recibo de Cobro";
            this.ContextMenuReciboCobro.Click += new System.EventHandler(this.ContextMenuReciboCobro_Click);
            // 
            // ContextMenuReciboPago
            // 
            this.ContextMenuReciboPago.Name = "ContextMenuReciboPago";
            this.ContextMenuReciboPago.Text = "Crear Recibo de Pago";
            this.ContextMenuReciboPago.Click += new System.EventHandler(this.ContextMenuReciboPago_Click);
            // 
            // ContextMenuDoctoRelacionado
            // 
            this.ContextMenuDoctoRelacionado.Name = "ContextMenuDoctoRelacionado";
            this.ContextMenuDoctoRelacionado.Text = "Agregar Docto. Relacionado";
            this.ContextMenuDoctoRelacionado.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // ContextMenuSerializarCFDI
            // 
            this.ContextMenuSerializarCFDI.Name = "ContextMenuSerializarCFDI";
            this.ContextMenuSerializarCFDI.Text = "Serializar Comprobante";
            this.ContextMenuSerializarCFDI.ToolTipText = "Recargar información desde el comprobante almacenado.";
            this.ContextMenuSerializarCFDI.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ContextMenuSerializarCFDI.Click += new System.EventHandler(this.ContextMenuSerializarCFDI_Click);
            // 
            // ContextMenuCargarArchivo
            // 
            this.ContextMenuCargarArchivo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ContextButtonSubirXML,
            this.ContextButtonSubirPDF});
            this.ContextMenuCargarArchivo.Name = "ContextMenuCargarArchivo";
            this.ContextMenuCargarArchivo.Text = "Cargar archivos";
            // 
            // ContextButtonSubirXML
            // 
            this.ContextButtonSubirXML.Image = global::Jaeger.UI.Properties.Resources.xml16;
            this.ContextButtonSubirXML.Name = "ContextButtonSubirXML";
            this.ContextButtonSubirXML.Text = "Comprobante Fiscal CFDI (XML)";
            this.ContextButtonSubirXML.Click += new System.EventHandler(this.ContextButtonSubirXml_Click);
            // 
            // ContextButtonSubirPDF
            // 
            this.ContextButtonSubirPDF.Image = global::Jaeger.UI.Properties.Resources.icons8_x16_pdf;
            this.ContextButtonSubirPDF.Name = "ContextButtonSubirPDF";
            this.ContextButtonSubirPDF.Text = "Representación Impresa (PDF)";
            this.ContextButtonSubirPDF.Click += new System.EventHandler(this.ContextButtonSubirPdf_Click);
            // 
            // Waiting
            // 
            this.Waiting.AssociatedControl = this.GridData;
            this.Waiting.Location = new System.Drawing.Point(551, 236);
            this.Waiting.Name = "Waiting";
            this.Waiting.Size = new System.Drawing.Size(70, 70);
            this.Waiting.TabIndex = 1;
            this.Waiting.Text = "radWaitingBar1";
            this.Waiting.WaitingIndicators.Add(this.dotsSpinnerWaitingBarIndicatorElement1);
            this.Waiting.WaitingSpeed = 100;
            this.Waiting.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsSpinner;
            ((Telerik.WinControls.UI.RadWaitingBarElement)(this.Waiting.GetChildAt(0))).WaitingSpeed = 100;
            ((Telerik.WinControls.UI.WaitingBarContentElement)(this.Waiting.GetChildAt(0).GetChildAt(0))).WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsSpinner;
            ((Telerik.WinControls.UI.WaitingBarSeparatorElement)(this.Waiting.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Dash = false;
            // 
            // dotsSpinnerWaitingBarIndicatorElement1
            // 
            this.dotsSpinnerWaitingBarIndicatorElement1.Name = "dotsSpinnerWaitingBarIndicatorElement1";
            // 
            // Iconos
            // 
            this.Iconos.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Iconos.ImageStream")));
            this.Iconos.TransparentColor = System.Drawing.Color.Transparent;
            this.Iconos.Images.SetKeyName(0, "UrlXML");
            this.Iconos.Images.SetKeyName(1, "UrlPDF");
            // 
            // ToolBarButtonHerramientas
            // 
            this.ToolBarButtonHerramientas.DisplayName = "Herramientas";
            this.ToolBarButtonHerramientas.DrawText = false;
            this.ToolBarButtonHerramientas.Image = global::Jaeger.UI.Properties.Resources.icons8_x32_caja_herramientas;
            this.ToolBarButtonHerramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonMetodo1});
            this.ToolBarButtonHerramientas.Name = "ToolBarButtonHerramientas";
            this.ToolBarButtonHerramientas.Text = "Herramientas";
            this.ToolBarButtonHerramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonMetodo1
            // 
            this.ToolBarButtonMetodo1.Name = "ToolBarButtonMetodo1";
            this.ToolBarButtonMetodo1.Text = "Método 1";
            this.ToolBarButtonMetodo1.Click += new System.EventHandler(this.ToolBarButtonMetodo1_Click);
            // 
            // ViewComprobantesFiscales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1297, 648);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.CommandBar);
            this.Name = "ViewComprobantesFiscales";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ComprobantesFiscales";
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobanteCfdiRelacionados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobanteCfdiRelacionadosCfdiRelacionado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridContable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComplementoPagos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComplementoDoctoRelacionado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridRecepcionPagos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridNotasCredito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Waiting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        

        #endregion

        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRow;
        internal Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarEdit;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarDelete;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator1;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarRefresh;
        internal Telerik.WinControls.UI.CommandBarToggleButton ToolBarFilter;
        internal Telerik.WinControls.UI.CommandBarToggleButton ToolBarAutoSum;
        internal Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonValidar;
        internal Telerik.WinControls.UI.RadCommandBar CommandBar;
        internal Telerik.WinControls.UI.GridViewTemplate GridFiles;
        internal Telerik.WinControls.UI.GridViewTemplate GridConceptos;
        internal Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarPrint;
        private Telerik.WinControls.UI.RadMenuItem PrintAcuse;
        private Telerik.WinControls.UI.RadMenuItem PrintComprobante;
        private Telerik.WinControls.UI.RadMenuItem PrintValidacion;
        private Telerik.WinControls.UI.GridViewTemplate GridComprobanteCfdiRelacionados;
        private Telerik.WinControls.UI.RadMenuItem PrintLista;
        private Telerik.WinControls.UI.CommandBarButton ToolBarLayOut;
        private Telerik.WinControls.UI.GridViewTemplate GridContable;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarNew;
        private Telerik.WinControls.UI.RadMenuItem ToolBarComprobanteNuevo;
        private Telerik.WinControls.UI.RadMenuItem ToolBarReciboCobro;
        private Telerik.WinControls.UI.RadMenuItem ToolBarReciboPago;
        private Telerik.WinControls.UI.RadMenuItem ToolBarComprobanteLink;
        private Telerik.WinControls.UI.GridViewTemplate GridComplementoPagos;
        private Telerik.WinControls.UI.GridViewTemplate GridComplementoDoctoRelacionado;
        private Telerik.WinControls.UI.RadMenuItem ToolBarComprobantePago;
        private Telerik.WinControls.UI.RadMenuItem ToolBarComprobanteClonar;
        private Telerik.WinControls.UI.RadMenuItem ToolBarComprobanteSustituir;
        private Telerik.WinControls.UI.RadContextMenu MenuContextual;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuClonar;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuCrearPago;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuSustituir;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuReciboCobro;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuReciboPago;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuCopiar;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuSeleccion;
        private Telerik.WinControls.UI.GridViewTemplate GridComprobanteCfdiRelacionadosCfdiRelacionado;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuCargarArchivo;
        private Telerik.WinControls.UI.RadMenuItem ContextButtonSubirXML;
        private Telerik.WinControls.UI.RadMenuItem ContextButtonSubirPDF;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonPrintEdoCuentaSimple;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonEnviar;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonExport;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonExportarExcel;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonExportarExcelReporte1;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuCrearNotaCredito;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuDoctoRelacionado;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuSerializarCFDI;
        private Telerik.WinControls.UI.GridViewTemplate GridRecepcionPagos;
        private Telerik.WinControls.UI.GridViewTemplate GridNotasCredito;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuAplicarNotaCredito;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelPeriodo;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarPeriodo;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelEjercicio;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarEjercicio;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.CommandBarButton ToolBarClose;
        private Telerik.WinControls.UI.RadWaitingBar Waiting;
        private Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement dotsSpinnerWaitingBarIndicatorElement1;
        private System.Windows.Forms.ImageList Iconos;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonValidarEstado;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonValidarSAT;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonProductos;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonPDFValidacion;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonHerramientas;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonMetodo1;
    }
}
