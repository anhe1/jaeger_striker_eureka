﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.Enumerations;
using Jaeger.Helpers;
using Jaeger.Edita.V2;
using Jaeger.Edita.V2.Almacen.Enums;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Base.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.Views
{
    public partial class ViewComprobanteProductos : Telerik.WinControls.UI.RadForm
    {
        private BackgroundWorker consultar;
        private SqlSugarProdServ data;
        private BindingList<ViewModelComprobanteModelo> datos;

        public ViewComprobanteProductos()
        {
            InitializeComponent();
        }

        private void ViewComprobanteProductos_Load(object sender, EventArgs e)
        {
            this.GridData.TelerikGridCommon();
            this.data = new SqlSugarProdServ(ConfigService.Synapsis.RDS.Edita);
            this.consultar = new BackgroundWorker();
            this.consultar.DoWork += ConsultarDoWork;
            this.consultar.RunWorkerCompleted += ConsultarRunWorkerCompleted;
        }

        #region barra de herramientas

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e)
        {
            this.Espera.StartWaiting();
            this.Espera.Visible = true;
            this.ToolBarButtonActualizar.Enabled = false;
            System.Threading.Thread.Sleep(10);
            this.consultar.RunWorkerAsync();
        }

        private void ToolBarButtonNuevo_Click(object sender, EventArgs e)
        {
            var nuevo = new ViewComprobanteProducto(null) { StartPosition = FormStartPosition.CenterParent };
            nuevo.ShowDialog(this);
        }

        private void ToolBarButtonEditar_Click(object sender, EventArgs e)
        {
            if (this.GridData.CurrentRow != null)
            {
                var producto = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteModelo;
                if (producto != null)
                {
                    var editar = new ViewComprobanteProducto(producto) { StartPosition = FormStartPosition.CenterParent };
                    editar.ShowDialog(this);
                }
            }
        }

        private void ToolBarButtonEliminar_Click(object sender, EventArgs e)
        {
            if (this.GridData.CurrentRow != null)
                if (RadMessageBox.Show("¿Esta seguro de eliminar este producto del catálogo?", "Atención", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    using (var espera = new Waiting2Form(this.Eliminar))
                    {
                        espera.Text = "Eliminando ...";
                        espera.ShowDialog(this);
                    }
                }
        }

        private void ToolBarButtonFiltrar_Click(object sender, EventArgs e)
        {
            this.GridData.ShowFilteringRow = this.ToolBarButtonFiltrar.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false)
                this.GridData.FilterDescriptors.Clear();
        }

        private void ToolBarButtonExportar_Click(object sender, EventArgs e)
        {
            TelerikGridExport exportar = new TelerikGridExport(this.GridData) { StartPosition = FormStartPosition.CenterParent };
            exportar.ShowDialog();
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region consulta de datos

        private void ConsultarDoWork(object sender, DoWorkEventArgs e)
        {
            this.datos = new BindingList<ViewModelComprobanteModelo>(this.data.Modelos.GetListConceptos(EnumAlmacen.PT));
        }

        private void ConsultarRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.GridData.DataSource = datos;
            this.Espera.Visible = false;
            this.Espera.StopWaiting();
            this.ToolBarButtonActualizar.Enabled = true;
        }

        #endregion

        private void Eliminar()
        {
            var producto = this.GridData.CurrentRow.DataBoundItem as ViewModelComprobanteModelo;
            if (producto != null)
            {
                producto.Activo = false;
                this.data.Modelos.Save(producto);
                this.GridData.CurrentRow.IsVisible = false;
            }
        }

        private void GridData_RowsChanged(object sender, Telerik.WinControls.UI.GridViewCollectionChangedEventArgs e)
        {
            this.ToolBarButtonEditar.Enabled = this.GridData.Rows.Count > 0;
            this.ToolBarButtonEliminar.Enabled = this.GridData.Rows.Count > 0;
            this.ToolBarButtonFiltrar.Enabled = this.GridData.Rows.Count > 0;
            this.ToolBarButtonExportar.Enabled = this.GridData.Rows.Count > 0;
        }
    }
}
