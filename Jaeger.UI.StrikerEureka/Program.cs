﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Base.Services;
using Jaeger.Helpers;
using Jaeger.UI.Login.Forms;

namespace Jaeger {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // creamos y comprobamos los directorios para uso local
            JaegerManagerPaths.CreatePaths();
            Application.Run(new LoginForm(args) { LogoTipo = UI.Properties.Resources.icono_final, Text = "EDITA Validador" });

            if (!(ConfigService.Synapsis == null) & !(ConfigService.Piloto == null)) {
                Application.Run(new Views.ViewPrincipalRibbon());
            }
        }
    }
}