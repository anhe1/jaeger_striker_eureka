﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Comprobantes;
using Jaeger.Catalogos.Repositories;
using Jaeger.Edita.Entities.Amazon;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2;
using Jaeger.Edita.V2.CFDI.Entities;
using Jaeger.Edita.V2.CFDI.Entities.Complemento;
using Jaeger.Edita.V2.CFDI.Enums;
using Jaeger.Edita.V2.Directorio.Entities;
using Jaeger.Edita.V2.Directorio.Enums;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Enums;
using Jaeger.SAT.Entities;
using Jaeger.Validador.V3;
using Jaeger.Validador.V3.Entities;

namespace Jaeger.Helpers {
    public class HelperValidador : Administrador {
        private readonly ISqlComprobanteFiscal data;
        private readonly SqlSugarDirectorio dir;
        private readonly SqlSugarBucket bucket;
        protected IEditaBucketService s3;
        private readonly CFDIExtension convertidorExt;
        private readonly CFDIExtension40 convertidorExt40;
        private readonly FormaPagoCatalogo formaPago = new FormaPagoCatalogo();
        private BackgroundWorker backup = new BackgroundWorker();
        private Progress<Progreso> reporteValidacion;
        private Progress<Progreso> reporteBusqueda;
        private Stopwatch tiempo;
        private bool validarTodos = false;
        private bool actulizarBucket = false;
        //private SAT.Consulta.Interfaces.IStatus statusSerticeSAT = new SAT.Consulta.Services.Status();

        protected virtual void OnSearchFiles(ProcessChanged e) {
            EventHandler<ProcessChanged> onSearchFiles = SearchFiles;
            if (onSearchFiles != null) {
                onSearchFiles(this, e);
            }
        }

        protected virtual void OnValidacionChanged(ProcessChanged e) {
            EventHandler<ProcessChanged> onValidacionChanged = ValidacionChanged;
            if (onValidacionChanged != null) {
                onValidacionChanged(this, e);
            }
        }

        protected virtual void OnValidacionCompleted(ProcessCompleted e) {
            EventHandler<ProcessCompleted> onValidacionCompleted = ValidacionCompleted;
            if (onValidacionCompleted != null) {
                onValidacionCompleted(this, e);
            }
        }

        protected virtual void OnBackupChanged(ProcessChanged e) {
            EventHandler<ProcessChanged> onBackupChanged = BackupChanged;
            if (onBackupChanged != null) {
                onBackupChanged(this, e);
            }
        }

        protected virtual void OnBackupCompleted(ProcessCompleted e) {
            EventHandler<ProcessCompleted> onBackupCompleted = BackupCompleted;
            if (onBackupCompleted != null) {
                onBackupCompleted(this, e);
            }
        }

        public event EventHandler<ProcessChanged> SearchFiles;
        public event EventHandler<ProcessChanged> ValidacionChanged;
        public event EventHandler<ProcessCompleted> ValidacionCompleted;
        public event EventHandler<ProcessChanged> BackupChanged;
        public event EventHandler<ProcessCompleted> BackupCompleted;

        public HelperValidador(Domain.Empresa.Entities.Configuracion objeto, Configuracion configuracion) : base(configuracion) {
            this.data = new MySqlComprobanteFiscal(objeto.RDS.Edita);
            this.dir = new SqlSugarDirectorio(objeto.RDS.Edita);
            this.s3 = new Aplication.Comprobantes.EditaBucketService(objeto.Amazon.S3, ConfigService.Synapsis.ProveedorAutorizado.Certificacion.Production);
            this.convertidorExt = new CFDIExtension();
            this.convertidorExt40 = new CFDIExtension40();
            this.AutoBackup = true;
            this.formaPago.Load();
            this.tiempo = new Stopwatch();
        }

        public bool MostrarAvisos { get; set; }

        /// <summary>
        /// Comprobar estado del comprobante
        /// </summary>
        public async Task<SatQueryResult> ExecuteEstadoSAT(int index) {
            //var e = await HelperServiceQuerySAT.QueryAsync(this.Documentos[index].EmisorRFC, this.Documentos[index].ReceptorRFC, this.Documentos[index].Total, this.Documentos[index].IdDocumento);
            var e = await ConsultaSAT.QueryAsync(this.Documentos[index].EmisorRFC, this.Documentos[index].ReceptorRFC, this.Documentos[index].Total, this.Documentos[index].IdDocumento);
            this.Documentos[index].EstadoSAT = e.Status;
            return null/* TODO Change to default(_) if this is not a reference type */;
        }

        public void ExecuteBackUp(int index) {
            // ' archivo XML
            if ((this.Documentos[index].ArchivoXML != null)) {
                this.Documentos[index].ArchivoXML = this.BackupS3(this.Documentos[index].ArchivoXML, this.Documentos[index].KeyName, this.Documentos[index].Id, this.Documentos[index].IdDocumento);
                if ((this.Documentos[index].ArchivoXML.Updated)) {
                    if ((this.data.UpdateUrlXml(this.Documentos[index].Id, this.Documentos[index].ArchivoXML.URL)))
                        Console.WriteLine("URL Actualizada: " + this.Documentos[index].ArchivoXML.URL);
                }
            }

            // ' archivo PDF
            if ((this.Documentos[index].ArchivoPDF != null)) {
                this.Documentos[index].ArchivoPDF = this.BackupS3(this.Documentos[index].ArchivoPDF, this.Documentos[index].KeyName, this.Documentos[index].Id, this.Documentos[index].IdDocumento);

                if ((this.Documentos[index].ArchivoPDF.Updated)) {
                    if ((this.data.UpdateUrlPdf(this.Documentos[index].Id, this.Documentos[index].ArchivoPDF.URL)))
                        Console.WriteLine("URL Actualizada: " + this.Documentos[index].ArchivoPDF.URL);
                }
            }

            // ' acuse de cancelacion
            if ((this.Documentos[index].AcuseXML != null)) {
                this.Documentos[index].AcuseXML = this.BackupS3(this.Documentos[index].AcuseXML, this.Documentos[index].KeyName + "-acuse", this.Documentos[index].Id, this.Documentos[index].IdDocumento);

                if ((this.Documentos[index].AcuseXML.Completed)) {
                    if ((this.data.UpdateUrlXmlAcuse(this.Documentos[index].Id, this.Documentos[index].AcuseXML.URL)))
                        Console.WriteLine("URL Actualizada: " + this.Documentos[index].AcuseXML.URL);
                }
            }

            // ' representacion impresa del acuse de cancelacion
            if ((this.Documentos[index].AcusePDF != null)) {
                this.Documentos[index].AcusePDF = this.BackupS3(this.Documentos[index].AcusePDF, this.Documentos[index].KeyName + "-acuse", this.Documentos[index].Id, this.Documentos[index].IdDocumento);

                if ((this.Documentos[index].AcusePDF.Completed)) {
                    if ((this.data.UpdateUrlPdfAcuse(this.Documentos[index].Id, this.Documentos[index].AcusePDF.URL)))
                        Console.WriteLine("URL Actualizada: " + this.Documentos[index].AcusePDF.URL);
                }
            }
        }

        public void ExecuteVerifica() {
            BindingList<ViewModelComprobanteSingle> singles = this.data.GetListIn(this.Documentos.Select(it => it.IdDocumento).ToArray());
            for (int index = 0; index < this.Documentos.Count; index++) {
                ViewModelComprobanteSingle single = null;
                try {
                    single = singles.Where(it => it.IdDocumento == this.Documentos[index].IdDocumento).First();
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                    single = null;
                }

                if (single != null) {
                    if (single.IdDocumento == this.Documentos[index].IdDocumento) {
                        this.Documentos[index].Id = single.Id;
                        this.Documentos[index].Registrado = true;
                        if ((HelperValidacion.ValidaUrl(single.UrlXml)))
                            this.Documentos[index].ArchivoXML.URL = single.UrlXml;
                        if ((HelperValidacion.ValidaUrl(single.UrlPdf)))
                            this.Documentos[index].ArchivoPDF.URL = single.UrlPdf;
                    }
                }

                // ' buscar referencias de los documentos relacionados, lo buscamos en la base de datos
                if ((this.Documentos[index].CFDIRelacionado != null)) {
                    foreach (var r in this.Documentos[index].CFDIRelacionado.CfdiRelacionado) {
                        ViewModelComprobanteSingle info = this.data.GetSingle(r.IdDocumento);
                        if ((info != null)) {
                            r.Folio = info.Folio;
                            r.FechaEmision = info.FechaEmision;
                            r.Nombre = info.Emisor;
                            r.Serie = info.Serie;
                            r.RFC = info.EmisorRFC;
                            r.Total = info.Total;
                        } else {
                            r.Folio = "??";
                            r.Serie = "??";
                        }
                    }
                }

                // ' si tiene complemento de recepcion de pagos
                if ((this.Documentos[index].ComplementoPago != null)) {
                    foreach (var pagoRef in this.Documentos[index].ComplementoPago.Pago) {
                        var forma = this.formaPago.Search(pagoRef.FormaDePagoP.Clave);
                        if ((forma != null))
                            pagoRef.FormaDePagoP.Descripcion = forma.Descripcion;

                        if ((pagoRef.DoctoRelacionado != null)) {
                            foreach (var doctoRef in pagoRef.DoctoRelacionado) {
                                ViewModelComprobanteSingle info = this.data.GetSingle(doctoRef.IdDocumento);
                                if ((info != null)) {
                                    doctoRef.Folio = info.Folio;
                                    doctoRef.Serie = info.Serie;
                                    doctoRef.Nombre = info.Emisor;
                                    doctoRef.RFC = info.EmisorRFC;
                                    doctoRef.Estado = EnumEdoPagoDoctoRel.Relacionado;
                                } else
                                    doctoRef.Estado = EnumEdoPagoDoctoRel.NoEncontrado;
                            }
                        }
                    }
                }
            }
        }

        public async Task ExecuteBusqueda(string carpeta, bool incluirSubCarpetas) {
            this.reporteBusqueda = new Progress<Progreso>();
            this.reporteBusqueda.ProgressChanged += Reporte_Busqueda;
            await this.ExecuteSearch(carpeta, incluirSubCarpetas, this.reporteBusqueda);
        }

        public async Task ExecuteCopiarA(string carpeta) {
            this.reporteBusqueda = new Progress<Progreso>();
            this.reporteBusqueda.ProgressChanged += Reporte_Busqueda;
            await this.ExecuteCopyTo(carpeta, this.reporteBusqueda);
        }

        public async Task ExecuteLog(string archvo) {
            this.reporteBusqueda = new Progress<Progreso>();
            this.reporteBusqueda.ProgressChanged += Reporte_Busqueda;
            await this.ExecuteLogTo(archvo, this.reporteBusqueda);
        }

        public async void ExecuteValidacion() {
            this.reporteValidacion = new Progress<Progreso>();
            this.reporteValidacion.ProgressChanged += Reporte_Validacion;
            if ((this.Configuracion.ModoParalelo))
                await this.RunValidarParallelAsync(this.Documentos, this.reporteValidacion);
            else
                await this.RunValidarAsync(this.Documentos, this.reporteValidacion);
            ValidadorComprobantes.CatalogoCertificados.Save();
        }

        public void ExecuteValidacion(int index) {
            this.reporteValidacion = new Progress<Progreso>();
            this.reporteValidacion.ProgressChanged += Reporte_Validacion;
            this.Documentos[index] = this.RunValidar(this.Documentos[index], this.reporteValidacion);
        }

        public int SearchRFC(ViewModelContribuyente objeto) {
            //int index = this.dir.ReturnId(objeto.RFC);
            int index = this.dir.ReturnId(objeto.RFC);
            if ((index <= 0)) {
                objeto.Creo = "SYSDBA";
                objeto.Id = this.dir.Insert(objeto);
                return objeto.Id;
            } else
                return index;
        }

        public int SearchNominaReceptorRFC(ComplementoNominaReceptor objeto) {
            int index = this.dir.ReturnId(objeto.RFC);
            if ((index == 0)) {
                objeto.IdDirectorio = this.dir.Insert(new ViewModelContribuyente() { RFC = objeto.RFC, Nombre = objeto.Nombre });
                return objeto.IdDirectorio;
            } else
                return index;
        }

        public DocumentoAnexo BackupS3(DocumentoAnexo item, string keyname, int index, string folioFiscal) {
            var fullKeyName = string.Concat(keyname, item.FileExtension);
            // ' solo en el case del xml cambiamos el nombre
            if ((item.FileExtension.ToLower().Contains("xml")))
                fullKeyName = string.Concat(keyname, ".Xml");

            // ' comprobamos si la url es valida, si lo es comprobamos si es el archivo esta disponible
            if ((HelperValidacion.ValidaUrl(item.URL)))
                item.Completed = s3.Exists(item.URL);
            else
                item.Completed = false;

            // ' de no estar disponible lo agregamos
            if (item.Completed == false) {
                if (item.FileInfo.Exists) {
                    //var fileToUpload = new FileStream(item.FullFileName, FileMode.Open, FileAccess.Read);
                    //item.URL = this.s3.Upload(fileToUpload, fullKeyName, item.ContentFile, true, ConfigService.Synapsis.ProveedorAutorizado.Validation.Production);
                    //item.Completed = HelperValidacion.ValidaUrl(item.URL);
                    //item.Updated = item.Completed;

                    item.URL = this.s3.Upload(item.FullFileName, fullKeyName);
                    item.Completed = HelperValidacion.ValidaUrl(item.URL);
                    item.Updated = item.Completed;
                } else {
                    //item.FullFileName = JaegerManagerPaths.JaegerPath(Edita.Enums.EnumPaths.Temporal, item.FileName);
                    //byte[] numArray = Convert.FromBase64String(item.B64);
                    //MemoryStream stream = new MemoryStream(numArray);
                    //item.URL = this.s3.Upload(stream, fullKeyName, item.ContentFile, true, ConfigService.Synapsis.ProveedorAutorizado.Validation.Production);
                    item.URL = this.s3.Upload(item.B64, item.FullFileName, fullKeyName);
                    item.Completed = HelperValidacion.ValidaUrl(item.URL);
                    item.Updated = item.Completed;
                }
            }

            //if (item.Completed == false) {
            //    byte[] numArray = Convert.FromBase64String(item.B64);
            //    Stream stream = new MemoryStream(numArray);
            //    item.URL = s32.Upload(stream, fullKeyName);
            //}

            if (item.Completed && actulizarBucket == true) {
                // ' buscar el registro en el bucket
                var bItem = this.bucket.GetByKeyName(fullKeyName);
                if ((bItem == null)) {
                    if ((item.FileInfo.Exists)) {
                        ViewModelBucketContent item2 = new ViewModelBucketContent();
                        item2.BucketName = ConfigService.Synapsis.Amazon.S3.BucketName + "/CFDI";
                        item2.Completed = item.Completed;
                        item2.ContentType = item.ContentFile;
                        item2.FileExt = item.FileExtension;
                        item2.FileName = Path.GetFileName(item.FullFileName);
                        item2.KeyName = fullKeyName;
                        item2.SubId = index;
                        item2.Uuid = folioFiscal;
                        item2.Url = item.URL;

                        if ((this.bucket.Insert(item2) == 0))
                            Console.WriteLine("No se creo el registro (" + item2.FileName + ")");
                    }
                } else if (!(bItem.Url.Equals(item.URL))) {
                    bItem.Url = item.URL;
                    if ((this.bucket.Update(bItem) == 0))
                        Console.WriteLine("No se creo el registro (" + bItem.FileName + ")");
                }
            } else
                Console.WriteLine("Error");

            return item;
        }

        public bool ExecuteRegistra(int index) {
            if (this.Documentos[index].Id == 0) {
                this.Documentos[index].Id = data.ReturnId(this.Documentos[index].IdDocumento);

                if (this.Documentos[index].Id == 0) {
                    byte[] numArray = Convert.FromBase64String(this.Documentos[index].ArchivoXML.B64);
                    ViewModelComprobante comun = null/* TODO Change to default(_) if this is not a reference type */;

                    if (this.Documentos[index].Version == EnumCfdVersion.V32 & this.Documentos[index].MetaData == EnumMetadataType.CFDI) {
                    } else if (this.Documentos[index].Version == EnumCfdVersion.V33 & this.Documentos[index].MetaData == EnumMetadataType.CFDI) {
                        Jaeger.CFDI.V33.Comprobante o33 = Jaeger.CFDI.V33.Comprobante.LoadXml(UTF8Encoding.UTF8.GetString(numArray));
                        o33.Validation = this.Documentos[index].Validacion;
                        comun = convertidorExt.Create(o33);
                        comun.Xml = o33.OriginalXmlString;

                        if ((o33.Complemento != null)) {
                            if ((o33.Complemento.Nomina12 != null))
                                comun.SubTipo = EnumCfdiSubType.Nomina;
                        }
                    } else if (this.Documentos[index].Version == EnumCfdVersion.V40 & this.Documentos[index].MetaData == EnumMetadataType.CFDI) {
                        var o40 = Jaeger.CFDI.V40.Comprobante.LoadXml(UTF8Encoding.UTF8.GetString(numArray));
                        o40.Validation = this.Documentos[index].Validacion;
                        comun = convertidorExt40.Create(o40);
                        comun.Xml = o40.OriginalXmlString;

                        if ((o40.Complemento != null)) {
                            if ((o40.Complemento.Nomina12 != null))
                                comun.SubTipo = EnumCfdiSubType.Nomina;
                        }
                    }

                    if (!(comun == null)) {
                        if (comun.Emisor.RFC == ConfigService.Synapsis.Empresa.RFC.Trim().ToUpper()) {
                            if ((comun.SubTipo == EnumCfdiSubType.Nomina)) {
                                comun.SubTipo = EnumCfdiSubType.Nomina;
                                comun.Receptor.Relacion = Enum.GetName(typeof(EnumRelationType), EnumRelationType.Empleado);
                                comun.Receptor.Id = SearchRFC(comun.Receptor);
                                comun.Nomina.Receptor.IdDirectorio = comun.Receptor.Id;
                                comun.Nomina.Receptor.Id = SearchNominaReceptorRFC(comun.Nomina.Receptor);
                            } else {
                                comun.SubTipo = EnumCfdiSubType.Emitido;
                                comun.Receptor.Relacion = Enum.GetName(typeof(EnumRelationType), EnumRelationType.Cliente);
                                comun.Receptor.Id = SearchRFC(comun.Receptor);
                            }
                        } else if (comun.Receptor.RFC == ConfigService.Synapsis.Empresa.RFC.Trim().ToUpper()) {
                            comun.SubTipo = EnumCfdiSubType.Recibido;
                            comun.Emisor.Relacion = Enum.GetName(typeof(EnumRelationType), EnumRelationType.Proveedor);
                            comun.Emisor.Id = SearchRFC(comun.Emisor);
                        } else
                            comun.SubTipo = EnumCfdiSubType.None;

                        if (!(comun.CfdiRelacionados == null)) {
                            if (!(comun.CfdiRelacionados.TipoRelacion == null))
                                comun.CfdiRelacionados.TipoRelacion.Descripcion = this.Documentos[index].CFDIRelacionado.TipoRelacion.Descripcion;

                            if (!(comun.CfdiRelacionados.CfdiRelacionado == null)) {
                                comun.CfdiRelacionados.CfdiRelacionado = new BindingList<ComprobanteCfdiRelacionadosCfdiRelacionado>();
                                comun.CfdiRelacionados.CfdiRelacionado = this.Documentos[index].CFDIRelacionado.CfdiRelacionado;
                            }
                        }

                        if ((!(this.Documentos[index].Acuse == null)))
                            comun.Accuse = this.Documentos[index].Acuse;
                        comun.ComplementoPagos = this.Documentos[index].ComplementoPago;
                        comun.Status = "Importado";
                        comun.Creo = ConfigService.Piloto.Clave;
                        comun.Estado = this.Documentos[index].EstadoSAT;
                        comun.FechaEstado = DateTime.Now;
                        this.Documentos[index].Id = this.data.Save(comun).Id;
                        //if ((this.data.Message != null))
                        //    Console.WriteLine(string.Concat(this.data.Message, " ", this.data.Message.Data));
                    }
                } else
                    this.Documentos[index].Registrado = true;
            }

            return this.Documentos[index].Id > 0;
        }

        public void ExecuteRegistra() {
            this.backup = new System.ComponentModel.BackgroundWorker();
            this.backup.DoWork += this.Backup_DoWork;
            this.backup.RunWorkerCompleted += this.Backup_RunWorkerCompleted;
            this.backup.RunWorkerAsync();
        }

        private void Reporte_Busqueda(object sender, Progreso e) {
            this.OnSearchFiles(new ProcessChanged() { Caption = e.Caption });
        }

        private void Reporte_Validacion(object sender, Progreso e) {
            if ((e.Terminado)) {
                if ((this.AutoBackup)) {
                    this.OnBackupChanged(new ProcessChanged() { Caption = "Verificando ..." });
                    this.ExecuteRegistra();
                } else
                    this.OnValidacionCompleted(new ProcessCompleted() { Caption = e.Caption });
            } else
                this.OnValidacionChanged(new ProcessChanged() { Caption = e.Caption });
        }

        private void Backup_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e) {
            this.tiempo = new Stopwatch();
            this.tiempo.Start();
            int contador = 0;
            this.ExecuteVerifica();
            this.OnBackupChanged(new ProcessChanged() { Caption = "Registrando: " + this.Documentos.Count.ToString() });

            foreach (Jaeger.Edita.V2.Validador.Entities.Documento item in this.Documentos) {
                if (item.Resultado == EnumValidateResult.Valido) {
                    int index = this.Documentos.IndexOf(item);

                    if (this.ExecuteRegistra(index)) {
                        if (this.Documentos[index].Id > 0) {
                            this.OnBackupChanged(new ProcessChanged() { Caption = string.Format("Procesando: {0} de {1} | Tiempo transcurrido: {2:00}:{3:00}:{4:00}", contador.ToString(), this.Documentos.Count, this.tiempo.Elapsed.Hours, this.tiempo.Elapsed.Minutes, this.tiempo.Elapsed.Seconds) });
                            this.ExecuteBackUp(index);
                        }
                    }
                }
                contador = contador + 1;
            }
            this.tiempo.Stop();
        }

        private void Backup_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e) {
            ProcessCompleted completado = new ProcessCompleted();
            completado.Caption = string.Format("Backup terminado {0:00}:{1:00}:{2:00}", this.tiempo.Elapsed.Hours, this.tiempo.Elapsed.Minutes, this.tiempo.Elapsed.Seconds);
            this.OnBackupCompleted(completado);
        }
    }
}
