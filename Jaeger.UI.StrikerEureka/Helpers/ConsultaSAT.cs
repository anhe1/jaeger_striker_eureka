﻿using System;
using System.Threading.Tasks;
using Jaeger.SAT.Entities;
using Jaeger.SAT.CFDI.Consulta.Interfaces;
using Jaeger.SAT.CFDI.Consulta;
using Jaeger.SAT.CFDI.Consulta.Services;
using Jaeger.SAT.CFDI.API.Consulta;

namespace Jaeger.Helpers {
    public static class ConsultaSAT {
        public static SatQueryResult Consulta(string emisorRFC, string receptorRFC, decimal total, string idDocumento) {
            var service = new Status();
            //var response = service.GetStatus(emisorRFC, receptorRFC, total, idDocumento);
            IRequest request = Request.Create().WithEmisorRFC(emisorRFC).WithReceptorRFC(receptorRFC).WithTotal(total).WithFolioFiscal(idDocumento).Build();
            Acuse response = service.Execute(request);

            if (response != null) {
                var r = new SatQueryResult {
                    Clave = response.CodigoEstatus,
                    Fecha = DateTime.Now,
                    Status = response.Estado
                };
                if (!string.IsNullOrEmpty(response.ValidacionEFOS)) {
                    r.EFOS = response.ValidacionEFOS;
                }
                return r;
            }
            return null;
        }

        public static async Task<SatQueryResult> QueryAsync(string emisorRFC, string receptorRFC, decimal total, string uuid) {
            var service = new Status();
            Jaeger.SAT.Entities.SatQueryResult estado = null;
            await Task.Run(() => {
                IRequest request = Request.Create().WithEmisorRFC(emisorRFC).WithReceptorRFC(receptorRFC).WithTotal(total).WithFolioFiscal(uuid).Build();
                var r = service.Execute(request);
                var response = new SatQueryResult() {
                    Clave = r.CodigoEstatus,
                    Fecha = DateTime.Now,
                    Status = r.Estado,
                    Key = r.CodigoEstatus.ToString().Substring(0, 1),
                    EFOS = !string.IsNullOrEmpty(r.ValidacionEFOS) ? r.ValidacionEFOS : ""
                };
            });
            return estado;
        }
    }
}
