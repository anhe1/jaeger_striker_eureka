﻿// develop: 020220182249
// purpose: clase para realizar tarea de certificación de comprobantes fiscales cfdiv33
using System;
using Jaeger.Edita.V2;
using Jaeger.Certificacion;
using Jaeger.Services;
using Jaeger.CFDI.V33;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Interface;
using Jaeger.Edita.V2.CFDI.Entities.Cancel;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Helpers
{
    public class HelperCertificacion : Domain.Empresa.Entities.ProveedorAutorizado
    {
        private readonly HelperConvertidor convertidor;
        private readonly SqlSugarCertificado dbCert;
        private readonly CFDIExtension cfdiExtension;
        private IHelperCertificacion servicio;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="objeto">Configuración del proveedor autorizado de certificación</param>
        public HelperCertificacion(Domain.Empresa.Entities.Configuracion objeto)
        {
            this.Certificacion = objeto.ProveedorAutorizado.Certificacion;
            this.Cancelacion = objeto.ProveedorAutorizado.Cancelacion;
            this.Validacion = objeto.ProveedorAutorizado.Validacion;
            this.convertidor = new HelperConvertidor();
            this.cfdiExtension = new CFDIExtension();
            this.dbCert = new SqlSugarCertificado(objeto.RDS.Edita);
            this.Mensaje = "";
            this.Codigo = 0;
        }

        #region propiedades

        public string Mensaje { get; set; }

        public int Codigo { get; set; }

        #endregion

        #region metodos publicos

        public Comprobante Timbrar(CFDI.Entities.Comprobante cfdi)
        {
            if (cfdi != null)
            {
                Comprobante newItem = this.convertidor.Create(cfdi);
                if (newItem != null)
                {
                    return this.Timbrar(newItem);
                }
                else
                {
                    this.Codigo = 2;
                    this.Mensaje = "Error al generar el comprobante fiscal (XML)";
                }
            }
            else
            {
                this.Codigo = 1;
                this.Mensaje = "No se especifico un objeto valido.";
            }
            return null;
        }

        public Comprobante Timbrar(Edita.V2.CFDI.Entities.ViewModelComprobante cfdi)
        {
            if (cfdi != null)
            {
                Comprobante newItem = this.cfdiExtension.Create(cfdi);
                if (newItem != null)
                {
                    return this.Timbrar(newItem);
                }
                else
                {
                    this.Codigo = 2;
                    this.Mensaje = "Error al generar el comprobante fiscal (XML)";
                }
            }
            else
            {
                this.Codigo = 1;
                this.Mensaje = "No se especifico un objeto valido.";
            }
            return null;
        }

        public Comprobante Timbrar(Comprobante cfdi)
        {
            if (cfdi != null)
            {
                cfdi = this.Sellar(cfdi);

                if (cfdi != null)
                {
                    if (this.Certificacion.Provider == Domain.Empresa.Entities.ServiceProvider.EnumServicePAC.SolucionFactible)
                    {
                        this.servicio = new Certificacion.HelperSolucionFactible(this.Certificacion);
                    }
                    else if (this.Certificacion.Provider == Domain.Empresa.Entities.ServiceProvider.EnumServicePAC.FiscoClic)
                    {
                        this.servicio = new Certificacion.HelperFiscoClic(this.Certificacion);
                    }
                    else if (this.Certificacion.Provider == Domain.Empresa.Entities.ServiceProvider.EnumServicePAC.Factorum)
                    {
                        this.servicio = new Certificacion.HelperFactorum(this.Certificacion);
                        this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
                    }
                    else
                    {
                        this.Codigo = 2;
                        this.Mensaje = "Error al generar el comprobante fiscal (XML)";
                        cfdi = null;
                    }

                    cfdi = this.servicio.Timbrar(cfdi.Serialize());
                    this.Codigo = this.servicio.Codigo;
                    this.Mensaje = this.servicio.Mensaje;

                    // resultado
                    if (cfdi != null)
                    {
                        return cfdi;
                    }
                }
                else
                {
                    this.Codigo = 3;
                    this.Mensaje = "No se envío un objeto válido";
                }
            }
            return null;
        }

        public CancelaCFDResponse Cancelar(CFDI.Entities.Comprobante cfdi)
        {
            if (cfdi != null)
            {
                if (this.Cancelacion.Provider == Domain.Empresa.Entities.ServiceProvider.EnumServicePAC.SolucionFactible)
                {
                    // obtener información de certificado y llave
                    Jaeger.Edita.Entities.Basico.ViewModelCertificado info = this.InfoCertificados(Enums.EnumOrigenCertificado.BaseDeDatos);
                    this.servicio = new HelperSolucionFactible(this.Cancelacion);
                    this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
                    this.servicio.CerBase64 = info.CerB64;
                    this.servicio.KeyBase64 = info.KeyB64;
                    this.servicio.PassKey = info.Password;
                }
                else if (this.Cancelacion.Provider == Domain.Empresa.Entities.ServiceProvider.EnumServicePAC.FiscoClic)
                {
                    this.servicio = new Certificacion.HelperFiscoClic(this.Cancelacion);
                    this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
                }
                else if (this.Cancelacion.Provider == Domain.Empresa.Entities.ServiceProvider.EnumServicePAC.Factorum)
                {
                    this.servicio = new Certificacion.HelperFactorum(this.Cancelacion);
                    this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
                }
                else
                {
                    this.Codigo = 1;
                    this.Mensaje = "No se especifico Proveedor Autorizado de Certificación";
                    return null;
                }

                CancelaCFDResponse response = this.servicio.Cancelar(cfdi.TimbreFiscal.UUID);
                this.Codigo = this.servicio.Codigo;
                this.Mensaje = this.servicio.Mensaje;
                return response;
            }
            else
            {
                this.Codigo = 1;
                this.Mensaje = "No se envio un objeto válido";
            }
            return null;
        }

        public CancelaCFDResponse Cancelar(Edita.V2.CFDI.Entities.ViewModelComprobante cfdi)
        {
            if (cfdi != null)
            {
                if (this.Cancelacion.Provider == Domain.Empresa.Entities.ServiceProvider.EnumServicePAC.SolucionFactible)
                {
                    // obtener información de certificado y llave
                    Edita.Entities.Basico.ViewModelCertificado info = this.InfoCertificados(Enums.EnumOrigenCertificado.BaseDeDatos);
                    this.servicio = new HelperSolucionFactible(this.Cancelacion);
                    this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
                    this.servicio.CerBase64 = info.CerB64;
                    this.servicio.KeyBase64 = info.KeyB64;
                    this.servicio.PassKey = info.Password;
                }
                else if (this.Cancelacion.Provider == Domain.Empresa.Entities.ServiceProvider.EnumServicePAC.FiscoClic)
                {
                    this.servicio = new HelperFiscoClic(this.Cancelacion);
                    this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
                }
                else if (this.Cancelacion.Provider == Domain.Empresa.Entities.ServiceProvider.EnumServicePAC.Factorum)
                {
                    this.servicio = new HelperFactorum(this.Cancelacion);
                    this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
                }
                else
                {
                    this.Codigo = 1;
                    this.Mensaje = "No se especifico Proveedor Autorizado de Certificación";
                    return null;
                }

                CancelaCFDResponse response = this.servicio.Cancelar(cfdi.TimbreFiscal.UUID);
                this.Codigo = this.servicio.Codigo;
                this.Mensaje = this.servicio.Mensaje;
                return response;
            }
            else
            {
                this.Codigo = 1;
                this.Mensaje = "No se envio un objeto válido";
            }
            return null;
        }

        public ValidateResponse Validar(Comprobante cfdi)
        {

            return null;
        }

        public ValidateResponse Validar(CFDI.V32.Comprobante cfdi)
        {
            return null;
        }

        public static bool ValidaFechaEmisionMenor72H(DateTime fechaComprobante, int offset)
        {
            bool flag = true;
            try
            {
                DateTime currentPactDateTime = DateTime.Now;
                currentPactDateTime = currentPactDateTime.ToUniversalTime();
                DateTimeOffset dateTimeOffset = new DateTimeOffset(fechaComprobante, new TimeSpan(offset, 0, 0));
                fechaComprobante = dateTimeOffset.ToUniversalTime().DateTime;
                if (fechaComprobante > currentPactDateTime)
                {
                    flag = false;
                }
                else if (fechaComprobante < currentPactDateTime.AddHours(-72))
                {
                    flag = false;
                }
            }
            catch (Exception)
            {
                flag = true;
            }
            return flag;
        }

        #endregion

        #region metodos privados

        private Comprobante Sellar(Comprobante cfdi)
        {
            // obtener información de certificado y llave
            Jaeger.Edita.Entities.Basico.ViewModelCertificado info = this.InfoCertificados(Enums.EnumOrigenCertificado.BaseDeDatos);

            if (info != null)
            {
                CryptoCertificate cert = new CryptoCertificate();
                CryptoPrivateKey key = new CryptoPrivateKey();
                // quitamos etiquetas
                int startIndex = info.CerB64.IndexOf("-----BEGIN CERTIFICATE-----");
                int endIndex = info.CerB64.IndexOf("-----END CERTIFICATE-----");
                string cerB64 = info.CerB64.Substring(startIndex + 27, endIndex - startIndex - 27).Trim();
                // quitamos etiquetas
                string keyB64 = info.KeyB64;
                keyB64 = keyB64.Replace("-----BEGIN PRIVATE KEY-----", "");
                keyB64 = keyB64.Replace("-----END PRIVATE KEY-----", "");
                keyB64 = keyB64.Replace("-----BEGIN RSA PRIVATE KEY-----", "");
                keyB64 = keyB64.Replace("-----END RSA PRIVATE KEY-----", "");
                // carga de informacion de llave y certificado
                cert.CargarCertificadoDeB64(cerB64);
                key.CargarLlaveDeB64(keyB64, info.KeyPassB64);
                cert.CargarLlavePrivada(key);
                // agregar no. de serie del certificado y el certificado en base64
                cfdi.NoCertificado = cert.NoSerie;
                cfdi.Certificado = cert.ExportarB64();
                // sellar el comprobante
                cfdi.Sello = key.SellarCadenaSha256(cfdi.CadenaOriginal);
                if (key.CodigoDeError != 0)
                {
                    this.Codigo = key.CodigoDeError;
                    this.Mensaje = string.Concat("Sello de Comprobante: ", key.MensajeDeError);
                    return null;
                }
                return cfdi;
            }
            else
            {
                this.Codigo = 3;
                this.Mensaje = "No existe información válida para llave y certificado";
            }
            return null;
        }

        private Jaeger.Edita.Entities.Basico.ViewModelCertificado InfoCertificados(Enums.EnumOrigenCertificado origen)
        {
            if (origen == Enums.EnumOrigenCertificado.BaseDeDatos)
            {
                Edita.Entities.Basico.ViewModelCertificado info = dbCert.GetById(1);
                return info;
            }
            else if (origen == Enums.EnumOrigenCertificado.Archivo)
            {

            }
            return null;
        }
        #endregion
    }
}
