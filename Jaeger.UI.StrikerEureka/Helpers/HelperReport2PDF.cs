﻿/// develop: anhe 110120202319
/// purpose: convertir un reporte RDLC a formato PDF sin vista previa
using System.IO;
using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Reporting.WinForms;
using Jaeger.Edita.V2.Validador.Entities;
using Jaeger.Util.Helpers;

namespace Jaeger.Helpers
{
    /// <summary>
    /// clase para convertir un rdlc a formato PDF
    /// </summary>
    public class HelperReport2PDF
    {
        private EmbeddedResources horizon;

        public HelperReport2PDF()
        {
            horizon = new EmbeddedResources("jaeger_horizon_brave");
        }

        /// <summary>
        /// exportar reporte de validación a formato PDF
        /// </summary>
        public string Crear(ValidateResponse validateResponse, string folder = "")
        {
            if (validateResponse == null)
            {
                Console.WriteLine("HelperReportToPDF: la validación se encuentra vacía.");
                return "";
            }

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;
            string nombre = string.Format("CFDI-{0}-{1}-{2}-{3}_validacion.pdf", validateResponse.IssueRfcId, validateResponse.CustomerRfcId, validateResponse.Uuid, validateResponse.DateTime.ToString("yyyyMMddHHmmss"));
            string filename = "";

            if (folder != "")
                filename = Path.Combine(folder, nombre);
            else
                filename = JaegerManagerPaths.JaegerPath(Edita.Enums.EnumPaths.Temporal, nombre);

            using (var reportViewer = new ReportViewer())
            {
                List<ValidateResponse> lista = new List<ValidateResponse>() { validateResponse };
                reportViewer.LocalReport.DisplayName = "Reporte de Validación de Comprobantes";
                reportViewer.LocalReport.LoadReportDefinition(horizon.GetStream("Jaeger.Reportes.ReporteValidacion.rdlc"));
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("ValidationResponse", (IEnumerable)lista));
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("Validations", (IEnumerable)validateResponse.Validations));
                reportViewer.RefreshReport();

                byte[] bytes = reportViewer.LocalReport.Render(
                    "PDF", null, out mimeType, out encoding, out filenameExtension,
                     out streamids, out warnings);

                using (FileStream fs = new FileStream(filename, FileMode.Create))
                {
                    fs.Write(bytes, 0, bytes.Length);
                }
            }

            return filename;
        }
    }
}
