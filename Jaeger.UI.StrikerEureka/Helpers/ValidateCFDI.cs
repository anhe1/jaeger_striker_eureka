﻿using Jaeger.Edita.V2.CFDI.Entities;

namespace Jaeger.Helpers {
    public class ValidateCFDI {
        public ViewModelComprobante Comprobante { get; set; }
        public string Mensaje { get; set; }

        public void Validar() {
            if (this.Comprobante.ComplementoPagos != null) {
                if (this.Comprobante.ComplementoPagos.Pago[0].FechaPago == null)
                    this.Mensaje = "La fecha no es valida";

            }
        }
    }
}
