﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ComponentModel;
using MiniExcelLibs.OpenXml;
using MiniExcelLibs;
using Jaeger.Edita.V2.Validador.Entities;

namespace Jaeger.UI.Helpers {
    class MiniExcelTemplete {
        public bool ExportarExcel(string fileName, BindingList<Documento> documentos, string fileTemplete = "") {
            // configuracion para la exportacion
            var config = new OpenXmlConfiguration() {
                IgnoreTemplateParameterMissing = false,
            };

            var data = new Dictionary<string, object>() {
                ["Validador"] = documentos,
            };

            // si pasan un templete se carga
            if (!File.Exists(fileName)) {
                try {
                    MiniExcel.SaveAsByTemplate(fileName, fileTemplete, data, config);
                    return true;
                } catch (Exception ex) {
                    Console.WriteLine($"Could not save file {fileName} " + ex.Message);
                }
            }
            //MiniExcel.SaveAs(fileName, data, true, "validacion", ExcelType.XLSX, config);
            // probamos el templete por default
            //try {
            //    var templatePath = this.resource.GetAsBytes("Jaeger.Aplication.Validador.Beta.Reports.Validacion.xlsx");
            //    MiniExcel.SaveAsByTemplate(fileName, templatePath, data, config);
            //    return true;
            //} catch (Exception ex) {
            //    Console.WriteLine(ex.Message);
            //}

            return false;
        }
    }
}
