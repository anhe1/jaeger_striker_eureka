﻿using System;
using Jaeger.Aplication.Validador.Beta.Contracts;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public interface IValidacionService {
        string RFC { get; set; }

        IDocumentoFiscal Procesar(IDocumentoFiscal documentoFiscal);
    }
}
