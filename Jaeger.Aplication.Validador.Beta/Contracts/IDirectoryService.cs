﻿using System.IO;
using System.Threading.Tasks;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public interface IDirectoryService {
        void CreateDirectory(string directory);

        void CreateDirectoryConPermisos(string directory);

        void Delete(string directory);

        void Delete(string directory, bool recursive);

        Task DeleteAsync(string directory, bool recursive);

        void DeleteDirectory(string directory);

        bool Exists(string directory);

        string GetCurrentDirectory();

        string[] GetDirectories(string directory);

        string[] GetDirectories(string directory, string searchPattern);

        string[] GetDirectories(string directory, string searchPattern, SearchOption options);

        Task<string[]> GetDirectoriesAsync(string directory, string searchPattern, SearchOption options);

        string[] GetFiles(string directory);

        string[] GetFiles(string directory, string searchPattern);

        string[] GetFiles(string directory, string searchPattern, SearchOption options);

        Task<string[]> GetFilesAsync(string directory, string searchPattern, SearchOption options);

        DirectoryInfo GetParent(string directory);

        void Move(string directory, string destination);
    }
}
