﻿using System.Collections.Generic;
using Jaeger.Aplication.Validador.Beta.Domain;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public interface IValidadorComprobante {
        object Comprobante { get; set; }

        bool IsCodificacion { get; set; }

        bool IsEsquemaValido { get; set; }

        bool IsSelloCFDI { get; set; }

        bool IsSelloSAT { get; set; }

        bool IsVigente { get; set; }

        ComprobanteValidacionPropiedad Esquema();

        ComprobanteValidacionPropiedad Codificacion();

        ComprobanteValidacionPropiedad LugarExpedicion();

        ComprobanteValidacionPropiedad UsoCFDI();

        ComprobanteValidacionPropiedad FormaPago();
        
        ComprobanteValidacionPropiedad MetodoPago();

        List<ComprobanteValidacionPropiedad> SelloCFDI();
        
        ComprobanteValidacionPropiedad SelloSAT();

        Dictionary<string, string> GetEmisor();
    }
}
