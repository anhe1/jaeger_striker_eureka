﻿using Jaeger.Aplication.Validador.Beta.Domain;
using System.IO;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public interface ICFDReader {
        DocumentoFiscal GetDocumento { get; }

        DocumentoFiscal Reader(FileInfo fileInfo);
    }
}
