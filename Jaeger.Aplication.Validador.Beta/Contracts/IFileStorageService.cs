﻿namespace Jaeger.Aplication.Validador.Beta.Contracts {
    public interface IFileStorageService {
        bool MoveFile(string source, string destination);
    }
}
