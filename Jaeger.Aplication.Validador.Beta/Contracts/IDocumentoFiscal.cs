﻿using System;
using Jaeger.Aplication.Validador.Beta.Domain;

namespace Jaeger.Aplication.Validador.Beta.Contracts {
    public interface IDocumentoFiscal {
        /// <summary>
        /// obtener o establecer folio fiscal del comprobante (UUID)
        /// </summary>
        string IdDocumento { get; set; }

        string Version { get; set;}

        string TipoComprobante { get; set; }

        string SubTipo { get; set; }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        string Folio { get; set; }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        string Serie { get; set; }

        /// <summary>
        /// obtener o establecer el rfc del emisor del comprobante
        /// </summary>
        string EmisorRFC { get; set; }

        /// <summary>
        /// obtener o establecer el Nombre o Razón Social del Emisor
        /// </summary>
        string EmisorNombre { get; set; }

        string EmisorRegimenFiscal { get; set; }

        string ReceptorRFC { get; set; }

        string ReceptorNombre { get; set; }

        string ReceptorRegimenFiscal { get; set; }

        string ReceptorDomicilioFiscal { get; set; }

        string UsoCFDI { get; set; }

        string FormaPago { get; set; }

        string MetodoPago { get; set; }

        string Moneda { get; set; }

        string ClaveExportacion { get; set; }

        DateTime? FechaEmision { get; set; }

        /// <summary>
        /// obtener o establecer el lugar de expedicion de comprobante, en el caso de CFDI v33 es el codigo postal.
        /// </summary>
        string LugarExpedicion { get; set;}

        DateTime? FechaTimbre { get; set; }

        /// <summary>
        /// obtener o establacer el numero de certificado del emisor del comprobante
        /// </summary>
        string NoCertificado { get; set;}

        /// <summary>
        /// obtener o establecer el numero de certificado del proveedor de certificacion
        /// </summary>
        string NoCertificadoProvCertif { get; set; }

        /// <summary>
        /// obtener o establecer el RFC del proveedor de certificacion
        /// </summary>
        string RFCProvCertif { get; set; }

        #region impuestos
        /// <summary>
        /// obtener o establecer el valor total del impuesto retenido ISR
        /// </summary>
        decimal RetencionISR { get; set; }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA
        /// </summary>
        decimal RetencionIVA { get; set; }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS
        /// </summary>
        decimal RetencionIEPS { get; set; }

        /// <summary>
        /// importe del traslado de IVA
        /// </summary>
        decimal TrasladoIVA { get; set; }

        /// <summary>
        /// obtener o establecer el valor del impuesto traslado IEPS
        /// </summary>
        decimal TrasladoIEPS { get; set; }

        /// <summary>
        /// obtener o establecer el subTotal del comprobante
        /// </summary>
        decimal SubTotal { get; set; }

        /// <summary>
        /// monto del descuento aplicado
        /// </summary>
        decimal Descuento { get; set; }

        /// <summary>
        /// obtener o establecer el total de comprobante
        /// </summary>
        decimal Total { get; set; }
        #endregion

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        string Estado { get; set; }

        string Situacion { get; set; }

        string PathXML { get; set; }

        string PathPDF { get; set; }

        string XmlContentB64 { get; set; }

        System.Collections.Generic.List<DocumentoCFDIRelacionados> CFDIRelacionado { get; set; }

        System.Collections.Generic.List<DocumentoComplementoPago> Pagos { get; set; }

        System.Collections.Generic.List<DocumentoFiscalConcepto> Conceptos { get; set; }


        ComprobanteValidacion Validacion { get; set; }

        string KeyName { get; }

        string Resultado { get; set; }
    }
}
