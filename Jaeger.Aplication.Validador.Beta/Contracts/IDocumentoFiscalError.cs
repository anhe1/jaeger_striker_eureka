﻿namespace Jaeger.Aplication.Validador.Beta.Contracts {
    public interface IDocumentoFiscalError {
        string PathXML { get; set; }
        string Resultado { get; set; }
    }
}
