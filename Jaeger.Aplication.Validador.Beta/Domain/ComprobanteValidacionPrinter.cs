﻿using Jaeger.Aplication.Validador.Beta.Contracts;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Aplication.Validador.Beta.Domain {
    public class ComprobanteValidacionPrinter : ComprobanteValidacion, IComprobanteValidacionModel {
        public ComprobanteValidacionPrinter() : base() {
        }

        public ComprobanteValidacionPrinter(ComprobanteValidacion source) {
            MapperClassExtensions.MatchAndMap<ComprobanteValidacion, ComprobanteValidacionPrinter>(source, this);
            this.Resultados = source.Resultados;
        }

        public string KeyName() {
            return string.Format("CFDI-{0}-{1}-{2}-{3}", this.EmisorRFC, this.ReceptorRFC, this.IdDocumento, this.FechaEmision.ToString("yyyyMMddHHmmss"));
        }

        public object Tag { get; set; }
    }
}
