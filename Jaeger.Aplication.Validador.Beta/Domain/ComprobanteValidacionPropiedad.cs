﻿using Newtonsoft.Json;
namespace Jaeger.Aplication.Validador.Beta.Domain {
    [JsonObject]
    public class ComprobanteValidacionPropiedad {
        public ComprobanteValidacionPropiedad() {
        }

        public ComprobanteValidacionPropiedad(PropiedadTipoEnum tipo, string codigo, string nombre, string valor, bool isValido = true) {
            Tipo = tipo;
            Codigo = codigo;
            Nombre = nombre;
            Valor = valor;
            IsValido = isValido;
        }

        /// <summary>
        /// obtener o establecer el tipo de propiedad
        /// </summary>
        [JsonProperty("type")]
        public PropiedadTipoEnum Tipo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el codigo de la validacion
        /// </summary>
        [JsonProperty("code")]
        public string Codigo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el nombre de la propieddad
        /// </summary>
        [JsonProperty("name")]
        public string Nombre {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el valor de la propieddad
        /// </summary>
        [JsonProperty("value")]
        public string Valor {
            get; set;
        }

        /// <summary>
        /// obtener o establecer si la propiedad asociada es valida
        /// </summary>
        [JsonProperty("valid")]
        public bool IsValido {
            get; set;
        }
    }
}
