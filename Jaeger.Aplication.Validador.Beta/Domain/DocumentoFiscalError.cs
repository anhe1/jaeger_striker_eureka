﻿using Jaeger.Aplication.Validador.Beta.Contracts;

namespace Jaeger.Aplication.Validador.Beta.Domain {
    public class DocumentoFiscalError : IDocumentoFiscalError {
        public DocumentoFiscalError() { }

        public DocumentoFiscalError(string pathXML, string resultado) {
            PathXML = pathXML;
            Resultado = resultado;
        }

        public string PathXML { get; set; }
        public string Resultado { get; set; }
    }
}
