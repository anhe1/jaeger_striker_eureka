﻿namespace Jaeger.Aplication.Validador.Beta.Domain {
    public class DocumentoFiscalConcepto {

        public DocumentoFiscalConcepto() {
        }

        public decimal Cantidad { get; set; }

        public string ClaveProdServ { get; set; }

        public string ClaveUnidad { get; set; }

        /// <summary>
        /// obtener o establecer si la operación comercial es objeto o no de impuesto.
        /// </summary>
        public string ObjetoImp { get; set; }

        public string Descripcion { get; set; } 

        public decimal Descuento { get; set; }

        public decimal Importe { get; set; }

        public string ImpuestosRetenciones { get; set; }

        public string ImpuestosTraslados { get; set; }

        public string InfoAduNumeroPedimento { get; set; }

        public string NoIdentificacion { get; set; }

        public string NumeroCuentaPredial { get; set; }

        public string TipoCambio { get; set; }

        public string Unidad { get; set; }

        public decimal ValorUnitario { get; set; }
    }
}
