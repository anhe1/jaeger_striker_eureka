﻿namespace Jaeger.Aplication.Validador.Beta.Domain {
    public class DocumentoComplementoPagoCFDIRelacionado {
        /// <summary>
        /// obtener o establecer identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electronica o bien 
        /// el numero de operacion de un documento digital.
        /// </summary>
        public string IdDocumento { get; set; }

        /// <summary>
        /// obtener o establecer serie del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        public string Serie { get; set; }

        /// <summary>
        /// obtener o establecer folio del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        public string Folio { get; set; }

        /// <summary>
        /// obtener o establecer RFC del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        public string RFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        public string Nombre { get; set; }

        public System.DateTime FechaEmision { get; set; }

        /// <summary>
        /// obtener o establecer la clave de la forma en que se realiza el pago.
        /// </summary>
        public string FormaDePagoP { get; set; }

        /// <summary>
        /// Atributo requerido para expresar la clave del método de pago que se registró en el documento relacionado
        /// </summary>
        public string MetodoPago { get; set; }

        /// <summary>
        /// obtener o establecer clave de la moneda utilizada en los importes del documento relacionado, cuando se usa moneda nacional o el documento 
        /// relacionado no especifica la moneda se registra MXN. Los importes registrados en los atributos “ImpSaldoAnt”, “ImpPagado” e “ImpSaldoInsoluto”
        /// de éste nodo, deben corresponder a esta moneda. Conforme con la especificación ISO 4217.
        /// </summary>
        public string MonedaDR { get; set; }

        /// <summary>
        /// obtener o establecer tipo de cambio conforme con la moneda registrada en el documento relacionado. Es requerido cuando la moneda del documento 
        /// relacionado es distinta de la moneda de pago. Se debe registrar el número de unidades de la moneda señalada en el documento relacionado que 
        /// equivalen a una unidad de la moneda del pago. Por ejemplo: El documento relacionado se registra en USD. El pago se realiza por 100 EUR. Este 
        /// atributo se registra como 1.114700 USD/EUR. El importe pagado equivale a 100 EUR * 1.114700 USD/EUR = 111.47 USD.
        /// </summary>
        public decimal EquivalenciaDR { get; set; }

        /// <summary>
        /// obtener o establecer el numero de parcialidad que corresponde al pago.
        /// </summary>
        public int NumParcialidad { get; set; }

        /// <summary>
        /// obtener o establecer monto del saldo insoluto de la parcialidad anterior. En el caso de que sea la primer parcialidad este atributo debe 
        /// contener el importe total del documento relacionado.
        /// </summary>
        public decimal ImpSaldoAnt { get; set; }

        /// <summary>
        /// obtener o establecer importe pagado para el documento relacionado.
        /// </summary>
        public decimal ImpPagado { get; set; }

        /// <summary>
        /// obtener o establecer diferencia entre el importe del saldo anterior y el monto del pago.
        /// </summary>
        public decimal ImpSaldoInsoluto { get; set; }

        /// <summary>
        /// obtener o establecer el pago del documento relacionado es objeto o no de impuesto.
        /// </summary>
        public string ObjetoImpDR { get; set; }
    }
}
