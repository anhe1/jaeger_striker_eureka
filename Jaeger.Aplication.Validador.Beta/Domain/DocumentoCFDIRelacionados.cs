﻿namespace Jaeger.Aplication.Validador.Beta.Domain {
    /// <summary>
    /// Nodo opcional para precisar la información de los comprobantes relacionados.<
    /// </summary>
    public class DocumentoCFDIRelacionados {
        public DocumentoCFDIRelacionados() {
            this.CFDIRelacionados = new System.Collections.Generic.List<DocumentoCFDIRelacionado>();
        }

        public DocumentoCFDIRelacionados(string tipoRelacion) {
            this.TipoRelacion = tipoRelacion;
            this.CFDIRelacionados = new System.Collections.Generic.List<DocumentoCFDIRelacionado>();
        }

        /// <summary>
        /// Atributo requerido para indicar la clave de la relación que existe entre éste que se está generando y el o los CFDI previos.
        /// </summary>
        public string TipoRelacion { get; set; }

        public System.Collections.Generic.List<DocumentoCFDIRelacionado> CFDIRelacionados { get; set; }
    }
}
