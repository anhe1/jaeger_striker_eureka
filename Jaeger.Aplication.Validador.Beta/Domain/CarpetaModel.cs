﻿using Newtonsoft.Json;

namespace Jaeger.Aplication.Validador.Beta.Domain {
    [JsonObject("item")]
    public partial class CarpetaModel : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation {
        private string carpetaField;

        public CarpetaModel() {
        }

        public CarpetaModel(string carpeta) {
            this.Carpeta = carpeta;
        }

        [JsonProperty("path")]
        public string Carpeta {
            get {
                return this.carpetaField;
            }
            set {
                this.carpetaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public string Accion {
            get {
                return "Quitar";
            }
        }
    }
}