﻿using Newtonsoft.Json;
using System;

namespace Jaeger.Aplication.Validador.Beta.Domain {
    public class DocumentoComplementoPago {

        public DocumentoComplementoPago() {
            this.Relacionados = new System.Collections.Generic.List<DocumentoComplementoPagoCFDIRelacionado>();
        }

        /// <summary>
        /// obtener o establecer la version del complemento para recepcion de pagos.
        /// </summary>
        [JsonProperty()]
        public string Version { get; set; }

        /// <summary>
        /// obtener o establecer la fecha y hora en la que el beneficiario recibe el pago. Se expresa en la forma aaaa-mm-ddThh:mm:ss, de acuerdo con la 
        /// especificación ISO 8601.En caso de no contar con la hora se debe registrar 12:00:00.
        /// </summary>
        [JsonProperty()]
        public DateTime FechaPagoP { get; set; }

        /// <summary>
        /// obtener o establecer la clave de la forma en que se realiza el pago.
        /// </summary>
        [JsonProperty()]
        public string FormaDePagoP { get; set; }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para realizar el pago conforme a la especificación ISO 4217. Cuando se usa moneda nacional se registra MXN. 
        /// El atributo Pagos:Pago:Monto debe ser expresado en la moneda registrada en este atributo.
        /// </summary>
        [JsonProperty()]
        public string MonedaP { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de cambio de la moneda a la fecha en que se realizó el pago. El valor debe reflejar el número de pesos mexicanos que equivalen a una 
        /// unidad de la divisa señalada en el atributo MonedaP. Es requerido cuando el atributo MonedaP es diferente a MXN.
        /// </summary>
        [JsonProperty()]
        public decimal TipoCambioP { get; set; }

        /// <summary>
        /// obtener o establecer el importe del pago.
        /// </summary>
        [JsonProperty()]
        public decimal MontoP { get; set; }

        /// <summary>
        /// obtener o establecer numero de cheque, numero de autorización, numero de referencia, clave de rastreo en caso de ser SPEI, línea de captura o algún número de referencia 
        /// análogo que identifique la operación que ampara el pago efectuado.
        /// </summary>
        [JsonProperty()]
        public string NumOperacion { get; set; }

        /// <summary>
        /// obtener o establecer clave RFC de la entidad emisora de la cuenta origen, es decir, la operadora, el banco, la institución financiera, emisor de monedero electrónico, etc.,
        /// en caso de ser extranjero colocar XEXX010101000, considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [JsonProperty()]
        public string RfcEmisorCtaOrd { get; set; }

        /// <summary>
        /// obtener o establecer nombre del banco ordenante, es requerido en caso de ser extranjero. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de 
        /// acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [JsonProperty()]
        public string NomBancoOrdExt { get; set; }

        /// <summary>
        /// obtener o establecer el numero de la cuenta con la que se realizo el pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con 
        /// el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [JsonProperty()]
        public string CtaOrdenante { get; set; }

        /// <summary>
        /// obtener o establecer clave RFC de la entidad operadora de la cuenta destino, es decir, la operadora, el banco, la institución financiera, emisor de monedero electrónico, etc. 
        /// Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [JsonProperty()]
        public string RfcEmisorCtaBen { get; set; }

        /// <summary>
        /// obtener o establecer clave del tipo de cadena de pago que genera la entidad receptora del pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para 
        /// éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [JsonProperty()]
        public string TipoCadPago { get; set; }

        /// <summary>
        /// obtener o establecer el número de cuenta en donde se recibió el pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo 
        /// con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [JsonProperty()]
        public string CtaBeneficiario { get; set; }

        /// <summary>
        /// obtener o establecer la cadena original del comprobante de pago generado por la entidad emisora de la cuenta beneficiaria. Es requerido en caso de que el atributo TipoCadPago 
        /// contenga información.
        /// </summary>
        [JsonProperty()]
        public string CadPago { get; set; }

        /// <summary>
        /// obtener o establecer certificado que ampara al pago, como una cadena de texto en formato base 64. Es requerido en caso de que el atributo TipoCadPago contenga información.
        /// </summary>
        [JsonProperty()]
        public string CertPago { get; set; }

        /// <summary>
        /// obtener o establecer el sello digital que se asocie al pago. La entidad que emite el comprobante de pago, ingresa una cadena original y el sello digital en una sección de 
        /// dicho comprobante, este sello digital es el que se debe registrar en este atributo. Debe ser expresado como una cadena de texto en formato base 64. Es requerido en caso 
        /// de que el atributo TipoCadPago contenga información.
        /// </summary>
        [JsonProperty()]
        public string SelloPago { get; set; }

        /// <summary>
        /// Comprobantes relacionados al pago
        /// </summary>
        [JsonProperty()]
        public System.Collections.Generic.List<DocumentoComplementoPagoCFDIRelacionado> Relacionados { get; set; }
    }
}
