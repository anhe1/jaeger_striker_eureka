﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;
using Jaeger.Aplication.Validador.Beta.Contracts;
using MiniExcelLibs.Attributes;

namespace Jaeger.Aplication.Validador.Beta.Domain {
    public class DocumentoFiscal : IDocumentoFiscal {
        #region declaraciones
        private string _IdDocumento;
        private DateTime? _FechaEmision;
        private DateTime? _FechaTimbre;
        private string _Folio;
        private string _Serie;
        private string _TipoComprobante;
        private string _EmisorRFC;
        private string _EmisorNombre;
        private string _EmisorRegimen;
        private string _ReceptorRFC;
        private string _ReceptorNombre;
        private string _ReceptorRegimenFiscal;
        private string _ClaveFormaPago;
        private string _ClaveMetodoPago;
        private string _ClaveUsoCFDI;
        private string _ClaveMoneda;
        private decimal _TotalRetencionISR;
        private decimal _TotalRetencionIVA;
        private decimal _TotalRetencionIEPS;
        private decimal _TotalTrasladoIVA;
        private decimal _TotalTrasladoIEPS;
        private decimal _SubTotal;
        private decimal _Descuento;
        private decimal _Total;
        private string _ClaveExportacion;
        private string _LugarExpedicion;
        private string _NoCertificadoEmisor;
        private string _NoCertificadoProvCertif;
        private string _RFCProvCertif;
        private string _Version;
        private string _EstadoSAT;
        private string _ReceptorDomicilioFiscal;
        private ComprobanteValidacion _Validacion;
        private string _Tipo;
        #endregion

        public DocumentoFiscal() {
            this.Resultado = "Pendiente";
            this._Tipo = "Recibido";
            this.Pagos = new System.Collections.Generic.List<DocumentoComplementoPago>();
        }

        /// <summary>
        /// obtener o establecer folio fiscal del comprobante (UUID)
        /// </summary>
        public string IdDocumento {
            get { return _IdDocumento; }
            set {
                if (!string.IsNullOrEmpty(value))
                    this._IdDocumento = value.ToUpper();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo del comprobante (Emitido o Recibido)
        /// </summary>
        public string Tipo {
            get {
                return this._Tipo;
            }
            set {
                this._Tipo = value;
            }
        }

        /// <summary>
        /// obtener o establecer version del estandar bajo el que se encuentra expresado el comprobante
        /// </summary>
        public string Version {
            get { return this._Version; }
            set { this._Version = value; }
        }

        /// <summary>
        /// obtener o establecer tipo o efecto del comprobante
        /// </summary>
        public string TipoComprobante {
            get { return this._TipoComprobante; }
            set { this._TipoComprobante = value; }
        }

        public string SubTipo { get; set; }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        public string Folio {
            get { return this._Folio; }
            set { this._Folio = value; }
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        public string Serie {
            get { return this._Serie; }
            set { this._Serie = value; }
        }

        /// <summary>
        /// obtener o establecer el rfc del emisor del comprobante
        /// </summary>
        public string EmisorRFC {
            get { return _EmisorRFC; }
            set { _EmisorRFC = value; }
        }

        /// <summary>
        /// obtener o establecer el Nombre o Razón Social del Emisor
        /// </summary>
        public string EmisorNombre {
            get { return _EmisorNombre; }
            set { this._EmisorNombre = value; }
        }

        public string EmisorRegimenFiscal {
            get { return this._EmisorRegimen; }
            set { this._EmisorRegimen = value; }
        }

        /// <summary>
        /// obtener o establacer el RFC del receptor del comprobante
        /// </summary>
        public string ReceptorRFC {
            get { return _ReceptorRFC; }
            set { this._ReceptorRFC = value; }
        }

        /// <summary>
        /// obtener o establecer el Nombre o Razón Social del Receptor
        /// </summary>
        public string ReceptorNombre {
            get { return _ReceptorNombre; }
            set { _ReceptorNombre = value; }
        }

        /// <summary>
        /// obtener o establecer la clave del régimen fiscal del contribuyente receptor al que aplicará el efecto fiscal de este comprobante
        /// </summary>
        public string ReceptorRegimenFiscal {
            get { return this._ReceptorRegimenFiscal; }
            set { this._ReceptorRegimenFiscal = value; }
        }

        /// <summary>
        /// obtener o establecer el código postal del domicilio fiscal del receptor del comprobante
        /// </summary>
        public string ReceptorDomicilioFiscal {
            get { return this._ReceptorDomicilioFiscal; }
            set { this._ReceptorDomicilioFiscal = value; }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.
        /// </summary>
        public string UsoCFDI {
            get { return this._ClaveUsoCFDI; }
            set { this._ClaveUsoCFDI = value; }
        }

        /// <summary>
        /// obtener o establecer obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, 
        /// Si no se conoce la forma de pago este atributo se debe omitir.
        /// </summary>
        public string FormaPago {
            get { return this._ClaveFormaPago; }
            set { this._ClaveFormaPago = value; }
        }

        /// <summary>
        /// obtener o establecer atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al 
        /// Articulo 29-A fracción VII incisos a y b del CFF.
        /// </summary>
        public string MetodoPago {
            get { return this._ClaveMetodoPago; }
            set { this._ClaveMetodoPago = value; }
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        public string Moneda {
            get { return this._ClaveMoneda; }
            set { this._ClaveMoneda = value; }
        }

        /// <summary>
        /// obtener o establecer si el comprobante ampara una operación de exportación.
        /// </summary>
        public string ClaveExportacion {
            get { return this._ClaveExportacion; }
            set { this._ClaveExportacion = value; }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// </summary>
        [ExcelFormat("dd/MM/yyyy")]
        public DateTime? FechaEmision {
            get { return _FechaEmision; }
            set {
                this._FechaEmision = value;
            }
        }

        /// <summary>
        /// obtener o establecer el lugar de expedicion de comprobante, en el caso de CFDI v33 es el codigo postal.
        /// </summary>
        public string LugarExpedicion {
            get {
                return this._LugarExpedicion;
            }
            set {
                this._LugarExpedicion = value;

            }
        }

        /// <summary>
        /// fecha de certificación
        /// </summary>
        [ExcelFormat("dd/MM/yyyy")]
        public DateTime? FechaTimbre {
            get { return this._FechaTimbre; }
            set { this._FechaTimbre = value; }
        }

        /// <summary>
        /// obtener o establacer el numero de certificado del emisor del comprobante
        /// </summary>
        public string NoCertificado {
            get {
                return this._NoCertificadoEmisor;
            }
            set {
                this._NoCertificadoEmisor = value;

            }
        }

        /// <summary>
        /// obtener o establecer el numero de certificado del proveedor de certificacion
        /// </summary>
        public string NoCertificadoProvCertif {
            get {
                return this._NoCertificadoProvCertif;
            }
            set {
                this._NoCertificadoProvCertif = value;

            }
        }

        /// <summary>
        /// obtener o establecer el RFC del proveedor de certificacion
        /// </summary>
        public string RFCProvCertif {
            get {
                return this._RFCProvCertif;
            }
            set {
                this._RFCProvCertif = value;

            }
        }

        #region impuestos
        /// <summary>
        /// obtener o establecer el valor total del impuesto retenido ISR
        /// </summary>
        public decimal RetencionISR {
            get {
                return this._TotalRetencionISR;
            }
            set {
                this._TotalRetencionISR = value;
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA
        /// </summary>
        public decimal RetencionIVA {
            get {
                return this._TotalRetencionIVA;
            }
            set {
                this._TotalRetencionIVA = value;
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS
        /// </summary>
        public decimal RetencionIEPS {
            get {
                return this._TotalRetencionIEPS;
            }
            set {
                this._TotalRetencionIEPS = value;
            }
        }

        /// <summary>
        /// importe del traslado de IVA
        /// </summary>
        public decimal TrasladoIVA {
            get {
                return this._TotalTrasladoIVA;
            }
            set {
                this._TotalTrasladoIVA = value;
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto traslado IEPS
        /// </summary>
        public decimal TrasladoIEPS {
            get {
                return this._TotalTrasladoIEPS;
            }
            set {
                this._TotalTrasladoIEPS = value;
            }
        }

        /// <summary>
        /// obtener o establecer el subTotal del comprobante
        /// </summary>
        public decimal SubTotal {
            get {
                return this._SubTotal;
            }
            set {
                this._SubTotal = value;
            }
        }

        /// <summary>
        /// monto del descuento aplicado
        /// </summary>
        public decimal Descuento {
            get {
                return this._Descuento;
            }
            set {
                this._Descuento = value;

            }
        }

        /// <summary>
        /// obtener o establecer el total de comprobante
        /// </summary>
        public decimal Total {
            get {
                return this._Total;
            }
            set {
                this._Total = value;

            }
        }
        #endregion

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        public string Estado {
            get {
                return this._EstadoSAT;
            }
            set {
                this._EstadoSAT = value;

            }
        }

        public string Situacion { get; set; }

        public string PathXML { get; set; }

        public string PathPDF { get; set; }

        public string XmlContentB64 { get; set; }

        public System.Collections.Generic.List<DocumentoCFDIRelacionados> CFDIRelacionado { get; set; }

        public System.Collections.Generic.List<DocumentoComplementoPago> Pagos { get; set; }

        public System.Collections.Generic.List<DocumentoFiscalConcepto> Conceptos { get; set; }

        public ComprobanteValidacion Validacion {
            get { return this._Validacion; }
            set { this._Validacion = value; }
        }

        public string MetodoVsForma {
            get {
                if (this.Version == "3.3" | this.Version == "4.0") {
                    if (this.TipoComprobante != "Pagos" && this.TipoComprobante != "Nomina" && this.TipoComprobante != "Egreso") {
                        if (this.MetodoPago == "PUE" && this.FormaPago != "99") {
                            return "Correcto";
                        } else if (this.MetodoPago == "PPD" && this.FormaPago == "99") {
                            return "Correcto";
                        }
                        return "No coincide";
                    }
                }
                return "N/A";
            }
        }

        public string KeyName {
            get {
                string[] emisorRfc;
                if (this.FechaTimbre != null) {
                    emisorRfc = new string[] { "CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.IdDocumento, "-", this.FechaTimbre.Value.ToString("yyyyMMddHHmmss") };
                } else if (this.FechaEmision != null) {
                    emisorRfc = new string[] { "CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.IdDocumento, "-", this.FechaEmision.Value.ToString("yyyyMMddHHmmss") };
                } else {
                    emisorRfc = new string[] { "CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.IdDocumento, "-00000000000000" };
                }
                return string.Concat(emisorRfc);
            }
        }

        public string Resultado { get; set; }
    }
}
