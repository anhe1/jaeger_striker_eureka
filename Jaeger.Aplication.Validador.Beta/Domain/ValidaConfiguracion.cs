﻿using Newtonsoft.Json;

namespace Jaeger.Aplication.Validador.Beta.Domain {
    [JsonObject]
    public class ValidaConfiguracion : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private bool _ValidarXSD;
        private bool _ValidarClaveConcepto;
        private bool _ValidarClaveUsoCFDI;
        private bool _ValidarClaveFormaPago;
        private bool _ValidarClaveMetodoPago;
        private bool _RequiereFormaYMetodoPago;
        private bool _VerificarEstadoSAT;
        private bool _ValidarAddendaSin;
        private bool _ValidarSelloCFDI;
        private bool _ValidarSelloSAT;
        private bool _ValidaAritmetica;
        private bool _ObtenerEmisor;
        private bool _ModoParalelo;
        private bool _AsociarPDF;
        private bool _RenombrarOrigen;
        private bool _RegistrarConceptos;
        private bool _DescargaCertificado;
        private bool _VerificarCodificacionUTF8;
        private bool _ValidarArticulo69B;
        private string _LugarExpedicion32;
        private bool _VerificarEmisorCP;
        private bool _RegistrarComprobante;
        private bool _LugarExpedicion32B;
        private bool _SoloCFDIValido;
        #endregion

        public ValidaConfiguracion() {
            this.ValidarXSD = true;
            this.ModoParalelo = true;
            this.AsociarPDF = true;
            this.ValidarSelloCFDI = true;
            this.ValidarSelloSAT = true;
            this.DescargaCertificado = true;
            this.RegistrarComprobante = true;
            this.VerificarCodificacionUTF8 = true;
            this.LugarExpedicion32B = false;
        }

        [JsonProperty("ValidarXSD")]
        public bool ValidarXSD {
            get { return this._ValidarXSD; }
            set {
                this._ValidarXSD = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("validarClaveConcepto")]
        public bool ValidarClaveConcepto {
            get { return this._ValidarClaveConcepto; }
            set {
                this._ValidarClaveConcepto = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("validarClaveUsoCFDI")]
        public bool ValidarClaveUsoCFDI {
            get { return this._ValidarClaveUsoCFDI; }
            set {
                this._ValidarClaveUsoCFDI = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("validarClaveFormaPago")]
        public bool ValidarClaveFormaPago {
            get { return this._ValidarClaveFormaPago; }
            set {
                this._ValidarClaveFormaPago = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("validarClaveMetodoPago")]
        public bool ValidarClaveMetodoPago {
            get { return this._ValidarClaveMetodoPago; }
            set {
                this._ValidarClaveMetodoPago = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("requiereFormaYMetodoPago")]
        public bool RequiereFormaYMetodoPago {
            get { return this._RequiereFormaYMetodoPago; }
            set {
                this._RequiereFormaYMetodoPago = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("verificarEstadoSAT")]
        public bool VerificarEstadoSAT {
            get { return this._VerificarEstadoSAT; }
            set {
                this._VerificarEstadoSAT = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("validarAddendaSin")]
        public bool ValidarAddendaSin {
            get { return this._ValidarAddendaSin; }
            set {
                this._ValidarAddendaSin = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("validarSelloCFDI")]
        public bool ValidarSelloCFDI {
            get { return this._ValidarSelloCFDI; }
            set {
                this._ValidarSelloCFDI = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("validarSelloSAT")]
        public bool ValidarSelloSAT {
            get { return this._ValidarSelloSAT; }
            set {
                this._ValidarSelloSAT = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("validaAritmetica")]
        public bool ValidaAritmetica {
            get { return this._ValidaAritmetica; }
            set {
                this._ValidaAritmetica = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("obtenerEmisor")]
        public bool ObtenerEmisor {
            get { return this._ObtenerEmisor; }
            set {
                this._ObtenerEmisor = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("modoParalelo")]
        public bool ModoParalelo {
            get { return this._ModoParalelo; }
            set {
                this._ModoParalelo = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("asociarPDF")]
        public bool AsociarPDF {
            get { return this._AsociarPDF; }
            set {
                this._AsociarPDF = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("renombrarOrigen")]
        public bool RenombrarOrigen {
            get { return this._RenombrarOrigen; }
            set {
                this._RenombrarOrigen = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("registrarComprobante")]
        public bool RegistrarComprobante {
            get { return this._RegistrarComprobante; }
            set {
                this._RegistrarComprobante = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("soloCFDIValido")]
        public bool SoloCFDIValido {
            get { return this._SoloCFDIValido; }
            set {
                this._SoloCFDIValido = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("registrarConceptos")]
        public bool RegistrarConceptos {
            get { return this._RegistrarConceptos; }
            set {
                this._RegistrarConceptos = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("descargaCertificado")]
        public bool DescargaCertificado {
            get { return this._DescargaCertificado; }
            set {
                this._DescargaCertificado = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("verificarCodificacionUTF8")]
        public bool VerificarCodificacionUTF8 {
            get { return this._VerificarCodificacionUTF8; }
            set {
                this._VerificarCodificacionUTF8 = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("validarArticulo69B")]
        public bool ValidarArticulo69B {
            get { return this._ValidarArticulo69B; }
            set {
                this._ValidarArticulo69B = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("verificarEmisorCP")]
        public bool VerificarEmisorCP {
            get { return this._VerificarEmisorCP; }
            set {
                this._VerificarEmisorCP = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("lugarExpedicion32B")]
        public bool LugarExpedicion32B {
            get { return this._LugarExpedicion32B; }
            set {
                this._LugarExpedicion32B = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("lugarExpedicion32")]
        public string LugarExpedicion32 {
            get { return this._LugarExpedicion32; }
            set {
                this._LugarExpedicion32 = value;
                this.OnPropertyChanged();
            }
        }

        public string Json() {
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.SerializeObject(this, Formatting.None, conf);
        }

        public static ValidaConfiguracion Json(string input) {
            return JsonConvert.DeserializeObject<ValidaConfiguracion>(input);
        }
    }
}
