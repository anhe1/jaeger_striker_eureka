﻿using System;
using Newtonsoft.Json;

namespace Jaeger.Aplication.Validador.Beta.Domain {
    [JsonObject("proxy")]
    public class ConfigurationProxy {
        private bool enabled;
        private string password;
        private string port;
        private string server;
        private string user;

        [JsonProperty("enabled")]
        public bool Enabled {
            get {
                return this.enabled;
            }
            set {
                this.enabled = value;
            }
        }

        [JsonProperty("server")]
        public string Server {
            get {
                return this.server;
            }
            set {
                this.server = value;
            }
        }

        [JsonProperty("port")]
        public string Port {
            get {
                return this.port;
            }
            set {
                this.port = value;
            }
        }

        [JsonProperty("user")]
        public string User {
            get {
                return this.user;
            }
            set {
                this.user = value;
            }
        }

        [JsonProperty("password")]
        public string Password {
            get {
                return this.password;
            }
            set {
                this.password = value;
            }
        }

        public static ConfigurationProxy GetProxyConfiguration() {
            throw new NotImplementedException();
        }
    }
}
