﻿using System;
using System.Collections.Generic;
using Jaeger.Aplication.Validador.Beta.Contracts;
using Jaeger.Aplication.Validador.Beta.Domain;
using Jaeger.Util.Services;
using Jaeger.SAT.Consulta.Interfaces;
using Jaeger.SAT.Consulta.Services;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public class ValidadorService : IValidacionService, IValidacionProveedor {
        protected internal IStatus status = new Status();
        protected internal ValidaConfiguracion configuracion = new ValidaConfiguracion();
        protected IValidadorComprobante validador33;
        protected IValidadorComprobante validador32;
        protected IValidadorComprobante validador40;

        public ValidadorService(ValidaConfiguracion configuracion = null) {
            this.validador40 = new ValidadorCFDIv40();
            if (configuracion != null)
                this.configuracion = configuracion;
        }

        public string RFC { get; set; }

        public IDocumentoFiscal Procesar(IDocumentoFiscal documentoFiscal) {
            IValidadorComprobante current;
            // respuesta, datos generales
            var respuesta = new ComprobanteValidacion {
                Provider = "local",
                IdDocumento = documentoFiscal.IdDocumento,
                TipoComprobante = documentoFiscal.TipoComprobante,
                Folio = documentoFiscal.Folio,
                Serie = documentoFiscal.Serie,
                FechaEmision = documentoFiscal.FechaEmision.Value,
                EmisorNombre = documentoFiscal.EmisorNombre,
                EmisorRFC = documentoFiscal.EmisorRFC,
                ReceptorNombre = documentoFiscal.ReceptorNombre,
                ReceptorRFC = documentoFiscal.ReceptorRFC,
                Total = documentoFiscal.Total,
                FechaValidacion = DateTime.Now
            };

            var comprobante = new object();
            current = this.GetComprobante(documentoFiscal, ref comprobante);
            if (comprobante == null) {
                Console.WriteLine("Validador: No se cargo el comprobante fiscal.");
                return documentoFiscal;
            }

            current.Comprobante = comprobante;

            if (this.configuracion.VerificarCodificacionUTF8) {
                respuesta.Resultados.Add(current.Codificacion());
            }

            if (this.configuracion.ValidarXSD) {
                respuesta.Resultados.Add(current.Esquema());
            }

            respuesta.Resultados.Add(current.LugarExpedicion());
            respuesta.Resultados.Add(current.SelloSAT());
            respuesta.Resultados.AddRange(current.SelloCFDI());
            respuesta.Resultados.Add(current.UsoCFDI());
            respuesta.Resultados.Add(current.FormaPago());
            respuesta.Resultados.Add(current.MetodoPago());
            respuesta.Resultados.AddRange(this.GetEstado(documentoFiscal));
            documentoFiscal.Validacion = respuesta;

            if (string.IsNullOrEmpty(documentoFiscal.EmisorNombre)) {
                var _emisor = current.GetEmisor();
                if (_emisor != null) {
                    if (_emisor.HasValue()) {
                        try {
                            respuesta.EmisorNombre = _emisor[documentoFiscal.EmisorRFC];
                            documentoFiscal.EmisorNombre = _emisor[documentoFiscal.EmisorRFC];
                        } catch (Exception exp) {
                            Console.WriteLine("No se pudo recuperar nombre del emisor: " + exp.Message);
                        }
                    }
                }
            }

            if (this.RFC == documentoFiscal.EmisorRFC) {
                documentoFiscal.SubTipo = "Emitidos";
            } else if (this.RFC == documentoFiscal.ReceptorRFC) {
                documentoFiscal.SubTipo = "Recibidos";
            } else {
                documentoFiscal.SubTipo = "No definido";
            }

            if (documentoFiscal.Estado == "Vigente" && current.IsSelloCFDI) {
                documentoFiscal.Resultado = "Válido";
            } else {
                documentoFiscal.Resultado = "No Válido";
            }
            return documentoFiscal;
        }

        private IValidadorComprobante GetComprobante(IDocumentoFiscal documentoFiscal, ref object comprobante) {
            IValidadorComprobante current;
            var bComprobante = FileService.FromBase64(documentoFiscal.XmlContentB64);

            if (documentoFiscal.Version == "3.2") {
                comprobante = CFDI.V32.Comprobante.Deserialize(System.Text.Encoding.UTF8.GetString(bComprobante));
                if (this.validador32 == null) { this.validador32 = new ValidadorCFDIv32(); }
                current = this.validador32;
            } else if (documentoFiscal.Version == "3.3") {
                comprobante = CFDI.V33.Comprobante.LoadBase64(documentoFiscal.XmlContentB64);
                if (this.validador33 == null) { this.validador33 = new ValidadorCFDIv33(); }
                current = this.validador33;
            } else if (documentoFiscal.Version == "4.0") {
                comprobante = CFDI.V40.Comprobante.Deserialize(System.Text.Encoding.UTF8.GetString(bComprobante));
                if (this.validador40 == null) { this.validador40 = new ValidadorCFDIv40(); }
                current = this.validador40;
            } else {
                current = new ValidadorFactory();
            }

            return current;
        }

        #region estado SAT
        private List<ComprobanteValidacionPropiedad> GetEstado(IDocumentoFiscal documentoFiscal) {
            var estado = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.EstadoSAT);
            if (string.IsNullOrEmpty(documentoFiscal.IdDocumento)) {
                var estado1 = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.EstadoSAT);
                estado1.Valor = "No existe UUID.";
                estado1.IsValido = false;
                return new List<ComprobanteValidacionPropiedad> { estado1 };
            }

            var response = new List<ComprobanteValidacionPropiedad>();
            //var q = this.status.GetStatusCFDI(documentoFiscal.EmisorRFC, documentoFiscal.ReceptorRFC, documentoFiscal.Total, documentoFiscal.IdDocumento);
            var q = this.status.Execute(
                SAT.Consulta.Request.Create()
                .WithEmisorRFC(documentoFiscal.EmisorRFC)
                .WithReceptorRFC(documentoFiscal.ReceptorRFC)
                .WithTotal(documentoFiscal.Total)
                .WithFolioFiscal(documentoFiscal.IdDocumento)
                .Build());

            if (q != null) {
                // si contiene S Comprobante obtenido satisfactoriamente entonces es vigente
                if (q.CodigoEstatus.ToUpper().StartsWith("S")) {
                    estado.IsValido = true;
                    estado.Valor = "Vigente";
                } else if (q.CodigoEstatus.Contains("602")) {
                    estado.Valor = "No encontrado";
                    estado.IsValido = false;
                } else if (q.CodigoEstatus.Contains("601")) {
                    estado.Valor = "601";
                    estado.IsValido = false;
                }
                documentoFiscal.Estado = estado.Valor;
                response.Add(estado);

                if (q.ValidacionEFOS == "200") {
                    documentoFiscal.Situacion = "Sin reporte";
                    response.Add(ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.EFOS));
                } else {

                }
            }
            return response;
        }

        public List<ComprobanteValidacionPropiedad> GetEstadoSAT(object cfd) {
            if (cfd.GetType() == typeof(CFDI.V32.Comprobante)) {
                return this.GetEstadoSAT(cfd as CFDI.V32.Comprobante);
            } else if (cfd.GetType() == typeof(CFDI.V33.Comprobante)) {
                return this.GetEstadoSAT(cfd as CFDI.V33.Comprobante);
            } else if (cfd.GetType() == typeof(CFDI.V40.Comprobante)) {
                return this.GetEstadoSAT(cfd as CFDI.V40.Comprobante);
            }
            return new List<ComprobanteValidacionPropiedad>();
        }

        public List<ComprobanteValidacionPropiedad> GetEstadoSAT(CFDI.V32.Comprobante cfd) {
            return this.GetAcuse(cfd.Emisor.rfc, cfd.Receptor.rfc, cfd.total, cfd.Complemento.TimbreFiscalDigital.UUID);
        }

        public List<ComprobanteValidacionPropiedad> GetEstadoSAT(CFDI.V33.Comprobante cfd) {
            return this.GetAcuse(cfd.Emisor.Rfc, cfd.Receptor.Rfc, cfd.Total, cfd.Complemento.TimbreFiscalDigital.UUID);
        }

        public List<ComprobanteValidacionPropiedad> GetEstadoSAT(CFDI.V40.Comprobante cfd) {
            return this.GetAcuse(cfd.Emisor.Rfc, cfd.Receptor.Rfc, cfd.Total, cfd.Complemento.TimbreFiscalDigital.UUID);
        }

        private List<ComprobanteValidacionPropiedad> GetAcuse(string rfcEmisor, string rfcReceptor, decimal total, string idDocumento) {
            if (string.IsNullOrEmpty(idDocumento)) {
                var estado1 = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.EstadoSAT);
                estado1.Valor = "No existe UUID.";
                estado1.IsValido = false;
                return new List<ComprobanteValidacionPropiedad> { estado1 };
            }

            var response = new List<ComprobanteValidacionPropiedad>();
            var estado = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.EstadoSAT);
            IRequest request = Jaeger.SAT.Consulta.Request.Create().WithEmisorRFC(rfcEmisor).WithReceptorRFC(rfcReceptor).WithTotal(total).WithFolioFiscal(idDocumento).Build();
            //var q = this.status.GetStatusCFDI(rfcEmisor, rfcReceptor, total, idDocumento);
            var q = this.status.Execute(request);
            if (q != null) {
                // si contiene S Comprobante obtenido satisfactoriamente entonces es vigente
                if (q.CodigoEstatus.ToUpper().StartsWith("S")) {
                    estado.IsValido = true;
                    estado.Valor = q.Estado;
                } else if (q.CodigoEstatus.Contains("602")) {
                    estado.Valor = "No encontrado";
                    estado.IsValido = false;
                } else if (q.CodigoEstatus.Contains("601")) {
                    estado.Valor = "601";
                    estado.IsValido = false;
                }
                response.Add(estado);

                if (q.ValidacionEFOS == "200") {
                    response.Add(ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.EFOS));
                }
            }

            return response;
        }
        #endregion
    }
}
