﻿using System;
using System.IO;
using Jaeger.Aplication.Validador.Beta.Domain;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public static class FileBakcupService {
        public static bool MoveTo(this DocumentoFiscal documentoFiscal) {
            var PathRoot = @"C:\CFDI\CFDI.Repositorio";
            var year = documentoFiscal.FechaEmision.Value.Year.ToString("0000");
            var month = documentoFiscal.FechaEmision.Value.Month.ToString("00");
            var day = documentoFiscal.FechaEmision.Value.Month.ToString("00");
            var rfc = documentoFiscal.Tipo == "Emitidos" ? documentoFiscal.EmisorRFC.ToUpper() : documentoFiscal.ReceptorRFC.ToUpper();
            var carpetaDestino = Path.Combine(PathRoot, rfc, documentoFiscal.Tipo, year, month, day);

            // si existe el origen continuamos
            if (!File.Exists(documentoFiscal.PathXML))
                return false;

            if (!File.Exists(Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".xml"))) {
                if (!Directory.Exists(carpetaDestino)) { Directory.CreateDirectory(carpetaDestino); }
                var si = MoveFile(documentoFiscal.PathXML, Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".xml"));
                if (si)
                    documentoFiscal.PathXML = Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".xml");
            }

            if (!File.Exists(Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".pdf"))) {
                if (!Directory.Exists(carpetaDestino)) { Directory.CreateDirectory(carpetaDestino); }
                var si = MoveFile(documentoFiscal.PathPDF, Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".pdf"));
                if (si)
                    documentoFiscal.PathPDF = Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".pdf");
            }

            return false;
        }

        public static bool MoveTo(this DocumentoFiscal documentoFiscal, string carpetaDestino) {
            // si existe el origen continuamos
            if (!File.Exists(documentoFiscal.PathXML))
                return false;

            if (!File.Exists(Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".xml"))) {
                if (!Directory.Exists(carpetaDestino)) { Directory.CreateDirectory(carpetaDestino); }
                var si = MoveFile(documentoFiscal.PathXML, Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".xml"));
                if (si)
                    documentoFiscal.PathXML = Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".xml");
            }

            if (!File.Exists(Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".pdf"))) {
                if (!Directory.Exists(carpetaDestino)) { Directory.CreateDirectory(carpetaDestino); }
                var si = MoveFile(documentoFiscal.PathPDF, Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".pdf"));
                if (si)
                    documentoFiscal.PathPDF = Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".pdf");
            }

            return false;
        }

        public static bool MoveFile(string source, string destination) {
            try {
                if (File.Exists(source)) {
                    File.Move(source, destination);
                }
                return true;
            } catch (Exception exception1) {
                Console.WriteLine(exception1.Message);
            }
            return false;
        }
    }
}