﻿using System;
using System.IO;
using System.Xml;
using Jaeger.Aplication.Validador.Beta.Domain;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public class CFDReaderSerialize : CFDReaderBase, ICFDReader {
        #region declaraciones
        protected internal DocumentoFiscal _DocumentoFiscal;
        #endregion

        public CFDReaderSerialize() {
        }

        public CFDReaderSerialize(string stringXml) {

        }

        public DocumentoFiscal GetDocumento {
            get { return this._DocumentoFiscal; }
        }

        public DocumentoFiscal Reader(FileInfo fileInfo) {
            return GetComprobante(fileInfo);
        }

        public static DocumentoFiscal GetComprobante(string stringReader) {
            return GetComprobante(new StringReader(stringReader));
        }

        public static DocumentoFiscal GetComprobante(FileInfo fileInfo) {
            var xml = new XmlDocument();
            xml.Load(fileInfo.FullName);
            return GetComprobante(new StringReader(xml.OuterXml));
        }

        public static DocumentoFiscal GetComprobante(StringReader stringReader) {
            var xml = new XmlDocument();
            var xml2 = stringReader.ReadToEnd();
            if (CFDReader.IsValidXml(xml2)) {
                xml.LoadXml(xml2);
                string str = GetVersion(xml);
                if (str == "4.0") {
                    return GetComprobante(CFDI.V40.Comprobante.LoadXml(xml.OuterXml));
                } else if (str != "3.3") {
                    if (str != "3.2") {
                        throw new Exception("El comprobante contiene una versión no soportada o el formato es incorrecto");
                    }
                    return GetComprobante(CFDI.V32.Comprobante.LoadXml(xml.OuterXml));
                } else {
                    return GetComprobante(CFDI.V33.Comprobante.LoadXml(xml.OuterXml));
                }
            }
            return null;
        }

        private static DocumentoFiscal GetComprobante(CFDI.V32.Comprobante comprobante) {
            if (comprobante == null) {
                return null;
            }
            var response = new DocumentoFiscal {
                Version = comprobante.version,
                ClaveExportacion = "NA",
                Descuento = comprobante.descuento,
                EmisorNombre = comprobante.Emisor.nombre,
                EmisorRegimenFiscal = "",
                EmisorRFC = comprobante.Emisor.rfc,
                FechaEmision = comprobante.fecha,
                Folio = comprobante.folio,
                TipoComprobante = GetTipoComprobante(comprobante.tipoDeComprobante),
                FormaPago = comprobante.formaDePago,
                MetodoPago = comprobante.metodoDePago,
                LugarExpedicion = comprobante.LugarExpedicion,
                Moneda = comprobante.Moneda,
                NoCertificado = comprobante.noCertificado,
                ReceptorNombre = comprobante.Receptor.nombre,
                ReceptorRFC = comprobante.Receptor.rfc,
                ReceptorRegimenFiscal = "",
                ReceptorDomicilioFiscal = "",
            };

            if (comprobante.Complemento != null) {
                if (comprobante.Complemento.TimbreFiscalDigital != null) {
                    response.IdDocumento = comprobante.Complemento.TimbreFiscalDigital.UUID;
                    response.FechaTimbre = comprobante.Complemento.TimbreFiscalDigital.FechaTimbrado;
                    //response.RFCProvCertif = comprobante.Complemento.TimbreFiscalDigital.rr;
                    response.NoCertificadoProvCertif = comprobante.Complemento.TimbreFiscalDigital.noCertificadoSAT;
                }
            }
            response.Conceptos = GetConceptos(comprobante.Conceptos);
            return response;
        }

        private static System.Collections.Generic.List<DocumentoFiscalConcepto> GetConceptos(CFDI.V32.ComprobanteConcepto[] conceptos) {
            if (conceptos.HasValue()) {
                var responses = new System.Collections.Generic.List<DocumentoFiscalConcepto>();
                foreach (var item in conceptos) {
                    responses.Add(new DocumentoFiscalConcepto {
                        Cantidad = item.cantidad,
                        ClaveProdServ = "",
                        ClaveUnidad = "",
                        Descripcion = item.descripcion,
                        Descuento = 0,
                        Importe = item.importe,
                        Unidad = item.unidad,
                        ValorUnitario = item.valorUnitario,
                        NoIdentificacion = "",
                        ObjetoImp = null
                    });
                }
                return responses;
            }
            return null;
        }

        private static DocumentoFiscal GetComprobante(CFDI.V33.Comprobante comprobante) {
            if (comprobante == null) {
                return null;
            }
            var response = new DocumentoFiscal {
                Version = comprobante.Version,
                ClaveExportacion = "NA",
                Descuento = comprobante.Descuento,
                EmisorNombre = comprobante.Emisor.Nombre,
                EmisorRegimenFiscal = comprobante.Emisor.RegimenFiscal,
                EmisorRFC = comprobante.Emisor.Rfc,
                FechaEmision = comprobante.Fecha,
                Folio = comprobante.Folio,
                TipoComprobante = GetTipoComprobante(comprobante.TipoDeComprobante),
                FormaPago = comprobante.FormaPago,
                MetodoPago = comprobante.MetodoPago,
                LugarExpedicion = comprobante.LugarExpedicion,
                Moneda = comprobante.Moneda,
                NoCertificado = comprobante.NoCertificado,
                ReceptorNombre = comprobante.Receptor.Nombre,
                ReceptorRFC = comprobante.Receptor.Rfc,
                ReceptorRegimenFiscal = "",
                ReceptorDomicilioFiscal = "", 
                UsoCFDI = comprobante.Receptor.UsoCFDI
            };

            if (comprobante.Complemento != null) {
                if (comprobante.Complemento.TimbreFiscalDigital != null) {
                    response.IdDocumento = comprobante.Complemento.TimbreFiscalDigital.UUID;
                    response.FechaTimbre = comprobante.Complemento.TimbreFiscalDigital.FechaTimbrado;
                    response.RFCProvCertif = comprobante.Complemento.TimbreFiscalDigital.RfcProvCertif;
                    response.NoCertificadoProvCertif = comprobante.Complemento.TimbreFiscalDigital.RfcProvCertif;
                }
            }

            if (comprobante.Impuestos.HasValue()) {
                response.TrasladoIVA = comprobante.Impuestos.TotalImpuestosTrasladados;
            }
            response.Conceptos = GetConceptos(comprobante.Conceptos);
            return response;
        }

        private static System.Collections.Generic.List<DocumentoFiscalConcepto> GetConceptos(CFDI.V33.ComprobanteConcepto[] conceptos) {
            if (conceptos != null) {
                var responses = new System.Collections.Generic.List<DocumentoFiscalConcepto>();
                foreach (var item in conceptos) {
                    responses.Add(new DocumentoFiscalConcepto {
                        Cantidad = item.Cantidad,
                        ClaveProdServ = item.ClaveProdServ,
                        ClaveUnidad = item.ClaveUnidad,
                        Descripcion = item.Descripcion,
                        Descuento = (item.DescuentoSpecified ? item.Descuento : 0),
                        Importe = item.Importe,
                        Unidad = item.Unidad,
                        ValorUnitario = item.ValorUnitario,
                        NoIdentificacion = item.NoIdentificacion,
                        ObjetoImp = null
                    });
                }
                return responses;
            }
            return null;
        }

        private static DocumentoFiscal GetComprobante(CFDI.V40.Comprobante comprobante) {
            if (comprobante == null) {
                return null;
            }
            var response = new DocumentoFiscal {
                Version = comprobante.Version,
                ClaveExportacion = comprobante.Exportacion,
                Descuento = comprobante.Descuento,
                EmisorNombre = comprobante.Emisor.Nombre,
                EmisorRegimenFiscal = "",
                EmisorRFC = comprobante.Emisor.Rfc,
                FechaEmision = comprobante.Fecha,
                Folio = comprobante.Folio,
                TipoComprobante = GetTipoComprobante(comprobante.TipoDeComprobante),
                FormaPago = comprobante.FormaPago,
                MetodoPago = comprobante.MetodoPago,
                LugarExpedicion = comprobante.LugarExpedicion,
                Moneda = comprobante.Moneda,
                NoCertificado = comprobante.NoCertificado,
                ReceptorNombre = comprobante.Receptor.Nombre,
                ReceptorRFC = comprobante.Receptor.Rfc,
                ReceptorRegimenFiscal = "",
                ReceptorDomicilioFiscal = "",
            };

            if (comprobante.Complemento != null) {
                if (comprobante.Complemento.TimbreFiscalDigital != null) {
                    response.IdDocumento = comprobante.Complemento.TimbreFiscalDigital.UUID;
                    response.FechaTimbre = comprobante.Complemento.TimbreFiscalDigital.FechaTimbrado;
                    response.RFCProvCertif = comprobante.Complemento.TimbreFiscalDigital.RfcProvCertif;
                    response.NoCertificadoProvCertif = comprobante.Complemento.TimbreFiscalDigital.NoCertificadoSAT;
                }
            }

            if (comprobante.Impuestos.HasValue()) {
                response.TrasladoIVA = comprobante.Impuestos.TotalImpuestosTrasladados;
            }
            response.Conceptos = GetConceptos(comprobante.Conceptos);
            return response;
        }

        private static System.Collections.Generic.List<DocumentoFiscalConcepto> GetConceptos(CFDI.V40.ComprobanteConcepto[] conceptos) {
            if (conceptos != null) {
                var responses = new System.Collections.Generic.List<DocumentoFiscalConcepto>();
                foreach (var item in conceptos) {
                    responses.Add(new DocumentoFiscalConcepto {
                        Cantidad = item.Cantidad,
                        ClaveProdServ = item.ClaveProdServ,
                        ClaveUnidad = item.ClaveUnidad,
                        Descripcion = item.Descripcion,
                        Descuento = (item.DescuentoSpecified ? item.Descuento : 0),
                        Importe = item.Importe,
                        Unidad = item.Unidad,
                        ObjetoImp = item.ObjetoImp,
                        ValorUnitario = item.ValorUnitario,
                        NoIdentificacion = item.NoIdentificacion
                    });
                }
                return responses;
            }
            return null;
        }
    }
}
