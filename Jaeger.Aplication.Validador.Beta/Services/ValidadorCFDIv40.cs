﻿using System.Collections.Generic;
using Jaeger.Aplication.Validador.Beta.Domain;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public class ValidadorCFDIv40 : ValidadorFactory, IValidadorComprobante {
        protected CFDI.V40.Comprobante cfdi40;

        public override object Comprobante {
            get { return base.Comprobante; }
            set {
                base.Comprobante = value;
                this.cfdi40 = value as CFDI.V40.Comprobante;
            }
        }

        public override ComprobanteValidacionPropiedad Codificacion() {
            var bcfdi = System.Text.UTF8Encoding.UTF8.GetBytes(this.cfdi40.OriginalXmlString);
            var validacion = ValidacionCodificacionService.Verificar(bcfdi);
            this.IsCodificacion = validacion.IsValido;
            return validacion;
        }

        public override ComprobanteValidacionPropiedad LugarExpedicion() {
            var validacion = CodigoPostalService.Validar(this.cfdi40.LugarExpedicion);
            return validacion;
        }

        public override ComprobanteValidacionPropiedad UsoCFDI() {
            var validacion = UsoCFDIService.Validar(this.cfdi40.Receptor.UsoCFDI);
            return validacion;
        }

        public override List<ComprobanteValidacionPropiedad> SelloCFDI() {
            this.IsSelloCFDI = false;
            if (string.IsNullOrEmpty(this.cfdi40.Certificado)) {
                return base.SelloCFDI();
            }
            var selloValido = new List<ComprobanteValidacionPropiedad>();
            var esValido = ValidacionSelloDigital.Validar(this.cfdi40.Sello, this.cfdi40.Certificado, this.cfdi40.CadenaOriginalOff, ValidacionSelloDigital.AlgoritmoSha.Sha256);
            if (esValido == false) {
                esValido = ValidacionSelloDigital.Validar(this.cfdi40.Sello, this.cfdi40.Certificado, this.cfdi40.CadenaOriginal, ValidacionSelloDigital.AlgoritmoSha.Sha256);
                if (esValido == false) {
                    esValido = ValidacionSelloDigital.Validar(this.cfdi40.Sello, this.cfdi40.Certificado, this.cfdi40.CadenaOriginal2Off, ValidacionSelloDigital.AlgoritmoSha.Sha256);
                }
            }
            this.IsSelloCFDI = esValido;
            var response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.SelloCFDI);
            response.IsValido = esValido;
            this.IsSelloCFDI = esValido;
            if (response.IsValido == false) { response.Valor = "No válido"; }
            selloValido.Add(response);

            selloValido.Add(this.GetCertificado(this.cfdi40.Certificado, this.cfdi40.Fecha));
            return selloValido;
        }

        public override ComprobanteValidacionPropiedad SelloSAT() {
            if (this.cfdi40.Complemento != null) {
                if (this.cfdi40.Complemento.TimbreFiscalDigital != null) {
                    return base.SelloSAT(this.cfdi40.Complemento.TimbreFiscalDigital);
                }
            }
            return base.SelloSAT();
        }

        public override ComprobanteValidacionPropiedad EstadoSAT() {
            if (this.cfdi40.Complemento != null) {
                if (this.cfdi40.Complemento.TimbreFiscalDigital != null) {
                    return this.EstadoSAT(this.cfdi40.Emisor.Rfc, this.cfdi40.Receptor.Rfc, this.cfdi40.Total, this.cfdi40.Complemento.TimbreFiscalDigital.UUID);
                }
            }
            return base.EstadoSAT();
        }

        public override Dictionary<string, string> GetEmisor() {
            if (this.cfdi40 != null) {
                if (this.cfdi40.Certificado != null) {
                    return this.GetEmisor(this.cfdi40.Certificado);
                }
            }
            return null;
        }
    }
}
