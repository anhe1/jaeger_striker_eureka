﻿using Jaeger.Aplication.Validador.Beta.Domain;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public static class ValidacionRespuesta {
        public enum PropiedadValidacionEnum {
            Esquemas,
            FormaPago,
            MetodoPago,
            LugarExpedicion,
            UsoCFDI,
            SelloCFDI,
            SelloSAT,
            Codificion,
            EstadoSAT,
            EFOS
        }

        public static ComprobanteValidacionPropiedad GetPropiedad(PropiedadValidacionEnum propiedad) {
            var response = new ComprobanteValidacionPropiedad() { Tipo = PropiedadTipoEnum.Informacion, IsValido = true };
            switch (propiedad) {
                case PropiedadValidacionEnum.Esquemas:
                    response.Nombre = "Esquema";
                    response.Valor = "Esquema válido";
                    break;
                case PropiedadValidacionEnum.Codificion:
                    response.Nombre = "Codificación";
                    response.Valor = "Codificación del CFD/CFDI es UTF-8";
                    break;
                case PropiedadValidacionEnum.FormaPago:
                    response.Nombre = "Forma de Pago";
                    response.Valor = "";
                    break;
                case PropiedadValidacionEnum.MetodoPago:
                    response.Nombre = "Método de Pago";
                    response.Valor = "";
                    break;
                case PropiedadValidacionEnum.LugarExpedicion:
                    response.Nombre = "Expedición";
                    break;
                case PropiedadValidacionEnum.SelloCFDI:
                    response.Nombre = "Sello CFDI";
                    response.Valor = "Correcto";
                    break;
                case PropiedadValidacionEnum.SelloSAT:
                    response.Nombre = "Sello SAT";
                    response.Valor = "Correcto";
                    break;
                case PropiedadValidacionEnum.EstadoSAT:
                    response.Nombre = "Estado CFDI";
                    response.Valor = "Vigente";
                    response.Tipo = PropiedadTipoEnum.Informacion;
                    break;
                case PropiedadValidacionEnum.EFOS:
                    response.Nombre = "Artículo 69-B";
                    response.Valor = "Sin reporte";
                    response.Tipo = PropiedadTipoEnum.Informacion;
                    break;
                case PropiedadValidacionEnum.UsoCFDI:
                    response.Nombre = "Uso de CFDI";
                    response.Valor = "Correcto";
                    response.Tipo = PropiedadTipoEnum.Informacion;
                    break;
            }
            return response;
        }
    }
}
