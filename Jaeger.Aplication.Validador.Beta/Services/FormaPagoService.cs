﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Jaeger.Aplication.Validador.Beta.Domain;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public static class FormaPagoService {
        public static Dictionary<string, string> Catalogo = new Dictionary<string, string>();

        static FormaPagoService() {
            Catalogo.Add("01", "Efectivo");
            Catalogo.Add("02", "Cheque nominativo");
            Catalogo.Add("03", "Transferencia electrónica de fondos");
            Catalogo.Add("04", "Tarjeta de crédito");
            Catalogo.Add("05", "Monedero electrónico");
            Catalogo.Add("06", "Dinero electrónico");
            Catalogo.Add("08", "Vales de despensa");
            Catalogo.Add("12", "Dación en pago");
            Catalogo.Add("13", "Pago por subrogación");
            Catalogo.Add("14", "Pago por consignación");
            Catalogo.Add("15", "Condonación");
            Catalogo.Add("17", "Compensación");
            Catalogo.Add("23", "Novación");
            Catalogo.Add("24", "Confusión");
            Catalogo.Add("25", "Remisión de deuda");
            Catalogo.Add("26", "Prescripción o caducidad");
            Catalogo.Add("27", "A satisfacción del acreedor");
            Catalogo.Add("28", "Tarjeta de débito");
            Catalogo.Add("29", "Tarjeta de servicios");
            Catalogo.Add("30", "Aplicación de anticipos");
            Catalogo.Add("31", "Intermediario pagos");
            Catalogo.Add("99", "Por definir");
        }

        public static ComprobanteValidacionPropiedad Validar(object cfd) {
            var _response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.FormaPago);
            if (cfd.GetType() == typeof(CFDI.V32.Comprobante)) {
                return Validar(cfd as CFDI.V32.Comprobante);
            } else if (cfd.GetType() == typeof(CFDI.V33.Comprobante)) {
                return Validar(cfd as CFDI.V33.Comprobante);
            } else if (cfd.GetType() == typeof(CFDI.V40.Comprobante)) {
                return Validar(cfd as CFDI.V40.Comprobante);
            }
            return _response;
        }

        public static ComprobanteValidacionPropiedad Validar(this CFDI.V32.Comprobante cfd) {
            var _response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.FormaPago);
            _response.Valor = cfd.formaDePago;
            return _response;
        }

        public static ComprobanteValidacionPropiedad Validar(this CFDI.V33.Comprobante cfd) {
            var _response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.FormaPago);
            _response.Valor = cfd.FormaPago;
            if (cfd != null) {
                if (cfd.TipoDeComprobante != "P") {
                    if (cfd.FormaPago != null) {
                        var existe = string.Empty;
                        if (!FormaPagoService.Catalogo.TryGetValue(Regex.Replace(cfd.FormaPago, "[^\\d]", ""), out existe)) {
                            _response.Valor = "El atributo condicional (opcional) para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, no esta disponible en este comprobante.";
                            _response.IsValido = false;
                        } else {
                            var clave = FormaPagoService.Catalogo.Where(it => it.Key == Regex.Replace(cfd.FormaPago, "[^\\d]", "")).FirstOrDefault();
                            _response.Valor = string.Concat("Correcto (", clave.Key, " ", clave.Value, ")");
                            return _response;
                        }
                    }
                        
                }
            }
            return _response;
        }

        public static ComprobanteValidacionPropiedad Validar(this CFDI.V40.Comprobante cfd) {
            var _response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.FormaPago);
            _response.Valor = cfd.FormaPago;
            if (cfd != null) {
                if (cfd.TipoDeComprobante != "P") {
                    if (cfd.FormaPago != null) {
                        var existe = string.Empty;
                        if (!FormaPagoService.Catalogo.TryGetValue(Regex.Replace(cfd.FormaPago, "[^\\d]", ""), out existe)) {
                            _response.Valor = "El atributo condicional (opcional) para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, no esta disponible en este comprobante.";
                            _response.IsValido = false;
                        } else {
                            var clave = FormaPagoService.Catalogo.Where(it => it.Key == Regex.Replace(cfd.FormaPago, "[^\\d]", "")).FirstOrDefault();
                            _response.Valor = string.Concat("Correcto (", clave.Key, " ", clave.Value, ")");
                            return _response;
                        }
                    }
                }
            }
            return _response;
        }
    }
}
