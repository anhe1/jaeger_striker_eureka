﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Threading;
using Jaeger.Catalogos.Entities;
using Jaeger.Util.Services;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public class DownloaderCertificadoSAT {
        public static readonly string HttpSAT = @"https://rdc.sat.gob.mx/rccf";

        public DownloaderCertificadoSAT() {

        }

        public bool ValidaNumeroSerie(string numeroDeSerie) {
            return DownloaderCertificadoSAT.ValidaSerial(numeroDeSerie);
        }

        public Certificate Descargar(string numeroDeSerie) {
            return DownloaderCertificadoSAT.Buscar(numeroDeSerie);
        }

        public string DescargarArchivo(string numeroDeSerie) {
            var cer = DownloaderCertificadoSAT.Buscar(numeroDeSerie);
            if (cer != null)
                return cer.CerB64;
            return string.Empty;
        }

        #region metodos publicos
        private static bool ValidaSerial(string serial) {
            // proposito: validar serial de certificado
            if ((string.IsNullOrEmpty(serial)))
                return false;
            if ((!serial.StartsWith("0")))
                return false;
            if ((serial.Replace("0", "").Length == 0))
                return false;
            if ((serial.Length < 20))
                return false;
            return true;
        }

        public static string GetPathHttpsSAT(string oPathHttp, string oSerial) {
            string sParte1 = oSerial.Substring(0, 6);
            string sParte2 = oSerial.Substring(6, 6);
            string sParte3 = oSerial.Substring(12, 2);
            string sParte4 = oSerial.Substring(14, 2);
            string sParte5 = oSerial.Substring(16, 2);
            string strUrlReturn = "{0}/{1}/{2}/{3}/{4}/{5}/{6}.cer";
            return string.Format(strUrlReturn, oPathHttp, sParte1, sParte2, sParte3, sParte4, sParte5, oSerial);
        }

        public static Certificate Buscar(string oSerial) {
            if ((!DownloaderCertificadoSAT.ValidaSerial(oSerial))) {
                Console.WriteLine("Numero de serie no valido");
                return null;
            }

            byte[] fileByZip = null;
            string pathHttpSat = DownloaderCertificadoSAT.GetPathHttpsSAT(DownloaderCertificadoSAT.HttpSAT, oSerial);
            int intento = 0;

            do {
                intento = intento + 1;
                if ((fileByZip != null))
                    continue;
                else {
                    fileByZip = FileService.DownloadFile(pathHttpSat);
                    if ((fileByZip != null))
                        continue;
                }
                Thread.Sleep(200 * intento);
            }
            while (fileByZip == null && intento < 3);
            
            var oCertificate = Helper2Certificado.GetInfo(fileByZip);
            Console.WriteLine("Despues de intentos");
            return oCertificate;
        }

        public static class Helper2Certificado {
            public static Certificate GetInfo(byte[] array) {
                X509Certificate2 x509 = new X509Certificate2(array);
                Certificate response = new Certificate() {
                    BeginDateExpiration = x509.NotBefore,
                    EndDateExpiration = x509.NotAfter,
                    Serial = Helper2Certificado.GetSerie(x509.SerialNumber),
                    CerB64 = Convert.ToBase64String(array)
                };
                return response;
            }

            public static Dictionary<string, string> GetEmisor(string base64) {
                var d1 = new Dictionary<string, string>();
                var array = System.Text.Encoding.UTF8.GetBytes(base64);
                X509Certificate2 certificadoX509Field = new X509Certificate2(array);

                int num1 = certificadoX509Field.Subject.IndexOf(" OID.2.5.4.41=") + 14;
                if (num1 == 13) {
                    num1 = certificadoX509Field.Subject.IndexOf("CN=") + 3;
                }
                int length = certificadoX509Field.Subject.IndexOf(", ", num1);
                if (length == -1) {
                    length = certificadoX509Field.Subject.Length;
                }
                var _RazonSocial = certificadoX509Field.Subject.Substring(num1, length - num1).Trim();

                Regex regex = new Regex("[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9A]");
                int num = certificadoX509Field.Subject.IndexOf("OID.2.5.4.45=");
                if (num < 0) {
                    num = 0;
                }
                Match match = regex.Match(certificadoX509Field.Subject, num);
                var _RFC = match.Value.ToString();

                d1.Add(_RFC, _RazonSocial);
                return d1;
            }

            public static string GetSerie(string serialAscci) {
                string serie = "";
                string str1 = "";
                for (int i = 0; i <= serialAscci.Length - 1; i += 2) {
                    str1 = serialAscci.Substring(i, 2);
                    serie = string.Concat(serie, char.ConvertFromUtf32(Convert.ToInt32(str1, 16)));
                }
                return serie;
            }
        }
        #endregion
    }
}
