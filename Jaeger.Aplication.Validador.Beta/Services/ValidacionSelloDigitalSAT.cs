﻿using Jaeger.Catalogos.Repositories;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public class ValidacionSelloDigitalSAT {
        // catalogo de certificados de proveedores de certificacion autorizados
        public static CertificadosCatalogo Catalogo = new CertificadosCatalogo();

        static ValidacionSelloDigitalSAT() {
            ValidacionSelloDigitalSAT.Catalogo.Load();
        }

        public ValidacionSelloDigitalSAT() {
            ValidacionSelloDigitalSAT.Catalogo.Load();
        }

        public bool Verificar(object timbreFiscal) {
            if (timbreFiscal.GetType() == typeof(CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital)) {
                return ValidacionSelloDigitalSAT.Validar(timbreFiscal as CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital);
            } else if (timbreFiscal.GetType() == typeof(CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital)) {
                return ValidacionSelloDigitalSAT.Validar(timbreFiscal as CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital);
            }
            return false;
        }

        #region metodos estaticos
        public static bool Validar(object timbreFiscal) {
            if (timbreFiscal.GetType() == typeof(CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital)) {
                return ValidacionSelloDigitalSAT.Validar(timbreFiscal as CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital);
            } else if (timbreFiscal.GetType() == typeof(CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital)) {
                return ValidacionSelloDigitalSAT.Validar(timbreFiscal as CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital);
            }
            return false;
        }

        public static bool Validar(CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital timbreFiscal) {
            if (timbreFiscal != null) {
                var certificado = ValidacionSelloDigitalSAT.Catalogo.Search(timbreFiscal.noCertificadoSAT);
                if (certificado == null) {
                    certificado = DownloaderCertificadoSAT.Buscar(timbreFiscal.noCertificadoSAT);
                    if (certificado != null) {
                        ValidacionSelloDigitalSAT.Catalogo.Add(certificado);
                        ValidacionSelloDigitalSAT.Catalogo.Save();
                    }
                }

                if (!string.IsNullOrEmpty(certificado.CerB64)) {
                    string cadenaOriginal = timbreFiscal.CadenaOriginal;
                    return ValidacionSelloDigital.Validar(timbreFiscal.selloSAT, certificado.CerB64, cadenaOriginal, ValidacionSelloDigital.AlgoritmoSha.Sha1);
                }
            }
            return false;
        }

        public static bool Validar(CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital timbreFiscal) {
            if (timbreFiscal != null) {
                var certificado = ValidacionSelloDigitalSAT.Catalogo.Search(timbreFiscal.NoCertificadoSAT);
                if (certificado == null) {
                    certificado = DownloaderCertificadoSAT.Buscar(timbreFiscal.NoCertificadoSAT);
                    if (certificado != null) {
                        ValidacionSelloDigitalSAT.Catalogo.Add(certificado);
                        ValidacionSelloDigitalSAT.Catalogo.Save();
                    }
                }

                if (certificado != null) {
                    if (!string.IsNullOrEmpty(certificado.CerB64)) {
                        string cadenaOriginal = timbreFiscal.CadenaOriginal;
                        return ValidacionSelloDigital.Validar(timbreFiscal.SelloSAT, certificado.CerB64, cadenaOriginal, ValidacionSelloDigital.AlgoritmoSha.Sha256);
                    }
                }
            }
            return false;
        }
        #endregion
    }
}
