﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Jaeger.Aplication.Validador.Beta.Domain;
using Jaeger.Util.Services;
using MiniExcelLibs;
using MiniExcelLibs.OpenXml;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public class Administrador {
        private readonly EmbeddedResources resource = new EmbeddedResources("Jaeger.Aplication.Validador.Beta");
        public BindingList<DocumentoFiscal> _DataSource;
        public BindingList<DocumentoFiscalError> _DataError;
        protected internal IDirectoryService directoryService;
        public IProgress<Progreso> progreso;

        public Administrador() {
            this._DataSource = new BindingList<DocumentoFiscal>();
            this._DataError = new BindingList<DocumentoFiscalError>();
            this.directoryService = new DirectoryService();
        }

        public void SearchNormal(string path, SearchOption options, IProgress<Progreso> progreso) {
            Stopwatch stopwatch = new Stopwatch();
            var aqui = this.directoryService.GetFiles(path, "*.xml", options);
            var reporte = new Progreso() { Caption = "Obteniendo lista de archivos ..." };
            var contador = 0;
            foreach (var item in aqui) {
                FileInfo info = new FileInfo(item);
                reporte.Caption = "Procesando: " + item;
                progreso.Report(reporte);
                contador++;
                var add = this.GetComprobante1(info);
                if (add != null) {
                    add.PathXML = info.FullName;
                    this.Agregar(add);
                } else {
                    this._DataError.Add(new DocumentoFiscalError(item, "Error al cargar el archivo."));

                }
            }
            var c1 = aqui.Count();
            Console.WriteLine(string.Format("Archivos {0} de {1}", contador.ToString(), c1));
            TimeSpan elapsed = stopwatch.Elapsed;
            Console.WriteLine(string.Format("{0:00}:{1:00}:{2:00}", elapsed.Hours, elapsed.Minutes, elapsed.Seconds));
        }

        public async Task Search(string path, SearchOption options) {
            var xmlFiles = this.directoryService.GetFiles(path, "*.xml", options);
            var lista = new List<DocumentoFiscal>();
            await Task.Run(() => {
                foreach (var file in xmlFiles) {
                    var d1 = GetComprobante(new FileInfo(file));
                    if (d1 != null) { lista.Add(d1); }
                }
            });
            this._DataSource = new BindingList<DocumentoFiscal>(lista);
            return;
        }

        public async Task SearchParallelAsync(string path, SearchOption options, IProgress<Progreso> progreso) {
            var reporte = new Progreso() { Caption = "Obteniendo lista de archivos ..." };
            var xmlFiles = this.directoryService.GetFiles(path, "*.xml", options);
            var lista = new List<DocumentoFiscal>();
            await Task.Run(() => {
                progreso.Report(reporte);
                Parallel.ForEach<string>(xmlFiles, (site) => {
                    DocumentoFiscal d1 = GetComprobante(new FileInfo(site));
                    if (d1 != null) {
                        this._DataSource.Add(d1);
                        reporte.Caption = "Procesando: " + site;
                    }
                    progreso.Report(reporte);
                });
            });
            return;
        }

        public void Agregar(string item) {
            FileInfo info = new FileInfo(item);
            var add = this.GetComprobante1(info);
            if (add != null) {
                add.PathXML = info.FullName;
                this.Agregar(add);
            }
        }

        public bool Agregar(DocumentoFiscal documento) {
            try {
                var _search = this._DataSource.Where(it => it.IdDocumento == documento.IdDocumento).FirstOrDefault();
                if (_search == null) {
                    this._DataSource.Add(documento);
                } else {
                    Console.WriteLine("Duplicado");
                    documento.Resultado = "Duplicado";
                    this._DataError.Add(new DocumentoFiscalError(documento.PathXML, documento.Resultado));
                }
                return true;
            } catch (Exception exp) {
                Console.WriteLine(exp.Message);
            }
            return false;
        }

        public bool Remover(DocumentoFiscal documento) {
            try {
                this._DataSource.Remove(documento);
                return true;
            } catch (Exception exp) {
                Console.WriteLine(exp.Message);
            }
            return false;
        }

        public bool Remover(int index) {
            try {
                this._DataSource.RemoveAt(index);
                return true;
            } catch (Exception exp) {
                Console.WriteLine(exp.Message);
            }
            return false;
        }

        public bool ExportarExcel(string fileName, string fileTemplete = "") {
            // configuracion para la exportacion
            var config = new OpenXmlConfiguration() {
                IgnoreTemplateParameterMissing = false,
            };

            var data = new Dictionary<string, object>() {
                ["Validador"] = this._DataSource,
            };

            // si pasan un templete se carga
            if (File.Exists(fileTemplete)) {
                try {
                    MiniExcel.SaveAsByTemplate(fileName, fileTemplete, data, config);
                    return true;
                } catch (Exception ex) {
                    Console.WriteLine($"Could not save file {fileName} " + ex.Message);
                }
            }

            // probamos el templete por default
            try {
                var templatePath = this.resource.GetAsBytes("Jaeger.Aplication.Validador.Beta.Reports.Validacion.xlsx");
                MiniExcel.SaveAsByTemplate(fileName, templatePath, data, config);
                return true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            return false;
        }

        public bool DescargarTemplete(string destino) {
            // probamos el templete por default
            try {
                var templatePath = this.resource.GetAsBytes("Jaeger.Aplication.Validador.Beta.Reports.Validacion.xlsx");
                Jaeger.Util.Services.FileService.WriteFileByte(templatePath, destino);
                return true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        public static DocumentoFiscal GetComprobante(FileInfo localFile) {
            if (localFile.Exists == false) { return null; }

            var xmlDoc = new XmlDocument();
            var stringXml = File.ReadAllText(localFile.FullName, Encoding.UTF8);
            if (CFDReader.IsValidXml(stringXml)) {
                var response = new DocumentoFiscal();

                try {
                    xmlDoc.LoadXml(stringXml);
                } catch (Exception exp) {
                    Console.WriteLine(exp.Message);
                    xmlDoc.LoadXml(CFDReader.ExtraeCFDI(stringXml));
                }

                // remover las addendas del comprobante
                try {
                    //primero removemos todas las adendas que pudiera tener
                    var xElement = XElement.Parse(xmlDoc.InnerXml.ToString());
                    var xmlAddenda = xElement.Elements().Where(p => p.Name.LocalName == "Addenda").FirstOrDefault();
                    if (!(xmlAddenda == null)) {
                        xmlAddenda.Remove();
                    }
                    xmlDoc.LoadXml(xElement.ToString());
                    response = CFDReader.Reader(xElement.ToString());
                    if (response == null) {
                        var d1 = CFDReaderSerialize.GetComprobante(localFile);
                        if (d1 != null) { response = d1; }
                    }

                    if (response != null) {
                        response.XmlContentB64 = CFDReader.FileToBase64(localFile);
                        if (File.Exists(Path.ChangeExtension(localFile.FullName, "pdf"))) {
                            response.PathPDF = Path.ChangeExtension(localFile.FullName, "pdf");
                        } else {
                            //if (!string.IsNullOrEmpty(response.IdDocumento)) {
                            //    var id = BuscarPDF(localFile, response.IdDocumento);
                            //    if (File.Exists(id)) {
                            //        response.PathPDF = id;
                            //    }
                            //}
                        }
                    } else {
                        return null;
                    }
                } catch (Exception ex) {
                    Console.WriteLine("CFDReader:GetVersion: " + ex.Message);
                    response = null;

                } finally {
                    xmlDoc = null;
                }
                return response;
            } else {
                xmlDoc.LoadXml(stringXml);
            }
            return null;
        }

        public DocumentoFiscal GetComprobante1(FileInfo localFile) {
            return Administrador.GetComprobante(localFile);
        }
        public class Meta {
            public Meta() {

            }

            public string Version { get; set; }
            public string MetaType { get; set; }

            public override string ToString() {
                return string.Format("{0}-{1}", this.Version, this.MetaType);
            }
        }

        public static Meta GetVersion(XmlDocument xmldoc) {
            XmlNodeList tagName;
            var meta = new Meta();

            try {
                try {
                    tagName = xmldoc.GetElementsByTagName("Comprobante");
                    if (tagName.Count != 1) {
                        tagName = xmldoc.GetElementsByTagName("cfdi:Comprobante");
                        if (tagName.Count != 1) {
                            tagName = xmldoc.GetElementsByTagName("retenciones:Retenciones");
                            meta.MetaType = "Retenciones";
                            if (tagName.Count != 1) {
                                tagName = xmldoc.GetElementsByTagName("CancelaCFDResult");
                                if (tagName.Count != 1) {
                                    tagName = xmldoc.GetElementsByTagName("Acuse");
                                    if (tagName.Count != 1) {
                                    } else {
                                        meta.Version = "None";
                                        meta.MetaType = "Acuse";
                                    }
                                } else {
                                    meta.Version = "None";
                                    meta.MetaType = "Acuse";
                                }
                            } else {
                                meta.Version = tagName[0].Attributes["Version"].Value.ToString();
                                meta.Version = "V10";
                            }
                        } else {
                            meta.MetaType = "CFDI";
                            try {
                                var x = tagName[0].Attributes.GetEnumerator();
                                while (x.MoveNext()) {
                                    Console.WriteLine(((XmlAttribute)x.Current).LocalName);
                                    if (((XmlAttribute)x.Current).LocalName.ToLower() == "version") {
                                        meta.Version = ((XmlAttribute)x.Current).Value;
                                        break;
                                    }
                                }
                                if (meta.Version == "4.0")
                                    meta.Version = "V40";
                                else if (meta.Version == "3.3")
                                    meta.Version = "V33";
                                else if (meta.Version == "3.2")
                                    meta.Version = "V32";


                            } catch (Exception ex) {
                                Console.WriteLine("HelperCFDIReader:GetVersion: " + ex.Message);
                                try {
                                    meta.Version = tagName[0].Attributes["version"].Value.ToString();
                                    meta.Version = "V32";
                                } catch (Exception ex1) {
                                    Console.WriteLine(ex1.Message);
                                }
                            }
                        }
                    } 
                } catch (Exception ex) {
                    Console.WriteLine("HelperCFDIReader:GetVersion: " + ex.Message);
                }
            } finally {
                xmldoc = null;
                tagName = null;
            }
            return meta;
        }

        public static string BuscarPDF(FileInfo archivo, string idDocumento) {
            foreach (string item in Directory.GetFiles(archivo.DirectoryName, "*.pdf", SearchOption.AllDirectories)) {
                if (PDFExtractorService.GetUUID(item).ToUpper() == idDocumento) {
                    Console.WriteLine("Buscar PDF: " + item);
                    return item;
                }
            }
            return string.Empty;
        }

        public class Progreso : EventArgs {
            public Progreso() {
                this.Terminado = false;
            }

            public Progreso(string caption) {
                this.Caption = caption;
                this.Terminado = false;
            }

            public Progreso(int completado, int contador, string caption, bool terminado = false) {
                this.Completado = completado;
                this.Contador = contador;
                this.Caption = caption;
                this.Terminado = terminado;
            }

            /// <summary>
            /// obtener o establecer el % completado
            /// </summary>
            public int Completado { get; set; }

            /// <summary>
            /// obtener o establecer el contador de items
            /// </summary>
            public int Contador { get; set; }

            /// <summary>
            /// obtener o establecer texto
            /// </summary>
            public string Caption { get; set; }

            /// <summary>
            /// obtener o establecer si el proceso esta terminado
            /// </summary>
            public bool Terminado { get; set; }
        }
    }
}
