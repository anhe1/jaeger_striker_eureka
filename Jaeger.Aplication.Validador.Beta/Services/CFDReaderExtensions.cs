﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public static class CFDReaderExtensions {
        public static bool HasValue(this object thisValue) {
            if (thisValue == null || thisValue == DBNull.Value) return false;
            return thisValue.ToString() != "";
        }

        public static bool HasValue(this IEnumerable<object> thisValue) {
            if (thisValue == null || thisValue.Count() == 0) return false;
            return true;
        }

        public static string ObjToString(this object thisValue) {
            if (thisValue != null) return thisValue.ToString().Trim();
            return "";
        }

        public static string ObjToString(this object thisValue, string errorValue) {
            if (thisValue != null) return thisValue.ToString().Trim();
            return errorValue;
        }

        public static decimal ObjToDecimal(this object thisValue) {
            decimal reval = 0;
            if (thisValue != null && thisValue != DBNull.Value && decimal.TryParse(thisValue.ToString(), out reval)) {
                return reval;
            }
            return 0;
        }
    }
}
