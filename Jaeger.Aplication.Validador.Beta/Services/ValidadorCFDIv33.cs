﻿using System.Collections.Generic;
using System.Xml;
using Jaeger.Aplication.Validador.Beta.Domain;

namespace Jaeger.Aplication.Validador.Beta.Services {

    public class ValidadorCFDIv33 : ValidadorFactory, IValidadorComprobante {
        protected CFDI.V33.Comprobante cfdi33;

        public override object Comprobante {
            get { return base.Comprobante; }
            set {
                base.Comprobante = value;
                this.cfdi33 = value as CFDI.V33.Comprobante;
            }
        }

        public override ComprobanteValidacionPropiedad Codificacion() {
            if (this.cfdi33 == null) {
                var response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.Codificion);
                response.Valor = "El objeto CFDI 3.3 no existe";
                response.IsValido = false;
                return response;
            }
            var bcfdi = System.Text.UTF8Encoding.UTF8.GetBytes(this.cfdi33.OriginalXmlString);
            return ValidacionCodificacionService.Verificar(bcfdi);
        }

        public override ComprobanteValidacionPropiedad LugarExpedicion() {
            return CodigoPostalService.Validar(this.cfdi33.LugarExpedicion);
        }

        public override ComprobanteValidacionPropiedad UsoCFDI() {
            return UsoCFDIService.Validar(this.cfdi33.Receptor.UsoCFDI);
        }

        public override List<ComprobanteValidacionPropiedad> SelloCFDI() {
            if (string.IsNullOrEmpty(this.cfdi33.Certificado)) {
                return base.SelloCFDI();
            }
            var selloValido = new List<ComprobanteValidacionPropiedad>();
            var esValido = ValidacionSelloDigital.Validar(this.cfdi33.Sello, this.cfdi33.Certificado, this.cfdi33.CadenaOriginalOff, ValidacionSelloDigital.AlgoritmoSha.Sha256);
            if (esValido == false) {
                esValido = ValidacionSelloDigital.Validar(this.cfdi33.Sello, this.cfdi33.Certificado, this.cfdi33.CadenaOriginal, ValidacionSelloDigital.AlgoritmoSha.Sha256);
            }
            var response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.SelloCFDI);
            response.IsValido = esValido;
            this.IsSelloCFDI = esValido;
            if (response.IsValido == false) { response.Valor = "No válido"; }
            selloValido.Add(this.GetCertificado(this.cfdi33.Certificado, this.cfdi33.Fecha));
            selloValido.Add(response);
            return selloValido;
        }

        public override ComprobanteValidacionPropiedad SelloSAT() {
            if (this.cfdi33.Complemento != null) {
                if (this.cfdi33.Complemento.TimbreFiscalDigital != null) {
                    return base.SelloSAT(this.cfdi33.Complemento.TimbreFiscalDigital);
                }
            }
            return base.SelloSAT();
        }

        public override ComprobanteValidacionPropiedad EstadoSAT() {
            if (this.cfdi33.Complemento != null) {
                if (this.cfdi33.Complemento.TimbreFiscalDigital != null) {
                    return this.EstadoSAT(this.cfdi33.Emisor.Rfc, this.cfdi33.Receptor.Rfc, this.cfdi33.Total, this.cfdi33.Complemento.TimbreFiscalDigital.UUID);
                }
            }
            return base.EstadoSAT();
        }

        public override Dictionary<string, string> GetEmisor() {
            if (this.cfdi33 != null) {
                if (this.cfdi33.Certificado != null) {
                    return this.GetEmisor(this.cfdi33.Certificado);
                }
            }
            return null;
        }
    }
}
