﻿using System.IO.Compression;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Jaeger.Util.Services;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public class DownloaderUpdate {

        public const string sUrlDescarga_EFOS = "https://www.dsoft.mx/public/descargas/ListasNegras/eC_Meta/ListaNegra.zip";

        public DownloaderUpdate() {
                Descargar(sUrlDescarga_EFOS);
            
        }

        public static DateTime ObtenerFecha(string sUrl) {
            try {
                WebClient webClient = new WebClient();
                string end = (new StreamReader(webClient.OpenRead(sUrl))).ReadToEnd();
                DateTime dateTime = Convert.ToDateTime(end.Replace("\r\n", ""));
                return dateTime;
            } catch (Exception exception1) {
                Exception exception = exception1;
                //Log.Escribir(string.Concat("[", Sesion.sNombreSistema, "] Error: ", exception.Message), exception.StackTrace, Sesion.sNombreSistema);
                //throw exception;
            }
            return DateTime.Now;
        }

        public static void Descargar(string url) {
            string empty = Path.GetFileName(url);
            string empyt2 = @"C:\AdminCFD\Temporal\" + empty;
            using (WebClient webClient = new WebClient()) {
                webClient.DownloadFile(new Uri(sUrlDescarga_EFOS), empyt2);
            }
            var fileStream = new FileStream(empyt2, FileMode.Open, FileAccess.Read);
            var d0 = FileZIPService.ReadFully(fileStream);
            fileStream.Close();
            var d1 = FileZIPService.Unzip(d0);
            Encoding utf8WithoutBom = new UTF8Encoding(false);
            File.WriteAllText(empyt2 + ".txt", d1, utf8WithoutBom);
        }
    }
}
