﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Jaeger.Aplication.Validador.Beta.Domain;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public class CFDReaderComplemento {
        public CFDReaderComplemento() { 
        }

        public List<DocumentoComplementoPago> Reader(string complemento) {
            return CFDReaderComplemento.Reader(new StringReader(complemento));
        }

        public static List<DocumentoComplementoPago> Reader(StringReader complemento) {
            var pago = new List<DocumentoComplementoPago>();
            var reader = XmlReader.Create(complemento);
            try {
                while (reader.Read()) {
                    try {
                        if (reader.IsStartElement()) {
                            if (reader.Name != null) {
                                // solo para el tipo elemento
                                if (reader.NodeType == XmlNodeType.Element) {
                                    // nombre del nodo
                                    string currentName = reader.Name.ToLower();
                                    if (currentName == "pago20:pago" | currentName == "pago10:pago") {
                                        var pago1 = GetPagos(ref reader);
                                        pago.Add(pago1);
                                    } else if (currentName == "pago20:doctorelacionado" | currentName == "pago10:doctorelacionado") {
                                        if (pago[pago.Count-1].Relacionados.HasValue()) { pago[pago.Count - 1].Relacionados = new List<DocumentoComplementoPagoCFDIRelacionado>(); }
                                        pago[pago.Count-1].Relacionados.Add(CFDReaderComplemento.GetDocumentoPago(ref reader));
                                    } else {
                                        Console.WriteLine(string.Format("Reader: {0}", currentName));
                                    }
                                }
                            }
                        }
                    } catch (Exception exp) {
                        Console.WriteLine(string.Concat("CFDReader: ", exp.Message));
                        throw;
                    }
                }
            } catch (Exception exp) {
                Console.WriteLine(string.Concat("CFDReader: ", exp.Message));
                throw;
            }
            return pago;
        }

        private static DocumentoComplementoPago GetPagos(ref XmlReader _reader) {
            var pago = new DocumentoComplementoPago();
            for (int attInd = 0; attInd < _reader.AttributeCount; attInd++) {
                _reader.MoveToAttribute(attInd);
                switch (_reader.Name.ToLower()) {
                    case "fechapago": 
                        pago.FechaPagoP = CFDReader.ReaderDateTime(_reader.Value);
                        break;
                    case "monedap":
                        pago.MonedaP = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "monto":
                        pago.MontoP = Convert.ToDecimal(_reader.Value);
                        break;
                    case "rfcemisorctaord":
                        pago.RfcEmisorCtaOrd = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "rfcemisorctaben":
                        pago.RfcEmisorCtaBen = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "ctabeneficiario":
                        pago.CtaBeneficiario = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "ctaordenante":
                        pago.CtaOrdenante = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "numoperacion":
                        pago.NumOperacion = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "nombancoordext":
                        pago.NomBancoOrdExt = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "tipocadpago":
                        pago.TipoCadPago = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "certpago":
                        pago.CertPago = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "cadpago":
                        pago.CadPago = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "sellopago":
                        pago.SelloPago = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "formadepagop":
                        pago.FormaDePagoP = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "version":
                        pago.Version = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "objetoimpdr":
                    default:
                        Console.WriteLine(string.Format("{0} {1}", _reader.Name, _reader.Value));
                        break;
                }
            }
            _reader.MoveToElement();
            return pago;
        }

        private static DocumentoComplementoPagoCFDIRelacionado GetDocumentoPago(ref XmlReader _reader) {
            var pago = new DocumentoComplementoPagoCFDIRelacionado();
            for (int attInd = 0; attInd < _reader.AttributeCount; attInd++) {
                _reader.MoveToAttribute(attInd);
                switch (_reader.Name.ToLower()) {
                    case "folio":
                        pago.Folio = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "iddocumento":
                        pago.IdDocumento = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "imppagado":
                        pago.ImpPagado = Convert.ToDecimal(_reader.Value);
                        break;
                    case "impsaldoant":
                        pago.ImpSaldoAnt = Convert.ToDecimal(_reader.Value);
                        break;
                    case "impsaldoinsoluto":
                        pago.ImpSaldoInsoluto = Convert.ToDecimal(_reader.Value);
                        break;
                    case "metododepagodr":
                        pago.MetodoPago = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "monedadr":
                        pago.MonedaDR = CFDReader.ReaderString(_reader.Value);
                        break;
                    case "numparcialidad":
                        pago.NumParcialidad = Convert.ToInt32(_reader.Value);
                        break;
                    case "serie":
                        pago.Serie = CFDReader.ReaderString(_reader.Value);
                        break;
                    default:
                        Console.WriteLine(string.Format("{0} {1}", _reader.Name, _reader.Value));
                        break;
                }
            }
            _reader.MoveToElement();
            return pago;
        }
    }
}
