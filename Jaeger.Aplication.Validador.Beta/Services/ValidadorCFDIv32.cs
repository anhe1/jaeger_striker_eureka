﻿using System.Collections.Generic;
using Jaeger.Aplication.Validador.Beta.Domain;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public class ValidadorCFDIv32 : ValidadorFactory, IValidadorComprobante {
        protected CFDI.V32.Comprobante cfdi32;

        public override object Comprobante {
            get { return base.Comprobante; }
            set {
                base.Comprobante = value;
                this.cfdi32 = value as CFDI.V32.Comprobante;
            }
        }

        public override ComprobanteValidacionPropiedad Codificacion() {
            var bcfdi = System.Text.UTF8Encoding.UTF8.GetBytes(this.cfdi32.OriginalXmlString);
            return ValidacionCodificacionService.Verificar(bcfdi);
        }

        public override ComprobanteValidacionPropiedad LugarExpedicion() {
            var _response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.LugarExpedicion);
            if (!(string.IsNullOrEmpty(this.cfdi32.LugarExpedicion))) {
                _response.Valor = string.Format("Correcto ({0})", this.cfdi32.LugarExpedicion);
                _response.IsValido = true;
                return _response;
            }
            
            _response.Valor = "No disponible";
            _response.IsValido = false;
            return _response;
        }

        public override ComprobanteValidacionPropiedad UsoCFDI() {
            var response = new ComprobanteValidacionPropiedad();
            response.IsValido = true;
            response.Valor = "No aplica";
            response.Nombre = "Uso CFDI";
            return response;
        }

        public override List<ComprobanteValidacionPropiedad> SelloCFDI() {
            if (string.IsNullOrEmpty(this.cfdi32.certificado)) {
                return base.SelloCFDI();
            }
            var selloValido = new List<ComprobanteValidacionPropiedad>();
            var esValido = ValidacionSelloDigital.Validar(this.cfdi32.sello, this.cfdi32.certificado, this.cfdi32.CadenaOriginal, ValidacionSelloDigital.AlgoritmoSha.Sha1);
            var response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.SelloCFDI);
            response.IsValido = esValido;
            this.IsSelloCFDI = esValido;
            if (response.IsValido == false) { response.Valor = "No válido"; }
            selloValido.Add(response);
            selloValido.Add(this.GetCertificado(this.cfdi32.certificado, this.cfdi32.fecha));
            return selloValido;
        }

        public override ComprobanteValidacionPropiedad SelloSAT() {
            if (this.cfdi32.Complemento != null) {
                if (this.cfdi32.Complemento.TimbreFiscalDigital != null) {
                    return base.SelloSAT(this.cfdi32.Complemento.TimbreFiscalDigital);
                }
            }
            return base.SelloSAT();
        }

        public override ComprobanteValidacionPropiedad EstadoSAT() {
            if (this.cfdi32.Complemento != null) {
                if (this.cfdi32.Complemento.TimbreFiscalDigital != null) {
                    return this.EstadoSAT(this.cfdi32.Emisor.rfc, this.cfdi32.Receptor.rfc, this.cfdi32.total, this.cfdi32.Complemento.TimbreFiscalDigital.UUID);
                }
            }
            return base.EstadoSAT();
        }

        public override Dictionary<string, string> GetEmisor() {
            if (this.cfdi32 != null) {
                if (this.cfdi32.certificado != null) {
                    return this.GetEmisor(this.cfdi32.certificado);
                }
            }
            return null;
        }
    }
}
