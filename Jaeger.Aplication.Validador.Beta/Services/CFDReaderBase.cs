﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public class CFDReaderBase {
        public static bool IsValidXml(string stringXml) {
            var c = (new Regex("cfdi:Comprobante", RegexOptions.Compiled)).Matches(stringXml);
            if (0 < c.Count) {
                foreach (Match item in c) {
                    string str = item.ToString();
                    if (str.ToLower().Contains("cfdi:comprobante")) {
                        return true;
                    }
                }
            }
            return false;
        }

        public static string GetVersion(XmlDocument doc) {
            string value = "";
            XmlNode item = null;
            item = doc["cfdi:Comprobante"];
            if (item == null) {
                item = doc["Comprobante"];
                XmlNode itemOf = item.Attributes["Version"] ?? item.Attributes["version"];
                if (itemOf != null) {
                    value = itemOf.Value;
                }
            } else {
                XmlNode xmlNodes = item.Attributes["Version"] ?? item.Attributes["version"];
                if (xmlNodes != null) {
                    value = xmlNodes.Value;
                }
            }
            return value;
        }

        public static string ExtraeCFDI(string contenidoXml) {
            int posicion1 = contenidoXml.IndexOf("<cfdi:Comprobante");
            int posicion2 = contenidoXml.IndexOf("</cfdi:Comprobante>");
            return contenidoXml.Substring(posicion1, contenidoXml.Length - posicion1);
        }

        /// <summary>
        /// obtener tipo de comprobante de modo texto
        /// </summary>
        public static string GetTipoComprobante(string clave) {
            if (clave.ToUpper().StartsWith("I")) {
                return "Ingreso";
            } else if (clave.ToUpper().StartsWith("E")) {
                return "Egreso";
            } else if (clave.ToUpper().StartsWith("N")) {
                return "Nomina";
            } else if (clave.ToUpper().StartsWith("T")) {
                return "Traslado";
            } else if (clave.ToUpper().StartsWith("P")) {
                return "Pagos";
            }
            return clave;
        }

        /// <summary>
        /// obtener valor del objeto en modo texto
        /// </summary>
        public static string ReaderString(object valueObject) {
            if (valueObject != DBNull.Value) {
                if (valueObject != null) {
                    if (valueObject.ToString().Trim() != string.Empty) {
                        return valueObject.ToString();
                    }
                }
            }
            return "";
        }

        public static DateTime ReaderDateTime(object valueObject) {
            if (valueObject != DBNull.Value) {
                if (valueObject != null) {
                    if (valueObject.ToString().Trim() != string.Empty) {
                        return DateTime.Parse(valueObject.ToString());
                    }
                }
            }
            return DateTime.MinValue;
        }

        public static string FileToBase64(FileInfo archivo) {
            UTF8Encoding objUtf8WithoutBom = new UTF8Encoding(false);
            try {
                using (FileStream fileStream = new FileStream(archivo.FullName, FileMode.Open, FileAccess.Read)) {
                    using (StreamReader streamReader = new StreamReader(fileStream, objUtf8WithoutBom)) {
                        return Convert.ToBase64String(Encoding.UTF8.GetBytes(streamReader.ReadToEnd()));
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine("CFD Reader Base:FileToB64: " + ex.Message);
                return "";
            }
        }
    }
}
