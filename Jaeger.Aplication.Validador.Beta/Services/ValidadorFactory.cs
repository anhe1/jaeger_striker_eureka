﻿using System;
using System.Collections.Generic;
using Jaeger.Aplication.Validador.Beta.Domain;
using Jaeger.SAT.Consulta.Interfaces;
using Jaeger.SAT.Consulta.Services;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public class ValidadorFactory : ValidadorProvider, IValidadorComprobante {
        protected internal IStatus status = new Status();

        public ValidadorFactory() : base() {
        }

        public override object Comprobante { get; set; }

        public override bool IsCodificacion { get; set; }

        public override bool IsEsquemaValido { get; set; }

        public override bool IsSelloCFDI { get; set; }

        public override bool IsSelloSAT { get; set; }

        public override bool IsVigente { get; set; }

        public override ComprobanteValidacionPropiedad Esquema() {
            var _response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.Esquemas);
            _response.IsValido = true;
            this.IsEsquemaValido = _response.IsValido;
            return _response;
        }

        public override ComprobanteValidacionPropiedad Codificacion() {
            throw new NotImplementedException();
        }

        public override ComprobanteValidacionPropiedad EstadoSAT() {
            var _response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.EstadoSAT);
            _response.IsValido = false;
            _response.Valor = "No es posible obtener el estado SAT";
            return _response;
        }

        public override ComprobanteValidacionPropiedad FormaPago() {
            return FormaPagoService.Validar(this.Comprobante);
        }

        public override ComprobanteValidacionPropiedad LugarExpedicion() {
            var _response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.LugarExpedicion);
            _response.IsValido = true;
            _response.Valor = "No aplica";
            return _response;
        }

        public override ComprobanteValidacionPropiedad MetodoPago() {
            return MetodoPagoService.Validar(this.Comprobante);
        }

        public override List<ComprobanteValidacionPropiedad> SelloCFDI() {
            var response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.SelloCFDI);
            response.IsValido = false;
            response.Valor = "El comprobante no contiene el certificado del emisor";
            return new List<ComprobanteValidacionPropiedad> { response };
        }

        public override ComprobanteValidacionPropiedad SelloSAT() {
            var response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.SelloSAT);
            response.IsValido = false;
            response.Valor = "El comprobante no contiene el complemento timbre fiscal.";
            return response;
        }

        public override ComprobanteValidacionPropiedad UsoCFDI() {
            throw new NotImplementedException();
        }

        public ComprobanteValidacionPropiedad SelloSAT(object timbreFiscal) {
            this.IsSelloSAT = false;
            var response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.SelloSAT);
            if (timbreFiscal != null) {
                if (timbreFiscal.GetType() == typeof(CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital)) {
                    var esValido = ValidacionSelloDigitalSAT.Validar(timbreFiscal as CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital);
                    response.IsValido = esValido;
                    this.IsSelloSAT = esValido;
                } else if (timbreFiscal.GetType() == typeof(CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital)) {
                    var esValido = ValidacionSelloDigitalSAT.Validar(timbreFiscal as CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital);
                    response.IsValido = esValido;
                    this.IsSelloSAT = esValido;
                } else {
                    return this.SelloSAT();
                }
            } else {
                return this.SelloSAT();
            }
            return response;
        }

        /// <summary>
        /// obtener información del certificado del emisor del comprobante
        /// </summary>
        /// <param name="base64">certificado en base 64</param>
        /// <param name="fechaEmision">fecha de emision del comprobante</param>
        /// <returns>Propiedad de validacion</returns>
        public ComprobanteValidacionPropiedad GetCertificado(string base64, DateTime fechaEmision) {
            var certificado = new Crypto.Services.Certificate();
            certificado.CargarB64(base64);

            if (fechaEmision >= DateTime.Parse(certificado.ValidoDesde) & fechaEmision <= DateTime.Parse(certificado.ValidoHasta)) {
                return new ComprobanteValidacionPropiedad {
                    IsValido = true,
                    Nombre = "CFDI con CSD",
                    Valor = string.Format("Vigente al crearse {0} al {1}", certificado.ValidoDesde, certificado.ValidoHasta)
                };
            }
            var response = new ComprobanteValidacionPropiedad {
                IsValido = false,
                Nombre = "CFDI con CSD",
                Valor = string.Format("Al crearse {0} al {1}", certificado.ValidoDesde, certificado.ValidoHasta)
            };
            return response;
        }

        public ComprobanteValidacionPropiedad EstadoSAT(string rfcEmisor, string rfcReceptor, decimal total, string idDocumento) {
            var _response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.EstadoSAT);
            var q = this.status.Execute(
                SAT.Consulta.Request.Create()
                .WithEmisorRFC(rfcEmisor)
                .WithReceptorRFC(rfcReceptor)
                .WithTotal(total)
                .WithFolioFiscal(idDocumento)
                .Build());

            if (q != null) {
                // si contiene S Comprobante obtenido satisfactoriamente entonces es vigente
                if (q.CodigoEstatus.ToUpper().StartsWith("S")) {
                    _response.IsValido = true;
                    _response.Valor = q.Estado;
                } else if (q.CodigoEstatus.Contains("602")) {
                    _response.Valor = "No encontrado";
                    _response.IsValido = false;
                } else if (q.CodigoEstatus.Contains("601")) {
                    _response.Valor = "601";
                    _response.IsValido = false;
                }
            }
            return _response;
        }

        public override Dictionary<string, string> GetEmisor() {
            return null;
        }
    }
}
