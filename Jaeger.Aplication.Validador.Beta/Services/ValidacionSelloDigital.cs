﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Text;

namespace Jaeger.Aplication.Validador.Beta.Services {
    /// <summary>
    /// clase para validacion de sello digital del emisor de un CFDI
    /// </summary>
    public class ValidacionSelloDigital {
        public ValidacionSelloDigital() {

        }

        /// <summary>
        /// algoritmo utilizado
        /// </summary>
        public enum AlgoritmoSha {
            Sha1, 
            Sha256,
        }

        public static bool Verificar(string sello, string certificado, string cadenaOriginal, AlgoritmoSha tipoSHA) {
            return ValidacionSelloDigital.Validar(sello, certificado, cadenaOriginal, tipoSHA);
        }

        public static bool Validar(string sello, string certificado, string cadenaOriginal, AlgoritmoSha tipoSHA) {
            bool flag = false;
            try {
                ValidacionSelloDigital.IsValidSello(sello);
                ValidacionSelloDigital.IsValidCeritificado(certificado);
                ValidacionSelloDigital.IsValidCadenaOriginal(cadenaOriginal);

                byte[] bSello = Convert.FromBase64String(sello);
                byte[] bCadenaOriginal = Encoding.UTF8.GetBytes(cadenaOriginal);
                
                var x509Certificate2 = new X509Certificate2(Convert.FromBase64String(certificado));
                string xmlString = x509Certificate2.PublicKey.Key.ToXmlString(false);
                AsymmetricAlgorithm asymmetricAlgorithm = AsymmetricAlgorithm.Create();
                asymmetricAlgorithm.FromXmlString(xmlString);
                RSACryptoServiceProvider rSACryptoServiceProvider = (RSACryptoServiceProvider)asymmetricAlgorithm;
                
                if (tipoSHA == AlgoritmoSha.Sha1) {
                    flag = ValidacionSelloDigital.VerifyHashSHA1(bCadenaOriginal, bSello, rSACryptoServiceProvider);
                } else if (tipoSHA == AlgoritmoSha.Sha256) {
                    flag = ValidacionSelloDigital.VerifyHashSHA256(bCadenaOriginal, bSello, rSACryptoServiceProvider);
                }
            } catch (Exception exp) {
                throw new Exception(exp.Message);
            }
            return flag;
        }

        private static bool VerifyHashSHA1(byte[] bCadenaOriginal, byte[] bSello, RSACryptoServiceProvider rsa) {
            bool flag;
            try {
                byte[] numArray = (new SHA1CryptoServiceProvider()).ComputeHash(bCadenaOriginal);
                flag = rsa.VerifyHash(numArray, CryptoConfig.MapNameToOID("SHA1"), bSello);
            } catch (Exception exp) {
                Console.WriteLine(exp.Message);
                throw;
            }
            return flag;
        }

        private static bool VerifyHashSHA256(byte[] bCadenaOriginal, byte[] bSello, RSACryptoServiceProvider rsa) {
            bool flag;
            try {
                byte[] numArray = (new SHA256CryptoServiceProvider()).ComputeHash(bCadenaOriginal);
                flag = rsa.VerifyHash(numArray, CryptoConfig.MapNameToOID("SHA256"), bSello);
            } catch (Exception exp) {
                Console.WriteLine(exp.Message);
                throw;
            }
            return flag;
        }

        private static void IsValidCadenaOriginal(string cadenaOriginal) {
            if ((string.IsNullOrEmpty(cadenaOriginal) && cadenaOriginal.Length > 0)) {
                throw new ArgumentNullException("Cadena original no válida.");
            }
        }

        private static void IsValidCeritificado(string certificado) {
            if ((string.IsNullOrEmpty(certificado) && certificado.Length > 0)) {
                throw new ArgumentNullException("Certificado no declarado.");
            }
        }

        private static void IsValidSello(string sello) {
            if ((string.IsNullOrEmpty(sello) && sello.Length > 0)) {
                throw new ArgumentNullException("Sello no declarado.");
            }
        }
    }
}
