﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Jaeger.Aplication.Validador.Beta.Domain;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public class CFDReader : CFDReaderBase, ICFDReader {
        #region declaraciones
        protected internal DocumentoFiscal _DocumentoFiscal;
        #endregion

        #region reader
        public CFDReader() { 
        }

        public CFDReader(string stringXml) {
            this._DocumentoFiscal = CFDReader.Reader(stringXml);
        }

        public DocumentoFiscal GetDocumento {
            get { return this._DocumentoFiscal; }
        }

        public DocumentoFiscal Reader(FileInfo fileInfo) {
            if (fileInfo.Exists) {
                var d1 = new StringReader(Util.Services.FileService.ReadFileText(fileInfo));
                if (d1 != null) {
                    return CFDReader.Reader(d1);
                }
            }
            return null;
        }

        #region metodos estaticos
        public static DocumentoFiscal Reader(string stringXml) {
            return Reader(new StringReader(stringXml));
        }

        public static DocumentoFiscal Reader(byte[] xmlBytes) {
            var d0 = new MemoryStream(xmlBytes);
            var d1 = new StreamReader(d0);
            return Reader(new StringReader(d1.ReadToEnd()));
        }

        public static DocumentoFiscal ReaderB64(string xmlBase64) {
            var encodedString = Convert.FromBase64String(xmlBase64);
            return Reader(encodedString);
        }

        public static DocumentoFiscal Reader(StringReader stringXml) {
            var reader = XmlReader.Create(stringXml);
            var response = new DocumentoFiscal();
            try {
                while (reader.Read()) {
                    try {
                        if (reader.IsStartElement()) {
                            if (reader.Name != null) {
                                // solo para el tipo elemento
                                if (reader.NodeType == XmlNodeType.Element) {
                                    // nombre del nodo
                                    string currentName = reader.Name.ToLower();
                                    if (currentName == "cfdi:comprobante" | currentName == "comprobante") {
                                        GetComprobante(ref response, ref reader);
                                    } else if (currentName == "cfdi:informacionglobal" | currentName == "informacionglobal") {

                                    } else if (currentName == "cfdi:acuentaterceros") {

                                    } else if (currentName == "cfdi:emisor" | currentName == "emisor") {
                                        GetEmisor(ref response, ref reader);
                                    } else if (currentName == "cfdi:receptor" | currentName == "receptor") {
                                        GetReceptor(ref response, ref reader);
                                    } else if (currentName == "cfdi:conceptos" | currentName == "conceptos") {
                                        if (response.HasValue()) response.Conceptos = new System.Collections.Generic.List<DocumentoFiscalConcepto>();
                                        response.Conceptos.Add(GetConcepto(ref reader));
                                    } else if (currentName == "cfdi:traslado" | currentName == "traslado") {

                                    } else if (currentName == "cfdi:retenido" | currentName == "retenido") {

                                    } else if (currentName == "cfdi:impuestos" | currentName == "impuestos") {
                                        GetImpuestos(ref response, reader.ReadOuterXml());
                                    } else if (currentName == "tfd:timbrefiscaldigital" | currentName == "timbrefiscaldigital") {
                                        GetTimbreFiscal(ref response, ref reader);
                                    } else if (currentName == "pago20:pago" | currentName == "pago10:pago") {
                                        var pago1 = CFDReaderComplemento.Reader(new StringReader(reader.ReadOuterXml()));
                                        response.Pagos.AddRange(pago1);
                                        reader.MoveToElement();
                                    } else if (currentName == "cfdi:cfdirelacionados" | currentName == "cfdirelacionados") {
                                        if (response.CFDIRelacionado.HasValue()) { response.CFDIRelacionado = new System.Collections.Generic.List<DocumentoCFDIRelacionados>(); }
                                        response.CFDIRelacionado = CFDReader.GetCFDIRelacionado(new StringReader(reader.ReadOuterXml()));
                                        reader.MoveToElement();
                                    } else if (currentName == "cfdi:cfdirelacionado") {
                                        
                                    } else if (currentName == "nomina12:nomina" | currentName == "nomina") {

                                    } else if (currentName == "implocal:impuestoslocales" | currentName == "impuestoslocales") {

                                    } else if (currentName == "valesdedespensa:valesdedespensa") {

                                    } else if (currentName == "aerolineas:aerolineas") {

                                    }
                                }
                            }
                        }
                    } catch (Exception exp) {
                        Console.WriteLine(string.Concat("CFDReader: ", exp.Message));
                        return CFDReaderSerialize.GetComprobante(stringXml);
                    }
                }
            } catch (Exception exp) {
                Console.WriteLine(string.Concat("CFDReader: ", exp.Message));
                return CFDReaderSerialize.GetComprobante(stringXml);
            }
            return response;
        }

        /// <summary>
        /// obtener datos generales del comprobante
        /// </summary>
        public static void GetComprobante(ref DocumentoFiscal _response, ref XmlReader _reader) {
            for (int attInd = 0; attInd < _reader.AttributeCount; attInd++) {
                _reader.MoveToAttribute(attInd);
                switch (_reader.Name.ToLower()) {
                    case "version":
                        _response.Version = ReaderString(_reader.Value);
                        break;
                    case "tipodecomprobante":
                        _response.TipoComprobante = GetTipoComprobante(ReaderString(_reader.Value));
                        break;
                    case "folio":
                        _response.Folio = ReaderString(_reader.Value);
                        break;
                    case "serie":
                        _response.Serie = ReaderString(_reader.Value);
                        break;
                    case "metodopago":
                    case "metododepago":
                        _response.MetodoPago = ReaderString(_reader.Value);
                        break;
                    case "formapago":
                        _response.FormaPago = ReaderString(_reader.Value);
                        break;
                    case "moneda":
                        _response.Moneda = ReaderString(_reader.Value);
                        break;
                    case "fecha":
                        _response.FechaEmision = ReaderDateTime(_reader.Value);
                        break;
                    case "subtotal":
                        _response.SubTotal = Convert.ToDecimal(_reader.Value);
                        break;
                    case "descuento":
                        _response.Descuento = Convert.ToDecimal(_reader.Value);
                        break;
                    case "total":
                        _response.Total = Convert.ToDecimal(_reader.Value);
                        break;
                    case "lugarexpedicion":
                        _response.LugarExpedicion = ReaderString(_reader.Value);
                        break;
                    case "condicionesdepago": break;
                    case "confirmacion": break;
                    case "motivodescuento":
                    case "numctapago":
                    case "foliofiscalorig":
                    case "seriefoliofiscalorig":
                    case "fechafoliofiscalorig":
                    case "montofoliofiscalorig":
                    case "Certificado":
                    case "nocertificado":
                    case "sello":
                    default:
                        Console.WriteLine(string.Format("{0} {1}", _reader.Name, _reader.Value));
                        break;
                }
            }
            _reader.MoveToElement();
        }

        /// <summary>
        /// obtener datos del nodo emisor
        /// </summary>
        public static void GetEmisor(ref DocumentoFiscal _response, ref XmlReader _reader) {
            for (int attInd = 0; attInd < _reader.AttributeCount; attInd++) {
                _reader.MoveToAttribute(attInd);
                switch (_reader.Name.ToLower()) {
                    case "nombre":
                        _response.EmisorNombre = ReaderString(_reader.Value);
                        break;
                    case "rfc":
                        _response.EmisorRFC = ReaderString(_reader.Value);
                        break;
                    case "regimenfiscal":
                        _response.EmisorRegimenFiscal = ReaderString(_reader.Value);
                        break;
                    default:
                        Console.WriteLine(string.Format("{0} {1}", _reader.Name, _reader.Value));
                        break;
                }
            }
            _reader.MoveToElement();
        }

        /// <summary>
        /// obtener datos del nodo receptor
        /// </summary>
        public static void GetReceptor(ref DocumentoFiscal _response, ref XmlReader _reader) {
            for (int attInd = 0; attInd < _reader.AttributeCount; attInd++) {
                _reader.MoveToAttribute(attInd);
                switch (_reader.Name.ToLower()) {
                    case "nombre":
                        _response.ReceptorNombre = ReaderString(_reader.Value);
                        break;
                    case "rfc":
                        _response.ReceptorRFC = ReaderString(_reader.Value);
                        break;
                    case "usocfdi":
                        _response.UsoCFDI = ReaderString(_reader.Value);
                        break;
                    case "domiciliofiscalreceptor":
                        _response.ReceptorDomicilioFiscal = ReaderString(_reader.Value);
                        break;
                    case "regimenfiscalreceptor":
                        _response.ReceptorRegimenFiscal = ReaderString(_reader.Value);
                        break;
                    default:
                        Console.WriteLine(string.Format("{0} {1}", _reader.Name, _reader.Value));
                        break;
                }
            }
            _reader.MoveToElement();
        }

        /// <summary>
        /// obtener informacion del nodo timbre fiscal para versiones 3.2, 3.3 y 4.0
        /// </summary>
        public static void GetTimbreFiscal(ref DocumentoFiscal _response, ref XmlReader _reader) {
            for (int attInd = 0; attInd < _reader.AttributeCount; attInd++) {
                _reader.MoveToAttribute(attInd);
                switch (_reader.Name.ToLower()) {
                    case "version": break;
                    case "uuid":
                        _response.IdDocumento = ReaderString(_reader.Value);
                        break;
                    case "fechatimbrado":
                        _response.FechaTimbre = ReaderDateTime(_reader.Value);
                        break;
                    case "rfcprovcertif":
                        _response.RFCProvCertif = ReaderString(_reader.Value);
                        break;
                    case "nocertificadosat":
                        _response.NoCertificadoProvCertif = ReaderString(_reader.Value);
                        break;
                    case "sellocfd": break;
                    case "sellosat": break;
                    case "leyenda": break;
                    default:
                        Console.WriteLine(string.Format("{0} {1}", _reader.Name, _reader.Value));
                        break;
                }
            }
            _reader.MoveToElement();
        }

        public static DocumentoFiscalConcepto GetConcepto(ref XmlReader _reader) {
            var concepto = new DocumentoFiscalConcepto();
            for (int attInd = 0; attInd < _reader.AttributeCount; attInd++) {
                _reader.MoveToAttribute(attInd);
                switch (_reader.Name.ToLower()) {
                    case "noidentificacion":
                        concepto.NoIdentificacion = _reader.Value.ObjToString();
                        break;
                    case "claveprodserv":
                        concepto.ClaveProdServ = _reader.Value.ObjToString();
                        break;
                    case "cantidad":
                        concepto.Cantidad = _reader.Value.ObjToDecimal();
                        break;
                    case "claveunidad":
                        concepto.ClaveUnidad = _reader.Value.ObjToString();
                        break;
                    case "unidad":
                        concepto.Unidad = _reader.Value.ObjToString();
                        break;
                    case "descripcion":
                        concepto.Descripcion = _reader.Value.ObjToString();
                        break;
                    case "valorunitario":
                        concepto.ValorUnitario = _reader.Value.ObjToDecimal();
                        break;
                    case "importe":
                        concepto.Importe = _reader.Value.ObjToDecimal();
                        break;
                    case "descuento":
                        concepto.Descuento = _reader.Value.ObjToDecimal();
                        break;
                    case "objetoimp":
                        concepto.ObjetoImp = _reader.Value.ObjToString();
                        break;
                    default:
                        Console.WriteLine(string.Format("{0} {1}", _reader.Name, _reader.Value));
                        break;
                }
            }
            _reader.MoveToElement();
            return concepto;
        }

        public static void GetImpuestos(ref DocumentoFiscal objeto, string inXml) {
            // 001 - ISR
            // 002 - IVA
            // 003 - IEPS
            objeto.RetencionIEPS = 0;
            objeto.RetencionISR = 0;
            objeto.RetencionIVA = 0;
            objeto.TrasladoIVA = 0;
            objeto.TrasladoIEPS = 0;

            if ((inXml.Contains("TotalImpuestosTrasladados") || inXml.Contains("TotalImpuestosRetenidos") ? true : false)) {
                XmlDocument xmlD = new XmlDocument();
                IEnumerator enumerator = null;
                xmlD.LoadXml(inXml);
                try {
                    enumerator = xmlD.SelectNodes("//*").GetEnumerator();
                    while (enumerator.MoveNext()) {
                        XmlElement current = (XmlElement)enumerator.Current;
                        try {
                            if (current.Name.ToLower() != "cfdi:traslado") {
                                if (current.Name.ToLower() == "cfdi:retencion") {
                                    if (current.GetAttribute("Impuesto").ToLower() == "002") {
                                        objeto.RetencionIVA = objeto.RetencionIVA + Convert.ToDecimal(current.GetAttribute("Importe"));
                                    } else if (current.GetAttribute("Impuesto").ToLower() == "001") {
                                        objeto.RetencionISR = objeto.RetencionISR + Convert.ToDecimal(current.GetAttribute("Importe"));
                                    } else if (current.GetAttribute("Impuesto").ToLower() == "003") {
                                        objeto.RetencionIEPS = objeto.RetencionIEPS + Convert.ToDecimal(current.GetAttribute("Importe"));
                                    }
                                }
                            } else if (current.GetAttribute("Impuesto").ToLower() == "002") {
                                objeto.TrasladoIVA = objeto.TrasladoIVA + Convert.ToDecimal(current.GetAttribute("Importe"));
                            } else if (current.GetAttribute("Impuesto").ToLower() == "001") {
                                objeto.TrasladoIEPS = objeto.TrasladoIEPS + Convert.ToDecimal(current.GetAttribute("Importe"));
                            }
                        } catch (Exception ex) {
                            Console.WriteLine(string.Concat("ReaderImpuesto: ", ex.Message));
                        }
                    }
                } catch (Exception ex) {
                    Console.WriteLine(string.Concat("ReaderImpuesto: ", ex.Message));
                }
            }
        }

        public static List<DocumentoCFDIRelacionados> GetCFDIRelacionado(StringReader complemento) {
            var pago = new List<DocumentoCFDIRelacionados>();
            var reader = XmlReader.Create(complemento);
            try {
                while (reader.Read()) {
                    try {
                        if (reader.IsStartElement()) {
                            if (reader.Name != null) {
                                // solo para el tipo elemento
                                if (reader.NodeType == XmlNodeType.Element) {
                                    // nombre del nodo
                                    string currentName = reader.Name.ToLower();
                                    if (currentName == "cfdi:cfdirelacionados" | currentName == "cfdirelacionados") {
                                        var pago1 = GetCFDIRelacionado(ref reader);
                                        pago.Add(pago1);
                                    } else if (currentName == "cfdi:cfdirelacionado" | currentName == "cfdirelacionado") {
                                        if (pago[pago.Count-1].CFDIRelacionados.HasValue()) { pago[pago.Count - 1].CFDIRelacionados = new List<DocumentoCFDIRelacionado>(); }
                                        pago[pago.Count-1].CFDIRelacionados = GetCFDIRelacionados(ref reader);
                                    } else {
                                        Console.WriteLine(string.Format("Reader: {0}", currentName));
                                    }
                                }
                            }
                        }
                    } catch (Exception exp) {
                        Console.WriteLine(string.Concat("CFDReader: ", exp.Message));
                        throw;
                    }
                }
            } catch (Exception exp) {
                Console.WriteLine(string.Concat("CFDReader: ", exp.Message));
                throw;
            }
            reader.MoveToElement();
            return pago;
        }

        public static DocumentoCFDIRelacionados GetCFDIRelacionado(ref XmlReader _reader) {
            var response = new DocumentoCFDIRelacionados();
            for (int attInd = 0; attInd < _reader.AttributeCount; attInd++) {
                _reader.MoveToAttribute(attInd);
                switch (_reader.Name.ToLower()) {
                    case "tiporelacion":
                        response.TipoRelacion = CFDReader.ReaderString(_reader.Value);
                        break;
                    default:
                        Console.WriteLine(string.Format("{0} {1}", _reader.Name, _reader.Value));
                        break;
                }
            }
            return response;
        }

        public static List<DocumentoCFDIRelacionado> GetCFDIRelacionados(ref XmlReader _reader) {
            var response = new List<DocumentoCFDIRelacionado>();
            for (int attInd = 0; attInd < _reader.AttributeCount; attInd++) {
                _reader.MoveToAttribute(attInd);
                switch (_reader.Name.ToLower()) {
                    case "uuid":
                        response.Add(new DocumentoCFDIRelacionado(CFDReader.ReaderString(_reader.Value)));
                        break;
                    default:
                        Console.WriteLine(string.Format("{0} {1}", _reader.Name, _reader.Value));
                        break;
                }
            }
            return response;
        }
        #endregion
        #endregion
    }
}
