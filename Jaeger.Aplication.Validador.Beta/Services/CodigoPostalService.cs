﻿using Jaeger.Aplication.Validador.Beta.Domain;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public static class CodigoPostalService {
        public static CodigoPostalCatalogo Catalogo = new CodigoPostalCatalogo();

        static CodigoPostalService() {
            Catalogo.FileName = @"C:\AdminCFD\Catalogos\CatalogoCodigoPostal.json";
            Catalogo.Load();
        }

        public static ComprobanteValidacionPropiedad Validar(string cp) {
            var response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.LugarExpedicion);
            if (!string.IsNullOrEmpty(cp)) {
                var codigo = CodigoPostalService.Catalogo.Search(cp);
                if (codigo != null) {
                    response.Valor = string.Concat("Correcto (", cp, ": ", codigo.Estado, ")");
                } else {
                    response.Valor = string.Concat("No válido (", cp, ": ", codigo.Estado, ")");
                    response.IsValido = false;
                }
                return response;
            }
            response.IsValido = false;
            response.Valor = "No disponible";
            return response;
        }
    }
}
