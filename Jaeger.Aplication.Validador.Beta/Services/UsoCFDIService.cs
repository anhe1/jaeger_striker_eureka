﻿using Jaeger.Aplication.Validador.Beta.Domain;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public static class UsoCFDIService {
        public static UsoCFDICatalogo Catalogo = new UsoCFDICatalogo();

        static UsoCFDIService() {
            UsoCFDIService.Catalogo.Load();
        }

        public static ComprobanteValidacionPropiedad Validar(string usoCFDI) {
            var response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.UsoCFDI);
            var clave = UsoCFDIService.Catalogo.Search(usoCFDI);
            if (clave != null) {
                response.Valor = string.Format("Correcto ({0} {1})", clave.Clave, clave.Descripcion);
                response.IsValido = true;
                return response;
            }
            response.IsValido = false;
            response.Valor = "No disponible";
            return response;
        }
    }
}
