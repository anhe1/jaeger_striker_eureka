﻿
using System;
using System.IO;
using Jaeger.Aplication.Validador.Beta.Contracts;

namespace Jaeger.Aplication.Validador.Beta.Services {

    public class FileStorageService : IFileStorageService {
        protected readonly string PathRoot = @"C:\CFDI\Jaeger.Repositorio\";

        public FileStorageService() { }

        public void BackUp(Domain.DocumentoFiscal documentoFiscal) {
            if (documentoFiscal != null) {
                var year = documentoFiscal.FechaEmision.Value.Year.ToString();
                var month = documentoFiscal.FechaEmision.Value.Month.ToString();
                var day = documentoFiscal.FechaEmision.Value.Month.ToString();
                var rfc = documentoFiscal.Tipo == "Emitidos" ? documentoFiscal.EmisorRFC.ToUpper() : documentoFiscal.ReceptorRFC.ToUpper();
                var carpeta = Path.Combine(this.PathRoot, rfc, documentoFiscal.Tipo, year, month, day);
                if (!Directory.Exists(carpeta)) {
                    Directory.CreateDirectory(carpeta);
                }
                var destino = Path.Combine(carpeta, documentoFiscal.KeyName.ToUpper() + ".xml");
                if (!File.Exists(destino)) {
                    Util.Services.FileService.WriteFileB64(documentoFiscal.XmlContentB64, destino);
                }
            }
        }

        public bool Exists(string file) {
            return File.Exists(file);
        }

        public bool MoveFile(string source, string destination) {
            try {
                if (!this.Exists(source)) {
                    File.Move(source, destination);
                }
                return true;
            } catch (Exception exception1) {
                Console.WriteLine(exception1.Message);
            }
            return false;
        }
    }
}