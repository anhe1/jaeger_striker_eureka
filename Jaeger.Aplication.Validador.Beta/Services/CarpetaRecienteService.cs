﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Jaeger.Aplication.Validador.Beta.Domain;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public class CarpetaRecienteService {
        private readonly string localFileName = @"C:\AdminCFD\Temporal\recientesBeta.json";
        private List<CarpetaModel> items;

        public CarpetaRecienteService() {
            this.items = new List<CarpetaModel>();
        }

        public List<CarpetaModel> Items {
            get {
                return this.items;
            }
            set {
                this.items = value;
            }
        }

        public void Add(string carpeta) {
            this.Add(new CarpetaModel(carpeta));
        }

        public void Add(CarpetaModel newItem) {
            try {
                var e = this.items.Find((CarpetaModel b) => b.Carpeta == newItem.Carpeta);
                if (e == null)
                    this.items.Add(newItem);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        public void Reset() {
            this.items = new List<CarpetaModel>();
            this.Save();
        }

        public void Save() {
            try {
                System.IO.File.WriteAllText(this.localFileName, JsonConvert.SerializeObject(this.items));
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        public void Load() {
            if (System.IO.File.Exists(this.localFileName)) {
                var contenido = System.IO.File.ReadAllText(this.localFileName);
                try {
                    this.items = JsonConvert.DeserializeObject<List<CarpetaModel>>(contenido);
                } catch (Exception) {
                    this.items = new List<CarpetaModel>();
                }
            } else {
                this.items = new List<CarpetaModel>();
            }
        }
    }
}
