﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Collections.Generic;
using Jaeger.CFDI.Services;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public class EsquemaService {
        private readonly XmlReaderSettings xmlReaderSetting32;
        private readonly XmlReaderSettings xmlReaderSetting33;
        private readonly XmlReaderSettings xmlReaderSetting40;


        public EsquemaService() {
            xmlReaderSetting32 = new XmlReaderSettings();
            xmlReaderSetting33 = new XmlReaderSettings();
            xmlReaderSetting40 = new XmlReaderSettings();
        }

        public void Init() {
            xmlReaderSetting32.Schemas.Add("http://www.sat.gob.mx/cfd/3", new GetXsd("cfdv32.xsd").XmlReader);
            xmlReaderSetting33.Schemas.Add("http://www.sat.gob.mx/cfd/3", new GetXsd("cfdv33.xsd").XmlReader);
            xmlReaderSetting40.Schemas.Add("http://www.sat.gob.mx/cfd/4", new GetXsd("cfdv40.xsd").XmlReader);
        }

        public bool Procesar(string xmlString, XmlReaderSettings settings) {
            bool flag = false;
            try {
                settings.Schemas.Compile();
                using (XmlReader xmlReader = XmlReader.Create(new StringReader(xmlString), settings)) {
                    while (xmlReader.Read()) { }
                }
                flag = true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return flag;
        }

        public bool Schema(object objeto) {
            if (objeto.GetType() == typeof(CFDI.V32.Comprobante)) {
                return this.Schema((CFDI.V32.Comprobante)objeto);
            }
            return false;
        }

        public bool Schema(CFDI.V32.Comprobante objeto) {
            return Procesar(objeto.OriginalXmlString, xmlReaderSetting32);
        }

        public bool Schema(CFDI.V33.Comprobante objeto) {
            return Procesar(objeto.OriginalXmlString, xmlReaderSetting33);
        }

        public bool Schema(CFDI.V40.Comprobante objeto) {
            return Procesar(objeto.OriginalXmlString, xmlReaderSetting40);
        }

        public bool ValidarEsquema(CFDI.V32.Comprobante comprobante) {
            bool flag = false;
            try {
                XmlReaderSettings xmlReaderSetting = new XmlReaderSettings();
                EsquemaService validateSchema = this;
                xmlReaderSetting.ValidationEventHandler += new ValidationEventHandler(validateSchema.ValidationEventHandler);
                xmlReaderSetting.ValidationType = ValidationType.Schema;
                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/cfd/3")) {
                    if (comprobante.version != "3.0") {
                        xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/cfd/3", new GetXsd("cfdv32.xsd").XmlReader);
                    } else {
                        xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/cfd/3", new GetXsd("cfdv3.xsd").XmlReader);
                    }
                    Console.WriteLine("El atributo schemaLocation no existe. Se agrego para la validación.");
                }
                if (comprobante.Complemento == null || comprobante.Complemento.Any == null ? false : true) {
                    List<XmlElement> any = comprobante.Complemento.Any;
                    int count = checked(any.Count - 1);
                    for (int i = 0; i < any.Count; i++) {

                        string lower = any[i].Name.ToLower();
                        string str = lower;
                        if (lower != null) {
                            string str1 = str;
                            if (str1 == "detallista:detallista") {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/detallista")) {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/detallista", new GetXsd("detallista.xsd").XmlReader);
                                }
                            } else if (str1 == "divisas:divisas") {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/divisas")) {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/divisas", new GetXsd("Divisas.xsd").XmlReader);
                                }
                            } else if (str1 == "implocal:impuestoslocales") {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/implocal")) {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/implocal", new GetXsd("implocal.xsd").XmlReader);
                                }
                            } else if (str1 == "ecc:estadodecuentacombustible") {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/ecc")) {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/ecc", new GetXsd("ecc.xsd").XmlReader);
                                }
                            } else if (str1 == "ecb:estadodecuentabancario") {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/ecb")) {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/ecb", new GetXsd("ecb.xsd").XmlReader);
                                }
                            } else if (str1 == "tfd:timbrefiscaldigital") {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/TimbreFiscalDigital")) {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/TimbreFiscalDigital", new GetXsd("TimbreFiscalDigital.xsd").XmlReader);
                                }
                            } else if (str1 == "donat:donatarias") {
                                if (comprobante.version != "3.0") {
                                    if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/donat")) {
                                        xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/donat", new GetXsd("donat11.xsd").XmlReader);
                                    }
                                } else if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/donat")) {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/donat", new GetXsd("donat.xsd").XmlReader);
                                }
                            } else if (str1 == "leyendasfisc:leyendasfiscales") {
                                if (comprobante.version != "3.2" || xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/leyendasFiscales")) {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/leyendasFiscales", new GetXsd("leyendasFisc.xsd").XmlReader);
                                }
                            } else if (str1 == "pfic:pfintegrantecoordinado") {
                                if (comprobante.version != "3.2" || xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/pfic")) {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/pfic", new GetXsd("pfic.xsd").XmlReader);
                                }
                            } else if (str1 == "tpe:turistapasajeroextranjero") {
                                if (comprobante.version != "3.2" || xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/TuristaPasajeroExtranjero")) {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/TuristaPasajeroExtranjero", new GetXsd("TuristaPasajeroExtranjero.xsd").XmlReader);
                                }
                            } else if (str1 == "nomina:nomina") {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/nomina")) {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/nomina", new GetXsd("nomina11.xsd").XmlReader);
                                }
                            } else if (str1 == "aerolineas:aerolineas") {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/aerolineas")) {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/aerolineas", new GetXsd("aerolineas.xsd").XmlReader);
                                }
                            } else if (str1 == "consumodecombustibles:consumodecombustibles") {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/consumodecombustibles")) {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/consumodecombustibles", new GetXsd("consumodecombustibles.xsd").XmlReader);
                                }
                            } else if (str1 == "valesdedespensa:valesdedespensa") {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/valesdedespensa")) {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/valesdedespensa", new GetXsd("valesdedespensa.xsd").XmlReader);
                                }
                            } else if (str1 == "notariospublicos:notariospublicos") {
                                if (!xmlReaderSetting.Schemas.Contains("http://www.sat.gob.mx/notariospublicos")) {
                                    xmlReaderSetting.Schemas.Add("http://www.sat.gob.mx/notariospublicos", new GetXsd("notariospublicos.xsd").XmlReader);
                                }
                            }
                        }
                    }
                }
                xmlReaderSetting.Schemas.Compile();
                EsquemaService validateSchema1 = this;
                xmlReaderSetting.ValidationEventHandler += new ValidationEventHandler(validateSchema1.ValidationEventHandler);
                using (XmlReader xmlReader = XmlReader.Create(new StringReader(comprobante.OriginalXmlString), xmlReaderSetting)) {
                    while (xmlReader.Read()) {
                    }
                }
                flag = true;
            } catch (Exception ex) {
                //this.LiErrores.Add(string.Concat("|", ex.Message));
                Console.WriteLine(ex.Message);
            }
            return flag;
        }

        private void ValidationEventHandler(object sender, ValidationEventArgs validationEventArgs) {
            switch (validationEventArgs.Severity) {
                case XmlSeverityType.Error: {
                        Console.WriteLine(validationEventArgs.Message);
                        Console.WriteLine(string.Concat("Error: ", validationEventArgs.Message));
                        break;
                    }
                case XmlSeverityType.Warning: {
                        Console.WriteLine(validationEventArgs.Message);
                        Console.WriteLine("Warning: ", validationEventArgs.Message);
                        break;
                    }
            }
        }
    }

    public class EsquemaReader {
        public static XmlReaderSettings xmlReaderSetting32;
        public static XmlReaderSettings xmlReaderSetting33;
        public static XmlReaderSettings xmlReaderSetting40;


        public EsquemaReader() {
            xmlReaderSetting32 = new XmlReaderSettings();
            xmlReaderSetting33 = new XmlReaderSettings();
            xmlReaderSetting40 = new XmlReaderSettings();
        }

        public void Init() {
            xmlReaderSetting32.ValidationType = ValidationType.Schema;
            xmlReaderSetting32.Schemas.Add("http://www.sat.gob.mx/cfd/3", new GetXsd("cfdv32.xsd").XmlReader);
            xmlReaderSetting33.Schemas.Add("http://www.sat.gob.mx/cfd/3", new GetXsd("cfdv33.xsd").XmlReader);
            xmlReaderSetting40.Schemas.Add("http://www.sat.gob.mx/cfd/4", new GetXsd("cfdv40.xsd").XmlReader);
        }

        public static void Load() {
            xmlReaderSetting32 = new XmlReaderSettings();
            xmlReaderSetting33 = new XmlReaderSettings();
            xmlReaderSetting40 = new XmlReaderSettings();
            xmlReaderSetting32.ValidationType = ValidationType.Schema;
            EsquemaReader.xmlReaderSetting32.Schemas.Add("http://www.sat.gob.mx/cfd/3", @"D:\bitbucket\jaeger_edita_desktop\Jaeger.CFDI\xsd\cfdv32.xsd");
            var d = new GetXsd("cfdv33.xsd");
            xmlReaderSetting33.Schemas.Add("http://www.sat.gob.mx/cfd/3", d.XmlReader);
            xmlReaderSetting40.Schemas.Add("http://www.sat.gob.mx/cfd/4", new GetXsd("cfdv40.xsd").XmlReader);
        }

        public static bool Procesar(string xmlString, XmlReaderSettings settings) {
            bool flag = false;
            try {
                settings.Schemas.Compile();
                using (XmlReader xmlReader = XmlReader.Create(new StringReader(xmlString), settings)) {
                    while (xmlReader.Read()) { }
                }
                flag = true;
            } catch (Exception ex) {
                Console.WriteLine("Esquemas: " + ex.Message);
            }
            return flag;
        }

        public static bool Schema(object objeto) {
            if (objeto.GetType() == typeof(CFDI.V32.Comprobante)) {
                return Procesar((objeto as CFDI.V32.Comprobante).OriginalXmlString, EsquemaReader.xmlReaderSetting32);
            } else if (objeto.GetType() == typeof(CFDI.V33.Comprobante)) {
                return Procesar((objeto as CFDI.V33.Comprobante).OriginalXmlString, EsquemaReader.xmlReaderSetting33);
            } else if (objeto.GetType() == typeof(CFDI.V32.Comprobante)) {
                return Procesar((objeto as CFDI.V40.Comprobante).OriginalXmlString, EsquemaReader.xmlReaderSetting40);
            }
            return false;
        }

        public bool Schema(CFDI.V32.Comprobante objeto) {
            return Procesar(objeto.OriginalXmlString, xmlReaderSetting32);
        }

        public bool Schema(CFDI.V33.Comprobante objeto) {
            return Procesar(objeto.OriginalXmlString, xmlReaderSetting33);
        }

        public bool Schema(CFDI.V40.Comprobante objeto) {
            return Procesar(objeto.OriginalXmlString, xmlReaderSetting40);
        }
    }
}
