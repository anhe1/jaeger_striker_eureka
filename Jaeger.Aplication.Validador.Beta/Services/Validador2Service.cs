﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Jaeger.Aplication.Validador.Beta.Contracts;
using Jaeger.Aplication.Validador.Beta.Domain;
using Jaeger.Util.Services;
using Jaeger.ValidaSAT.Service.SAT;
using static Jaeger.Aplication.Validador.Beta.Services.Administrador;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public class Validador2Service {
        public static Status StatusService = new Status();
        public static ValidaConfiguracion configuracion = new ValidaConfiguracion();
        public static IValidadorComprobante validador33;
        public static IValidadorComprobante validador32;
        public static IValidadorComprobante validador40;

        public static string RFC { get; set; }

        public Validador2Service(ValidaConfiguracion configuracion = null) {
            validador40 = new ValidadorCFDIv40();
            if (configuracion != null)
                Validador2Service.configuracion = configuracion;
        }

        public async Task ValidarParallelAsync(BindingList<DocumentoFiscal> dataSource, IProgress<Progreso> progreso) {
            var reporte = new Progreso() { Caption = "Obteniendo lista de archivos ..." };
            await Task.Run(() => {
                Parallel.ForEach<DocumentoFiscal>(dataSource, (documento) => {
                    Procesar(documento);
                    reporte.Caption = "Procesando: " + documento.PathXML;
                    progreso.Report(reporte);
                });
            });
            reporte.Caption = "Proceso terminado";
            progreso.Report(reporte);
            return;
        }

        public static IDocumentoFiscal Procesar(IDocumentoFiscal documentoFiscal) {
            IValidadorComprobante current;
            // respuesta, datos generales
            var respuesta = new ComprobanteValidacion {
                Provider = "local",
                IdDocumento = documentoFiscal.IdDocumento,
                TipoComprobante = documentoFiscal.TipoComprobante,
                Folio = documentoFiscal.Folio,
                Serie = documentoFiscal.Serie,
                FechaEmision = documentoFiscal.FechaEmision.Value,
                EmisorNombre = documentoFiscal.EmisorNombre,
                EmisorRFC = documentoFiscal.EmisorRFC,
                ReceptorNombre = documentoFiscal.ReceptorNombre,
                ReceptorRFC = documentoFiscal.ReceptorRFC,
                Total = documentoFiscal.Total,
                FechaValidacion = DateTime.Now
            };

            var comprobante = new object();
            current = Validador2Service.GetComprobante(documentoFiscal, ref comprobante);
            current.Comprobante = comprobante;

            if (Validador2Service.configuracion.VerificarCodificacionUTF8) {
                respuesta.Resultados.Add(current.Codificacion());
            }

            if (Validador2Service.configuracion.ValidarXSD) {
                respuesta.Resultados.Add(current.Esquema());
            }

            respuesta.Resultados.Add(current.LugarExpedicion());
            respuesta.Resultados.Add(current.SelloSAT());
            respuesta.Resultados.AddRange(current.SelloCFDI());
            respuesta.Resultados.Add(current.UsoCFDI());
            respuesta.Resultados.Add(current.FormaPago());
            respuesta.Resultados.Add(current.MetodoPago());
            respuesta.Resultados.AddRange(Validador2Service.GetEstado(documentoFiscal));
            documentoFiscal.Validacion = respuesta;

            if (string.IsNullOrEmpty(documentoFiscal.EmisorNombre)) {
                var _emisor = current.GetEmisor();
                if (_emisor != null) {
                    if (_emisor.HasValue()) {
                        try {
                            respuesta.EmisorNombre = current.Emisor[documentoFiscal.EmisorRFC];
                            documentoFiscal.EmisorNombre = current.Emisor[documentoFiscal.EmisorRFC];
                        } catch (Exception exp) {
                            Console.WriteLine("No se pudo recuperar nombre del emisor: " + exp.Message);
                        }
                    }
                }
            }

            if (Validador2Service.RFC == documentoFiscal.EmisorRFC) {
                documentoFiscal.TipoComprobante = "Emitidos";
            } else if (Validador2Service.RFC == documentoFiscal.ReceptorRFC) {
                documentoFiscal.TipoComprobante = "Recibidos";
            } else {
                documentoFiscal.TipoComprobante = "No definido";
            }

            if (documentoFiscal.Estado == "Vigente" && current.IsSelloCFDI) {
                documentoFiscal.Resultado = "Válido";
            } else {
                documentoFiscal.Resultado = "No Válido";
            }
            return documentoFiscal;
        }

        private static IValidadorComprobante GetComprobante(IDocumentoFiscal documentoFiscal, ref object comprobante) {
            IValidadorComprobante current;
            var bComprobante = FileService.FromBase64(documentoFiscal.XmlContentB64);

            if (documentoFiscal.Version == "3.2") {
                comprobante = CFDI.V32.Comprobante.Deserialize(System.Text.Encoding.UTF8.GetString(bComprobante));
                if (Validador2Service.validador32 == null) { Validador2Service.validador32 = new ValidadorCFDIv32(); }
                current = Validador2Service.validador32;
            } else if (documentoFiscal.Version == "3.3") {
                comprobante = CFDI.V33.Comprobante.LoadBase64(documentoFiscal.XmlContentB64);
                if (Validador2Service.validador33 == null) { Validador2Service.validador33 = new ValidadorCFDIv33(); }
                current = Validador2Service.validador33;
            } else if (documentoFiscal.Version == "4.0") {
                comprobante = CFDI.V40.Comprobante.Deserialize(System.Text.Encoding.UTF8.GetString(bComprobante));
                if (Validador2Service.validador40 == null) { Validador2Service.validador40 = new ValidadorCFDIv40(); }
                current = Validador2Service.validador40;
            } else {
                current = new ValidadorFactory();
            }

            return current;
        }

        #region estado SAT
        private static List<ComprobanteValidacionPropiedad> GetEstado(IDocumentoFiscal documentoFiscal) {
            var response = new List<ComprobanteValidacionPropiedad>();
            var estado = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.EstadoSAT);
            var q = Validador2Service.StatusService.GetStatusCFDI(documentoFiscal.EmisorRFC, documentoFiscal.ReceptorRFC, documentoFiscal.Total, documentoFiscal.IdDocumento);
            if (q != null) {
                // si contiene S Comprobante obtenido satisfactoriamente entonces es vigente
                if (q.CodigoEstatus.ToUpper().StartsWith("S")) {
                    estado.IsValido = true;
                    estado.Valor = "Vigente";
                } else if (q.CodigoEstatus.Contains("602")) {
                    estado.Valor = "No encontrado";
                    estado.IsValido = false;
                } else if (q.CodigoEstatus.Contains("601")) {
                    estado.Valor = "601";
                    estado.IsValido = false;
                }
                documentoFiscal.Estado = estado.Valor;
                response.Add(estado);

                if (q.ValidacionEFOS == "200") {
                    documentoFiscal.Situacion = "Sin reporte";
                    response.Add(ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.EFOS));
                } else {

                }
            }
            return response;
        }

        public static List<ComprobanteValidacionPropiedad> GetEstadoSAT(object cfd) {
            if (cfd.GetType() == typeof(CFDI.V32.Comprobante)) {
                return Validador2Service.GetEstadoSAT(cfd as CFDI.V32.Comprobante);
            } else if (cfd.GetType() == typeof(CFDI.V33.Comprobante)) {
                return Validador2Service.GetEstadoSAT(cfd as CFDI.V33.Comprobante);
            } else if (cfd.GetType() == typeof(CFDI.V40.Comprobante)) {
                return Validador2Service.GetEstadoSAT(cfd as CFDI.V40.Comprobante);
            }
            return new List<ComprobanteValidacionPropiedad>();
        }

        public static List<ComprobanteValidacionPropiedad> GetEstadoSAT(CFDI.V32.Comprobante cfd) {
            if (cfd.Complemento.TimbreFiscalDigital.UUID != null)
            return Validador2Service.GetAcuse(cfd.Emisor.rfc, cfd.Receptor.rfc, cfd.total, cfd.Complemento.TimbreFiscalDigital.UUID);
            return null;
        }

        public static List<ComprobanteValidacionPropiedad> GetEstadoSAT(CFDI.V33.Comprobante cfd) {
            if (cfd.Complemento.TimbreFiscalDigital.UUID != null)
                return Validador2Service.GetAcuse(cfd.Emisor.Rfc, cfd.Receptor.Rfc, cfd.Total, cfd.Complemento.TimbreFiscalDigital.UUID);
            return null;
        }

        public static List<ComprobanteValidacionPropiedad> GetEstadoSAT(CFDI.V40.Comprobante cfd) {
            if (cfd.Complemento.TimbreFiscalDigital.UUID != null)
                return Validador2Service.GetAcuse(cfd.Emisor.Rfc, cfd.Receptor.Rfc, cfd.Total, cfd.Complemento.TimbreFiscalDigital.UUID);
            return null;
        }

        private static List<ComprobanteValidacionPropiedad> GetAcuse(string rfcEmisor, string rfcReceptor, decimal total, string idDocumento) {
            var response = new List<ComprobanteValidacionPropiedad>();
            var estado = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.EstadoSAT);
            var q = Validador2Service.StatusService.GetStatusCFDI(rfcEmisor, rfcReceptor, total, idDocumento);
            if (q != null) {
                // si contiene S Comprobante obtenido satisfactoriamente entonces es vigente
                if (q.CodigoEstatus.ToUpper().StartsWith("S")) {
                    estado.IsValido = true;
                    estado.Valor = q.Estado;
                } else if (q.CodigoEstatus.Contains("602")) {
                    estado.Valor = "No encontrado";
                    estado.IsValido = false;
                } else if (q.CodigoEstatus.Contains("601")) {
                    estado.Valor = "601";
                    estado.IsValido = false;
                }
                response.Add(estado);

                if (q.ValidacionEFOS == "200") {
                    response.Add(ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.EFOS));
                }
            }

            return response;
        }
        #endregion
    }
}
