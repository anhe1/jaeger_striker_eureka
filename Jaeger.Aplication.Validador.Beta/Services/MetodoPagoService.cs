﻿using System;
using System.Collections.Generic;
using Jaeger.Aplication.Validador.Beta.Domain;

namespace Jaeger.Aplication.Validador.Beta.Services {
    public static class MetodoPagoService {
        public static Dictionary<string, string> Catalogo = new Dictionary<string, string>();

        static MetodoPagoService() {
            Catalogo.Add("PUE", "Pago en una sola exhibición");
            Catalogo.Add("PPD", "Pago en parcialidades o diferido");
            Catalogo.Add("PIP", "");
        }

        public static ComprobanteValidacionPropiedad Validar(this object cfd) {
            if (cfd.GetType() == typeof(CFDI.V32.Comprobante)) {
                return MetodoPagoService.Validar(cfd as CFDI.V32.Comprobante);
            } else if (cfd.GetType() == typeof(CFDI.V33.Comprobante)) {
                return MetodoPagoService.Validar(cfd as CFDI.V33.Comprobante);
            } else if (cfd.GetType() == typeof(CFDI.V40.Comprobante)) {
                return MetodoPagoService.Validar(cfd as CFDI.V40.Comprobante);
            }
            var response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.MetodoPago);
            response.Valor = "No se reconoce el tipo de comprobante";
            response.IsValido = false;
            return response;
        }

        public static ComprobanteValidacionPropiedad Validar(this CFDI.V32.Comprobante cfd) {
            var _response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.MetodoPago);
            _response.Valor = cfd.metodoDePago;
            return _response;
        }

        public static ComprobanteValidacionPropiedad Validar(this CFDI.V33.Comprobante cfd) {
            var _response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.MetodoPago);
            if (cfd.TipoDeComprobante != "P") {
                if (cfd.Complemento != null) {
                    if (cfd.Complemento.TimbreFiscalDigital != null) {
                        if (cfd.Complemento.TimbreFiscalDigital.FechaTimbrado >= new DateTime(2017, 1, 1)) {
                            if (cfd.MetodoPagoSpecified) {
                                if (MetodoPagoService.Catalogo.ContainsKey(cfd.MetodoPago)) {
                                    _response.IsValido = true;
                                }
                            }
                        }
                    }
                }
            }
            _response.Valor = cfd.MetodoPago;
            return _response;
        }

        public static ComprobanteValidacionPropiedad Validar(this CFDI.V40.Comprobante cfd) {
            var _response = ValidacionRespuesta.GetPropiedad(ValidacionRespuesta.PropiedadValidacionEnum.MetodoPago);
            _response.Valor = cfd.MetodoPago;
            return _response;
        }
    }
}
