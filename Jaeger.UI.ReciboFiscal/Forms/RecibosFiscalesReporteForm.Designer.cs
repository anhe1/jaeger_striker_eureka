﻿namespace Jaeger.UI.ReciboFiscal.Forms {
    partial class RecibosFiscalesReporteForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RecibosFiscalesReporteForm));
            this.TRecibo = new Jaeger.UI.Common.Forms.GridCommonControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TRecibo
            // 
            this.TRecibo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TRecibo.Location = new System.Drawing.Point(0, 0);
            this.TRecibo.Name = "TRecibo";
            this.TRecibo.PDF = null;
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TRecibo.Permisos = uiAction1;
            this.TRecibo.ShowActualizar = true;
            this.TRecibo.ShowAutosuma = false;
            this.TRecibo.ShowCancelar = false;
            this.TRecibo.ShowCerrar = true;
            this.TRecibo.ShowEditar = false;
            this.TRecibo.ShowEjercicio = true;
            this.TRecibo.ShowExportarExcel = true;
            this.TRecibo.ShowFiltro = true;
            this.TRecibo.ShowHerramientas = true;
            this.TRecibo.ShowImprimir = false;
            this.TRecibo.ShowItem = false;
            this.TRecibo.ShowNuevo = false;
            this.TRecibo.ShowPeriodo = true;
            this.TRecibo.ShowSeleccionMultiple = true;
            this.TRecibo.Size = new System.Drawing.Size(1147, 579);
            this.TRecibo.TabIndex = 0;
            // 
            // RecibosFiscalesReporteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1147, 579);
            this.Controls.Add(this.TRecibo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RecibosFiscalesReporteForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Recibos Fiscales";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RecibosFiscalesReporteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal Common.Forms.GridCommonControl TRecibo;
    }
}
