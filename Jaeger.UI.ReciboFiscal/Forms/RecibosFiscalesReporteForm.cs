﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.ReciboFiscal;
using Jaeger.Domain.ReciboFiscal.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.ReciboFiscal.Builder;

namespace Jaeger.UI.ReciboFiscal.Forms {
    public partial class RecibosFiscalesReporteForm : RadForm {
        protected IReciboFiscalService Service;
        protected BindingList<ReciboFiscalModel> _DataSource;
        protected internal RadMenuItem TRecibo_PrePoliza = new RadMenuItem { Text = "Pre Póliza" };

        public RecibosFiscalesReporteForm() {
            InitializeComponent();
        }

        private void RecibosFiscalesReporteForm_Load(object sender, EventArgs e) {
            using (IRecibosFiscalesGridBuilder view = new RecibosFiscalesGridBuilder()) {
                this.TRecibo.GridData.Columns.AddRange(view.Templetes().Master().Build());
            }

            this.Service = new ReciboFiscalService();
            this.TRecibo.Herramientas.Items.Add(this.TRecibo_PrePoliza);
            this.TRecibo_PrePoliza.Click += TRecibo_PrePoliza_Click;
            this.TRecibo.ShowHerramientas = true;
            this.TRecibo.ShowAutosuma = true;
            this.TRecibo.Actualizar.Click += this.TRecibo_Actualizar_Click;
            this.TRecibo.Cerrar.Click += this.TRecibo_Cerrar_Click;
        }

        private void TRecibo_PrePoliza_Click(object sender, EventArgs e) {
            SaveFileDialog openFileDialog = new SaveFileDialog() { FileName = "PrePoliza.csv", Filter = "*.csv|*.CSV", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
            if (openFileDialog.ShowDialog() != DialogResult.OK)
                return;
            this.TRecibo_PrePoliza.Tag = openFileDialog.FileName;
            this.Reporte();
        }

        private void TRecibo_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.OnActualizar)) {
                espera.Text = "Consultando...";
                espera.ShowDialog(this);
            }
            this.TRecibo.GridData.DataSource = this._DataSource;
        }

        private void TRecibo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void OnActualizar() {
            this._DataSource = this.Service.GetRecibosFiscales(this.TRecibo.GetPeriodo(), this.TRecibo.GetEjercicio(), 0, ConfigService.Synapsis.Empresa.RFC);
        }

        private List<List<Partida>> ConfiguraCuentas() {
            List<List<Partida>> cuentas = new List<List<Partida>>();
            // plan cero
            var planCero = new List<Partida>()
            {
                new Partida(){ Campo2 = "--------------------"},
                new Partida(){ Campo2 = "--------------------"},
                new Partida(){ Campo2 = "--------------------"},
                new Partida(){ Campo2 = "--------------------"},
                new Partida(){ Campo2 = "--------------------"},
            };
            cuentas.Add(planCero);

            // plan flexible
            var planFlexible = new List<Partida>()
            {
                new Partida(){ Campo2 = "1105-0001-0001-0040", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "1105-0001-0002-0040", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "5102-0001-0040-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2110-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2105-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2105-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" }
            };
            cuentas.Add(planFlexible);

            // a la medida
            var planAlaMedida = new List<Partida>()
            {
                new Partida(){ Campo2 = "1105-0001-0001-0041", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "1105-0001-0002-0041", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "5102-0001-0041-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2110-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
            };
            cuentas.Add(planAlaMedida);

            // a la medida
            var planEconomico = new List<Partida>()
            {
                new Partida(){ Campo2 = "1105-0001-0001-0039", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "1105-0001-0002-0039", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "5102-0001-0039-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2110-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
            };
            cuentas.Add(planEconomico);

            // plan clasico n.r.
            var planClasico = new List<Partida>()
            {
                new Partida(){ Campo2 = "1105-0001-0001-0038", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "1105-0001-0002-0038", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "5102-0001-0038-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2110-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
            };
            cuentas.Add(planClasico);

            // plan auto clasico
            var planAutoClasico = new List<Partida>()
            {
                new Partida(){ Campo2 = "1105-0001-0001-0042", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "1105-0001-0002-0042", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "5102-0001-0042-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2110-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
            };
            cuentas.Add(planAutoClasico);

            // plan estrena ya
            var planEstrenaYa = new List<Partida>()
            {
                new Partida(){ Campo2 = "1105-0001-0001-0043", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "1105-0001-0002-0043", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "5102-0001-0043-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2110-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
            };
            cuentas.Add(planEstrenaYa);

            // plan afasa 3
            var planAfasa3 = new List<Partida>()
            {
                new Partida(){ Campo2 = "1105-0001-0001-0044", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "1105-0001-0002-0043", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "5102-0001-0043-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2110-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
            };
            cuentas.Add(planAfasa3);

            // afasa rapido 60 A
            var planAfasaRapido = new List<Partida>() {
                new Partida(){ Campo2 = "1104-0002-0001-0001", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "4101-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2104-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2105-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2105-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" }
            };
            cuentas.Add(planAfasaRapido);

            // afasa comodo 60 C
            var planAfasaComodo = new List<Partida>() {
                new Partida(){ Campo2 = "1104-0002-0001-0002", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "4101-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2104-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2105-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" }
            };
            cuentas.Add(planAfasaComodo);

            // afa flexible 60 B
            var planAfasaFlexible60B = new List<Partida>() {
                new Partida(){ Campo2 = "1104-0002-0001-0003", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "4101-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2104-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" },
                new Partida(){ Campo2 = "2105-0001-0001-0000", Campo3 = "0", Campo4 = "RFISC-{0} A {1} SIST. VARIOS ({2})", Campo5 = "1" }
            };
            cuentas.Add(planAfasaFlexible60B);

            return cuentas;
        }

        private void Reporte() {
            var inicioCFDI = new Partida() { Campo3 = "INICIO_CFDI" };
            var finCFDI = new Partida() { Campo3 = "FIN_CFDI" };

            List<List<Partida>> cuentasContables = this.ConfiguraCuentas();
            List<Partida> prePolizas = new List<Partida>();
            var recibosFiscales = this.TRecibo.GridData.ChildRows.Select(it => it.DataBoundItem as ReciboFiscalModel).ToList();

            for (int i = 0; i < 32; i++) {
                List<ReciboFiscalModel> datos2 = recibosFiscales.Where(it => it.Dia == i).OrderBy(it => it.NoPlan).Select(it => it as ReciboFiscalModel).ToList();
                if (datos2 != null) {
                    if (datos2.Count > 0) {
                        List<int> minimo = datos2.Select(it => Convert.ToInt32(it.Folio)).ToList();
                        var descripcion = new Partida() {
                            Campo2 = i.ToString(),
                            Campo1 = "Dr",
                            Campo3 = string.Format("RFISC-{0} A {1} SIST. VARIOS ({2})", minimo.Min<int>().ToString(), minimo.Max<int>().ToString(), datos2[0].FechaEmision.Value.ToString("dd/MM/yy"))
                        };

                        prePolizas.Add(descripcion);
                        string Campo3 = descripcion.Campo3;
                        descripcion = null;
                        for (int plan = 0; plan < 11; plan++) {
                            List<Partida> tipo1 = datos2.Where(it => it.NoPlan == plan).OrderBy(it => Convert.ToInt32(it.Folio)).Select(it => new Partida {
                                Campo3 = it.FechaEmision.Value.ToString("dd/MM/yy"),
                                Campo6 = it.EmisorRFC,
                                Campo7 = it.ReceptorRfc,
                                Campo8 = it.Total.ToString(),
                                Campo9 = it.IdDocumento
                            }).ToList();

                            if (tipo1.Count > 0) {
                                decimal iva = datos2.Where(it => it.NoPlan == plan).Sum(it => it.Iva);
                                decimal total = datos2.Where(it => it.NoPlan == plan).Sum(it => it.SubTotal);

                                Partida uno = (Partida)cuentasContables[plan][0].Clone();
                                uno.Campo4 = Campo3;
                                uno.Campo6 = (iva + total).ToString();
                                uno.Campo7 = "0";
                                prePolizas.Add(uno);
                                prePolizas.Add(inicioCFDI);
                                prePolizas.AddRange(tipo1);
                                prePolizas.Add(finCFDI);

                                Partida dos = (Partida)cuentasContables[plan][1].Clone();
                                prePolizas.Add(dos);
                                dos.Campo4 = Campo3;
                                dos.Campo6 = "0";
                                dos.Campo7 = total.ToString();
                                prePolizas.Add(inicioCFDI);
                                prePolizas.AddRange(tipo1);
                                prePolizas.Add(finCFDI);

                                Partida tres = (Partida)cuentasContables[plan][2].Clone();
                                tres.Campo4 = Campo3;
                                tres.Campo6 = "0";
                                tres.Campo7 = iva.ToString();
                                prePolizas.Add(tres);
                                prePolizas.Add(inicioCFDI);
                                prePolizas.AddRange(tipo1);
                                prePolizas.Add(finCFDI);

                                Partida cuatro = (Partida)cuentasContables[plan][3].Clone();
                                cuatro.Campo4 = Campo3;
                                cuatro.Campo6 = iva.ToString();
                                cuatro.Campo7 = "0";
                                prePolizas.Add(cuatro);
                                prePolizas.Add(inicioCFDI);
                                prePolizas.AddRange(tipo1);
                                prePolizas.Add(finCFDI);

                                if (cuentasContables[plan].Count > 4) {
                                    Partida cinco = (Partida)cuentasContables[plan][4].Clone();
                                    cinco.Campo4 = Campo3;
                                    cinco.Campo6 = "0";
                                    cinco.Campo7 = iva.ToString();
                                    prePolizas.Add(cinco);
                                    prePolizas.Add(inicioCFDI);
                                    prePolizas.AddRange(tipo1);
                                    prePolizas.Add(finCFDI);
                                }
                            }
                        }
                    }
                }
            }
            var prueba = this.Service.ExportPrePoliza(prePolizas, (string)this.TRecibo_PrePoliza.Tag);
            if (prueba == false)
                RadMessageBox.Show(this, "No es posible crear el archivo de salida. Es posible que el archivo se encuentre bloqueado.", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
        }
    }
}
