﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.ReciboFiscal.Builder {
    /// <summary>
    /// Recibos Fiscales
    /// </summary>
    public interface IRecibosFiscalesGridBuilder : IGridViewBuilder, IGridViewTempleteBuild, IGridViewColumnsBuild {
        IRecibosFiscalesTempletesGridBuilder Templetes();
    }

    public interface IRecibosFiscalesTempletesGridBuilder : IGridViewBuilder, IGridViewTempleteBuild {
        IRecibosFiscalesTempletesGridBuilder Master();
    }

    public interface IRecibosFiscalesColumnsGridBuilder : IGridViewColumnsBuild {
        IRecibosFiscalesColumnsGridBuilder Estado();

        IRecibosFiscalesColumnsGridBuilder Folio();

        IRecibosFiscalesColumnsGridBuilder Serie();

        IRecibosFiscalesColumnsGridBuilder EmisorRFC();

        IRecibosFiscalesColumnsGridBuilder Emisor();

        IRecibosFiscalesColumnsGridBuilder ReceptorRFC();

        IRecibosFiscalesColumnsGridBuilder Receptor();

        IRecibosFiscalesColumnsGridBuilder IdDocumento();

        IRecibosFiscalesColumnsGridBuilder NoPlan();

        IRecibosFiscalesColumnsGridBuilder NoPlantText();

        IRecibosFiscalesColumnsGridBuilder Dia();

        IRecibosFiscalesColumnsGridBuilder FechaEmision();

        IRecibosFiscalesColumnsGridBuilder FechaTimbre();

        IRecibosFiscalesColumnsGridBuilder FechaCancela();

        IRecibosFiscalesColumnsGridBuilder Total();

        IRecibosFiscalesColumnsGridBuilder TotalPagado();

        IRecibosFiscalesColumnsGridBuilder Consucutivo();

        IRecibosFiscalesColumnsGridBuilder Mensualidad();

        IRecibosFiscalesColumnsGridBuilder SeguroAuto();

        IRecibosFiscalesColumnsGridBuilder Otros();

        IRecibosFiscalesColumnsGridBuilder CuotaSeguroVida();

        IRecibosFiscalesColumnsGridBuilder CuotaIVA();

        IRecibosFiscalesColumnsGridBuilder CuotaGastosAdmon();

        IRecibosFiscalesColumnsGridBuilder CuotaAutomovil();
    }
}
