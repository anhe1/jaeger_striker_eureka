﻿using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.ReciboFiscal.Builder {
    /// <summary>
    /// Recibos Fiscales
    /// </summary>
    public class RecibosFiscalesGridBuilder : GridViewBuilder, IRecibosFiscalesGridBuilder, IGridViewBuilder, IGridViewTempleteBuild, IGridViewColumnsBuild, IRecibosFiscalesColumnsGridBuilder,
        IRecibosFiscalesTempletesGridBuilder {

        public RecibosFiscalesGridBuilder() : base() { }

        #region templetes
        public IRecibosFiscalesTempletesGridBuilder Master() {
            this._Columns.Clear();
            this.Estado().Folio().Serie().EmisorRFC().Emisor().ReceptorRFC().Receptor().IdDocumento().NoPlan().NoPlantText().Dia().FechaEmision().FechaTimbre().FechaCancela().Total().TotalPagado().Consucutivo().
                          Mensualidad().SeguroAuto().Otros().CuotaSeguroVida().CuotaIVA().CuotaGastosAdmon().CuotaAutomovil();

            return this;
        }

        public IRecibosFiscalesTempletesGridBuilder Templetes() {
            return this;
        }
        #endregion

        #region columnas
        public IRecibosFiscalesColumnsGridBuilder Estado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Estado",
                HeaderText = "Estado",
                IsVisible = false,
                Name = "Estado"
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder Folio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Folio",
                HeaderText = "Folio",
                Name = "Folio",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 80
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder Serie() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Serie",
                HeaderText = "Serie",
                Name = "Serie",
                Width = 85,
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder EmisorRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "EmisorRFC",
                HeaderText = "Emisor (RFC)",
                IsVisible = false,
                Name = "EmisorRFC",
                Width = 90
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder Emisor() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Emisor",
                HeaderText = "Emisor",
                IsVisible = false,
                Name = "Emisor",
                Width = 240
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder ReceptorRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorRfc",
                HeaderText = "RFC",
                Name = "ReceptorRfc",
                Width = 90
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder Receptor() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Receptor",
                HeaderText = "Receptor",
                Name = "Receptor",
                Width = 240
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder IdDocumento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdDocumento",
                HeaderText = "IdDocumento (UUID)",
                Name = "IdDocumento",
                Width = 195
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder NoPlan() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NoPlan",
                HeaderText = "Tipo",
                Name = "NoPlan",
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder NoPlantText() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NoPlanText",
                HeaderText = "Plan",
                Name = "NoPlanText"
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder Dia() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Dia",
                HeaderText = "Día",
                Name = "Dia",
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder FechaEmision() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaEmision",
                FormatString = "{0:d}",
                HeaderText = "Fec. Emisión",
                Name = "FechaEmision",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder FechaTimbre() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaTimbre",
                FormatString = "{0:d}",
                HeaderText = "Fec. Timbre",
                Name = "FechaTimbre",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85

            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder FechaCancela() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaCancela",
                FormatString = "{0:d}",
                HeaderText = "Fec. Cancela",
                Name = "FechaCancela",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder Total() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Total",
                FormatString = "{0:n}",
                HeaderText = "Total",
                Name = "Total",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder TotalPagado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "TotalPagado",
                FormatString = "{0:n}",
                HeaderText = "Total Pagado",
                Name = "TotalPagado",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder Consucutivo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Consecutivo",
                HeaderText = "Consecutivo",
                Name = "Consecutivo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder Mensualidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Mensualidad",
                HeaderText = "Mensualidad",
                Name = "Mensualidad",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder SeguroAuto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(double),
                FieldName = "SeguroAuto",
                FormatString = "{0:n}",
                HeaderText = "Seguro \r\nde Auto",
                Name = "SeguroAuto",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder Otros() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(double),
                FieldName = "Otros",
                FormatString = "{0:n}",
                HeaderText = "Otros",
                Name = "Otros",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder CuotaSeguroVida() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(double),
                FieldName = "CuotaSeguroVida",
                FormatString = "{0:n}",
                HeaderText = "Cuota \r\nSeguro Vida",
                Name = "CuotaSeguroVida",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder CuotaIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(double),
                FieldName = "CuotaIva",
                FormatString = "{0:n}",
                HeaderText = "Cuota IVA",
                Name = "CuotaIva",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder CuotaGastosAdmon() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(double),
                FieldName = "CuotaGastosAdmon",
                FormatString = "{0:n}",
                HeaderText = "Cuota \r\nGastos Admon.",
                Name = "CuotaGastosAdmon",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IRecibosFiscalesColumnsGridBuilder CuotaAutomovil() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(double),
                FieldName = "CuotaAutomovil",
                FormatString = "{0:n}",
                HeaderText = "Cuota \r\nAutomovil",
                Name = "CuotaAutomovil",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }
        #endregion
    }
}
