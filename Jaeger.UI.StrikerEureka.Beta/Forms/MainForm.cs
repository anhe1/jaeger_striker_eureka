﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Jaeger.Aplication.Base;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Forms.Login;

namespace Jaeger.UI.Forms {
    public partial class MainForm : Form {
        public MainForm() {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e) {
            //Console.WriteLine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "DDW.ini"));
            
            ConfigService.Synapsis = new Domain.Empresa.Entities.Configuracion();
            ConfigService.Synapsis.Empresa = new Domain.Empresa.Entities.EmpresaGeneral { RFC = "N", RazonSocial = "Empresa" };
            ConfigService.Piloto = new Domain.Base.Profile.KaijuLogger();
            ConfigService.Piloto.Clave = ConfigService.Sysdba;
            ConfigService.Piloto.Roles.Add(new Rol());
            (ConfigService.Piloto.Roles[0] as Rol).Name = "desarrollador";
            ConfigService.Menus = this.Menus();
            var MenuPermissions = new UIMenuItemPermission(UIPermissionEnum.Invisible);
            MenuPermissions.Load(ConfigService.Menus);
            UIMenuUtility.SetPermission(this, ConfigService.Piloto, MenuPermissions);
            this.m_Archivo_Session.PerformClick();
            if (ConfigService.Synapsis == null) {
                MessageBox.Show(this, "No selecciono una sesión válida.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }

            //Aplication.Aspel.Base.ConfigService.Synapsis = new Domain.Aspel.Base.Configuracion() {
            //    Bancos = new Domain.DataBase.Entities.DataBaseConfiguracion { Database = "/dbs/bancos50.fdb", Password = "masterkey", UserID = "sysdba", PortNumber = 3050, HostName = "fileserver4" },
            //    Coi = new Domain.DataBase.Entities.DataBaseConfiguracion { Database = "/dbs/coi90.fdb", Password = "masterkey", UserID = "sysdba", PortNumber = 3050, HostName = "fileserver4" }
            //};
            this.Empresa.Text = ConfigService.Synapsis.Empresa.RFC;
        }

        private void Menu_Archivo_Session_Click(object sender, EventArgs e) {
            var sesion_form = new SessionForm();
            sesion_form.ShowDialog(this);
        }

        private void Menu_Archivo_Salir_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Menu_Ayuda_AcercaDe_Click(object sender, EventArgs e) {
            var acercade_form = new AboutBoxForm();
            acercade_form.ShowDialog(this);
        }

        private void Aspel_Configuracion_Click(object sender, EventArgs e) {
            var m = new Aspel.ConfiguracionForm();
            m.ShowDialog(this);
        }

        private void Form_Active(Type type, string title = "") {
            this.menuStrip1.SuspendLayout();
            var _form = (Form)Activator.CreateInstance(type);
            if (_form.WindowState == FormWindowState.Maximized) { 
                _form.MdiParent = this;
                _form.Size = this.Size;
                _form.Show();
            } else {
                _form.StartPosition = FormStartPosition.CenterParent;
                _form.ShowDialog(this);
            }

            if (title != "")
                _form.Text = title;

            this.menuStrip1.Visible = false;
            this.menuStrip1.Visible = true;
            this.menuStrip1.ResumeLayout();
        }

        private void Menu_Common_Click(object sender, EventArgs e) {
            var _item = (ToolStripMenuItem)sender;
            var _menuElement = ConfigService.GeMenuElement(_item.Name);
            var localAssembly = Assembly.Load("Jaeger.UI.StrikerEureka.Beta");

            if (_menuElement != null) {
                try {
                    var d = localAssembly.GetTypes();
                    Type type = localAssembly.GetType("Jaeger.UI." + _menuElement.Form, true);
                    this.Form_Active(type, _item.Text);
                } catch (Exception ex) {
                    MessageBox.Show(this, ex.Message, "Jaeger.UI", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            } else {
                MessageBox.Show(this, Properties.Resources.Message_UI_Error, "Jaeger.UI", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void Menu_UI_Aspel_Click(object sender, EventArgs e) {
            var item = (ToolStripMenuItem)sender;
            var localAssembly = Assembly.Load("Jaeger.UI.Aspel");

            if (item.Tag != null) {
                try {
                    var d = localAssembly.GetTypes();
                    Type type = localAssembly.GetType("Jaeger.UI.Aspel." + item.Tag.ToString(), true);
                    this.Form_Active(type);
                } catch (Exception ex) {
                    MessageBox.Show(this, ex.Message, "Jaeger.UI", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            } else {
                MessageBox.Show(this, Properties.Resources.Message_UI_Error, "Jaeger.UI", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public List<UIMenuElement> Menus() {
            var response = new List<UIMenuElement> {
                new UIMenuElement { Name = "m_Archivo", Label = "Archivo", Rol = "*" },
                new UIMenuElement { Name = "m_Archivo_Configuracion", Label = "Configuración", Form = "Forms.General.ConfiguracionForm" },
                new UIMenuElement { Name = "m_Archivo_Usuarios", Label = "Usuarios" },
                new UIMenuElement { Name = "m_Archivo_Session", Label = "Sesión" },
                new UIMenuElement { Name = "m_Archivo_Salir", Label = "Salir", Rol = "*" },

                new UIMenuElement { Name = "m_Emision", Label = "Emisión" },
                new UIMenuElement { Name = "m_Emision_Contribuyente", Label = "Clientes", Form = "Forms.Contribuyentes.ClientesCatalogoForm" },
                new UIMenuElement { Name = "m_Emision_Comprobante", Label = "Facturación", Form = "Forms.Comprobantes.ComprobantesFiscalesEmitidoForm" },
                new UIMenuElement { Name = "m_Emision_ReciboFiscal", Label = "Recibos Fiscales", Form = "Forms.Comprobantes.RecibosFiscalesForm" },
                new UIMenuElement { Name = "m_Emision_Retenciones", Label = "Retenciones", Form = "Forms.Retenciones.ComprobanteRetencionesForm" },

                new UIMenuElement { Name = "m_Recepcion", Label = "Recepción" },
                new UIMenuElement { Name = "m_Recepcion_Contribuyente", Label = "Proveedor" },
                new UIMenuElement { Name = "m_Recepcion_Validador", Label = "Validador", Form = "Forms.Validador.ComprobanteValidadorForm" },
                new UIMenuElement { Name = "m_Recepcion_Comprobante", Label = "Comprobante", Form = "Forms.Comprobantes.ComprobantesFiscalesRecibidoForm"},

                new UIMenuElement { Name = "menu_Nominas", Label = "Nómina" },

                new UIMenuElement { Name = "m_Herramientas", Label = "Herramientas" },
                new UIMenuElement { Name = "m_Herramientas_Repositorio", Label = "Repositorio SAT", Form = "Forms.Repositorio.RepositorioForm" },
                new UIMenuElement { Name = "m_Herramientas_RepositorioLog", Label = "Repositorio Log" },
                new UIMenuElement { Name = "m_Herramientas_ValidaRFC", Label = "Validación de RFC", Form = "Forms.Validador.ValidaRFCForm" },
                new UIMenuElement { Name = "m_Herramientas_ValidaRetencion", Label = "Validación de Retención", Form = "Forms.Validador.ValidaRetencionForm" },

                new UIMenuElement { Name = "menu_Herramientas_Aspel", Label = "ASPEL" },
                new UIMenuElement { Name = "m_Aspel_Banco", Label = "Bancos" },
                new UIMenuElement { Name = "m_Aspel_Beneficiarios", Label = "Beneficiarios" },
                new UIMenuElement { Name = "m_Aspel_CuentasBancarias", Label = "Cuentas Bancarias" },
                new UIMenuElement { Name = "m_Aspel_Movimientos", Label = "Movimientos" },

                new UIMenuElement { Name = "menu_Herramientas_DB2", Label = "AS 400" },
                new UIMenuElement { Name = "m_db2_Configuracion", Label = "Configuración" },
                new UIMenuElement { Name = "m_db2_Solicitantes", Label = "Solicitantes" },

                new UIMenuElement { Name = "m_Aspel_COI", Label = "COI" },
                new UIMenuElement { Name = "m_COI_Cuentas", Label = "Cuentas" },
                new UIMenuElement { Name = "m_COI_Rubros", Label = "Rubros" },
                new UIMenuElement { Name = "m_COI_Polizas", Label = "Polizas" },
                new UIMenuElement { Name = "m_Aspel_Configuracion", Label = "Configuración" },

                new UIMenuElement { Name = "m_Ventana", Label = "Ventana", Rol = "*" },
                new UIMenuElement { Name = "m_Ayuda", Label = "Ayuda", Rol = "*" },
                new UIMenuElement { Name = "m_Ayuda_AcercaDe", Label = "Acerca de ...", Rol = "*" }
            };

            return response;
        }
    }
}
