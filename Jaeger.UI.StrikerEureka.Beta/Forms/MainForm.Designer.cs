﻿namespace Jaeger.UI.Forms {
    partial class MainForm {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.Empresa = new System.Windows.Forms.ToolStripDropDownButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.m_Archivo = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Archivo_Configuracion = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Archivo_Session = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Archivo_Salir = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Emision = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Emision_Contribuyente = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Emision_Comprobante = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Emision_ReciboFiscal = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Emision_Retenciones = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Banco = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Recepcion = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Recepcion_Contribuyente = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Recepcion_Validador = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Recepcion_Comprobante = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Nominas = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Contabilidad = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Herramientas = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Herramientas_Repositorio = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Herramientas_RepositorioLog = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Herramientas_Aspel = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Aspel_Banco = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Aspel_Beneficiarios = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Aspel_CuentasBancarias = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Aspel_Movimientos = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Aspel_COI = new System.Windows.Forms.ToolStripMenuItem();
            this.m_COI_Rubros = new System.Windows.Forms.ToolStripMenuItem();
            this.m_COI_Cuentas = new System.Windows.Forms.ToolStripMenuItem();
            this.m_COI_Polizas = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Aspel_Configuracion = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Herramientas_DB2 = new System.Windows.Forms.ToolStripMenuItem();
            this.m_db2_Configuracion = new System.Windows.Forms.ToolStripMenuItem();
            this.m_db2_Solicitantes = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Herramientas_ValidaRFC = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Herramientas_ValidaRetencion = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Ventana = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Ayuda = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Ayuda_AcercaDe = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Empresa});
            this.statusStrip1.Location = new System.Drawing.Point(0, 599);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1344, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // Empresa
            // 
            this.Empresa.Image = global::Jaeger.UI.Properties.Resources.company_16px;
            this.Empresa.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Empresa.Name = "Empresa";
            this.Empresa.Size = new System.Drawing.Size(127, 20);
            this.Empresa.Text = "XXXXXXXXXXXXX";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Archivo,
            this.m_Emision,
            this.m_Banco,
            this.m_Recepcion,
            this.m_Nominas,
            this.m_Contabilidad,
            this.m_Herramientas,
            this.m_Ventana,
            this.m_Ayuda});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.MdiWindowListItem = this.m_Ventana;
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1344, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "Menú";
            // 
            // m_Archivo
            // 
            this.m_Archivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Archivo_Configuracion,
            this.m_Archivo_Session,
            this.m_Archivo_Salir});
            this.m_Archivo.Name = "m_Archivo";
            this.m_Archivo.Size = new System.Drawing.Size(76, 20);
            this.m_Archivo.Text = "m_Archivo";
            // 
            // m_Archivo_Configuracion
            // 
            this.m_Archivo_Configuracion.Name = "m_Archivo_Configuracion";
            this.m_Archivo_Configuracion.Size = new System.Drawing.Size(166, 22);
            this.m_Archivo_Configuracion.Text = "m_Configuracion";
            this.m_Archivo_Configuracion.Click += new System.EventHandler(this.Menu_Common_Click);
            // 
            // m_Archivo_Session
            // 
            this.m_Archivo_Session.Name = "m_Archivo_Session";
            this.m_Archivo_Session.Size = new System.Drawing.Size(166, 22);
            this.m_Archivo_Session.Text = "m_Session";
            this.m_Archivo_Session.Click += new System.EventHandler(this.Menu_Archivo_Session_Click);
            // 
            // m_Archivo_Salir
            // 
            this.m_Archivo_Salir.Name = "m_Archivo_Salir";
            this.m_Archivo_Salir.Size = new System.Drawing.Size(166, 22);
            this.m_Archivo_Salir.Text = "m_Salir";
            this.m_Archivo_Salir.Click += new System.EventHandler(this.Menu_Archivo_Salir_Click);
            // 
            // m_Emision
            // 
            this.m_Emision.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Emision_Contribuyente,
            this.m_Emision_Comprobante,
            this.m_Emision_ReciboFiscal,
            this.m_Emision_Retenciones});
            this.m_Emision.Name = "m_Emision";
            this.m_Emision.Size = new System.Drawing.Size(77, 20);
            this.m_Emision.Text = "m_Emision";
            // 
            // m_Emision_Contribuyente
            // 
            this.m_Emision_Contribuyente.Name = "m_Emision_Contribuyente";
            this.m_Emision_Contribuyente.Size = new System.Drawing.Size(189, 22);
            this.m_Emision_Contribuyente.Tag = "Forms.Contribuyentes.ClientesCatalogoForm";
            this.m_Emision_Contribuyente.Text = "menu_Clientes";
            this.m_Emision_Contribuyente.Click += new System.EventHandler(this.Menu_Common_Click);
            // 
            // m_Emision_Comprobante
            // 
            this.m_Emision_Comprobante.Name = "m_Emision_Comprobante";
            this.m_Emision_Comprobante.Size = new System.Drawing.Size(189, 22);
            this.m_Emision_Comprobante.Tag = "Forms.Comprobantes.ComprobantesFiscalesEmitidoForm";
            this.m_Emision_Comprobante.Text = "menu_Comprobantes";
            this.m_Emision_Comprobante.Click += new System.EventHandler(this.Menu_Common_Click);
            // 
            // m_Emision_ReciboFiscal
            // 
            this.m_Emision_ReciboFiscal.Name = "m_Emision_ReciboFiscal";
            this.m_Emision_ReciboFiscal.Size = new System.Drawing.Size(189, 22);
            this.m_Emision_ReciboFiscal.Tag = "Forms.Comprobantes.RecibosFiscalesForm";
            this.m_Emision_ReciboFiscal.Text = "menu_ReciboFiscal";
            this.m_Emision_ReciboFiscal.Click += new System.EventHandler(this.Menu_Common_Click);
            // 
            // m_Emision_Retenciones
            // 
            this.m_Emision_Retenciones.Name = "m_Emision_Retenciones";
            this.m_Emision_Retenciones.Size = new System.Drawing.Size(189, 22);
            this.m_Emision_Retenciones.Text = "m_Retenciones";
            this.m_Emision_Retenciones.Click += new System.EventHandler(this.Menu_Common_Click);
            // 
            // m_Banco
            // 
            this.m_Banco.Name = "m_Banco";
            this.m_Banco.Size = new System.Drawing.Size(68, 20);
            this.m_Banco.Text = "m_Banco";
            // 
            // m_Recepcion
            // 
            this.m_Recepcion.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Recepcion_Contribuyente,
            this.m_Recepcion_Validador,
            this.m_Recepcion_Comprobante});
            this.m_Recepcion.Name = "m_Recepcion";
            this.m_Recepcion.Size = new System.Drawing.Size(90, 20);
            this.m_Recepcion.Text = "m_Recepcion";
            // 
            // m_Recepcion_Contribuyente
            // 
            this.m_Recepcion_Contribuyente.Name = "m_Recepcion_Contribuyente";
            this.m_Recepcion_Contribuyente.Size = new System.Drawing.Size(180, 22);
            this.m_Recepcion_Contribuyente.Text = "m_Proveedor";
            // 
            // m_Recepcion_Validador
            // 
            this.m_Recepcion_Validador.Name = "m_Recepcion_Validador";
            this.m_Recepcion_Validador.Size = new System.Drawing.Size(180, 22);
            this.m_Recepcion_Validador.Tag = "";
            this.m_Recepcion_Validador.Text = "m_Validador";
            this.m_Recepcion_Validador.Click += new System.EventHandler(this.Menu_Common_Click);
            // 
            // m_Recepcion_Comprobante
            // 
            this.m_Recepcion_Comprobante.Name = "m_Recepcion_Comprobante";
            this.m_Recepcion_Comprobante.Size = new System.Drawing.Size(180, 22);
            this.m_Recepcion_Comprobante.Text = "m_Comprobante";
            this.m_Recepcion_Comprobante.Click += new System.EventHandler(this.Menu_Common_Click);
            // 
            // m_Nominas
            // 
            this.m_Nominas.Name = "m_Nominas";
            this.m_Nominas.Size = new System.Drawing.Size(83, 20);
            this.m_Nominas.Text = "m_Nominas";
            // 
            // m_Contabilidad
            // 
            this.m_Contabilidad.Name = "m_Contabilidad";
            this.m_Contabilidad.Size = new System.Drawing.Size(103, 20);
            this.m_Contabilidad.Text = "m_Contabilidad";
            // 
            // m_Herramientas
            // 
            this.m_Herramientas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Herramientas_Repositorio,
            this.m_Herramientas_RepositorioLog,
            this.m_Herramientas_Aspel,
            this.m_Herramientas_DB2,
            this.m_Herramientas_ValidaRFC,
            this.m_Herramientas_ValidaRetencion});
            this.m_Herramientas.Name = "m_Herramientas";
            this.m_Herramientas.Size = new System.Drawing.Size(106, 20);
            this.m_Herramientas.Text = "m_Herramientas";
            // 
            // m_Herramientas_Repositorio
            // 
            this.m_Herramientas_Repositorio.Image = global::Jaeger.UI.Properties.Resources.sat_logo_x16;
            this.m_Herramientas_Repositorio.Name = "m_Herramientas_Repositorio";
            this.m_Herramientas_Repositorio.Size = new System.Drawing.Size(175, 22);
            this.m_Herramientas_Repositorio.Tag = "Forms.Repositorio.RepositorioForm";
            this.m_Herramientas_Repositorio.Text = "m_Repositorio";
            this.m_Herramientas_Repositorio.Click += new System.EventHandler(this.Menu_Common_Click);
            // 
            // m_Herramientas_RepositorioLog
            // 
            this.m_Herramientas_RepositorioLog.Name = "m_Herramientas_RepositorioLog";
            this.m_Herramientas_RepositorioLog.Size = new System.Drawing.Size(175, 22);
            this.m_Herramientas_RepositorioLog.Tag = "Forms.Validador.ComprobantesRepositorioLogForm";
            this.m_Herramientas_RepositorioLog.Text = "m_Repositorio_Log";
            this.m_Herramientas_RepositorioLog.Click += new System.EventHandler(this.Menu_Common_Click);
            // 
            // m_Herramientas_Aspel
            // 
            this.m_Herramientas_Aspel.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Aspel_Banco,
            this.m_Aspel_COI,
            this.m_Aspel_Configuracion});
            this.m_Herramientas_Aspel.Name = "m_Herramientas_Aspel";
            this.m_Herramientas_Aspel.Size = new System.Drawing.Size(175, 22);
            this.m_Herramientas_Aspel.Text = "m_Aspel";
            // 
            // m_Aspel_Banco
            // 
            this.m_Aspel_Banco.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Aspel_Beneficiarios,
            this.m_Aspel_CuentasBancarias,
            this.m_Aspel_Movimientos});
            this.m_Aspel_Banco.Image = global::Jaeger.UI.Properties.Resources.bank_16px;
            this.m_Aspel_Banco.Name = "m_Aspel_Banco";
            this.m_Aspel_Banco.Size = new System.Drawing.Size(166, 22);
            this.m_Aspel_Banco.Text = "m_Banco";
            // 
            // m_Aspel_Beneficiarios
            // 
            this.m_Aspel_Beneficiarios.Name = "m_Aspel_Beneficiarios";
            this.m_Aspel_Beneficiarios.Size = new System.Drawing.Size(188, 22);
            this.m_Aspel_Beneficiarios.Tag = "Banco.Forms.BeneficiariosForm";
            this.m_Aspel_Beneficiarios.Text = "Beneficiarios";
            this.m_Aspel_Beneficiarios.Click += new System.EventHandler(this.Menu_UI_Aspel_Click);
            // 
            // m_Aspel_CuentasBancarias
            // 
            this.m_Aspel_CuentasBancarias.Name = "m_Aspel_CuentasBancarias";
            this.m_Aspel_CuentasBancarias.Size = new System.Drawing.Size(188, 22);
            this.m_Aspel_CuentasBancarias.Tag = "Banco.Forms.CuentasBancariasForm";
            this.m_Aspel_CuentasBancarias.Text = "m_Cuentas_Bancarias";
            this.m_Aspel_CuentasBancarias.Click += new System.EventHandler(this.Menu_UI_Aspel_Click);
            // 
            // m_Aspel_Movimientos
            // 
            this.m_Aspel_Movimientos.Name = "m_Aspel_Movimientos";
            this.m_Aspel_Movimientos.Size = new System.Drawing.Size(188, 22);
            this.m_Aspel_Movimientos.Tag = "Banco.Forms.MovimientosForm";
            this.m_Aspel_Movimientos.Text = "m_Movimientos";
            this.m_Aspel_Movimientos.Click += new System.EventHandler(this.Menu_UI_Aspel_Click);
            // 
            // m_Aspel_COI
            // 
            this.m_Aspel_COI.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_COI_Rubros,
            this.m_COI_Cuentas,
            this.m_COI_Polizas});
            this.m_Aspel_COI.Name = "m_Aspel_COI";
            this.m_Aspel_COI.Size = new System.Drawing.Size(166, 22);
            this.m_Aspel_COI.Text = "m_COI";
            // 
            // m_COI_Rubros
            // 
            this.m_COI_Rubros.Name = "m_COI_Rubros";
            this.m_COI_Rubros.Size = new System.Drawing.Size(133, 22);
            this.m_COI_Rubros.Tag = "COI8.Forms.RubrosCatalogoForm";
            this.m_COI_Rubros.Text = "m_Rubros";
            this.m_COI_Rubros.Click += new System.EventHandler(this.Menu_UI_Aspel_Click);
            // 
            // m_COI_Cuentas
            // 
            this.m_COI_Cuentas.Name = "m_COI_Cuentas";
            this.m_COI_Cuentas.Size = new System.Drawing.Size(133, 22);
            this.m_COI_Cuentas.Tag = "COI8.Forms.CuentasCatalogoForm";
            this.m_COI_Cuentas.Text = "m_Cuentas";
            this.m_COI_Cuentas.Click += new System.EventHandler(this.Menu_UI_Aspel_Click);
            // 
            // m_COI_Polizas
            // 
            this.m_COI_Polizas.Name = "m_COI_Polizas";
            this.m_COI_Polizas.Size = new System.Drawing.Size(133, 22);
            this.m_COI_Polizas.Tag = "COI8.Forms.PolizasCatalogoForm";
            this.m_COI_Polizas.Text = "m_Polizas";
            this.m_COI_Polizas.Click += new System.EventHandler(this.Menu_UI_Aspel_Click);
            // 
            // m_Aspel_Configuracion
            // 
            this.m_Aspel_Configuracion.Image = global::Jaeger.UI.Properties.Resources.settings_16px;
            this.m_Aspel_Configuracion.Name = "m_Aspel_Configuracion";
            this.m_Aspel_Configuracion.Size = new System.Drawing.Size(166, 22);
            this.m_Aspel_Configuracion.Text = "m_Configuracion";
            this.m_Aspel_Configuracion.Click += new System.EventHandler(this.Aspel_Configuracion_Click);
            // 
            // m_Herramientas_DB2
            // 
            this.m_Herramientas_DB2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_db2_Configuracion,
            this.m_db2_Solicitantes});
            this.m_Herramientas_DB2.Image = global::Jaeger.UI.Properties.Resources.db_2_16px;
            this.m_Herramientas_DB2.Name = "m_Herramientas_DB2";
            this.m_Herramientas_DB2.Size = new System.Drawing.Size(175, 22);
            this.m_Herramientas_DB2.Text = "m_AS400";
            // 
            // m_db2_Configuracion
            // 
            this.m_db2_Configuracion.Image = global::Jaeger.UI.Properties.Resources.settings_16px;
            this.m_db2_Configuracion.Name = "m_db2_Configuracion";
            this.m_db2_Configuracion.Size = new System.Drawing.Size(166, 22);
            this.m_db2_Configuracion.Text = "m_Configuración";
            // 
            // m_db2_Solicitantes
            // 
            this.m_db2_Solicitantes.Image = global::Jaeger.UI.Properties.Resources.account_16px;
            this.m_db2_Solicitantes.Name = "m_db2_Solicitantes";
            this.m_db2_Solicitantes.Size = new System.Drawing.Size(166, 22);
            this.m_db2_Solicitantes.Text = "m_Solicitantes";
            // 
            // m_Herramientas_ValidaRFC
            // 
            this.m_Herramientas_ValidaRFC.Name = "m_Herramientas_ValidaRFC";
            this.m_Herramientas_ValidaRFC.Size = new System.Drawing.Size(175, 22);
            this.m_Herramientas_ValidaRFC.Text = "Valida RFC";
            this.m_Herramientas_ValidaRFC.Click += new System.EventHandler(this.Menu_Common_Click);
            // 
            // m_Herramientas_ValidaRetencion
            // 
            this.m_Herramientas_ValidaRetencion.Name = "m_Herramientas_ValidaRetencion";
            this.m_Herramientas_ValidaRetencion.Size = new System.Drawing.Size(175, 22);
            this.m_Herramientas_ValidaRetencion.Text = "Valida Retención";
            this.m_Herramientas_ValidaRetencion.Click += new System.EventHandler(this.Menu_Common_Click);
            // 
            // m_Ventana
            // 
            this.m_Ventana.Name = "m_Ventana";
            this.m_Ventana.Size = new System.Drawing.Size(77, 20);
            this.m_Ventana.Text = "m_Ventana";
            // 
            // m_Ayuda
            // 
            this.m_Ayuda.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Ayuda_AcercaDe});
            this.m_Ayuda.Name = "m_Ayuda";
            this.m_Ayuda.Size = new System.Drawing.Size(69, 20);
            this.m_Ayuda.Text = "m_Ayuda";
            // 
            // m_Ayuda_AcercaDe
            // 
            this.m_Ayuda_AcercaDe.Name = "m_Ayuda_AcercaDe";
            this.m_Ayuda_AcercaDe.Size = new System.Drawing.Size(140, 22);
            this.m_Ayuda_AcercaDe.Text = "m_AcercaDe";
            this.m_Ayuda_AcercaDe.Click += new System.EventHandler(this.Menu_Ayuda_AcercaDe_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 621);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Striker Eureka (Beta)";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem m_Archivo;
        private System.Windows.Forms.ToolStripMenuItem m_Archivo_Salir;
        private System.Windows.Forms.ToolStripMenuItem m_Ventana;
        private System.Windows.Forms.ToolStripMenuItem m_Ayuda;
        private System.Windows.Forms.ToolStripMenuItem m_Ayuda_AcercaDe;
        private System.Windows.Forms.ToolStripMenuItem m_Emision;
        private System.Windows.Forms.ToolStripMenuItem m_Recepcion;
        private System.Windows.Forms.ToolStripMenuItem m_Nominas;
        private System.Windows.Forms.ToolStripMenuItem m_Contabilidad;
        private System.Windows.Forms.ToolStripMenuItem m_Herramientas;
        private System.Windows.Forms.ToolStripMenuItem m_Emision_Contribuyente;
        private System.Windows.Forms.ToolStripMenuItem m_Emision_Comprobante;
        private System.Windows.Forms.ToolStripMenuItem m_Herramientas_Repositorio;
        private System.Windows.Forms.ToolStripMenuItem m_Emision_ReciboFiscal;
        private System.Windows.Forms.ToolStripMenuItem m_Archivo_Session;
        private System.Windows.Forms.ToolStripMenuItem m_Herramientas_RepositorioLog;
        private System.Windows.Forms.ToolStripMenuItem m_Recepcion_Contribuyente;
        private System.Windows.Forms.ToolStripMenuItem m_Recepcion_Comprobante;
        private System.Windows.Forms.ToolStripMenuItem m_Herramientas_Aspel;
        private System.Windows.Forms.ToolStripMenuItem m_Aspel_Banco;
        private System.Windows.Forms.ToolStripMenuItem m_Aspel_COI;
        private System.Windows.Forms.ToolStripMenuItem m_Aspel_Configuracion;
        private System.Windows.Forms.ToolStripMenuItem m_Banco;
        private System.Windows.Forms.ToolStripMenuItem m_Aspel_Beneficiarios;
        private System.Windows.Forms.ToolStripMenuItem m_Aspel_CuentasBancarias;
        private System.Windows.Forms.ToolStripMenuItem m_Aspel_Movimientos;
        private System.Windows.Forms.ToolStripMenuItem m_Recepcion_Validador;
        private System.Windows.Forms.ToolStripDropDownButton Empresa;
        private System.Windows.Forms.ToolStripMenuItem m_COI_Rubros;
        private System.Windows.Forms.ToolStripMenuItem m_COI_Polizas;
        private System.Windows.Forms.ToolStripMenuItem m_COI_Cuentas;
        private System.Windows.Forms.ToolStripMenuItem m_Herramientas_DB2;
        private System.Windows.Forms.ToolStripMenuItem m_db2_Configuracion;
        private System.Windows.Forms.ToolStripMenuItem m_db2_Solicitantes;
        private System.Windows.Forms.ToolStripMenuItem m_Emision_Retenciones;
        private System.Windows.Forms.ToolStripMenuItem m_Archivo_Configuracion;
        private System.Windows.Forms.ToolStripMenuItem m_Herramientas_ValidaRFC;
        private System.Windows.Forms.ToolStripMenuItem m_Herramientas_ValidaRetencion;
    }
}

