﻿namespace Jaeger.UI.Forms.Retenciones {
    partial class ComprobanteRetencionForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TabControl = new System.Windows.Forms.TabControl();
            this.ComprobanteControl = new Jaeger.UI.Forms.Retenciones.ComprobanteFiscalControl();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl.Location = new System.Drawing.Point(0, 331);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(1178, 119);
            this.TabControl.TabIndex = 2;
            // 
            // ComprobanteControl
            // 
            this.ComprobanteControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.ComprobanteControl.Location = new System.Drawing.Point(0, 0);
            this.ComprobanteControl.Name = "ComprobanteControl";
            this.ComprobanteControl.Size = new System.Drawing.Size(1178, 331);
            this.ComprobanteControl.TabIndex = 0;
            // 
            // Comprobante1RetencionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1178, 450);
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.ComprobanteControl);
            this.Name = "Comprobante1RetencionForm";
            this.Text = "Comprobante1RetencionForm";
            this.Load += new System.EventHandler(this.Comprobante1RetencionForm_Load);
            this.ResumeLayout(false);

        }

        #endregion
        protected internal ComprobanteFiscalControl ComprobanteControl;
        protected internal System.Windows.Forms.TabControl TabControl;
    }
}