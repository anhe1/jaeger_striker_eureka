﻿namespace Jaeger.UI.Forms.Retenciones {
    partial class ComplementoPagoAExtranjeroControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.IsBenefEfectDelCobro = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.DescripcionConcepto1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ConceptoPago1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PaisDeResidParaEfecFisc = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.DescripcionConcepto2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ConceptoPago2 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.NomDenRazSocB = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.BeneficiarioCURP = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.BeneficiarioRFC = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // IsBenefEfectDelCobro
            // 
            this.IsBenefEfectDelCobro.AutoSize = true;
            this.IsBenefEfectDelCobro.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.IsBenefEfectDelCobro.Location = new System.Drawing.Point(18, 12);
            this.IsBenefEfectDelCobro.Name = "IsBenefEfectDelCobro";
            this.IsBenefEfectDelCobro.Size = new System.Drawing.Size(290, 17);
            this.IsBenefEfectDelCobro.TabIndex = 0;
            this.IsBenefEfectDelCobro.Text = "El beneficiario del pago es la misma persona que retiene";
            this.IsBenefEfectDelCobro.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.IsBenefEfectDelCobro.UseVisualStyleBackColor = true;
            this.IsBenefEfectDelCobro.CheckedChanged += new System.EventHandler(this.IsBenefEfectDelCobro_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DescripcionConcepto1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.ConceptoPago1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.PaisDeResidParaEfecFisc);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(465, 124);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "No Beneficiario";
            // 
            // DescripcionConcepto1
            // 
            this.DescripcionConcepto1.Location = new System.Drawing.Point(15, 94);
            this.DescripcionConcepto1.MaxLength = 255;
            this.DescripcionConcepto1.Name = "DescripcionConcepto1";
            this.DescripcionConcepto1.Size = new System.Drawing.Size(439, 20);
            this.DescripcionConcepto1.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(322, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Descripción de la definición del pago del residente en el extranjero:";
            // 
            // ConceptoPago1
            // 
            this.ConceptoPago1.FormattingEnabled = true;
            this.ConceptoPago1.Location = new System.Drawing.Point(226, 47);
            this.ConceptoPago1.Name = "ConceptoPago1";
            this.ConceptoPago1.Size = new System.Drawing.Size(228, 21);
            this.ConceptoPago1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(196, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tipo contribuyente sujeto a la retención:";
            // 
            // PaisDeResidParaEfecFisc
            // 
            this.PaisDeResidParaEfecFisc.FormattingEnabled = true;
            this.PaisDeResidParaEfecFisc.Location = new System.Drawing.Point(226, 20);
            this.PaisDeResidParaEfecFisc.Name = "PaisDeResidParaEfecFisc";
            this.PaisDeResidParaEfecFisc.Size = new System.Drawing.Size(228, 21);
            this.PaisDeResidParaEfecFisc.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(210, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Clave del país de residencia del extranjero:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.DescripcionConcepto2);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.ConceptoPago2);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.NomDenRazSocB);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.BeneficiarioCURP);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.BeneficiarioRFC);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(3, 133);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(465, 196);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Beneficiario";
            // 
            // DescripcionConcepto2
            // 
            this.DescripcionConcepto2.Location = new System.Drawing.Point(15, 166);
            this.DescripcionConcepto2.MaxLength = 255;
            this.DescripcionConcepto2.Name = "DescripcionConcepto2";
            this.DescripcionConcepto2.Size = new System.Drawing.Size(439, 20);
            this.DescripcionConcepto2.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 150);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(322, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Descripción de la definición del pago del residente en el extranjero:";
            // 
            // ConceptoPago2
            // 
            this.ConceptoPago2.FormattingEnabled = true;
            this.ConceptoPago2.Location = new System.Drawing.Point(226, 119);
            this.ConceptoPago2.Name = "ConceptoPago2";
            this.ConceptoPago2.Size = new System.Drawing.Size(228, 21);
            this.ConceptoPago2.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 123);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(196, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Tipo contribuyente sujeto a la retención:";
            // 
            // NomDenRazSocB
            // 
            this.NomDenRazSocB.Location = new System.Drawing.Point(15, 93);
            this.NomDenRazSocB.MaxLength = 300;
            this.NomDenRazSocB.Name = "NomDenRazSocB";
            this.NomDenRazSocB.Size = new System.Drawing.Size(439, 20);
            this.NomDenRazSocB.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(333, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "El nombre, denominación o razón social del contribuyente en México:";
            // 
            // BeneficiarioCURP
            // 
            this.BeneficiarioCURP.Location = new System.Drawing.Point(340, 47);
            this.BeneficiarioCURP.Name = "BeneficiarioCURP";
            this.BeneficiarioCURP.Size = new System.Drawing.Size(114, 20);
            this.BeneficiarioCURP.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "CURP del representante legal:";
            // 
            // BeneficiarioRFC
            // 
            this.BeneficiarioRFC.Location = new System.Drawing.Point(340, 21);
            this.BeneficiarioRFC.Name = "BeneficiarioRFC";
            this.BeneficiarioRFC.Size = new System.Drawing.Size(114, 20);
            this.BeneficiarioRFC.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(240, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Clave del RFC del representante legal en México:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.IsBenefEfectDelCobro);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(475, 35);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.groupBox1);
            this.flowLayoutPanel1.Controls.Add(this.groupBox2);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 35);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(475, 332);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // ComplementoPagoAExtranjeroControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.groupBox3);
            this.Name = "ComplementoPagoAExtranjeroControl";
            this.Size = new System.Drawing.Size(475, 367);
            this.Load += new System.EventHandler(this.ComplementoPagoAExtranjeroControl_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        protected internal System.Windows.Forms.CheckBox IsBenefEfectDelCobro;
        protected internal System.Windows.Forms.TextBox DescripcionConcepto1;
        protected internal System.Windows.Forms.ComboBox ConceptoPago1;
        protected internal System.Windows.Forms.ComboBox PaisDeResidParaEfecFisc;
        protected internal System.Windows.Forms.TextBox DescripcionConcepto2;
        protected internal System.Windows.Forms.ComboBox ConceptoPago2;
        protected internal System.Windows.Forms.TextBox NomDenRazSocB;
        protected internal System.Windows.Forms.TextBox BeneficiarioCURP;
        protected internal System.Windows.Forms.TextBox BeneficiarioRFC;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}
