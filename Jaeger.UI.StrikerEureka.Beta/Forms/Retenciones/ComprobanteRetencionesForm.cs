﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Retencion.Contracts;
using Jaeger.Aplication.Retencion.Services;
using Jaeger.Domain.Retencion.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Forms.Retenciones {
    public partial class ComprobanteRetencionesForm : Form {
        private readonly ToolStripMenuItem TCrearTablaRetenciones = new ToolStripMenuItem() { Text = "Crear tabla carta porte" };
        private readonly ToolStripMenuItem TImportar = new ToolStripMenuItem() { Text = "Importar" };

        protected IComprobanteRetencionService service;
        private BindingList<RetencionSingle> retenciones;

        public ComprobanteRetencionesForm() {
            InitializeComponent();
        }

        #region barra de herramientas
        private void RetencionesForm_Load(object sender, EventArgs e) {
            this.service = new ComprobanteRetencionService();
            this.gridData.DataGridCommon();
            this.TRetencion.Herramientas.DropDownItems.Add(this.TCrearTablaRetenciones);
            this.TRetencion.Herramientas.DropDownItems.Add(this.TImportar);
            this.TCrearTablaRetenciones.Click += this.TRetencion_CrearTabla_Click;
            this.TImportar.Click += this.TRetencion_Importar_Click;
        }

        public virtual void TRetencion_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new ComprobanteRetencionForm(null, Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido) { MdiParent = this.MdiParent };
            _nuevo.Show();
        }

        public virtual void TRetencion_Editar_Click(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                var _seleccionado = this.gridData.CurrentRow.DataBoundItem as RetencionSingle;
                if (_seleccionado != null) {
                    var _editar = new ComprobanteRetencionForm(new ComprobanteRetencionDetailModel { Id = _seleccionado.Id }, Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido) { MdiParent = this.ParentForm };
                    _editar.Show();
                }
            }
        }

        public virtual void TRetencion_Importar_Click(object sender, EventArgs eventArgs) {
            var openFile = new OpenFileDialog() { Filter = "*.xml|*.XML" };
            if (openFile.ShowDialog(this) == DialogResult.OK) {

            this.service.Load(openFile.FileName);
            }
        }

        public virtual void TRetencion_Actualizar_Click(object sender, EventArgs e) {
            this.retenciones = this.service.GetList(this.TRetencion.GetEjercicio(), this.TRetencion.GetMes());
            this.gridData.DataSource = this.retenciones;
        }

        public virtual void TRetencion_CrearTabla_Click(object sender, EventArgs e) {
            if (MessageBox.Show(this, "¿Esta seguro de crear tabla correspondiente a Carta Porte?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                using (var espera = new WaitingForm(this.Crear_TablaRetenciones)) {
                    espera.Text = "Creando tablas del sistema";
                    espera.ShowDialog(this);
                }
            }
        }

        public virtual void TRetencion_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region metodos privados
        private void Crear_TablaRetenciones() {
            this.service.Create();
        }
        #endregion
    }
}
