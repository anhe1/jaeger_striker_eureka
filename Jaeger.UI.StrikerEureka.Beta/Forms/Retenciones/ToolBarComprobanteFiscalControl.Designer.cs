﻿namespace Jaeger.UI.Forms.Retenciones {
    partial class ToolBarComprobanteFiscalControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToolBarComprobanteFiscalControl));
            this.ToolBar = new System.Windows.Forms.ToolStrip();
            this.Emisor = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.Status = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.Nuevo = new System.Windows.Forms.ToolStripButton();
            this.Duplicar = new System.Windows.Forms.ToolStripButton();
            this.Guardar = new System.Windows.Forms.ToolStripButton();
            this.Actualizar = new System.Windows.Forms.ToolStripButton();
            this.Certificar = new System.Windows.Forms.ToolStripButton();
            this.Cancelar = new System.Windows.Forms.ToolStripButton();
            this.FilePDF = new System.Windows.Forms.ToolStripButton();
            this.FileXML = new System.Windows.Forms.ToolStripButton();
            this.Correo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.Complementos = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.UUID = new System.Windows.Forms.ToolStripLabel();
            this.IdDocumento = new System.Windows.Forms.ToolStripTextBox();
            this.Cerrar = new System.Windows.Forms.ToolStripButton();
            this.ToolBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Emisor,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.Status,
            this.toolStripSeparator2,
            this.Nuevo,
            this.Duplicar,
            this.Guardar,
            this.Actualizar,
            this.Certificar,
            this.Cancelar,
            this.FilePDF,
            this.FileXML,
            this.Correo,
            this.toolStripSeparator3,
            this.Complementos,
            this.toolStripSeparator4,
            this.UUID,
            this.IdDocumento,
            this.Cerrar});
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Size = new System.Drawing.Size(1256, 25);
            this.ToolBar.TabIndex = 0;
            this.ToolBar.Text = "toolStrip1";
            // 
            // Emisor
            // 
            this.Emisor.Image = ((System.Drawing.Image)(resources.GetObject("Emisor.Image")));
            this.Emisor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Emisor.Name = "Emisor";
            this.Emisor.Size = new System.Drawing.Size(130, 22);
            this.Emisor.Text = "XXXXXXXXXXXXX";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(39, 22);
            this.toolStripLabel1.Text = "Status";
            // 
            // Status
            // 
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(90, 25);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // Nuevo
            // 
            this.Nuevo.Image = global::Jaeger.UI.Properties.Resources.new_16px;
            this.Nuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Size = new System.Drawing.Size(62, 22);
            this.Nuevo.Text = "Nuevo";
            // 
            // Duplicar
            // 
            this.Duplicar.Image = global::Jaeger.UI.Properties.Resources.copy_16px;
            this.Duplicar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Duplicar.Name = "Duplicar";
            this.Duplicar.Size = new System.Drawing.Size(71, 22);
            this.Duplicar.Text = "Duplicar";
            // 
            // Guardar
            // 
            this.Guardar.Image = global::Jaeger.UI.Properties.Resources.save_16px;
            this.Guardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(69, 22);
            this.Guardar.Text = "Guardar";
            // 
            // Actualizar
            // 
            this.Actualizar.Image = global::Jaeger.UI.Properties.Resources.refresh_16px;
            this.Actualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Size = new System.Drawing.Size(79, 22);
            this.Actualizar.Text = "Actualizar";
            // 
            // Certificar
            // 
            this.Certificar.Image = global::Jaeger.UI.Properties.Resources.approval_16px;
            this.Certificar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Certificar.Name = "Certificar";
            this.Certificar.Size = new System.Drawing.Size(68, 22);
            this.Certificar.Text = "Timbrar";
            this.Certificar.Visible = false;
            // 
            // Cancelar
            // 
            this.Cancelar.Image = global::Jaeger.UI.Properties.Resources.cancel_16;
            this.Cancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Size = new System.Drawing.Size(73, 22);
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.Visible = false;
            // 
            // FilePDF
            // 
            this.FilePDF.Image = global::Jaeger.UI.Properties.Resources.pdf_16px;
            this.FilePDF.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FilePDF.Name = "FilePDF";
            this.FilePDF.Size = new System.Drawing.Size(48, 22);
            this.FilePDF.Text = "PDF";
            this.FilePDF.Visible = false;
            // 
            // FileXML
            // 
            this.FileXML.Image = global::Jaeger.UI.Properties.Resources.xml_16px;
            this.FileXML.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FileXML.Name = "FileXML";
            this.FileXML.Size = new System.Drawing.Size(51, 22);
            this.FileXML.Text = "XML";
            this.FileXML.Visible = false;
            // 
            // Correo
            // 
            this.Correo.Image = global::Jaeger.UI.Properties.Resources.send_16px;
            this.Correo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Correo.Name = "Correo";
            this.Correo.Size = new System.Drawing.Size(63, 22);
            this.Correo.Text = "Correo";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // Complementos
            // 
            this.Complementos.Image = global::Jaeger.UI.Properties.Resources.plugin_16px;
            this.Complementos.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Complementos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Complementos.Name = "Complementos";
            this.Complementos.Size = new System.Drawing.Size(118, 22);
            this.Complementos.Text = "Complementos";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // UUID
            // 
            this.UUID.Name = "UUID";
            this.UUID.Size = new System.Drawing.Size(37, 22);
            this.UUID.Text = "UUID:";
            // 
            // IdDocumento
            // 
            this.IdDocumento.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.IdDocumento.MaxLength = 36;
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.Size = new System.Drawing.Size(200, 25);
            // 
            // Cerrar
            // 
            this.Cerrar.Image = global::Jaeger.UI.Properties.Resources.close_window_16px;
            this.Cerrar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(59, 20);
            this.Cerrar.Text = "Cerrar";
            // 
            // ToolBarComprobanteFiscalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ToolBar);
            this.Name = "ToolBarComprobanteFiscalControl";
            this.Size = new System.Drawing.Size(1256, 25);
            this.Load += new System.EventHandler(this.ToolBarComprobanteFiscalControl_Load);
            this.ToolBar.ResumeLayout(false);
            this.ToolBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected internal System.Windows.Forms.ToolStrip ToolBar;
        protected internal System.Windows.Forms.ToolStripSplitButton Emisor;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        protected internal System.Windows.Forms.ToolStripComboBox Status;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        protected internal System.Windows.Forms.ToolStripButton Nuevo;
        protected internal System.Windows.Forms.ToolStripButton Duplicar;
        protected internal System.Windows.Forms.ToolStripButton Guardar;
        protected internal System.Windows.Forms.ToolStripButton Actualizar;
        protected internal System.Windows.Forms.ToolStripButton Certificar;
        protected internal System.Windows.Forms.ToolStripButton Cancelar;
        protected internal System.Windows.Forms.ToolStripButton FilePDF;
        protected internal System.Windows.Forms.ToolStripButton FileXML;
        protected internal System.Windows.Forms.ToolStripButton Correo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripLabel UUID;
        protected internal System.Windows.Forms.ToolStripTextBox IdDocumento;
        protected internal System.Windows.Forms.ToolStripButton Cerrar;
        protected internal System.Windows.Forms.ToolStripDropDownButton Complementos;
    }
}
