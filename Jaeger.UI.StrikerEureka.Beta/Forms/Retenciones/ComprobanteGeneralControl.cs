﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Retenciones {
    public partial class ComprobanteGeneralControl : UserControl {
        protected BindingList<ComprobanteContribuyenteModel> contribuyenteModels;
        protected IContribuyenteService service;
        protected IRetencionesCatalogo retencionesCatalogo;
        protected IRetencionTipoImpuestoCatalogo catalogoTipoImpuesto;
        protected IRetencionTipoPagoCatalogo catalogoTipoPago;
        

        public ComprobanteGeneralControl() {
            InitializeComponent();
        }

        private void ComprobanteGeneralControl_Load(object sender, EventArgs e) {
            this.Folio.TextBoxOnlyNumbers();
            this.gridImpuestos.DataGridCommon();
            this.MesInicial.ValueMember = "Id";
            this.MesInicial.DisplayMember = "Descripcion";
            this.MesInicial.DataSource = ConfigService.GetMeses();
            this.MesFinal.ValueMember = "Id";
            this.MesFinal.DisplayMember = "Descripcion";
            this.MesFinal.DataSource = ConfigService.GetMeses();
        }

        public virtual void Start() {
            this.retencionesCatalogo = new RetencionesCatalogo();
            this.retencionesCatalogo.Load();

            this.catalogoTipoImpuesto = new RetencionTipoImpuestoCatalogo();
            this.catalogoTipoPago = new RetencionTipoPagoCatalogo();

            this.CveRetenc.DisplayMember = "Descriptor";
            this.CveRetenc.ValueMember = "Clave";
            this.CveRetenc.DataSource = this.retencionesCatalogo.Items;

            this.TipoImpuesto.ValueMember = "Clave";
            this.TipoImpuesto.DisplayMember = "Descriptor";
            this.TipoImpuesto.DataSource = this.catalogoTipoImpuesto.Items;

            this.TipoPagoRet.ValueMember = "Clave";
            this.TipoPagoRet.DisplayMember = "Descriptor";
            this.TipoPagoRet.DataSource = this.catalogoTipoPago.Items;
        }

        public virtual void StartContribuyente(CFDISubTipoEnum subTipo) {
            this.service = new ContribuyenteService(subTipo);
            this.Receptor.Enabled = false;
            this.preparar_contribuyentes.RunWorkerAsync();
        }

        private void PrepararContribuyentes_DoWork(object sender, DoWorkEventArgs e) {
            this.buttonActualizarDirectorio.Enabled = false;
            this.Receptor.Enabled = false;
            this.contribuyenteModels = this.service.GetContribuyentes();
            
        }

        private void PrepararContribuyentes_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Receptor.SelectedIndex = -1;
            this.Receptor.SelectedValue = null;
            this.Receptor.Enabled = true;
            this.buttonActualizarDirectorio.Enabled = true;
            this.Receptor.Enabled = true;
        }

        private void Receptor_DropDown(object sender, EventArgs e) {
            this.Receptor.DataSource = this.contribuyenteModels;
        }

        private void Receptor_SelectedValueChanged(object sender, EventArgs e) {
            var _seleccionado = this.Receptor.SelectedItem as ComprobanteContribuyenteModel;
            if (_seleccionado != null) {
                this.ReceptorRFC.Text = _seleccionado.RFC;
                this.IdDirectorio.Text = _seleccionado.IdDirectorio.ToString();
            } else {
                MessageBox.Show(this, "Properties.Resources.Message_Objeto_NoValido", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
