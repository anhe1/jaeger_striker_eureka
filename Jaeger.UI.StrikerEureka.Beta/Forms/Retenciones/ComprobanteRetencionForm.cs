﻿using System;
using System.Windows.Forms;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Retencion.Entities;

namespace Jaeger.UI.Forms.Retenciones {
    public partial class ComprobanteRetencionForm : Form {
        protected internal ToolStripMenuItem DesdeArchivo = new ToolStripMenuItem { Text = "Desde archivo" };
        protected internal ToolStripMenuItem ComplementoPagoAExtranjero = new ToolStripMenuItem { Text = "Pago A Extranjero" };
        protected internal ToolStripMenuItem ComplementoDividendos = new ToolStripMenuItem { Text = "Dividendos" };
        protected CFDISubTipoEnum subTipo;
        protected internal ComplementoPagoAExtranjeroControl pagoAExtranjeroControl = new ComplementoPagoAExtranjeroControl() { Dock = DockStyle.Fill };

        public ComprobanteRetencionForm(ComprobanteRetencionDetailModel model, CFDISubTipoEnum subTipo) {
            InitializeComponent();
            this.subTipo = subTipo;
            if (model != null) {
                this.ComprobanteControl.Comprobante = model;
            }
        }

        private void Comprobante1RetencionForm_Load(object sender, EventArgs e) {
            this.ComprobanteControl.Start(this.subTipo);
            this.ComprobanteControl.BindingCompleted += ComprobanteControl_BindingCompleted;
            this.ComprobanteControl.TRetencion.Complementos.DropDownItems.Add(this.DesdeArchivo);
            this.ComprobanteControl.TRetencion.Complementos.DropDownItems.Add(this.ComplementoPagoAExtranjero);
            this.ComprobanteControl.TRetencion.Complementos.DropDownItems.Add(this.ComplementoDividendos);
            this.DesdeArchivo.Click += this.DesdeArchivo_Click;
            this.ComplementoPagoAExtranjero.Click += ComplementoPagoAExtranjero_Click;
            this.ComprobanteControl.TRetencion.Cerrar.Click += this.Cerrar_Click;
        }

        #region barra de heramientas
        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void DesdeArchivo_Click(object sender, EventArgs e) {
            var _openFile = new OpenFileDialog();
            if (_openFile.ShowDialog() == DialogResult.OK) {
                this.ComprobanteControl.Comprobante = this.ComprobanteControl.Service.GetComprobante(_openFile.FileName);
                this.ComprobanteControl.TRetencion.Actualizar.PerformClick();
            }
        }

        private void ComprobanteControl_BindingCompleted(object sender, EventArgs e) {
            if (ComprobanteControl.Comprobante.Pagosaextranjeros != null) {
                this.ComplementoPagoAExtranjero_Click(sender, null);
            }
        }

        private void ComplementoPagoAExtranjero_Click(object sender, EventArgs e) {
            var _page = new TabPage() { Text = "Complemento: Pago A Extranjero", Padding = new Padding(3), UseVisualStyleBackColor = true, AutoScroll = true };
            this.pagoAExtranjeroControl.Start();
            _page.Controls.Add(this.pagoAExtranjeroControl);
            this.TabControl.TabPages.Add(_page);

            if (this.ComprobanteControl.Comprobante.Pagosaextranjeros == null) {
                this.ComprobanteControl.Comprobante.Pagosaextranjeros = new Domain.Retencion.Entities.Complemento.ComplementoPagosaextranjeros();
            }
            this.pagoAExtranjeroControl.Complemento = this.ComprobanteControl.Comprobante.Pagosaextranjeros;
            this.pagoAExtranjeroControl.CreateBinding();
        }
        #endregion
    }
}
