﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Retencion.Entities.Complemento;

namespace Jaeger.UI.Forms.Retenciones {
    public partial class ComplementoPagoAExtranjeroControl : UserControl {
        public ComplementoPagosaextranjeros Complemento;
        protected IRetencionTipoContribuyenteSujetoCatalogo tipoContribuyenteSujetoCatalogo;
        protected IRetencionPaisesCatalogo catalogoPaises;
        protected BindingList<ClaveRetencionTipoContribuyenteSujeto> tipos1;
        protected BindingList<ClaveRetencionTipoContribuyenteSujeto> tipos2;

        public ComplementoPagoAExtranjeroControl() {
            InitializeComponent();
        }

        private void ComplementoPagoAExtranjeroControl_Load(object sender, EventArgs e) {

        }

        public virtual void Start() {
            this.tipoContribuyenteSujetoCatalogo = new RetencionTipoContribuyenteSujetoCatalogo();
            this.tipoContribuyenteSujetoCatalogo.Load();

            this.catalogoPaises = new RetencionPaisesCatalogo();
            this.catalogoPaises.Load();

            this.PaisDeResidParaEfecFisc.DisplayMember = "Descriptor";
            this.PaisDeResidParaEfecFisc.ValueMember = "Clave";
            this.PaisDeResidParaEfecFisc.DataSource = this.catalogoPaises.Items;

            this.tipos1 = new BindingList<ClaveRetencionTipoContribuyenteSujeto>(this.tipoContribuyenteSujetoCatalogo.Items);
            this.tipos2 = new BindingList<ClaveRetencionTipoContribuyenteSujeto>(this.tipoContribuyenteSujetoCatalogo.Items);
            
            this.ConceptoPago1.DisplayMember = "Descriptor";
            this.ConceptoPago1.ValueMember = "Clave";
            this.ConceptoPago1.DataSource = this.tipos1;

            this.ConceptoPago2.DisplayMember = "Descriptor";
            this.ConceptoPago2.ValueMember = "Clave";
            this.ConceptoPago2.DataSource = this.tipos2;
        }

        public virtual void CreateBinding() {
            this.IsBenefEfectDelCobro.DataBindings.Clear();
            this.IsBenefEfectDelCobro.DataBindings.Add("Checked", this.Complemento, "IsBenefEfectDelCobro", true, DataSourceUpdateMode.OnPropertyChanged);

            this.PaisDeResidParaEfecFisc.DataBindings.Clear();
            this.PaisDeResidParaEfecFisc.DataBindings.Add("SelectedValue", this.Complemento.NoBeneficiario, "PaisDeResidParaEfecFisc", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ConceptoPago1.DataBindings.Clear();
            this.ConceptoPago1.DataBindings.Add("SelectedValue", this.Complemento.NoBeneficiario, "ConceptoPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.DescripcionConcepto1.DataBindings.Clear();
            this.DescripcionConcepto1.DataBindings.Add("Text", this.Complemento.NoBeneficiario, "DescripcionConcepto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.BeneficiarioRFC.DataBindings.Clear();
            this.BeneficiarioRFC.DataBindings.Add("Text", this.Complemento.Beneficiario, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);
            this.BeneficiarioCURP.DataBindings.Clear();
            this.BeneficiarioCURP.DataBindings.Add("Text", this.Complemento.Beneficiario, "CURP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NomDenRazSocB.DataBindings.Clear();
            this.NomDenRazSocB.DataBindings.Add("Text", this.Complemento.Beneficiario, "NomDenRazSocB", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ConceptoPago2.DataBindings.Clear();
            this.ConceptoPago2.DataBindings.Add("SelectedValue", this.Complemento.NoBeneficiario, "ConceptoPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.DescripcionConcepto2.DataBindings.Clear();
            this.DescripcionConcepto2.DataBindings.Add("Text", this.Complemento.Beneficiario, "DescripcionConcepto", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        public virtual void IsBenefEfectDelCobro_CheckedChanged(object sender, EventArgs e) {
            this.groupBox1.Enabled = this.IsBenefEfectDelCobro.Checked == true;
            this.groupBox2.Enabled = !this.IsBenefEfectDelCobro.Checked == true;
        }
    }
}
