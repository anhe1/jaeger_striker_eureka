﻿namespace Jaeger.UI.Forms.Retenciones {
    partial class ComprobanteRetencionesForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TRetencion = new Jaeger.UI.Common.Forms.ToolBar1CommonControl();
            this.gridData = new System.Windows.Forms.DataGridView();
            this.EmisorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorExtranjero = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.IdDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MesInicial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MesFinal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ejercicio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MontoTotalOperacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MontoTotalGravado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MontoTotalExento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MontoTotalRetencion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaEmision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaTimbre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Version = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveRetencion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            this.SuspendLayout();
            // 
            // TRetencion
            // 
            this.TRetencion.Dock = System.Windows.Forms.DockStyle.Top;
            this.TRetencion.Location = new System.Drawing.Point(0, 0);
            this.TRetencion.Name = "TRetencion";
            this.TRetencion.ShowActualizar = true;
            this.TRetencion.ShowCancelar = true;
            this.TRetencion.ShowCerrar = true;
            this.TRetencion.ShowEditar = true;
            this.TRetencion.ShowHerramientas = true;
            this.TRetencion.ShowImprimir = false;
            this.TRetencion.ShowNuevo = true;
            this.TRetencion.ShowPeriodo = true;
            this.TRetencion.Size = new System.Drawing.Size(800, 25);
            this.TRetencion.TabIndex = 0;
            this.TRetencion.Nuevo.Click += this.TRetencion_Nuevo_Click;
            this.TRetencion.Editar.Click += this.TRetencion_Editar_Click;
            this.TRetencion.Actualizar.Click += this.TRetencion_Actualizar_Click;
            this.TRetencion.Cerrar.Click += this.TRetencion_Cerrar_Click;
            // 
            // gridData
            // 
            this.gridData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EmisorRFC,
            this.ReceptorRFC,
            this.ReceptorNombre,
            this.ReceptorExtranjero,
            this.IdDocumento,
            this.Descripcion,
            this.MesInicial,
            this.MesFinal,
            this.Ejercicio,
            this.MontoTotalOperacion,
            this.MontoTotalGravado,
            this.MontoTotalExento,
            this.MontoTotalRetencion,
            this.FechaEmision,
            this.FechaTimbre,
            this.Version,
            this.ClaveRetencion,
            this.Estado,
            this.Folio,
            this.Serie});
            this.gridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridData.Location = new System.Drawing.Point(0, 25);
            this.gridData.Name = "gridData";
            this.gridData.Size = new System.Drawing.Size(800, 425);
            this.gridData.TabIndex = 1;
            // 
            // EmisorRFC
            // 
            this.EmisorRFC.DataPropertyName = "EmisorRFC";
            this.EmisorRFC.HeaderText = "EmisorRFC";
            this.EmisorRFC.Name = "EmisorRFC";
            this.EmisorRFC.ReadOnly = true;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.DataPropertyName = "ReceptorRFC";
            this.ReceptorRFC.HeaderText = "ReceptorRFC";
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.ReadOnly = true;
            // 
            // ReceptorNombre
            // 
            this.ReceptorNombre.DataPropertyName = "ReceptorNombre";
            this.ReceptorNombre.HeaderText = "ReceptorNombre";
            this.ReceptorNombre.Name = "ReceptorNombre";
            this.ReceptorNombre.ReadOnly = true;
            // 
            // ReceptorExtranjero
            // 
            this.ReceptorExtranjero.DataPropertyName = "ReceptorExtranjero";
            this.ReceptorExtranjero.HeaderText = "Extranjero";
            this.ReceptorExtranjero.Name = "ReceptorExtranjero";
            this.ReceptorExtranjero.ReadOnly = true;
            this.ReceptorExtranjero.Width = 50;
            // 
            // IdDocumento
            // 
            this.IdDocumento.DataPropertyName = "IdDocumento";
            this.IdDocumento.HeaderText = "IdDocumento";
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.ReadOnly = true;
            // 
            // Descripcion
            // 
            this.Descripcion.DataPropertyName = "Descripcion";
            this.Descripcion.HeaderText = "Descripcion";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.ReadOnly = true;
            // 
            // MesInicial
            // 
            this.MesInicial.DataPropertyName = "MesInicial";
            this.MesInicial.HeaderText = "MesInicial";
            this.MesInicial.Name = "MesInicial";
            this.MesInicial.ReadOnly = true;
            this.MesInicial.Width = 50;
            // 
            // MesFinal
            // 
            this.MesFinal.DataPropertyName = "MesFinal";
            this.MesFinal.HeaderText = "MesFinal";
            this.MesFinal.Name = "MesFinal";
            this.MesFinal.ReadOnly = true;
            this.MesFinal.Width = 50;
            // 
            // Ejercicio
            // 
            this.Ejercicio.DataPropertyName = "Ejercicio";
            this.Ejercicio.HeaderText = "Ejercicio";
            this.Ejercicio.Name = "Ejercicio";
            this.Ejercicio.ReadOnly = true;
            this.Ejercicio.Width = 50;
            // 
            // MontoTotalOperacion
            // 
            this.MontoTotalOperacion.DataPropertyName = "MontoTotalOperacion";
            this.MontoTotalOperacion.HeaderText = "MontoTotalOperacion";
            this.MontoTotalOperacion.Name = "MontoTotalOperacion";
            this.MontoTotalOperacion.ReadOnly = true;
            // 
            // MontoTotalGravado
            // 
            this.MontoTotalGravado.DataPropertyName = "MontoTotalGravado";
            this.MontoTotalGravado.HeaderText = "MontoTotalGravado";
            this.MontoTotalGravado.Name = "MontoTotalGravado";
            this.MontoTotalGravado.ReadOnly = true;
            // 
            // MontoTotalExento
            // 
            this.MontoTotalExento.DataPropertyName = "MontoTotalExento";
            this.MontoTotalExento.HeaderText = "MontoTotalExento";
            this.MontoTotalExento.Name = "MontoTotalExento";
            this.MontoTotalExento.ReadOnly = true;
            // 
            // MontoTotalRetencion
            // 
            this.MontoTotalRetencion.DataPropertyName = "MontoTotalRetencion";
            this.MontoTotalRetencion.HeaderText = "MontoTotalRetencion";
            this.MontoTotalRetencion.Name = "MontoTotalRetencion";
            this.MontoTotalRetencion.ReadOnly = true;
            // 
            // FechaEmision
            // 
            this.FechaEmision.DataPropertyName = "FechaEmision";
            this.FechaEmision.HeaderText = "FechaEmision";
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.ReadOnly = true;
            // 
            // FechaTimbre
            // 
            this.FechaTimbre.DataPropertyName = "FechaTimbre";
            this.FechaTimbre.HeaderText = "FechaTimbre";
            this.FechaTimbre.Name = "FechaTimbre";
            this.FechaTimbre.ReadOnly = true;
            // 
            // Version
            // 
            this.Version.DataPropertyName = "Version";
            this.Version.HeaderText = "Version";
            this.Version.Name = "Version";
            this.Version.ReadOnly = true;
            // 
            // ClaveRetencion
            // 
            this.ClaveRetencion.DataPropertyName = "ClaveRetencion";
            this.ClaveRetencion.HeaderText = "ClaveRetencion";
            this.ClaveRetencion.Name = "ClaveRetencion";
            this.ClaveRetencion.ReadOnly = true;
            // 
            // Estado
            // 
            this.Estado.DataPropertyName = "Estado";
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            this.Estado.ReadOnly = true;
            // 
            // Folio
            // 
            this.Folio.DataPropertyName = "Folio";
            this.Folio.HeaderText = "Folio";
            this.Folio.Name = "Folio";
            this.Folio.ReadOnly = true;
            // 
            // Serie
            // 
            this.Serie.DataPropertyName = "Serie";
            this.Serie.HeaderText = "Serie";
            this.Serie.Name = "Serie";
            this.Serie.ReadOnly = true;
            // 
            // ComprobanteRetencionesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.gridData);
            this.Controls.Add(this.TRetencion);
            this.Name = "ComprobanteRetencionesForm";
            this.Text = "RetencionesForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RetencionesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBar1CommonControl TRetencion;
        private System.Windows.Forms.DataGridView gridData;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmisorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorNombre;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ReceptorExtranjero;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn MesInicial;
        private System.Windows.Forms.DataGridViewTextBoxColumn MesFinal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ejercicio;
        private System.Windows.Forms.DataGridViewTextBoxColumn MontoTotalOperacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn MontoTotalGravado;
        private System.Windows.Forms.DataGridViewTextBoxColumn MontoTotalExento;
        private System.Windows.Forms.DataGridViewTextBoxColumn MontoTotalRetencion;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaEmision;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaTimbre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Version;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveRetencion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Serie;
    }
}