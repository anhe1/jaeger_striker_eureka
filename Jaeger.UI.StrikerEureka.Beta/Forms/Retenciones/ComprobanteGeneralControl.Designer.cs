﻿namespace Jaeger.UI.Forms.Retenciones {
    partial class ComprobanteGeneralControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.IdDirectorio = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonActualizarDirectorio = new System.Windows.Forms.Button();
            this.NumRegistro = new System.Windows.Forms.TextBox();
            this.ResidenciaFiscal = new System.Windows.Forms.TextBox();
            this.FechaTimbre = new System.Windows.Forms.DateTimePicker();
            this.CURP = new System.Windows.Forms.TextBox();
            this.ReceptorRFC = new System.Windows.Forms.TextBox();
            this.Receptor = new System.Windows.Forms.ComboBox();
            this.FechaEmision = new System.Windows.Forms.DateTimePicker();
            this.Folio = new System.Windows.Forms.TextBox();
            this.Serie = new System.Windows.Forms.ComboBox();
            this.Documento = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.preparar_contribuyentes = new System.ComponentModel.BackgroundWorker();
            this.providerError = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gridImpuestos = new System.Windows.Forms.DataGridView();
            this.BaseRet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoImpuesto = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.montoRet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoPagoRet = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TImpuesto = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.MesFinal = new System.Windows.Forms.ComboBox();
            this.CveRetenc = new System.Windows.Forms.ComboBox();
            this.MesInicial = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.Descripcion = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Ejercicio = new System.Windows.Forms.NumericUpDown();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LugarExpedicion = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.ReceptorDomicilioFiscal = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.Extranjero = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.MontoTotalOperacion = new System.Windows.Forms.NumericUpDown();
            this.MontoTotalGravado = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.MontoTotalExento = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.MontoTotalRetencion = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabComprobante = new System.Windows.Forms.TabPage();
            this.tabCFDIRelacionado = new System.Windows.Forms.TabPage();
            this.ISRCorrespondiente = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.UtilidadBimestral = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.Totales = new System.Windows.Forms.GroupBox();
            this.Version = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.comprobanteRelacionadoControl1 = new Jaeger.UI.Forms.Comprobantes.ComprobanteRelacionadoControl();
            ((System.ComponentModel.ISupportInitialize)(this.providerError)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridImpuestos)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalOperacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalGravado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalExento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalRetencion)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabComprobante.SuspendLayout();
            this.tabCFDIRelacionado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ISRCorrespondiente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UtilidadBimestral)).BeginInit();
            this.Totales.SuspendLayout();
            this.SuspendLayout();
            // 
            // IdDirectorio
            // 
            this.IdDirectorio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdDirectorio.Location = new System.Drawing.Point(1224, 57);
            this.IdDirectorio.Name = "IdDirectorio";
            this.IdDirectorio.Size = new System.Drawing.Size(27, 20);
            this.IdDirectorio.TabIndex = 63;
            // 
            // button1
            // 
            this.button1.Image = global::Jaeger.UI.Properties.Resources.refresh_16px;
            this.button1.Location = new System.Drawing.Point(370, 34);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(26, 20);
            this.button1.TabIndex = 62;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // buttonActualizarDirectorio
            // 
            this.buttonActualizarDirectorio.Image = global::Jaeger.UI.Properties.Resources.refresh_16px;
            this.buttonActualizarDirectorio.Location = new System.Drawing.Point(345, 34);
            this.buttonActualizarDirectorio.Name = "buttonActualizarDirectorio";
            this.buttonActualizarDirectorio.Size = new System.Drawing.Size(26, 20);
            this.buttonActualizarDirectorio.TabIndex = 61;
            this.buttonActualizarDirectorio.UseVisualStyleBackColor = true;
            // 
            // NumRegistro
            // 
            this.NumRegistro.Location = new System.Drawing.Point(342, 59);
            this.NumRegistro.Name = "NumRegistro";
            this.NumRegistro.Size = new System.Drawing.Size(116, 20);
            this.NumRegistro.TabIndex = 60;
            // 
            // ResidenciaFiscal
            // 
            this.ResidenciaFiscal.Location = new System.Drawing.Point(108, 59);
            this.ResidenciaFiscal.Name = "ResidenciaFiscal";
            this.ResidenciaFiscal.Size = new System.Drawing.Size(100, 20);
            this.ResidenciaFiscal.TabIndex = 59;
            // 
            // FechaTimbre
            // 
            this.FechaTimbre.Location = new System.Drawing.Point(907, 8);
            this.FechaTimbre.Name = "FechaTimbre";
            this.FechaTimbre.Size = new System.Drawing.Size(200, 20);
            this.FechaTimbre.TabIndex = 58;
            // 
            // CURP
            // 
            this.CURP.Location = new System.Drawing.Point(605, 34);
            this.CURP.MaxLength = 20;
            this.CURP.Name = "CURP";
            this.CURP.Size = new System.Drawing.Size(124, 20);
            this.CURP.TabIndex = 57;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.Location = new System.Drawing.Point(440, 34);
            this.ReceptorRFC.MaxLength = 14;
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.Size = new System.Drawing.Size(107, 20);
            this.ReceptorRFC.TabIndex = 56;
            // 
            // Receptor
            // 
            this.Receptor.DisplayMember = "Nombre";
            this.Receptor.FormattingEnabled = true;
            this.Receptor.Location = new System.Drawing.Point(70, 34);
            this.Receptor.Name = "Receptor";
            this.Receptor.Size = new System.Drawing.Size(274, 21);
            this.Receptor.TabIndex = 55;
            this.Receptor.DropDown += new System.EventHandler(this.Receptor_DropDown);
            this.Receptor.SelectedValueChanged += new System.EventHandler(this.Receptor_SelectedValueChanged);
            // 
            // FechaEmision
            // 
            this.FechaEmision.Location = new System.Drawing.Point(600, 8);
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.Size = new System.Drawing.Size(201, 20);
            this.FechaEmision.TabIndex = 54;
            // 
            // Folio
            // 
            this.Folio.Location = new System.Drawing.Point(370, 8);
            this.Folio.Name = "Folio";
            this.Folio.Size = new System.Drawing.Size(124, 20);
            this.Folio.TabIndex = 53;
            // 
            // Serie
            // 
            this.Serie.FormattingEnabled = true;
            this.Serie.Location = new System.Drawing.Point(225, 8);
            this.Serie.Name = "Serie";
            this.Serie.Size = new System.Drawing.Size(103, 21);
            this.Serie.TabIndex = 52;
            // 
            // Documento
            // 
            this.Documento.FormattingEnabled = true;
            this.Documento.Location = new System.Drawing.Point(81, 8);
            this.Documento.Name = "Documento";
            this.Documento.Size = new System.Drawing.Size(98, 21);
            this.Documento.TabIndex = 51;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(213, 63);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(123, 13);
            this.label10.TabIndex = 50;
            this.label10.Text = "Núm. de Reg. Tributario:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 63);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 49;
            this.label9.Text = "Residencia Fiscal:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(807, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 13);
            this.label8.TabIndex = 48;
            this.label8.Text = "Fecha Certificación:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(559, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 47;
            this.label7.Text = "CURP:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(404, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 46;
            this.label6.Text = "RFC:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 45;
            this.label5.Text = "Receptor:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(500, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 44;
            this.label4.Text = "Fecha de Emisión:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(332, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 42;
            this.label3.Text = "Folio:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(185, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "Serie:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 41;
            this.label1.Text = "Documento:";
            // 
            // preparar_contribuyentes
            // 
            this.preparar_contribuyentes.DoWork += new System.ComponentModel.DoWorkEventHandler(this.PrepararContribuyentes_DoWork);
            this.preparar_contribuyentes.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.PrepararContribuyentes_RunWorkerCompleted);
            // 
            // providerError
            // 
            this.providerError.ContainerControl = this;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 119);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1127, 145);
            this.groupBox1.TabIndex = 64;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalle de Retenciones:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.gridImpuestos);
            this.panel3.Controls.Add(this.TImpuesto);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(474, 16);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(650, 126);
            this.panel3.TabIndex = 32;
            // 
            // gridImpuestos
            // 
            this.gridImpuestos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridImpuestos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BaseRet,
            this.TipoImpuesto,
            this.montoRet,
            this.TipoPagoRet});
            this.gridImpuestos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridImpuestos.Location = new System.Drawing.Point(0, 25);
            this.gridImpuestos.Name = "gridImpuestos";
            this.gridImpuestos.Size = new System.Drawing.Size(650, 101);
            this.gridImpuestos.TabIndex = 1;
            // 
            // BaseRet
            // 
            this.BaseRet.DataPropertyName = "Base";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            this.BaseRet.DefaultCellStyle = dataGridViewCellStyle1;
            this.BaseRet.HeaderText = "BaseRet";
            this.BaseRet.Name = "BaseRet";
            // 
            // TipoImpuesto
            // 
            this.TipoImpuesto.DataPropertyName = "TipoImpuesto";
            this.TipoImpuesto.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.TipoImpuesto.HeaderText = "Impuesto";
            this.TipoImpuesto.Name = "TipoImpuesto";
            this.TipoImpuesto.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TipoImpuesto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // montoRet
            // 
            this.montoRet.DataPropertyName = "MontoRetenido";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            this.montoRet.DefaultCellStyle = dataGridViewCellStyle2;
            this.montoRet.HeaderText = "montoRet";
            this.montoRet.Name = "montoRet";
            // 
            // TipoPagoRet
            // 
            this.TipoPagoRet.DataPropertyName = "TipoPago";
            this.TipoPagoRet.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.TipoPagoRet.HeaderText = "TipoPagoRet";
            this.TipoPagoRet.Name = "TipoPagoRet";
            this.TipoPagoRet.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TipoPagoRet.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // TImpuesto
            // 
            this.TImpuesto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TImpuesto.Etiqueta = "Impuestos";
            this.TImpuesto.Location = new System.Drawing.Point(0, 0);
            this.TImpuesto.Name = "TImpuesto";
            this.TImpuesto.ShowActualizar = false;
            this.TImpuesto.ShowCerrar = false;
            this.TImpuesto.ShowEditar = false;
            this.TImpuesto.ShowGuardar = false;
            this.TImpuesto.ShowHerramientas = false;
            this.TImpuesto.ShowImprimir = false;
            this.TImpuesto.ShowNuevo = true;
            this.TImpuesto.ShowRemover = true;
            this.TImpuesto.Size = new System.Drawing.Size(650, 25);
            this.TImpuesto.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.MesFinal);
            this.panel2.Controls.Add(this.CveRetenc);
            this.panel2.Controls.Add(this.MesInicial);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.Descripcion);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.Ejercicio);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(3, 16);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(471, 126);
            this.panel2.TabIndex = 31;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 13);
            this.label15.TabIndex = 16;
            this.label15.Text = "Retención:";
            // 
            // MesFinal
            // 
            this.MesFinal.FormattingEnabled = true;
            this.MesFinal.Location = new System.Drawing.Point(233, 63);
            this.MesFinal.Name = "MesFinal";
            this.MesFinal.Size = new System.Drawing.Size(93, 21);
            this.MesFinal.TabIndex = 30;
            // 
            // CveRetenc
            // 
            this.CveRetenc.FormattingEnabled = true;
            this.CveRetenc.Location = new System.Drawing.Point(74, 9);
            this.CveRetenc.Name = "CveRetenc";
            this.CveRetenc.Size = new System.Drawing.Size(388, 21);
            this.CveRetenc.TabIndex = 17;
            // 
            // MesInicial
            // 
            this.MesInicial.FormattingEnabled = true;
            this.MesInicial.Location = new System.Drawing.Point(74, 63);
            this.MesInicial.Name = "MesInicial";
            this.MesInicial.Size = new System.Drawing.Size(93, 21);
            this.MesInicial.TabIndex = 29;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 40);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "Descripción:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(172, 67);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Mes Final:";
            // 
            // Descripcion
            // 
            this.Descripcion.Location = new System.Drawing.Point(74, 36);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(388, 20);
            this.Descripcion.TabIndex = 19;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 67);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Mes Inicial:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(343, 66);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Ejercicio:";
            // 
            // Ejercicio
            // 
            this.Ejercicio.Location = new System.Drawing.Point(399, 63);
            this.Ejercicio.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.Ejercicio.Minimum = new decimal(new int[] {
            2001,
            0,
            0,
            0});
            this.Ejercicio.Name = "Ejercicio";
            this.Ejercicio.Size = new System.Drawing.Size(63, 20);
            this.Ejercicio.TabIndex = 26;
            this.Ejercicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Ejercicio.Value = new decimal(new int[] {
            2001,
            0,
            0,
            0});
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Version);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.LugarExpedicion);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.ReceptorDomicilioFiscal);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.Extranjero);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.buttonActualizarDirectorio);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.NumRegistro);
            this.panel1.Controls.Add(this.IdDirectorio);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.ResidenciaFiscal);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.FechaTimbre);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.CURP);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.ReceptorRFC);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.Receptor);
            this.panel1.Controls.Add(this.Documento);
            this.panel1.Controls.Add(this.FechaEmision);
            this.panel1.Controls.Add(this.Serie);
            this.panel1.Controls.Add(this.Folio);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1113, 87);
            this.panel1.TabIndex = 65;
            // 
            // LugarExpedicion
            // 
            this.LugarExpedicion.Location = new System.Drawing.Point(1001, 34);
            this.LugarExpedicion.Name = "LugarExpedicion";
            this.LugarExpedicion.Size = new System.Drawing.Size(106, 20);
            this.LugarExpedicion.TabIndex = 67;
            this.LugarExpedicion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(904, 38);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(92, 13);
            this.label21.TabIndex = 66;
            this.label21.Text = "Lugar Expedición:";
            // 
            // ReceptorDomicilioFiscal
            // 
            this.ReceptorDomicilioFiscal.Location = new System.Drawing.Point(554, 59);
            this.ReceptorDomicilioFiscal.Name = "ReceptorDomicilioFiscal";
            this.ReceptorDomicilioFiscal.Size = new System.Drawing.Size(90, 20);
            this.ReceptorDomicilioFiscal.TabIndex = 65;
            this.ReceptorDomicilioFiscal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(466, 63);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(82, 13);
            this.label20.TabIndex = 64;
            this.label20.Text = "Domicilio Fiscal:";
            // 
            // Extranjero
            // 
            this.Extranjero.AutoSize = true;
            this.Extranjero.Location = new System.Drawing.Point(735, 36);
            this.Extranjero.Name = "Extranjero";
            this.Extranjero.Size = new System.Drawing.Size(73, 17);
            this.Extranjero.TabIndex = 63;
            this.Extranjero.Text = "Extranjero";
            this.Extranjero.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(114, 13);
            this.label16.TabIndex = 66;
            this.label16.Text = "Total de la operación*:";
            // 
            // MontoTotalOperacion
            // 
            this.MontoTotalOperacion.DecimalPlaces = 2;
            this.MontoTotalOperacion.Location = new System.Drawing.Point(126, 14);
            this.MontoTotalOperacion.Maximum = new decimal(new int[] {
            276447231,
            23283,
            0,
            0});
            this.MontoTotalOperacion.Name = "MontoTotalOperacion";
            this.MontoTotalOperacion.Size = new System.Drawing.Size(92, 20);
            this.MontoTotalOperacion.TabIndex = 67;
            this.MontoTotalOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.MontoTotalOperacion.ThousandsSeparator = true;
            // 
            // MontoTotalGravado
            // 
            this.MontoTotalGravado.DecimalPlaces = 2;
            this.MontoTotalGravado.Location = new System.Drawing.Point(311, 14);
            this.MontoTotalGravado.Maximum = new decimal(new int[] {
            276447231,
            23283,
            0,
            0});
            this.MontoTotalGravado.Name = "MontoTotalGravado";
            this.MontoTotalGravado.Size = new System.Drawing.Size(92, 20);
            this.MontoTotalGravado.TabIndex = 69;
            this.MontoTotalGravado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.MontoTotalGravado.ThousandsSeparator = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(223, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(82, 13);
            this.label17.TabIndex = 68;
            this.label17.Text = "Total Gravado*:";
            // 
            // MontoTotalExento
            // 
            this.MontoTotalExento.DecimalPlaces = 2;
            this.MontoTotalExento.Location = new System.Drawing.Point(492, 14);
            this.MontoTotalExento.Maximum = new decimal(new int[] {
            276447231,
            23283,
            0,
            0});
            this.MontoTotalExento.Name = "MontoTotalExento";
            this.MontoTotalExento.Size = new System.Drawing.Size(92, 20);
            this.MontoTotalExento.TabIndex = 71;
            this.MontoTotalExento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.MontoTotalExento.ThousandsSeparator = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(406, 18);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 13);
            this.label18.TabIndex = 70;
            this.label18.Text = "Total Excento*:";
            // 
            // MontoTotalRetencion
            // 
            this.MontoTotalRetencion.DecimalPlaces = 2;
            this.MontoTotalRetencion.Location = new System.Drawing.Point(683, 14);
            this.MontoTotalRetencion.Maximum = new decimal(new int[] {
            276447231,
            23283,
            0,
            0});
            this.MontoTotalRetencion.Name = "MontoTotalRetencion";
            this.MontoTotalRetencion.Size = new System.Drawing.Size(92, 20);
            this.MontoTotalRetencion.TabIndex = 73;
            this.MontoTotalRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.MontoTotalRetencion.ThousandsSeparator = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(592, 18);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(84, 13);
            this.label19.TabIndex = 72;
            this.label19.Text = "Total Retenido*:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabComprobante);
            this.tabControl1.Controls.Add(this.tabCFDIRelacionado);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1127, 119);
            this.tabControl1.TabIndex = 74;
            // 
            // tabComprobante
            // 
            this.tabComprobante.Controls.Add(this.panel1);
            this.tabComprobante.Location = new System.Drawing.Point(4, 22);
            this.tabComprobante.Name = "tabComprobante";
            this.tabComprobante.Padding = new System.Windows.Forms.Padding(3);
            this.tabComprobante.Size = new System.Drawing.Size(1119, 93);
            this.tabComprobante.TabIndex = 0;
            this.tabComprobante.Text = "Comprobante";
            this.tabComprobante.UseVisualStyleBackColor = true;
            // 
            // tabCFDIRelacionado
            // 
            this.tabCFDIRelacionado.Controls.Add(this.comprobanteRelacionadoControl1);
            this.tabCFDIRelacionado.Location = new System.Drawing.Point(4, 22);
            this.tabCFDIRelacionado.Name = "tabCFDIRelacionado";
            this.tabCFDIRelacionado.Padding = new System.Windows.Forms.Padding(3);
            this.tabCFDIRelacionado.Size = new System.Drawing.Size(1119, 93);
            this.tabCFDIRelacionado.TabIndex = 1;
            this.tabCFDIRelacionado.Text = "CFDI Relacionado";
            this.tabCFDIRelacionado.UseVisualStyleBackColor = true;
            // 
            // ISRCorrespondiente
            // 
            this.ISRCorrespondiente.DecimalPlaces = 2;
            this.ISRCorrespondiente.Location = new System.Drawing.Point(1028, 14);
            this.ISRCorrespondiente.Maximum = new decimal(new int[] {
            276447231,
            23283,
            0,
            0});
            this.ISRCorrespondiente.Name = "ISRCorrespondiente";
            this.ISRCorrespondiente.Size = new System.Drawing.Size(92, 20);
            this.ISRCorrespondiente.TabIndex = 78;
            this.ISRCorrespondiente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ISRCorrespondiente.ThousandsSeparator = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(989, 18);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(28, 13);
            this.label22.TabIndex = 77;
            this.label22.Text = "ISR:";
            // 
            // UtilidadBimestral
            // 
            this.UtilidadBimestral.DecimalPlaces = 2;
            this.UtilidadBimestral.Location = new System.Drawing.Point(879, 14);
            this.UtilidadBimestral.Maximum = new decimal(new int[] {
            276447231,
            23283,
            0,
            0});
            this.UtilidadBimestral.Name = "UtilidadBimestral";
            this.UtilidadBimestral.Size = new System.Drawing.Size(92, 20);
            this.UtilidadBimestral.TabIndex = 76;
            this.UtilidadBimestral.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.UtilidadBimestral.ThousandsSeparator = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(779, 18);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(90, 13);
            this.label23.TabIndex = 75;
            this.label23.Text = "Utilidad Bimestral:";
            // 
            // Totales
            // 
            this.Totales.Controls.Add(this.label16);
            this.Totales.Controls.Add(this.ISRCorrespondiente);
            this.Totales.Controls.Add(this.MontoTotalOperacion);
            this.Totales.Controls.Add(this.label22);
            this.Totales.Controls.Add(this.label17);
            this.Totales.Controls.Add(this.UtilidadBimestral);
            this.Totales.Controls.Add(this.MontoTotalGravado);
            this.Totales.Controls.Add(this.label23);
            this.Totales.Controls.Add(this.label18);
            this.Totales.Controls.Add(this.MontoTotalRetencion);
            this.Totales.Controls.Add(this.MontoTotalExento);
            this.Totales.Controls.Add(this.label19);
            this.Totales.Dock = System.Windows.Forms.DockStyle.Top;
            this.Totales.Location = new System.Drawing.Point(0, 264);
            this.Totales.Name = "Totales";
            this.Totales.Size = new System.Drawing.Size(1127, 44);
            this.Totales.TabIndex = 79;
            this.Totales.TabStop = false;
            this.Totales.Text = "Totales";
            // 
            // Version
            // 
            this.Version.Location = new System.Drawing.Point(1001, 59);
            this.Version.Name = "Version";
            this.Version.Size = new System.Drawing.Size(106, 20);
            this.Version.TabIndex = 69;
            this.Version.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(904, 63);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 13);
            this.label24.TabIndex = 68;
            this.label24.Text = "Versión:";
            // 
            // comprobanteRelacionadoControl1
            // 
            this.comprobanteRelacionadoControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comprobanteRelacionadoControl1.Editable = true;
            this.comprobanteRelacionadoControl1.Location = new System.Drawing.Point(3, 3);
            this.comprobanteRelacionadoControl1.Name = "comprobanteRelacionadoControl1";
            this.comprobanteRelacionadoControl1.Size = new System.Drawing.Size(1113, 87);
            this.comprobanteRelacionadoControl1.TabIndex = 0;
            // 
            // ComprobanteGeneralControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Totales);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabControl1);
            this.Name = "ComprobanteGeneralControl";
            this.Size = new System.Drawing.Size(1127, 308);
            this.Load += new System.EventHandler(this.ComprobanteGeneralControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.providerError)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridImpuestos)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalOperacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalGravado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalExento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalRetencion)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabComprobante.ResumeLayout(false);
            this.tabCFDIRelacionado.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ISRCorrespondiente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UtilidadBimestral)).EndInit();
            this.Totales.ResumeLayout(false);
            this.Totales.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal System.Windows.Forms.TextBox IdDirectorio;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonActualizarDirectorio;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.ComponentModel.BackgroundWorker preparar_contribuyentes;
        private System.Windows.Forms.ErrorProvider providerError;
        protected internal System.Windows.Forms.TextBox NumRegistro;
        protected internal System.Windows.Forms.TextBox ResidenciaFiscal;
        protected internal System.Windows.Forms.DateTimePicker FechaTimbre;
        protected internal System.Windows.Forms.TextBox CURP;
        protected internal System.Windows.Forms.TextBox ReceptorRFC;
        protected internal System.Windows.Forms.ComboBox Receptor;
        protected internal System.Windows.Forms.DateTimePicker FechaEmision;
        protected internal System.Windows.Forms.TextBox Folio;
        protected internal System.Windows.Forms.ComboBox Serie;
        protected internal System.Windows.Forms.ComboBox Documento;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel1;
        protected internal System.Windows.Forms.NumericUpDown Ejercicio;
        protected internal System.Windows.Forms.TextBox Descripcion;
        protected internal System.Windows.Forms.ComboBox CveRetenc;
        private System.Windows.Forms.Panel panel3;
        protected internal System.Windows.Forms.DataGridView gridImpuestos;
        protected internal Common.Forms.ToolBarStandarControl TImpuesto;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        protected internal System.Windows.Forms.CheckBox Extranjero;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        protected internal System.Windows.Forms.NumericUpDown MontoTotalRetencion;
        protected internal System.Windows.Forms.NumericUpDown MontoTotalExento;
        protected internal System.Windows.Forms.NumericUpDown MontoTotalGravado;
        protected internal System.Windows.Forms.NumericUpDown MontoTotalOperacion;
        protected internal System.Windows.Forms.ComboBox MesFinal;
        protected internal System.Windows.Forms.ComboBox MesInicial;
        protected internal System.Windows.Forms.TextBox ReceptorDomicilioFiscal;
        private System.Windows.Forms.Label label20;
        protected internal System.Windows.Forms.TextBox LugarExpedicion;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabComprobante;
        private System.Windows.Forms.TabPage tabCFDIRelacionado;
        private Comprobantes.ComprobanteRelacionadoControl comprobanteRelacionadoControl1;
        protected internal System.Windows.Forms.NumericUpDown ISRCorrespondiente;
        private System.Windows.Forms.Label label22;
        protected internal System.Windows.Forms.NumericUpDown UtilidadBimestral;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox Totales;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaseRet;
        private System.Windows.Forms.DataGridViewComboBoxColumn TipoImpuesto;
        private System.Windows.Forms.DataGridViewTextBoxColumn montoRet;
        private System.Windows.Forms.DataGridViewComboBoxColumn TipoPagoRet;
        protected internal System.Windows.Forms.TextBox Version;
        private System.Windows.Forms.Label label24;
    }
}
