﻿namespace Jaeger.UI.Forms.Validador {
    partial class ValidaRFCForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ValidaRFCForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gridRegistros = new System.Windows.Forms.DataGridView();
            this.Registro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RazonSocial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodigoPostal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Resultado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TValida = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.Status = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.title = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRegistros)).BeginInit();
            this.Status.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(571, 50);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // gridRegistros
            // 
            this.gridRegistros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridRegistros.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Registro,
            this.RFC,
            this.RazonSocial,
            this.CodigoPostal,
            this.Resultado});
            this.gridRegistros.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRegistros.Location = new System.Drawing.Point(0, 75);
            this.gridRegistros.Name = "gridRegistros";
            this.gridRegistros.Size = new System.Drawing.Size(571, 353);
            this.gridRegistros.TabIndex = 2;
            // 
            // Registro
            // 
            this.Registro.DataPropertyName = "Registro";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Format = "#000";
            dataGridViewCellStyle1.NullValue = "0";
            this.Registro.DefaultCellStyle = dataGridViewCellStyle1;
            this.Registro.HeaderText = "Núm.";
            this.Registro.Name = "Registro";
            this.Registro.ReadOnly = true;
            this.Registro.Visible = false;
            this.Registro.Width = 50;
            // 
            // RFC
            // 
            this.RFC.DataPropertyName = "RFC";
            this.RFC.HeaderText = "RFC";
            this.RFC.MaxInputLength = 14;
            this.RFC.Name = "RFC";
            // 
            // RazonSocial
            // 
            this.RazonSocial.DataPropertyName = "NombreRazonSocial";
            this.RazonSocial.HeaderText = "Nombre ó Razon Social";
            this.RazonSocial.MaxInputLength = 365;
            this.RazonSocial.Name = "RazonSocial";
            this.RazonSocial.Width = 250;
            // 
            // CodigoPostal
            // 
            this.CodigoPostal.DataPropertyName = "CodigoPostal";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "00000";
            this.CodigoPostal.DefaultCellStyle = dataGridViewCellStyle2;
            this.CodigoPostal.HeaderText = "C. P.";
            this.CodigoPostal.MaxInputLength = 6;
            this.CodigoPostal.MinimumWidth = 65;
            this.CodigoPostal.Name = "CodigoPostal";
            this.CodigoPostal.Width = 65;
            // 
            // Resultado
            // 
            this.Resultado.DataPropertyName = "Resultado";
            this.Resultado.HeaderText = "Resultado";
            this.Resultado.Name = "Resultado";
            this.Resultado.Width = 200;
            // 
            // TValida
            // 
            this.TValida.Dock = System.Windows.Forms.DockStyle.Top;
            this.TValida.Etiqueta = "";
            this.TValida.Location = new System.Drawing.Point(0, 50);
            this.TValida.Name = "TValida";
            this.TValida.ShowActualizar = true;
            this.TValida.ShowCerrar = true;
            this.TValida.ShowEditar = false;
            this.TValida.ShowGuardar = false;
            this.TValida.ShowHerramientas = true;
            this.TValida.ShowImprimir = false;
            this.TValida.ShowNuevo = true;
            this.TValida.ShowRemover = true;
            this.TValida.Size = new System.Drawing.Size(571, 25);
            this.TValida.TabIndex = 3;
            // 
            // Status
            // 
            this.Status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.Status.Location = new System.Drawing.Point(0, 428);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(571, 22);
            this.Status.TabIndex = 4;
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(16, 17);
            this.lblStatus.Text = "...";
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.BackColor = System.Drawing.Color.White;
            this.title.Location = new System.Drawing.Point(12, 20);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(161, 13);
            this.title.TabIndex = 5;
            this.title.Text = "Validación de la clave en el RFC";
            // 
            // ValidaRFCForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 450);
            this.Controls.Add(this.title);
            this.Controls.Add(this.gridRegistros);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.TValida);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ValidaRFCForm";
            this.ShowInTaskbar = false;
            this.Text = "Valida RFC";
            this.Load += new System.EventHandler(this.ValidaRFCForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRegistros)).EndInit();
            this.Status.ResumeLayout(false);
            this.Status.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView gridRegistros;
        private Common.Forms.ToolBarStandarControl TValida;
        private System.Windows.Forms.StatusStrip Status;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.Label title;
        private System.Windows.Forms.DataGridViewTextBoxColumn Registro;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn RazonSocial;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodigoPostal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Resultado;
    }
}