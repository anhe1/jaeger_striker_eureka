﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.Domain;
using Jaeger.Aplication.Validador.Services;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Validador {
    public partial class TComprobanteValdiadorControl : UserControl {
        protected ICarpetaRecienteService recientes;
        private Progress<Progreso> reporteAvance;
        public event EventHandler<Progreso> ProcesoAvance;
        public event EventHandler<Progreso> ProcesoIniciado;
        public event EventHandler<Progreso> ProcesoCompletado;

        public virtual void OnProcesoIniciado(Progreso e) {
            if (this.ProcesoIniciado != null)
                this.ProcesoIniciado(this, e);
        }

        public virtual void OnProcesoAvance(Progreso e) {
            if (this.ProcesoAvance != null) {
                this.ProcesoAvance(this, e);
            }
        }

        public virtual void OnProcesoCompletado(Progreso e) {
            if (this.ProcesoCompletado != null)
                this.ProcesoCompletado(this, e);
        }

        public TComprobanteValdiadorControl() {
            InitializeComponent();
        }

        public IAdministradorService Administrador {
            get; set;
        }

        public IValidadorService Validador {
            get; set;
        }

        private void TComprobanteValdiadorControl_Load(object sender, EventArgs e) {
            this.recientes = new CarpetaRecienteService();
            this.recientes.Load();
            this.Carpeta.ComboBox.DisplayMember = "Carpeta";
            this.Carpeta.ComboBox.DataSource = this.recientes.Items;
            this.Carpeta.ComboBox.DropDownWidth();
        }

        private void Agregar_Click(object sender, EventArgs e) {
            using (var _openFile = new OpenFileDialog() { Filter = "*.xml|*.XML" }) {
                if (_openFile.ShowDialog(this) == DialogResult.OK) {
                    this.Agregar.Enabled = false;
                    this.Administrador.Agregar(new FileInfo(_openFile.FileName));
                    this.Agregar.Enabled = true;
                }
            }
        }

        private void RemoverTodo_Click(object sender, EventArgs e) {
            if (MessageBox.Show(this, "¿Esta seguro de remover todo? No se elimina ningún archivo.", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                this.Administrador.Documentos.Clear();
            }
        }

        public virtual void Buscar_Click(object sender, EventArgs e) {
            var _openFolder = new FolderBrowserDialog() { Description = Properties.Resources.Message_Carpeta_Seleccion };
            if (_openFolder.ShowDialog(this) == DialogResult.OK) {
                this.recientes.Add(_openFolder.SelectedPath);
                this.recientes.Save();
                this.Carpeta.ComboBox.DataSource = this.recientes.Items;
                this.Carpeta.SelectedText = _openFolder.SelectedPath;
            }
        }

        public virtual void Detener_Click(object sender, EventArgs e) {
            throw new NotImplementedException();
        }

        protected virtual async void Actualizar_Click(object sender, EventArgs e) {
            this.OnProcesoIniciado(new Progreso { Caption = "Iniciando ..." });
            this.ToolBar.Enabled = false;
            this.Administrador.Documentos.Clear();
            await this.ExecuteBusqueda(this.Carpeta.Text, true);
        }

        #region metodos
        public virtual async Task ExecuteBusqueda(string carpeta, bool incluirSubCarpetas) {
            this.reporteAvance = new Progress<Progreso>();
            this.reporteAvance.ProgressChanged += Reporte_Avance;
            await this.Administrador.ExecuteSearch(carpeta, incluirSubCarpetas, this.reporteAvance, this.Validador.Configuracion.ObtenerEmisor);
        }

        private void Reporte_Avance(object sender, Progreso e) {
            this.ToolBar.Enabled = e.Terminado;
            this.OnProcesoAvance(e);
            if (e.Terminado)
                this.OnProcesoCompletado(e);
        }
        #endregion

        private void ValidarTodo_Click(object sender, EventArgs e) {
            if (this.Administrador.Documentos.Count > 0) {
                this.OnProcesoIniciado(new Progreso { Caption = "Iniciando validación ..." });
                this.ToolBar.Enabled = false;
                this.reporteAvance = new Progress<Progreso>();
                this.reporteAvance.ProgressChanged += Reporte_Avance;
                this.Validador.RunValidarAsync(this.Administrador.Documentos, this.reporteAvance);
            }
        }
    }
}
