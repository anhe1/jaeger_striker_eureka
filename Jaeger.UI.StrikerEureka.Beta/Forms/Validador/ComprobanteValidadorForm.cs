﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.Aplication.Validador.Domain;
using Jaeger.Aplication.Validador.Services;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.UI.Forms.Validador {
    public partial class ComprobanteValidadorForm : Form {
        protected internal ToolStripMenuItem PolizaModelb = new ToolStripMenuItem { Text = "Desde archivo" };
        protected IValidadorService service;
        private Progress<Progreso> reporteBusqueda;
        private Progress<Progreso> reporteValidacion;

        public ComprobanteValidadorForm() {
            InitializeComponent();
        }

        private void ComprobanteValidadorForm_Load(object sender, EventArgs e) {
            this.dataGridResult.DataGridCommon();
            this.service = new ValidadorService();
            this.service.Configuracion = new Configuracion() { ObtenerEmisor = true };
            AdministradorService.Configuracion = service.Configuracion;
            this.TValida.Validador = new ValidadorService();
            this.TValida.Administrador = new AdministradorService();
            this.TValida.Validador.Configuracion = new Configuracion() { ObtenerEmisor = true };
            //this.TValida.Configuracion_Click += TValida_Configuracion_Click;
            PolizaModelb.Click += this.PolizaModelb_Click;
            this.TValida.Herramientas.DropDownItems.Add(PolizaModelb);
        }

        private void PolizaModelb_Click(object sender, EventArgs e) {
            var _nuevo = new Polizas.PolizaModeloForm();
            _nuevo.ShowDialog(this);
        }

        private void TValida_Configuracion_Click(object sender, EventArgs e) {
            var _configuracion = new ConfiguracionForm(this.service.Configuracion);
            _configuracion.ShowDialog(this);
        }

        private void TValida_RemoverSeleccionado_Click(object sender, EventArgs e) {
            if (this.dataGridResult.CurrentRow != null) {
                this.dataGridResult.Rows.Remove(this.dataGridResult.CurrentRow);
            }
        }

        private void TValida_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual async Task ExecuteBusqueda(string carpeta, bool incluirSubCarpetas) {
            this.reporteBusqueda = new Progress<Progreso>();
            this.reporteBusqueda.ProgressChanged += Reporte_Busqueda;
            this.dataGridResult.DataSource = null;
            await this.TValida.Administrador.ExecuteSearch(carpeta, incluirSubCarpetas, this.reporteBusqueda, this.service.Configuracion.ObtenerEmisor);
        }

        private void Reporte_Busqueda(object sender, Progreso e) {
            if (e.Terminado) {
                this.dataGridResult.DataSource = this.TValida.Administrador.Documentos;
            }
            this.Progreso.Text = e.Caption;
        }

        private void TValida_Validar_Click(object sender, EventArgs e) {
            var seleccionado = this.dataGridResult.CurrentRow.DataBoundItem as DocumentoFiscal;
            if (seleccionado != null) {
                this.reporteValidacion = new Progress<Progreso>();
                this.reporteValidacion.ProgressChanged += ReporteValidacion_ProgressChanged;
                this.service.ValidacionCompletada += Service_ValidacionCompletada;
                this.service.RunValidarAsync(this.TValida.Administrador.Documentos, this.reporteValidacion);
            }
        }

        private void Service_ValidacionCompletada(object sender, ProcesoCompletado e) {
            using (var espera = new Common.Forms.WaitingForm(this.Registra)) {
                espera.Text = "Registrando comprobantes, espere un momento";
                espera.ShowDialog(this);
            }
        }

        private void ReporteValidacion_ProgressChanged(object sender, Progreso e) {
            this.Progreso.Text = e.Caption;
        }

        private void TValida_ImprimirValidacion_Click(object sender, EventArgs e) {
            var seleccionado = this.dataGridResult.CurrentRow.DataBoundItem as DocumentoFiscal;
            if (seleccionado != null) {
                var _val = ComprobanteValidacionPrinter.Json(seleccionado.Validacion.Json());
                var _imprimir = new ReporteForm(_val);
                _imprimir.Show();
            }
        }

        private void TValida_BackupTodos_Click(object sender, EventArgs e) {

        }

        private void Registra() {
            this.TValida.Administrador.Registrar();
        }

        private void TValida_ProcesoCompletado(object sender, Progreso e) {
            if (e.Terminado) {
                this.dataGridResult.DataSource = this.TValida.Administrador.Documentos;
                this.Progreso.Text = e.Caption;
            }
        }

        #region acciones del grid
        private void dataGridResult_CellContentClick(object sender, DataGridViewCellEventArgs e) {
            if (this.dataGridResult.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.ColumnIndex > 0) {
                if (this.dataGridResult.CurrentRow != null) {
                    var seleccionado = this.dataGridResult.CurrentRow.DataBoundItem as DocumentoFiscal;
                    if (seleccionado != null) {
                        if (seleccionado.Resultado == Domain.Base.ValueObjects.CFDIValidacionEnum.EnEspera) {
                            MessageBox.Show(this, "No existe validación asociada al comprobante seleccionado", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        } else if (seleccionado.Resultado == Domain.Base.ValueObjects.CFDIValidacionEnum.Valido) {
                            this.TValida_ImprimirValidacion_Click(sender, e);
                        }
                    }
                }
            }
        }
        #endregion
    }
}
