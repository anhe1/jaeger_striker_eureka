﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.ValidaSAT.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Base.Services;
using Jaeger.ValidaSAT.Contracts;
using System.Linq;

namespace Jaeger.UI.Forms.Validador {
    public partial class ValidaRFCForm : Form {
        private IConsultaRFCService service = new ValidaSAT.ConsultaRFCService();
        private BindingList<LayoutValidaRFCModel> registos = new BindingList<LayoutValidaRFCModel>();
        protected internal ToolStripMenuItem Exportar = new ToolStripMenuItem { Text = "Exportar" };
        protected internal ToolStripMenuItem Importar = new ToolStripMenuItem { Text = "Importar" };

        public ValidaRFCForm() {
            InitializeComponent();
        }

        private void ValidaRFCForm_Load(object sender, EventArgs e) {
            this.TValida.Nuevo.Text = "Agregar";
            this.TValida.Actualizar.Text = "Validar";
            this.TValida.Actualizar.Image = Properties.Resources.approval_16px;
            this.TValida.Nuevo.Click += this.Nuevo_Click;
            this.TValida.Remover.Click += this.Remover_Click;
            this.TValida.Actualizar.Click += this.Actualizar_Click;
            this.TValida.Cerrar.Click += this.Cerrar_Click;
            this.TValida.Herramientas.DropDownItems.AddRange(new ToolStripMenuItem[] { this.Importar, this.Exportar });
            this.Importar.Click += this.Importar_Click;
            this.Exportar.Click += this.Exportar_Click;
            this.gridRegistros.DataGridCommon();
            this.gridRegistros.ReadOnly = false;
            this.gridRegistros.EditMode = DataGridViewEditMode.EditOnKeystrokeOrF2;
            this.gridRegistros.DataSource = this.registos;
            this.gridRegistros.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }

        private void Exportar_Click(object sender, EventArgs e) {
            if (this.registos.Count > 0) {
                var saveDialog = new SaveFileDialog {
                    Title = "Exportar",
                    Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*",
                    RestoreDirectory = true
                };

                if (saveDialog.ShowDialog(this) == DialogResult.OK) {
                    var contenido = new System.Collections.Generic.List<string>();
                    foreach (var item in this.registos) {
                        contenido.Add(item.ToString() + Environment.NewLine);
                    }

                    try {
                        System.IO.File.WriteAllLines(saveDialog.FileName, contenido.ToArray());
                    } catch (Exception) {
                        
                    }
                }
            }
        }

        private void Importar_Click(object sender, EventArgs e) {
            var _openDialog = new OpenFileDialog { Title = "Importar", Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*" };
            if (_openDialog.ShowDialog(this) == DialogResult.OK) {
                if (System.IO.File.Exists(_openDialog.FileName)) {
                    var _contenido = System.IO.File.ReadAllText(_openDialog.FileName);
                    if (!string.IsNullOrEmpty(_contenido)) {
                        var _registros = _contenido.Split(new char[] { '\r' });
                        for (int i = 0; i < _registros.Length; i++) {
                            var columnas = _registros[i].Split(new char[] { '|' });
                            if (columnas.Count() > 3) {
                                var _registro = new LayoutValidaRFCModel {
                                    Registro = Convert.ToInt32(columnas[0].ToString()),
                                    RFC = columnas[1].ToString(),
                                    NombreRazonSocial = columnas[2].ToString(),
                                    CodigoPostal = columnas[3].ToString(),
                                    Resultado = string.Empty
                                };
                                this.registos.Add(_registro);
                            } else {
                                var _registro = new LayoutValidaRFCModel {
                                    Registro = Convert.ToInt32(columnas[0].ToString()),
                                    RFC = columnas[1].ToString(),
                                    Resultado = string.Empty
                                };
                                this.registos.Add(_registro);
                            }
                        }
                    }
                } else {
                    MessageBox.Show(this, "No se tiene acceso al archivo.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Actualizar_Click(object sender, EventArgs e) {
            if (this.registos.Count > 0) {
                this.lblStatus.Text = "Esperando al servidor ...";
                using (var espera = new WaitingForm(this.Consultar)) {
                    espera.Text = "Solitando información ...";
                    espera.ShowDialog(this);
                }
            } else {
                MessageBox.Show(this, "Es necesario al menos algún registro para validar.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Remover_Click(object sender, EventArgs e) {
            if (this.gridRegistros.CurrentRow != null) {
                this.gridRegistros.Rows.Remove(this.gridRegistros.CurrentRow);
            }
        }

        private void Nuevo_Click(object sender, EventArgs e) {
            this.registos.Add(new LayoutValidaRFCModel() { Registro = this.registos.Count + 1 });
            this.lblStatus.Text = string.Format("Registros: {0}", this.registos.Count);
        }

        private void Consultar() {
            service.Initialize();

            var request = new ValidacionRequest();

            foreach (LayoutValidaRFCModel item in registos) {
                request.Registros.Add(item.MapProperties<ContribuyenteResponse>());
                var person = new ContribuyenteResponse();
                person.CopyPropertiesFrom(item);
                var person1 = new ContribuyenteResponse();
                person1.MatchPropertiesFrom(item);
            }

            var validarfc = service.Request(request);

            registos.Clear();
            foreach (var item in validarfc.Registros) {
                this.registos.Add(item.MapProperties<LayoutValidaRFCModel>());
                var person1 = new LayoutValidaRFCModel();
                person1.MatchPropertiesFrom(item);
            }
            this.lblStatus.Text = validarfc.Mensaje;
        }
    }
}
