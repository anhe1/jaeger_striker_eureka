﻿using System;
using Jaeger.ValidaSAT.Entities;
using System.Windows.Forms;

namespace Jaeger.UI.Forms.Validador {
    public partial class ValidaRetencionForm : Form {
            private Jaeger.ValidaSAT.Service.ConsultaRetencionService service = new ValidaSAT.Service.ConsultaRetencionService();
        public ValidaRetencionForm() {
            InitializeComponent();
        }

        private void ValidaRetencionForm_Load(object sender, EventArgs e) {
            this.TValida.Nuevo.Click += this.TValida_Nuevo_Click;
            this.TValida.Actualizar.Text = "Validar";
            this.TValida.Actualizar.Image = Properties.Resources.approval_16px;
            this.TValida.Actualizar.Click += this.TValida_Procesar_Click;
            this.TValida.Cerrar.Click += this.TValida_Cerrar_Click;
        }

        private void TValida_Nuevo_Click(object sender, EventArgs e) {
            this.EmisorRFC.Text = string.Empty;
            this.ReceptorRFC.Text = string.Empty;
            this.IdDocumento.Text = string.Empty;
            this.Log.Text = string.Empty;
        }

        private void TValida_Procesar_Click(object sender, EventArgs e) {
            var response = service.Consulta(new ComprobanteRetencionRequest(this.EmisorRFC.Text, this.ReceptorRFC.Text, this.IdDocumento.Text));
            this.Log.Text = response.ToString();
        }

        private void TValida_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
