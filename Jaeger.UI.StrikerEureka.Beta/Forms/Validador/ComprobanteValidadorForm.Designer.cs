﻿namespace Jaeger.UI.Forms.Validador {
    partial class ComprobanteValidadorForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComprobanteValidadorForm));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.Progreso = new System.Windows.Forms.ToolStripStatusLabel();
            this.dataGridResult = new System.Windows.Forms.DataGridView();
            this.TValida = new Jaeger.UI.Forms.Validador.TComprobanteValdiadorControl();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Registrado = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.TipoComprobante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Version = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmisorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmisorNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaEmision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaTimbre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EstadoSAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoCertificado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RFCProvCertif = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveMoneda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveMetodoPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveFormaPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveUsoCFDI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetencionISR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetencionIVA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetencionIEPS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrasladoIVA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrasladoIEPS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descuento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Situacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Resultado = new System.Windows.Forms.DataGridViewButtonColumn();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridResult)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Progreso});
            this.statusStrip1.Location = new System.Drawing.Point(0, 558);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1227, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // Progreso
            // 
            this.Progreso.Name = "Progreso";
            this.Progreso.Size = new System.Drawing.Size(32, 17);
            this.Progreso.Text = "Listo";
            // 
            // dataGridResult
            // 
            this.dataGridResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Registrado,
            this.TipoComprobante,
            this.Version,
            this.Folio,
            this.Serie,
            this.EmisorRFC,
            this.EmisorNombre,
            this.ReceptorRFC,
            this.ReceptorNombre,
            this.FechaEmision,
            this.FechaTimbre,
            this.IdDocumento,
            this.EstadoSAT,
            this.NoCertificado,
            this.RFCProvCertif,
            this.ClaveMoneda,
            this.ClaveMetodoPago,
            this.ClaveFormaPago,
            this.ClaveUsoCFDI,
            this.RetencionISR,
            this.RetencionIVA,
            this.RetencionIEPS,
            this.TrasladoIVA,
            this.TrasladoIEPS,
            this.SubTotal,
            this.Descuento,
            this.Total,
            this.Situacion,
            this.Resultado});
            this.dataGridResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridResult.Location = new System.Drawing.Point(0, 25);
            this.dataGridResult.Name = "dataGridResult";
            this.dataGridResult.Size = new System.Drawing.Size(1227, 533);
            this.dataGridResult.TabIndex = 3;
            this.dataGridResult.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridResult_CellContentClick);
            // 
            // TValida
            // 
            this.TValida.Administrador = null;
            this.TValida.Dock = System.Windows.Forms.DockStyle.Top;
            this.TValida.Location = new System.Drawing.Point(0, 0);
            this.TValida.Name = "TValida";
            this.TValida.Size = new System.Drawing.Size(1227, 25);
            this.TValida.TabIndex = 2;
            this.TValida.Validador = null;
            this.TValida.ProcesoCompletado += new System.EventHandler<Jaeger.Aplication.Validador.Domain.Progreso>(this.TValida_ProcesoCompletado);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Width = 50;
            // 
            // Registrado
            // 
            this.Registrado.DataPropertyName = "Registrado";
            this.Registrado.HeaderText = "Reg";
            this.Registrado.Name = "Registrado";
            this.Registrado.ReadOnly = true;
            this.Registrado.Width = 40;
            // 
            // TipoComprobante
            // 
            this.TipoComprobante.DataPropertyName = "TipoComprobante";
            this.TipoComprobante.HeaderText = "Tipo";
            this.TipoComprobante.Name = "TipoComprobante";
            this.TipoComprobante.ReadOnly = true;
            this.TipoComprobante.Width = 50;
            // 
            // Version
            // 
            this.Version.DataPropertyName = "Version";
            this.Version.HeaderText = "Versión";
            this.Version.Name = "Version";
            this.Version.ReadOnly = true;
            this.Version.Width = 50;
            // 
            // Folio
            // 
            this.Folio.DataPropertyName = "Folio";
            this.Folio.HeaderText = "Folio";
            this.Folio.Name = "Folio";
            this.Folio.ReadOnly = true;
            // 
            // Serie
            // 
            this.Serie.DataPropertyName = "Serie";
            this.Serie.HeaderText = "Serie";
            this.Serie.Name = "Serie";
            this.Serie.ReadOnly = true;
            // 
            // EmisorRFC
            // 
            this.EmisorRFC.DataPropertyName = "EmisorRFC";
            this.EmisorRFC.HeaderText = "RFC (Emisor)";
            this.EmisorRFC.Name = "EmisorRFC";
            this.EmisorRFC.ReadOnly = true;
            // 
            // EmisorNombre
            // 
            this.EmisorNombre.DataPropertyName = "EmisorNombre";
            this.EmisorNombre.HeaderText = "Emisor";
            this.EmisorNombre.Name = "EmisorNombre";
            this.EmisorNombre.ReadOnly = true;
            this.EmisorNombre.Width = 200;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.DataPropertyName = "ReceptorRFC";
            this.ReceptorRFC.HeaderText = "RFC (Receptor)";
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.ReadOnly = true;
            // 
            // ReceptorNombre
            // 
            this.ReceptorNombre.DataPropertyName = "ReceptorNombre";
            this.ReceptorNombre.HeaderText = "Receptor";
            this.ReceptorNombre.Name = "ReceptorNombre";
            this.ReceptorNombre.ReadOnly = true;
            this.ReceptorNombre.Width = 200;
            // 
            // FechaEmision
            // 
            this.FechaEmision.DataPropertyName = "FechaEmision";
            this.FechaEmision.HeaderText = "Fec. Emisión";
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.ReadOnly = true;
            // 
            // FechaTimbre
            // 
            this.FechaTimbre.DataPropertyName = "FechaTimbre";
            this.FechaTimbre.HeaderText = "Fec. Cert.";
            this.FechaTimbre.Name = "FechaTimbre";
            this.FechaTimbre.ReadOnly = true;
            // 
            // IdDocumento
            // 
            this.IdDocumento.DataPropertyName = "IdDocumento";
            this.IdDocumento.HeaderText = "IdDocumento";
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.ReadOnly = true;
            this.IdDocumento.Width = 200;
            // 
            // EstadoSAT
            // 
            this.EstadoSAT.DataPropertyName = "EstadoSAT";
            this.EstadoSAT.HeaderText = "Estado SAT";
            this.EstadoSAT.Name = "EstadoSAT";
            this.EstadoSAT.ReadOnly = true;
            // 
            // NoCertificado
            // 
            this.NoCertificado.DataPropertyName = "NoCertificado";
            this.NoCertificado.HeaderText = "No. Certificado";
            this.NoCertificado.Name = "NoCertificado";
            this.NoCertificado.ReadOnly = true;
            // 
            // RFCProvCertif
            // 
            this.RFCProvCertif.DataPropertyName = "RFCProvCertif";
            this.RFCProvCertif.HeaderText = "RFCProvCertif";
            this.RFCProvCertif.Name = "RFCProvCertif";
            this.RFCProvCertif.ReadOnly = true;
            // 
            // ClaveMoneda
            // 
            this.ClaveMoneda.DataPropertyName = "ClaveMoneda";
            this.ClaveMoneda.HeaderText = "Moneda";
            this.ClaveMoneda.Name = "ClaveMoneda";
            this.ClaveMoneda.ReadOnly = true;
            this.ClaveMoneda.Width = 50;
            // 
            // ClaveMetodoPago
            // 
            this.ClaveMetodoPago.DataPropertyName = "ClaveMetodoPago";
            this.ClaveMetodoPago.HeaderText = "Met. Pago";
            this.ClaveMetodoPago.Name = "ClaveMetodoPago";
            this.ClaveMetodoPago.ReadOnly = true;
            this.ClaveMetodoPago.Width = 65;
            // 
            // ClaveFormaPago
            // 
            this.ClaveFormaPago.DataPropertyName = "ClaveFormaPago";
            this.ClaveFormaPago.HeaderText = "Forma Pago";
            this.ClaveFormaPago.Name = "ClaveFormaPago";
            this.ClaveFormaPago.ReadOnly = true;
            this.ClaveFormaPago.Width = 65;
            // 
            // ClaveUsoCFDI
            // 
            this.ClaveUsoCFDI.DataPropertyName = "ClaveUsoCFDI";
            this.ClaveUsoCFDI.HeaderText = "Uso CFDI";
            this.ClaveUsoCFDI.Name = "ClaveUsoCFDI";
            this.ClaveUsoCFDI.ReadOnly = true;
            this.ClaveUsoCFDI.Width = 65;
            // 
            // RetencionISR
            // 
            this.RetencionISR.DataPropertyName = "RetencionISR";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            this.RetencionISR.DefaultCellStyle = dataGridViewCellStyle1;
            this.RetencionISR.HeaderText = "Ret. ISR";
            this.RetencionISR.Name = "RetencionISR";
            this.RetencionISR.ReadOnly = true;
            this.RetencionISR.Width = 75;
            // 
            // RetencionIVA
            // 
            this.RetencionIVA.DataPropertyName = "RetencionIVA";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            this.RetencionIVA.DefaultCellStyle = dataGridViewCellStyle2;
            this.RetencionIVA.HeaderText = "Ret. IVA";
            this.RetencionIVA.Name = "RetencionIVA";
            this.RetencionIVA.ReadOnly = true;
            this.RetencionIVA.Width = 75;
            // 
            // RetencionIEPS
            // 
            this.RetencionIEPS.DataPropertyName = "RetencionIEPS";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            this.RetencionIEPS.DefaultCellStyle = dataGridViewCellStyle3;
            this.RetencionIEPS.HeaderText = "Ret. IEPS";
            this.RetencionIEPS.Name = "RetencionIEPS";
            this.RetencionIEPS.ReadOnly = true;
            this.RetencionIEPS.Width = 75;
            // 
            // TrasladoIVA
            // 
            this.TrasladoIVA.DataPropertyName = "TrasladoIVA";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            this.TrasladoIVA.DefaultCellStyle = dataGridViewCellStyle4;
            this.TrasladoIVA.HeaderText = "Tras. IVA";
            this.TrasladoIVA.Name = "TrasladoIVA";
            this.TrasladoIVA.ReadOnly = true;
            this.TrasladoIVA.Width = 75;
            // 
            // TrasladoIEPS
            // 
            this.TrasladoIEPS.DataPropertyName = "TrasladoIEPS";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            this.TrasladoIEPS.DefaultCellStyle = dataGridViewCellStyle5;
            this.TrasladoIEPS.HeaderText = "Tras. IEPS";
            this.TrasladoIEPS.Name = "TrasladoIEPS";
            this.TrasladoIEPS.ReadOnly = true;
            this.TrasladoIEPS.Width = 75;
            // 
            // SubTotal
            // 
            this.SubTotal.DataPropertyName = "SubTotal";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            this.SubTotal.DefaultCellStyle = dataGridViewCellStyle6;
            this.SubTotal.HeaderText = "SubTotal";
            this.SubTotal.Name = "SubTotal";
            this.SubTotal.ReadOnly = true;
            // 
            // Descuento
            // 
            this.Descuento.DataPropertyName = "Descuento";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            this.Descuento.DefaultCellStyle = dataGridViewCellStyle7;
            this.Descuento.HeaderText = "Descuento";
            this.Descuento.Name = "Descuento";
            this.Descuento.ReadOnly = true;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            this.Total.DefaultCellStyle = dataGridViewCellStyle8;
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // Situacion
            // 
            this.Situacion.DataPropertyName = "Situacion";
            this.Situacion.HeaderText = "Situación";
            this.Situacion.Name = "Situacion";
            this.Situacion.ReadOnly = true;
            // 
            // Resultado
            // 
            this.Resultado.DataPropertyName = "Resultado";
            this.Resultado.HeaderText = "Resultado";
            this.Resultado.Name = "Resultado";
            this.Resultado.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Resultado.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ComprobanteValidadorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1227, 580);
            this.Controls.Add(this.dataGridResult);
            this.Controls.Add(this.TValida);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ComprobanteValidadorForm";
            this.Text = "Validador de Comprobantes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ComprobanteValidadorForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridResult)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip1;
        private TComprobanteValdiadorControl TValida;
        private System.Windows.Forms.DataGridView dataGridResult;
        private System.Windows.Forms.ToolStripStatusLabel Progreso;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Registrado;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoComprobante;
        private System.Windows.Forms.DataGridViewTextBoxColumn Version;
        private System.Windows.Forms.DataGridViewTextBoxColumn Folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Serie;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmisorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmisorNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaEmision;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaTimbre;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn EstadoSAT;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoCertificado;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFCProvCertif;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveMoneda;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveMetodoPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveFormaPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveUsoCFDI;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetencionISR;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetencionIVA;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetencionIEPS;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrasladoIVA;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrasladoIEPS;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descuento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn Situacion;
        private System.Windows.Forms.DataGridViewButtonColumn Resultado;
    }
}