﻿using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Jaeger.Aplication.StrikerEureka.Log;
using Jaeger.Domain.StrikerEureka.Log.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Util.Services;

namespace Jaeger.UI.Forms.Validador {
    public partial class ComprobantesRepositorioLogForm : Form {
        protected ILogValidadorService service;
        private BindingList<CFDResponseValidacionModel> datos = new BindingList<CFDResponseValidacionModel>();

        public ComprobantesRepositorioLogForm() {
            InitializeComponent();
        }

        private void ComprobanteLogRepositorioForm_Load(object sender, EventArgs e) {
            this.service = new LogValidadorService();
            this.ToolBarComboStatus.ComboBox.DataSource = this.service.GetEstados();
            this.ToolBarComboStatus.ComboBox.DisplayMember = "Descripcion";
            this.ToolBarComboStatus.ComboBox.ValueMember = "Descripcion";
            this.dataGridComprobantes.DataGridCommon();
            this.dataGridComprobantes.MultiSelect = true;
            this.dataGridComprobantes.ReadOnly = true;
        }

        private void ToolBarButtonCrear_Click(object sender, EventArgs e) {
            this.service.CrearBase();
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.dataGridComprobantes.DataSource = this.datos;
        }

        private void ToolBarButtonValidacion_Click(object sender, EventArgs e) {
            if (this.dataGridComprobantes.CurrentRow != null) {
                var seleccionado = this.dataGridComprobantes.CurrentRow.DataBoundItem as CFDResponseValidacionModel;
                if (seleccionado != null) {
                    var validacion = Domain.Comprobante.Entities.ComprobanteValidacionPrinter.Json(seleccionado.JValidacion);
                    if (validacion != null) {
                        var imprimirValidacion = new ReporteForm(validacion);
                        imprimirValidacion.Show();
                    }
                }
            }
        }

        private void ToolBarButton_Importar_Archivo_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog { Filter = "*.json|*.JSON" };
            if (openFile.ShowDialog(this) == DialogResult.OK) {
                this.ToolBarButton_Importar_Archivo.Tag = openFile.FileName;
                if (System.IO.File.Exists(openFile.FileName)) {
                    using (var espera = new WaitingForm(this.Importar)) {
                        espera.Text = "Importando ...";
                        espera.ShowDialog(this);
                    }
                }
            }
        }

        private void ToolBarButton_Importar_Carpeta_Click(object sender, EventArgs e) {
            var openFolder = new FolderBrowserDialog();
            if (openFolder.ShowDialog() != DialogResult.OK) {
                return;
            }

            ToolBarButton_Importar_Carpeta.Tag = openFolder.SelectedPath;
            using (var espera = new WaitingForm(this.ImportarCarpeta)) {
                espera.Text = "Importando ...";
                espera.ShowDialog(this);
            }
        }

        private void contextMenu_Verificar_Click(object sender, EventArgs e) {
            if (this.dataGridComprobantes.CurrentRow != null) {
                BindingList<CFDResponseValidacionModel> da = new BindingList<CFDResponseValidacionModel>();
                foreach (var item in this.dataGridComprobantes.SelectedRows.Cast<DataGridViewRow>()) {
                    da.Add(item.DataBoundItem as CFDResponseValidacionModel);
                }

                this.service.Verificar(da);
            }
        }

        private void contextMenu_Archivo_XML_Click(object sender, EventArgs e) {
            if (this.dataGridComprobantes.CurrentRow != null) {
                var seleccionado = this.dataGridComprobantes.CurrentRow.DataBoundItem as CFDResponseValidacionModel;
                if (seleccionado != null) {
                    var saveFile = new SaveFileDialog { AddExtension = true, DefaultExt = "xml", FileName = seleccionado.IdDocumento };
                    if (saveFile.ShowDialog(this) == DialogResult.OK) {
                        if (FileService.WriteFileB64(seleccionado.XML, saveFile.FileName)) {
                            if (MessageBox.Show(this, string.Format("Properties.Resources.MessageBox_AbrirDocumento", saveFile.FileName), "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes) {
                                try {
                                    System.Diagnostics.Process.Start(saveFile.FileName);
                                } catch (Exception ex) {
                                    MessageBox.Show(this, ex.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void contextMenu_Archivo_PDF_Click(object sender, EventArgs e) {
            if (this.dataGridComprobantes.CurrentRow != null) {
                var seleccionado = this.dataGridComprobantes.CurrentRow.DataBoundItem as CFDResponseValidacionModel;
                if (seleccionado != null) {
                    if (seleccionado.PDF != null) {
                        var saveFile = new SaveFileDialog { AddExtension = true, DefaultExt = "pdf", FileName = seleccionado.IdDocumento };
                        if (saveFile.ShowDialog(this) == DialogResult.OK) {
                            if (FileService.WriteFileB64(seleccionado.PDF, saveFile.FileName)) {
                                if (MessageBox.Show(this, string.Format("Properties.Resources.MessageBox_AbrirDocumento", saveFile.FileName), "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes) {
                                    try {
                                        System.Diagnostics.Process.Start(saveFile.FileName);
                                    } catch (Exception ex) {
                                        MessageBox.Show(this, ex.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    }
                                }
                            }
                        }
                    } else {
                        MessageBox.Show(this, "Properties.Resources.MessageBox_No_Existe_Objeto", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void Importar() {
            if (this.ToolBarButton_Importar_Archivo.Tag != null) {
                var filename = (string)this.ToolBarButton_Importar_Archivo.Tag;
                this.service.WriteLog(filename);
                this.ToolBarButton_Importar_Archivo.Tag = null;
            }
        }

        private void ImportarCarpeta() {
            var d = ToolBarButton_Importar_Carpeta.Tag as string;
            this.service.LoadFolderLog(d, true);
        }

        private void Consultar() {
            this.datos = this.service.GetList(this.ToolBarComboStatus.Text, int.Parse(this.ToolBarTextCantidad.TextBox.Text));
        }

        private void dataGridComprobantes_BindingContextChanged(object sender, EventArgs e) {
        }

        private void dataGridComprobantes_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e) {
            this.toolStripStatusLabel1.Text = "Filas: " + this.dataGridComprobantes.Rows.Count.ToString() + " Server: " + Aplication.Base.ConfigService.Synapsis.RDS.Edita.HostName;
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
