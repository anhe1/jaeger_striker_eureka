﻿namespace Jaeger.UI.Forms.Validador {
    partial class TComprobanteValdiadorControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.ToolBar = new System.Windows.Forms.ToolStrip();
            this.LabelCarpeta = new System.Windows.Forms.ToolStripLabel();
            this.Carpeta = new System.Windows.Forms.ToolStripComboBox();
            this.Buscar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Agregar = new System.Windows.Forms.ToolStripButton();
            this.Remover = new System.Windows.Forms.ToolStripSplitButton();
            this.RemoverTodo = new System.Windows.Forms.ToolStripMenuItem();
            this.RemoverSeleccionado = new System.Windows.Forms.ToolStripMenuItem();
            this.Actualizar = new System.Windows.Forms.ToolStripButton();
            this.Validar = new System.Windows.Forms.ToolStripDropDownButton();
            this.Backup = new System.Windows.Forms.ToolStripDropDownButton();
            this.BackupTodos = new System.Windows.Forms.ToolStripMenuItem();
            this.PDF = new System.Windows.Forms.ToolStripDropDownButton();
            this.Imprimir = new System.Windows.Forms.ToolStripDropDownButton();
            this.ImprimirValidacion = new System.Windows.Forms.ToolStripMenuItem();
            this.Herramientas = new System.Windows.Forms.ToolStripDropDownButton();
            this.Configuracion = new System.Windows.Forms.ToolStripMenuItem();
            this.Cerrar = new System.Windows.Forms.ToolStripButton();
            this.ToolBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LabelCarpeta,
            this.Carpeta,
            this.Buscar,
            this.toolStripSeparator1,
            this.Agregar,
            this.Remover,
            this.Actualizar,
            this.Validar,
            this.Backup,
            this.PDF,
            this.Imprimir,
            this.Herramientas,
            this.Cerrar});
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Size = new System.Drawing.Size(1067, 25);
            this.ToolBar.TabIndex = 0;
            this.ToolBar.Text = "toolStrip1";
            // 
            // LabelCarpeta
            // 
            this.LabelCarpeta.Name = "LabelCarpeta";
            this.LabelCarpeta.Size = new System.Drawing.Size(51, 22);
            this.LabelCarpeta.Text = "Carpeta:";
            // 
            // Carpeta
            // 
            this.Carpeta.Name = "Carpeta";
            this.Carpeta.Size = new System.Drawing.Size(205, 25);
            // 
            // Buscar
            // 
            this.Buscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Buscar.Image = global::Jaeger.UI.Properties.Resources.search_folder_16px;
            this.Buscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(23, 22);
            this.Buscar.Text = "toolStripButton1";
            this.Buscar.Click += new System.EventHandler(this.Buscar_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // Agregar
            // 
            this.Agregar.Image = global::Jaeger.UI.Properties.Resources.add_16px;
            this.Agregar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Agregar.Name = "Agregar";
            this.Agregar.Size = new System.Drawing.Size(69, 22);
            this.Agregar.Text = "Agregar";
            this.Agregar.Click += new System.EventHandler(this.Agregar_Click);
            // 
            // Remover
            // 
            this.Remover.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RemoverTodo,
            this.RemoverSeleccionado});
            this.Remover.Image = global::Jaeger.UI.Properties.Resources.delete_16px;
            this.Remover.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Remover.Name = "Remover";
            this.Remover.Size = new System.Drawing.Size(86, 22);
            this.Remover.Text = "Remover";
            // 
            // RemoverTodo
            // 
            this.RemoverTodo.Name = "RemoverTodo";
            this.RemoverTodo.Size = new System.Drawing.Size(144, 22);
            this.RemoverTodo.Text = "Todo";
            // 
            // RemoverSeleccionado
            // 
            this.RemoverSeleccionado.Name = "RemoverSeleccionado";
            this.RemoverSeleccionado.Size = new System.Drawing.Size(144, 22);
            this.RemoverSeleccionado.Text = "Seleccionado";
            // 
            // Actualizar
            // 
            this.Actualizar.Image = global::Jaeger.UI.Properties.Resources.refresh_16px;
            this.Actualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Size = new System.Drawing.Size(79, 22);
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.Click += new System.EventHandler(this.Actualizar_Click);
            // 
            // Validar
            // 
            this.Validar.Image = global::Jaeger.UI.Properties.Resources.protect_16px;
            this.Validar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Validar.Name = "Validar";
            this.Validar.Size = new System.Drawing.Size(71, 22);
            this.Validar.Text = "Validar";
            // 
            // Backup
            // 
            this.Backup.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BackupTodos});
            this.Backup.Image = global::Jaeger.UI.Properties.Resources.data_protection_16px;
            this.Backup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Backup.Name = "Backup";
            this.Backup.Size = new System.Drawing.Size(75, 22);
            this.Backup.Text = "Backup";
            // 
            // BackupTodos
            // 
            this.BackupTodos.Name = "BackupTodos";
            this.BackupTodos.Size = new System.Drawing.Size(105, 22);
            this.BackupTodos.Text = "Todos";
            // 
            // PDF
            // 
            this.PDF.Image = global::Jaeger.UI.Properties.Resources.pdf_16px;
            this.PDF.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PDF.Name = "PDF";
            this.PDF.Size = new System.Drawing.Size(57, 22);
            this.PDF.Text = "PDF";
            // 
            // Imprimir
            // 
            this.Imprimir.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ImprimirValidacion});
            this.Imprimir.Image = global::Jaeger.UI.Properties.Resources.print_16px;
            this.Imprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Size = new System.Drawing.Size(82, 22);
            this.Imprimir.Text = "Imprimir";
            // 
            // ImprimirValidacion
            // 
            this.ImprimirValidacion.Name = "ImprimirValidacion";
            this.ImprimirValidacion.Size = new System.Drawing.Size(177, 22);
            this.ImprimirValidacion.Text = "Imprimir validación";
            // 
            // Herramientas
            // 
            this.Herramientas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Configuracion});
            this.Herramientas.Image = global::Jaeger.UI.Properties.Resources.toolbox_16px;
            this.Herramientas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Size = new System.Drawing.Size(107, 22);
            this.Herramientas.Text = "Herramientas";
            // 
            // Configuracion
            // 
            this.Configuracion.Image = global::Jaeger.UI.Properties.Resources.settings_16px;
            this.Configuracion.Name = "Configuracion";
            this.Configuracion.Size = new System.Drawing.Size(150, 22);
            this.Configuracion.Text = "Configuración";
            // 
            // Cerrar
            // 
            this.Cerrar.Image = global::Jaeger.UI.Properties.Resources.close_window_16px;
            this.Cerrar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(59, 22);
            this.Cerrar.Text = "Cerrar";
            // 
            // TComprobanteValdiadorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ToolBar);
            this.Name = "TComprobanteValdiadorControl";
            this.Size = new System.Drawing.Size(1067, 25);
            this.Load += new System.EventHandler(this.TComprobanteValdiadorControl_Load);
            this.ToolBar.ResumeLayout(false);
            this.ToolBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip ToolBar;
        private System.Windows.Forms.ToolStripLabel LabelCarpeta;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton Agregar;
        internal System.Windows.Forms.ToolStripSplitButton Remover;
        internal System.Windows.Forms.ToolStripButton Actualizar;
        internal System.Windows.Forms.ToolStripDropDownButton Validar;
        internal System.Windows.Forms.ToolStripDropDownButton Backup;
        internal System.Windows.Forms.ToolStripDropDownButton PDF;
        internal System.Windows.Forms.ToolStripComboBox Carpeta;
        internal System.Windows.Forms.ToolStripButton Buscar;
        internal System.Windows.Forms.ToolStripDropDownButton Imprimir;
        internal System.Windows.Forms.ToolStripDropDownButton Herramientas;
        internal System.Windows.Forms.ToolStripButton Cerrar;
        private System.Windows.Forms.ToolStripMenuItem Configuracion;
        private System.Windows.Forms.ToolStripMenuItem RemoverTodo;
        private System.Windows.Forms.ToolStripMenuItem RemoverSeleccionado;
        private System.Windows.Forms.ToolStripMenuItem ImprimirValidacion;
        private System.Windows.Forms.ToolStripMenuItem BackupTodos;
    }
}
