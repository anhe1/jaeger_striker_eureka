﻿namespace Jaeger.UI.Forms.Validador {
    partial class ValidaRetencionForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.title = new System.Windows.Forms.Label();
            this.TValida = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.EmisorRFC = new System.Windows.Forms.TextBox();
            this.ReceptorRFC = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.IdDocumento = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Log = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.BackColor = System.Drawing.Color.White;
            this.title.Location = new System.Drawing.Point(11, 11);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(314, 28);
            this.title.TabIndex = 8;
            this.title.Text = "Verifica el folio fiscal de las facturas de retenciones e información de pagos";
            // 
            // TValida
            // 
            this.TValida.Dock = System.Windows.Forms.DockStyle.Top;
            this.TValida.Etiqueta = "";
            this.TValida.Location = new System.Drawing.Point(0, 50);
            this.TValida.Name = "TValida";
            this.TValida.ShowActualizar = true;
            this.TValida.ShowCerrar = true;
            this.TValida.ShowEditar = false;
            this.TValida.ShowGuardar = false;
            this.TValida.ShowHerramientas = true;
            this.TValida.ShowImprimir = false;
            this.TValida.ShowNuevo = true;
            this.TValida.ShowRemover = false;
            this.TValida.Size = new System.Drawing.Size(338, 25);
            this.TValida.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(338, 50);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Emisor RFC:";
            // 
            // EmisorRFC
            // 
            this.EmisorRFC.Location = new System.Drawing.Point(96, 23);
            this.EmisorRFC.Name = "EmisorRFC";
            this.EmisorRFC.Size = new System.Drawing.Size(101, 20);
            this.EmisorRFC.TabIndex = 10;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.Location = new System.Drawing.Point(96, 49);
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.Size = new System.Drawing.Size(101, 20);
            this.ReceptorRFC.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Receptor RFC";
            // 
            // IdDocumento
            // 
            this.IdDocumento.Location = new System.Drawing.Point(58, 75);
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.Size = new System.Drawing.Size(263, 20);
            this.IdDocumento.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "UUID:";
            // 
            // Log
            // 
            this.Log.Location = new System.Drawing.Point(18, 116);
            this.Log.Multiline = true;
            this.Log.Name = "Log";
            this.Log.Size = new System.Drawing.Size(303, 124);
            this.Log.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Log:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.EmisorRFC);
            this.groupBox1.Controls.Add(this.Log);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.IdDocumento);
            this.groupBox1.Controls.Add(this.ReceptorRFC);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 75);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(338, 254);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            // 
            // ValidaRetencionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 329);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.title);
            this.Controls.Add(this.TValida);
            this.Controls.Add(this.pictureBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ValidaRetencionForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Retención e Infomracion de pagos";
            this.Load += new System.EventHandler(this.ValidaRetencionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label title;
        private Common.Forms.ToolBarStandarControl TValida;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox EmisorRFC;
        private System.Windows.Forms.TextBox ReceptorRFC;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox IdDocumento;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Log;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}