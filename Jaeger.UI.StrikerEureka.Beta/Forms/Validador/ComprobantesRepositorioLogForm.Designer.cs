﻿namespace Jaeger.UI.Forms.Validador {
    partial class ComprobantesRepositorioLogForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ToolBar = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.ToolBarComboStatus = new System.Windows.Forms.ToolStripComboBox();
            this.ToolBarButtonImportar = new System.Windows.Forms.ToolStripDropDownButton();
            this.ToolBarButton_Importar_Archivo = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolBarButton_Importar_Carpeta = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolBarTextCantidad = new System.Windows.Forms.ToolStripTextBox();
            this.dataGridComprobantes = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comprobado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdComprobante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdDirectorio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Version = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MetaData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoComprobante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmisorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Emisor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Receptor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaEmision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaCertificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaValidacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoCertificado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoCertificadoProvCertif = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RFCProvCertif = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LugarExpedicion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MetodoPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FormaPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UsoCFDI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descuento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Registrado = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Situacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Resultado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenu_Verificar = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenu_Remover = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenu_Archivo = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenu_Archivo_XML = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenu_Archivo_PDF = new System.Windows.Forms.ToolStripMenuItem();
            this.StatusBar = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ToolBarButtonActualizar = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonImprimir = new System.Windows.Forms.ToolStripDropDownButton();
            this.ToolBarButtonValidacion = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolBarButtonHerramientas = new System.Windows.Forms.ToolStripDropDownButton();
            this.ToolBarButtonCrear = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolBarButtonCerrar = new System.Windows.Forms.ToolStripButton();
            this.ToolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridComprobantes)).BeginInit();
            this.contextMenu.SuspendLayout();
            this.StatusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.ToolBarComboStatus,
            this.ToolBarTextCantidad,
            this.ToolBarButtonImportar,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonImprimir,
            this.ToolBarButtonHerramientas,
            this.ToolBarButtonCerrar});
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Size = new System.Drawing.Size(1032, 25);
            this.ToolBar.TabIndex = 0;
            this.ToolBar.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(42, 22);
            this.toolStripLabel1.Text = "Status:";
            // 
            // ToolBarComboStatus
            // 
            this.ToolBarComboStatus.Name = "ToolBarComboStatus";
            this.ToolBarComboStatus.Size = new System.Drawing.Size(100, 25);
            // 
            // ToolBarButtonImportar
            // 
            this.ToolBarButtonImportar.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolBarButton_Importar_Archivo,
            this.ToolBarButton_Importar_Carpeta});
            this.ToolBarButtonImportar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonImportar.Name = "ToolBarButtonImportar";
            this.ToolBarButtonImportar.Size = new System.Drawing.Size(66, 22);
            this.ToolBarButtonImportar.Text = "Importar";
            // 
            // ToolBarButton_Importar_Archivo
            // 
            this.ToolBarButton_Importar_Archivo.Name = "ToolBarButton_Importar_Archivo";
            this.ToolBarButton_Importar_Archivo.Size = new System.Drawing.Size(141, 22);
            this.ToolBarButton_Importar_Archivo.Text = "Archivo LOG";
            this.ToolBarButton_Importar_Archivo.Click += new System.EventHandler(this.ToolBarButton_Importar_Archivo_Click);
            // 
            // ToolBarButton_Importar_Carpeta
            // 
            this.ToolBarButton_Importar_Carpeta.Name = "ToolBarButton_Importar_Carpeta";
            this.ToolBarButton_Importar_Carpeta.Size = new System.Drawing.Size(141, 22);
            this.ToolBarButton_Importar_Carpeta.Text = "Carpeta";
            this.ToolBarButton_Importar_Carpeta.Click += new System.EventHandler(this.ToolBarButton_Importar_Carpeta_Click);
            // 
            // ToolBarTextCantidad
            // 
            this.ToolBarTextCantidad.MaxLength = 14;
            this.ToolBarTextCantidad.Name = "ToolBarTextCantidad";
            this.ToolBarTextCantidad.Size = new System.Drawing.Size(45, 25);
            this.ToolBarTextCantidad.Text = "1000";
            this.ToolBarTextCantidad.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dataGridComprobantes
            // 
            this.dataGridComprobantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridComprobantes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Comprobado,
            this.IdComprobante,
            this.IdDirectorio,
            this.Version,
            this.MetaData,
            this.TipoComprobante,
            this.Folio,
            this.Serie,
            this.EmisorRFC,
            this.Emisor,
            this.ReceptorRFC,
            this.Receptor,
            this.IdDocumento,
            this.Estado,
            this.FechaEmision,
            this.FechaCertificacion,
            this.FechaValidacion,
            this.NoCertificado,
            this.NoCertificadoProvCertif,
            this.RFCProvCertif,
            this.LugarExpedicion,
            this.MetodoPago,
            this.FormaPago,
            this.UsoCFDI,
            this.SubTotal,
            this.Descuento,
            this.Total,
            this.Registrado,
            this.Situacion,
            this.Resultado});
            this.dataGridComprobantes.ContextMenuStrip = this.contextMenu;
            this.dataGridComprobantes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridComprobantes.Location = new System.Drawing.Point(0, 25);
            this.dataGridComprobantes.Name = "dataGridComprobantes";
            this.dataGridComprobantes.Size = new System.Drawing.Size(1032, 520);
            this.dataGridComprobantes.TabIndex = 1;
            this.dataGridComprobantes.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridComprobantes_DataBindingComplete);
            this.dataGridComprobantes.BindingContextChanged += new System.EventHandler(this.dataGridComprobantes_BindingContextChanged);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Width = 50;
            // 
            // Comprobado
            // 
            this.Comprobado.DataPropertyName = "Comprobado";
            this.Comprobado.HeaderText = "Comprobado";
            this.Comprobado.Name = "Comprobado";
            this.Comprobado.Width = 75;
            // 
            // IdComprobante
            // 
            this.IdComprobante.DataPropertyName = "IdComprobante";
            this.IdComprobante.HeaderText = "IdComprobante";
            this.IdComprobante.Name = "IdComprobante";
            this.IdComprobante.ReadOnly = true;
            this.IdComprobante.Width = 50;
            // 
            // IdDirectorio
            // 
            this.IdDirectorio.DataPropertyName = "IdDirectorio";
            this.IdDirectorio.HeaderText = "IdDirectorio";
            this.IdDirectorio.Name = "IdDirectorio";
            this.IdDirectorio.ReadOnly = true;
            this.IdDirectorio.Width = 50;
            // 
            // Version
            // 
            this.Version.DataPropertyName = "Version";
            this.Version.HeaderText = "Versión";
            this.Version.Name = "Version";
            this.Version.ReadOnly = true;
            this.Version.Width = 50;
            // 
            // MetaData
            // 
            this.MetaData.DataPropertyName = "MetaData";
            this.MetaData.HeaderText = "Meta";
            this.MetaData.Name = "MetaData";
            this.MetaData.ReadOnly = true;
            this.MetaData.Width = 50;
            // 
            // TipoComprobante
            // 
            this.TipoComprobante.DataPropertyName = "TipoComprobante";
            this.TipoComprobante.HeaderText = "Tipo";
            this.TipoComprobante.Name = "TipoComprobante";
            this.TipoComprobante.ReadOnly = true;
            this.TipoComprobante.Width = 50;
            // 
            // Folio
            // 
            this.Folio.DataPropertyName = "Folio";
            this.Folio.HeaderText = "Folio";
            this.Folio.Name = "Folio";
            this.Folio.ReadOnly = true;
            // 
            // Serie
            // 
            this.Serie.DataPropertyName = "Serie";
            this.Serie.HeaderText = "Serie";
            this.Serie.Name = "Serie";
            this.Serie.ReadOnly = true;
            // 
            // EmisorRFC
            // 
            this.EmisorRFC.DataPropertyName = "EmisorRFC";
            this.EmisorRFC.HeaderText = "Emisor (RFC)";
            this.EmisorRFC.Name = "EmisorRFC";
            this.EmisorRFC.ReadOnly = true;
            // 
            // Emisor
            // 
            this.Emisor.DataPropertyName = "Emisor";
            this.Emisor.HeaderText = "Emisor";
            this.Emisor.Name = "Emisor";
            this.Emisor.ReadOnly = true;
            this.Emisor.Width = 200;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.DataPropertyName = "ReceptorRFC";
            this.ReceptorRFC.HeaderText = "Receptor (RFC)";
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.ReadOnly = true;
            // 
            // Receptor
            // 
            this.Receptor.DataPropertyName = "Receptor";
            this.Receptor.HeaderText = "Receptor";
            this.Receptor.Name = "Receptor";
            this.Receptor.ReadOnly = true;
            this.Receptor.Width = 200;
            // 
            // IdDocumento
            // 
            this.IdDocumento.DataPropertyName = "IdDocumento";
            this.IdDocumento.HeaderText = "IdDocumento";
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.ReadOnly = true;
            this.IdDocumento.Width = 150;
            // 
            // Estado
            // 
            this.Estado.DataPropertyName = "EstadoSAT";
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            this.Estado.ReadOnly = true;
            this.Estado.Width = 50;
            // 
            // FechaEmision
            // 
            this.FechaEmision.DataPropertyName = "FechaEmision";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.Format = "d";
            dataGridViewCellStyle8.NullValue = null;
            this.FechaEmision.DefaultCellStyle = dataGridViewCellStyle8;
            this.FechaEmision.HeaderText = "Fec. Emisión";
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.ReadOnly = true;
            this.FechaEmision.Width = 75;
            // 
            // FechaCertificacion
            // 
            this.FechaCertificacion.DataPropertyName = "FechaCertificacion";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.Format = "d";
            this.FechaCertificacion.DefaultCellStyle = dataGridViewCellStyle9;
            this.FechaCertificacion.HeaderText = "Fec. Cert.";
            this.FechaCertificacion.Name = "FechaCertificacion";
            this.FechaCertificacion.ReadOnly = true;
            this.FechaCertificacion.Width = 75;
            // 
            // FechaValidacion
            // 
            this.FechaValidacion.DataPropertyName = "FechaValidacion";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.Format = "d";
            this.FechaValidacion.DefaultCellStyle = dataGridViewCellStyle10;
            this.FechaValidacion.HeaderText = "Fec. Val.";
            this.FechaValidacion.Name = "FechaValidacion";
            this.FechaValidacion.ReadOnly = true;
            this.FechaValidacion.Width = 75;
            // 
            // NoCertificado
            // 
            this.NoCertificado.DataPropertyName = "NoCertificado";
            this.NoCertificado.HeaderText = "NoCertificado";
            this.NoCertificado.Name = "NoCertificado";
            this.NoCertificado.ReadOnly = true;
            this.NoCertificado.Width = 110;
            // 
            // NoCertificadoProvCertif
            // 
            this.NoCertificadoProvCertif.DataPropertyName = "NoCertificadoProvCertif";
            this.NoCertificadoProvCertif.HeaderText = "NoCertificadoProvCertif";
            this.NoCertificadoProvCertif.Name = "NoCertificadoProvCertif";
            this.NoCertificadoProvCertif.ReadOnly = true;
            this.NoCertificadoProvCertif.Width = 110;
            // 
            // RFCProvCertif
            // 
            this.RFCProvCertif.DataPropertyName = "RFCProvCertif";
            this.RFCProvCertif.HeaderText = "RFCProvCertif";
            this.RFCProvCertif.Name = "RFCProvCertif";
            this.RFCProvCertif.ReadOnly = true;
            // 
            // LugarExpedicion
            // 
            this.LugarExpedicion.DataPropertyName = "LugarExpedicion";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.LugarExpedicion.DefaultCellStyle = dataGridViewCellStyle11;
            this.LugarExpedicion.HeaderText = "Expedición";
            this.LugarExpedicion.Name = "LugarExpedicion";
            this.LugarExpedicion.ReadOnly = true;
            this.LugarExpedicion.Width = 50;
            // 
            // MetodoPago
            // 
            this.MetodoPago.DataPropertyName = "MetodoPago";
            this.MetodoPago.HeaderText = "Met. Pago";
            this.MetodoPago.Name = "MetodoPago";
            this.MetodoPago.ReadOnly = true;
            this.MetodoPago.Width = 75;
            // 
            // FormaPago
            // 
            this.FormaPago.DataPropertyName = "FormaPago";
            this.FormaPago.HeaderText = "Forma Pago";
            this.FormaPago.Name = "FormaPago";
            this.FormaPago.ReadOnly = true;
            this.FormaPago.Width = 75;
            // 
            // UsoCFDI
            // 
            this.UsoCFDI.DataPropertyName = "UsoCFDI";
            this.UsoCFDI.HeaderText = "UsoCFDI";
            this.UsoCFDI.Name = "UsoCFDI";
            this.UsoCFDI.ReadOnly = true;
            this.UsoCFDI.Width = 75;
            // 
            // SubTotal
            // 
            this.SubTotal.DataPropertyName = "SubTotal";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "N2";
            this.SubTotal.DefaultCellStyle = dataGridViewCellStyle12;
            this.SubTotal.HeaderText = "SubTotal";
            this.SubTotal.Name = "SubTotal";
            this.SubTotal.ReadOnly = true;
            this.SubTotal.Width = 75;
            // 
            // Descuento
            // 
            this.Descuento.DataPropertyName = "Descuento";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "N2";
            this.Descuento.DefaultCellStyle = dataGridViewCellStyle13;
            this.Descuento.HeaderText = "Descuento";
            this.Descuento.Name = "Descuento";
            this.Descuento.ReadOnly = true;
            this.Descuento.Width = 75;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.Format = "N2";
            this.Total.DefaultCellStyle = dataGridViewCellStyle14;
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.Width = 75;
            // 
            // Registrado
            // 
            this.Registrado.DataPropertyName = "Registrado";
            this.Registrado.HeaderText = "Registrado";
            this.Registrado.Name = "Registrado";
            this.Registrado.ReadOnly = true;
            this.Registrado.Width = 50;
            // 
            // Situacion
            // 
            this.Situacion.DataPropertyName = "Situacion";
            this.Situacion.HeaderText = "Situación";
            this.Situacion.Name = "Situacion";
            this.Situacion.ReadOnly = true;
            this.Situacion.Width = 50;
            // 
            // Resultado
            // 
            this.Resultado.DataPropertyName = "Resultado";
            this.Resultado.HeaderText = "Resultado";
            this.Resultado.Name = "Resultado";
            this.Resultado.ReadOnly = true;
            this.Resultado.Width = 50;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextMenu_Verificar,
            this.contextMenu_Remover,
            this.contextMenu_Archivo});
            this.contextMenu.Name = "contextMenuStrip1";
            this.contextMenu.Size = new System.Drawing.Size(122, 70);
            // 
            // contextMenu_Verificar
            // 
            this.contextMenu_Verificar.Name = "contextMenu_Verificar";
            this.contextMenu_Verificar.Size = new System.Drawing.Size(121, 22);
            this.contextMenu_Verificar.Text = "Verificar";
            this.contextMenu_Verificar.Click += new System.EventHandler(this.contextMenu_Verificar_Click);
            // 
            // contextMenu_Remover
            // 
            this.contextMenu_Remover.Name = "contextMenu_Remover";
            this.contextMenu_Remover.Size = new System.Drawing.Size(121, 22);
            this.contextMenu_Remover.Text = "Remover";
            // 
            // contextMenu_Archivo
            // 
            this.contextMenu_Archivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextMenu_Archivo_XML,
            this.contextMenu_Archivo_PDF});
            this.contextMenu_Archivo.Name = "contextMenu_Archivo";
            this.contextMenu_Archivo.Size = new System.Drawing.Size(121, 22);
            this.contextMenu_Archivo.Text = "Archivos";
            // 
            // contextMenu_Archivo_XML
            // 
            this.contextMenu_Archivo_XML.Name = "contextMenu_Archivo_XML";
            this.contextMenu_Archivo_XML.Size = new System.Drawing.Size(98, 22);
            this.contextMenu_Archivo_XML.Text = "XML";
            this.contextMenu_Archivo_XML.Click += new System.EventHandler(this.contextMenu_Archivo_XML_Click);
            // 
            // contextMenu_Archivo_PDF
            // 
            this.contextMenu_Archivo_PDF.Name = "contextMenu_Archivo_PDF";
            this.contextMenu_Archivo_PDF.Size = new System.Drawing.Size(98, 22);
            this.contextMenu_Archivo_PDF.Text = "PDF";
            this.contextMenu_Archivo_PDF.Click += new System.EventHandler(this.contextMenu_Archivo_PDF_Click);
            // 
            // StatusBar
            // 
            this.StatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.StatusBar.Location = new System.Drawing.Point(0, 545);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Size = new System.Drawing.Size(1032, 22);
            this.StatusBar.TabIndex = 2;
            this.StatusBar.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.Properties.Resources.refresh_16px;
            this.ToolBarButtonActualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Size = new System.Drawing.Size(79, 22);
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.ToolBarButtonActualizar_Click);
            // 
            // ToolBarButtonImprimir
            // 
            this.ToolBarButtonImprimir.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolBarButtonValidacion});
            this.ToolBarButtonImprimir.Image = global::Jaeger.UI.Properties.Resources.print_16px;
            this.ToolBarButtonImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonImprimir.Name = "ToolBarButtonImprimir";
            this.ToolBarButtonImprimir.Size = new System.Drawing.Size(82, 22);
            this.ToolBarButtonImprimir.Text = "Imprimir";
            // 
            // ToolBarButtonValidacion
            // 
            this.ToolBarButtonValidacion.Name = "ToolBarButtonValidacion";
            this.ToolBarButtonValidacion.Size = new System.Drawing.Size(128, 22);
            this.ToolBarButtonValidacion.Text = "Validación";
            this.ToolBarButtonValidacion.Click += new System.EventHandler(this.ToolBarButtonValidacion_Click);
            // 
            // ToolBarButtonHerramientas
            // 
            this.ToolBarButtonHerramientas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolBarButtonCrear});
            this.ToolBarButtonHerramientas.Image = global::Jaeger.UI.Properties.Resources.toolbox_16px;
            this.ToolBarButtonHerramientas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonHerramientas.Name = "ToolBarButtonHerramientas";
            this.ToolBarButtonHerramientas.Size = new System.Drawing.Size(107, 22);
            this.ToolBarButtonHerramientas.Text = "Herramientas";
            // 
            // ToolBarButtonCrear
            // 
            this.ToolBarButtonCrear.Name = "ToolBarButtonCrear";
            this.ToolBarButtonCrear.Size = new System.Drawing.Size(102, 22);
            this.ToolBarButtonCrear.Text = "Crear";
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.close_window_16px;
            this.ToolBarButtonCerrar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Size = new System.Drawing.Size(59, 22);
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // ComprobantesRepositorioLogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1032, 567);
            this.Controls.Add(this.dataGridComprobantes);
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.ToolBar);
            this.Name = "ComprobantesRepositorioLogForm";
            this.Text = "Repositorio LOG";
            this.Load += new System.EventHandler(this.ComprobanteLogRepositorioForm_Load);
            this.ToolBar.ResumeLayout(false);
            this.ToolBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridComprobantes)).EndInit();
            this.contextMenu.ResumeLayout(false);
            this.StatusBar.ResumeLayout(false);
            this.StatusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip ToolBar;
        private System.Windows.Forms.DataGridView dataGridComprobantes;
        private System.Windows.Forms.ToolStripButton ToolBarButtonActualizar;
        private System.Windows.Forms.ToolStripButton ToolBarButtonCerrar;
        private System.Windows.Forms.ToolStripDropDownButton ToolBarButtonImprimir;
        private System.Windows.Forms.ToolStripMenuItem ToolBarButtonValidacion;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem contextMenu_Verificar;
        private System.Windows.Forms.ToolStripMenuItem contextMenu_Remover;
        private System.Windows.Forms.ToolStripMenuItem contextMenu_Archivo;
        private System.Windows.Forms.ToolStripMenuItem contextMenu_Archivo_XML;
        private System.Windows.Forms.ToolStripMenuItem contextMenu_Archivo_PDF;
        private System.Windows.Forms.ToolStripDropDownButton ToolBarButtonImportar;
        private System.Windows.Forms.ToolStripMenuItem ToolBarButton_Importar_Archivo;
        private System.Windows.Forms.ToolStripMenuItem ToolBarButton_Importar_Carpeta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comprobado;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdComprobante;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDirectorio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Version;
        private System.Windows.Forms.DataGridViewTextBoxColumn MetaData;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoComprobante;
        private System.Windows.Forms.DataGridViewTextBoxColumn Folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Serie;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmisorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Emisor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Receptor;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaEmision;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaCertificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaValidacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoCertificado;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoCertificadoProvCertif;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFCProvCertif;
        private System.Windows.Forms.DataGridViewTextBoxColumn LugarExpedicion;
        private System.Windows.Forms.DataGridViewTextBoxColumn MetodoPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn FormaPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn UsoCFDI;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descuento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Registrado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Situacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Resultado;
        private System.Windows.Forms.ToolStripComboBox ToolBarComboStatus;
        private System.Windows.Forms.ToolStripTextBox ToolBarTextCantidad;
        private System.Windows.Forms.StatusStrip StatusBar;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripDropDownButton ToolBarButtonHerramientas;
        private System.Windows.Forms.ToolStripMenuItem ToolBarButtonCrear;
    }
}