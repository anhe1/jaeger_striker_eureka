﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.Domain.Aspel.Coi80.Entities;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Polizas {
    public partial class CatalogoCuentasBuscarForm : Form {
        protected internal List<CuentaContableModel> cuentas;
        public event EventHandler<CuentaContableModel> Seleted;

        public void OnSelected(CuentaContableModel e) {
            if (this.Seleted != null) {
                this.Seleted(this, e);
            }
        }

        public CatalogoCuentasBuscarForm() {
            InitializeComponent();
        }

        private void CatalogoCuentasBuscarForm_Load(object sender, EventArgs e) {
            this.toolBarStandarSearchControl1.Nuevo.Text = "Agregar";
            this.dataGridView1.DataGridCommon0();
            this.cuentas = new List<CuentaContableModel>() { 
                new CuentaContableModel { NUM_CTA = "1108-0001-0001-0000", NOMBRE = "I.V.A. acreditable al 16%" } ,
                new CuentaContableModel { NUM_CTA = "5101-0002-0017-0006", NOMBRE = "Boletos de avion" },
                new CuentaContableModel { NUM_CTA = "2001-0001-0001-0000", NOMBRE = "Varios" }
            };
            this.dataGridView1.DataSource = this.cuentas;
            this.toolBarStandarSearchControl1.Nuevo.Click += this.Nuevo_Click;
        }

        private void Nuevo_Click(object sender, EventArgs e) {
            if (this.dataGridView1.CurrentRow != null) {
                var _seleccionado = this.dataGridView1.CurrentRow.DataBoundItem as CuentaContableModel;
                if (_seleccionado != null) {
                    this.OnSelected(_seleccionado);
                    this.Close();
                }
            }
        }
    }
}
