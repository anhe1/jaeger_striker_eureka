﻿namespace Jaeger.UI.Forms.Polizas {
    partial class CatalogoCuentasBuscarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.toolBarStandarSearchControl1 = new Jaeger.UI.Common.Forms.ToolBarStandarSearchControl();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.NUM_CTA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOMBRE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // toolBarStandarSearchControl1
            // 
            this.toolBarStandarSearchControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBarStandarSearchControl1.Etiqueta = "Titulo";
            this.toolBarStandarSearchControl1.Location = new System.Drawing.Point(0, 0);
            this.toolBarStandarSearchControl1.Name = "toolBarStandarSearchControl1";
            this.toolBarStandarSearchControl1.ShowActualizar = false;
            this.toolBarStandarSearchControl1.ShowCerrar = false;
            this.toolBarStandarSearchControl1.ShowEditar = false;
            this.toolBarStandarSearchControl1.ShowGuardar = false;
            this.toolBarStandarSearchControl1.ShowHerramientas = false;
            this.toolBarStandarSearchControl1.ShowImprimir = false;
            this.toolBarStandarSearchControl1.ShowNuevo = true;
            this.toolBarStandarSearchControl1.ShowRemover = false;
            this.toolBarStandarSearchControl1.Size = new System.Drawing.Size(360, 25);
            this.toolBarStandarSearchControl1.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NUM_CTA,
            this.NOMBRE});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(360, 313);
            this.dataGridView1.TabIndex = 1;
            // 
            // NUM_CTA
            // 
            this.NUM_CTA.DataPropertyName = "NUM_CTA";
            this.NUM_CTA.HeaderText = "No. Cuenta";
            this.NUM_CTA.Name = "NUM_CTA";
            // 
            // NOMBRE
            // 
            this.NOMBRE.DataPropertyName = "NOMBRE";
            this.NOMBRE.HeaderText = "Descripción";
            this.NOMBRE.Name = "NOMBRE";
            this.NOMBRE.Width = 200;
            // 
            // CatalogoCuentasBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(360, 338);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.toolBarStandarSearchControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CatalogoCuentasBuscarForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cuenta";
            this.Load += new System.EventHandler(this.CatalogoCuentasBuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarSearchControl toolBarStandarSearchControl1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUM_CTA;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOMBRE;
    }
}