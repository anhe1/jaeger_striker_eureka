﻿namespace Jaeger.UI.Forms.Polizas {
    partial class PolizaModeloForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TPoliza = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Concepto = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Fecha = new System.Windows.Forms.DateTimePicker();
            this.Numero = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Tipo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.gridPartidas = new System.Windows.Forms.DataGridView();
            this.NoCuenta = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Concepto1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoCambio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Debe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Haber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPartidas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // TPoliza
            // 
            this.TPoliza.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPoliza.Etiqueta = "Titulo";
            this.TPoliza.Location = new System.Drawing.Point(0, 0);
            this.TPoliza.Name = "TPoliza";
            this.TPoliza.ShowActualizar = true;
            this.TPoliza.ShowCerrar = true;
            this.TPoliza.ShowEditar = true;
            this.TPoliza.ShowGuardar = true;
            this.TPoliza.ShowHerramientas = true;
            this.TPoliza.ShowImprimir = true;
            this.TPoliza.ShowNuevo = true;
            this.TPoliza.ShowRemover = true;
            this.TPoliza.Size = new System.Drawing.Size(753, 25);
            this.TPoliza.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Concepto);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.Fecha);
            this.panel1.Controls.Add(this.Numero);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.Tipo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(753, 83);
            this.panel1.TabIndex = 1;
            // 
            // Concepto
            // 
            this.Concepto.Location = new System.Drawing.Point(80, 33);
            this.Concepto.MaxLength = 300;
            this.Concepto.Multiline = true;
            this.Concepto.Name = "Concepto";
            this.Concepto.Size = new System.Drawing.Size(661, 40);
            this.Concepto.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Concepto:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(495, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Fecha:";
            // 
            // Fecha
            // 
            this.Fecha.Location = new System.Drawing.Point(541, 6);
            this.Fecha.Name = "Fecha";
            this.Fecha.Size = new System.Drawing.Size(200, 20);
            this.Fecha.TabIndex = 4;
            // 
            // Numero
            // 
            this.Numero.Location = new System.Drawing.Point(225, 6);
            this.Numero.Name = "Numero";
            this.Numero.Size = new System.Drawing.Size(100, 20);
            this.Numero.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(172, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Número:";
            // 
            // Tipo
            // 
            this.Tipo.FormattingEnabled = true;
            this.Tipo.Location = new System.Drawing.Point(55, 6);
            this.Tipo.Name = "Tipo";
            this.Tipo.Size = new System.Drawing.Size(98, 21);
            this.Tipo.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tipo:";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 108);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gridPartidas);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridView1);
            this.splitContainer1.Size = new System.Drawing.Size(753, 342);
            this.splitContainer1.SplitterDistance = 175;
            this.splitContainer1.TabIndex = 2;
            // 
            // gridPartidas
            // 
            this.gridPartidas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPartidas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NoCuenta,
            this.Concepto1,
            this.TipoCambio,
            this.Debe,
            this.Haber});
            this.gridPartidas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPartidas.Location = new System.Drawing.Point(0, 0);
            this.gridPartidas.Name = "gridPartidas";
            this.gridPartidas.Size = new System.Drawing.Size(753, 175);
            this.gridPartidas.TabIndex = 0;
            this.gridPartidas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridPartidas_CellContentClick);
            // 
            // NoCuenta
            // 
            this.NoCuenta.DataPropertyName = "NUM_CTA";
            this.NoCuenta.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.NoCuenta.HeaderText = "Núm. Cuenta";
            this.NoCuenta.Name = "NoCuenta";
            // 
            // Concepto1
            // 
            this.Concepto1.DataPropertyName = "CONCEP_PO";
            this.Concepto1.HeaderText = "Concepto";
            this.Concepto1.Name = "Concepto1";
            this.Concepto1.Width = 220;
            // 
            // TipoCambio
            // 
            this.TipoCambio.HeaderText = "T. Cambio";
            this.TipoCambio.Name = "TipoCambio";
            // 
            // Debe
            // 
            this.Debe.HeaderText = "Debe";
            this.Debe.Name = "Debe";
            // 
            // Haber
            // 
            this.Haber.HeaderText = "Haber";
            this.Haber.Name = "Haber";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(753, 163);
            this.dataGridView1.TabIndex = 0;
            // 
            // PolizaModeloForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 450);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.TPoliza);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PolizaModeloForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Poliza-Modelo";
            this.Load += new System.EventHandler(this.PolizaModeloForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPartidas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarControl TPoliza;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox Concepto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker Fecha;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox Tipo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView gridPartidas;
        private System.Windows.Forms.TextBox Numero;
        private System.Windows.Forms.DataGridViewButtonColumn NoCuenta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Concepto1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoCambio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Debe;
        private System.Windows.Forms.DataGridViewTextBoxColumn Haber;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}