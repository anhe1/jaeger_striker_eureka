﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.Domain.Aspel.Coi80.Entities;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Polizas {
    public partial class PolizaModeloForm : Form {
        protected internal PolizaDetailModel poliza;
        

        public PolizaModeloForm() {
            InitializeComponent();
        }

        private void PolizaModeloForm_Load(object sender, EventArgs e) {
            this.gridPartidas.DataGridCommon_cplite();
            this.poliza = new PolizaDetailModel();
            this.CreateBinding();
            this.TPoliza.Nuevo.Click += this.Nuevo_Click;
        }

        private void Nuevo_Click(object sender, EventArgs e) {
            var _cuenta = new CatalogoCuentasBuscarForm();
            _cuenta.Seleted += this._cuenta_Seleted;
            _cuenta.ShowDialog(this);
        }

        private void CreateBinding() {
            this.Numero.DataBindings.Clear();
            this.Numero.DataBindings.Add("Text", this.poliza, "NUM_POLIZ", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Fecha.DataBindings.Clear();
            this.Fecha.DataBindings.Add("Value", this.poliza, "FECHA_POL", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Concepto.DataBindings.Clear();
            this.Concepto.DataBindings.Add("Text", this.poliza, "CONCEP_PO", true, DataSourceUpdateMode.OnPropertyChanged);

            this.gridPartidas.DataSource = this.poliza.Auxiliares;
        }

        private void gridPartidas_CellContentClick(object sender, DataGridViewCellEventArgs e) {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0) {
                var _cuenta = new CatalogoCuentasBuscarForm();
                _cuenta.Seleted += this._cuenta_Seleted;
                _cuenta.ShowDialog(this);
            }
        }

        private void _cuenta_Seleted(object sender, CuentaContableModel e) {
            if (e != null) {
                this.poliza.Auxiliares.Add(new AuxiliarDetailModel { NUM_CTA = e.NUM_CTA, CONCEP_PO = e.NOMBRE });
            }
        }
    }
}
