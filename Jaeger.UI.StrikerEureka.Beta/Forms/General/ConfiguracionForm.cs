﻿using System;
using System.Windows.Forms;

namespace Jaeger.UI.Forms.General {
    public partial class ConfiguracionForm : Form {
        public ConfiguracionForm() {
            InitializeComponent();
        }

        private void ConfiguracionForm_Load(object sender, EventArgs e) {
            this.propertyGrid1.SelectedObject = Jaeger.Aplication.Base.ConfigService.Synapsis;
        }

        private void BCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
