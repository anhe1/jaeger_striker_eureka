﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.ReciboFiscal.Entities;
using Jaeger.Domain.Contable.Entities;
using Jaeger.Domain.Contable.ValueObjects;

namespace Jaeger.UI.Forms.ReciboFiscal {
    public partial class PrePolizaAForm : Form {
        private BindingList<ReciboFiscalModel> recibosFiscales;
        private PrePolizaDetailModel prePoliza = new PrePolizaDetailModel();
        private PrePolizaConf prePolizaConf = new PrePolizaConf();
        public PrePolizaAForm() {
            InitializeComponent();
        }

        public PrePolizaAForm(BindingList<ReciboFiscalModel> recibosFiscales) {
            InitializeComponent();
            this.recibosFiscales = recibosFiscales;
        }

        private void PrePolizaAForm_Load(object sender, EventArgs e) {
            this.TPrePoliza.Nuevo.Text = "Abrir";
            this.TPrePoliza.Editar.Text = "Procesar";
            this.gridCuentas.DataGridCommon();
            this.gridComprobantes.DataGridCommon();
            this.TipoPoliza.DataSource = Enum.GetValues(typeof(TipoPrePolizaAEnum));
            this.TPrePoliza.Nuevo.Click += this.TPrePoliza_Nuevo_Click;
            this.TPrePoliza.Editar.Click += this.TPrePoliza_Editar_Click;
            this.TPrePoliza.Guardar.Click += this.TPrePoliza_Guardar_Click;
            this.TPrePoliza.Cerrar.Click += this.TPrePoliza_Cerrar_Click;

            this.TCuenta.Remover.Click += this.TCuenta_Remover_Click;
            this.TComprobante.Remover.Click += this.TComprobante_Remover_Click;
        }

        private void TComprobante_Remover_Click(object sender, EventArgs e) {
            if (this.gridComprobantes.CurrentRow != null) {
                if (MessageBox.Show(this, "¡Esta seguro de remover el objeto seleccionado?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                    this.gridComprobantes.Rows.Remove(this.gridComprobantes.CurrentRow);
                }
            }
        }

        private void TCuenta_Remover_Click(object sender, EventArgs e) {
            if (this.gridCuentas.CurrentRow != null) {
                if (MessageBox.Show(this, "¡Esta seguro de remover el objeto seleccionado?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                    this.gridCuentas.Rows.Remove(this.gridCuentas.CurrentRow);
                }
            }
        }

        private void TPrePoliza_Editar_Click(object sender, EventArgs e) {
            foreach (var item in this.prePolizaConf.Auxiliar) {
                var d = this.Obtener(item);
                foreach (var item1 in d) {
                    this.prePoliza.Auxiliar.Add(item1);
                }
            }
        }

        private void TPrePoliza_Nuevo_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog { Title = "Abrir Pre Póliza", Filter = "*.json|*.JSON" };
            if (openFile.ShowDialog(this) == DialogResult.OK) {
                var contenido = Util.Services.FileService.ReadFileText(openFile.FileName);
                try {
                    this.prePolizaConf = PrePolizaConf.FromJson(contenido);
                    this.prePoliza = new PrePolizaDetailModel();
                    this.prePoliza.Concepto = this.prePolizaConf.Concepto;
                    this.CreateBinding();
                } catch (Exception) {

                }
            }
        }

        private void TPrePoliza_Guardar_Click(object sender, EventArgs e) {
            var saveFile = new SaveFileDialog { Title = "Guardar", Filter = "*.csv|*.CSV" };
            if (saveFile.ShowDialog(this) == DialogResult.OK) {
                Jaeger.Util.Services.FileService.WriteFileText(saveFile.FileName, this.prePoliza.Exportar());
            }
        }

        private void TPrePoliza_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CreateBinding() {
            this.TipoPoliza.DataBindings.Clear();
            this.TipoPoliza.DataBindings.Add("Text", this.prePoliza, "Tipo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.NoPoliza.DataBindings.Clear();
            this.NoPoliza.DataBindings.Add("Text", this.prePoliza, "NumPoliza", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Fecha.DataBindings.Clear();
            this.Fecha.DataBindings.Add("Value", this.prePoliza, "Fecha", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Concepto.DataBindings.Clear();
            this.Concepto.DataBindings.Add("Text", this.prePoliza, "Concepto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.gridCuentas.DataSource = this.prePoliza.Auxiliar;
            this.gridComprobantes.DataSource = this.prePoliza.Auxiliar;
            this.gridComprobantes.DataMember = "Comprobantes";
        }

        private BindingList<PrePolizaAuxiliarDetailModel> Obtener(PrePolizaAuxiliarConf conf) {
            if (conf.IsDetail) {
                return this.Test1(conf);
            } else {
                return new BindingList<PrePolizaAuxiliarDetailModel> { this.Test(conf) };
            }
        }
		
		private PrePolizaAuxiliarDetailModel Test(PrePolizaAuxiliarConf conf) {
			var importe = new decimal(0);
			var response = new PrePolizaAuxiliarDetailModel();
			switch (conf.Propiedad.ToLower()) {
				case "total":
					importe = this.recibosFiscales.Sum(it => it.Total);
					break;
				case "subtotal":
					importe = this.recibosFiscales.Sum(it => it.SubTotal);
					break;
				case "iva":
					importe = this.recibosFiscales.Sum(it => it.Iva);
					break;
				default:
					importe = 0;
					break;
			}
			
			
			response.NumCuenta = conf.NumCuenta;
            response.Concepto = conf.Concepto;
            //response.NumPartida = numero;
            response.DEBE_HABER = conf.IsCargo ? "1" : "0";
            response.Monto = importe;
			response.Comprobantes = this.GetComprobantes();
			return response;
		}
		
		private BindingList<PrePolizaAuxiliarDetailModel> Test1(PrePolizaAuxiliarConf conf) {
			var importe = new decimal(0);
			var _response = new BindingList<PrePolizaAuxiliarDetailModel>();
			foreach (var item in this.recibosFiscales) {
				var response = new PrePolizaAuxiliarDetailModel();
				switch (conf.Propiedad.ToLower()) {
					case "total":
						response.Monto = item.Total;
						break;
					case "subtotal":
						response.Monto = item.SubTotal;
						break;
					case "iva":
						response.Monto = item.Iva;
						break;
					default:
						importe = 0;
						break;
				}
			
				response.NumCuenta = conf.NumCuenta;
				response.Concepto = conf.Concepto;
                //response.NumPartida = numero;
                response.DEBE_HABER = conf.IsCargo ? "1" : "0";
				response.Comprobantes.Add(ParseP(item));
				_response.Add(response);
			}
			return _response;
		}

        private BindingList<PrePolizaComprobanteModel> GetComprobantes() {
            var _response = new BindingList<PrePolizaComprobanteModel>();
            foreach (var recibo in this.recibosFiscales) {
                _response.Add(ParseP(recibo));
            }
            return _response;
        } 
		
		private PrePolizaComprobanteModel ParseP(ReciboFiscalModel recibo) {
			return new PrePolizaComprobanteModel {
                    EmisorRFC = recibo.EmisorRFC,
                    ReceptorRFC = recibo.ReceptorRfc,
                    Fecha = recibo.FechaEmision.Value,
                    IdDocumento = recibo.IdDocumento,
                    Monto = recibo.Total,
                    Serie = recibo.Serie,
					Folio = recibo.Folio
			};
		}

        private void Ejemplo() {
            this.prePolizaConf.Concepto = "De muestra";
            this.prePolizaConf.Fecha = DateTime.Now;
            this.prePolizaConf.Auxiliar = new BindingList<PrePolizaAuxiliarConf> {
                new PrePolizaAuxiliarConf { NumCuenta = "1104-0002-0001-0003", Propiedad = "Total", Concepto = "RFISC-{0} A {1} SIST. VARIOS ({2})", IsDetail = false, IsCargo = true  },
                new PrePolizaAuxiliarConf { NumCuenta = "4101-0001-0001-0000", Propiedad = "SubTotal", Concepto = "RFISC-{0} A {1} SIST. VARIOS ({2})", IsDetail = true, IsCargo = false },
                new PrePolizaAuxiliarConf { NumCuenta = "2104-0001-0001-0000", Propiedad = "Iva", Concepto = "RFISC-{0} A {1} SIST. VARIOS ({2})", IsDetail = false, IsCargo = false }
                };

            this.prePoliza.Concepto = this.prePolizaConf.Concepto;
            this.prePoliza.NumPoliza = "";
            this.prePoliza.Tipo = "";

            foreach (var item in this.prePolizaConf.Auxiliar) {
                var d = this.Obtener(item);
                foreach (var item1 in d) {
                    this.prePoliza.Auxiliar.Add(item1);
                }
            }


            Console.WriteLine(this.prePoliza.Exportar());
        }
    }
}
