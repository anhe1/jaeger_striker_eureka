﻿namespace Jaeger.UI.Forms.ReciboFiscal {
    partial class PrePolizaAConfForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Concepto = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Fecha = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.NoPoliza = new System.Windows.Forms.TextBox();
            this.TipoPoliza = new System.Windows.Forms.ComboBox();
            this.TPrePoliza = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.grdCuentas = new System.Windows.Forms.DataGridView();
            this.Cuenta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsDetail = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.IsCargo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.AsociaCFDI = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PropiedadD = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TCuenta = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCuentas)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.Concepto);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.Fecha);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.NoPoliza);
            this.groupBox1.Controls.Add(this.TipoPoliza);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(614, 105);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tipo Póliza:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(166, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Número:";
            // 
            // Concepto
            // 
            this.Concepto.Location = new System.Drawing.Point(78, 46);
            this.Concepto.MaxLength = 255;
            this.Concepto.Multiline = true;
            this.Concepto.Name = "Concepto";
            this.Concepto.Size = new System.Drawing.Size(522, 51);
            this.Concepto.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(339, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Fecha:";
            // 
            // Fecha
            // 
            this.Fecha.Location = new System.Drawing.Point(385, 19);
            this.Fecha.Name = "Fecha";
            this.Fecha.Size = new System.Drawing.Size(215, 20);
            this.Fecha.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Concepto:";
            // 
            // NoPoliza
            // 
            this.NoPoliza.Location = new System.Drawing.Point(219, 19);
            this.NoPoliza.Name = "NoPoliza";
            this.NoPoliza.Size = new System.Drawing.Size(100, 20);
            this.NoPoliza.TabIndex = 7;
            // 
            // TipoPoliza
            // 
            this.TipoPoliza.FormattingEnabled = true;
            this.TipoPoliza.Location = new System.Drawing.Point(84, 19);
            this.TipoPoliza.Name = "TipoPoliza";
            this.TipoPoliza.Size = new System.Drawing.Size(76, 21);
            this.TipoPoliza.TabIndex = 6;
            // 
            // TPrePoliza
            // 
            this.TPrePoliza.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPrePoliza.Etiqueta = "Titulo";
            this.TPrePoliza.Location = new System.Drawing.Point(0, 0);
            this.TPrePoliza.Name = "TPrePoliza";
            this.TPrePoliza.ShowActualizar = false;
            this.TPrePoliza.ShowCerrar = true;
            this.TPrePoliza.ShowEditar = false;
            this.TPrePoliza.ShowGuardar = true;
            this.TPrePoliza.ShowHerramientas = true;
            this.TPrePoliza.ShowImprimir = false;
            this.TPrePoliza.ShowNuevo = true;
            this.TPrePoliza.ShowRemover = false;
            this.TPrePoliza.Size = new System.Drawing.Size(614, 25);
            this.TPrePoliza.TabIndex = 14;
            // 
            // grdCuentas
            // 
            this.grdCuentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCuentas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Cuenta,
            this.IsDetail,
            this.IsCargo,
            this.AsociaCFDI,
            this.PropiedadD});
            this.grdCuentas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCuentas.Location = new System.Drawing.Point(0, 155);
            this.grdCuentas.Name = "grdCuentas";
            this.grdCuentas.Size = new System.Drawing.Size(614, 295);
            this.grdCuentas.TabIndex = 12;
            // 
            // Cuenta
            // 
            this.Cuenta.DataPropertyName = "NumCuenta";
            this.Cuenta.HeaderText = "No. Cuenta";
            this.Cuenta.Name = "Cuenta";
            this.Cuenta.Width = 150;
            // 
            // IsDetail
            // 
            this.IsDetail.DataPropertyName = "IsDetail";
            this.IsDetail.HeaderText = "IsDetail";
            this.IsDetail.Name = "IsDetail";
            // 
            // IsCargo
            // 
            this.IsCargo.DataPropertyName = "IsCargo";
            this.IsCargo.HeaderText = "IsCargo";
            this.IsCargo.Name = "IsCargo";
            // 
            // AsociaCFDI
            // 
            this.AsociaCFDI.DataPropertyName = "AsociaCFDI";
            this.AsociaCFDI.HeaderText = "AsociaCFDI";
            this.AsociaCFDI.Name = "AsociaCFDI";
            // 
            // PropiedadD
            // 
            this.PropiedadD.DataPropertyName = "Propiedad";
            this.PropiedadD.HeaderText = "Valor";
            this.PropiedadD.Name = "PropiedadD";
            // 
            // TCuenta
            // 
            this.TCuenta.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCuenta.Etiqueta = "Cuentas";
            this.TCuenta.Location = new System.Drawing.Point(0, 130);
            this.TCuenta.Name = "TCuenta";
            this.TCuenta.ShowActualizar = false;
            this.TCuenta.ShowCerrar = false;
            this.TCuenta.ShowEditar = false;
            this.TCuenta.ShowGuardar = false;
            this.TCuenta.ShowHerramientas = false;
            this.TCuenta.ShowImprimir = false;
            this.TCuenta.ShowNuevo = true;
            this.TCuenta.ShowRemover = true;
            this.TCuenta.Size = new System.Drawing.Size(614, 25);
            this.TCuenta.TabIndex = 16;
            // 
            // PrePolizaAConfForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 450);
            this.Controls.Add(this.grdCuentas);
            this.Controls.Add(this.TCuenta);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TPrePoliza);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PrePolizaAConfForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "PrePolizaAConfForm";
            this.Load += new System.EventHandler(this.PrePolizaAConfForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCuentas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Concepto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker Fecha;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox NoPoliza;
        private System.Windows.Forms.ComboBox TipoPoliza;
        private Common.Forms.ToolBarStandarControl TPrePoliza;
        private System.Windows.Forms.DataGridView grdCuentas;
        private Common.Forms.ToolBarStandarControl TCuenta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cuenta;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsDetail;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsCargo;
        private System.Windows.Forms.DataGridViewCheckBoxColumn AsociaCFDI;
        private System.Windows.Forms.DataGridViewComboBoxColumn PropiedadD;
    }
}