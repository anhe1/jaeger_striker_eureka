﻿namespace Jaeger.UI.Forms.ReciboFiscal {
    partial class PrePolizaAForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gridCuentas = new System.Windows.Forms.DataGridView();
            this.Cuenta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConceptoMovimiento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Debe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Haber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridComprobantes = new System.Windows.Forms.DataGridView();
            this.EmisorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TipoPoliza = new System.Windows.Forms.ComboBox();
            this.NoPoliza = new System.Windows.Forms.TextBox();
            this.Fecha = new System.Windows.Forms.DateTimePicker();
            this.Concepto = new System.Windows.Forms.TextBox();
            this.TPrePoliza = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.TCuenta = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TComprobante = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridCuentas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComprobantes)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridCuentas
            // 
            this.gridCuentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridCuentas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Cuenta,
            this.ConceptoMovimiento,
            this.Debe,
            this.Haber});
            this.gridCuentas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCuentas.Location = new System.Drawing.Point(0, 25);
            this.gridCuentas.Name = "gridCuentas";
            this.gridCuentas.Size = new System.Drawing.Size(701, 121);
            this.gridCuentas.TabIndex = 0;
            // 
            // Cuenta
            // 
            this.Cuenta.DataPropertyName = "NumCuenta";
            this.Cuenta.HeaderText = "No. Cuenta";
            this.Cuenta.Name = "Cuenta";
            this.Cuenta.Width = 150;
            // 
            // ConceptoMovimiento
            // 
            this.ConceptoMovimiento.DataPropertyName = "Concepto";
            this.ConceptoMovimiento.HeaderText = "Concepto";
            this.ConceptoMovimiento.Name = "ConceptoMovimiento";
            this.ConceptoMovimiento.Width = 300;
            // 
            // Debe
            // 
            this.Debe.DataPropertyName = "Debe";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.Debe.DefaultCellStyle = dataGridViewCellStyle1;
            this.Debe.HeaderText = "Debe";
            this.Debe.Name = "Debe";
            // 
            // Haber
            // 
            this.Haber.DataPropertyName = "Haber";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            this.Haber.DefaultCellStyle = dataGridViewCellStyle2;
            this.Haber.HeaderText = "Haber";
            this.Haber.Name = "Haber";
            // 
            // gridComprobantes
            // 
            this.gridComprobantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridComprobantes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EmisorRFC,
            this.ReceptorRFC,
            this.Fecha1,
            this.IdDocumento,
            this.Total});
            this.gridComprobantes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridComprobantes.Location = new System.Drawing.Point(0, 25);
            this.gridComprobantes.Name = "gridComprobantes";
            this.gridComprobantes.Size = new System.Drawing.Size(701, 116);
            this.gridComprobantes.TabIndex = 1;
            // 
            // EmisorRFC
            // 
            this.EmisorRFC.DataPropertyName = "EmisorRFC";
            this.EmisorRFC.HeaderText = "Emisor RFC";
            this.EmisorRFC.Name = "EmisorRFC";
            this.EmisorRFC.Width = 120;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.DataPropertyName = "ReceptorRFC";
            this.ReceptorRFC.HeaderText = "Receptor RFC";
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.Width = 120;
            // 
            // Fecha1
            // 
            this.Fecha1.DataPropertyName = "Fecha";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "d";
            dataGridViewCellStyle3.NullValue = null;
            this.Fecha1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Fecha1.HeaderText = "Fecha";
            this.Fecha1.Name = "Fecha1";
            // 
            // IdDocumento
            // 
            this.IdDocumento.DataPropertyName = "IdDocumento";
            this.IdDocumento.HeaderText = "UUID";
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.Width = 200;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Monto";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            this.Total.DefaultCellStyle = dataGridViewCellStyle4;
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tipo Póliza:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(166, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Número:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(339, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Fecha:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Concepto:";
            // 
            // TipoPoliza
            // 
            this.TipoPoliza.FormattingEnabled = true;
            this.TipoPoliza.Location = new System.Drawing.Point(84, 19);
            this.TipoPoliza.Name = "TipoPoliza";
            this.TipoPoliza.Size = new System.Drawing.Size(76, 21);
            this.TipoPoliza.TabIndex = 6;
            // 
            // NoPoliza
            // 
            this.NoPoliza.Location = new System.Drawing.Point(219, 19);
            this.NoPoliza.Name = "NoPoliza";
            this.NoPoliza.Size = new System.Drawing.Size(100, 20);
            this.NoPoliza.TabIndex = 7;
            // 
            // Fecha
            // 
            this.Fecha.Location = new System.Drawing.Point(385, 19);
            this.Fecha.Name = "Fecha";
            this.Fecha.Size = new System.Drawing.Size(215, 20);
            this.Fecha.TabIndex = 8;
            // 
            // Concepto
            // 
            this.Concepto.Location = new System.Drawing.Point(78, 46);
            this.Concepto.MaxLength = 255;
            this.Concepto.Multiline = true;
            this.Concepto.Name = "Concepto";
            this.Concepto.Size = new System.Drawing.Size(522, 51);
            this.Concepto.TabIndex = 9;
            // 
            // TPrePoliza
            // 
            this.TPrePoliza.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPrePoliza.Etiqueta = "Titulo";
            this.TPrePoliza.Location = new System.Drawing.Point(0, 0);
            this.TPrePoliza.Name = "TPrePoliza";
            this.TPrePoliza.ShowActualizar = false;
            this.TPrePoliza.ShowCerrar = true;
            this.TPrePoliza.ShowEditar = true;
            this.TPrePoliza.ShowGuardar = true;
            this.TPrePoliza.ShowHerramientas = true;
            this.TPrePoliza.ShowImprimir = false;
            this.TPrePoliza.ShowNuevo = true;
            this.TPrePoliza.ShowRemover = false;
            this.TPrePoliza.Size = new System.Drawing.Size(701, 25);
            this.TPrePoliza.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.Concepto);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.Fecha);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.NoPoliza);
            this.groupBox1.Controls.Add(this.TipoPoliza);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(701, 105);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 130);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gridCuentas);
            this.splitContainer1.Panel1.Controls.Add(this.TCuenta);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gridComprobantes);
            this.splitContainer1.Panel2.Controls.Add(this.TComprobante);
            this.splitContainer1.Size = new System.Drawing.Size(701, 291);
            this.splitContainer1.SplitterDistance = 146;
            this.splitContainer1.TabIndex = 12;
            // 
            // TCuenta
            // 
            this.TCuenta.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCuenta.Etiqueta = "Cuentas";
            this.TCuenta.Location = new System.Drawing.Point(0, 0);
            this.TCuenta.Name = "TCuenta";
            this.TCuenta.ShowActualizar = false;
            this.TCuenta.ShowCerrar = false;
            this.TCuenta.ShowEditar = false;
            this.TCuenta.ShowGuardar = false;
            this.TCuenta.ShowHerramientas = false;
            this.TCuenta.ShowImprimir = false;
            this.TCuenta.ShowNuevo = true;
            this.TCuenta.ShowRemover = true;
            this.TCuenta.Size = new System.Drawing.Size(701, 25);
            this.TCuenta.TabIndex = 11;
            // 
            // TComprobante
            // 
            this.TComprobante.Dock = System.Windows.Forms.DockStyle.Top;
            this.TComprobante.Etiqueta = "Comprobantes";
            this.TComprobante.Location = new System.Drawing.Point(0, 0);
            this.TComprobante.Name = "TComprobante";
            this.TComprobante.ShowActualizar = false;
            this.TComprobante.ShowCerrar = false;
            this.TComprobante.ShowEditar = false;
            this.TComprobante.ShowGuardar = false;
            this.TComprobante.ShowHerramientas = false;
            this.TComprobante.ShowImprimir = false;
            this.TComprobante.ShowNuevo = true;
            this.TComprobante.ShowRemover = true;
            this.TComprobante.Size = new System.Drawing.Size(701, 25);
            this.TComprobante.TabIndex = 11;
            // 
            // PrePolizaAForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 421);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TPrePoliza);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PrePolizaAForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "PrePolizaAForm";
            this.Load += new System.EventHandler(this.PrePolizaAForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridCuentas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComprobantes)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridCuentas;
        private System.Windows.Forms.DataGridView gridComprobantes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox TipoPoliza;
        private System.Windows.Forms.TextBox NoPoliza;
        private System.Windows.Forms.DateTimePicker Fecha;
        private System.Windows.Forms.TextBox Concepto;
        private Common.Forms.ToolBarStandarControl TPrePoliza;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmisorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha1;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cuenta;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConceptoMovimiento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Debe;
        private System.Windows.Forms.DataGridViewTextBoxColumn Haber;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Common.Forms.ToolBarStandarControl TCuenta;
        private Common.Forms.ToolBarStandarControl TComprobante;
    }
}