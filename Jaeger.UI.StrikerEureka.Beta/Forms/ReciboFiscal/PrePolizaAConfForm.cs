﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.Domain.Contable.Entities;
using Jaeger.Domain.Contable.ValueObjects;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.ReciboFiscal {
    public partial class PrePolizaAConfForm : Form {
        private PrePolizaConf prePoliza = new PrePolizaConf();
        private List<Propiedad> properties = new List<Propiedad>() { new Propiedad("SubTotal"), new Propiedad("IVA"), new Propiedad("Total") };

        public PrePolizaAConfForm() {
            InitializeComponent();
        }

        private void PrePolizaAConfForm_Load(object sender, EventArgs e) {
            this.grdCuentas.DataGridCommon();
            this.grdCuentas.ReadOnly = false;
            this.TipoPoliza.DataSource = Enum.GetValues(typeof(TipoPrePolizaAEnum));
            this.PropiedadD.DataSource = this.properties;
            this.PropiedadD.ValueMember = "Name";
            this.PropiedadD.DisplayMember = "Name";

            this.CreateBinding();
            this.TPrePoliza.Nuevo.Text = "Abrir";
            this.TPrePoliza.Nuevo.Click += this.TPrePoliza_Nuevo_Click;
            this.TPrePoliza.Guardar.Click += this.TPrePoliza_Guardar_Click;
            this.TPrePoliza.Cerrar.Click += this.TPrePoliza_Cerrar_Click;
            this.TCuenta.Nuevo.Click += this.TCuenta_Nuevo_Click;
            this.TCuenta.Remover.Click += this.TCuenta_Remover_Click;
        }

        private void TPrePoliza_Nuevo_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog { Title = "Abrir Pre Póliza", Filter = "*.json|*.JSON" };
            if (openFile.ShowDialog(this) == DialogResult.OK) {
                var contenido = Util.Services.FileService.ReadFileText(openFile.FileName);
                try {
                    this.prePoliza = PrePolizaConf.FromJson(contenido);
                    this.CreateBinding();
                } catch (Exception) {

                }
            }
        }

        private void TPrePoliza_Guardar_Click(object sender, EventArgs e) {
            var saveFile = new SaveFileDialog { Title = "Guardar", Filter = "*.json|*.JSON" };
            if (saveFile.ShowDialog(this) == DialogResult.OK) {
                Jaeger.Util.Services.FileService.WriteFileText(saveFile.FileName, this.prePoliza.ToJson());
            }
        }

        private void TPrePoliza_Cerrar_Click(object sender, EventArgs eventArgs) {
            this.Close();
        }

        private void TCuenta_Nuevo_Click(object sender, EventArgs e) {
            this.prePoliza.Auxiliar.Add(new PrePolizaAuxiliarConf());
        }

        private void TCuenta_Remover_Click(object sender, EventArgs e) {
            if (this.grdCuentas.CurrentRow != null) {
                this.grdCuentas.Rows.Remove(this.grdCuentas.CurrentRow);
            }
        }

        private void CreateBinding() {
            this.TipoPoliza.DataBindings.Clear();
            this.TipoPoliza.DataBindings.Add("Text", this.prePoliza, "Tipo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.NoPoliza.DataBindings.Clear();
            this.NoPoliza.DataBindings.Add("Text", this.prePoliza, "NumPoliza", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Fecha.DataBindings.Clear();
            this.Fecha.DataBindings.Add("Value", this.prePoliza, "Fecha", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Concepto.DataBindings.Clear();
            this.Concepto.DataBindings.Add("Text", this.prePoliza, "Concepto", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.grdCuentas.DataSource = this.prePoliza.Auxiliar;
        }
        
        private class Propiedad {
        public Propiedad() {
        }

        public Propiedad(string name) {
                this.Name = name;
            }

            public string Name { get; set; }
        }
    }

}
