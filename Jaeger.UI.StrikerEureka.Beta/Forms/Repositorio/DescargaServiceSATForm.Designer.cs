﻿namespace Jaeger.UI.Forms.Repositorio {
    partial class DescargaServiceSATForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TDescarga = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.Estado = new System.Windows.Forms.StatusStrip();
            this.GridSolicitudes = new System.Windows.Forms.DataGridView();
            this.IdTipo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.IdTipoSolicitud = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.GridSolicitudes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // TDescarga
            // 
            this.TDescarga.Dock = System.Windows.Forms.DockStyle.Top;
            this.TDescarga.Etiqueta = "Solicitudes de Descarga:";
            this.TDescarga.Location = new System.Drawing.Point(0, 38);
            this.TDescarga.Name = "TDescarga";
            this.TDescarga.ShowActualizar = true;
            this.TDescarga.ShowCerrar = true;
            this.TDescarga.ShowEditar = true;
            this.TDescarga.ShowGuardar = false;
            this.TDescarga.ShowHerramientas = false;
            this.TDescarga.ShowImprimir = false;
            this.TDescarga.ShowNuevo = true;
            this.TDescarga.ShowRemover = true;
            this.TDescarga.Size = new System.Drawing.Size(846, 25);
            this.TDescarga.TabIndex = 0;
            this.TDescarga.ButtonNuevo_Click += new System.EventHandler<System.EventArgs>(this.TDescarga_Nuevo_Click);
            this.TDescarga.ButtonEditar_Click += new System.EventHandler<System.EventArgs>(this.TDescarga_Verificar_Click);
            this.TDescarga.ButtonRemover_Click += new System.EventHandler<System.EventArgs>(this.TDescarga_Descargar_Click);
            this.TDescarga.ButtonActualizar_Click += new System.EventHandler<System.EventArgs>(this.TDescarga_Actualizar_Click);
            this.TDescarga.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TDescarga_Cerrar_Click);
            // 
            // Estado
            // 
            this.Estado.Location = new System.Drawing.Point(0, 322);
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(846, 22);
            this.Estado.SizingGrip = false;
            this.Estado.TabIndex = 1;
            this.Estado.Text = "statusStrip1";
            // 
            // GridSolicitudes
            // 
            this.GridSolicitudes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridSolicitudes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdTipo,
            this.IdTipoSolicitud,
            this.Column2,
            this.Column3,
            this.Status,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Descripcion});
            this.GridSolicitudes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridSolicitudes.Location = new System.Drawing.Point(0, 63);
            this.GridSolicitudes.Name = "GridSolicitudes";
            this.GridSolicitudes.Size = new System.Drawing.Size(846, 259);
            this.GridSolicitudes.TabIndex = 2;
            // 
            // IdTipo
            // 
            this.IdTipo.DataPropertyName = "IdTipo";
            this.IdTipo.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.IdTipo.HeaderText = "Comprobantes";
            this.IdTipo.Name = "IdTipo";
            this.IdTipo.ReadOnly = true;
            this.IdTipo.Width = 90;
            // 
            // IdTipoSolicitud
            // 
            this.IdTipoSolicitud.DataPropertyName = "IdTipoSolicitud";
            this.IdTipoSolicitud.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.IdTipoSolicitud.HeaderText = "Solicitud";
            this.IdTipoSolicitud.Name = "IdTipoSolicitud";
            this.IdTipoSolicitud.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IdTipoSolicitud.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.IdTipoSolicitud.Width = 75;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Id";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Format = "#000";
            this.Column2.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column2.HeaderText = "Núm.";
            this.Column2.Name = "Column2";
            this.Column2.Width = 50;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "IdSolicitud";
            this.Column3.HeaderText = "Folio de descarga";
            this.Column3.Name = "Column3";
            this.Column3.Width = 230;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Estatus";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Status.DefaultCellStyle = dataGridViewCellStyle2;
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.Width = 50;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "TotalCFDIS";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            this.Column5.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column5.HeaderText = "Total";
            this.Column5.Name = "Column5";
            this.Column5.Width = 65;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "RFCSolicitante";
            this.Column6.HeaderText = "RFC";
            this.Column6.Name = "Column6";
            this.Column6.Width = 90;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "FechaInicio";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Format = "dd/MM/yyyy";
            this.Column7.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column7.HeaderText = "Fec. Inicial";
            this.Column7.Name = "Column7";
            this.Column7.Width = 75;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "FechaFinal";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Format = "dd/MM/yyyy";
            this.Column8.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column8.HeaderText = "Fec. Final";
            this.Column8.Name = "Column8";
            this.Column8.Width = 75;
            // 
            // Descripcion
            // 
            this.Descripcion.DataPropertyName = "Descripcion";
            this.Descripcion.HeaderText = "Descripción";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Width = 150;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(846, 38);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(244, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Solicitudes de descarga de comprobantes";
            // 
            // DescargaServiceSATForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 344);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.GridSolicitudes);
            this.Controls.Add(this.Estado);
            this.Controls.Add(this.TDescarga);
            this.Controls.Add(this.pictureBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DescargaServiceSATForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Descarga con e.firma (FIEL)";
            this.Load += new System.EventHandler(this.DescargaServiceSATForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridSolicitudes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Common.Forms.ToolBarStandarControl TDescarga;
        private System.Windows.Forms.StatusStrip Estado;
        private System.Windows.Forms.DataGridView GridSolicitudes;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewComboBoxColumn IdTipo;
        private System.Windows.Forms.DataGridViewComboBoxColumn IdTipoSolicitud;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
    }
}