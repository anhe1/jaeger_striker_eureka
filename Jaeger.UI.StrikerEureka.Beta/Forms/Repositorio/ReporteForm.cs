﻿using System;
using System.Collections.Generic;
using Jaeger.Domain.Services;
using Jaeger.Util.Services;

namespace Jaeger.UI.Forms.Repositorio {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private EmbeddedResources horizon = new EmbeddedResources("Jaeger.CFDI");

        public ReporteForm(string rfc, string razonSocial) : base(rfc, razonSocial) {
        }
        public ReporteForm(object sender) : base("", "") {
            this.CurrentObject = sender;
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            this.PathLogo = @"C:\Jaeger\Jaeger.Media\logo-ipo.png";
            if (this.CurrentObject.GetType() == typeof(CFDI.V33.Comprobante)) {
                this.CrearComprobanteV33();
            } else if (this.CurrentObject.GetType() == typeof(CFDI.V32.Comprobante)) {
                this.CrearComprobanteV32();
            }
        }

        private void CrearComprobanteV32() {
            throw new NotImplementedException();
        }

        private void CrearComprobanteV33() {
            var current = (CFDI.V33.Comprobante)this.CurrentObject;

            if (current.Complemento.Pagos == null) {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.CFDI.Reports.Cfdiv33.rdlc");
            } else {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.CFDI.Reports.Cfdiv33Pagos10.rdlc");
                this.SetDataSource("PagosPago", Domain.Base.Services.DbConvert.ConvertToDataTable(new List<CFDI.Complemento.Pagos.V10.PagosPago>() { current.Complemento.Pagos.Pago[0] }));
                this.SetDataSource("PagoDoctoRelacionado", Domain.Base.Services.DbConvert.ConvertToDataTable(current.Complemento.Pagos.Pago[0].DoctoRelacionado));
            }
            this.SetLogotipo();
            string[] qr = { "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?", current.Emisor.Rfc, current.Receptor.Rfc, current.Total.ToString(), current.Complemento.TimbreFiscalDigital.UUID, current.Complemento.TimbreFiscalDigital.SelloSAT };
            this.ImagenQR = QRCodeExtension.GetQRBase64(qr);
            this.SetDisplayName("Comprobante Fiscal V33");
            this.SetParameter("ImagenQR", this.ImagenQR);
            this.SetParameter("TotalEnLetra", Domain.Base.Services.NumeroALetras.Convertir(Double.Parse(current.Total.ToString()), 1));
            this.SetDataSource("Comprobante", Domain.Base.Services.DbConvert.ConvertToDataTable(new List<CFDI.V33.Comprobante>() { current }));
            this.SetDataSource("Conceptos", Domain.Base.Services.DbConvert.ConvertToDataTable(current.Conceptos));
            this.SetDataSource("TimbreFiscalV11", Domain.Base.Services.DbConvert.ConvertToDataTable(new List<CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital>() { current.Complemento.TimbreFiscalDigital }));
            this.SetDataSource("Emisor", Domain.Base.Services.DbConvert.ConvertToDataTable(new List<CFDI.V33.ComprobanteEmisor>() { current.Emisor }));
            this.SetDataSource("Receptor", Domain.Base.Services.DbConvert.ConvertToDataTable(new List<CFDI.V33.ComprobanteReceptor>() { current.Receptor }));
            this.SetDataSource("Impuestos", Domain.Base.Services.DbConvert.ConvertToDataTable(new List<CFDI.V33.ComprobanteImpuestos>() { current.Impuestos }));
            this.Finalizar();
        }
    }
}
