﻿using System;
using System.Collections.Generic;
using System.IO;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Util.Services;

namespace Jaeger.UI.Forms {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private EmbeddedResources horizon = new EmbeddedResources("Jaeger.Domain.Comprobante");

        public ReporteForm(string rfc, string razonSocial) : base(rfc, razonSocial) {
        }

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(ComprobanteValidacionPrinter)) {
                this.CrearReporteValidacion();
            }
        }

        private void CrearReporteValidacion() {
            var current = (ComprobanteValidacionPrinter)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Comprobante.Reports.ComprobanteValidacion35Report.rdlc");
            this.SetDisplayName("Reporte Validación");
            var d = Domain.Base.Services.DbConvert.ConvertToDataTable<ComprobanteValidacionPrinter>(new List<ComprobanteValidacionPrinter>() { current });
            this.SetDataSource("Validacion", d);
            var d1 = Domain.Base.Services.DbConvert.ConvertToDataTable<PropertyValidate>(current.Resultados);
            this.SetDataSource("Resultados", d1);
            this.Finalizar();
        }
    }
}
