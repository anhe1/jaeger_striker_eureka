﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Jaeger.Aplication.Contribuyentes;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Contribuyentes {
    public partial class ContribuyenteCatalogoForm : Form {
        protected IDirectorioService _Service;
        protected TipoRelacionComericalEnum _RelacionComericalEnum;
        private BindingList<ContribuyenteDomicilioSingleModel> _ContribuyenteDomicilio;

        public ContribuyenteCatalogoForm() {
            InitializeComponent();
        }

        /// <summary>
        /// tipo de relacion 1 = Cliente, 2 = Provedor
        /// </summary>
        public ContribuyenteCatalogoForm(int relacionComericalEnum) {
            InitializeComponent();
            this._RelacionComericalEnum = (TipoRelacionComericalEnum)relacionComericalEnum;
        }

        private void ContribuyenteCatalogoForm_Load(object sender, EventArgs e) {
            this.GridData.DataGridCommon();
            this._Service = new DirectorioService(this._RelacionComericalEnum);
        }


        private void TContribuyente_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this._ContribuyenteDomicilio;
        }

        private void TContribuyente_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TContribuyente_Nuevo_Click(object sender, EventArgs e) {
            using (var nuevo = new ContribuyenteForm(null)) {
                nuevo.Text = "Nuevo";
                nuevo.ShowDialog(this);
            }
        }

        private void ToolBarButtonEditar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                using (var espera = new WaitingForm(this.d)) {
                    espera.Text = "Consultando información ...";
                    espera.ShowDialog(this);
                }
                if (this.Tag != null) {
                    var seleccionado = this.Tag as ContribuyenteDetailModel;
                    if (seleccionado != null) {
                        using (var editar = new ContribuyenteForm(seleccionado)) {
                            editar.ShowDialog(this);
                        }
                    }
                }
            }
        }

        private void ToolBarButtonRemover_Click(object sender, EventArgs e) {
            //if (this.GridData.CurrentRow != null) {
            //    if (RadMessageBox.Show(this, Properties.Resources.Pregunta_RemoveRegistro, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {

            //    }
            //}
        }

        private void ToolBarButtonFiltro_Click(object sender, EventArgs e) {
            //this.GridData.ActivateFilterRow(this.ToolBarButtonFiltro.ToggleState);
        }

        private void Consultar() {
            this._ContribuyenteDomicilio = new BindingList<ContribuyenteDomicilioSingleModel>(this._Service.GetList<ContribuyenteDomicilioSingleModel>().ToList());
        }

        private void d() {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as ContribuyenteDomicilioSingleModel;
                this.Tag = this._Service.GetById(seleccionado.IdDirectorio);
            }
        }
    }
}
