﻿namespace Jaeger.UI.Forms.Contribuyentes {
    partial class ContribuyenteForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.contribuyente = new Jaeger.UI.Forms.Contribuyentes.ContribuyenteControl();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.SuspendLayout();
            // 
            // contribuyente
            // 
            this.contribuyente.Dock = System.Windows.Forms.DockStyle.Top;
            this.contribuyente.Location = new System.Drawing.Point(0, 25);
            this.contribuyente.Name = "contribuyente";
            this.contribuyente.Size = new System.Drawing.Size(800, 133);
            this.contribuyente.TabIndex = 1;
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "Titulo";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = true;
            this.ToolBar.ShowGuardar = true;
            this.ToolBar.ShowHerramientas = true;
            this.ToolBar.ShowImprimir = true;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.ShowRemover = true;
            this.ToolBar.Size = new System.Drawing.Size(800, 25);
            this.ToolBar.TabIndex = 2;
            this.ToolBar.ButtonNuevo_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonNuevo_Click);
            this.ToolBar.ButtonGuardar_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonGuardar_Click);
            this.ToolBar.ButtonActualizar_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonActualizar_Click);
            this.ToolBar.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonCerrar_Click);
            // 
            // ContribuyenteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.contribuyente);
            this.Controls.Add(this.ToolBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ContribuyenteForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Contribuyente";
            this.Load += new System.EventHandler(this.ContribuyenteForm_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private ContribuyenteControl contribuyente;
        private Common.Forms.ToolBarStandarControl ToolBar;
    }
}