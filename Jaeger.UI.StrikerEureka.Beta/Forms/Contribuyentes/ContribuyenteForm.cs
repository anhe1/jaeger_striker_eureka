﻿using System;
using System.Windows.Forms;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.UI.Forms.Contribuyentes {
    public partial class ContribuyenteForm : Form {
        private ContribuyenteDetailModel persona;

        public ContribuyenteForm(ContribuyenteDetailModel model) {
            InitializeComponent();
            this.persona = model;
        }

        private void ContribuyenteForm_Load(object sender, EventArgs e) {
            this.ToolBar.Actualizar.PerformClick();
        }

        private void ToolBarButtonNuevo_Click(object sender, EventArgs e) {

        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            if (this.persona == null) {
                this.persona = new ContribuyenteDetailModel();
            }
            this.CreateBinding();
        }

        private void ToolBarButtonGuardar_Click(object sender, EventArgs e) {

        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CreateBinding() {
            this.contribuyente.Clave.DataBindings.Clear();
            this.contribuyente.Clave.DataBindings.Add("Text", this.persona, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.contribuyente.RFC.DataBindings.Clear();
            this.contribuyente.RFC.DataBindings.Add("Text", this.persona, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);
            this.contribuyente.CURP.DataBindings.Clear();
            this.contribuyente.CURP.DataBindings.Add("Text", this.persona, "CURP", true, DataSourceUpdateMode.OnPropertyChanged);
            this.contribuyente.NumRegIdTrib.DataBindings.Clear();
            this.contribuyente.NumRegIdTrib.DataBindings.Add("Text", this.persona, "NumRegIdTrib", true, DataSourceUpdateMode.OnPropertyChanged);
            this.contribuyente.Nombre.DataBindings.Clear();
            this.contribuyente.Nombre.DataBindings.Add("Text", this.persona, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);
        }
    }
}
