﻿using System;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class ComplementoPagoControl : UserControl {
        protected internal ComplementoPagoDetailModel Pago;
        protected IFormaPagoCatalogo catalogoFormaPago = new FormaPagoCatalogo();
        public ComplementoPagoControl() {
            InitializeComponent();
        }

        private void ComplementoPagoControl_Load(object sender, EventArgs e) {
            this.gridDoctoRelacionados.DataGridCommon();
            this.gridImpuestosTraslado.DataGridCommon();
        }

        public void Start() {
            this.catalogoFormaPago.Load();

            // catalogo formas de pago
            this.FormaDePago.DataSource = this.catalogoFormaPago.Items;
            this.FormaDePago.SelectedIndex = -1;
        }

        public virtual void CreateBinding() {
            this.FechaPago.DataBindings.Clear();
            this.FechaPago.DataBindings.Add("Value", this.Pago, "FechaPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FormaDePago.DataBindings.Clear();
            this.FormaDePago.DataBindings.Add("SelectedValue", this.Pago, "FormaDePagoP", true, DataSourceUpdateMode.OnPropertyChanged);
            this.NumOperacion.DataBindings.Clear();
            this.NumOperacion.DataBindings.Add("Text", this.Pago, "NumOperacion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TipoCambioP.DataBindings.Clear();
            this.TipoCambioP.DataBindings.Add("Value", this.Pago, "TipoCambioP", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Monto.DataBindings.Clear();
            this.Monto.DataBindings.Add("Value", this.Pago, "Monto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.RfcEmisorCtaOrd.DataBindings.Clear();
            this.RfcEmisorCtaOrd.DataBindings.Add("Text", this.Pago, "RfcEmisorCtaOrd", true, DataSourceUpdateMode.OnPropertyChanged);
            this.NomBancoOrdExt.DataBindings.Clear();
            this.NomBancoOrdExt.DataBindings.Add("Text", this.Pago, "NomBancoOrdExt", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CtaOrdenante.DataBindings.Clear();
            this.CtaOrdenante.DataBindings.Add("Text", this.Pago, "CtaOrdenante", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Moneda1.DataBindings.Clear();
            this.Moneda1.DataBindings.Add("SelectedValue", this.Pago, "MonedaP", true, DataSourceUpdateMode.OnPropertyChanged);
            this.RfcEmisorCtaBen.DataBindings.Clear();
            this.RfcEmisorCtaBen.DataBindings.Add("Text", this.Pago, "RfcEmisorCtaBen", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CtaBeneficiario.DataBindings.Clear();
            this.CtaBeneficiario.DataBindings.Add("Text", this.Pago, "CtaBeneficiario", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TipoCadPago.DataBindings.Clear();
            this.TipoCadPago.DataBindings.Add("SelectedValue", this.Pago, "TipoCadPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CertPago.DataBindings.Clear();
            this.CertPago.DataBindings.Add("Text", this.Pago, "CertPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.SelloPago.DataBindings.Clear();
            this.SelloPago.DataBindings.Add("Text", this.Pago, "SelloPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CadPago.DataBindings.Clear();
            this.CadPago.DataBindings.Add("Text", this.Pago, "CadPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.gridDoctoRelacionados.DataSource = this.Pago.DoctoRelacionados;
        }
    }
}
