﻿using System;
using System.Windows.Forms;

using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.Catalogos.Entities;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class ComprobanteFiscalForm : Form {
        protected Aplication.Comprobante.IComprobanteFiscalService service;
        protected CFDISubTipoEnum subTipo;
        protected internal ToolStripMenuItem ConceptoACuentaTercero = new ToolStripMenuItem { Text = "A cuenta de terceros" };
        protected internal ToolStripMenuItem DesdeArchivo = new ToolStripMenuItem { Text = "Desde archivo" };
        protected internal ToolStripMenuItem ComplementoPagos = new ToolStripMenuItem { Text = "Pagos" };

        public ComprobanteFiscalForm(ComprobanteFiscalDetailModel model, CFDISubTipoEnum subTipo) {
            InitializeComponent();
            this.subTipo = subTipo;
            if (model != null) {
                this.ComprobanteControl.Comprobante = model;
            }
        }

        private void ComprobanteFiscalForm_Load(object sender, EventArgs e) {
            this.gridConceptos.DataGridCommon();
            this.gridConceptos.ReadOnly = false;
            this.gridConceptoParte.DataGridCommon();
            this.gridConceptoImpuestos.DataGridCommon();
            this.gridConceptoImpuestos.ReadOnly = false;
            this.gridConceptoAduana.DataGridCommon();
            this.gridCuentaPredial.DataGridCommon();
            this.gridCuentaPredial.ReadOnly = false;
            this.service = new ComprobanteFiscalService();

            this.TConceptos.Nuevo.Click += TConceptos_Nuevo_Click;
            this.TConceptos.Duplicar.Click += TConceptos_Duplicar_Click;
            this.TConceptos.Remover.Click += TConceptos_Remover_Click;
            this.TConceptos.Productos.Click += TConceptos_Productos_Click;
            this.TConceptos.Unidad.Click += TConceptos_Unidad_Click;
            this.TConceptos.Complementos.DropDownItems.Add(this.ConceptoACuentaTercero);
            this.TConceptoParte.Complementos.Visible = false;

            this.TConceptoParte.Nuevo.Click += TConceptoParte_Nuevo_Click;
            this.TConceptoParte.Remover.Click += TConceptoParte_Remover_Click;
            this.TConceptoParte.Duplicar.Click += TConceptoParte_Duplicar_Click;
            this.TConceptoParte.Productos.Click += TConceptoParte_Productos_Click;
            this.TConceptoParte.Unidad.Click += TConceptoParte_Unidad_Click;
            this.TImpuesto.Nuevo.Click += TImpuestos_Nuevo_Click;
            this.TImpuesto.Remover.Click += TImpuestos_Remover_Click;

            var _c = this.ComprobanteControl.TComprobante.Items["Complementos"] as ToolStripDropDownButton;
            _c.DropDownItems.Add(this.DesdeArchivo);
            this.DesdeArchivo.Click += DesdeArchivo_Click;
            this.ComprobanteControl.BindingCompleted += ComprobanteControl_BindingCompleted;
            this.ComprobanteControl.Start(this.subTipo);

            this.TImpuesto.ShowNuevo = true;
            this.TImpuesto.ShowRemover = true;

            this.ConceptoACuentaTercero.Click += ConceptoACuentaTercero_Click;

            this.ComprobanteControl.Cerrar.Click += this.Cerrar_Click;

            var cbo = this.ObjImpuesto as DataGridViewComboBoxColumn;
            cbo.DataSource = ComprobanteCommonService.GetObjetoImpuesto();
            cbo.ValueMember = "Id";
            cbo.DisplayMember = "Descriptor";

            var _tipo = this.Tipo as DataGridViewComboBoxColumn;
            _tipo.DisplayMember = "Descripcion";
            _tipo.ValueMember = "Id";
            _tipo.DataSource = Aplication.Base.ConfigService.GetImpuestoTipo();
            var _impuesto = this.Impuesto as DataGridViewComboBoxColumn;
            _impuesto.DisplayMember = "Descripcion";
            _impuesto.ValueMember = "Id";
            _impuesto.DataSource = Aplication.Base.ConfigService.GetImpuestos();
            var _factor = this.TipoFactor as DataGridViewComboBoxColumn;
            _factor.ValueMember = "Id";
            _factor.DisplayMember = "Descripcion";
            _factor.DataSource = Aplication.Base.ConfigService.GetImpuestoTipoFactor();
        }

        public virtual void DesdeArchivo_Click(object sender, EventArgs e) {
            var _openFile = new OpenFileDialog();
            if (_openFile.ShowDialog() == DialogResult.OK) {
                //this.ComprobanteControl.Comprobante = this.service.GetComprobante(_openFile.FileName);
                //this.ComprobanteControl_BindingCompleted(sender, e);
            }
        }

        #region barra de herramientas control
        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region barra de herramientas conceptos
        public virtual void TConceptos_Nuevo_Click(object sender, EventArgs e) {
            this.ComprobanteControl.Comprobante.Conceptos.Add(new ComprobanteConceptoDetailModel { Activo = true });
        }

        public virtual void TConceptos_Duplicar_Click(object sender, EventArgs e) {
            if (this.gridConceptos.CurrentRow != null) {
                var _seleccionado = this.gridConceptos.CurrentRow.DataBoundItem as ComprobanteConceptoDetailModel;
                if (_seleccionado != null) {
                    var _clon = _seleccionado.Clone();
                    this.ComprobanteControl.Comprobante.Conceptos.Add(_clon);
                }
            }
        }

        public virtual void TConceptos_Remover_Click(object sender, EventArgs e) {
            if (this.gridConceptos.CurrentRow != null) {
                var _seleccionado = this.gridConceptos.CurrentRow.DataBoundItem as ComprobanteConceptoDetailModel;
                if (_seleccionado != null) {
                    if (_seleccionado.Id > 0) {
                        _seleccionado.Activo = false;
                        this.gridConceptos.CurrentRow.Visible = false;
                    } else {
                        this.gridConceptos.Rows.Remove(this.gridConceptos.CurrentRow);
                    }
                }
            }
        }

        public virtual void TConceptos_Productos_Click(object sender, EventArgs e) {
            var _producto = new ProdServCatalogoBuscarForm();
            _producto.Agregar += Producto_Agregar;
            _producto.ShowDialog(this);
        }

        public virtual void Producto_Agregar(object sender, ComprobanteConceptoDetailModel e) {
            if (e != null) {
                this.ComprobanteControl.Comprobante.Conceptos.Add(e);
            }
        }

        public virtual void TConceptos_Unidad_Click(object sender, EventArgs e) {
            if (this.gridConceptos.CurrentRow != null) {
                var _unidad = new UnidadCatalogoBuscarForm();
                _unidad.Seleted += Unidad_Seleted;
                _unidad.ShowDialog(this);
            }
        }

        public virtual void Unidad_Seleted(object sender, ClaveUnidad e) {
            if (e != null) {
                if (this.gridConceptos.CurrentRow != null) {
                    var _seleccionado = this.gridConceptos.CurrentRow.DataBoundItem as ComprobanteConceptoDetailModel;
                    if (_seleccionado != null) {
                        _seleccionado.ClaveUnidad = e.Clave;
                    }
                }
            }
        }

        public virtual void ConceptoACuentaTercero_Click(object sender, EventArgs e) {
            if (this.gridConceptos.CurrentRow != null) {
                var _tercero = new ConceptoACuentaTerceroForm(null);
                _tercero.ShowDialog(this);
            }
        }
        #endregion

        #region barra de herramientas partes
        public virtual void TConceptoParte_Productos_Click(object sender, EventArgs e) {
            var _producto = new ProdServCatalogoBuscarForm();
            _producto.Agregar += TConceptoParte_Producto_Agregar;
            _producto.ShowDialog(this);
        }

        private void TConceptoParte_Producto_Agregar(object sender, ComprobanteConceptoDetailModel e) {
            if (e != null) {
                if (this.gridConceptos.CurrentRow != null) {
                    var selected = this.gridConceptos.Rows.IndexOf(this.gridConceptos.CurrentRow);
                    this.ComprobanteControl.Comprobante.Conceptos[selected].Parte.Add(new ConceptoParte() { ClaveProdServ = e.ClaveProdServ, Descripcion = e.Descripcion });
                }
            }
        }

        public virtual void TConceptoParte_Remover_Click(object sender, EventArgs e) {
            if (this.gridConceptoParte.CurrentRow != null) {
                this.gridConceptoParte.Rows.Remove(this.gridConceptoParte.CurrentRow);
            }
        }

        public virtual void TConceptoParte_Nuevo_Click(object sender, EventArgs e) {
            if (this.gridConceptos.CurrentRow != null) {
                var selected = this.gridConceptos.Rows.IndexOf(this.gridConceptos.CurrentRow);
                this.ComprobanteControl.Comprobante.Conceptos[selected].Parte.Add(new Domain.Comprobante.Entities.ConceptoParte());
            }
        }

        private void TConceptoParte_Unidad_Click(object sender, EventArgs e) {
            if (this.gridConceptoParte.CurrentRow != null) {
                var _unidad = new UnidadCatalogoBuscarForm();
                _unidad.Seleted += TConceptoParte_Unidad_Seleted;
                _unidad.ShowDialog(this);
            }
        }

        private void TConceptoParte_Unidad_Seleted(object sender, ClaveUnidad e) {
            if (e != null) {
                if (this.gridConceptoParte.CurrentRow != null) {
                    var _seleccionado = this.gridConceptoParte.CurrentRow.DataBoundItem as ConceptoParte;
                    if (_seleccionado != null) {
                        _seleccionado.Unidad = e.Nombre;
                    }
                }
            }
        }

        private void TConceptoParte_Duplicar_Click(object sender, EventArgs e) {
            if (this.gridConceptoParte.CurrentRow != null) {
                var _seleccionado = this.gridConceptoParte.CurrentRow.DataBoundItem as ConceptoParte;
                if (_seleccionado != null) {
                    this.gridConceptoParte.Rows.Add(new ConceptoParte {
                        Cantidad = _seleccionado.Cantidad,
                        ClaveProdServ = _seleccionado.ClaveProdServ,
                        Descripcion = _seleccionado.Descripcion,
                        Unidad = _seleccionado.Unidad,
                        NoIdentificacion = _seleccionado.NoIdentificacion,
                        ValorUnitario = _seleccionado.ValorUnitario
                    });
                }
            }
        }
        #endregion

        #region barra de herramientas impuestos
        private void TImpuestos_Nuevo_Click(object sender, EventArgs e) {
            if (this.gridConceptos.CurrentRow != null) {
                var selected = this.gridConceptos.Rows.IndexOf(this.gridConceptos.CurrentRow);
                this.ComprobanteControl.Comprobante.Conceptos[selected].Impuestos.Add(new ComprobanteConceptoImpuesto(this.ComprobanteControl.Comprobante.Conceptos[selected].Importe));
            }
        }

        public virtual void TImpuestos_Remover_Click(object sender, EventArgs e) {
            if (this.gridConceptoImpuestos.CurrentRow != null) {
                this.gridConceptoImpuestos.Rows.Remove(this.gridConceptoImpuestos.CurrentRow);
            }
        }
        #endregion

        private void ComprobanteControl_BindingCompleted(object sender, EventArgs e) {
            this.gridConceptos.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
            this.gridConceptos.ReadOnly = !this.ComprobanteControl.Comprobante.IsEditable;
            this.gridConceptoParte.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
            this.gridConceptoImpuestos.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
            this.gridConceptoAduana.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
            this.gridConceptoParte.DataMember = "Parte";
            this.gridConceptoImpuestos.DataMember = "Impuestos";
            this.gridConceptoAduana.DataMember = "InformacionAduanera";

            this.SubTotal.DataBindings.Clear();
            this.SubTotal.DataBindings.Add("Value", this.ComprobanteControl.Comprobante, "SubTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TDescuento.DataBindings.Clear();
            this.TDescuento.DataBindings.Add("Value", this.ComprobanteControl.Comprobante, "Descuento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Total.DataBindings.Clear();
            this.Total.DataBindings.Add("Text", this.ComprobanteControl.Comprobante, "Total", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TrasladoIVA.DataBindings.Clear();
            this.TrasladoIVA.DataBindings.Add("Value", this.ComprobanteControl.Comprobante, "TrasladoIVA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TrasladoIEPS.DataBindings.Clear();
            this.TrasladoIEPS.DataBindings.Add("Value", this.ComprobanteControl.Comprobante, "TrasladoIEPS", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RetencionIVA.DataBindings.Clear();
            this.RetencionIVA.DataBindings.Add("Value", this.ComprobanteControl.Comprobante, "RetencionIVA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RetencionISR.DataBindings.Clear();
            this.RetencionISR.DataBindings.Add("Value", this.ComprobanteControl.Comprobante, "RetencionISR", true, DataSourceUpdateMode.OnPropertyChanged);
        }
    }
}
