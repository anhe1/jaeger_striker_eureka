﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class CartaPorteUbicacionDomicilioControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Calle = new System.Windows.Forms.TextBox();
            this.NumeroExterior = new System.Windows.Forms.TextBox();
            this.NumeroInterior = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Colonia = new System.Windows.Forms.TextBox();
            this.Localidad = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Referencia = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Municipio = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Estado = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Pais = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.CodigoPostal = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Cerrar = new System.Windows.Forms.Button();
            this.Guardar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Calle:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(218, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "NumeroExterior";
            // 
            // Calle
            // 
            this.Calle.Location = new System.Drawing.Point(9, 32);
            this.Calle.MaxLength = 100;
            this.Calle.Name = "Calle";
            this.Calle.Size = new System.Drawing.Size(206, 20);
            this.Calle.TabIndex = 2;
            // 
            // NumeroExterior
            // 
            this.NumeroExterior.Location = new System.Drawing.Point(221, 32);
            this.NumeroExterior.MaxLength = 55;
            this.NumeroExterior.Name = "NumeroExterior";
            this.NumeroExterior.Size = new System.Drawing.Size(100, 20);
            this.NumeroExterior.TabIndex = 3;
            // 
            // NumeroInterior
            // 
            this.NumeroInterior.Location = new System.Drawing.Point(327, 32);
            this.NumeroInterior.MaxLength = 55;
            this.NumeroInterior.Name = "NumeroInterior";
            this.NumeroInterior.Size = new System.Drawing.Size(100, 20);
            this.NumeroInterior.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(324, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "NumeroInterior";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(430, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Colonia:";
            // 
            // Colonia
            // 
            this.Colonia.Location = new System.Drawing.Point(433, 32);
            this.Colonia.MaxLength = 120;
            this.Colonia.Name = "Colonia";
            this.Colonia.Size = new System.Drawing.Size(206, 20);
            this.Colonia.TabIndex = 7;
            // 
            // Localidad
            // 
            this.Localidad.Location = new System.Drawing.Point(9, 110);
            this.Localidad.MaxLength = 120;
            this.Localidad.Name = "Localidad";
            this.Localidad.Size = new System.Drawing.Size(312, 20);
            this.Localidad.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Localidad:";
            // 
            // Referencia
            // 
            this.Referencia.Location = new System.Drawing.Point(327, 110);
            this.Referencia.MaxLength = 250;
            this.Referencia.Name = "Referencia";
            this.Referencia.Size = new System.Drawing.Size(312, 20);
            this.Referencia.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(324, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Referencia:";
            // 
            // Municipio
            // 
            this.Municipio.Location = new System.Drawing.Point(9, 71);
            this.Municipio.MaxLength = 120;
            this.Municipio.Name = "Municipio";
            this.Municipio.Size = new System.Drawing.Size(206, 20);
            this.Municipio.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Municipio:";
            // 
            // Estado
            // 
            this.Estado.Location = new System.Drawing.Point(221, 71);
            this.Estado.MaxLength = 30;
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(206, 20);
            this.Estado.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(220, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Estado:";
            // 
            // Pais
            // 
            this.Pais.Location = new System.Drawing.Point(433, 71);
            this.Pais.MaxLength = 30;
            this.Pais.Name = "Pais";
            this.Pais.Size = new System.Drawing.Size(100, 21);
            this.Pais.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(430, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Pais:";
            // 
            // CodigoPostal
            // 
            this.CodigoPostal.Location = new System.Drawing.Point(539, 71);
            this.CodigoPostal.MaxLength = 12;
            this.CodigoPostal.Name = "CodigoPostal";
            this.CodigoPostal.Size = new System.Drawing.Size(100, 20);
            this.CodigoPostal.TabIndex = 19;
            this.CodigoPostal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(536, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Codigo Postal:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.CodigoPostal);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.Calle);
            this.groupBox1.Controls.Add(this.Pais);
            this.groupBox1.Controls.Add(this.NumeroExterior);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.Estado);
            this.groupBox1.Controls.Add(this.NumeroInterior);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.Municipio);
            this.groupBox1.Controls.Add(this.Colonia);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Referencia);
            this.groupBox1.Controls.Add(this.Localidad);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(653, 144);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // Cerrar
            // 
            this.Cerrar.Location = new System.Drawing.Point(564, 150);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(75, 23);
            this.Cerrar.TabIndex = 21;
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.UseVisualStyleBackColor = true;
            this.Cerrar.Click += new System.EventHandler(this.Cerrar_Click);
            // 
            // Guardar
            // 
            this.Guardar.Location = new System.Drawing.Point(483, 150);
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(75, 23);
            this.Guardar.TabIndex = 22;
            this.Guardar.Text = "Guardar";
            this.Guardar.UseVisualStyleBackColor = true;
            this.Guardar.Click += new System.EventHandler(this.Guardar_Click);
            // 
            // CartaPorteUbicacionDomicilioControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 179);
            this.Controls.Add(this.Guardar);
            this.Controls.Add(this.Cerrar);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CartaPorteUbicacionDomicilioControl";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Domicilio";
            this.Load += new System.EventHandler(this.CartaPorteUbicacionDomicilioControl_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Calle;
        private System.Windows.Forms.TextBox NumeroExterior;
        private System.Windows.Forms.TextBox NumeroInterior;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Colonia;
        private System.Windows.Forms.TextBox Localidad;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Referencia;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Municipio;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Estado;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox Pais;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox CodigoPostal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Cerrar;
        private System.Windows.Forms.Button Guardar;
    }
}
