﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class ComprobanteFiscalCancelarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TCancelar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.MotivoCancelacion = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Relacionado = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.IdDocumento = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TCancelar
            // 
            this.TCancelar.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCancelar.Etiqueta = "";
            this.TCancelar.Location = new System.Drawing.Point(0, 0);
            this.TCancelar.Name = "TCancelar";
            this.TCancelar.ShowActualizar = false;
            this.TCancelar.ShowCerrar = true;
            this.TCancelar.ShowEditar = false;
            this.TCancelar.ShowGuardar = true;
            this.TCancelar.ShowHerramientas = false;
            this.TCancelar.ShowImprimir = false;
            this.TCancelar.ShowNuevo = false;
            this.TCancelar.ShowRemover = false;
            this.TCancelar.Size = new System.Drawing.Size(417, 25);
            this.TCancelar.TabIndex = 0;
            this.TCancelar.ButtonGuardar_Click += new System.EventHandler<System.EventArgs>(this.TCancelar_Guardar_Click);
            this.TCancelar.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TCancelar_Cerrar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MotivoCancelacion);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.Relacionado);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.IdDocumento);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(417, 104);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Para cada CFDI que desea cancelar debe capturar el motivo de cancelación";
            // 
            // MotivoCancelacion
            // 
            this.MotivoCancelacion.FormattingEnabled = true;
            this.MotivoCancelacion.Location = new System.Drawing.Point(131, 72);
            this.MotivoCancelacion.Name = "MotivoCancelacion";
            this.MotivoCancelacion.Size = new System.Drawing.Size(274, 21);
            this.MotivoCancelacion.TabIndex = 5;
            this.MotivoCancelacion.SelectedValueChanged += new System.EventHandler(this.MotivoCancelacion_SelectedValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Motivo de cancelación:";
            // 
            // Relacionado
            // 
            this.Relacionado.Location = new System.Drawing.Point(131, 46);
            this.Relacionado.MaxLength = 36;
            this.Relacionado.Name = "Relacionado";
            this.Relacionado.Size = new System.Drawing.Size(274, 20);
            this.Relacionado.TabIndex = 3;
            this.Relacionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Folio relacionado:";
            // 
            // IdDocumento
            // 
            this.IdDocumento.Location = new System.Drawing.Point(131, 20);
            this.IdDocumento.MaxLength = 36;
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.Size = new System.Drawing.Size(274, 20);
            this.IdDocumento.TabIndex = 1;
            this.IdDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Folio Fiscal:";
            // 
            // ComprobanteFiscalCancelarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 129);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TCancelar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ComprobanteFiscalCancelarForm";
            this.ShowInTaskbar = false;
            this.Text = "Cancelar";
            this.Load += new System.EventHandler(this.ComprobanteFiscalCancelarForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarControl TCancelar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox MotivoCancelacion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Relacionado;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox IdDocumento;
        private System.Windows.Forms.Label label1;
    }
}