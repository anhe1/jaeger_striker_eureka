﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class CartaPorteDomicilioFiscalcontrol {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.CodigoPostal = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Calle = new System.Windows.Forms.TextBox();
            this.Pais = new System.Windows.Forms.ComboBox();
            this.NumeroExterior = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.Estado = new System.Windows.Forms.ComboBox();
            this.NumeroInterior = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.Municipio = new System.Windows.Forms.ComboBox();
            this.Colonia = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.Referencia = new System.Windows.Forms.TextBox();
            this.Localidad = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.CodigoPostal);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.Calle);
            this.groupBox1.Controls.Add(this.Pais);
            this.groupBox1.Controls.Add(this.NumeroExterior);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.Estado);
            this.groupBox1.Controls.Add(this.NumeroInterior);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.Municipio);
            this.groupBox1.Controls.Add(this.Colonia);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.Referencia);
            this.groupBox1.Controls.Add(this.Localidad);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(567, 142);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Domicilio";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "Calle:";
            // 
            // CodigoPostal
            // 
            this.CodigoPostal.Location = new System.Drawing.Point(6, 112);
            this.CodigoPostal.MaxLength = 12;
            this.CodigoPostal.Name = "CodigoPostal";
            this.CodigoPostal.Size = new System.Drawing.Size(79, 20);
            this.CodigoPostal.TabIndex = 39;
            this.CodigoPostal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(285, 18);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "Núm. Exterior:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 96);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 13);
            this.label15.TabIndex = 38;
            this.label15.Text = "Codigo Postal:*";
            // 
            // Calle
            // 
            this.Calle.Location = new System.Drawing.Point(6, 34);
            this.Calle.MaxLength = 100;
            this.Calle.Name = "Calle";
            this.Calle.Size = new System.Drawing.Size(273, 20);
            this.Calle.TabIndex = 22;
            // 
            // Pais
            // 
            this.Pais.Items.AddRange(new object[] {
            "MEX"});
            this.Pais.Location = new System.Drawing.Point(459, 34);
            this.Pais.MaxLength = 30;
            this.Pais.Name = "Pais";
            this.Pais.Size = new System.Drawing.Size(100, 21);
            this.Pais.TabIndex = 37;
            // 
            // NumeroExterior
            // 
            this.NumeroExterior.Location = new System.Drawing.Point(285, 34);
            this.NumeroExterior.MaxLength = 55;
            this.NumeroExterior.Name = "NumeroExterior";
            this.NumeroExterior.Size = new System.Drawing.Size(81, 20);
            this.NumeroExterior.TabIndex = 23;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(459, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 13);
            this.label16.TabIndex = 36;
            this.label16.Text = "País:*";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(372, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "Núm. Interior:";
            // 
            // Estado
            // 
            this.Estado.Location = new System.Drawing.Point(6, 73);
            this.Estado.MaxLength = 30;
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(180, 21);
            this.Estado.TabIndex = 35;
            // 
            // NumeroInterior
            // 
            this.NumeroInterior.Location = new System.Drawing.Point(372, 34);
            this.NumeroInterior.MaxLength = 55;
            this.NumeroInterior.Name = "NumeroInterior";
            this.NumeroInterior.Size = new System.Drawing.Size(81, 20);
            this.NumeroInterior.TabIndex = 25;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 57);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 13);
            this.label18.TabIndex = 34;
            this.label18.Text = "Estado:*";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(91, 95);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(45, 13);
            this.label19.TabIndex = 26;
            this.label19.Text = "Colonia:";
            // 
            // Municipio
            // 
            this.Municipio.Location = new System.Drawing.Point(192, 73);
            this.Municipio.MaxLength = 120;
            this.Municipio.Name = "Municipio";
            this.Municipio.Size = new System.Drawing.Size(180, 21);
            this.Municipio.TabIndex = 33;
            // 
            // Colonia
            // 
            this.Colonia.Location = new System.Drawing.Point(91, 111);
            this.Colonia.MaxLength = 120;
            this.Colonia.Name = "Colonia";
            this.Colonia.Size = new System.Drawing.Size(227, 21);
            this.Colonia.TabIndex = 27;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(192, 57);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 13);
            this.label20.TabIndex = 32;
            this.label20.Text = "Municipio:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(379, 57);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 13);
            this.label21.TabIndex = 28;
            this.label21.Text = "Localidad:";
            // 
            // Referencia
            // 
            this.Referencia.Location = new System.Drawing.Point(324, 112);
            this.Referencia.MaxLength = 250;
            this.Referencia.Name = "Referencia";
            this.Referencia.Size = new System.Drawing.Size(235, 20);
            this.Referencia.TabIndex = 31;
            // 
            // Localidad
            // 
            this.Localidad.Location = new System.Drawing.Point(379, 73);
            this.Localidad.MaxLength = 120;
            this.Localidad.Name = "Localidad";
            this.Localidad.Size = new System.Drawing.Size(180, 21);
            this.Localidad.TabIndex = 29;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(324, 96);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(62, 13);
            this.label22.TabIndex = 30;
            this.label22.Text = "Referencia:";
            // 
            // CartaPorteDomicilioFiscalcontrol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "CartaPorteDomicilioFiscalcontrol";
            this.Size = new System.Drawing.Size(567, 142);
            this.Load += new System.EventHandler(this.CartaPorteDomicilioFiscalcontrol_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label13;
        protected internal System.Windows.Forms.TextBox CodigoPostal;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        protected internal System.Windows.Forms.TextBox Calle;
        protected internal System.Windows.Forms.ComboBox Pais;
        protected internal System.Windows.Forms.TextBox NumeroExterior;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        protected internal System.Windows.Forms.ComboBox Estado;
        protected internal System.Windows.Forms.TextBox NumeroInterior;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        protected internal System.Windows.Forms.ComboBox Municipio;
        protected internal System.Windows.Forms.ComboBox Colonia;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        protected internal System.Windows.Forms.TextBox Referencia;
        protected internal System.Windows.Forms.ComboBox Localidad;
        private System.Windows.Forms.Label label22;
    }
}
