﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class CartaPorteForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.cartaPorteControl1 = new Jaeger.UI.Forms.Comprobantes.CartaPorteControl();
            this.SuspendLayout();
            // 
            // cartaPorteControl1
            // 
            this.cartaPorteControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cartaPorteControl1.Location = new System.Drawing.Point(0, 0);
            this.cartaPorteControl1.Name = "cartaPorteControl1";
            this.cartaPorteControl1.Size = new System.Drawing.Size(709, 411);
            this.cartaPorteControl1.TabIndex = 0;
            // 
            // CartaPorteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 411);
            this.Controls.Add(this.cartaPorteControl1);
            this.Name = "CartaPorteForm";
            this.Text = "Carta Porte";
            this.Load += new System.EventHandler(this.CartaPorteForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CartaPorteControl cartaPorteControl1;
    }
}