﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class CartaPorteMercanciasTransporteAereoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.PermSCT = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.NumPermisoSCT = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.NombreAseg = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.NumPolizaSeguro = new System.Windows.Forms.TextBox();
            this.CodigoTransportista = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.MatriculaAeronave = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.NumeroGuia = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.NombreEmbarcador = new System.Windows.Forms.TextBox();
            this.LugarContrato = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ResidenciaFiscalEmbarc = new System.Windows.Forms.TextBox();
            this.RFCEmbarcador = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.NumRegIdTribEmbarc = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.PermSCT);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.NumPermisoSCT);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.NombreAseg);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.NumPolizaSeguro);
            this.groupBox1.Controls.Add(this.CodigoTransportista);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.MatriculaAeronave);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.NumeroGuia);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.NombreEmbarcador);
            this.groupBox1.Controls.Add(this.LugarContrato);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.ResidenciaFiscalEmbarc);
            this.groupBox1.Controls.Add(this.RFCEmbarcador);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.NumRegIdTribEmbarc);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(580, 450);
            this.groupBox1.TabIndex = 68;
            this.groupBox1.TabStop = false;
            // 
            // PermSCT
            // 
            this.PermSCT.FormattingEnabled = true;
            this.PermSCT.Location = new System.Drawing.Point(10, 37);
            this.PermSCT.Name = "PermSCT";
            this.PermSCT.Size = new System.Drawing.Size(100, 21);
            this.PermSCT.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "PermSCT";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(116, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "NumPermisoSCT";
            // 
            // NumPermisoSCT
            // 
            this.NumPermisoSCT.Location = new System.Drawing.Point(116, 37);
            this.NumPermisoSCT.Name = "NumPermisoSCT";
            this.NumPermisoSCT.Size = new System.Drawing.Size(100, 20);
            this.NumPermisoSCT.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(222, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "NombreAseg";
            // 
            // NombreAseg
            // 
            this.NombreAseg.Location = new System.Drawing.Point(222, 37);
            this.NombreAseg.Name = "NombreAseg";
            this.NombreAseg.Size = new System.Drawing.Size(247, 20);
            this.NombreAseg.TabIndex = 30;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(474, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "NumPolizaSeguro";
            // 
            // NumPolizaSeguro
            // 
            this.NumPolizaSeguro.Location = new System.Drawing.Point(474, 37);
            this.NumPolizaSeguro.Name = "NumPolizaSeguro";
            this.NumPolizaSeguro.Size = new System.Drawing.Size(100, 20);
            this.NumPolizaSeguro.TabIndex = 32;
            // 
            // CodigoTransportista
            // 
            this.CodigoTransportista.FormattingEnabled = true;
            this.CodigoTransportista.Location = new System.Drawing.Point(10, 77);
            this.CodigoTransportista.Name = "CodigoTransportista";
            this.CodigoTransportista.Size = new System.Drawing.Size(200, 21);
            this.CodigoTransportista.TabIndex = 34;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "CodigoTransportista";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(221, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 35;
            this.label4.Text = "MatriculaAeronave";
            // 
            // MatriculaAeronave
            // 
            this.MatriculaAeronave.Location = new System.Drawing.Point(218, 77);
            this.MatriculaAeronave.Name = "MatriculaAeronave";
            this.MatriculaAeronave.Size = new System.Drawing.Size(141, 20);
            this.MatriculaAeronave.TabIndex = 36;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(365, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 37;
            this.label5.Text = "NumeroGuia";
            // 
            // NumeroGuia
            // 
            this.NumeroGuia.Location = new System.Drawing.Point(365, 77);
            this.NumeroGuia.Name = "NumeroGuia";
            this.NumeroGuia.Size = new System.Drawing.Size(104, 20);
            this.NumeroGuia.TabIndex = 38;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(474, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 39;
            this.label6.Text = "LugarContrato";
            // 
            // NombreEmbarcador
            // 
            this.NombreEmbarcador.Location = new System.Drawing.Point(123, 118);
            this.NombreEmbarcador.Name = "NombreEmbarcador";
            this.NombreEmbarcador.Size = new System.Drawing.Size(220, 20);
            this.NombreEmbarcador.TabIndex = 52;
            // 
            // LugarContrato
            // 
            this.LugarContrato.Location = new System.Drawing.Point(477, 77);
            this.LugarContrato.Name = "LugarContrato";
            this.LugarContrato.Size = new System.Drawing.Size(97, 20);
            this.LugarContrato.TabIndex = 40;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(123, 102);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 13);
            this.label14.TabIndex = 51;
            this.label14.Text = "NombreEmbarcador";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 101);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 13);
            this.label8.TabIndex = 41;
            this.label8.Text = "RFCEmbarcador";
            // 
            // ResidenciaFiscalEmbarc
            // 
            this.ResidenciaFiscalEmbarc.Location = new System.Drawing.Point(474, 118);
            this.ResidenciaFiscalEmbarc.Name = "ResidenciaFiscalEmbarc";
            this.ResidenciaFiscalEmbarc.Size = new System.Drawing.Size(100, 20);
            this.ResidenciaFiscalEmbarc.TabIndex = 50;
            // 
            // RFCEmbarcador
            // 
            this.RFCEmbarcador.Location = new System.Drawing.Point(10, 118);
            this.RFCEmbarcador.Name = "RFCEmbarcador";
            this.RFCEmbarcador.Size = new System.Drawing.Size(107, 20);
            this.RFCEmbarcador.TabIndex = 42;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(474, 100);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(123, 13);
            this.label13.TabIndex = 49;
            this.label13.Text = "ResidenciaFiscalEmbarc";
            // 
            // NumRegIdTribEmbarc
            // 
            this.NumRegIdTribEmbarc.Location = new System.Drawing.Point(349, 118);
            this.NumRegIdTribEmbarc.Name = "NumRegIdTribEmbarc";
            this.NumRegIdTribEmbarc.Size = new System.Drawing.Size(120, 20);
            this.NumRegIdTribEmbarc.TabIndex = 46;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(346, 101);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(112, 13);
            this.label11.TabIndex = 45;
            this.label11.Text = "NumRegIdTribEmbarc";
            // 
            // CartaPorteMercanciasTransporteAereoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "CartaPorteMercanciasTransporteAereoControl";
            this.Size = new System.Drawing.Size(580, 450);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox PermSCT;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox NumPermisoSCT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NombreAseg;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox NumPolizaSeguro;
        private System.Windows.Forms.ComboBox CodigoTransportista;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox MatriculaAeronave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox NumeroGuia;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox NombreEmbarcador;
        private System.Windows.Forms.TextBox LugarContrato;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox ResidenciaFiscalEmbarc;
        private System.Windows.Forms.TextBox RFCEmbarcador;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox NumRegIdTribEmbarc;
        private System.Windows.Forms.Label label11;
    }
}
