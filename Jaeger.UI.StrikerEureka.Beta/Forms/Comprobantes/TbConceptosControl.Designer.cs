﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class TbConceptosControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.TConcepto = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.Agregar = new System.Windows.Forms.ToolStripButton();
            this.Nuevo = new System.Windows.Forms.ToolStripButton();
            this.Duplicar = new System.Windows.Forms.ToolStripButton();
            this.Remover = new System.Windows.Forms.ToolStripButton();
            this.Productos = new System.Windows.Forms.ToolStripButton();
            this.Unidad = new System.Windows.Forms.ToolStripButton();
            this.Complementos = new System.Windows.Forms.ToolStripDropDownButton();
            this.TConcepto.SuspendLayout();
            this.SuspendLayout();
            // 
            // TConcepto
            // 
            this.TConcepto.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator1,
            this.toolStripLabel2,
            this.toolStripComboBox1,
            this.Agregar,
            this.Nuevo,
            this.Duplicar,
            this.Remover,
            this.Productos,
            this.Unidad,
            this.Complementos});
            this.TConcepto.Location = new System.Drawing.Point(0, 0);
            this.TConcepto.Name = "TConcepto";
            this.TConcepto.Size = new System.Drawing.Size(1002, 25);
            this.TConcepto.TabIndex = 0;
            this.TConcepto.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(67, 22);
            this.toolStripLabel1.Text = "Conceptos:";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(74, 22);
            this.toolStripLabel2.Text = "Prod. / Serv.:";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 25);
            // 
            // Agregar
            // 
            this.Agregar.Image = global::Jaeger.UI.Properties.Resources.add_16px;
            this.Agregar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Agregar.Name = "Agregar";
            this.Agregar.Size = new System.Drawing.Size(69, 22);
            this.Agregar.Text = "Agregar";
            // 
            // Nuevo
            // 
            this.Nuevo.Image = global::Jaeger.UI.Properties.Resources.new_16px;
            this.Nuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Size = new System.Drawing.Size(62, 22);
            this.Nuevo.Text = "Nuevo";
            // 
            // Duplicar
            // 
            this.Duplicar.Image = global::Jaeger.UI.Properties.Resources.copy_16px;
            this.Duplicar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Duplicar.Name = "Duplicar";
            this.Duplicar.Size = new System.Drawing.Size(71, 22);
            this.Duplicar.Text = "Duplicar";
            // 
            // Remover
            // 
            this.Remover.Image = global::Jaeger.UI.Properties.Resources.delete_16px;
            this.Remover.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Remover.Name = "Remover";
            this.Remover.Size = new System.Drawing.Size(74, 22);
            this.Remover.Text = "Remover";
            // 
            // Productos
            // 
            this.Productos.Image = global::Jaeger.UI.Properties.Resources.product_16px;
            this.Productos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Productos.Name = "Productos";
            this.Productos.Size = new System.Drawing.Size(81, 22);
            this.Productos.Text = "Productos";
            // 
            // Unidad
            // 
            this.Unidad.Image = global::Jaeger.UI.Properties.Resources.cube_16px;
            this.Unidad.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Unidad.Name = "Unidad";
            this.Unidad.Size = new System.Drawing.Size(65, 22);
            this.Unidad.Text = "Unidad";
            // 
            // Complementos
            // 
            this.Complementos.Image = global::Jaeger.UI.Properties.Resources.plugin_16px;
            this.Complementos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Complementos.Name = "Complementos";
            this.Complementos.Size = new System.Drawing.Size(118, 22);
            this.Complementos.Text = "Complementos";
            // 
            // TbConceptosControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TConcepto);
            this.Name = "TbConceptosControl";
            this.Size = new System.Drawing.Size(1002, 25);
            this.TConcepto.ResumeLayout(false);
            this.TConcepto.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip TConcepto;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        protected internal System.Windows.Forms.ToolStripButton Agregar;
        protected internal System.Windows.Forms.ToolStripButton Nuevo;
        protected internal System.Windows.Forms.ToolStripButton Duplicar;
        protected internal System.Windows.Forms.ToolStripButton Remover;
        protected internal System.Windows.Forms.ToolStripButton Productos;
        protected internal System.Windows.Forms.ToolStripButton Unidad;
        protected internal System.Windows.Forms.ToolStripDropDownButton Complementos;
    }
}
