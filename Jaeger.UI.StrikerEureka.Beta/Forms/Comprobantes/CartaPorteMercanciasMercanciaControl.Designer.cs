﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class CartaPorteMercanciasMercanciaControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.BuscarUnidadPeso = new System.Windows.Forms.Button();
            this.DetalleMercanciaSpecified = new System.Windows.Forms.CheckBox();
            this.NumPiezas = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.UnidadPesoMerc = new System.Windows.Forms.ComboBox();
            this.PesoNeto = new System.Windows.Forms.NumericUpDown();
            this.PesoBruto = new System.Windows.Forms.NumericUpDown();
            this.PesoTara = new System.Windows.Forms.NumericUpDown();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gridCantidadTransporte = new System.Windows.Forms.DataGridView();
            this.Cantidad1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDOrigen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDDestino = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CvesTransporte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TCantidad = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.gridGuias = new System.Windows.Forms.DataGridView();
            this.NumeroGuiaIdentificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescripGuiaIdentificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PesoGuiaIdentificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TGuia = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.gridPedimentos = new System.Windows.Forms.DataGridView();
            this.Pedimentos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TPedimento = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BuscarMaterialPeligroso = new System.Windows.Forms.Button();
            this.BuscarEmbalaje = new System.Windows.Forms.Button();
            this.BuscarUnidad = new System.Windows.Forms.Button();
            this.BuscarProducto = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CveMaterialPeligroso = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ClaveSTCC = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.UUIDComercioExt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.FraccionArancelaria = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.DescripEmbalaje = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Embalaje = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Dimensiones = new System.Windows.Forms.TextBox();
            this.Moneda = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.ValorMercancia = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.PesoEnKg = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.MaterialPeligroso = new System.Windows.Forms.CheckBox();
            this.BienesTransp = new System.Windows.Forms.TextBox();
            this.Unidad = new System.Windows.Forms.TextBox();
            this.Descripcion = new System.Windows.Forms.TextBox();
            this.ClaveUnidad = new System.Windows.Forms.TextBox();
            this.Cantidad = new System.Windows.Forms.NumericUpDown();
            this.ProviderError = new System.Windows.Forms.ErrorProvider(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumPiezas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoNeto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoBruto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoTara)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCantidadTransporte)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridGuias)).BeginInit();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPedimentos)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ValorMercancia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoEnKg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cantidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProviderError)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 216);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(580, 195);
            this.tabControl1.TabIndex = 38;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.BuscarUnidadPeso);
            this.tabPage1.Controls.Add(this.DetalleMercanciaSpecified);
            this.tabPage1.Controls.Add(this.NumPiezas);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.UnidadPesoMerc);
            this.tabPage1.Controls.Add(this.PesoNeto);
            this.tabPage1.Controls.Add(this.PesoBruto);
            this.tabPage1.Controls.Add(this.PesoTara);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(572, 169);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Detalles";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // BuscarUnidadPeso
            // 
            this.BuscarUnidadPeso.Image = global::Jaeger.UI.Properties.Resources.search_16;
            this.BuscarUnidadPeso.Location = new System.Drawing.Point(405, 33);
            this.BuscarUnidadPeso.Name = "BuscarUnidadPeso";
            this.BuscarUnidadPeso.Size = new System.Drawing.Size(23, 23);
            this.BuscarUnidadPeso.TabIndex = 42;
            this.BuscarUnidadPeso.UseVisualStyleBackColor = true;
            this.BuscarUnidadPeso.Click += new System.EventHandler(this.BuscarUnidadPeso_Click);
            // 
            // DetalleMercanciaSpecified
            // 
            this.DetalleMercanciaSpecified.AutoSize = true;
            this.DetalleMercanciaSpecified.Location = new System.Drawing.Point(476, 14);
            this.DetalleMercanciaSpecified.Name = "DetalleMercanciaSpecified";
            this.DetalleMercanciaSpecified.Size = new System.Drawing.Size(88, 17);
            this.DetalleMercanciaSpecified.TabIndex = 41;
            this.DetalleMercanciaSpecified.Text = "Incluir detalle";
            this.DetalleMercanciaSpecified.UseVisualStyleBackColor = true;
            this.DetalleMercanciaSpecified.CheckedChanged += new System.EventHandler(this.DetalleMercanciaSpecified_CheckedChanged);
            // 
            // NumPiezas
            // 
            this.NumPiezas.Enabled = false;
            this.NumPiezas.Location = new System.Drawing.Point(350, 74);
            this.NumPiezas.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.NumPiezas.Name = "NumPiezas";
            this.NumPiezas.Size = new System.Drawing.Size(77, 20);
            this.NumPiezas.TabIndex = 40;
            this.NumPiezas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(347, 58);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 13);
            this.label17.TabIndex = 39;
            this.label17.Text = "Núm. de Piezas";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(123, 58);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(57, 13);
            this.label21.TabIndex = 31;
            this.label21.Text = "Peso Neto";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(8, 18);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(86, 13);
            this.label22.TabIndex = 32;
            this.label22.Text = "Unidad de Peso:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(237, 58);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 13);
            this.label23.TabIndex = 33;
            this.label23.Text = "Peso Tara";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(8, 58);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(59, 13);
            this.label24.TabIndex = 34;
            this.label24.Text = "Peso Bruto";
            // 
            // UnidadPesoMerc
            // 
            this.UnidadPesoMerc.Enabled = false;
            this.UnidadPesoMerc.FormattingEnabled = true;
            this.UnidadPesoMerc.Location = new System.Drawing.Point(8, 34);
            this.UnidadPesoMerc.Name = "UnidadPesoMerc";
            this.UnidadPesoMerc.Size = new System.Drawing.Size(395, 21);
            this.UnidadPesoMerc.TabIndex = 38;
            // 
            // PesoNeto
            // 
            this.PesoNeto.DecimalPlaces = 2;
            this.PesoNeto.Enabled = false;
            this.PesoNeto.Location = new System.Drawing.Point(123, 74);
            this.PesoNeto.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.PesoNeto.Name = "PesoNeto";
            this.PesoNeto.Size = new System.Drawing.Size(105, 20);
            this.PesoNeto.TabIndex = 37;
            this.PesoNeto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // PesoBruto
            // 
            this.PesoBruto.DecimalPlaces = 2;
            this.PesoBruto.Enabled = false;
            this.PesoBruto.Location = new System.Drawing.Point(8, 74);
            this.PesoBruto.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.PesoBruto.Name = "PesoBruto";
            this.PesoBruto.Size = new System.Drawing.Size(107, 20);
            this.PesoBruto.TabIndex = 36;
            this.PesoBruto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // PesoTara
            // 
            this.PesoTara.DecimalPlaces = 2;
            this.PesoTara.Enabled = false;
            this.PesoTara.Location = new System.Drawing.Point(237, 74);
            this.PesoTara.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.PesoTara.Name = "PesoTara";
            this.PesoTara.Size = new System.Drawing.Size(107, 20);
            this.PesoTara.TabIndex = 35;
            this.PesoTara.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gridCantidadTransporte);
            this.tabPage2.Controls.Add(this.TCantidad);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(572, 169);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Cantidad Transporte";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gridCantidadTransporte
            // 
            this.gridCantidadTransporte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridCantidadTransporte.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Cantidad1,
            this.IDOrigen,
            this.IDDestino,
            this.CvesTransporte});
            this.gridCantidadTransporte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCantidadTransporte.Location = new System.Drawing.Point(3, 28);
            this.gridCantidadTransporte.Name = "gridCantidadTransporte";
            this.gridCantidadTransporte.Size = new System.Drawing.Size(566, 138);
            this.gridCantidadTransporte.TabIndex = 35;
            // 
            // Cantidad1
            // 
            this.Cantidad1.DataPropertyName = "Cantidad";
            this.Cantidad1.HeaderText = "Cantidad";
            this.Cantidad1.Name = "Cantidad1";
            // 
            // IDOrigen
            // 
            this.IDOrigen.DataPropertyName = "IDOrigen";
            this.IDOrigen.HeaderText = "IDOrigen";
            this.IDOrigen.Name = "IDOrigen";
            // 
            // IDDestino
            // 
            this.IDDestino.DataPropertyName = "IDDestino";
            this.IDDestino.HeaderText = "IDDestino";
            this.IDDestino.Name = "IDDestino";
            // 
            // CvesTransporte
            // 
            this.CvesTransporte.DataPropertyName = "CvesTransporte";
            this.CvesTransporte.HeaderText = "CvesTransporte";
            this.CvesTransporte.Name = "CvesTransporte";
            // 
            // TCantidad
            // 
            this.TCantidad.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCantidad.Etiqueta = "";
            this.TCantidad.Location = new System.Drawing.Point(3, 3);
            this.TCantidad.Name = "TCantidad";
            this.TCantidad.ShowActualizar = false;
            this.TCantidad.ShowCerrar = false;
            this.TCantidad.ShowEditar = false;
            this.TCantidad.ShowGuardar = false;
            this.TCantidad.ShowHerramientas = false;
            this.TCantidad.ShowImprimir = false;
            this.TCantidad.ShowNuevo = false;
            this.TCantidad.ShowRemover = false;
            this.TCantidad.Size = new System.Drawing.Size(566, 25);
            this.TCantidad.TabIndex = 34;
            this.TCantidad.ButtonRemover_Click += new System.EventHandler<System.EventArgs>(this.TCantidad_Remover_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.gridGuias);
            this.tabPage3.Controls.Add(this.TGuia);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(572, 169);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Guías";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // gridGuias
            // 
            this.gridGuias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridGuias.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NumeroGuiaIdentificacion,
            this.DescripGuiaIdentificacion,
            this.PesoGuiaIdentificacion});
            this.gridGuias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridGuias.Location = new System.Drawing.Point(0, 25);
            this.gridGuias.Name = "gridGuias";
            this.gridGuias.Size = new System.Drawing.Size(572, 144);
            this.gridGuias.TabIndex = 37;
            // 
            // NumeroGuiaIdentificacion
            // 
            this.NumeroGuiaIdentificacion.DataPropertyName = "NumeroGuiaIdentificacion";
            this.NumeroGuiaIdentificacion.HeaderText = "Núm. de Guia";
            this.NumeroGuiaIdentificacion.MaxInputLength = 30;
            this.NumeroGuiaIdentificacion.Name = "NumeroGuiaIdentificacion";
            this.NumeroGuiaIdentificacion.ToolTipText = "Atributo requerido para expresar el número de guía de cada paquete que se encuent" +
    "ra asociado con el traslado de los bienes y/o mercancías en territorio nacional." +
    "";
            this.NumeroGuiaIdentificacion.Width = 150;
            // 
            // DescripGuiaIdentificacion
            // 
            this.DescripGuiaIdentificacion.DataPropertyName = "DescripGuiaIdentificacion";
            this.DescripGuiaIdentificacion.HeaderText = "Descripción";
            this.DescripGuiaIdentificacion.MaxInputLength = 1000;
            this.DescripGuiaIdentificacion.Name = "DescripGuiaIdentificacion";
            this.DescripGuiaIdentificacion.Width = 250;
            // 
            // PesoGuiaIdentificacion
            // 
            this.PesoGuiaIdentificacion.DataPropertyName = "PesoGuiaIdentificacion";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N3";
            dataGridViewCellStyle1.NullValue = null;
            this.PesoGuiaIdentificacion.DefaultCellStyle = dataGridViewCellStyle1;
            this.PesoGuiaIdentificacion.HeaderText = "Peso";
            this.PesoGuiaIdentificacion.Name = "PesoGuiaIdentificacion";
            // 
            // TGuia
            // 
            this.TGuia.Dock = System.Windows.Forms.DockStyle.Top;
            this.TGuia.Etiqueta = "";
            this.TGuia.Location = new System.Drawing.Point(0, 0);
            this.TGuia.Name = "TGuia";
            this.TGuia.ShowActualizar = false;
            this.TGuia.ShowCerrar = false;
            this.TGuia.ShowEditar = false;
            this.TGuia.ShowGuardar = false;
            this.TGuia.ShowHerramientas = false;
            this.TGuia.ShowImprimir = false;
            this.TGuia.ShowNuevo = false;
            this.TGuia.ShowRemover = false;
            this.TGuia.Size = new System.Drawing.Size(572, 25);
            this.TGuia.TabIndex = 36;
            this.TGuia.ButtonRemover_Click += new System.EventHandler<System.EventArgs>(this.TGuia_Remover_Click);
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.gridPedimentos);
            this.tabPage9.Controls.Add(this.TPedimento);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(572, 169);
            this.tabPage9.TabIndex = 3;
            this.tabPage9.Text = "Pedimentos";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // gridPedimentos
            // 
            this.gridPedimentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPedimentos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Pedimentos});
            this.gridPedimentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPedimentos.Location = new System.Drawing.Point(0, 25);
            this.gridPedimentos.Name = "gridPedimentos";
            this.gridPedimentos.Size = new System.Drawing.Size(572, 144);
            this.gridPedimentos.TabIndex = 37;
            // 
            // Pedimentos
            // 
            this.Pedimentos.DataPropertyName = "Pedimentos";
            this.Pedimentos.HeaderText = "Pedimentos";
            this.Pedimentos.Name = "Pedimentos";
            this.Pedimentos.Width = 200;
            // 
            // TPedimento
            // 
            this.TPedimento.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPedimento.Etiqueta = "";
            this.TPedimento.Location = new System.Drawing.Point(0, 0);
            this.TPedimento.Name = "TPedimento";
            this.TPedimento.ShowActualizar = false;
            this.TPedimento.ShowCerrar = false;
            this.TPedimento.ShowEditar = false;
            this.TPedimento.ShowGuardar = false;
            this.TPedimento.ShowHerramientas = false;
            this.TPedimento.ShowImprimir = false;
            this.TPedimento.ShowNuevo = false;
            this.TPedimento.ShowRemover = false;
            this.TPedimento.Size = new System.Drawing.Size(572, 25);
            this.TPedimento.TabIndex = 36;
            this.TPedimento.ButtonRemover_Click += new System.EventHandler<System.EventArgs>(this.TPedimento_Remover_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.BuscarMaterialPeligroso);
            this.groupBox1.Controls.Add(this.BuscarEmbalaje);
            this.groupBox1.Controls.Add(this.BuscarUnidad);
            this.groupBox1.Controls.Add(this.BuscarProducto);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.CveMaterialPeligroso);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.ClaveSTCC);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.UUIDComercioExt);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.FraccionArancelaria);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.DescripEmbalaje);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.Embalaje);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.Dimensiones);
            this.groupBox1.Controls.Add(this.Moneda);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.ValorMercancia);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.PesoEnKg);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.MaterialPeligroso);
            this.groupBox1.Controls.Add(this.BienesTransp);
            this.groupBox1.Controls.Add(this.Unidad);
            this.groupBox1.Controls.Add(this.Descripcion);
            this.groupBox1.Controls.Add(this.ClaveUnidad);
            this.groupBox1.Controls.Add(this.Cantidad);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(580, 216);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            // 
            // BuscarMaterialPeligroso
            // 
            this.BuscarMaterialPeligroso.Image = global::Jaeger.UI.Properties.Resources.search_16;
            this.BuscarMaterialPeligroso.Location = new System.Drawing.Point(316, 119);
            this.BuscarMaterialPeligroso.Name = "BuscarMaterialPeligroso";
            this.BuscarMaterialPeligroso.Size = new System.Drawing.Size(23, 23);
            this.BuscarMaterialPeligroso.TabIndex = 36;
            this.BuscarMaterialPeligroso.UseVisualStyleBackColor = true;
            this.BuscarMaterialPeligroso.Click += new System.EventHandler(this.BuscarMaterialPeligroso_Click);
            // 
            // BuscarEmbalaje
            // 
            this.BuscarEmbalaje.Image = global::Jaeger.UI.Properties.Resources.search_16;
            this.BuscarEmbalaje.Location = new System.Drawing.Point(316, 160);
            this.BuscarEmbalaje.Name = "BuscarEmbalaje";
            this.BuscarEmbalaje.Size = new System.Drawing.Size(23, 23);
            this.BuscarEmbalaje.TabIndex = 35;
            this.BuscarEmbalaje.UseVisualStyleBackColor = true;
            this.BuscarEmbalaje.Click += new System.EventHandler(this.BuscarEmbalaje_Click);
            // 
            // BuscarUnidad
            // 
            this.BuscarUnidad.Image = global::Jaeger.UI.Properties.Resources.search_16;
            this.BuscarUnidad.Location = new System.Drawing.Point(92, 72);
            this.BuscarUnidad.Name = "BuscarUnidad";
            this.BuscarUnidad.Size = new System.Drawing.Size(23, 23);
            this.BuscarUnidad.TabIndex = 34;
            this.BuscarUnidad.UseVisualStyleBackColor = true;
            this.BuscarUnidad.Click += new System.EventHandler(this.BuscarUnidad_Click);
            // 
            // BuscarProducto
            // 
            this.BuscarProducto.Image = global::Jaeger.UI.Properties.Resources.search_16;
            this.BuscarProducto.Location = new System.Drawing.Point(92, 30);
            this.BuscarProducto.Name = "BuscarProducto";
            this.BuscarProducto.Size = new System.Drawing.Size(23, 23);
            this.BuscarProducto.TabIndex = 33;
            this.BuscarProducto.UseVisualStyleBackColor = true;
            this.BuscarProducto.Click += new System.EventHandler(this.BuscarProducto_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bienes transportados*:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(119, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Descripción*:";
            // 
            // CveMaterialPeligroso
            // 
            this.CveMaterialPeligroso.Enabled = false;
            this.CveMaterialPeligroso.FormattingEnabled = true;
            this.CveMaterialPeligroso.Location = new System.Drawing.Point(46, 120);
            this.CveMaterialPeligroso.Name = "CveMaterialPeligroso";
            this.CveMaterialPeligroso.Size = new System.Drawing.Size(269, 21);
            this.CveMaterialPeligroso.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Clave unidad*:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 124);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "Clave:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(348, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Dimensiones:";
            // 
            // ClaveSTCC
            // 
            this.ClaveSTCC.Location = new System.Drawing.Point(461, 120);
            this.ClaveSTCC.MaxLength = 7;
            this.ClaveSTCC.Name = "ClaveSTCC";
            this.ClaveSTCC.Size = new System.Drawing.Size(107, 20);
            this.ClaveSTCC.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(232, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Embalaje:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(461, 104);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 13);
            this.label9.TabIndex = 29;
            this.label9.Text = "Clave STCC:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(234, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Peso Kg*:";
            // 
            // UUIDComercioExt
            // 
            this.UUIDComercioExt.Location = new System.Drawing.Point(112, 188);
            this.UUIDComercioExt.MaxLength = 36;
            this.UUIDComercioExt.Name = "UUIDComercioExt";
            this.UUIDComercioExt.Size = new System.Drawing.Size(227, 20);
            this.UUIDComercioExt.TabIndex = 28;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(461, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Moneda:";
            // 
            // FraccionArancelaria
            // 
            this.FraccionArancelaria.FormattingEnabled = true;
            this.FraccionArancelaria.Location = new System.Drawing.Point(6, 161);
            this.FraccionArancelaria.Name = "FraccionArancelaria";
            this.FraccionArancelaria.Size = new System.Drawing.Size(220, 21);
            this.FraccionArancelaria.TabIndex = 27;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 146);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Fracción arancelaria:";
            // 
            // DescripEmbalaje
            // 
            this.DescripEmbalaje.Location = new System.Drawing.Point(348, 161);
            this.DescripEmbalaje.MaxLength = 100;
            this.DescripEmbalaje.Name = "DescripEmbalaje";
            this.DescripEmbalaje.Size = new System.Drawing.Size(220, 20);
            this.DescripEmbalaje.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(461, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Cantidad*:";
            // 
            // Embalaje
            // 
            this.Embalaje.Location = new System.Drawing.Point(232, 161);
            this.Embalaje.Name = "Embalaje";
            this.Embalaje.Size = new System.Drawing.Size(83, 20);
            this.Embalaje.TabIndex = 25;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(119, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Unidad:";
            // 
            // Dimensiones
            // 
            this.Dimensiones.Location = new System.Drawing.Point(348, 120);
            this.Dimensiones.Name = "Dimensiones";
            this.Dimensiones.Size = new System.Drawing.Size(107, 20);
            this.Dimensiones.TabIndex = 24;
            // 
            // Moneda
            // 
            this.Moneda.FormattingEnabled = true;
            this.Moneda.Items.AddRange(new object[] {
            "MXN"});
            this.Moneda.Location = new System.Drawing.Point(461, 75);
            this.Moneda.Name = "Moneda";
            this.Moneda.Size = new System.Drawing.Size(107, 21);
            this.Moneda.TabIndex = 22;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(345, 146);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Descripción embalaje:";
            // 
            // ValorMercancia
            // 
            this.ValorMercancia.DecimalPlaces = 2;
            this.ValorMercancia.Location = new System.Drawing.Point(348, 75);
            this.ValorMercancia.Name = "ValorMercancia";
            this.ValorMercancia.Size = new System.Drawing.Size(107, 20);
            this.ValorMercancia.TabIndex = 21;
            this.ValorMercancia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(345, 59);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Valor mercancia:";
            // 
            // PesoEnKg
            // 
            this.PesoEnKg.DecimalPlaces = 3;
            this.PesoEnKg.Location = new System.Drawing.Point(234, 75);
            this.PesoEnKg.Name = "PesoEnKg";
            this.PesoEnKg.Size = new System.Drawing.Size(105, 20);
            this.PesoEnKg.TabIndex = 21;
            this.PesoEnKg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 191);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 13);
            this.label15.TabIndex = 14;
            this.label15.Text = "UUID comercio ext:";
            // 
            // MaterialPeligroso
            // 
            this.MaterialPeligroso.Location = new System.Drawing.Point(6, 100);
            this.MaterialPeligroso.Name = "MaterialPeligroso";
            this.MaterialPeligroso.Size = new System.Drawing.Size(109, 21);
            this.MaterialPeligroso.TabIndex = 20;
            this.MaterialPeligroso.Text = "Material peligroso";
            this.MaterialPeligroso.CheckedChanged += new System.EventHandler(this.MaterialPeligrosoSpecified_CheckedChanged);
            // 
            // BienesTransp
            // 
            this.BienesTransp.Location = new System.Drawing.Point(6, 32);
            this.BienesTransp.MaxLength = 8;
            this.BienesTransp.Name = "BienesTransp";
            this.BienesTransp.Size = new System.Drawing.Size(85, 20);
            this.BienesTransp.TabIndex = 15;
            this.BienesTransp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Unidad
            // 
            this.Unidad.Location = new System.Drawing.Point(119, 75);
            this.Unidad.MaxLength = 20;
            this.Unidad.Name = "Unidad";
            this.Unidad.Size = new System.Drawing.Size(107, 20);
            this.Unidad.TabIndex = 19;
            // 
            // Descripcion
            // 
            this.Descripcion.Location = new System.Drawing.Point(119, 32);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(337, 20);
            this.Descripcion.TabIndex = 16;
            // 
            // ClaveUnidad
            // 
            this.ClaveUnidad.Location = new System.Drawing.Point(6, 74);
            this.ClaveUnidad.Name = "ClaveUnidad";
            this.ClaveUnidad.Size = new System.Drawing.Size(85, 20);
            this.ClaveUnidad.TabIndex = 18;
            this.ClaveUnidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Cantidad
            // 
            this.Cantidad.DecimalPlaces = 6;
            this.Cantidad.Location = new System.Drawing.Point(461, 33);
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.Size = new System.Drawing.Size(107, 20);
            this.Cantidad.TabIndex = 17;
            this.Cantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ProviderError
            // 
            this.ProviderError.ContainerControl = this;
            // 
            // CartaPorteMercanciasMercanciaControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.Name = "CartaPorteMercanciasMercanciaControl";
            this.Size = new System.Drawing.Size(580, 414);
            this.Load += new System.EventHandler(this.CartaPorteMercanciaControl_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumPiezas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoNeto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoBruto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoTara)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCantidadTransporte)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridGuias)).EndInit();
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPedimentos)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ValorMercancia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoEnKg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cantidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProviderError)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad1;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDOrigen;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDDestino;
        private System.Windows.Forms.DataGridViewTextBoxColumn CvesTransporte;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pedimentos;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        protected internal System.Windows.Forms.NumericUpDown NumPiezas;
        protected internal System.Windows.Forms.ComboBox UnidadPesoMerc;
        protected internal System.Windows.Forms.NumericUpDown PesoNeto;
        protected internal System.Windows.Forms.NumericUpDown PesoBruto;
        protected internal System.Windows.Forms.NumericUpDown PesoTara;
        protected internal System.Windows.Forms.ComboBox CveMaterialPeligroso;
        protected internal System.Windows.Forms.TextBox ClaveSTCC;
        protected internal System.Windows.Forms.TextBox UUIDComercioExt;
        protected internal System.Windows.Forms.ComboBox FraccionArancelaria;
        protected internal System.Windows.Forms.TextBox DescripEmbalaje;
        protected internal System.Windows.Forms.TextBox Embalaje;
        protected internal System.Windows.Forms.TextBox Dimensiones;
        protected internal System.Windows.Forms.ComboBox Moneda;
        protected internal System.Windows.Forms.NumericUpDown ValorMercancia;
        protected internal System.Windows.Forms.NumericUpDown PesoEnKg;
        protected internal System.Windows.Forms.CheckBox MaterialPeligroso;
        protected internal System.Windows.Forms.TextBox BienesTransp;
        protected internal System.Windows.Forms.TextBox Unidad;
        protected internal System.Windows.Forms.TextBox Descripcion;
        protected internal System.Windows.Forms.TextBox ClaveUnidad;
        protected internal System.Windows.Forms.NumericUpDown Cantidad;
        protected internal System.Windows.Forms.DataGridView gridCantidadTransporte;
        protected internal System.Windows.Forms.DataGridView gridGuias;
        protected internal System.Windows.Forms.DataGridView gridPedimentos;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumeroGuiaIdentificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescripGuiaIdentificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn PesoGuiaIdentificacion;
        protected internal Common.Forms.ToolBarStandarControl TCantidad;
        protected internal Common.Forms.ToolBarStandarControl TGuia;
        protected internal Common.Forms.ToolBarStandarControl TPedimento;
        protected internal System.Windows.Forms.CheckBox DetalleMercanciaSpecified;
        private System.Windows.Forms.Button BuscarProducto;
        private System.Windows.Forms.Button BuscarUnidad;
        private System.Windows.Forms.Button BuscarEmbalaje;
        private System.Windows.Forms.Button BuscarMaterialPeligroso;
        private System.Windows.Forms.Button BuscarUnidadPeso;
        protected internal System.Windows.Forms.ErrorProvider ProviderError;
    }
}
