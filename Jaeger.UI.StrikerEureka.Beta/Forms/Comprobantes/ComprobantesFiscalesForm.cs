﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class ComprobantesFiscalesForm : Form {
        protected IComprobantesFiscalesService service;
        private CFDISubTipoEnum subTipo;
        protected internal ContextMenuStrip contextMenuStrip = new ContextMenuStrip();
        private BindingList<IComprobanteFiscalSingleModel> comprobanteFiscalSingles;
        private readonly ToolStripMenuItem bComprobante = new ToolStripMenuItem() { Text = "Comprobante" };
        private readonly ToolStripMenuItem bComprobanteCartaPorte = new ToolStripMenuItem() { Text = "Carta Porte" };
        private readonly ToolStripMenuItem bComprobanteRecepcionPago = new ToolStripMenuItem() { Text = "Recepción Pago" };
        private readonly ToolStripMenuItem bComprobanteNomina = new ToolStripMenuItem() { Text = "Recibo de Nómina" };
        private readonly ToolStripMenuItem CrearTablaRecepcionPago = new ToolStripMenuItem() { Text = "Crear tabla recepción de pago" };
        private readonly ToolStripMenuItem CrearTablaCartaPorte = new ToolStripMenuItem() { Text = "Crear tabla carta porte" };
        protected internal ToolStripMenuItem consultaStatus = new ToolStripMenuItem { Text = "Consulta Status SAT" };
        protected internal ToolStripMenuItem bSerializar = new ToolStripMenuItem { Text = "Serializar (Parche)" };

        public ComprobantesFiscalesForm(CFDISubTipoEnum subTipo) {
            InitializeComponent();
            this.subTipo = subTipo;
        }

        private void ComprobantesFiscalesForm_Load(object sender, EventArgs e) {
            this.gridData.DataGridCommon();
            this.service = new ComprobantesFiscalesService(this.subTipo);
            this.gridData.ContextMenuStrip = this.contextMenuStrip;
            this.bComprobante.Click += TComprobante_Nuevo_Click;
            this.bComprobanteCartaPorte.Click += TComprobante_CartaPorte_Click;
            this.bComprobanteRecepcionPago.Click += TComprobante_RecepcionPago_Click;
            this.bComprobanteNomina.Click += this.TComprobante_Nomina_Click;

            this.CrearTablaRecepcionPago.Click += this.TComprobante_CrearTablaRecepcionPago_Click;
            this.CrearTablaCartaPorte.Click += this.TComprobante_CrearTablaCartaPorte_Click;
            this.TComprobante.Nuevo.DropDownItems.Add(this.bComprobante);
            this.TComprobante.Nuevo.DropDownItems.Add(this.bComprobanteRecepcionPago);
            this.TComprobante.Nuevo.DropDownItems.Add(this.bComprobanteCartaPorte);
            this.TComprobante.Nuevo.DropDownItems.Add(this.bComprobanteNomina);

            this.consultaStatus.Click += this.ConsultaStatus_Click;
            this.contextMenuStrip.Items.Add(this.consultaStatus);
            this.TComprobante.Actualizar.Click += this.TComprobante_Actualizar_Click;
            this.TComprobante.Herramientas.DropDownItems.Add(this.CrearTablaRecepcionPago);
            this.TComprobante.Herramientas.DropDownItems.Add(this.CrearTablaCartaPorte);
            this.TComprobante.Herramientas.DropDownItems.Add(this.bSerializar);

            this.bSerializar.Click += BSerializar_Click;
        }

        private void BSerializar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.SerializarComplementoPagos)) {
                espera.Text = "Serializando complemento pagos";
                espera.ShowDialog();
            }
        }

        private void ConsultaStatus_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.GetStatus)) {
                espera.Text = "Consultando servicio SAT";
                espera.ShowDialog(this);
            }
            if (this.gridData.CurrentRow != null) {
                var _seleccionado = this.gridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (_seleccionado != null) {
                    MessageBox.Show(this, (string)_seleccionado.Tag, "Consulta CFDI", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        public virtual void TComprobante_Nomina_Click(object sender, EventArgs e) {
            var _nuevo = new ComprobanteFiscalNominaForm(null, this.subTipo) { MdiParent = this.ParentForm };
            _nuevo.Show();
        }

        public virtual void TComprobante_RecepcionPago_Click(object sender, EventArgs e) {
            var _nuevo = new ComprobanteFiscalRecepcionPagoForm(null, this.subTipo) { MdiParent = this.ParentForm };
            _nuevo.Show();
        }

        public virtual void TComprobante_CartaPorte_Click(object sender, EventArgs e) {
            var _nuevo = new ComprobanteFiscalCartaPorteForm(null, this.subTipo) { MdiParent = this.ParentForm };
            _nuevo.Show();
        }

        public virtual void TComprobante_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new ComprobanteFiscalForm(null, this.subTipo) { MdiParent = this.ParentForm };
            _nuevo.Show();
        }

        public virtual void TComprobante_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Consultar)) {
                espera.Text = "Properties.Resources.Message_Consulta";
                espera.ShowDialog(this);
                this.gridData.DataSource = this.comprobanteFiscalSingles;
            }
        }

        public virtual void TComprobante_Editar_Click(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                var _seleccionado = this.gridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (_seleccionado != null) {
                    var _cfd = new ComprobanteFiscalDetailModel { Id = _seleccionado.Id, Documento = _seleccionado.Documento, TipoComprobante = _seleccionado.TipoComprobante };
                    if (_cfd != null) {
                        if (_cfd.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Pagos) {
                            var _pago = new ComprobanteFiscalRecepcionPagoForm(_cfd, CFDISubTipoEnum.Emitido) { MdiParent = this.ParentForm };
                            _pago.Show();
                        } else if (_cfd.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Ingreso && _cfd.Documento == null) {
                            var _pago = new ComprobanteFiscalForm(_cfd, CFDISubTipoEnum.Emitido) { MdiParent = this.ParentForm };
                            _pago.Show();
                        } else if (_cfd.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Ingreso && _cfd.Documento == "porte") {
                            var _pago = new ComprobanteFiscalCartaPorteForm(_cfd, CFDISubTipoEnum.Emitido) { MdiParent = this.ParentForm };
                            _pago.Show();
                        }
                    }

                }
            }
            this.Tag = null;
        }

        public virtual void TComprobante_Cancelar_Click(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                var _seleccionado = this.gridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (_seleccionado != null) {
                    if (MessageBox.Show(this, "Estas seguro?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                        var _cancelar = new ComprobanteFiscalCancelarForm(_seleccionado);
                        _cancelar.StartPosition = FormStartPosition.CenterParent;
                        _cancelar.ShowDialog(this);
                    }
                }
            }
        }

        public virtual void TComprobante_CrearTablaCartaPorte_Click(object sender, EventArgs e) {
            if (MessageBox.Show(this, "¿Esta seguro de crear tabla correspondiente a Carta Porte?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                this.service.CrearTablaCartaPorte();
            }
        }

        public virtual void TComprobante_CrearTablaRecepcionPago_Click(object sender, EventArgs e) {
            if (MessageBox.Show(this, "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                this.service.CrearTablaRecepcionPago();
            }
        }

        private void Consultar() {
            //this.comprobanteFiscalSingles = this.service.GetList(this.TComprobante.GetEjercicio(), this.TComprobante.GetMes());
        }

        private void Cancelar() {

        }

        private void GetComprobante() {
            var _seleccionado = this.gridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
            if (_seleccionado != null) {
                this.Tag = this.service.GetComprobante(_seleccionado.Id);
            }
        }

        private void GetStatus() {
            //var d = new ValidaSAT.Service.SAT.Status();
            //if (this.gridData.CurrentRow != null) {
            //    var _seleccionado = this.gridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
            //    if (_seleccionado != null) {
            //        var s = d.GetStatus(_seleccionado.EmisorRFC, _seleccionado.ReceptorRFC, _seleccionado.Total, _seleccionado.IdDocumento);
            //        _seleccionado.Tag = s.ToString();
            //    }
            //}
        }

        private void SerializarComplementoPagos() {
            if (this.gridData.CurrentRow != null) {
                //var seleccionado = GetSelected();
                //this.service.Serializar(seleccionado);
                this.service.ParcheComplemento(this.TComprobante.GetEjercicio());
            }
        }
    }
}
