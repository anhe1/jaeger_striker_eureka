﻿using System;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Repositories;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class MaterialPeligrosoCatalogoBuscarForm : Form {
        protected IClaveMaterialPeligrosoCatalogo catalogo;
        private CveMaterialPeligroso seleccionado = null;
        public event EventHandler<CveMaterialPeligroso> Seleted;

        public void OnSelected(CveMaterialPeligroso e) {
            if (this.Seleted != null) {
                this.Seleted(this, e);
            }
        }
        public MaterialPeligrosoCatalogoBuscarForm() {
            InitializeComponent();
            this.catalogo = new MaterialPeligrosoCatalogo();
        }

        private void CatalogoBuscarForm_Load(object sender, EventArgs e) {
            this.TCatalogo.Actualizar.Image = Properties.Resources.add_16px;
            this.gridUnidades.DataGridCommon();
            this.TCatalogo.Imprimir.Text = "Agregar";
            this.TCatalogo.Actualizar.Text = "Buscar";
            this.catalogo.Load();
            this.gridUnidades.DataSource = this.catalogo.Items;
            this.TCatalogo.Buscar.TextChanged += Buscar_TextChanged;
        }

        private void Buscar_TextChanged(object sender, EventArgs e) {
            if (this.TCatalogo.Buscar.Text.Length > 3) {
                this.TCatalogo.Actualizar.PerformClick();
            }
        }

        private void TCatalogo_Actualizar_Click(object sender, EventArgs e) {
            this.gridUnidades.DataSource = this.catalogo.GetSearch(this.TCatalogo.Buscar.Text);
        }

        private void TCatalogo_Imprimir_Click(object sender, EventArgs e) {
            if (this.gridUnidades.CurrentRow != null) {
                this.seleccionado = this.gridUnidades.CurrentRow.DataBoundItem as CveMaterialPeligroso;
                if (seleccionado != null) {
                    this.OnSelected(this.seleccionado);
                    this.Close();
                }
            }
        }

        private void TCatalogo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
