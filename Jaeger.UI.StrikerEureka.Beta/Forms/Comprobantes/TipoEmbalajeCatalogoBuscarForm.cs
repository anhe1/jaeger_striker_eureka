﻿using System;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Repositories;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class TipoEmbalajeCatalogoBuscarForm : Form {
        protected IClaveTipoEmbalajeCatalogo catalogo;
        private CveTipoEmbalaje seleccionado = null;
        public event EventHandler<CveTipoEmbalaje> Seleted;

        public void OnSelected(CveTipoEmbalaje e) {
            if (this.Seleted != null) {
                this.Seleted(this, e);
            }
        }
        public TipoEmbalajeCatalogoBuscarForm() {
            InitializeComponent();
            this.catalogo = new TipoEmbalajeCatalogo();
        }

        private void TipoEmbalajeCatalogoBuscarForm_Load(object sender, EventArgs e) {
            this.gridCatalogo.DataGridCommon();
            this.TCatalogo.Imprimir.Image = Properties.Resources.add_16px;
            this.TCatalogo.Imprimir.Text = "Agregar";
            this.TCatalogo.Actualizar.Text = "Buscar";
            this.catalogo.Load();
            this.gridCatalogo.DataSource = this.catalogo.Items;
            this.TCatalogo.Buscar.TextChanged += Buscar_TextChanged;
        }

        private void Buscar_TextChanged(object sender, EventArgs e) {
            if (this.TCatalogo.Buscar.Text.Length > 3) {
                this.TCatalogo.Actualizar.PerformClick();
            }
        }

        private void TCatalogo_Actualizar_Click(object sender, EventArgs e) {
            this.gridCatalogo.DataSource = this.catalogo.GetSearch(this.TCatalogo.Buscar.Text);
        }

        private void TCatalogo_Imprimir_Click(object sender, EventArgs e) {
            if (this.gridCatalogo.CurrentRow != null) {
                this.seleccionado = this.gridCatalogo.CurrentRow.DataBoundItem as CveTipoEmbalaje;
                if (seleccionado != null) {
                    this.OnSelected(this.seleccionado);
                    this.Close();
                }
            }
        }

        private void TCatalogo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
