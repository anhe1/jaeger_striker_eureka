﻿using System;
using System.Windows.Forms;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.UI.Forms.Comprobantes {
    public class ComprobanteFiscalRecepcionPagoForm : ComprobanteFiscalForm {
        protected internal ComplementoPagoControl complementoPago = new ComplementoPagoControl() { Dock = DockStyle.Fill };
        protected internal TabPage _page = new TabPage() { Text = "Complemento: Recepción Pago", Padding = new Padding(3), UseVisualStyleBackColor = true };


        public ComprobanteFiscalRecepcionPagoForm(ComprobanteFiscalDetailModel model, CFDISubTipoEnum subTipo) : base(model, subTipo) {
            _page.Controls.Add(complementoPago);
            this.tabControls.TabPages.RemoveAt(0);
            this.tabControls.TabPages.Add(_page);
            this.Load += ComprobanteFiscalRecepcionPagoForm_Load;
        }

        private void ComprobanteFiscalRecepcionPagoForm_Load(object sender, EventArgs e) {
            this.Text = "Recepción de Pago";
            this.complementoPago.Start();
            this.ComprobanteControl.BindingCompleted += ComprobanteControl_BindingCompleted;
        }

        private void ComprobanteControl_BindingCompleted(object sender, EventArgs e) {
            if (this.ComprobanteControl.Comprobante.Id == 0) {
                if (this.ComprobanteControl.Comprobante.RecepcionPago == null) {
                    this.ComprobanteControl.Comprobante.RecepcionPago = new System.ComponentModel.BindingList<Domain.Comprobante.Entities.Complemento.Pagos.ComplementoPagoDetailModel>();
                    this.ComprobanteControl.Comprobante.RecepcionPago.Add(new Domain.Comprobante.Entities.Complemento.Pagos.ComplementoPagoDetailModel() { FechaPago = DateTime.Now });
                }
            }
            if (this.ComprobanteControl.Comprobante.RecepcionPago.Count == 0) {
                MessageBox.Show(this, "El comprobante no cuenta con un complemento de pago asociado.", "Complemento de Pago", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.ComprobanteControl.Comprobante.RecepcionPago = new System.ComponentModel.BindingList<Domain.Comprobante.Entities.Complemento.Pagos.ComplementoPagoDetailModel>();
                this.ComprobanteControl.Comprobante.RecepcionPago.Add(new Domain.Comprobante.Entities.Complemento.Pagos.ComplementoPagoDetailModel());
            }
            this.complementoPago.Pago = this.ComprobanteControl.Comprobante.RecepcionPago[0];
            this.complementoPago.CreateBinding();
        }
    }
}
