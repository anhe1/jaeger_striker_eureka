﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Catalogos;
using Jaeger.Aplication.Comprobante;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class ComprobanteGeneralControl : UserControl {
        protected IFolioYSeriesService empresaService;
        protected IMetodoPagoCatalogo catalogoMetodoPago = new MetodoPagoCatalogo();
        protected IFormaPagoCatalogo catalogoFormaPago = new FormaPagoCatalogo();
        protected IUsoCFDICatalogo catalogoUsoCfdi = new UsoCFDICatalogo();
        protected IExportacionCatalogo exportacionCatalogo = new ExportacionCatalogo();
        protected IRegimenesFiscalesCatalogo regimenesfiscales = new RegimenesFiscalesCatalogo();
        protected IContribuyenteService service;
        protected BindingList<ComprobanteContribuyenteModel> contribuyenteModels;

        public ComprobanteGeneralControl() {
            InitializeComponent();
        }

        private void ComprobanteGeneralControl_Load(object sender, EventArgs e) {
            // tipo de comprobantes
            this.TipoComprobante.DataSource = ComprobanteCommonService.GetTipoComprobantes();
            this.TipoComprobante.SelectedIndex = -1;
            this.LugarExpedicion.MaxLength = 5;
            this.DomicilioFiscal.MaxLength = 5;
            this.LugarExpedicion.TextBoxOnlyNumbers();
            this.DomicilioFiscal.TextBoxOnlyNumbers();
        }

        public void Start() {
            this.empresaService = new FolioYSeriesService();
            this.catalogoMetodoPago.Load();
            this.catalogoFormaPago.Load();
            this.catalogoUsoCfdi.Load();
            this.regimenesfiscales.Load();
            this.exportacionCatalogo.Load();

            this.Serie.DataSource = this.empresaService.GetSeries(true);
            this.Serie.DisplayMember = "Nombre";
            this.Serie.ValueMember = "Id";
            this.Serie.SelectedIndex = 0;
            this.Serie.SelectedValueChanged += this.Serie_SelectedValueChanged;

            // catalogo formas de pago
            this.FormaPago.DataSource = this.catalogoFormaPago.Items;
            this.FormaPago.SelectedIndex = -1;

            // catalogo metodos de pago
            this.MetodoPago.DataSource = this.catalogoMetodoPago.Items;
            this.MetodoPago.SelectedIndex = -1;

            // catalogo uso de cfdi
            this.UsoCFDI.DataSource = catalogoUsoCfdi.Items;
            this.UsoCFDI.SelectedIndex = -1;

            // regimen fiscal
            this.RegimenFiscal.DataSource = this.regimenesfiscales.Items;
            this.RegimenFiscal.SelectedIndex = -1;

            // exportacion
            this.Exportacion.DataSource = this.exportacionCatalogo.Items;
            this.Exportacion.SelectedIndex = -1;

            this.Moneda.Items.Add("MXN");
            this.Moneda.Items.Add("USD");
        }

        private void Serie_SelectedValueChanged(object sender, EventArgs e) {
            var _seleccionado = this.Serie.SelectedItem as SerieFolioModel;
            if (_seleccionado != null) {
                this.IdSerie.Text = _seleccionado.Id.ToString();
            }
        }

        public void LoadContribuyente(CFDISubTipoEnum subTipo) {
            this.service = new ContribuyenteService(subTipo);
            this.Receptor.Enabled = false;
            this.preparar_contribuyentes.RunWorkerAsync();
        }

        private void Preparar_DoWork(object sender, DoWorkEventArgs e) {
            this.catalogoMetodoPago.Load();
            this.catalogoFormaPago.Load();
            this.catalogoUsoCfdi.Load();
            this.regimenesfiscales.Load();
            this.exportacionCatalogo.Load();
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            // catalogo formas de pago
            this.FormaPago.DataSource = this.catalogoFormaPago.Items;
            this.FormaPago.SelectedIndex = -1;

            // catalogo metodos de pago
            this.MetodoPago.DataSource = this.catalogoMetodoPago.Items;
            this.MetodoPago.SelectedIndex = -1;

            // catalogo uso de cfdi
            this.UsoCFDI.DataSource = catalogoUsoCfdi.Items;
            this.UsoCFDI.SelectedIndex = -1;

            // regimen fiscal
            this.RegimenFiscal.ValueMember = "Clave";
            this.RegimenFiscal.DisplayMember = "Descriptor";
            this.RegimenFiscal.DataSource = this.regimenesfiscales.Items;
            this.RegimenFiscal.SelectedIndex = -1;

            // exportacion
            this.Exportacion.DataSource = this.exportacionCatalogo.Items;
            this.Exportacion.SelectedIndex = -1;
        }

        private void PrepararContribuyentes_DoWork(object sender, DoWorkEventArgs e) {
            this.buttonActualizarDirectorio.Enabled = false;
            this.Receptor.Enabled = false;
            this.contribuyenteModels = this.service.GetContribuyentes();
        }

        private void PrepararContribuyentes_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Receptor.SelectedIndex = -1;
            this.Receptor.SelectedValue = null;
            this.Receptor.Enabled = true;
            this.buttonActualizarDirectorio.Enabled = true;
            this.Receptor.Enabled = true;
        }

        private void Receptor_DropDown(object sender, EventArgs e) {
            this.Receptor.DataSource = this.contribuyenteModels;
        }

        private void Receptor_SelectedValueChanged(object sender, EventArgs e) {
            var _seleccionado = this.Receptor.SelectedItem as ComprobanteContribuyenteModel;
            if (_seleccionado != null) {
                this.ReceptorRFC.Text = _seleccionado.RFC;
                this.IdDirectorio.Text = _seleccionado.IdDirectorio.ToString();

                if (_seleccionado.DomicilioFiscal != null) {
                    this.DomicilioFiscal.Text = _seleccionado.DomicilioFiscal.ToString();
                }

                if (this.TipoComprobante.Text != "Pagos") {
                    if (_seleccionado.ClaveUsoCFDI == null) {
                        this.UsoCFDI.SelectedValue = "P01";
                    } else {
                        this.UsoCFDI.SelectedValue = _seleccionado.ClaveUsoCFDI.ToString();
                    }
                } else {
                    this.UsoCFDI.SelectedValue = "P01";
                }

                if (_seleccionado.RegimenFiscal != null) {
                    this.RegimenFiscal.Text = _seleccionado.RegimenFiscal.ToString();
                }
            } else {
                MessageBox.Show(this, "Properties.Resources.Message_Objeto_NoValido", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void DomicilioFiscal_TextChanged(object sender, EventArgs e) {
            this.providerError.Clear();
            if (!Domain.Base.Services.ValidacionService.CodigoPostal(this.DomicilioFiscal.Text)) {
                this.providerError.SetError(this.DomicilioFiscal, "No valido!");
            }
        }

        private void LugarExpedicion_TextChanged(object sender, EventArgs e) {
            this.providerError.Clear();
            if (!Domain.Base.Services.ValidacionService.CodigoPostal(this.LugarExpedicion.Text)) {
                this.providerError.SetError(this.LugarExpedicion, "No valido!");
            }
        }

        private void TipoComprobante_SelectedValueChanged(object sender, EventArgs e) {
            this.FormaPago.Enabled = true;
            this.MetodoPago.Enabled = true;
            this.Condiciones.Enabled = true;
            this.TipoComprobante.Enabled = true;
            this.Moneda.Enabled = true;
            this.TipoCambio.Enabled = true;
            this.UsoCFDI.Enabled = true;
            if (this.TipoComprobante.Text == "Pagos") {
                this.FormaPago.Enabled = false;
                this.MetodoPago.Enabled = false;
                this.Condiciones.Enabled = false;
                this.TipoComprobante.Enabled = false;
                this.Moneda.Enabled = false;
                this.TipoCambio.Enabled = false;
                this.UsoCFDI.Enabled = false;
            }
        }
    }
}
