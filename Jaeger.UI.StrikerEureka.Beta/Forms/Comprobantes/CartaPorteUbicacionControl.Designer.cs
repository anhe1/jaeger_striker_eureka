﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class CartaPorteUbicacionControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.gUbicacion = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TipoUbicacion = new System.Windows.Forms.ComboBox();
            this.TotalDistRec = new System.Windows.Forms.NumericUpDown();
            this.ResidenciaFiscalFigura = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.NumRegIdTrib = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.FechaSalida = new System.Windows.Forms.DateTimePicker();
            this.TipoEstacion = new System.Windows.Forms.ComboBox();
            this.NombreEstacion = new System.Windows.Forms.TextBox();
            this.NombreRemitenteDestinatario = new System.Windows.Forms.TextBox();
            this.NavegacionTrafico = new System.Windows.Forms.ComboBox();
            this.NumEstacion = new System.Windows.Forms.ComboBox();
            this.RFCRemitenteDestinatario = new System.Windows.Forms.TextBox();
            this.IDUbicacion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TUbicacion = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.CodigoPostal = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Calle = new System.Windows.Forms.TextBox();
            this.Pais = new System.Windows.Forms.ComboBox();
            this.NumeroExterior = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.Estado = new System.Windows.Forms.TextBox();
            this.NumeroInterior = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.Municipio = new System.Windows.Forms.TextBox();
            this.Colonia = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.Referencia = new System.Windows.Forms.TextBox();
            this.Localidad = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.providerError = new System.Windows.Forms.ErrorProvider(this.components);
            this.gUbicacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDistRec)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.providerError)).BeginInit();
            this.SuspendLayout();
            // 
            // gUbicacion
            // 
            this.gUbicacion.Controls.Add(this.label10);
            this.gUbicacion.Controls.Add(this.TipoUbicacion);
            this.gUbicacion.Controls.Add(this.TotalDistRec);
            this.gUbicacion.Controls.Add(this.ResidenciaFiscalFigura);
            this.gUbicacion.Controls.Add(this.label12);
            this.gUbicacion.Controls.Add(this.NumRegIdTrib);
            this.gUbicacion.Controls.Add(this.label11);
            this.gUbicacion.Controls.Add(this.FechaSalida);
            this.gUbicacion.Controls.Add(this.TipoEstacion);
            this.gUbicacion.Controls.Add(this.NombreEstacion);
            this.gUbicacion.Controls.Add(this.NombreRemitenteDestinatario);
            this.gUbicacion.Controls.Add(this.NavegacionTrafico);
            this.gUbicacion.Controls.Add(this.NumEstacion);
            this.gUbicacion.Controls.Add(this.RFCRemitenteDestinatario);
            this.gUbicacion.Controls.Add(this.IDUbicacion);
            this.gUbicacion.Controls.Add(this.label5);
            this.gUbicacion.Controls.Add(this.label8);
            this.gUbicacion.Controls.Add(this.label7);
            this.gUbicacion.Controls.Add(this.label9);
            this.gUbicacion.Controls.Add(this.label6);
            this.gUbicacion.Controls.Add(this.label4);
            this.gUbicacion.Controls.Add(this.label3);
            this.gUbicacion.Controls.Add(this.label2);
            this.gUbicacion.Controls.Add(this.label1);
            this.gUbicacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.gUbicacion.Location = new System.Drawing.Point(0, 25);
            this.gUbicacion.Name = "gUbicacion";
            this.gUbicacion.Size = new System.Drawing.Size(681, 193);
            this.gUbicacion.TabIndex = 0;
            this.gUbicacion.TabStop = false;
            this.gUbicacion.Text = "Generales";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Tipo de ubicación:*";
            // 
            // TipoUbicacion
            // 
            this.TipoUbicacion.BackColor = System.Drawing.SystemColors.Window;
            this.TipoUbicacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TipoUbicacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TipoUbicacion.FormattingEnabled = true;
            this.TipoUbicacion.Items.AddRange(new object[] {
            "Origen",
            "Destino"});
            this.TipoUbicacion.Location = new System.Drawing.Point(117, 18);
            this.TipoUbicacion.Name = "TipoUbicacion";
            this.TipoUbicacion.Size = new System.Drawing.Size(134, 21);
            this.TipoUbicacion.TabIndex = 12;
            // 
            // TotalDistRec
            // 
            this.TotalDistRec.Location = new System.Drawing.Point(547, 160);
            this.TotalDistRec.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.TotalDistRec.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.TotalDistRec.Name = "TotalDistRec";
            this.TotalDistRec.Size = new System.Drawing.Size(121, 20);
            this.TotalDistRec.TabIndex = 32;
            this.TotalDistRec.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TotalDistRec.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            // 
            // ResidenciaFiscalFigura
            // 
            this.ResidenciaFiscalFigura.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ResidenciaFiscalFigura.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ResidenciaFiscalFigura.Location = new System.Drawing.Point(547, 68);
            this.ResidenciaFiscalFigura.MaxLength = 40;
            this.ResidenciaFiscalFigura.Name = "ResidenciaFiscalFigura";
            this.ResidenciaFiscalFigura.Size = new System.Drawing.Size(121, 21);
            this.ResidenciaFiscalFigura.TabIndex = 31;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(545, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 13);
            this.label12.TabIndex = 30;
            this.label12.Text = "Residencia Fiscal";
            // 
            // NumRegIdTrib
            // 
            this.NumRegIdTrib.Location = new System.Drawing.Point(420, 68);
            this.NumRegIdTrib.Name = "NumRegIdTrib";
            this.NumRegIdTrib.Size = new System.Drawing.Size(121, 20);
            this.NumRegIdTrib.TabIndex = 29;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(418, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(114, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "Núm. Reg Id Tributario";
            // 
            // FechaSalida
            // 
            this.FechaSalida.CustomFormat = "dddd, dd/MMMM/yyyy hh:mm";
            this.FechaSalida.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FechaSalida.Location = new System.Drawing.Point(12, 160);
            this.FechaSalida.Name = "FechaSalida";
            this.FechaSalida.Size = new System.Drawing.Size(265, 20);
            this.FechaSalida.TabIndex = 17;
            // 
            // TipoEstacion
            // 
            this.TipoEstacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TipoEstacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TipoEstacion.Location = new System.Drawing.Point(283, 160);
            this.TipoEstacion.Name = "TipoEstacion";
            this.TipoEstacion.Size = new System.Drawing.Size(258, 21);
            this.TipoEstacion.TabIndex = 13;
            // 
            // NombreEstacion
            // 
            this.NombreEstacion.Location = new System.Drawing.Point(283, 113);
            this.NombreEstacion.MaxLength = 50;
            this.NombreEstacion.Name = "NombreEstacion";
            this.NombreEstacion.Size = new System.Drawing.Size(258, 20);
            this.NombreEstacion.TabIndex = 16;
            // 
            // NombreRemitenteDestinatario
            // 
            this.NombreRemitenteDestinatario.Location = new System.Drawing.Point(136, 68);
            this.NombreRemitenteDestinatario.MaxLength = 254;
            this.NombreRemitenteDestinatario.Name = "NombreRemitenteDestinatario";
            this.NombreRemitenteDestinatario.Size = new System.Drawing.Size(278, 20);
            this.NombreRemitenteDestinatario.TabIndex = 15;
            // 
            // NavegacionTrafico
            // 
            this.NavegacionTrafico.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.NavegacionTrafico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NavegacionTrafico.Items.AddRange(new object[] {
            "No Aplica",
            "Altura",
            "Cabotaje"});
            this.NavegacionTrafico.Location = new System.Drawing.Point(547, 113);
            this.NavegacionTrafico.Name = "NavegacionTrafico";
            this.NavegacionTrafico.Size = new System.Drawing.Size(121, 21);
            this.NavegacionTrafico.TabIndex = 12;
            // 
            // NumEstacion
            // 
            this.NumEstacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.NumEstacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NumEstacion.FormattingEnabled = true;
            this.NumEstacion.Location = new System.Drawing.Point(12, 113);
            this.NumEstacion.Name = "NumEstacion";
            this.NumEstacion.Size = new System.Drawing.Size(263, 21);
            this.NumEstacion.TabIndex = 11;
            // 
            // RFCRemitenteDestinatario
            // 
            this.RFCRemitenteDestinatario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.RFCRemitenteDestinatario.Location = new System.Drawing.Point(12, 68);
            this.RFCRemitenteDestinatario.MaxLength = 14;
            this.RFCRemitenteDestinatario.Name = "RFCRemitenteDestinatario";
            this.RFCRemitenteDestinatario.Size = new System.Drawing.Size(118, 20);
            this.RFCRemitenteDestinatario.TabIndex = 10;
            this.RFCRemitenteDestinatario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // IDUbicacion
            // 
            this.IDUbicacion.Location = new System.Drawing.Point(591, 19);
            this.IDUbicacion.MaxLength = 8;
            this.IDUbicacion.Name = "IDUbicacion";
            this.IDUbicacion.Size = new System.Drawing.Size(77, 20);
            this.IDUbicacion.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(280, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tipo estación:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(280, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Nombre estación:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(134, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Nombre remitente:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 140);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Fecha salida ó llegada:*";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(544, 144);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Distancia recorrida:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(544, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Navegación tráfico:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Núm. estación:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "RFC remitente:*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(513, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Ubicación:";
            // 
            // TUbicacion
            // 
            this.TUbicacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.TUbicacion.Etiqueta = "Ubicación:";
            this.TUbicacion.Location = new System.Drawing.Point(0, 0);
            this.TUbicacion.Name = "TUbicacion";
            this.TUbicacion.ShowActualizar = false;
            this.TUbicacion.ShowCerrar = true;
            this.TUbicacion.ShowEditar = false;
            this.TUbicacion.ShowGuardar = true;
            this.TUbicacion.ShowHerramientas = false;
            this.TUbicacion.ShowImprimir = false;
            this.TUbicacion.ShowNuevo = false;
            this.TUbicacion.ShowRemover = false;
            this.TUbicacion.Size = new System.Drawing.Size(681, 25);
            this.TUbicacion.TabIndex = 15;
            this.TUbicacion.ButtonGuardar_Click += new System.EventHandler<System.EventArgs>(this.TUbicacion_Guardar_Click);
            this.TUbicacion.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TUbicacion_Cerrar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.CodigoPostal);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.Calle);
            this.groupBox1.Controls.Add(this.Pais);
            this.groupBox1.Controls.Add(this.NumeroExterior);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.Estado);
            this.groupBox1.Controls.Add(this.NumeroInterior);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.Municipio);
            this.groupBox1.Controls.Add(this.Colonia);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.Referencia);
            this.groupBox1.Controls.Add(this.Localidad);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 218);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(681, 144);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Domicilio";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Calle:";
            // 
            // CodigoPostal
            // 
            this.CodigoPostal.Location = new System.Drawing.Point(539, 71);
            this.CodigoPostal.MaxLength = 12;
            this.CodigoPostal.Name = "CodigoPostal";
            this.CodigoPostal.Size = new System.Drawing.Size(100, 20);
            this.CodigoPostal.TabIndex = 19;
            this.CodigoPostal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(218, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Núm. Exterior:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(536, 55);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Codigo Postal:*";
            // 
            // Calle
            // 
            this.Calle.Location = new System.Drawing.Point(9, 32);
            this.Calle.MaxLength = 100;
            this.Calle.Name = "Calle";
            this.Calle.Size = new System.Drawing.Size(206, 20);
            this.Calle.TabIndex = 2;
            // 
            // Pais
            // 
            this.Pais.Items.AddRange(new object[] {
            "MEX"});
            this.Pais.Location = new System.Drawing.Point(433, 71);
            this.Pais.MaxLength = 30;
            this.Pais.Name = "Pais";
            this.Pais.Size = new System.Drawing.Size(100, 21);
            this.Pais.TabIndex = 17;
            // 
            // NumeroExterior
            // 
            this.NumeroExterior.Location = new System.Drawing.Point(221, 32);
            this.NumeroExterior.MaxLength = 55;
            this.NumeroExterior.Name = "NumeroExterior";
            this.NumeroExterior.Size = new System.Drawing.Size(100, 20);
            this.NumeroExterior.TabIndex = 3;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(430, 55);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "País:*";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(324, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Núm. Interior:";
            // 
            // Estado
            // 
            this.Estado.Location = new System.Drawing.Point(221, 71);
            this.Estado.MaxLength = 30;
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(206, 20);
            this.Estado.TabIndex = 15;
            // 
            // NumeroInterior
            // 
            this.NumeroInterior.Location = new System.Drawing.Point(327, 32);
            this.NumeroInterior.MaxLength = 55;
            this.NumeroInterior.Name = "NumeroInterior";
            this.NumeroInterior.Size = new System.Drawing.Size(100, 20);
            this.NumeroInterior.TabIndex = 5;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(220, 55);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 13);
            this.label18.TabIndex = 14;
            this.label18.Text = "Estado:*";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(430, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(45, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "Colonia:";
            // 
            // Municipio
            // 
            this.Municipio.Location = new System.Drawing.Point(9, 71);
            this.Municipio.MaxLength = 120;
            this.Municipio.Name = "Municipio";
            this.Municipio.Size = new System.Drawing.Size(206, 20);
            this.Municipio.TabIndex = 13;
            // 
            // Colonia
            // 
            this.Colonia.Location = new System.Drawing.Point(433, 32);
            this.Colonia.MaxLength = 120;
            this.Colonia.Name = "Colonia";
            this.Colonia.Size = new System.Drawing.Size(206, 20);
            this.Colonia.TabIndex = 7;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 55);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 13);
            this.label20.TabIndex = 12;
            this.label20.Text = "Municipio:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 94);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 13);
            this.label21.TabIndex = 8;
            this.label21.Text = "Localidad:";
            // 
            // Referencia
            // 
            this.Referencia.Location = new System.Drawing.Point(327, 110);
            this.Referencia.MaxLength = 250;
            this.Referencia.Name = "Referencia";
            this.Referencia.Size = new System.Drawing.Size(312, 20);
            this.Referencia.TabIndex = 11;
            // 
            // Localidad
            // 
            this.Localidad.Location = new System.Drawing.Point(9, 110);
            this.Localidad.MaxLength = 120;
            this.Localidad.Name = "Localidad";
            this.Localidad.Size = new System.Drawing.Size(312, 20);
            this.Localidad.TabIndex = 9;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(324, 94);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(62, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "Referencia:";
            // 
            // providerError
            // 
            this.providerError.ContainerControl = this;
            // 
            // CartaPorteUbicacionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 367);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gUbicacion);
            this.Controls.Add(this.TUbicacion);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CartaPorteUbicacionControl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ubicación";
            this.Load += new System.EventHandler(this.CartaPorteUbicacionControl_Load);
            this.gUbicacion.ResumeLayout(false);
            this.gUbicacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDistRec)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.providerError)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gUbicacion;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ErrorProvider providerError;
        protected internal System.Windows.Forms.DateTimePicker FechaSalida;
        protected internal System.Windows.Forms.TextBox NombreEstacion;
        protected internal System.Windows.Forms.TextBox NombreRemitenteDestinatario;
        protected internal System.Windows.Forms.ComboBox TipoEstacion;
        protected internal System.Windows.Forms.ComboBox NavegacionTrafico;
        protected internal System.Windows.Forms.ComboBox NumEstacion;
        protected internal System.Windows.Forms.TextBox RFCRemitenteDestinatario;
        protected internal System.Windows.Forms.TextBox IDUbicacion;
        protected internal System.Windows.Forms.ComboBox TipoUbicacion;
        protected internal System.Windows.Forms.TextBox NumRegIdTrib;
        protected internal System.Windows.Forms.ComboBox ResidenciaFiscalFigura;
        protected internal System.Windows.Forms.NumericUpDown TotalDistRec;
        protected internal Common.Forms.ToolBarStandarControl TUbicacion;
        protected internal System.Windows.Forms.TextBox CodigoPostal;
        protected internal System.Windows.Forms.TextBox Calle;
        protected internal System.Windows.Forms.ComboBox Pais;
        protected internal System.Windows.Forms.TextBox NumeroExterior;
        protected internal System.Windows.Forms.TextBox Estado;
        protected internal System.Windows.Forms.TextBox NumeroInterior;
        protected internal System.Windows.Forms.TextBox Municipio;
        protected internal System.Windows.Forms.TextBox Colonia;
        protected internal System.Windows.Forms.TextBox Referencia;
        protected internal System.Windows.Forms.TextBox Localidad;
    }
}
