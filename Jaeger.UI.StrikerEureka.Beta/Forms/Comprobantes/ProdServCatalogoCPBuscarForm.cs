﻿using System;
using System.Windows.Forms;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class ProdServCatalogoCPBuscarForm : Form {

        public void OnAgregar(ComprobanteConceptoDetailModel e) {
            if (this.Agregar != null) {
                this.Agregar(this, e);
            }
        }

        public event EventHandler<ComprobanteConceptoDetailModel> Agregar;

        public ProdServCatalogoCPBuscarForm() {
            InitializeComponent();
        }

        private void ProdServCatalogoCPBuscarForm_Load(object sender, EventArgs e) {
            this.CatalogoSAT.SetService();
            this.TCatalogo.Imprimir.Image = Properties.Resources.add_16px;
            this.TCatalogo.Imprimir.Text = "Agregar";
            this.TCatalogo.Actualizar.Text = "Buscar";
            this.TCatalogo.Buscar.TextChanged += Buscar_TextChanged;
            this.TCatalogo.Actualizar.Click += Actualizar_Click;
            this.TCatalogo.Imprimir.Click += Imprimir_Click;
        }

        private void Imprimir_Click(object sender, EventArgs e) {
            var seleccionado = this.CatalogoSAT.GetSelected();
            this.OnAgregar(seleccionado);
        }

        private void Actualizar_Click(object sender, EventArgs e) {
            this.CatalogoSAT.Search(this.TCatalogo.Buscar.Text);
        }

        private void Buscar_TextChanged(object sender, EventArgs e) {
            if (this.TCatalogo.Buscar.Text.Length > 0) { 
                this.TCatalogo.Actualizar.PerformClick();
            }
        }

        private void TCatalogo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
