﻿using System;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class CartaPorteMercanciasAutotransporteControl : UserControl {
        protected IClaveTipoPermisoCatalogo tipoPermisoCatalogo;
        protected IClaveConfigAutotransporteCatalogo configAutotransporteCatalogo;
        public CartaPorteMercanciasAutotransporteControl() {
            InitializeComponent();
        }

        private void CartaPorteMercanciasAutotransporteControl_Load(object sender, EventArgs e) {
            this.gridRemolques.DataGridCommon();
            this.gridRemolques.ReadOnly = false;

            this.TRemolque.ShowNuevo = true;
            this.TRemolque.ShowRemover = true;

            this.tipoPermisoCatalogo = new TipoPermisoCatalogo();
            this.tipoPermisoCatalogo.Load();

            this.configAutotransporteCatalogo = new ConfigAutotransporteCatalogo();
            this.configAutotransporteCatalogo.Load();

            this.PermSCT.DisplayMember = "Descriptor";
            this.PermSCT.ValueMember = "Clave";
            this.PermSCT.DataSource = this.tipoPermisoCatalogo.Items;

            this.ConfigVehicular.DisplayMember = "Descriptor";
            this.ConfigVehicular.ValueMember = "Clave";
            this.ConfigVehicular.DataSource = this.configAutotransporteCatalogo.Items;
        }

        private void TRemolque_Remover_Click(object sender, EventArgs e) {
            if (this.gridRemolques.CurrentRow != null) {
                this.gridRemolques.Rows.Remove(this.gridRemolques.CurrentRow);
            }
        }

        public virtual void Incluir_CheckedChanged(object sender, EventArgs e) {
            this.PermSCT.Enabled = this.Incluir.Checked;
            this.NumPermisoSCT.Enabled = this.Incluir.Checked;
            this.ConfigVehicular.Enabled = this.Incluir.Checked;
            this.PlacaVM.Enabled = this.Incluir.Checked;
            this.AnioModeloVM.Enabled = this.Incluir.Checked;

            this.AseguraRespCivil.Enabled = this.Incluir.Checked;
            this.PolizaRespCivil.Enabled = this.Incluir.Checked;
            this.AseguraMedAmbiente.Enabled = this.Incluir.Checked;
            this.PolizaMedAmbiente.Enabled = this.Incluir.Checked;
            this.AseguraCarga.Enabled = this.Incluir.Checked;
            this.PolizaCarga.Enabled = this.Incluir.Checked;
            this.PrimaSeguro.Enabled = this.Incluir.Checked;
        }

        private void PlacaVM_TextChanged(object sender, EventArgs e) {
            if (this.PlacaVM.Text.Length > 3) {
                if (!Domain.Base.Services.ValidacionService.RegexValid(@"[^(?!.*\s)-]{5,7}", this.PlacaVM.Text)) {
                    this.errorProvider1.SetError(this.PlacaVM, "Error");
                }
            }
        }
    }
}
