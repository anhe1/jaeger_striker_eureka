﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class ComprobanteFiscalForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.gridConceptos = new System.Windows.Forms.DataGridView();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CveUnidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoIdent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CveProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ObjImpuesto = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descuento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CtaPredial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TConceptos = new Jaeger.UI.Forms.Comprobantes.TbConceptosControl();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabConceptoParte = new System.Windows.Forms.TabPage();
            this.gridConceptoParte = new System.Windows.Forms.DataGridView();
            this.CantidadParte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnidadParte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoIdentParte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CveProductoParte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConceptoParte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitarioParte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImporteParte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TConceptoParte = new Jaeger.UI.Forms.Comprobantes.TbConceptosControl();
            this.tabConceptoImpuestos = new System.Windows.Forms.TabPage();
            this.gridConceptoImpuestos = new System.Windows.Forms.DataGridView();
            this.Tipo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Impuesto = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Base = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoFactor = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TasaCuota = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Importe1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TImpuesto = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.tabConceptoAduana = new System.Windows.Forms.TabPage();
            this.gridConceptoAduana = new System.Windows.Forms.DataGridView();
            this.NumeroPedimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TAduana = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.tabConceptoCuentaPredial = new System.Windows.Forms.TabPage();
            this.gridCuentaPredial = new System.Windows.Forms.DataGridView();
            this.Numero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TPredial = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Total = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.RetencionISR = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.RetencionIVA = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.TrasladoIEPS = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.TrasladoIVA = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.TDescuento = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.SubTotal = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControls = new System.Windows.Forms.TabControl();
            this.tabConceptos = new System.Windows.Forms.TabPage();
            this.ComprobanteControl = new Jaeger.UI.Forms.Comprobantes.ComprobanteFiscalControl();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabConceptoParte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptoParte)).BeginInit();
            this.tabConceptoImpuestos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptoImpuestos)).BeginInit();
            this.tabConceptoAduana.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptoAduana)).BeginInit();
            this.tabConceptoCuentaPredial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCuentaPredial)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionISR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TDescuento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubTotal)).BeginInit();
            this.tabControls.SuspendLayout();
            this.tabConceptos.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gridConceptos);
            this.splitContainer1.Panel1.Controls.Add(this.TConceptos);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(1263, 394);
            this.splitContainer1.SplitterDistance = 214;
            this.splitContainer1.TabIndex = 2;
            // 
            // gridConceptos
            // 
            this.gridConceptos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridConceptos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Cantidad,
            this.Unidad,
            this.CveUnidad,
            this.NoIdent,
            this.CveProducto,
            this.ObjImpuesto,
            this.Descripcion,
            this.Unitario,
            this.Descuento,
            this.CtaPredial,
            this.Importe});
            this.gridConceptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridConceptos.Location = new System.Drawing.Point(0, 25);
            this.gridConceptos.Name = "gridConceptos";
            this.gridConceptos.Size = new System.Drawing.Size(1263, 189);
            this.gridConceptos.TabIndex = 1;
            // 
            // Cantidad
            // 
            this.Cantidad.DataPropertyName = "Cantidad";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.Cantidad.DefaultCellStyle = dataGridViewCellStyle1;
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.Width = 75;
            // 
            // Unidad
            // 
            this.Unidad.DataPropertyName = "Unidad";
            this.Unidad.HeaderText = "Unidad";
            this.Unidad.Name = "Unidad";
            this.Unidad.Width = 75;
            // 
            // CveUnidad
            // 
            this.CveUnidad.DataPropertyName = "ClaveUnidad";
            this.CveUnidad.HeaderText = "Cve. Unidad";
            this.CveUnidad.Name = "CveUnidad";
            this.CveUnidad.Width = 75;
            // 
            // NoIdent
            // 
            this.NoIdent.DataPropertyName = "NoIdentificacion";
            this.NoIdent.HeaderText = "No. Ident.";
            this.NoIdent.Name = "NoIdent";
            this.NoIdent.Width = 75;
            // 
            // CveProducto
            // 
            this.CveProducto.DataPropertyName = "ClaveProdServ";
            this.CveProducto.HeaderText = "Cve. Producto";
            this.CveProducto.Name = "CveProducto";
            this.CveProducto.Width = 75;
            // 
            // ObjImpuesto
            // 
            this.ObjImpuesto.DataPropertyName = "ObjetoImp";
            this.ObjImpuesto.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.ObjImpuesto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ObjImpuesto.HeaderText = "Obj. Imp.";
            this.ObjImpuesto.Name = "ObjImpuesto";
            this.ObjImpuesto.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ObjImpuesto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Descripcion
            // 
            this.Descripcion.DataPropertyName = "Descripcion";
            this.Descripcion.HeaderText = "Descripción";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Width = 300;
            // 
            // Unitario
            // 
            this.Unitario.DataPropertyName = "ValorUnitario";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.Unitario.DefaultCellStyle = dataGridViewCellStyle2;
            this.Unitario.HeaderText = "Unitario";
            this.Unitario.Name = "Unitario";
            this.Unitario.Width = 75;
            // 
            // Descuento
            // 
            this.Descuento.DataPropertyName = "Descuento";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            this.Descuento.DefaultCellStyle = dataGridViewCellStyle3;
            this.Descuento.HeaderText = "Descuento";
            this.Descuento.Name = "Descuento";
            this.Descuento.Width = 75;
            // 
            // CtaPredial
            // 
            this.CtaPredial.DataPropertyName = "CtaPredial";
            this.CtaPredial.HeaderText = "Cta. Predial";
            this.CtaPredial.Name = "CtaPredial";
            // 
            // Importe
            // 
            this.Importe.DataPropertyName = "Importe";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            this.Importe.DefaultCellStyle = dataGridViewCellStyle4;
            this.Importe.HeaderText = "Importe";
            this.Importe.Name = "Importe";
            this.Importe.Width = 75;
            // 
            // TConceptos
            // 
            this.TConceptos.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConceptos.Location = new System.Drawing.Point(0, 0);
            this.TConceptos.Name = "TConceptos";
            this.TConceptos.Size = new System.Drawing.Size(1263, 25);
            this.TConceptos.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabConceptoParte);
            this.tabControl1.Controls.Add(this.tabConceptoImpuestos);
            this.tabControl1.Controls.Add(this.tabConceptoAduana);
            this.tabControl1.Controls.Add(this.tabConceptoCuentaPredial);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1041, 176);
            this.tabControl1.TabIndex = 1;
            // 
            // tabConceptoParte
            // 
            this.tabConceptoParte.Controls.Add(this.gridConceptoParte);
            this.tabConceptoParte.Controls.Add(this.TConceptoParte);
            this.tabConceptoParte.Location = new System.Drawing.Point(4, 22);
            this.tabConceptoParte.Name = "tabConceptoParte";
            this.tabConceptoParte.Padding = new System.Windows.Forms.Padding(3);
            this.tabConceptoParte.Size = new System.Drawing.Size(1033, 150);
            this.tabConceptoParte.TabIndex = 0;
            this.tabConceptoParte.Text = "Concepto: Parte";
            this.tabConceptoParte.UseVisualStyleBackColor = true;
            // 
            // gridConceptoParte
            // 
            this.gridConceptoParte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridConceptoParte.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CantidadParte,
            this.UnidadParte,
            this.NoIdentParte,
            this.CveProductoParte,
            this.ConceptoParte,
            this.UnitarioParte,
            this.ImporteParte});
            this.gridConceptoParte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridConceptoParte.Location = new System.Drawing.Point(3, 28);
            this.gridConceptoParte.Name = "gridConceptoParte";
            this.gridConceptoParte.Size = new System.Drawing.Size(1027, 119);
            this.gridConceptoParte.TabIndex = 2;
            // 
            // CantidadParte
            // 
            this.CantidadParte.DataPropertyName = "Cantidad";
            this.CantidadParte.HeaderText = "Cantidad";
            this.CantidadParte.Name = "CantidadParte";
            this.CantidadParte.Width = 75;
            // 
            // UnidadParte
            // 
            this.UnidadParte.DataPropertyName = "Unidad";
            this.UnidadParte.HeaderText = "Unidad";
            this.UnidadParte.Name = "UnidadParte";
            this.UnidadParte.Width = 75;
            // 
            // NoIdentParte
            // 
            this.NoIdentParte.DataPropertyName = "NoIdentificacion";
            this.NoIdentParte.HeaderText = "No. Ident.";
            this.NoIdentParte.Name = "NoIdentParte";
            this.NoIdentParte.Width = 75;
            // 
            // CveProductoParte
            // 
            this.CveProductoParte.DataPropertyName = "ClaveProdServ";
            this.CveProductoParte.HeaderText = "Cve. Producto";
            this.CveProductoParte.Name = "CveProductoParte";
            this.CveProductoParte.Width = 75;
            // 
            // ConceptoParte
            // 
            this.ConceptoParte.DataPropertyName = "Descripcion";
            this.ConceptoParte.HeaderText = "Concepto";
            this.ConceptoParte.Name = "ConceptoParte";
            this.ConceptoParte.Width = 300;
            // 
            // UnitarioParte
            // 
            this.UnitarioParte.DataPropertyName = "ValorUnitario";
            this.UnitarioParte.HeaderText = "Unitario";
            this.UnitarioParte.Name = "UnitarioParte";
            this.UnitarioParte.Width = 75;
            // 
            // ImporteParte
            // 
            this.ImporteParte.DataPropertyName = "Importe";
            this.ImporteParte.HeaderText = "Importe";
            this.ImporteParte.Name = "ImporteParte";
            this.ImporteParte.Width = 75;
            // 
            // TConceptoParte
            // 
            this.TConceptoParte.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConceptoParte.Location = new System.Drawing.Point(3, 3);
            this.TConceptoParte.Name = "TConceptoParte";
            this.TConceptoParte.Size = new System.Drawing.Size(1027, 25);
            this.TConceptoParte.TabIndex = 1;
            // 
            // tabConceptoImpuestos
            // 
            this.tabConceptoImpuestos.Controls.Add(this.gridConceptoImpuestos);
            this.tabConceptoImpuestos.Controls.Add(this.TImpuesto);
            this.tabConceptoImpuestos.Location = new System.Drawing.Point(4, 22);
            this.tabConceptoImpuestos.Name = "tabConceptoImpuestos";
            this.tabConceptoImpuestos.Padding = new System.Windows.Forms.Padding(3);
            this.tabConceptoImpuestos.Size = new System.Drawing.Size(1033, 165);
            this.tabConceptoImpuestos.TabIndex = 1;
            this.tabConceptoImpuestos.Text = "Concepto: Impuestos";
            this.tabConceptoImpuestos.UseVisualStyleBackColor = true;
            // 
            // gridConceptoImpuestos
            // 
            this.gridConceptoImpuestos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridConceptoImpuestos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Tipo,
            this.Impuesto,
            this.Base,
            this.TipoFactor,
            this.TasaCuota,
            this.Importe1});
            this.gridConceptoImpuestos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridConceptoImpuestos.Location = new System.Drawing.Point(3, 28);
            this.gridConceptoImpuestos.Name = "gridConceptoImpuestos";
            this.gridConceptoImpuestos.Size = new System.Drawing.Size(1027, 134);
            this.gridConceptoImpuestos.TabIndex = 1;
            // 
            // Tipo
            // 
            this.Tipo.DataPropertyName = "Tipo";
            this.Tipo.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.Tipo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Tipo.HeaderText = "Tipo";
            this.Tipo.Name = "Tipo";
            // 
            // Impuesto
            // 
            this.Impuesto.DataPropertyName = "Impuesto";
            this.Impuesto.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.Impuesto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Impuesto.HeaderText = "Impuesto";
            this.Impuesto.Name = "Impuesto";
            this.Impuesto.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Impuesto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Base
            // 
            this.Base.DataPropertyName = "Base";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N4";
            this.Base.DefaultCellStyle = dataGridViewCellStyle5;
            this.Base.HeaderText = "Base";
            this.Base.Name = "Base";
            // 
            // TipoFactor
            // 
            this.TipoFactor.DataPropertyName = "TipoFactor";
            this.TipoFactor.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.TipoFactor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TipoFactor.HeaderText = "Tipo Factor";
            this.TipoFactor.Name = "TipoFactor";
            this.TipoFactor.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TipoFactor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // TasaCuota
            // 
            this.TasaCuota.DataPropertyName = "TasaOCuota";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N4";
            this.TasaCuota.DefaultCellStyle = dataGridViewCellStyle6;
            this.TasaCuota.HeaderText = "Tasa ó Cuota";
            this.TasaCuota.Name = "TasaCuota";
            // 
            // Importe1
            // 
            this.Importe1.DataPropertyName = "Importe";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N4";
            dataGridViewCellStyle7.NullValue = null;
            this.Importe1.DefaultCellStyle = dataGridViewCellStyle7;
            this.Importe1.HeaderText = "Importe";
            this.Importe1.Name = "Importe1";
            // 
            // TImpuesto
            // 
            this.TImpuesto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TImpuesto.Etiqueta = "";
            this.TImpuesto.Location = new System.Drawing.Point(3, 3);
            this.TImpuesto.Name = "TImpuesto";
            this.TImpuesto.ShowActualizar = false;
            this.TImpuesto.ShowCerrar = false;
            this.TImpuesto.ShowEditar = false;
            this.TImpuesto.ShowGuardar = false;
            this.TImpuesto.ShowHerramientas = false;
            this.TImpuesto.ShowImprimir = false;
            this.TImpuesto.ShowNuevo = false;
            this.TImpuesto.ShowRemover = false;
            this.TImpuesto.Size = new System.Drawing.Size(1027, 25);
            this.TImpuesto.TabIndex = 0;
            // 
            // tabConceptoAduana
            // 
            this.tabConceptoAduana.Controls.Add(this.gridConceptoAduana);
            this.tabConceptoAduana.Controls.Add(this.TAduana);
            this.tabConceptoAduana.Location = new System.Drawing.Point(4, 22);
            this.tabConceptoAduana.Name = "tabConceptoAduana";
            this.tabConceptoAduana.Size = new System.Drawing.Size(1033, 165);
            this.tabConceptoAduana.TabIndex = 2;
            this.tabConceptoAduana.Text = "Concepto: Aduana";
            this.tabConceptoAduana.UseVisualStyleBackColor = true;
            // 
            // gridConceptoAduana
            // 
            this.gridConceptoAduana.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridConceptoAduana.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NumeroPedimento});
            this.gridConceptoAduana.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridConceptoAduana.Location = new System.Drawing.Point(0, 25);
            this.gridConceptoAduana.Name = "gridConceptoAduana";
            this.gridConceptoAduana.Size = new System.Drawing.Size(1033, 140);
            this.gridConceptoAduana.TabIndex = 1;
            // 
            // NumeroPedimento
            // 
            this.NumeroPedimento.HeaderText = "Núm. de Pedimento";
            this.NumeroPedimento.Name = "NumeroPedimento";
            this.NumeroPedimento.Width = 150;
            // 
            // TAduana
            // 
            this.TAduana.Dock = System.Windows.Forms.DockStyle.Top;
            this.TAduana.Etiqueta = "";
            this.TAduana.Location = new System.Drawing.Point(0, 0);
            this.TAduana.Name = "TAduana";
            this.TAduana.ShowActualizar = false;
            this.TAduana.ShowCerrar = false;
            this.TAduana.ShowEditar = false;
            this.TAduana.ShowGuardar = false;
            this.TAduana.ShowHerramientas = false;
            this.TAduana.ShowImprimir = false;
            this.TAduana.ShowNuevo = false;
            this.TAduana.ShowRemover = false;
            this.TAduana.Size = new System.Drawing.Size(1033, 25);
            this.TAduana.TabIndex = 0;
            // 
            // tabConceptoCuentaPredial
            // 
            this.tabConceptoCuentaPredial.Controls.Add(this.gridCuentaPredial);
            this.tabConceptoCuentaPredial.Controls.Add(this.TPredial);
            this.tabConceptoCuentaPredial.Location = new System.Drawing.Point(4, 22);
            this.tabConceptoCuentaPredial.Name = "tabConceptoCuentaPredial";
            this.tabConceptoCuentaPredial.Padding = new System.Windows.Forms.Padding(3);
            this.tabConceptoCuentaPredial.Size = new System.Drawing.Size(1033, 165);
            this.tabConceptoCuentaPredial.TabIndex = 3;
            this.tabConceptoCuentaPredial.Text = "Cuenta Predial";
            this.tabConceptoCuentaPredial.UseVisualStyleBackColor = true;
            // 
            // gridCuentaPredial
            // 
            this.gridCuentaPredial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridCuentaPredial.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Numero});
            this.gridCuentaPredial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCuentaPredial.Location = new System.Drawing.Point(3, 28);
            this.gridCuentaPredial.Name = "gridCuentaPredial";
            this.gridCuentaPredial.Size = new System.Drawing.Size(1027, 134);
            this.gridCuentaPredial.TabIndex = 1;
            // 
            // Numero
            // 
            this.Numero.DataPropertyName = "Numero";
            this.Numero.HeaderText = "Numero";
            this.Numero.Name = "Numero";
            // 
            // TPredial
            // 
            this.TPredial.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPredial.Etiqueta = "Cuenta Predial:";
            this.TPredial.Location = new System.Drawing.Point(3, 3);
            this.TPredial.Name = "TPredial";
            this.TPredial.ShowActualizar = false;
            this.TPredial.ShowCerrar = false;
            this.TPredial.ShowEditar = false;
            this.TPredial.ShowGuardar = false;
            this.TPredial.ShowHerramientas = false;
            this.TPredial.ShowImprimir = false;
            this.TPredial.ShowNuevo = false;
            this.TPredial.ShowRemover = false;
            this.TPredial.Size = new System.Drawing.Size(1027, 25);
            this.TPredial.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Total);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.RetencionISR);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.RetencionIVA);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.TrasladoIEPS);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.TrasladoIVA);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.TDescuento);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.SubTotal);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(1041, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(222, 176);
            this.panel1.TabIndex = 0;
            // 
            // Total
            // 
            this.Total.DecimalPlaces = 4;
            this.Total.Location = new System.Drawing.Point(96, 146);
            this.Total.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.Size = new System.Drawing.Size(114, 20);
            this.Total.TabIndex = 13;
            this.Total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Total:";
            // 
            // RetencionISR
            // 
            this.RetencionISR.DecimalPlaces = 4;
            this.RetencionISR.Location = new System.Drawing.Point(96, 124);
            this.RetencionISR.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.RetencionISR.Name = "RetencionISR";
            this.RetencionISR.ReadOnly = true;
            this.RetencionISR.Size = new System.Drawing.Size(114, 20);
            this.RetencionISR.TabIndex = 11;
            this.RetencionISR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Retención ISR";
            // 
            // RetencionIVA
            // 
            this.RetencionIVA.DecimalPlaces = 4;
            this.RetencionIVA.Location = new System.Drawing.Point(96, 102);
            this.RetencionIVA.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.RetencionIVA.Name = "RetencionIVA";
            this.RetencionIVA.ReadOnly = true;
            this.RetencionIVA.Size = new System.Drawing.Size(114, 20);
            this.RetencionIVA.TabIndex = 9;
            this.RetencionIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Retención IVA";
            // 
            // TrasladoIEPS
            // 
            this.TrasladoIEPS.DecimalPlaces = 4;
            this.TrasladoIEPS.Location = new System.Drawing.Point(96, 80);
            this.TrasladoIEPS.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.TrasladoIEPS.Name = "TrasladoIEPS";
            this.TrasladoIEPS.ReadOnly = true;
            this.TrasladoIEPS.Size = new System.Drawing.Size(114, 20);
            this.TrasladoIEPS.TabIndex = 7;
            this.TrasladoIEPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "IEPS";
            // 
            // TrasladoIVA
            // 
            this.TrasladoIVA.DecimalPlaces = 4;
            this.TrasladoIVA.Location = new System.Drawing.Point(96, 58);
            this.TrasladoIVA.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.TrasladoIVA.Name = "TrasladoIVA";
            this.TrasladoIVA.ReadOnly = true;
            this.TrasladoIVA.Size = new System.Drawing.Size(114, 20);
            this.TrasladoIVA.TabIndex = 5;
            this.TrasladoIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "IVA";
            // 
            // TDescuento
            // 
            this.TDescuento.DecimalPlaces = 4;
            this.TDescuento.Location = new System.Drawing.Point(96, 36);
            this.TDescuento.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.TDescuento.Name = "TDescuento";
            this.TDescuento.ReadOnly = true;
            this.TDescuento.Size = new System.Drawing.Size(114, 20);
            this.TDescuento.TabIndex = 3;
            this.TDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Descuento:";
            // 
            // SubTotal
            // 
            this.SubTotal.DecimalPlaces = 4;
            this.SubTotal.Location = new System.Drawing.Point(96, 14);
            this.SubTotal.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.SubTotal.Name = "SubTotal";
            this.SubTotal.ReadOnly = true;
            this.SubTotal.Size = new System.Drawing.Size(114, 20);
            this.SubTotal.TabIndex = 1;
            this.SubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sub Total:";
            // 
            // tabControls
            // 
            this.tabControls.Controls.Add(this.tabConceptos);
            this.tabControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControls.Location = new System.Drawing.Point(0, 165);
            this.tabControls.Name = "tabControls";
            this.tabControls.SelectedIndex = 0;
            this.tabControls.Size = new System.Drawing.Size(1277, 426);
            this.tabControls.TabIndex = 2;
            // 
            // tabConceptos
            // 
            this.tabConceptos.Controls.Add(this.splitContainer1);
            this.tabConceptos.Location = new System.Drawing.Point(4, 22);
            this.tabConceptos.Name = "tabConceptos";
            this.tabConceptos.Padding = new System.Windows.Forms.Padding(3);
            this.tabConceptos.Size = new System.Drawing.Size(1269, 400);
            this.tabConceptos.TabIndex = 0;
            this.tabConceptos.Text = "Conceptos";
            this.tabConceptos.UseVisualStyleBackColor = true;
            // 
            // ComprobanteControl
            // 
            this.ComprobanteControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.ComprobanteControl.IdComprobante = 0;
            this.ComprobanteControl.Location = new System.Drawing.Point(0, 0);
            this.ComprobanteControl.Name = "ComprobanteControl";
            this.ComprobanteControl.Size = new System.Drawing.Size(1277, 165);
            this.ComprobanteControl.TabIndex = 1;
            this.ComprobanteControl.TipoComprobante = Jaeger.Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Ingreso;
            // 
            // ComprobanteFiscalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1277, 591);
            this.Controls.Add(this.tabControls);
            this.Controls.Add(this.ComprobanteControl);
            this.Name = "ComprobanteFiscalForm";
            this.Text = "Comprobante";
            this.Load += new System.EventHandler(this.ComprobanteFiscalForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabConceptoParte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptoParte)).EndInit();
            this.tabConceptoImpuestos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptoImpuestos)).EndInit();
            this.tabConceptoAduana.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptoAduana)).EndInit();
            this.tabConceptoCuentaPredial.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCuentaPredial)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionISR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TDescuento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubTotal)).EndInit();
            this.tabControls.ResumeLayout(false);
            this.tabConceptos.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private TbConceptosControl TConceptos;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabConceptoParte;
        private System.Windows.Forms.TabPage tabConceptoImpuestos;
        protected internal System.Windows.Forms.DataGridView gridConceptos;
        private TbConceptosControl TConceptoParte;
        private Common.Forms.ToolBarStandarControl TImpuesto;
        private System.Windows.Forms.TabPage tabConceptoAduana;
        private Common.Forms.ToolBarStandarControl TAduana;
        private System.Windows.Forms.NumericUpDown Total;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown RetencionISR;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown RetencionIVA;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown TrasladoIEPS;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown TrasladoIVA;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown TDescuento;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown SubTotal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView gridConceptoParte;
        private System.Windows.Forms.TabPage tabConceptos;
        private System.Windows.Forms.DataGridView gridConceptoAduana;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumeroPedimento;
        protected internal System.Windows.Forms.TabControl tabControls;
        protected internal ComprobanteFiscalControl ComprobanteControl;
        protected internal System.Windows.Forms.DataGridView gridConceptoImpuestos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn CveUnidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoIdent;
        private System.Windows.Forms.DataGridViewTextBoxColumn CveProducto;
        private System.Windows.Forms.DataGridViewComboBoxColumn ObjImpuesto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descuento;
        private System.Windows.Forms.DataGridViewTextBoxColumn CtaPredial;
        private System.Windows.Forms.DataGridViewTextBoxColumn Importe;
        private System.Windows.Forms.DataGridViewComboBoxColumn Tipo;
        private System.Windows.Forms.DataGridViewComboBoxColumn Impuesto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Base;
        private System.Windows.Forms.DataGridViewComboBoxColumn TipoFactor;
        private System.Windows.Forms.DataGridViewTextBoxColumn TasaCuota;
        private System.Windows.Forms.DataGridViewTextBoxColumn Importe1;
        private System.Windows.Forms.DataGridViewTextBoxColumn CantidadParte;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnidadParte;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoIdentParte;
        private System.Windows.Forms.DataGridViewTextBoxColumn CveProductoParte;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConceptoParte;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitarioParte;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImporteParte;
        private System.Windows.Forms.TabPage tabConceptoCuentaPredial;
        private System.Windows.Forms.DataGridView gridCuentaPredial;
        private System.Windows.Forms.DataGridViewTextBoxColumn Numero;
        private Common.Forms.ToolBarStandarControl TPredial;
    }
}