﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Comprobante.Entities.Complemento;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class ComplementoNominaControl : UserControl, IComplementoControl {
        private bool _isEditable = false;
        private string _Caption = "Complemento: Nómina";
        private ComplementoNomina _Nomina;
        protected ITipoPercepcionCatalogo tipoPercepcionCatalogo;
        protected ITipoDeduccionCatalogo tipoDeduccionCatalogo;
        protected ITipoOtroPagoCatalogo tipoOtroPagoCatalogo;
        protected ITipoIncapacidadCatalogo incapacidadCatalogo;

        public ComplementoNominaControl() {
            InitializeComponent();
        }

        private void ComplementoNominaControl_Load(object sender, EventArgs e) {
            this.gridPercepciones.DataGridCommon();
            this.gridDeducciones.DataGridCommon();
            this.gridOtrosPagos.DataGridCommon();
            //this.gridIncapacidades.DataGridCommon();

            this.TIncapacidad.Nuevo.Click += this.TIncapacidadNuevo_Click;
        }

        private void TIncapacidadNuevo_Click(object sender, EventArgs e) {
            if (this._Nomina.Incapacidades == null) {
                this._Nomina.Incapacidades = new BindingList<ComplementoNominaIncapacidad>();
                this.gridIncapacidades.DataSource = this._Nomina.Incapacidades;
            }
            this._Nomina.Incapacidades.Add(new ComplementoNominaIncapacidad());
        }

        public ComplementoNomina Nomina {
            get { return _Nomina; }
            set { _Nomina = value; }
        }

        public bool IsEditable {
            get { return _isEditable; }
            set { _isEditable = value; }
        }

        public string Caption {
            get { return _Caption; }
            set { _Caption = value; }
        }

        public virtual void Start() {
            this.ReceptorControl.Start();
            this.TipoNomina.DisplayMember = "Descripcion";
            this.TipoNomina.ValueMember = "Id";
            this.TipoNomina.DataSource = ComprobanteCommonService.GetTipoNominas();

            this.tipoPercepcionCatalogo = new TipoPercepcionCatalogo();
            this.tipoPercepcionCatalogo.Load();

            this.TipoPercepcion.DisplayMember = "Clave";
            this.TipoPercepcion.ValueMember = "Clave";
            this.TipoPercepcion.DataSource = this.tipoPercepcionCatalogo.Items;

            this.tipoDeduccionCatalogo = new TipoDeduccionCatalogo();
            this.tipoDeduccionCatalogo.Load();
            this.TipoDeduccion.DisplayMember = "Clave";
            this.TipoDeduccion.ValueMember = "Clave";
            this.TipoDeduccion.DataSource = this.tipoDeduccionCatalogo.Items;

            this.tipoOtroPagoCatalogo = new TipoOtroPagoCatalogo();
            this.tipoOtroPagoCatalogo.Load();
            this.TipoOtroPago.DisplayMember = "Clave";
            this.TipoOtroPago.ValueMember = "Clave";
            this.TipoOtroPago.DataSource = this.tipoOtroPagoCatalogo.Items;

            this.incapacidadCatalogo = new TipoIncapacidadCatalogo();
            this.incapacidadCatalogo.Load();
            this.TipoIncapacidad.DisplayMember = "Clave";
            this.TipoIncapacidad.ValueMember = "Clave";
            this.TipoIncapacidad.DataSource = this.incapacidadCatalogo.Items;
        }

        public virtual bool IsValid() {
            return true;
        }

        public virtual void CreateBinding() {
            this.TipoNomina.DataBindings.Clear();
            this.TipoNomina.DataBindings.Add("SelectedValue", this._Nomina, "TipoNomina", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaPago.DataBindings.Clear();
            this.FechaPago.DataBindings.Add("Value", this._Nomina, "FechaPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaInicialPago.DataBindings.Clear();
            this.FechaInicialPago.DataBindings.Add("Value", this._Nomina, "FechaInicialPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaFinalPago.DataBindings.Clear();
            this.FechaFinalPago.DataBindings.Add("Value", this._Nomina, "FechaFinalPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumDiasPagados.DataBindings.Clear();
            this.NumDiasPagados.DataBindings.Add("Value", this._Nomina, "NumDiasPagados", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TotalPercepciones.DataBindings.Clear();
            this.TotalPercepciones.DataBindings.Add("Value", this._Nomina, "TotalPercepciones", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TotalDeducciones.DataBindings.Clear();
            this.TotalDeducciones.DataBindings.Add("Value", this._Nomina, "TotalDeducciones", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TotalOtrosPagos.DataBindings.Clear();
            this.TotalOtrosPagos.DataBindings.Add("Value", this._Nomina, "TotalOtrosPagos", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.NumEmpleado.DataBindings.Clear();
            this.ReceptorControl.NumEmpleado.DataBindings.Add("Text", this._Nomina.Receptor, "NumEmpleado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.CURP.DataBindings.Clear();
            this.ReceptorControl.CURP.DataBindings.Add("Text", this._Nomina.Receptor, "CURP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.NumSeguridadSocial.DataBindings.Clear();
            this.ReceptorControl.NumSeguridadSocial.DataBindings.Add("Text", this._Nomina.Receptor, "NumSeguridadSocial", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.FechaInicioRelLaboral.DataBindings.Clear();
            this.ReceptorControl.FechaInicioRelLaboral.DataBindings.Add("Value", this._Nomina.Receptor, "FechaInicioRelLaboral", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.Antiguedad.DataBindings.Clear();
            this.ReceptorControl.Antiguedad.DataBindings.Add("Text", this._Nomina.Receptor, "Antiguedad", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.TipoContrato.DataBindings.Clear();
            this.ReceptorControl.TipoContrato.DataBindings.Add("SelectedValue", this._Nomina.Receptor, "TipoContrato", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.Sindicalizado.DataBindings.Clear();
            this.ReceptorControl.Sindicalizado.DataBindings.Add("Checked", this._Nomina.Receptor, "Sindicalizado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.TipoJornada.DataBindings.Clear();
            this.ReceptorControl.TipoJornada.DataBindings.Add("SelectedValue", this._Nomina.Receptor, "TipoJornada", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.TipoRegimen.DataBindings.Clear();
            this.ReceptorControl.TipoRegimen.DataBindings.Add("SelectedValue", this._Nomina.Receptor, "TipoRegimen", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.Departamento.DataBindings.Clear();
            this.ReceptorControl.Departamento.DataBindings.Add("Text", this._Nomina.Receptor, "Departamento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.Puesto.DataBindings.Clear();
            this.ReceptorControl.Puesto.DataBindings.Add("Text", this._Nomina.Receptor, "Puesto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.RiesgoPuesto.DataBindings.Clear();
            this.ReceptorControl.RiesgoPuesto.DataBindings.Add("SelectedValue", this._Nomina.Receptor, "RiesgoPuesto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.PeriodicidadPago.DataBindings.Clear();
            this.ReceptorControl.PeriodicidadPago.DataBindings.Add("SelectedValue", this._Nomina.Receptor, "PeriodicidadPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.Banco.DataBindings.Clear();
            this.ReceptorControl.Banco.DataBindings.Add("SelectedValue", this._Nomina.Receptor, "Banco", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.CuentaBancaria.DataBindings.Clear();
            this.ReceptorControl.CuentaBancaria.DataBindings.Add("Text", this._Nomina.Receptor, "CuentaBancaria", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.ClaveEntFed.DataBindings.Clear();
            this.ReceptorControl.ClaveEntFed.DataBindings.Add("Text", this._Nomina.Receptor, "ClaveEntFed", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.SalarioBaseCotApor.DataBindings.Clear();
            this.ReceptorControl.SalarioBaseCotApor.DataBindings.Add("Value", this._Nomina.Receptor, "SalarioBaseCotApor", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ReceptorControl.SalarioDiarioIntegrado.DataBindings.Clear();
            this.ReceptorControl.SalarioDiarioIntegrado.DataBindings.Add("Value", this._Nomina.Receptor, "SalarioDiarioIntegrado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.gridPercepciones.DataSource = this._Nomina.Percepciones.Percepcion;
            this.TotalSueldos.DataBindings.Clear();
            this.TotalSueldos.DataBindings.Add("Value", this._Nomina.Percepciones, "TotalSueldos", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TotalSeparacionIndemnizacion.DataBindings.Clear();
            this.TotalSeparacionIndemnizacion.DataBindings.Add("Value", this._Nomina.Percepciones, "TotalSeparacionIndemnizacion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TotalJubilacionPensionRetiro.DataBindings.Clear();
            this.TotalJubilacionPensionRetiro.DataBindings.Add("Value", this._Nomina.Percepciones, "TotalJubilacionPensionRetiro", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TotalGravado.DataBindings.Clear();
            this.TotalGravado.DataBindings.Add("Value", this._Nomina.Percepciones, "TotalGravado", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TotalExento.DataBindings.Clear();
            this.TotalExento.DataBindings.Add("Value", this._Nomina.Percepciones, "TotalExento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.gridDeducciones.DataSource = this._Nomina.Deducciones.Deduccion;
            this.TotalOtrasDeducciones.DataBindings.Clear();
            this.TotalOtrasDeducciones.DataBindings.Add("Value", this._Nomina.Deducciones, "TotalOtrasDeducciones", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TotalImpuestosRetenidos.DataBindings.Clear();
            this.TotalImpuestosRetenidos.DataBindings.Add("Value", this._Nomina.Deducciones, "TotalImpuestosRetenidos", true, DataSourceUpdateMode.OnPropertyChanged);

            this.gridOtrosPagos.DataSource = this._Nomina.OtrosPagos;
            //this.SubsidioCausado.DataBindings.Clear();
            //this.SubsidioCausado.DataBindings.Add("Value", this._Nomina, "TotalImpuestosRetenidos", true, DataSourceUpdateMode.OnPropertyChanged);

            this.gridIncapacidades.DataSource = this._Nomina.Incapacidades;
        }
    }
}
