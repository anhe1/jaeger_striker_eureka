﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class CartaPorteMercanciasTransporteFerroviarioControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TipoDeServicio = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.NombreAseg = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.NumPolizaSeguro = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TipoDeTrafico = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.NumeroGuiaIdentificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescripGuiaIdentificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PesoGuiaIdentificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolBarStandarControl1 = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolBarStandarControl2 = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TipoDeServicio);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.NombreAseg);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.NumPolizaSeguro);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.TipoDeTrafico);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(580, 74);
            this.groupBox1.TabIndex = 68;
            this.groupBox1.TabStop = false;
            // 
            // TipoDeServicio
            // 
            this.TipoDeServicio.FormattingEnabled = true;
            this.TipoDeServicio.Location = new System.Drawing.Point(10, 37);
            this.TipoDeServicio.Name = "TipoDeServicio";
            this.TipoDeServicio.Size = new System.Drawing.Size(100, 21);
            this.TipoDeServicio.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "TipoDeServicio";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(222, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "NombreAseg";
            // 
            // NombreAseg
            // 
            this.NombreAseg.Location = new System.Drawing.Point(229, 37);
            this.NombreAseg.Name = "NombreAseg";
            this.NombreAseg.Size = new System.Drawing.Size(240, 20);
            this.NombreAseg.TabIndex = 30;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(474, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "NumPolizaSeguro";
            // 
            // NumPolizaSeguro
            // 
            this.NumPolizaSeguro.Location = new System.Drawing.Point(474, 37);
            this.NumPolizaSeguro.Name = "NumPolizaSeguro";
            this.NumPolizaSeguro.Size = new System.Drawing.Size(100, 20);
            this.NumPolizaSeguro.TabIndex = 32;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(116, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 13);
            this.label9.TabIndex = 43;
            this.label9.Text = "TipoDeTrafico";
            // 
            // TipoDeTrafico
            // 
            this.TipoDeTrafico.FormattingEnabled = true;
            this.TipoDeTrafico.Location = new System.Drawing.Point(116, 37);
            this.TipoDeTrafico.Name = "TipoDeTrafico";
            this.TipoDeTrafico.Size = new System.Drawing.Size(107, 21);
            this.TipoDeTrafico.TabIndex = 44;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 74);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(580, 376);
            this.tabControl1.TabIndex = 69;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView2);
            this.tabPage1.Controls.Add(this.toolBarStandarControl1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(572, 350);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "DerechosDePaso";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NumeroGuiaIdentificacion,
            this.DescripGuiaIdentificacion,
            this.PesoGuiaIdentificacion});
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 28);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(566, 319);
            this.dataGridView2.TabIndex = 39;
            // 
            // NumeroGuiaIdentificacion
            // 
            this.NumeroGuiaIdentificacion.DataPropertyName = "NumeroGuiaIdentificacion";
            this.NumeroGuiaIdentificacion.HeaderText = "NumeroGuiaIdentificacion";
            this.NumeroGuiaIdentificacion.Name = "NumeroGuiaIdentificacion";
            // 
            // DescripGuiaIdentificacion
            // 
            this.DescripGuiaIdentificacion.DataPropertyName = "DescripGuiaIdentificacion";
            this.DescripGuiaIdentificacion.HeaderText = "DescripGuiaIdentificacion";
            this.DescripGuiaIdentificacion.Name = "DescripGuiaIdentificacion";
            // 
            // PesoGuiaIdentificacion
            // 
            this.PesoGuiaIdentificacion.DataPropertyName = "PesoGuiaIdentificacion";
            this.PesoGuiaIdentificacion.HeaderText = "PesoGuiaIdentificacion";
            this.PesoGuiaIdentificacion.Name = "PesoGuiaIdentificacion";
            // 
            // toolBarStandarControl1
            // 
            this.toolBarStandarControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBarStandarControl1.Etiqueta = "Mercancia";
            this.toolBarStandarControl1.Location = new System.Drawing.Point(3, 3);
            this.toolBarStandarControl1.Name = "toolBarStandarControl1";
            this.toolBarStandarControl1.ShowActualizar = false;
            this.toolBarStandarControl1.ShowCerrar = false;
            this.toolBarStandarControl1.ShowEditar = false;
            this.toolBarStandarControl1.ShowGuardar = false;
            this.toolBarStandarControl1.ShowHerramientas = false;
            this.toolBarStandarControl1.ShowImprimir = false;
            this.toolBarStandarControl1.ShowNuevo = false;
            this.toolBarStandarControl1.ShowRemover = false;
            this.toolBarStandarControl1.Size = new System.Drawing.Size(566, 25);
            this.toolBarStandarControl1.TabIndex = 38;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Controls.Add(this.toolBarStandarControl2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(572, 368);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Carro";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 28);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(566, 337);
            this.dataGridView1.TabIndex = 39;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NumeroGuiaIdentificacion";
            this.dataGridViewTextBoxColumn1.HeaderText = "NumeroGuiaIdentificacion";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "DescripGuiaIdentificacion";
            this.dataGridViewTextBoxColumn2.HeaderText = "DescripGuiaIdentificacion";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "PesoGuiaIdentificacion";
            this.dataGridViewTextBoxColumn3.HeaderText = "PesoGuiaIdentificacion";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // toolBarStandarControl2
            // 
            this.toolBarStandarControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBarStandarControl2.Etiqueta = "Mercancia";
            this.toolBarStandarControl2.Location = new System.Drawing.Point(3, 3);
            this.toolBarStandarControl2.Name = "toolBarStandarControl2";
            this.toolBarStandarControl2.ShowActualizar = false;
            this.toolBarStandarControl2.ShowCerrar = false;
            this.toolBarStandarControl2.ShowEditar = false;
            this.toolBarStandarControl2.ShowGuardar = false;
            this.toolBarStandarControl2.ShowHerramientas = false;
            this.toolBarStandarControl2.ShowImprimir = false;
            this.toolBarStandarControl2.ShowNuevo = false;
            this.toolBarStandarControl2.ShowRemover = false;
            this.toolBarStandarControl2.Size = new System.Drawing.Size(566, 25);
            this.toolBarStandarControl2.TabIndex = 38;
            // 
            // CartaPorteMercanciasTransporteFerroviarioControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.Name = "CartaPorteMercanciasTransporteFerroviarioControl";
            this.Size = new System.Drawing.Size(580, 450);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox TipoDeServicio;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NombreAseg;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox NumPolizaSeguro;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox TipoDeTrafico;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumeroGuiaIdentificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescripGuiaIdentificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn PesoGuiaIdentificacion;
        private Common.Forms.ToolBarStandarControl toolBarStandarControl1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private Common.Forms.ToolBarStandarControl toolBarStandarControl2;
    }
}
