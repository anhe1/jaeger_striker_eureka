﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class ProdServCatalogoCPSATBuscarControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.gridProductoSAT = new System.Windows.Forms.DataGridView();
            this.Clave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkerService = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductoSAT)).BeginInit();
            this.SuspendLayout();
            // 
            // gridProductoSAT
            // 
            this.gridProductoSAT.AllowUserToAddRows = false;
            this.gridProductoSAT.AllowUserToDeleteRows = false;
            this.gridProductoSAT.AllowUserToResizeRows = false;
            this.gridProductoSAT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridProductoSAT.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Clave,
            this.Descripcion});
            this.gridProductoSAT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridProductoSAT.Location = new System.Drawing.Point(0, 0);
            this.gridProductoSAT.Name = "gridProductoSAT";
            this.gridProductoSAT.RowHeadersVisible = false;
            this.gridProductoSAT.Size = new System.Drawing.Size(441, 197);
            this.gridProductoSAT.TabIndex = 0;
            this.gridProductoSAT.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridProductoSAT_CellDoubleClick);
            // 
            // Clave
            // 
            this.Clave.DataPropertyName = "Clave";
            this.Clave.HeaderText = "Clave";
            this.Clave.Name = "Clave";
            this.Clave.ReadOnly = true;
            // 
            // Descripcion
            // 
            this.Descripcion.DataPropertyName = "Descripcion";
            this.Descripcion.HeaderText = "Descripción";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Width = 250;
            // 
            // WorkerService
            // 
            this.WorkerService.DoWork += new System.ComponentModel.DoWorkEventHandler(this.WorkerService_DoWork);
            // 
            // ProdServCatalogoCPSATBuscarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridProductoSAT);
            this.Name = "ProdServCatalogoCPSATBuscarControl";
            this.Size = new System.Drawing.Size(441, 197);
            this.Load += new System.EventHandler(this.ProdServCatalogoSATBuscarControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridProductoSAT)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridProductoSAT;
        private System.ComponentModel.BackgroundWorker WorkerService;
        private System.Windows.Forms.DataGridViewTextBoxColumn Clave;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
    }
}
