﻿using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Forms.Comprobantes {
    public class ComprobantesFiscalesEmitidoForm : ComprobantesFiscalesForm {
        public ComprobantesFiscalesEmitidoForm() : base(CFDISubTipoEnum.Emitido) {
        }
    }
}
