﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class ComprobanteGeneralControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Serie = new System.Windows.Forms.ComboBox();
            this.Folio = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.FechaEmision = new System.Windows.Forms.DateTimePicker();
            this.FechaCertifica = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.TipoComprobante = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Receptor = new System.Windows.Forms.ComboBox();
            this.lblReceptor = new System.Windows.Forms.Label();
            this.ReceptorRFC = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.RegimenFiscal = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.LugarExpedicion = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.ResidenciaFiscal = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.NumRegIdTrib = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.DomicilioFiscal = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.UsoCFDI = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Exportacion = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.MetodoPago = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.FormaPago = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Condiciones = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.TipoCambio = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.Moneda = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.buttonActualizarDirectorio = new System.Windows.Forms.Button();
            this.Preparar = new System.ComponentModel.BackgroundWorker();
            this.preparar_contribuyentes = new System.ComponentModel.BackgroundWorker();
            this.IdDirectorio = new System.Windows.Forms.NumericUpDown();
            this.providerError = new System.Windows.Forms.ErrorProvider(this.components);
            this.IdSerie = new System.Windows.Forms.TextBox();
            this.lblVersion = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDirectorio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.providerError)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Serie:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(180, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Folio";
            // 
            // Serie
            // 
            this.Serie.FormattingEnabled = true;
            this.Serie.Location = new System.Drawing.Point(69, 6);
            this.Serie.Name = "Serie";
            this.Serie.Size = new System.Drawing.Size(91, 21);
            this.Serie.TabIndex = 2;
            // 
            // Folio
            // 
            this.Folio.Location = new System.Drawing.Point(215, 6);
            this.Folio.Name = "Folio";
            this.Folio.Size = new System.Drawing.Size(100, 20);
            this.Folio.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(321, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Fecha Emisión:";
            // 
            // FechaEmision
            // 
            this.FechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEmision.Location = new System.Drawing.Point(406, 6);
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.Size = new System.Drawing.Size(200, 20);
            this.FechaEmision.TabIndex = 4;
            // 
            // FechaCertifica
            // 
            this.FechaCertifica.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaCertifica.Enabled = false;
            this.FechaCertifica.Location = new System.Drawing.Point(719, 6);
            this.FechaCertifica.Name = "FechaCertifica";
            this.FechaCertifica.Size = new System.Drawing.Size(200, 20);
            this.FechaCertifica.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(612, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Fecha Certificación:";
            // 
            // TipoComprobante
            // 
            this.TipoComprobante.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TipoComprobante.DisplayMember = "Descripcion";
            this.TipoComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TipoComprobante.FormattingEnabled = true;
            this.TipoComprobante.Location = new System.Drawing.Point(1043, 6);
            this.TipoComprobante.Name = "TipoComprobante";
            this.TipoComprobante.Size = new System.Drawing.Size(121, 21);
            this.TipoComprobante.TabIndex = 8;
            this.TipoComprobante.ValueMember = "Clave";
            this.TipoComprobante.SelectedValueChanged += new System.EventHandler(this.TipoComprobante_SelectedValueChanged);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(930, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Tipo:";
            // 
            // Receptor
            // 
            this.Receptor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Receptor.DisplayMember = "Nombre";
            this.Receptor.FormattingEnabled = true;
            this.Receptor.Location = new System.Drawing.Point(68, 30);
            this.Receptor.Name = "Receptor";
            this.Receptor.Size = new System.Drawing.Size(332, 21);
            this.Receptor.TabIndex = 10;
            this.Receptor.DropDown += new System.EventHandler(this.Receptor_DropDown);
            this.Receptor.SelectedValueChanged += new System.EventHandler(this.Receptor_SelectedValueChanged);
            // 
            // lblReceptor
            // 
            this.lblReceptor.AutoSize = true;
            this.lblReceptor.Location = new System.Drawing.Point(8, 34);
            this.lblReceptor.Name = "lblReceptor";
            this.lblReceptor.Size = new System.Drawing.Size(54, 13);
            this.lblReceptor.TabIndex = 9;
            this.lblReceptor.Text = "Receptor:";
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ReceptorRFC.Location = new System.Drawing.Point(478, 30);
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.Size = new System.Drawing.Size(128, 20);
            this.ReceptorRFC.TabIndex = 12;
            this.ReceptorRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(442, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "RFC:";
            // 
            // RegimenFiscal
            // 
            this.RegimenFiscal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RegimenFiscal.DisplayMember = "Descriptor";
            this.RegimenFiscal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RegimenFiscal.FormattingEnabled = true;
            this.RegimenFiscal.Location = new System.Drawing.Point(719, 30);
            this.RegimenFiscal.Name = "RegimenFiscal";
            this.RegimenFiscal.Size = new System.Drawing.Size(200, 21);
            this.RegimenFiscal.TabIndex = 14;
            this.RegimenFiscal.ValueMember = "Clave";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(612, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Régimen Fiscal:";
            // 
            // LugarExpedicion
            // 
            this.LugarExpedicion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LugarExpedicion.Location = new System.Drawing.Point(1043, 30);
            this.LugarExpedicion.Name = "LugarExpedicion";
            this.LugarExpedicion.Size = new System.Drawing.Size(121, 20);
            this.LugarExpedicion.TabIndex = 16;
            this.LugarExpedicion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.LugarExpedicion.TextChanged += new System.EventHandler(this.LugarExpedicion_TextChanged);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(930, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Lugar Expedición:";
            // 
            // ResidenciaFiscal
            // 
            this.ResidenciaFiscal.FormattingEnabled = true;
            this.ResidenciaFiscal.Location = new System.Drawing.Point(103, 54);
            this.ResidenciaFiscal.Name = "ResidenciaFiscal";
            this.ResidenciaFiscal.Size = new System.Drawing.Size(114, 21);
            this.ResidenciaFiscal.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Residencia Fiscal:";
            // 
            // NumRegIdTrib
            // 
            this.NumRegIdTrib.Location = new System.Drawing.Point(314, 54);
            this.NumRegIdTrib.Name = "NumRegIdTrib";
            this.NumRegIdTrib.Size = new System.Drawing.Size(86, 20);
            this.NumRegIdTrib.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(223, 58);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "Núm. Reg. Trib.:";
            // 
            // DomicilioFiscal
            // 
            this.DomicilioFiscal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DomicilioFiscal.Location = new System.Drawing.Point(516, 54);
            this.DomicilioFiscal.Name = "DomicilioFiscal";
            this.DomicilioFiscal.Size = new System.Drawing.Size(90, 20);
            this.DomicilioFiscal.TabIndex = 22;
            this.DomicilioFiscal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.DomicilioFiscal.TextChanged += new System.EventHandler(this.DomicilioFiscal_TextChanged);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(428, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "Domicilio Fiscal:";
            // 
            // UsoCFDI
            // 
            this.UsoCFDI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.UsoCFDI.DisplayMember = "Descriptor";
            this.UsoCFDI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.UsoCFDI.FormattingEnabled = true;
            this.UsoCFDI.Location = new System.Drawing.Point(719, 54);
            this.UsoCFDI.Name = "UsoCFDI";
            this.UsoCFDI.Size = new System.Drawing.Size(200, 21);
            this.UsoCFDI.TabIndex = 24;
            this.UsoCFDI.ValueMember = "Clave";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(612, 58);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "Uso del CFDI:";
            // 
            // Exportacion
            // 
            this.Exportacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Exportacion.DisplayMember = "Descriptor";
            this.Exportacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Exportacion.FormattingEnabled = true;
            this.Exportacion.Location = new System.Drawing.Point(1043, 54);
            this.Exportacion.Name = "Exportacion";
            this.Exportacion.Size = new System.Drawing.Size(121, 21);
            this.Exportacion.TabIndex = 26;
            this.Exportacion.ValueMember = "Clave";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(930, 58);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "Exportación:";
            // 
            // MetodoPago
            // 
            this.MetodoPago.DisplayMember = "Descriptor";
            this.MetodoPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MetodoPago.FormattingEnabled = true;
            this.MetodoPago.Location = new System.Drawing.Point(103, 79);
            this.MetodoPago.Name = "MetodoPago";
            this.MetodoPago.Size = new System.Drawing.Size(212, 21);
            this.MetodoPago.TabIndex = 28;
            this.MetodoPago.ValueMember = "Clave";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 83);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(89, 13);
            this.label15.TabIndex = 27;
            this.label15.Text = "Método de Pago:";
            // 
            // FormaPago
            // 
            this.FormaPago.DisplayMember = "Descriptor";
            this.FormaPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FormaPago.FormattingEnabled = true;
            this.FormaPago.Location = new System.Drawing.Point(409, 79);
            this.FormaPago.Name = "FormaPago";
            this.FormaPago.Size = new System.Drawing.Size(197, 21);
            this.FormaPago.TabIndex = 30;
            this.FormaPago.ValueMember = "Clave";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(321, 83);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(82, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "Forma de Pago:";
            // 
            // Condiciones
            // 
            this.Condiciones.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Condiciones.FormattingEnabled = true;
            this.Condiciones.Location = new System.Drawing.Point(719, 79);
            this.Condiciones.Name = "Condiciones";
            this.Condiciones.Size = new System.Drawing.Size(200, 21);
            this.Condiciones.TabIndex = 32;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(612, 83);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 13);
            this.label17.TabIndex = 31;
            this.label17.Text = "Condiciones:";
            // 
            // TipoCambio
            // 
            this.TipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TipoCambio.Location = new System.Drawing.Point(994, 79);
            this.TipoCambio.Name = "TipoCambio";
            this.TipoCambio.Size = new System.Drawing.Size(40, 20);
            this.TipoCambio.TabIndex = 34;
            this.TipoCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(930, 83);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "T. Cambio:";
            // 
            // Moneda
            // 
            this.Moneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Moneda.FormattingEnabled = true;
            this.Moneda.Location = new System.Drawing.Point(1094, 79);
            this.Moneda.Name = "Moneda";
            this.Moneda.Size = new System.Drawing.Size(70, 21);
            this.Moneda.TabIndex = 36;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(1040, 83);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(49, 13);
            this.label19.TabIndex = 35;
            this.label19.Text = "Moneda:";
            // 
            // buttonActualizarDirectorio
            // 
            this.buttonActualizarDirectorio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonActualizarDirectorio.Image = global::Jaeger.UI.Properties.Resources.refresh_16px;
            this.buttonActualizarDirectorio.Location = new System.Drawing.Point(401, 30);
            this.buttonActualizarDirectorio.Name = "buttonActualizarDirectorio";
            this.buttonActualizarDirectorio.Size = new System.Drawing.Size(26, 20);
            this.buttonActualizarDirectorio.TabIndex = 37;
            this.buttonActualizarDirectorio.UseVisualStyleBackColor = true;
            // 
            // Preparar
            // 
            this.Preparar.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Preparar_DoWork);
            this.Preparar.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Preparar_RunWorkerCompleted);
            // 
            // preparar_contribuyentes
            // 
            this.preparar_contribuyentes.DoWork += new System.ComponentModel.DoWorkEventHandler(this.PrepararContribuyentes_DoWork);
            this.preparar_contribuyentes.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.PrepararContribuyentes_RunWorkerCompleted);
            // 
            // IdDirectorio
            // 
            this.IdDirectorio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdDirectorio.Location = new System.Drawing.Point(1187, 53);
            this.IdDirectorio.Name = "IdDirectorio";
            this.IdDirectorio.Size = new System.Drawing.Size(40, 20);
            this.IdDirectorio.TabIndex = 38;
            // 
            // providerError
            // 
            this.providerError.ContainerControl = this;
            // 
            // IdSerie
            // 
            this.IdSerie.Location = new System.Drawing.Point(161, 7);
            this.IdSerie.Name = "IdSerie";
            this.IdSerie.Size = new System.Drawing.Size(13, 20);
            this.IdSerie.TabIndex = 39;
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(1006, 10);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(31, 13);
            this.lblVersion.TabIndex = 40;
            this.lblVersion.Text = "Tipo:";
            // 
            // ComprobanteGeneralControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.IdSerie);
            this.Controls.Add(this.IdDirectorio);
            this.Controls.Add(this.buttonActualizarDirectorio);
            this.Controls.Add(this.Moneda);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.TipoCambio);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.Condiciones);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.FormaPago);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.MetodoPago);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Exportacion);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.UsoCFDI);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.DomicilioFiscal);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.NumRegIdTrib);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.ResidenciaFiscal);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.LugarExpedicion);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.RegimenFiscal);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.ReceptorRFC);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Receptor);
            this.Controls.Add(this.lblReceptor);
            this.Controls.Add(this.TipoComprobante);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.FechaCertifica);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.FechaEmision);
            this.Controls.Add(this.Folio);
            this.Controls.Add(this.Serie);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ComprobanteGeneralControl";
            this.Size = new System.Drawing.Size(1177, 107);
            this.Load += new System.EventHandler(this.ComprobanteGeneralControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDirectorio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.providerError)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button buttonActualizarDirectorio;
        protected internal System.Windows.Forms.ComboBox Serie;
        protected internal System.Windows.Forms.TextBox Folio;
        protected internal System.Windows.Forms.DateTimePicker FechaEmision;
        protected internal System.Windows.Forms.DateTimePicker FechaCertifica;
        protected internal System.Windows.Forms.ComboBox TipoComprobante;
        protected internal System.Windows.Forms.ComboBox Receptor;
        protected internal System.Windows.Forms.TextBox ReceptorRFC;
        protected internal System.Windows.Forms.ComboBox RegimenFiscal;
        protected internal System.Windows.Forms.ComboBox ResidenciaFiscal;
        protected internal System.Windows.Forms.TextBox NumRegIdTrib;
        protected internal System.Windows.Forms.TextBox DomicilioFiscal;
        protected internal System.Windows.Forms.ComboBox UsoCFDI;
        protected internal System.Windows.Forms.ComboBox Exportacion;
        protected internal System.Windows.Forms.ComboBox MetodoPago;
        protected internal System.Windows.Forms.ComboBox FormaPago;
        protected internal System.Windows.Forms.ComboBox Condiciones;
        protected internal System.Windows.Forms.NumericUpDown TipoCambio;
        protected internal System.Windows.Forms.ComboBox Moneda;
        protected internal System.ComponentModel.BackgroundWorker Preparar;
        private System.ComponentModel.BackgroundWorker preparar_contribuyentes;
        protected internal System.Windows.Forms.Label lblReceptor;
        protected internal System.Windows.Forms.NumericUpDown IdDirectorio;
        protected internal System.Windows.Forms.TextBox LugarExpedicion;
        private System.Windows.Forms.ErrorProvider providerError;
        protected internal System.Windows.Forms.TextBox IdSerie;
        protected internal System.Windows.Forms.Label lblVersion;
    }
}
