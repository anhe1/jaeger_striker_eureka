﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class ComplementoPagoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CadPago = new System.Windows.Forms.TextBox();
            this.SelloPago = new System.Windows.Forms.TextBox();
            this.CertPago = new System.Windows.Forms.TextBox();
            this.TipoCadPago = new System.Windows.Forms.ComboBox();
            this.CtaBeneficiario = new System.Windows.Forms.TextBox();
            this.RfcEmisorCtaBen = new System.Windows.Forms.TextBox();
            this.Moneda1 = new System.Windows.Forms.ComboBox();
            this.CtaOrdenante = new System.Windows.Forms.TextBox();
            this.NomBancoOrdExt = new System.Windows.Forms.TextBox();
            this.RfcEmisorCtaOrd = new System.Windows.Forms.TextBox();
            this.Monto = new System.Windows.Forms.NumericUpDown();
            this.TipoCambioP = new System.Windows.Forms.NumericUpDown();
            this.NumOperacion = new System.Windows.Forms.TextBox();
            this.FormaDePago = new System.Windows.Forms.ComboBox();
            this.FechaPago = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.gridDoctoRelacionados = new System.Windows.Forms.DataGridView();
            this.IdSubTipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Receptor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaEmision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Moneda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoCambio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FormaDePagoP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MetodoPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ObjetoImpDR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumParcialidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImpSaldoAnt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImpPagado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaldoInsoluto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TDocumentosRel = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gridImpuestosTraslado = new System.Windows.Forms.DataGridView();
            this.Tipo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Impuesto = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Base = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoFactor = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TasaCuota = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Importe1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TImpuesto = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Monto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambioP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDoctoRelacionados)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridImpuestosTraslado)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CadPago);
            this.groupBox1.Controls.Add(this.SelloPago);
            this.groupBox1.Controls.Add(this.CertPago);
            this.groupBox1.Controls.Add(this.TipoCadPago);
            this.groupBox1.Controls.Add(this.CtaBeneficiario);
            this.groupBox1.Controls.Add(this.RfcEmisorCtaBen);
            this.groupBox1.Controls.Add(this.Moneda1);
            this.groupBox1.Controls.Add(this.CtaOrdenante);
            this.groupBox1.Controls.Add(this.NomBancoOrdExt);
            this.groupBox1.Controls.Add(this.RfcEmisorCtaOrd);
            this.groupBox1.Controls.Add(this.Monto);
            this.groupBox1.Controls.Add(this.TipoCambioP);
            this.groupBox1.Controls.Add(this.NumOperacion);
            this.groupBox1.Controls.Add(this.FormaDePago);
            this.groupBox1.Controls.Add(this.FechaPago);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1186, 116);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // CadPago
            // 
            this.CadPago.Location = new System.Drawing.Point(978, 87);
            this.CadPago.MaxLength = 100;
            this.CadPago.Name = "CadPago";
            this.CadPago.Size = new System.Drawing.Size(195, 20);
            this.CadPago.TabIndex = 27;
            // 
            // SelloPago
            // 
            this.SelloPago.Location = new System.Drawing.Point(684, 87);
            this.SelloPago.MaxLength = 100;
            this.SelloPago.Name = "SelloPago";
            this.SelloPago.Size = new System.Drawing.Size(193, 20);
            this.SelloPago.TabIndex = 26;
            // 
            // CertPago
            // 
            this.CertPago.Location = new System.Drawing.Point(415, 87);
            this.CertPago.MaxLength = 100;
            this.CertPago.Name = "CertPago";
            this.CertPago.Size = new System.Drawing.Size(182, 20);
            this.CertPago.TabIndex = 25;
            // 
            // TipoCadPago
            // 
            this.TipoCadPago.FormattingEnabled = true;
            this.TipoCadPago.Location = new System.Drawing.Point(124, 87);
            this.TipoCadPago.Name = "TipoCadPago";
            this.TipoCadPago.Size = new System.Drawing.Size(178, 21);
            this.TipoCadPago.TabIndex = 24;
            // 
            // CtaBeneficiario
            // 
            this.CtaBeneficiario.Location = new System.Drawing.Point(415, 61);
            this.CtaBeneficiario.MaxLength = 50;
            this.CtaBeneficiario.Name = "CtaBeneficiario";
            this.CtaBeneficiario.Size = new System.Drawing.Size(182, 20);
            this.CtaBeneficiario.TabIndex = 23;
            // 
            // RfcEmisorCtaBen
            // 
            this.RfcEmisorCtaBen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.RfcEmisorCtaBen.Location = new System.Drawing.Point(172, 61);
            this.RfcEmisorCtaBen.MaxLength = 15;
            this.RfcEmisorCtaBen.Name = "RfcEmisorCtaBen";
            this.RfcEmisorCtaBen.Size = new System.Drawing.Size(130, 20);
            this.RfcEmisorCtaBen.TabIndex = 22;
            this.RfcEmisorCtaBen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Moneda1
            // 
            this.Moneda1.FormattingEnabled = true;
            this.Moneda1.Location = new System.Drawing.Point(1087, 35);
            this.Moneda1.Name = "Moneda1";
            this.Moneda1.Size = new System.Drawing.Size(86, 21);
            this.Moneda1.TabIndex = 21;
            // 
            // CtaOrdenante
            // 
            this.CtaOrdenante.Location = new System.Drawing.Point(773, 35);
            this.CtaOrdenante.MaxLength = 50;
            this.CtaOrdenante.Name = "CtaOrdenante";
            this.CtaOrdenante.Size = new System.Drawing.Size(246, 20);
            this.CtaOrdenante.TabIndex = 20;
            // 
            // NomBancoOrdExt
            // 
            this.NomBancoOrdExt.Location = new System.Drawing.Point(485, 35);
            this.NomBancoOrdExt.Name = "NomBancoOrdExt";
            this.NomBancoOrdExt.Size = new System.Drawing.Size(181, 20);
            this.NomBancoOrdExt.TabIndex = 19;
            // 
            // RfcEmisorCtaOrd
            // 
            this.RfcEmisorCtaOrd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.RfcEmisorCtaOrd.Location = new System.Drawing.Point(172, 35);
            this.RfcEmisorCtaOrd.MaxLength = 15;
            this.RfcEmisorCtaOrd.Name = "RfcEmisorCtaOrd";
            this.RfcEmisorCtaOrd.Size = new System.Drawing.Size(130, 20);
            this.RfcEmisorCtaOrd.TabIndex = 18;
            this.RfcEmisorCtaOrd.Text = "0123456789123450";
            this.RfcEmisorCtaOrd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Monto
            // 
            this.Monto.Location = new System.Drawing.Point(1087, 10);
            this.Monto.Name = "Monto";
            this.Monto.Size = new System.Drawing.Size(86, 20);
            this.Monto.TabIndex = 17;
            this.Monto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TipoCambioP
            // 
            this.TipoCambioP.Location = new System.Drawing.Point(963, 10);
            this.TipoCambioP.Name = "TipoCambioP";
            this.TipoCambioP.Size = new System.Drawing.Size(56, 20);
            this.TipoCambioP.TabIndex = 17;
            this.TipoCambioP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // NumOperacion
            // 
            this.NumOperacion.Location = new System.Drawing.Point(711, 10);
            this.NumOperacion.MaxLength = 100;
            this.NumOperacion.Name = "NumOperacion";
            this.NumOperacion.Size = new System.Drawing.Size(156, 20);
            this.NumOperacion.TabIndex = 17;
            // 
            // FormaDePago
            // 
            this.FormaDePago.DisplayMember = "Descriptor";
            this.FormaDePago.FormattingEnabled = true;
            this.FormaDePago.Location = new System.Drawing.Point(396, 10);
            this.FormaDePago.Name = "FormaDePago";
            this.FormaDePago.Size = new System.Drawing.Size(201, 21);
            this.FormaDePago.TabIndex = 17;
            this.FormaDePago.ValueMember = "Clave";
            // 
            // FechaPago
            // 
            this.FechaPago.CustomFormat = "dd/MM/yyyy hh:mm tt";
            this.FechaPago.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FechaPago.Location = new System.Drawing.Point(95, 10);
            this.FechaPago.Name = "FechaPago";
            this.FechaPago.Size = new System.Drawing.Size(207, 20);
            this.FechaPago.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Fecha de Pago:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(603, 91);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Sello de pago:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(883, 91);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 13);
            this.label16.TabIndex = 15;
            this.label16.Text = "Cadena de pago:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(308, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Forma de Pago:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(603, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Núm. de Operación:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(308, 91);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Certificado de pago:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(873, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Tipo de Cambio:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 91);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(112, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Tipo cadena de pago:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1032, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Monto:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(308, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Cuenta beneficiario:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(151, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "RFC Emisor de Cuenta Origen:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(160, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "RFC Emisor Cuenta Beneficiaria:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(308, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(171, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Nombre del Banco Ord. Extranjero:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1032, 39);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Moneda:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(672, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Cuenta ordenante:";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 116);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gridDoctoRelacionados);
            this.splitContainer1.Panel1.Controls.Add(this.TDocumentosRel);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(1186, 304);
            this.splitContainer1.SplitterDistance = 145;
            this.splitContainer1.TabIndex = 1;
            // 
            // gridDoctoRelacionados
            // 
            this.gridDoctoRelacionados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDoctoRelacionados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdSubTipo,
            this.IdDocumento,
            this.Serie,
            this.Folio,
            this.RFC,
            this.Receptor,
            this.FechaEmision,
            this.Moneda,
            this.TipoCambio,
            this.FormaDePagoP,
            this.MetodoPago,
            this.ObjetoImpDR,
            this.NumParcialidad,
            this.ImpSaldoAnt,
            this.ImpPagado,
            this.SaldoInsoluto});
            this.gridDoctoRelacionados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDoctoRelacionados.Location = new System.Drawing.Point(0, 25);
            this.gridDoctoRelacionados.Name = "gridDoctoRelacionados";
            this.gridDoctoRelacionados.Size = new System.Drawing.Size(1186, 120);
            this.gridDoctoRelacionados.TabIndex = 1;
            // 
            // IdSubTipo
            // 
            this.IdSubTipo.DataPropertyName = "IdSubTipo";
            this.IdSubTipo.HeaderText = "IdSubTipo";
            this.IdSubTipo.Name = "IdSubTipo";
            this.IdSubTipo.ReadOnly = true;
            // 
            // IdDocumento
            // 
            this.IdDocumento.DataPropertyName = "IdDocumento";
            this.IdDocumento.HeaderText = "IdDocumento";
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.ReadOnly = true;
            this.IdDocumento.Width = 220;
            // 
            // Serie
            // 
            this.Serie.DataPropertyName = "Serie";
            this.Serie.HeaderText = "Serie";
            this.Serie.Name = "Serie";
            this.Serie.ReadOnly = true;
            this.Serie.Width = 75;
            // 
            // Folio
            // 
            this.Folio.DataPropertyName = "Folio";
            this.Folio.HeaderText = "Folio";
            this.Folio.Name = "Folio";
            this.Folio.ReadOnly = true;
            // 
            // RFC
            // 
            this.RFC.DataPropertyName = "RFC";
            this.RFC.HeaderText = "RFC";
            this.RFC.Name = "RFC";
            this.RFC.ReadOnly = true;
            // 
            // Receptor
            // 
            this.Receptor.DataPropertyName = "Nombre";
            this.Receptor.HeaderText = "Receptor";
            this.Receptor.Name = "Receptor";
            this.Receptor.ReadOnly = true;
            // 
            // FechaEmision
            // 
            this.FechaEmision.DataPropertyName = "FechaEmision";
            this.FechaEmision.HeaderText = "Fecha Emisión";
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.ReadOnly = true;
            // 
            // Moneda
            // 
            this.Moneda.DataPropertyName = "MonedaDR";
            this.Moneda.HeaderText = "Moneda";
            this.Moneda.Name = "Moneda";
            this.Moneda.ReadOnly = true;
            // 
            // TipoCambio
            // 
            this.TipoCambio.DataPropertyName = "EquivalenciaDR";
            this.TipoCambio.HeaderText = "Tipo ";
            this.TipoCambio.Name = "TipoCambio";
            this.TipoCambio.ReadOnly = true;
            // 
            // FormaDePagoP
            // 
            this.FormaDePagoP.DataPropertyName = "FormaDePagoP";
            this.FormaDePagoP.HeaderText = "Forma de Pago";
            this.FormaDePagoP.Name = "FormaDePagoP";
            this.FormaDePagoP.ReadOnly = true;
            // 
            // MetodoPago
            // 
            this.MetodoPago.DataPropertyName = "MetodoPago";
            this.MetodoPago.HeaderText = "Método de Pago";
            this.MetodoPago.Name = "MetodoPago";
            this.MetodoPago.ReadOnly = true;
            // 
            // ObjetoImpDR
            // 
            this.ObjetoImpDR.DataPropertyName = "ObjetoImpDR";
            this.ObjetoImpDR.HeaderText = "Objeto Imp.";
            this.ObjetoImpDR.Name = "ObjetoImpDR";
            this.ObjetoImpDR.ReadOnly = true;
            // 
            // NumParcialidad
            // 
            this.NumParcialidad.DataPropertyName = "NumParcialidad";
            this.NumParcialidad.HeaderText = "NumParcialidad";
            this.NumParcialidad.Name = "NumParcialidad";
            this.NumParcialidad.ReadOnly = true;
            // 
            // ImpSaldoAnt
            // 
            this.ImpSaldoAnt.DataPropertyName = "ImpSaldoAnt";
            this.ImpSaldoAnt.HeaderText = "ImpSaldoAnt";
            this.ImpSaldoAnt.Name = "ImpSaldoAnt";
            this.ImpSaldoAnt.ReadOnly = true;
            // 
            // ImpPagado
            // 
            this.ImpPagado.DataPropertyName = "ImpPagado";
            this.ImpPagado.HeaderText = "ImpPagado";
            this.ImpPagado.Name = "ImpPagado";
            this.ImpPagado.ReadOnly = true;
            // 
            // SaldoInsoluto
            // 
            this.SaldoInsoluto.DataPropertyName = "ImpSaldoInsoluto";
            this.SaldoInsoluto.HeaderText = "SaldoInsoluto";
            this.SaldoInsoluto.Name = "SaldoInsoluto";
            this.SaldoInsoluto.ReadOnly = true;
            // 
            // TDocumentosRel
            // 
            this.TDocumentosRel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TDocumentosRel.Etiqueta = "Titulo";
            this.TDocumentosRel.Location = new System.Drawing.Point(0, 0);
            this.TDocumentosRel.Name = "TDocumentosRel";
            this.TDocumentosRel.ShowActualizar = true;
            this.TDocumentosRel.ShowCerrar = true;
            this.TDocumentosRel.ShowEditar = true;
            this.TDocumentosRel.ShowGuardar = true;
            this.TDocumentosRel.ShowHerramientas = true;
            this.TDocumentosRel.ShowImprimir = true;
            this.TDocumentosRel.ShowNuevo = true;
            this.TDocumentosRel.ShowRemover = true;
            this.TDocumentosRel.Size = new System.Drawing.Size(1186, 25);
            this.TDocumentosRel.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1186, 155);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gridImpuestosTraslado);
            this.tabPage1.Controls.Add(this.TImpuesto);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1178, 129);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Impuestos: Traslados";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // gridImpuestosTraslado
            // 
            this.gridImpuestosTraslado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridImpuestosTraslado.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Tipo,
            this.Impuesto,
            this.Base,
            this.TipoFactor,
            this.TasaCuota,
            this.Importe1});
            this.gridImpuestosTraslado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridImpuestosTraslado.Location = new System.Drawing.Point(3, 28);
            this.gridImpuestosTraslado.Name = "gridImpuestosTraslado";
            this.gridImpuestosTraslado.Size = new System.Drawing.Size(1172, 98);
            this.gridImpuestosTraslado.TabIndex = 3;
            // 
            // Tipo
            // 
            this.Tipo.DataPropertyName = "Tipo";
            this.Tipo.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.Tipo.HeaderText = "Tipo";
            this.Tipo.Name = "Tipo";
            // 
            // Impuesto
            // 
            this.Impuesto.DataPropertyName = "Impuesto";
            this.Impuesto.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.Impuesto.HeaderText = "Impuesto";
            this.Impuesto.Name = "Impuesto";
            this.Impuesto.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Impuesto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Base
            // 
            this.Base.DataPropertyName = "Base";
            this.Base.HeaderText = "Base";
            this.Base.Name = "Base";
            // 
            // TipoFactor
            // 
            this.TipoFactor.DataPropertyName = "TipoFactor";
            this.TipoFactor.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.TipoFactor.HeaderText = "Tipo Factor";
            this.TipoFactor.Name = "TipoFactor";
            this.TipoFactor.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TipoFactor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // TasaCuota
            // 
            this.TasaCuota.DataPropertyName = "TasaOCuota";
            this.TasaCuota.HeaderText = "Tasa ó Cuota";
            this.TasaCuota.Name = "TasaCuota";
            // 
            // Importe1
            // 
            this.Importe1.DataPropertyName = "Importe";
            this.Importe1.HeaderText = "Importe";
            this.Importe1.Name = "Importe1";
            // 
            // TImpuesto
            // 
            this.TImpuesto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TImpuesto.Etiqueta = "";
            this.TImpuesto.Location = new System.Drawing.Point(3, 3);
            this.TImpuesto.Name = "TImpuesto";
            this.TImpuesto.ShowActualizar = false;
            this.TImpuesto.ShowCerrar = false;
            this.TImpuesto.ShowEditar = false;
            this.TImpuesto.ShowGuardar = false;
            this.TImpuesto.ShowHerramientas = false;
            this.TImpuesto.ShowImprimir = false;
            this.TImpuesto.ShowNuevo = true;
            this.TImpuesto.ShowRemover = true;
            this.TImpuesto.Size = new System.Drawing.Size(1172, 25);
            this.TImpuesto.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1178, 129);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Impuestos: Retenidos";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ComplementoPagoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.groupBox1);
            this.Name = "ComplementoPagoControl";
            this.Size = new System.Drawing.Size(1186, 420);
            this.Load += new System.EventHandler(this.ComplementoPagoControl_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Monto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambioP)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDoctoRelacionados)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridImpuestosTraslado)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Common.Forms.ToolBarStandarControl TDocumentosRel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        protected internal System.Windows.Forms.DataGridView gridImpuestosTraslado;
        private System.Windows.Forms.DataGridViewComboBoxColumn Tipo;
        private System.Windows.Forms.DataGridViewComboBoxColumn Impuesto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Base;
        private System.Windows.Forms.DataGridViewComboBoxColumn TipoFactor;
        private System.Windows.Forms.DataGridViewTextBoxColumn TasaCuota;
        private System.Windows.Forms.DataGridViewTextBoxColumn Importe1;
        private Common.Forms.ToolBarStandarControl TImpuesto;
        protected internal System.Windows.Forms.DataGridView gridDoctoRelacionados;
        protected internal System.Windows.Forms.TextBox CadPago;
        protected internal System.Windows.Forms.TextBox SelloPago;
        protected internal System.Windows.Forms.TextBox CertPago;
        protected internal System.Windows.Forms.ComboBox TipoCadPago;
        protected internal System.Windows.Forms.TextBox CtaBeneficiario;
        protected internal System.Windows.Forms.TextBox RfcEmisorCtaBen;
        protected internal System.Windows.Forms.ComboBox Moneda1;
        protected internal System.Windows.Forms.TextBox CtaOrdenante;
        protected internal System.Windows.Forms.TextBox NomBancoOrdExt;
        protected internal System.Windows.Forms.TextBox RfcEmisorCtaOrd;
        protected internal System.Windows.Forms.NumericUpDown Monto;
        protected internal System.Windows.Forms.NumericUpDown TipoCambioP;
        protected internal System.Windows.Forms.TextBox NumOperacion;
        protected internal System.Windows.Forms.ComboBox FormaDePago;
        protected internal System.Windows.Forms.DateTimePicker FechaPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdSubTipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Serie;
        private System.Windows.Forms.DataGridViewTextBoxColumn Folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Receptor;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaEmision;
        private System.Windows.Forms.DataGridViewTextBoxColumn Moneda;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoCambio;
        private System.Windows.Forms.DataGridViewTextBoxColumn FormaDePagoP;
        private System.Windows.Forms.DataGridViewTextBoxColumn MetodoPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn ObjetoImpDR;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumParcialidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImpSaldoAnt;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImpPagado;
        private System.Windows.Forms.DataGridViewTextBoxColumn SaldoInsoluto;
    }
}
