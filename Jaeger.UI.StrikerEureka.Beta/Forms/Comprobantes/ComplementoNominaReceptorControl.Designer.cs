﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class ComplementoNominaReceptorControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CURP = new System.Windows.Forms.TextBox();
            this.NumSeguridadSocial = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.FechaInicioRelLaboral = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.Antiguedad = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TipoContrato = new System.Windows.Forms.ComboBox();
            this.Sindicalizado = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TipoJornada = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TipoRegimen = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.NumEmpleado = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Departamento = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Puesto = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.RiesgoPuesto = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.PeriodicidadPago = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Banco = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.CuentaBancaria = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.SalarioBaseCotApor = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.SalarioDiarioIntegrado = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.ClaveEntFed = new System.Windows.Forms.ComboBox();
            this.gReceptor = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.SalarioBaseCotApor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SalarioDiarioIntegrado)).BeginInit();
            this.gReceptor.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(206, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "CURP:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(385, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Núm de Seguridad Social:*";
            // 
            // CURP
            // 
            this.CURP.Location = new System.Drawing.Point(252, 14);
            this.CURP.MaxLength = 18;
            this.CURP.Name = "CURP";
            this.CURP.Size = new System.Drawing.Size(127, 20);
            this.CURP.TabIndex = 2;
            // 
            // NumSeguridadSocial
            // 
            this.NumSeguridadSocial.Location = new System.Drawing.Point(525, 14);
            this.NumSeguridadSocial.MaxLength = 15;
            this.NumSeguridadSocial.Name = "NumSeguridadSocial";
            this.NumSeguridadSocial.Size = new System.Drawing.Size(104, 20);
            this.NumSeguridadSocial.TabIndex = 3;
            this.NumSeguridadSocial.Text = "012345678912345";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(635, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Fecha Inicio Rel. Laboral:";
            // 
            // FechaInicioRelLaboral
            // 
            this.FechaInicioRelLaboral.Location = new System.Drawing.Point(769, 14);
            this.FechaInicioRelLaboral.Name = "FechaInicioRelLaboral";
            this.FechaInicioRelLaboral.Size = new System.Drawing.Size(200, 20);
            this.FechaInicioRelLaboral.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(976, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Antigüedad:";
            // 
            // Antiguedad
            // 
            this.Antiguedad.Location = new System.Drawing.Point(1046, 14);
            this.Antiguedad.MaxLength = 9;
            this.Antiguedad.Name = "Antiguedad";
            this.Antiguedad.Size = new System.Drawing.Size(68, 20);
            this.Antiguedad.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Tipo de Contrato:*";
            // 
            // TipoContrato
            // 
            this.TipoContrato.FormattingEnabled = true;
            this.TipoContrato.Location = new System.Drawing.Point(103, 40);
            this.TipoContrato.Name = "TipoContrato";
            this.TipoContrato.Size = new System.Drawing.Size(276, 21);
            this.TipoContrato.TabIndex = 5;
            // 
            // Sindicalizado
            // 
            this.Sindicalizado.AutoSize = true;
            this.Sindicalizado.Location = new System.Drawing.Point(388, 44);
            this.Sindicalizado.Name = "Sindicalizado";
            this.Sindicalizado.Size = new System.Drawing.Size(88, 17);
            this.Sindicalizado.TabIndex = 7;
            this.Sindicalizado.Text = "Sindicalizado";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(482, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Tipo de Jornada:";
            // 
            // TipoJornada
            // 
            this.TipoJornada.FormattingEnabled = true;
            this.TipoJornada.Location = new System.Drawing.Point(575, 40);
            this.TipoJornada.Name = "TipoJornada";
            this.TipoJornada.Size = new System.Drawing.Size(188, 21);
            this.TipoJornada.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(773, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "TipoRegimen:*";
            // 
            // TipoRegimen
            // 
            this.TipoRegimen.FormattingEnabled = true;
            this.TipoRegimen.Location = new System.Drawing.Point(856, 40);
            this.TipoRegimen.Name = "TipoRegimen";
            this.TipoRegimen.Size = new System.Drawing.Size(258, 21);
            this.TipoRegimen.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Núm. Empleado:*";
            // 
            // NumEmpleado
            // 
            this.NumEmpleado.Location = new System.Drawing.Point(100, 14);
            this.NumEmpleado.MaxLength = 15;
            this.NumEmpleado.Name = "NumEmpleado";
            this.NumEmpleado.Size = new System.Drawing.Size(100, 20);
            this.NumEmpleado.TabIndex = 3;
            this.NumEmpleado.Text = "123456789012345";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 71);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Departamento:";
            // 
            // Departamento
            // 
            this.Departamento.Location = new System.Drawing.Point(89, 67);
            this.Departamento.MaxLength = 100;
            this.Departamento.Name = "Departamento";
            this.Departamento.Size = new System.Drawing.Size(222, 20);
            this.Departamento.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(317, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Puesto:";
            // 
            // Puesto
            // 
            this.Puesto.Location = new System.Drawing.Point(366, 67);
            this.Puesto.MaxLength = 100;
            this.Puesto.Name = "Puesto";
            this.Puesto.Size = new System.Drawing.Size(234, 20);
            this.Puesto.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(606, 71);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Riesgo de Puesto:";
            // 
            // RiesgoPuesto
            // 
            this.RiesgoPuesto.FormattingEnabled = true;
            this.RiesgoPuesto.Location = new System.Drawing.Point(704, 67);
            this.RiesgoPuesto.Name = "RiesgoPuesto";
            this.RiesgoPuesto.Size = new System.Drawing.Size(158, 21);
            this.RiesgoPuesto.TabIndex = 7;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(868, 71);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(115, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Periodicidad de Pago:*";
            // 
            // PeriodicidadPago
            // 
            this.PeriodicidadPago.FormattingEnabled = true;
            this.PeriodicidadPago.Location = new System.Drawing.Point(987, 67);
            this.PeriodicidadPago.Name = "PeriodicidadPago";
            this.PeriodicidadPago.Size = new System.Drawing.Size(127, 21);
            this.PeriodicidadPago.TabIndex = 7;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 98);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Banco:";
            // 
            // Banco
            // 
            this.Banco.Location = new System.Drawing.Point(55, 94);
            this.Banco.Name = "Banco";
            this.Banco.Size = new System.Drawing.Size(213, 21);
            this.Banco.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(276, 98);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(89, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Cuenta Bancaria:";
            // 
            // CuentaBancaria
            // 
            this.CuentaBancaria.Location = new System.Drawing.Point(371, 94);
            this.CuentaBancaria.MaxLength = 18;
            this.CuentaBancaria.Name = "CuentaBancaria";
            this.CuentaBancaria.Size = new System.Drawing.Size(132, 20);
            this.CuentaBancaria.TabIndex = 3;
            this.CuentaBancaria.Text = "123456789012345678";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(513, 98);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(116, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Salario Base Cot. Apor:";
            // 
            // SalarioBaseCotApor
            // 
            this.SalarioBaseCotApor.DecimalPlaces = 2;
            this.SalarioBaseCotApor.Location = new System.Drawing.Point(635, 94);
            this.SalarioBaseCotApor.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.SalarioBaseCotApor.Name = "SalarioBaseCotApor";
            this.SalarioBaseCotApor.Size = new System.Drawing.Size(82, 20);
            this.SalarioBaseCotApor.TabIndex = 8;
            this.SalarioBaseCotApor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(723, 98);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(120, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Salario Diario Integrado:";
            // 
            // SalarioDiarioIntegrado
            // 
            this.SalarioDiarioIntegrado.DecimalPlaces = 2;
            this.SalarioDiarioIntegrado.Location = new System.Drawing.Point(849, 94);
            this.SalarioDiarioIntegrado.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.SalarioDiarioIntegrado.Name = "SalarioDiarioIntegrado";
            this.SalarioDiarioIntegrado.Size = new System.Drawing.Size(82, 20);
            this.SalarioDiarioIntegrado.TabIndex = 8;
            this.SalarioDiarioIntegrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(937, 98);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "ClaveEntFed:";
            // 
            // ClaveEntFed
            // 
            this.ClaveEntFed.Location = new System.Drawing.Point(1014, 94);
            this.ClaveEntFed.Name = "ClaveEntFed";
            this.ClaveEntFed.Size = new System.Drawing.Size(100, 21);
            this.ClaveEntFed.TabIndex = 3;
            // 
            // gReceptor
            // 
            this.gReceptor.Controls.Add(this.CURP);
            this.gReceptor.Controls.Add(this.SalarioDiarioIntegrado);
            this.gReceptor.Controls.Add(this.label1);
            this.gReceptor.Controls.Add(this.SalarioBaseCotApor);
            this.gReceptor.Controls.Add(this.label2);
            this.gReceptor.Controls.Add(this.PeriodicidadPago);
            this.gReceptor.Controls.Add(this.label4);
            this.gReceptor.Controls.Add(this.label13);
            this.gReceptor.Controls.Add(this.label5);
            this.gReceptor.Controls.Add(this.RiesgoPuesto);
            this.gReceptor.Controls.Add(this.label9);
            this.gReceptor.Controls.Add(this.label12);
            this.gReceptor.Controls.Add(this.label3);
            this.gReceptor.Controls.Add(this.TipoRegimen);
            this.gReceptor.Controls.Add(this.label14);
            this.gReceptor.Controls.Add(this.label8);
            this.gReceptor.Controls.Add(this.label10);
            this.gReceptor.Controls.Add(this.TipoJornada);
            this.gReceptor.Controls.Add(this.label18);
            this.gReceptor.Controls.Add(this.label7);
            this.gReceptor.Controls.Add(this.label15);
            this.gReceptor.Controls.Add(this.Sindicalizado);
            this.gReceptor.Controls.Add(this.label16);
            this.gReceptor.Controls.Add(this.label17);
            this.gReceptor.Controls.Add(this.TipoContrato);
            this.gReceptor.Controls.Add(this.label11);
            this.gReceptor.Controls.Add(this.FechaInicioRelLaboral);
            this.gReceptor.Controls.Add(this.NumSeguridadSocial);
            this.gReceptor.Controls.Add(this.Puesto);
            this.gReceptor.Controls.Add(this.Antiguedad);
            this.gReceptor.Controls.Add(this.Departamento);
            this.gReceptor.Controls.Add(this.NumEmpleado);
            this.gReceptor.Controls.Add(this.CuentaBancaria);
            this.gReceptor.Controls.Add(this.Banco);
            this.gReceptor.Controls.Add(this.ClaveEntFed);
            this.gReceptor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gReceptor.Location = new System.Drawing.Point(0, 0);
            this.gReceptor.Name = "gReceptor";
            this.gReceptor.Size = new System.Drawing.Size(1121, 124);
            this.gReceptor.TabIndex = 9;
            this.gReceptor.TabStop = false;
            // 
            // ComplementoNominaReceptorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gReceptor);
            this.Name = "ComplementoNominaReceptorControl";
            this.Size = new System.Drawing.Size(1121, 124);
            this.Load += new System.EventHandler(this.ComplementoNominaReceptorControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SalarioBaseCotApor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SalarioDiarioIntegrado)).EndInit();
            this.gReceptor.ResumeLayout(false);
            this.gReceptor.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        protected internal System.Windows.Forms.NumericUpDown SalarioBaseCotApor;
        private System.Windows.Forms.Label label17;
        protected internal System.Windows.Forms.NumericUpDown SalarioDiarioIntegrado;
        private System.Windows.Forms.Label label18;
        protected internal System.Windows.Forms.ComboBox ClaveEntFed;
        protected internal System.Windows.Forms.TextBox CURP;
        protected internal System.Windows.Forms.TextBox NumSeguridadSocial;
        protected internal System.Windows.Forms.DateTimePicker FechaInicioRelLaboral;
        protected internal System.Windows.Forms.TextBox Antiguedad;
        protected internal System.Windows.Forms.ComboBox TipoContrato;
        protected internal System.Windows.Forms.CheckBox Sindicalizado;
        protected internal System.Windows.Forms.ComboBox TipoJornada;
        protected internal System.Windows.Forms.ComboBox TipoRegimen;
        protected internal System.Windows.Forms.TextBox NumEmpleado;
        protected internal System.Windows.Forms.TextBox Departamento;
        protected internal System.Windows.Forms.TextBox Puesto;
        protected internal System.Windows.Forms.ComboBox RiesgoPuesto;
        protected internal System.Windows.Forms.ComboBox PeriodicidadPago;
        protected internal System.Windows.Forms.ComboBox Banco;
        protected internal System.Windows.Forms.TextBox CuentaBancaria;
        private System.Windows.Forms.GroupBox gReceptor;
    }
}
