﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class ComprobanteFiscalControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComprobanteFiscalControl));
            this.TComprobante = new System.Windows.Forms.ToolStrip();
            this.ToolBarButtonEmisor = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.Status = new System.Windows.Forms.ToolStripComboBox();
            this.ToolBarButtonNuevo = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonDuplicar = new System.Windows.Forms.ToolStripButton();
            this.Guardar = new System.Windows.Forms.ToolStripButton();
            this.Actualizar = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonCertificar = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonCancelar = new System.Windows.Forms.ToolStripButton();
            this.FilePDF = new System.Windows.Forms.ToolStripButton();
            this.FileXML = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.Complementos = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.IdDocumento = new System.Windows.Forms.ToolStripTextBox();
            this.Cerrar = new System.Windows.Forms.ToolStripButton();
            this.TabGeneral = new System.Windows.Forms.TabControl();
            this.TabComprobante = new System.Windows.Forms.TabPage();
            this.General = new Jaeger.UI.Forms.Comprobantes.ComprobanteGeneralControl();
            this.TabCFDIRelacionado = new System.Windows.Forms.TabPage();
            this.CFDIRelacionadoControl = new Jaeger.UI.Forms.Comprobantes.ComprobanteRelacionadoControl();
            this.TabInfoGeneral = new System.Windows.Forms.TabPage();
            this.InformacionGeneral = new Jaeger.UI.Comprobante.Forms.ComprobanteFiscalInformacionGeneralControl();
            this.OnLoadStart = new System.ComponentModel.BackgroundWorker();
            this.TComprobante.SuspendLayout();
            this.TabGeneral.SuspendLayout();
            this.TabComprobante.SuspendLayout();
            this.TabCFDIRelacionado.SuspendLayout();
            this.TabInfoGeneral.SuspendLayout();
            this.SuspendLayout();
            // 
            // TComprobante
            // 
            this.TComprobante.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolBarButtonEmisor,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.Status,
            this.ToolBarButtonNuevo,
            this.ToolBarButtonDuplicar,
            this.Guardar,
            this.Actualizar,
            this.ToolBarButtonCertificar,
            this.ToolBarButtonCancelar,
            this.FilePDF,
            this.FileXML,
            this.ToolBarButtonEmail,
            this.toolStripSeparator2,
            this.Complementos,
            this.toolStripSeparator3,
            this.toolStripLabel2,
            this.IdDocumento,
            this.Cerrar});
            this.TComprobante.Location = new System.Drawing.Point(0, 0);
            this.TComprobante.Name = "TComprobante";
            this.TComprobante.Size = new System.Drawing.Size(1265, 25);
            this.TComprobante.TabIndex = 0;
            this.TComprobante.Text = "toolStrip1";
            // 
            // ToolBarButtonEmisor
            // 
            this.ToolBarButtonEmisor.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarButtonEmisor.Image")));
            this.ToolBarButtonEmisor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonEmisor.Name = "ToolBarButtonEmisor";
            this.ToolBarButtonEmisor.Size = new System.Drawing.Size(137, 22);
            this.ToolBarButtonEmisor.Text = "XXXXXXXXXXXXXX";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(42, 22);
            this.toolStripLabel1.Text = "Status:";
            // 
            // Status
            // 
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(95, 25);
            // 
            // ToolBarButtonNuevo
            // 
            this.ToolBarButtonNuevo.Image = global::Jaeger.UI.Properties.Resources.new_16px;
            this.ToolBarButtonNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonNuevo.Name = "ToolBarButtonNuevo";
            this.ToolBarButtonNuevo.Size = new System.Drawing.Size(62, 22);
            this.ToolBarButtonNuevo.Text = "Nuevo";
            // 
            // ToolBarButtonDuplicar
            // 
            this.ToolBarButtonDuplicar.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarButtonDuplicar.Image")));
            this.ToolBarButtonDuplicar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonDuplicar.Name = "ToolBarButtonDuplicar";
            this.ToolBarButtonDuplicar.Size = new System.Drawing.Size(71, 22);
            this.ToolBarButtonDuplicar.Text = "Duplicar";
            // 
            // Guardar
            // 
            this.Guardar.Image = global::Jaeger.UI.Properties.Resources.save_16px;
            this.Guardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(69, 22);
            this.Guardar.Text = "Guardar";
            // 
            // Actualizar
            // 
            this.Actualizar.Image = global::Jaeger.UI.Properties.Resources.refresh_16px;
            this.Actualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Size = new System.Drawing.Size(79, 22);
            this.Actualizar.Text = "Actualizar";
            // 
            // ToolBarButtonCertificar
            // 
            this.ToolBarButtonCertificar.Image = global::Jaeger.UI.Properties.Resources.approval_16px;
            this.ToolBarButtonCertificar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonCertificar.Name = "ToolBarButtonCertificar";
            this.ToolBarButtonCertificar.Size = new System.Drawing.Size(68, 22);
            this.ToolBarButtonCertificar.Text = "Timbrar";
            this.ToolBarButtonCertificar.Click += new System.EventHandler(this.TComprobante_Certificar_Click);
            // 
            // ToolBarButtonCancelar
            // 
            this.ToolBarButtonCancelar.Image = global::Jaeger.UI.Properties.Resources.cancel_16;
            this.ToolBarButtonCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonCancelar.Name = "ToolBarButtonCancelar";
            this.ToolBarButtonCancelar.Size = new System.Drawing.Size(73, 22);
            this.ToolBarButtonCancelar.Text = "Cancelar";
            this.ToolBarButtonCancelar.ToolTipText = "Cancelar";
            this.ToolBarButtonCancelar.Click += new System.EventHandler(this.TComprobante_Cancelar_Click);
            // 
            // ToolBarButtonUrlFilePDF
            // 
            this.FilePDF.Image = global::Jaeger.UI.Properties.Resources.pdf_16px;
            this.FilePDF.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FilePDF.Name = "ToolBarButtonUrlFilePDF";
            this.FilePDF.Size = new System.Drawing.Size(48, 22);
            this.FilePDF.Text = "PDF";
            this.FilePDF.Click += new System.EventHandler(this.TComprobante_FilePDF_Click);
            // 
            // ToolBarButtonUrlFileXML
            // 
            this.FileXML.Image = global::Jaeger.UI.Properties.Resources.xml_16px;
            this.FileXML.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FileXML.Name = "ToolBarButtonUrlFileXML";
            this.FileXML.Size = new System.Drawing.Size(51, 22);
            this.FileXML.Text = "XML";
            this.FileXML.Click += new System.EventHandler(this.TComprobante_FileXML_Click);
            // 
            // ToolBarButtonEmail
            // 
            this.ToolBarButtonEmail.Image = global::Jaeger.UI.Properties.Resources.send_16px;
            this.ToolBarButtonEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonEmail.Name = "ToolBarButtonEmail";
            this.ToolBarButtonEmail.Size = new System.Drawing.Size(59, 22);
            this.ToolBarButtonEmail.Text = "Enviar";
            this.ToolBarButtonEmail.Click += new System.EventHandler(this.TComprobante_Email_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // Complementos
            // 
            this.Complementos.Image = global::Jaeger.UI.Properties.Resources.plugin_16px;
            this.Complementos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Complementos.Name = "Complementos";
            this.Complementos.Size = new System.Drawing.Size(118, 22);
            this.Complementos.Text = "Complementos";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(37, 22);
            this.toolStripLabel2.Text = "UUID:";
            // 
            // IdDocumento
            // 
            this.IdDocumento.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.Size = new System.Drawing.Size(225, 23);
            // 
            // Cerrar
            // 
            this.Cerrar.Image = global::Jaeger.UI.Properties.Resources.close_window_16px;
            this.Cerrar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(59, 20);
            this.Cerrar.Text = "Cerrar";
            // 
            // TabGeneral
            // 
            this.TabGeneral.Controls.Add(this.TabComprobante);
            this.TabGeneral.Controls.Add(this.TabCFDIRelacionado);
            this.TabGeneral.Controls.Add(this.TabInfoGeneral);
            this.TabGeneral.Dock = System.Windows.Forms.DockStyle.Top;
            this.TabGeneral.Location = new System.Drawing.Point(0, 25);
            this.TabGeneral.Name = "TabGeneral";
            this.TabGeneral.SelectedIndex = 0;
            this.TabGeneral.Size = new System.Drawing.Size(1265, 136);
            this.TabGeneral.TabIndex = 1;
            // 
            // TabComprobante
            // 
            this.TabComprobante.Controls.Add(this.General);
            this.TabComprobante.Location = new System.Drawing.Point(4, 22);
            this.TabComprobante.Name = "TabComprobante";
            this.TabComprobante.Padding = new System.Windows.Forms.Padding(3);
            this.TabComprobante.Size = new System.Drawing.Size(1257, 110);
            this.TabComprobante.TabIndex = 0;
            this.TabComprobante.Text = "Comprobante";
            this.TabComprobante.UseVisualStyleBackColor = true;
            // 
            // General
            // 
            this.General.Dock = System.Windows.Forms.DockStyle.Fill;
            this.General.Location = new System.Drawing.Point(3, 3);
            this.General.Name = "General";
            this.General.Size = new System.Drawing.Size(1251, 104);
            this.General.TabIndex = 0;
            // 
            // TabCFDIRelacionado
            // 
            this.TabCFDIRelacionado.Controls.Add(this.CFDIRelacionadoControl);
            this.TabCFDIRelacionado.Location = new System.Drawing.Point(4, 22);
            this.TabCFDIRelacionado.Name = "TabCFDIRelacionado";
            this.TabCFDIRelacionado.Padding = new System.Windows.Forms.Padding(3);
            this.TabCFDIRelacionado.Size = new System.Drawing.Size(1257, 110);
            this.TabCFDIRelacionado.TabIndex = 1;
            this.TabCFDIRelacionado.Text = "CFDI Relacionado";
            this.TabCFDIRelacionado.UseVisualStyleBackColor = true;
            // 
            // CFDIRelacionadoControl
            // 
            this.CFDIRelacionadoControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CFDIRelacionadoControl.Editable = true;
            this.CFDIRelacionadoControl.Location = new System.Drawing.Point(3, 3);
            this.CFDIRelacionadoControl.Name = "CFDIRelacionadoControl";
            this.CFDIRelacionadoControl.Size = new System.Drawing.Size(1251, 104);
            this.CFDIRelacionadoControl.TabIndex = 0;
            // 
            // TabInfoGeneral
            // 
            this.TabInfoGeneral.Controls.Add(this.InformacionGeneral);
            this.TabInfoGeneral.Location = new System.Drawing.Point(4, 22);
            this.TabInfoGeneral.Name = "TabInfoGeneral";
            this.TabInfoGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.TabInfoGeneral.Size = new System.Drawing.Size(1257, 110);
            this.TabInfoGeneral.TabIndex = 2;
            this.TabInfoGeneral.Text = "Información General";
            this.TabInfoGeneral.UseVisualStyleBackColor = true;
            // 
            // InformacionGeneral
            // 
            this.InformacionGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InformacionGeneral.Editable = false;
            this.InformacionGeneral.Location = new System.Drawing.Point(3, 3);
            this.InformacionGeneral.MinimumSize = new System.Drawing.Size(1200, 102);
            this.InformacionGeneral.Name = "InformacionGeneral";
            this.InformacionGeneral.Size = new System.Drawing.Size(1251, 104);
            this.InformacionGeneral.TabIndex = 0;
            // 
            // OnLoadStart
            // 
            this.OnLoadStart.DoWork += new System.ComponentModel.DoWorkEventHandler(this.OnLoadStart_DoWork);
            this.OnLoadStart.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.OnLoadStart_RunWorkerCompleted);
            // 
            // ComprobanteFiscalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TabGeneral);
            this.Controls.Add(this.TComprobante);
            this.Name = "ComprobanteFiscalControl";
            this.Size = new System.Drawing.Size(1265, 161);
            this.Load += new System.EventHandler(this.ComprobanteFiscalControl_Load);
            this.TComprobante.ResumeLayout(false);
            this.TComprobante.PerformLayout();
            this.TabGeneral.ResumeLayout(false);
            this.TabComprobante.ResumeLayout(false);
            this.TabCFDIRelacionado.ResumeLayout(false);
            this.TabInfoGeneral.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        protected internal System.Windows.Forms.ToolStripSplitButton ToolBarButtonEmisor;
        protected internal System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        protected internal System.Windows.Forms.ToolStripComboBox Status;
        protected internal System.Windows.Forms.ToolStripButton ToolBarButtonNuevo;
        protected internal System.Windows.Forms.ToolStripButton ToolBarButtonDuplicar;
        protected internal System.Windows.Forms.ToolStripButton Actualizar;
        protected internal System.Windows.Forms.ToolStripButton ToolBarButtonCertificar;
        protected internal System.Windows.Forms.ToolStripButton ToolBarButtonCancelar;
        protected internal System.Windows.Forms.ToolStripButton FilePDF;
        protected internal System.Windows.Forms.ToolStripButton FileXML;
        protected internal System.Windows.Forms.ToolStripButton ToolBarButtonEmail;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.TabPage TabInfoGeneral;
        protected internal ComprobanteGeneralControl General;
        private System.ComponentModel.BackgroundWorker OnLoadStart;
        public System.Windows.Forms.ToolStripTextBox IdDocumento;
        protected internal System.Windows.Forms.ToolStripButton Guardar;
        protected internal System.Windows.Forms.ToolStrip TComprobante;
        protected internal System.Windows.Forms.ToolStripButton Cerrar;
        protected internal System.Windows.Forms.TabControl TabGeneral;
        protected internal System.Windows.Forms.TabPage TabComprobante;
        protected internal System.Windows.Forms.TabPage TabCFDIRelacionado;
        protected internal Jaeger.UI.Comprobante.Forms.ComprobanteFiscalInformacionGeneralControl InformacionGeneral;
        protected internal System.Windows.Forms.ToolStripDropDownButton Complementos;
        protected internal ComprobanteRelacionadoControl CFDIRelacionadoControl;
    }
}
