﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class ComprobanteRelacionadoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.TRelacion = new System.Windows.Forms.ToolStrip();
            this.Incluir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.TipoRelacion = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.gridDocumentos = new System.Windows.Forms.DataGridView();
            this.IdDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorRFC = new System.Windows.Forms.TextBox();
            this.TRelacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDocumentos)).BeginInit();
            this.SuspendLayout();
            // 
            // TRelacion
            // 
            this.TRelacion.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Incluir,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.TipoRelacion,
            this.toolStripSeparator2,
            this.toolStripButton2,
            this.toolStripButton3});
            this.TRelacion.Location = new System.Drawing.Point(0, 0);
            this.TRelacion.Name = "TRelacion";
            this.TRelacion.Size = new System.Drawing.Size(1085, 25);
            this.TRelacion.TabIndex = 0;
            this.TRelacion.Text = "toolStrip1";
            // 
            // Incluir
            // 
            this.Incluir.CheckOnClick = true;
            this.Incluir.Image = global::Jaeger.UI.Properties.Resources.add_16px;
            this.Incluir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Incluir.Name = "Incluir";
            this.Incluir.Size = new System.Drawing.Size(60, 22);
            this.Incluir.Text = "Incluir";
            this.Incluir.Click += new System.EventHandler(this.Incluir_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(94, 22);
            this.toolStripLabel1.Text = "Tipo de Relación";
            // 
            // TipoRelacion
            // 
            this.TipoRelacion.Name = "TipoRelacion";
            this.TipoRelacion.Size = new System.Drawing.Size(250, 25);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::Jaeger.UI.Properties.Resources.add_16px;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(69, 22);
            this.toolStripButton2.Text = "Agregar";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = global::Jaeger.UI.Properties.Resources.delete_16px;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(74, 22);
            this.toolStripButton3.Text = "Remover";
            // 
            // gridDocumentos
            // 
            this.gridDocumentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDocumentos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdDocumento,
            this.RFC,
            this.Nombre,
            this.Serie,
            this.Folio,
            this.Total,
            this.Importe});
            this.gridDocumentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDocumentos.Location = new System.Drawing.Point(0, 25);
            this.gridDocumentos.Name = "gridDocumentos";
            this.gridDocumentos.Size = new System.Drawing.Size(1085, 125);
            this.gridDocumentos.TabIndex = 1;
            // 
            // IdDocumento
            // 
            this.IdDocumento.DataPropertyName = "IdDocumento";
            this.IdDocumento.HeaderText = "UUID";
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.ReadOnly = true;
            this.IdDocumento.Width = 220;
            // 
            // RFC
            // 
            this.RFC.DataPropertyName = "RFC";
            this.RFC.HeaderText = "RFC";
            this.RFC.Name = "RFC";
            this.RFC.ReadOnly = true;
            // 
            // Nombre
            // 
            this.Nombre.DataPropertyName = "Nombre";
            this.Nombre.HeaderText = "Identidad Fiscal";
            this.Nombre.Name = "Nombre";
            this.Nombre.ReadOnly = true;
            this.Nombre.Width = 250;
            // 
            // Serie
            // 
            this.Serie.DataPropertyName = "Serie";
            this.Serie.HeaderText = "Serie";
            this.Serie.Name = "Serie";
            this.Serie.ReadOnly = true;
            // 
            // Folio
            // 
            this.Folio.DataPropertyName = "Folio";
            this.Folio.HeaderText = "Folio";
            this.Folio.Name = "Folio";
            this.Folio.ReadOnly = true;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // Importe
            // 
            this.Importe.DataPropertyName = "ImporteAplicado";
            this.Importe.HeaderText = "Importe";
            this.Importe.Name = "Importe";
            this.Importe.ReadOnly = true;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.Location = new System.Drawing.Point(512, 103);
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.Size = new System.Drawing.Size(100, 20);
            this.ReceptorRFC.TabIndex = 2;
            // 
            // ComprobanteRelacionadoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridDocumentos);
            this.Controls.Add(this.TRelacion);
            this.Controls.Add(this.ReceptorRFC);
            this.Name = "ComprobanteRelacionadoControl";
            this.Size = new System.Drawing.Size(1085, 150);
            this.Load += new System.EventHandler(this.ComprobanteRelacionadoControl_Load);
            this.TRelacion.ResumeLayout(false);
            this.TRelacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDocumentos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip TRelacion;
        private System.Windows.Forms.ToolStripButton Incluir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox TipoRelacion;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.DataGridView gridDocumentos;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Serie;
        private System.Windows.Forms.DataGridViewTextBoxColumn Folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn Importe;
        protected internal System.Windows.Forms.TextBox ReceptorRFC;
    }
}
