﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class UnidadPesoCatalogoBuscarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TCatalogo = new Jaeger.UI.Common.Forms.ToolBarStandarSearchControl();
            this.gridUnidades = new System.Windows.Forms.DataGridView();
            this.Clave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridUnidades)).BeginInit();
            this.SuspendLayout();
            // 
            // TCatalogo
            // 
            this.TCatalogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCatalogo.Etiqueta = "Buscar:";
            this.TCatalogo.Location = new System.Drawing.Point(0, 0);
            this.TCatalogo.Name = "TCatalogo";
            this.TCatalogo.ShowActualizar = true;
            this.TCatalogo.ShowCerrar = true;
            this.TCatalogo.ShowEditar = false;
            this.TCatalogo.ShowGuardar = false;
            this.TCatalogo.ShowHerramientas = false;
            this.TCatalogo.ShowImprimir = true;
            this.TCatalogo.ShowNuevo = false;
            this.TCatalogo.ShowRemover = false;
            this.TCatalogo.Size = new System.Drawing.Size(548, 25);
            this.TCatalogo.TabIndex = 0;
            this.TCatalogo.ButtonImprimir_Click += new System.EventHandler<System.EventArgs>(this.TCatalogo_Imprimir_Click);
            this.TCatalogo.ButtonActualizar_Click += new System.EventHandler<System.EventArgs>(this.TCatalogo_Actualizar_Click);
            this.TCatalogo.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TCatalogo_Cerrar_Click);
            // 
            // gridUnidades
            // 
            this.gridUnidades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridUnidades.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Clave,
            this.Descripcion});
            this.gridUnidades.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridUnidades.Location = new System.Drawing.Point(0, 25);
            this.gridUnidades.Name = "gridUnidades";
            this.gridUnidades.Size = new System.Drawing.Size(548, 290);
            this.gridUnidades.TabIndex = 1;
            // 
            // Clave
            // 
            this.Clave.DataPropertyName = "Clave";
            this.Clave.HeaderText = "Clave";
            this.Clave.Name = "Clave";
            this.Clave.Width = 75;
            // 
            // Descripcion
            // 
            this.Descripcion.DataPropertyName = "Descripcion";
            this.Descripcion.HeaderText = "Descripción";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.ReadOnly = true;
            this.Descripcion.Width = 350;
            // 
            // UnidadPesoCatalogoBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 315);
            this.Controls.Add(this.gridUnidades);
            this.Controls.Add(this.TCatalogo);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UnidadPesoCatalogoBuscarForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Catálogo: Unidad de Peso";
            this.Load += new System.EventHandler(this.CatalogoBuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridUnidades)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarSearchControl TCatalogo;
        private System.Windows.Forms.DataGridView gridUnidades;
        private System.Windows.Forms.DataGridViewTextBoxColumn Clave;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
    }
}