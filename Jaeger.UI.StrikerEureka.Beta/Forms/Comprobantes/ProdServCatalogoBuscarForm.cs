﻿using Jaeger.Domain.Comprobante.Entities;
using System;
using System.Windows.Forms;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class ProdServCatalogoBuscarForm : Form {

        public void OnAgregar(ComprobanteConceptoDetailModel e) {
            if (this.Agregar != null) {
                this.Agregar(this, e);
            }
        }

        public event EventHandler<ComprobanteConceptoDetailModel> Agregar;

        public ProdServCatalogoBuscarForm() {
            InitializeComponent();
        }

        private void ProdServCatalogoBuscarForm_Load(object sender, EventArgs e) {
            this.CatalogoSAT.SetService();
            this.TProducto.Imprimir.Image = Properties.Resources.add_16px;
            this.TProducto.Imprimir.Text = "Agregar";
            this.TProducto.Actualizar.Text = "Buscar";
            this.TProducto.Buscar.TextChanged += Buscar_TextChanged;
            this.TProducto.Actualizar.Click += Actualizar_Click;
            this.TProducto.Imprimir.Click += Imprimir_Click;
        }

        private void Imprimir_Click(object sender, EventArgs e) {
            var seleccionado = this.CatalogoSAT.GetSelected();
            this.OnAgregar(seleccionado);
        }

        private void Actualizar_Click(object sender, EventArgs e) {
            this.CatalogoSAT.Search(this.TProducto.Buscar.Text);
        }

        private void Buscar_TextChanged(object sender, EventArgs e) {
            if (this.TProducto.Buscar.Text.Length > 0) { 
                this.TProducto.Actualizar.PerformClick();
            }
        }

        private void TProducto_ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
