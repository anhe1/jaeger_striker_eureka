﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class ComprobanteTreeViewForm : Form {
        private ComprobanteFiscalExtended model;
        public ComprobanteTreeViewForm(ComprobanteFiscalExtended model) {
            InitializeComponent();
            this.model = model;
        }

        private void ComprobanteTreeViewForm_Load(object sender, EventArgs e) {
            if (model.XmlContentB64 != null) {
                try {
                    var encodedString = Convert.FromBase64String(model.XmlContentB64);
                    // Put the byte array into a stream and rewind it to the beginning
                    MemoryStream ms = new MemoryStream();
                    ms.Write(encodedString, 0, encodedString.Length);
                    ms.Seek(0, SeekOrigin.Begin);
                    ms.Flush();
                    ms.Position = 0;
                    StreamReader sr = new StreamReader(ms);
                    this.ucXmlRichTextBox1.Xml = sr.ReadToEnd();
                    this.ucXmlRichTextBox1.ReadOnly = true;
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
            this.Text = model.KeyName();
            this.treeViewControl1.Crear(model.XmlContentB64);
            this.treeView2Control1.Crear(model.XmlContentB64);
        }

        public static Stream GenerateStreamFromString(string s) {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        static string GetRtfUnicodeEscapedString(string s) {
            var sb = new StringBuilder();
            foreach (var c in s) {
                if (c == '\\' || c == '{' || c == '}')
                    sb.Append(@"\" + c);
                else if (c <= 0x7f)
                    sb.Append(c);
                else
                    sb.Append("\\u" + Convert.ToUInt32(c) + "?");
            }
            return sb.ToString();
        }

        public string PrintXML(string xml) {
            string result = "";

            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
            XmlDocument document = new XmlDocument();

            try {
                // Load the XmlDocument with the XML.
                document.LoadXml(xml);

                writer.Formatting = Formatting.Indented;

                // Write the XML into a formatting XmlTextWriter
                document.WriteContentTo(writer);
                writer.Flush();
                mStream.Flush();

                // Have to rewind the MemoryStream in order to read
                // its contents.
                mStream.Position = 0;

                // Read MemoryStream contents into a StreamReader.
                StreamReader sReader = new StreamReader(mStream);

                // Extract the text from the StreamReader.
                string formattedXml = sReader.ReadToEnd();

                result = formattedXml;
            } catch (XmlException ex) {
                Console.WriteLine(ex.Message);
                // Handle the exception
            }

            mStream.Close();
            writer.Close();

            return result;
        }
    }
}
