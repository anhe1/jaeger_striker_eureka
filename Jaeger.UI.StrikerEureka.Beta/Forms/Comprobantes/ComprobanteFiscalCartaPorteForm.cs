﻿using System;
using System.Windows.Forms;
using System.Linq;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.UI.Forms.Comprobantes {
    /// <summary>
    /// Comprobante Ingreso con Carta Porte
    /// </summary>
    public class ComprobanteFiscalCartaPorteForm : ComprobanteFiscalForm {
        protected internal ToolStripMenuItem CopiarConceptos = new ToolStripMenuItem { Text = "Copiar a conceptos" };
        protected internal CartaPorteControl _CartaPorteControl = new CartaPorteControl() { Dock = DockStyle.Fill };
        protected internal TabPage _page = new TabPage() { Text = "Complemento: Carta Porte", Padding = new Padding(3), UseVisualStyleBackColor = true };

        public ComprobanteFiscalCartaPorteForm(ComprobanteFiscalDetailModel model, CFDISubTipoEnum subTipo) : base(model, subTipo) {
            _page.Controls.Add(_CartaPorteControl);
            this.tabControls.TabPages.Add(_page);
            this.ComprobanteControl.TabGeneral.TabPages.RemoveAt(2);

            this.Load += ComprobanteFiscalCartaPorteForm_Load;
        }

        private void ComprobanteFiscalCartaPorteForm_Load(object sender, EventArgs e) {
            this.Text = "Carta Porte";
            this.CopiarConceptos.Click += this.CopiarConceptos_Click;
            this._CartaPorteControl.TMercancia.Herramientas.DropDownItems.Add(this.CopiarConceptos);
            this.ComprobanteControl.BindingCompleted += ComprobanteControl_BindingCompleted;
        }

        public virtual void CopiarConceptos_Click(object sender, EventArgs e) {
            if (MessageBox.Show(this, "¿Esta seguro de copiar las mercancias a los conceptos del comprobante? se reemplazaran todos los items", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) { 
                if (this._CartaPorteControl._CartaPorte.Mercancias.Mercancia.Count == 0) {
                    MessageBox.Show(this, "No existen mercancias!", "Atención", MessageBoxButtons.OK);
                    return;
                }

                var d = this.ComprobanteControl.Comprobante.Conceptos.Where(it => it.Id > 0).ToList();
                if (d.Count == 0) {
                    this.ComprobanteControl.Comprobante.Conceptos.Clear();
                } else {
                    this.ComprobanteControl.Comprobante.Conceptos = new System.ComponentModel.BindingList<ComprobanteConceptoDetailModel>(d);
                }

                var c = this.ComprobanteControl.Comprobante.Conceptos.Count();
                if (c > 0) {
                    var d1 = new System.ComponentModel.BindingList<ComprobanteConceptoDetailModel>(this.ComprobanteControl.Comprobante.Conceptos);

                } else {
                    foreach (var item in this._CartaPorteControl._CartaPorte.Mercancias.Mercancia) {
                        this.ComprobanteControl.Comprobante.Conceptos.Add(new ComprobanteConceptoDetailModel { Activo = true, ClaveProdServ = item.BienesTransp, Cantidad = item.Cantidad, ClaveUnidad = item.ClaveUnidad, ValorUnitario = item.ValorMercancia, Descripcion = item.Descripcion, Unidad = item.Unidad });
                    }
                }

            }
        }

        private void ComprobanteControl_BindingCompleted(object sender, EventArgs e) {
            if (this.ComprobanteControl.Comprobante.Id == 0) {
                if (this.ComprobanteControl.Comprobante.CartaPorte == null)
                    this.ComprobanteControl.Comprobante.CartaPorte = new Domain.Comprobante.Entities.Complemento.CartaPorte.CartaPorteDetailModel();
            }
            _CartaPorteControl._CartaPorte = this.ComprobanteControl.Comprobante.CartaPorte;
            _CartaPorteControl.CreateBinding();
        }

        public override void DesdeArchivo_Click(object sender, EventArgs e) {
            var _openFile = new OpenFileDialog();
            if (_openFile.ShowDialog() == DialogResult.OK) {
                //var cfdi = this.service.GetComprobante(_openFile.FileName);
                //if (cfdi != null) {
                //    cfdi.TimbreFiscal = null;
                //    cfdi.IdDocumento = null;
                //    cfdi.FechaTimbre = null;
                //    cfdi.FechaEmision = DateTime.Now;
                //    this.ComprobanteControl.Comprobante = cfdi;
                //    this.ComprobanteControl.CreateBinding();
                //    cfdi = null;
                //}
            }
        }
    }
}
