﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class ConceptoACuentaTerceroForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TConcepto = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.DomicilioFiscalACuentaTerceros = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.RegimenFiscalACuentaTerceros = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.RfcACuentaTerceros = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.buttonActualizarDirectorio = new System.Windows.Forms.Button();
            this.NombreACuentaTerceros = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CargarReceptores = new System.ComponentModel.BackgroundWorker();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TConcepto
            // 
            this.TConcepto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConcepto.Etiqueta = "";
            this.TConcepto.Location = new System.Drawing.Point(0, 0);
            this.TConcepto.Name = "TConcepto";
            this.TConcepto.ShowActualizar = false;
            this.TConcepto.ShowCerrar = true;
            this.TConcepto.ShowEditar = false;
            this.TConcepto.ShowGuardar = true;
            this.TConcepto.ShowHerramientas = false;
            this.TConcepto.ShowImprimir = false;
            this.TConcepto.ShowNuevo = false;
            this.TConcepto.ShowRemover = false;
            this.TConcepto.Size = new System.Drawing.Size(593, 25);
            this.TConcepto.TabIndex = 0;
            this.TConcepto.ButtonGuardar_Click += new System.EventHandler<System.EventArgs>(this.TComplemento_Guardar_Click);
            this.TConcepto.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TComplemento_Cerrar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DomicilioFiscalACuentaTerceros);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.RegimenFiscalACuentaTerceros);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.RfcACuentaTerceros);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.buttonActualizarDirectorio);
            this.groupBox1.Controls.Add(this.NombreACuentaTerceros);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(593, 85);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Información";
            // 
            // DomicilioFiscalACuentaTerceros
            // 
            this.DomicilioFiscalACuentaTerceros.Location = new System.Drawing.Point(471, 46);
            this.DomicilioFiscalACuentaTerceros.Name = "DomicilioFiscalACuentaTerceros";
            this.DomicilioFiscalACuentaTerceros.Size = new System.Drawing.Size(100, 20);
            this.DomicilioFiscalACuentaTerceros.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(383, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Domicilio Fiscal:";
            // 
            // RegimenFiscalACuentaTerceros
            // 
            this.RegimenFiscalACuentaTerceros.FormattingEnabled = true;
            this.RegimenFiscalACuentaTerceros.Location = new System.Drawing.Point(102, 46);
            this.RegimenFiscalACuentaTerceros.Name = "RegimenFiscalACuentaTerceros";
            this.RegimenFiscalACuentaTerceros.Size = new System.Drawing.Size(258, 21);
            this.RegimenFiscalACuentaTerceros.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Régimen Fiscal:";
            // 
            // RfcACuentaTerceros
            // 
            this.RfcACuentaTerceros.Location = new System.Drawing.Point(471, 19);
            this.RfcACuentaTerceros.Name = "RfcACuentaTerceros";
            this.RfcACuentaTerceros.Size = new System.Drawing.Size(100, 20);
            this.RfcACuentaTerceros.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(434, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "RFC:";
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Image = global::Jaeger.UI.Properties.Resources.add_16px;
            this.button2.Location = new System.Drawing.Point(386, 19);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(20, 20);
            this.button2.TabIndex = 3;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // buttonActualizarDirectorio
            // 
            this.buttonActualizarDirectorio.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonActualizarDirectorio.Image = global::Jaeger.UI.Properties.Resources.refresh_16px;
            this.buttonActualizarDirectorio.Location = new System.Drawing.Point(366, 19);
            this.buttonActualizarDirectorio.Name = "buttonActualizarDirectorio";
            this.buttonActualizarDirectorio.Size = new System.Drawing.Size(20, 20);
            this.buttonActualizarDirectorio.TabIndex = 2;
            this.buttonActualizarDirectorio.UseVisualStyleBackColor = true;
            // 
            // NombreACuentaTerceros
            // 
            this.NombreACuentaTerceros.FormattingEnabled = true;
            this.NombreACuentaTerceros.Location = new System.Drawing.Point(74, 19);
            this.NombreACuentaTerceros.Name = "NombreACuentaTerceros";
            this.NombreACuentaTerceros.Size = new System.Drawing.Size(286, 21);
            this.NombreACuentaTerceros.TabIndex = 1;
            this.NombreACuentaTerceros.SelectedValueChanged += new System.EventHandler(this.Receptor_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Receptor:";
            // 
            // CargarReceptores
            // 
            this.CargarReceptores.DoWork += new System.ComponentModel.DoWorkEventHandler(this.CargarReceptores_DoWork);
            this.CargarReceptores.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.CargarReceptores_RunWorkerCompleted);
            // 
            // ConceptoACuentaTerceroForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 110);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TConcepto);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConceptoACuentaTerceroForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Concepto a Cuenta de Tercero";
            this.Load += new System.EventHandler(this.ConceptoACuentaTerceroForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarControl TConcepto;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox DomicilioFiscalACuentaTerceros;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox RegimenFiscalACuentaTerceros;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox RfcACuentaTerceros;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttonActualizarDirectorio;
        private System.Windows.Forms.ComboBox NombreACuentaTerceros;
        private System.Windows.Forms.Label label1;
        private System.ComponentModel.BackgroundWorker CargarReceptores;
    }
}