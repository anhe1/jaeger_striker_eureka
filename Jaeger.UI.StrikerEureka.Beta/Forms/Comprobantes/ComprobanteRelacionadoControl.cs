﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class ComprobanteRelacionadoControl : UserControl {
        protected IRelacionCFDICatalogo catalogoRelacionesCfdi;
        private bool editable = true;
        public ComprobanteRelacionadoControl() {
            InitializeComponent();
        }

        public bool Editable {
            get {
                return this.editable;
            }
            set {
                this.editable = value;
            }
        }

        private void ComprobanteRelacionadoControl_Load(object sender, EventArgs e) {
            this.gridDocumentos.DataGridCommon();
        }

        public void Start() {
            this.catalogoRelacionesCfdi = new RelacionCFDICatalogo();
            this.catalogoRelacionesCfdi.Load();
            
            this.TipoRelacion.ComboBox.DataSource = this.catalogoRelacionesCfdi.Items;
            this.TipoRelacion.ComboBox.DisplayMember = "Descripcion";
            this.TipoRelacion.ComboBox.ValueMember = "Clave";
        }

        private void Incluir_Click(object sender, EventArgs e) {
            this.Start();
        }
    }
}
