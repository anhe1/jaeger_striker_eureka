﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class CartaPorteControl : UserControl {
        
        protected IClaveTransporteCatalogo transporteCatalogo;
        protected IClaveClaveUnidadPesoCatalogo claveClaveUnidadPesoCatalogo;
        public CartaPorteDetailModel _CartaPorte;
        public CartaPorteControl() {
            InitializeComponent();
        }

        public CartaPorteControl(CartaPorteDetailModel model) {
            InitializeComponent();
            if (model == null) {
                this._CartaPorte = new CartaPorteDetailModel();
            } else {
                this._CartaPorte = model;
            }
        }

        private void CartaPorteControl_Load(object sender, EventArgs e) {
            this.gridUbicaciones.DataGridCommon();
            this.gridMercancias.DataGridCommon();
            this.gridTransporte.DataGridCommon();

            this.TUbicacion.ShowNuevo = true;
            this.TUbicacion.ShowRemover = true;
            this.TUbicacion.ShowEditar = true;

            this.TMercancia.ShowNuevo = true;
            this.TMercancia.ShowRemover = true;
            this.TMercancia.ShowEditar = true;
            this.TMercancia.ShowHerramientas = true;
            this.TMercancia.ShowImprimir = true;
            this.TMercancia.Imprimir.Text = "Duplicar";
            this.TMercancia.Imprimir.Click += this.TMercancia_Duplicar_Click;

            this.TTransporte.ShowNuevo = true;
            this.TTransporte.ShowRemover = true;
            this.TTransporte.ShowEditar = true;

            this.TranspInternac.DataSource = ComprobanteCommonService.GetTipoTranporte();
            this.TranspInternac.DisplayMember = "Descripcion";
            this.TranspInternac.ValueMember = "Id";

            this.EntradaSalidaMerc.DisplayMember = "Descripcion";
            this.EntradaSalidaMerc.ValueMember = "Id";
            this.EntradaSalidaMerc.DataSource = ComprobanteCommonService.GetCartaPorteEntradaSalidaMercs();

            this.transporteCatalogo = new TransporteCatalogo();
            this.transporteCatalogo.Load();

            this.ViaEntradaSalida.DisplayMember = "Descriptor";
            this.ViaEntradaSalida.ValueMember = "Clave";
            this.ViaEntradaSalida.DataSource = this.transporteCatalogo.Items;

            this.AutotransporteControl.Incluir.CheckedChanged += Incluir_CheckedChanged;
            this.AutotransporteControl.TRemolque.ButtonNuevo_Click += TRemolque_ButtonNuevo_Click;

            this.claveClaveUnidadPesoCatalogo = new UnidadPesoCatalogo();
            this.claveClaveUnidadPesoCatalogo.Load();
            this.UnidadPeso.DisplayMember = "Descriptor";
            this.UnidadPeso.ValueMember = "Clave";
            this.UnidadPeso.DataSource = this.claveClaveUnidadPesoCatalogo.Items;
        }

        private void TRemolque_ButtonNuevo_Click(object sender, EventArgs e) {
            this._CartaPorte.Mercancias.Autotransporte.Remolques.Add(new CartaPorteMercanciasAutotransporteRemolque());
        }

        private void TUbicacion_Nuevo_Click(object sender, EventArgs e) {
            var _ubicacion = new CartaPorteUbicacionControl(null);
            _ubicacion.ViaEntradaSalida = this._CartaPorte.ViaEntradaSalida;
            _ubicacion.CartaPorteUbicacionAdd += Ubicacion_UbicacionAdd;
            _ubicacion.ShowDialog(this);
        }

        private void TUbicacion_Editar_Click(object sender, EventArgs e) {
            if (this.gridUbicaciones.CurrentRow != null) {
                var _seleccionado = this.gridUbicaciones.CurrentRow.DataBoundItem as CartaPorteUbicacion;
                if (_seleccionado != null) {
                    var _editar = new CartaPorteUbicacionControl(_seleccionado);
                    _editar.ViaEntradaSalida = this._CartaPorte.ViaEntradaSalida;
                    _editar.CartaPorteUbicacionEdit += Ubicacion_UbicacionEdit;
                    _editar.ShowDialog(this);
                }
            }
        }

        private void TUbicacion_Remover_Click(object sender, EventArgs e) {
            if (this.gridUbicaciones.CurrentRow != null) {
                if (MessageBox.Show(this, "¿Esta seguro de remover el elemento?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                    var _seleccionado = this.gridUbicaciones.CurrentRow.DataBoundItem as CartaPorteUbicacion;
                    if (_seleccionado != null) {
                        this._CartaPorte.Ubicaciones.Remove(_seleccionado);
                    }
                }
            }
        }

        private void Ubicacion_UbicacionEdit(object sender, CartaPorteUbicacion e) {
            if (e != null) {

            }
        }

        private void Ubicacion_UbicacionAdd(object sender, CartaPorteUbicacion e) {
            if (e != null) {
                this._CartaPorte.Ubicaciones.Add(e);
            }
        }

        private void TMercancia_Nuevo_Click(object sender, EventArgs e) {
            var _Mercancia = new CartaPorteMercanciaControl(null);
            _Mercancia.Add += Mercancia_Add;
            _Mercancia.ShowDialog(this);
        }

        private void Mercancia_Add(object sender, CartaPorteMercanciasMercancia e) {
            if (e != null) {
                this._CartaPorte.Mercancias.Mercancia.Add(e);
            }
        }

        private void TMercancia_Editar_Click(object sender, EventArgs e) {
            if (this.gridMercancias.CurrentRow != null) {
                var _seleccionado = this.gridMercancias.CurrentRow.DataBoundItem as CartaPorteMercanciasMercancia;
                if (_seleccionado != null) {
                    var _editar = new CartaPorteMercanciaControl(_seleccionado);
                    _editar.ShowDialog(this);
                }
            }
        }

        private void TMercancia_Remover_Click(object sender, EventArgs e) {
            if (this.gridMercancias.CurrentRow != null) {
                if (MessageBox.Show(this, "¿Esta seguro de remover el elemento?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                    var _seleccionado = this.gridMercancias.CurrentRow.DataBoundItem as CartaPorteMercanciasMercancia;
                    if (_seleccionado != null) {
                        this._CartaPorte.Mercancias.Mercancia.Remove(_seleccionado);
                    }
                }
            }
        }

        private void TMercancia_Duplicar_Click(object sender, EventArgs eventArgs) {
            if (this.gridMercancias.CurrentRow != null) {
                var _seleccionado = this.gridMercancias.CurrentRow.DataBoundItem as CartaPorteMercanciasMercancia;
                if (_seleccionado != null) {
                    this._CartaPorte.Mercancias.Mercancia.Add(_seleccionado.Clone());
                }
            }
        }

        private void TTransporte_Nuevo_Click(object sender, EventArgs e) {
            var _figuraTransporte = new CartaPorteTiposFiguraControl();
            _figuraTransporte.Add += FiguraTransporte_Add;
            _figuraTransporte.ShowDialog(this);
        }

        private void FiguraTransporte_Add(object sender, CartaPorteTiposFigura e) {
            if (e != null) {
                this._CartaPorte.FiguraTransporte.Add(e);
            }
        }

        private void TTransporte_Editar_Click(object sender, EventArgs e) {
            if (this.gridTransporte.CurrentRow != null) {
                var _seleccionado = this.gridTransporte.CurrentRow.DataBoundItem as CartaPorteTiposFigura;
                if (_seleccionado != null) {
                    var _figuraTransporte = new CartaPorteTiposFiguraControl(_seleccionado);
                    _figuraTransporte.Add += FiguraTransporte_Add;
                    _figuraTransporte.ShowDialog(this);
                }
            }
        }

        private void TTransporte_Remover_Click(object sender, EventArgs e) {
            if (this.gridTransporte.CurrentRow != null) {
                this.gridTransporte.Rows.Remove(this.gridTransporte.CurrentRow);
            }
        }

        public virtual void CreateBinding() {
            this.TranspInternac.DataBindings.Clear();
            this.TranspInternac.DataBindings.Add("SelectedValue", this._CartaPorte, "TranspInternac", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ViaEntradaSalida.DataBindings.Clear();
            this.ViaEntradaSalida.DataBindings.Add("SelectedValue", this._CartaPorte, "ViaEntradaSalida", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TotalDistRec.DataBindings.Clear();
            this.TotalDistRec.DataBindings.Add("Value", this._CartaPorte, "TotalDistRec", true, DataSourceUpdateMode.OnPropertyChanged);

            this.EntradaSalidaMerc.DataBindings.Clear();
            this.EntradaSalidaMerc.DataBindings.Add("SelectedValue", this._CartaPorte, "EntradaSalidaMerc", true, DataSourceUpdateMode.OnPropertyChanged);
            this.EntradaSalidaMerc.DataBindings.Add("Enabled", this._CartaPorte, "TranspInternac", true, DataSourceUpdateMode.OnPropertyChanged);
            this.PaisOrigenDestino.DataBindings.Clear();
            this.PaisOrigenDestino.DataBindings.Add("Text", this._CartaPorte, "PaisOrigenDestino", true, DataSourceUpdateMode.OnPropertyChanged);
            this.PaisOrigenDestino.DataBindings.Add("Enabled", this._CartaPorte, "TranspInternac", true, DataSourceUpdateMode.OnPropertyChanged);

            this.UnidadPeso.DataBindings.Clear();
            this.UnidadPeso.DataBindings.Add("SelectedValue", this._CartaPorte.Mercancias, "UnidadPeso", true, DataSourceUpdateMode.OnPropertyChanged);

            this.PesoBrutoTotal.DataBindings.Clear();
            this.PesoBrutoTotal.DataBindings.Add("Text", this._CartaPorte.Mercancias, "PesoBrutoTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.PesoNetoTotal.DataBindings.Clear();
            this.PesoNetoTotal.DataBindings.Add("Text", this._CartaPorte.Mercancias, "PesoNetoTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CargoPorTasacion.DataBindings.Clear();
            this.CargoPorTasacion.DataBindings.Add("Text", this._CartaPorte.Mercancias, "CargoPorTasacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumTotalMercancias.DataBindings.Clear();
            this.NumTotalMercancias.DataBindings.Add("Text", this._CartaPorte.Mercancias, "NumTotalMercancias", true, DataSourceUpdateMode.OnPropertyChanged);

            this.gridUbicaciones.DataSource = this._CartaPorte;
            this.gridMercancias.DataSource = this._CartaPorte.Mercancias;
            this.gridTransporte.DataSource = this._CartaPorte;
            this.gridUbicaciones.DataMember = "Ubicaciones";
            this.gridMercancias.DataMember = "Mercancia";
            this.gridTransporte.DataMember = "FiguraTransporte";


            if (this._CartaPorte.Mercancias.Autotransporte != null) {
                this.AutotransporteControl.Incluir.Checked = true;
                this.CreateBindingAutoTransporte();
            }
        }

        public virtual void CreateBindingAutoTransporte() {
            this.AutotransporteControl.PermSCT.DataBindings.Clear();
            this.AutotransporteControl.PermSCT.DataBindings.Add("SelectedValue", this._CartaPorte.Mercancias.Autotransporte, "PermSCT", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AutotransporteControl.NumPermisoSCT.DataBindings.Clear();
            this.AutotransporteControl.NumPermisoSCT.DataBindings.Add("Text", this._CartaPorte.Mercancias.Autotransporte, "NumPermisoSCT", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AutotransporteControl.ConfigVehicular.DataBindings.Clear();
            this.AutotransporteControl.ConfigVehicular.DataBindings.Add("SelectedValue", this._CartaPorte.Mercancias.Autotransporte.IdentificacionVehicular, "ConfigVehicular", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AutotransporteControl.PlacaVM.DataBindings.Clear();
            this.AutotransporteControl.PlacaVM.DataBindings.Add("Text", this._CartaPorte.Mercancias.Autotransporte.IdentificacionVehicular, "PlacaVM", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AutotransporteControl.AnioModeloVM.DataBindings.Clear();
            this.AutotransporteControl.AnioModeloVM.DataBindings.Add("Text", this._CartaPorte.Mercancias.Autotransporte.IdentificacionVehicular, "AnioModeloVM", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.AutotransporteControl.AseguraRespCivil.DataBindings.Clear();
            this.AutotransporteControl.AseguraRespCivil.DataBindings.Add("Text", this._CartaPorte.Mercancias.Autotransporte.Seguros, "AseguraRespCivil", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AutotransporteControl.PolizaRespCivil.DataBindings.Clear();
            this.AutotransporteControl.PolizaRespCivil.DataBindings.Add("Text", this._CartaPorte.Mercancias.Autotransporte.Seguros, "PolizaRespCivil", true, DataSourceUpdateMode.OnPropertyChanged);

            this.AutotransporteControl.AseguraMedAmbiente.DataBindings.Clear();
            this.AutotransporteControl.AseguraMedAmbiente.DataBindings.Add("Text", this._CartaPorte.Mercancias.Autotransporte.Seguros, "AseguraMedAmbiente", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AutotransporteControl.PolizaMedAmbiente.DataBindings.Clear();
            this.AutotransporteControl.PolizaMedAmbiente.DataBindings.Add("Text", this._CartaPorte.Mercancias.Autotransporte.Seguros, "PolizaMedAmbiente", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.AutotransporteControl.AseguraCarga.DataBindings.Clear();
            this.AutotransporteControl.AseguraCarga.DataBindings.Add("Text", this._CartaPorte.Mercancias.Autotransporte.Seguros, "AseguraCarga", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AutotransporteControl.PolizaCarga.DataBindings.Clear();
            this.AutotransporteControl.PolizaCarga.DataBindings.Add("Text", this._CartaPorte.Mercancias.Autotransporte.Seguros, "PolizaCarga", true, DataSourceUpdateMode.OnPropertyChanged);

            this.AutotransporteControl.PrimaSeguro.DataBindings.Clear();
            this.AutotransporteControl.PrimaSeguro.DataBindings.Add("Text", this._CartaPorte.Mercancias.Autotransporte.Seguros, "PrimaSeguro", true, DataSourceUpdateMode.OnPropertyChanged);

            this.AutotransporteControl.gridRemolques.DataSource = this._CartaPorte.Mercancias.Autotransporte.Remolques;
        }

        private void Incluir_CheckedChanged(object sender, EventArgs e) {
            if (this._CartaPorte.Mercancias.Autotransporte == null) {
                this._CartaPorte.Mercancias.Autotransporte = new CartaPorteMercanciasAutotransporte();
                this.CreateBindingAutoTransporte();
            }
        }
    }

}
