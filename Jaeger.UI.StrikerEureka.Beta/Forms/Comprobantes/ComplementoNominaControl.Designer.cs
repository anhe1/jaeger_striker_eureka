﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class ComplementoNominaControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gNomina = new System.Windows.Forms.GroupBox();
            this.TotalOtrosPagos = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TotalDeducciones = new System.Windows.Forms.NumericUpDown();
            this.FechaFinalPago = new System.Windows.Forms.DateTimePicker();
            this.labelTotalDeducciones = new System.Windows.Forms.Label();
            this.TipoNomina = new System.Windows.Forms.ComboBox();
            this.TotalPercepciones = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.labelTotalPercepciones = new System.Windows.Forms.Label();
            this.FechaPago = new System.Windows.Forms.DateTimePicker();
            this.NumDiasPagados = new System.Windows.Forms.NumericUpDown();
            this.FechaInicialPago = new System.Windows.Forms.DateTimePicker();
            this.labelNumDiasPagados = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TabPercepciones = new System.Windows.Forms.TabPage();
            this.TabPercepcion1 = new System.Windows.Forms.TabControl();
            this.TabPercepcion = new System.Windows.Forms.TabPage();
            this.gridPercepciones = new System.Windows.Forms.DataGridView();
            this.TipoPercepcion = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Clave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Concepto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImporteGravado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImporteExento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TPercepcion = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TabJubilacionPensionRetiro = new System.Windows.Forms.TabPage();
            this.TabSeparacionIndemnizacion = new System.Windows.Forms.TabPage();
            this.TotalesPercepciones = new System.Windows.Forms.Panel();
            this.TotalExento = new System.Windows.Forms.NumericUpDown();
            this.labelTotalExento = new System.Windows.Forms.Label();
            this.TotalGravado = new System.Windows.Forms.NumericUpDown();
            this.labelTotalGravado = new System.Windows.Forms.Label();
            this.TotalJubilacionPensionRetiro = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.TotalSeparacionIndemnizacion = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.TotalSueldos = new System.Windows.Forms.NumericUpDown();
            this.labelTotalSueldos = new System.Windows.Forms.Label();
            this.TabDeducciones = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TotalImpuestosRetenidos = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.TotalOtrasDeducciones = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.gridDeducciones = new System.Windows.Forms.DataGridView();
            this.TipoDeduccion = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Clave2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Concepto2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TDeduccion = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TabOtrosPagos = new System.Windows.Forms.TabPage();
            this.gridOtrosPagos = new System.Windows.Forms.DataGridView();
            this.TOtrosPagos = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.SubsidioCausado = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.TabIncapacidades = new System.Windows.Forms.TabPage();
            this.gridIncapacidades = new System.Windows.Forms.DataGridView();
            this.TIncapacidad = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TipoOtroPago = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.OtroPagoClave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OtroPagoConcepto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OtroPagoImporte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiasIncapacidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoIncapacidad = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ImporteMonetario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.complementoNominaJubilacionPensionRetiroControl1 = new Jaeger.UI.Forms.Comprobantes.ComplementoNominaJubilacionPensionRetiroControl();
            this.complementoNominaSeparacionIndemnizacionControl1 = new Jaeger.UI.Forms.Comprobantes.ComplementoNominaSeparacionIndemnizacionControl();
            this.ReceptorControl = new Jaeger.UI.Forms.Comprobantes.ComplementoNominaReceptorControl();
            this.gNomina.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalOtrosPagos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDeducciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalPercepciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumDiasPagados)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.TabPercepciones.SuspendLayout();
            this.TabPercepcion1.SuspendLayout();
            this.TabPercepcion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPercepciones)).BeginInit();
            this.TabJubilacionPensionRetiro.SuspendLayout();
            this.TabSeparacionIndemnizacion.SuspendLayout();
            this.TotalesPercepciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalExento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalGravado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalJubilacionPensionRetiro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSeparacionIndemnizacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSueldos)).BeginInit();
            this.TabDeducciones.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalImpuestosRetenidos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalOtrasDeducciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDeducciones)).BeginInit();
            this.TabOtrosPagos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridOtrosPagos)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SubsidioCausado)).BeginInit();
            this.TabIncapacidades.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridIncapacidades)).BeginInit();
            this.SuspendLayout();
            // 
            // gNomina
            // 
            this.gNomina.Controls.Add(this.TotalOtrosPagos);
            this.gNomina.Controls.Add(this.label4);
            this.gNomina.Controls.Add(this.label6);
            this.gNomina.Controls.Add(this.label1);
            this.gNomina.Controls.Add(this.TotalDeducciones);
            this.gNomina.Controls.Add(this.FechaFinalPago);
            this.gNomina.Controls.Add(this.labelTotalDeducciones);
            this.gNomina.Controls.Add(this.TipoNomina);
            this.gNomina.Controls.Add(this.TotalPercepciones);
            this.gNomina.Controls.Add(this.label3);
            this.gNomina.Controls.Add(this.labelTotalPercepciones);
            this.gNomina.Controls.Add(this.FechaPago);
            this.gNomina.Controls.Add(this.NumDiasPagados);
            this.gNomina.Controls.Add(this.FechaInicialPago);
            this.gNomina.Controls.Add(this.labelNumDiasPagados);
            this.gNomina.Controls.Add(this.label2);
            this.gNomina.Dock = System.Windows.Forms.DockStyle.Top;
            this.gNomina.Location = new System.Drawing.Point(0, 0);
            this.gNomina.Name = "gNomina";
            this.gNomina.Size = new System.Drawing.Size(1140, 71);
            this.gNomina.TabIndex = 0;
            this.gNomina.TabStop = false;
            // 
            // TotalOtrosPagos
            // 
            this.TotalOtrosPagos.DecimalPlaces = 2;
            this.TotalOtrosPagos.Location = new System.Drawing.Point(736, 41);
            this.TotalOtrosPagos.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.TotalOtrosPagos.Name = "TotalOtrosPagos";
            this.TotalOtrosPagos.Size = new System.Drawing.Size(100, 20);
            this.TotalOtrosPagos.TabIndex = 16;
            this.TotalOtrosPagos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(817, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Fecha Final Pago:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(641, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Total Otros Pagos:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Tipo de Nómina:";
            // 
            // TotalDeducciones
            // 
            this.TotalDeducciones.DecimalPlaces = 2;
            this.TotalDeducciones.Location = new System.Drawing.Point(534, 41);
            this.TotalDeducciones.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.TotalDeducciones.Name = "TotalDeducciones";
            this.TotalDeducciones.Size = new System.Drawing.Size(100, 20);
            this.TotalDeducciones.TabIndex = 14;
            this.TotalDeducciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // FechaFinalPago
            // 
            this.FechaFinalPago.Location = new System.Drawing.Point(916, 15);
            this.FechaFinalPago.Name = "FechaFinalPago";
            this.FechaFinalPago.Size = new System.Drawing.Size(200, 20);
            this.FechaFinalPago.TabIndex = 10;
            // 
            // labelTotalDeducciones
            // 
            this.labelTotalDeducciones.AutoSize = true;
            this.labelTotalDeducciones.Location = new System.Drawing.Point(432, 45);
            this.labelTotalDeducciones.Name = "labelTotalDeducciones";
            this.labelTotalDeducciones.Size = new System.Drawing.Size(100, 13);
            this.labelTotalDeducciones.TabIndex = 13;
            this.labelTotalDeducciones.Text = "Total Deducciones:";
            // 
            // TipoNomina
            // 
            this.TipoNomina.Location = new System.Drawing.Point(97, 15);
            this.TipoNomina.Name = "TipoNomina";
            this.TipoNomina.Size = new System.Drawing.Size(109, 21);
            this.TipoNomina.TabIndex = 5;
            // 
            // TotalPercepciones
            // 
            this.TotalPercepciones.DecimalPlaces = 2;
            this.TotalPercepciones.Location = new System.Drawing.Point(326, 41);
            this.TotalPercepciones.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.TotalPercepciones.Name = "TotalPercepciones";
            this.TotalPercepciones.Size = new System.Drawing.Size(100, 20);
            this.TotalPercepciones.TabIndex = 12;
            this.TotalPercepciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(507, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Fecha Inicial Pago:";
            // 
            // labelTotalPercepciones
            // 
            this.labelTotalPercepciones.AutoSize = true;
            this.labelTotalPercepciones.Location = new System.Drawing.Point(224, 45);
            this.labelTotalPercepciones.Name = "labelTotalPercepciones";
            this.labelTotalPercepciones.Size = new System.Drawing.Size(102, 13);
            this.labelTotalPercepciones.TabIndex = 11;
            this.labelTotalPercepciones.Text = "Total Percepciones:";
            // 
            // FechaPago
            // 
            this.FechaPago.Location = new System.Drawing.Point(301, 15);
            this.FechaPago.Name = "FechaPago";
            this.FechaPago.Size = new System.Drawing.Size(200, 20);
            this.FechaPago.TabIndex = 6;
            // 
            // NumDiasPagados
            // 
            this.NumDiasPagados.Location = new System.Drawing.Point(118, 41);
            this.NumDiasPagados.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.NumDiasPagados.Name = "NumDiasPagados";
            this.NumDiasPagados.Size = new System.Drawing.Size(88, 20);
            this.NumDiasPagados.TabIndex = 10;
            this.NumDiasPagados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // FechaInicialPago
            // 
            this.FechaInicialPago.Location = new System.Drawing.Point(611, 15);
            this.FechaInicialPago.Name = "FechaInicialPago";
            this.FechaInicialPago.Size = new System.Drawing.Size(200, 20);
            this.FechaInicialPago.TabIndex = 8;
            // 
            // labelNumDiasPagados
            // 
            this.labelNumDiasPagados.AutoSize = true;
            this.labelNumDiasPagados.Location = new System.Drawing.Point(6, 45);
            this.labelNumDiasPagados.Name = "labelNumDiasPagados";
            this.labelNumDiasPagados.Size = new System.Drawing.Size(106, 13);
            this.labelNumDiasPagados.TabIndex = 9;
            this.labelNumDiasPagados.Text = "Núm. Días Pagados:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(212, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Fecha de Pago:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.TabPercepciones);
            this.tabControl1.Controls.Add(this.TabDeducciones);
            this.tabControl1.Controls.Add(this.TabOtrosPagos);
            this.tabControl1.Controls.Add(this.TabIncapacidades);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 192);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1140, 329);
            this.tabControl1.TabIndex = 2;
            // 
            // TabPercepciones
            // 
            this.TabPercepciones.Controls.Add(this.TabPercepcion1);
            this.TabPercepciones.Controls.Add(this.TotalesPercepciones);
            this.TabPercepciones.Location = new System.Drawing.Point(4, 22);
            this.TabPercepciones.Name = "TabPercepciones";
            this.TabPercepciones.Size = new System.Drawing.Size(1132, 303);
            this.TabPercepciones.TabIndex = 0;
            this.TabPercepciones.Text = "Percepciones";
            this.TabPercepciones.UseVisualStyleBackColor = true;
            // 
            // TabPercepcion1
            // 
            this.TabPercepcion1.Controls.Add(this.TabPercepcion);
            this.TabPercepcion1.Controls.Add(this.TabJubilacionPensionRetiro);
            this.TabPercepcion1.Controls.Add(this.TabSeparacionIndemnizacion);
            this.TabPercepcion1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabPercepcion1.Location = new System.Drawing.Point(0, 0);
            this.TabPercepcion1.Name = "TabPercepcion1";
            this.TabPercepcion1.SelectedIndex = 0;
            this.TabPercepcion1.Size = new System.Drawing.Size(1132, 276);
            this.TabPercepcion1.TabIndex = 1;
            // 
            // TabPercepcion
            // 
            this.TabPercepcion.Controls.Add(this.gridPercepciones);
            this.TabPercepcion.Controls.Add(this.TPercepcion);
            this.TabPercepcion.Location = new System.Drawing.Point(4, 22);
            this.TabPercepcion.Name = "TabPercepcion";
            this.TabPercepcion.Size = new System.Drawing.Size(1124, 250);
            this.TabPercepcion.TabIndex = 0;
            this.TabPercepcion.Text = "Percepciones";
            this.TabPercepcion.UseVisualStyleBackColor = true;
            // 
            // gridPercepciones
            // 
            this.gridPercepciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPercepciones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TipoPercepcion,
            this.Clave,
            this.Concepto,
            this.ImporteGravado,
            this.ImporteExento});
            this.gridPercepciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPercepciones.Location = new System.Drawing.Point(0, 25);
            this.gridPercepciones.Name = "gridPercepciones";
            this.gridPercepciones.Size = new System.Drawing.Size(1124, 225);
            this.gridPercepciones.TabIndex = 1;
            // 
            // TipoPercepcion
            // 
            this.TipoPercepcion.DataPropertyName = "TipoPercepcion";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TipoPercepcion.DefaultCellStyle = dataGridViewCellStyle19;
            this.TipoPercepcion.HeaderText = "Tipo";
            this.TipoPercepcion.Name = "TipoPercepcion";
            this.TipoPercepcion.Width = 75;
            // 
            // Clave
            // 
            this.Clave.DataPropertyName = "Clave";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Clave.DefaultCellStyle = dataGridViewCellStyle20;
            this.Clave.HeaderText = "Clave";
            this.Clave.Name = "Clave";
            this.Clave.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Clave.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Clave.Width = 75;
            // 
            // Concepto
            // 
            this.Concepto.DataPropertyName = "Concepto";
            this.Concepto.HeaderText = "Concepto";
            this.Concepto.Name = "Concepto";
            this.Concepto.Width = 250;
            // 
            // ImporteGravado
            // 
            this.ImporteGravado.DataPropertyName = "ImporteGravado";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle21.Format = "N2";
            this.ImporteGravado.DefaultCellStyle = dataGridViewCellStyle21;
            this.ImporteGravado.HeaderText = "Importe Gravado";
            this.ImporteGravado.Name = "ImporteGravado";
            this.ImporteGravado.Width = 115;
            // 
            // ImporteExento
            // 
            this.ImporteExento.DataPropertyName = "ImporteExento";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle22.Format = "N2";
            this.ImporteExento.DefaultCellStyle = dataGridViewCellStyle22;
            this.ImporteExento.HeaderText = "Importe Exento";
            this.ImporteExento.Name = "ImporteExento";
            this.ImporteExento.Width = 115;
            // 
            // TPercepcion
            // 
            this.TPercepcion.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPercepcion.Etiqueta = "Percepción";
            this.TPercepcion.Location = new System.Drawing.Point(0, 0);
            this.TPercepcion.Name = "TPercepcion";
            this.TPercepcion.ShowActualizar = false;
            this.TPercepcion.ShowCerrar = false;
            this.TPercepcion.ShowEditar = false;
            this.TPercepcion.ShowGuardar = false;
            this.TPercepcion.ShowHerramientas = false;
            this.TPercepcion.ShowImprimir = false;
            this.TPercepcion.ShowNuevo = true;
            this.TPercepcion.ShowRemover = true;
            this.TPercepcion.Size = new System.Drawing.Size(1124, 25);
            this.TPercepcion.TabIndex = 0;
            // 
            // TabJubilacionPensionRetiro
            // 
            this.TabJubilacionPensionRetiro.Controls.Add(this.complementoNominaJubilacionPensionRetiroControl1);
            this.TabJubilacionPensionRetiro.Location = new System.Drawing.Point(4, 22);
            this.TabJubilacionPensionRetiro.Name = "TabJubilacionPensionRetiro";
            this.TabJubilacionPensionRetiro.Padding = new System.Windows.Forms.Padding(3);
            this.TabJubilacionPensionRetiro.Size = new System.Drawing.Size(1124, 250);
            this.TabJubilacionPensionRetiro.TabIndex = 1;
            this.TabJubilacionPensionRetiro.Text = "Jubilacion / Pension / Retiro";
            this.TabJubilacionPensionRetiro.UseVisualStyleBackColor = true;
            // 
            // TabSeparacionIndemnizacion
            // 
            this.TabSeparacionIndemnizacion.Controls.Add(this.complementoNominaSeparacionIndemnizacionControl1);
            this.TabSeparacionIndemnizacion.Location = new System.Drawing.Point(4, 22);
            this.TabSeparacionIndemnizacion.Name = "TabSeparacionIndemnizacion";
            this.TabSeparacionIndemnizacion.Padding = new System.Windows.Forms.Padding(3);
            this.TabSeparacionIndemnizacion.Size = new System.Drawing.Size(1124, 250);
            this.TabSeparacionIndemnizacion.TabIndex = 2;
            this.TabSeparacionIndemnizacion.Text = "Separacion Indemnización";
            this.TabSeparacionIndemnizacion.UseVisualStyleBackColor = true;
            // 
            // TotalesPercepciones
            // 
            this.TotalesPercepciones.Controls.Add(this.TotalExento);
            this.TotalesPercepciones.Controls.Add(this.labelTotalExento);
            this.TotalesPercepciones.Controls.Add(this.TotalGravado);
            this.TotalesPercepciones.Controls.Add(this.labelTotalGravado);
            this.TotalesPercepciones.Controls.Add(this.TotalJubilacionPensionRetiro);
            this.TotalesPercepciones.Controls.Add(this.label8);
            this.TotalesPercepciones.Controls.Add(this.TotalSeparacionIndemnizacion);
            this.TotalesPercepciones.Controls.Add(this.label7);
            this.TotalesPercepciones.Controls.Add(this.TotalSueldos);
            this.TotalesPercepciones.Controls.Add(this.labelTotalSueldos);
            this.TotalesPercepciones.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TotalesPercepciones.Location = new System.Drawing.Point(0, 276);
            this.TotalesPercepciones.Name = "TotalesPercepciones";
            this.TotalesPercepciones.Size = new System.Drawing.Size(1132, 27);
            this.TotalesPercepciones.TabIndex = 0;
            // 
            // TotalExento
            // 
            this.TotalExento.DecimalPlaces = 2;
            this.TotalExento.Location = new System.Drawing.Point(1017, 3);
            this.TotalExento.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.TotalExento.Name = "TotalExento";
            this.TotalExento.Size = new System.Drawing.Size(100, 20);
            this.TotalExento.TabIndex = 24;
            this.TotalExento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelTotalExento
            // 
            this.labelTotalExento.AutoSize = true;
            this.labelTotalExento.Location = new System.Drawing.Point(941, 7);
            this.labelTotalExento.Name = "labelTotalExento";
            this.labelTotalExento.Size = new System.Drawing.Size(70, 13);
            this.labelTotalExento.TabIndex = 23;
            this.labelTotalExento.Text = "Total Exento:";
            // 
            // TotalGravado
            // 
            this.TotalGravado.DecimalPlaces = 2;
            this.TotalGravado.Location = new System.Drawing.Point(837, 3);
            this.TotalGravado.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.TotalGravado.Name = "TotalGravado";
            this.TotalGravado.Size = new System.Drawing.Size(100, 20);
            this.TotalGravado.TabIndex = 22;
            this.TotalGravado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelTotalGravado
            // 
            this.labelTotalGravado.AutoSize = true;
            this.labelTotalGravado.Location = new System.Drawing.Point(753, 7);
            this.labelTotalGravado.Name = "labelTotalGravado";
            this.labelTotalGravado.Size = new System.Drawing.Size(78, 13);
            this.labelTotalGravado.TabIndex = 21;
            this.labelTotalGravado.Text = "Total Gravado:";
            // 
            // TotalJubilacionPensionRetiro
            // 
            this.TotalJubilacionPensionRetiro.DecimalPlaces = 2;
            this.TotalJubilacionPensionRetiro.Location = new System.Drawing.Point(647, 3);
            this.TotalJubilacionPensionRetiro.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.TotalJubilacionPensionRetiro.Name = "TotalJubilacionPensionRetiro";
            this.TotalJubilacionPensionRetiro.Size = new System.Drawing.Size(100, 20);
            this.TotalJubilacionPensionRetiro.TabIndex = 20;
            this.TotalJubilacionPensionRetiro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(469, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(172, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Total Jubilación / Pensión / Retiro:";
            // 
            // TotalSeparacionIndemnizacion
            // 
            this.TotalSeparacionIndemnizacion.DecimalPlaces = 2;
            this.TotalSeparacionIndemnizacion.Location = new System.Drawing.Point(364, 3);
            this.TotalSeparacionIndemnizacion.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.TotalSeparacionIndemnizacion.Name = "TotalSeparacionIndemnizacion";
            this.TotalSeparacionIndemnizacion.Size = new System.Drawing.Size(100, 20);
            this.TotalSeparacionIndemnizacion.TabIndex = 18;
            this.TotalSeparacionIndemnizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(193, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(170, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Total Separacion / Indemnizacion:";
            // 
            // TotalSueldos
            // 
            this.TotalSueldos.DecimalPlaces = 2;
            this.TotalSueldos.Location = new System.Drawing.Point(87, 3);
            this.TotalSueldos.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.TotalSueldos.Name = "TotalSueldos";
            this.TotalSueldos.Size = new System.Drawing.Size(100, 20);
            this.TotalSueldos.TabIndex = 16;
            this.TotalSueldos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelTotalSueldos
            // 
            this.labelTotalSueldos.AutoSize = true;
            this.labelTotalSueldos.Location = new System.Drawing.Point(9, 7);
            this.labelTotalSueldos.Name = "labelTotalSueldos";
            this.labelTotalSueldos.Size = new System.Drawing.Size(72, 13);
            this.labelTotalSueldos.TabIndex = 15;
            this.labelTotalSueldos.Text = "Total Sueldos";
            // 
            // TabDeducciones
            // 
            this.TabDeducciones.Controls.Add(this.panel1);
            this.TabDeducciones.Controls.Add(this.gridDeducciones);
            this.TabDeducciones.Controls.Add(this.TDeduccion);
            this.TabDeducciones.Location = new System.Drawing.Point(4, 22);
            this.TabDeducciones.Name = "TabDeducciones";
            this.TabDeducciones.Size = new System.Drawing.Size(1132, 303);
            this.TabDeducciones.TabIndex = 1;
            this.TabDeducciones.Text = "Deducciones";
            this.TabDeducciones.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.TotalImpuestosRetenidos);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.TotalOtrasDeducciones);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 276);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1132, 27);
            this.panel1.TabIndex = 2;
            // 
            // TotalImpuestosRetenidos
            // 
            this.TotalImpuestosRetenidos.DecimalPlaces = 2;
            this.TotalImpuestosRetenidos.Location = new System.Drawing.Point(383, 3);
            this.TotalImpuestosRetenidos.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.TotalImpuestosRetenidos.Name = "TotalImpuestosRetenidos";
            this.TotalImpuestosRetenidos.Size = new System.Drawing.Size(100, 20);
            this.TotalImpuestosRetenidos.TabIndex = 18;
            this.TotalImpuestosRetenidos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(245, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(136, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "Total Impuestos Retenidos:";
            // 
            // TotalOtrasDeducciones
            // 
            this.TotalOtrasDeducciones.DecimalPlaces = 2;
            this.TotalOtrasDeducciones.Location = new System.Drawing.Point(139, 3);
            this.TotalOtrasDeducciones.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.TotalOtrasDeducciones.Name = "TotalOtrasDeducciones";
            this.TotalOtrasDeducciones.Size = new System.Drawing.Size(100, 20);
            this.TotalOtrasDeducciones.TabIndex = 16;
            this.TotalOtrasDeducciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 7);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(128, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "Total Otras Deducciones:";
            // 
            // gridDeducciones
            // 
            this.gridDeducciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDeducciones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TipoDeduccion,
            this.Clave2,
            this.Concepto2,
            this.Importe});
            this.gridDeducciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDeducciones.Location = new System.Drawing.Point(0, 25);
            this.gridDeducciones.Name = "gridDeducciones";
            this.gridDeducciones.Size = new System.Drawing.Size(1132, 278);
            this.gridDeducciones.TabIndex = 4;
            // 
            // TipoDeduccion
            // 
            this.TipoDeduccion.DataPropertyName = "TipoDeduccion";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TipoDeduccion.DefaultCellStyle = dataGridViewCellStyle23;
            this.TipoDeduccion.HeaderText = "Tipo";
            this.TipoDeduccion.Name = "TipoDeduccion";
            this.TipoDeduccion.Width = 75;
            // 
            // Clave2
            // 
            this.Clave2.DataPropertyName = "Clave";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Clave2.DefaultCellStyle = dataGridViewCellStyle24;
            this.Clave2.HeaderText = "Clave";
            this.Clave2.Name = "Clave2";
            this.Clave2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Clave2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Clave2.Width = 75;
            // 
            // Concepto2
            // 
            this.Concepto2.DataPropertyName = "Concepto";
            this.Concepto2.HeaderText = "Concepto";
            this.Concepto2.Name = "Concepto2";
            this.Concepto2.Width = 250;
            // 
            // Importe
            // 
            this.Importe.DataPropertyName = "Importe";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle25.Format = "N2";
            this.Importe.DefaultCellStyle = dataGridViewCellStyle25;
            this.Importe.HeaderText = "Importe";
            this.Importe.Name = "Importe";
            this.Importe.Width = 115;
            // 
            // TDeduccion
            // 
            this.TDeduccion.Dock = System.Windows.Forms.DockStyle.Top;
            this.TDeduccion.Etiqueta = "Deducción";
            this.TDeduccion.Location = new System.Drawing.Point(0, 0);
            this.TDeduccion.Name = "TDeduccion";
            this.TDeduccion.ShowActualizar = false;
            this.TDeduccion.ShowCerrar = false;
            this.TDeduccion.ShowEditar = false;
            this.TDeduccion.ShowGuardar = false;
            this.TDeduccion.ShowHerramientas = false;
            this.TDeduccion.ShowImprimir = false;
            this.TDeduccion.ShowNuevo = false;
            this.TDeduccion.ShowRemover = false;
            this.TDeduccion.Size = new System.Drawing.Size(1132, 25);
            this.TDeduccion.TabIndex = 3;
            // 
            // TabOtrosPagos
            // 
            this.TabOtrosPagos.Controls.Add(this.gridOtrosPagos);
            this.TabOtrosPagos.Controls.Add(this.TOtrosPagos);
            this.TabOtrosPagos.Controls.Add(this.panel2);
            this.TabOtrosPagos.Location = new System.Drawing.Point(4, 22);
            this.TabOtrosPagos.Name = "TabOtrosPagos";
            this.TabOtrosPagos.Size = new System.Drawing.Size(1132, 303);
            this.TabOtrosPagos.TabIndex = 2;
            this.TabOtrosPagos.Text = "Otros Pagos";
            this.TabOtrosPagos.UseVisualStyleBackColor = true;
            // 
            // gridOtrosPagos
            // 
            this.gridOtrosPagos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridOtrosPagos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TipoOtroPago,
            this.OtroPagoClave,
            this.OtroPagoConcepto,
            this.OtroPagoImporte});
            this.gridOtrosPagos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridOtrosPagos.Location = new System.Drawing.Point(0, 25);
            this.gridOtrosPagos.Name = "gridOtrosPagos";
            this.gridOtrosPagos.Size = new System.Drawing.Size(1132, 239);
            this.gridOtrosPagos.TabIndex = 6;
            // 
            // TOtrosPagos
            // 
            this.TOtrosPagos.Dock = System.Windows.Forms.DockStyle.Top;
            this.TOtrosPagos.Etiqueta = "Otros Pagos";
            this.TOtrosPagos.Location = new System.Drawing.Point(0, 0);
            this.TOtrosPagos.Name = "TOtrosPagos";
            this.TOtrosPagos.ShowActualizar = false;
            this.TOtrosPagos.ShowCerrar = false;
            this.TOtrosPagos.ShowEditar = false;
            this.TOtrosPagos.ShowGuardar = false;
            this.TOtrosPagos.ShowHerramientas = false;
            this.TOtrosPagos.ShowImprimir = false;
            this.TOtrosPagos.ShowNuevo = false;
            this.TOtrosPagos.ShowRemover = false;
            this.TOtrosPagos.Size = new System.Drawing.Size(1132, 25);
            this.TOtrosPagos.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.SubsidioCausado);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 264);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1132, 39);
            this.panel2.TabIndex = 0;
            // 
            // SubsidioCausado
            // 
            this.SubsidioCausado.DecimalPlaces = 2;
            this.SubsidioCausado.Location = new System.Drawing.Point(167, 6);
            this.SubsidioCausado.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.SubsidioCausado.Name = "SubsidioCausado";
            this.SubsidioCausado.Size = new System.Drawing.Size(100, 20);
            this.SubsidioCausado.TabIndex = 20;
            this.SubsidioCausado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "SubsidioCausado";
            // 
            // TabIncapacidades
            // 
            this.TabIncapacidades.Controls.Add(this.gridIncapacidades);
            this.TabIncapacidades.Controls.Add(this.TIncapacidad);
            this.TabIncapacidades.Location = new System.Drawing.Point(4, 22);
            this.TabIncapacidades.Name = "TabIncapacidades";
            this.TabIncapacidades.Size = new System.Drawing.Size(1132, 303);
            this.TabIncapacidades.TabIndex = 3;
            this.TabIncapacidades.Text = "Incapacidades";
            this.TabIncapacidades.UseVisualStyleBackColor = true;
            // 
            // gridIncapacidades
            // 
            this.gridIncapacidades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridIncapacidades.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DiasIncapacidad,
            this.TipoIncapacidad,
            this.ImporteMonetario});
            this.gridIncapacidades.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridIncapacidades.Location = new System.Drawing.Point(0, 25);
            this.gridIncapacidades.Name = "gridIncapacidades";
            this.gridIncapacidades.Size = new System.Drawing.Size(1132, 278);
            this.gridIncapacidades.TabIndex = 8;
            // 
            // TIncapacidad
            // 
            this.TIncapacidad.Dock = System.Windows.Forms.DockStyle.Top;
            this.TIncapacidad.Etiqueta = "Incapacidad";
            this.TIncapacidad.Location = new System.Drawing.Point(0, 0);
            this.TIncapacidad.Name = "TIncapacidad";
            this.TIncapacidad.ShowActualizar = false;
            this.TIncapacidad.ShowCerrar = false;
            this.TIncapacidad.ShowEditar = false;
            this.TIncapacidad.ShowGuardar = false;
            this.TIncapacidad.ShowHerramientas = false;
            this.TIncapacidad.ShowImprimir = false;
            this.TIncapacidad.ShowNuevo = true;
            this.TIncapacidad.ShowRemover = true;
            this.TIncapacidad.Size = new System.Drawing.Size(1132, 25);
            this.TIncapacidad.TabIndex = 7;
            // 
            // TipoOtroPago
            // 
            this.TipoOtroPago.DataPropertyName = "TipoOtroPago";
            this.TipoOtroPago.HeaderText = "Tipo";
            this.TipoOtroPago.Name = "TipoOtroPago";
            // 
            // OtroPagoClave
            // 
            this.OtroPagoClave.DataPropertyName = "Clave";
            this.OtroPagoClave.HeaderText = "Clave";
            this.OtroPagoClave.Name = "OtroPagoClave";
            this.OtroPagoClave.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.OtroPagoClave.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // OtroPagoConcepto
            // 
            this.OtroPagoConcepto.DataPropertyName = "Concepto";
            this.OtroPagoConcepto.HeaderText = "Concepto";
            this.OtroPagoConcepto.Name = "OtroPagoConcepto";
            this.OtroPagoConcepto.Width = 250;
            // 
            // OtroPagoImporte
            // 
            this.OtroPagoImporte.DataPropertyName = "Importe";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle26.Format = "N2";
            this.OtroPagoImporte.DefaultCellStyle = dataGridViewCellStyle26;
            this.OtroPagoImporte.HeaderText = "Importe";
            this.OtroPagoImporte.Name = "OtroPagoImporte";
            this.OtroPagoImporte.Width = 115;
            // 
            // DiasIncapacidad
            // 
            this.DiasIncapacidad.DataPropertyName = "DiasIncapacidad";
            this.DiasIncapacidad.HeaderText = "DiasIncapacidad";
            this.DiasIncapacidad.Name = "DiasIncapacidad";
            this.DiasIncapacidad.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DiasIncapacidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // TipoIncapacidad
            // 
            this.TipoIncapacidad.DataPropertyName = "TipoIncapacidad";
            this.TipoIncapacidad.HeaderText = "TipoIncapacidad";
            this.TipoIncapacidad.Name = "TipoIncapacidad";
            // 
            // ImporteMonetario
            // 
            this.ImporteMonetario.DataPropertyName = "ImporteMonetario";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle27.Format = "N2";
            this.ImporteMonetario.DefaultCellStyle = dataGridViewCellStyle27;
            this.ImporteMonetario.HeaderText = "Importe";
            this.ImporteMonetario.Name = "ImporteMonetario";
            this.ImporteMonetario.Width = 115;
            // 
            // complementoNominaJubilacionPensionRetiroControl1
            // 
            this.complementoNominaJubilacionPensionRetiroControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.complementoNominaJubilacionPensionRetiroControl1.Location = new System.Drawing.Point(3, 3);
            this.complementoNominaJubilacionPensionRetiroControl1.Name = "complementoNominaJubilacionPensionRetiroControl1";
            this.complementoNominaJubilacionPensionRetiroControl1.Size = new System.Drawing.Size(1118, 244);
            this.complementoNominaJubilacionPensionRetiroControl1.TabIndex = 0;
            // 
            // complementoNominaSeparacionIndemnizacionControl1
            // 
            this.complementoNominaSeparacionIndemnizacionControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.complementoNominaSeparacionIndemnizacionControl1.Location = new System.Drawing.Point(3, 3);
            this.complementoNominaSeparacionIndemnizacionControl1.Name = "complementoNominaSeparacionIndemnizacionControl1";
            this.complementoNominaSeparacionIndemnizacionControl1.Size = new System.Drawing.Size(1118, 244);
            this.complementoNominaSeparacionIndemnizacionControl1.TabIndex = 0;
            // 
            // ReceptorControl
            // 
            this.ReceptorControl.Caption = "Complemento: Nómina";
            this.ReceptorControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.ReceptorControl.IsEditable = false;
            this.ReceptorControl.Location = new System.Drawing.Point(0, 71);
            this.ReceptorControl.Name = "ReceptorControl";
            this.ReceptorControl.Size = new System.Drawing.Size(1140, 121);
            this.ReceptorControl.TabIndex = 1;
            // 
            // ComplementoNominaControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.ReceptorControl);
            this.Controls.Add(this.gNomina);
            this.Name = "ComplementoNominaControl";
            this.Size = new System.Drawing.Size(1140, 521);
            this.Load += new System.EventHandler(this.ComplementoNominaControl_Load);
            this.gNomina.ResumeLayout(false);
            this.gNomina.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalOtrosPagos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDeducciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalPercepciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumDiasPagados)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.TabPercepciones.ResumeLayout(false);
            this.TabPercepcion1.ResumeLayout(false);
            this.TabPercepcion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPercepciones)).EndInit();
            this.TabJubilacionPensionRetiro.ResumeLayout(false);
            this.TabSeparacionIndemnizacion.ResumeLayout(false);
            this.TotalesPercepciones.ResumeLayout(false);
            this.TotalesPercepciones.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalExento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalGravado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalJubilacionPensionRetiro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSeparacionIndemnizacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSueldos)).EndInit();
            this.TabDeducciones.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalImpuestosRetenidos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalOtrasDeducciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDeducciones)).EndInit();
            this.TabOtrosPagos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridOtrosPagos)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SubsidioCausado)).EndInit();
            this.TabIncapacidades.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridIncapacidades)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gNomina;
        private System.Windows.Forms.Label label1;
        protected internal System.Windows.Forms.ComboBox TipoNomina;
        private System.Windows.Forms.Label label4;
        protected internal System.Windows.Forms.DateTimePicker FechaFinalPago;
        private System.Windows.Forms.Label label3;
        protected internal System.Windows.Forms.DateTimePicker FechaPago;
        protected internal System.Windows.Forms.DateTimePicker FechaInicialPago;
        private System.Windows.Forms.Label label2;
        protected internal System.Windows.Forms.NumericUpDown NumDiasPagados;
        private System.Windows.Forms.Label labelNumDiasPagados;
        protected internal System.Windows.Forms.NumericUpDown TotalOtrosPagos;
        private System.Windows.Forms.Label label6;
        protected internal System.Windows.Forms.NumericUpDown TotalDeducciones;
        private System.Windows.Forms.Label labelTotalDeducciones;
        protected internal System.Windows.Forms.NumericUpDown TotalPercepciones;
        private System.Windows.Forms.Label labelTotalPercepciones;
        private ComplementoNominaReceptorControl ReceptorControl;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TabPercepciones;
        private System.Windows.Forms.TabPage TabDeducciones;
        private System.Windows.Forms.TabPage TabOtrosPagos;
        private System.Windows.Forms.TabPage TabIncapacidades;
        private System.Windows.Forms.TabControl TabPercepcion1;
        private System.Windows.Forms.TabPage TabPercepcion;
        private System.Windows.Forms.DataGridView gridPercepciones;
        private Common.Forms.ToolBarStandarControl TPercepcion;
        private System.Windows.Forms.TabPage TabJubilacionPensionRetiro;
        private System.Windows.Forms.TabPage TabSeparacionIndemnizacion;
        private System.Windows.Forms.Panel TotalesPercepciones;
        protected internal System.Windows.Forms.NumericUpDown TotalExento;
        private System.Windows.Forms.Label labelTotalExento;
        protected internal System.Windows.Forms.NumericUpDown TotalGravado;
        private System.Windows.Forms.Label labelTotalGravado;
        protected internal System.Windows.Forms.NumericUpDown TotalJubilacionPensionRetiro;
        private System.Windows.Forms.Label label8;
        protected internal System.Windows.Forms.NumericUpDown TotalSeparacionIndemnizacion;
        private System.Windows.Forms.Label label7;
        protected internal System.Windows.Forms.NumericUpDown TotalSueldos;
        private System.Windows.Forms.Label labelTotalSueldos;
        private ComplementoNominaJubilacionPensionRetiroControl complementoNominaJubilacionPensionRetiroControl1;
        private ComplementoNominaSeparacionIndemnizacionControl complementoNominaSeparacionIndemnizacionControl1;
        private System.Windows.Forms.Panel panel1;
        protected internal System.Windows.Forms.NumericUpDown TotalImpuestosRetenidos;
        private System.Windows.Forms.Label label11;
        protected internal System.Windows.Forms.NumericUpDown TotalOtrasDeducciones;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView gridDeducciones;
        private Common.Forms.ToolBarStandarControl TDeduccion;
        private System.Windows.Forms.DataGridView gridOtrosPagos;
        private Common.Forms.ToolBarStandarControl TOtrosPagos;
        private System.Windows.Forms.Panel panel2;
        protected internal System.Windows.Forms.NumericUpDown SubsidioCausado;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView gridIncapacidades;
        private Common.Forms.ToolBarStandarControl TIncapacidad;
        private System.Windows.Forms.DataGridViewComboBoxColumn TipoPercepcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Clave;
        private System.Windows.Forms.DataGridViewTextBoxColumn Concepto;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImporteGravado;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImporteExento;
        private System.Windows.Forms.DataGridViewComboBoxColumn TipoDeduccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Clave2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Concepto2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Importe;
        private System.Windows.Forms.DataGridViewComboBoxColumn TipoOtroPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn OtroPagoClave;
        private System.Windows.Forms.DataGridViewTextBoxColumn OtroPagoConcepto;
        private System.Windows.Forms.DataGridViewTextBoxColumn OtroPagoImporte;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiasIncapacidad;
        private System.Windows.Forms.DataGridViewComboBoxColumn TipoIncapacidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImporteMonetario;
    }
}
