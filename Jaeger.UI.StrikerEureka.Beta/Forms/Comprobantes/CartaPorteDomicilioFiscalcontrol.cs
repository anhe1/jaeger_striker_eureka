﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class CartaPorteDomicilioFiscalcontrol : UserControl {
        private bool bSupressEstadoCode;
        protected ICveTipoEstacionCatalogo cveTipoEstacion;
        protected IClaveEstacionesCatalogo claveEstacionesCatalogo;
        protected ICveEstadoCatalogo cveEstadoCatalogo;
        protected IClaveMunicipioCatalogo claveMunicipioCatalogo;
        protected IClaveLocalidadCatalogo claveLocalidadCatalogo;
        protected IClaveColoniaCatalogo claveColoniaCatalogo;
        protected System.Collections.Generic.List<Catalogos.Entities.ClavePais> Paises;
        public event EventHandler<Catalogos.Entities.ClavePais> ClavePais;
        public event EventHandler<Catalogos.Entities.CveEstado> d2;
        public event EventHandler<Catalogos.Entities.CveMunicipio> d3;
        public event EventHandler<Catalogos.Entities.ClaveCodigoPostal> ClaveCodigoPostal;

        public void OnClavePaisChange(Catalogos.Entities.ClavePais e) {
            if (this.ClavePais != null) {
                this.ClavePais(this, e);
            }
        }

        public void OnEstadoChange(Catalogos.Entities.CveEstado e) {
            if (this.d2 != null) {
                this.d2(this, e);
            }
        }

        public void OnCodigoPostalChange(Catalogos.Entities.ClaveCodigoPostal codigoPostal) {
            if (this.ClaveCodigoPostal != null) {
                this.ClaveCodigoPostal(this, codigoPostal);
            }
        }

        public CartaPorteDomicilioFiscalcontrol() {
            InitializeComponent();
        }

        private void CartaPorteDomicilioFiscalcontrol_Load(object sender, EventArgs e) {
            this.Paises = new List<Catalogos.Entities.ClavePais>() {
                new Catalogos.Entities.ClavePais { Clave = "MEX" },
                new Catalogos.Entities.ClavePais { Clave = "USA" },
                new Catalogos.Entities.ClavePais { Clave = "CAN" }
            };

            this.Pais.SelectedValueChanged += this.Pais_SelectedValueChanged;
            this.Pais.ValueMember = "Clave";
            this.Pais.DisplayMember = "Clave";
            this.Pais.DataSource = this.Paises;
        }

        public virtual void Pais_SelectedValueChanged(object sender, EventArgs e) {
            this.bSupressEstadoCode = true;
            if (this.Pais.SelectedValue != null) {
                this.OnClavePaisChange(new Catalogos.Entities.ClavePais() { Clave = this.Pais.SelectedValue.ToString() });
                this.Estado.DataSource = this.cveEstadoCatalogo.Items.Where(it => it.ClavePais == this.Pais.SelectedValue.ToString());
            }
            this.Estado.DisplayMember = "Descriptor";
            this.Estado.ValueMember = "Clave";
            this.bSupressEstadoCode = false;
        }
    }
}
