﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class CartaPorteControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.Generales = new System.Windows.Forms.GroupBox();
            this.PaisOrigenDestino = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.EntradaSalidaMerc = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TotalDistRec = new System.Windows.Forms.NumericUpDown();
            this.ViaEntradaSalida = new System.Windows.Forms.ComboBox();
            this.TranspInternac = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gridUbicaciones = new System.Windows.Forms.DataGridView();
            this.TipoUbicacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdUbicacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RFCRemitenteDestinatario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreRemitenteDestinatario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumRegIdTrib = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResidenciaFiscal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumEstacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TUbicacion = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.gridMercancias = new System.Windows.Forms.DataGridView();
            this.BienesTransp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveUnidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dimensiones = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaterialPeligroso = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CveMaterialPeligroso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveSTCC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Embalaje = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescripEmbalaje = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PesoEnKg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValorMercancia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Moneda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FraccionArancelaria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UUIDComercioExt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TMercancia = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.gridTransporte = new System.Windows.Forms.DataGridView();
            this.TipoFigura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RFCFigura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumLicencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreFigura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumRegIdTribFigura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResidenciaFiscalFigura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TTransporte = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PesoNetoTotal = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.PesoBrutoTotal = new System.Windows.Forms.TextBox();
            this.CargoPorTasacion = new System.Windows.Forms.TextBox();
            this.UnidadPeso = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.NumTotalMercancias = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.AutotransporteControl = new Jaeger.UI.Forms.Comprobantes.CartaPorteMercanciasAutotransporteControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.Generales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDistRec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridUbicaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMercancias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTransporte)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // Generales
            // 
            this.Generales.Controls.Add(this.PaisOrigenDestino);
            this.Generales.Controls.Add(this.label7);
            this.Generales.Controls.Add(this.EntradaSalidaMerc);
            this.Generales.Controls.Add(this.label6);
            this.Generales.Controls.Add(this.TotalDistRec);
            this.Generales.Controls.Add(this.ViaEntradaSalida);
            this.Generales.Controls.Add(this.TranspInternac);
            this.Generales.Controls.Add(this.label3);
            this.Generales.Controls.Add(this.label2);
            this.Generales.Controls.Add(this.label1);
            this.Generales.Dock = System.Windows.Forms.DockStyle.Top;
            this.Generales.Location = new System.Drawing.Point(0, 0);
            this.Generales.Name = "Generales";
            this.Generales.Size = new System.Drawing.Size(1117, 47);
            this.Generales.TabIndex = 0;
            this.Generales.TabStop = false;
            this.Generales.Text = "Generales:";
            // 
            // PaisOrigenDestino
            // 
            this.PaisOrigenDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PaisOrigenDestino.FormattingEnabled = true;
            this.PaisOrigenDestino.Items.AddRange(new object[] {
            "MXN",
            "USA"});
            this.PaisOrigenDestino.Location = new System.Drawing.Point(1017, 17);
            this.PaisOrigenDestino.Name = "PaisOrigenDestino";
            this.PaisOrigenDestino.Size = new System.Drawing.Size(88, 21);
            this.PaisOrigenDestino.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(904, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "País Origen/Destino:";
            // 
            // EntradaSalidaMerc
            // 
            this.EntradaSalidaMerc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.EntradaSalidaMerc.FormattingEnabled = true;
            this.EntradaSalidaMerc.Location = new System.Drawing.Point(813, 17);
            this.EntradaSalidaMerc.Name = "EntradaSalidaMerc";
            this.EntradaSalidaMerc.Size = new System.Drawing.Size(85, 21);
            this.EntradaSalidaMerc.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(661, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(146, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "mercancías Entrada / Salida:";
            // 
            // TotalDistRec
            // 
            this.TotalDistRec.Location = new System.Drawing.Point(572, 17);
            this.TotalDistRec.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.TotalDistRec.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.TotalDistRec.Name = "TotalDistRec";
            this.TotalDistRec.Size = new System.Drawing.Size(77, 20);
            this.TotalDistRec.TabIndex = 5;
            this.TotalDistRec.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TotalDistRec.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            // 
            // ViaEntradaSalida
            // 
            this.ViaEntradaSalida.FormattingEnabled = true;
            this.ViaEntradaSalida.Location = new System.Drawing.Point(275, 17);
            this.ViaEntradaSalida.Name = "ViaEntradaSalida";
            this.ViaEntradaSalida.Size = new System.Drawing.Size(161, 21);
            this.ViaEntradaSalida.TabIndex = 4;
            // 
            // TranspInternac
            // 
            this.TranspInternac.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TranspInternac.FormattingEnabled = true;
            this.TranspInternac.Location = new System.Drawing.Point(103, 17);
            this.TranspInternac.Name = "TranspInternac";
            this.TranspInternac.Size = new System.Drawing.Size(121, 21);
            this.TranspInternac.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(442, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Total distancia recorrida:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(238, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Vía:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tipo de Traslado:";
            // 
            // gridUbicaciones
            // 
            this.gridUbicaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridUbicaciones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TipoUbicacion,
            this.IdUbicacion,
            this.RFCRemitenteDestinatario,
            this.NombreRemitenteDestinatario,
            this.NumRegIdTrib,
            this.ResidenciaFiscal,
            this.NumEstacion});
            this.gridUbicaciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridUbicaciones.Location = new System.Drawing.Point(3, 28);
            this.gridUbicaciones.Name = "gridUbicaciones";
            this.gridUbicaciones.RowHeadersVisible = false;
            this.gridUbicaciones.Size = new System.Drawing.Size(1103, 339);
            this.gridUbicaciones.TabIndex = 1;
            // 
            // TipoUbicacion
            // 
            this.TipoUbicacion.DataPropertyName = "TipoUbicacion";
            this.TipoUbicacion.HeaderText = "Tipo";
            this.TipoUbicacion.Name = "TipoUbicacion";
            this.TipoUbicacion.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TipoUbicacion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // IdUbicacion
            // 
            this.IdUbicacion.DataPropertyName = "IDUbicacion";
            this.IdUbicacion.HeaderText = "ID Ubicación";
            this.IdUbicacion.Name = "IdUbicacion";
            this.IdUbicacion.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IdUbicacion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // RFCRemitenteDestinatario
            // 
            this.RFCRemitenteDestinatario.DataPropertyName = "RFCRemitenteDestinatario";
            this.RFCRemitenteDestinatario.HeaderText = "RFC Remitente";
            this.RFCRemitenteDestinatario.Name = "RFCRemitenteDestinatario";
            this.RFCRemitenteDestinatario.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.RFCRemitenteDestinatario.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // NombreRemitenteDestinatario
            // 
            this.NombreRemitenteDestinatario.DataPropertyName = "NombreRemitenteDestinatario";
            this.NombreRemitenteDestinatario.HeaderText = "Nombre";
            this.NombreRemitenteDestinatario.Name = "NombreRemitenteDestinatario";
            this.NombreRemitenteDestinatario.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NombreRemitenteDestinatario.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // NumRegIdTrib
            // 
            this.NumRegIdTrib.DataPropertyName = "NumRegIdTrib";
            this.NumRegIdTrib.HeaderText = "NumRegIdTrib";
            this.NumRegIdTrib.Name = "NumRegIdTrib";
            this.NumRegIdTrib.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NumRegIdTrib.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ResidenciaFiscal
            // 
            this.ResidenciaFiscal.DataPropertyName = "ResidenciaFiscal";
            this.ResidenciaFiscal.HeaderText = "ResidenciaFiscal";
            this.ResidenciaFiscal.Name = "ResidenciaFiscal";
            this.ResidenciaFiscal.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ResidenciaFiscal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // NumEstacion
            // 
            this.NumEstacion.DataPropertyName = "NumEstacion";
            this.NumEstacion.HeaderText = "NumEstacion";
            this.NumEstacion.Name = "NumEstacion";
            this.NumEstacion.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NumEstacion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // TUbicacion
            // 
            this.TUbicacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.TUbicacion.Etiqueta = "Ubicaciones:";
            this.TUbicacion.Location = new System.Drawing.Point(3, 3);
            this.TUbicacion.Name = "TUbicacion";
            this.TUbicacion.ShowActualizar = false;
            this.TUbicacion.ShowCerrar = false;
            this.TUbicacion.ShowEditar = false;
            this.TUbicacion.ShowGuardar = false;
            this.TUbicacion.ShowHerramientas = false;
            this.TUbicacion.ShowImprimir = false;
            this.TUbicacion.ShowNuevo = false;
            this.TUbicacion.ShowRemover = false;
            this.TUbicacion.Size = new System.Drawing.Size(1103, 25);
            this.TUbicacion.TabIndex = 0;
            this.TUbicacion.ButtonNuevo_Click += new System.EventHandler<System.EventArgs>(this.TUbicacion_Nuevo_Click);
            this.TUbicacion.ButtonEditar_Click += new System.EventHandler<System.EventArgs>(this.TUbicacion_Editar_Click);
            this.TUbicacion.ButtonRemover_Click += new System.EventHandler<System.EventArgs>(this.TUbicacion_Remover_Click);
            // 
            // gridMercancias
            // 
            this.gridMercancias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridMercancias.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BienesTransp,
            this.Descripcion,
            this.Cantidad,
            this.ClaveUnidad,
            this.Unidad,
            this.Dimensiones,
            this.MaterialPeligroso,
            this.CveMaterialPeligroso,
            this.ClaveSTCC,
            this.Embalaje,
            this.DescripEmbalaje,
            this.PesoEnKg,
            this.ValorMercancia,
            this.Moneda,
            this.FraccionArancelaria,
            this.UUIDComercioExt});
            this.gridMercancias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridMercancias.Location = new System.Drawing.Point(3, 28);
            this.gridMercancias.Name = "gridMercancias";
            this.gridMercancias.RowHeadersVisible = false;
            this.gridMercancias.Size = new System.Drawing.Size(1089, 282);
            this.gridMercancias.TabIndex = 2;
            // 
            // BienesTransp
            // 
            this.BienesTransp.DataPropertyName = "BienesTransp";
            this.BienesTransp.HeaderText = "BienesTransp";
            this.BienesTransp.Name = "BienesTransp";
            // 
            // Descripcion
            // 
            this.Descripcion.DataPropertyName = "Descripcion";
            this.Descripcion.HeaderText = "Descripción";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Width = 200;
            // 
            // Cantidad
            // 
            this.Cantidad.DataPropertyName = "Cantidad";
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            this.Cantidad.Width = 75;
            // 
            // ClaveUnidad
            // 
            this.ClaveUnidad.DataPropertyName = "ClaveUnidad";
            this.ClaveUnidad.HeaderText = "Cve. Unidad";
            this.ClaveUnidad.Name = "ClaveUnidad";
            this.ClaveUnidad.ReadOnly = true;
            // 
            // Unidad
            // 
            this.Unidad.DataPropertyName = "Unidad";
            this.Unidad.HeaderText = "Unidad";
            this.Unidad.Name = "Unidad";
            this.Unidad.ReadOnly = true;
            // 
            // Dimensiones
            // 
            this.Dimensiones.DataPropertyName = "Dimensiones";
            this.Dimensiones.HeaderText = "Dimensiones";
            this.Dimensiones.Name = "Dimensiones";
            this.Dimensiones.ReadOnly = true;
            // 
            // MaterialPeligroso
            // 
            this.MaterialPeligroso.DataPropertyName = "MaterialPeligroso";
            this.MaterialPeligroso.HeaderText = "Material Peligroso";
            this.MaterialPeligroso.Name = "MaterialPeligroso";
            this.MaterialPeligroso.ReadOnly = true;
            this.MaterialPeligroso.Width = 50;
            // 
            // CveMaterialPeligroso
            // 
            this.CveMaterialPeligroso.DataPropertyName = "CveMaterialPeligroso";
            this.CveMaterialPeligroso.HeaderText = "Cve. Material Peligroso";
            this.CveMaterialPeligroso.Name = "CveMaterialPeligroso";
            this.CveMaterialPeligroso.ReadOnly = true;
            this.CveMaterialPeligroso.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CveMaterialPeligroso.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CveMaterialPeligroso.Width = 75;
            // 
            // ClaveSTCC
            // 
            this.ClaveSTCC.DataPropertyName = "ClaveSTCC";
            this.ClaveSTCC.HeaderText = "ClaveSTCC";
            this.ClaveSTCC.Name = "ClaveSTCC";
            this.ClaveSTCC.ReadOnly = true;
            this.ClaveSTCC.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ClaveSTCC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Embalaje
            // 
            this.Embalaje.DataPropertyName = "Embalaje";
            this.Embalaje.HeaderText = "Embalaje";
            this.Embalaje.Name = "Embalaje";
            this.Embalaje.ReadOnly = true;
            this.Embalaje.Width = 50;
            // 
            // DescripEmbalaje
            // 
            this.DescripEmbalaje.DataPropertyName = "DescripEmbalaje";
            this.DescripEmbalaje.HeaderText = "DescripEmbalaje";
            this.DescripEmbalaje.Name = "DescripEmbalaje";
            this.DescripEmbalaje.ReadOnly = true;
            this.DescripEmbalaje.Width = 150;
            // 
            // PesoEnKg
            // 
            this.PesoEnKg.DataPropertyName = "PesoEnKg";
            this.PesoEnKg.HeaderText = "PesoEnKg";
            this.PesoEnKg.Name = "PesoEnKg";
            this.PesoEnKg.ReadOnly = true;
            // 
            // ValorMercancia
            // 
            this.ValorMercancia.DataPropertyName = "ValorMercancia";
            this.ValorMercancia.HeaderText = "ValorMercancia";
            this.ValorMercancia.Name = "ValorMercancia";
            this.ValorMercancia.ReadOnly = true;
            // 
            // Moneda
            // 
            this.Moneda.DataPropertyName = "Moneda";
            this.Moneda.HeaderText = "Moneda";
            this.Moneda.Name = "Moneda";
            this.Moneda.ReadOnly = true;
            // 
            // FraccionArancelaria
            // 
            this.FraccionArancelaria.DataPropertyName = "FraccionArancelaria";
            this.FraccionArancelaria.HeaderText = "FraccionArancelaria";
            this.FraccionArancelaria.Name = "FraccionArancelaria";
            this.FraccionArancelaria.ReadOnly = true;
            // 
            // UUIDComercioExt
            // 
            this.UUIDComercioExt.DataPropertyName = "UUIDComercioExt";
            this.UUIDComercioExt.HeaderText = "UUIDComercioExt";
            this.UUIDComercioExt.Name = "UUIDComercioExt";
            this.UUIDComercioExt.ReadOnly = true;
            // 
            // TMercancia
            // 
            this.TMercancia.Dock = System.Windows.Forms.DockStyle.Top;
            this.TMercancia.Etiqueta = "Mercancias:";
            this.TMercancia.Location = new System.Drawing.Point(3, 3);
            this.TMercancia.Name = "TMercancia";
            this.TMercancia.ShowActualizar = false;
            this.TMercancia.ShowCerrar = false;
            this.TMercancia.ShowEditar = false;
            this.TMercancia.ShowGuardar = false;
            this.TMercancia.ShowHerramientas = false;
            this.TMercancia.ShowImprimir = false;
            this.TMercancia.ShowNuevo = false;
            this.TMercancia.ShowRemover = false;
            this.TMercancia.Size = new System.Drawing.Size(1089, 25);
            this.TMercancia.TabIndex = 1;
            this.TMercancia.ButtonNuevo_Click += new System.EventHandler<System.EventArgs>(this.TMercancia_Nuevo_Click);
            this.TMercancia.ButtonEditar_Click += new System.EventHandler<System.EventArgs>(this.TMercancia_Editar_Click);
            this.TMercancia.ButtonRemover_Click += new System.EventHandler<System.EventArgs>(this.TMercancia_Remover_Click);
            // 
            // gridTransporte
            // 
            this.gridTransporte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridTransporte.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TipoFigura,
            this.RFCFigura,
            this.NumLicencia,
            this.NombreFigura,
            this.NumRegIdTribFigura,
            this.ResidenciaFiscalFigura});
            this.gridTransporte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTransporte.Location = new System.Drawing.Point(3, 28);
            this.gridTransporte.Name = "gridTransporte";
            this.gridTransporte.RowHeadersVisible = false;
            this.gridTransporte.Size = new System.Drawing.Size(1103, 339);
            this.gridTransporte.TabIndex = 4;
            // 
            // TipoFigura
            // 
            this.TipoFigura.DataPropertyName = "TipoFigura";
            this.TipoFigura.HeaderText = "TipoFigura";
            this.TipoFigura.Name = "TipoFigura";
            // 
            // RFCFigura
            // 
            this.RFCFigura.DataPropertyName = "RFCFigura";
            this.RFCFigura.HeaderText = "RFCFigura";
            this.RFCFigura.Name = "RFCFigura";
            // 
            // NumLicencia
            // 
            this.NumLicencia.DataPropertyName = "NumLicencia";
            this.NumLicencia.HeaderText = "NumLicencia";
            this.NumLicencia.Name = "NumLicencia";
            // 
            // NombreFigura
            // 
            this.NombreFigura.DataPropertyName = "NombreFigura";
            this.NombreFigura.HeaderText = "NombreFigura";
            this.NombreFigura.Name = "NombreFigura";
            // 
            // NumRegIdTribFigura
            // 
            this.NumRegIdTribFigura.DataPropertyName = "NumRegIdTribFigura";
            this.NumRegIdTribFigura.HeaderText = "NumRegIdTribFigura";
            this.NumRegIdTribFigura.Name = "NumRegIdTribFigura";
            // 
            // ResidenciaFiscalFigura
            // 
            this.ResidenciaFiscalFigura.DataPropertyName = "ResidenciaFiscalFigura";
            this.ResidenciaFiscalFigura.HeaderText = "ResidenciaFiscalFigura";
            this.ResidenciaFiscalFigura.Name = "ResidenciaFiscalFigura";
            // 
            // TTransporte
            // 
            this.TTransporte.Dock = System.Windows.Forms.DockStyle.Top;
            this.TTransporte.Etiqueta = "Transporte";
            this.TTransporte.Location = new System.Drawing.Point(3, 3);
            this.TTransporte.Name = "TTransporte";
            this.TTransporte.ShowActualizar = false;
            this.TTransporte.ShowCerrar = false;
            this.TTransporte.ShowEditar = false;
            this.TTransporte.ShowGuardar = false;
            this.TTransporte.ShowHerramientas = false;
            this.TTransporte.ShowImprimir = false;
            this.TTransporte.ShowNuevo = false;
            this.TTransporte.ShowRemover = false;
            this.TTransporte.Size = new System.Drawing.Size(1103, 25);
            this.TTransporte.TabIndex = 3;
            this.TTransporte.ButtonNuevo_Click += new System.EventHandler<System.EventArgs>(this.TTransporte_Nuevo_Click);
            this.TTransporte.ButtonEditar_Click += new System.EventHandler<System.EventArgs>(this.TTransporte_Editar_Click);
            this.TTransporte.ButtonRemover_Click += new System.EventHandler<System.EventArgs>(this.TTransporte_Remover_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 47);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1117, 396);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gridUbicaciones);
            this.tabPage1.Controls.Add(this.TUbicacion);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1109, 370);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ubicaciones";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tabControl2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1109, 370);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Mercancia";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(3, 3);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1103, 364);
            this.tabControl2.TabIndex = 3;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.gridMercancias);
            this.tabPage4.Controls.Add(this.panel1);
            this.tabPage4.Controls.Add(this.TMercancia);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1095, 338);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "Mercancias";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.PesoNetoTotal);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.PesoBrutoTotal);
            this.panel1.Controls.Add(this.CargoPorTasacion);
            this.panel1.Controls.Add(this.UnidadPeso);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.NumTotalMercancias);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 310);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1089, 25);
            this.panel1.TabIndex = 3;
            // 
            // PesoNetoTotal
            // 
            this.PesoNetoTotal.Location = new System.Drawing.Point(455, 3);
            this.PesoNetoTotal.Name = "PesoNetoTotal";
            this.PesoNetoTotal.Size = new System.Drawing.Size(72, 20);
            this.PesoNetoTotal.TabIndex = 9;
            this.PesoNetoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(362, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Peso Neto Total:";
            // 
            // PesoBrutoTotal
            // 
            this.PesoBrutoTotal.Location = new System.Drawing.Point(813, 3);
            this.PesoBrutoTotal.Name = "PesoBrutoTotal";
            this.PesoBrutoTotal.Size = new System.Drawing.Size(84, 20);
            this.PesoBrutoTotal.TabIndex = 7;
            this.PesoBrutoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CargoPorTasacion
            // 
            this.CargoPorTasacion.Location = new System.Drawing.Point(640, 3);
            this.CargoPorTasacion.Name = "CargoPorTasacion";
            this.CargoPorTasacion.Size = new System.Drawing.Size(72, 20);
            this.CargoPorTasacion.TabIndex = 6;
            this.CargoPorTasacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // UnidadPeso
            // 
            this.UnidadPeso.FormattingEnabled = true;
            this.UnidadPeso.Location = new System.Drawing.Point(111, 3);
            this.UnidadPeso.Name = "UnidadPeso";
            this.UnidadPeso.Size = new System.Drawing.Size(245, 21);
            this.UnidadPeso.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(533, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Cargo Por Tasación";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Unidad de Peso:";
            // 
            // NumTotalMercancias
            // 
            this.NumTotalMercancias.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NumTotalMercancias.Location = new System.Drawing.Point(1014, 3);
            this.NumTotalMercancias.Name = "NumTotalMercancias";
            this.NumTotalMercancias.Size = new System.Drawing.Size(72, 20);
            this.NumTotalMercancias.TabIndex = 2;
            this.NumTotalMercancias.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(903, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Núm. de Mercancias";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(718, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Peso Bruto Total:";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.AutotransporteControl);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1095, 338);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "Autotransporte";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // AutotransporteControl
            // 
            this.AutotransporteControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AutotransporteControl.Location = new System.Drawing.Point(3, 3);
            this.AutotransporteControl.Name = "AutotransporteControl";
            this.AutotransporteControl.Size = new System.Drawing.Size(1089, 332);
            this.AutotransporteControl.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.gridTransporte);
            this.tabPage3.Controls.Add(this.TTransporte);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1109, 370);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Transporte";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // CartaPorteControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.Generales);
            this.Name = "CartaPorteControl";
            this.Size = new System.Drawing.Size(1117, 443);
            this.Load += new System.EventHandler(this.CartaPorteControl_Load);
            this.Generales.ResumeLayout(false);
            this.Generales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDistRec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridUbicaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMercancias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTransporte)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Generales;
        private System.Windows.Forms.NumericUpDown TotalDistRec;
        private System.Windows.Forms.ComboBox ViaEntradaSalida;
        private System.Windows.Forms.ComboBox TranspInternac;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView gridUbicaciones;
        private Common.Forms.ToolBarStandarControl TUbicacion;
        private System.Windows.Forms.DataGridView gridMercancias;
        private System.Windows.Forms.DataGridView gridTransporte;
        private Common.Forms.ToolBarStandarControl TTransporte;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoUbicacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdUbicacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFCRemitenteDestinatario;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreRemitenteDestinatario;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumRegIdTrib;
        private System.Windows.Forms.DataGridViewTextBoxColumn ResidenciaFiscal;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumEstacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn BienesTransp;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveUnidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dimensiones;
        private System.Windows.Forms.DataGridViewCheckBoxColumn MaterialPeligroso;
        private System.Windows.Forms.DataGridViewTextBoxColumn CveMaterialPeligroso;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveSTCC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Embalaje;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescripEmbalaje;
        private System.Windows.Forms.DataGridViewTextBoxColumn PesoEnKg;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValorMercancia;
        private System.Windows.Forms.DataGridViewTextBoxColumn Moneda;
        private System.Windows.Forms.DataGridViewTextBoxColumn FraccionArancelaria;
        private System.Windows.Forms.DataGridViewTextBoxColumn UUIDComercioExt;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private CartaPorteMercanciasAutotransporteControl AutotransporteControl;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoFigura;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFCFigura;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumLicencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreFigura;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumRegIdTribFigura;
        private System.Windows.Forms.DataGridViewTextBoxColumn ResidenciaFiscalFigura;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox EntradaSalidaMerc;
        private System.Windows.Forms.ComboBox PaisOrigenDestino;
        private System.Windows.Forms.Label label7;
        protected internal System.Windows.Forms.TextBox NumTotalMercancias;
        protected internal System.Windows.Forms.TextBox PesoNetoTotal;
        private System.Windows.Forms.Label label10;
        protected internal System.Windows.Forms.TextBox PesoBrutoTotal;
        protected internal System.Windows.Forms.TextBox CargoPorTasacion;
        private System.Windows.Forms.ComboBox UnidadPeso;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        protected internal Common.Forms.ToolBarStandarControl TMercancia;
    }
}
