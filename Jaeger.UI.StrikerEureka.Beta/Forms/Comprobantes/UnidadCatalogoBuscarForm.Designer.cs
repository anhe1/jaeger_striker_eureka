﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class UnidadCatalogoBuscarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TCatalogo = new Jaeger.UI.Common.Forms.ToolBarStandarSearchControl();
            this.gridCatalogo = new System.Windows.Forms.DataGridView();
            this.Clave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridCatalogo)).BeginInit();
            this.SuspendLayout();
            // 
            // TCatalogo
            // 
            this.TCatalogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCatalogo.Etiqueta = "Buscar:";
            this.TCatalogo.Location = new System.Drawing.Point(0, 0);
            this.TCatalogo.Name = "TCatalogo";
            this.TCatalogo.ShowActualizar = true;
            this.TCatalogo.ShowCerrar = true;
            this.TCatalogo.ShowEditar = false;
            this.TCatalogo.ShowGuardar = false;
            this.TCatalogo.ShowHerramientas = false;
            this.TCatalogo.ShowImprimir = true;
            this.TCatalogo.ShowNuevo = false;
            this.TCatalogo.ShowRemover = false;
            this.TCatalogo.Size = new System.Drawing.Size(548, 25);
            this.TCatalogo.TabIndex = 0;
            this.TCatalogo.ButtonImprimir_Click += new System.EventHandler<System.EventArgs>(this.TCatalogo_Imprimir_Click);
            this.TCatalogo.ButtonActualizar_Click += new System.EventHandler<System.EventArgs>(this.TCatalogo_Actualizar_Click);
            this.TCatalogo.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TCatalogo_Cerrar_Click);
            // 
            // gridCatalogo
            // 
            this.gridCatalogo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridCatalogo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Clave,
            this.Nombre,
            this.Descripcion});
            this.gridCatalogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCatalogo.Location = new System.Drawing.Point(0, 25);
            this.gridCatalogo.Name = "gridCatalogo";
            this.gridCatalogo.Size = new System.Drawing.Size(548, 290);
            this.gridCatalogo.TabIndex = 1;
            // 
            // Clave
            // 
            this.Clave.DataPropertyName = "Clave";
            this.Clave.HeaderText = "Clave";
            this.Clave.Name = "Clave";
            // 
            // Nombre
            // 
            this.Nombre.DataPropertyName = "Nombre";
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            this.Nombre.ReadOnly = true;
            this.Nombre.Width = 150;
            // 
            // Descripcion
            // 
            this.Descripcion.DataPropertyName = "Descripcion";
            this.Descripcion.HeaderText = "Tipo";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.ReadOnly = true;
            this.Descripcion.Width = 250;
            // 
            // UnidadCatalogoBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 315);
            this.Controls.Add(this.gridCatalogo);
            this.Controls.Add(this.TCatalogo);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UnidadCatalogoBuscarForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Catálogo: Unidad";
            this.Load += new System.EventHandler(this.CatalogoBuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridCatalogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarSearchControl TCatalogo;
        private System.Windows.Forms.DataGridView gridCatalogo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Clave;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
    }
}