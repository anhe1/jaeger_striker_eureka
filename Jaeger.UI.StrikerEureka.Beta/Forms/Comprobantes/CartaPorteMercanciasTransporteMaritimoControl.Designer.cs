﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class CartaPorteMercanciasTransporteMaritimoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.PermSCT = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.NumPermisoSCT = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.NombreAseg = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.NumPolizaSeguro = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TipoEmbarcacion = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Matricula = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.NumeroOMI = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.AnioEmbarcacion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.NombreEmbarc = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.NacionalidadEmbarc = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.UnidadesDeArqBruto = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TipoCarga = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.NumCertITC = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Eslora = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Manga = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.Calado = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.LineaNaviera = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.NombreAgenteNaviero = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.NumAutorizacionNaviero = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.NumViaje = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.NumConocEmbarc = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.SubTipoRem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Placa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TRemolque = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // PermSCT
            // 
            this.PermSCT.FormattingEnabled = true;
            this.PermSCT.Location = new System.Drawing.Point(10, 37);
            this.PermSCT.Name = "PermSCT";
            this.PermSCT.Size = new System.Drawing.Size(100, 21);
            this.PermSCT.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "PermSCT";
            // 
            // NumPermisoSCT
            // 
            this.NumPermisoSCT.Location = new System.Drawing.Point(116, 37);
            this.NumPermisoSCT.Name = "NumPermisoSCT";
            this.NumPermisoSCT.Size = new System.Drawing.Size(100, 20);
            this.NumPermisoSCT.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(116, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "NumPermisoSCT";
            // 
            // NombreAseg
            // 
            this.NombreAseg.Location = new System.Drawing.Point(222, 37);
            this.NombreAseg.Name = "NombreAseg";
            this.NombreAseg.Size = new System.Drawing.Size(247, 20);
            this.NombreAseg.TabIndex = 30;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(222, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "NombreAseg";
            // 
            // NumPolizaSeguro
            // 
            this.NumPolizaSeguro.Location = new System.Drawing.Point(474, 37);
            this.NumPolizaSeguro.Name = "NumPolizaSeguro";
            this.NumPolizaSeguro.Size = new System.Drawing.Size(100, 20);
            this.NumPolizaSeguro.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(474, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "NumPolizaSeguro";
            // 
            // TipoEmbarcacion
            // 
            this.TipoEmbarcacion.FormattingEnabled = true;
            this.TipoEmbarcacion.Location = new System.Drawing.Point(10, 77);
            this.TipoEmbarcacion.Name = "TipoEmbarcacion";
            this.TipoEmbarcacion.Size = new System.Drawing.Size(200, 21);
            this.TipoEmbarcacion.TabIndex = 34;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "TipoEmbarcacion";
            // 
            // Matricula
            // 
            this.Matricula.Location = new System.Drawing.Point(218, 77);
            this.Matricula.Name = "Matricula";
            this.Matricula.Size = new System.Drawing.Size(141, 20);
            this.Matricula.TabIndex = 36;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(221, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 35;
            this.label4.Text = "Matricula";
            // 
            // NumeroOMI
            // 
            this.NumeroOMI.Location = new System.Drawing.Point(365, 77);
            this.NumeroOMI.Name = "NumeroOMI";
            this.NumeroOMI.Size = new System.Drawing.Size(104, 20);
            this.NumeroOMI.TabIndex = 38;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(365, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 37;
            this.label5.Text = "NumeroOMI";
            // 
            // AnioEmbarcacion
            // 
            this.AnioEmbarcacion.Location = new System.Drawing.Point(477, 77);
            this.AnioEmbarcacion.Name = "AnioEmbarcacion";
            this.AnioEmbarcacion.Size = new System.Drawing.Size(97, 20);
            this.AnioEmbarcacion.TabIndex = 40;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(474, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 39;
            this.label6.Text = "AnioEmbarcacion";
            // 
            // NombreEmbarc
            // 
            this.NombreEmbarc.Location = new System.Drawing.Point(10, 118);
            this.NombreEmbarc.Name = "NombreEmbarc";
            this.NombreEmbarc.Size = new System.Drawing.Size(107, 20);
            this.NombreEmbarc.TabIndex = 42;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 101);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 41;
            this.label8.Text = "NombreEmbarc";
            // 
            // NacionalidadEmbarc
            // 
            this.NacionalidadEmbarc.FormattingEnabled = true;
            this.NacionalidadEmbarc.Location = new System.Drawing.Point(123, 118);
            this.NacionalidadEmbarc.Name = "NacionalidadEmbarc";
            this.NacionalidadEmbarc.Size = new System.Drawing.Size(107, 21);
            this.NacionalidadEmbarc.TabIndex = 44;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(123, 101);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 13);
            this.label9.TabIndex = 43;
            this.label9.Text = "NacionalidadEmbarc";
            // 
            // UnidadesDeArqBruto
            // 
            this.UnidadesDeArqBruto.Location = new System.Drawing.Point(239, 118);
            this.UnidadesDeArqBruto.Name = "UnidadesDeArqBruto";
            this.UnidadesDeArqBruto.Size = new System.Drawing.Size(120, 20);
            this.UnidadesDeArqBruto.TabIndex = 46;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(236, 101);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(107, 13);
            this.label11.TabIndex = 45;
            this.label11.Text = "UnidadesDeArqBruto";
            // 
            // TipoCarga
            // 
            this.TipoCarga.FormattingEnabled = true;
            this.TipoCarga.Location = new System.Drawing.Point(365, 118);
            this.TipoCarga.Name = "TipoCarga";
            this.TipoCarga.Size = new System.Drawing.Size(104, 21);
            this.TipoCarga.TabIndex = 48;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(365, 101);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 47;
            this.label12.Text = "TipoCarga";
            // 
            // NumCertITC
            // 
            this.NumCertITC.Location = new System.Drawing.Point(474, 118);
            this.NumCertITC.Name = "NumCertITC";
            this.NumCertITC.Size = new System.Drawing.Size(100, 20);
            this.NumCertITC.TabIndex = 50;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(474, 100);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 13);
            this.label13.TabIndex = 49;
            this.label13.Text = "NumCertITC";
            // 
            // Eslora
            // 
            this.Eslora.Location = new System.Drawing.Point(10, 157);
            this.Eslora.Name = "Eslora";
            this.Eslora.Size = new System.Drawing.Size(107, 20);
            this.Eslora.TabIndex = 52;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 141);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 13);
            this.label14.TabIndex = 51;
            this.label14.Text = "Eslora";
            // 
            // Manga
            // 
            this.Manga.Location = new System.Drawing.Point(123, 157);
            this.Manga.Name = "Manga";
            this.Manga.Size = new System.Drawing.Size(107, 20);
            this.Manga.TabIndex = 54;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(123, 141);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 53;
            this.label15.Text = "Manga";
            // 
            // Calado
            // 
            this.Calado.Location = new System.Drawing.Point(239, 157);
            this.Calado.Name = "Calado";
            this.Calado.Size = new System.Drawing.Size(120, 20);
            this.Calado.TabIndex = 56;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(239, 141);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 55;
            this.label16.Text = "Calado";
            // 
            // LineaNaviera
            // 
            this.LineaNaviera.Location = new System.Drawing.Point(365, 157);
            this.LineaNaviera.Name = "LineaNaviera";
            this.LineaNaviera.Size = new System.Drawing.Size(104, 20);
            this.LineaNaviera.TabIndex = 58;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(365, 141);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 57;
            this.label17.Text = "LineaNaviera";
            // 
            // NombreAgenteNaviero
            // 
            this.NombreAgenteNaviero.Location = new System.Drawing.Point(10, 196);
            this.NombreAgenteNaviero.Name = "NombreAgenteNaviero";
            this.NombreAgenteNaviero.Size = new System.Drawing.Size(107, 20);
            this.NombreAgenteNaviero.TabIndex = 60;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(10, 180);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(115, 13);
            this.label18.TabIndex = 59;
            this.label18.Text = "NombreAgenteNaviero";
            // 
            // NumAutorizacionNaviero
            // 
            this.NumAutorizacionNaviero.FormattingEnabled = true;
            this.NumAutorizacionNaviero.Location = new System.Drawing.Point(126, 196);
            this.NumAutorizacionNaviero.Name = "NumAutorizacionNaviero";
            this.NumAutorizacionNaviero.Size = new System.Drawing.Size(124, 21);
            this.NumAutorizacionNaviero.TabIndex = 62;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(126, 180);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(124, 13);
            this.label19.TabIndex = 61;
            this.label19.Text = "NumAutorizacionNaviero";
            // 
            // NumViaje
            // 
            this.NumViaje.Location = new System.Drawing.Point(256, 196);
            this.NumViaje.Name = "NumViaje";
            this.NumViaje.Size = new System.Drawing.Size(107, 20);
            this.NumViaje.TabIndex = 64;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(256, 180);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(52, 13);
            this.label20.TabIndex = 63;
            this.label20.Text = "NumViaje";
            // 
            // NumConocEmbarc
            // 
            this.NumConocEmbarc.Location = new System.Drawing.Point(369, 196);
            this.NumConocEmbarc.Name = "NumConocEmbarc";
            this.NumConocEmbarc.Size = new System.Drawing.Size(100, 20);
            this.NumConocEmbarc.TabIndex = 66;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(369, 180);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(96, 13);
            this.label21.TabIndex = 65;
            this.label21.Text = "NumConocEmbarc";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.PermSCT);
            this.groupBox1.Controls.Add(this.NumConocEmbarc);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.NumViaje);
            this.groupBox1.Controls.Add(this.NumPermisoSCT);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.NumAutorizacionNaviero);
            this.groupBox1.Controls.Add(this.NombreAseg);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.NombreAgenteNaviero);
            this.groupBox1.Controls.Add(this.NumPolizaSeguro);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.TipoEmbarcacion);
            this.groupBox1.Controls.Add(this.LineaNaviera);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.Calado);
            this.groupBox1.Controls.Add(this.Matricula);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Manga);
            this.groupBox1.Controls.Add(this.NumeroOMI);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.Eslora);
            this.groupBox1.Controls.Add(this.AnioEmbarcacion);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.NumCertITC);
            this.groupBox1.Controls.Add(this.NombreEmbarc);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.TipoCarga);
            this.groupBox1.Controls.Add(this.NacionalidadEmbarc);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.UnidadesDeArqBruto);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(580, 229);
            this.groupBox1.TabIndex = 67;
            this.groupBox1.TabStop = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SubTipoRem,
            this.Placa});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 254);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(580, 196);
            this.dataGridView1.TabIndex = 69;
            // 
            // SubTipoRem
            // 
            this.SubTipoRem.DataPropertyName = "SubTipoRem";
            this.SubTipoRem.HeaderText = "SubTipoRem";
            this.SubTipoRem.Name = "SubTipoRem";
            this.SubTipoRem.Width = 150;
            // 
            // Placa
            // 
            this.Placa.DataPropertyName = "Placa";
            this.Placa.HeaderText = "Placa";
            this.Placa.Name = "Placa";
            // 
            // TRemolque
            // 
            this.TRemolque.Dock = System.Windows.Forms.DockStyle.Top;
            this.TRemolque.Etiqueta = "Remolques";
            this.TRemolque.Location = new System.Drawing.Point(0, 229);
            this.TRemolque.Name = "TRemolque";
            this.TRemolque.ShowActualizar = false;
            this.TRemolque.ShowCerrar = false;
            this.TRemolque.ShowEditar = false;
            this.TRemolque.ShowGuardar = false;
            this.TRemolque.ShowHerramientas = false;
            this.TRemolque.ShowImprimir = false;
            this.TRemolque.ShowNuevo = true;
            this.TRemolque.ShowRemover = true;
            this.TRemolque.Size = new System.Drawing.Size(580, 25);
            this.TRemolque.TabIndex = 68;
            // 
            // CartaPorteMercanciasTransporteMaritimoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.TRemolque);
            this.Controls.Add(this.groupBox1);
            this.Name = "CartaPorteMercanciasTransporteMaritimoControl";
            this.Size = new System.Drawing.Size(580, 450);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox PermSCT;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox NumPermisoSCT;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox NombreAseg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NumPolizaSeguro;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox TipoEmbarcacion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Matricula;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox NumeroOMI;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox AnioEmbarcacion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox NombreEmbarc;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox NacionalidadEmbarc;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox UnidadesDeArqBruto;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox TipoCarga;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox NumCertITC;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Eslora;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Manga;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox Calado;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox LineaNaviera;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox NombreAgenteNaviero;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox NumAutorizacionNaviero;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox NumViaje;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox NumConocEmbarc;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubTipoRem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Placa;
        private Common.Forms.ToolBarStandarControl TRemolque;
    }
}
