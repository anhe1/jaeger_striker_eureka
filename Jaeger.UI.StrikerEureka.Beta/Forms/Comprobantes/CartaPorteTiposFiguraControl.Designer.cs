﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class CartaPorteTiposFiguraControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.TipoFigura = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.RFCFigura = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.NumLicencia = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.NombreFigura = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.NumRegIdTribFigura = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ResidenciaFiscalFigura = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TFigura = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.label13 = new System.Windows.Forms.Label();
            this.CodigoPostal = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Calle = new System.Windows.Forms.TextBox();
            this.Pais = new System.Windows.Forms.ComboBox();
            this.NumeroExterior = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.Estado = new System.Windows.Forms.TextBox();
            this.NumeroInterior = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.Municipio = new System.Windows.Forms.TextBox();
            this.Colonia = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.Referencia = new System.Windows.Forms.TextBox();
            this.Localidad = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.TParte = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // TipoFigura
            // 
            this.TipoFigura.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TipoFigura.FormattingEnabled = true;
            this.TipoFigura.Location = new System.Drawing.Point(6, 32);
            this.TipoFigura.Name = "TipoFigura";
            this.TipoFigura.Size = new System.Drawing.Size(121, 21);
            this.TipoFigura.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Tipo:";
            // 
            // RFCFigura
            // 
            this.RFCFigura.Location = new System.Drawing.Point(387, 32);
            this.RFCFigura.Name = "RFCFigura";
            this.RFCFigura.Size = new System.Drawing.Size(121, 20);
            this.RFCFigura.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(387, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "RFC";
            // 
            // NumLicencia
            // 
            this.NumLicencia.Location = new System.Drawing.Point(260, 73);
            this.NumLicencia.Name = "NumLicencia";
            this.NumLicencia.Size = new System.Drawing.Size(121, 20);
            this.NumLicencia.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(260, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Núm. Licencia:";
            // 
            // NombreFigura
            // 
            this.NombreFigura.Location = new System.Drawing.Point(133, 32);
            this.NombreFigura.Name = "NombreFigura";
            this.NombreFigura.Size = new System.Drawing.Size(248, 20);
            this.NombreFigura.TabIndex = 25;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(133, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Nombre:";
            // 
            // NumRegIdTribFigura
            // 
            this.NumRegIdTribFigura.Location = new System.Drawing.Point(514, 32);
            this.NumRegIdTribFigura.Name = "NumRegIdTribFigura";
            this.NumRegIdTribFigura.Size = new System.Drawing.Size(121, 20);
            this.NumRegIdTribFigura.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(514, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Núm. Reg Id Tributario";
            // 
            // ResidenciaFiscalFigura
            // 
            this.ResidenciaFiscalFigura.Location = new System.Drawing.Point(133, 73);
            this.ResidenciaFiscalFigura.Name = "ResidenciaFiscalFigura";
            this.ResidenciaFiscalFigura.Size = new System.Drawing.Size(121, 20);
            this.ResidenciaFiscalFigura.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(133, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "Residencia Fiscal";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.ResidenciaFiscalFigura);
            this.groupBox1.Controls.Add(this.TipoFigura);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.NumRegIdTribFigura);
            this.groupBox1.Controls.Add(this.RFCFigura);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.NombreFigura);
            this.groupBox1.Controls.Add(this.NumLicencia);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(660, 101);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Figura de Transporte";
            // 
            // TFigura
            // 
            this.TFigura.Dock = System.Windows.Forms.DockStyle.Top;
            this.TFigura.Etiqueta = "";
            this.TFigura.Location = new System.Drawing.Point(0, 0);
            this.TFigura.Name = "TFigura";
            this.TFigura.ShowActualizar = false;
            this.TFigura.ShowCerrar = true;
            this.TFigura.ShowEditar = false;
            this.TFigura.ShowGuardar = true;
            this.TFigura.ShowHerramientas = false;
            this.TFigura.ShowImprimir = false;
            this.TFigura.ShowNuevo = false;
            this.TFigura.ShowRemover = false;
            this.TFigura.Size = new System.Drawing.Size(660, 25);
            this.TFigura.TabIndex = 31;
            this.TFigura.ButtonGuardar_Click += new System.EventHandler<System.EventArgs>(this.TFigura_ButtonGuardar_Click);
            this.TFigura.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TFigura_ButtonCerrar_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Calle:";
            // 
            // CodigoPostal
            // 
            this.CodigoPostal.Location = new System.Drawing.Point(539, 60);
            this.CodigoPostal.MaxLength = 12;
            this.CodigoPostal.Name = "CodigoPostal";
            this.CodigoPostal.Size = new System.Drawing.Size(100, 20);
            this.CodigoPostal.TabIndex = 19;
            this.CodigoPostal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(218, 5);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Núm. Exterior:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(536, 44);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Codigo Postal:*";
            // 
            // Calle
            // 
            this.Calle.Location = new System.Drawing.Point(9, 21);
            this.Calle.MaxLength = 100;
            this.Calle.Name = "Calle";
            this.Calle.Size = new System.Drawing.Size(206, 20);
            this.Calle.TabIndex = 2;
            // 
            // Pais
            // 
            this.Pais.Location = new System.Drawing.Point(433, 60);
            this.Pais.MaxLength = 30;
            this.Pais.Name = "Pais";
            this.Pais.Size = new System.Drawing.Size(100, 21);
            this.Pais.TabIndex = 17;
            // 
            // NumeroExterior
            // 
            this.NumeroExterior.Location = new System.Drawing.Point(221, 21);
            this.NumeroExterior.MaxLength = 55;
            this.NumeroExterior.Name = "NumeroExterior";
            this.NumeroExterior.Size = new System.Drawing.Size(100, 20);
            this.NumeroExterior.TabIndex = 3;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(430, 44);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "País:*";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(324, 5);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Núm. Interior:";
            // 
            // Estado
            // 
            this.Estado.Location = new System.Drawing.Point(221, 60);
            this.Estado.MaxLength = 30;
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(206, 20);
            this.Estado.TabIndex = 15;
            // 
            // NumeroInterior
            // 
            this.NumeroInterior.Location = new System.Drawing.Point(327, 21);
            this.NumeroInterior.MaxLength = 55;
            this.NumeroInterior.Name = "NumeroInterior";
            this.NumeroInterior.Size = new System.Drawing.Size(100, 20);
            this.NumeroInterior.TabIndex = 5;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(220, 44);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 13);
            this.label18.TabIndex = 14;
            this.label18.Text = "Estado:*";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(430, 5);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(45, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "Colonia:";
            // 
            // Municipio
            // 
            this.Municipio.Location = new System.Drawing.Point(9, 60);
            this.Municipio.MaxLength = 120;
            this.Municipio.Name = "Municipio";
            this.Municipio.Size = new System.Drawing.Size(206, 20);
            this.Municipio.TabIndex = 13;
            // 
            // Colonia
            // 
            this.Colonia.Location = new System.Drawing.Point(433, 21);
            this.Colonia.MaxLength = 120;
            this.Colonia.Name = "Colonia";
            this.Colonia.Size = new System.Drawing.Size(206, 20);
            this.Colonia.TabIndex = 7;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 44);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 13);
            this.label20.TabIndex = 12;
            this.label20.Text = "Municipio:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 83);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 13);
            this.label21.TabIndex = 8;
            this.label21.Text = "Localidad:";
            // 
            // Referencia
            // 
            this.Referencia.Location = new System.Drawing.Point(327, 99);
            this.Referencia.MaxLength = 250;
            this.Referencia.Name = "Referencia";
            this.Referencia.Size = new System.Drawing.Size(312, 20);
            this.Referencia.TabIndex = 11;
            // 
            // Localidad
            // 
            this.Localidad.Location = new System.Drawing.Point(9, 99);
            this.Localidad.MaxLength = 120;
            this.Localidad.Name = "Localidad";
            this.Localidad.Size = new System.Drawing.Size(312, 20);
            this.Localidad.TabIndex = 9;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(324, 83);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(62, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "Referencia:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 126);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(660, 163);
            this.tabControl1.TabIndex = 33;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.CodigoPostal);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.Localidad);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.Referencia);
            this.tabPage1.Controls.Add(this.Calle);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.Pais);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.NumeroExterior);
            this.tabPage1.Controls.Add(this.Colonia);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.Municipio);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.Estado);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.NumeroInterior);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(652, 137);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Domicilio";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.TParte);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1064, 403);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Partes";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // TParte
            // 
            this.TParte.Dock = System.Windows.Forms.DockStyle.Top;
            this.TParte.Etiqueta = "";
            this.TParte.Location = new System.Drawing.Point(3, 3);
            this.TParte.Name = "TParte";
            this.TParte.ShowActualizar = false;
            this.TParte.ShowCerrar = false;
            this.TParte.ShowEditar = false;
            this.TParte.ShowGuardar = false;
            this.TParte.ShowHerramientas = false;
            this.TParte.ShowImprimir = false;
            this.TParte.ShowNuevo = false;
            this.TParte.ShowRemover = false;
            this.TParte.Size = new System.Drawing.Size(1058, 25);
            this.TParte.TabIndex = 0;
            // 
            // CartaPorteTiposFiguraControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 289);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TFigura);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CartaPorteTiposFiguraControl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.CartaPorteTiposFiguraControl_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox TipoFigura;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox RFCFigura;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox NumLicencia;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NombreFigura;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox NumRegIdTribFigura;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ResidenciaFiscalFigura;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private Common.Forms.ToolBarStandarControl TFigura;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox CodigoPostal;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox Calle;
        private System.Windows.Forms.ComboBox Pais;
        private System.Windows.Forms.TextBox NumeroExterior;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Estado;
        private System.Windows.Forms.TextBox NumeroInterior;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox Municipio;
        private System.Windows.Forms.TextBox Colonia;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox Referencia;
        private System.Windows.Forms.TextBox Localidad;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private Common.Forms.ToolBarStandarControl TParte;
    }
}
