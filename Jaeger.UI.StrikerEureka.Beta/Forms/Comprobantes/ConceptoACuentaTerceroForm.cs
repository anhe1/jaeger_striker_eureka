﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class ConceptoACuentaTerceroForm : Form {
        protected IContribuyenteService service;
        protected IRegimenesFiscalesCatalogo regimenesfiscales = new RegimenesFiscalesCatalogo();
        protected BindingList<ComprobanteContribuyenteModel> contribuyenteModels;
        private ComprobanteConceptoACuentaTerceros CurrentComplemento;

        public void OnAgregarComplemento(ComprobanteConceptoACuentaTerceros e) {
            if (this.AgregarComplemento != null)
                this.AgregarComplemento(this, e);
        }

        public event EventHandler<ComprobanteConceptoACuentaTerceros> AgregarComplemento;

        public ConceptoACuentaTerceroForm(ComprobanteConceptoACuentaTerceros complemento) {
            InitializeComponent();
            this.CurrentComplemento = complemento;
        }

        private void ConceptoACuentaTerceroForm_Load(object sender, EventArgs e) {
            this.service = new ContribuyenteService(Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido);
            if (this.CurrentComplemento == null) {
                this.CurrentComplemento = new ComprobanteConceptoACuentaTerceros();
            }
            this.CargarReceptores.RunWorkerAsync();
        }

        private void CreateBinding() {
            //if (this.CurrentComplemento == null) {
            //    this.CurrentComplemento = new ComprobanteConceptoACuentaTerceros();
            //}

            this.RfcACuentaTerceros.DataBindings.Clear();
            this.RfcACuentaTerceros.DataBindings.Add("Text", this.CurrentComplemento, "RfcACuentaTerceros", true, DataSourceUpdateMode.OnPropertyChanged);
            this.NombreACuentaTerceros.DataBindings.Clear();
            this.NombreACuentaTerceros.DataBindings.Add("Text", this.CurrentComplemento, "NombreACuentaTerceros", true, DataSourceUpdateMode.OnPropertyChanged);
            this.RegimenFiscalACuentaTerceros.DataBindings.Clear();
            this.RegimenFiscalACuentaTerceros.DataBindings.Add("SelectedValue", this.CurrentComplemento, "RegimenFiscalACuentaTerceros", true, DataSourceUpdateMode.OnPropertyChanged);
            this.DomicilioFiscalACuentaTerceros.DataBindings.Clear();
            this.DomicilioFiscalACuentaTerceros.DataBindings.Add("Text", this.CurrentComplemento, "DomicilioFiscalACuentaTerceros", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void Receptor_SelectedValueChanged(object sender, EventArgs e) {
                var receptor = NombreACuentaTerceros.SelectedItem as ComprobanteContribuyenteModel;
                if (receptor != null) {
                    this.RfcACuentaTerceros.Text = receptor.RFC;
                    this.NombreACuentaTerceros.Text = receptor.Nombre;

                    this.DomicilioFiscalACuentaTerceros.Text = receptor.DomicilioFiscal;

                    if (receptor.RegimenFiscal != null) {
                        this.RegimenFiscalACuentaTerceros.SelectedValue = receptor.RegimenFiscal;
                    }
                } else {
                    MessageBox.Show(this, "Properties.Resources.Message_Objeto_NoValido", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
        }

        private void Receptor_DropDownOpened(object sender, EventArgs e) {
            this.NombreACuentaTerceros.DataSource = this.contribuyenteModels;
        }

        private void TComplemento_Guardar_Click(object sender, EventArgs e) {
            this.OnAgregarComplemento(this.CurrentComplemento);
            this.Close();
        }

        private void TComplemento_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CargarReceptores_DoWork(object sender, DoWorkEventArgs e) {
            this.regimenesfiscales.Load();
            this.buttonActualizarDirectorio.Enabled = false;
            this.NombreACuentaTerceros.Enabled = false;
            this.contribuyenteModels = this.service.GetContribuyentes();

        }

        private void CargarReceptores_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            // regimen fiscal
            this.RegimenFiscalACuentaTerceros.DataSource = this.regimenesfiscales.Items;
            this.RegimenFiscalACuentaTerceros.SelectedIndex = -1;
            //this.RegimenFiscalACuentaTerceros.SelectedValue = null;

            this.NombreACuentaTerceros.SelectedIndex = -1;
            this.NombreACuentaTerceros.SelectedValue = null;
            this.NombreACuentaTerceros.Enabled = true;
            this.buttonActualizarDirectorio.Enabled = true;
            this.NombreACuentaTerceros.Enabled = true;
            this.NombreACuentaTerceros.SelectedValueChanged += new EventHandler(this.Receptor_SelectedValueChanged);
            //this.NombreACuentaTerceros.DropDownOpened += new EventHandler(this.Receptor_DropDownOpened);
            this.CreateBinding();
        }
    }
}
