﻿namespace Jaeger.UI.Forms.Comprobantes {
    interface IComplementoControl {
        string Caption { get; set; }

        bool IsEditable { get; set; }

        bool IsValid();

        void Start();

        void CreateBinding();
    }
}
