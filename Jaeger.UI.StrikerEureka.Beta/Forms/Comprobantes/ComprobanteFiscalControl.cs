﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Aplication.Comprobante;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Aplication.Comprobante.Services;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class ComprobanteFiscalControl : UserControl {
        protected CFDISubTipoEnum subTipo;

        #region eventos
        public event EventHandler<EventArgs> BindingCompleted;
        public ComprobanteFiscalDetailModel Comprobante;
        public IComprobanteFiscalService Service;

        public void OnBindingClompleted(EventArgs e) {
            if (this.BindingCompleted != null)
                this.BindingCompleted(this, e);
        }
        #endregion

        public void Start(CFDISubTipoEnum subTipo) {
            this.subTipo = subTipo;
            this.OnLoadStart.RunWorkerAsync();
        }

        public int IdComprobante { 
            get; set; 
        }

        public CFDITipoComprobanteEnum TipoComprobante {
            get; set;
        }

        public ComprobanteFiscalControl() {
            InitializeComponent();
        }

        private void ComprobanteFiscalControl_Load(object sender, EventArgs e) {
            this.Actualizar.Click += TComprobante_Actualizar_Click;
            this.Guardar.Click += TComprobante_Guardar_Click;
            this.InformacionGeneral.Incluir.CheckedChanged += Incluir_CheckedChanged;
            this.IdDocumento.TextBox.DataBindings.CollectionChanged += this.TextBox_BindingContextChanged;
        }

        private void TextBox_BindingContextChanged(object sender, EventArgs e) {
            this.FileXML.Visible = Domain.Base.Services.ValidacionService.URL(this.Comprobante.UrlFileXML);
            this.FilePDF.Visible = Domain.Base.Services.ValidacionService.URL(this.Comprobante.UrlFilePDF);
        }

        public virtual void Incluir_CheckedChanged(object sender, EventArgs e) {
            if (this.Comprobante.InformacionGlobal == null) {
                this.Comprobante.InformacionGlobal = new ComprobanteInformacionGlobalDetailModel();
            }
            if (this.Comprobante.IsEditable && this.InformacionGeneral.Incluir.Checked == false) {
                this.Comprobante.InformacionGlobal = null;
            }
            this.BindingInfoGeneral();
        }

        public virtual void TComprobante_Guardar_Click(object sender, EventArgs e) {
            this.Comprobante = this.Service.Save(this.Comprobante);
        }

        public virtual void TComprobante_Actualizar_Click(object sender, EventArgs e) {
            //if (this.Comprobante == null) {
            //    this.Comprobante = this.Service.GetNew(this.TipoComprobante);
            //} else if (this.Comprobante.Id > 0) {
            //    using (var espera = new WaitingForm(this.GetComprobante)) {
            //        espera.Text = "Obteniendo comprobante ...";
            //        espera.ShowDialog(this);
            //    }
            //}

            //if (this.Comprobante.IsEditable) {
            //    this.General.LoadContribuyente(this.subTipo);
            //}
            //this.CreateBinding();
        }

        public virtual void TComprobante_Certificar_Click(object sender, EventArgs e) {

        }

        public virtual void TComprobante_Cancelar_Click(object sender, EventArgs e) {

        }

        public virtual void TComprobante_FilePDF_Click(object sender, EventArgs e) {

        }

        public virtual void TComprobante_FileXML_Click(object sender, EventArgs e) {

        }

        public virtual void TComprobante_Email_Click(object sender, EventArgs e) {

        }

        public virtual void GetComprobante() {
            this.Comprobante = this.Service.GetComprobante(this.Comprobante.Id);
        }

        public virtual void CreateBinding() {

            this.Status.ComboBox.DataBindings.Clear();
            this.Status.ComboBox.DataBindings.Add("Text", this.Comprobante, "Status", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.lblVersion.DataBindings.Clear();
            this.General.lblVersion.DataBindings.Add("Text", this.Comprobante, "Version", true, DataSourceUpdateMode.OnPropertyChanged);
            this.General.Receptor.DataBindings.Clear();
            this.General.ReceptorRFC.DataBindings.Clear();
            this.General.ReceptorRFC.ReadOnly = true;
            //this.General.Receptor.SetEditable(this.Comprobante.Editable);
            this.CFDIRelacionadoControl.ReceptorRFC.DataBindings.Clear();

            //this.General.Version.DataBindings.Clear();
            //this.General.Version.DataBindings.Add("Text", this.Comprobante, "Version", true, DataSourceUpdateMode.OnPropertyChanged);

            if (this.Comprobante.SubTipo == CFDISubTipoEnum.Emitido) {
                this.General.Receptor.DataBindings.Add("Text", this.Comprobante, "ReceptorNombre", true, DataSourceUpdateMode.OnPropertyChanged);
                this.General.ReceptorRFC.DataBindings.Add("Text", this.Comprobante, "ReceptorRFC", true, DataSourceUpdateMode.OnPropertyChanged);
                this.CFDIRelacionadoControl.ReceptorRFC.DataBindings.Add("Text", this.Comprobante, "ReceptorRFC", true, DataSourceUpdateMode.OnPropertyChanged);
            } else {
                this.General.Receptor.DataBindings.Add("Text", this.Comprobante, "EmisorNombre", true, DataSourceUpdateMode.OnPropertyChanged);
                this.General.ReceptorRFC.DataBindings.Add("Text", this.Comprobante, "EmisorRFC", true, DataSourceUpdateMode.OnPropertyChanged);
            }

            this.IdDocumento.TextBox.DataBindings.Clear();
            this.IdDocumento.TextBox.DataBindings.Add("Text", this.Comprobante, "IdDocumento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.IdDirectorio.DataBindings.Clear();
            this.General.IdDirectorio.DataBindings.Add("Value", this.Comprobante, "IdDirectorio", true, DataSourceUpdateMode.OnPropertyChanged);

            //this.General.Documento.DataBindings.Clear();
            //this.General.Documento.DataBindings.Add("SelectedValue", this.Comprobante, "Documento", true, DataSourceUpdateMode.OnPropertyChanged);
            //this.General.Documento.SetEditable(this.Comprobante.Editable);

            this.General.Folio.DataBindings.Clear();
            this.General.Folio.ReadOnly = !this.Comprobante.IsEditable;
            this.General.Folio.DataBindings.Add("Text", this.Comprobante, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Serie.DataBindings.Clear();
            this.General.Serie.Enabled = this.Comprobante.IsEditable;
            this.General.Serie.DataBindings.Add("Text", this.Comprobante, "Serie", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.IdSerie.DataBindings.Clear();
            this.General.IdSerie.DataBindings.Add("Text", this.Comprobante, "IdSerie", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.ResidenciaFiscal.DataBindings.Clear();
            //this.General.ResidenciaFiscal.SetEditable(this.Comprobante.Editable);
            this.General.ResidenciaFiscal.DataBindings.Add("Text", this.Comprobante, "ResidenciaFiscal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.NumRegIdTrib.DataBindings.Clear();
            //this.General.NumRegIdTrib.ReadOnly = !this.Comprobante.Editable;
            this.General.NumRegIdTrib.DataBindings.Add("Text", this.Comprobante, "NumRegIdTrib", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.DomicilioFiscal.DataBindings.Clear();
            this.General.DomicilioFiscal.DataBindings.Add("Text", this.Comprobante, "DomicilioFiscal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Exportacion.DataBindings.Clear();
            //this.General.Exportacion.SetEditable(this.Comprobante.Editable);
            this.General.Exportacion.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveExportacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.TipoCambio.DataBindings.Clear();
            //this.General.TipoCambio.SetEditable(this.Comprobante.Editable);
            this.General.TipoCambio.DataBindings.Add("Value", this.Comprobante, "TipoCambio", true, DataSourceUpdateMode.OnPropertyChanged);

            //this.General.Decimales.DataBindings.Clear();
            //this.General.Decimales.SetEditable(this.Comprobante.Editable);
            //this.General.Decimales.DataBindings.Add("Value", this.Comprobante, "PrecisionDecimal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.TipoComprobante.DataBindings.Clear();
            //this.General.TipoComprobante.SetEditable(this.Comprobante.Editable);
            this.General.TipoComprobante.DataBindings.Add("Text", this.Comprobante, "TipoComprobanteText", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.UsoCFDI.DataBindings.Clear();
            //this.General.UsoCFDI.SetEditable(this.Comprobante.Editable);
            this.General.UsoCFDI.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveUsoCFDI", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.RegimenFiscal.DataBindings.Clear();
            //this.General.RegimenFiscal.SetEditable(this.Comprobante.Editable);
            this.General.RegimenFiscal.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveRegimenFiscal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.MetodoPago.DataBindings.Clear();
            //this.General.MetodoPago.SetEditable(this.Comprobante.Editable);
            this.General.MetodoPago.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveMetodoPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.FormaPago.DataBindings.Clear();
            //this.General.FormaPago.SetEditable(this.Comprobante.Editable);
            this.General.FormaPago.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveFormaPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Condiciones.DataBindings.Clear();
            //this.General.Condiciones.SetEditable(this.Comprobante.Editable);
            this.General.Condiciones.DataBindings.Add("Text", this.Comprobante, "CondicionPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Moneda.DataBindings.Clear();
            //this.General.Moneda.SetEditable(this.Comprobante.Editable);
            this.General.Moneda.DataBindings.Add("Text", this.Comprobante, "ClaveMoneda", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.FechaEmision.DataBindings.Clear();
            //this.General.FechaEmision.SetEditable(this.Comprobante.Editable);
            this.General.FechaEmision.DataBindings.Add("Value", this.Comprobante, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.FechaCertifica.DataBindings.Clear();
            //this.General.FechaCertifica.DateTimePickerElement.ArrowButton.Visibility = ElementVisibility.Hidden;
            //this.General.FechaCertifica.DateTimePickerElement.ReadOnly = true;
            //this.General.FechaCertifica.DateTimePickerElement.TextBoxElement.TextAlign = HorizontalAlignment.Center;
            this.General.FechaCertifica.DataBindings.Add("Text", this.Comprobante, "FechaTimbre", true, DataSourceUpdateMode.OnPropertyChanged);
            //this.General.FechaCertifica.SetEditable(this.Comprobante.Editable);

            this.General.LugarExpedicion.DataBindings.Clear();
            //this.General.LugarExpedicion.ReadOnly = true;
            this.General.LugarExpedicion.DataBindings.Add("Text", this.Comprobante, "LugarExpedicion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.BindingInfoGeneral();
            this.OnBindingClompleted(new EventArgs());
        }

        public virtual void BindingInfoGeneral() {
            if (this.Comprobante.InformacionGlobal != null) {
                this.InformacionGeneral.Incluir.Checked = true;
                this.InformacionGeneral.ClavePeriodicidad.DataBindings.Clear();
                this.InformacionGeneral.ClavePeriodicidad.DataBindings.Add("SelectedValue", this.Comprobante.InformacionGlobal, "Periodicidad", true, DataSourceUpdateMode.OnPropertyChanged);
                this.InformacionGeneral.ClaveMeses.DataBindings.Clear();
                this.InformacionGeneral.ClaveMeses.DataBindings.Add("SelectedValue", this.Comprobante.InformacionGlobal, "ClaveMeses", true, DataSourceUpdateMode.OnPropertyChanged);
                this.InformacionGeneral.Ejercicio.DataBindings.Clear();
                this.InformacionGeneral.Ejercicio.DataBindings.Add("Value", this.Comprobante.InformacionGlobal, "Anio", true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }

        private void OnLoadStart_DoWork(object sender, DoWorkEventArgs e) {
            this.General.Start();
            this.InformacionGeneral.Start();
            this.Service = new ComprobanteFiscalService();
        }

        private void OnLoadStart_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Actualizar.PerformClick();
        }
    }
}
