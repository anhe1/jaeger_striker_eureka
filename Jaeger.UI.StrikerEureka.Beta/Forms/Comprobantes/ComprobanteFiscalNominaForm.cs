﻿using System;
using System.Windows.Forms;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.UI.Forms.Comprobantes {
    public class ComprobanteFiscalNominaForm : ComprobanteFiscalForm {
        protected internal ComplementoNominaControl complementoNomina = new ComplementoNominaControl() { Dock = DockStyle.Fill };
        protected internal TabPage _page = new TabPage() { Text = "Complemento: Nómina", Padding = new Padding(3), UseVisualStyleBackColor = true };

        public ComprobanteFiscalNominaForm(ComprobanteFiscalDetailModel model, CFDISubTipoEnum subTipo) : base(model, subTipo) {
            _page.Controls.Add(this.complementoNomina);
            //this.tabControls.TabPages.RemoveAt(0);
            this.tabControls.TabPages.Add(_page);
            this.Load += this.ComprobanteFiscalNominaForm_Load;
        }

        private void ComprobanteFiscalNominaForm_Load(object sender, EventArgs e) {
            this.Text = "Recibo de Nómina";
            this.complementoNomina.Start();
            this.ComprobanteControl.BindingCompleted += ComprobanteControl_BindingCompleted;
        }

        public override void DesdeArchivo_Click(object sender, EventArgs e) {
            base.DesdeArchivo_Click(sender, e);
            ComprobanteControl_BindingCompleted(sender, e);
        }

        private void ComprobanteControl_BindingCompleted(object sender, EventArgs e) {
            if (this.ComprobanteControl.Comprobante.Id == 0) {
                if (this.ComprobanteControl.Comprobante.Nomina == null) {
                    this.ComprobanteControl.Comprobante.Nomina = new Domain.Comprobante.Entities.Complemento.ComplementoNomina();
                }
            }
            this.complementoNomina.Nomina = this.ComprobanteControl.Comprobante.Nomina;
            this.complementoNomina.CreateBinding();
        }
    }
}
