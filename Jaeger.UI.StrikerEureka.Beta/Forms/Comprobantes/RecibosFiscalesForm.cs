﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.ReciboFiscal;
using Jaeger.Domain.ReciboFiscal.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class RecibosFiscalesForm : Form {
        protected Aplication.ReciboFiscal.IReciboFiscalService service;
        protected BindingList<ReciboFiscalModel> recibos;
        protected internal ToolStripMenuItem PrePoliza = new ToolStripMenuItem { Text = "Pre-Póliza" };
        protected internal ToolStripMenuItem PrePolizaConfigurar = new ToolStripMenuItem { Text = "Configurar" };
        protected internal ToolStripMenuItem PrePolizaExportar = new ToolStripMenuItem { Text = "Procesar selccionados" };

        public RecibosFiscalesForm() {
            InitializeComponent();
        }

        private void RecibosFiscalesForm_Load(object sender, EventArgs e) {
            this.service = new ReciboFiscalService();
            this.gridRecibos.DataGridCommon();
            this.gridRecibos.MultiSelect = true;
            this.Refresh();
            this.PrePoliza.DropDownItems.Add(this.PrePolizaConfigurar);
            this.PrePoliza.DropDownItems.Add(this.PrePolizaExportar);
            this.TRecibo.Herramientas.DropDownItems.Add(this.PrePoliza);
            this.PrePolizaConfigurar.Click += this.TPrePoliza_Configurar_Click;
            this.PrePolizaExportar.Click += this.TPrePoliza_Exportar_Click;
        }

        private void TPrePoliza_Configurar_Click(object sender, EventArgs e) {
            var a = new ReciboFiscal.PrePolizaAConfForm();
            a.ShowDialog(this);
        }

        private void TPrePoliza_Exportar_Click(object sender, EventArgs e) {
            var selectedRowCount = gridRecibos.Rows.GetRowCount(DataGridViewElementStates.Selected);
            var seleccionados = new BindingList<ReciboFiscalModel>();
            for (int i = 0; i < selectedRowCount; i++) {
                var d = gridRecibos.Rows[i].DataBoundItem as ReciboFiscalModel;
                seleccionados.Add(d);
            }
            var a = new ReciboFiscal.PrePolizaAForm(seleccionados);
            a.ShowDialog(this);
        }

        private void TRecibo_ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TRecibo_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.OnActualizar)) {
                espera.Text = "Consultando...";
                espera.ShowDialog(this);
            }
            this.gridRecibos.DataSource = this.recibos;
        }

        private void OnActualizar() {
            this.recibos = this.service.GetRecibosFiscales(this.TRecibo.GetMes(), this.TRecibo.GetEjercicio(), 0, ConfigService.Synapsis.Empresa.RFC);
        }
    }
}
