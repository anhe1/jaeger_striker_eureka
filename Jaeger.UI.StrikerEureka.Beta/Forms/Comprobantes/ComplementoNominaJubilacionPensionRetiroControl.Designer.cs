﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class ComplementoNominaJubilacionPensionRetiroControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.IngresoNoAcumulable = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.IngresoAcumulable = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.MontoDiario = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.TotalParcialidad = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.TotalUnaExhibicion = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IngresoNoAcumulable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IngresoAcumulable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoDiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalParcialidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalUnaExhibicion)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.IngresoNoAcumulable);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.IngresoAcumulable);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.MontoDiario);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.TotalParcialidad);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.TotalUnaExhibicion);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(670, 90);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // IngresoNoAcumulable
            // 
            this.IngresoNoAcumulable.DecimalPlaces = 2;
            this.IngresoNoAcumulable.Location = new System.Drawing.Point(353, 59);
            this.IngresoNoAcumulable.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.IngresoNoAcumulable.Name = "IngresoNoAcumulable";
            this.IngresoNoAcumulable.Size = new System.Drawing.Size(100, 20);
            this.IngresoNoAcumulable.TabIndex = 18;
            this.IngresoNoAcumulable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(227, 63);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(120, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "Ingreso No Acumulable:";
            // 
            // IngresoAcumulable
            // 
            this.IngresoAcumulable.DecimalPlaces = 2;
            this.IngresoAcumulable.Location = new System.Drawing.Point(117, 59);
            this.IngresoAcumulable.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.IngresoAcumulable.Name = "IngresoAcumulable";
            this.IngresoAcumulable.Size = new System.Drawing.Size(100, 20);
            this.IngresoAcumulable.TabIndex = 18;
            this.IngresoAcumulable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 63);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "Ingreso Acumulable:";
            // 
            // MontoDiario
            // 
            this.MontoDiario.DecimalPlaces = 2;
            this.MontoDiario.Location = new System.Drawing.Point(528, 33);
            this.MontoDiario.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.MontoDiario.Name = "MontoDiario";
            this.MontoDiario.Size = new System.Drawing.Size(100, 20);
            this.MontoDiario.TabIndex = 18;
            this.MontoDiario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(452, 37);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Monto Diario:";
            // 
            // TotalParcialidad
            // 
            this.TotalParcialidad.DecimalPlaces = 2;
            this.TotalParcialidad.Location = new System.Drawing.Point(332, 33);
            this.TotalParcialidad.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.TotalParcialidad.Name = "TotalParcialidad";
            this.TotalParcialidad.Size = new System.Drawing.Size(100, 20);
            this.TotalParcialidad.TabIndex = 18;
            this.TotalParcialidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(227, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Total Parcialidad";
            // 
            // TotalUnaExhibicion
            // 
            this.TotalUnaExhibicion.DecimalPlaces = 2;
            this.TotalUnaExhibicion.Location = new System.Drawing.Point(117, 33);
            this.TotalUnaExhibicion.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.TotalUnaExhibicion.Name = "TotalUnaExhibicion";
            this.TotalUnaExhibicion.Size = new System.Drawing.Size(100, 20);
            this.TotalUnaExhibicion.TabIndex = 18;
            this.TotalUnaExhibicion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Total una exhibición:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(15, 17);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(54, 17);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "Incluir";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // ComplementoNominaJubilacionPensionRetiroControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "ComplementoNominaJubilacionPensionRetiroControl";
            this.Size = new System.Drawing.Size(670, 90);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IngresoNoAcumulable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IngresoAcumulable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoDiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalParcialidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalUnaExhibicion)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        protected internal System.Windows.Forms.NumericUpDown IngresoNoAcumulable;
        private System.Windows.Forms.Label label12;
        protected internal System.Windows.Forms.NumericUpDown IngresoAcumulable;
        private System.Windows.Forms.Label label11;
        protected internal System.Windows.Forms.NumericUpDown MontoDiario;
        private System.Windows.Forms.Label label10;
        protected internal System.Windows.Forms.NumericUpDown TotalParcialidad;
        private System.Windows.Forms.Label label9;
        protected internal System.Windows.Forms.NumericUpDown TotalUnaExhibicion;
        private System.Windows.Forms.Label label5;
        protected internal System.Windows.Forms.CheckBox checkBox1;
    }
}
