﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class CartaPorteMercanciasAutotransporteControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.label7 = new System.Windows.Forms.Label();
            this.PermSCT = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.NumPermisoSCT = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ConfigVehicular = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PlacaVM = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.AnioModeloVM = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.AseguraRespCivil = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.PolizaRespCivil = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.AseguraMedAmbiente = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.PolizaMedAmbiente = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.AseguraCarga = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.PolizaCarga = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.PrimaSeguro = new System.Windows.Forms.NumericUpDown();
            this.Incluir = new System.Windows.Forms.CheckBox();
            this.gridRemolques = new System.Windows.Forms.DataGridView();
            this.SubTipoRem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Placa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TRemolque = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TabControlAutoTransporte = new System.Windows.Forms.TabControl();
            this.TabGeneral = new System.Windows.Forms.TabPage();
            this.TabRemolque = new System.Windows.Forms.TabPage();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.AnioModeloVM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimaSeguro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRemolques)).BeginInit();
            this.TabControlAutoTransporte.SuspendLayout();
            this.TabGeneral.SuspendLayout();
            this.TabRemolque.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Permiso SCT:";
            // 
            // PermSCT
            // 
            this.PermSCT.Enabled = false;
            this.PermSCT.FormattingEnabled = true;
            this.PermSCT.Location = new System.Drawing.Point(93, 24);
            this.PermSCT.Name = "PermSCT";
            this.PermSCT.Size = new System.Drawing.Size(348, 21);
            this.PermSCT.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(448, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "Núm. Permiso:*";
            // 
            // NumPermisoSCT
            // 
            this.NumPermisoSCT.Enabled = false;
            this.NumPermisoSCT.Location = new System.Drawing.Point(533, 24);
            this.NumPermisoSCT.MaxLength = 50;
            this.NumPermisoSCT.Name = "NumPermisoSCT";
            this.NumPermisoSCT.Size = new System.Drawing.Size(141, 20);
            this.NumPermisoSCT.TabIndex = 26;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "ConfigVehicular";
            // 
            // ConfigVehicular
            // 
            this.ConfigVehicular.Enabled = false;
            this.ConfigVehicular.FormattingEnabled = true;
            this.ConfigVehicular.Location = new System.Drawing.Point(93, 50);
            this.ConfigVehicular.Name = "ConfigVehicular";
            this.ConfigVehicular.Size = new System.Drawing.Size(348, 21);
            this.ConfigVehicular.TabIndex = 28;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(448, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "Placa:";
            // 
            // PlacaVM
            // 
            this.PlacaVM.Enabled = false;
            this.PlacaVM.Location = new System.Drawing.Point(491, 50);
            this.PlacaVM.MaxLength = 7;
            this.PlacaVM.Name = "PlacaVM";
            this.PlacaVM.Size = new System.Drawing.Size(70, 20);
            this.PlacaVM.TabIndex = 30;
            this.PlacaVM.TextChanged += new System.EventHandler(this.PlacaVM_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(567, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 31;
            this.label3.Text = "Año:";
            // 
            // AnioModeloVM
            // 
            this.AnioModeloVM.Enabled = false;
            this.AnioModeloVM.Location = new System.Drawing.Point(602, 50);
            this.AnioModeloVM.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.AnioModeloVM.Minimum = new decimal(new int[] {
            1899,
            0,
            0,
            0});
            this.AnioModeloVM.Name = "AnioModeloVM";
            this.AnioModeloVM.Size = new System.Drawing.Size(72, 20);
            this.AnioModeloVM.TabIndex = 32;
            this.AnioModeloVM.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AnioModeloVM.Value = new decimal(new int[] {
            1899,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(182, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "Aseguradora de responsabilidad civil ";
            // 
            // AseguraRespCivil
            // 
            this.AseguraRespCivil.Enabled = false;
            this.AseguraRespCivil.Location = new System.Drawing.Point(194, 78);
            this.AseguraRespCivil.MaxLength = 50;
            this.AseguraRespCivil.Name = "AseguraRespCivil";
            this.AseguraRespCivil.Size = new System.Drawing.Size(265, 20);
            this.AseguraRespCivil.TabIndex = 34;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(465, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 35;
            this.label5.Text = "Núm. de póliza:";
            // 
            // PolizaRespCivil
            // 
            this.PolizaRespCivil.Enabled = false;
            this.PolizaRespCivil.Location = new System.Drawing.Point(551, 78);
            this.PolizaRespCivil.MaxLength = 30;
            this.PolizaRespCivil.Name = "PolizaRespCivil";
            this.PolizaRespCivil.Size = new System.Drawing.Size(123, 20);
            this.PolizaRespCivil.TabIndex = 36;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(205, 13);
            this.label6.TabIndex = 37;
            this.label6.Text = "Aseguradora de daños al medio ambiente:";
            // 
            // AseguraMedAmbiente
            // 
            this.AseguraMedAmbiente.Enabled = false;
            this.AseguraMedAmbiente.Location = new System.Drawing.Point(217, 104);
            this.AseguraMedAmbiente.MaxLength = 50;
            this.AseguraMedAmbiente.Name = "AseguraMedAmbiente";
            this.AseguraMedAmbiente.Size = new System.Drawing.Size(242, 20);
            this.AseguraMedAmbiente.TabIndex = 38;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(465, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 39;
            this.label8.Text = "Núm. de póliza:";
            // 
            // PolizaMedAmbiente
            // 
            this.PolizaMedAmbiente.Enabled = false;
            this.PolizaMedAmbiente.Location = new System.Drawing.Point(551, 104);
            this.PolizaMedAmbiente.Name = "PolizaMedAmbiente";
            this.PolizaMedAmbiente.Size = new System.Drawing.Size(123, 20);
            this.PolizaMedAmbiente.TabIndex = 40;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 134);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(188, 13);
            this.label9.TabIndex = 41;
            this.label9.Text = "Aseguradora de la carga transportada:";
            // 
            // AseguraCarga
            // 
            this.AseguraCarga.Enabled = false;
            this.AseguraCarga.Location = new System.Drawing.Point(200, 130);
            this.AseguraCarga.Name = "AseguraCarga";
            this.AseguraCarga.Size = new System.Drawing.Size(155, 20);
            this.AseguraCarga.TabIndex = 42;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(361, 134);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 43;
            this.label11.Text = "Núm. de póliza:";
            // 
            // PolizaCarga
            // 
            this.PolizaCarga.Enabled = false;
            this.PolizaCarga.Location = new System.Drawing.Point(447, 130);
            this.PolizaCarga.Name = "PolizaCarga";
            this.PolizaCarga.Size = new System.Drawing.Size(68, 20);
            this.PolizaCarga.TabIndex = 44;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(521, 134);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 13);
            this.label12.TabIndex = 45;
            this.label12.Text = "Prima Seguro:";
            // 
            // PrimaSeguro
            // 
            this.PrimaSeguro.DecimalPlaces = 2;
            this.PrimaSeguro.Location = new System.Drawing.Point(594, 130);
            this.PrimaSeguro.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.PrimaSeguro.Name = "PrimaSeguro";
            this.PrimaSeguro.Size = new System.Drawing.Size(80, 20);
            this.PrimaSeguro.TabIndex = 46;
            this.PrimaSeguro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Incluir
            // 
            this.Incluir.AutoSize = true;
            this.Incluir.Location = new System.Drawing.Point(620, 4);
            this.Incluir.Name = "Incluir";
            this.Incluir.Size = new System.Drawing.Size(54, 17);
            this.Incluir.TabIndex = 47;
            this.Incluir.Text = "Incluir";
            this.Incluir.UseVisualStyleBackColor = true;
            this.Incluir.CheckedChanged += new System.EventHandler(this.Incluir_CheckedChanged);
            // 
            // gridRemolques
            // 
            this.gridRemolques.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridRemolques.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SubTipoRem,
            this.Placa});
            this.gridRemolques.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRemolques.Location = new System.Drawing.Point(3, 28);
            this.gridRemolques.Name = "gridRemolques";
            this.gridRemolques.Size = new System.Drawing.Size(679, 126);
            this.gridRemolques.TabIndex = 51;
            // 
            // SubTipoRem
            // 
            this.SubTipoRem.DataPropertyName = "SubTipoRem";
            this.SubTipoRem.HeaderText = "SubTipoRem";
            this.SubTipoRem.Name = "SubTipoRem";
            this.SubTipoRem.Width = 150;
            // 
            // Placa
            // 
            this.Placa.DataPropertyName = "Placa";
            this.Placa.HeaderText = "Placa";
            this.Placa.Name = "Placa";
            // 
            // TRemolque
            // 
            this.TRemolque.Dock = System.Windows.Forms.DockStyle.Top;
            this.TRemolque.Etiqueta = "Remolques";
            this.TRemolque.Location = new System.Drawing.Point(3, 3);
            this.TRemolque.Name = "TRemolque";
            this.TRemolque.ShowActualizar = false;
            this.TRemolque.ShowCerrar = false;
            this.TRemolque.ShowEditar = false;
            this.TRemolque.ShowGuardar = false;
            this.TRemolque.ShowHerramientas = false;
            this.TRemolque.ShowImprimir = false;
            this.TRemolque.ShowNuevo = false;
            this.TRemolque.ShowRemover = false;
            this.TRemolque.Size = new System.Drawing.Size(679, 25);
            this.TRemolque.TabIndex = 50;
            this.TRemolque.ButtonRemover_Click += new System.EventHandler<System.EventArgs>(this.TRemolque_Remover_Click);
            // 
            // TabControlAutoTransporte
            // 
            this.TabControlAutoTransporte.Controls.Add(this.TabGeneral);
            this.TabControlAutoTransporte.Controls.Add(this.TabRemolque);
            this.TabControlAutoTransporte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControlAutoTransporte.Location = new System.Drawing.Point(0, 0);
            this.TabControlAutoTransporte.Multiline = true;
            this.TabControlAutoTransporte.Name = "TabControlAutoTransporte";
            this.TabControlAutoTransporte.SelectedIndex = 0;
            this.TabControlAutoTransporte.Size = new System.Drawing.Size(693, 183);
            this.TabControlAutoTransporte.TabIndex = 52;
            // 
            // TabGeneral
            // 
            this.TabGeneral.Controls.Add(this.Incluir);
            this.TabGeneral.Controls.Add(this.AseguraRespCivil);
            this.TabGeneral.Controls.Add(this.ConfigVehicular);
            this.TabGeneral.Controls.Add(this.label4);
            this.TabGeneral.Controls.Add(this.PolizaRespCivil);
            this.TabGeneral.Controls.Add(this.PermSCT);
            this.TabGeneral.Controls.Add(this.label12);
            this.TabGeneral.Controls.Add(this.label1);
            this.TabGeneral.Controls.Add(this.label5);
            this.TabGeneral.Controls.Add(this.PlacaVM);
            this.TabGeneral.Controls.Add(this.PrimaSeguro);
            this.TabGeneral.Controls.Add(this.label7);
            this.TabGeneral.Controls.Add(this.AseguraMedAmbiente);
            this.TabGeneral.Controls.Add(this.label2);
            this.TabGeneral.Controls.Add(this.label11);
            this.TabGeneral.Controls.Add(this.NumPermisoSCT);
            this.TabGeneral.Controls.Add(this.label6);
            this.TabGeneral.Controls.Add(this.AnioModeloVM);
            this.TabGeneral.Controls.Add(this.PolizaCarga);
            this.TabGeneral.Controls.Add(this.label10);
            this.TabGeneral.Controls.Add(this.PolizaMedAmbiente);
            this.TabGeneral.Controls.Add(this.label3);
            this.TabGeneral.Controls.Add(this.label9);
            this.TabGeneral.Controls.Add(this.AseguraCarga);
            this.TabGeneral.Controls.Add(this.label8);
            this.TabGeneral.Location = new System.Drawing.Point(4, 22);
            this.TabGeneral.Name = "TabGeneral";
            this.TabGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.TabGeneral.Size = new System.Drawing.Size(685, 157);
            this.TabGeneral.TabIndex = 0;
            this.TabGeneral.Text = "General";
            this.TabGeneral.UseVisualStyleBackColor = true;
            // 
            // TabRemolque
            // 
            this.TabRemolque.Controls.Add(this.gridRemolques);
            this.TabRemolque.Controls.Add(this.TRemolque);
            this.TabRemolque.Location = new System.Drawing.Point(4, 22);
            this.TabRemolque.Name = "TabRemolque";
            this.TabRemolque.Padding = new System.Windows.Forms.Padding(3);
            this.TabRemolque.Size = new System.Drawing.Size(685, 157);
            this.TabRemolque.TabIndex = 1;
            this.TabRemolque.Text = "Remolques";
            this.TabRemolque.UseVisualStyleBackColor = true;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // CartaPorteMercanciasAutotransporteControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TabControlAutoTransporte);
            this.Name = "CartaPorteMercanciasAutotransporteControl";
            this.Size = new System.Drawing.Size(693, 183);
            this.Load += new System.EventHandler(this.CartaPorteMercanciasAutotransporteControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AnioModeloVM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimaSeguro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRemolques)).EndInit();
            this.TabControlAutoTransporte.ResumeLayout(false);
            this.TabGeneral.ResumeLayout(false);
            this.TabGeneral.PerformLayout();
            this.TabRemolque.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        
        private System.Windows.Forms.DataGridViewTextBoxColumn SubTipoRem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Placa;
        protected internal System.Windows.Forms.ComboBox PermSCT;
        protected internal System.Windows.Forms.TextBox NumPermisoSCT;
        protected internal System.Windows.Forms.ComboBox ConfigVehicular;
        protected internal System.Windows.Forms.TextBox PlacaVM;
        protected internal System.Windows.Forms.NumericUpDown AnioModeloVM;
        protected internal System.Windows.Forms.TextBox AseguraRespCivil;
        protected internal System.Windows.Forms.TextBox PolizaRespCivil;
        protected internal System.Windows.Forms.TextBox AseguraMedAmbiente;
        protected internal System.Windows.Forms.TextBox PolizaMedAmbiente;
        protected internal System.Windows.Forms.TextBox AseguraCarga;
        protected internal System.Windows.Forms.TextBox PolizaCarga;
        protected internal System.Windows.Forms.NumericUpDown PrimaSeguro;
        protected internal System.Windows.Forms.DataGridView gridRemolques;
        protected internal Common.Forms.ToolBarStandarControl TRemolque;
        private System.Windows.Forms.TabPage TabGeneral;
        private System.Windows.Forms.TabPage TabRemolque;
        protected internal System.Windows.Forms.TabControl TabControlAutoTransporte;
        protected internal System.Windows.Forms.CheckBox Incluir;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}
