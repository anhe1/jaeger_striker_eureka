﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class ComprobanteFiscalInformacionGeneralControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.RadLabel12 = new System.Windows.Forms.Label();
            this.ClavePeriodicidad = new System.Windows.Forms.ComboBox();
            this.radLabel1 = new System.Windows.Forms.Label();
            this.ClaveMeses = new System.Windows.Forms.ComboBox();
            this.Ejercicio = new System.Windows.Forms.NumericUpDown();
            this.RadLabel20 = new System.Windows.Forms.Label();
            this.Incluir = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).BeginInit();
            this.SuspendLayout();
            // 
            // RadLabel12
            // 
            this.RadLabel12.Location = new System.Drawing.Point(12, 27);
            this.RadLabel12.Name = "RadLabel12";
            this.RadLabel12.Size = new System.Drawing.Size(71, 18);
            this.RadLabel12.TabIndex = 18;
            this.RadLabel12.Text = "Periodicidad:";
            // 
            // ClavePeriodicidad
            // 
            this.ClavePeriodicidad.DisplayMember = "Descriptor";
            this.ClavePeriodicidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ClavePeriodicidad.Enabled = false;
            this.ClavePeriodicidad.Location = new System.Drawing.Point(94, 26);
            this.ClavePeriodicidad.Name = "ClavePeriodicidad";
            this.ClavePeriodicidad.Size = new System.Drawing.Size(207, 21);
            this.ClavePeriodicidad.TabIndex = 19;
            this.ClavePeriodicidad.TabStop = false;
            this.ClavePeriodicidad.ValueMember = "Clave";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 53);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(40, 18);
            this.radLabel1.TabIndex = 20;
            this.radLabel1.Text = "Meses:";
            // 
            // ClaveMeses
            // 
            this.ClaveMeses.DisplayMember = "Descriptor";
            this.ClaveMeses.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ClaveMeses.Enabled = false;
            this.ClaveMeses.Location = new System.Drawing.Point(94, 52);
            this.ClaveMeses.Name = "ClaveMeses";
            this.ClaveMeses.Size = new System.Drawing.Size(207, 21);
            this.ClaveMeses.TabIndex = 21;
            this.ClaveMeses.TabStop = false;
            this.ClaveMeses.ValueMember = "Clave";
            // 
            // Ejercicio
            // 
            this.Ejercicio.Enabled = false;
            this.Ejercicio.Location = new System.Drawing.Point(94, 78);
            this.Ejercicio.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.Ejercicio.Name = "Ejercicio";
            this.Ejercicio.Size = new System.Drawing.Size(72, 20);
            this.Ejercicio.TabIndex = 40;
            this.Ejercicio.TabStop = false;
            // 
            // RadLabel20
            // 
            this.RadLabel20.Location = new System.Drawing.Point(12, 79);
            this.RadLabel20.Name = "RadLabel20";
            this.RadLabel20.Size = new System.Drawing.Size(29, 18);
            this.RadLabel20.TabIndex = 39;
            this.RadLabel20.Text = "Año:";
            // 
            // Incluir
            // 
            this.Incluir.Location = new System.Drawing.Point(12, 3);
            this.Incluir.Name = "Incluir";
            this.Incluir.Size = new System.Drawing.Size(155, 18);
            this.Incluir.TabIndex = 64;
            this.Incluir.Text = "Incluir Información General";
            this.Incluir.CheckStateChanged += new System.EventHandler(this.Incluir_CheckStateChanged);
            // 
            // ComprobanteFiscalInformacionGeneralControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Incluir);
            this.Controls.Add(this.Ejercicio);
            this.Controls.Add(this.RadLabel20);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.ClaveMeses);
            this.Controls.Add(this.RadLabel12);
            this.Controls.Add(this.ClavePeriodicidad);
            this.MinimumSize = new System.Drawing.Size(1200, 102);
            this.Name = "ComprobanteFiscalInformacionGeneralControl";
            this.Size = new System.Drawing.Size(1200, 103);
            this.Load += new System.EventHandler(this.ComprobanteFiscalInformacionGeneralControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label RadLabel12;
        internal System.Windows.Forms.Label radLabel1;
        internal System.Windows.Forms.Label RadLabel20;
        protected internal System.Windows.Forms.CheckBox Incluir;
        protected internal System.Windows.Forms.ComboBox ClavePeriodicidad;
        protected internal System.Windows.Forms.ComboBox ClaveMeses;
        protected internal System.Windows.Forms.NumericUpDown Ejercicio;
    }
}
