﻿using Jaeger.Aplication.Comprobante.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.Aplication.Comprobante;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class ComprobanteFiscalCancelarForm : Form {
        private ComprobanteFiscalDetailSingleModel _IdDocumento;
        
        protected IComprobanteFiscalService certificacion;

        public ComprobanteFiscalCancelarForm(ComprobanteFiscalDetailSingleModel idDocumento) {
            InitializeComponent();
            this._IdDocumento = idDocumento;
        }

        private void ComprobanteFiscalCancelarForm_Load(object sender, EventArgs e) {
            this.IdDocumento.Text = this._IdDocumento.IdDocumento;
            this.MotivoCancelacion.DisplayMember = "Descriptor";
            this.MotivoCancelacion.ValueMember = "Clave";
            this.MotivoCancelacion.DataSource = ComprobanteCommonService.GetMotivoCancelacion();

            this.certificacion = new ComprobanteFiscalService();
        }

        private void TCancelar_Guardar_Click(object sender, EventArgs e) {
            var _motivo = this.MotivoCancelacion.SelectedValue as MotivoCancelacionModel;
            if (_motivo != null) {
                using (var espera = new WaitingForm(this.Cancelar)) {
                    espera.Text = "Procesando cancelación...";
                    espera.ShowDialog(this);
                }
            } else {
                MessageBox.Show(this, "Indica un motivo válido para la cancelación.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void TCancelar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Cancelar() {
            var _motivo = this.MotivoCancelacion.SelectedValue as MotivoCancelacionModel;
            this.certificacion.Cancelar(this._IdDocumento, _motivo.Id + "|"+ this.Relacionado.Text);
        }

        private void MotivoCancelacion_SelectedValueChanged(object sender, EventArgs e) {
            var _motivo = this.MotivoCancelacion.SelectedValue as MotivoCancelacionModel;
            if (_motivo.Id != "01") {
                this.Relacionado.Enabled = false;
            } else {
                this.Relacionado.Enabled = true;
            }
        }
    }
}
