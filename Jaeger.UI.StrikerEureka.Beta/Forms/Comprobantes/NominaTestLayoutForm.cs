﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class NominaTestLayoutForm : Form {
        public NominaTestLayoutForm() {
            InitializeComponent();
        }

        private void NominaTestLayoutForm_Load(object sender, EventArgs e) {
            List<string> archivos = Directory.GetFiles(@"C:\Users\anhed\Downloads\SEMANA 48", "*.xml", SearchOption.AllDirectories).ToList<string>();
            var stringBuilder = new StringBuilder();
            foreach (var archivo in archivos) {
                var _cfdi = CFDI.V33.Comprobante.Load(archivo);
                stringBuilder.Append(_cfdi.Emisor.Rfc + "|");
                stringBuilder.Append(_cfdi.Emisor.Nombre + "|");
                stringBuilder.Append(_cfdi.Emisor.RegimenFiscal + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Emisor.RegistroPatronal + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.TipoNomina + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.FechaPago.ToString("dd/MM/yyyy") + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.FechaInicialPago.ToString("dd/MM/yyyy") + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.FechaFinalPago.ToString("dd/MM/yyyy") + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.NumDiasPagados + "|");
                stringBuilder.Append(_cfdi.LugarExpedicion + "|");
                stringBuilder.Append(_cfdi.Receptor.Rfc + "|");
                stringBuilder.Append(_cfdi.Receptor.Nombre + "||");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.Curp + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.NumSeguridadSocial + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.FechaInicioRelLaboral + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.Antigüedad + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.TipoContrato + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.TipoJornada + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.TipoRegimen + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.NumEmpleado + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.Departamento + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.Puesto + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.RiesgoPuesto + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.PeriodicidadPago + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.Banco + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.CuentaBancaria + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.SalarioBaseCotApor + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.SalarioDiarioIntegrado + "|");
                stringBuilder.Append(_cfdi.Complemento.Nomina12.Receptor.ClaveEntFed + "|||||||");

                foreach (var item in _cfdi.Complemento.Nomina12.Percepciones.Percepcion) {
                    stringBuilder.Append("1|" + item.TipoPercepcion + "|" + item.Clave + "|" + item.Concepto + "|" + item.ImporteGravado + "|" + item.ImporteExento + "|");
                }

                foreach (var item in _cfdi.Complemento.Nomina12.Deducciones.Deduccion) {
                    stringBuilder.Append("2|" + item.TipoDeduccion + "|" + item.Clave + "|" + item.Concepto + "|0|" + item.Importe + "|");
                }

                foreach (var item in _cfdi.Complemento.Nomina12.OtrosPagos) {
                    stringBuilder.Append("2|" + item.TipoOtroPago + "|" + item.Clave + "|" + item.Concepto + "|0|" + item.Importe + "|");
                }
                stringBuilder.Append("\r\n");
            }

            Util.Services.FileService.WriteFileText(@"C:\Users\anhed\Downloads\SEMANA 48\Layout.txt", stringBuilder.ToString());
        }
    }
}
