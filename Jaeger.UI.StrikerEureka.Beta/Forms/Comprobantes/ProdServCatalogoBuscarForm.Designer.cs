﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class ProdServCatalogoBuscarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TProducto = new Jaeger.UI.Common.Forms.ToolBarStandarSearchControl();
            this.CatalogoSAT = new Jaeger.UI.Forms.Comprobantes.ProdServCatalogoSATBuscarControl();
            this.SuspendLayout();
            // 
            // TProducto
            // 
            this.TProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TProducto.Etiqueta = "Buscar";
            this.TProducto.Location = new System.Drawing.Point(0, 0);
            this.TProducto.Name = "TProducto";
            this.TProducto.ShowActualizar = true;
            this.TProducto.ShowCerrar = true;
            this.TProducto.ShowEditar = false;
            this.TProducto.ShowGuardar = false;
            this.TProducto.ShowHerramientas = false;
            this.TProducto.ShowImprimir = true;
            this.TProducto.ShowNuevo = false;
            this.TProducto.ShowRemover = false;
            this.TProducto.Size = new System.Drawing.Size(494, 25);
            this.TProducto.TabIndex = 1;
            this.TProducto.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TProducto_ButtonCerrar_Click);
            // 
            // CatalogoSAT
            // 
            this.CatalogoSAT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CatalogoSAT.Location = new System.Drawing.Point(0, 25);
            this.CatalogoSAT.Name = "CatalogoSAT";
            this.CatalogoSAT.Size = new System.Drawing.Size(494, 265);
            this.CatalogoSAT.TabIndex = 0;
            // 
            // ProdServCatalogoBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 290);
            this.Controls.Add(this.CatalogoSAT);
            this.Controls.Add(this.TProducto);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProdServCatalogoBuscarForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Productos y Servicios";
            this.Load += new System.EventHandler(this.ProdServCatalogoBuscarForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ProdServCatalogoSATBuscarControl CatalogoSAT;
        private Common.Forms.ToolBarStandarSearchControl TProducto;
    }
}