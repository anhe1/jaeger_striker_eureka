﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.UI.Forms.Comprobantes {
    public partial class ComplementoNominaReceptorControl : UserControl, IComplementoControl {
        private bool _isEditable = false;
        private string _Caption = "Complemento: Nómina";
        protected ITipoContratoCatalogo _tipoContratoCatalogo;
        protected ITipoJornadaCatalogo _tipoJornadaCatalogo;
        protected ITipoRegimenCatalogo _tipoRegimenCatalogo;
        protected IRiesgoPuestoCatalogo _riesgoPuestoCatalogo;
        protected IPeriodicidadPagoCatalogo _periodicidadPagoCatalogo;
        protected IBancosCatalogo _bancosCatalogo;
        public ComplementoNominaReceptorControl() {
            InitializeComponent();
        }

        private void ComplementoNominaReceptorControl_Load(object sender, EventArgs e) {

        }

        public bool IsEditable {
            get { return _isEditable; }
            set { _isEditable = value; }
        }
        public string Caption {
            get { return _Caption; }
            set { _Caption = value; }
        }

        public virtual void Start() {
            this._tipoContratoCatalogo = new TipoContratoCatalogo();
            this._tipoContratoCatalogo.Load();
            this.TipoContrato.DisplayMember = "Descriptor";
            this.TipoContrato.ValueMember = "Clave";
            this.TipoContrato.DataSource = this._tipoContratoCatalogo.Items;

            this._tipoJornadaCatalogo = new TipoJornadaCatalogo();
            this._tipoJornadaCatalogo.Load();
            this.TipoJornada.DisplayMember = "Descriptor";
            this.TipoJornada.ValueMember = "Clave";
            this.TipoJornada.DataSource = this._tipoJornadaCatalogo.Items;

            this._tipoRegimenCatalogo = new CatalogoTipoRegimen();
            this._tipoRegimenCatalogo.Load();
            this.TipoRegimen.DisplayMember = "Descriptor";
            this.TipoRegimen.ValueMember = "Clave";
            this.TipoRegimen.DataSource = this._tipoRegimenCatalogo.Items;

            this._riesgoPuestoCatalogo = new RiesgoPuestoCatalogo();
            this._riesgoPuestoCatalogo.Load();
            this.RiesgoPuesto.DisplayMember = "Descriptor";
            this.RiesgoPuesto.ValueMember = "Clave";
            this.RiesgoPuesto.DataSource = this._riesgoPuestoCatalogo.Items;

            this._periodicidadPagoCatalogo = new PeriodicidadPagoCatalogo();
            this._periodicidadPagoCatalogo.Load();
            this.PeriodicidadPago.DisplayMember = "Descriptor";
            this.PeriodicidadPago.ValueMember = "Clave";
            this.PeriodicidadPago.DataSource = this._periodicidadPagoCatalogo.Items;

            this._bancosCatalogo = new BancosCatalogo();
            this._bancosCatalogo.Load();
            this.Banco.DisplayMember = "Descriptor";
            this.Banco.ValueMember = "Clave";
            this.Banco.DataSource = this._bancosCatalogo.Items;
        }

        public virtual bool IsValid() {
            return true;
        }

        public virtual void CreateBinding() {

        }
    }
}
