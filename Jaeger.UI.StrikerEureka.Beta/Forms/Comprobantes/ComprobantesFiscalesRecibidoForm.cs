﻿using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Forms.Comprobantes {
    public class ComprobantesFiscalesRecibidoForm : ComprobantesFiscalesForm {
        public ComprobantesFiscalesRecibidoForm() : base(CFDISubTipoEnum.Recibido) {
        }
    }
}
