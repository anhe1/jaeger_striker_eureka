﻿namespace Jaeger.UI.Forms.Comprobantes {
    partial class RecibosFiscalesForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RecibosFiscalesForm));
            this.gridRecibos = new System.Windows.Forms.DataGridView();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmisorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Emisor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Receptor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoPlan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoPlanText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaEmision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaTimbre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaCancela = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPagado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Consecutivo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mensualidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SeguroAuto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Otros = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CuotaSeguroVida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CuotaIva = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CuotaGastosAdmon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CuotaAutomovil = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TRecibo = new Jaeger.UI.Common.Forms.ToolBar1CommonControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridRecibos)).BeginInit();
            this.SuspendLayout();
            // 
            // gridRecibos
            // 
            this.gridRecibos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridRecibos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Estado,
            this.Folio,
            this.Serie,
            this.EmisorRFC,
            this.Emisor,
            this.ReceptorRFC,
            this.Receptor,
            this.IdDocumento,
            this.NoPlan,
            this.NoPlanText,
            this.Dia,
            this.FechaEmision,
            this.FechaTimbre,
            this.FechaCancela,
            this.Total,
            this.TotalPagado,
            this.Consecutivo,
            this.Mensualidad,
            this.SeguroAuto,
            this.Otros,
            this.CuotaSeguroVida,
            this.CuotaIva,
            this.CuotaGastosAdmon,
            this.CuotaAutomovil});
            this.gridRecibos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRecibos.Location = new System.Drawing.Point(0, 25);
            this.gridRecibos.Name = "gridRecibos";
            this.gridRecibos.Size = new System.Drawing.Size(800, 425);
            this.gridRecibos.TabIndex = 1;
            // 
            // Estado
            // 
            this.Estado.DataPropertyName = "Estado";
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            this.Estado.Width = 50;
            // 
            // Folio
            // 
            this.Folio.DataPropertyName = "Folio";
            this.Folio.HeaderText = "Folio";
            this.Folio.Name = "Folio";
            this.Folio.Width = 65;
            // 
            // Serie
            // 
            this.Serie.DataPropertyName = "Serie";
            this.Serie.HeaderText = "Serie";
            this.Serie.Name = "Serie";
            this.Serie.Width = 65;
            // 
            // EmisorRFC
            // 
            this.EmisorRFC.DataPropertyName = "EmisorRFC";
            this.EmisorRFC.HeaderText = "RFC (Emisor)";
            this.EmisorRFC.Name = "EmisorRFC";
            // 
            // Emisor
            // 
            this.Emisor.DataPropertyName = "Emisor";
            this.Emisor.HeaderText = "Emisor";
            this.Emisor.Name = "Emisor";
            this.Emisor.Width = 200;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.DataPropertyName = "ReceptorRFC";
            this.ReceptorRFC.HeaderText = "RFC (Receptor)";
            this.ReceptorRFC.Name = "ReceptorRFC";
            // 
            // Receptor
            // 
            this.Receptor.DataPropertyName = "Receptor";
            this.Receptor.HeaderText = "Receptor";
            this.Receptor.Name = "Receptor";
            this.Receptor.Width = 200;
            // 
            // IdDocumento
            // 
            this.IdDocumento.DataPropertyName = "IdDocumento";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.IdDocumento.DefaultCellStyle = dataGridViewCellStyle1;
            this.IdDocumento.HeaderText = "IdDocumento";
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.Width = 250;
            // 
            // NoPlan
            // 
            this.NoPlan.DataPropertyName = "NoPlan";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.NoPlan.DefaultCellStyle = dataGridViewCellStyle2;
            this.NoPlan.HeaderText = "NoPlan";
            this.NoPlan.Name = "NoPlan";
            this.NoPlan.Width = 50;
            // 
            // NoPlanText
            // 
            this.NoPlanText.DataPropertyName = "NoPlanText";
            this.NoPlanText.HeaderText = "Plan";
            this.NoPlanText.Name = "NoPlanText";
            this.NoPlanText.Width = 75;
            // 
            // Dia
            // 
            this.Dia.DataPropertyName = "Dia";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Dia.DefaultCellStyle = dataGridViewCellStyle3;
            this.Dia.HeaderText = "Día";
            this.Dia.Name = "Dia";
            this.Dia.Width = 50;
            // 
            // FechaEmision
            // 
            this.FechaEmision.DataPropertyName = "FechaEmision";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Format = "dd MMM yy";
            this.FechaEmision.DefaultCellStyle = dataGridViewCellStyle4;
            this.FechaEmision.HeaderText = "Fec. Emisión";
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.Width = 75;
            // 
            // FechaTimbre
            // 
            this.FechaTimbre.DataPropertyName = "FechaTimbre";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Format = "dd MMM yy";
            this.FechaTimbre.DefaultCellStyle = dataGridViewCellStyle5;
            this.FechaTimbre.HeaderText = "Fec. Timbre";
            this.FechaTimbre.Name = "FechaTimbre";
            this.FechaTimbre.Width = 75;
            // 
            // FechaCancela
            // 
            this.FechaCancela.DataPropertyName = "FechaCancela";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.Format = "dd MMM yy";
            this.FechaCancela.DefaultCellStyle = dataGridViewCellStyle6;
            this.FechaCancela.HeaderText = "Fec. Cancela";
            this.FechaCancela.Name = "FechaCancela";
            this.FechaCancela.Width = 75;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            dataGridViewCellStyle7.NullValue = null;
            this.Total.DefaultCellStyle = dataGridViewCellStyle7;
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.Width = 75;
            // 
            // TotalPagado
            // 
            this.TotalPagado.DataPropertyName = "TotalPagado";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            this.TotalPagado.DefaultCellStyle = dataGridViewCellStyle8;
            this.TotalPagado.HeaderText = "Total Pagado";
            this.TotalPagado.Name = "TotalPagado";
            // 
            // Consecutivo
            // 
            this.Consecutivo.DataPropertyName = "Consecutivo";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Consecutivo.DefaultCellStyle = dataGridViewCellStyle9;
            this.Consecutivo.HeaderText = "Consecutivo";
            this.Consecutivo.Name = "Consecutivo";
            this.Consecutivo.Width = 75;
            // 
            // Mensualidad
            // 
            this.Mensualidad.DataPropertyName = "Mensualidad";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Mensualidad.DefaultCellStyle = dataGridViewCellStyle10;
            this.Mensualidad.HeaderText = "Mensualidad";
            this.Mensualidad.Name = "Mensualidad";
            this.Mensualidad.Width = 75;
            // 
            // SeguroAuto
            // 
            this.SeguroAuto.DataPropertyName = "SeguroAuto";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N2";
            this.SeguroAuto.DefaultCellStyle = dataGridViewCellStyle11;
            this.SeguroAuto.HeaderText = "SeguroAuto";
            this.SeguroAuto.Name = "SeguroAuto";
            this.SeguroAuto.Width = 75;
            // 
            // Otros
            // 
            this.Otros.DataPropertyName = "Otros";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "N2";
            this.Otros.DefaultCellStyle = dataGridViewCellStyle12;
            this.Otros.HeaderText = "Otros";
            this.Otros.Name = "Otros";
            this.Otros.Width = 75;
            // 
            // CuotaSeguroVida
            // 
            this.CuotaSeguroVida.DataPropertyName = "CuotaSeguroVida";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "N2";
            this.CuotaSeguroVida.DefaultCellStyle = dataGridViewCellStyle13;
            this.CuotaSeguroVida.HeaderText = "CuotaSeguroVida";
            this.CuotaSeguroVida.Name = "CuotaSeguroVida";
            this.CuotaSeguroVida.Width = 75;
            // 
            // CuotaIva
            // 
            this.CuotaIva.DataPropertyName = "CuotaIva";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.Format = "N2";
            this.CuotaIva.DefaultCellStyle = dataGridViewCellStyle14;
            this.CuotaIva.HeaderText = "CuotaIva";
            this.CuotaIva.Name = "CuotaIva";
            this.CuotaIva.Width = 75;
            // 
            // CuotaGastosAdmon
            // 
            this.CuotaGastosAdmon.DataPropertyName = "CuotaGastosAdmon";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle15.Format = "N2";
            this.CuotaGastosAdmon.DefaultCellStyle = dataGridViewCellStyle15;
            this.CuotaGastosAdmon.HeaderText = "CuotaGastosAdmon";
            this.CuotaGastosAdmon.Name = "CuotaGastosAdmon";
            this.CuotaGastosAdmon.Width = 75;
            // 
            // CuotaAutomovil
            // 
            this.CuotaAutomovil.DataPropertyName = "CuotaAutomovil";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle16.Format = "N2";
            this.CuotaAutomovil.DefaultCellStyle = dataGridViewCellStyle16;
            this.CuotaAutomovil.HeaderText = "CuotaAutomovil";
            this.CuotaAutomovil.Name = "CuotaAutomovil";
            this.CuotaAutomovil.Width = 75;
            // 
            // TRecibo
            // 
            this.TRecibo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TRecibo.Location = new System.Drawing.Point(0, 0);
            this.TRecibo.Name = "TRecibo";
            this.TRecibo.ShowActualizar = true;
            this.TRecibo.ShowCancelar = true;
            this.TRecibo.ShowCerrar = true;
            this.TRecibo.ShowEditar = true;
            this.TRecibo.ShowHerramientas = true;
            this.TRecibo.ShowImprimir = false;
            this.TRecibo.ShowNuevo = true;
            this.TRecibo.ShowPeriodo = true;
            this.TRecibo.Size = new System.Drawing.Size(800, 25);
            this.TRecibo.TabIndex = 2;
            this.TRecibo.Actualizar.Click += this.TRecibo_Actualizar_Click;
            this.TRecibo.Cerrar.Click += this.TRecibo_ButtonCerrar_Click;
            // 
            // RecibosFiscalesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.gridRecibos);
            this.Controls.Add(this.TRecibo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RecibosFiscalesForm";
            this.Text = "Recibos Fiscales";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RecibosFiscalesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridRecibos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView gridRecibos;
        private Common.Forms.ToolBar1CommonControl TRecibo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Serie;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmisorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Emisor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Receptor;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoPlan;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoPlanText;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dia;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaEmision;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaTimbre;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaCancela;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalPagado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Consecutivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mensualidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn SeguroAuto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Otros;
        private System.Windows.Forms.DataGridViewTextBoxColumn CuotaSeguroVida;
        private System.Windows.Forms.DataGridViewTextBoxColumn CuotaIva;
        private System.Windows.Forms.DataGridViewTextBoxColumn CuotaGastosAdmon;
        private System.Windows.Forms.DataGridViewTextBoxColumn CuotaAutomovil;
    }
}