﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Shatterdome.Empresas;
using Jaeger.Domain.Shatterdome.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Login {
    public partial class SessionForm : Form {
        protected IShatterdomeService service;
        private BindingList<ShatterdomeDetailModel> empresas;
        private LocalSynapsisService localSynapsisService;

        public SessionForm() {
            InitializeComponent();
        }

        private void SessionForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.gridData.DataGridCommon();
            this.localSynapsisService = new LocalSynapsisService();
            this.localSynapsisService.Load();
            this.service = new ShatterdomeService();
            using (var espera = new WaitingForm(this.Consultar)) {
                espera.Text = "Consultando";
                espera.ShowDialog(this);
            }
            this.gridData.DataSource = this.empresas;
        }

        private void Consultar() {
            this.empresas = this.service.GetList();
        }

        private void ButtonSeleccionar_Click(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                var seleccionado = (ShatterdomeDetailModel)this.gridData.CurrentRow.DataBoundItem;
                if (seleccionado != null) {

                    Jaeger.Aplication.Base.ConfigService.Synapsis = new Domain.Empresa.Entities.Configuracion {
                        RDS = seleccionado.Synapsis.BaseDatos,
                        Amazon = seleccionado.Synapsis.Amazon,
                        ProveedorAutorizado = seleccionado.Synapsis.PAC,
                        Empresa = new Domain.Empresa.Entities.EmpresaGeneral() { RFC = seleccionado.RFC, Clave = seleccionado.Clave, RazonSocial = seleccionado.Nombre }
                    };
                    this.Close();
                }
            }
        }

        private void ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void gridData_DoubleClick(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                this.ToolBarButtonSeleccionar.PerformClick();
            }
        }
    }
}
