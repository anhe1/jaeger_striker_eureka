﻿namespace Jaeger.UI.Forms.Login {
    partial class SessionForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SessionForm));
            this.gridData = new System.Windows.Forms.DataGridView();
            this.gcId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcClave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcDominio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcActivo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ToolBar = new System.Windows.Forms.ToolStrip();
            this.ToolBarButtonSeleccionar = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonCerrar = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            this.ToolBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridData
            // 
            this.gridData.AllowUserToAddRows = false;
            this.gridData.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gcId,
            this.gcClave,
            this.gcRFC,
            this.gcNombre,
            this.gcDominio,
            this.gcActivo});
            this.gridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridData.Location = new System.Drawing.Point(0, 25);
            this.gridData.Name = "gridData";
            this.gridData.RowHeadersVisible = false;
            this.gridData.Size = new System.Drawing.Size(615, 292);
            this.gridData.TabIndex = 2;
            this.gridData.DoubleClick += new System.EventHandler(this.gridData_DoubleClick);
            // 
            // gcId
            // 
            this.gcId.DataPropertyName = "Id";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "#00000";
            this.gcId.DefaultCellStyle = dataGridViewCellStyle2;
            this.gcId.FillWeight = 50F;
            this.gcId.HeaderText = "ID";
            this.gcId.Name = "gcId";
            this.gcId.ReadOnly = true;
            this.gcId.Width = 50;
            // 
            // gcClave
            // 
            this.gcClave.DataPropertyName = "Clave";
            this.gcClave.HeaderText = "Clave";
            this.gcClave.Name = "gcClave";
            this.gcClave.ReadOnly = true;
            this.gcClave.Width = 45;
            // 
            // gcRFC
            // 
            this.gcRFC.DataPropertyName = "RFC";
            this.gcRFC.HeaderText = "RFC";
            this.gcRFC.Name = "gcRFC";
            this.gcRFC.ReadOnly = true;
            // 
            // gcNombre
            // 
            this.gcNombre.DataPropertyName = "Nombre";
            this.gcNombre.HeaderText = "Nombre";
            this.gcNombre.Name = "gcNombre";
            this.gcNombre.ReadOnly = true;
            this.gcNombre.Width = 200;
            // 
            // gcDominio
            // 
            this.gcDominio.DataPropertyName = "Dominio";
            this.gcDominio.HeaderText = "Dominio";
            this.gcDominio.Name = "gcDominio";
            this.gcDominio.ReadOnly = true;
            this.gcDominio.Width = 175;
            // 
            // gcActivo
            // 
            this.gcActivo.DataPropertyName = "Activo";
            this.gcActivo.FillWeight = 50F;
            this.gcActivo.HeaderText = "A";
            this.gcActivo.Name = "gcActivo";
            this.gcActivo.ReadOnly = true;
            this.gcActivo.Width = 35;
            // 
            // ToolBar
            // 
            this.ToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolBarButtonSeleccionar,
            this.ToolBarButtonCerrar});
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Size = new System.Drawing.Size(615, 25);
            this.ToolBar.TabIndex = 3;
            this.ToolBar.Text = "toolStrip1";
            // 
            // ToolBarButtonSeleccionar
            // 
            this.ToolBarButtonSeleccionar.Image = global::Jaeger.UI.Properties.Resources.select_16px;
            this.ToolBarButtonSeleccionar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonSeleccionar.Name = "ToolBarButtonSeleccionar";
            this.ToolBarButtonSeleccionar.Size = new System.Drawing.Size(87, 22);
            this.ToolBarButtonSeleccionar.Text = "Seleccionar";
            this.ToolBarButtonSeleccionar.Click += new System.EventHandler(this.ButtonSeleccionar_Click);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.close_window_16px;
            this.ToolBarButtonCerrar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Size = new System.Drawing.Size(59, 22);
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ButtonCerrar_Click);
            // 
            // SessionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 317);
            this.Controls.Add(this.gridData);
            this.Controls.Add(this.ToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SessionForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sesión";
            this.Load += new System.EventHandler(this.SessionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            this.ToolBar.ResumeLayout(false);
            this.ToolBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gridData;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcId;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcClave;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcDominio;
        private System.Windows.Forms.DataGridViewCheckBoxColumn gcActivo;
        private System.Windows.Forms.ToolStrip ToolBar;
        private System.Windows.Forms.ToolStripButton ToolBarButtonSeleccionar;
        private System.Windows.Forms.ToolStripButton ToolBarButtonCerrar;
    }
}