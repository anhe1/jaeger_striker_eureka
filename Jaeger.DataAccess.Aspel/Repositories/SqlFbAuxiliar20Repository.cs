﻿using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Aspel.Coi80.Contracts;
using Jaeger.Domain.Aspel.Coi80.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.Aspel.Repositories {
    public class SqlFbAuxiliar20Repository : RepositoryMaster<AuxiliarModel>, ISqlAuxiliar20Repository {

        public SqlFbAuxiliar20Repository(DataBaseConfiguracion configuracion) : base(configuracion) { }

        public int Insert(AuxiliarModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO auxiliar20 (tipo_poli, num_poliz, num_part, periodo, ejercicio, num_cta, fecha_pol, concep_po, debe_haber, montomov, numdepto, tipcambio, contrapar, orden, ccostos, cgrupos, idinfadipar, iduuid) 
                    			VALUES (@tipo_poli, @num_poliz, @num_part, @periodo, @ejercicio, @num_cta, @fecha_pol, @concep_po, @debe_haber, @montomov, @numdepto, @tipcambio, @contrapar, @orden, @ccostos, @cgrupos, @idinfadipar, @iduuid)"
            };
            //item.TIPO_POLI = this.Max("TIPO_POLI");
            sqlCommand.Parameters.AddWithValue("@tipo_poli", item.TIPO_POLI);
            sqlCommand.Parameters.AddWithValue("@num_poliz", item.NUM_POLIZ);
            sqlCommand.Parameters.AddWithValue("@num_part", item.NUM_PART);
            sqlCommand.Parameters.AddWithValue("@periodo", item.PERIODO);
            sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
            sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
            sqlCommand.Parameters.AddWithValue("@fecha_pol", item.FECHA_POL);
            sqlCommand.Parameters.AddWithValue("@concep_po", item.CONCEP_PO);
            sqlCommand.Parameters.AddWithValue("@debe_haber", item.DEBE_HABER);
            sqlCommand.Parameters.AddWithValue("@montomov", item.MONTOMOV);
            sqlCommand.Parameters.AddWithValue("@numdepto", item.NUMDEPTO);
            sqlCommand.Parameters.AddWithValue("@tipcambio", item.TIPCAMBIO);
            sqlCommand.Parameters.AddWithValue("@contrapar", item.CONTRAPAR);
            sqlCommand.Parameters.AddWithValue("@orden", item.ORDEN);
            sqlCommand.Parameters.AddWithValue("@ccostos", item.CCOSTOS);
            sqlCommand.Parameters.AddWithValue("@cgrupos", item.CGRUPOS);
            sqlCommand.Parameters.AddWithValue("@idinfadipar", item.IDINFADIPAR);
            sqlCommand.Parameters.AddWithValue("@iduuid", item.IDUUID);
            return this.ExecuteScalar(sqlCommand);
        }

        public int Update(AuxiliarModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE auxiliar20 
                    			SET num_poliz = @num_poliz, num_part = @num_part, periodo = @periodo, ejercicio = @ejercicio, num_cta = @num_cta, fecha_pol = @fecha_pol, concep_po = @concep_po, debe_haber = @debe_haber, montomov = @montomov, numdepto = @numdepto, tipcambio = @tipcambio, contrapar = @contrapar, orden = @orden, ccostos = @ccostos, cgrupos = @cgrupos, idinfadipar = @idinfadipar, iduuid = @iduuid
                    			WHERE tipo_poli = @tipo_poli;"
            };
            sqlCommand.Parameters.AddWithValue("@tipo_poli", item.TIPO_POLI);
            sqlCommand.Parameters.AddWithValue("@num_poliz", item.NUM_POLIZ);
            sqlCommand.Parameters.AddWithValue("@num_part", item.NUM_PART);
            sqlCommand.Parameters.AddWithValue("@periodo", item.PERIODO);
            sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
            sqlCommand.Parameters.AddWithValue("@num_cta", item.NUM_CTA);
            sqlCommand.Parameters.AddWithValue("@fecha_pol", item.FECHA_POL);
            sqlCommand.Parameters.AddWithValue("@concep_po", item.CONCEP_PO);
            sqlCommand.Parameters.AddWithValue("@debe_haber", item.DEBE_HABER);
            sqlCommand.Parameters.AddWithValue("@montomov", item.MONTOMOV);
            sqlCommand.Parameters.AddWithValue("@numdepto", item.NUMDEPTO);
            sqlCommand.Parameters.AddWithValue("@tipcambio", item.TIPCAMBIO);
            sqlCommand.Parameters.AddWithValue("@contrapar", item.CONTRAPAR);
            sqlCommand.Parameters.AddWithValue("@orden", item.ORDEN);
            sqlCommand.Parameters.AddWithValue("@ccostos", item.CCOSTOS);
            sqlCommand.Parameters.AddWithValue("@cgrupos", item.CGRUPOS);
            sqlCommand.Parameters.AddWithValue("@idinfadipar", item.IDINFADIPAR);
            sqlCommand.Parameters.AddWithValue("@iduuid", item.IDUUID);
            return this.ExecuteScalar(sqlCommand);
        }

        public AuxiliarModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM auxiliar20 WHERE tipo_poli = @id")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<AuxiliarModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<AuxiliarModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM auxiliar20")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<AuxiliarModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado de auxiliares de polizas
        /// </summary>
        /// <param name="numpoliza">numero de poliza, -1 para obtener todas</param>
        /// <param name="tipo">tipo de poliza, "*" para obtener todas</param>
        /// <param name="periodo">perido</param>
        /// <param name="ejercicio">ejercicio</param>
        public IEnumerable<AuxiliarDetailModel> GetList(int numpoliza, string tipo, int periodo, int ejercicio) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM auxiliar{0}
                                where @numpoliza periodo = @periodo and ejercicio = @ejercicio @tipo
                                order by num_part asc"
            };

            if (numpoliza > 0) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@numpoliza", "cast(NUM_POLIZ as integer) = @numpoliza and");
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@numpoliza", "");
            }

            if (tipo.Trim() != "*") {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@tipo", "and tipo_poli like @tipo");
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@tipo", "");
            }

            sqlCommand.CommandText = string.Format(sqlCommand.CommandText, ejercicio.ToString().Substring(2, 2));
            sqlCommand.Parameters.AddWithValue("@numpoliza", numpoliza);
            sqlCommand.Parameters.AddWithValue("@periodo", periodo);
            sqlCommand.Parameters.AddWithValue("@ejercicio", ejercicio);
            sqlCommand.Parameters.AddWithValue("@tipo", tipo);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<AuxiliarDetailModel>();
            return mapper.Map(tabla).ToList();
        }

        public bool Delete(int index) {
            throw new System.NotImplementedException();
        }
    }
}
