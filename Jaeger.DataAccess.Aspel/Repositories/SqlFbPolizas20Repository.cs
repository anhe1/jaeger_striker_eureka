﻿using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Aspel.Coi80.Contracts;
using Jaeger.Domain.Aspel.Coi80.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.Aspel.Repositories {
    public class SqlFbPolizas20Repository : RepositoryMaster<PolizaModel>, ISqlPolizas20Repository {
        protected ISqlAuxiliar20Repository auxiliar20Repository;

        public SqlFbPolizas20Repository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.auxiliar20Repository = new SqlFbAuxiliar20Repository(configuracion);
        }

        public int Insert(PolizaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO polizas20 (tipo_poli, num_poliz, periodo, ejercicio, fecha_pol, concep_po, num_part, logaudita, contabiliz, numparcua, tienedocum, proccontab, origen, uuid, espolizapr, uuidop) 
                    			VALUES (@tipo_poli, @num_poliz, @periodo, @ejercicio, @fecha_pol, @concep_po, @num_part, @logaudita, @contabiliz, @numparcua, @tienedocum, @proccontab, @origen, @uuid, @espolizapr, @uuidop)"
            };
            //item.TIPO_POLI = this.Max("TIPO_POLI");
            sqlCommand.Parameters.AddWithValue("@tipo_poli", item.TIPO_POLI);
            sqlCommand.Parameters.AddWithValue("@num_poliz", item.NUM_POLIZ);
            sqlCommand.Parameters.AddWithValue("@periodo", item.PERIODO);
            sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
            sqlCommand.Parameters.AddWithValue("@fecha_pol", item.FECHA_POL);
            sqlCommand.Parameters.AddWithValue("@concep_po", item.CONCEP_PO);
            sqlCommand.Parameters.AddWithValue("@num_part", item.NUM_PART);
            sqlCommand.Parameters.AddWithValue("@logaudita", item.LOGAUDITA);
            sqlCommand.Parameters.AddWithValue("@contabiliz", item.CONTABILIZ);
            sqlCommand.Parameters.AddWithValue("@numparcua", item.NUMPARCUA);
            sqlCommand.Parameters.AddWithValue("@tienedocum", item.TIENEDOCUMENTOS);
            sqlCommand.Parameters.AddWithValue("@proccontab", item.PROCCONTAB);
            sqlCommand.Parameters.AddWithValue("@origen", item.ORIGEN);
            sqlCommand.Parameters.AddWithValue("@uuid", item.UUID);
            sqlCommand.Parameters.AddWithValue("@espolizapr", item.ESPOLIZAPRIVADA);
            sqlCommand.Parameters.AddWithValue("@uuidop", item.UUIDOP);
            return this.ExecuteScalar(sqlCommand);
        }

        public int Update(PolizaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE polizas20 
                    			SET num_poliz = @num_poliz, periodo = @periodo, ejercicio = @ejercicio, fecha_pol = @fecha_pol, concep_po = @concep_po, num_part = @num_part, logaudita = @logaudita, contabiliz = @contabiliz, numparcua = @numparcua, tienedocum = @tienedocum, proccontab = @proccontab, origen = @origen, uuid = @uuid, espolizapr = @espolizapr, uuidop = @uuidop 
                    			WHERE tipo_poli = @tipo_poli;"
            };
            sqlCommand.Parameters.AddWithValue("@tipo_poli", item.TIPO_POLI);
            sqlCommand.Parameters.AddWithValue("@num_poliz", item.NUM_POLIZ);
            sqlCommand.Parameters.AddWithValue("@periodo", item.PERIODO);
            sqlCommand.Parameters.AddWithValue("@ejercicio", item.EJERCICIO);
            sqlCommand.Parameters.AddWithValue("@fecha_pol", item.FECHA_POL);
            sqlCommand.Parameters.AddWithValue("@concep_po", item.CONCEP_PO);
            sqlCommand.Parameters.AddWithValue("@num_part", item.NUM_PART);
            sqlCommand.Parameters.AddWithValue("@logaudita", item.LOGAUDITA);
            sqlCommand.Parameters.AddWithValue("@contabiliz", item.CONTABILIZ);
            sqlCommand.Parameters.AddWithValue("@numparcua", item.NUMPARCUA);
            sqlCommand.Parameters.AddWithValue("@tienedocum", item.TIENEDOCUMENTOS);
            sqlCommand.Parameters.AddWithValue("@proccontab", item.PROCCONTAB);
            sqlCommand.Parameters.AddWithValue("@origen", item.ORIGEN);
            sqlCommand.Parameters.AddWithValue("@uuid", item.UUID);
            sqlCommand.Parameters.AddWithValue("@espolizapr", item.ESPOLIZAPRIVADA);
            sqlCommand.Parameters.AddWithValue("@uuidop", item.UUIDOP);
            return this.ExecuteScalar(sqlCommand);
        }

        public PolizaModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM polizas20 WHERE cast(NUM_POLIZ as integer) = @id")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<PolizaModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public PolizaDetailModel GetById(int numpoliza, string tipo, int periodo, int ejercicio) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM polizas20 
                                where cast(NUM_POLIZ as integer) = @numpoliza and periodo = @periodo and ejercicio = @ejercicio and tipo_poli like @tipo"
            };
            sqlCommand.Parameters.AddWithValue("@numpoliza", numpoliza);
            sqlCommand.Parameters.AddWithValue("@periodo", periodo);
            sqlCommand.Parameters.AddWithValue("@ejercicio", ejercicio);
            sqlCommand.Parameters.AddWithValue("@tipo", tipo);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<PolizaDetailModel>();
            var result = mapper.Map(tabla).SingleOrDefault();

            result.Auxiliares = new System.ComponentModel.BindingList<AuxiliarDetailModel>(this.auxiliar20Repository.GetList(numpoliza, tipo, periodo, ejercicio).ToList());

            return result;
        }

        public IEnumerable<PolizaModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM polizas20")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<PolizaModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado de polizas
        /// </summary>
        /// <param name="periodo">periodo</param>
        /// <param name="ejercicio">ejercicio</param>
        /// <returns></returns>
        public IEnumerable<PolizaDetailModel> GetList(int periodo, int ejercicio) {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from polizas{0} where periodo = @periodo and ejercicio = @ejercicio"
            };

            sqlCommand.CommandText = string.Format(sqlCommand.CommandText, ejercicio.ToString().Substring(2, 2));
            sqlCommand.Parameters.AddWithValue("@periodo", periodo);
            sqlCommand.Parameters.AddWithValue("@ejercicio", ejercicio);

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<PolizaDetailModel>();
            var result = mapper.Map(tabla).ToList();
            var auxiliar = this.auxiliar20Repository.GetList(-1, "*", periodo, ejercicio);

            for (int i = 0; i < result.Count; i++) {
                result[i].Auxiliares = new System.ComponentModel.BindingList<AuxiliarDetailModel>(auxiliar.Where(it => it.EJERCICIO == result[i].EJERCICIO).Where(it => it.PERIODO == result[i].PERIODO).Where(it => it.TIPO_POLI == result[i].TIPO_POLI).Where(it => it.NUM_POLIZ == result[i].NUM_POLIZ).ToList());
            }

            return result;
        }

        public bool Delete(int index) {
            throw new System.NotImplementedException();
        }
    }
}
