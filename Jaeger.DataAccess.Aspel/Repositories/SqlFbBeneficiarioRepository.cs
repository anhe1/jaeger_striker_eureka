﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Aspel.Banco.Contracts;
using Jaeger.Domain.Aspel.Banco.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Aspel.Repositories {
    /// <summary>
    /// repositorio de Beneficiarios (BENEF)
    /// </summary>
    public class SqlFbBeneficiarioRepository : RepositoryMaster<BeneficiarioModel>, ISqlBeneficiarioRepository {
        public SqlFbBeneficiarioRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public BeneficiarioModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<BeneficiarioModel> GetList() {
            var sqlCommand3 = new FbCommand {
                CommandText = @"select * from benef"
            };
            var tabla3 = this.ExecuteReader(sqlCommand3);
            var mapper3 = new DataNamesMapper<BeneficiarioModel>();
            var etapas = mapper3.Map(tabla3).ToList();
            return etapas;
        }

        public int Insert(BeneficiarioModel item) {
            throw new NotImplementedException();
        }

        public int Update(BeneficiarioModel item) {
            throw new NotImplementedException();
        }
    }
}
