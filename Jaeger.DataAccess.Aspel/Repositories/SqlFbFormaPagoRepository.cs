﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Aspel.Banco.Contracts;
using Jaeger.Domain.Aspel.Banco.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.Aspel.Repositories {
    public class SqlFbFormaPagoRepository : RepositoryMaster<FormaPagoModel>, ISqlFormaPagoRepository {
        public SqlFbFormaPagoRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public FormaPagoModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<FormaPagoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from formpago"
            };
            var tabla3 = this.ExecuteReader(sqlCommand);
            var mapper3 = new DataNamesMapper<FormaPagoModel>();
            var etapas = mapper3.Map(tabla3).ToList();
            return etapas;
        }

        public int Insert(FormaPagoModel item) {
            throw new NotImplementedException();
        }

        public int Update(FormaPagoModel item) {
            throw new NotImplementedException();
        }
    }
}
