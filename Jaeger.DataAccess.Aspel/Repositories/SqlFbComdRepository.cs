﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Aspel.Banco.Contracts;
using Jaeger.Domain.Aspel.Banco.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.Aspel.Repositories {
    public class SqlFbComdRepository : RepositoryMaster<COMD>, ISqlComdRepository {
        public SqlFbComdRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public COMD GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<COMD> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from COMD"
            };
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<COMD>();
            var response = mapper.Map(tabla).ToList();
            return response;
        }

        public int Insert(COMD item) {
            throw new NotImplementedException();
        }

        public int Update(COMD item) {
            throw new NotImplementedException();
        }
    }
}
