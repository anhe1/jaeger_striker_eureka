﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Aspel.Banco.Contracts;
using Jaeger.Domain.Aspel.Banco.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.Aspel.Repositories {
    public class SqlFbMovimientosPartidaRepository : RepositoryMaster<PARMOVS01>, ISqlMovimientosPartidaRepository {
        public SqlFbMovimientosPartidaRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public PARMOVS01 GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<PARMOVS01> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from moneda"
            };
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<PARMOVS01>();
            var response = mapper.Map(tabla).ToList();
            return response;
        }

        public int Insert(PARMOVS01 item) {
            throw new NotImplementedException();
        }

        public int Update(PARMOVS01 item) {
            throw new NotImplementedException();
        }
    }
}
