﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Aspel.Banco.Contracts;
using Jaeger.Domain.Aspel.Banco.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.Aspel.Repositories {
    public class SqlFbMonedaRepository : RepositoryMaster<MonedaModel>, ISqlMonedaRepository {
        public SqlFbMonedaRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"delete from moneda where NUM_REG = @NUM_REG"
            };
            sqlCommand.Parameters.AddWithValue("@NUM_REG", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public MonedaModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from moneda where NUM_REG = @NUM_REG"
            };
            sqlCommand.Parameters.AddWithValue("@NUM_REG", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<MonedaModel>();
            var response = mapper.Map(tabla).FirstOrDefault();
            return response;
        }

        public IEnumerable<MonedaModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from moneda"
            };
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<MonedaModel>();
            var response = mapper.Map(tabla).ToList();
            return response;
        }

        public int Insert(MonedaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"insert into moneda ( NUM_REG, MONEDA, PREFIJO, FECHA, TIPOCAMBIO, LEY_SING, LEY_PLUR, TERMINA, IDIOMA, MONEDASAT) value 
                                                   (@NUM_REG,@MONEDA,@PREFIJO,@FECHA,@TIPOCAMBIO,@LEY_SING,@LEY_PLUR,@TERMINA,@IDIOMA,@MONEDASAT)"
            };
            sqlCommand.Parameters.AddWithValue("@NUM_REG", item.NUM_REG);
            sqlCommand.Parameters.AddWithValue("@MONEDA", item.MONEDA);
            sqlCommand.Parameters.AddWithValue("@PREFIJO", item.PREFIJO);
            sqlCommand.Parameters.AddWithValue("@FECHA", item.FECHA);
            sqlCommand.Parameters.AddWithValue("@TIPOCAMBIO", item.TIPOCAMBIO);
            sqlCommand.Parameters.AddWithValue("@LEY_SING", item.LEY_SING);
            sqlCommand.Parameters.AddWithValue("@LEY_PLUR", item.LEY_PLUR);
            sqlCommand.Parameters.AddWithValue("@TERMINA", item.TERMINA);
            sqlCommand.Parameters.AddWithValue("@IDIOMA", item.IDIOMA);
            sqlCommand.Parameters.AddWithValue("@MONEDASAT", item.MONEDASAT);
            return this.ExecuteTransaction(sqlCommand);
        }

        public int Update(MonedaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"update moneda set MONEDA = @MONEDA, PREFIJO = @PREFIJO, FECHA = @FECHA, TIPOCAMBIO = @TIPOCAMBIO, LEY_SING = @LEY_SING, LEY_PLUR = @LEY_PLUR, TERMINA = @TERMINA, IDIOMA = @IDIOMA, MONEDASAT = @MONEDASAT 
                                where NUM_REG = @NUM_REG"
            };
            sqlCommand.Parameters.AddWithValue("@NUM_REG", item.NUM_REG);
            sqlCommand.Parameters.AddWithValue("@MONEDA", item.MONEDA);
            sqlCommand.Parameters.AddWithValue("@PREFIJO", item.PREFIJO);
            sqlCommand.Parameters.AddWithValue("@FECHA", item.FECHA);
            sqlCommand.Parameters.AddWithValue("@TIPOCAMBIO", item.TIPOCAMBIO);
            sqlCommand.Parameters.AddWithValue("@LEY_SING", item.LEY_SING);
            sqlCommand.Parameters.AddWithValue("@LEY_PLUR", item.LEY_PLUR);
            sqlCommand.Parameters.AddWithValue("@TERMINA", item.TERMINA);
            sqlCommand.Parameters.AddWithValue("@IDIOMA", item.IDIOMA);
            sqlCommand.Parameters.AddWithValue("@MONEDASAT", item.MONEDASAT);
            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
