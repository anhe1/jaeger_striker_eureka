﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Aspel.Banco.Contracts;
using Jaeger.Domain.Aspel.Banco.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.Aspel.Repositories {
    /// <summary>
    /// repositorio de cuentas bancarias (CTAS)
    /// </summary>
    public class SqlFbCuentaBancariaRepository : RepositoryMaster<CuentaBancariaModel>, ISqlCuentaBancariaRepository {
        public SqlFbCuentaBancariaRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public CuentaBancariaModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<CuentaBancariaModel> GetList() {
            var sqlCommand3 = new FbCommand {
                CommandText = @"select * from ctas"
            };
            var tabla3 = this.ExecuteReader(sqlCommand3);
            var mapper3 = new DataNamesMapper<CuentaBancariaModel>();
            var etapas = mapper3.Map(tabla3).ToList();
            return etapas;
        }

        public int Insert(CuentaBancariaModel item) {
            throw new NotImplementedException();
        }

        public int Update(CuentaBancariaModel item) {
            throw new NotImplementedException();
        }
    }
}
