﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Aspel.Coi80.Contracts;
using Jaeger.Domain.Aspel.Coi80.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.Aspel.Repositories {
    public class SqlFbRangosRepository : RepositoryMaster<RANGOS>, ISqlRangosRepository {
        public SqlFbRangosRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public int Insert(RANGOS item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO rangos (rango, descrip) 
                    			VALUES (@rango, @descrip)"
            };
            item.RANGO = this.Max("RANGO");
            sqlCommand.Parameters.AddWithValue("@rango", item.RANGO);
            sqlCommand.Parameters.AddWithValue("@descrip", item.DESCRIP);
            return this.ExecuteScalar(sqlCommand);
        }

        public int Update(RANGOS item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE rangos 
                    			SET descrip = @descrip
                    			WHERE rango = @rango;"
            };
            sqlCommand.Parameters.AddWithValue("@rango", item.RANGO);
            sqlCommand.Parameters.AddWithValue("@descrip", item.DESCRIP);
            return this.ExecuteScalar(sqlCommand);
        }

        public RANGOS GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM rangos WHERE rango = @id")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<RANGOS>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<RANGOS> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM rangos")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<RANGOS>();
            return mapper.Map(tabla).ToList();
        }

        public bool Delete(int index) {
            return false;
        }
    }
}
