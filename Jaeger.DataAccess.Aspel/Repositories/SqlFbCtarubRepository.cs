﻿using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Aspel.Coi80.Contracts;
using Jaeger.Domain.Aspel.Coi80.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.Aspel.Repositories {
    public class SqlFbCtarubRepository : RepositoryMaster<CTARUB>, ISqlCtarubRepository {

        public SqlFbCtarubRepository(DataBaseConfiguracion configuracion) : base(configuracion) { }

        public int Insert(CTARUB item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO ctarub (rubro, cuenta, segpapa)
                            VALUES (@rubro, @cuenta, @segpapa)"
            };
            item.RUBRO = this.Max("RUBRO");
            sqlCommand.Parameters.AddWithValue("@rubro", item.RUBRO);
            sqlCommand.Parameters.AddWithValue("@cuenta", item.CUENTA);
            sqlCommand.Parameters.AddWithValue("@segpapa", item.SEGPAPA);
            return this.ExecuteScalar(sqlCommand);
        }

        public int Update(CTARUB item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE ctarub 
                            SET cuenta = @cuenta, segpapa = @segpapa 
                            WHERE rubro = @rubro;"
            };
            sqlCommand.Parameters.AddWithValue("@rubro", item.RUBRO);
            sqlCommand.Parameters.AddWithValue("@cuenta", item.CUENTA);
            sqlCommand.Parameters.AddWithValue("@segpapa", item.SEGPAPA);
            return this.ExecuteScalar(sqlCommand);
        }

        public CTARUB GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM ctarub WHERE rubro = @id")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CTARUB>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<CTARUB> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM ctarub")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CTARUB>();
            return mapper.Map(tabla).ToList();
        }

        public bool Delete(int index) {
            throw new System.NotImplementedException();
        }
    }
}
