﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Aspel.Banco.Contracts;
using Jaeger.Domain.Aspel.Banco.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.Aspel.Repositories {
    public class SqlFbMovimientosRepository : RepositoryMaster<MOVS01>, ISqlMovimientosRepository {
        public SqlFbMovimientosRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public MOVS01 GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<MOVS01> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from movs01"
            };
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<MOVS01>();
            var response = mapper.Map(tabla).ToList();
            return response;
        }

        public int Insert(MOVS01 item) {
            throw new NotImplementedException();
        }

        public int Update(MOVS01 item) {
            throw new NotImplementedException();
        }
    }
}
