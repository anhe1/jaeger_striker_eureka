﻿using System;

namespace Jaeger.UI.LibUtilidades.ObjectsData {
    public class DataControl {
        private string host;

        private string puerto;

        private string pathDescarga;

        private string usuario;

        private string nombreUsuario;

        private bool utlizarDLL;

        private int velocidadDescarga = 10;

        private string cadenaEmpresas = "";

        public string CadenaEmpresas {
            get {
                return this.cadenaEmpresas;
            }
            set {
                this.cadenaEmpresas = value;
            }
        }

        public string Host {
            get {
                return this.host;
            }
            set {
                this.host = value;
            }
        }

        public string NombreUsuario {
            get {
                return this.nombreUsuario;
            }
            set {
                this.nombreUsuario = value;
            }
        }

        public string PathDescarga {
            get {
                return this.pathDescarga;
            }
            set {
                this.pathDescarga = value;
            }
        }

        public string Puerto {
            get {
                return this.puerto;
            }
            set {
                this.puerto = value;
            }
        }

        public string Usuario {
            get {
                return this.usuario;
            }
            set {
                this.usuario = value;
            }
        }

        public bool UtlizarDLL {
            get {
                return this.utlizarDLL;
            }
            set {
                this.utlizarDLL = value;
            }
        }

        public int VelocidadDescarga {
            get {
                return this.velocidadDescarga;
            }
            set {
                this.velocidadDescarga = value;
            }
        }

        public DataControl() {
        }

        public DataControl(string host, string puerto, string pathDescarga, string utlizarDLL, string velocidadDescarga) {
            this.Host = host;
            this.Puerto = puerto;
            this.PathDescarga = pathDescarga;
            this.UtlizarDLL = (utlizarDLL.ToLower().Equals("true") ? true : false);
            try {
                this.VelocidadDescarga = int.Parse(velocidadDescarga);
            }
            catch (Exception exception) {
            }
        }

        public DataControl(string host, string puerto) {
            this.host = host;
            this.puerto = puerto;
        }
    }
}
