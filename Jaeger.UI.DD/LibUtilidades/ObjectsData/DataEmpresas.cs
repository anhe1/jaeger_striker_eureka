﻿namespace Jaeger.UI.LibUtilidades.ObjectsData {
    public class DataEmpresas {
        private string nombreEmpresa;

        private string rfcEmpresa;

        private string grupoEmpresa;

        private bool selectable = true;

        private string idEmpresa;

        public string GrupoEmpresa {
            get {
                return this.grupoEmpresa;
            }
            set {
                this.grupoEmpresa = value;
            }
        }

        public string IdEmpresa {
            get {
                return this.idEmpresa;
            }
            set {
                this.idEmpresa = value;
            }
        }

        public string NombreEmpresa {
            get {
                return this.nombreEmpresa;
            }
            set {
                this.nombreEmpresa = value;
            }
        }

        public string RfcEmpresa {
            get {
                return this.rfcEmpresa;
            }
            set {
                this.rfcEmpresa = value;
            }
        }

        public bool Selectable {
            get {
                return this.selectable;
            }
            set {
                this.selectable = value;
            }
        }

        public DataEmpresas() {
        }

        public DataEmpresas(string idEmpresa, string nombreEmpresa, string rfcEmpresa, string grupoEmpresa) {
            this.idEmpresa = idEmpresa;
            this.nombreEmpresa = nombreEmpresa;
            this.rfcEmpresa = rfcEmpresa;
            this.grupoEmpresa = grupoEmpresa;
        }

        public override string ToString() {
            return this.nombreEmpresa;
        }
    }
}
