﻿using System;
using System.Runtime.CompilerServices;

namespace Jaeger.UI.LibUtilidades {
    public class ManagerControlImport {
        private static volatile ManagerControlImport instancia;

        private long countEncontrado = (long)0;

        private long countProcesado = (long)0;

        private long countImportadas = (long)0;

        private long countImportadasYaExistian = (long)0;

        private volatile int progreso;

        private volatile bool detener = false;

        public long CountEncontrado {
            get {
                return this.countEncontrado;
            }
            set {
                this.countEncontrado = value;
            }
        }

        public long CountImportadas {
            get {
                return this.countImportadas;
            }
            set {
                this.countImportadas = value;
            }
        }

        public long CountImportadasYaExistian {
            get {
                return this.countImportadasYaExistian;
            }
            set {
                this.countImportadasYaExistian = value;
            }
        }

        public long CountProcesado {
            get {
                return this.countProcesado;
            }
            set {
                this.countProcesado = value;
            }
        }

        public bool Detener {
            get {
                return this.detener;
            }
            set {
                this.detener = value;
            }
        }

        public int Progreso {
            get {
                return this.progreso;
            }
            set {
                this.progreso = value;
            }
        }

        static ManagerControlImport() {
            ManagerControlImport.instancia = null;
        }

        private ManagerControlImport() {
        }

        public static ManagerControlImport getSingleton() {
            if (ManagerControlImport.instancia == null) {
                ManagerControlImport.instancia = new ManagerControlImport();
            }
            return ManagerControlImport.instancia;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void sumaCountEncontrado() {
            this.countEncontrado += (long)1;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void sumaCountImportadas() {
            this.countImportadas += (long)1;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void sumaCountImportadasYaExistian() {
            this.countImportadasYaExistian += (long)1;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void sumaCountProcesado() {
            this.countProcesado += (long)1;
            this.progreso = (int)Math.Ceiling((double)this.countProcesado / (double)this.countEncontrado * 100);
        }
    }
}

