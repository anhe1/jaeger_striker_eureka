﻿using Jaeger.UI.LibUtilidades.ObjectsData;
using Jaeger.UI.WSDL;
using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace Jaeger.UI.LibUtilidades {
    public class WorkXmlToWsdl {
        private string pathFile = "";

        private string name = "";

        private string rfc = "";

        private bool elimina = false;

        private bool registra = true;

        private string rfcEmpresa;

        private bool copia = true;

        private PrincipalAdminCom wsdlClient = null;

        private volatile bool trabajando = true;

        private DataControl datos = null;

        private volatile bool _shouldStop = false;

        public bool Copia {
            get {
                return this.copia;
            }
            set {
                this.copia = value;
            }
        }

        public DataControl Datos {
            get {
                return this.datos;
            }
            set {
                this.datos = value;
            }
        }

        public bool Elimina {
            get {
                return this.elimina;
            }
            set {
                this.elimina = value;
            }
        }

        public string Name {
            get {
                return this.name;
            }
            set {
                this.name = value;
            }
        }

        public string PathFile {
            get {
                return this.pathFile;
            }
            set {
                this.pathFile = value;
            }
        }

        public bool Registra {
            get {
                return this.registra;
            }
            set {
                this.registra = value;
            }
        }

        public string RfcEmpresa {
            get {
                return this.rfcEmpresa;
            }
            set {
                this.rfcEmpresa = value;
            }
        }

        public bool Trabajando {
            get {
                return this.trabajando;
            }
            set {
                this.trabajando = value;
            }
        }

        public WorkXmlToWsdl() {
        }

        public void DoWork() {
            bool flag;
            //ObjResponse objResponse;
            string value;
            string str;
            Match match;
            //DataRespWsdlBuzon dataRespWsdlBuzon;
            string str1;
            this.trabajando = true;
            try {
            //    try {
                    this.rfc = string.Concat("\"", ControllerEmpresas.getSingleton().Selected.RfcEmpresa);
                    if (!this.pathFile.ToUpper().EndsWith(".XML")) {
                        Console.WriteLine(string.Concat("El archivo es un pdf->", this.pathFile));
                        flag = false;
                        value = "";
                        str = "[a-f0-9A-F]{8}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{12}";
                        if (Regex.IsMatch(this.pathFile, str)) {
                            match = Regex.Match(this.pathFile, str);
                            value = match.Groups[0].Value;
                            flag = true;
                        }
                        if (ManagerControlImport.getSingleton().Detener) {
                            return;
                        }
            //            else if (flag) {
            //                if (this.wsdlClient == null) {
            //                    this.wsdlClient = new PrincipalAdminCom();
            //                }
            //                objResponse = null;
            //                if (!this.registra) {
            //                    dataRespWsdlBuzon = this.wsdlClient.sincronizaBuzonTributario(this.datos, false, value, File.ReadAllBytes(this.pathFile), ControllerEmpresas.getSingleton().Selected.IdEmpresa, ControllerEmpresas.getSingleton().Selected.RfcEmpresa, "");
            //                    objResponse = (dataRespWsdlBuzon.Error.Length == 0 ? new ObjResponse("", false) : new ObjResponse(dataRespWsdlBuzon.Error, true));
            //                }
            //                else {
            //                    objResponse = this.wsdlClient.enviaXMLyPDF(this.datos, Encoding.Default.GetBytes(value), File.ReadAllBytes(this.pathFile), false, ControllerEmpresas.getSingleton().Selected.IdEmpresa);
            //                }
            //                if (objResponse.getMsjError().Length != 0) {
            //                    str1 = string.Concat(ManagerBitacora.getSingleton().obtenerPathLog(), Path.DirectorySeparatorChar, (new FileInfo(this.pathFile)).Name);
            //                    if (!File.Exists(str1)) {
            //                        File.Copy(this.pathFile, str1);
            //                    }
            //                    ManagerBitacora.getSingleton().escribeLog(string.Concat("Error al importar el archivo: ", this.pathFile, " -> ", objResponse.getMsjError()), false);
            //                }
            //                else if ((objResponse.getSaldo() == null ? false : objResponse.getSaldo().Length != 0)) {
            //                    ManagerBitacora.getSingleton().escribeLog(string.Concat("Ya existia en el Servidor: ", this.pathFile, " Servidor Respondio-> ", objResponse.getData()), false);
            //                }
            //                else {
            //                    ManagerBitacora.getSingleton().escribeLog(string.Concat("Importado con exito: ", this.pathFile, " Servidor Respondio-> ", objResponse.getData()), false);
            //                }
            //            }
                    }
                    else if (File.Exists(this.pathFile)) {
            //            string[] strArrays = null;
            //            try {
            //                strArrays = File.ReadAllLines(this.pathFile, Encoding.UTF8);
            //            }
            //            catch (Exception exception) {
            //                Thread.Sleep(2000);
            //                strArrays = File.ReadAllLines(this.pathFile, Encoding.UTF8);
            //            }
            //            flag = false;
            //            string[] strArrays1 = strArrays;
            //            int i = 0;
            //            while (i < (int)strArrays1.Length) {
            //                string str2 = strArrays1[i].Replace("&amp;", "&");
            //                if (str2.Contains(this.rfc)) {
            //                    flag = true;
            //                    break;
            //                }
            //                else if (!str2.ToLower().Contains(this.rfc.ToLower())) {
            //                    i++;
            //                }
            //                else {
            //                    flag = true;
            //                    break;
            //                }
            //            }
            //            if (!flag) {
            //                strArrays = null;
            //                ManagerBitacora.getSingleton().escribeLog(string.Concat("No es un CFDI de esta Empresa: ", this.pathFile), false);
            //            }
            //            else {
            //                string str3 = "";
            //                strArrays1 = strArrays;
            //                for (i = 0; i < (int)strArrays1.Length; i++) {
            //                    str3 = string.Concat(str3, strArrays1[i]);
            //                }
            //                if (!ManagerControlImport.getSingleton().Detener) {
            //                    if (this.wsdlClient == null) {
            //                        this.wsdlClient = new PrincipalAdminCom();
            //                    }
            //                    objResponse = null;
            //                    if (!this.registra) {
            //                        value = "";
            //                        str = "[a-f0-9A-F]{8}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{12}";
            //                        if (Regex.IsMatch(this.pathFile, str)) {
            //                            match = Regex.Match(this.pathFile, str);
            //                            value = match.Groups[0].Value;
            //                            flag = true;
            //                        }
            //                        if (value.Length == 0) {
            //                            objResponse = new ObjResponse("El XML que se intenta enviar no tiene como nombre el UUID, no se podra enviar.", true);
            //                        }
            //                        else {
            //                            dataRespWsdlBuzon = this.wsdlClient.sincronizaBuzonTributario(this.datos, false, value, Encoding.UTF8.GetBytes(str3), ControllerEmpresas.getSingleton().Selected.IdEmpresa, this.rfcEmpresa, "");
            //                            objResponse = (dataRespWsdlBuzon.Error.Length == 0 ? new ObjResponse("", false) : new ObjResponse(dataRespWsdlBuzon.Error, true));
            //                        }
            //                    }
            //                    else {
            //                        objResponse = this.wsdlClient.enviaXMLyPDF(this.datos, Encoding.UTF8.GetBytes(str3), null, false, ControllerEmpresas.getSingleton().Selected.IdEmpresa);
            //                    }
            //                    if (objResponse.getMsjError().Length == 0) {
            //                        if ((objResponse.getSaldo() == null ? false : objResponse.getSaldo().Length != 0)) {
            //                            ManagerBitacora.getSingleton().escribeLog(string.Concat("Ya existia en el Servidor: ", this.pathFile, " Servidor Respondio-> ", objResponse.getData()), false);
            //                            ManagerControlImport.getSingleton().sumaCountImportadasYaExistian();
            //                        }
            //                        else {
            //                            ManagerBitacora.getSingleton().escribeLog(string.Concat("Importado con exito: ", this.pathFile, " Servidor Respondio-> ", objResponse.getData()), false);
            //                            ManagerControlImport.getSingleton().sumaCountImportadas();
            //                        }
            //                        if (this.elimina) {
            //                            File.Delete(this.pathFile);
            //                        }
            //                    }
            //                    else {
            //                        str1 = string.Concat(ManagerBitacora.getSingleton().obtenerPathLog(), Path.DirectorySeparatorChar, (new FileInfo(this.pathFile)).Name);
            //                        if (!File.Exists(str1)) {
            //                            File.WriteAllLines(str1, strArrays);
            //                        }
            //                        ManagerBitacora.getSingleton().escribeLog(string.Concat("Error al importar el archivo: ", this.pathFile, " Servidor Respondio-> ", objResponse.getMsjError()), false);
            //                    }
            //                }
            //                else {
            //                    return;
            //                }
            //            }
            //        }
            //        else {
            //            ManagerBitacora.getSingleton().escribeLog(string.Concat("No se logró descargar del Portal del SAT el XML: ", this.pathFile, " intente nuevamente o directamente del portal del SAT para asegurar que es no es un problema de la aplicación."), false);
            //            Thread.Sleep(500);
            //            return;
            //        }
            //        Console.WriteLine(string.Concat("FINALIZO->", this.name));
            //    }
            //    catch (Exception exception2) {
            //        Exception exception1 = exception2;
            //        if ((exception1.Message.Contains("extremo escuchando en http") ? true : exception1.Message.Contains("no endpoint listening at http"))) {
            //            ManagerControlImport.getSingleton().Detener = true;
            //        }
            //        MessageBox.Show(null, exception1.Message ?? "", "Error:");
            //        Console.WriteLine(string.Concat("Error: ", exception1.Message));
            //        Console.WriteLine(string.Concat("StackTrace: ", exception1.StackTrace));
            //        ManagerBitacora singleton = ManagerBitacora.getSingleton();
            //        string[] message = new string[] { "Error desconocido: ", exception1.Message, " en: ", this.pathFile, " traza error: ", exception1.StackTrace };
            //        singleton.escribeLog(string.Concat(message), true);
               }
                return;
            }
            finally {
                ManagerControlImport.getSingleton().sumaCountProcesado();
                this.trabajando = false;
            }
        }

        public void RequestStop() {
            this._shouldStop = true;
        }
    }
}

