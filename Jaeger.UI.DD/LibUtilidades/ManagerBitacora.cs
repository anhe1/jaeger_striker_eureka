﻿using System.Runtime.CompilerServices;

namespace Jaeger.UI.LibUtilidades {
    public class ManagerBitacora {
        private static volatile ManagerBitacora instancia;

        private volatile Bitacora bitacora = new Bitacora();

        static ManagerBitacora() {
            ManagerBitacora.instancia = null;
        }

        private ManagerBitacora() {
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void escribeLog(string datos, bool error) {
            if (!error) {
                this.bitacora.escribeINFO(datos);
            }
            else {
                this.bitacora.escribeERROR(datos);
            }
        }

        public void finaliza() {
            this.bitacora.cerrarBitacora();
        }

        public static ManagerBitacora getSingleton() {
            if (ManagerBitacora.instancia == null) {
                ManagerBitacora.instancia = new ManagerBitacora();
            }
            return ManagerBitacora.instancia;
        }

        public void inicializa(string path, string nombreFile) {
            this.bitacora.iniciaBitacora(path, nombreFile);
        }

        public string obtenerPathFileLog() {
            return this.bitacora.PathFileLog;
        }

        public string obtenerPathLog() {
            return this.bitacora.Path;
        }
    }
}

