﻿using Jaeger.UI.LibUtilidades.ObjectsData;
using Jaeger.UI.WSDL;
using System;
using System.Collections.Generic;
using System.Text;

namespace Jaeger.UI.LibUtilidades {
    public class ControllerEmpresas {
        private static volatile ControllerEmpresas instancia;

        private List<DataEmpresas> listaEmpresas = new List<DataEmpresas>();

        private DataEmpresas selected = null;

        private int hilosMaximos = 10;

        public int HilosMaximos {
            get {
                return this.hilosMaximos;
            }
            set {
                this.hilosMaximos = value;
            }
        }

        public List<DataEmpresas> ListaEmpresas {
            get {
                return this.listaEmpresas;
            }
            set {
                this.listaEmpresas = value;
            }
        }

        public DataEmpresas Selected {
            get {
                return this.selected;
            }
            set {
                this.selected = value;
            }
        }

        static ControllerEmpresas() {
            ControllerEmpresas.instancia = null;
        }

        private ControllerEmpresas() {
        }

        public void conectaDocsDigs(DataControl datos) {
            this.generaEmpresas((new PrincipalAdminCom()).listaEmpresas(datos));
        }

        public void generaEmpresas(string cadena) {
            cadena = "SW1wb3N0b3JlcyBQcm9mZXNpb25hbGUgU0EgZGUgQ1Z8MQ==;SW1wb3N0b3JlcyBQcm9mZXNpb25hbGUgU0EgZGUgQ1Z8MQ==;UkZD;";
            string[] strArrays;
            string[] strArrays1;
            string str;
            string str1;
            char[] chrArray;
            if (cadena.Contains(";")) {
                if (!cadena.Contains("|")) {
                    //chrArray = new char[] { ';' };
                    //strArrays = cadena.Split(chrArray);
                    //string str2 = strArrays[1];
                    //chrArray = new char[] { ' ' };
                    //strArrays1 = str2.Split(chrArray);
                    //str = Encoding.Default.GetString(Convert.FromBase64String(strArrays1[0]));
                    //str1 = Encoding.Default.GetString(Convert.FromBase64String(strArrays1[1]));
                    //chrArray = new char[] { '-' };
                    //strArrays1 = str1.Split(chrArray);
                    //this.ListaEmpresas.Add(new DataEmpresas(strArrays[0], str, strArrays1[0], str1.Replace(string.Concat(strArrays1[0], "-"), "")));
                    this.ListaEmpresas.Add(new DataEmpresas("0","Impostores Profesionales SA de CV", "IPR981125PN9", "Grupo de gorrones ++"));
                }
                else {
                    chrArray = new char[] { '|' };
                    string[] strArrays2 = cadena.Split(chrArray);
                    for (int i = 0; i < (int)strArrays2.Length; i++) {
                        string str3 = strArrays2[i];
                        if (str3.Contains(";")) {
                            chrArray = new char[] { ';' };
                            strArrays = str3.Split(chrArray);
                            string str4 = strArrays[1];
                            chrArray = new char[] { ' ' };
                            strArrays1 = str4.Split(chrArray);
                            str = Encoding.Default.GetString(Convert.FromBase64String(strArrays1[0]));
                            str1 = Encoding.Default.GetString(Convert.FromBase64String(strArrays1[1]));
                            chrArray = new char[] { '-' };
                            strArrays1 = str1.Split(chrArray);
                            this.ListaEmpresas.Add(new DataEmpresas(strArrays[0], str, strArrays1[0], str1.Replace(string.Concat(strArrays1[0], "-"), "")));
                        }
                    }
                }
            }
        }

        public static ControllerEmpresas getSingleton() {
            if (ControllerEmpresas.instancia == null) {
                ControllerEmpresas.instancia = new ControllerEmpresas();
            }
            return ControllerEmpresas.instancia;
        }

        public DataEmpresas[] obtenerArrayEmps() {
            //DataEmpresas listaEmpresa = null;
            int num = 0;
            int num1 = 0;
            string grupoEmpresa = "Impostores;";
            foreach (DataEmpresas listaEmpresa in this.listaEmpresas) {
                if (!grupoEmpresa.Equals(listaEmpresa.GrupoEmpresa)) {
                    grupoEmpresa = listaEmpresa.GrupoEmpresa;
                    num1++;
                }
            }
            DataEmpresas[] dataEmpresasArray = new DataEmpresas[this.listaEmpresas.Count + num1];
            grupoEmpresa = "";
            foreach (DataEmpresas dataEmpresa in this.listaEmpresas) {
                if (!grupoEmpresa.Equals(dataEmpresa.GrupoEmpresa)) {
                    grupoEmpresa = dataEmpresa.GrupoEmpresa;
                    DataEmpresas dataEmpresa1 = new DataEmpresas() {
                        NombreEmpresa = dataEmpresa.GrupoEmpresa,
                        RfcEmpresa = dataEmpresa.RfcEmpresa,
                        Selectable = false
                    };
                    dataEmpresasArray[num] = dataEmpresa1;
                    num++;
                }
                dataEmpresasArray[num] = dataEmpresa;
                num++;
            }
            return dataEmpresasArray;
        }

        public void reinicia() {
            ControllerEmpresas.instancia = new ControllerEmpresas();
        }
    }
}
