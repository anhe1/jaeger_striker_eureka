﻿using System.Linq;
using Jaeger.UI.LibUtilidades.ObjectsData;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection.Emit;
using System.Threading;
using System.Windows.Forms;

namespace Jaeger.UI.LibUtilidades {
    public class RecursiveFileSearch {
        private static StringCollection log;

        private DataControl datos = null;

        private bool elimina = false;

        private bool registra = true;

        private bool copia = true;

        private string rfcEmpresa;

        public BackgroundWorker fondoTrabajador1;

        private string tipoProgreso = "Subiendo 0 archivos encontrados ";

        private frmProgresoUpload alert = null;

        private List<string> listaXML = new List<string>();

        private List<string> listaXmlAnterior = new List<string>();

        private bool unaVez = false;

        private Form padre = null;

        private string pathInicial;

        public bool Copia {
            get {
                return this.copia;
            }
            set {
                this.copia = value;
            }
        }

        public DataControl Datos {
            get {
                return this.datos;
            }
            set {
                this.datos = value;
            }
        }

        public bool Elimina {
            get {
                return this.elimina;
            }
            set {
                this.elimina = value;
            }
        }

        public bool Registra {
            get {
                return this.registra;
            }
            set {
                this.registra = value;
            }
        }

        public string RfcEmpresa {
            get {
                return this.rfcEmpresa;
            }
            set {
                this.rfcEmpresa = value;
            }
        }

        static RecursiveFileSearch() {
            RecursiveFileSearch.log = new StringCollection();
        }

        public RecursiveFileSearch(Form papa) {
            this.padre = papa;
            this.unaVez = false;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e) {
            BackgroundWorker backgroundWorker = sender as BackgroundWorker;
            int progreso = 0;
            int num = ManagerControlImport.getSingleton().Progreso;
            backgroundWorker.ReportProgress(progreso);
            Thread.Sleep(50);
            while (progreso != 100) {
                if (backgroundWorker.CancellationPending) {
                    e.Cancel = true;
                    break;
                }
                else if (progreso != ManagerControlImport.getSingleton().Progreso) {
                    progreso = ManagerControlImport.getSingleton().Progreso;
                    backgroundWorker.ReportProgress(progreso);
                    Thread.Sleep(200);
                }
                else {
                    Thread.Sleep(200);
                }
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            try {
                System.Windows.Forms.Label label = this.alert.labelMessage;
                string str = this.tipoProgreso;
                int progressPercentage = e.ProgressPercentage;
                label.Text = string.Concat(str, progressPercentage.ToString(), "%");
                this.alert.progressBar.Value = (e.ProgressPercentage > 100 ? 99 : e.ProgressPercentage);
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                string str1 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "BitacoraModuloDescargas.txt");
                StackTrace stackTrace = new StackTrace(exception, true);
                string[] strArrays = new string[] { string.Concat("Exception a las ", DateTime.Now), string.Concat("Message ---", exception.Message), string.Concat("HelpLink ---", exception.HelpLink), string.Concat("Source ---", exception.Source), string.Concat("StackTrace ---", exception.StackTrace), string.Concat("TargetSite ---", exception.TargetSite), string.Concat("Exception ---", e.ToString()), string.Concat("GetFileLineNumber ---", stackTrace.GetFrame(0).GetFileLineNumber()) };
                File.WriteAllLines(str1, strArrays);
                MessageBox.Show(string.Concat(exception.Message, " Mas informacion en el Log: ", str1), "Error durante la ejecucion:");
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            if (e.Cancelled) {
                ManagerControlImport.getSingleton().Detener = true;
            }
            else if (e.Error != null) {
                ManagerControlImport.getSingleton().Detener = true;
            }
            this.alert.Close();
            this.alert.Dispose();
            this.fondoTrabajador1.Dispose();
        }

        private void buttonCancel_Click(object sender, EventArgs e) {
            if (this.fondoTrabajador1.WorkerSupportsCancellation) {
                this.fondoTrabajador1.CancelAsync();
                this.alert.Close();
                this.alert.Dispose();
                this.fondoTrabajador1.Dispose();
            }
        }

        public void ejecuta_y_Espera(bool iniciaBarra) {
            if (this.listaXML.Count<string>() > 0) {
                if (iniciaBarra) {
                    this.inicializarBarra();
                }
                int count = this.listaXML.Count;
                int num = 0;
                while (num < count) {
                    string item = this.listaXML[num];
                    object[] objArray = new object[] { "------------------------------nombreFile[", num, "]->", item };
                    Console.WriteLine(string.Concat(objArray));
                    Application.DoEvents();
                    if (!ManagerControlImport.getSingleton().Detener) {
                        ManagerThreads.getSingleton().ejecutaTarea(item, this.datos, this.elimina, this.registra, this.rfcEmpresa, this.copia);
                        num++;
                    }
                    else {
                        break;
                    }
                }
            }
        }

        public void inicializarBarra() {
            this.fondoTrabajador1 = new BackgroundWorker() {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };
            this.fondoTrabajador1.DoWork += new DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.fondoTrabajador1.ProgressChanged += new ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.fondoTrabajador1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            this.alert = new frmProgresoUpload();
            this.tipoProgreso = this.tipoProgreso.Replace("0", string.Concat(ManagerControlImport.getSingleton().CountEncontrado));
            this.alert.labelMessage.Text = string.Concat(this.tipoProgreso, "0%");
            this.alert.Canceled += new EventHandler<EventArgs>(this.buttonCancel_Click);
            this.alert.TopMost = true;
            this.alert.Show(this.padre);
            this.fondoTrabajador1.RunWorkerAsync();
        }

        public void iniciarEnPath(string path, bool elimina, string rfcEmpresa) {
            this.pathInicial = path;
            this.Elimina = elimina;
            this.RfcEmpresa = rfcEmpresa;
            this.listaXML.Clear();
            this.listaXmlAnterior.Clear();
            ManagerControlImport.getSingleton().CountEncontrado = (long)0;
            ManagerControlImport.getSingleton().CountProcesado = (long)0;
            ManagerControlImport.getSingleton().CountImportadas = (long)0;
            ManagerControlImport.getSingleton().CountImportadasYaExistian = (long)0;
            ManagerControlImport.getSingleton().Progreso = 0;
            ManagerControlImport.getSingleton().Detener = false;
            DirectoryInfo directoryInfo = new DirectoryInfo(this.pathInicial);
            if (directoryInfo.Name.Equals("Bitacora de Importacion")) {
                this.unaVez = true;
            }
            this.WalkDirectoryTree(directoryInfo);
            this.ejecuta_y_Espera(true);
        }

        public void mueveArchivos(string pathOrigen, string pathDestino) {
            if (!Directory.Exists(pathDestino)) {
                Directory.CreateDirectory(pathDestino);
            }
            if (Directory.Exists(pathOrigen)) {
                string[] files = Directory.GetFiles(pathOrigen);
                for (int i = 0; i < (int)files.Length; i++) {
                    string str = files[i];
                    string fileName = Path.GetFileName(str);
                    File.Copy(str, Path.Combine(pathDestino, fileName), true);
                    File.Delete(str);
                }
            }
        }

        public void revisarDeNuevo() {
            Console.WriteLine(string.Concat(" 1 listaXmlAnterior.Count->", this.listaXmlAnterior.Count));
            this.listaXmlAnterior.AddRange(this.listaXML);
            Console.WriteLine(string.Concat(" 2 listaXmlAnterior.Count->", this.listaXmlAnterior.Count));
            this.listaXML.Clear();
            DirectoryInfo directoryInfo = new DirectoryInfo(this.pathInicial);
            if (directoryInfo.Name.Equals("Bitacora de Importacion")) {
                this.unaVez = true;
            }
            this.WalkDirectoryTree(directoryInfo);
            this.ejecuta_y_Espera(false);
            this.listaXML.Clear();
        }

        private void WalkDirectoryTree(DirectoryInfo root) {
            int i;
            FileInfo[] files = null;
            try {
                if (this.unaVez) {
                    this.unaVez = false;
                }
                else if (root.Name.Equals("Bitacora de Importacion")) {
                    return;
                }
                files = root.GetFiles("*.*");
            }
            catch (UnauthorizedAccessException unauthorizedAccessException1) {
                UnauthorizedAccessException unauthorizedAccessException = unauthorizedAccessException1;
                ManagerBitacora.getSingleton().escribeLog(string.Concat("Error al buscar en la carpeta: ", root.Name, " detalle: ", unauthorizedAccessException.Message), true);
            }
            catch (DirectoryNotFoundException directoryNotFoundException1) {
                DirectoryNotFoundException directoryNotFoundException = directoryNotFoundException1;
                ManagerBitacora.getSingleton().escribeLog(string.Concat("Error al buscar en la carpeta: ", root.Name, " detalle: ", directoryNotFoundException.Message), true);
            }
            if (files != null) {
                FileInfo[] fileInfoArray = files;
                for (i = 0; i < (int)fileInfoArray.Length; i++) {
                    string str = fileInfoArray[i].FullName.Trim();
                    if ((str.ToUpper().EndsWith(".XML") ? true : str.ToUpper().EndsWith(".PDF"))) {
                        if (this.listaXmlAnterior.Count != 0) {
                            Console.WriteLine(string.Concat(" listaXmlAnterior.Contains(", str, ")"));
                            if (!this.listaXmlAnterior.Contains(str)) {
                                this.listaXML.Add(str);
                                ManagerControlImport.getSingleton().sumaCountEncontrado();
                            }
                        }
                        else {
                            this.listaXML.Add(str);
                            ManagerControlImport.getSingleton().sumaCountEncontrado();
                        }
                    }
                }
                DirectoryInfo[] directories = root.GetDirectories();
                for (i = 0; i < (int)directories.Length; i++) {
                    this.WalkDirectoryTree(directories[i]);
                }
            }
        }
    }
}

