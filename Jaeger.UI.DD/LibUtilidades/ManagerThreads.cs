﻿using Jaeger.UI.LibUtilidades.ObjectsData;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Jaeger.UI.LibUtilidades {
    public class ManagerThreads {
        private static volatile ManagerThreads instancia;

        private static int maxHilos;

        private List<WorkThread> listaHilos = new List<WorkThread>();

        private int hiloActual = 0;

        static ManagerThreads() {
            ManagerThreads.instancia = null;
            ManagerThreads.maxHilos = 10;
        }

        private ManagerThreads() {
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void ejecutaTarea(string pathFile, DataControl datos, bool elimina, bool registra, string rfcEmpresa, bool copia) {
            ManagerThreads.maxHilos = ControllerEmpresas.getSingleton().HilosMaximos;
            if (this.listaHilos.LongCount<WorkThread>() >= (long)ManagerThreads.maxHilos) {
                bool flag = true;
                while (flag) {
                    int num = -1;
                    foreach (WorkThread listaHilo in this.listaHilos) {
                        num++;
                        if (this.hiloActual == ManagerThreads.maxHilos - 1) {
                            this.hiloActual = 0;
                        }
                        if (this.hiloActual == num) {
                            this.hiloActual++;
                            if (!listaHilo.isAlive()) {
                                listaHilo.PathFile = pathFile;
                                listaHilo.Registra = registra;
                                listaHilo.run();
                                flag = false;
                                break;
                            }
                        }
                    }
                    if (flag) {
                        Thread.Sleep(10);
                    }
                }
            }
            else if (!ManagerControlImport.getSingleton().Detener) {
                WorkThread workThread = new WorkThread() {
                    PathFile = pathFile,
                    Datos = datos,
                    Elimina = elimina,
                    Registra = registra,
                    RfcEmpresa = rfcEmpresa,
                    Copia = copia,
                    Name = string.Concat("Hilo ", this.listaHilos.LongCount<WorkThread>() + (long)1)
                };
                this.listaHilos.Add(workThread);
                workThread.run();
            }
        }

        public static ManagerThreads getSingleton() {
            if (ManagerThreads.instancia == null) {
                ManagerThreads.instancia = new ManagerThreads();
            }
            return ManagerThreads.instancia;
        }

        public bool terminoTodos() {
            bool flag;
            foreach (WorkThread listaHilo in this.listaHilos) {
                if (listaHilo.isAlive()) {
                    flag = false;
                    return flag;
                }
            }
            flag = true;
            return flag;
        }
    }

    public class WorkThread {
        private string pathFile = "";

        private string name = "";

        private DataControl datos = null;

        private bool elimina = false;

        private bool registra = true;

        private string rfcEmpresa;

        private bool copia = true;

        private Thread workerThread = null;

        private WorkXmlToWsdl workerObject = null;

        public bool Copia {
            get {
                return this.copia;
            }
            set {
                this.copia = value;
            }
        }

        public DataControl Datos {
            get {
                return this.datos;
            }
            set {
                this.datos = value;
            }
        }

        public bool Elimina {
            get {
                return this.elimina;
            }
            set {
                this.elimina = value;
            }
        }

        public string Name {
            get {
                return this.name;
            }
            set {
                this.name = value;
            }
        }

        public string PathFile {
            get {
                return this.pathFile;
            }
            set {
                this.pathFile = value;
            }
        }

        public bool Registra {
            get {
                return this.registra;
            }
            set {
                this.registra = value;
            }
        }

        public string RfcEmpresa {
            get {
                return this.rfcEmpresa;
            }
            set {
                this.rfcEmpresa = value;
            }
        }

        public WorkThread() {
        }

        public void Destroy() {
            this.workerObject.RequestStop();
            if (this.workerThread != null) {
                this.workerThread.Join();
            }
        }

        public bool isAlive() {
            return this.workerObject.Trabajando;
        }

        public void RequestStop() {
            this.workerObject.RequestStop();
        }

        public void run() {
            this.workerObject = new WorkXmlToWsdl() {
                PathFile = this.pathFile,
                Name = this.name,
                Datos = this.datos,
                Elimina = this.elimina,
                Registra = this.registra,
                RfcEmpresa = this.rfcEmpresa,
                Copia = this.copia
            };
            if (this.workerThread != null) {
                this.workerThread.Join();
            }
            this.workerThread = new Thread(new ThreadStart(this.workerObject.DoWork));
            this.workerThread.Start();
        }
    }
}

