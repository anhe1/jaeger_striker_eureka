﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace Jaeger.UI.LibUtilidades {
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [XmlType(AnonymousType = true)]
    public enum DATOSFechaTimbrado {
        [XmlEnum("")]
        Item
    }
}
