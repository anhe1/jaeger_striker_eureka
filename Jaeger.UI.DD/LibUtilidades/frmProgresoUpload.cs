﻿using System;
using System.Windows.Forms;

namespace Jaeger.UI.LibUtilidades {
    public partial class frmProgresoUpload : Form {
        public event EventHandler<EventArgs> Canceled;

        public frmProgresoUpload() {
            InitializeComponent();
        }

        private void frmProgresoUpload_Load(object sender, EventArgs e) {

        }

        private void btnCancelar_Click(object sender, EventArgs e) {
            EventHandler<EventArgs> eventHandler = this.Canceled;
            if (eventHandler != null) {
                eventHandler(this, e);
            }
        }
    }
}
