﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Jaeger.UI.LibUtilidades {

    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [XmlRoot(Namespace = "", IsNullable = false)]
    [XmlType(AnonymousType = true)]
    public class DATOS {
        private DATOSUuid uuidField;

        private bool uuidFieldSpecified;

        private string nombreFinalField;

        private DATOSFechaTimbrado fechaTimbradoField;

        private bool fechaTimbradoFieldSpecified;

        private string cadenaCFDIField;

        private string cadenaPACField;

        [XmlAttribute]
        public string cadenaCFDI {
            get {
                return this.cadenaCFDIField;
            }
            set {
                this.cadenaCFDIField = value;
            }
        }

        [XmlAttribute]
        public string cadenaPAC {
            get {
                return this.cadenaPACField;
            }
            set {
                this.cadenaPACField = value;
            }
        }

        [XmlAttribute]
        public DATOSFechaTimbrado fechaTimbrado {
            get {
                return this.fechaTimbradoField;
            }
            set {
                this.fechaTimbradoField = value;
            }
        }

        [XmlIgnore]
        public bool fechaTimbradoSpecified {
            get {
                return this.fechaTimbradoFieldSpecified;
            }
            set {
                this.fechaTimbradoFieldSpecified = value;
            }
        }

        [XmlAttribute]
        public string nombreFinal {
            get {
                return this.nombreFinalField;
            }
            set {
                this.nombreFinalField = value;
            }
        }

        [XmlAttribute]
        public DATOSUuid uuid {
            get {
                return this.uuidField;
            }
            set {
                this.uuidField = value;
            }
        }

        [XmlIgnore]
        public bool uuidSpecified {
            get {
                return this.uuidFieldSpecified;
            }
            set {
                this.uuidFieldSpecified = value;
            }
        }

        public DATOS() {
        }
    }
}

