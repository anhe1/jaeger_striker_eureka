﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace Jaeger.UI.LibUtilidades {
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [XmlType(AnonymousType = true)]
    public enum DATOSUuid {
        [XmlEnum("")]
        Item,
        [XmlEnum("0193B2EF-82DF-481F-92E6-34D8AB1F849D")]
        Item0193B2EF82DF481F92E634D8AB1F849D
    }
}
