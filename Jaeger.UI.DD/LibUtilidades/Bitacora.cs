﻿using System;
using System.IO;

namespace Jaeger.UI.LibUtilidades {
    public class Bitacora {
        private string path;

        private string nombreFile;

        private string pathFileLog;

        private StreamWriter sw = null;

        public string NombreFile {
            get {
                return this.nombreFile;
            }
            set {
                this.nombreFile = value;
            }
        }

        public string Path {
            get {
                return this.path;
            }
            set {
                this.path = value;
            }
        }

        public string PathFileLog {
            get {
                return this.pathFileLog;
            }
            set {
                this.pathFileLog = value;
            }
        }

        public Bitacora() {
        }

        public void cerrarBitacora() {
            try {
                this.sw.Close();
            }
            catch (Exception exception) {
                Console.WriteLine(string.Concat("Bitacora.cerrarBitacora()->", exception.Message));
            }
        }

        public void escribeERROR(string cadena) {
            try {
                object[] now = new object[] { DateTime.Now, " ERROR - ", cadena, Environment.NewLine };
                cadena = string.Concat(now);
                this.sw.WriteLine(cadena);
                this.sw.Flush();
            }
            catch (Exception exception) {
                Console.WriteLine(string.Concat("Bitacora.escribe()->", exception.Message));
            }
        }

        public void escribeINFO(string cadena) {
            try {
                object[] now = new object[] { DateTime.Now, " INFO - ", cadena, Environment.NewLine };
                cadena = string.Concat(now);
                this.sw.WriteLine(cadena);
                this.sw.Flush();
            }
            catch (Exception exception) {
                Console.WriteLine(string.Concat("Bitacora.escribe()->", exception.Message));
            }
        }

        public void iniciaBitacora(string path, string nombreFile) {
            try {
                this.Path = path;
                object[] day = new object[] { nombreFile, "_", null, null, null, null, null, null, null, null, null, null };
                DateTime now = DateTime.Now;
                day[2] = now.Day;
                day[3] = "_";
                now = DateTime.Now;
                day[4] = now.Month;
                day[5] = "_";
                now = DateTime.Now;
                day[6] = now.Year;
                day[7] = "_";
                now = DateTime.Now;
                day[8] = now.Hour;
                now = DateTime.Now;
                day[9] = now.Minute;
                now = DateTime.Now;
                day[10] = now.Second;
                day[11] = ".txt";
                this.NombreFile = string.Concat(day);
                if (!Directory.Exists(this.Path)) {
                    Directory.CreateDirectory(this.Path);
                }
                this.pathFileLog = string.Concat(this.Path, "\\n", this.NombreFile);
                this.sw = new StreamWriter(this.pathFileLog, true);
            }
            catch (Exception exception) {
                Console.WriteLine(string.Concat("Bitacora.iniciaBitacora()->", exception.Message));
            }
        }
    }
}

