﻿using System.Drawing;
using System.Windows.Forms;

namespace Jaeger.UI.LibAdminCfdi {
    public class FileDownloadStatusCtlHeader : TableLayoutPanel {
        public Label lblFrom = new Label();

        public Label lblTo = new Label();

        public Label lblStatus = new Label();

        public Label lblBytesReceived = new Label();

        public Label lblBytesTotal = new Label();

        public Label lblProgress = new Label();

        public Label lblCancel = new Label();

        public int RowIndex = 0;

        public FileDownloadStatusCtlHeader() {
            this.InitControls();
            base.CellBorderStyle = TableLayoutPanelCellBorderStyle.OutsetDouble;
            this.BackColor = Color.Khaki;
            base.ColumnCount = 7;
            base.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20f));
            base.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20f));
            base.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 15f));
            base.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10f));
            base.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10f));
            base.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 15f));
            base.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10f));
            base.Controls.Add(this.lblFrom, 0, 0);
            base.Controls.Add(this.lblTo, 1, 0);
            base.Controls.Add(this.lblStatus, 2, 0);
            base.Controls.Add(this.lblBytesReceived, 3, 0);
            base.Controls.Add(this.lblBytesTotal, 4, 0);
            base.Controls.Add(this.lblProgress, 5, 0);
            base.Controls.Add(this.lblCancel, 6, 0);
            base.Name = "tableLayoutPanel1";
            base.Size = new Size(700, 28);
            base.RowCount = 1;
            base.RowStyles.Add(new RowStyle(SizeType.Percent, 100f));
        }

        private void InitControls() {
            this.lblFrom.AutoEllipsis = true;
            this.lblFrom.AutoSize = true;
            this.lblFrom.Dock = DockStyle.Fill;
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.TabIndex = 0;
            this.lblFrom.Text = "Downloading From";
            this.lblFrom.TextAlign = ContentAlignment.MiddleLeft;
            this.lblTo.AutoEllipsis = true;
            this.lblTo.AutoSize = true;
            this.lblTo.Dock = DockStyle.Fill;
            this.lblTo.Name = "lblTo";
            this.lblTo.TabIndex = 1;
            this.lblTo.Text = "Downloading To";
            this.lblTo.TextAlign = ContentAlignment.MiddleLeft;
            this.lblStatus.AutoEllipsis = true;
            this.lblStatus.AutoSize = true;
            this.lblStatus.Dock = DockStyle.Fill;
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Text = "Status";
            this.lblStatus.TextAlign = ContentAlignment.MiddleLeft;
            this.lblBytesReceived.AutoEllipsis = true;
            this.lblBytesReceived.AutoSize = true;
            this.lblBytesReceived.Dock = DockStyle.Fill;
            this.lblBytesReceived.Name = "lblBytesReceived";
            this.lblBytesReceived.Text = "Received";
            this.lblBytesReceived.TextAlign = ContentAlignment.MiddleRight;
            this.lblBytesTotal.AutoEllipsis = true;
            this.lblBytesTotal.AutoSize = true;
            this.lblBytesTotal.Dock = DockStyle.Fill;
            this.lblBytesTotal.Name = "lblBytesTotal";
            this.lblBytesTotal.Text = "Total";
            this.lblBytesTotal.TextAlign = ContentAlignment.MiddleRight;
            this.lblProgress.AutoEllipsis = true;
            this.lblProgress.AutoSize = true;
            this.lblProgress.Dock = DockStyle.Fill;
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Text = "Progress";
            this.lblProgress.TextAlign = ContentAlignment.MiddleLeft;
            this.lblCancel.AutoEllipsis = true;
            this.lblCancel.AutoSize = true;
            this.lblCancel.Dock = DockStyle.Fill;
            this.lblCancel.Name = "lblCancel";
            this.lblCancel.Text = "Cancel";
            this.lblCancel.TextAlign = ContentAlignment.MiddleLeft;
        }
    }
}
