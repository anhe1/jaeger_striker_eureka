﻿using System;
using System.Windows.Forms;

namespace Jaeger.UI.LibAdminCfdi {
    public class WindowManager {
        private TabControl _tabControl;

        public ExtendedWebBrowser ActiveBrowser {
            get {
                ExtendedWebBrowser webBrowser;
                TabPage selectedTab = this._tabControl.SelectedTab;
                if (selectedTab != null) {
                    BrowserControl tag = selectedTab.Tag as BrowserControl;
                    if (tag != null) {
                        webBrowser = tag.WebBrowser;
                        return webBrowser;
                    }
                }
                webBrowser = null;
                return webBrowser;
            }
        }

        public BrowserControl ActualBrowserControl {
            get {
                BrowserControl tag;
                TabPage selectedTab = this._tabControl.SelectedTab;
                if (selectedTab == null) {
                    tag = null;
                }
                else {
                    tag = selectedTab.Tag as BrowserControl;
                }
                return tag;
            }
        }

        public WindowManager(TabControl tabControl) {
            this._tabControl = tabControl;
            this._tabControl.SelectedIndexChanged += new EventHandler(this.tabControl_SelectedIndexChanged);
        }

        private static BrowserControl BrowserControlFromBrowser(ExtendedWebBrowser browser) {
            BrowserControl parent;
            if (browser == null) {
                parent = null;
            }
            else if (browser.Parent != null) {
                parent = browser.Parent.Parent as BrowserControl;
            }
            else {
                parent = null;
            }
            return parent;
        }

        private void CheckCommandState() {
            BrowserCommands browserCommand = BrowserCommands.None;
            if (this.ActiveBrowser != null) {
                if (this.ActiveBrowser.CanGoBack) {
                    browserCommand |= BrowserCommands.Back;
                }
                if (this.ActiveBrowser.CanGoForward) {
                    browserCommand |= BrowserCommands.Forward;
                }
                if (this.ActiveBrowser.IsBusy) {
                    browserCommand |= BrowserCommands.Stop;
                }
                browserCommand |= BrowserCommands.Home;
                browserCommand |= BrowserCommands.Search;
                browserCommand |= BrowserCommands.Print;
                browserCommand |= BrowserCommands.PrintPreview;
                browserCommand |= BrowserCommands.Reload;
            }
            this.OnCommandStateChanged(new CommandStateEventArgs(browserCommand));
        }

        public void Close() {
            TabPage selectedTab = this._tabControl.SelectedTab;
            if (selectedTab != null) {
                this._tabControl.TabPages.Remove(selectedTab);
                selectedTab.Dispose();
            }
            if (this._tabControl.TabPages.Count == 0) {
                this._tabControl.Visible = true;
            }
        }

        public ExtendedWebBrowser New() {
            return this.New(true);
        }

        public ExtendedWebBrowser New(bool navigateHome) {
            TabPage tabPage = new TabPage();
            BrowserControl browserControl = new BrowserControl() {
                Tag = tabPage
            };
            tabPage.Tag = browserControl;
            tabPage.Text = "Control 2000";
            browserControl.Dock = DockStyle.Fill;
            tabPage.Controls.Add(browserControl);
            if (navigateHome) {
            }
            browserControl.WebBrowser.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/");
            browserControl.WebBrowser.StatusTextChanged += new EventHandler(this.WebBrowser_StatusTextChanged);
            browserControl.WebBrowser.DocumentTitleChanged += new EventHandler(this.WebBrowser_DocumentTitleChanged);
            browserControl.WebBrowser.CanGoBackChanged += new EventHandler(this.WebBrowser_CanGoBackChanged);
            browserControl.WebBrowser.CanGoForwardChanged += new EventHandler(this.WebBrowser_CanGoForwardChanged);
            browserControl.WebBrowser.Navigated += new WebBrowserNavigatedEventHandler(this.WebBrowser_Navigated);
            browserControl.WebBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this.WebBrowser_DocumentCompleted);
            browserControl.WebBrowser.Quit += new EventHandler(this.WebBrowser_Quit);
            this._tabControl.TabPages.Add(tabPage);
            this._tabControl.SelectedTab = tabPage;
            this._tabControl.Visible = true;
            this._tabControl.Dock = DockStyle.Fill;
            browserControl.Dock = DockStyle.Fill;
            return browserControl.WebBrowser;
        }

        protected virtual void OnCommandStateChanged(CommandStateEventArgs e) {
            if (this.CommandStateChanged != null) {
                this.CommandStateChanged(this, e);
            }
        }

        protected virtual void OnStatusTextChanged(TextChangedEventArgs e) {
            if (this.StatusTextChanged != null) {
                this.StatusTextChanged(this, e);
            }
        }

        public void Open(Uri url) {
            this.New(false).Navigate(url);
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e) {
            this.CheckCommandState();
        }

        private void WebBrowser_CanGoBackChanged(object sender, EventArgs e) {
            this.CheckCommandState();
        }

        private void WebBrowser_CanGoForwardChanged(object sender, EventArgs e) {
            this.CheckCommandState();
        }

        private void WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e) {
            this.CheckCommandState();
        }

        private void WebBrowser_DocumentTitleChanged(object sender, EventArgs e) {
            ExtendedWebBrowser extendedWebBrowser = sender as ExtendedWebBrowser;
            if (extendedWebBrowser != null) {
                BrowserControl browserControl = WindowManager.BrowserControlFromBrowser(extendedWebBrowser);
                if (browserControl != null) {
                    TabPage tag = browserControl.Tag as TabPage;
                    if (tag != null) {
                        string documentTitle = extendedWebBrowser.DocumentTitle;
                        if (string.IsNullOrEmpty(documentTitle)) {
                            documentTitle = "Control 2000";
                        }
                        else if (documentTitle.Length > 30) {
                            documentTitle = string.Concat(documentTitle.Substring(0, 30), "...");
                        }
                        tag.Text = documentTitle;
                        tag.ToolTipText = extendedWebBrowser.DocumentTitle;
                    }
                }
            }
        }

        private void WebBrowser_Navigated(object sender, WebBrowserNavigatedEventArgs e) {
            this.CheckCommandState();
        }

        private void WebBrowser_Quit(object sender, EventArgs e) {
            ExtendedWebBrowser extendedWebBrowser = sender as ExtendedWebBrowser;
            if (extendedWebBrowser != null) {
                BrowserControl browserControl = WindowManager.BrowserControlFromBrowser(extendedWebBrowser);
                if (browserControl != null) {
                    TabPage tag = browserControl.Tag as TabPage;
                    if (tag != null) {
                        this._tabControl.TabPages.Remove(tag);
                        tag.Dispose();
                        if (this._tabControl.TabPages.Count == 0) {
                            this._tabControl.Visible = true;
                        }
                    }
                }
            }
        }

        private void WebBrowser_StatusTextChanged(object sender, EventArgs e) {
            ExtendedWebBrowser extendedWebBrowser = sender as ExtendedWebBrowser;
            //if (extendedWebBrowser != null) {
            //    TabPage tag = WindowManager.BrowserControlFromBrowser(extendedWebBrowser).Tag as TabPage;
            //    if (tag != null) {
            //        if (this._tabControl.SelectedTab == tag) {
            //            this.OnStatusTextChanged(new TextChangedEventArgs(extendedWebBrowser.StatusText));
            //        }
            //    }
            //}
        }

        public event EventHandler<CommandStateEventArgs> CommandStateChanged;

        public event EventHandler<TextChangedEventArgs> StatusTextChanged;
    }
}
