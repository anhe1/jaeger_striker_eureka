﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Windows.Forms;

namespace Jaeger.UI.LibAdminCfdi {
    internal class UnsafeNativeMethods {
        private UnsafeNativeMethods() {
        }

        [Guid("34A715A0-6587-11D0-924A-0020AFC7AC4D")]
        [InterfaceType(2)]
        [TypeLibType(4112)]
        public interface DWebBrowserEvents2 {
            [DispId(250)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void BeforeNavigate2([In] object pDisp, [In] ref object URL, [In] ref object Flags, [In] ref object TargetFrameName, [In] ref object PostData, [In] ref object Headers, [In][Out] ref bool Cancel);

            [DispId(268)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void ClientToHostWindow([In][Out] ref int CX, [In][Out] ref int CY);

            [DispId(105)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void CommandStateChange([In] int Command, [In] bool Enable);

            [DispId(259)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void DocumentComplete([In] object pDisp, [In] ref object URL);

            [DispId(106)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void DownloadBegin();

            [DispId(104)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void DownloadComplete();

            [DispId(270)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void FileDownload([In][Out] ref bool Cancel);

            [DispId(252)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void NavigateComplete2([In] object pDisp, [In] ref object URL);

            [DispId(271)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void NavigateError([In] object pDisp, [In] ref object URL, [In] ref object Frame, [In] ref object StatusCode, [In][Out] ref bool Cancel);

            [DispId(251)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void NewWindow2([In][Out] ref object ppDisp, [In][Out] ref bool Cancel);

            [DispId(273)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void NewWindow3([In][Out] ref object ppDisp, [In][Out] ref bool Cancel, [In] uint dwFlags, [In] string bstrUrlContext, [In] string bstrUrl);

            [DispId(258)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void OnFullScreen([In] bool FullScreen);

            [DispId(256)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void OnMenuBar([In] bool MenuBar);

            [DispId(253)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void OnQuit();

            [DispId(257)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void OnStatusBar([In] bool StatusBar);

            [DispId(260)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void OnTheaterMode([In] bool TheaterMode);

            [DispId(255)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void OnToolBar([In] bool ToolBar);

            [DispId(254)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void OnVisible([In] bool Visible);

            [DispId(225)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void PrintTemplateInstantiation([In] object pDisp);

            [DispId(226)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void PrintTemplateTeardown([In] object pDisp);

            [DispId(272)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void PrivacyImpactedStateChange([In] bool bImpacted);

            [DispId(108)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void ProgressChange([In] int Progress, [In] int ProgressMax);

            [DispId(112)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void PropertyChange([In] string szProperty);

            [DispId(269)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void SetSecureLockIcon([In] int SecureLockIcon);

            [DispId(102)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void StatusTextChange([In] string Text);

            [DispId(113)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void TitleChange([In] string Text);

            [DispId(227)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void UpdatePageStatus([In] object pDisp, [In] ref object nPage, [In] ref object fDone);

            [DispId(263)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void WindowClosing([In] bool IsChildWindow, [In][Out] ref bool Cancel);

            [DispId(267)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void WindowSetHeight([In] int Height);

            [DispId(264)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void WindowSetLeft([In] int Left);

            [DispId(262)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void WindowSetResizable([In] bool Resizable);

            [DispId(265)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void WindowSetTop([In] int Top);

            [DispId(266)]
            [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void WindowSetWidth([In] int Width);
        }

        [Guid("D30C1661-CDAF-11d0-8A3E-00C04FC9E26E")]
        [SuppressUnmanagedCodeSecurity]
        [TypeLibType(TypeLibTypeFlags.FHidden | TypeLibTypeFlags.FDual | TypeLibTypeFlags.FOleAutomation)]
        public interface IWebBrowser2 {
            [DispId(555)]
            bool AddressBar {
                get;
                set;
            }

            [DispId(200)]
            object Application {
                get;
            }

            [DispId(212)]
            bool Busy {
                get;
            }

            [DispId(202)]
            object Container {
                get;
            }

            [DispId(203)]
            object Document {
                get;
            }

            [DispId(400)]
            string FullName {
                get;
            }

            [DispId(407)]
            bool FullScreen {
                get;
                set;
            }

            [DispId(209)]
            int Height {
                get;
                set;
            }

            [DispId(-515)]
            int HWND {
                get;
            }

            [DispId(206)]
            int Left {
                get;
                set;
            }

            [DispId(210)]
            string LocationName {
                get;
            }

            [DispId(211)]
            string LocationURL {
                get;
            }

            [DispId(406)]
            bool MenuBar {
                get;
                set;
            }

            [DispId(0)]
            string Name {
                get;
            }

            [DispId(550)]
            bool Offline {
                get;
                set;
            }

            [DispId(201)]
            object Parent {
                get;
            }

            [DispId(401)]
            string Path {
                get;
            }

            [DispId(-525)]
            WebBrowserReadyState ReadyState {
                get;
            }

            [DispId(552)]
            bool RegisterAsBrowser {
                get;
                set;
            }

            [DispId(553)]
            bool RegisterAsDropTarget {
                get;
                set;
            }

            [DispId(556)]
            bool Resizable {
                get;
                set;
            }

            [DispId(551)]
            bool Silent {
                get;
                set;
            }

            [DispId(403)]
            bool StatusBar {
                get;
                set;
            }

            [DispId(404)]
            string StatusText {
                get;
                set;
            }

            [DispId(554)]
            bool TheaterMode {
                get;
                set;
            }

            [DispId(405)]
            int ToolBar {
                get;
                set;
            }

            [DispId(207)]
            int Top {
                get;
                set;
            }

            [DispId(204)]
            bool TopLevelContainer {
                get;
            }

            [DispId(205)]
            string Type {
                get;
            }

            [DispId(402)]
            bool Visible {
                get;
                set;
            }

            [DispId(208)]
            int Width {
                get;
                set;
            }

            [DispId(301)]
            void ClientToWindow(out int pcx, out int pcy);

            [DispId(502)]
            void ExecWB([In] NativeMethods.OLECMDID cmdID, [In] NativeMethods.OLECMDEXECOPT cmdexecopt, ref object pvaIn, IntPtr pvaOut);

            [DispId(303)]
            object GetProperty([In] string property);

            [DispId(100)]
            void GoBack();

            [DispId(101)]
            void GoForward();

            [DispId(102)]
            void GoHome();

            [DispId(103)]
            void GoSearch();

            [DispId(104)]
            void Navigate([In] string Url, [In] ref object flags, [In] ref object targetFrameName, [In] ref object postData, [In] ref object headers);

            [DispId(500)]
            void Navigate2([In] ref object URL, [In] ref object flags, [In] ref object targetFrameName, [In] ref object postData, [In] ref object headers);

            [DispId(302)]
            void PutProperty([In] string property, [In] object vtValue);

            [DispId(501)]
            NativeMethods.OLECMDF QueryStatusWB([In] NativeMethods.OLECMDID cmdID);

            [DispId(300)]
            void Quit();

            [DispId(-550)]
            void Refresh();

            [DispId(105)]
            void Refresh2([In] ref object level);

            [DispId(503)]
            void ShowBrowserBar([In] ref object pvaClsid, [In] ref object pvarShow, [In] ref object pvarSize);

            [DispId(106)]
            void Stop();
        }
    }
}
