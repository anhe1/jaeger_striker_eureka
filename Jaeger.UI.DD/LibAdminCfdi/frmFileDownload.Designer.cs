﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Jaeger.UI.LibAdminCfdi {
    partial class frmFileDownload {

        protected override void Dispose(bool disposing) {
            if ((!disposing ? false : this.components != null)) {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent() {
            this.tsMain = new ToolStrip();
            this.tsNotifyEndDownload = new ToolStripButton();
            this.tsCloseDownload = new ToolStripButton();
            this.flowLayoutPanel1 = new FlowLayoutPanel();
            this.toolStripSeparator1 = new ToolStripSeparator();
            this.tsClearFinished = new ToolStripButton();
            this.tsSave = new ToolStripButton();
            this.tsMain.SuspendLayout();
            base.SuspendLayout();
            this.tsMain.Items.AddRange(new ToolStripItem[] { this.tsNotifyEndDownload, this.tsCloseDownload, this.toolStripSeparator1, this.tsClearFinished, this.tsSave });

            this.tsMain.Location = new Point(0, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.Size = new Size(808, 25);
            this.tsMain.TabIndex = 0;
            this.tsMain.Text = "toolStrip1";
            this.tsMain.ItemClicked += new ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            this.tsNotifyEndDownload.CheckOnClick = true;
            this.tsNotifyEndDownload.DisplayStyle = ToolStripItemDisplayStyle.Text;

            this.tsNotifyEndDownload.ImageTransparentColor = Color.Magenta;
            this.tsNotifyEndDownload.Name = "tsNotifyEndDownload";
            this.tsNotifyEndDownload.Size = new Size(172, 22);
            this.tsNotifyEndDownload.Text = "Notify When Download Ends";
            this.tsCloseDownload.Alignment = ToolStripItemAlignment.Right;
            this.tsCloseDownload.DisplayStyle = ToolStripItemDisplayStyle.Text;

            this.tsCloseDownload.ImageTransparentColor = Color.Magenta;
            this.tsCloseDownload.Name = "tsCloseDownload";
            this.tsCloseDownload.Size = new Size(43, 22);
            this.tsCloseDownload.Text = "Close";
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Dock = DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new Point(0, 25);
            this.flowLayoutPanel1.Margin = new Padding(1);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new Size(808, 363);
            this.flowLayoutPanel1.TabIndex = 1;
            this.flowLayoutPanel1.WrapContents = false;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new Size(6, 25);
            this.tsClearFinished.DisplayStyle = ToolStripItemDisplayStyle.Text;

            this.tsClearFinished.ImageTransparentColor = Color.Magenta;
            this.tsClearFinished.Name = "tsClearFinished";
            this.tsClearFinished.Size = new Size(93, 22);
            this.tsClearFinished.Text = "Clear Finished";
            this.tsSave.DisplayStyle = ToolStripItemDisplayStyle.Text;

            this.tsSave.ImageTransparentColor = Color.Magenta;
            this.tsSave.Name = "tsSave";
            this.tsSave.Size = new Size(79, 22);
            this.tsSave.Text = "Save to Log";
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(808, 388);
            base.Controls.Add(this.flowLayoutPanel1);
            base.Controls.Add(this.tsMain);
            base.Name = "frmFileDownload";
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "< 0 > File Downloads";
            base.FormClosing += new FormClosingEventHandler(this.frmFileDownload_FormClosing);
            base.Load += new EventHandler(this.frmFileDownload_Load);
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private IContainer components = null;

        private ToolStrip tsMain;

        private ToolStripButton tsNotifyEndDownload;

        private ToolStripButton tsCloseDownload;

        private FlowLayoutPanel flowLayoutPanel1;

        private ToolStripSeparator toolStripSeparator1;

        private ToolStripButton tsClearFinished;

        private ToolStripButton tsSave;
    }
}
