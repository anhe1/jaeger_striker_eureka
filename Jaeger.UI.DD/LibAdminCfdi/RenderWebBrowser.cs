﻿using System;

namespace Jaeger.UI.LibAdminCfdi {
    public class RenderWebBrowser {
        private frmMain ventana = null;

        public RenderWebBrowser() {
            this.ejecutaTarea();
        }

        public void descargaEsta(string url, string nombrefile) {
            this.ventana.descargaEspecificaMava(url, nombrefile);
        }

        public void ejecutaTarea() {
            try {
                this.ventana = new frmMain();
                this.ventana.Show();
                this.ventana.maxWindow();
                this.ventana.openSession = true;
            }
            catch (Exception exception) {
                Console.WriteLine(string.Concat("Error en RenderWebBrowser->", exception.Message));
            }
        }
    }
}
