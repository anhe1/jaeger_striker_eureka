﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Jaeger.UI.LibAdminCfdi {
    partial class frmFileDownload : Form {
        private int m_TotalDownloads = 0;
        private int m_namecounter = 0;

        public bool NotifyEndDownload {
            get {
                return this.tsNotifyEndDownload.Checked;
            }
        }

        public frmFileDownload() {
            this.InitializeComponent();
        }

        public void AddDownloadItem(string browsername, string filename, string url, int uDlId, string FromUrl, string ToPath, int filesize) {
            try {
                FileDownloadStatusCtl fileDownloadStatusCtl = new FileDownloadStatusCtl();
                this.m_namecounter++;
                fileDownloadStatusCtl.Name = string.Concat("DownloadStatus", this.m_namecounter.ToString());
                fileDownloadStatusCtl.lblFrom.Text = FromUrl;
                fileDownloadStatusCtl.lblTo.Text = ToPath;
                fileDownloadStatusCtl.lblStatus.Text = "Downloading";
                fileDownloadStatusCtl.lblBytesReceived.Text = "0";
                fileDownloadStatusCtl.lblBytesTotal.Text = filesize.ToString();
                frmFileDownload.DLIDS dLID = new frmFileDownload.DLIDS(browsername, filename, url, uDlId, filesize);
                fileDownloadStatusCtl.Tag = dLID;
                fileDownloadStatusCtl.btnCancel.Tag = dLID;
                fileDownloadStatusCtl.btnCancel.Click += new EventHandler(this.btnCancel_Click);
                this.flowLayoutPanel1.Controls.Add(fileDownloadStatusCtl);
                this.UpdateThisText(true);
            }
            catch (Exception exception) {
                throw;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            try {
                Button button = sender as Button;
                if (button != null) {
                    frmFileDownload.DLIDS tag = (frmFileDownload.DLIDS)button.Tag;
                    if (base.Owner != null) {
                        //((frmMain)base.Owner).StopFileDownload(tag.BrowserName, tag.DlUid);
                        //button.Enabled = false;
                    }
                }
                else {
                    return;
                }
            }
            catch (Exception exception) {
                throw;
            }
        }

        public void DeleteDownloadItem(string browsername, int uDlId, string url, string Msg) {
            try {
                frmFileDownload.DLIDS tag = new frmFileDownload.DLIDS();
                FileDownloadStatusCtl str = null;
                foreach (Control control in this.flowLayoutPanel1.Controls) {
                    if (control.Tag != null) {
                        tag = (frmFileDownload.DLIDS)control.Tag;
                        if ((tag.DlUid != uDlId || !(tag.URL == url) ? false : tag.BrowserName == browsername)) {
                            tag.DlDone = true;
                            str = (FileDownloadStatusCtl)control;
                            if (str != null) {
                                if (tag.FileSize > 0) {
                                    str.lblBytesReceived.Text = tag.FileSize.ToString();
                                }
                                str.btnCancel.Enabled = false;
                                str.lblStatus.Text = Msg;
                                this.UpdateThisText(false);
                                if (this.NotifyEndDownload) {
                                    MessageBox.Show(this, string.Concat("Finished downloading\r\n", str.lblFrom.ToString(), "\r\nTO:\r\n", str.lblTo.ToString()));
                                }
                                break;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception exception) {
                throw;
            }
        }

        private void frmFileDownload_FormClosing(object sender, FormClosingEventArgs e) {
            if (e.CloseReason == CloseReason.UserClosing) {
                e.Cancel = true;
                base.Hide();
            }
        }

        private void frmFileDownload_Load(object sender, EventArgs e) {
            base.Icon = AllForms.BitmapToIcon(45);
            try {
                FileDownloadStatusCtlHeader fileDownloadStatusCtlHeader = new FileDownloadStatusCtlHeader() {
                    Name = "DownloadStatusHeader"
                };
                this.flowLayoutPanel1.Controls.Add(fileDownloadStatusCtlHeader);
            }
            catch (Exception exception) {
                throw;
            }
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e) {
            frmFileDownload.DLIDS tag;
            Control control = null;
            if (e.ClickedItem.Name == this.tsCloseDownload.Name) {
                base.Hide();
            }
            else if (e.ClickedItem.Name == this.tsSave.Name) {
                if (AllForms.ShowStaticSaveDialogForText(this) == DialogResult.OK) {
                    string str = "====================\r\nDownload Date: ";
                    DateTime today = DateTime.Today;
                    str = string.Concat(str, today.ToLongDateString());
                    str = string.Concat(str, "\r\nDownload From: {0}\r\nDownload To: {0}\r\nFile Size: {0} Bytes\r\n====================\r\n");
                    StreamWriter streamWriter = new StreamWriter(AllForms.m_dlgSave.FileName, true);
                    try {
                        try {
                            tag = new frmFileDownload.DLIDS();
                            FileDownloadStatusCtl fileDownloadStatusCtl = null;
                            foreach (Control control1 in this.flowLayoutPanel1.Controls) {
                                if (control1.Tag != null) {
                                    tag = (frmFileDownload.DLIDS)control1.Tag;
                                    if (tag.DlDone) {
                                        fileDownloadStatusCtl = (FileDownloadStatusCtl)control1;
                                        if (fileDownloadStatusCtl != null) {
                                            streamWriter.Write(string.Format(str, fileDownloadStatusCtl.lblFrom.Text, fileDownloadStatusCtl.lblTo.Text, tag.FileSize.ToString()));
                                        }
                                        else {
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception exception) {
                            throw;
                        }
                    }
                    finally {
                        if (streamWriter != null) {
                            ((IDisposable)streamWriter).Dispose();
                        }
                    }
                }
            }
            else if (e.ClickedItem.Name == this.tsClearFinished.Name) {
                try {
                    tag = new frmFileDownload.DLIDS();
                    foreach (Control control1 in this.flowLayoutPanel1.Controls) {
                        if (control1.Tag != null) {
                            tag = (frmFileDownload.DLIDS)control1.Tag;
                            if (tag.DlDone) {
                                this.flowLayoutPanel1.Controls.Remove(control);
                                break;
                            }
                        }
                    }
                }
                catch (Exception exception1) {
                    throw;
                }
            }
        }

        public void UpdateDownloadItem(string browsername, int uDlId, string url, int progress, int progressmax) {
            try {
                frmFileDownload.DLIDS tag = new frmFileDownload.DLIDS();
                foreach (Control control in this.flowLayoutPanel1.Controls) {
                    if (control.Tag != null) {
                        tag = (frmFileDownload.DLIDS)control.Tag;
                        if ((tag.DlUid != uDlId || !(tag.URL == url) ? false : tag.BrowserName == browsername)) {
                            FileDownloadStatusCtl str = (FileDownloadStatusCtl)control;
                            if ((str == null ? false : progress > 0)) {
                                if ((tag.FileSize != 0 ? false : progressmax > 0)) {
                                    tag.FileSize = progressmax;
                                }
                                if ((tag.FileSize <= 0 ? true : tag.FileSize <= progress)) {
                                    str.lblBytesReceived.Text = progress.ToString();
                                }
                                else {
                                    str.pbProgress.Maximum = tag.FileSize;
                                    str.pbProgress.Value = progress;
                                    str.lblBytesReceived.Text = progress.ToString();
                                }
                            }
                            return;
                        }
                    }
                }
            }
            catch (Exception exception) {
                throw;
            }
        }

        private void UpdateThisText(bool add) {
            if (!add) {
                this.m_TotalDownloads--;
            }
            else {
                this.m_TotalDownloads++;
            }
            this.Text = string.Concat("<< ", this.m_TotalDownloads.ToString(), " >> File Download(s) in progress....");
        }

        private struct DLIDS {
            public string BrowserName;

            public string FileName;

            public string URL;

            public int DlUid;

            public int FileSize;

            public bool DlDone;

            public DLIDS(string browsername_, string filename_, string url_, int dluid_, int filesize_) {
                this.BrowserName = browsername_;
                this.FileName = filename_;
                this.URL = url_;
                this.DlUid = dluid_;
                this.FileSize = filesize_;
                this.DlDone = false;
            }
        }
    }
}
