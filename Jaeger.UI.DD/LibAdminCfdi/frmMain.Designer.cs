﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jaeger.UI.LibAdminCfdi {
    partial class frmMain {

        protected override void Dispose(bool disposing) {
            if ((!disposing ? false : this.components != null)) {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.tsBtnLogin = new System.Windows.Forms.ToolStripButton();
            this.statusStripMain = new System.Windows.Forms.StatusStrip();
            this.tsStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.tsSecurity = new System.Windows.Forms.ToolStripStatusLabel();
            this.ctxMnuOpenWBs = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsGoSearch = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsLblAddress = new System.Windows.Forms.ToolStripLabel();
            this.comboURL = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tsViewMnuTextSize = new System.Windows.Forms.ToolStripMenuItem();
            this.tsViewMnuTextSizeLargest = new System.Windows.Forms.ToolStripMenuItem();
            this.tsViewMnuTextSizeLarger = new System.Windows.Forms.ToolStripMenuItem();
            this.tsViewMnuTextSizeMedium = new System.Windows.Forms.ToolStripMenuItem();
            this.tsViewMnuTextSizeSmaller = new System.Windows.Forms.ToolStripMenuItem();
            this.tsViewMnuTextSizeSmallest = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxMnuCloseWB = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsMnuCloseWB = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsMnuCloasAllWBs = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxMnuWB_A = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ctxMnuACopyUrl = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxMnuACopyUrlText = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.ctxMnuAOpenInFront = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxMnuAOpenInBack = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxMnuWB_Img = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ctxMnuImgCopyImageSource = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxMnuImgCopyImageAlt = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxMnuImgCopyUrlText = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.ctxMnuImgOpenInBack = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxMnuImgOpenInFront = new System.Windows.Forms.ToolStripMenuItem();
            this.fswFavorites = new System.IO.FileSystemWatcher();
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.fondoTrabajador1 = new System.ComponentModel.BackgroundWorker();
            this.statusStripMain.SuspendLayout();
            this.tsGoSearch.SuspendLayout();
            this.ctxMnuCloseWB.SuspendLayout();
            this.ctxMnuWB_A.SuspendLayout();
            this.ctxMnuWB_Img.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fswFavorites)).BeginInit();
            this.toolStripContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsBtnLogin
            // 
            this.tsBtnLogin.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtnLogin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnLogin.Name = "tsBtnLogin";
            this.tsBtnLogin.Size = new System.Drawing.Size(41, 22);
            this.tsBtnLogin.Text = "Login";
            this.tsBtnLogin.ToolTipText = "Login";
            this.tsBtnLogin.Click += new System.EventHandler(this.LoginToolStripButtonClickHandler);
            // 
            // statusStripMain
            // 
            this.statusStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsStatus,
            this.tsProgress,
            this.tsSecurity});
            this.statusStripMain.Location = new System.Drawing.Point(0, 439);
            this.statusStripMain.Name = "statusStripMain";
            this.statusStripMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStripMain.Size = new System.Drawing.Size(740, 22);
            this.statusStripMain.TabIndex = 5;
            // 
            // tsStatus
            // 
            this.tsStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsStatus.ImageTransparentColor = System.Drawing.Color.White;
            this.tsStatus.Name = "tsStatus";
            this.tsStatus.Size = new System.Drawing.Size(667, 17);
            this.tsStatus.Spring = true;
            this.tsStatus.Text = "Ready...";
            this.tsStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsProgress
            // 
            this.tsProgress.Name = "tsProgress";
            this.tsProgress.Size = new System.Drawing.Size(100, 16);
            this.tsProgress.Step = 1;
            this.tsProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.tsProgress.Visible = false;
            // 
            // tsSecurity
            // 
            this.tsSecurity.Name = "tsSecurity";
            this.tsSecurity.Size = new System.Drawing.Size(58, 17);
            this.tsSecurity.Text = "Unknown";
            // 
            // ctxMnuOpenWBs
            // 
            this.ctxMnuOpenWBs.Name = "ctxWB";
            this.ctxMnuOpenWBs.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ctxMnuOpenWBs.Size = new System.Drawing.Size(61, 4);
            // 
            // tsGoSearch
            // 
            this.tsGoSearch.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsGoSearch.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnLogin,
            this.toolStripSeparator3,
            this.tsLblAddress,
            this.comboURL,
            this.toolStripSeparator6});
            this.tsGoSearch.Location = new System.Drawing.Point(0, 0);
            this.tsGoSearch.Name = "tsGoSearch";
            this.tsGoSearch.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsGoSearch.Size = new System.Drawing.Size(740, 25);
            this.tsGoSearch.Stretch = true;
            this.tsGoSearch.TabIndex = 2;
            this.tsGoSearch.Text = "Address";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // tsLblAddress
            // 
            this.tsLblAddress.Name = "tsLblAddress";
            this.tsLblAddress.Size = new System.Drawing.Size(52, 22);
            this.tsLblAddress.Text = "Address:";
            // 
            // comboURL
            // 
            this.comboURL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboURL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.comboURL.Name = "comboURL";
            this.comboURL.Size = new System.Drawing.Size(300, 25);
            this.comboURL.KeyUp += new System.Windows.Forms.KeyEventHandler(this.comboURL_KeyUp);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // tsViewMnuTextSize
            // 
            this.tsViewMnuTextSize.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsViewMnuTextSizeLargest,
            this.tsViewMnuTextSizeLarger,
            this.tsViewMnuTextSizeMedium,
            this.tsViewMnuTextSizeSmaller,
            this.tsViewMnuTextSizeSmallest});
            this.tsViewMnuTextSize.Name = "tsViewMnuTextSize";
            this.tsViewMnuTextSize.Size = new System.Drawing.Size(152, 22);
            this.tsViewMnuTextSize.Text = "Text Size";
            this.tsViewMnuTextSize.DropDownOpening += new System.EventHandler(this.tsMnuTextSize_DropDownOpening);
            // 
            // tsViewMnuTextSizeLargest
            // 
            this.tsViewMnuTextSizeLargest.Name = "tsViewMnuTextSizeLargest";
            this.tsViewMnuTextSizeLargest.Size = new System.Drawing.Size(119, 22);
            this.tsViewMnuTextSizeLargest.Text = "Largest";
            this.tsViewMnuTextSizeLargest.Click += new System.EventHandler(this.tsMnuTextSizeClickHandler);
            // 
            // tsViewMnuTextSizeLarger
            // 
            this.tsViewMnuTextSizeLarger.Name = "tsViewMnuTextSizeLarger";
            this.tsViewMnuTextSizeLarger.Size = new System.Drawing.Size(119, 22);
            this.tsViewMnuTextSizeLarger.Text = "Larger";
            this.tsViewMnuTextSizeLarger.Click += new System.EventHandler(this.tsMnuTextSizeClickHandler);
            // 
            // tsViewMnuTextSizeMedium
            // 
            this.tsViewMnuTextSizeMedium.Name = "tsViewMnuTextSizeMedium";
            this.tsViewMnuTextSizeMedium.Size = new System.Drawing.Size(119, 22);
            this.tsViewMnuTextSizeMedium.Text = "Medium";
            this.tsViewMnuTextSizeMedium.Click += new System.EventHandler(this.tsMnuTextSizeClickHandler);
            // 
            // tsViewMnuTextSizeSmaller
            // 
            this.tsViewMnuTextSizeSmaller.Name = "tsViewMnuTextSizeSmaller";
            this.tsViewMnuTextSizeSmaller.Size = new System.Drawing.Size(119, 22);
            this.tsViewMnuTextSizeSmaller.Text = "Smaller";
            this.tsViewMnuTextSizeSmaller.Click += new System.EventHandler(this.tsMnuTextSizeClickHandler);
            // 
            // tsViewMnuTextSizeSmallest
            // 
            this.tsViewMnuTextSizeSmallest.Name = "tsViewMnuTextSizeSmallest";
            this.tsViewMnuTextSizeSmallest.Size = new System.Drawing.Size(119, 22);
            this.tsViewMnuTextSizeSmallest.Text = "Smallest";
            this.tsViewMnuTextSizeSmallest.Click += new System.EventHandler(this.tsMnuTextSizeClickHandler);
            // 
            // ctxMnuCloseWB
            // 
            this.ctxMnuCloseWB.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMnuCloseWB,
            this.toolStripSeparator4,
            this.tsMnuCloasAllWBs});
            this.ctxMnuCloseWB.Name = "ctxMnuCloseWB";
            this.ctxMnuCloseWB.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ctxMnuCloseWB.Size = new System.Drawing.Size(121, 54);
            // 
            // tsMnuCloseWB
            // 
            this.tsMnuCloseWB.Name = "tsMnuCloseWB";
            this.tsMnuCloseWB.Size = new System.Drawing.Size(120, 22);
            this.tsMnuCloseWB.Text = "Close";
            this.tsMnuCloseWB.Click += new System.EventHandler(this.tsMnuCloseWB_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(117, 6);
            // 
            // tsMnuCloasAllWBs
            // 
            this.tsMnuCloasAllWBs.Name = "tsMnuCloasAllWBs";
            this.tsMnuCloasAllWBs.Size = new System.Drawing.Size(120, 22);
            this.tsMnuCloasAllWBs.Text = "Close All";
            this.tsMnuCloasAllWBs.Click += new System.EventHandler(this.tsMnuCloasAllWBs_Click);
            // 
            // ctxMnuWB_A
            // 
            this.ctxMnuWB_A.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ctxMnuACopyUrl,
            this.ctxMnuACopyUrlText,
            this.toolStripSeparator7,
            this.ctxMnuAOpenInFront,
            this.ctxMnuAOpenInBack});
            this.ctxMnuWB_A.Name = "ctxMnuRClickWB";
            this.ctxMnuWB_A.Size = new System.Drawing.Size(225, 98);
            // 
            // ctxMnuACopyUrl
            // 
            this.ctxMnuACopyUrl.Name = "ctxMnuACopyUrl";
            this.ctxMnuACopyUrl.Size = new System.Drawing.Size(224, 22);
            this.ctxMnuACopyUrl.Text = "Copy URL";
            this.ctxMnuACopyUrl.Click += new System.EventHandler(this.BrowserCtxMenuClickHandler);
            // 
            // ctxMnuACopyUrlText
            // 
            this.ctxMnuACopyUrlText.Name = "ctxMnuACopyUrlText";
            this.ctxMnuACopyUrlText.Size = new System.Drawing.Size(224, 22);
            this.ctxMnuACopyUrlText.Text = "Copy URL text";
            this.ctxMnuACopyUrlText.Click += new System.EventHandler(this.BrowserCtxMenuClickHandler);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(221, 6);
            // 
            // ctxMnuAOpenInFront
            // 
            this.ctxMnuAOpenInFront.Name = "ctxMnuAOpenInFront";
            this.ctxMnuAOpenInFront.Size = new System.Drawing.Size(224, 22);
            this.ctxMnuAOpenInFront.Text = "Open in new foreground tab";
            this.ctxMnuAOpenInFront.Click += new System.EventHandler(this.BrowserCtxMenuClickHandler);
            // 
            // ctxMnuAOpenInBack
            // 
            this.ctxMnuAOpenInBack.Name = "ctxMnuAOpenInBack";
            this.ctxMnuAOpenInBack.Size = new System.Drawing.Size(224, 22);
            this.ctxMnuAOpenInBack.Text = "Open in new backgrond tab";
            this.ctxMnuAOpenInBack.Click += new System.EventHandler(this.BrowserCtxMenuClickHandler);
            // 
            // ctxMnuWB_Img
            // 
            this.ctxMnuWB_Img.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ctxMnuImgCopyImageSource,
            this.ctxMnuImgCopyImageAlt,
            this.ctxMnuImgCopyUrlText,
            this.toolStripSeparator9,
            this.ctxMnuImgOpenInBack,
            this.ctxMnuImgOpenInFront});
            this.ctxMnuWB_Img.Name = "ctxMnuWB_Img";
            this.ctxMnuWB_Img.Size = new System.Drawing.Size(232, 120);
            // 
            // ctxMnuImgCopyImageSource
            // 
            this.ctxMnuImgCopyImageSource.Name = "ctxMnuImgCopyImageSource";
            this.ctxMnuImgCopyImageSource.Size = new System.Drawing.Size(231, 22);
            this.ctxMnuImgCopyImageSource.Text = "Copy Image Source";
            this.ctxMnuImgCopyImageSource.Click += new System.EventHandler(this.BrowserCtxMenuClickHandler);
            // 
            // ctxMnuImgCopyImageAlt
            // 
            this.ctxMnuImgCopyImageAlt.Name = "ctxMnuImgCopyImageAlt";
            this.ctxMnuImgCopyImageAlt.Size = new System.Drawing.Size(231, 22);
            this.ctxMnuImgCopyImageAlt.Text = "Copy Image Alt";
            this.ctxMnuImgCopyImageAlt.Click += new System.EventHandler(this.BrowserCtxMenuClickHandler);
            // 
            // ctxMnuImgCopyUrlText
            // 
            this.ctxMnuImgCopyUrlText.Name = "ctxMnuImgCopyUrlText";
            this.ctxMnuImgCopyUrlText.Size = new System.Drawing.Size(231, 22);
            this.ctxMnuImgCopyUrlText.Text = "Copy Url";
            this.ctxMnuImgCopyUrlText.Click += new System.EventHandler(this.BrowserCtxMenuClickHandler);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(228, 6);
            // 
            // ctxMnuImgOpenInBack
            // 
            this.ctxMnuImgOpenInBack.Name = "ctxMnuImgOpenInBack";
            this.ctxMnuImgOpenInBack.Size = new System.Drawing.Size(231, 22);
            this.ctxMnuImgOpenInBack.Text = "Open in New Background Tab";
            this.ctxMnuImgOpenInBack.Click += new System.EventHandler(this.BrowserCtxMenuClickHandler);
            // 
            // ctxMnuImgOpenInFront
            // 
            this.ctxMnuImgOpenInFront.Name = "ctxMnuImgOpenInFront";
            this.ctxMnuImgOpenInFront.Size = new System.Drawing.Size(231, 22);
            this.ctxMnuImgOpenInFront.Text = "Open in New Foreground Tab";
            this.ctxMnuImgOpenInFront.Click += new System.EventHandler(this.BrowserCtxMenuClickHandler);
            // 
            // fswFavorites
            // 
            this.fswFavorites.EnableRaisingEvents = true;
            this.fswFavorites.IncludeSubdirectories = true;
            this.fswFavorites.SynchronizingObject = this;
            this.fswFavorites.Changed += new System.IO.FileSystemEventHandler(this.fswFavorites_Changed);
            this.fswFavorites.Created += new System.IO.FileSystemEventHandler(this.fswFavorites_Created);
            this.fswFavorites.Deleted += new System.IO.FileSystemEventHandler(this.fswFavorites_Deleted);
            this.fswFavorites.Renamed += new System.IO.RenamedEventHandler(this.fswFavorites_Renamed);
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.BackColor = System.Drawing.SystemColors.Control;
            this.ContentPanel.Size = new System.Drawing.Size(740, 365);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(740, 389);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 25);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(740, 414);
            this.toolStripContainer1.TabIndex = 7;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // fondoTrabajador1
            // 
            this.fondoTrabajador1.WorkerReportsProgress = true;
            this.fondoTrabajador1.WorkerSupportsCancellation = true;
            this.fondoTrabajador1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.fondoTrabajador1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.fondoTrabajador1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 461);
            this.Controls.Add(this.toolStripContainer1);
            this.Controls.Add(this.statusStripMain);
            this.Controls.Add(this.tsGoSearch);
            this.Location = new System.Drawing.Point(150, 0);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "DemoApp";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.statusStripMain.ResumeLayout(false);
            this.statusStripMain.PerformLayout();
            this.tsGoSearch.ResumeLayout(false);
            this.tsGoSearch.PerformLayout();
            this.ctxMnuCloseWB.ResumeLayout(false);
            this.ctxMnuWB_A.ResumeLayout(false);
            this.ctxMnuWB_Img.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fswFavorites)).EndInit();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private IContainer components = null;

        private StatusStrip statusStripMain;

        private ToolStripStatusLabel tsStatus;

        private ContextMenuStrip ctxMnuOpenWBs;

        private ToolStrip tsGoSearch;

        private ToolStripLabel tsLblAddress;

        private ToolStripComboBox comboURL;

        private ToolStripProgressBar tsProgress;

        private ToolStripStatusLabel tsSecurity;

        private ToolStripSeparator toolStripSeparator6;

        private ToolStripSeparator toolStripSeparator3;

        private ContextMenuStrip ctxMnuCloseWB;

        private ToolStripMenuItem tsMnuCloseWB;

        private ToolStripSeparator toolStripSeparator4;

        private ToolStripMenuItem tsMnuCloasAllWBs;

        private ContextMenuStrip ctxMnuWB_A;

        private ToolStripMenuItem ctxMnuACopyUrl;

        private ToolStripMenuItem ctxMnuACopyUrlText;

        private ToolStripSeparator toolStripSeparator7;

        private ToolStripMenuItem ctxMnuAOpenInFront;

        private ToolStripMenuItem ctxMnuAOpenInBack;

        private ContextMenuStrip ctxMnuWB_Img;

        private ToolStripMenuItem ctxMnuImgCopyImageSource;

        private ToolStripMenuItem ctxMnuImgCopyImageAlt;

        private ToolStripMenuItem ctxMnuImgCopyUrlText;

        private ToolStripSeparator toolStripSeparator9;

        private ToolStripMenuItem ctxMnuImgOpenInBack;

        private ToolStripMenuItem ctxMnuImgOpenInFront;

        private ToolStripMenuItem tsViewMnuTextSize;

        private ToolStripMenuItem tsViewMnuTextSizeLargest;

        private ToolStripMenuItem tsViewMnuTextSizeLarger;

        private ToolStripMenuItem tsViewMnuTextSizeMedium;

        private ToolStripMenuItem tsViewMnuTextSizeSmaller;

        private ToolStripMenuItem tsViewMnuTextSizeSmallest;

        private FileSystemWatcher fswFavorites;

        private ToolStripPanel BottomToolStripPanel;

        private ToolStripPanel TopToolStripPanel;

        private ToolStripPanel RightToolStripPanel;

        private ToolStripPanel LeftToolStripPanel;

        private ToolStripContentPanel ContentPanel;

        private ToolStripContainer toolStripContainer1;

        private ToolStripButton tsBtnLogin;

        private Thread demoThread;

        private Thread descargaThread;

        private Thread iterarThread;
    }
}
