﻿namespace Jaeger.UI.LibAdminCfdi {
    internal enum PopupBlockerFilterLevel {
        None,
        Low,
        Medium,
        High
    }
}
