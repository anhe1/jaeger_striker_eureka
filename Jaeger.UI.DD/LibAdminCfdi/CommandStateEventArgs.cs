﻿using System;

namespace Jaeger.UI.LibAdminCfdi {
    public class CommandStateEventArgs : EventArgs {
        private BrowserCommands _commands;

        public BrowserCommands BrowserCommands {
            get {
                return this._commands;
            }
        }

        public CommandStateEventArgs(BrowserCommands commands) {
            this._commands = commands;
        }
    }
}
