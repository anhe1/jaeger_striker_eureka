﻿using Jaeger.UI.LibAdminCfdi.AdminCFDI;
using Jaeger.UI.LibUtilidades;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace Jaeger.UI.LibAdminCfdi {
    partial class MainForm : Form {
        public WindowManager _windowManager;

        private object cliente = null;

        
        public bool registra = true;

        public WindowManager WindowManager {
            get {
                return this._windowManager;
            }
        }

        public MainForm() {
            this.InitializeComponent();
            this._windowManager = new WindowManager(this.tabControl);
            
            this._windowManager.CommandStateChanged += new EventHandler<CommandStateEventArgs>(this._windowManager_CommandStateChanged);
            this._windowManager.StatusTextChanged += new EventHandler<TextChangedEventArgs>(this._windowManager_StatusTextChanged);
        }

        private void _windowManager_CommandStateChanged(object sender, CommandStateEventArgs e) {
            this.forwardToolStripButton.Enabled = (e.BrowserCommands & BrowserCommands.Forward) == BrowserCommands.Forward;
            this.backToolStripButton.Enabled = (e.BrowserCommands & BrowserCommands.Back) == BrowserCommands.Back;
            this.printPreviewToolStripButton.Enabled = (e.BrowserCommands & BrowserCommands.PrintPreview) == BrowserCommands.PrintPreview;
            this.printToolStripButton.Enabled = (e.BrowserCommands & BrowserCommands.Print) == BrowserCommands.Print;
            this.homeToolStripButton.Enabled = (e.BrowserCommands & BrowserCommands.Home) == BrowserCommands.Home;
            this.searchToolStripButton.Enabled = (e.BrowserCommands & BrowserCommands.Search) == BrowserCommands.Search;
            this.refreshToolStripButton.Enabled = (e.BrowserCommands & BrowserCommands.Reload) == BrowserCommands.Reload;
            this.stopToolStripButton.Enabled = (e.BrowserCommands & BrowserCommands.Stop) == BrowserCommands.Stop;
        }

        private void _windowManager_StatusTextChanged(object sender, TextChangedEventArgs e) {
            this.toolStripStatusLabel.Text = e.Text;
        }

        private void About() {
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e) {
            this.About();
        }

        private void backToolStripButton_Click(object sender, EventArgs e) {
            if ((this._windowManager.ActiveBrowser == null ? false : this._windowManager.ActiveBrowser.CanGoBack)) {
                this._windowManager.ActiveBrowser.GoBack();
            }
        }

        private void btnCerrarSession_Click(object sender, EventArgs e) {
            this.CerrarSession();
        }

        private void btnDescargar_Click(object sender, EventArgs e) {
            try {
                try {
                    if (this._windowManager.ActualBrowserControl.openSession) {
                        this._windowManager.ActualBrowserControl.inicializarBarra();
                        this._windowManager.ActualBrowserControl.ListaAll = new List<DatosCfdiSat>();
                        this._windowManager.ActualBrowserControl.countCancelados = 0;
                        this._windowManager.ActualBrowserControl.cfdiMes = "";
                        this._windowManager.ActualBrowserControl.respuesta = "";
                        Application.DoEvents();
                        if (!this._windowManager.ActualBrowserControl.Recibidas) {
                            this._windowManager.ActualBrowserControl.DescargarTodoToolStripButtonClickHandler(15);
                        }
                        else {
                            this._windowManager.ActualBrowserControl.DescargarTodoToolStripButtonClickHandler(15);
                        }
                        Application.DoEvents();
                        if (this._windowManager.ActualBrowserControl.Lista.Count > 0) {
                            if (this.cliente == null) {
                        //        this.cliente = new PrincipalAdminCom();
                            }
                        //    DataRespWsdlBuzon dataRespWsdlBuzon = this.cliente.sincronizaBuzonTributario(this._windowManager.ActualBrowserControl.datos, this._windowManager.ActualBrowserControl.Recibidas, this._windowManager.ActualBrowserControl.construyeIniRespuesta(), null, this._windowManager.ActualBrowserControl.idEmpresa, this._windowManager.ActualBrowserControl.rfcEmpresa, string.Concat(this.registra));
                        //    if (dataRespWsdlBuzon.Error.Length == 0) {
                        //        if (dataRespWsdlBuzon.Solicitud != null) {
                        //            string[] solicitud = new string[(int)dataRespWsdlBuzon.Solicitud.Length];
                        //            for (int i = 0; i < (int)dataRespWsdlBuzon.Solicitud.Length; i++) {
                        //                solicitud[i] = dataRespWsdlBuzon.Solicitud[i].referencia;
                        //            }
                        //            this._windowManager.ActualBrowserControl.soloUUIDs = solicitud;
                                    this._windowManager.ActualBrowserControl.comienzaDescargaMasiva();
                                    this._windowManager.ActualBrowserControl.tipoProgreso = "Enviando a Documentos Digitales...";
                                    this._windowManager.ActualBrowserControl.progess = 10;
                                    Application.DoEvents();
                                    string str = string.Concat(this._windowManager.ActualBrowserControl.pathBandeja, Path.DirectorySeparatorChar, (this._windowManager.ActualBrowserControl.Recibidas ? "RECIBIDOS" : "EMITIDOS"));
                                    DateTime now = DateTime.Now;
                                    string str1 = string.Concat(this._windowManager.ActualBrowserControl.pathBandeja, Path.DirectorySeparatorChar, "Bitacora de Importacion");
                                    ManagerBitacora.getSingleton().inicializa(str1, "BitImportacion");
                        //            RecursiveFileSearch recursiveFileSearch = new RecursiveFileSearch(this) {
                                        //Datos = this._windowManager.ActualBrowserControl.datos,
                        //                Registra = this.registra
                        //            };
                        //            object[] countEncontrado = new object[] { "registra->", this.registra, "<-pathDescarga->", str };
                        //            Console.WriteLine(string.Concat(countEncontrado));
                        //            recursiveFileSearch.iniciarEnPath(str, true, this._windowManager.ActualBrowserControl.rfcEmpresa);
                        //            while (!ManagerThreads.getSingleton().terminoTodos()) {
                        //                Thread.Sleep(100);
                        //                Application.DoEvents();
                        //            }
                        //            Thread.Sleep(1000);
                        //            recursiveFileSearch.revisarDeNuevo();
                        //            while (!ManagerThreads.getSingleton().terminoTodos()) {
                        //                Thread.Sleep(100);
                        //                Application.DoEvents();
                        //            }
                        //            if (ManagerControlImport.getSingleton().CountEncontrado > (long)50) {
                        //                Thread.Sleep(8000);
                        //            }
                        //            if (ManagerControlImport.getSingleton().CountEncontrado > (long)100) {
                        //                Thread.Sleep(10000);
                        //            }
                        //            if (ManagerControlImport.getSingleton().CountEncontrado > (long)200) {
                        //                Thread.Sleep(12000);
                        //            }
                        //            if (ManagerControlImport.getSingleton().CountEncontrado > (long)300) {
                        //                Thread.Sleep(14000);
                        //            }
                        //            if (ManagerControlImport.getSingleton().CountEncontrado > (long)400) {
                        //                Thread.Sleep(16000);
                        //            }
                        //            if (ManagerControlImport.getSingleton().CountEncontrado > (long)499) {
                        //                Thread.Sleep(18000);
                        //            }
                        //            recursiveFileSearch.revisarDeNuevo();
                        //            string str2 = ManagerBitacora.getSingleton().obtenerPathFileLog();
                        //            TimeSpan timeSpan = DateTime.Now - now;
                        //            ManagerBitacora singleton = ManagerBitacora.getSingleton();
                        //            countEncontrado = new object[] { "Finalizo la importacion, se encontraron ", ManagerControlImport.getSingleton().CountEncontrado, " XML´s de los cuales ", ManagerControlImport.getSingleton().CountImportadas, " fueron Importados Nuevos, ", ManagerControlImport.getSingleton().CountImportadasYaExistian, " ya existian en D.D. y ", this._windowManager.ActualBrowserControl.countCancelados, " estan Cancelados. Esto en un tiempo total de ", timeSpan.Hours, " hora(s) con ", timeSpan.Minutes, " minuto(s) con ", timeSpan.Seconds, " segundo(s). Si hubo archivos no procesados se enviaron a la carpeta: ", str1 };
                        //            singleton.escribeLog(string.Concat(countEncontrado), false);
                        //            countEncontrado = new object[] { "Se encontraron ", ManagerControlImport.getSingleton().CountEncontrado, " XML´s nuevos de los cuales ", ManagerControlImport.getSingleton().CountImportadas, " fueron Importados como Nuevos, ", ManagerControlImport.getSingleton().CountImportadasYaExistian, " ya existian en D.D. y ", this._windowManager.ActualBrowserControl.countCancelados, " estan Cancelados. Esto en un tiempo total de ", timeSpan.Hours, " hora(s) con ", timeSpan.Minutes, " minuto(s) con ", timeSpan.Seconds, " segundo(s). La bitacora de importacion (", str2, ") se abrira al Aceptar." };
                        //            MessageBox.Show(this, string.Concat(countEncontrado), "Finalizo:");
                        //            if (ManagerControlImport.getSingleton().CountEncontrado == (long)0) {
                        //                MessageBox.Show(this, "Si su búsqueda realmente obtuvo resultados por favor habilite la opción que se encuentra en utilerías forzar descarga con DLL.", "Alerta:");
                        //            }
                        //            ManagerBitacora.getSingleton().finaliza();
                        //            recursiveFileSearch.mueveArchivos(str, str1);
                        //            Process.Start(str2);
                        //        }
                        //        else {
                        //            this._windowManager.ActualBrowserControl.progess = 10;
                        //            Application.DoEvents();
                        //            Thread.Sleep(1000);
                        //            MessageBox.Show(this, string.Concat("Estos ", this._windowManager.ActualBrowserControl.Lista.Count, " CFDI´s ya se encontraban en Documentos Digitales, solo se actualizo el estatus de los comprobantes."), "Finalizo:");
                        //        }
                        //        if (this._windowManager.ActualBrowserControl.Lista.Count >= 499) {
                        //            MessageBox.Show(this, string.Concat("Su búsqueda obtuvo 500 XML´s por favor genere una nueva búsqueda a partir del ", this._windowManager.ActualBrowserControl.Lista[this._windowManager.ActualBrowserControl.Lista.Count - 1].Fecha_Emision), "ALERTA:");
                        //        }
                        //    }
                        //    else {
                        //        this._windowManager.ActualBrowserControl.progess = 10;
                        //        Application.DoEvents();
                        //        Thread.Sleep(1000);
                        //        Console.WriteLine("Detalle: ");
                        //        MessageBox.Show(this, string.Concat("Detalle: ", dataRespWsdlBuzon.Error), "Error enviado desde el Servidor de Documentos Digitales:");
                        //    }
                        }
                    }
                }
                catch (Exception exception1) {
                    Exception exception = exception1;
                    Console.WriteLine(string.Concat("MainForm.btnDescargar_Click() ->", exception.Message));
                    Console.WriteLine(string.Concat("MainForm.btnDescargar_Click() ->", exception.StackTrace));
                }
            }
            finally {
            }
        }

        private void btnDescargarNew_Click(object sender, EventArgs e) {
            try {
                if (this._windowManager.ActualBrowserControl.openSession) {
                    this._windowManager.ActualBrowserControl.redireccionDescarga();
                }
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                Console.WriteLine(string.Concat("MainForm.btnDescargarNew_Click() ->", exception.Message));
                Console.WriteLine(string.Concat("MainForm.btnDescargarNew_Click() ->", exception.StackTrace));
            }
        }

        private void btnSolicitarZip_Click(object sender, EventArgs e) {
            try {
                if (this._windowManager.ActualBrowserControl.openSession) {
                    this._windowManager.ActualBrowserControl.activaZip();
                }
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                Console.WriteLine(string.Concat("MainForm.btnSolicitarZip_Click() ->", exception.Message));
                Console.WriteLine(string.Concat("MainForm.btnSolicitarZip_Click() ->", exception.StackTrace));
            }
        }

        public void CerrarSession() {
            try {
                if (!this._windowManager.ActualBrowserControl.openSession) {
                    this._windowManager.ActualBrowserControl.trabajando = false;
                }
                else {
                    this._windowManager.ActiveBrowser.Navigate("https://cfdiau.sat.gob.mx/nidp/lofc.jsp");
                }
                (new Thread(() => {
                    Thread.CurrentThread.IsBackground = true;
                    Thread.Sleep(3000);
                    try {
                        this._windowManager.ActualBrowserControl.trabajando = false;
                    }
                    catch (Exception exception) {
                    }
                    try {
                        this._windowManager.ActualBrowserControl.openSession = false;
                    }
                    catch (Exception exception1) {
                    }
                })).Start();
            }
            catch (Exception exception2) {
            }
        }

        private void closeToolStripButton_Click(object sender, EventArgs e) {
            this._windowManager.Close();
        }

        private void closeWindowToolStripButton_Click(object sender, EventArgs e) {
            this._windowManager.New();
        }

        

        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            base.Close();
        }

        private void forwardToolStripButton_Click(object sender, EventArgs e) {
            if ((this._windowManager.ActiveBrowser == null ? false : this._windowManager.ActiveBrowser.CanGoForward)) {
                this._windowManager.ActiveBrowser.GoForward();
            }
        }

        private void homeToolStripButton_Click(object sender, EventArgs e) {
            HtmlDocument document = this._windowManager.ActiveBrowser.Document;
            IEnumerator enumerator = document.GetElementsByTagName("img").GetEnumerator();
            string outerHtml = "";
            string str = "";
            while (enumerator.MoveNext()) {
                outerHtml = ((HtmlElement)enumerator.Current).OuterHtml;
                if ((!outerHtml.Contains("BtnDescarga") ? false : outerHtml.Contains("World-download.png"))) {
                    try {
                        int num = outerHtml.IndexOf("RecuperaCfdi");
                        int num1 = outerHtml.IndexOf("','Recuperacion");
                        outerHtml = outerHtml.Substring(num, num1 - num);
                        str = string.Concat("https://portalcfdi.facturaelectronica.sat.gob.mx/", outerHtml);
                    }
                    catch (Exception exception) {
                    }
                    break;
                }
            }
            this._windowManager.ActiveBrowser.DownloadFile(str, "C:\\algo1.xml");
        }

        

        private void MainForm_Load(object sender, EventArgs e) {
            this._windowManager.New();
        }

        protected override void OnFormClosing(FormClosingEventArgs e) {
            base.OnFormClosing(e);
            if (e.CloseReason != CloseReason.WindowsShutDown) {
                if (MessageBox.Show(this, "Esta seguro que desea cerrar la ventana?", "Finalizar", MessageBoxButtons.YesNo) == DialogResult.No) {
                    e.Cancel = true;
                }
                else {
                    this._windowManager.ActualBrowserControl.trabajando = false;
                }
            }
        }

        
        private void Print() {
            ExtendedWebBrowser activeBrowser = this._windowManager.ActiveBrowser;
            if (activeBrowser != null) {
                activeBrowser.ShowPrintDialog();
            }
        }

        private void PrintPreview() {
            ExtendedWebBrowser activeBrowser = this._windowManager.ActiveBrowser;
            if (activeBrowser != null) {
                activeBrowser.ShowPrintPreviewDialog();
            }
        }

        private void printPreviewToolStripButton_Click(object sender, EventArgs e) {
            this.PrintPreview();
        }

        private void printToolStripButton_Click(object sender, EventArgs e) {
            this.Print();
        }

       
        private void refreshToolStripButton_Click(object sender, EventArgs e) {
            if (this._windowManager.ActiveBrowser != null) {
                this._windowManager.ActiveBrowser.Refresh(WebBrowserRefreshOption.Normal);
            }
        }

       
        private void searchToolStripButton_Click(object sender, EventArgs e) {
            if (this._windowManager.ActiveBrowser != null) {
                this._windowManager.ActiveBrowser.GoSearch();
            }
        }

        private void stopToolStripButton_Click(object sender, EventArgs e) {
            if (this._windowManager.ActiveBrowser != null) {
                this._windowManager.ActiveBrowser.Stop();
            }
            this.stopToolStripButton.Enabled = false;
        }

        private void tabControl_VisibleChanged(object sender, EventArgs e) {
            if (!this.tabControl.Visible) {
                this.panel1.BackColor = SystemColors.AppWorkspace;
            }
            else {
                this.panel1.BackColor = SystemColors.Control;
            }
        }
    }
}
