﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Jaeger.UI.LibAdminCfdi {
    public class frmProgress : Form {
        private IContainer components = null;

        public Label labelMessage;

        public ProgressBar progressBar;

        public Button buttonCancel;

        public frmProgress() {
            this.InitializeComponent();
        }

        private void buttonCancel_Click(object sender, EventArgs e) {
            EventHandler<EventArgs> eventHandler = this.Canceled;
            if (eventHandler != null) {
                eventHandler(this, e);
            }
        }

        protected override void Dispose(bool disposing) {
            if ((!disposing ? false : this.components != null)) {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent() {
            this.labelMessage = new Label();
            this.progressBar = new ProgressBar();
            this.buttonCancel = new Button();
            base.SuspendLayout();
            this.labelMessage.AutoSize = true;
            this.labelMessage.Font = new Font("Microsoft Sans Serif", 9.5f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.labelMessage.Location = new Point(16, 18);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new Size(212, 16);
            this.labelMessage.TabIndex = 0;
            this.labelMessage.Text = "Conectando: DIGITE EL CAPTCHA y OPRIMA ENVIAR ";
            this.progressBar.Location = new Point(17, 52);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new Size(359, 27);
            this.progressBar.TabIndex = 1;
            this.buttonCancel.Font = new Font("Microsoft Sans Serif", 10f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.buttonCancel.Location = new Point(273, 98);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new Size(103, 37);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Cancelar";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new EventHandler(this.buttonCancel_Click);
            base.AutoScaleDimensions = new SizeF(7f, 15f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(388, 147);
            base.ControlBox = false;
            base.Controls.Add(this.buttonCancel);
            base.Controls.Add(this.progressBar);
            base.Controls.Add(this.labelMessage);
            this.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
            base.Name = "frmProgress";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Logueo con el SAT";
            base.TopMost = true;
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public event EventHandler<EventArgs> Canceled;
    }
}
