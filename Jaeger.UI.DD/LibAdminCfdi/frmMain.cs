﻿using IfacesEnumsStructsClasses;
using System;
using System.Collections.Generic;
using System.Threading;
using csExWB;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Web;
using System.Windows.Forms;
using Jaeger.UI.LibAdminCfdi.AdminCFDI;

namespace Jaeger.UI.LibAdminCfdi {
        
    partial class frmMain : Form {
        private const string m_AboutBlank = "about:blank";

        private const string m_Blank = "Blank";

        private const int m_MaxTextLen = 15;

        

        public bool porHora = false;

        public bool showWindow = true;

        public bool openSession = false;

        public bool inicializando = true;

        public bool trabajando = true;

        public string respuesta = "";

        public string usuario = "";

        public string pass = "";

        public bool Recibidas = true;

        public string estadoComprobante = "";

        public string diaConsulta = "";

        public bool siDescarga = false;

        public string[] soloUUIDs = null;

        public int mesConInicial = 8;

        public int mesConFinal = 12;

        public int diaConInicial = 1;

        public int diaConFinal = 28;

        public int lastDayMonthInicial = 0;

        public int anno = 2015;

        public int numBusquedas = 0;

        public int auxNoHoraDia = 0;

        private string fileNameCurrent = "";

        public string fechaInicial = "01/10/2014";

        public string fechaFinal = "31/10/2014";

        private List<DatosCfdiSat> lista = null;

        public cEXWB cEXWB1;

        public int contadorDescargas = 0;

        public BackgroundWorker fondoTrabajador1;

        public int progess = 0;

        public string tipoProgreso = "Conectando con Repositorio Fiscal:";

        public int versionProgreso = 0;

        public cEXWB m_CurWB = null;

        private int m_iCurMenu = 0;

        private int m_iCountWB = 1;

        private ToolStripButton m_tsBtnFirstTab = null;

        private ToolStripButton m_tsBtnctxMnu = null;

        private object m_oHTMLCtxMenu = null;

        private Image m_imgLock = null;

        private Image m_imgUnLock = null;

        private Image m_BlankImage = null;

        private frmFileDownload m_frmFileDownload = new frmFileDownload();

        public frmProgress alert;

        private int contadorBusquedas = 0;

        private int contadorHoras2 = 0;

        private bool muestraDescarga = false;

        private bool primeraVez = true;

        private string cfdiMes = "";

        private int noColumn = 0;

        private bool m_FavNeedReload = false;

        private IHTMLElement comboBoxMes = null;

        private IHTMLElement optionMes = null;

        private IHTMLElement comboBoxAnno = null;

        private IHTMLElement optionAnno = null;

        private IHTMLElement comboBoxDia = null;

        private IHTMLElement optionDia = null;

        private IHTMLElement comboBoxHoraIni = null;

        private IHTMLElement optionHoraIni = null;

        private IHTMLElement comboBoxHoraFin = null;

        private IHTMLElement optionHoraFin = null;

        private IHTMLElement comboBoxMinuFin = null;

        private IHTMLElement optionMinuFin = null;

        private IHTMLElement comboBoxSeguFin = null;

        private IHTMLElement optionSeguFin = null;

        private bool diaConsultaDifVacio = false;

        private int m_mposX = 0;

        private int m_mposY = 0;

        private int m_lFileSize = 0;

        public cEXWB CurrentBrowserControl {
            get {
                return this.m_CurWB;
            }
        }

        public frmMain() {
            this.InitializeComponent();
        }

        private bool AddNewBrowser(string TabText, string TabTooltip, string Url, bool BringToFront) {
            bool flag;
            int wBDOCHOSTUIFLAG = 262276;
            int wBDOCDOWNLOADCTLFLAG = 112;
            if (this.m_CurWB != null) {
                wBDOCHOSTUIFLAG = this.m_CurWB.WBDOCHOSTUIFLAG;
                wBDOCDOWNLOADCTLFLAG = this.m_CurWB.WBDOCDOWNLOADCTLFLAG;
            }
            cEXWB _cEXWB = null;
            int mICountWB = this.m_iCountWB + 1;
            string str = string.Concat("cEXWB", mICountWB.ToString());
            try {
                _cEXWB = new cEXWB() {
                    Anchor = this.cEXWB1.Anchor,
                    Name = str,
                    Location = this.cEXWB1.Location,
                    Size = this.cEXWB1.Size,
                    RegisterAsBrowser = true,
                    WBDOCDOWNLOADCTLFLAG = wBDOCDOWNLOADCTLFLAG,
                    WBDOCHOSTUIFLAG = wBDOCHOSTUIFLAG,
                    FileDownloadDirectory = this.cEXWB1.FileDownloadDirectory
                };
                _cEXWB.TitleChange += new TitleChangeEventHandler(this.cEXWB1_TitleChange);
                _cEXWB.StatusTextChange += new StatusTextChangeEventHandler(this.cEXWB1_StatusTextChange);
                _cEXWB.CommandStateChange += new CommandStateChangeEventHandler(this.cEXWB1_CommandStateChange);
                _cEXWB.WBKeyDown += new WBKeyDownEventHandler(this.cEXWB1_WBKeyDown);
                _cEXWB.WBEvaluteNewWindow += new EvaluateNewWindowEventHandler(this.cEXWB1_WBEvaluteNewWindow);
                _cEXWB.BeforeNavigate2 += new BeforeNavigate2EventHandler(this.cEXWB1_BeforeNavigate2);
                _cEXWB.ProgressChange += new ProgressChangeEventHandler(this.cEXWB1_ProgressChange);
                _cEXWB.NavigateComplete2 += new NavigateComplete2EventHandler(this.cEXWB1_NavigateComplete2);
                _cEXWB.DownloadBegin += new DownloadBeginEventHandler(this.cEXWB1_DownloadBegin);
                _cEXWB.ScriptError += new ScriptErrorEventHandler(this.cEXWB1_ScriptError);
                _cEXWB.DownloadComplete += new DownloadCompleteEventHandler(this.cEXWB1_DownloadComplete);
                _cEXWB.StatusTextChange += new StatusTextChangeEventHandler(this.cEXWB1_StatusTextChange);
                _cEXWB.DocumentCompleteEX += new DocumentCompleteExEventHandler(this.cEXWB1_DocumentCompleteEX);
                _cEXWB.WBDragDrop += new WBDropEventHandler(this.cEXWB1_WBDragDrop);
                _cEXWB.SetSecureLockIcon += new SetSecureLockIconEventHandler(this.cEXWB1_SetSecureLockIcon);
                _cEXWB.NavigateError += new NavigateErrorEventHandler(this.cEXWB1_NavigateError);
                _cEXWB.WBSecurityProblem += new SecurityProblemEventHandler(this.cEXWB1_WBSecurityProblem);
                _cEXWB.DocumentComplete += new DocumentCompleteEventHandler(this.cEXWB1_DocumentComplete);
                _cEXWB.WBKeyUp += new WBKeyUpEventHandler(this.cEXWB1_WBKeyUp);
                _cEXWB.WindowClosing += new WindowClosingEventHandler(this.cEXWB1_WindowClosing);
                _cEXWB.WBContextMenu += new ContextMenuEventHandler(this.cEXWB1_WBContextMenu);
                _cEXWB.WBDocHostShowUIShowMessage += new DocHostShowUIShowMessageEventHandler(this.cEXWB1_WBDocHostShowUIShowMessage);
                _cEXWB.FileDownload += new FileDownloadEventHandler(this.cEXWB1_FileDownload);
                _cEXWB.WBStop += new StopEventHandler(this.cEXWB1_WBStop);
                _cEXWB.FileDownloadExStart += new FileDownloadExEventHandler(this.cEXWB1_FileDownloadExStart);
                _cEXWB.FileDownloadExEnd += new FileDownloadExEndEventHandler(this.cEXWB1_FileDownloadExEnd);
                _cEXWB.FileDownloadExProgress += new FileDownloadExProgressEventHandler(this.cEXWB1_FileDownloadExProgress);
                _cEXWB.FileDownloadExError += new FileDownloadExErrorEventHandler(this.cEXWB1_FileDownloadExError);
                _cEXWB.FileDownloadExAuthenticate += new FileDownloadExAuthenticateEventHandler(this.cEXWB1_FileDownloadExAuthenticate);
                _cEXWB.FileDownloadExDownloadFileFullyWritten += new FileDownloadExDownloadFileFullyWrittenEventHandler(this.cEXWB1_FileDownloadExDownloadFileFullyWritten);
                _cEXWB.WBStop += new StopEventHandler(this.cEXWB1_WBStop);
                _cEXWB.WBLButtonDown += new HTMLMouseEventHandler(this.cEXWB1_WBLButtonDown);
                _cEXWB.WBLButtonUp += new HTMLMouseEventHandler(this.cEXWB1_WBLButtonUp);
                _cEXWB.RegisterAsBrowser = true;
                this.toolStripContainer1.ContentPanel.Controls.Add(_cEXWB);
                if (BringToFront) {
                    ((ToolStripMenuItem)this.ctxMnuOpenWBs.Items[this.m_iCurMenu]).Checked = false;
                    this.m_iCurMenu = this.ctxMnuOpenWBs.Items.Count - 1;
                    this.m_CurWB = _cEXWB;
                    this.tsProgress.Value = 0;
                    this.tsProgress.Maximum = 0;
                    this.tsProgress.Visible = false;
                    _cEXWB.BringToFront();
                }
                this.m_iCountWB++;
                if (Url.Length > 0) {
                    _cEXWB.Navigate(Url);
                }
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                AllForms.m_frmLog.AppendToLog(string.Concat("AddNewBrowser\r\n", exception.ToString()));
                flag = false;
                return flag;
            }
            flag = true;
            return flag;
        }

        private void AssignPopup(ref object obj) {
            if (this.m_CurWB != null) {
                if (!this.m_CurWB.RegisterAsBrowser) {
                    this.m_CurWB.RegisterAsBrowser = true;
                }
                obj = this.m_CurWB.WebbrowserObject;
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e) {
            BackgroundWorker backgroundWorker = sender as BackgroundWorker;
            int num = this.progess;
            backgroundWorker.ReportProgress(num * 10);
            Thread.Sleep(50);
            while (num != 10) {
                if (backgroundWorker.CancellationPending) {
                    e.Cancel = true;
                    break;
                }
                else if (num != this.progess) {
                    num = this.progess;
                    backgroundWorker.ReportProgress(num * 10);
                    Thread.Sleep(50);
                }
                else {
                    Thread.Sleep(50);
                }
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            Label label = this.alert.labelMessage;
            string str = this.tipoProgreso;
            int progressPercentage = e.ProgressPercentage;
            label.Text = string.Concat(str, progressPercentage.ToString(), "%");
            this.alert.progressBar.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            object[] objArray;
            if (e.Cancelled) {
                this.respuesta = "El usuario ha cancelado la Consulta/Descarga del portal del SAT";
                this.trabajando = false;
            }
            else if (e.Error != null) {
                this.respuesta = "El usuario ha cancelado la Consulta/Descarga del portal del SAT";
                this.trabajando = false;
            }
            this.alert.Close();
            if (this.trabajando) {
                this.versionProgreso++;
                if (this.versionProgreso == 2) {
                    if (!this.muestraDescarga) {
                        this.versionProgreso = 3;
                    }
                    else {
                        this.tipoProgreso = "Descargando CFDI´s encontrados ";
                        this.progess = 0;
                        this.alert = new frmProgress();
                        this.alert.labelMessage.Text = string.Concat(this.tipoProgreso, "0%");
                        this.alert.Text = "Descargando desde el portal SAT";
                        this.alert.Canceled += new EventHandler<EventArgs>(this.buttonCancel_Click);
                        this.alert.Show();
                        this.fondoTrabajador1.RunWorkerAsync();
                        this.muestraDescarga = false;
                    }
                }
                if (this.versionProgreso == 3) {
                    if ((this.Recibidas ? true : this.porHora)) {
                        if (this.diaConsulta.Equals("")) {
                            if (this.mesConInicial < this.mesConFinal) {
                                this.versionProgreso = 1;
                            }
                        }
                        else if (this.lastDayMonthInicial != 0) {
                            this.versionProgreso = 1;
                        }
                        else if (this.diaConInicial < this.diaConFinal) {
                            this.versionProgreso = 1;
                        }
                        else if (this.diaConInicial == this.diaConFinal) {
                            if (this.porHora) {
                                if (this.auxNoHoraDia < 23) {
                                    this.versionProgreso = 1;
                                }
                            }
                        }
                    }
                }
                if (this.versionProgreso == 1) {
                    if (this.numBusquedas == 0) {
                        if (!this.Recibidas) {
                            string[] strArrays = new string[] { "Buscando CFDI´s Emitidos del ", this.fechaInicial, " al ", this.fechaFinal, " " };
                            this.tipoProgreso = string.Concat(strArrays);
                        }
                        else {
                            if (this.contadorBusquedas == 0) {
                                this.contadorBusquedas = this.mesConInicial;
                            }
                            this.tipoProgreso = string.Concat("Buscando CFDI´s Recibidos de ", this.dameElMes(this.contadorBusquedas), " ");
                            this.contadorBusquedas++;
                        }
                    }
                    else if (!this.porHora) {
                        this.contadorBusquedas++;
                        objArray = new object[] { "Buscando CFDI´s del día ", this.diaConInicial, "/", this.mesConInicial, "/", this.anno, " [", this.contadorBusquedas, "/", this.numBusquedas, "] " };
                        this.tipoProgreso = string.Concat(objArray);
                    }
                    else {
                        if (this.auxNoHoraDia == 23) {
                            this.contadorHoras2 = 0;
                        }
                        if (this.contadorHoras2 != 0) {
                            objArray = new object[] { "Buscando CFDI´s del día ", this.diaConInicial, "/", this.mesConInicial, "/", this.anno, " con ", this.contadorHoras2, " hrs [", this.contadorBusquedas, "/", this.numBusquedas, "] " };
                            this.tipoProgreso = string.Concat(objArray);
                        }
                        else {
                            this.contadorBusquedas++;
                            if (!this.primeraVez) {
                                objArray = new object[] { "Buscando CFDI´s del día siguiente con ", this.contadorHoras2, " hrs [", this.contadorBusquedas, "/", this.numBusquedas, "] " };
                                this.tipoProgreso = string.Concat(objArray);
                            }
                            else {
                                this.primeraVez = false;
                                objArray = new object[] { "Buscando CFDI´s del día ", this.diaConInicial, "/", this.mesConInicial, "/", this.anno, " con ", this.contadorHoras2, " hrs [", this.contadorBusquedas, "/", this.numBusquedas, "] " };
                                this.tipoProgreso = string.Concat(objArray);
                            }
                        }
                        this.contadorHoras2++;
                    }
                    this.progess = 0;
                    this.alert = new frmProgress();
                    this.alert.labelMessage.Text = string.Concat(this.tipoProgreso, "0%");
                    this.alert.Text = "Buscando en la Base de Datos del SAT";
                    this.alert.Canceled += new EventHandler<EventArgs>(this.buttonCancel_Click);
                    this.alert.Show();
                    this.fondoTrabajador1.RunWorkerAsync();
                }
            }
        }

        private void BrowserCtxMenuClickHandler(object sender, EventArgs e) {
            IHTMLAnchorElement mOHTMLCtxMenu;
            IHTMLImgElement hTMLImgElement;
            if (this.m_oHTMLCtxMenu != null) {
                try {
                    if (sender == this.ctxMnuACopyUrl) {
                        mOHTMLCtxMenu = (IHTMLAnchorElement)this.m_oHTMLCtxMenu;
                        Clipboard.Clear();
                        Clipboard.SetText(HttpUtility.UrlDecode(mOHTMLCtxMenu.href));
                    }
                    else if (sender == this.ctxMnuACopyUrlText) {
                        IHTMLElement hTMLElement = (IHTMLElement)this.m_oHTMLCtxMenu;
                        Clipboard.Clear();
                        Clipboard.SetText(hTMLElement.outerText);
                    }
                    else if (sender == this.ctxMnuAOpenInBack) {
                        mOHTMLCtxMenu = (IHTMLAnchorElement)this.m_oHTMLCtxMenu;
                        this.AddNewBrowser("Blank", "", mOHTMLCtxMenu.href, false);
                    }
                    else if (sender == this.ctxMnuAOpenInFront) {
                        mOHTMLCtxMenu = (IHTMLAnchorElement)this.m_oHTMLCtxMenu;
                        this.AddNewBrowser("Blank", "", mOHTMLCtxMenu.href, true);
                    }
                    else if (sender == this.ctxMnuImgCopyImageSource) {
                        hTMLImgElement = (IHTMLImgElement)this.m_oHTMLCtxMenu;
                        Clipboard.Clear();
                        Clipboard.SetText(hTMLImgElement.src);
                    }
                    else if (sender == this.ctxMnuImgCopyImageAlt) {
                        hTMLImgElement = (IHTMLImgElement)this.m_oHTMLCtxMenu;
                        Clipboard.Clear();
                        Clipboard.SetText(hTMLImgElement.alt);
                    }
                    else if (sender == this.ctxMnuImgCopyUrlText) {
                        mOHTMLCtxMenu = (IHTMLAnchorElement)((IHTMLElement)this.m_oHTMLCtxMenu).parentElement;
                        Clipboard.Clear();
                        Clipboard.SetText(HttpUtility.UrlDecode(mOHTMLCtxMenu.href));
                    }
                    else if (sender == this.ctxMnuImgOpenInBack) {
                        mOHTMLCtxMenu = (IHTMLAnchorElement)((IHTMLElement)this.m_oHTMLCtxMenu).parentElement;
                        this.AddNewBrowser("Blank", "", mOHTMLCtxMenu.href, false);
                    }
                    else if (sender == this.ctxMnuImgOpenInFront) {
                        mOHTMLCtxMenu = (IHTMLAnchorElement)((IHTMLElement)this.m_oHTMLCtxMenu).parentElement;
                        this.AddNewBrowser("Blank", "", mOHTMLCtxMenu.href, true);
                    }
                }
                catch (Exception exception1) {
                    Exception exception = exception1;
                    AllForms.m_frmLog.AppendToLog(string.Concat("BrowserCtxMenuClickHandler\r\n", exception.ToString()));
                }
                this.m_oHTMLCtxMenu = null;
            }
        }

        private void buscarMes() {
            try {
                if (!this.Recibidas) {
                    this.demoThread = new Thread(new ThreadStart(this.ConsultaEmitidas));
                }
                else {
                    this.demoThread = new Thread(new ThreadStart(this.ConsultaRecibidas));
                }
                this.demoThread.Start();
            }
            catch (Exception exception) {
                this.respuesta = string.Concat("frmMain.buscarMes()->", exception.ToString());
                this.trabajando = false;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e) {
            if (this.fondoTrabajador1.WorkerSupportsCancellation) {
                this.fondoTrabajador1.CancelAsync();
                this.alert.Close();
            }
        }

        public void CerrarSession() {
            try {
                if (this.openSession) {
                    this.m_CurWB.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/logout.aspx?salir=y");
                }
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                this.respuesta = string.Concat("frmMain.CerrarSession()->", exception.ToString());
                this.openSession = false;
                AllForms.m_frmLog.AppendToLog(string.Concat("CerrarSession->", exception.ToString()));
            }
        }

        private void cEXWB1_BeforeNavigate2(object sender, BeforeNavigate2EventArgs e) {
        }

        private void cEXWB1_CommandStateChange(object sender, CommandStateChangeEventArgs e) {
            if (sender != this.m_CurWB) {
            }
        }

        private void cEXWB1_DocumentComplete(object sender, DocumentCompleteEventArgs e) {
            try {
                string str = e.url;
                this.m_CurWB.SetOpticalZoomValue(50);
            }
            catch (Exception exception) {
                this.respuesta = string.Concat("frmMain.cEXWB1_DocumentComplete(sender,DocumentCompleteEventArgs)->", exception.ToString());
                this.trabajando = false;
            }
        }

        private void cEXWB1_DocumentCompleteEX(object sender, DocumentCompleteExEventArgs e) {
        }

        private void cEXWB1_DownloadBegin(object sender) {
            if (sender == this.m_CurWB) {
                this.tsProgress.Visible = true;
            }
        }

        private void cEXWB1_DownloadComplete(object sender) {
            if (sender == this.m_CurWB) {
                this.tsProgress.Value = 0;
                this.tsProgress.Maximum = 0;
                this.tsProgress.Visible = false;
            }
        }

        private void cEXWB1_FileDownload(object sender, FileDownloadEventArgs e) {
        }

        private void cEXWB1_FileDownloadExAuthenticate(object sender, FileDownloadExAuthenticateEventArgs e) {
            cEXWB _cEXWB = (cEXWB)sender;
        }

        private void cEXWB1_FileDownloadExDownloadFileFullyWritten(object sender, FileDownloadExDownloadFileFullyWrittenEventArgs e) {
            AllForms.m_frmLog.AppendToLog("cEXWB1_FileDownloadExDownloadFileFullyWritten");
        }

        private void cEXWB1_FileDownloadExEnd(object sender, FileDownloadExEndEventArgs e) {
            cEXWB _cEXWB = (cEXWB)sender;
            try {
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                AllForms.m_frmLog.AppendToLog(string.Concat(_cEXWB.Name, "_FileDownloadExEnd", exception.ToString()));
            }
        }

        private void cEXWB1_FileDownloadExError(object sender, FileDownloadExErrorEventArgs e) {
            cEXWB _cEXWB = (cEXWB)sender;
            try {
                AllForms.m_frmLog.AppendToLog(string.Concat("An error occured during downloading of ", e.m_URL, "\r\n", e.m_ErrorMsg));
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                AllForms.m_frmLog.AppendToLog(string.Concat(_cEXWB.Name, "_FileDownloadExError", exception.ToString()));
            }
        }

        private void cEXWB1_FileDownloadExProgress(object sender, FileDownloadExProgressEventArgs e) {
            cEXWB _cEXWB = (cEXWB)sender;
            try {
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                AllForms.m_frmLog.AppendToLog(string.Concat(_cEXWB.Name, "_FileDownloadExProgress", exception.ToString()));
            }
        }

        private void cEXWB1_FileDownloadExStart(object sender, FileDownloadExEventArgs e) {
            cEXWB _cEXWB = (cEXWB)sender;
            try {
                if (string.IsNullOrEmpty(e.m_FileSize)) {
                    this.m_lFileSize = 0;
                }
                else {
                    this.m_lFileSize = int.Parse(e.m_FileSize);
                }
                e.m_SendProgressEvents = true;
                e.m_PathToSave = this.fileNameCurrent;
                if (this.openSession) {
                    this.m_frmFileDownload.AddDownloadItem(_cEXWB.Name, e.m_Filename, e.m_URL, e.m_dlUID, e.m_URL, e.m_PathToSave, this.m_lFileSize);
                }
            }
            catch (Exception exception) {
                this.respuesta = string.Concat("frmMain.cEXWB1_FileDownloadExStart(sender, FileDownloadExEventArgs)", exception.ToString());
                this.trabajando = false;
            }
        }

        private void cEXWB1_NavigateComplete2(object sender, NavigateComplete2EventArgs e) {
        }

        private void cEXWB1_NavigateError(object sender, NavigateErrorEventArgs e) {
            if ((e.statuscode == WinInetErrors.HTTP_STATUS_OK || e.statuscode == WinInetErrors.HTTP_STATUS_CONTINUE || e.statuscode == WinInetErrors.HTTP_STATUS_REDIRECT ? false : e.statuscode != WinInetErrors.HTTP_STATUS_ACCEPTED)) {
                cEXWB _cEXWB = (cEXWB)sender;
                try {
                    object obj = null;
                    string[] strArrays = new string[] { "about:<HEAD><title>Page Not Found</title></Head><Body><H3>Unable to load</H3><p>", e.url, "<br>Reason:<br>", e.statuscode.ToString(), "</p></Body>" };
                    string str = string.Concat(strArrays);
                    this.respuesta = string.Concat("Se ha perdido la conexion de la pagina del SAT:", e.url, " Codigo de Error:", e.statuscode.ToString());
                    this.trabajando = false;
                    this.openSession = false;
                    if (_cEXWB.FramesCount() != 0) {
                        e.Cancel = true;
                        ((IWebBrowser2)e.browser).Navigate(str, ref obj, ref obj, ref obj, ref obj);
                    }
                    else {
                        _cEXWB.Stop();
                        _cEXWB.Navigate(str);
                        return;
                    }
                }
                catch (Exception exception1) {
                    Exception exception = exception1;
                    AllForms.m_frmLog.AppendToLog(string.Concat("frmMain.cEXWB1_NavigateError(sender, NavigateErrorEventArgs)->", exception.ToString()));
                }
            }
        }

        private void cEXWB1_ProgressChange(object sender, ProgressChangeEventArgs e) {
            if (sender == this.m_CurWB) {
                try {
                    if (!(e.progress == -1 ? false : e.progressmax != e.progress)) {
                        this.tsProgress.Value = 0;
                        this.tsProgress.Maximum = 0;
                        return;
                    }
                    else if ((e.progressmax <= 0 || e.progress <= 0 ? false : e.progress < e.progressmax)) {
                        this.tsProgress.Maximum = e.progressmax;
                        this.tsProgress.Value = e.progress;
                    }
                }
                catch (Exception exception) {
                }
            }
        }

        private void cEXWB1_RefreshBegin(object sender) {
        }

        private void cEXWB1_RefreshEnd(object sender) {
        }

        private void cEXWB1_ScriptError(object sender, ScriptErrorEventArgs e) {
            string str = (this.m_CurWB != null ? this.m_CurWB.Name : "cEXWBxx");
            e.continueScripts = true;
            AllForms.m_frmLog.AppendToLog(string.Concat(str, "_ScriptError - Continuing to run scripts"));
            AllForms.m_frmLog.AppendToLog(string.Concat("Error Message", e.errorMessage, "\r\nLine Number", e.lineNumber.ToString()));
        }

        private void cEXWB1_SetSecureLockIcon(object sender, SetSecureLockIconEventArgs e) {
            if (sender == this.m_CurWB) {
                try {
                    this.UpdateSecureLockIcon(e.securelockicon);
                }
                catch (Exception exception1) {
                    Exception exception = exception1;
                    AllForms.m_frmLog.AppendToLog(string.Concat(this.m_CurWB.Name, "_SetSecureLockIcon", exception.ToString()));
                }
            }
        }

        private void cEXWB1_StatusTextChange(object sender, StatusTextChangeEventArgs e) {
            if (sender == this.m_CurWB) {
                this.tsStatus.Text = e.text;
            }
        }

        private void cEXWB1_TitleChange(object sender, TitleChangeEventArgs e) {
            if (sender == this.m_CurWB) {
                this.Text = e.title;
            }
        }

        private void cEXWB1_WBContextMenu(object sender, ContextMenuEventArgs e) {
            try {
                if (e.contextmenutype == WB_CONTEXTMENU_TYPES.CONTEXT_MENU_ANCHOR) {
                    e.displaydefault = false;
                    this.m_oHTMLCtxMenu = e.dispctxmenuobj;
                    this.ctxMnuWB_A.Show(e.pt);
                }
                else if (e.contextmenutype == WB_CONTEXTMENU_TYPES.CONTEXT_MENU_IMAGE) {
                    e.displaydefault = false;
                    this.m_oHTMLCtxMenu = e.dispctxmenuobj;
                    this.ctxMnuImgCopyUrlText.Enabled = false;
                    IHTMLElement mOHTMLCtxMenu = (IHTMLElement)this.m_oHTMLCtxMenu;
                    if (mOHTMLCtxMenu != null) {
                        IHTMLElement hTMLElement = mOHTMLCtxMenu.parentElement;
                        this.ctxMnuImgCopyUrlText.Enabled = (hTMLElement.tagName.ToUpper() == "A" ? true : false);
                    }
                    this.ctxMnuImgOpenInBack.Enabled = this.ctxMnuImgCopyUrlText.Enabled;
                    this.ctxMnuImgOpenInFront.Enabled = this.ctxMnuImgCopyUrlText.Enabled;
                    this.ctxMnuWB_Img.Show(e.pt);
                }
                else if (e.contextmenutype == WB_CONTEXTMENU_TYPES.CONTEXT_MENU_CONTROL) {
                    AllForms.m_frmLog.AppendToLog("CONTEXT_MENU_CONTROL");
                }
                else if (e.contextmenutype == WB_CONTEXTMENU_TYPES.CONTEXT_MENU_TEXTSELECT) {
                    AllForms.m_frmLog.AppendToLog("CONTEXT_MENU_TEXTSELECT");
                }
                else if (e.contextmenutype == WB_CONTEXTMENU_TYPES.CONTEXT_MENU_TABLE) {
                    AllForms.m_frmLog.AppendToLog("CONTEXT_MENU_TABLE");
                }
                AllForms.m_frmLog.AppendToLog(string.Concat("CONTEXT_MENU_TAGNAME = ", e.ctxmenuelem.tagName));
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                AllForms.m_frmLog.AppendToLog(string.Concat("cEXWB1_WBContextMenu\r\n", exception.ToString()));
            }
        }

        private void cEXWB1_WBDocHostShowUIShowMessage(object sender, DocHostShowUIShowMessageEventArgs e) {
            string str = (this.m_CurWB != null ? this.m_CurWB.Name : "cEXWBxx");
            AllForms.m_frmLog.AppendToLog(string.Concat(str, "_WBDocHostShowUIShowMessage - handled - Text\r\n", e.text));
            e.handled = true;
            e.result = 2;
            if (e.type == 48) {
                e.result = (int)MessageBox.Show(e.text, "DemoApp");
            }
            else if (e.type == 33) {
                e.result = (int)MessageBox.Show(e.text, "DemoApp", MessageBoxButtons.OKCancel);
            }
        }

        private void cEXWB1_WBDragDrop(object sender, WBDropEventArgs e) {
            if (e.dataobject != null) {
                if (e.dataobject.ContainsText()) {
                    AllForms.m_frmLog.AppendToLog(string.Concat("cEXWB1_WBDragDrop\r\n", e.dataobject.GetText()));
                }
                else if (!e.dataobject.ContainsFileDropList()) {
                    object data = e.dataobject.GetData("WindowsForms10PersistentObject");
                    if (data != null) {
                        if (data is ListViewItem) {
                            ListViewItem listViewItem = (ListViewItem)data;
                            AllForms.m_frmLog.AppendToLog(string.Concat("cEXWB1_WBDragDrop\r\n", listViewItem.Text));
                        }
                    }
                }
                else {
                    StringCollection fileDropList = e.dataobject.GetFileDropList();
                    if (fileDropList.Count > 1) {
                        MessageBox.Show("Can not do multi file drop!");
                    }
                    else if (this.m_CurWB != null) {
                        this.m_CurWB.Navigate(fileDropList[0]);
                    }
                }
            }
        }

        private void cEXWB1_WBEvaluteNewWindow(object sender, EvaluateNewWindowEventArgs e) {
            try {
                string empty = string.Empty;
                if (e.flags != NWMF.HTMLDIALOG) {
                    empty = (e.flags != NWMF.SHOWHELP ? e.flags.ToString() : "Show Help");
                }
                else {
                    empty = "HTML Dialog";
                }
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                AllForms.m_frmLog.AppendToLog(string.Concat(this.m_CurWB.Name, "_WBEvaluteNewWindow\r\n", exception.ToString()));
            }
        }

        private void cEXWB1_WBKeyDown(object sender, WBKeyDownEventArgs e) {
            try {
                if (e.virtualkey == Keys.ControlKey) {
                    switch (e.keycode) {
                        case Keys.N: {
                                this.AddNewBrowser("Blank", "about:blank", "about:blank", true);
                                e.handled = true;
                                break;
                            }
                        case Keys.O: {
                                this.AddNewBrowser("Blank", "about:blank", "about:blank", true);
                                e.handled = true;
                                break;
                            }
                    }
                }
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                if (this.m_CurWB == null) {
                    AllForms.m_frmLog.AppendToLog(string.Concat("cEXWBxx_WBKeyDown\r\n", exception.ToString()));
                }
                else {
                    AllForms.m_frmLog.AppendToLog(string.Concat(this.m_CurWB.Name, "_WBKeyDown\r\n", exception.ToString()));
                }
            }
        }

        private void cEXWB1_WBKeyUp(object sender, WBKeyUpEventArgs e) {
        }

        private void cEXWB1_WBLButtonDown(object sender, HTMLMouseEventArgs e) {
            this.m_mposX = e.ClientX;
            this.m_mposY = e.ClientY;
            if (e.SrcElement == null) {
                AllForms.m_frmLog.AppendToLog("cEXWB1_WBLButtonDown");
            }
            else {
                AllForms.m_frmLog.AppendToLog(string.Concat("cEXWB1_WBLButtonDown==>", e.SrcElement.tagName));
            }
        }

        private void cEXWB1_WBLButtonUp(object sender, HTMLMouseEventArgs e) {
            if (e.SrcElement == null) {
                AllForms.m_frmLog.AppendToLog("cEXWB1_WBLButtonUp");
            }
            else {
                AllForms.m_frmLog.AppendToLog(string.Concat("cEXWB1_WBLButtonUp==>", e.SrcElement.tagName));
            }
            Rectangle rectangle = new Rectangle(this.m_mposX - 1, this.m_mposY - 1, 2, 2);
            if (rectangle.Contains(e.ClientX, e.ClientY)) {
                AllForms.m_frmLog.AppendToLog("MOUSE CLICKED");
            }
        }

        private void cEXWB1_WBSecurityProblem(object sender, SecurityProblemEventArgs e) {
            if ((e.problem == WinInetErrors.HTTP_REDIRECT_NEEDS_CONFIRMATION || e.problem == WinInetErrors.ERROR_INTERNET_HTTP_TO_HTTPS_ON_REDIR || e.problem == WinInetErrors.ERROR_INTERNET_HTTPS_HTTP_SUBMIT_REDIR || e.problem == WinInetErrors.ERROR_INTERNET_HTTPS_TO_HTTP_ON_REDIR ? true : e.problem == WinInetErrors.ERROR_INTERNET_MIXED_SECURITY)) {
                e.handled = true;
                e.retvalue = 1;
            }
            string str = (this.m_CurWB != null ? this.m_CurWB.Name : "cEXWBxx");
            AllForms.m_frmLog.AppendToLog(string.Concat(str, "_WBSecurityProblem - Wininet Problem=", e.problem.ToString()));
        }

        private void cEXWB1_WBStop(object sender) {
            AllForms.m_frmLog.AppendToLog("STOPPED");
        }

        private void cEXWB1_WindowClosing(object sender, WindowClosingEventArgs e) {
            e.Cancel = true;
            if (MessageBox.Show(this, "A script has requested to close this window. Proceed?", "Browser Closing Request", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.No) {
                cEXWB _cEXWB = (cEXWB)sender;
                if (_cEXWB != null) {
                    if (!(_cEXWB.Name == this.m_CurWB.Name)) {
                        this.RemoveBrowser2(_cEXWB.Name, true);
                    }
                    else {
                        this.RemoveBrowser(_cEXWB.Name);
                    }
                }
            }
        }

        private bool CheckWBPointer() {
            return (this.m_CurWB == null ? false : true);
        }

        private void comboURL_KeyUp(object sender, KeyEventArgs e) {
            try {
                if (e.KeyCode == Keys.Return) {
                    e.Handled = true;
                    this.NavToUrl(this.comboURL.Text);
                }
            }
            catch (Exception exception) {
                MessageBox.Show(exception.ToString(), "comboUrl_KeyUp");
            }
        }

        private void comienzaDescargaMasiva() {
            try {
                this.descargaThread = new Thread(new ThreadStart(this.ThreadDownload));
                this.descargaThread.Start();
            }
            catch (Exception exception) {
                this.respuesta = string.Concat("frmMain.comienzaDescargaMasiva()->", exception.ToString());
                this.trabajando = false;
            }
        }

        private void ConsultaEmitidas() {
            try {
                this.progess = 1;
                this.m_CurWB.GetElementByID(true, "ctl00_MainContent_RdoFechas").click();
                Thread.Sleep(1000);
                this.progess = 2;
                if (!this.porHora) {
                    this.m_CurWB.GetElementByID(true, "ctl00_MainContent_CldFechaInicial2_Calendario_text").setAttribute("value", this.fechaInicial, 1);
                    Thread.Sleep(1000);
                    this.progess = 3;
                    this.m_CurWB.GetElementByID(true, "ctl00_MainContent_CldFechaFinal2_Calendario_text").setAttribute("value", this.fechaFinal, 1);
                    Thread.Sleep(1000);
                    this.progess = 4;
                    this.setHoraMinEmitidas();
                    Thread.Sleep(1000);
                    this.progess = 5;
                }
                else {
                    if (this.auxNoHoraDia == 0) {
                        IHTMLElement elementByID = this.m_CurWB.GetElementByID(true, "ctl00_MainContent_CldFechaInicial2_Calendario_text");
                        object[] objArray = new object[] { this.diaConInicial, "/", this.mesConInicial, "/", this.anno };
                        elementByID.setAttribute("value", string.Concat(objArray), 1);
                        Thread.Sleep(500);
                        this.progess = 3;
                        IHTMLElement hTMLElement = this.m_CurWB.GetElementByID(true, "ctl00_MainContent_CldFechaFinal2_Calendario_text");
                        objArray = new object[] { this.diaConInicial, "/", this.mesConInicial, "/", this.anno };
                        hTMLElement.setAttribute("value", string.Concat(objArray), 1);
                        Thread.Sleep(500);
                    }
                    this.progess = 4;
                    this.setHoraEmitidas();
                    this.progess = 5;
                }
                this.m_CurWB.GetElementByID(true, "ctl00_MainContent_BtnBusqueda").click();
                this.progess = 6;
                this.DescargarTodoToolStripButtonClickHandler(12);
            }
            catch (Exception exception) {
                this.respuesta = string.Concat("frmMain.ThreadProcSafe()->", exception.ToString());
                this.trabajando = false;
            }
        }

        private void ConsultaRecibidas() {
            IHTMLElement current;
            try {
                this.m_CurWB.GetElementByID(true, "ctl00_MainContent_RdoFechas").click();
                this.progess = 1;
                Thread.Sleep(1500);
                IHTMLElement elementByID = this.m_CurWB.GetElementByID(true, "DdlAnio");
                elementByID.removeAttribute("onchange", 1);
                elementByID.setAttribute("onchange", "", 1);
                IHTMLElement hTMLElement = this.m_CurWB.GetElementByID(true, "ctl00_MainContent_CldFecha_DdlMes");
                hTMLElement.removeAttribute("onchange", 1);
                hTMLElement.setAttribute("onchange", "", 1);
                this.progess = 2;
                Thread.Sleep(1000);
                IEnumerator enumerator = this.m_CurWB.GetElementsByTagName(true, "option").GetEnumerator();
                DateTime now = DateTime.Now;
                if (now.Year != this.anno) {
                    while (enumerator.MoveNext()) {
                        current = (IHTMLElement)enumerator.Current;
                        string str = current.getAttribute("value", 1).ToString();
                        if (this.comboBoxAnno == null) {
                            if (str.Equals(string.Concat(now.Year))) {
                                this.comboBoxAnno = current;
                            }
                        }
                        if (this.optionAnno == null) {
                            if (str.Equals(string.Concat(this.anno))) {
                                this.optionAnno = current;
                            }
                        }
                        if ((this.comboBoxAnno == null ? false : this.optionAnno != null)) {
                            this.comboBoxAnno.removeAttribute("selected", 1);
                            this.optionAnno.setAttribute("selected", "selected", 1);
                            break;
                        }
                    }
                    Thread.Sleep(700);
                }
                this.progess = 3;
                bool flag = false;
                this.optionMes = null;
                while (enumerator.MoveNext()) {
                    current = (IHTMLElement)enumerator.Current;
                    string str1 = current.getAttribute("value", 1).ToString();
                    if (this.comboBoxMes == null) {
                        if (str1.Equals("1")) {
                            this.comboBoxMes = current;
                            if (this.mesConInicial == 1) {
                                flag = true;
                            }
                        }
                    }
                    if ((this.comboBoxMes == null ? false : this.optionMes == null)) {
                        if (str1.Equals(string.Concat(this.mesConInicial))) {
                            if (!flag) {
                                if (!this.diaConsultaDifVacio) {
                                    this.optionMes = current;
                                    this.comboBoxMes.removeAttribute("selected", 1);
                                    this.optionMes.setAttribute("selected", "selected", 1);
                                    this.comboBoxMes = this.optionMes;
                                }
                            }
                        }
                    }
                    if (str1.Equals("12")) {
                        break;
                    }
                }
                Thread.Sleep(700);
                this.optionDia = null;
                this.progess = 4;
                if (!this.diaConsulta.Equals("")) {
                    this.diaConsultaDifVacio = true;
                    while (enumerator.MoveNext()) {
                        current = (IHTMLElement)enumerator.Current;
                        string str2 = current.getAttribute("value", 1).ToString();
                        if (this.comboBoxDia == null) {
                            if (str2.Equals("0")) {
                                this.comboBoxDia = current;
                            }
                        }
                        if ((this.comboBoxDia == null ? false : this.optionDia == null)) {
                            if (str2.Equals((this.diaConInicial.ToString().Length == 1 ? string.Concat("0", this.diaConInicial) : string.Concat(this.diaConInicial)))) {
                                this.optionDia = current;
                                this.comboBoxDia.removeAttribute("selected", 1);
                                this.optionDia.setAttribute("selected", "selected", 1);
                                this.comboBoxDia = this.optionDia;
                            }
                        }
                        if (str2.Equals("31")) {
                            break;
                        }
                    }
                    Thread.Sleep(700);
                    this.progess = 5;
                }
                if (this.porHora) {
                    this.optionHoraIni = null;
                    int num = 0;
                    while (enumerator.MoveNext()) {
                        current = (IHTMLElement)enumerator.Current;
                        string str3 = current.getAttribute("value", 1).ToString();
                        if (this.comboBoxHoraIni == null) {
                            if (str3.Equals("0")) {
                                this.comboBoxHoraIni = current;
                            }
                        }
                        if ((this.comboBoxHoraIni == null ? false : this.optionHoraIni == null)) {
                            if (str3.Equals(this.auxNoHoraDia.ToString() ?? "")) {
                                this.optionHoraIni = current;
                                this.comboBoxHoraIni.removeAttribute("selected", 1);
                                this.optionHoraIni.setAttribute("selected", "selected", 1);
                                this.comboBoxHoraIni = this.optionHoraIni;
                            }
                        }
                        if (str3.Equals("59")) {
                            num++;
                        }
                        if (num == 2) {
                            break;
                        }
                    }
                    this.optionHoraFin = null;
                    while (enumerator.MoveNext()) {
                        current = (IHTMLElement)enumerator.Current;
                        string str4 = current.getAttribute("value", 1).ToString();
                        if (this.comboBoxHoraFin == null) {
                            if (str4.Equals("0")) {
                                this.comboBoxHoraFin = current;
                            }
                        }
                        if ((this.comboBoxHoraFin == null ? false : this.optionHoraFin == null)) {
                            if (str4.Equals(this.auxNoHoraDia.ToString() ?? "")) {
                                this.optionHoraFin = current;
                                this.comboBoxHoraFin.removeAttribute("selected", 1);
                                this.optionHoraFin.setAttribute("selected", "selected", 1);
                                this.comboBoxHoraFin = this.optionHoraFin;
                            }
                        }
                        if (str4.Equals("23")) {
                            break;
                        }
                    }
                    this.optionMinuFin = null;
                    while (enumerator.MoveNext()) {
                        current = (IHTMLElement)enumerator.Current;
                        string str5 = current.getAttribute("value", 1).ToString();
                        if (this.comboBoxMinuFin == null) {
                            if (str5.Equals("59")) {
                                this.comboBoxMinuFin = current;
                            }
                        }
                        if ((this.comboBoxMinuFin == null ? false : this.optionMinuFin == null)) {
                            if (str5.Equals("59")) {
                                this.optionMinuFin = current;
                                this.comboBoxMinuFin.removeAttribute("selected", 1);
                                this.optionMinuFin.setAttribute("selected", "selected", 1);
                                this.comboBoxMinuFin = this.optionMinuFin;
                            }
                        }
                        if (str5.Equals("59")) {
                            break;
                        }
                    }
                    this.optionSeguFin = null;
                    while (enumerator.MoveNext()) {
                        current = (IHTMLElement)enumerator.Current;
                        string str6 = current.getAttribute("value", 1).ToString();
                        if (this.comboBoxSeguFin == null) {
                            if (str6.Equals("59")) {
                                this.comboBoxSeguFin = current;
                            }
                        }
                        if ((this.comboBoxSeguFin == null ? false : this.optionSeguFin == null)) {
                            if (str6.Equals("59")) {
                                this.optionSeguFin = current;
                                this.comboBoxSeguFin.removeAttribute("selected", 1);
                                this.optionSeguFin.setAttribute("selected", "selected", 1);
                                this.comboBoxSeguFin = this.optionSeguFin;
                            }
                        }
                        if (str6.Equals("59")) {
                            break;
                        }
                    }
                    Thread.Sleep(700);
                }
                if (!this.estadoComprobante.Equals("")) {
                    IHTMLElement hTMLElement1 = null;
                    while (enumerator.MoveNext()) {
                        current = (IHTMLElement)enumerator.Current;
                        if (hTMLElement1 == null) {
                            if ((current.outerText ?? "").Equals("Seleccione un valor...")) {
                                hTMLElement1 = current;
                            }
                        }
                        if ((current.outerText ?? "").Equals(this.estadoComprobante)) {
                            hTMLElement1.removeAttribute("selected", 1);
                            current.setAttribute("selected", "selected", 1);
                            break;
                        }
                    }
                    Thread.Sleep(700);
                }
                this.m_CurWB.GetElementByID(true, "ctl00_MainContent_BtnBusqueda").click();
                this.progess = 6;
                this.DescargarTodoToolStripButtonClickHandler(13);
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                this.respuesta = string.Concat("frmMain.ConsultaRecibidas()->", exception.ToString(), " e.StackTrace->", exception.StackTrace);
                StackTrace stackTrace = new StackTrace(exception, true);
                string[] strArrays = new string[] { string.Concat("Exception a las ", DateTime.Now), string.Concat("Message ---", exception.Message), string.Concat("HelpLink ---", exception.HelpLink), string.Concat("Source ---", exception.Source), string.Concat("StackTrace ---", exception.StackTrace), string.Concat("TargetSite ---", exception.TargetSite), string.Concat("Exception ---", exception.ToString()), string.Concat("GetFileLineNumber ---", stackTrace.GetFrame(0).GetFileLineNumber()) };
                File.WriteAllLines("BitacoraDescargaSAT.txt", strArrays);
                this.trabajando = false;
            }
        }

        public string dameElMes(int month) {
            string str = "";
            switch (month) {
                case 1: {
                        str = "Enero";
                        break;
                    }
                case 2: {
                        str = "Febrero";
                        break;
                    }
                case 3: {
                        str = "Marzo";
                        break;
                    }
                case 4: {
                        str = "Abril";
                        break;
                    }
                case 5: {
                        str = "Mayo";
                        break;
                    }
                case 6: {
                        str = "Junio";
                        break;
                    }
                case 7: {
                        str = "Julio";
                        break;
                    }
                case 8: {
                        str = "Agosto";
                        break;
                    }
                case 9: {
                        str = "Septiembre";
                        break;
                    }
                case 10: {
                        str = "Octubre";
                        break;
                    }
                case 11: {
                        str = "Noviembre";
                        break;
                    }
                case 12: {
                        str = "Diciembre";
                        break;
                    }
                default: {
                        str = "Error";
                        break;
                    }
            }
            return str;
        }

        public void descargaEspecificaMava(string url, string nombrefile) {
            this.fileNameCurrent = nombrefile;
            this.m_CurWB.Navigate(url);
        }

        private void DescargarTodoToolStripButtonClickHandler(int Column) {
            try {
                this.noColumn = Column;
                this.iterarThread = new Thread(new ThreadStart(this.ThreadIteraColumnas));
                this.iterarThread.Start();
            }
            catch (Exception exception) {
                this.respuesta = string.Concat("frmMain.DescargarTodoToolStripButtonClickHandler(noColumn)->", exception.ToString());
                this.trabajando = false;
            }
        }

        

        public void duermeFalse() {
            this.m_CurWB.duerme = false;
        }

        private cEXWB FindBrowser(string name) {
            cEXWB _cEXWB;
            try {
                foreach (Control control in this.toolStripContainer1.ContentPanel.Controls) {
                    if (control.Name == name) {
                        if (control is cEXWB) {
                            _cEXWB = (cEXWB)control;
                            return _cEXWB;
                        }
                    }
                }
            }
            catch (Exception exception) {
            }
            _cEXWB = null;
            return _cEXWB;
        }

        private ToolStripMenuItem FindWBMenu(string name) {
            ToolStripMenuItem toolStripMenuItem;
            foreach (ToolStripItem item in this.ctxMnuOpenWBs.Items) {
                if (item.Name == name) {
                    if (item is ToolStripMenuItem) {
                        toolStripMenuItem = (ToolStripMenuItem)item;
                        return toolStripMenuItem;
                    }
                }
            }
            toolStripMenuItem = null;
            return toolStripMenuItem;
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e) {
        }

        private void frmMain_Load(object sender, EventArgs e) {
            try {
                this.cEXWB1.DownloadComplete += new DownloadCompleteEventHandler(this.cEXWB1_DownloadComplete);
                this.cEXWB1.DocumentComplete += new DocumentCompleteEventHandler(this.cEXWB1_DocumentComplete);
                this.cEXWB1.TitleChange += new TitleChangeEventHandler(this.cEXWB1_TitleChange);
                this.cEXWB1.ScriptError += new ScriptErrorEventHandler(this.cEXWB1_ScriptError);
                this.cEXWB1.WBKeyDown += new WBKeyDownEventHandler(this.cEXWB1_WBKeyDown);
                this.cEXWB1.WindowClosing += new WindowClosingEventHandler(this.cEXWB1_WindowClosing);
                this.cEXWB1.DocumentCompleteEX += new DocumentCompleteExEventHandler(this.cEXWB1_DocumentCompleteEX);
                this.cEXWB1.WBSecurityProblem += new SecurityProblemEventHandler(this.cEXWB1_WBSecurityProblem);
                this.cEXWB1.WBKeyUp += new WBKeyUpEventHandler(this.cEXWB1_WBKeyUp);
                this.cEXWB1.WBContextMenu += new ContextMenuEventHandler(this.cEXWB1_WBContextMenu);
                this.cEXWB1.FileDownload += new FileDownloadEventHandler(this.cEXWB1_FileDownload);
                this.cEXWB1.StatusTextChange += new StatusTextChangeEventHandler(this.cEXWB1_StatusTextChange);
                this.cEXWB1.WBDragDrop += new WBDropEventHandler(this.cEXWB1_WBDragDrop);
                this.cEXWB1.WBDocHostShowUIShowMessage += new DocHostShowUIShowMessageEventHandler(this.cEXWB1_WBDocHostShowUIShowMessage);
                this.cEXWB1.SetSecureLockIcon += new SetSecureLockIconEventHandler(this.cEXWB1_SetSecureLockIcon);
                this.cEXWB1.DownloadBegin += new DownloadBeginEventHandler(this.cEXWB1_DownloadBegin);
                this.cEXWB1.NavigateComplete2 += new NavigateComplete2EventHandler(this.cEXWB1_NavigateComplete2);
                this.cEXWB1.WBEvaluteNewWindow += new EvaluateNewWindowEventHandler(this.cEXWB1_WBEvaluteNewWindow);
                this.cEXWB1.RefreshEnd += new RefreshEndEventHandler(this.cEXWB1_RefreshEnd);
                this.cEXWB1.NavigateError += new NavigateErrorEventHandler(this.cEXWB1_NavigateError);
                this.cEXWB1.BeforeNavigate2 += new BeforeNavigate2EventHandler(this.cEXWB1_BeforeNavigate2);
                this.cEXWB1.RefreshBegin += new RefreshBeginEventHandler(this.cEXWB1_RefreshBegin);
                this.cEXWB1.CommandStateChange += new CommandStateChangeEventHandler(this.cEXWB1_CommandStateChange);
                this.cEXWB1.ProgressChange += new ProgressChangeEventHandler(this.cEXWB1_ProgressChange);
                this.cEXWB1.FileDownloadExStart += new FileDownloadExEventHandler(this.cEXWB1_FileDownloadExStart);
                this.cEXWB1.FileDownloadExEnd += new FileDownloadExEndEventHandler(this.cEXWB1_FileDownloadExEnd);
                this.cEXWB1.FileDownloadExProgress += new FileDownloadExProgressEventHandler(this.cEXWB1_FileDownloadExProgress);
                this.cEXWB1.FileDownloadExError += new FileDownloadExErrorEventHandler(this.cEXWB1_FileDownloadExError);
                this.cEXWB1.FileDownloadExAuthenticate += new FileDownloadExAuthenticateEventHandler(this.cEXWB1_FileDownloadExAuthenticate);
                this.cEXWB1.FileDownloadExDownloadFileFullyWritten += new FileDownloadExDownloadFileFullyWrittenEventHandler(this.cEXWB1_FileDownloadExDownloadFileFullyWritten);
                this.cEXWB1.WBStop += new StopEventHandler(this.cEXWB1_WBStop);
                this.cEXWB1.WBLButtonDown += new HTMLMouseEventHandler(this.cEXWB1_WBLButtonDown);
                this.cEXWB1.WBLButtonUp += new HTMLMouseEventHandler(this.cEXWB1_WBLButtonUp);
                this.cEXWB1.RegisterAsBrowser = true;
                this.cEXWB1.FileDownloadDirectory = "C:\\";
                this.cEXWB1.NavToBlank();
                string name = this.cEXWB1.Name;
                this.m_tsBtnFirstTab = new ToolStripButton() {
                    ImageScaling = ToolStripItemImageScaling.None,
                    ImageAlign = ContentAlignment.MiddleCenter,
                    TextAlign = ContentAlignment.TopLeft,
                    TextImageRelation = TextImageRelation.TextAboveImage,
                    Name = name,
                    Text = "Blank",
                    Image = this.m_BlankImage,
                    ToolTipText = "about:blank",
                    Checked = true
                };
                this.m_tsBtnFirstTab.MouseUp += new MouseEventHandler(this.tsWBTabs_ToolStripButtonCtxMenuHandler);
                this.m_CurWB = this.cEXWB1;
                ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem("Blank", this.m_imgUnLock) {
                    Name = name,
                    Checked = true
                };
                this.ctxMnuOpenWBs.Items.Add(toolStripMenuItem);
                this.m_iCurMenu = this.ctxMnuOpenWBs.Items.Count - 1;
                this.fswFavorites.Path = Environment.GetFolderPath(Environment.SpecialFolder.Favorites);
                base.Size = new Size(1, 1);
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                this.respuesta = string.Concat("frmMain.frmMain_Load(sender,EventArgs)->", exception.ToString());
                throw exception;
            }
        }

        private void fswFavorites_Changed(object sender, FileSystemEventArgs e) {
            this.m_FavNeedReload = true;
        }

        private void fswFavorites_Created(object sender, FileSystemEventArgs e) {
            this.m_FavNeedReload = true;
        }

        private void fswFavorites_Deleted(object sender, FileSystemEventArgs e) {
            this.m_FavNeedReload = true;
        }

        private void fswFavorites_Renamed(object sender, RenamedEventArgs e) {
            this.m_FavNeedReload = true;
        }

        public void hideWindow() {
            try {
                if (!this.showWindow) {
                    base.Hide();
                }
            }
            catch (Exception exception) {
            }
        }

        

        public void iteraHorasDiasMeses(bool edita) {
            if (this.diaConsulta.Equals("")) {
                if (this.mesConInicial >= this.mesConFinal) {
                    if (edita && this.respuesta.Equals("")) {
                        this.respuesta = "No existen registros que cumplan con los criterios de búsqueda ingresados, intentar nuevamente.";
                    }
                    this.trabajando = false;
                }
                else {
                    Thread.Sleep(500);
                    this.mesConInicial++;
                    this.buscarMes();
                }
            }
            else if (this.lastDayMonthInicial != 0) {
                if (this.diaConInicial >= this.lastDayMonthInicial) {
                    Thread.Sleep(500);
                    this.mesConInicial++;
                    if (this.mesConInicial != this.mesConFinal) {
                        this.lastDayMonthInicial = DateTime.DaysInMonth(this.anno, this.mesConInicial);
                    }
                    else {
                        this.lastDayMonthInicial = 0;
                    }
                    this.diaConInicial = 1;
                    this.diaConsultaDifVacio = false;
                    this.buscarMes();
                }
                else {
                    Thread.Sleep(500);
                    if (!this.porHora) {
                        this.diaConInicial++;
                    }
                    else if (this.auxNoHoraDia >= 23) {
                        this.auxNoHoraDia = 0;
                        this.diaConInicial++;
                    }
                    else {
                        this.auxNoHoraDia++;
                    }
                    this.buscarMes();
                }
            }
            else if (this.diaConInicial >= this.diaConFinal) {
                Thread.Sleep(500);
                if (!this.porHora) {
                    if (edita && this.respuesta.Equals("")) {
                        this.respuesta = "No existen registros que cumplan con los criterios de búsqueda ingresados, intentar nuevamente.";
                    }
                    this.trabajando = false;
                }
                else if (this.auxNoHoraDia >= 23) {
                    if (edita && this.respuesta.Equals("")) {
                        this.respuesta = "No existen registros que cumplan con los criterios de búsqueda ingresados, intentar nuevamente.";
                    }
                    this.trabajando = false;
                }
                else {
                    this.auxNoHoraDia++;
                    this.buscarMes();
                }
            }
            else {
                Thread.Sleep(500);
                if (!this.porHora) {
                    this.diaConInicial++;
                }
                else if (this.auxNoHoraDia >= 23) {
                    this.auxNoHoraDia = 0;
                    this.diaConInicial++;
                }
                else {
                    this.auxNoHoraDia++;
                }
                this.buscarMes();
            }
        }

        private void LoadFavoriteMenuItems(DirectoryInfo objDir, ToolStripMenuItem menuitem) {
            try {
                string empty = string.Empty;
                string str = string.Empty;
                bool flag = (objDir.Name.Equals("links", StringComparison.CurrentCultureIgnoreCase) ? true : false);
                FileInfo[] files = objDir.GetFiles("*.url");
                for (int i = 0; i < (int)files.Length; i++) {
                    FileInfo fileInfo = files[i];
                    empty = Path.GetFileNameWithoutExtension(fileInfo.Name);
                    str = this.m_CurWB.ResolveInternetShortCut(fileInfo.FullName);
                    if (flag) {
                        ToolStripButton toolStripButton = new ToolStripButton(empty, this.m_imgUnLock) {
                            Tag = str
                        };
                        toolStripButton.Click += new EventHandler(this.ToolStripLinksButtonClickHandler);
                    }
                }
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                AllForms.m_frmLog.AppendToLog(string.Concat("LoadFavoriteMenuItems\r\n", exception.ToString()));
            }
        }

        public void LoginToolStripButtonClickHandler(object sender, EventArgs e) {
            try {
                this.m_CurWB.GetElementByID(true, "Ecom_User_ID").setAttribute("value", this.usuario, 1);
                this.m_CurWB.GetElementByID(true, "Ecom_Password").setAttribute("value", this.pass, 1);
                this.m_CurWB.GetElementByID(true, "loginButton2").click();
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                this.respuesta = string.Concat("LoginToolStripButtonClickHandler->", exception.ToString());
                this.trabajando = false;
                AllForms.m_frmLog.AppendToLog(string.Concat("LoginToolStripButtonClickHandler->", exception.ToString()));
            }
        }

        public void maxWindow() {
            try {
                base.Hide();
            }
            catch (Exception exception) {
            }
        }

        private void NavToUrl(string sUrl) {
            if (this.CheckWBPointer()) {
                try {
                    this.m_CurWB.Navigate(sUrl);
                }
                catch (Exception exception1) {
                    Exception exception = exception1;
                    AllForms.m_frmLog.AppendToLog(string.Concat("NavToUrl\r\n", exception.ToString()));
                }
            }
        }

        private bool RemoveBrowser(string name) {
            bool flag;
            bool flag1 = false;
            if ((this.m_iCountWB == 1 ? false : !(name == this.m_tsBtnFirstTab.Name))) {
                this.tsProgress.Value = 0;
                this.tsProgress.Maximum = 0;
                this.tsProgress.Visible = false;
                ToolStripButton mTsBtnFirstTab = null;
                try {
                    cEXWB _cEXWB = this.FindBrowser(name);
                    this.toolStripContainer1.ContentPanel.Controls.Remove(_cEXWB);
                    _cEXWB.Dispose();
                    _cEXWB = null;
                }
                catch (Exception exception1) {
                    Exception exception = exception1;
                    AllForms.m_frmLog.AppendToLog(string.Concat("RemoveBrowser2\r\n", exception.ToString()));
                }
                ToolStripMenuItem toolStripMenuItem = this.FindWBMenu(name);
                ToolStripMenuItem toolStripMenuItem1 = null;
                try {
                    foreach (ToolStripItem item in this.ctxMnuOpenWBs.Items) {
                        if (item.Name == toolStripMenuItem.Name) {
                            break;
                        }
                        else if (item is ToolStripMenuItem) {
                            toolStripMenuItem1 = (ToolStripMenuItem)item;
                        }
                    }
                    this.ctxMnuOpenWBs.Items.Remove(toolStripMenuItem);
                    toolStripMenuItem.Dispose();
                    toolStripMenuItem = null;
                }
                catch (Exception exception3) {
                    Exception exception2 = exception3;
                    AllForms.m_frmLog.AppendToLog(string.Concat("RemoveBrowser3\r\n", exception2.ToString()));
                }
                try {
                    if (mTsBtnFirstTab != null) {
                        this.m_CurWB = this.FindBrowser(mTsBtnFirstTab.Name);
                        this.m_iCurMenu = this.ctxMnuOpenWBs.Items.IndexOf(toolStripMenuItem1);
                    }
                    else {
                        this.m_CurWB = this.cEXWB1;
                        this.m_iCurMenu = 0;
                        mTsBtnFirstTab = this.m_tsBtnFirstTab;
                    }
                    this.Text = this.m_CurWB.GetTitle(true);
                    if (this.Text.Length == 0) {
                        this.Text = "Blank";
                    }
                    this.comboURL.Text = mTsBtnFirstTab.ToolTipText;
                    mTsBtnFirstTab.Checked = true;
                    toolStripMenuItem1.Checked = true;
                    this.m_CurWB.BringToFront();
                    this.m_CurWB.SetFocus();
                }
                catch (Exception exception5) {
                    Exception exception4 = exception5;
                    AllForms.m_frmLog.AppendToLog(string.Concat("RemoveBrowser4\r\n", exception4.ToString()));
                }
                this.m_iCountWB--;
                flag = flag1;
            }
            else {
                flag = flag1;
            }
            return flag;
        }

        private bool RemoveBrowser2(string name, bool RemoveMenu) {
            bool flag;
            try {
                if ((this.m_iCountWB == 1 ? false : !(name == this.m_tsBtnFirstTab.Name))) {
                    cEXWB _cEXWB = this.FindBrowser(name);
                    this.toolStripContainer1.ContentPanel.Controls.Remove(_cEXWB);
                    _cEXWB.Dispose();
                    _cEXWB = null;
                    if (RemoveMenu) {
                        ToolStripMenuItem toolStripMenuItem = this.FindWBMenu(name);
                        this.ctxMnuOpenWBs.Items.Remove(toolStripMenuItem);
                        toolStripMenuItem.Dispose();
                        toolStripMenuItem = null;
                    }
                    this.m_iCountWB--;
                }
                else {
                    flag = false;
                    return flag;
                }
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                AllForms.m_frmLog.AppendToLog(string.Concat("RemoveBrowser2\r\n", exception.ToString()));
            }
            flag = true;
            return flag;
        }

        public void setHoraEmitidas() {
            IHTMLElement current;
            try {
                IEnumerator enumerator = this.m_CurWB.GetElementsByTagName(true, "option").GetEnumerator();
                int num = 0;
                this.optionHoraIni = null;
                while (enumerator.MoveNext()) {
                    current = (IHTMLElement)enumerator.Current;
                    string str = current.getAttribute("value", 1).ToString();
                    if (this.comboBoxHoraIni == null) {
                        if (str.Equals("0")) {
                            this.comboBoxHoraIni = current;
                        }
                    }
                    if ((this.comboBoxHoraIni == null ? false : this.optionHoraIni == null)) {
                        if (str.Equals(this.auxNoHoraDia.ToString() ?? "")) {
                            this.optionHoraIni = current;
                            this.comboBoxHoraIni.removeAttribute("selected", 1);
                            this.optionHoraIni.setAttribute("selected", "selected", 1);
                            this.comboBoxHoraIni = this.optionHoraIni;
                        }
                    }
                    if (str.Equals("59")) {
                        num++;
                    }
                    if (num == 2) {
                        break;
                    }
                }
                this.optionHoraFin = null;
                while (enumerator.MoveNext()) {
                    current = (IHTMLElement)enumerator.Current;
                    string str1 = current.getAttribute("value", 1).ToString();
                    if (this.comboBoxHoraFin == null) {
                        if (str1.Equals("0")) {
                            this.comboBoxHoraFin = current;
                        }
                    }
                    if ((this.comboBoxHoraFin == null ? false : this.optionHoraFin == null)) {
                        if (str1.Equals(this.auxNoHoraDia.ToString() ?? "")) {
                            this.optionHoraFin = current;
                            this.comboBoxHoraFin.removeAttribute("selected", 1);
                            this.optionHoraFin.setAttribute("selected", "selected", 1);
                            this.comboBoxHoraFin = this.optionHoraFin;
                        }
                    }
                    if (str1.Equals("23")) {
                        break;
                    }
                }
                this.optionMinuFin = null;
                while (enumerator.MoveNext()) {
                    current = (IHTMLElement)enumerator.Current;
                    string str2 = current.getAttribute("value", 1).ToString();
                    if (this.comboBoxMinuFin == null) {
                        if (str2.Equals("59")) {
                            this.comboBoxMinuFin = current;
                        }
                    }
                    if ((this.comboBoxMinuFin == null ? false : this.optionMinuFin == null)) {
                        if (str2.Equals("59")) {
                            this.optionMinuFin = current;
                            this.comboBoxMinuFin.removeAttribute("selected", 1);
                            this.optionMinuFin.setAttribute("selected", "selected", 1);
                            this.comboBoxMinuFin = this.optionMinuFin;
                        }
                    }
                    if (str2.Equals("59")) {
                        break;
                    }
                }
                this.optionSeguFin = null;
                while (enumerator.MoveNext()) {
                    current = (IHTMLElement)enumerator.Current;
                    string str3 = current.getAttribute("value", 1).ToString();
                    if (this.comboBoxSeguFin == null) {
                        if (str3.Equals("59")) {
                            this.comboBoxSeguFin = current;
                        }
                    }
                    if ((this.comboBoxSeguFin == null ? false : this.optionSeguFin == null)) {
                        if (str3.Equals("59")) {
                            this.optionSeguFin = current;
                            this.comboBoxSeguFin.removeAttribute("selected", 1);
                            this.optionSeguFin.setAttribute("selected", "selected", 1);
                            this.comboBoxSeguFin = this.optionSeguFin;
                        }
                    }
                    if (str3.Equals("59")) {
                        break;
                    }
                }
                Thread.Sleep(700);
            }
            catch (Exception exception) {
                this.respuesta = string.Concat("frmMain.setHoraEmitidas()->", exception.ToString());
                this.trabajando = false;
            }
        }

        public void setHoraMinEmitidas() {
            try {
                IEnumerator enumerator = this.m_CurWB.GetElementsByTagName(true, "option").GetEnumerator();
                IHTMLElement hTMLElement = null;
                int num = 0;
                while (enumerator.MoveNext()) {
                    IHTMLElement current = (IHTMLElement)enumerator.Current;
                    string str = current.outerText.ToString();
                    if (hTMLElement == null) {
                        if (str.Equals("00")) {
                            hTMLElement = current;
                            num++;
                        }
                    }
                    if (num > 3) {
                        if (num == 4) {
                            if (str.Equals("23")) {
                                hTMLElement.removeAttribute("selected", 1);
                                current.setAttribute("selected", "selected", 1);
                                Thread.Sleep(700);
                                hTMLElement = null;
                            }
                        }
                        if (num == 5) {
                            if (str.Equals("59")) {
                                hTMLElement.removeAttribute("selected", 1);
                                current.setAttribute("selected", "selected", 1);
                                Thread.Sleep(700);
                                hTMLElement = null;
                            }
                        }
                        if (num == 6) {
                            if (str.Equals("59")) {
                                hTMLElement.removeAttribute("selected", 1);
                                current.setAttribute("selected", "selected", 1);
                                break;
                            }
                        }
                    }
                    else {
                        hTMLElement = null;
                    }
                }
            }
            catch (Exception exception) {
                this.respuesta = string.Concat("frmMain.setHoraMinEmitidas()->", exception.ToString());
                this.trabajando = false;
            }
        }

        public void setPathBandeja(string ruta) {
            try {
                this.cEXWB1.FileDownloadDirectory = ruta;
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                AllForms.m_frmLog.AppendToLog(string.Concat("LoginToolStripButtonClickHandler\r\n", exception.ToString()));
            }
        }

        private void SetZoomLevel(int iLevel) {
            this.tsViewMnuTextSizeLargest.Checked = false;
            this.tsViewMnuTextSizeLarger.Checked = false;
            this.tsViewMnuTextSizeMedium.Checked = false;
            this.tsViewMnuTextSizeSmaller.Checked = false;
            this.tsViewMnuTextSizeSmallest.Checked = false;
            switch (iLevel) {
                case 0: {
                        this.tsViewMnuTextSizeLargest.Checked = true;
                        if (this.m_CurWB != null) {
                            this.m_CurWB.TextSize = TextSizeWB.Largest;
                        }
                        break;
                    }
                case 1: {
                        this.tsViewMnuTextSizeLarger.Checked = true;
                        if (this.m_CurWB != null) {
                            this.m_CurWB.TextSize = TextSizeWB.Larger;
                        }
                        break;
                    }
                case 2: {
                        this.tsViewMnuTextSizeMedium.Checked = true;
                        if (this.m_CurWB != null) {
                            this.m_CurWB.TextSize = TextSizeWB.Medium;
                        }
                        break;
                    }
                case 3: {
                        this.tsViewMnuTextSizeSmaller.Checked = true;
                        if (this.m_CurWB != null) {
                            this.m_CurWB.TextSize = TextSizeWB.Smaller;
                        }
                        break;
                    }
                case 4: {
                        this.tsViewMnuTextSizeSmallest.Checked = true;
                        if (this.m_CurWB != null) {
                            this.m_CurWB.TextSize = TextSizeWB.Smallest;
                        }
                        break;
                    }
            }
        }

        public void showVentana() {
            try {
                if (this.showWindow) {
                    base.Show();
                }
            }
            catch (Exception exception) {
            }
        }

        public void StopFileDownload(string browsername, int dlUid) {
            cEXWB _cEXWB = this.FindBrowser(browsername);
            if (_cEXWB != null) {
                _cEXWB.StopFileDownload(dlUid);
            }
        }

        private void SwitchTabs(string name, ToolStripButton btn) {
            try {
                cEXWB _cEXWB = this.FindBrowser(name);
                if (_cEXWB != null) {
                    this.m_CurWB = _cEXWB;
                    this.m_CurWB.BringToFront();
                    this.m_CurWB.SetFocus();
                    this.Text = this.m_CurWB.GetTitle(true);
                    if (this.Text.Length == 0) {
                        this.Text = "Blank";
                    }
                    if (btn != null) {
                        btn.Checked = true;
                        btn.Text = this.Text;
                        if (btn.Text.Length > 15) {
                            btn.Text = string.Concat(btn.Text.Substring(0, 15), "...");
                        }
                        btn.ToolTipText = HttpUtility.UrlDecode(this.m_CurWB.LocationUrl);
                        this.comboURL.Text = btn.ToolTipText;
                    }
                    foreach (ToolStripItem item in this.ctxMnuOpenWBs.Items) {
                        if (item is ToolStripMenuItem) {
                            ((ToolStripMenuItem)item).Checked = false;
                        }
                    }
                    ToolStripMenuItem text = this.FindWBMenu(name);
                    this.m_iCurMenu = this.ctxMnuOpenWBs.Items.IndexOf(text);
                    if (text != null) {
                        if (btn != null) {
                            text.Text = btn.Text;
                        }
                        text.Checked = true;
                    }
                    this.tsProgress.Value = 0;
                    this.tsProgress.Maximum = 0;
                    this.tsProgress.Visible = false;
                    this.UpdateSecureLockIcon(this.m_CurWB.SecureLockIcon);
                }
                else {
                    return;
                }
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                AllForms.m_frmLog.AppendToLog(string.Concat("SwitchingTabs\r\n", exception.ToString()));
            }
        }

        private void ThreadDownload() {
            int length;
            float single;
            DirectoryInfo directoryInfo;
            string[] fullName;
            try {
                if (this.siDescarga) {
                    IEnumerator<DatosCfdiSat> enumerator = this.lista.GetEnumerator();
                    string str = Path.DirectorySeparatorChar.ToString();
                    bool flag = true;
                    int num = 0;
                    if (this.soloUUIDs != null) {
                        length = (int)this.soloUUIDs.Length;
                        single = (float)length / 10f;
                        if (length < 10) {
                            flag = false;
                        }
                        while (enumerator.MoveNext()) {
                            bool flag1 = false;
                            int num1 = 0;
                            while (num1 < (int)this.soloUUIDs.Length) {
                                if (!this.soloUUIDs[num1].Equals(enumerator.Current.Folio_Fiscal)) {
                                    num1++;
                                }
                                else {
                                    flag1 = true;
                                    break;
                                }
                            }
                            if (flag1) {
                                if (enumerator.Current.UrlDescarga != null) {
                                    Thread.Sleep(50);
                                    if (!this.Recibidas) {
                                        directoryInfo = new DirectoryInfo(string.Concat(this.cEXWB1.FileDownloadDirectory, "EMITIDOS", str));
                                        if (!directoryInfo.Exists) {
                                            directoryInfo.Create();
                                        }
                                        fullName = new string[] { directoryInfo.FullName, enumerator.Current.Rfc_Emisor, "_", enumerator.Current.RFC_Receptor, "_", enumerator.Current.Folio_Fiscal, ".xml" };
                                        this.fileNameCurrent = string.Concat(fullName);
                                    }
                                    else {
                                        directoryInfo = new DirectoryInfo(string.Concat(this.cEXWB1.FileDownloadDirectory, "RECIBIDOS", str));
                                        if (!directoryInfo.Exists) {
                                            directoryInfo.Create();
                                        }
                                        fullName = new string[] { directoryInfo.FullName, enumerator.Current.RFC_Receptor, "_", enumerator.Current.Rfc_Emisor, "_", enumerator.Current.Folio_Fiscal, ".xml" };
                                        this.fileNameCurrent = string.Concat(fullName);
                                    }
                                    this.m_CurWB.Navigate(enumerator.Current.UrlDescarga);
                                    while (this.m_CurWB.duerme) {
                                        Thread.Sleep(50);
                                    }
                                    this.m_CurWB.duerme = true;
                                    num++;
                                    if (!flag) {
                                        this.progess = (int)Math.Ceiling((double)num / (double)length * 10);
                                    }
                                    else if ((double)((float)num % single) < 1) {
                                        this.progess++;
                                    }
                                }
                            }
                        }
                    }
                    else {
                        length = this.lista.Count;
                        single = (float)length / 10f;
                        if (length < 10) {
                            flag = false;
                        }
                        while (enumerator.MoveNext()) {
                            if (enumerator.Current.UrlDescarga != null) {
                                Thread.Sleep(50);
                                if (!this.Recibidas) {
                                    directoryInfo = new DirectoryInfo(string.Concat(this.cEXWB1.FileDownloadDirectory, "EMITIDOS", str));
                                    if (!directoryInfo.Exists) {
                                        directoryInfo.Create();
                                    }
                                    fullName = new string[] { directoryInfo.FullName, enumerator.Current.Rfc_Emisor, "_", enumerator.Current.RFC_Receptor, "_", enumerator.Current.Folio_Fiscal, ".xml" };
                                    this.fileNameCurrent = string.Concat(fullName);
                                }
                                else {
                                    directoryInfo = new DirectoryInfo(string.Concat(this.cEXWB1.FileDownloadDirectory, "RECIBIDOS", str));
                                    if (!directoryInfo.Exists) {
                                        directoryInfo.Create();
                                    }
                                    fullName = new string[] { directoryInfo.FullName, enumerator.Current.RFC_Receptor, "_", enumerator.Current.Rfc_Emisor, "_", enumerator.Current.Folio_Fiscal, ".xml" };
                                    this.fileNameCurrent = string.Concat(fullName);
                                }
                                this.contadorDescargas++;
                                this.m_CurWB.Navigate(enumerator.Current.UrlDescarga);
                                while (this.m_CurWB.duerme) {
                                    Thread.Sleep(50);
                                }
                                this.m_CurWB.duerme = true;
                                num++;
                                if (!flag) {
                                    this.progess = (int)Math.Ceiling((double)num / (double)length * 10);
                                }
                                else if ((double)((float)num % single) < 1) {
                                    this.progess++;
                                }
                            }
                        }
                    }
                    this.progess = 10;
                }
                this.verificaContinuaBsuqueda(false);
            }
            catch (Exception exception) {
                this.respuesta = string.Concat("frmMain.ThreadDownload()->", exception.ToString());
                this.trabajando = false;
            }
        }

        private void ThreadIteraColumnas() {
            DateTime dateTime;
            IHTMLElement current;
            try {
                Thread.Sleep(2500);
                this.progess = 7;
                int num = 0;
                while (this.m_CurWB.GetElementByID(true, "ctl00_MainContent_UpdateProgress1").getAttribute("aria-hidden", 1).ToString().Equals("false")) {
                    Thread.Sleep(1000);
                    num++;
                    if (num > 20) {
                        break;
                    }
                }
                Thread.Sleep(1000);
                this.progess = 8;
                IHTMLElementCollection elementsByTagName = this.m_CurWB.GetElementsByTagName(true, "td");
                IHTMLElementCollection hTMLElementCollections = this.m_CurWB.GetElementsByTagName(true, "img");
                IEnumerator enumerator = elementsByTagName.GetEnumerator();
                IEnumerator enumerator1 = hTMLElementCollections.GetEnumerator();
                num = 0;
                DatosCfdiSat datosCfdiSat = new DatosCfdiSat();
                this.lista = new List<DatosCfdiSat>();
                int num1 = 0;
                int num2 = 0;
                string str = "";
                string str1 = "";
                DateTime dateTime1 = DateTime.Parse(this.fechaInicial);
                DateTime dateTime2 = DateTime.Parse(this.fechaFinal);
                while (enumerator.MoveNext()) {
                    IHTMLElement hTMLElement = (IHTMLElement)enumerator.Current;
                    try {
                        str = hTMLElement.outerHTML;
                    }
                    catch (Exception exception) {
                        frmMain _frmMain = this;
                        _frmMain.cfdiMes = string.Concat(_frmMain.cfdiMes, "Demaciados Datos\n");
                        break;
                    }
                    if (str.ToUpper().Contains("WORD-BREAK")) {
                        str1 = str;
                        num++;
                        if (num == this.noColumn) {
                            if (!this.Recibidas) {
                                frmMain _frmMain1 = this;
                                _frmMain1.cfdiMes = string.Concat(_frmMain1.cfdiMes, "|");
                                this.lista.Add(datosCfdiSat);
                            }
                            else {
                                dateTime = DateTime.Parse(datosCfdiSat.Fecha_Emision);
                                if ((dateTime1.Date > dateTime.Date ? false : dateTime.Date <= dateTime2.Date)) {
                                    this.lista.Add(datosCfdiSat);
                                }
                            }
                            datosCfdiSat = new DatosCfdiSat();
                            frmMain _frmMain2 = this;
                            _frmMain2.cfdiMes = string.Concat(_frmMain2.cfdiMes, "\n");
                            num = 1;
                        }
                        string upper = str1.ToUpper();
                        if (!upper.Contains("BLOCK;\">")) {
                            num1 = upper.IndexOf("BLOCK\">") + 7;
                            num2 = upper.IndexOf("</SPAN>");
                        }
                        else {
                            num1 = upper.IndexOf("BLOCK;\">") + 8;
                            num2 = upper.IndexOf("</SPAN>");
                        }
                        try {
                            str1 = str1.Substring(num1, num2 - num1);
                        }
                        catch (Exception exception1) {
                            str1 = upper;
                        }
                        str1 = str1.Replace("\n", "").Replace("&amp;", "&");
                        frmMain _frmMain3 = this;
                        _frmMain3.cfdiMes = string.Concat(_frmMain3.cfdiMes, "|", str1);
                        switch (num) {
                            case 1: {
                                    datosCfdiSat.Folio_Fiscal = str1;
                                    break;
                                }
                            case 2: {
                                    datosCfdiSat.Rfc_Emisor = str1;
                                    break;
                                }
                            case 3: {
                                    datosCfdiSat.Nombre_Emisor = str1;
                                    break;
                                }
                            case 4: {
                                    datosCfdiSat.RFC_Receptor = str1;
                                    break;
                                }
                            case 5: {
                                    datosCfdiSat.Nombre_Receptor = str1;
                                    break;
                                }
                            case 6: {
                                    datosCfdiSat.Fecha_Emision = str1;
                                    break;
                                }
                            case 7: {
                                    datosCfdiSat.Fecha_Certificacion = str1;
                                    break;
                                }
                            case 8: {
                                    datosCfdiSat.Pac_Certifico = str1;
                                    break;
                                }
                            case 9: {
                                    datosCfdiSat.Total = str1;
                                    break;
                                }
                            case 10: {
                                    datosCfdiSat.Efecto_Comprobante = str1;
                                    break;
                                }
                            case 11: {
                                    datosCfdiSat.Estado_Comprobante = str1;
                                    break;
                                }
                            case 12: {
                                    datosCfdiSat.Fecha_Cancelacion = str1;
                                    break;
                                }
                        }
                        if (num == 11) {
                            if (this.siDescarga) {
                                string str2 = "";
                                if (!this.Recibidas) {
                                    while (enumerator1.MoveNext()) {
                                        current = (IHTMLElement)enumerator1.Current;
                                        str2 = current.outerHTML;
                                        if ((!str2.Contains("BtnDescarga") ? false : str2.Contains("World-download.png"))) {
                                            if (str1.ToUpper().Contains("VIGENTE")) {
                                                try {
                                                    num1 = str2.IndexOf("RecuperaCfdi");
                                                    num2 = str2.IndexOf("','Recuperacion");
                                                    str2 = str2.Substring(num1, num2 - num1);
                                                    datosCfdiSat.UrlDescarga = string.Concat("https://portalcfdi.facturaelectronica.sat.gob.mx/", str2);
                                                }
                                                catch (Exception exception2) {
                                                    datosCfdiSat.UrlDescarga = null;
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                                else if (str1.ToUpper().Contains("VIGENTE")) {
                                    while (enumerator1.MoveNext()) {
                                        current = (IHTMLElement)enumerator1.Current;
                                        str2 = current.outerHTML;
                                        if ((!str2.Contains("BtnDescarga") ? false : str2.Contains("World-download.png"))) {
                                            try {
                                                num1 = str2.IndexOf("RecuperaCfdi");
                                                num2 = str2.IndexOf("','Recuperacion");
                                                str2 = str2.Substring(num1, num2 - num1);
                                                datosCfdiSat.UrlDescarga = string.Concat("https://portalcfdi.facturaelectronica.sat.gob.mx/", str2);
                                            }
                                            catch (Exception exception3) {
                                                datosCfdiSat.UrlDescarga = null;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                this.progess = 9;
                if (datosCfdiSat.Folio_Fiscal != null) {
                    if (!this.Recibidas) {
                        this.lista.Add(datosCfdiSat);
                    }
                    else {
                        dateTime = DateTime.Parse(datosCfdiSat.Fecha_Emision);
                        if ((dateTime1.Date > dateTime.Date ? false : dateTime.Date <= dateTime2.Date)) {
                            this.lista.Add(datosCfdiSat);
                        }
                    }
                }
                if (this.lista.Count <= 0) {
                    this.progess = 10;
                    Thread.Sleep(500);
                    this.verificaContinuaBsuqueda(true);
                }
                else {
                    frmMain _frmMain4 = this;
                    _frmMain4.cfdiMes = string.Concat(_frmMain4.cfdiMes, "\n");
                    this.respuesta = this.cfdiMes;
                    if (this.siDescarga) {
                        this.muestraDescarga = true;
                    }
                    this.progess = 10;
                    this.comienzaDescargaMasiva();
                }
            }
            catch (Exception exception5) {
                Exception exception4 = exception5;
                StackTrace stackTrace = new StackTrace(exception4, true);
                object[] objArray = new object[] { "frmMain.ThreadIteraColumnas()->", exception4.ToString(), "<->", stackTrace.GetFrame(0).GetFileLineNumber(), "<-cfdiMes->", this.cfdiMes, "<-" };
                this.respuesta = string.Concat(objArray);
                string[] strArrays = new string[] { string.Concat("Exception a las ", DateTime.Now), string.Concat("Message ---", exception4.Message), string.Concat("HelpLink ---", exception4.HelpLink), string.Concat("Source ---", exception4.Source), string.Concat("StackTrace ---", exception4.StackTrace), string.Concat("TargetSite ---", exception4.TargetSite), string.Concat("Exception ---", exception4.ToString()), string.Concat("GetFileLineNumber ---", stackTrace.GetFrame(0).GetFileLineNumber()) };
                File.WriteAllLines("BitacoraDescargaSAT.txt", strArrays);
                this.trabajando = false;
            }
        }

        private void ToolStripLinksButtonClickHandler(object sender, EventArgs e) {
            try {
                ToolStripItem toolStripItem = (ToolStripItem)sender;
                if (toolStripItem.Tag != null) {
                    this.NavToUrl(toolStripItem.Tag.ToString());
                }
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                AllForms.m_frmLog.AppendToLog(string.Concat("ToolStripLinksButtonClickHandler\r\n", exception.ToString()));
            }
        }

        private void tsFavoritesMnu_DropDownOpening(object sender, EventArgs e) {
            if (!this.m_FavNeedReload) {
            }
        }

        private void tsMnuCloasAllWBs_Click(object sender, EventArgs e) {
            if (this.m_iCountWB != 1) {
                try {
                    foreach (ToolStripMenuItem item in this.ctxMnuOpenWBs.Items) {
                        this.RemoveBrowser2(item.Name, false);
                    }
                    this.ctxMnuOpenWBs.Items.Clear();
                    this.m_CurWB = this.cEXWB1;
                    this.m_CurWB.BringToFront();
                    this.m_CurWB.SetFocus();
                    this.m_tsBtnFirstTab.Checked = true;
                    string title = this.m_CurWB.GetTitle(true);
                    if (title.Length == 0) {
                        title = "Blank";
                    }
                    ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem(title, this.m_imgUnLock) {
                        Name = this.m_tsBtnFirstTab.Name,
                        Checked = true
                    };
                    this.ctxMnuOpenWBs.Items.Add(toolStripMenuItem);
                    this.m_iCurMenu = this.ctxMnuOpenWBs.Items.Count - 1;
                }
                catch (Exception exception1) {
                    Exception exception = exception1;
                    AllForms.m_frmLog.AppendToLog(string.Concat("tsMnuCloasAllWBs_Click\r\n", exception.ToString()));
                }
            }
        }

        private void tsMnuCloseWB_Click(object sender, EventArgs e) {
            try {
                if (this.m_tsBtnctxMnu != null) {
                    if (!(this.m_tsBtnctxMnu.Name == this.m_CurWB.Name)) {
                        this.RemoveBrowser2(this.m_tsBtnctxMnu.Name, true);
                    }
                    else {
                        this.RemoveBrowser(this.m_tsBtnctxMnu.Name);
                    }
                    this.m_tsBtnctxMnu = null;
                }
                else {
                    return;
                }
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                AllForms.m_frmLog.AppendToLog(string.Concat("tsMnuCloseWB_Click\r\n", exception.ToString()));
            }
        }

        private void tsMnuTextSize_DropDownOpening(object sender, EventArgs e) {
            this.tsViewMnuTextSizeLargest.Checked = false;
            this.tsViewMnuTextSizeLarger.Checked = false;
            this.tsViewMnuTextSizeMedium.Checked = false;
            this.tsViewMnuTextSizeSmaller.Checked = false;
            this.tsViewMnuTextSizeSmallest.Checked = false;
            if (this.cEXWB1.TextSize == TextSizeWB.Largest) {
                this.tsViewMnuTextSizeLargest.Checked = true;
            }
            else if (this.cEXWB1.TextSize == TextSizeWB.Larger) {
                this.tsViewMnuTextSizeLarger.Checked = true;
            }
            else if (this.cEXWB1.TextSize == TextSizeWB.Medium) {
                this.tsViewMnuTextSizeMedium.Checked = true;
            }
            else if (this.cEXWB1.TextSize == TextSizeWB.Smaller) {
                this.tsViewMnuTextSizeSmaller.Checked = true;
            }
            else if (this.cEXWB1.TextSize == TextSizeWB.Smallest) {
                this.tsViewMnuTextSizeSmallest.Checked = true;
            }
        }

        private void tsMnuTextSizeClickHandler(object sender, EventArgs e) {
            if (sender == this.tsViewMnuTextSizeLargest) {
                this.SetZoomLevel(0);
            }
            else if (sender == this.tsViewMnuTextSizeLarger) {
                this.SetZoomLevel(1);
            }
            else if (sender == this.tsViewMnuTextSizeMedium) {
                this.SetZoomLevel(2);
            }
            else if (sender == this.tsViewMnuTextSizeSmaller) {
                this.SetZoomLevel(3);
            }
            else if (sender == this.tsViewMnuTextSizeSmallest) {
                this.SetZoomLevel(4);
            }
        }

        private void tsWBTabs_ItemClicked(object sender, ToolStripItemClickedEventArgs e) {
            try {
                this.AddNewBrowser("Blank", "about:blank", "about:blank", true);
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                AllForms.m_frmLog.AppendToLog(string.Concat("tsWBTabs_ItemClicked\r\n", exception.ToString()));
            }
        }

        private void tsWBTabs_ToolStripButtonCtxMenuHandler(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Right) {
                try {
                    this.tsMnuCloasAllWBs.Enabled = (this.m_iCountWB > 1 ? true : false);
                    this.ctxMnuCloseWB.Show(Cursor.Position.X, Cursor.Position.Y);
                }
                catch (Exception exception1) {
                    Exception exception = exception1;
                    AllForms.m_frmLog.AppendToLog(string.Concat("TabContextMenuHandler\r\n", exception.ToString()));
                }
            }
        }

        private void UpdateSecureLockIcon(SecureLockIconConstants slic) {
            if (slic == SecureLockIconConstants.secureLockIconUnsecure) {
                this.tsSecurity.Image = this.m_imgUnLock;
                this.tsSecurity.Text = "Not Secure";
            }
            else if (slic == SecureLockIconConstants.secureLockIcon128Bit) {
                this.tsSecurity.Image = this.m_imgLock;
                this.tsSecurity.Text = "128 Bit";
            }
            else if (slic == SecureLockIconConstants.secureLockIcon40Bit) {
                this.tsSecurity.Image = this.m_imgLock;
                this.tsSecurity.Text = "40 Bit";
            }
            else if (slic == SecureLockIconConstants.secureLockIcon56Bit) {
                this.tsSecurity.Image = this.m_imgLock;
                this.tsSecurity.Text = "56 Bit";
            }
            else if (slic == SecureLockIconConstants.secureLockIconFortezza) {
                this.tsSecurity.Image = this.m_imgLock;
                this.tsSecurity.Text = "Fortezza";
            }
            else if (slic == SecureLockIconConstants.secureLockIconMixed) {
                this.tsSecurity.Image = this.m_imgUnLock;
                this.tsSecurity.Text = "Mixed";
            }
            else if (slic == SecureLockIconConstants.secureLockIconUnknownBits) {
                this.tsSecurity.Image = this.m_imgUnLock;
                this.tsSecurity.Text = "UnknownBits";
            }
        }

        public void validaDescargas() {
            string[] fileDownloadDirectory;
            IEnumerator<DatosCfdiSat> enumerator = this.lista.GetEnumerator();
            string str = Path.DirectorySeparatorChar.ToString();
            string str1 = "";
            while (enumerator.MoveNext()) {
                if (!this.Recibidas) {
                    fileDownloadDirectory = new string[] { this.cEXWB1.FileDownloadDirectory, "EMITIDOS", str, enumerator.Current.Rfc_Emisor, "_", enumerator.Current.RFC_Receptor, "_", enumerator.Current.Folio_Fiscal, ".xml" };
                    str1 = string.Concat(fileDownloadDirectory);
                }
                else {
                    fileDownloadDirectory = new string[] { this.cEXWB1.FileDownloadDirectory, "RECIBIDOS", str, enumerator.Current.RFC_Receptor, "_", enumerator.Current.Rfc_Emisor, "_", enumerator.Current.Folio_Fiscal, ".xml" };
                    str1 = string.Concat(fileDownloadDirectory);
                }
                if (!File.Exists(str1)) {
                    if (enumerator.Current.UrlDescarga != null) {
                        int num = 0;
                        while (num <= 3) {
                            this.m_CurWB.Navigate(enumerator.Current.UrlDescarga);
                            while (this.m_CurWB.duerme) {
                                Thread.Sleep(800);
                            }
                            this.m_CurWB.duerme = true;
                            if (!File.Exists(str1)) {
                                num++;
                            }
                            else {
                                break;
                            }
                        }
                    }
                    else if (enumerator.Current.Estado_Comprobante.ToUpper().Equals("VIGENTE")) {
                        File.WriteAllText(string.Concat("C:\\DivContenedor_", this.mesConInicial, ".txt"), string.Concat(str1, " venia con la url NULL Y esta vigente"));
                    }
                }
            }
        }

        public void verificaContinuaBsuqueda(bool edita) {
            try {
                if (this.Recibidas) {
                    this.iteraHorasDiasMeses(edita);
                }
                else if (!this.porHora) {
                    if (edita && this.respuesta.Equals("")) {
                        this.respuesta = "No existen registros que cumplan con los criterios de búsqueda ingresados, intentar nuevamente.";
                    }
                    this.trabajando = false;
                }
                else {
                    this.iteraHorasDiasMeses(edita);
                }
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                object[] objArray = new object[] { "frmMain.verificaContinuaBsuqueda(", edita, ")->", exception.ToString() };
                this.respuesta = string.Concat(objArray);
                this.trabajando = false;
            }
        }
    }
}