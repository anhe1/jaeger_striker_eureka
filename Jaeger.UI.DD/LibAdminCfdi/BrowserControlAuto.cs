﻿using Jaeger.UI.LibAdminCfdi.AdminCFDI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace Jaeger.UI.LibAdminCfdi {
    public class BrowserControlAuto : UserControl {
        private HtmlElement comboBoxMes = null;

        private HtmlElement comboBoxAnno = null;

        private HtmlElement comboBoxDia = null;

        private HtmlElement comboBoxHoraIni = null;

        private HtmlElement comboBoxHoraFin = null;

        private HtmlElement comboBoxMinuFin = null;

        private HtmlElement comboBoxSeguFin = null;

        private bool diaConsultaDifVacio = false;

        private int noColumn = 0;

        private string cfdiMes = "";

        private bool trabajandoDescarga = true;

        private int sumaXML = 0;

        private int count = 0;

        private ExtendedWebBrowser _browser;

        public HtmlElement btnEnviar = null;

        private Thread descargaThread;

        public bool porHora = false;

        public bool showWindow = true;

        public bool openSession = false;

        public bool inicializando = true;

        public bool trabajando = true;

        public string respuesta = "";

        public string usuario = "";

        public string pass = "";

        public bool Recibidas = true;

        public string estadoComprobante = "";

        public string diaConsulta = "";

        public bool siDescarga = true;

        public string[] soloUUIDs = null;

        public int mesConInicial = 8;

        public int mesConFinal = 12;

        public int diaConInicial = 1;

        public int diaConFinal = 28;

        public int lastDayMonthInicial = 0;

        public int anno = 2015;

        public int numBusquedas = 0;

        public int auxNoHoraDia = 0;

        private string fileNameCurrent = "";

        public string fechaInicial = "01/10/2014";

        public string fechaFinal = "31/10/2014";

        private List<DatosCfdiSat> lista = null;

        private List<DatosCfdiSat> listaAll = new List<DatosCfdiSat>();

        public int contadorDescargas = 0;

        public BackgroundWorker fondoTrabajador1;

        public int progess = 10;

        public string tipoProgreso = "Conectando con Repositorio Fiscal:";

        public int versionProgreso = 0;

        public string pathBandeja = "";

        public frmProgress alert;

        private IContainer components = null;

        private Panel containerPanel;

        private Panel panel2;

        private Button goButton;

        private TextBox addressTextBox;

        private Label label1;

        public ExtendedWebBrowser WebBrowser {
            get {
                return this._browser;
            }
        }

        public BrowserControlAuto() {
            this.InitializeComponent();
            this._browser = new ExtendedWebBrowser() {
                Dock = DockStyle.Fill
            };
            this._browser.DownloadComplete += new EventHandler(this._browser_DownloadComplete);
            this._browser.Navigated += new WebBrowserNavigatedEventHandler(this._browser_Navigated);
            this._browser.StartNewWindow += new EventHandler<BrowserExtendedNavigatingEventArgs>(this._browser_StartNewWindow);
            this._browser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this._browser_DocumentCompleted);
            this.containerPanel.Controls.Add(this._browser);
            this.fondoTrabajador1.RunWorkerAsync();
            this.alert = new frmProgress();
            this.alert.Canceled += new EventHandler<EventArgs>(this.buttonCancel_Click);
            this.alert.Show();
            ((Bitmap)this.goButton.Image).MakeTransparent(Color.Magenta);
        }

        private void _browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e) {
            HtmlDocument document;
            string str;
            string str1 = this.WebBrowser.Url.ToString();
            this.WebBrowser.Zoom(70);
            this.WebBrowser.ScriptErrorsSuppressed = true;
            string str2 = str1;
            if (str2 == null) {
                if (str1.StartsWith("https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATUPCFDiCon")) {
                    document = this.WebBrowser.Document;
                    str = this.usuario;
                    if (this.usuario.Contains("&")) {
                        str = this.usuario.Replace("&", " ");
                    }
                    document.GetElementById("Ecom_User_ID").SetAttribute("value", str);
                    document.GetElementById("Ecom_Password").SetAttribute("value", this.pass);
                }
                this.UpdateAddressBox();
                return;
            }
            else if (str2 == "https://portalcfdi.facturaelectronica.sat.gob.mx/") {
                this.openSession = true;
                if (!this.Recibidas) {
                    this.WebBrowser.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx");
                }
                else {
                    this.WebBrowser.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx");
                }
            }
            else if (str2 == "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx") {
                this.WebBrowser.Document.GetElementById("ctl00_MainContent_RdoFechas").InvokeMember("click");
            }
            else if (str2 == "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx") {
                this.WebBrowser.Document.GetElementById("ctl00_MainContent_RdoFechas").InvokeMember("click");
            }
            else {
                if (str2 != "https://cfdiau.sat.gob.mx/nidp/lofc.jsp") {
                    if (str1.StartsWith("https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATUPCFDiCon")) {
                        document = this.WebBrowser.Document;
                        str = this.usuario;
                        if (this.usuario.Contains("&")) {
                            str = this.usuario.Replace("&", " ");
                        }
                        document.GetElementById("Ecom_User_ID").SetAttribute("value", str);
                        document.GetElementById("Ecom_Password").SetAttribute("value", this.pass);
                    }
                    this.UpdateAddressBox();
                    return;
                }
                this.openSession = false;
                this.trabajando = false;
            }
            this.UpdateAddressBox();
        }

        private void _browser_DownloadComplete(object sender, EventArgs e) {
            if (this.WebBrowser.Document != null) {
                this.WebBrowser.Document.Window.Error += new HtmlElementErrorEventHandler(this.Window_Error);
                this.UpdateAddressBox();
            }
        }

        private void _browser_Navigated(object sender, WebBrowserNavigatedEventArgs e) {
            this.UpdateAddressBox();
        }

        private void _browser_StartNewWindow(object sender, BrowserExtendedNavigatingEventArgs e) {
            bool flag;
            MainForm mainFormFromControl = BrowserControlAuto.GetMainFormFromControl(sender as Control);
            if (mainFormFromControl != null) {
                bool flag1 = (e.NavigationContext == UrlContext.None ? true : (e.NavigationContext & UrlContext.OverrideKey) == UrlContext.OverrideKey);
                if (!flag1) {
                    switch (0) {
                        case 0: {
                                flag1 = true;
                                break;
                            }
                        case 1: {
                                if (this.WebBrowser.EncryptionLevel == WebBrowserEncryptionLevel.Insecure) {
                                    goto case 2;
                                }
                                else {
                                    flag1 = true;
                                    break;
                                }
                            }
                        case 2: {
                                flag = ((e.NavigationContext & UrlContext.UserFirstInited) != UrlContext.UserFirstInited ? true : (e.NavigationContext & UrlContext.UserInited) != UrlContext.UserInited);
                                if (!flag) {
                                    flag1 = true;
                                }
                                break;
                            }
                    }
                }
                if (!flag1) {
                    e.Cancel = true;
                }
                else if ((e.NavigationContext & UrlContext.HtmlDialog) != UrlContext.HtmlDialog) {
                    e.AutomationObject = mainFormFromControl.WindowManager.New(false).Application;
                }
            }
        }

        private void addressTextBox_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Return) {
                e.Handled = true;
                this.Navigate();
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e) {
            BackgroundWorker backgroundWorker = sender as BackgroundWorker;
            int num = this.progess;
            backgroundWorker.ReportProgress(num * 10);
            Thread.Sleep(50);
            while (num != 10) {
                if (backgroundWorker.CancellationPending) {
                    e.Cancel = true;
                    break;
                }
                else if (num != this.progess) {
                    num = this.progess;
                    backgroundWorker.ReportProgress(num * 10);
                    Thread.Sleep(50);
                }
                else {
                    Thread.Sleep(50);
                }
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            Label label = this.alert.labelMessage;
            string str = this.tipoProgreso;
            int progressPercentage = e.ProgressPercentage;
            label.Text = string.Concat(str, progressPercentage.ToString(), "%");
            this.alert.progressBar.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            if (e.Cancelled) {
                this.respuesta = "El usuario ha cancelado la Consulta/Descarga del portal del SAT";
                this.trabajando = false;
            }
            else if (e.Error != null) {
                this.respuesta = "El usuario ha cancelado la Consulta/Descarga del portal del SAT";
                this.trabajando = false;
            }
            this.alert.Close();
            if (this.trabajando) {
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e) {
            if (this.fondoTrabajador1.WorkerSupportsCancellation) {
                this.fondoTrabajador1.CancelAsync();
                this.alert.Close();
            }
        }

        private void comienzaDescargaMasiva() {
            try {
                this.trabajandoDescarga = true;
                this.descargaThread = new Thread(new ThreadStart(this.ThreadDownload));
                this.descargaThread.Start();
                while (this.trabajandoDescarga) {
                    Application.DoEvents();
                }
                Control parent = this._browser.Parent;
                object[] objArray = new object[] { "Se descargaron ", this.count, " XML(s), ahora tiene en total ", this.sumaXML, ". Puede seguir Buscando y Descargando ó puede Cerrar Sesión para continuar." };
                MessageBox.Show(parent, string.Concat(objArray), "Finalizó la Descarga:");
            }
            catch (Exception exception) {
                this.respuesta = string.Concat("BrowserControlAuto.comienzaDescargaMasiva()->", exception.ToString());
                this.trabajando = false;
            }
        }

        public void DescargarTodoToolStripButtonClickHandler(int Column) {
            try {
                this.noColumn = Column;
                this.ThreadIteraColumnasNuevo();
            }
            catch (Exception exception) {
                this.respuesta = string.Concat("BrowserControlAuto.DescargarTodoToolStripButtonClickHandler(noColumn)->", exception.ToString());
                this.trabajando = false;
            }
        }

        protected override void Dispose(bool disposing) {
            if ((!disposing ? false : this.components != null)) {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private static MainForm GetMainFormFromControl(Control control) {
            while (control != null) {
                if (!(control is MainForm)) {
                    control = control.Parent;
                }
                else {
                    break;
                }
            }
            return control as MainForm;
        }

        private void goButton_Click(object sender, EventArgs e) {
            this.Navigate();
        }

        public void inicializarBarra() {
            this.tipoProgreso = "Descargando CFDI´s encontrados ";
            this.progess = 0;
            this.alert = new frmProgress();
            this.alert.labelMessage.Text = string.Concat(this.tipoProgreso, "0%");
            this.alert.Text = "Descargando desde el portal SAT";
            this.alert.Canceled += new EventHandler<EventArgs>(this.buttonCancel_Click);
            this.alert.Show();
            this.fondoTrabajador1.RunWorkerAsync();
        }

        private void InitializeComponent() {
            this.containerPanel = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.goButton = new System.Windows.Forms.Button();
            this.addressTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fondoTrabajador1 = new System.ComponentModel.BackgroundWorker();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // containerPanel
            // 
            this.containerPanel.Location = new System.Drawing.Point(0, 0);
            this.containerPanel.Name = "containerPanel";
            this.containerPanel.Size = new System.Drawing.Size(776, 321);
            this.containerPanel.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.goButton);
            this.panel2.Controls.Add(this.addressTextBox);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 100);
            this.panel2.TabIndex = 1;
            // 
            // goButton
            // 
            this.goButton.Location = new System.Drawing.Point(0, 0);
            this.goButton.Name = "goButton";
            this.goButton.Size = new System.Drawing.Size(75, 23);
            this.goButton.TabIndex = 0;
            this.goButton.UseVisualStyleBackColor = false;
            this.goButton.Click += new System.EventHandler(this.goButton_Click);
            // 
            // addressTextBox
            // 
            this.addressTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.addressTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.addressTextBox.Location = new System.Drawing.Point(0, 0);
            this.addressTextBox.Name = "addressTextBox";
            this.addressTextBox.Size = new System.Drawing.Size(100, 20);
            this.addressTextBox.TabIndex = 1;
            this.addressTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.addressTextBox_KeyUp);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 2;
            // 
            // fondoTrabajador1
            // 
            this.fondoTrabajador1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.fondoTrabajador1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.fondoTrabajador1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // BrowserControlAuto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.containerPanel);
            this.Controls.Add(this.panel2);
            this.Name = "BrowserControlAuto";
            this.Size = new System.Drawing.Size(879, 396);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        public void LoginToolStripButtonClickHandler(object sender, EventArgs e) {
            try {
                HtmlDocument document = this._browser.Document;
                string str = this.usuario;
                if (this.usuario.Contains("&")) {
                    str = this.usuario.Replace("&", " ");
                }
                document.GetElementById("Ecom_User_ID").SetAttribute("value", str);
                document.GetElementById("Ecom_Password").SetAttribute("value", this.pass);
                document.GetElementById("loginButton2").InvokeMember("click");
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                this.respuesta = string.Concat("LoginToolStripButtonClickHandler->", exception.ToString());
                this.trabajando = false;
                AllForms.m_frmLog.AppendToLog(string.Concat("LoginToolStripButtonClickHandler->", exception.ToString()));
            }
        }

        private void Navigate() {
            this.WebBrowser.Navigate(this.addressTextBox.Text);
        }

        private void ThreadDownload() {
            int length;
            float single;
            int num;
            bool flag;
            string[] fullName;
            try {
                try {
                    this.count = 0;
                    if (this.siDescarga) {
                        string str = Path.DirectorySeparatorChar.ToString();
                        DirectoryInfo directoryInfo = null;
                        if (!this.Recibidas) {
                            directoryInfo = new DirectoryInfo(string.Concat(this.pathBandeja, str, "EMITIDOS", str));
                            if (!directoryInfo.Exists) {
                                directoryInfo.Create();
                            }
                        }
                        else {
                            directoryInfo = new DirectoryInfo(string.Concat(this.pathBandeja, str, "RECIBIDOS", str));
                            if (!directoryInfo.Exists) {
                                directoryInfo.Create();
                            }
                        }
                        IEnumerator<DatosCfdiSat> enumerator = this.lista.GetEnumerator();
                        bool flag1 = true;
                        if (this.soloUUIDs != null) {
                            length = (int)this.soloUUIDs.Length;
                            single = (float)length / 10f;
                            if (length < 10) {
                                flag1 = false;
                            }
                            while (enumerator.MoveNext()) {
                                bool flag2 = false;
                                int num1 = 0;
                                while (num1 < (int)this.soloUUIDs.Length) {
                                    if (!this.soloUUIDs[num1].Equals(enumerator.Current.Folio_Fiscal)) {
                                        num1++;
                                    }
                                    else {
                                        flag2 = true;
                                        break;
                                    }
                                }
                                if (flag2) {
                                    if (enumerator.Current.UrlDescarga != null) {
                                        Thread.Sleep(50);
                                        if (!this.Recibidas) {
                                            fullName = new string[] { directoryInfo.FullName, enumerator.Current.Rfc_Emisor, "_", enumerator.Current.RFC_Receptor, "_", enumerator.Current.Folio_Fiscal, ".xml" };
                                            this.fileNameCurrent = string.Concat(fullName);
                                        }
                                        else {
                                            fullName = new string[] { directoryInfo.FullName, enumerator.Current.RFC_Receptor, "_", enumerator.Current.Rfc_Emisor, "_", enumerator.Current.Folio_Fiscal, ".xml" };
                                            this.fileNameCurrent = string.Concat(fullName);
                                        }
                                        FileInfo fileInfo = this.WebBrowser.DownloadFile(enumerator.Current.UrlDescarga, this.fileNameCurrent);
                                        num = 0;
                                        flag = true;
                                        while (fileInfo.Length == (long)0) {
                                            Thread.Sleep(100);
                                            num++;
                                            if (num == 50) {
                                                flag = false;
                                                break;
                                            }
                                        }
                                        if (flag) {
                                            this.sumaXML++;
                                            this.count++;
                                        }
                                        if (!flag1) {
                                            this.progess = (int)Math.Ceiling((double)this.count / (double)length * 10);
                                        }
                                        else if ((double)((float)this.count % single) < 1) {
                                            this.progess++;
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            length = this.lista.Count;
                            single = (float)length / 10f;
                            if (length < 10) {
                                flag1 = false;
                            }
                            while (enumerator.MoveNext()) {
                                if (enumerator.Current.UrlDescarga != null) {
                                    Thread.Sleep(50);
                                    if (!this.Recibidas) {
                                        fullName = new string[] { directoryInfo.FullName, enumerator.Current.Rfc_Emisor, "_", enumerator.Current.RFC_Receptor, "_", enumerator.Current.Folio_Fiscal, ".xml" };
                                        this.fileNameCurrent = string.Concat(fullName);
                                    }
                                    else {
                                        fullName = new string[] { directoryInfo.FullName, enumerator.Current.RFC_Receptor, "_", enumerator.Current.Rfc_Emisor, "_", enumerator.Current.Folio_Fiscal, ".xml" };
                                        this.fileNameCurrent = string.Concat(fullName);
                                    }
                                    this.contadorDescargas++;
                                    FileInfo fileInfo1 = new FileInfo(this.fileNameCurrent);
                                    if (!fileInfo1.Exists) {
                                        fileInfo1 = this.WebBrowser.DownloadFile(enumerator.Current.UrlDescarga, this.fileNameCurrent);
                                    }
                                    else if (fileInfo1.Length < (long)500) {
                                        fileInfo1 = this.WebBrowser.DownloadFile(enumerator.Current.UrlDescarga, this.fileNameCurrent);
                                    }
                                    num = 0;
                                    flag = true;
                                    while (fileInfo1.Length == (long)0) {
                                        Thread.Sleep(100);
                                        num++;
                                        if (num == 50) {
                                            flag = false;
                                            break;
                                        }
                                    }
                                    if (flag) {
                                        this.sumaXML++;
                                        this.count++;
                                    }
                                    if (!flag1) {
                                        this.progess = (int)Math.Ceiling((double)this.count / (double)length * 10);
                                    }
                                    else if ((double)((float)this.count % single) < 1) {
                                        this.progess++;
                                    }
                                }
                            }
                        }
                        this.progess = 10;
                    }
                }
                catch (Exception exception) {
                    this.respuesta = string.Concat("BrowserControlAuto.ThreadDownload()->", exception.ToString());
                    this.trabajando = false;
                }
            }
            finally {
                this.trabajandoDescarga = false;
            }
        }

        private void ThreadIteraColumnas() {
            DateTime dateTime;
            HtmlElement current;
            try {
                this.progess = 7;
                int num = 0;
                this.progess = 8;
                HtmlDocument document = this._browser.Document;
                HtmlElementCollection elementsByTagName = document.GetElementsByTagName("td");
                HtmlElementCollection htmlElementCollections = document.GetElementsByTagName("img");
                IEnumerator enumerator = elementsByTagName.GetEnumerator();
                IEnumerator enumerator1 = htmlElementCollections.GetEnumerator();
                num = 0;
                DatosCfdiSat datosCfdiSat = new DatosCfdiSat();
                this.lista = new List<DatosCfdiSat>();
                int num1 = 0;
                int num2 = 0;
                string outerHtml = "";
                string str = "";
                DateTime dateTime1 = DateTime.Parse(this.fechaInicial);
                DateTime dateTime2 = DateTime.Parse(this.fechaFinal);
                while (enumerator.MoveNext()) {
                    HtmlElement htmlElement = (HtmlElement)enumerator.Current;
                    try {
                        outerHtml = htmlElement.OuterHtml;
                    }
                    catch (Exception exception) {
                        BrowserControlAuto browserControlAuto = this;
                        browserControlAuto.cfdiMes = string.Concat(browserControlAuto.cfdiMes, "Demaciados Datos\n");
                        break;
                    }
                    if (outerHtml.ToUpper().Contains("WORD-BREAK")) {
                        str = outerHtml;
                        num++;
                        if (num == this.noColumn) {
                            if (!this.Recibidas) {
                                BrowserControlAuto browserControlAuto1 = this;
                                browserControlAuto1.cfdiMes = string.Concat(browserControlAuto1.cfdiMes, "|");
                                this.lista.Add(datosCfdiSat);
                            }
                            else {
                                dateTime = DateTime.Parse(datosCfdiSat.Fecha_Emision);
                                if ((dateTime1.Date > dateTime.Date ? false : dateTime.Date <= dateTime2.Date)) {
                                    this.lista.Add(datosCfdiSat);
                                }
                            }
                            datosCfdiSat = new DatosCfdiSat();
                            BrowserControlAuto browserControlAuto2 = this;
                            browserControlAuto2.cfdiMes = string.Concat(browserControlAuto2.cfdiMes, "\n");
                            num = 1;
                        }
                        string upper = str.ToUpper();
                        if (!upper.Contains("BLOCK;\">")) {
                            num1 = upper.IndexOf("BLOCK\">") + 7;
                            num2 = upper.IndexOf("</SPAN>");
                        }
                        else {
                            num1 = upper.IndexOf("BLOCK;\">") + 8;
                            num2 = upper.IndexOf("</SPAN>");
                        }
                        try {
                            str = str.Substring(num1, num2 - num1);
                        }
                        catch (Exception exception1) {
                            str = upper;
                        }
                        str = str.Replace("\n", "").Replace("&amp;", "&");
                        BrowserControlAuto browserControlAuto3 = this;
                        browserControlAuto3.cfdiMes = string.Concat(browserControlAuto3.cfdiMes, "|", str);
                        switch (num) {
                            case 1: {
                                    datosCfdiSat.Folio_Fiscal = str;
                                    break;
                                }
                            case 2: {
                                    datosCfdiSat.Rfc_Emisor = str;
                                    break;
                                }
                            case 3: {
                                    datosCfdiSat.Nombre_Emisor = str;
                                    break;
                                }
                            case 4: {
                                    datosCfdiSat.RFC_Receptor = str;
                                    break;
                                }
                            case 5: {
                                    datosCfdiSat.Nombre_Receptor = str;
                                    break;
                                }
                            case 6: {
                                    datosCfdiSat.Fecha_Emision = str;
                                    break;
                                }
                            case 7: {
                                    datosCfdiSat.Fecha_Certificacion = str;
                                    break;
                                }
                            case 8: {
                                    datosCfdiSat.Pac_Certifico = str;
                                    break;
                                }
                            case 9: {
                                    datosCfdiSat.Total = str;
                                    break;
                                }
                            case 10: {
                                    datosCfdiSat.Efecto_Comprobante = str;
                                    break;
                                }
                            case 11: {
                                    datosCfdiSat.Estado_Comprobante = str;
                                    break;
                                }
                            case 12: {
                                    datosCfdiSat.Fecha_Cancelacion = str;
                                    break;
                                }
                        }
                        if (num == 11) {
                            if (this.siDescarga) {
                                string outerHtml1 = "";
                                if (!this.Recibidas) {
                                    while (enumerator1.MoveNext()) {
                                        current = (HtmlElement)enumerator1.Current;
                                        outerHtml1 = current.OuterHtml;
                                        if ((!outerHtml1.Contains("BtnDescarga") ? false : outerHtml1.Contains("World-download.png"))) {
                                            if (str.ToUpper().Contains("VIGENTE")) {
                                                try {
                                                    num1 = outerHtml1.IndexOf("RecuperaCfdi");
                                                    num2 = outerHtml1.IndexOf("','Recuperacion");
                                                    outerHtml1 = outerHtml1.Substring(num1, num2 - num1);
                                                    datosCfdiSat.UrlDescarga = string.Concat("https://portalcfdi.facturaelectronica.sat.gob.mx/", outerHtml1);
                                                }
                                                catch (Exception exception2) {
                                                    datosCfdiSat.UrlDescarga = null;
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                                else if (str.ToUpper().Contains("VIGENTE")) {
                                    while (enumerator1.MoveNext()) {
                                        current = (HtmlElement)enumerator1.Current;
                                        outerHtml1 = current.OuterHtml;
                                        if ((!outerHtml1.Contains("BtnDescarga") ? false : outerHtml1.Contains("World-download.png"))) {
                                            try {
                                                num1 = outerHtml1.IndexOf("RecuperaCfdi");
                                                num2 = outerHtml1.IndexOf("','Recuperacion");
                                                outerHtml1 = outerHtml1.Substring(num1, num2 - num1);
                                                datosCfdiSat.UrlDescarga = string.Concat("https://portalcfdi.facturaelectronica.sat.gob.mx/", outerHtml1);
                                            }
                                            catch (Exception exception3) {
                                                datosCfdiSat.UrlDescarga = null;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                this.progess = 9;
                if (datosCfdiSat.Folio_Fiscal != null) {
                    if (!this.Recibidas) {
                        this.lista.Add(datosCfdiSat);
                    }
                    else {
                        dateTime = DateTime.Parse(datosCfdiSat.Fecha_Emision);
                        if ((dateTime1.Date > dateTime.Date ? false : dateTime.Date <= dateTime2.Date)) {
                            this.lista.Add(datosCfdiSat);
                        }
                    }
                }
                if (this.lista.Count <= 0) {
                    this.progess = 10;
                    Thread.Sleep(500);
                }
                else {
                    BrowserControlAuto browserControlAuto4 = this;
                    browserControlAuto4.cfdiMes = string.Concat(browserControlAuto4.cfdiMes, "\n");
                    this.respuesta = this.cfdiMes;
                    this.progess = 10;
                    this.comienzaDescargaMasiva();
                }
            }
            catch (Exception exception5) {
                Exception exception4 = exception5;
                StackTrace stackTrace = new StackTrace(exception4, true);
                object[] objArray = new object[] { "BrowserControlAuto.ThreadIteraColumnas()->", exception4.ToString(), "<->", stackTrace.GetFrame(0).GetFileLineNumber(), "<-cfdiMes->", this.cfdiMes, "<-" };
                this.respuesta = string.Concat(objArray);
                string[] strArrays = new string[] { string.Concat("Exception a las ", DateTime.Now), string.Concat("Message ---", exception4.Message), string.Concat("HelpLink ---", exception4.HelpLink), string.Concat("Source ---", exception4.Source), string.Concat("StackTrace ---", exception4.StackTrace), string.Concat("TargetSite ---", exception4.TargetSite), string.Concat("Exception ---", exception4.ToString()), string.Concat("GetFileLineNumber ---", stackTrace.GetFrame(0).GetFileLineNumber()) };
                File.WriteAllLines("BitacoraDescargaSAT.txt", strArrays);
                this.trabajando = false;
            }
        }

        private void ThreadIteraColumnasNuevo() {
            HtmlElement current;
            try {
                this.progess = 1;
                int num = 0;
                HtmlDocument document = this._browser.Document;
                HtmlElementCollection elementsByTagName = document.GetElementsByTagName("td");
                HtmlElementCollection htmlElementCollections = document.GetElementsByTagName("img");
                IEnumerator enumerator = elementsByTagName.GetEnumerator();
                IEnumerator enumerator1 = htmlElementCollections.GetEnumerator();
                num = 0;
                DatosCfdiSat datosCfdiSat = new DatosCfdiSat();
                this.lista = new List<DatosCfdiSat>();
                int num1 = 0;
                int num2 = 0;
                string outerHtml = "";
                string str = "";
                string str1 = "";
                bool flag = false;
                bool flag1 = false;
                while (enumerator.MoveNext()) {
                    HtmlElement htmlElement = (HtmlElement)enumerator.Current;
                    try {
                        outerHtml = htmlElement.OuterHtml;
                    }
                    catch (Exception exception) {
                        BrowserControlAuto browserControlAuto = this;
                        browserControlAuto.cfdiMes = string.Concat(browserControlAuto.cfdiMes, "Demaciados Datos\n");
                        break;
                    }
                    if (outerHtml.ToUpper().Contains("WORD-BREAK")) {
                        str = outerHtml;
                        num++;
                        if (num == this.noColumn) {
                            if (this.Recibidas) {
                                if (this.listaAll.Find((DatosCfdiSat x) => x.Folio_Fiscal.Equals(datosCfdiSat.Folio_Fiscal)) == null) {
                                    this.lista.Add(datosCfdiSat);
                                    this.listaAll.Add(datosCfdiSat);
                                    flag = true;
                                }
                            }
                            else if (this.listaAll.Find((DatosCfdiSat x) => x.Folio_Fiscal.Equals(datosCfdiSat.Folio_Fiscal)) == null) {
                                str1 = string.Concat(str1, "|");
                                this.lista.Add(datosCfdiSat);
                                this.listaAll.Add(datosCfdiSat);
                                flag = true;
                            }
                            flag1 = true;
                            datosCfdiSat = new DatosCfdiSat();
                            if (flag) {
                                BrowserControlAuto browserControlAuto1 = this;
                                browserControlAuto1.cfdiMes = string.Concat(browserControlAuto1.cfdiMes, str1, "\n");
                            }
                            str1 = "";
                            num = 1;
                            flag = false;
                        }
                        string upper = str.ToUpper();
                        if (!upper.Contains("BLOCK;\">")) {
                            num1 = upper.IndexOf("BLOCK\">") + 7;
                            num2 = upper.IndexOf("</SPAN>");
                        }
                        else {
                            num1 = upper.IndexOf("BLOCK;\">") + 8;
                            num2 = upper.IndexOf("</SPAN>");
                        }
                        try {
                            str = str.Substring(num1, num2 - num1);
                        }
                        catch (Exception exception1) {
                            str = upper;
                        }
                        str = str.Replace("\n", "").Replace("&amp;", "&");
                        str1 = string.Concat(str1, "|", str);
                        switch (num) {
                            case 1: {
                                    datosCfdiSat.Folio_Fiscal = str;
                                    break;
                                }
                            case 2: {
                                    datosCfdiSat.Rfc_Emisor = str;
                                    break;
                                }
                            case 3: {
                                    datosCfdiSat.Nombre_Emisor = str;
                                    break;
                                }
                            case 4: {
                                    datosCfdiSat.RFC_Receptor = str;
                                    break;
                                }
                            case 5: {
                                    datosCfdiSat.Nombre_Receptor = str;
                                    break;
                                }
                            case 6: {
                                    datosCfdiSat.Fecha_Emision = str;
                                    break;
                                }
                            case 7: {
                                    datosCfdiSat.Fecha_Certificacion = str;
                                    break;
                                }
                            case 8: {
                                    datosCfdiSat.Pac_Certifico = str;
                                    break;
                                }
                            case 9: {
                                    datosCfdiSat.Total = str;
                                    break;
                                }
                            case 10: {
                                    datosCfdiSat.Efecto_Comprobante = str;
                                    break;
                                }
                            case 11: {
                                    datosCfdiSat.Estado_Comprobante = str;
                                    break;
                                }
                            case 12: {
                                    datosCfdiSat.Fecha_Cancelacion = str;
                                    break;
                                }
                        }
                        if (num == 11) {
                            if (this.siDescarga) {
                                string outerHtml1 = "";
                                if (!this.Recibidas) {
                                    while (enumerator1.MoveNext()) {
                                        current = (HtmlElement)enumerator1.Current;
                                        outerHtml1 = current.OuterHtml;
                                        if ((!outerHtml1.Contains("BtnDescarga") ? false : outerHtml1.Contains("World-download.png"))) {
                                            if (str.ToUpper().Contains("VIGENTE")) {
                                                try {
                                                    num1 = outerHtml1.IndexOf("RecuperaCfdi");
                                                    num2 = outerHtml1.IndexOf("','Recuperacion");
                                                    outerHtml1 = outerHtml1.Substring(num1, num2 - num1);
                                                    datosCfdiSat.UrlDescarga = string.Concat("https://portalcfdi.facturaelectronica.sat.gob.mx/", outerHtml1);
                                                }
                                                catch (Exception exception2) {
                                                    datosCfdiSat.UrlDescarga = null;
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                                else if (str.ToUpper().Contains("VIGENTE")) {
                                    while (enumerator1.MoveNext()) {
                                        current = (HtmlElement)enumerator1.Current;
                                        outerHtml1 = current.OuterHtml;
                                        if ((!outerHtml1.Contains("BtnDescarga") ? false : outerHtml1.Contains("World-download.png"))) {
                                            try {
                                                num1 = outerHtml1.IndexOf("RecuperaCfdi");
                                                num2 = outerHtml1.IndexOf("','Recuperacion");
                                                outerHtml1 = outerHtml1.Substring(num1, num2 - num1);
                                                datosCfdiSat.UrlDescarga = string.Concat("https://portalcfdi.facturaelectronica.sat.gob.mx/", outerHtml1);
                                            }
                                            catch (Exception exception3) {
                                                datosCfdiSat.UrlDescarga = null;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                flag = false;
                if (datosCfdiSat.Folio_Fiscal != null) {
                    if (this.Recibidas) {
                        if (this.listaAll.Find((DatosCfdiSat x) => x.Folio_Fiscal.Equals(datosCfdiSat.Folio_Fiscal)) == null) {
                            this.listaAll.Add(datosCfdiSat);
                            this.lista.Add(datosCfdiSat);
                            flag = true;
                        }
                    }
                    else if (this.listaAll.Find((DatosCfdiSat x) => x.Folio_Fiscal.Equals(datosCfdiSat.Folio_Fiscal)) == null) {
                        this.listaAll.Add(datosCfdiSat);
                        this.lista.Add(datosCfdiSat);
                        flag = true;
                    }
                }
                if (this.lista.Count <= 0) {
                    this.progess = 10;
                    Thread.Sleep(500);
                    if (!flag1) {
                        MessageBox.Show(this._browser.Parent, "Primero debe indicar la fecha de búsqueda, después oprimir \"Buscar CFDI\" y cuando encuentre resultados ya puede oprimir este boton \"Descargar\".", "Mensaje:");
                    }
                    else {
                        MessageBox.Show(this._browser.Parent, "Los CFDI´s que tiene esta búsqueda ya los tiene en su lista. Puede seguir buscando más y oprimir Descargar o puede Cerrar Sesión para continuar.", "Finalizó:");
                    }
                }
                else {
                    if (flag) {
                        BrowserControlAuto browserControlAuto2 = this;
                        browserControlAuto2.cfdiMes = string.Concat(browserControlAuto2.cfdiMes, str1, "\n");
                    }
                    this.respuesta = this.cfdiMes;
                    this.progess = 2;
                    this.comienzaDescargaMasiva();
                }
            }
            catch (Exception exception5) {
                Exception exception4 = exception5;
                StackTrace stackTrace = new StackTrace(exception4, true);
                object[] objArray = new object[] { "BrowserControlAuto.ThreadIteraColumnas()->", exception4.ToString(), "<->", stackTrace.GetFrame(0).GetFileLineNumber(), "<-cfdiMes->", this.cfdiMes, "<-" };
                this.respuesta = string.Concat(objArray);
                string[] strArrays = new string[] { string.Concat("Exception a las ", DateTime.Now), string.Concat("Message ---", exception4.Message), string.Concat("HelpLink ---", exception4.HelpLink), string.Concat("Source ---", exception4.Source), string.Concat("StackTrace ---", exception4.StackTrace), string.Concat("TargetSite ---", exception4.TargetSite), string.Concat("Exception ---", exception4.ToString()), string.Concat("GetFileLineNumber ---", stackTrace.GetFrame(0).GetFileLineNumber()) };
                File.WriteAllLines("BitacoraDescargaSAT.txt", strArrays);
                this.trabajando = false;
            }
        }

        private void UpdateAddressBox() {
            string str = this.WebBrowser.Document.Url.ToString();
            if (!str.Equals(this.addressTextBox.Text, StringComparison.InvariantCultureIgnoreCase)) {
                this.addressTextBox.Text = str;
            }
        }

        private void Window_Error(object sender, HtmlElementErrorEventArgs e) {
            e.Handled = true;
        }
    }
}
