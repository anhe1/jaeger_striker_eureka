﻿using System;

namespace Jaeger.UI.LibAdminCfdi {
    public class TextChangedEventArgs : EventArgs {
        private string _text;

        public string Text {
            get {
                return this._text;
            }
        }

        public TextChangedEventArgs(string text) {
            this._text = text;
        }
    }
}
