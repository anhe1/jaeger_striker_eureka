﻿using System.Drawing;
using System.Windows.Forms;

namespace Jaeger.UI.LibAdminCfdi {
    public class FileDownloadStatusCtl : TableLayoutPanel {
        public Label lblFrom = new Label();

        public Label lblTo = new Label();

        public Label lblStatus = new Label();

        public Label lblBytesReceived = new Label();

        public Label lblBytesTotal = new Label();

        public ProgressBar pbProgress = new ProgressBar();

        public Button btnCancel = new Button();

        public int RowIndex = 0;

        public FileDownloadStatusCtl() {
            this.InitControls();
            base.ColumnCount = 7;
            base.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20f));
            base.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20f));
            base.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 15f));
            base.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10f));
            base.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10f));
            base.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 15f));
            base.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10f));
            base.Controls.Add(this.lblFrom, 0, 0);
            base.Controls.Add(this.lblTo, 1, 0);
            base.Controls.Add(this.lblStatus, 2, 0);
            base.Controls.Add(this.lblBytesReceived, 3, 0);
            base.Controls.Add(this.lblBytesTotal, 4, 0);
            base.Controls.Add(this.pbProgress, 5, 0);
            base.Controls.Add(this.btnCancel, 6, 0);
            base.Name = "tableLayoutPanel1";
            base.Size = new Size(700, 28);
            base.RowCount = 1;
            base.RowStyles.Add(new RowStyle(SizeType.Percent, 100f));
        }

        private void InitControls() {
            this.lblFrom.AutoEllipsis = true;
            this.lblFrom.AutoSize = true;
            this.lblFrom.Dock = DockStyle.Fill;
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.TabIndex = 0;
            this.lblFrom.Text = "Downloading From";
            this.lblFrom.TextAlign = ContentAlignment.MiddleLeft;
            this.lblTo.AutoEllipsis = true;
            this.lblTo.AutoSize = true;
            this.lblTo.Dock = DockStyle.Fill;
            this.lblTo.Name = "lblTo";
            this.lblTo.TabIndex = 1;
            this.lblTo.Text = "Downloading To";
            this.lblTo.TextAlign = ContentAlignment.MiddleLeft;
            this.lblStatus.AutoEllipsis = true;
            this.lblStatus.AutoSize = true;
            this.lblStatus.Dock = DockStyle.Fill;
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Text = "Status";
            this.lblStatus.TextAlign = ContentAlignment.MiddleLeft;
            this.lblBytesReceived.AutoEllipsis = true;
            this.lblBytesReceived.AutoSize = true;
            this.lblBytesReceived.Dock = DockStyle.Fill;
            this.lblBytesReceived.Name = "lblBytesReceived";
            this.lblBytesReceived.Text = "Received";
            this.lblBytesReceived.TextAlign = ContentAlignment.MiddleRight;
            this.lblBytesTotal.AutoEllipsis = true;
            this.lblBytesTotal.AutoSize = true;
            this.lblBytesTotal.Dock = DockStyle.Fill;
            this.lblBytesTotal.Name = "lblBytesTotal";
            this.lblBytesTotal.Text = "Total";
            this.lblBytesTotal.TextAlign = ContentAlignment.MiddleRight;
            this.pbProgress.Dock = DockStyle.Fill;
            this.pbProgress.Name = "pbProgress";
            this.btnCancel.AutoSize = true;
            this.btnCancel.Dock = DockStyle.Fill;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Text = "Cancel";
        }
    }
}
