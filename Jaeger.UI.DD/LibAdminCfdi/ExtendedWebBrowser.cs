﻿using System;
using System.IO;
using System.Runtime.InteropServices;

using System.Security.Permissions;
using System.Windows.Forms;

namespace Jaeger.UI.LibAdminCfdi {
    public partial class ExtendedWebBrowser : WebBrowser {
        private UnsafeNativeMethods.IWebBrowser2 axIWebBrowser2;

        private AxHost.ConnectionPointCookie cookie;

        private ExtendedWebBrowser.WebBrowserExtendedEvents events;

        public object Application {
            get {
                return this.axIWebBrowser2.Application;
            }
        }

        public ExtendedWebBrowser() {
        }

        [PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
        protected override void AttachInterfaces(object nativeActiveXObject) {
            this.axIWebBrowser2 = (UnsafeNativeMethods.IWebBrowser2)nativeActiveXObject;
            base.AttachInterfaces(nativeActiveXObject);
        }

        [PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
        protected override void CreateSink() {
            base.CreateSink();
            this.events = new ExtendedWebBrowser.WebBrowserExtendedEvents(this);
            //this.cookie = new AxHost.ConnectionPointCookie(base.ActiveXInstance, (object)this.events, typeof(UnsafeNativeMethods.DWebBrowserEvents2));
        }

        [PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
        protected override void DetachInterfaces() {
            this.axIWebBrowser2 = null;
            base.DetachInterfaces();
        }

        [PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
        protected override void DetachSink() {
            if (null != this.cookie) {
                this.cookie.Disconnect();
                this.cookie = null;
            }
        }

        public FileInfo DownloadFile(string url, string destinationFullPathWithName) {
            ExtendedWebBrowser.URLDownloadToFile(null, url, destinationFullPathWithName, 0, 0);
            return new FileInfo(destinationFullPathWithName);
        }

        protected virtual void OnDownloadComplete(EventArgs e) {
            if (this.DownloadComplete != null) {
                this.DownloadComplete(this, e);
            }
        }

        protected void OnDownloading(EventArgs e) {
            if (this.Downloading != null) {
                this.Downloading(this, e);
            }
        }

        protected void OnQuit() {
            EventHandler eventHandler = this.Quit;
            if (null != eventHandler) {
                eventHandler(this, EventArgs.Empty);
            }
        }

        protected void OnStartNavigate(BrowserExtendedNavigatingEventArgs e) {
            if (e == null) {
                throw new ArgumentNullException("e");
            }
            if (this.StartNavigate != null) {
                this.StartNavigate(this, e);
            }
        }

        protected void OnStartNewWindow(BrowserExtendedNavigatingEventArgs e) {
            if (e == null) {
                throw new ArgumentNullException("e");
            }
            if (this.StartNewWindow != null) {
                this.StartNewWindow(this, e);
            }
        }

        [DllImport("urlmon.dll", CharSet = CharSet.Auto, ExactSpelling = false, SetLastError = true)]
        private static extern int URLDownloadToFile(object callerPointer, string url, string filePathWithName, int reserved, int callBack);

        [PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
        protected override void WndProc(ref Message m) {
            if (m.Msg == 528) {
                if ((m.WParam.ToInt32() & 65535) == 2) {
                    this.OnQuit();
                }
            }
            base.WndProc(ref m);
        }

        public void Zoom(int factor) {
            object obj = factor;
            try {
                //this.axIWebBrowser2.ExecWB(NativeMethods.OLECMDID.OLECMDID_OPTICAL_ZOOM, NativeMethods.OLECMDEXECOPT.OLECMDEXECOPT_DONTPROMPTUSER, ref obj, IntPtr.Zero);
            }
            catch (Exception exception) {
                throw;
            }
        }

        public event EventHandler DownloadComplete;

        public event EventHandler Downloading;

        public event EventHandler Quit;

        public event EventHandler<BrowserExtendedNavigatingEventArgs> StartNavigate;

        public event EventHandler<BrowserExtendedNavigatingEventArgs> StartNewWindow;

        private class WebBrowserExtendedEvents : UnsafeNativeMethods.DWebBrowserEvents2 {
            private ExtendedWebBrowser _Browser;

            public WebBrowserExtendedEvents() {
            }

            public WebBrowserExtendedEvents(ExtendedWebBrowser browser) {
                this._Browser = browser;
            }

            public void BeforeNavigate2(object pDisp, ref object URL, ref object flags, ref object targetFrameName, ref object postData, ref object headers, ref bool cancel) {
                Uri uri = new Uri(URL.ToString());
                string str = null;
                if (targetFrameName != null) {
                    str = targetFrameName.ToString();
                }
                BrowserExtendedNavigatingEventArgs browserExtendedNavigatingEventArg = new BrowserExtendedNavigatingEventArgs(pDisp, uri, str, UrlContext.None);
                this._Browser.OnStartNavigate(browserExtendedNavigatingEventArg);
                cancel = browserExtendedNavigatingEventArg.Cancel;
                pDisp = browserExtendedNavigatingEventArg.AutomationObject;
            }

            public void ClientToHostWindow(ref int CX, ref int CY) {
            }

            public void CommandStateChange(int Command, bool Enable) {
            }

            public void DocumentComplete(object pDisp, ref object URL) {
            }

            public void DownloadBegin() {
                this._Browser.OnDownloading(EventArgs.Empty);
            }

            public void DownloadComplete() {
                this._Browser.OnDownloadComplete(EventArgs.Empty);
            }

            public void FileDownload(ref bool cancel) {
            }

            public void NavigateComplete2(object pDisp, ref object URL) {
            }

            public void NavigateError(object pDisp, ref object URL, ref object frame, ref object statusCode, ref bool cancel) {
            }

            public void NewWindow2(ref object pDisp, ref bool cancel) {
                BrowserExtendedNavigatingEventArgs browserExtendedNavigatingEventArg = new BrowserExtendedNavigatingEventArgs(pDisp, null, null, UrlContext.None);
                this._Browser.OnStartNewWindow(browserExtendedNavigatingEventArg);
                cancel = browserExtendedNavigatingEventArg.Cancel;
                pDisp = browserExtendedNavigatingEventArg.AutomationObject;
            }

            public void NewWindow3(ref object ppDisp, ref bool Cancel, uint dwFlags, string bstrUrlContext, string bstrUrl) {
                BrowserExtendedNavigatingEventArgs browserExtendedNavigatingEventArg = new BrowserExtendedNavigatingEventArgs(ppDisp, new Uri(bstrUrl), null, (UrlContext)dwFlags);
                this._Browser.OnStartNewWindow(browserExtendedNavigatingEventArg);
                Cancel = browserExtendedNavigatingEventArg.Cancel;
                ppDisp = browserExtendedNavigatingEventArg.AutomationObject;
            }

            public void OnFullScreen(bool fullScreen) {
            }

            public void OnMenuBar(bool menuBar) {
            }

            public void OnQuit() {
            }

            public void OnStatusBar(bool statusBar) {
            }

            public void OnTheaterMode(bool theaterMode) {
            }

            public void OnToolBar(bool toolBar) {
            }

            public void OnVisible(bool visible) {
            }

            public void PrintTemplateInstantiation(object pDisp) {
            }

            public void PrintTemplateTeardown(object pDisp) {
            }

            public void PrivacyImpactedStateChange(bool bImpacted) {
            }

            public void ProgressChange(int progress, int progressMax) {
            }

            public void PropertyChange(string szProperty) {
            }

            public void SetSecureLockIcon(int secureLockIcon) {
            }

            public void StatusTextChange(string text) {
            }

            public void TitleChange(string text) {
            }

            public void UpdatePageStatus(object pDisp, ref object nPage, ref object fDone) {
            }

            [DispId(263)]
            public void WindowClosing(bool isChildWindow, ref bool cancel) {
            }

            public void WindowSetHeight(int height) {
            }

            public void WindowSetLeft(int left) {
            }

            public void WindowSetResizable(bool resizable) {
            }

            public void WindowSetTop(int top) {
            }

            public void WindowSetWidth(int width) {
            }
        }
    }
}
