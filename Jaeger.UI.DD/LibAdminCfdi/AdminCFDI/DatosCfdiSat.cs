﻿namespace Jaeger.UI.LibAdminCfdi.AdminCFDI {
    internal class DatosCfdiSat {
        private string folio_Fiscal;

        private string rfc_Emisor;

        private string nombre_Emisor;

        private string rfc_Receptor;

        private string nombre_Receptor;

        private string fecha_Emision;

        private string fecha_Certificacion;

        private string pac_Certifico;

        private string total;

        private string efecto_Comprobante;

        private string estado_Comprobante;

        private string fecha_Cancelacion;

        private string urlDescarga;

        public string Efecto_Comprobante {
            get {
                return this.efecto_Comprobante;
            }
            set {
                this.efecto_Comprobante = value;
            }
        }

        public string Estado_Comprobante {
            get {
                return this.estado_Comprobante;
            }
            set {
                this.estado_Comprobante = value;
            }
        }

        public string Fecha_Cancelacion {
            get {
                return this.fecha_Cancelacion;
            }
            set {
                this.fecha_Cancelacion = value;
            }
        }

        public string Fecha_Certificacion {
            get {
                return this.fecha_Certificacion;
            }
            set {
                this.fecha_Certificacion = value;
            }
        }

        public string Fecha_Emision {
            get {
                return this.fecha_Emision;
            }
            set {
                this.fecha_Emision = value;
            }
        }

        public string Folio_Fiscal {
            get {
                return this.folio_Fiscal;
            }
            set {
                this.folio_Fiscal = value;
            }
        }

        public string Nombre_Emisor {
            get {
                return this.nombre_Emisor;
            }
            set {
                this.nombre_Emisor = value;
            }
        }

        public string Nombre_Receptor {
            get {
                return this.nombre_Receptor;
            }
            set {
                this.nombre_Receptor = value;
            }
        }

        public string Pac_Certifico {
            get {
                return this.pac_Certifico;
            }
            set {
                this.pac_Certifico = value;
            }
        }

        public string Rfc_Emisor {
            get {
                return this.rfc_Emisor;
            }
            set {
                this.rfc_Emisor = value;
            }
        }

        public string RFC_Receptor {
            get {
                return this.rfc_Receptor;
            }
            set {
                this.rfc_Receptor = value;
            }
        }

        public string Total {
            get {
                return this.total;
            }
            set {
                this.total = value;
            }
        }

        public string UrlDescarga {
            get {
                return this.urlDescarga;
            }
            set {
                this.urlDescarga = value;
            }
        }

        public DatosCfdiSat() {
        }
    }
}
