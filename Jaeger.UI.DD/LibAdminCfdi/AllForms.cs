﻿using Jaeger.UI.LibAdminCfdi.AdminCFDI;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Jaeger.UI.LibAdminCfdi {
    public sealed class AllForms {
        public const string COOKIE = "Cookie:";

        public const string VISITED = "Visited:";

        public const string DLG_HTMLS_FILTER = "Html files (*.html)|*.html|Htm files (*.htm)|*.htm|Text files (*.txt)|*.txt|All files (*.*)|*.*";

        public const string DLG_XMLS_FILTER = "XML files (*.xml)|*.xml";

        public const string DLG_TEXTFILES_FILTER = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

        public const string DLG_IMAGES_FILTER = "Bmp files (*.bmp)|*.bmp|Gif files (*.gif)|*.gif|Jpeg files (*.Jpeg)|*.Jpeg|Png files (*.png)|*.png|Wmf files (*.wmf)|*.wmf|Tiff files (*.tiff)|*.tiff|Emf files (*.emf)|*.emf";

        public static frmLog m_frmLog;

        public static ImageList m_imgListMain;

        public static SaveFileDialog m_dlgSave;

        public static OpenFileDialog m_dlgOpen;

        static AllForms() {
            AllForms.m_frmLog = new frmLog();
            AllForms.m_imgListMain = new ImageList();
            AllForms.m_dlgSave = new SaveFileDialog();
            AllForms.m_dlgOpen = new OpenFileDialog();
        }

        public AllForms() {
        }

        public static bool AskForConfirmation(string Msg, IWin32Window Win) {
            return (MessageBox.Show(Win, Msg, "Confirmation Requested", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes ? true : false);
        }

        public static Icon BitmapToIcon(Image orgImage) {
            Icon icon;
            Icon icon1 = null;
            if (orgImage != null) {
                Bitmap bitmap = new Bitmap(orgImage);
                icon1 = Icon.FromHandle(bitmap.GetHicon());
                bitmap.Dispose();
                icon = icon1;
            }
            else {
                icon = icon1;
            }
            return icon;
        }

        public static Icon BitmapToIcon(int orgImage) {
            Icon icon = null;
            if (AllForms.m_imgListMain.Images.Count > 0) {
                Bitmap bitmap = new Bitmap(AllForms.m_imgListMain.Images[orgImage]);
                icon = Icon.FromHandle(bitmap.GetHicon());
                bitmap.Dispose();
            }
            return icon;
        }

        public static void LoadWebColors(ComboBox combo) {
            Array values = Enum.GetValues(typeof(KnownColor));
            combo.Items.Add(Color.Empty);
            foreach (KnownColor value in values) {
                Color color = Color.FromKnownColor(value);
                if ((color.IsSystemColor ? false : color.A > 0)) {
                    combo.Items.Add(color);
                }
            }
            combo.SelectedIndex = 0;
        }

        public static string SetupCookieCachePattern(string pattern, string replace) {
            string host = pattern;
            if (host.Length > 0) {
                host = (new Uri(host)).Host;
                host = host.Replace(".", "\\.");
                host = string.Concat(replace, ".*", host);
            }
            return host;
        }

        public static DialogResult ShowStaticOpenDialog(IWin32Window Win, string filter, string title, string initialdir, bool showreadonly) {
            AllForms.m_dlgOpen.Filter = filter;
            if (!string.IsNullOrEmpty(title)) {
                AllForms.m_dlgOpen.Title = title;
            }
            else {
                AllForms.m_dlgOpen.Title = "Open File";
            }
            if (!string.IsNullOrEmpty(initialdir)) {
                AllForms.m_dlgOpen.InitialDirectory = initialdir;
            }
            else {
                AllForms.m_dlgOpen.InitialDirectory = Environment.CurrentDirectory;
            }
            AllForms.m_dlgOpen.ShowReadOnly = showreadonly;
            return AllForms.m_dlgOpen.ShowDialog(Win);
        }

        public static DialogResult ShowStaticSaveDialog(IWin32Window Win, string defaultext, string filename, string filter, string title, string initialdir) {
            if (!string.IsNullOrEmpty(defaultext)) {
                AllForms.m_dlgSave.DefaultExt = defaultext;
            }
            else {
                AllForms.m_dlgSave.DefaultExt = "txt";
            }
            if (!string.IsNullOrEmpty(filename)) {
                AllForms.m_dlgSave.FileName = filename;
            }
            else {
                AllForms.m_dlgSave.FileName = "FileName";
            }
            if (!string.IsNullOrEmpty(filter)) {
                AllForms.m_dlgSave.Filter = filter;
            }
            else {
                AllForms.m_dlgSave.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            }
            if (!string.IsNullOrEmpty(title)) {
                AllForms.m_dlgSave.Title = title;
            }
            else {
                AllForms.m_dlgSave.Title = "Save File";
            }
            if (!string.IsNullOrEmpty(initialdir)) {
                AllForms.m_dlgSave.InitialDirectory = initialdir;
            }
            else {
                AllForms.m_dlgSave.InitialDirectory = Environment.CurrentDirectory;
            }
            return AllForms.m_dlgSave.ShowDialog(Win);
        }

        public static DialogResult ShowStaticSaveDialogForHTML(IWin32Window Win) {
            DialogResult dialogResult = AllForms.ShowStaticSaveDialog(Win, string.Empty, string.Empty, "Html files (*.html)|*.html|Htm files (*.htm)|*.htm|Text files (*.txt)|*.txt|All files (*.*)|*.*", string.Empty, string.Empty);
            return dialogResult;
        }

        public static DialogResult ShowStaticSaveDialogForImage(IWin32Window Win) {
            DialogResult dialogResult = AllForms.ShowStaticSaveDialog(Win, "bmp", "ImageFileName", "Bmp files (*.bmp)|*.bmp|Gif files (*.gif)|*.gif|Jpeg files (*.Jpeg)|*.Jpeg|Png files (*.png)|*.png|Wmf files (*.wmf)|*.wmf|Tiff files (*.tiff)|*.tiff|Emf files (*.emf)|*.emf", "Save Image", string.Empty);
            return dialogResult;
        }

        public static DialogResult ShowStaticSaveDialogForText(IWin32Window Win) {
            DialogResult dialogResult = AllForms.ShowStaticSaveDialog(Win, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
            return dialogResult;
        }

        public static DialogResult ShowStaticSaveDialogForXML(IWin32Window Win) {
            DialogResult dialogResult = AllForms.ShowStaticSaveDialog(Win, "xml", "XMLFileName", "XML files (*.xml)|*.xml", "Save XML", string.Empty);
            return dialogResult;
        }
    }
}
