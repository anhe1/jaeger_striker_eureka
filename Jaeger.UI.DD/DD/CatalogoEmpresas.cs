﻿using Jaeger.UI.LibUtilidades.ObjectsData;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Jaeger.UI.DD {
    partial class CatalogoEmpresas : Form {
        private DataEmpresas[] listaEmp;

        private DataEmpresas empSel;

        private IContainer components = null;

        private TreeView treeView1;

        public DataEmpresas EmpSel {
            get {
                return this.empSel;
            }
            set {
                this.empSel = value;
            }
        }

        public DataEmpresas[] ListaEmp {
            get {
                return this.listaEmp;
            }
            set {
                this.listaEmp = value;
            }
        }

        public CatalogoEmpresas() {
            this.InitializeComponent();
        }

        

        public void renderizaLista() {
            Dictionary<string, List<TreeNode>> strs = new Dictionary<string, List<TreeNode>>();
            string str = "";
            for (int i = 0; i < (int)this.listaEmp.Length; i++) {
                DataEmpresas dataEmpresa = this.listaEmp[i];
                List<TreeNode> treeNodes = new List<TreeNode>();
                str = (!dataEmpresa.Selectable ? dataEmpresa.NombreEmpresa : dataEmpresa.GrupoEmpresa);
                if (!strs.TryGetValue(str, out treeNodes)) {
                    strs.Add(str, new List<TreeNode>());
                }
                else {
                    treeNodes.Add(new TreeNode(dataEmpresa.NombreEmpresa));
                }
            }
            foreach (KeyValuePair<string, List<TreeNode>> keyValuePair in strs) {
                List<TreeNode> value = keyValuePair.Value;
                TreeNode treeNode = new TreeNode(keyValuePair.Key, value.ToArray());
                this.treeView1.Nodes.Add(treeNode);
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e) {
            TreeNode selectedNode = this.treeView1.SelectedNode;
            MessageBox.Show(string.Format("Ha seleccionado: {0}", selectedNode.Text));
        }
    }
}


