﻿using System;
using System.Windows.Forms;
using Jaeger.UI.LibUtilidades.ObjectsData;
using Jaeger.UI.WSDL;

namespace Jaeger.UI.DD {
    public partial class Login : Form {
        private DataControl datos = null;

        private int key;

        internal DataControl Datos {
            get {
                return this.datos;
            }
            set {
                this.datos = value;
            }
        }
        public Login() {
            InitializeComponent();
        }

        public Login(int key) {
            this.key = key;
            this.InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e) {

        }

        private void Button1_Click(object sender, EventArgs e) {
            this.Ejecuta();
        }

        private void Ejecuta() {
            if (this.txtUsuario.Text.ToString().Trim().Length != 0) {
                this.datos.Usuario = this.txtUsuario.Text.ToString().Trim();
                if (this.txtPass.Text.ToString().Trim().Length != 0) {
                    try {
                        RespLogin respLogin = (new PrincipalAdminCom()).validaUsuario(this.datos, this.txtPass.Text.ToString().Trim(), true, this.key);
                        if (respLogin.Error.Length == 0) {
                            this.datos.NombreUsuario = respLogin.NombreUsuario;
                            base.Dispose();
                            base.Close();
                        } else {
                            MessageBox.Show(respLogin.Error, "Error de Inicio de Session:");
                        }
                    } catch (Exception exception1) {
                        Exception exception = exception1;
                        Console.WriteLine(string.Concat("Error: ", exception.Message));
                        Console.WriteLine(string.Concat("StackTrace: ", exception.StackTrace));
                        MessageBox.Show(string.Concat("Sucedió un error al conectarse al servidor, verifique que este encendido el servidor y el servicio de Apache Tomcat 8, detalle: ", exception.Message), "Datos Incorrectos:");
                    }
                } else {
                    MessageBox.Show("Es necesario indicar su Contraseña", "Faltan Campos:");
                }
            } else {
                MessageBox.Show("Es necesario el Usuario", "Faltan Campos:");
            }
        }

        private void TxtPass_TextChanged(object sender, KeyEventArgs e) {
            if ((e.KeyCode == Keys.F9 ? true : e.KeyCode == Keys.Return)) {
                this.Ejecuta();
            }
            e.Handled = true;
        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e) {
            Environment.Exit(0);
        }
    }
}