﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Jaeger.UI.DD {
    partial class Principal {

        protected override void Dispose(bool disposing) {
            if ((!disposing ? false : this.components != null)) {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent() {
            this.cboxEmpresas = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radBtnEmitidos = new System.Windows.Forms.RadioButton();
            this.lblRfc = new System.Windows.Forms.Label();
            this.txtPassSAT = new System.Windows.Forms.TextBox();
            this.btnDescargaSat = new System.Windows.Forms.Button();
            this.lblNombreUsuario = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.forzarDescargasConDLLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDownHilos = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHilos)).BeginInit();
            this.SuspendLayout();
            // 
            // cboxEmpresas
            // 
            this.cboxEmpresas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxEmpresas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxEmpresas.FormattingEnabled = true;
            this.cboxEmpresas.Location = new System.Drawing.Point(16, 129);
            this.cboxEmpresas.Name = "cboxEmpresas";
            this.cboxEmpresas.Size = new System.Drawing.Size(628, 25);
            this.cboxEmpresas.TabIndex = 1;
            this.cboxEmpresas.SelectedIndexChanged += new System.EventHandler(this.cboxEmpresas_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(262, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Seleccione la Empresa de Trabajo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(199, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Contraseña del Portal del SAT";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "R.F.C.:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radBtnEmitidos);
            this.groupBox1.Controls.Add(this.lblRfc);
            this.groupBox1.Controls.Add(this.txtPassSAT);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnDescargaSat);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(16, 173);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(628, 137);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos para Descarga Masiva";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(383, 33);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(88, 21);
            this.radioButton2.TabIndex = 10;
            this.radioButton2.Text = "Recibidos";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radBtnEmitidos
            // 
            this.radBtnEmitidos.AutoSize = true;
            this.radBtnEmitidos.Checked = true;
            this.radBtnEmitidos.Location = new System.Drawing.Point(287, 33);
            this.radBtnEmitidos.Name = "radBtnEmitidos";
            this.radBtnEmitidos.Size = new System.Drawing.Size(79, 21);
            this.radBtnEmitidos.TabIndex = 9;
            this.radBtnEmitidos.TabStop = true;
            this.radBtnEmitidos.Text = "Emitidos";
            this.radBtnEmitidos.UseVisualStyleBackColor = true;
            // 
            // lblRfc
            // 
            this.lblRfc.AutoSize = true;
            this.lblRfc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRfc.Location = new System.Drawing.Point(65, 32);
            this.lblRfc.Name = "lblRfc";
            this.lblRfc.Size = new System.Drawing.Size(168, 17);
            this.lblRfc.TabIndex = 8;
            this.lblRfc.Text = "____________________";
            this.lblRfc.UseMnemonic = false;
            // 
            // txtPassSAT
            // 
            this.txtPassSAT.Location = new System.Drawing.Point(11, 93);
            this.txtPassSAT.Name = "txtPassSAT";
            this.txtPassSAT.Size = new System.Drawing.Size(196, 23);
            this.txtPassSAT.TabIndex = 7;
            this.txtPassSAT.UseSystemPasswordChar = true;
            // 
            // btnDescargaSat
            // 
            this.btnDescargaSat.BackColor = System.Drawing.SystemColors.Control;
            this.btnDescargaSat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDescargaSat.ForeColor = System.Drawing.Color.Black;
            this.btnDescargaSat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDescargaSat.Location = new System.Drawing.Point(301, 67);
            this.btnDescargaSat.Name = "btnDescargaSat";
            this.btnDescargaSat.Size = new System.Drawing.Size(170, 49);
            this.btnDescargaSat.TabIndex = 4;
            this.btnDescargaSat.Text = "Descarga del SAT";
            this.btnDescargaSat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDescargaSat.UseVisualStyleBackColor = false;
            this.btnDescargaSat.Click += new System.EventHandler(this.btnDescargaSat_Click);
            // 
            // lblNombreUsuario
            // 
            this.lblNombreUsuario.AutoSize = true;
            this.lblNombreUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreUsuario.Location = new System.Drawing.Point(153, 31);
            this.lblNombreUsuario.Name = "lblNombreUsuario";
            this.lblNombreUsuario.Size = new System.Drawing.Size(74, 17);
            this.lblNombreUsuario.TabIndex = 9;
            this.lblNombreUsuario.Text = "Usuario: ";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.ayudaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(659, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(60, 20);
            this.toolStripMenuItem1.Text = "Archivo";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(96, 22);
            this.toolStripMenuItem3.Text = "Salir";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.forzarDescargasConDLLToolStripMenuItem});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem2.Text = "Utilerías";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(208, 22);
            this.toolStripMenuItem4.Text = "Importación Masiva";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(208, 22);
            this.toolStripMenuItem5.Text = "Crear Acceso a DD";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // forzarDescargasConDLLToolStripMenuItem
            // 
            this.forzarDescargasConDLLToolStripMenuItem.Name = "forzarDescargasConDLLToolStripMenuItem";
            this.forzarDescargasConDLLToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.forzarDescargasConDLLToolStripMenuItem.Text = "Forzar Descargas con DLL";
            this.forzarDescargasConDLLToolStripMenuItem.Click += new System.EventHandler(this.forzarDescargasConDLLToolStripMenuItem_Click);
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.acercaDeToolStripMenuItem});
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            // 
            // acercaDeToolStripMenuItem
            // 
            this.acercaDeToolStripMenuItem.Name = "acercaDeToolStripMenuItem";
            this.acercaDeToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.acercaDeToolStripMenuItem.Text = "Acerca de";
            this.acercaDeToolStripMenuItem.Click += new System.EventHandler(this.acercaDeToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(480, 31);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(164, 72);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "Nombre de Usuario:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 17);
            this.label5.TabIndex = 15;
            this.label5.Text = "Velocidad de Carga:";
            // 
            // numericUpDownHilos
            // 
            this.numericUpDownHilos.Location = new System.Drawing.Point(152, 59);
            this.numericUpDownHilos.Name = "numericUpDownHilos";
            this.numericUpDownHilos.Size = new System.Drawing.Size(38, 20);
            this.numericUpDownHilos.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(197, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 17);
            this.label6.TabIndex = 17;
            this.label6.Text = "envios simultaneos.";
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 320);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numericUpDownHilos);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblNombreUsuario);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboxEmpresas);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Módulo de Descargas de Documentos Digitales";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHilos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private IContainer components = null;

        private ComboBox cboxEmpresas;

        private Label label1;

        private Button btnDescargaSat;

        private Label label2;

        private Label label3;

        private GroupBox groupBox1;

        private Label lblRfc;

        private TextBox txtPassSAT;

        private RadioButton radioButton2;

        private RadioButton radBtnEmitidos;

        private Label lblNombreUsuario;

        private MenuStrip menuStrip1;

        private ToolStripMenuItem toolStripMenuItem1;

        private ToolStripMenuItem toolStripMenuItem3;

        private ToolStripMenuItem toolStripMenuItem2;

        private ToolStripMenuItem toolStripMenuItem4;

        private ToolStripMenuItem toolStripMenuItem5;

        private PictureBox pictureBox1;

        private Label label4;

        private Label label5;

        private NumericUpDown numericUpDownHilos;

        private Label label6;

        private ToolStripMenuItem ayudaToolStripMenuItem;

        private ToolStripMenuItem acercaDeToolStripMenuItem;

        private ToolStripMenuItem forzarDescargasConDLLToolStripMenuItem;
    }
}
