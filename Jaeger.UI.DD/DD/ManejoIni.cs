﻿using Jaeger.UI.LibUtilidades.ObjectsData;
using System;
using System.IO;

namespace Jaeger.UI.DD {
    internal class ManejoIni {
        private string nombreIni = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "DDW.ini");

        public ManejoIni() {
        }

        public void crearIni(DataControl datos) {
            //IniFile iniFile = new IniFile(new IniOptions());
            //IniSection iniSection = new IniSection(iniFile, "MADMILKMAN_INI_FILE_GLOBAL_SECTION");
            //iniFile.Sections.Add(iniSection);
            //iniSection.Keys.Add(new IniKey(iniFile, "Host", datos.Host));
            //iniSection.Keys.Add(new IniKey(iniFile, "Puerto", datos.Puerto));
            //iniSection.Keys.Add(new IniKey(iniFile, "PathDescarga", datos.PathDescarga));
            //iniSection.Keys.Add(new IniKey(iniFile, "UtlizarDLL", string.Concat(datos.UtlizarDLL)));
            //iniSection.Keys.Add(new IniKey(iniFile, "VelocidadDescarga", string.Concat(datos.VelocidadDescarga)));
            //iniFile.Save(this.nombreIni);
        }

        public bool existeIni() {
            bool flag;
            Console.WriteLine(string.Concat("nombreIni->", this.nombreIni));
            flag = (!File.Exists(this.nombreIni) ? false : true);
            return flag;
        }

        public DataControl leerIni() {
            //IniFile iniFile = new IniFile(new IniOptions());
            //iniFile.Load(this.nombreIni);
            //IniSection item = iniFile.Sections[0];
            //return new DataControl(item.Keys["Host"].Value, item.Keys["Puerto"].Value, item.Keys["PathDescarga"].Value, (item.Keys["UtlizarDLL"] == null ? "false" : item.Keys["UtlizarDLL"].Value), (item.Keys["VelocidadDescarga"] == null ? "10" : item.Keys["VelocidadDescarga"].Value));
            return new DataControl("localhost", "3050", "C:\\Jaeger", "No", "10");
        }
    }
}