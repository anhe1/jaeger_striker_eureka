﻿using Jaeger.UI.LibUtilidades;
using Jaeger.UI.LibUtilidades.ObjectsData;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace Jaeger.UI.DD {
    partial class ImportarXMLs : Form {
        private FolderBrowserDialog folderBrowserDialog;

        private DataControl datos = null;

        private string rfcEmpresa;

        

        public DataControl Datos {
            get {
                return this.datos;
            }
            set {
                this.datos = value;
            }
        }

        public string RfcEmpresa {
            get {
                return this.rfcEmpresa;
            }
            set {
                this.rfcEmpresa = value;
            }
        }

        public ImportarXMLs() {
            this.InitializeComponent();
            this.folderBrowserDialog = new FolderBrowserDialog() {
                RootFolder = Environment.SpecialFolder.MyComputer
            };
        }

        private void btnAceptar_Click(object sender, EventArgs e) {
            if (this.txtPathSeleccionado.Text.Length == 0) {
                MessageBox.Show("Indique un path de busqueda de XML´s");
            }
            else if (!Directory.Exists(this.txtPathSeleccionado.Text)) {
                MessageBox.Show("No existe la Ruta Indicada");
            }
            else {
                base.Hide();
                DateTime now = DateTime.Now;
                ManagerBitacora.getSingleton().inicializa(string.Concat(this.txtPathSeleccionado.Text, Path.DirectorySeparatorChar, "Bitacora de Importacion"), "BitImportacion");
                RecursiveFileSearch recursiveFileSearch = new RecursiveFileSearch(this) {
                    Datos = this.datos,
                    Copia = this.chkCopia.Checked
                };
                recursiveFileSearch.iniciarEnPath(this.txtPathSeleccionado.Text, false, this.rfcEmpresa);
                while (!ManagerThreads.getSingleton().terminoTodos()) {
                    Thread.Sleep(100);
                }
                base.Show();
                string str = ManagerBitacora.getSingleton().obtenerPathFileLog();
                TimeSpan timeSpan = DateTime.Now - now;
                ManagerBitacora singleton = ManagerBitacora.getSingleton();
                object[] countEncontrado = new object[] { "Finalizo la importacion, se encontraron ", ManagerControlImport.getSingleton().CountEncontrado, " XML´s de los cuales ", ManagerControlImport.getSingleton().CountImportadas, " fueron Importados Nuevos y ", ManagerControlImport.getSingleton().CountImportadasYaExistian, " ya existian. Esto en un tiempo total de ", timeSpan.Hours, " hora(s) con ", timeSpan.Minutes, " minuto(s) con ", timeSpan.Seconds, " segundo(s)." };
                singleton.escribeLog(string.Concat(countEncontrado), false);
                countEncontrado = new object[] { "Por favor revise la bitacora de importacion que se encuentra en: ", str, "  , RESUMEN: Se encontraron ", ManagerControlImport.getSingleton().CountEncontrado, " XML´s de los cuales ", ManagerControlImport.getSingleton().CountImportadas, " fueron Importados Nuevos y ", ManagerControlImport.getSingleton().CountImportadasYaExistian, " ya existian. Esto en un tiempo total de ", timeSpan.Hours, " hora(s) con ", timeSpan.Minutes, " minuto(s) con ", timeSpan.Seconds, " segundo(s)." };
                MessageBox.Show(string.Concat(countEncontrado), "Finalizo:");
                Process.Start(str);
                ManagerBitacora.getSingleton().finaliza();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e) {
            this.folderBrowserDialog.Dispose();
            base.Dispose();
            base.Close();
        }

        private void btnExaminar_Click(object sender, EventArgs e) {
            this.folderBrowserDialog.ShowNewFolderButton = false;
            if (this.folderBrowserDialog.ShowDialog() == DialogResult.OK) {
                this.txtPathSeleccionado.Text = this.folderBrowserDialog.SelectedPath;
            }
        }

        

        protected override void OnFormClosing(FormClosingEventArgs e) {
            this.folderBrowserDialog.Dispose();
            base.Dispose();
            base.Close();
        }

        protected override bool ProcessDialogKey(Keys keyData) {
            bool flag;
            if ((Control.ModifierKeys != Keys.None ? true : keyData != Keys.Escape)) {
                flag = base.ProcessDialogKey(keyData);
            }
            else {
                this.folderBrowserDialog.Dispose();
                base.Dispose();
                base.Close();
                flag = true;
            }
            return flag;
        }
    }
}