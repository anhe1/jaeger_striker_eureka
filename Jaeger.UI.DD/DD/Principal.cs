﻿using Jaeger.UI.LibAdminCfdi;
using Jaeger.UI.LibUtilidades;
using Jaeger.UI.LibUtilidades.ObjectsData;
using Jaeger.UI.WSDL;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace Jaeger.UI.DD {
    partial class Principal : Form {
        private string respuesta = string.Empty;

        private string pathDescargas = string.Empty;

        private DataControl datos = null;

        private bool emitido = true;

        private int key = 0;

        private string nombreLog = "";

        private AcercaDe algo = null;

        private bool usarUltilitiesLib = true;

        private ManejoIni ini = new ManejoIni();

        private string error = "0";

        private MainForm ventana = null;

        public RenderWebBrowser renderWebBrowser = null;

        public Principal() {
            Exception exception;
            try {
                this.nombreLog = Path.Combine("C:\\Jaeger\\Jaeger.Log\\", "BitacoraModuloDescargas.txt");
                this.key = (int)DateTime.Now.Ticks;
                this.InitializeComponent();
                if (!this.ini.existeIni()) {
                    this.ini.crearIni(new DataControl("localhost", "8080", "", "false", "10"));
                }
                bool flag = false;
                try {
                    this.datos = this.ini.leerIni();
                }
                catch (Exception exception1) {
                    exception = exception1;
                    MessageBox.Show(exception.Message ?? "", "Error al leer archivo DDW.ini");
                    flag = true;
                }
                try {
                    if (!flag) {
                        ControllerEmpresas.getSingleton().conectaDocsDigs(this.datos);
                    }
                }
                catch (Exception exception2) {
                    exception = exception2;
                    if (!exception.Message.Contains("Error al conectarse con el Servidor")) {
                        MessageBox.Show(exception.Message ?? "", "Error al intentar conectarse:");
                    }
                    else {
                        MessageBox.Show(string.Concat("Sucedió un error al conectarse al servidor, verifique que este encendido el servidor y el servicio de Apache Tomcat 8, detalle: ", exception.Message), "Datos Incorrectos:");
                    }
                    flag = true;
                }
                if (flag) {
                    this.pideDatosAplicacion();
                    this.ini.crearIni(this.datos);
                }
                this.pideLogin();
                if (this.datos.CadenaEmpresas.Length != 0) {
                    ControllerEmpresas.getSingleton().reinicia();
                    ControllerEmpresas.getSingleton().generaEmpresas(this.datos.CadenaEmpresas);
                }
                this.lblNombreUsuario.Text = this.datos.NombreUsuario ?? "";
                this.usarUltilitiesLib = this.datos.UtlizarDLL;
                ControllerEmpresas.getSingleton().HilosMaximos = this.datos.VelocidadDescarga;
                this.numericUpDownHilos.Value = this.datos.VelocidadDescarga;
                string lower = Environment.OSVersion.ToString().ToLower();
                if ((lower.Contains("2003") || lower.Contains("2008") || lower.Contains("2012") || lower.Contains("2016") ? true : lower.Contains("server"))) {
                    this.usarUltilitiesLib = true;
                }
                this.cboxEmpresas.Items.AddRange(ControllerEmpresas.getSingleton().obtenerArrayEmps());
                this.cboxEmpresas.DrawMode = DrawMode.OwnerDrawFixed;
                this.cboxEmpresas.DrawItem += new DrawItemEventHandler(this.comboBox1_DrawItem);
                this.cboxEmpresas.SelectedIndexChanged += new EventHandler((object cbSender, EventArgs cbe) => {
                    ComboBox comboBox = cbSender as ComboBox;
                    if ((comboBox.SelectedItem == null || !(comboBox.SelectedItem is DataEmpresas) ? false : !((DataEmpresas)comboBox.SelectedItem).Selectable)) {
                        comboBox.SelectedIndex = -1;
                    }
                });
                if (this.cboxEmpresas.Items.Count != 0) {
                    this.cboxEmpresas.SelectedItem = this.cboxEmpresas.Items[1];
                    ControllerEmpresas.getSingleton().Selected = (DataEmpresas)this.cboxEmpresas.SelectedItem;
                    this.lblRfc.Text = ControllerEmpresas.getSingleton().Selected.RfcEmpresa;
                    this.pathDescargas = string.Concat(@"C:\Jaeger\Jaeger.Temporal", Path.DirectorySeparatorChar, "DescargasDocsDigsSAT");
                    if (!Directory.Exists(this.pathDescargas)) {
                        Directory.CreateDirectory(this.pathDescargas);
                    }
                    Console.WriteLine(string.Concat("id empresa->", ControllerEmpresas.getSingleton().Selected.IdEmpresa));
                }
                else {
                    MessageBox.Show("Aun no se han registrado ninguna empresa, primero entre a Documentos Digitales Web", "Error de Inicio:");
                    try {
                        (new PrincipalAdminCom()).validaUsuario(this.datos, "", false, this.key);
                    }
                    catch (Exception exception3) {
                    }
                    Environment.Exit(0);
                    return;
                }
            }
            catch (Exception exception4) {
                exception = exception4;
                Console.WriteLine(string.Concat("Error: ", exception.Message));
                StackTrace stackTrace = new StackTrace(exception, true);
                object[] str = new object[] { "Principal.Principal()->", exception.ToString(), "<->", stackTrace.GetFrame(0).GetFileLineNumber() };
                this.respuesta = string.Concat(str);
                string[] strArrays = new string[] { string.Concat("Exception a las ", DateTime.Now), string.Concat("Message ---", exception.Message), string.Concat("HelpLink ---", exception.HelpLink), string.Concat("Source ---", exception.Source), string.Concat("StackTrace ---", exception.StackTrace), string.Concat("TargetSite ---", exception.TargetSite), string.Concat("Exception ---", exception.ToString()), string.Concat("GetFileLineNumber ---", stackTrace.GetFrame(0).GetFileLineNumber()), this.respuesta, this.pathDescargas, string.Concat("Linea error->", this.error) };
                File.WriteAllLines(this.nombreLog, strArrays);
                MessageBox.Show(string.Concat(exception.Message, " Mas informacion en el Log: ", this.nombreLog), "Error al iniciar la conexion:");
            }
        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e) {
            try {
                if (this.algo == null) {
                    this.algo = new AcercaDe() {
                        TopMost = true
                    };
                }
                this.algo.Show();
            }
            catch (Exception exception) {
                Console.WriteLine(string.Concat("error->", exception.Message));
            }
        }

        private void btnDescargaSat_Click(object sender, EventArgs e) {
            this.emitido = this.radBtnEmitidos.Checked;
            this.ejecutaTarea();
        }

        private void cboxEmpresas_SelectedIndexChanged(object sender, EventArgs e) {
            try {
                ComboBox comboBox = (ComboBox)sender;
                ControllerEmpresas.getSingleton().Selected = (DataEmpresas)comboBox.SelectedItem;
                if (ControllerEmpresas.getSingleton().Selected == null) {
                    this.lblRfc.Text = "";
                }
                else {
                    this.lblRfc.Text = ControllerEmpresas.getSingleton().Selected.RfcEmpresa;
                }
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                StackTrace stackTrace = new StackTrace(exception, true);
                object[] str = new object[] { "Principal.Principal()->", exception.ToString(), "<->", stackTrace.GetFrame(0).GetFileLineNumber() };
                this.respuesta = string.Concat(str);
                string[] strArrays = new string[] { string.Concat("Exception a las ", DateTime.Now), string.Concat("Message ---", exception.Message), string.Concat("HelpLink ---", exception.HelpLink), string.Concat("Source ---", exception.Source), string.Concat("StackTrace ---", exception.StackTrace), string.Concat("TargetSite ---", exception.TargetSite), string.Concat("Exception ---", e.ToString()), string.Concat("GetFileLineNumber ---", stackTrace.GetFrame(0).GetFileLineNumber()) };
                File.WriteAllLines(this.nombreLog, strArrays);
                MessageBox.Show(string.Concat(exception.Message, " Mas informacion en el Log: ", this.nombreLog), "Error durante la ejecucion:");
            }
        }

        private void comboBox1_DrawItem(object sender, DrawItemEventArgs e) {
            if (e.Index != -1) {
                DataEmpresas item = (DataEmpresas)this.cboxEmpresas.Items[e.Index];
                if (!item.Selectable) {
                    e.Graphics.DrawString(item.NombreEmpresa, new Font("Aerial", 10f, FontStyle.Bold), SystemBrushes.ControlText, e.Bounds);
                }
                else {
                    e.Graphics.DrawString(item.NombreEmpresa, new Font("Aerial", 10f, FontStyle.Regular), SystemBrushes.ControlText, e.Bounds);
                }
            }
        }

        public void ejecutaTarea() {
            Exception exception;
            StackTrace stackTrace;
            object[] str;
            string[] strArrays;
            try {
                try {
                    if (ControllerEmpresas.getSingleton().Selected != null) {
                        if ((!this.usarUltilitiesLib ? false : this.renderWebBrowser == null)) {
                            this.renderWebBrowser = new RenderWebBrowser();
                        }
                        ControllerEmpresas.getSingleton().HilosMaximos = (int)this.numericUpDownHilos.Value;
                        this.ventana = new MainForm();
                        this.ventana.Show(this);
                        this.ventana.WindowState = FormWindowState.Maximized;
                        this.ventana._windowManager.ActualBrowserControl.progess = 2;
                        this.ventana._windowManager.ActualBrowserControl.usuario = ControllerEmpresas.getSingleton().Selected.RfcEmpresa;
                        this.ventana._windowManager.ActualBrowserControl.pass = this.txtPassSAT.Text;
                        this.ventana._windowManager.ActualBrowserControl.pathBandeja = this.pathDescargas;
                        this.ventana._windowManager.ActualBrowserControl.showWindow = true;
                        this.ventana._windowManager.ActualBrowserControl.porHora = false;
                        this.ventana._windowManager.ActualBrowserControl.Recibidas = !this.emitido;
                        this.ventana._windowManager.ActualBrowserControl.siDescarga = true;
                        this.ventana._windowManager.ActualBrowserControl.idEmpresa = ControllerEmpresas.getSingleton().Selected.IdEmpresa;
                        this.ventana._windowManager.ActualBrowserControl.rfcEmpresa = ControllerEmpresas.getSingleton().Selected.RfcEmpresa;
                        this.ventana._windowManager.ActualBrowserControl.datos = this.datos;
                        this.ventana.registra = true;
                        Console.WriteLine(string.Concat("ventana.registra->", this.ventana.registra));
                        this.ventana.WindowManager.ActualBrowserControl.renderWebBrowser = this.renderWebBrowser;
                        this.ventana.WindowManager.ActualBrowserControl.usarUltilitiesLib = this.usarUltilitiesLib;
                        while (this.ventana._windowManager.ActualBrowserControl.trabajando) {
                            Application.DoEvents();
                        }
                        this.respuesta = this.ventana._windowManager.ActualBrowserControl.respuesta;
                    }
                    else {
                        MessageBox.Show("Primero seleccione una Empresa de trabajo del combo box.", "Alerta:");
                        return;
                    }
                }
                catch (Exception exception1) {
                    exception = exception1;
                    stackTrace = new StackTrace(exception, true);
                    str = new object[] { "Principal.Principal()->", exception.ToString(), "<->", stackTrace.GetFrame(0).GetFileLineNumber() };
                    this.respuesta = string.Concat(str);
                    strArrays = new string[] { string.Concat("Exception a las ", DateTime.Now), string.Concat("Message ---", exception.Message), string.Concat("HelpLink ---", exception.HelpLink), string.Concat("Source ---", exception.Source), string.Concat("StackTrace ---", exception.StackTrace), string.Concat("TargetSite ---", exception.TargetSite), string.Concat("Exception ---", exception.ToString()), string.Concat("GetFileLineNumber ---", stackTrace.GetFrame(0).GetFileLineNumber()) };
                    File.WriteAllLines(this.nombreLog, strArrays);
                    MessageBox.Show(string.Concat(exception.Message, " Mas informacion en el Log: ", this.nombreLog), "Error durante la ejecucion:");
                }
            }
            finally {
                try {
                    if (this.ventana != null) {
                        this.ventana._windowManager.ActualBrowserControl.alert.Dispose();
                        this.ventana._windowManager.ActualBrowserControl.alert.Close();
                        if (!this.respuesta.Equals("USUARIO O CONTRASEÑA INVALIDOS")) {
                            this.ventana.CerrarSession();
                            while (this.ventana._windowManager.ActualBrowserControl.openSession) {
                                Application.DoEvents();
                            }
                        }
                        this.ventana._windowManager.ActualBrowserControl.Dispose();
                        this.ventana._windowManager.Close();
                        this.ventana.Dispose();
                        this.ventana.Close();
                        this.ventana = null;
                    }
                }
                catch (Exception exception3) {
                    exception = exception3;
                    try {
                        stackTrace = new StackTrace(exception, true);
                        str = new object[] { "Principal.ejecutaTarea()->", exception.ToString(), "<->", stackTrace.GetFrame(0).GetFileLineNumber() };
                        this.respuesta = string.Concat(str);
                        strArrays = new string[] { string.Concat("Exception a las ", DateTime.Now), string.Concat("Message ---", exception.Message), string.Concat("HelpLink ---", exception.HelpLink), string.Concat("Source ---", exception.Source), string.Concat("StackTrace ---", exception.StackTrace), string.Concat("TargetSite ---", exception.TargetSite), string.Concat("Exception ---", exception.ToString()), string.Concat("GetFileLineNumber ---", stackTrace.GetFrame(0).GetFileLineNumber()) };
                        File.WriteAllLines(this.nombreLog, strArrays);
                    }
                    catch (Exception exception2) {
                    }
                }
            }
        }

        private void forzarDescargasConDLLToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!this.usarUltilitiesLib) {
                this.usarUltilitiesLib = true;
                this.datos.UtlizarDLL = true;
                this.ini.crearIni(this.datos);
                MessageBox.Show(this, "La funcionalidad forzar descarga con DLL se ha activado.", "Activado:");
            }
            else {
                this.usarUltilitiesLib = false;
                this.datos.UtlizarDLL = false;
                this.ini.crearIni(this.datos);
                MessageBox.Show(this, "La funcionalidad forzar descarga con DLL se ha desactivado.", "Desactivado:");
            }
        }

        

        public void muestraEmpresas() {
            Application.EnableVisualStyles();
            CatalogoEmpresas catalogoEmpresa = new CatalogoEmpresas() {
                ListaEmp = ControllerEmpresas.getSingleton().obtenerArrayEmps()
            };
            catalogoEmpresa.renderizaLista();
            catalogoEmpresa.TopMost = true;
            Application.Run(catalogoEmpresa);
            this.cboxEmpresas.SelectedItem = catalogoEmpresa.EmpSel;
        }

        protected override void OnFormClosing(FormClosingEventArgs e) {
            try {
                if (this.numericUpDownHilos.Value != this.datos.VelocidadDescarga) {
                    this.datos.VelocidadDescarga = (int)this.numericUpDownHilos.Value;
                    this.ini.crearIni(this.datos);
                }
                (new PrincipalAdminCom()).validaUsuario(this.datos, "", false, this.key);
            }
            catch (Exception exception) {
            }
            Environment.Exit(0);
        }

        public void pideDatosAplicacion() {
            Application.EnableVisualStyles();
            InicioAplicacion inicioAplicacion = new InicioAplicacion();
            Application.Run(inicioAplicacion);
            this.datos = inicioAplicacion.Datos;
            this.datos.PathDescarga = "";
        }

        public void pideLogin() {
            Application.EnableVisualStyles();
            Login login = new Login(this.key) {
                Datos = this.datos,
                TopMost = true
            };
            Application.Run(login);
            this.datos = login.Datos;
        }

        private void rBtnRegistraAuto_CheckedChanged(object sender, EventArgs e) {
            if (this.ventana != null) {
                this.ventana.registra = true;
                Console.WriteLine(string.Concat("uno ventana.registra->", this.ventana.registra));
            }
        }

        private void rBtnSoloAgrega_CheckedChanged(object sender, EventArgs e) {
            if (this.ventana != null) {
                this.ventana.registra = true;
                Console.WriteLine(string.Concat("dos ventana.registra->", this.ventana.registra));
            }
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e) {
            try {
                (new PrincipalAdminCom()).validaUsuario(this.datos, "", false, this.key);
            }
            catch (Exception exception) {
            }
            Environment.Exit(0);
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e) {
            ImportarXMLs importarXML = new ImportarXMLs() {
                Datos = this.datos,
                RfcEmpresa = ControllerEmpresas.getSingleton().Selected.RfcEmpresa,
                MdiParent = base.MdiParent
            };
            importarXML.ShowDialog(this);
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e) {
            string str = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), Path.DirectorySeparatorChar, "Documentos Digitales Web.html");
            StreamWriter streamWriter = new StreamWriter(str);
            try {
                string[] host = new string[] { "<html><head></head><body onload=\"window.open('http://", this.datos.Host, ":", this.datos.Puerto, "/AdminWeb/web.xhtml','_self');\"></body></html>" };
                streamWriter.WriteLine(string.Concat(host));
                streamWriter.Flush();
                streamWriter.Close();
            }
            finally {
                if (streamWriter != null) {
                    ((IDisposable)streamWriter).Dispose();
                }
            }
            MessageBox.Show(this, "Se ha creado un Acceso a Documentos Digitales Web en su Escritorio, por favor utilice un explorador web diferente a Internet Explorer.", "Finalizo:");
            Process.Start(str);
        }

        public void validaServidor() {
        }
    }
}