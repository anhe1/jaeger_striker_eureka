﻿using System.Drawing;
using System.Windows.Forms;

namespace Jaeger.UI.DD {
    partial class CatalogoEmpresas {
        protected override void Dispose(bool disposing) {
            if ((!disposing ? false : this.components != null)) {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent() {
         
            this.treeView1 = new TreeView();
            base.SuspendLayout();
            this.treeView1.Location = new Point(13, 13);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new Size(388, 275);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new TreeViewEventHandler(this.treeView1_AfterSelect);
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(413, 300);
            base.Controls.Add(this.treeView1);

            base.Name = "CatalogoEmpresas";
            this.Text = "Catalogo Empresas";
            base.ResumeLayout(false);
        }
    }
}
