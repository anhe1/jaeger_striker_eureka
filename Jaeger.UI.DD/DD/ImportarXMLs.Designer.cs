﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jaeger.UI.DD {
    partial class ImportarXMLs {
        protected override void Dispose(bool disposing) {
            if ((!disposing ? false : this.components != null)) {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.btnExaminar = new System.Windows.Forms.Button();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.chkCopia = new System.Windows.Forms.CheckBox();
            this.txtPathSeleccionado = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 0;
            // 
            // btnExaminar
            // 
            this.btnExaminar.Location = new System.Drawing.Point(389, 98);
            this.btnExaminar.Name = "btnExaminar";
            this.btnExaminar.Size = new System.Drawing.Size(128, 28);
            this.btnExaminar.TabIndex = 1;
            this.btnExaminar.Text = "Examinar";
            this.btnExaminar.UseVisualStyleBackColor = true;
            this.btnExaminar.Click += new System.EventHandler(this.btnExaminar_Click);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(442, 205);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 28);
            this.btnAceptar.TabIndex = 2;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(361, 206);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 27);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // chkCopia
            // 
            this.chkCopia.AutoSize = true;
            this.chkCopia.Checked = true;
            this.chkCopia.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCopia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCopia.Location = new System.Drawing.Point(16, 139);
            this.chkCopia.Name = "chkCopia";
            this.chkCopia.Size = new System.Drawing.Size(511, 55);
            this.chkCopia.TabIndex = 4;
            this.chkCopia.Text = "Hacer una copia dentro de la carpeta \"Bitácora de Importación\" de todos los \r\nXML" +
    "´s que le pertenecen a la empresa seleccionada pero que no pudieron \r\nser import" +
    "ados .";
            this.chkCopia.UseVisualStyleBackColor = true;
            // 
            // txtPathSeleccionado
            // 
            this.txtPathSeleccionado.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPathSeleccionado.Location = new System.Drawing.Point(13, 101);
            this.txtPathSeleccionado.Name = "txtPathSeleccionado";
            this.txtPathSeleccionado.Size = new System.Drawing.Size(370, 23);
            this.txtPathSeleccionado.TabIndex = 5;
            // 
            // ImportarXMLs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 245);
            this.Controls.Add(this.txtPathSeleccionado);
            this.Controls.Add(this.chkCopia);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.btnExaminar);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportarXMLs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Importación Masiva";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private IContainer components = null;

        private Label label1;

        private Button btnExaminar;

        private Button btnAceptar;

        private Button btnCancelar;

        private CheckBox chkCopia;
        private TextBox txtPathSeleccionado;
    }
}
