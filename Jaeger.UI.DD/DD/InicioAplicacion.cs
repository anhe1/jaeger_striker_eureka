﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.UI.LibUtilidades;
using Jaeger.UI.LibUtilidades.ObjectsData;

namespace Jaeger.UI.DD {
    partial class InicioAplicacion : Form {
        private DataControl datos;

        private IContainer components = null;

        private TextBox txtHost;

        private Label label1;

        private Label label2;

        private TextBox txtPuerto;

        private Button button1;

        internal DataControl Datos {
            get {
                return this.datos;
            }
            set {
                this.datos = value;
            }
        }

        public InicioAplicacion() {
            this.InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            this.ejecuta();
        }

        private void ejecuta() {
            if (this.txtHost.Text.ToString().Trim().Length == 0) {
                MessageBox.Show("Es necesario indicar la IP del Servidor", "Faltan Campos:");
            }
            else if (this.txtPuerto.Text.ToString().Trim().Length != 0) {
                try {
                    this.datos = new DataControl(this.txtHost.Text.ToString().Trim(), this.txtPuerto.Text.ToString().Trim());
                    ControllerEmpresas.getSingleton().conectaDocsDigs(this.datos);
                    base.Dispose();
                    base.Close();
                }
                catch (Exception exception1) {
                    Exception exception = exception1;
                    Console.WriteLine(string.Concat("Error: ", exception.Message));
                    Console.WriteLine(string.Concat("StackTrace: ", exception.StackTrace));
                    MessageBox.Show(string.Concat("Sucedió un error al conectarse al servidor, verifique que este encendido el servidor y el servicio de Apache Tomcat 8, detalle: ", exception.Message), "Datos Incorrectos:");
                }
            }
            else {
                MessageBox.Show("Es necesario indicar el puerto del Servidor", "Faltan Campos:");
            }
        }

        protected override void OnFormClosing(FormClosingEventArgs e) {
            Environment.Exit(0);
        }

        private void tb_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Return) {
                this.ejecuta();
            }
        }
    }
}