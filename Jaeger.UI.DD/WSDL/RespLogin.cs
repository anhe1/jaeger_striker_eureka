﻿namespace Jaeger.UI.WSDL {
    public class RespLogin {
        private string error = "";

        private string nombreUsuario;

        private string cadenaEmpresas = "";

        public string CadenaEmpresas {
            get {
                return this.cadenaEmpresas;
            }
            set {
                this.cadenaEmpresas = value;
            }
        }

        public string Error {
            get {
                return this.error;
            }
            set {
                this.error = value;
            }
        }

        public string NombreUsuario {
            get {
                return this.nombreUsuario;
            }
            set {
                this.nombreUsuario = value;
            }
        }

        public RespLogin() {
            
        }

        public RespLogin(string mensaje) {
            this.error = mensaje;
        }

        public RespLogin(string value, string saldo) {
            this.nombreUsuario = value;
            this.cadenaEmpresas = saldo;
        }
    }
}
