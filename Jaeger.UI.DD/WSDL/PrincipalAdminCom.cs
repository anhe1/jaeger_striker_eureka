﻿using Jaeger.UI.LibUtilidades.ObjectsData;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jaeger.UI.WSDL {
    public class PrincipalAdminCom {

        
        //public class AdminComClient : ClientBase<AdminCom>, AdminCom {
        //    public AdminComClient() {
        //    }

        //    public AdminComClient(string endpointConfigurationName) : base(endpointConfigurationName) {
        //    }

        //    public AdminComClient(string endpointConfigurationName, string remoteAddress) : base(endpointConfigurationName, remoteAddress) {
        //    }

        //    public AdminComClient(string endpointConfigurationName, EndpointAddress remoteAddress) : base(endpointConfigurationName, remoteAddress) {
        //    }

        //    public AdminComClient(Binding binding, EndpointAddress remoteAddress) : base(binding, remoteAddress) {
        //    }

        //    public string actualizaCtas(string[] nombre, string[] cuenta, string idEmpresa) {
        //        actualizaCtasRequest _actualizaCtasRequest = new actualizaCtasRequest() {
        //            nombre = nombre,
        //            cuenta = cuenta,
        //            idEmpresa = idEmpresa
        //        };
        //        return ((AdminCom)this).actualizaCtas(_actualizaCtasRequest).actualizaCtasReturn;
        //    }

        //    public string borrarPolizaCFD(string[] UUID, string sPoliza, string idEmpresa) {
        //        borrarPolizaCFDRequest _borrarPolizaCFDRequest = new borrarPolizaCFDRequest() {
        //            UUID = UUID,
        //            sPoliza = sPoliza,
        //            idEmpresa = idEmpresa
        //        };
        //        return ((AdminCom)this).borrarPolizaCFD(_borrarPolizaCFDRequest).borrarPolizaCFDReturn;
        //    }

        //    public string buscaCFDI(string usuario, string pass, string idEmpresa, string sUUID) {
        //        return base.Channel.buscaCFDI(usuario, pass, idEmpresa, sUUID);
        //    }

        //    public string cargaCFD(byte[] xml, byte[] pdf, bool bValida, string idEmpresa) {
        //        cargaCFDRequest _cargaCFDRequest = new cargaCFDRequest() {
        //            xml = xml,
        //            pdf = pdf,
        //            bValida = bValida,
        //            idEmpresa = idEmpresa
        //        };
        //        return ((AdminCom)this).cargaCFD(_cargaCFDRequest).cargaCFDReturn;
        //    }

        //    public string consultaCFDs(string[] UUID, string idEmpresa) {
        //        consultaCFDsRequest _consultaCFDsRequest = new consultaCFDsRequest() {
        //            UUID = UUID,
        //            idEmpresa = idEmpresa
        //        };
        //        return ((AdminCom)this).consultaCFDs(_consultaCFDsRequest).consultaCFDsReturn;
        //    }

        //    [EditorBrowsable(EditorBrowsableState.Advanced)]
        //    actualizaCtasResponse LibUtilidades.ServiceRefAdminCom.AdminCom.actualizaCtas(actualizaCtasRequest request) {
        //        return base.Channel.actualizaCtas(request);
        //    }

        //    [EditorBrowsable(EditorBrowsableState.Advanced)]
        //    borrarPolizaCFDResponse LibUtilidades.ServiceRefAdminCom.AdminCom.borrarPolizaCFD(borrarPolizaCFDRequest request) {
        //        return base.Channel.borrarPolizaCFD(request);
        //    }

        //    [EditorBrowsable(EditorBrowsableState.Advanced)]
        //    cargaCFDResponse LibUtilidades.ServiceRefAdminCom.AdminCom.cargaCFD(cargaCFDRequest request) {
        //        return base.Channel.cargaCFD(request);
        //    }

        //    [EditorBrowsable(EditorBrowsableState.Advanced)]
        //    consultaCFDsResponse LibUtilidades.ServiceRefAdminCom.AdminCom.consultaCFDs(consultaCFDsRequest request) {
        //        return base.Channel.consultaCFDs(request);
        //    }

        //    [EditorBrowsable(EditorBrowsableState.Advanced)]
        //    mainResponse LibUtilidades.ServiceRefAdminCom.AdminCom.main(mainRequest request) {
        //        return base.Channel.main(request);
        //    }

        //    [EditorBrowsable(EditorBrowsableState.Advanced)]
        //    pideYCargaCFDsResponse LibUtilidades.ServiceRefAdminCom.AdminCom.pideYCargaCFDs(pideYCargaCFDsRequest request) {
        //        return base.Channel.pideYCargaCFDs(request);
        //    }

        //    [EditorBrowsable(EditorBrowsableState.Advanced)]
        //    polizaCFDResponse LibUtilidades.ServiceRefAdminCom.AdminCom.polizaCFD(polizaCFDRequest request) {
        //        return base.Channel.polizaCFD(request);
        //    }

        //    [EditorBrowsable(EditorBrowsableState.Advanced)]
        //    sincronizaBuzonTributarioResponse LibUtilidades.ServiceRefAdminCom.AdminCom.sincronizaBuzonTributario(sincronizaBuzonTributarioRequest request) {
        //        return base.Channel.sincronizaBuzonTributario(request);
        //    }

        //    public string listaEmpresas() {
        //        return base.Channel.listaEmpresas();
        //    }

        //    public void main(string[] args) {
        //        mainRequest _mainRequest = new mainRequest() {
        //            args = args
        //        };
        //        ((AdminCom)this).main(_mainRequest);
        //    }

        //    public string pdfCFD(string UUID, string idEmpresa) {
        //        return base.Channel.pdfCFD(UUID, idEmpresa);
        //    }

        //    public string pideYCargaCFDs(byte[] xml, byte[] pdf, bool bValida, string idEmpresa) {
        //        pideYCargaCFDsRequest _pideYCargaCFDsRequest = new pideYCargaCFDsRequest() {
        //            xml = xml,
        //            pdf = pdf,
        //            bValida = bValida,
        //            idEmpresa = idEmpresa
        //        };
        //        return ((AdminCom)this).pideYCargaCFDs(_pideYCargaCFDsRequest).pideYCargaCFDsReturn;
        //    }

        //    public string polizaCFD(string[] UUID, string sPoliza, string idEmpresa) {
        //        polizaCFDRequest _polizaCFDRequest = new polizaCFDRequest() {
        //            UUID = UUID,
        //            sPoliza = sPoliza,
        //            idEmpresa = idEmpresa
        //        };
        //        return ((AdminCom)this).polizaCFD(_polizaCFDRequest).polizaCFDReturn;
        //    }

        //    public string seleccionaCFDs(string usuario, string idEmpresa, string sistema) {
        //        return base.Channel.seleccionaCFDs(usuario, idEmpresa, sistema);
        //    }

        //    public string sincronizaBuzonTributario(string usuario, bool emitidoRecibido, string strDatos, byte[] dFile, string idEmpresa, string dExt3, string dExt4) {
        //        sincronizaBuzonTributarioRequest _sincronizaBuzonTributarioRequest = new sincronizaBuzonTributarioRequest() {
        //            usuario = usuario,
        //            emitidoRecibido = emitidoRecibido,
        //            strDatos = strDatos,
        //            dFile = dFile,
        //            idEmpresa = idEmpresa,
        //            dExt3 = dExt3,
        //            dExt4 = dExt4
        //        };
        //        return ((AdminCom)this).sincronizaBuzonTributario(_sincronizaBuzonTributarioRequest).sincronizaBuzonTributarioReturn;
        //    }
        //}

        

        public PrincipalAdminCom() {
        }

        //public ObjResponse enviaXMLyPDF(DataControl data, byte[] xml, byte[] pdf, bool bValida, string idEmpresa) {
        //    ObjResponse objResponse;
        //    try {
        //        if (this.wsJava == null) {
        //            this.wsJava = new AdminComClient();
        //            ServiceEndpoint endpoint = this.wsJava.Endpoint;
        //            string[] host = new string[] { "http://", data.Host, ":", data.Puerto, "/AdminWeb/services/AdminCom" };
        //            endpoint.Address = new EndpointAddress(string.Concat(host));
        //        }
        //        string str = this.wsJava.cargaCFD(xml, pdf, bValida, idEmpresa);
        //        RESPUESTA rESPUESTum = SerializacionXML.XmlDeserializarStringXml<RESPUESTA>(str);
        //        objResponse = ((rESPUESTum.GENERAL.ERROR == null || rESPUESTum.GENERAL.ERROR.Value == null ? true : rESPUESTum.GENERAL.ERROR.Value.Length == 0) ? new ObjResponse(rESPUESTum.DATA.Value, false, rESPUESTum.DATA.Saldo) : new ObjResponse(rESPUESTum.GENERAL.ERROR.Value, true));
        //    }
        //    catch (Exception exception1) {
        //        Exception exception = exception1;
        //        Console.WriteLine(string.Concat("StackTrace: ", exception.StackTrace));
        //        Console.WriteLine(string.Concat("Error: ", exception.Message));
        //        throw new Exception(string.Concat("Error al conectarse con el Servidor, detalle tecnico: ", exception.Message));
        //    }
        //    return objResponse;
        //}

        public string listaEmpresas(DataControl data) {
            string str;
            try {
                //if (this.wsJava == null) {
                //    this.wsJava = new AdminComClient();
                //    ServiceEndpoint endpoint = this.wsJava.Endpoint;
                //    string[] host = new string[] { "http://", data.Host, ":", data.Puerto, "/AdminWeb/services/AdminCom" };
                //    endpoint.Address = new EndpointAddress(string.Concat(host));
                //}
                //RESPUESTA rESPUESTum = SerializacionXML.XmlDeserializarStringXml<RESPUESTA>(this.wsJava.listaEmpresas());
                //str = ((rESPUESTum.GENERAL.ERROR == null || rESPUESTum.GENERAL.ERROR.Value == null ? true : rESPUESTum.GENERAL.ERROR.Value.Length == 0) ? rESPUESTum.DATA.Value : string.Concat("Error: ", rESPUESTum.GENERAL.ERROR.Value));
                str = "";
            }
            catch (Exception exception1) {
                Exception exception = exception1;
                Console.WriteLine(string.Concat("StackTrace: ", exception.StackTrace));
                Console.WriteLine(string.Concat("Error: ", exception.Message));
                throw new Exception(string.Concat("Error al conectarse con el Servidor, detalle tecnico: ", exception.Message));
            }
            return str;
        }

        //public DataRespWsdlBuzon sincronizaBuzonTributario(DataControl data, bool emitidoRecibido, string strDatos, byte[] dFile, string idEmpresa, string dExt3, string dExt4) {
        //    DataRespWsdlBuzon dataRespWsdlBuzon;
        //    try {
        //        if (this.wsJava == null) {
        //            this.wsJava = new AdminComClient();
        //            ServiceEndpoint endpoint = this.wsJava.Endpoint;
        //            string[] host = new string[] { "http://", data.Host, ":", data.Puerto, "/AdminWeb/services/AdminCom" };
        //            endpoint.Address = new EndpointAddress(string.Concat(host));
        //        }
        //        DataRespWsdlBuzon dOCUMENTOS = new DataRespWsdlBuzon();
        //        string str = this.wsJava.sincronizaBuzonTributario(data.Usuario, emitidoRecibido, strDatos, dFile, idEmpresa, dExt3, dExt4);
        //        RESPUESTA rESPUESTum = SerializacionXML.XmlDeserializarStringXml<RESPUESTA>(str);
        //        if ((rESPUESTum.GENERAL.ERROR == null || rESPUESTum.GENERAL.ERROR.Value == null ? true : rESPUESTum.GENERAL.ERROR.Value.Length == 0)) {
        //            dOCUMENTOS.Solicitud = rESPUESTum.DOCUMENTOS;
        //            dataRespWsdlBuzon = dOCUMENTOS;
        //        }
        //        else {
        //            dOCUMENTOS.Error = string.Concat("Error: ", rESPUESTum.GENERAL.ERROR.Value);
        //            dataRespWsdlBuzon = dOCUMENTOS;
        //        }
        //    }
        //    catch (Exception exception1) {
        //        Exception exception = exception1;
        //        File.WriteAllText("C:\\CFDI_envioWSDLexcepcion.txt", string.Concat("Error al conectarse con el Servidor, detalle tecnico: ", exception.Message, "StackTrace: ", exception.StackTrace));
        //        Console.WriteLine(string.Concat("StackTrace: ", exception.StackTrace));
        //        Console.WriteLine(string.Concat("Error: ", exception.Message));
        //        throw new Exception(string.Concat("Error al conectarse con el Servidor, detalle tecnico: ", exception.Message));
        //    }
        //    return dataRespWsdlBuzon;
        //}

        public RespLogin validaUsuario(DataControl data, string pass, bool entrando, int key) {
            RespLogin respLogin = new RespLogin();
            respLogin.NombreUsuario = "ANHE1";
        //    try {
        //        if (this.wsJava == null) {
        //            this.wsJava = new AdminComClient();
        //            ServiceEndpoint endpoint = this.wsJava.Endpoint;
        //            string[] host = new string[] { "http://", data.Host, ":", data.Puerto, "/AdminWeb/services/AdminCom" };
        //            endpoint.Address = new EndpointAddress(string.Concat(host));
        //        }
        //        string str = this.wsJava.sincronizaBuzonTributario(data.Usuario, entrando, pass, Encoding.UTF8.GetBytes(string.Concat(key)), "", "", "");
        //        Console.WriteLine(string.Concat("respuesta validaUsuario: ", str));
        //        RESPUESTA rESPUESTum = SerializacionXML.XmlDeserializarStringXml<RESPUESTA>(str);
        //        respLogin = ((rESPUESTum.GENERAL.ERROR == null || rESPUESTum.GENERAL.ERROR.Value == null ? true : rESPUESTum.GENERAL.ERROR.Value.Length == 0) ? new RespLogin(rESPUESTum.DATA.Value, rESPUESTum.DATA.Saldo) : new RespLogin(string.Concat("Error: ", rESPUESTum.GENERAL.ERROR.Value)));
        //    }
        //    catch (Exception exception1) {
        //        Exception exception = exception1;
        //        Console.WriteLine(string.Concat("StackTrace: ", exception.StackTrace));
        //        Console.WriteLine(string.Concat("Error: ", exception.Message));
        //        throw new Exception(string.Concat("Error al conectarse con el Servidor, detalle tecnico: ", exception.Message));
        //    }
            return respLogin;
        }
    }
}
