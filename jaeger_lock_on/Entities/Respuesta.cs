﻿using System;
using Newtonsoft.Json;

namespace Jaeger.Loggin.Entities
{
    public class Respuesta
    {
        public Respuesta()
        {

        }

        public bool exito { get; set; }
        public Mensajes mensajes { get; set; }

        public static Respuesta Json(string inputString)
        {
            try
            {
                return JsonConvert.DeserializeObject<Respuesta>(inputString);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
    }

    public partial class Mensajes
    {
        public string[] app { get; set; }
    }
}
