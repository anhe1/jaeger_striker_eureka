﻿using System;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;
using Jaeger.Loggin.Helpers;
using Jaeger.Util.Helpers;

namespace Jaeger.Loggin.Entities
{
    public class LocalSynapsis : BasePropertyChangeImplementation
    {
        private readonly JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
        private readonly string fileName = @"C:\Jaeger\jaeger_striker_eureka.json";
        

        [JsonObject]
        public partial class SynapsisBasic
        {
            public SynapsisBasic()
            {
                this.V1 = new LocalDataBase();
                this.V2 = new LocalDataBase();
                this.Recientes = new List<Reciente>();
            }

            [JsonProperty("v1")]
            public LocalDataBase V1 { get; set; }

            [JsonProperty("v2")]
            public LocalDataBase V2 { get; set; }

            [JsonProperty("ftp")]
            public FileTransferProtocol ServerFTP { get; set; }

            [JsonProperty("recientes")]
            public List<Reciente> Recientes { get; set; }

        }

        [JsonObject]
        public class Reciente
        {
            [JsonProperty("rfc")]
            public string RFC { get; set; }

            [JsonProperty("tema")]
            public string Tema { get; set; }
        }

        public LocalSynapsis()
        {
            this.Edita = new SynapsisBasic();
            this.Encrypt = true;
        }

        public bool Encrypt { get; set; }

        public SynapsisBasic Edita { get; set; }

        public bool Load()
        {
            string contenido = "";
            EmbeddedResources horizon = new EmbeddedResources("jaeger_lock_on");
            if (!File.Exists(this.fileName))
                contenido = horizon.GetAsString("Jaeger.Loggin.Recursos.jaeger_striker_eureka.json");
            else
                contenido = this.ReadFileText();
            
            try
            {
                this.Edita = JsonConvert.DeserializeObject<SynapsisBasic>(HelperCsTripleDes.Decrypt(contenido, "", true), this.conf);
                if (Edita == null)
                    this.Edita = JsonConvert.DeserializeObject<SynapsisBasic>(contenido, this.conf);
                return true;
            }
            catch (JsonException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        public void Save()
        {
            this.WriteFileText();
        }

        #region metodos privados

        private string ToJson(Formatting formato = Formatting.None)
        {
            return JsonConvert.SerializeObject(this.Edita, formato, this.conf);
        }

        private string ReadFileText()
        {
            FileInfo info = new FileInfo(this.fileName);
            string empty = string.Empty;
            if (!info.Exists)
            {
                throw new Exception(string.Concat("No existe el archivo: ", this.fileName));
            }
            try
            {
                StreamReader streamReader = File.OpenText(this.fileName);
                empty = streamReader.ReadToEnd();
                streamReader.Close();
                streamReader.Dispose();
                streamReader = null;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return empty;
        }

        private void WriteFileText()
        {
            using (StreamWriter streamWriter = new StreamWriter(this.fileName, false))
            {
                // encriptar informacion
                if (this.Encrypt)
                    streamWriter.Write(HelperCsTripleDes.Encrypt(this.ToJson(), "", true));
                else
                    streamWriter.Write(this.ToJson());
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        #endregion
    }
}
