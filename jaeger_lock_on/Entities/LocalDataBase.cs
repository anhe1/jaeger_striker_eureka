﻿using Newtonsoft.Json;
using Jaeger.Loggin.Helpers;

namespace Jaeger.Loggin.Entities
{
    public class LocalDataBase : BasePropertyChangeImplementation
    {
        private string host;
        private string port;
        private string user;
        private string password;
        private string dataBase;

        [JsonProperty("database")]
        public string Base
        {
            get
            {
                return this.dataBase;
            }
            set
            {
                this.dataBase = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("host")]
        public string Host
        {
            get
            {
                return this.host;
            }
            set
            {
                this.host = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("user")]
        public string User
        {
            get
            {
                return this.user;
            }
            set
            {
                this.user = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("pass")]
        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("port")]
        public string Port
        {
            get
            {
                return this.port;
            }
            set
            {
                this.port = value;
                this.OnPropertyChanged();
            }
        }
    }
}
