﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Jaeger.Loggin.Entities
{
    public class LocalPreferences
    {
        [JsonObject]
        public class Reciente
        {
            [JsonProperty("rfc")]
            public string RFC { get; set; }

            [JsonProperty("tema")]
            public string Tema { get; set; }

            [JsonProperty("user")]
            public string User { get; set; }

            [JsonProperty("pass")]
            public string Password { get; set; }
        }

        [JsonObject]
        public partial class LPreferences
        {
            public LPreferences()
            {
                this.Recientes = new List<Reciente>();
            }

            [JsonProperty("recientes")]
            public List<Reciente> Recientes { get; set; }
        }

        private readonly JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
        private readonly string fileName = @"C:\Jaeger\jaeger_lock_on.json";

        public LocalPreferences()
        {
            this.Local = new LPreferences();
        }

        public LPreferences Local { get; set; }

        public void Agregar(Reciente reciente)
        {
            try
            {
                Reciente si = this.Local.Recientes.Where(it => it.RFC == reciente.RFC).FirstOrDefault();
                if (si == null)
                {
                    this.Local.Recientes.Add(reciente);
                }
                else
                {
                    try
                    {
                        int index = this.Local.Recientes.IndexOf(si);
                        this.Local.Recientes[index] = reciente;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Load()
        {
            if (File.Exists(this.fileName))
            {
                try
                {
                    string contenido = this.ReadFileText();
                    this.Local = JsonConvert.DeserializeObject<LPreferences>(contenido, this.conf);
                    if (Local == null)
                        this.Local.Recientes = new List<Reciente>();
                }
                catch (JsonException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
                this.Local.Recientes = new List<Reciente>();
            }
        }

        public void Save()
        {
            this.WriteFileText();
        }

        private string ToJson(Formatting formato = Formatting.None)
        {
            return JsonConvert.SerializeObject(this.Local, formato, this.conf);
        }

        private string ReadFileText()
        {
            FileInfo info = new FileInfo(this.fileName);
            string empty = string.Empty;
            if (!info.Exists)
            {
                throw new Exception(string.Concat("No existe el archivo: ", this.fileName));
            }
            try
            {
                StreamReader streamReader = File.OpenText(this.fileName);
                empty = streamReader.ReadToEnd();
                streamReader.Close();
                streamReader.Dispose();
                streamReader = null;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return empty;
        }

        private void WriteFileText()
        {
            using (StreamWriter streamWriter = new StreamWriter(this.fileName, false))
            {
                streamWriter.Write(this.ToJson());
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }
    }
}
