﻿using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.Loggin.Interfaces
{
    public interface IEditaLoggin
    {
        /// <summary>
        /// argumentos obtenidos desde la linea de comandos
        /// </summary>
        string[] Arguments { get; set; }

        /// <summary>
        /// configuraciones
        /// </summary>
        Configuracion Configuracion { get; set; }

        /// <summary>
        /// rfc de la empresa registrada
        /// </summary>
        string Empresa { get; set; }

        /// <summary>
        /// nombre del usuario o correo electronico
        /// </summary>
        string Usuario { get; set; }

        /// <summary>
        /// obtener o establecer contraseña 
        /// </summary>
        string Password { get; set; }

        UsuarioDetailModel Piloto { get; set; }
        
        bool Ready { get; set; }
        
        bool Success { get; set; }

        /// <summary>
        /// 
        /// </summary>
        string Message { get; set; }

        void ExecuteCommandLine();
        
        bool ExecuteLogin();
        
        void Load();
        
        void Save();

        string Recuperar();
    }
}