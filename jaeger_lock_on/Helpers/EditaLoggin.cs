﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Kaiju;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Kaiju.Entities;
using Jaeger.Edita.Interfaces;
using Jaeger.Edita.V2;
using Jaeger.Edita.V2.Empresa.Entities;
using Jaeger.Loggin.Entities;
using Jaeger.Loggin.Interfaces;

namespace Jaeger.Loggin.Helpers {
    public class EditaLoggin : BasePropertyChangeImplementation, IEditaLoggin {
        private readonly LocalSynapsis synapsis;
        private ISqlConfiguracionEmpresas serverJaeger;
        private UsuarioDetailModel piloto;
        private string empresa;
        private string usuario;
        private string password;
        private string message;
        private string[] arguments;
        private bool ready = false;
        private bool success = false;

        public EditaLoggin() {
            this.message = "Servicio bloqueado. No se ha podido establecer la conexión con el host remoto.";
            this.synapsis = new LocalSynapsis();
            this.Configuracion = new Configuracion();
        }

        public string Empresa {
            get {
                return this.empresa;
            }
            set {
                this.empresa = value.Trim().ToUpper().Replace(";", "");
                this.OnPropertyChanged();
            }
        }

        public string Usuario {
            get {
                return this.usuario;
            }
            set {
                this.usuario = value;
                this.OnPropertyChanged();
            }
        }

        public string Password {
            get {
                return this.password;
            }
            set {
                this.password = value;
                this.OnPropertyChanged();
            }
        }

        public string Message {
            get {
                return this.message;
            }
            set {
                this.message = value;
                this.OnPropertyChanged();
            }
        }

        public string[] Arguments {
            get {
                return this.arguments;
            }
            set {
                this.arguments = value;
                this.OnPropertyChanged();
            }
        }

        public bool Ready {
            get {
                return this.ready;
            }
            set {
                this.ready = value;
                this.OnPropertyChanged();
            }
        }

        public bool Success {
            get {
                return this.success;
            }
            set {
                this.success = value;
                this.OnPropertyChanged();
            }
        }

        public void Load() {
            this.synapsis.Load();
        }

        public void Save() {
            this.synapsis.Save();
        }

        public Configuracion Configuracion { get; set; }

        public UsuarioDetailModel Piloto {
            get {
                return this.piloto;
            }
            set {
                this.piloto = value;
                this.OnPropertyChanged();
            }
        }

        public bool ExecuteLogin() {
            this.serverJaeger = new MySqlConfiguracionEmpresas(
                        new DataBaseConfiguracion {
                            Database = this.synapsis.Edita.V2.Base,
                            HostName = this.synapsis.Edita.V2.Host,
                            UserID = this.synapsis.Edita.V2.User,
                            Password = this.synapsis.Edita.V2.Password,
                            PortNumber = int.Parse(this.synapsis.Edita.V2.Port)
                        });

            ViewModelEmpresa confEdita = serverJaeger.GetByRFC(this.empresa);
            if (confEdita != null) {
                var usuarios = new SqlSugarUsuarioRepository(confEdita.Synapsis.BaseDatos.Edita);

                this.Piloto = usuarios.GetUsuario(this.Usuario, Jaeger.Helpers.HelperValidacion.EsCorreo(this.usuario));
                if (this.Piloto != null) {
                    this.Piloto.Password = "$2a" + this.Piloto.Password.Substring(3, this.Piloto.Password.Length - 3);
                    this.Success = BCrypt.Net.BCrypt.Verify(this.password, this.Piloto.Password);
                    this.message = "";
                    if (this.success) {
                        // aqui asignamos la información de la configuracion de EDITA
                        Configuracion.Empresa.Clave = confEdita.Clave;
                        Configuracion.Empresa.RFC = confEdita.RFC;
                        Configuracion.Empresa.RazonSocial = confEdita.Nombre;
                        Configuracion.Amazon = confEdita.Synapsis.Amazon;
                        // proveedores de certificacion
                        Configuracion.ProveedorAutorizado = confEdita.Synapsis.PAC;
                        Configuracion.ProveedorAutorizado.Certificacion.RFC = confEdita.RFC;
                        Configuracion.ProveedorAutorizado.Cancelacion.RFC = confEdita.RFC;
                        Configuracion.ProveedorAutorizado.Validacion.RFC = confEdita.RFC;
                        // informacion de la base de datos
                        Configuracion.RDS = confEdita.Synapsis.BaseDatos;

                        SqlSugarConfiguracion serverIterno = new SqlSugarConfiguracion(Configuracion.RDS.Edita);
                        EmpresaData confEdita2 = serverIterno.GetSynapsis();

                        if (confEdita2 != null) {
                            //configuracion.Customer.Clave = confEdita2.General.Clave;
                            Configuracion.Empresa.RegistroPatronal = confEdita2.General.RegistroPatronal;
                            Configuracion.Empresa.RegimenFiscal = confEdita2.General.RegimenFiscal;
                            //Configuracion.Empresa.DomicilioFiscal = confEdita2.DomicilioFiscal;
                        }
                    } else {
                        this.Message = "Usuario ó Contraseña no validos, verifique la información y vuelva a intentar.";
                    }
                    return this.success;
                }
            }
            this.success = false;
            this.Message = "No se reconoce el usuario ó empresa referida, verifique la información y vuelva a intentar.";
            return false;
        }

        public void ExecuteCommandLine() {
            List<ParseCommand.CommandLine> args = ParseCommand.ParseCommandLine(string.Join(" ", this.arguments), false);
            List<ParseCommand.CommandLine>.Enumerator enumerator = new List<ParseCommand.CommandLine>.Enumerator();
            try {
                enumerator = args.GetEnumerator();
                while (enumerator.MoveNext()) {
                    ParseCommand.CommandLine commandItem = enumerator.Current;
                    string lower = commandItem.Name.ToLower();
                    if (lower == "start") {
                        this.ExecuteLogin(ParseCommand.ParseCommandLine(commandItem.Value.ToString()));
                    } else if (lower == "help") {
                        Console.WriteLine("help: No hay!");
                    }
                }
            } finally {
                ((IDisposable)enumerator).Dispose();
            }
        }

        public string Recuperar() {
            this.serverJaeger = new SqlSugarConfiguracionEmpresas(
                        new DataBaseConfiguracion {
                            Database = this.synapsis.Edita.V2.Base,
                            HostName = this.synapsis.Edita.V2.Host,
                            UserID = this.synapsis.Edita.V2.User,
                            Password = this.synapsis.Edita.V2.Password,
                            PortNumber = int.Parse(this.synapsis.Edita.V2.Port)
                        });

            ViewModelEmpresa confEdita = serverJaeger.GetByRFC(this.empresa);

            return confEdita.Clave;
        }

        private void ExecuteLogin(List<ParseCommand.CommandLine> args) {
            List<ParseCommand.CommandLine>.Enumerator enumerator = new List<ParseCommand.CommandLine>.Enumerator();
            try {
                enumerator = args.GetEnumerator();
                while (enumerator.MoveNext()) {
                    ParseCommand.CommandLine objeto = enumerator.Current;
                    string lower = objeto.Name.ToLower();
                    if (lower == "u") {
                        this.Usuario = objeto.Value;
                    } else if (lower == "p") {
                        this.Password = objeto.Value;
                    } else if (lower == "e") {
                        this.Empresa = objeto.Value;
                    }
                }
            } finally {
                ((IDisposable)enumerator).Dispose();
                this.Ready = true;
            }
        }
    }
}
