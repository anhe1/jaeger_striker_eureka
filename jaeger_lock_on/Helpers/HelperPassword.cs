﻿using System;

namespace Jaeger.Loggin.Helpers
{
    public class HelperPassword
    {
        public static string Password(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }
    }
}
