﻿using Jaeger.Nomina.Entities;
using System.Collections.Generic;
using System.ComponentModel;

namespace Jaeger.Nomina.Interface
{
    public interface ISqlEmpleados
    {
        /// <summary>
        /// insertar lista de objetos empleado
        /// </summary>
        int Insert(ViewModelEmpleado[] data);

        ViewModelEmpleado Save(ViewModelEmpleado item);

        /// <summary>
        /// obtener lista de empleados
        /// </summary>
        List<ViewModelEmpleado> GetList(bool activos = true);

        /// <summary>
        /// procedimiento para crear tabla de catalogo de empleados
        /// </summary>
        void CreateTable();
    }
}
