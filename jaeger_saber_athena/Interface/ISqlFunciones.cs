﻿using Jaeger.Nomina.Entities;
using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Nomina.Interface
{
    public interface ISqlFunciones
    {
        BindingList<ViewModelDeptoFuncion> GetLitBy(int index, bool activo = true);

        ViewModelDeptoFuncion Save(ViewModelDeptoFuncion funcion);
    }
}
