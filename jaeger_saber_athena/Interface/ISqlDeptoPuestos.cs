﻿using Jaeger.Nomina.Entities;
using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Nomina.Interface
{
    public interface ISqlDeptoPuestos
    {
        ISqlFunciones Funciones { get; set; }

        int Insert(ViewModelDeptoPuesto item);

        int Update(ViewModelDeptoPuesto item);

        BindingList<ViewModelDeptoPuesto> GetListBy(int index, bool activo = true);

        ViewModelDeptoPuesto Save(ViewModelDeptoPuesto puesto);

        bool Create();
    }
}
