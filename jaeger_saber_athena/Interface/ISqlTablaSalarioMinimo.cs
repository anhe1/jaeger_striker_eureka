﻿using Jaeger.Nomina.Entities;
using System.Collections.Generic;
using System.ComponentModel;

namespace Jaeger.Nomina.Interface
{
    public interface ISqlTablaSalarioMinimo
    {
        IEnumerable<ViewModelTablaSalarioMinimo> GetList();

        BindingList<ViewModelTablaSalarioMinimo> Save(BindingList<ViewModelTablaSalarioMinimo> items);

        bool Create();
    }
}
