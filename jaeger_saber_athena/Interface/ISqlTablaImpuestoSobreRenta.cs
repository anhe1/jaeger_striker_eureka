﻿using Jaeger.Nomina.Entities;
using System.Collections.Generic;

namespace Jaeger.Nomina.Interface
{
    public interface ISqlTablaImpuestoSobreRenta
    {
        IEnumerable<ViewModelTablaImpuestoSobreRenta> GetList();

        ViewModelTablaImpuestoSobreRenta GetListBy(int periodo);
    }
}
