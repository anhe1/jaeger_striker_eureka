﻿using Jaeger.Nomina.Entities;
using System.Collections.Generic;
using System.ComponentModel;

namespace Jaeger.Nomina.Interface
{
    public interface ISqlTablaISR
    {
        IEnumerable<ViewModelTablaImpuestoSobreRenta> GetList();

        BindingList<ViewModelTablaImpuestoSobreRenta> Save(BindingList<ViewModelTablaImpuestoSobreRenta> items);

        bool Create();
    }
}
