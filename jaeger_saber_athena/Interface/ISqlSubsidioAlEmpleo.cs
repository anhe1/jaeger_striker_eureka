﻿using Jaeger.Nomina.Entities;
using System.Collections.Generic;
using System.ComponentModel;

namespace Jaeger.Nomina.Interface
{
    public interface ISqlSubsidioAlEmpleo
    {
        IEnumerable<ViewModelTablaSubsidioAlEmpleo> GetList();

        /// <summary>
        /// obtener tabla del subsidio al empleo por el periodo
        /// </summary>
        /// <param name="periodo">periodo</param>
        /// <returns>ViewModelTablaSubsidioAlEmpleo</returns>
        ViewModelTablaSubsidioAlEmpleo GetListBy(int periodo);

        BindingList<ViewModelTablaSubsidioAlEmpleo> Save(BindingList<ViewModelTablaSubsidioAlEmpleo> items);

        bool Create();
    }
}
