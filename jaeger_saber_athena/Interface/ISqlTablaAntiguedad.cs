﻿using Jaeger.Edita.Entities;
using Jaeger.Nomina.Entities;
using System.Collections.Generic;
using System.ComponentModel;

namespace Jaeger.Nomina.Interface
{
    /// <summary>
    /// interface para tabla de antiguedad
    /// </summary>
    public interface ISqlTablaAntiguedad
    {
        

        IEnumerable<ViewModelAntiguedad> GetList();

        /// <summary>
        /// almacenar tabla de antiguedades
        /// </summary>
        /// <param name="tabla">List<ViewModelAntiguedad></param>
        BindingList<ViewModelAntiguedad> Save(BindingList<ViewModelAntiguedad> tabla);

        bool Create();
    }
}
