﻿using System;

namespace Jaeger.Nomina.Interface
{
    public interface IClaveSATVencimiento
    {
        string Clave { get; set; }

        string Descripcion { get; set; }

        DateTime FechaInicioVigencia { get; set; }

        DateTime? FechaFinVigencia { get; set; }
    }
}
