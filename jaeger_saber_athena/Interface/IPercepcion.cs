﻿/// <summary>
/// develop: antonio hernandez 141120181506
/// pupose: interface para percepciones
/// </summary>

namespace Jaeger.Nomina.Interface
{
    public interface IPercepcion
    {
        /// <summary>
        /// obtener o establecer identificador unico de la percepcion
        /// </summary>
        string Identificador { get; set; }

        /// <summary>
        /// 
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// 
        /// </summary>
        string Tipo { get; set; }

        /// <summary>
        /// obtener o establacer el concepto de la percepcion
        /// </summary>
        string Concepto { get; set; }

        /// <summary>
        /// obtener o establacer el monto del importe exento para la percepcion
        /// </summary>
        decimal ImporteExento { get; set; }

        /// <summary>
        /// obtener o establecer el monto del importe gravado para la percepcion
        /// </summary>
        decimal ImporteGravado { get; set; }

        /// <summary>
        /// representa el importe total de la percepcion sumando la parte gravada y exenta
        /// </summary>
        decimal ImporteTotal { get; }

    }
}