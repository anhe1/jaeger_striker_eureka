﻿
namespace Jaeger.Nomina.Interface
{
    public interface IPersona
    {
        string CURP { get; set; }

        string RFC { get; set; }

        string Nombre { get; set; }

        string PrimerApellido { get; set; }

        string SegundoApellido { get; set; }

        string Telefono { get; set; }

        string Correo { get; set; }

        string Sitio { get; set; }

        byte[] Avatar { get; set; }
    }
}
