﻿/// <summary>
/// develop: antonio hernandez 141120181506
/// pupose: interface para percepciones
/// </summary>

namespace Jaeger.Nomina.Interface
{
    public interface IDeduccion
    {
        string Identificador { get; set; }
        string Clave { get; set; }
        string Tipo { get; set; }
        string Concepto { get; }
        decimal Importe { get; set; }
    }
}