﻿using Jaeger.Nomina.Entities;
using System.Collections.Generic;
using System.ComponentModel;

namespace Jaeger.Nomina.Interface
{
    public interface ISqlTablaUMA
    {
        IEnumerable<ViewModelTablaUMA> GetList();

        ViewModelTablaUMA GetByActivo();

        BindingList<ViewModelTablaUMA> Save(BindingList<ViewModelTablaUMA> items);

        bool Create();
    }
}
