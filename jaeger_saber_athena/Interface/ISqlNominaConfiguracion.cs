﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Nomina.Interface
{
    public interface ISqlNominaConfiguracion
    {
        Entities.Configuracion LoadConf();

        bool Create(Entities.Configuracion objeto);

        bool Save(Entities.Configuracion objeto);

        bool CrearSistema();
    }
}
