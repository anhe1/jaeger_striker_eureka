﻿
namespace Jaeger.Nomina.Interface
{
    /// <summary>
    /// interface para las claves SAT
    /// </summary>
    public interface IClaveSAT
    {
        string Clave { get; set; }
        string Descripcion { get; set; }
    }
}
