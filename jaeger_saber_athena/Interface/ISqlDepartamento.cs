﻿using Jaeger.Nomina.Entities;
using System.ComponentModel;
using System.Collections.Generic;

namespace Jaeger.Nomina.Interface
{
    public interface ISqlDepartamento
    {
        ISqlDeptoPuestos Puestos { get; set; }

        ViewModelDepartamento Save(ViewModelDepartamento objeto);

        BindingList<ViewModelDepartamento> Save(BindingList<ViewModelDepartamento> datos);

        IEnumerable<ViewModelDepartamento> GetList();

        BindingList<ViewModelDepartamento> GetListBy();

        bool Create();
    }
}
