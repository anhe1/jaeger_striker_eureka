﻿using System.ComponentModel;

namespace Jaeger.Nomina.Enums
{
    public enum EnumFormaPago
    {
        [Description("No definido")]
        NoDefinido,
        [Description("Efectivo")]
        Efectivo,
        [Description("Cheque")]
        Cheque,
        [Description("Tarjeta")]
        Tarjeta
    }
}
