namespace Jaeger.Nomina.Enums
{
    public enum EnumEstadoCivil
    {
        Soltero,
        Casado
    }
}