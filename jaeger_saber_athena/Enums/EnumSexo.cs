﻿using System.ComponentModel;

namespace Jaeger.Nomina.Enums
{
        public enum EnumSexo
        {
            [Description("Hombre")]
            Hombre = 'H',
            [Description("Mujer")]
            Mujer = 'M'
        }
}
