﻿using System.ComponentModel;

namespace Jaeger.Nomina.Enums
{
    public enum EnumTipoCuentaBancaria
    {
        [Description("Tarjeta")]
        Tarjeta,
        [Description("Numero Cuenta")]
        Cuenta,
        [Description("Cuenta de Cheques")]
        CuentaCheques
    }
}
