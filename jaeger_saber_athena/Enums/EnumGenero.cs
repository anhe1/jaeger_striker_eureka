﻿using System.ComponentModel;

namespace Jaeger.Nomina.Enums
{
    public enum EnumGenero
    {
        [Description("Masculino")]
        Masculino,
        [Description("Femenino")]
        Femenino
    }
}
