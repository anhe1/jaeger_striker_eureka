namespace Jaeger.Nomina.Enums
{
    public enum EnumBaseCotizacion
    {
        Fijo,
        Variable,
        Mixto
    }
}