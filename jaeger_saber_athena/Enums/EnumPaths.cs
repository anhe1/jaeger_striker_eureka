﻿namespace Jaeger.Nomina.Enums
{
    public enum EnumPaths
    {
        Accuse,
        Catalogos,
        Comprobantes,
        Downloads,
        Google,
        Log,
        Media,
        Reportes,
        Repositorio,
        Resources,
        SAT,
        Templates,
        Temporal
    }
}