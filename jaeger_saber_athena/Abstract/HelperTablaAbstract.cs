﻿using System;
using System.Linq;
using Jaeger.Edita.V2.Empresa.Entities;
using SqlSugar;

namespace Jaeger.Nomina.Abstract
{
    public class HelperTablaAbstract : Jaeger.Edita.Helpers.HelperMySql
    {
        private SqlSugarClient dbase;
        private DataBaseConfiguracion settingDataBase;

        public HelperTablaAbstract(DataBaseConfiguracion objeto)
            : base(objeto)
        {
            this.settingDataBase = objeto;
            this.dbase = new SqlSugarClient(new ConnectionConfig()
            {
                ConnectionString = Jaeger.Edita.Helpers.HelperMySql.StringBuilderToString(this.Settings),
                DbType = DbType.MySql,
                InitKeyType = SqlSugar.InitKeyType.Attribute,
                IsAutoCloseConnection = true
            });
            this.dbase.Ado.IsEnableLogEvent = true;
            this.dbase.Aop.OnLogExecuted = (sql, pars) =>
            {
                Console.WriteLine(sql + "\r\n" + dbase.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value)));
                Console.WriteLine();
            };
        }

        public SqlSugarClient Db
        {
            get
            {
                return this.dbase;
            }
            set
            {
                this.dbase = value;
            }
        }

        public DataBaseConfiguracion DataBase
        {
            get
            {
                return this.settingDataBase;
            }
            set
            {
                this.settingDataBase = value;
            }
        }
    }
}
