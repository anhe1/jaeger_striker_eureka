﻿using System;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Nomina.Entities;
using Jaeger.Nomina.Interface;

namespace Jaeger.Nomina.Helpers
{
    public class SqlSugarNominaConfiguracion : MySqlSugarContext<ViewModelNominaConfiguracion>, ISqlNominaConfiguracion
    {
        private string etiqueta = "nom12";

        public SqlSugarNominaConfiguracion(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public Entities.Configuracion LoadConf()
        {
            ViewModelNominaConfiguracion temp = this.Db.Queryable<ViewModelNominaConfiguracion>().Where(it => it.Key == etiqueta).Single();
            if (temp != null)
            {
                return temp.Configuracion;
            }
            return null;
        }

        public bool Create(Entities.Configuracion objeto)
        {
            ViewModelNominaConfiguracion temp = new ViewModelNominaConfiguracion();
            temp.Key = etiqueta;
            temp.Configuracion = objeto;
            return this.Db.Insertable(temp).ExecuteCommand() > 0;
        }

        public bool Save(Entities.Configuracion objeto)
        {
            ViewModelNominaConfiguracion temp = new ViewModelNominaConfiguracion();
            temp.Key = etiqueta;
            temp.Configuracion = objeto;
            return this.Db.Updateable(temp).ExecuteCommand() > 0;
        }

        /// <summary>
        /// crear sistema de tablas para nomina ViewModelTablaSubsidioAlEmpleo, ViewModelAntiguedad, ViewModelTablaImpuestoSobreRenta, ViewModelTablaImpuestoSobreRenta, ViewModelTablaProductividad, ViewModelTablaSalarioMinimo, ViewModelTablaUMA
        /// </summary>
        /// <returns></returns>
        public bool CrearSistema()
        {
            try
            {
                this.Db.CodeFirst.InitTables<ViewModelTablaSubsidioAlEmpleo, ViewModelAntiguedad, ViewModelTablaImpuestoSobreRenta, ViewModelEmpleado>();
                this.Db.CodeFirst.InitTables<ViewModelTablaImpuestoSobreRenta, ViewModelTablaProductividad, ViewModelTablaSalarioMinimo, ViewModelTablaUMA>();
                this.Db.CodeFirst.InitTables<Jaeger.Nomina.Entities.ViewModelDepartamento, Jaeger.Nomina.Entities.ViewModelDeptoFuncion, Jaeger.Nomina.Entities.ViewModelDeptoPuesto, Jaeger.Nomina.Entities.ViewModelEmpleado>();
                this.Db.CodeFirst.InitTables<ViewModelNominaControl, ViewModelNominaPeriodo>();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("SqlSugarNominaConfiguracion: " + ex.Message);
                return false;
            }
        }
    }
}
