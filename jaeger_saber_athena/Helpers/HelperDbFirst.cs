﻿using System;
using System.Linq;
using Jaeger.Edita.V2.Empresa.Entities;
using SqlSugar;

namespace Jaeger.Nomina.Helpers
{
    public class HelperDbFirst : Jaeger.Edita.Helpers.HelperMySql
    {
        private readonly SqlSugarClient db;

        public HelperDbFirst(DataBaseConfiguracion objeto)
            : base(objeto)
        {
            this.db = new SqlSugarClient(new ConnectionConfig()
            {
                ConnectionString = Jaeger.Edita.Helpers.HelperMySql.StringBuilderToString(this.Settings),
                DbType = DbType.MySql,
                InitKeyType = SqlSugar.InitKeyType.Attribute,
                IsAutoCloseConnection = true
            });
            this.db.Ado.IsEnableLogEvent = true;
            this.db.Aop.OnLogExecuted = (sql, pars) =>
            {
                Console.WriteLine(sql + "\r\n" + db.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value)));
                Console.WriteLine();
            };
        }

        public void prueba()
        {
            this.db.DbFirst.Where("_nmnprt").IsCreateAttribute().CreateClassFile("C:\\Jaeger\\Jaeger.Temporal");
        }
    }
}
