﻿/// develop: anhe 18062019 1715
/// purpose: control 

using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Edita.V2.Nomina.Entities;

namespace Jaeger.Nomina.Helpers
{
    public class SqlSugarNominaControl : MySqlSugarContext<ViewModelControlNominaSingle>
    {
        public SqlSugarNominaControl(DataBaseConfiguracion configuracion)
            : base(configuracion)
        {
        }
    }
}
