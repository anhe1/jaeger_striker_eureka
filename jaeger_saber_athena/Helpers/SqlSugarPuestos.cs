﻿using System.ComponentModel;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Nomina.Entities;
using Jaeger.Nomina.Interface;

/// <summary>
/// develop: anhe 23082338
/// purpose: 
/// </summary>
namespace Jaeger.Nomina.Helpers
{
    public class SqlSugarDeptoPuestos : MySqlSugarContext<ViewModelDeptoPuesto>, ISqlDeptoPuestos
    {
        public SqlSugarDeptoPuestos(DataBaseConfiguracion configuracion) : base(configuracion)
        {
            this.Funciones = new SqlSugarFunciones(configuracion);
        }

        public ISqlFunciones Funciones { get; set; }

        /// <summary>
        /// obtener listado de puestos relacionados al indice del departamento
        /// </summary>
        /// <param name="index">indice del departamento</param>
        /// <param name="activo">falso si debe obtener registros no activos</param>
        /// <returns></returns>
        public BindingList<ViewModelDeptoPuesto> GetListBy(int index, bool activo = true)
        {
            return new BindingList<ViewModelDeptoPuesto>(this.Db.Queryable<ViewModelDeptoPuesto>().Where(it => it.IdDepto == index).WhereIF(activo, it => it.Activo == true).OrderBy(it => it.Secuencia, SqlSugar.OrderByType.Asc).ToList());
        }

        public ViewModelDeptoPuesto Save(ViewModelDeptoPuesto puesto)
        {
            if (puesto.Id == 0)
                puesto.Id = this.Insert(puesto);
            else
                this.Update(puesto);
            return puesto;
        }

        public bool Create()
        {
            try
            {
                this.Db.CodeFirst.InitTables(typeof(ViewModelDeptoPuesto));
                return true;
            }
            catch (SqlSugar.VersionExceptions ex)
            {
                //this.Message = new Edita.Entities.SqlSugarMessage() { Value = ex.Message };
            }

            return false;
        }
    }
}
