﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Nomina.Entities;
using Jaeger.Nomina.Interface;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Nomina.Helpers
{
    public class SqlSugarEmpleados : MySqlSugarContext<ViewModelEmpleado>, ISqlEmpleados
    {
        public SqlSugarEmpleados(DataBaseConfiguracion configuracion) : base(configuracion)
        {
        }

        /// <summary>
        /// insertar lista de objetos empleado
        /// </summary>
        public int Insert(ViewModelEmpleado[] data)
        {
            return this.Db.Insertable(data).ExecuteCommand();
        }

        public ViewModelEmpleado Save(ViewModelEmpleado item)
        {
            if (item.Id == 0)
            {
                item.Id = this.Insert(item);
            }
            else
            {
                item.FechaModifica = DateTime.Now;
                this.Update(item);
            }
            return item;
        }

        /// <summary>
        /// obtener lista de empleados
        /// </summary>
        public List<ViewModelEmpleado> GetList(bool activos = true)
        {
            if (activos)
                return this.Db.Queryable<ViewModelEmpleado>().Where(it => it.IsActive == true).OrderBy(o => o.PrimerApellido).ToList();
            else
                return this.Db.Queryable<ViewModelEmpleado>().ToList();
        }

        /// <summary>
        /// procedimiento para crear tabla de catalogo de empleados
        /// </summary>
        public void CreateTable()
        {
            this.Db.CodeFirst.InitTables(typeof(ViewModelEmpleado));
        }
    }
}
