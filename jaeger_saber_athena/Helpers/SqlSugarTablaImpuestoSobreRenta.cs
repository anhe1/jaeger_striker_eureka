﻿using System.ComponentModel;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Nomina.Entities;
using Jaeger.Nomina.Interface;

namespace Jaeger.Nomina.Helpers
{
    public class SqlSugarTablaImpuestoSobreRenta : MySqlSugarContext<ViewModelTablaImpuestoSobreRenta>, ISqlTablaImpuestoSobreRenta
    {
        public SqlSugarTablaImpuestoSobreRenta(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        //public BindingList<ViewModelTablaImpuestoSobreRenta> GetList()
        //{
        //    return new BindingList<ViewModelTablaImpuestoSobreRenta>(this.Db.Queryable<ViewModelTablaImpuestoSobreRenta>().ToList());
        //}

        public ViewModelTablaImpuestoSobreRenta GetListBy(int periodo)
        {
            return this.Db.Queryable<ViewModelTablaImpuestoSobreRenta>().Where(it => it.Periodo == periodo).ToList()[0];
        }
    }
}
