using System.ComponentModel;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Nomina.Entities;
using Jaeger.Nomina.Interface;

namespace Jaeger.Nomina.Helpers
{
    public class SqlSugarTablaAntiguedad : MySqlSugarContext<ViewModelAntiguedad>, ISqlTablaAntiguedad
    {
        public SqlSugarTablaAntiguedad(DataBaseConfiguracion configuracion) : base(configuracion)
        {
        }

        public bool Create() {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// almacenar tabla de antiguedades
        /// </summary>
        /// <param name="tabla">List<ViewModelAntiguedad></param>
        public BindingList<ViewModelAntiguedad> Save(BindingList<ViewModelAntiguedad> tabla)
        {
            for (int i = 0; i < tabla.Count; i++)
            {
                if (tabla[i].Id == 0)
                {
                    tabla[i].Id = this.Db.Insertable(tabla[i]).ExecuteReturnIdentity();
                }
                else
                {
                    this.Db.Updateable(tabla[i]).ExecuteCommand();
                }
            }
            return tabla;
        }
    }
}