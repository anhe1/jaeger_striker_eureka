﻿/// develop:
/// purpose: calcular salario diario integrado

using System;
using Jaeger.Nomina.Entities;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Helpers
{
    public class SalarioDiarioIntegrado : BasePropertyChangeImplementation
    {
        private Configuracion configuracion;
        private DateTime fechaIngresoField;

        public SalarioDiarioIntegrado(ref Configuracion objeto, DateTime fechaIngreso)
        {
            this.configuracion = objeto;
            this.fechaIngresoField = fechaIngreso;
        }

        public Configuracion Configuracion
        {
            get
            {
                return this.configuracion;
            }
            set
            {
                this.configuracion = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime FechaIngreso
        {
            get
            {
                return this.fechaIngresoField;
            }
            set
            {
                this.fechaIngresoField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal FactorIntegracion
        {
            get
            {
                return Math.Round(1 + this.ParteProporcionalDiarioAguinaldo + this.ParteProporcionalDiariaPrimaVacacional, 4);
            }
        }

        public decimal ParteProporcionalDiarioAguinaldo
        {
            get
            {
                return Math.Round((this.configuracion.DiasAguinaldo / this.configuracion.DiasPorAnio), 4);
            }
        }

        public decimal ParteProporcionalDiariaPrimaVacacional
        {
            get
            {
                return Math.Round((this.DiasAcumuladosVacaciones * this.configuracion.PrimaVacacional) / this.configuracion.DiasPorAnio, 4);
            }
        }

        public int DiasAcumuladosVacaciones
        {
            get
            {
                int anios = DateTime.Now.Year - this.fechaIngresoField.Year;
                int dias = 6;
                if (anios == 1 | anios == 0)
                {
                    dias = 6;
                }
                else if (anios == 2)
                {
                    dias = 8;
                }
                else if (anios == 3)
                {
                    dias = 10;
                }
                else if (anios == 4)
                {
                    dias = 12;
                }
                else if (anios >= 5 & anios <= 14)
                {
                    dias = 14;
                }
                else if (anios >= 15 & anios <= 19)
                {
                    dias = 18;
                }
                else if (anios >= 20 & anios <= 24)
                {
                    dias = 20;
                }
                else if (anios >= 25 & anios <= 29)
                {
                    dias = 22;
                }
                else if (anios >= 30 & anios <= 34)
                {
                    dias = 24;
                }
                return dias;
            }
        }
    }
}