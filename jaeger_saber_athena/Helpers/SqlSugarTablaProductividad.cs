﻿using System;
using System.Linq;
using System.ComponentModel;
using Jaeger.Nomina.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Nomina.Helpers
{
    public class SqlSugarTablaProductividad : MySqlSugarContext<ViewModelTablaProductividad>
    {
        private BindingList<ViewModelTablaProductividad> tablas;

        public SqlSugarTablaProductividad(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public BindingList<ViewModelTablaProductividad> Tablas
        {
            get
            {
                return this.tablas;
            }
            set
            {
                this.tablas = value;
            }
        }

        public BindingList<ViewModelTablaProductividad> GetTablas()
        {
            try
            {
                this.tablas = new BindingList<ViewModelTablaProductividad>(this.Db.Queryable<ViewModelTablaProductividad>().OrderBy(it => it.Descripcion).ToList());
            }
            catch (Exception)
            {
                this.tablas = null;
            }
            if (this.tablas == null)
            {
                this.tablas = new BindingList<ViewModelTablaProductividad>();
            }
            return this.tablas;
        }

        public bool CrearTabla()
        {
            try
            {
                this.Db.CodeFirst.InitTables(typeof(ViewModelTablaProductividad));
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public void Save()
        {
            for (int i = 0; i < tablas.Count; i++)
            {
                if (this.tablas[i].Id == 0)
                {
                    this.tablas[i].Id = this.Db.Insertable(this.tablas[i]).ExecuteReturnIdentity();
                }
                else
                {
                    this.Db.Updateable(this.tablas[i]).ExecuteCommand();
                }
            }
        }
    }
}
