﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using SqlSugar;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Nomina.Entities;

namespace Jaeger.Nomina.Helpers
{
    public class HelperNominaCalcular : Jaeger.DataAccess.Abstractions.MySqlSugarContext<ViewModelNominaControl>
    {
        private DataBaseConfiguracion DataBase;
        private Jaeger.Nomina.Entities.Configuracion configuracion;
        private ViewModelNominaControl periodo;
        private SqlSugarTablaProductividad tablaProductividad;
        private readonly SqlSugarNominaConfiguracion conf;

        public HelperNominaCalcular(ref Entities.Configuracion conf, DataBaseConfiguracion objeto) : base(objeto)
        {
            this.DataBase = objeto;
            this.configuracion = conf;
            this.periodo = new ViewModelNominaControl();
        }

        public HelperNominaCalcular(DataBaseConfiguracion objeto) : base(objeto)
        {
            this.conf = new SqlSugarNominaConfiguracion(objeto);
        }

        #region propiedades
     
        public ViewModelNominaControl Periodo
        {
            get
            {
                return this.periodo;
            }
            set
            {
                this.periodo = value;
            }
        }

        #endregion

        public void CrearTabla()
        {
            this.Db.CodeFirst.InitTables(typeof(ViewModelNominaControl));
            this.Db.CodeFirst.InitTables(typeof(ViewModelNominaPeriodo));
        }

        public ViewModelNominaControl Insert(ViewModelNominaControl objeto)
        {
            objeto.Id = this.Db.Insertable(objeto).ExecuteReturnIdentity();
            return objeto;
        }

        public ViewModelNominaControl Update(ViewModelNominaControl objeto)
        {
            this.Db.Updateable(objeto).ExecuteCommand();
            return objeto;
        }

        public bool Remove(ViewModelNominaControl objeto)
        {
            return false;
        }

        public ViewModelNominaPeriodo Insert(ViewModelNominaPeriodo e)
        {
            e.Id = this.Db.Insertable(e).ExecuteReturnIdentity();
            return e;
        }

        public bool Update(ViewModelNominaPeriodo e)
        {
            return this.Db.Updateable(e).ExecuteCommand() > 0;
        }

        public bool Remove(ViewModelNominaPeriodo e)
        {
            if (e.Id > 0)
            {
                e.Activo = false;
                return this.Update(e);
            }
            else
            {
                return this.periodo.Empleados.Remove(e);
            }
        }

        public void CargarNomina(int indice)
        {
            ViewModelNominaControl temp = this.Db.Queryable<ViewModelNominaControl>().Where(it => it.Id == indice).Single();
            if (temp != null)
            {
                this.Periodo.Descripcion = temp.Descripcion;
                this.Periodo.Id = temp.Id;
                this.Periodo.FechaInicio = temp.FechaInicio;
                this.Periodo.FechaFin = temp.FechaFin;
                this.Periodo.Estado = temp.Estado;
                this.Periodo.FechaPago = temp.FechaPago;
                this.Periodo.TipoNomina = temp.TipoNomina;
                this.Periodo.TipoPeriodo = temp.TipoPeriodo;
                this.Periodo.Activo = temp.Activo;

                var tabla = this.Db.Queryable<ViewModelNominaPeriodo, ViewModelEmpleado>((n, e) => new object[]
                {
                    JoinType.Left, n.EmpleadoId == e.Id
                }).Where((n) => n.ControlId == indice && n.Activo == true).ToDataTable();
                var mapper = new Jaeger.Domain.Services.Mapping.DataNamesMapper<ViewModelNominaPeriodo>();
                
                // empleados
                //List<ViewModelNominaPeriodo> lista = this.Db.Queryable<ViewModelNominaPeriodo, ViewModelEmpleado>((n, e) => new object[]
                //{
                //    JoinType.Left, n.EmpleadoId == e.Id
                //}).Where((n) => n.ControlId == indice && n.Activo == true).Select((n, e) => new ViewModelNominaPeriodo
                //{
                //    Id = n.Id,
                //    Activo = n.Activo,
                //    EmpleadoId = e.Id,
                //    JPercepciones = n.JPercepciones,
                //    JDeducciones = n.JDeducciones,
                //    JCuotasObrero = n.JCuotasObrero,
                //    JCuotasPatronales = n.JCuotasPatronales,
                //    Clave = e.Clave,
                //    Numero = e.Num,
                //    Nombres = e.Nombre,
                //    SegundoApellido = e.SegundoApellido,
                //    PrimerApellido = e.PrimerApellido,
                //    RFC = e.RFC,
                //    CURP = e.CURP,
                //    JornadasTrabajo = e.JornadasTrabajo,
                //    SalarioDiario = e.SalarioDiario,
                //    SalarioDiarioIntegrado = e.SalarioDiarioIntegrado,
                //    FechaIngreso = e.FechaInicioRelLaboral,
                //    PremiosPuntualidad = e.PremiosPuntualidad,
                //    PremiosAsistencia = e.PremiosAsistencia,
                //    AyudaArtEscolares = e.AyudaArtEscolares,
                //    AyudaTransporte = e.AyudaTransporte,
                //    Becas = e.Becas,
                //    ProductividadId = e.ProductividadId,
                //    Puntualidad = n.Puntualidad,
                //    HorasReloj = n.HorasReloj,
                //    Productividad = n.Productividad,
                //    Antiguedad = n.Antiguedad,
                //    Ausencias = n.Ausencias,
                //    ControlId = n.ControlId,
                //    CuotasIMSS = n.CuotasIMSS,
                //    CuotasIMSSPatronal = n.CuotasIMSSPatronal,
                //    DescuentoPresatamo = n.DescuentoPresatamo,
                //    DiasVacaciones = n.DiasVacaciones,
                //    HorasAutorizadas = n.HorasAutorizadas,
                //    HorasExtra = n.HorasExtra,
                //    PercepcionEspecial = n.PercepcionEspecial,
                //    ISR = n.ISR,
                //    ImpuestoSobreNomina = n.ImpuestoSobreNomina,
                //    Incapacidad = n.Incapacidad,
                //    SubSidio = n.SubSidio,
                //    HorasAbono = n.HorasAbono,
                //    HorasCargo = n.HorasCargo,
                //    CreditoVivienda = e.CreditoVivienda
                //}).ToList();
                //this.Periodo.Empleados = new BindingList<ViewModelNominaPeriodo>(lista);
                this.periodo.Empleados = new BindingList<ViewModelNominaPeriodo>(mapper.Map(tabla).ToList());
            }
        }

        public void NuevaNomina()
        {
            this.Periodo.Estado = ViewModelNominaControl.EnumStatusNomina.PorAutorizar;
            this.Periodo.FechaInicio = this.PrimerDiaSemana();
            this.Periodo.FechaFin = this.periodo.FechaInicio.AddDays(value: 6);
            this.Periodo.Descripcion = "Semana Núm.: " + NumeroSemana(this.periodo.FechaInicio).ToString();
            List<ViewModelEmpleado> lista = this.Db.Queryable<ViewModelEmpleado>().Where((empleado) => empleado.IsActive == true).ToList();
            this.Periodo.Empleados = new BindingList<ViewModelNominaPeriodo>();

            foreach (var empleado in lista)
            {
                ViewModelNominaPeriodo nuevo = new ViewModelNominaPeriodo()
                {
                    EmpleadoId = empleado.Id,
                    Clave = empleado.Clave,
                    Numero = empleado.Num,
                    Nombres = empleado.Nombre,
                    SegundoApellido = empleado.SegundoApellido,
                    PrimerApellido = empleado.PrimerApellido,
                    RFC = empleado.RFC,
                    CURP = empleado.CURP,
                    JornadasTrabajo = empleado.JornadasTrabajo,
                    SalarioDiario = empleado.SalarioDiario,
                    SalarioDiarioIntegrado = empleado.SalarioDiarioIntegrado,
                    FechaIngreso = empleado.FechaInicioRelLaboral,
                    PremiosPuntualidad = empleado.PremiosPuntualidad,
                    PremiosAsistencia = empleado.PremiosAsistencia,
                    AyudaArtEscolares = empleado.AyudaArtEscolares,
                    AyudaTransporte = empleado.AyudaTransporte,
                    Becas = empleado.Becas,
                    ProductividadId = empleado.ProductividadId,
                    CreditoVivienda = empleado.CreditoVivienda,
                    Puntualidad = true,
                    HorasReloj = 48,
                    Productividad = 100,
                    HorasJornada = 48
                };
                this.Periodo.Empleados.Add(nuevo);
            }

            //List<ViewModelNominaPeriodo> lista = this.Db.Queryable<ViewModelEmpleado>().Where((empleado) => empleado.IsActive == true).Select((empleado) => new ViewModelNominaPeriodo
            //{
            //    EmpleadoId = empleado.Id,
            //    Clave = empleado.Clave,
            //    Numero = empleado.Num,
            //    Nombres = empleado.Nombre,
            //    SegundoApellido = empleado.SegundoApellido,
            //    PrimerApellido = empleado.PrimerApellido,
            //    RFC = empleado.RFC,
            //    CURP = empleado.CURP,
            //    JornadasTrabajo = empleado.JornadasTrabajo,
            //    SalarioDiario = empleado.SalarioDiario,
            //    SalarioDiarioIntegrado = empleado.SalarioDiarioIntegrado,
            //    FechaIngreso = empleado.FechaInicioRelLaboral,
            //    PremiosPuntualidad = empleado.PremiosPuntualidad,
            //    PremiosAsistencia = empleado.PremiosAsistencia,
            //    AyudaArtEscolares = empleado.AyudaArtEscolares,
            //    AyudaTransporte = empleado.AyudaTransporte,
            //    Becas = empleado.Becas,
            //    ProductividadId = empleado.ProductividadId,
            //    CreditoVivienda = empleado.CreditoVivienda,
            //    Puntualidad = true,
            //    HorasReloj = 48,
            //    Productividad = 100,
            //    HorasJornada = 48
            //}).ToList();

            //this.periodo.Empleados = new BindingList<ViewModelNominaPeriodo>(lista);
        }

        public BindingList<ViewModelNominaControl> GetPeriodos()
        {
            return new BindingList<ViewModelNominaControl>(this.Db.Queryable<ViewModelNominaControl>().ToList());
        }

        public BindingList<ViewModelNominaPeriodo> CargarEmpleados()
        {

            List<ViewModelNominaPeriodo> lista = this.Db.Queryable<ViewModelEmpleado>().Where((empleado) => empleado.IsActive == true).Select((empleado) => new ViewModelNominaPeriodo
            {
                EmpleadoId = empleado.Id,
                Clave = empleado.Clave,
                Numero = empleado.Num,
                Nombres = empleado.Nombre,
                SegundoApellido = empleado.SegundoApellido,
                PrimerApellido = empleado.PrimerApellido,
                RFC = empleado.RFC,
                CURP = empleado.CURP,
                JornadasTrabajo = empleado.JornadasTrabajo,
                SalarioDiario = empleado.SalarioDiario,
                SalarioDiarioIntegrado = empleado.SalarioDiarioIntegrado,
                FechaIngreso = empleado.FechaInicioRelLaboral,
                PremiosPuntualidad = empleado.PremiosPuntualidad,
                PremiosAsistencia = empleado.PremiosAsistencia,
                AyudaArtEscolares = empleado.AyudaArtEscolares,
                AyudaTransporte = empleado.AyudaTransporte,
                Becas = empleado.Becas,
                ProductividadId = empleado.ProductividadId,
                CreditoVivienda = empleado.CreditoVivienda,
                Puntualidad = true,
                HorasReloj = 48,
                Productividad = 100
            }).ToList();

            this.Periodo.Empleados = new BindingList<ViewModelNominaPeriodo>(lista);
            return this.Periodo.Empleados;
        }

        public List<EmpleadoAcumuladoHoras> AcumuladoHoras()
        {

            return this.Db.Queryable<ViewModelNominaPeriodo, ViewModelEmpleado>((v, e) => new object[]
            {
                JoinType.Left, v.EmpleadoId == e.Id
            }).GroupBy(v => v.EmpleadoId).Select((v, e) => new EmpleadoAcumuladoHoras
            {
                Id = e.Id,
                Clave = e.Clave,
                Nombres = e.Nombre,
                PrimerApellido = e.PrimerApellido,
                SegundoApellido = e.SegundoApellido,
                HorasAbono = SqlFunc.AggregateSum(v.HorasAbono),
                HorasCargo = SqlFunc.AggregateSum(v.HorasCargo)
            }).ToList();
        }

        /// <summary>
        /// obtener la informacion del sobrercibo de un empleado
        /// </summary>
        /// <param name="indice">Id del registro del periodo de pago</param>
        /// <returns></returns>
        public EmpleadoSobreRecibo SobreRecibo(int indice)
        {
            EmpleadoSobreRecibo objeto = this.Db.Queryable<ViewModelNominaControl, ViewModelNominaPeriodo, ViewModelEmpleado>((p, n, e) => new object[] 
            { 
                JoinType.Left, p.Id == n.ControlId,
                JoinType.Left, n.EmpleadoId == e.Id }
            ).Where((p, n, e) => p.Id == n.ControlId && n.Id == indice && e.Id == n.EmpleadoId).Select((p, n, e) => new EmpleadoSobreRecibo
            {
                Numero = e.Num,
                Nombres = e.Nombre,
                PrimerApellido = e.PrimerApellido,
                SegundoApellido= e.SegundoApellido,
                NumSeguridadSocial = e.NumSeguridadSocial,
                RFC = e.RFC,
                CURP = e.CURP,
                Departamento = e.Departamento,
                Puesto = e.Puesto,
                Antiguedad = n.Antiguedad,
                SalarioDiarioIntegrado = n.SalarioDiarioIntegrado,
                SalarioDiario = n.SalarioDiario,
                PerioricidadPago = e.PeriodicidadPago,
                TipoRegimen = e.TipoRegimen,
                TipoContrato = e.TipoContrato,
                TipoJornada = e.TipoJornada,
                DiasTrabajados = p.DiasPeriodo,
                Moneda = "MXN",
                FechaInicioRelLaboral = e.FechaInicioRelLaboral,
                JPercepciones = n.JPercepciones,
                JDeducciones = n.JDeducciones,
                FechaInicial = p.FechaInicio,
                FechaFinal = p.FechaFin
            }).Single();
            return objeto;
        }

        public void Guardar()
        {
            if (this.Periodo.Id == 0)
            {
                this.Periodo.Id = this.Db.Insertable(this.periodo).ExecuteReturnIdentity();
            }
            else
            {
                this.Db.Updateable(this.Periodo).ExecuteCommand();
            }

            if (this.Periodo.Id > 0)
            {
                for (int i = 0; i < this.Periodo.Empleados.Count; i++)
                {
                    this.Periodo.Empleados[i].ControlId = this.Periodo.Id;
                    if (this.Periodo.Empleados[i].Id == 0)
                    {
                        this.Periodo.Empleados[i].Id = this.Db.Insertable(this.Periodo.Empleados[i]).ExecuteReturnIdentity();
                    }
                    else
                    {
                        this.Db.Updateable(this.Periodo.Empleados[i]).ExecuteCommand();
                    }
                }
            }
        }

        public void Calcular()
        {
            SqlSugarTablaImpuestoSobreRenta tablaImpuestoSobreRenta = new SqlSugarTablaImpuestoSobreRenta(this.DataBase);
            SqlSugarSubsidioAlEmpleo tablasSubsidio = new SqlSugarSubsidioAlEmpleo(this.DataBase);
            ViewModelTablaImpuestoSobreRenta tablaISR = tablaImpuestoSobreRenta.GetListBy(7);
            ViewModelTablaSubsidioAlEmpleo tablaSubsidio = tablasSubsidio.GetListBy(7);
            this.tablaProductividad = new SqlSugarTablaProductividad(this.DataBase);
            this.Periodo.TablaProductividad = this.tablaProductividad.GetTablas();
            this.Periodo.TablaISR = tablaISR;
            this.periodo.TablaSubsidio = tablaSubsidio;
            for (int i = 0; i < this.Periodo.Empleados.Count; i++)
            {
                //SalarioDiarioIntegrado salarioDiarioIntegrado = new SalarioDiarioIntegrado(ref this.configuracion, this.Periodo.Empleados[i].FechaIngreso);
                //this.Periodo.Empleados[i].SalarioDiarioIntegrado = this.Periodo.Empleados[i].SalarioDiario * salarioDiarioIntegrado.FactorIntegracion;
                this.Periodo.Empleados[i].Percepciones = new BindingList<Percepcion>();
                this.Periodo.Empleados[i].Percepciones.Add(new Percepcion("SSR", "001", "001", "Sueldos, Salarios, Rayas y Jornales"));
                this.Periodo.Empleados[i].Percepciones.Add(new Percepcion("PPP", "010", "010", "Premios por puntualidad"));
                this.Periodo.Empleados[i].Percepciones.Add(new Percepcion("VDD", "002", "002", "Vales de Despensa"));
                this.Periodo.Empleados[i].Percepciones.Add(new Percepcion("FDA", "005", "005", "Fondo de Ahorro"));
                this.Periodo.Empleados[i].Percepciones.Add(new Percepcion("AAE", "034", "034", "Ayuda para artículos escolares"));
                this.Periodo.Empleados[i].Percepciones.Add(new Percepcion("ATR", "036", "036", "Ayuda para transporte"));
                this.Periodo.Empleados[i].Percepciones.Add(new Percepcion("BTI", "015", "015", "Becas para trabajadores y/o hijos"));
                this.Periodo.Empleados[i].Percepciones.Add(new Percepcion("HET", "019", "019", "Horas Extra"));
                this.Periodo.Empleados[i].Percepciones.Add(new Percepcion("OPA", "016", "016", "Otros; Premios por Asistencia"));
                this.Periodo.Empleados[i].Percepciones.Add(new Percepcion("PPR", "031", "031", "Premio de Productividad"));
                if (this.Periodo.Empleados[i].DiasVacaciones > 0) { this.Periodo.Empleados[i].Percepciones.Add(new Percepcion("VPV", "021", "021", "Vacaciones + Prima vacacional")); }
                this.periodo.Empleados[i].Percepciones.Add(new Percepcion("COM", "028", "028", "Comisiones"));
                this.Periodo.Empleados[i].Percepciones.Add(new Percepcion("SBE", "017", "017", "Subsidio para el empleo"));
                this.Periodo.Empleados[i].Deducciones = new BindingList<Deduccion>();
                this.Periodo.Empleados[i].Deducciones.Add(new Deduccion("SES", "001", "001", "Seguridad Social"));
                this.Periodo.Empleados[i].Deducciones.Add(new Deduccion("ISR", "002", "002", "ISR"));
                this.Periodo.Empleados[i].Deducciones.Add(new Deduccion("CFE", "018", "018", "Cuotas de fomento a Sociedades Cooperativas y Cajas de Ahorro"));
                if (this.periodo.Empleados[i].CreditoVivienda > 0)
                    this.periodo.Empleados[i].Deducciones.Add(new Deduccion("INF", "010", "010", "Pago por crédito de vivienda"));
                this.Periodo.Empleados[i] = this.CalcularPercepciones(this.Periodo.Empleados[i]);
                this.Periodo.Empleados[i] = this.CalcularDeducciones(this.Periodo.Empleados[i]);
            }

            // por ultimo removemos percepciones y deducciones que no se utilizaron durante el calculo
            for (int i = 0; i < this.Periodo.Empleados.Count; i++)
            {
                this.Periodo.Empleados[i].Percepciones = new BindingList<Percepcion>(this.Periodo.Empleados[i].Percepciones.Where(it => it.ImporteTotal > 0).ToList());
                this.Periodo.Empleados[i].Deducciones = new BindingList<Deduccion>(this.Periodo.Empleados[i].Deducciones.Where(it => it.Importe > 0).ToList());
            }
        }

        public BindingList<EmpleadoAguinaldo> CalcularAguinaldo()
        {
            this.configuracion = this.conf.LoadConf();
            SqlSugarTablaImpuestoSobreRenta tablaImpuestoSobreRenta = new SqlSugarTablaImpuestoSobreRenta(this.DataBase);
            SqlSugarSubsidioAlEmpleo tablasSubsidio = new SqlSugarSubsidioAlEmpleo(this.DataBase);
            ViewModelTablaImpuestoSobreRenta tablaISR = tablaImpuestoSobreRenta.GetListBy(30);
            ViewModelTablaSubsidioAlEmpleo tablaSubsidio = tablasSubsidio.GetListBy(30);
            List<ViewModelEmpleado> t = this.Db.Queryable<ViewModelEmpleado>().Where((it) => it.IsActive == true).ToList();

            BindingList<EmpleadoAguinaldo> resultado = new BindingList<EmpleadoAguinaldo>();
            foreach (ViewModelEmpleado item in t)
            {
                resultado.Add(this.Aguinaldo(new EmpleadoAguinaldo(item), tablaISR, tablaSubsidio));
            }
            return resultado;
        }

        public ViewModelNominaPeriodo CalcularPercepciones(ViewModelNominaPeriodo objeto)
        {
            for (int p = 0; p < objeto.Percepciones.Count; p++)
            {
                switch (objeto.Percepciones[p].Identificador)
                {
                    case "SSR":
                        objeto.Percepciones[p] = this.SueldosSalarios(objeto.Percepciones[p], objeto);
                        break;
                    case "PPP":
                        objeto.Percepciones[p] = this.PremioPuntualidad(objeto.Percepciones[p], objeto);
                        break;
                    case "VDD":
                        objeto.Percepciones[p] = this.ValesDespensa(objeto.Percepciones[p], objeto);
                        break;
                    case "FDA":
                        objeto.Percepciones[p] = this.FondoAhorro(objeto.Percepciones[p], objeto);
                        break;
                    case "ATR":
                        objeto.Percepciones[p] = this.Ayudatransporte(objeto.Percepciones[p], objeto);
                        break;
                    case "HET":
                        objeto.Percepciones[p] = this.HorasExtra(objeto.Percepciones[p], objeto);
                        break;
                    case "OPA":
                        objeto.Percepciones[p] = this.OtrosPremiosAsistencia(objeto.Percepciones[p], objeto);
                        break;
                    case "SBE":
                        objeto.Percepciones[p] = this.SubsidioAlEmpleo(objeto.Percepciones[p], objeto);
                        break;
                    case "PPR":
                        objeto.Percepciones[p] = this.PremioProductividad(objeto.Percepciones[p], objeto);
                        break;
                    case "AAE":
                        objeto.Percepciones[p] = this.AyudaArticulosEscolares(objeto.Percepciones[p], objeto);
                        break;
                    case "BTI":
                        objeto.Percepciones[p] = this.BecasTrabajadores(objeto.Percepciones[p], objeto);
                        break;
                    case "VPV":
                        objeto.Percepciones[p] = this.VacacionesMasPrimaVacacional(objeto.Percepciones[p], objeto);
                        break;
                    case "COM":
                        objeto.Percepciones[p] = this.Comisiones(objeto.Percepciones[p], objeto);
                        break;
                    default:
                        break;
                }
            }
            return objeto;
        }

        public ViewModelNominaPeriodo CalcularDeducciones(ViewModelNominaPeriodo objeto)
        {
            for (int d = 0; d < objeto.Deducciones.Count; d++)
            {
                switch (objeto.Deducciones[d].Identificador)
                {
                    case "SES":
                        objeto.Deducciones[d] = this.SeguridadSocial(objeto.Deducciones[d], objeto);
                        break;
                    case "ISR":
                        objeto.Deducciones[d] = this.ImpuestoSobreRenta(objeto.Deducciones[d], objeto);
                        break;
                    case "CFE":
                        objeto.Deducciones[d] = this.CooperativaCajaAhorro(objeto.Deducciones[d], objeto);
                        break;
                    case "INF":
                        objeto.Deducciones[d] = this.CreditoVivienda(objeto.Deducciones[d], objeto);
                        break;
                    default:
                        break;
                }
            }
            return objeto;
        }

        /// <summary>
        /// crear layout para bancomer de la nomina actual
        /// </summary>
        public string CrearLayoutBancomer(int indice, string archivo)
        {

            List<Jaeger.Entities.Layout.BancomerNominaRegistro> lista = this.Db.Queryable<ViewModelNominaPeriodo, ViewModelEmpleado>((n, e) => new object[]
                {
                    JoinType.Left, n.EmpleadoId == e.Id
                }).Where((n, e) => n.ControlId == indice && n.Activo == true && e.ClaveBanco == "012").Select((n, e) => new Jaeger.Entities.Layout.BancomerNominaRegistro
                {
                      //Nombre = e.Nombre,
                      //PrimerApellido = e.PrimerApellido,
                      //SegundoApellido = e.SegundoApellido,
                      ImportePagar = (double)n.Sueldo, 
                      NumeroDeCuenta = e.CuentaBancaria
                }).ToList();
            Jaeger.Entities.Layout.BancomerNomina temp = new Jaeger.Entities.Layout.BancomerNomina();
            temp.Items = new BindingList<Jaeger.Entities.Layout.BancomerNominaRegistro>(lista);
            temp.Create(@"c:\Jaeger\Jaeger.Temporal\salida.txt");
            return "";
        }

        #region percepciones

        private Percepcion SueldosSalarios(Percepcion p, ViewModelNominaPeriodo e)
        {
            //= IF(OR('Nómina'!Status = "Z", 'Nómina'!SD = "", 'Nómina'!Status = "E"), "", 
            //('Nómina'!DP - (TRUNC(('Nómina'!DP / 'Nómina'!JT)) * ('Nómina'!INC + 'Nómina'!AUS))*('Nómina'!DP / 'Nómina'!JT))*'Nómina'!SD)
            p.ImporteGravado = (this.Periodo.DiasPeriodo - ((Math.Truncate((decimal)this.Periodo.DiasPeriodo / (decimal)e.JornadasTrabajo) * (e.Incapacidad + e.Ausencias)) * ((decimal)this.Periodo.DiasPeriodo / (decimal)e.JornadasTrabajo))) * e.SalarioDiario;
            p.ImporteExento = 0;
            return p;
        }

        /// <summary>
        /// fondo de ahorro
        /// </summary>
        private Percepcion FondoAhorro(Percepcion p, ViewModelNominaPeriodo e)
        {
            // IF(('Nómina'!INC+'Nómina'!AUS>0),
            // ROUND(('Nómina'!DP-('Nómina'!DP/'Nómina'!JT*('Nómina'!INC+'Nómina'!AUS)))*'Nómina'!SD*0.13),'Nómina'!DP*'Nómina'!SD*0.13)
            if (e.Ausencias + e.Incapacidad > 0)
            {
                p.ImporteExento = ((this.Periodo.DiasPeriodo - ((decimal)this.Periodo.DiasPeriodo / (decimal)e.JornadasTrabajo * (e.Incapacidad + e.Ausencias))) * e.SalarioDiario * this.configuracion.PorcentajeFondoAhorro);
            }
            else
            {
                p.ImporteExento = periodo.DiasPeriodo * e.SalarioDiario * this.configuracion.PorcentajeFondoAhorro;
            }
            p.ImporteGravado = 0;
            return p;
        }

        private Percepcion PremioPuntualidad(Percepcion p, ViewModelNominaPeriodo e)
        {
            // =IF(OR('Nómina'!Status="Z",'Nómina'!SD=""),"",IF('Nómina'!PUNT=1,ROUND('Nómina'!SDI*('Nómina'!DP)*0.1),""))
            if (e.Puntualidad == true)
            {
                p.ImporteExento = (e.SalarioDiarioIntegrado * (periodo.DiasPeriodo) * this.configuracion.PorcentajePremioPuntualidad);
            }
            else
            {
                p.ImporteExento = 0;
            }
            p.ImporteGravado = 0;
            return p;
        }

        private Percepcion BecasTrabajadores(Percepcion p, ViewModelNominaPeriodo e)
        {
            //= IF('Nómina'!Status = "Z", "", IF(AND('Nómina'!INC = "", 'Nómina'!AUS = ""), Becas, Becas - (Becas / 'Nómina'!JT * ('Nómina'!INC + 'Nómina'!AUS))))
            if (e.Ausencias + e.Incapacidad > 0)
            {
                p.ImporteExento = e.Becas - ((e.Becas / (decimal)e.JornadasTrabajo) * (e.Incapacidad + e.Ausencias));
            }
            else
            {
                p.ImporteExento = e.Becas;
                p.ImporteGravado = 0;
            }
            return p;
        }

        /// <summary>
        /// a) El premio de puntualidad ** que no sobrepase del 10% del salario base de cotización del IMSS de ese trabajador b) El premio de asistencia **, bajo el mismo supuesto anterior
        /// </summary>
        private Percepcion OtrosPremiosAsistencia(Percepcion p, ViewModelNominaPeriodo e)
        {
            // IF(AND('Nómina'!SDI>0,'Nómina'!INC="",'Nómina'!AUS=""),('Nómina'!DP*'Nómina'!SDI*0.1),"")
            if (e.Incapacidad == 0 && e.Ausencias == 0)
            {
                p.ImporteExento = e.SalarioDiarioIntegrado * periodo.DiasPeriodo * new decimal(value: 0.1);
            }
            else
            {
                p.ImporteExento = 0;
            }
            p.ImporteGravado = 0;
            return p;
        }

        /// <summary>
        /// Artículo 27 de la Ley del IMSS. No pasan a formar parte del SDI solo que se repitan cada período
        /// </summary>
        private Percepcion HorasExtra(Percepcion p, ViewModelNominaPeriodo e)
        {
            if (e.HorasExtra > 9)
            {
                p.ImporteExento = e.SalarioDiario / 8 * 2 * 9;
            }
            else
            {
                if (e.HorasExtra < 1)
                {
                    p.ImporteExento = 0;
                }
                else
                {
                    p.ImporteExento = e.HorasExtra * ((e.SalarioDiario / 8) * 2);
                }
            }
            return p;
        }

        private Percepcion VacacionesMasPrimaVacacional(Percepcion p, ViewModelNominaPeriodo e)
        {
            //IF('Nómina'!VAC > 0,if ((('Nómina'!VAC * 'Nómina'!SD) *1.25)> ('Nómina'!SM * 15),(('Nómina'!VAC * 'Nómina'!SD) *1.25)-('Nómina'!SM * 15),0),"")
            if (e.DiasVacaciones > 0)
            {
                if ((e.DiasVacaciones * e.SalarioDiario) * new decimal(1.25) > (this.configuracion.SalarioMinimoGeneral * 15))
                {
                    p.ImporteGravado = (e.DiasVacaciones * e.SalarioDiario) * new decimal(1.25) - (this.configuracion.SalarioMinimoGeneral * 15);
                }
                else
                {
                    p.ImporteGravado = 0;
                }
            }
            //IF(AND('Nómina'!VAC>1, (('Nómina'!SD*'Nómina'!VAC)>('Nómina'!SM*15))), ('Nómina'!SM*15), ('Nómina'!VAC * 'Nómina'!SD)*1.25)
            if (e.DiasVacaciones > 1)
            {
                if ((e.SalarioDiario * e.DiasVacaciones) > (this.configuracion.SalarioMinimoGeneral * 15))
                {
                    p.ImporteExento = this.configuracion.SalarioMinimoGeneral * 15;
                }
                else
                {
                    p.ImporteExento = (e.DiasVacaciones * e.SalarioDiario) * new decimal(1.25);
                }
            }

            return p;
        }

        private Percepcion Comisiones(Percepcion p, ViewModelNominaPeriodo e)
        {
            //IF(OR('Nómina'!SD = "", 'Nómina'!SUELDO = ""), 
            //"", 
            // IF(OR(('Nómina'!HRST + J27) < ('Nómina'!HRST + 9), ('Nómina'!HRST + J27) = ('Nómina'!HRST + 9)),
            //   "",
            //     (('Nómina'!HRST + J27)-('Nómina'!HRST + 9))*('Nómina'!SD / 8 * 2)))
            if ((e.HorasJornada + e.HorasExtra) < (e.HorasJornada + 9) | (e.HorasJornada + e.HorasExtra) == (e.HorasJornada + 9))
            {
                p.ImporteGravado = 0;
            }
            else
            {
                p.ImporteGravado = ((e.HorasJornada + e.HorasExtra) - (e.HorasJornada + 9)) * (e.SalarioDiario / 8 * 2);
            }
            return p;
        }

        private Percepcion AyudaArticulosEscolares(Percepcion p, ViewModelNominaPeriodo e)
        {
            // IF(OR('Nómina'!Status="Z",'Nómina'!Status=""),"",
            //IF(AND('Nómina'!INC="",'Nómina'!AUS=""),Esclrs,Esclrs-(Esclrs/'Nómina'!JT*('Nómina'!INC+'Nómina'!AUS))))
            if (e.Incapacidad == 0 && e.Ausencias == 0)
            {
                p.ImporteExento = e.AyudaArtEscolares;
            }
            else
            {
                p.ImporteExento = e.AyudaArtEscolares - (e.AyudaArtEscolares / new decimal(e.JornadasTrabajo) * (e.Incapacidad + e.Ausencias));
            }
            p.ImporteGravado = 0;
            return p;
        }

        /// <summary>
        /// SEGURO DE VIDA, INVALIDEZ Y GASTOS MÉDICOS; PRODUCTIVIDAD, ALIMENTACIÓN Y AYUDA DE TRANSPORTE ACUERDO DEL CONSEJO TÉCNICO DEL IMSS No. 77/94 "Este Consejo Técnico, 
        /// con fundamento en los artículos 240, fracciones I, IV y XIII, 252 y 253, fracción X-Bis de la Ley del Seguro Social, con base en la resolución del Comité de Asuntos 
        /// Jurídicos de este Cuerpo Colegiado, contenida en el acta del 28 de febrero de 1994, y con el propósito de precisar el contenido y alcance de algunos de los conceptos 
        /// regulados por el artículo 32 de la misma ley, reformado por el Decreto publicado en el Diario Oficial de la Federación el 20 de julio de 1993, acuerda lo siguiente: 
        /// VI.Bono o ayuda para transporte.- Este concepto no integra salario cuando la prestación se otorgue como instrumento de trabajo, en forma de boleto, cupón o bien a 
        /// manera de reembolso, por un gasto específico sujeto a comprobación.Por el contrario, si la prestación se otorga en efectivo, en forma general y permanente, debe 
        /// considerarse como integrante del salario, toda vez que no se encuentra excluida expresamente en ninguna de las fracciones del artículo 32 de la Ley del Seguro Social; 
        /// y VII.Hágase del conocimiento de las diversas dependencias del Instituto para que se cumpla debidamente y publíquese el presente Acuerdo en los diarios de mayor 
        /// circulación en el país y en el Diario Oficial de la Federación y solamente en éste último los Acuerdo 494/93, 495/93, 496/93 y 497/93, de fecha 18 de agosto de 1993, 
        /// a fin de que los patrones y trabajadores tengan un conocimiento preciso al respecto"
        /// </summary>
        private Percepcion Ayudatransporte(Percepcion p, ViewModelNominaPeriodo e)
        {
            // IF(OR('Nómina'!Status="Z",'Nómina'!Status=""),"",
            //  IF(AND('Nómina'!INC="",'Nómina'!AUS=""),'Nómina'!VIAT_CAP,'Nómina'!VIAT_CAP-('Nómina'!VIAT_CAP/'Nómina'!JT*('Nómina'!INC+'Nómina'!AUS))))
            if (e.Incapacidad == 0 & e.Ausencias == 0)
            {
                p.ImporteExento = e.AyudaTransporte;
            }
            else
            {
                p.ImporteExento = e.AyudaTransporte - (e.AyudaTransporte / new decimal(e.JornadasTrabajo) * (e.Incapacidad + e.Ausencias));
            }
            p.ImporteGravado = 0;
            return p;
        }

        private Percepcion ValesDespensa(Percepcion objeto, ViewModelNominaPeriodo objeto2)
        {
            objeto.ImporteExento = this.configuracion.SalarioMinimoGeneral * (periodo.DiasPeriodo - ((decimal)periodo.DiasPeriodo / (decimal)objeto2.JornadasTrabajo * (objeto2.Incapacidad + objeto2.Ausencias))) * this.configuracion.PorcentajeValesDespensa;
            return objeto;
        }

        private Percepcion SubsidioAlEmpleo(Percepcion p, ViewModelNominaPeriodo e)
        {
            p.ImporteExento = periodo.TablaSubsidio.GetSubsidio(e.TotalPercepcionesGravado).Cantidad;
            p.ImporteGravado = 0;
            e.SubSidio = p.ImporteExento;
            return p;
        }

        private Percepcion PremioProductividad(Percepcion p, ViewModelNominaPeriodo e)
        {
            p.ImporteExento = 0;
            if (e.ProductividadId != 0)
            {
                ViewModelTablaProductividad d;
                try
                {
                    d = this.periodo.TablaProductividad.First((ViewModelTablaProductividad x) => x.Id == e.ProductividadId);
                }
                catch (Exception)
                {
                    d = null;
                }
                if (d != null)
                {
                    p.ImporteExento = d.GetProductividad(e.Productividad).Importe;
                }
            }
            else
            {
                p.ImporteExento = 0;
            }
            p.ImporteGravado = 0;
            return p;
        }

        #endregion

        #region deducciones

        private Deduccion CooperativaCajaAhorro(Deduccion d, ViewModelNominaPeriodo e)
        {
            if (e.Ausencias + e.Incapacidad > 0)
            {
                d.Importe = (periodo.DiasPeriodo - (periodo.DiasPeriodo / new decimal(e.JornadasTrabajo) * (e.Incapacidad + e.Ausencias))) * e.SalarioDiario * this.configuracion.PorcentajeFondoAhorro;
            }
            else
            {
                d.Importe = periodo.DiasPeriodo * e.SalarioDiario * this.configuracion.PorcentajeFondoAhorro;
            }
            return d;
        }

        private Deduccion ImpuestoSobreRenta(Deduccion d, ViewModelNominaPeriodo e)
        {
            d.Importe = periodo.TablaISR.Calcular(e.TotalPercepcionesGravado);
            e.ISR = d.Importe;
            return d;
        }

        private Deduccion SeguridadSocial(Deduccion d, ViewModelNominaPeriodo e)
        {
            CuotasObreroIMSS co = new CuotasObreroIMSS(e.SalarioDiario, e.SalarioDiarioIntegrado, ref this.configuracion);
            d.Importe = co.CuotaIMSS(periodo.DiasPeriodo, ref e, periodo);
            e.CuotasIMSS = d.Importe;
            e.CuotasObrero = co;
            CuotasPatronalesIMSS cp = new CuotasPatronalesIMSS(e.SalarioDiario, e.SalarioDiarioIntegrado, ref this.configuracion);
            e.CuotasIMSSPatronal = cp.Calcular(ref e, this.Periodo);
            e.CuotasPatronales = cp;
            return d;
        }

        private Deduccion CreditoVivienda(Deduccion d, ViewModelNominaPeriodo e)
        {
            //= IF('Nómina'!JT > 0, 
            if (e.JornadasTrabajo > 0)
            {
                //(('Nómina'!DP / 'Nómina'!JT) * ('Nómina'!JT - 'Nómina'!INC - 'Nómina'!AUS)) *DescInfonavit, 0)
                d.Importe = ((this.Periodo.DiasPeriodo / (decimal)e.JornadasTrabajo) * (e.JornadasTrabajo - e.Incapacidad - e.Ausencias)) * e.CreditoVivienda;
            }
            else
            {
                d.Importe = 0;
            }
            return d;
        }

        #endregion

        /// <summary>
        /// procedimiento para calcular el aguinaldo
        /// </summary>
        public EmpleadoAguinaldo Aguinaldo(EmpleadoAguinaldo e, ViewModelTablaImpuestoSobreRenta isr, ViewModelTablaSubsidioAlEmpleo subsidio)
        {
            // calcular salario mensual
            e.SalarioMensual = e.SalarioDiario * this.configuracion.DiasPorMes;

            // monto del aguinaldo
            e.Aguinaldo = ((e.SalarioDiario * this.configuracion.DiasAguinaldo) / this.configuracion.DiasPorAnio) * e.DiasLaborados;

            // base exento
            if (this.configuracion.UsarParaCalcular == Jaeger.Nomina.Entities.Configuracion.EnumUsarParaCalcular.UMA)
            {
                e.BaseExento = this.configuracion.UMA * 30;
            }
            else
            {
                e.BaseExento = this.configuracion.SalarioMinimoGeneral * 30;
            }

            // base gravable
            if (this.configuracion.ArticuloISR == Entities.Configuracion.EnumArticuloISR.Articulo96)
            {
                e.BaseGravable = (e.SalarioMensual + e.Aguinaldo) - e.BaseExento;
            }
            else
            {
                e.BaseGravable = e.SalarioMensual + (((e.Aguinaldo - e.BaseExento) / this.configuracion.DiasPorAnio) * this.configuracion.DiasPorMes);
            }

            // subsidio al empleo
            e.Subsidio = subsidio.GetSubsidio(e.SalarioMensual).Cantidad;

            // isr causado por el sueldo
            e.ISR = isr.Calcular(e.BaseGravable);

            // isr del salario mensual
            e.ISRSueldo = isr.Calcular(e.SalarioMensual) - e.Subsidio;
            if (e.ISRSueldo < 0)
                e.ISRSueldo = 0;
            
            // isr del aguinaldo
            if (this.configuracion.ArticuloISR == Entities.Configuracion.EnumArticuloISR.Articulo96)
            {
                e.ISRAguinaldo = e.ISRSalarioMasAguinaldo - e.ISRSueldo;
            }
            else if (this.configuracion.ArticuloISR == Entities.Configuracion.EnumArticuloISR.Articulo174)
            {
                e.ISRAguinaldo = (e.ISRSalarioMasAguinaldo - e.ISRSueldo) / (((e.Aguinaldo - e.BaseExento) / this.configuracion.DiasPorAnio) * this.configuracion.DiasPorMes) * (e.Aguinaldo - e.BaseExento);
            }
            if (e.ISRAguinaldo < 0)
            {
                e.ISRAguinaldo = 0;
            }
            return e;
        }

        #region funciones

        /// <summary>
        /// devulve el numero de la semana correspondiente a la fecha
        /// </summary>
        public static int NumeroSemana(DateTime fechaInput)
        {
            // formatea la fecha de acuerdo a la zona del computador
            CultureInfo cul = CultureInfo.CurrentCulture;
            // Usa la fecha formateada y calcula el número de la semana
            return cul.Calendar.GetWeekOfYear(
                 fechaInput,
                 CalendarWeekRule.FirstDay,
                 DayOfWeek.Monday);
        }

        /// <summary>
        /// funcion para devolver el primer dia de la semana
        /// </summary>
        private DateTime PrimerDiaSemana()
        {
            DateTime dt = DateTime.Now.AddDays(-6);
            DateTime wkStDt = DateTime.MinValue;
            wkStDt = dt.AddDays(1 - Convert.ToDouble(dt.DayOfWeek));
            DateTime fechadesdesemana = wkStDt.Date;
            return fechadesdesemana;
        }

        #endregion
    }
}
