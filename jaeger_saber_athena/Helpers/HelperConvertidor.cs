﻿using System;
using System.Linq;
using Jaeger.Nomina.Entities;

namespace Jaeger.Nomina.Helpers
{
    public class HelperConvertidor
    {
        public EmpleadoExpediente Expediente(ViewModelEmpleado objeto)
        {
            ViewModelEmpleado e = new ViewModelEmpleado();
            EmpleadoExpediente r = new EmpleadoExpediente();
            r.Nombre = e.NombreCompleto();
            r.CURP = e.CURP;
            r.RFC = e.RFC;

            return r;
        }
    }
}
