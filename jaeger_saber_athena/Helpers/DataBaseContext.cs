﻿/// develop: anhe1 29052019
/// purpose: 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Jaeger.Edita.V2.Empresa.Entities;
using SqlSugar;
using Jaeger.Edita.Helpers;
using Jaeger.Edita.Entities;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.Nomina.Helpers
{
    public class DataBaseContext<T> where T : class, new() 
    {

        private SqlSugarMessage menssageField = new SqlSugarMessage();
        private DataBaseConfiguracion configuracionField;
        private readonly SqlSugarClient dbase;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion"></param>
        public DataBaseContext(DataBaseConfiguracion configuracion)
        {
            this.configuracionField = configuracion;
            this.Message = new SqlSugarMessage();
            this.dbase = new SqlSugarClient(new ConnectionConfig()
            {
                ConnectionString = HelperMySql.StringBuilderToString(configuracion),
                DbType = DbType.MySql,
                IsAutoCloseConnection = true,
                InitKeyType = InitKeyType.Attribute,
                AopEvents = new AopEvents()
                {
                    OnLogExecuting = (sql, p) =>
                    {
                        Console.WriteLine(string.Concat("Executing SQL: ", sql));
                        Console.WriteLine(string.Join(",", p.Select(it => it.ParameterName + ":" + it.Value)));
                    }
                }
            });
        }

        /// <summary>
        /// obtener o establecer la configuracion para las conexiones a la base de datos
        /// </summary>
        public DataBaseConfiguracion Settings
        {
            get
            {
                return this.configuracionField;
            }
            set
            {
                this.configuracionField = value;
            }
        }

        public SqlSugarClient Db
        {
            get
            {
                return this.dbase;
            }
        }

        public SimpleClient<T> CurrentDb
        {
            get
            {
                return new SimpleClient<T>(Db);
            }
        }

        public SqlSugarMessage Message
        {
            get
            {
                return this.menssageField;
            }
            set
            {
                this.menssageField = value;
            }
        }
        
        public virtual T GetById(int id)
        {
            return CurrentDb.GetById(id);
        }
        
        public virtual List<T> GetList()
        {
            return CurrentDb.GetList();
        }
        
        public virtual bool Delete(int id)
        {
            return CurrentDb.DeleteById(id);
        }

        public virtual int Insert(T objeto)
        {
            return this.CurrentDb.InsertReturnIdentity(objeto);
        }

        public virtual int Update(T objeto)
        {
            return this.CurrentDb.AsUpdateable(objeto).ExecuteCommand();
        }

        /// <summary>
        /// crear tabla del modelo en la base de datos
        /// </summary>
        /// <returns></returns>
        public virtual bool Create()
        {
            try
            {
                this.Db.CodeFirst.InitTables(typeof(T));
                return true;
            }
            catch (SqlSugarException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        public string CreateGuid(string[] datos)
        {
            //use MD5 hash to get a 16-byte hash of the string:
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();

            byte[] inputBytes = Encoding.Default.GetBytes(string.Join("", datos).Trim().ToUpper());

            byte[] hashBytes = provider.ComputeHash(inputBytes);

            //generate a guid from the hash:

            Guid hashGuid = new Guid(hashBytes);

            return hashGuid.ToString().ToUpper();
        }
 
    }
}