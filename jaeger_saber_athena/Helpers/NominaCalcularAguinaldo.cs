﻿/// LFT Art.87: Los trabajadores tendran derecho a un aguinaldo anual que debera pagarse antes del dia veinte de diciembre, equivalente a quince dias de salario, por lo menos
/// Los que no hayan cumplido el año de servicios, independiente de que se encuentren laborando o no en la fecha de liquidacion del aguinaldo, tendran derecho a que se les pague
/// la parte proporcional del mismo, conforme al tiempo que hubieren trabajado, cualquiere que fuese este.
/// Extencion del Aguinaldo con base en ISR
/// LISR Art. 93: No se pagara el impuesto sobre la renta por la otencion de los siguientes ingresos:
/// XIV. Las gratificaciones que reciban los trabajadores de sus patrones, durante un año de calendario, hasta el equivalente del salario minimo general del area geografica del
/// trabajador elevado a 30 dias, cuando dichas gratificaciones se otorguen en forma general.
/// Procedimientso de calculo de Aguinaldo
/// Ley de Impuesto Sobre la Renta Art. 96
/// Reglamento de la ley del impuesto sobre la renta Art 174.
using Jaeger.Nomina.Entities;

namespace Jaeger.Nomina.Helpers
{
    public class NominaCalcularAguinaldo : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private readonly Configuracion configuracion;
        private readonly ViewModelTablaImpuestoSobreRenta tablaIsr;
        private readonly ViewModelTablaSubsidioAlEmpleo tablaSubsidio;
        private int numField;
        private string claveField;
        private string nombreField;
        private decimal salarioDiarioField;
        private decimal diasLaborados;

        public NominaCalcularAguinaldo(ViewModelEmpleado empleado, ref Configuracion conf, ViewModelTablaImpuestoSobreRenta tabla1, ViewModelTablaSubsidioAlEmpleo tabla2)
        {
            this.configuracion = conf;
            this.numField = empleado.Num;
            this.claveField = empleado.Clave;
            this.nombreField = empleado.NombreCompleto();
            this.configuracion = conf;
            this.salarioDiarioField = empleado.SalarioDiario;
            this.tablaIsr = tabla1;
            this.tablaSubsidio = tabla2;
            this.diasLaborados = 365;
        }

        #region propiedades

        /// <summary>
        /// obtener o establecer el numero del empleado
        /// </summary>
        public int Num
        {
            get
            {
                return this.numField;
            }
            set
            {
                this.numField = value;
                this.OnPropertyChanged();
            }
        }

        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal SalarioDiario
        {
            get
            {
                return this.salarioDiarioField;
            }
            set
            {
                this.salarioDiarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener el monto del salario mensual
        /// </summary>
        public decimal SalarioMensual
        {
            get
            {
                return this.SalarioDiario * this.configuracion.DiasPorMes;
            }
        }

        /// <summary>
        /// obtener el monto del aguinaldo
        /// </summary>
        public decimal Aguinaldo
        {
            get
            {
                return ((this.SalarioDiario * this.configuracion.DiasAguinaldo) / this.configuracion.DiasPorAnio) * this.DiasLaborados;
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad de dias laborados para el aguinaldo
        /// </summary>
        public decimal DiasLaborados
        {
            get
            {
                return this.diasLaborados;
            }
            set
            {
                this.diasLaborados = value;
                this.OnPropertyChanged();
            }
        }

        public decimal BaseGravable
        {
            get
            {
                if (this.configuracion.ArticuloISR == Configuracion.EnumArticuloISR.Articulo96)
                {
                    return (this.SalarioMensual + this.Aguinaldo) - this.BaseExento;
                }
                return this.SalarioMensual + (((this.Aguinaldo - this.BaseExento) / 365) * new decimal(30.4));
            }
        }

        public decimal BaseExento
        {
            get
            {
                if (this.configuracion.UsarParaCalcular == Configuracion.EnumUsarParaCalcular.UMA)
                {
                    return this.configuracion.UMA * 30;
                }
                return this.configuracion.SalarioMinimoGeneral * 30;
            }
        }

        public decimal BaseGravada
        {
            get
            {
                decimal c = this.Aguinaldo - this.BaseExento;
                if (c <= 0)
                {
                    return 0;
                }
                return this.Aguinaldo - this.BaseExento;
            }
        }

        public decimal ISR
        {
            get
            {
                decimal c = this.tablaIsr.Calcular(this.BaseGravable);
                if (c < 0)
                {
                    return 0;
                }
                return c;
            }
        }

        public decimal Subsidio
        {
            get
            {
                return this.tablaSubsidio.GetSubsidio(this.BaseGravable).Cantidad;
            }
        }

        public decimal ISRSalarioMasAguinaldo
        {
            get
            {
                return this.ISR - this.Subsidio;
            }
        }

        public decimal ISRSueldo
        {
            get
            {
                decimal c = this.tablaIsr.Calcular(this.SalarioMensual) - this.tablaSubsidio.GetSubsidio(this.SalarioMensual).Cantidad;
                if (c < 0)
                {
                    return 0;
                }
                return c;
            }
        }

        public decimal ISRAguinaldo
        {
            get
            {
                decimal c = 0;
                if (this.configuracion.ArticuloISR == Configuracion.EnumArticuloISR.Articulo96)
                {
                    c = this.ISRSalarioMasAguinaldo - this.ISRSueldo;
                }
                else if (this.configuracion.ArticuloISR == Configuracion.EnumArticuloISR.Articulo174)
                {
                    c = (this.ISRSalarioMasAguinaldo - this.ISRSueldo) / (((this.Aguinaldo - this.BaseExento) / 365) * new decimal(30.4)) * (this.Aguinaldo - this.BaseExento);
                }
                if (c < 0)
                {
                    return 0;
                }
                return c;
            }
        }

        public decimal AguinaldoPagar
        {
            get
            {
                return this.Aguinaldo - this.ISRAguinaldo;
            }
        }
        #endregion
    }
}
