﻿using System;
using System.ComponentModel;
using Jaeger.Nomina.Entities;
using SqlSugar;
using Jaeger.Nomina.Interface;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.Nomina.Helpers
{
    public class SqlSugarTablaUMA : MySqlSugarContext<ViewModelTablaUMA>, ISqlTablaUMA
    {
        public SqlSugarTablaUMA(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public bool Create() {
            throw new NotImplementedException();
        }

        public ViewModelTablaUMA GetByActivo()
        {
            return this.Db.Queryable<ViewModelTablaUMA>().OrderBy(it => it.FechaInicio, OrderByType.Desc).First();
        }

        public BindingList<ViewModelTablaUMA> Save(BindingList<ViewModelTablaUMA> items)
        {
            return items;
        }
    }
}
