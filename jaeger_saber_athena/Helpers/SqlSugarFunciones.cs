﻿using System;
using System.ComponentModel;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Nomina.Entities;
using Jaeger.Nomina.Interface;

namespace Jaeger.Nomina.Helpers
{
    public class SqlSugarFunciones : MySqlSugarContext<ViewModelDeptoFuncion>, ISqlFunciones
    {
        public SqlSugarFunciones(DataBaseConfiguracion configuracion)
            : base(configuracion)
        {

        }

        public BindingList<ViewModelDeptoFuncion> GetLitBy(int index, bool activo = true)
        {
            return new BindingList<ViewModelDeptoFuncion>(this.Db.Queryable<ViewModelDeptoFuncion>().Where(it => it.IdPuesto == index).WhereIF(activo, it => it.Activo == true).OrderBy(it => it.Secuencia, SqlSugar.OrderByType.Asc).ToList());
        }

        public ViewModelDeptoFuncion Save(ViewModelDeptoFuncion funcion)
        {
            if (funcion.Id == 0)
                funcion.Id = this.Insert(funcion);
            else
                this.Update(funcion);
            return funcion;
        }
    }
}
