﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Nomina.Helpers
{
    public class HelperCalcularAntiguedad
    {
        /// <summary>
        /// comprobación de la antiguedad
        /// </summary>
        public string Comprobar(DateTime fechaInicioRelLab, DateTime fechaFinPago, string antiguedad)
        {
            string response;
            TimeSpan timeSpan;

            try
            {
                string str1 = "P";
                if (antiguedad.Contains("W"))
                {
                    timeSpan = fechaFinPago - fechaInicioRelLab;
                    double totalDays = timeSpan.TotalDays + 1;
                    double num1 = Math.Floor(totalDays / 7);
                    str1 = string.Concat(str1, num1.ToString(), "W");
                }
                else
                {
                    fechaFinPago = fechaFinPago.AddDays(1);
                    int year = checked(fechaFinPago.Year - fechaInicioRelLab.Year);
                    if (DateTime.Compare(fechaInicioRelLab.AddYears(year), fechaFinPago) > 0)
                    {
                        year = checked(year - 1);
                    }

                    fechaInicioRelLab = fechaInicioRelLab.AddYears(year);
                    int month = checked(fechaFinPago.Month - fechaInicioRelLab.Month);
                    if (month == 0)
                    {
                        if (fechaInicioRelLab.Year < fechaFinPago.Year)
                        {
                            if (DateTime.Compare(fechaFinPago, fechaInicioRelLab) <= 0)
                            {
                                month = 12;
                                if (year > 0)
                                {
                                    year = checked(year - 1);
                                    fechaInicioRelLab = fechaInicioRelLab.AddYears(-1);
                                }
                            }
                            else
                            {
                                month = checked(month - 1);
                            }
                        }
                    }

                    if (month < 0)
                    {
                        month = checked(month + 12);
                    }

                    if (DateTime.Compare(fechaInicioRelLab.AddMonths(month), fechaFinPago) > 0)
                    {
                        month = checked(month - 1);
                    }

                    fechaInicioRelLab = fechaInicioRelLab.AddMonths(month);
                    if ((year <= 0 ? false : month == 0))
                    {
                        year = checked(year - 1);
                        month = 12;
                    }

                    timeSpan = fechaFinPago - fechaInicioRelLab;
                    int num = Convert.ToInt16(timeSpan.Days);

                    if (year > 0)
                    {
                        str1 = string.Concat(str1, year.ToString(), "Y");
                    }

                    if (month > 0)
                    {
                        str1 = string.Concat(str1, month.ToString(), "M");
                    }

                    str1 = string.Concat(str1, num.ToString(), "D");
                }
                antiguedad = str1;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            response = antiguedad;
            return response;
        }

        public string Calcular(DateTime fechaInicioRelLab, DateTime fechaFinPago)
        {
            TimeSpan diferencia = fechaFinPago - fechaInicioRelLab;
            int dias = ((diferencia.Days + 1) / 7);
            return string.Format("P{0}W", dias.ToString());
        }

        public string Calcular(DateTime fechaInicioRelLab, DateTime fechaFinPago, string metodo)
        {
            return "P{0}";
        }
    }
}
