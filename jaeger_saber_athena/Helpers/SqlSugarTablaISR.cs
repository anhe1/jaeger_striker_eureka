﻿using System;
using System.Linq;
using System.ComponentModel;
using Jaeger.Nomina.Entities;
using Jaeger.Nomina.Interface;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Nomina.Helpers
{
    public class SqlSugarTablaISR : MySqlSugarContext<ViewModelTablaImpuestoSobreRenta>, ISqlTablaISR
    {
        public SqlSugarTablaISR(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public bool Create() {
            throw new NotImplementedException();
        }

        public BindingList<ViewModelTablaImpuestoSobreRenta> Save(BindingList<ViewModelTablaImpuestoSobreRenta> items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].Id == 0)
                    items[i].Id = this.Insert(items[i]);
                else
                    this.Update(items[i]);
            }
            return items;
        }
    }
}
