﻿using System;
using System.Linq;
using System.ComponentModel;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Nomina.Entities;
using Jaeger.Nomina.Interface;

namespace Jaeger.Nomina.Helpers
{
    public class SqlSugarDepartamento : MySqlSugarContext<ViewModelDepartamento>, ISqlDepartamento
    {

        public SqlSugarDepartamento(DataBaseConfiguracion objeto) : base(objeto)
        {
            this.Puestos = new SqlSugarDeptoPuestos(objeto);
        }

        public ISqlDeptoPuestos Puestos { get; set; }

        public BindingList<ViewModelDepartamento> GetListBy()
        {
            var response = new BindingList<ViewModelDepartamento>(this.GetList().ToList());
            for (int i = 0; i < response.Count; i++)
            {
                response[i].Puestos = this.Puestos.GetListBy(response[i].Id);
                if (response[i].Puestos != null)
                {
                    for (int i2 = 0; i2 < response[i].Puestos.Count; i2++)
                    {
                        response[i].Puestos[i2].Funciones = this.Puestos.Funciones.GetLitBy(response[i].Puestos[i2].Id);
                    }
                }
            }
            return response;
        }

        public ViewModelDepartamento Save(ViewModelDepartamento objeto)
        {
            if (objeto.Id == 0)
                objeto.Id = this.Insert(objeto);
            else
                this.Update(objeto);
            return objeto;
        }

        public BindingList<ViewModelDepartamento> Save(BindingList<ViewModelDepartamento> datos)
        {
            for (int i = 0; i < datos.Count; i++)
            {
                if (datos[i].Id == 0)
                {
                    datos[i].Id = this.Insert(datos[i]);
                }
                else
                {
                    this.Update(datos[i]);
                }
                if (datos[i].Id > 0 && datos[i].Puestos.Count > 0)
                {
                    for (int idPuesto = 0; idPuesto < datos[i].Puestos.Count; idPuesto++)
                    {
                        if (datos[i].Puestos[idPuesto].Id == 0)
                        {
                            datos[i].Puestos[idPuesto].IdDepto = datos[i].Id;
                            datos[i].Puestos[idPuesto].Id = this.Puestos.Insert(datos[i].Puestos[idPuesto]);
                        }
                        else
                            this.Puestos.Update(datos[i].Puestos[idPuesto]);
                    }
                }
            }
            return datos;
        }
        
        public bool Create()
        {
            try
            {
                this.Db.CodeFirst.InitTables<ViewModelDepartamento,ViewModelDeptoPuesto, ViewModelDeptoFuncion>();
                return true;
            }
            catch (SqlSugar.VersionExceptions ex)
            {
                Console.WriteLine(ex.Message);
            }

            return false;
        }
    }
}
