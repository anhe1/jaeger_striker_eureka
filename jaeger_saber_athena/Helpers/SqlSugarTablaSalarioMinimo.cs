﻿using System.ComponentModel;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Nomina.Entities;
using Jaeger.Nomina.Interface;

namespace Jaeger.Nomina.Helpers
{
    public class SqlSugarTablaSalarioMinimo : MySqlSugarContext<ViewModelTablaSalarioMinimo>, ISqlTablaSalarioMinimo
    {
        public SqlSugarTablaSalarioMinimo(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public bool Create() {
            throw new System.NotImplementedException();
        }

        public BindingList<ViewModelTablaSalarioMinimo> Save(BindingList<ViewModelTablaSalarioMinimo> items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].Id == 0)
                    items[i].Id = this.Insert(items[i]);
                else
                    this.Update(items[i]);
            }
            return items;
        }
    }
}
