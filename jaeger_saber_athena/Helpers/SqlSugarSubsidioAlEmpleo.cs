﻿using System;
using System.Linq;
using System.ComponentModel;
using Jaeger.Nomina.Entities;
using Jaeger.Nomina.Interface;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.Nomina.Helpers
{
    public class SqlSugarSubsidioAlEmpleo : MySqlSugarContext<ViewModelTablaSubsidioAlEmpleo>, ISqlSubsidioAlEmpleo
    {
        public SqlSugarSubsidioAlEmpleo(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public bool Create() {
            throw new NotImplementedException();
        }

        public ViewModelTablaSubsidioAlEmpleo GetListBy(int periodo)
        {
            return this.Db.Queryable<ViewModelTablaSubsidioAlEmpleo>().Where(it => it.Periodo == periodo).ToList()[0];
        }

        public BindingList<ViewModelTablaSubsidioAlEmpleo> Save(BindingList<ViewModelTablaSubsidioAlEmpleo> items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].Id == 0)
                    items[i].Id = this.Insert(items[i]);
                else
                    this.Update(items[i]);
            }
            return items;
        }
    }

}
