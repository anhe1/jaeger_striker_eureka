﻿using System;
using Newtonsoft.Json;

namespace Jaeger.Nomina.Entities
{
    public class CuotasPatronalesIMSS : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private Configuracion configuracion;
        private decimal salarioDiarioField;
        private decimal salarioDiarioIntegradoField;
        private decimal cuotaFijaField;
        private decimal excedenteField;
        private decimal prestacionesDineroField;
        private decimal prestacionesEspecieField;
        private decimal invalidezVidaField;
        private decimal cesantiaVejezField;
        private decimal riesgoTrabajoField;
        private decimal guarderiasPrestacionesSocialesField;
        private decimal seguroRetiroField;
        private decimal infonavitField;
        private decimal totalField;

        public CuotasPatronalesIMSS()
        {

        }

        public Configuracion Configuracion
        {
            get
            {
                return this.configuracion;
            }
            set
            {
                this.configuracion = value;
            }
        }

        public CuotasPatronalesIMSS(ref Configuracion conf)
        {
            this.configuracion = conf;
        }

        public CuotasPatronalesIMSS(decimal sd, decimal sdi, ref Configuracion conf)
        {
            this.configuracion = conf;
            this.salarioDiarioField = sd;
            this.salarioDiarioIntegradoField = sdi;
        }

        [JsonProperty("sd")]
        public decimal SalarioDiario
        {
            get
            {
                return this.salarioDiarioField;
            }
            set
            {
                this.salarioDiarioField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("sdi")]
        public decimal SalarioDiarioIntegrado
        {
            get
            {
                return this.salarioDiarioIntegradoField;
            }
            set
            {
                this.salarioDiarioIntegradoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("exc")]
        public decimal Excedente
        {
            get
            {
                return this.excedenteField;
            }
            set
            {
                this.excedenteField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("prsdnr")]
        public decimal PrestacionesDinero
        {
            get
            {
                return this.prestacionesDineroField;
            }
            set
            {
                this.prestacionesDineroField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("prsesp")]
        public decimal PrestacionesEspecie
        {
            get
            {
                return this.prestacionesEspecieField;
            }
            set
            {
                this.prestacionesEspecieField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("invid")]
        public decimal InvalidezVida
        {
            get
            {
                return this.invalidezVidaField;
            }
            set
            {
                this.invalidezVidaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("cesvej")]
        public decimal CesantiaVejez
        {
            get
            {
                return this.cesantiaVejezField;
            }
            set
            {
                this.cesantiaVejezField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("cufij")]
        public decimal CuotaFija
        {
            get
            {
                return this.cuotaFijaField;
            }
            set
            {
                this.cuotaFijaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("ritr")]
        public decimal RiesgoTrabajo
        {
            get
            {
                return this.riesgoTrabajoField;
            }
            set
            {
                this.riesgoTrabajoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("guprs")]
        public decimal GuarderíasPrestacionesSociales
        {
            get
            {
                return this.guarderiasPrestacionesSocialesField;
            }
            set
            {
                this.guarderiasPrestacionesSocialesField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("sgrrt")]
        public decimal SeguroRetiro
        {
            get
            {
                return this.seguroRetiroField;
            }
            set
            {
                this.seguroRetiroField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("infon")]
        public decimal Infonavit
        {
            get
            {
                return this.infonavitField;
            }
            set
            {
                this.infonavitField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("ttl")]
        public decimal Total
        {
            get
            {
                return this.totalField;
            }
            set
            {
                this.totalField = value;
                this.OnPropertyChanged();
            }
        }

        public string Json(Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static CuotasPatronalesIMSS Json(string stringInput)
        {
            try
            {
                return JsonConvert.DeserializeObject<CuotasPatronalesIMSS>(stringInput);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public decimal Cuotas(int dias)
        {
            this.CuotaFija = ((this.configuracion.CuotasPatronal.CuotaFija * this.configuracion.SalarioMinimoGeneral)) * dias;
            // tres veces el salario minimo general menos el salario base de cotizacion por % cuota fija
            this.Excedente = (((this.configuracion.SalarioMinimoGeneral * 3) - this.SalarioDiarioIntegrado) * this.configuracion.CuotasPatronal.CuotaFija);
            this.PrestacionesDinero = ((this.SalarioDiarioIntegrado * this.configuracion.CuotasPatronal.PrestacionesDinero)) * dias;
            this.InvalidezVida = ((this.SalarioDiarioIntegrado * this.configuracion.CuotasPatronal.InvalidezVejez)) * dias;
            this.RiesgoTrabajo = ((this.SalarioDiarioIntegrado * this.configuracion.CuotasPatronal.RiesgoTrabajo)) * dias;
            this.GuarderíasPrestacionesSociales = ((this.SalarioDiarioIntegrado * this.configuracion.CuotasPatronal.Guarderias)) * dias;
            this.SeguroRetiro = ((this.SalarioDiarioIntegrado * this.configuracion.CuotasPatronal.Retiro)) * dias;
            this.CesantiaVejez = ((this.SalarioDiarioIntegrado * this.configuracion.CuotasPatronal.Cesantia)) * dias;
            this.Infonavit = ((this.SalarioDiarioIntegrado * 5) / 100) * dias;
            this.Total = this.CuotaFija + this.Excedente + this.PrestacionesDinero + this.InvalidezVida + this.InvalidezVida + this.RiesgoTrabajo + this.GuarderíasPrestacionesSociales + this.SeguroRetiro + this.CesantiaVejez + this.Infonavit;
            return this.Total;
        }

        public decimal Calcular(ref ViewModelNominaPeriodo e, ViewModelNominaControl p)
        {
            decimal smg3 = this.configuracion.SalarioMinimoGeneral * 3;
            #region riesgo de trabajo
            // =IF(OR('Nómina'!Status="Z",'Nómina'!Status=""),"",IF(OR('Nómina'!INC>0,'Nómina'!AUS>0),'Nómina'!SDI*('Nómina'!JT-(('Nómina'!INC+'Nómina'!AUS)/'Nómina'!JT)*'Nómina'!JT)*'Nómina'!RT,('Nómina'!SDI*'Nómina'!DP*'Nómina'!RT)))
            #endregion

            #region calcular cuota fija
            //= IF(OR('Nómina'!Status = "Z", 'Nómina'!Status = ""), "", 
            //IF(OR('Nómina'!INC > 0, 'Nómina'!AUS > 0), 
            if ((e.Incapacidad + e.Ausencias) > 0)
            {
                //(('Nómina'!SM) * ('Nómina'!JT - (('Nómina'!INC + 'Nómina'!AUS) / 'Nómina'!JT)*'Nómina'!JT)*'Nómina'!CF),
                this.CuotaFija = ((this.configuracion.SalarioMinimoGeneral) * (e.JornadasTrabajo - ((e.Incapacidad + e.Ausencias) / (decimal)e.JornadasTrabajo) * e.JornadasTrabajo) * this.configuracion.CuotasPatronal.CuotaFija);
            }
            else
            {
                //(('Nómina'!SM)*('Nómina'!DP * 'Nómina'!CF))))
                this.CuotaFija = ((this.configuracion.SalarioMinimoGeneral) * (p.DiasPeriodo * this.configuracion.CuotasPatronal.CuotaFija));
            }
            #endregion

            #region calcular excedente (excedente de 3 smgdf)
            //= IF(AND('Nómina'!Status = "A", 'Nómina'!SDI > 'Nómina'!TRESSM, 'Nómina'!INC + 'Nómina'!AUS > 0), 
            if ((e.SalarioDiarioIntegrado > smg3) && (e.Incapacidad + e.Ausencias) > 0)
            {
                //('Nómina'!SDI - 'Nómina'!TRESSM) * (('Nómina'!JT - ('Nómina'!INC + 'Nómina'!AUS)/ 'Nómina'!JT * 'Nómina'!JT)*'Nómina'!EXC_P),
                this.Excedente = (e.SalarioDiarioIntegrado - smg3) * ((e.JornadasTrabajo - (e.Incapacidad + e.Ausencias) / (decimal)e.JornadasTrabajo * e.JornadasTrabajo) * this.configuracion.CuotasPatronal.CuotaFija);//'Nómina'!EXC_P)
            }
            else if ((e.SalarioDiarioIntegrado > smg3) && (e.Incapacidad + e.Ausencias) == 0) //IF(AND('Nómina'!Status = "A", 'Nómina'!SDI > 'Nómina'!TRESSM, 'Nómina'!INC + 'Nómina'!AUS = 0), 
            {
                //('Nómina'!SDI - 'Nómina'!TRESSM) * 'Nómina'!DP * ('Nómina'!EXC_P),""))
                this.Excedente = (e.SalarioDiarioIntegrado - smg3) * p.DiasPeriodo * this.configuracion.CuotasPatronal.CuotaFija; // ('Nómina'!EXC_P)
            }
            else
            {
                this.Excedente = 0;
            }
            #endregion

            #region calcular prestaciones en dinero
            //IF(OR('Nómina'!Status = "Z", 'Nómina'!Status = ""), "", 
            //IF(OR('Nómina'!INC > 0, 'Nómina'!AUS > 0), 
            if (e.Incapacidad > 0 | e.Ausencias > 0)
            {
                //'Nómina'!SDI * ('Nómina'!JT - (('Nómina'!INC + 'Nómina'!AUS) / 'Nómina'!JT) * 'Nómina'!JT)*'Nómina'!PD_P,
                this.PrestacionesDinero = e.SalarioDiarioIntegrado * (e.JornadasTrabajo - ((e.Incapacidad + e.Ausencias) / (decimal)e.JornadasTrabajo) * e.JornadasTrabajo) * this.configuracion.CuotasPatronal.PrestacionesDinero;
            }
            else
            {
                //('Nómina'!SDI * 'Nómina'!DP * 'Nómina'!PD_P)))
                this.PrestacionesDinero = e.SalarioDiario * p.DiasPeriodo * this.configuracion.CuotasPatronal.PrestacionesDinero;
            }
            #endregion

            #region gastos medicos pensionados
            //= IF(OR('Nómina'!Status = "Z", 'Nómina'!Status = ""), "", 
            //IF(OR('Nómina'!INC > 0, 'Nómina'!AUS > 0), 
            if (e.Incapacidad > 0 | e.Ausencias > 0)
            {
                //'Nómina'!SDI * ('Nómina'!JT - (('Nómina'!INC + 'Nómina'!AUS) / 'Nómina'!JT) * 'Nómina'!JT)*'Nómina'!GMP_P,
                this.PrestacionesDinero = e.SalarioDiarioIntegrado * (e.JornadasTrabajo - ((e.Incapacidad + e.Ausencias) / (decimal)e.JornadasTrabajo) * e.JornadasTrabajo) * this.configuracion.CuotasPatronal.GastosMedicosPensionados;
            }
            else
            {
                //('Nómina'!SDI * 'Nómina'!DP * 'Nómina'!GMP_P)))
                this.PrestacionesDinero = e.SalarioDiarioIntegrado * p.DiasPeriodo * this.configuracion.CuotasPatronal.GastosMedicosPensionados;
            }
            #endregion

            #region Invalidez Vejez
            //= IF(OR('Nómina'!Status = "Z", 'Nómina'!Status = ""), "", 
            //IF(OR('Nómina'!INC > 0, 'Nómina'!AUS > 0), 
            if (e.Incapacidad > 0 | e.Ausencias > 0)
            {
                //'Nómina'!SDI * ('Nómina'!JT - (('Nómina'!INC + 'Nómina'!AUS) / 'Nómina'!JT) * 'Nómina'!JT)*'Nómina'!IV_P,
                this.InvalidezVida = e.SalarioDiarioIntegrado * (e.JornadasTrabajo - ((e.Incapacidad + e.Ausencias) / (decimal)e.JornadasTrabajo) * e.JornadasTrabajo) * this.configuracion.CuotasPatronal.InvalidezVejez;
            }
            else
            {
                //('Nómina'!SDI * 'Nómina'!DP * 'Nómina'!IV_P)))
                this.InvalidezVida = e.SalarioDiarioIntegrado * p.DiasPeriodo * this.configuracion.CuotasPatronal.InvalidezVejez;
            }
            #endregion

            #region Guarderias
            // =IF(OR('Nómina'!Status="Z",'Nómina'!Status=""),"",
            //IF(OR('Nómina'!INC>0,'Nómina'!AUS>0),
            if (e.Incapacidad > 0 | e.Ausencias > 0)
            {
                //'Nómina'!SDI*('Nómina'!JT-(('Nómina'!INC+'Nómina'!AUS)/'Nómina'!JT)*'Nómina'!JT)*'Nómina'!GPS,
                this.GuarderíasPrestacionesSociales = e.SalarioDiarioIntegrado * (e.JornadasTrabajo - ((e.Incapacidad + e.Ausencias) / (decimal)e.JornadasTrabajo) * e.JornadasTrabajo) * this.configuracion.CuotasPatronal.Guarderias;
            }
            else
            {
                //('Nómina'!SDI*'Nómina'!DP*'Nómina'!GPS)))
                this.GuarderíasPrestacionesSociales = e.SalarioDiarioIntegrado * p.DiasPeriodo * this.configuracion.CuotasPatronal.Guarderias;
            }
            #endregion

            #region calcular cesantia
            //= IF(OR('Nómina'!Status = "Z", 'Nómina'!Status = ""), "", 
            //IF(OR('Nómina'!INC > 0, 'Nómina'!AUS > 0), 
            if (e.Incapacidad > 0 | e.Ausencias > 0)
            {
                //'Nómina'!SDI * ('Nómina'!JT - (('Nómina'!INC + 'Nómina'!AUS) / 'Nómina'!JT) * 'Nómina'!JT)*'Nómina'!CESA_P,
                this.CesantiaVejez = e.SalarioDiarioIntegrado * (e.JornadasTrabajo - ((e.Incapacidad + e.Ausencias) / (decimal)e.JornadasTrabajo) * e.JornadasTrabajo) * this.configuracion.CuotasPatronal.Cesantia;
            }
            else
            {
                //('Nómina'!SDI * 'Nómina'!DP * 'Nómina'!CESA_P)))
                this.CesantiaVejez = e.SalarioDiarioIntegrado * p.DiasPeriodo * this.configuracion.CuotasPatronal.Cesantia;
            }
            #endregion

            #region calcular retiro
            //=IF(OR('Nómina'!Status="Z",'Nómina'!Status=""),"",
            //IF(OR('Nómina'!INC>0,'Nómina'!AUS>0),
            if (e.Incapacidad > 0 | e.Ausencias > 0)
            {
                //'Nómina'!SDI*('Nómina'!JT-(('Nómina'!INC+'Nómina'!AUS)/'Nómina'!JT)*'Nómina'!JT)*'Nómina'!SAR,}
                this.SeguroRetiro = e.SalarioDiarioIntegrado * (e.JornadasTrabajo - ((e.Incapacidad + e.Ausencias) / (decimal)e.JornadasTrabajo) * e.JornadasTrabajo) * this.configuracion.CuotasPatronal.Retiro;
            }
            else
            {
                //('Nómina'!SDI*'Nómina'!DP*'Nómina'!SAR)))
                this.SeguroRetiro = e.SalarioDiarioIntegrado * p.DiasPeriodo * this.configuracion.CuotasPatronal.Retiro;
            }
            #endregion

            #region calcular infonavit
            //= IF(OR('Nómina'!Status = "Z", 'Nómina'!Status = ""), "", 
            //IF(OR('Nómina'!INC > 0, 'Nómina'!AUS > 0), 
            if (e.Incapacidad > 0 | e.Ausencias > 0)
            {
                //'Nómina'!SDI * ('Nómina'!JT - (('Nómina'!INC + 'Nómina'!AUS) / 'Nómina'!JT) * 'Nómina'!JT)*'Nómina'!INFO,
                this.Infonavit = e.SalarioDiarioIntegrado * (e.JornadasTrabajo - ((e.Incapacidad + e.Ausencias) / (decimal)e.JornadasTrabajo) * e.JornadasTrabajo) * new decimal(0.0500);
            }
            else
            {
                //('Nómina'!SDI * 'Nómina'!DP * 'Nómina'!INFO)))
                this.Infonavit = e.SalarioDiarioIntegrado * p.DiasPeriodo * new decimal(0.0500);
            }
            #endregion

            this.Total = this.CuotaFija + this.Excedente + this.PrestacionesDinero + this.InvalidezVida + this.InvalidezVida + this.RiesgoTrabajo + this.GuarderíasPrestacionesSociales + this.SeguroRetiro + this.CesantiaVejez + this.Infonavit;
            return this.Total;
        }
    }
}
