﻿using System.ComponentModel;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.Services;
using Newtonsoft.Json;

namespace Jaeger.Nomina.Entities
{
    public class Configuracion : BasePropertyChangeImplementation
    {
        public enum EnumArticuloISR
        {
            Articulo96,
            Articulo174
        }

        public enum EnumUsarParaCalcular
        {
            UMA,
            SalarioMinimoGeneral
        }

        private decimal salarioMinimoGeneralField;
        private decimal umaField;
        private decimal diasPorAnioField;
        private decimal diasPorMesField;
        private decimal diasDeVacacionesField;
        private decimal primaVacacionalField;
        private decimal diasPrimaVacacionalField;
        private decimal diasAguinaldoField;
        private EnumArticuloISR articuloISR;
        private EnumUsarParaCalcular usarParaCalcular;
        private decimal porcentajeValesDespensaField;
        private decimal porcentajeFondoAhorroField;
        private decimal porcentajePremioPuntualidadField;
        private decimal impuestoSobreNominaField;
        private ConfiguracionCuotasObrero cuotasObreroField;
        private ConfiguracionCuotasPatronal cuotasPatronalField;

        public Configuracion()
        {
            this.salarioMinimoGeneralField = new decimal(88.36);
            this.umaField = new decimal(80.6);
            this.diasPorAnioField = 365;
            this.diasPorMesField = new decimal(30.4);
            this.diasDeVacacionesField = 6;
            this.primaVacacionalField = new decimal(0.25);
            this.diasPrimaVacacionalField = new decimal(15);
            this.diasAguinaldoField = 15;
            this.articuloISR = EnumArticuloISR.Articulo96;
            this.usarParaCalcular = EnumUsarParaCalcular.SalarioMinimoGeneral;
            this.porcentajeValesDespensaField = new decimal(.4);
            this.porcentajeFondoAhorroField = new decimal(.13);
            this.porcentajePremioPuntualidadField = new decimal(.1);
            this.impuestoSobreNominaField = new decimal(.03);
            this.cuotasObreroField = new ConfiguracionCuotasObrero();
            this.cuotasPatronalField = new ConfiguracionCuotasPatronal();
        }

        /// <summary>
        /// obtener o establecer el salario minimo general
        /// </summary>
        [DisplayName("Salario Minimo General")]
        [Description("Según fue estipulado por la Comisión Nacional de los Salarios Mínimos, a partir del 1 de enero de 2010, mediante resolución publicada en el Diario Oficial de la Federación del 23 de diciembre de 2009 se estableció el siguiente salario mínimo para 2010 según cada aérea geográfica")]
        public decimal SalarioMinimoGeneral
        {
            get
            {
                return this.salarioMinimoGeneralField;
            }
            set
            {
                this.salarioMinimoGeneralField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Unidad de Medida Actualizada (UMA)
        /// </summary>
        [DisplayName("Unidad de Medida Actualizada (UMA)"), Category("Calcular ISR")]
        [Description("a Unidad de Medida y Actualización (UMA) es la referencia económica en pesos para determinar la cuantía del pago de las obligaciones y supuestos previstos en las leyes federales, de las entidades federativas, así como en las disposiciones jurídicas que emanen de todas las anteriores.")]
        public decimal UMA
        {
            get
            {
                return this.umaField;
            }
            set
            {
                this.umaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer dias por año
        /// </summary>
        [DisplayName("Dias por Año")]
        public decimal DiasPorAnio
        {
            get
            {
                return this.diasPorAnioField;
            }
            set
            {
                this.diasPorAnioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer dias por mes
        /// </summary>
        [DisplayName("Dias por Mes")]
        public decimal DiasPorMes
        {
            get
            {
                return this.diasPorMesField;
            }
            set
            {
                this.diasPorMesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer dias de vacaciones
        /// </summary>
        [DisplayName("Dias de Vacaciones")]
        public decimal DiasDeVacaciones
        {
            get
            {
                return this.diasDeVacacionesField;
            }
            set
            {
                this.diasDeVacacionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer prima vacacional
        /// </summary>
        [DisplayName("Prima Vacacional %")]
        [Description("La prima vacacional es la prestación en dinero, a la cual tiene derecho el trabajador y que tiene como objetivo que este último, tenga un ingreso extra para disfrutar en sus vacaciones.")]
        public decimal PrimaVacacional
        {
            get
            {
                return this.primaVacacionalField;
            }
            set
            {
                this.primaVacacionalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer dias de la prima vacacional
        /// </summary>
        [DisplayName("Dias de la Prima Vacacional")]
        public decimal DiasPrimaVacacional
        {
            get
            {
                return this.diasPrimaVacacionalField;
            }
            set
            {
                this.diasPrimaVacacionalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer dias de aguinaldo
        /// </summary>
        [DisplayName("Dias de Aguinaldo")]
        public decimal DiasAguinaldo
        {
            get
            {
                return this.diasAguinaldoField;
            }
            set
            {
                this.diasAguinaldoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si debera usar el Articulo 96 o 174 para la retencion del ISR
        /// </summary>
        [Category("Calcular ISR"), Description("Articulo que debera usarse para el calculo del ISR")]
        public EnumArticuloISR ArticuloISR
        {
            get
            {
                return this.articuloISR;
            }
            set
            {
                this.articuloISR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si debe usar UMA o Salario Minimo General
        /// </summary>
        [Category("Calcular ISR")]
        public EnumUsarParaCalcular UsarParaCalcular
        {
            get
            {
                return this.usarParaCalcular;
            }
            set
            {
                this.usarParaCalcular = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el % maximo para el tope de los vales de despensa
        /// </summary>
        [DisplayName("% Max. Vales de Despensa"), Description("% Max. utilizado como tope para vales de despesnsa")]
        public decimal PorcentajeValesDespensa
        {
            get
            {
                return this.porcentajeValesDespensaField;
            }
            set
            {
                this.porcentajeValesDespensaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el porcetaje que se debera aplicar para calcular el monto del fondo de ahorro
        /// </summary>
        [DisplayName("% Max. Fondo de Ahorro"), Description("Porcentaje (%) que debera ser aplicado para el fondo de ahorro.")]
        public decimal PorcentajeFondoAhorro
        {
            get
            {
                return this.porcentajeFondoAhorroField;
            }
            set
            {
                this.porcentajeFondoAhorroField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el porcentaje utilizado para calcular los premios de puntualidad
        /// </summary>
        [DisplayName("% Max. Premios de Puntualidad"), Description("Porcentaje (%) que debera ser aplicado para los premios de puntualidad y asistencia.")]
        public decimal PorcentajePremioPuntualidad
        {
            get
            {
                return this.porcentajePremioPuntualidadField;
            }
            set
            {
                this.porcentajePremioPuntualidadField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Impuesto sober Nominas GDF"), Description("3% de nómina.")]
        public decimal ImpuestoSobreNomina
        {
            get
            {
                return this.impuestoSobreNominaField;
            }
            set
            {
                this.impuestoSobreNominaField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Cuotas Obrero"), Category("IMSS Cuotas Obrero Patronales")]
        [TypeConverter(typeof(PropertiesConvert))]
        public ConfiguracionCuotasObrero CuotasObrero
        {
            get
            {
                return this.cuotasObreroField;
            }
            set
            {
                this.cuotasObreroField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Cuotas Patron"), Category("IMSS Cuotas Obrero Patronales")]
        [TypeConverter(typeof(PropertiesConvert))]
        public ConfiguracionCuotasPatronal CuotasPatronal
        {
            get
            {
                return this.cuotasPatronalField;
            }
            set
            {
                this.cuotasPatronalField = value;
                this.OnPropertyChanged();
            }
        }

        public string Json()
        {
            return JsonConvert.SerializeObject(this, Formatting.None);
        }

        public static Configuracion Json(string value)
        {
            return JsonConvert.DeserializeObject<Jaeger.Nomina.Entities.Configuracion>(value);
        }
    }
}