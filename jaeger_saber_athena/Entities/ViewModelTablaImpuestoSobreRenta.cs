﻿using System;
using System.Linq;
using System.ComponentModel;
using Newtonsoft.Json;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    [SugarTable("_tblisr", "nomina: tabla de isr")]
    public class ViewModelTablaImpuestoSobreRenta : BasePropertyChangeImplementation
    {
        private int indiceField;
        private BindingList<ImpuestoSobreRenta> rangos;
        private string etiquetaField;
        private int periodoField;

        public ViewModelTablaImpuestoSobreRenta()
        {
            this.rangos = new BindingList<ImpuestoSobreRenta>();
        }

        [SugarColumn(ColumnName = "_tblisr_id", IsPrimaryKey = true, IsOnlyIgnoreInsert = true, IsIdentity = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_tblisr_desc", ColumnDescription = "descripcion", Length = 30)]
        public string Etiqueta
        {
            get
            {
                return etiquetaField;
            }
            set
            {
                etiquetaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_tblisr_prd", ColumnDescription = "dias del periodo")]
        public int Periodo
        {
            get
            {
                return periodoField;
            }
            set
            {
                periodoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ImpuestoSobreRenta> Rangos
        {
            get
            {
                return this.rangos;
            }
            set
            {
                this.rangos = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_tblisr_json", ColumnDataType = "TEXT", ColumnDescription = "tabla de rangos", IsNullable = true)]
        public string Json
        {
            get
            {
                return JsonConvert.SerializeObject(this.Rangos);
            }
            set
            {
                try
                {
                    this.Rangos = JsonConvert.DeserializeObject<BindingList<ImpuestoSobreRenta>>(value);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.Rangos = null;
                }
            }
        }

        public ImpuestoSobreRenta GetImpuestoSobreRenta(decimal baseGravable)
        {
            try
            {
                return this.rangos.First((ImpuestoSobreRenta p) => baseGravable >= p.LimiteInferior & baseGravable <= p.LimiteSuperior);
            }
            catch (Exception)
            {
                ImpuestoSobreRenta isr = new ImpuestoSobreRenta() { LimiteInferior = 0 };
                return isr;
            }
        }

        public decimal Calcular(decimal baseGravable)
        {
            ImpuestoSobreRenta t = this.GetImpuestoSobreRenta(baseGravable);
            decimal excedenteLimiteInferior = baseGravable - t.LimiteInferior;
            decimal impuestoMarginal = (excedenteLimiteInferior * t.PorcentajeExcedente) / 100;
            decimal impuestoDeterminado = impuestoMarginal + t.CuotaFija;
            return impuestoDeterminado;
        }
    }
}
