﻿using System;
using System.Linq;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    public class EmpleadoAguinaldo : BasePropertyChangeImplementation
    {
        private int numField;
        private string claveField;
        private string nombreField;
        private decimal salarioDiarioField;
        private decimal diasLaborados;
        private decimal salarioMensualField;
        private decimal aguinaldoField;
        private decimal baseGravableField;
        private decimal baseExcentoField;
        private decimal impuestoSobreRentaField;
        private decimal subsidioField;
        private decimal isrSueldoField;
        private decimal isrAguinaldoField;

        public EmpleadoAguinaldo()
        {

        }

        public EmpleadoAguinaldo(ViewModelEmpleado empleado)
        {
            this.numField = empleado.Num;
            this.claveField = empleado.Clave;
            this.nombreField = empleado.NombreCompleto();
            this.salarioDiarioField = empleado.SalarioDiario;
            this.diasLaborados = 365;
        }

        /// <summary>
        /// obtener o establecer el numero del empleado
        /// </summary>
        public int Num
        {
            get
            {
                return this.numField;
            }
            set
            {
                this.numField = value;
                this.OnPropertyChanged();
            }
        }

        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        public string Nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal SalarioDiario
        {
            get
            {
                return this.salarioDiarioField;
            }
            set
            {
                this.salarioDiarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener el monto del salario mensual
        /// </summary>
        public decimal SalarioMensual
        {
            get
            {
                return this.salarioMensualField;
            }
            set
            {
                this.salarioMensualField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener el monto del aguinaldo
        /// </summary>
        public decimal Aguinaldo
        {
            get
            {
                return this.aguinaldoField;
            }
            set
            {
                this.aguinaldoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad de dias laborados para el aguinaldo
        /// </summary>
        public decimal DiasLaborados
        {
            get
            {
                return this.diasLaborados;
            }
            set
            {
                this.diasLaborados = value;
                this.OnPropertyChanged();
            }
        }

        public decimal BaseGravable
        {
            get
            {
                return this.baseGravableField;
            }
            set
            {
                this.baseGravableField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal BaseExento
        {
            get
            {
                return this.baseExcentoField;
            }
            set
            {
                this.baseExcentoField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal BaseGravada
        {
            get
            {
                decimal c = this.Aguinaldo - this.BaseExento;
                if (c <= 0)
                {
                    return 0;
                }
                return this.Aguinaldo - this.BaseExento;
            }
        }

        public decimal ISR
        {
            get
            {
                return this.impuestoSobreRentaField;
            }
            set
            {
                this.impuestoSobreRentaField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Subsidio
        {
            get
            {
                return this.subsidioField;
            }
            set
            {
                this.subsidioField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal ISRSalarioMasAguinaldo
        {
            get
            {
                return this.ISR - this.Subsidio;
            }
        }

        public decimal ISRSueldo
        {
            get
            {
                return this.isrSueldoField;
            }
            set
            {
                this.isrSueldoField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal ISRAguinaldo
        {
            get
            {
                return this.isrAguinaldoField;
            }
            set
            {
                this.isrAguinaldoField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal AguinaldoPagar
        {
            get
            {
                return this.Aguinaldo - this.ISRAguinaldo;
            }
        }
    }
}
