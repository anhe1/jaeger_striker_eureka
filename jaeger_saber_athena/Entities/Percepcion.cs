﻿using Newtonsoft.Json;
using Jaeger.Nomina.Interface;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    [JsonObject]
    public class Percepcion : BasePropertyChangeImplementation, IPercepcion
    {
        private string identificadorField;
        private string claveField;
        private string tipoField;
        private string conceptoField;
        private decimal importeExentoField;
        private decimal importeGravadoField;

        public Percepcion()
        {
        }

        public Percepcion(string pIndetificador, string pClave, string pTipo, string pConcepto, decimal pExcento = 0, decimal pGravado = 0)
        {
            this.identificadorField = pIndetificador;
            this.claveField = pClave;
            this.tipoField = pTipo;
            this.conceptoField = pConcepto;
            this.importeExentoField = pExcento;
            this.importeGravadoField = pGravado;
        }

        /// <summary>
        /// clave de indetifacion del catalogo SAT
        /// </summary>
        [JsonProperty(propertyName: "ide")]
        public string Identificador
        {
            get
            {
                return this.identificadorField;
            }
            set
            {
                this.identificadorField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty(propertyName: "clv")]
        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty(propertyName: "tip")]
        public string Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty(propertyName: "cnp")]
        public string Concepto
        {
            get
            {
                return this.conceptoField;
            }
            set
            {
                this.conceptoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty(propertyName: "ext")]
        public decimal ImporteExento
        {
            get
            {
                return this.importeExentoField;
            }
            set
            {
                this.importeExentoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty(propertyName: "grv")]
        public decimal ImporteGravado
        {
            get
            {
                return this.importeGravadoField;
            }
            set
            {
                this.importeGravadoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty(propertyName: "ttl")]
        public decimal ImporteTotal
        {
            get
            {
                return this.ImporteGravado + this.ImporteExento;
            }
        }
    }
}