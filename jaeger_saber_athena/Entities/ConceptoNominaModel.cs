﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Nomina.Entities
{
    public class ConceptoNominaModel
    {
        private int index;
        private bool activo;
        private int orden;
        private string descripcion;
        private string concepto;
        private string claveSAT;
        private EnumTipo tipo;

        public enum EnumTipo
        {
            Percepcion,
            Deduccion
        }

        public int Id
        {
            get { return index; }
            set { index = value; }
        }

        public bool Activo
        {
            get { return activo; }
            set { activo = value; }
        }

        public int Orden
        {
            get { return orden; }
            set { orden = value; }
        }
        
        public string Concepto
        {
            get { return concepto; }
            set { concepto = value; }
        }
        
        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        public string ClaveSAT
        {
            get { return claveSAT; }
            set { claveSAT = value; }
        }

        public EnumTipo Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
    }
}
