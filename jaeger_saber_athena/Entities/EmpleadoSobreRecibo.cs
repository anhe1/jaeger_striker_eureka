﻿/// develop: anhe 020220192337
/// purpose: clase base para crear el sobre recibo de pago del empleado
using System;
using System.Linq;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Jaeger.Nomina.Entities
{
    public class EmpleadoSobreRecibo
    {
        public EmpleadoSobreRecibo()
        {

        }

        public EmpleadoSobreRecibo(ViewModelNominaPeriodo n, ViewModelEmpleado e)
        {
            this.Numero = n.Numero;
            this.RFC = n.RFC;
            this.CURP = n.CURP;
            this.Nombres = n.Nombres;
            this.PrimerApellido = n.PrimerApellido;
            this.SegundoApellido = n.SegundoApellido;
            this.SalarioDiario = n.SalarioDiario;
            this.SalarioDiarioIntegrado = n.SalarioDiarioIntegrado;
            this.FechaInicial = DateTime.Now;
            this.FechaFinal = DateTime.Now;
            this.FechaInicioRelLaboral = DateTime.Now;
            this.FechaPago = DateTime.Now;
            this.JPercepciones = n.JPercepciones;
            this.JDeducciones = n.JDeducciones;
            Console.Write(e.RFC);
        }

        /// <summary>
        /// obtener o establecer el numero del empleado
        /// </summary>
        public int Numero { get; set; }

        /// <summary>
        /// obtener o establacer la clave del registro federal de contribuyentes
        /// </summary>
        public string RFC { get; set; }

        /// <summary>
        /// obtener o establecer clave unica de registro de poblacion (CURP)
        /// </summary>
        public string CURP { get; set; }

        /// <summary>
        /// obtener el nombre completo del trabajador
        /// </summary>
        public string Nombre
        {
            get
            {
                return string.Format(format: "{0} {1} {2}", arg0: this.PrimerApellido, arg1: this.SegundoApellido, arg2: this.Nombres);
            }
        }

        /// <summary>
        /// obtener o establecer el o los nombres del trabajador
        /// </summary>
        public string Nombres { get; set; }

        /// <summary>
        /// obtener o establecer el apellido paterno del trabajador
        /// </summary>
        public string PrimerApellido { get; set; }

        /// <summary>
        /// obtener o establecer el apellido materno del trabajador
        /// </summary>
        public string SegundoApellido { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de inicio de relacion laboral
        /// </summary>
        public DateTime? FechaInicioRelLaboral { get; set; }
        
        /// <summary>
        /// obtener o establacer el salario base de cotizacion
        /// </summary>
        public decimal SalarioBase { get; set; }

        /// <summary>
        /// obtener o establcer el salario diario del trabajador
        /// </summary>
        public decimal SalarioDiario { get; set; }

        /// <summary>
        /// obtener o establecer el salario diario integrado
        /// </summary>
        public decimal SalarioDiarioIntegrado { get; set; }

        /// <summary>
        /// obtener o establecer el numero de seguridad social
        /// </summary>
        public string NumSeguridadSocial { get; set; }

        /// <summary>
        /// obtener o establecer el registro patronal
        /// </summary>
        public string RegistroPatronal { get; set; }

        /// <summary>
        /// obtener o establecer el departamento
        /// </summary>
        public string Departamento { get; set; }

        /// <summary>
        /// obtener o establcer el puesto
        /// </summary>
        public string Puesto { get; set; }

        /// <summary>
        /// obtener o establecer la clave de antiguedad del empleado
        /// </summary>
        public string Antiguedad { get; set; }

        /// <summary>
        /// obtener o establecer los dias trabajados
        /// </summary>
        public int DiasTrabajados { get; set; }

        /// <summary>
        /// obtener o establecer la perioricidad de pago
        /// </summary>
        public string PerioricidadPago { get; set; }

        /// <summary>
        /// obtener o establecer la fecha inicial de pago
        /// </summary>
        public DateTime FechaInicial { get; set; }

        /// <summary>
        /// obtener o establecer la fecha final de pago
        /// </summary>
        public DateTime FechaFinal { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de pago
        /// </summary>
        public DateTime FechaPago { get; set; }

        /// <summary>
        /// obtener o establecer la clave del tipo de regimen del empleado
        /// </summary>
        public string TipoRegimen { get; set; }

        /// <summary>
        /// obtener o establecer la clave de tipo de contrato del empleado
        /// </summary>
        public string TipoContrato { get; set; }

        /// <summary>
        /// obtener o establecer la clave de tipo de jornada del empleado
        /// </summary>
        public string TipoJornada { get; set; }

        /// <summary>
        /// obtener o establecer la clave de moneda
        /// </summary>
        public string Moneda { get; set; }

        public string Leyenda
        {
            get
            {
                return string.Format("Recibí de {0} la cantidad indicada que cubre a la fecha el importe de mi salario, tiempo extra y todas las percepciones y prestaciones a que tengo derecho sin que se me adeude alguna cantidad por otro concepto", "");
            }
        }

        public BindingList<Percepcion> Percepciones;

        public BindingList<Deduccion> Deducciones;

        public string JPercepciones
        {
            get
            {
                if (this.Percepciones.Count > 0)
                {
                    return JsonConvert.SerializeObject(this.Percepciones, Formatting.None);
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {
                this.Percepciones = JsonConvert.DeserializeObject<BindingList<Percepcion>>(value);
            }
        }

        public string JDeducciones
        {
            get
            {
                if (this.Deducciones.Count > 0)
                {
                    return JsonConvert.SerializeObject(this.Deducciones, Formatting.None);
                }
                return string.Empty;
            }
            set
            {
                this.Deducciones = JsonConvert.DeserializeObject<BindingList<Deduccion>>(value);
            }
        }
    }
}
