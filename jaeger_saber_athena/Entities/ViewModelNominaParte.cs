﻿using System;
using System.Linq;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    ///<summary>
    ///percepciones, deducciones
    ///</summary>
    [SugarTable("_nmnprt")]
    public class ViewModelNominaParte : BasePropertyChangeImplementation
    {
        private int id;
        private bool activo = true;
        private int subId;
        private ViewModelNominaParte.EnumNominaElemento tipo;
        private string claveTipo;
        private int quincena;
        private int anio;
        private int dias;
        private int horasExtra;
        private int diasIncapacidad;
        private decimal importeGravado;
        private decimal importeExento;
        private decimal descuento;
        private decimal importePagado;
        private string tipoHoras;
        private string clave;
        private string concepto;
        private DateTime fechaNuevo;
        private DateTime? fechaModificaField;
        private string creo;
        private string modifica;
        private string numEmpleado;
        private decimal importe;

        public enum EnumNominaElemento
        {
            None,
            Percepcion,
            Deduccion,
            HorasExtra,
            Incapacidad,
            AccionesOTitulos,
            JubilacionPensionRetiro,
            SeparacionIndemnizacion,
            OtrosPagos,
            CompensacionSaldosAFavor
        }

        public ViewModelNominaParte()
        {
        }

        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_id", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:registro activo
        /// Default:1
        /// Nullable:False
        /// </summary>       
        [SugarColumn(ColumnName = "_nmnprt_a", ColumnDescription = "registro activo", IsNullable = false)]
        public bool Activo
        {
            get
            {
                return this.activo;
            }
            set
            {
                this.activo = value;
            }
        }

        /// <summary>
        /// Desc:relacion con catalogo de nominas
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_cfdnmn_id", IsNullable = true)]
        public int SubId
        {
            get
            {
                return this.subId;
            }
            set
            {
                this.subId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:1=Percepcion, 2=Deduccion, 3=Horas Extras, 4=Incapacidad
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_doc", IsNullable = true)]
        public ViewModelNominaParte.EnumNominaElemento Elemento
        {
            get
            {
                return this.tipo;
            }
            set
            {
                this.tipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Tipo: Clave agrupadora. Clasifica la percepción o deducción, ó Razón de la incapacidad: Catálogo publicado en el portal del SAT en internet
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_tipo", IsNullable = true)]
        public string ClaveTipo
        {
            get
            {
                return this.claveTipo;
            }
            set
            {
                this.claveTipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Numero de Quincena
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_qnc", IsNullable = true)]
        public int Quincena
        {
            get
            {
                return this.quincena;
            }
            set
            {
                this.quincena = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Año de poliza
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_anio", IsNullable = true)]
        public int Anio 
        {
            get
            {
                return this.anio;
            }
            set
            {
                this.anio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Número de días en que el trabajador realizó horas extra en el periodo
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_dias", IsNullable = true)]
        public int Dias 
        {
            get
            {

                return this.dias;
            }
            set
            {
                this.dias = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:HorasExtra: Número de horas extra trabajadas en el periodo
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_hrsxtr", IsNullable = true)]
        public int HorasExtra
        {
            get
            {
                return this.horasExtra;
            }
            set
            {
                this.horasExtra = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:DiasIncapacidad: Número de días que el trabajador se incapacitó en el periodo
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_dsincp", IsNullable = true)]
        public int DiasIncapacidad
        {
            get
            {
                return this.diasIncapacidad;
            }
            set
            {
                this.diasIncapacidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ImporteGravado: representa el importe gravado de un concepto de percepción o deducción
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_impgrv", IsNullable = true)]
        public decimal ImporteGravado
        {
            get
            {
                return this.importeGravado;
            }
            set
            {
                this.importeGravado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ImporteExento: representa el importe exento de un concepto de percepción o deducción
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_impext", IsNullable = true)]
        public decimal ImporteExento
        {
            get
            {
                return this.importeExento;
            }
            set
            {
                this.importeExento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Monto del descuento por la incapacidad
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_dscnt", IsNullable = true)]
        public decimal Descuento
        {
            get
            {
                return this.descuento;
            }
            set
            {
                this.descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Importe pagado por las horas extra
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_imppgd", IsNullable = true)]
        public decimal ImportePagado
        {
            get
            {
                return this.importePagado;
            }
            set
            {
                this.importePagado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:TipoHoras: tipo de pago de las horas extra: dobles o triples
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_tphrs", IsNullable = true)]
        public string TipoHoras
        {
            get
            {
                return this.tipoHoras;
            }
            set
            {
                this.tipoHoras = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave de percepción ó deducción de nómina propia de la contabilidad de cada patrón, puede conformarse desde 3 hasta 15 caracteres
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_clv", IsNullable = true)]
        public string Clave
        {
            get
            {
                return this.clave;
            }
            set
            {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:descripción del concepto de percepción o deducción
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_cncpt", IsNullable = true)]
        public string Concepto
        {
            get
            {
                return this.concepto;
            }
            set
            {
                this.concepto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fec. sist.
        /// Default:CURRENT_TIMESTAMP
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_fn")]
        public DateTime FecNuevo
        {
            get
            {
                return this.fechaNuevo;
            }
            set
            {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        
        /// <summary>
        /// Desc:fecha de modificacion
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_fm", IsNullable = true)]
        public DateTime? FecModifica
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModificaField >= firstGoodDate)
                    return this.fechaModificaField.Value;
                return null;
            }
            set
            {
                this.fechaModificaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:usuario creo
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_usr_n", IsNullable = true)]
        public string Creo
        {
            get
            {
                return this.creo;
            }
            set
            {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ultimo usuario que modifico
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_usr_m", IsNullable = true)]
        public string Modifica
        {
            get
            {
                return this.modifica;
            }
            set
            {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:NumEmpleado: requerido para expresar el número de empleado de 1 a 15 posiciones
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_numem", IsNullable = true)]
        public string NumEmpleado
        {
            get
            {
                return this.numEmpleado;
            }
            set
            {
                this.numEmpleado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:
        /// Default:0.00
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_imprt", IsNullable = true)]
        public decimal Importe
        {
            get
            {
                return this.importe;
            }
            set
            {
                this.importe = value;
                this.OnPropertyChanged();
            }
        }

    }
}
