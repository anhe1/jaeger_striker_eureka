﻿using System;
using System.ComponentModel;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    public class EmpleadoAcumuladoHoras : BasePropertyChangeImplementation
    {
        private int indiceField;
        private string claveField;
        private string nombreCompletoField;
        private string primerapellidoField;
        private string segundoapellidoField;
        private long horasCargoField;
        private long horasAbonoField;

        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        public string Nombre
        {
            get
            {
                return string.Format(format: "{0} {1} {2}", arg0: this.PrimerApellido, arg1: this.SegundoApellido, arg2: this.Nombres);
            }
        }

        [Browsable(false)]
        public string Nombres
        {
            get
            {
                return this.nombreCompletoField;
            }
            set
            {
                this.nombreCompletoField = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public string PrimerApellido
        {
            get
            {
                return this.primerapellidoField;
            }
            set
            {
                this.primerapellidoField = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public string SegundoApellido
        {
            get
            {
                return this.segundoapellidoField;
            }
            set
            {
                this.segundoapellidoField = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public long HorasCargo
        {
            get
            {
                return this.horasCargoField;
            }
            set
            {
                this.horasCargoField = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public long HorasAbono
        {
            get
            {
                return this.horasAbonoField;
            }
            set
            {
                this.horasAbonoField = value;
                this.OnPropertyChanged();
            }
        }

        public TimeSpan HrsCargo
        {
            get
            {
                return TimeSpan.FromTicks(this.HorasCargo);
            }
            set
            {
                this.HorasCargo = value.Ticks;
                this.OnPropertyChanged();
            }
        }

        public TimeSpan HrsAbono
        {
            get
            {
                return TimeSpan.FromTicks(this.HorasAbono);
            }
            set
            {
                this.horasAbonoField = value.Ticks;
                this.OnPropertyChanged();
            }
        }
    }
}
