﻿using System;
using System.ComponentModel;
using System.Linq;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    [SugarTable("_nmnctrl", "nomina: control de periodos de la nomina")]
    public class ViewModelNominaControl : BasePropertyChangeImplementation
    {
        public enum EnumTipoPeriodo
        {
            Semanal
        }

        public enum EnumTipoNomina
        {
            Ordinaria,
            Extraordinaria,
        }

        public enum EnumStatusNomina
        {
            [Description("Por Autorizar")]
            PorAutorizar,
            [Description("Autorizada")]
            Autorizada
        }

        private int indiceField;
        private bool activoField;
        private string tituloField;
        private DateTime fechaInicioField;
        private DateTime fechaFinField;
        private DateTime? fechaPagoField;
        private DateTime? fechaAutorizaField;
        private EnumTipoPeriodo tipoField;
        private EnumTipoNomina tipoNominaField;
        private EnumStatusNomina estadoField;
        private ViewModelTablaImpuestoSobreRenta tablaIsrField;
        private ViewModelTablaSubsidioAlEmpleo tablaSubsidioField;
        private BindingList<ViewModelTablaProductividad> tablaProductividadField;
        private BindingList<ViewModelNominaPeriodo> empleadosField;
        private string autorizaField;
        private string creoField;
        private string modificaField;
        private DateTime fechaNuevoField;
        private DateTime? fechaModificaField; 

        public ViewModelNominaControl()
        {
            this.activoField = true;
            this.tablaProductividadField = new BindingList<ViewModelTablaProductividad>();
            this.tablaSubsidioField = new ViewModelTablaSubsidioAlEmpleo();
            this.tablaIsrField = new ViewModelTablaImpuestoSobreRenta();
            this.empleadosField = new BindingList<ViewModelNominaPeriodo>();
            this.fechaNuevoField = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_id", ColumnDescription = "indice", IsOnlyIgnoreInsert = true, IsPrimaryKey = true, IsIdentity = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el modo activo de la nomina
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_a", ColumnDescription = "activo")]
        public bool Activo
        {
            get
            {
                return this.activoField;
            }
            set
            {
                this.activoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer es estado de la nomina
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_edo", ColumnDescription = "estado")]
        public EnumStatusNomina Estado
        {
            get
            {
                return this.estadoField;
            }
            set
            {
                this.estadoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string EstadoText
        {
            get
            {
                return Enum.GetName(typeof(EnumStatusNomina), this.Estado);
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion de la nomina
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_ttl", ColumnDescription = "descripcion", Length = 65)]
        public string Descripcion
        {
            get
            {
                return this.tituloField;
            }
            set
            {
                this.tituloField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de inicio de pago
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_fcini", ColumnDescription = "fecha de inicio de pago", IsNullable = true)]
        public DateTime FechaInicio
        {
            get
            {
                return this.fechaInicioField;
            }
            set
            {
                this.fechaInicioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de fin de pago
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_fcfin", ColumnDescription = "fecha de fin de pago", IsNullable = true)]
        public DateTime FechaFin
        {
            get
            {
                return this.fechaFinField;
            }
            set
            {
                this.fechaFinField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de pago de la nomina
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_fcpgo", ColumnDescription = "fecha de pago", IsNullable = true)]
        public DateTime? FechaPago
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaPagoField >= firstGoodDate)
                {
                    return this.fechaPagoField;
                }
                return null;
            }
            set
            {
                this.fechaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener dias del periodo
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_dsprd", ColumnDescription = "dias del periodo")]
        public int DiasPeriodo
        {
            get
            {
                TimeSpan tspan = this.FechaFin - this.FechaInicio;
                return tspan.Days + 1;
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de periodo de la nomina
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_tpprd", ColumnDescription = "tipo de periodo")]
        public EnumTipoPeriodo TipoPeriodo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string TipoPeriodoText
        {
            get
            {
                return Enum.GetName(typeof(EnumTipoPeriodo), this.TipoPeriodo);
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de nomina si es Ordinaria o Extraordinaria
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_tpnmn", ColumnDescription = "tipo de nomina")]
        public EnumTipoNomina TipoNomina
        {
            get
            {
                return this.tipoNominaField;
            }
            set
            {
                this.tipoNominaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string TipoNominaText
        {
            get
            {
                return Enum.GetName(typeof(EnumTipoNomina), this.TipoNomina);
            }
        }

        /// <summary>
        /// obtener o establecer el total de percepciones
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_tper", ColumnDescription = "total de percepciones", DecimalDigits = 4)]
        public decimal TotalPercepciones
        {
            get
            {
                return Math.Round(this.Empleados.Sum(it => it.TotalPercepciones), 4);
            }
        }

        /// <summary>
        /// obtener o establecer el total de deducciones
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_tded", ColumnDescription = "total de deducciones", IsNullable = true, DecimalDigits = 4)]
        public decimal TotalDeducciones
        {
            get
            {
                return this.Empleados.Sum(it => it.TotalDeducciones);
            }
        }

        /// <summary>
        /// obtener o establecer el total del fondo de ahorro
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_tfda", ColumnDescription = "total de fondo de ahorro", IsNullable = true, DecimalDigits = 4)]
        public decimal TotalFondoAhorro
        {
            get
            {
                return this.Empleados.Sum(it => it.FondoAhorro);
            }
        }

        /// <summary>
        /// obtener o establecer el total de vales de despensa
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_tvdd", ColumnDescription = "total de vales de despensa", IsNullable = true, DecimalDigits = 4)]
        public decimal TotalValesDespensa
        {
            get
            {
                return this.Empleados.Sum(it => it.ValesDespensa);
            }
        }

        /// <summary>
        /// obtener o establecer usuario que autoriza la nomina
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_usr_a", ColumnDescription = "usuario que autoriza", IsNullable = true)]
        public string Autoriza
        {
            get
            {
                return this.autorizaField;
            }
            set
            {
                this.autorizaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el usuario que crea el registro
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_usr_n", ColumnDescription = "usuario que creo el registro")]
        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultimo usuario que modifica el registro
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_usr_m", ColumnDescription = "usuario que modifico el registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica
        {
            get
            {
                return this.modificaField;
            }
            set
            {
                this.modificaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_nmnctrl_fa", ColumnDescription = "fecha de autorizacion", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FecAutoriza
        {
            get
            {
                return this.fechaAutorizaField;
            }
            set
            {
                this.fechaAutorizaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_fn", ColumnDataType = "DATETIME", ColumnDescription = "fecha de creacion", IsNullable = true)]
        public DateTime FecNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [SugarColumn(ColumnName = "_nmnctrl_fm", ColumnDescription = "fecha de modificacion", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FecModifca
        {
            get
            {
                return this.fechaModificaField;
            }
            set
            {
                this.fechaModificaField = value;
                this.OnPropertyChanged();
            }
        }

        #region campos ignorados

        /// <summary>
        /// obtener o establecer la tabla de ISR
        /// </summary>
        [SugarColumn(IsIgnore = true, ColumnName = "_nmnctrl_tblisr", ColumnDescription = "tabla isr", IsNullable = true, DecimalDigits = 4)]
        public ViewModelTablaImpuestoSobreRenta TablaISR
        {
            get
            {
                return this.tablaIsrField;
            }
            set
            {
                this.tablaIsrField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la tabla de subsidio al empleo
        /// </summary>
        [SugarColumn(IsIgnore = true, ColumnName = "_nmnctrl_tblsds", ColumnDescription = "tabla subsidio", IsNullable = true, DecimalDigits = 4)]
        public ViewModelTablaSubsidioAlEmpleo TablaSubsidio
        {
            get
            {
                return this.tablaSubsidioField;
            }
            set
            {
                this.tablaSubsidioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la tabla de premios de produccion
        /// </summary>
        [SugarColumn(IsIgnore = true, ColumnName = "_nmnctrl_tbprd", ColumnDescription = "tabla premio productividad", IsNullable = true, DecimalDigits = 4)]
        public BindingList<ViewModelTablaProductividad> TablaProductividad
        {
            get
            {
                return this.tablaProductividadField;
            }
            set
            {
                this.tablaProductividadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la lista de empleados de la nomina
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<ViewModelNominaPeriodo> Empleados
        {
            get
            {
                return this.empleadosField;
            }
            set
            {
                this.empleadosField = value;
                this.OnPropertyChanged();
            }
        }
 
        public string NombreLayout()
        {
            return this.TipoPeriodo.ToString() + DateTime.Now.ToString("yyyymmdd");
        }

        #endregion
    }
}