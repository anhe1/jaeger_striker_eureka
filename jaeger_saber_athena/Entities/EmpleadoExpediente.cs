﻿using System;
using System.Linq;
using Jaeger.Nomina.Enums;

namespace Jaeger.Nomina.Entities
{
    public class EmpleadoExpediente
    {

        public string EmpresaRFC { get; set; }

        public string Empresa { get; set; }

        public string RegistroPatronal { get; set; }

        public int Numero { get; set; }

        public string Nombre { get; set; }

        public string RFC { get; set; }

        public string CURP { get; set; }

        public string TipoRegimen { get; set; }

        public string RiesgoPuesto { get; set; }

        public string PeriodicidadPago { get; set; }

        public string MetodoPago { get; set; }

        public string TipoContrato { get; set; }

        public string TipoJornada { get; set; }

        public EnumTipoCuentaBancaria TipoCuenta
        {
            set
            {
                this.TipoCuentaText = Enum.GetName(typeof(EnumTipoCuentaBancaria), value);
            }
        }

        public string TipoCuentaText { get; set; }

        public string Banco { get; set; }

        public string CuentaBancaria { get; set; }

        public string CLABE { get; set; }

        public string CuentaTarjetaValesDespensa { get; set; }

        public string Sucursal { get; set; }

        public string Correo { get; set; }

        public string NumSeguridadSocial { get; set; }

        /// <summary>
        /// obtener o establacer la unidad medica familiar
        /// </summary>
        public string UMF { get; set; }

        public string NumFonacot { get; set; }

        public string Afore { get; set; }

        public string Telefono { get; set; }

        public string Departamento { get; set; }

        public string Puesto { get; set; }

        public EnumEstado EntidadFederativaNacimiento
        {
            set
            {
                this.EntidadFederativaNacimientoText = Enum.GetName(typeof(EnumEstado), value);
            }
        }

        public string EntidadFederativaNacimientoText { get; set; }

        public string CiudadNacimiento { get; set; }

        public string Nacionalidad { get; set; }

        DateTime? fechaNacimientoField;
        public DateTime? FechaNacimiento
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaNacimientoField >= firstGoodDate)
                {
                    return this.fechaNacimientoField;
                }
                return null;
            }
            set
            {
                fechaNacimientoField = value;
            }
        }

        public DateTime FechaInicioRelLaboral { get; set; }

        public EnumBaseCotizacion BaseCotizacion
        {
            set
            {
                this.BaseCotizacionText = Enum.GetName(typeof(EnumBaseCotizacion), value);
            }
        }

        public string BaseCotizacionText { get; set; }

        public decimal SalarioBase { get; set; }

        public decimal SalarioDiario { get; set; }

        public decimal SalarioDiarioIntegrado { get; set; }

        public EnumGenero Genero
        {
            set
            {
                this.GeneroText = Enum.GetName(typeof(EnumGenero), value);
            }
        }

        public string GeneroText { get; set; }

        public EnumEstadoCivil EstadoCivil
        {
            set
            {
                this.EstadoCivilText = Enum.GetName(typeof(EnumEstadoCivil), value);
            }
        }

        public string EstadoCivilText { get; set; }

        public int JornadasTrabajo { get; set; }

        public decimal HorasJornadaTrabajo { get; set; }

        public decimal PremiosPuntualidad { get; set; }

        public decimal AyudaBecas { get; set; }

        public decimal PremiosAsistencia { get; set; }

        public decimal AyudaArtEscolares{ get; set; }

        public decimal AyudaTransporte { get; set; }

        public decimal Productividad { get; set; }

        public decimal CreditoVivienda { get; set; }

        public byte[] Avatar { get; set; }

        public string Creo { get; set; }
    }
}