﻿using System;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    [SugarTable("_ctldpt", "nomina: catalogo de departamentos")]
    public class ViewModelDepartamento : BasePropertyChangeImplementation
    {
        private int indiceField;
        private bool activoField = true;
        private int indiceAreaField;
        private int secuencia;
        private string claveField;
        private string creoField;
        private string modificoField;
        private string departamentoField;
        private string responsableField;
        private string descripcionField;
        private DateTime fechaNuevoField;
        private DateTime? fechaModificoField;
        private BindingList<ViewModelDeptoPuesto> puestosField;

        /// <summary>
        /// constructor
        /// </summary>
        public ViewModelDepartamento()
        {
            this.FechaNuevo = DateTime.Now;
            this.Puestos = new BindingList<ViewModelDeptoPuesto>() { RaiseListChangedEvents = true };
        }

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_id", ColumnDescription = "indice", IsIdentity = true, IsPrimaryKey = true, Length = 11)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_a", ColumnDescription = "registro activo", Length = 1)]
        public bool Activo
        {
            get
            {
                return this.activoField;
            }
            set
            {
                this.activoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la secuencia de ordenamiento
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_sec", ColumnDescription = "indice del area al que pertenece el departamento", Length = 11, IsNullable = true)]
        public int Secuencia
        {
            get
            {
                return this.secuencia;
            }
            set
            {
                this.secuencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la clasifiacion
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_sbid", ColumnDescription = "indice del area al que pertenece el departamento", Length = 11, IsNullable = true)]
        public int IdClase
        {
            get
            {
                return this.indiceAreaField;
            }
            set
            {
                this.indiceAreaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_fn", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_fm", ColumnDescription = "fecha de la ultima modifiacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifico
        {
            get
            {
                return this.fechaModificoField;
            }
            set
            {
                this.fechaModificoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del departamento
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_clv", ColumnDescription = "clave de departamento", IsNullable = true, Length = 10)]
        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value.ToUpper();
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_usr_n", ColumnDescription = "clave del usuario que crea el registro", Length = 10)]
        public string Creo
        {
            get
            {
                return this.creoField;
            }
            set
            {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifico
        {
            get
            {
                return this.modificoField;
            }
            set
            {
                this.modificoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del departamento
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_depto", ColumnDescription = "nombre del departamento", Length = 128, IsNullable = false)]
        public string Nombre
        {
            get
            {
                return this.departamentoField;
            }
            set
            {
                this.departamentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre de la persona responsable del departamento
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_nom", ColumnDescription = "responsable del departamento", Length = 128, IsNullable = false)]
        public string Responsable
        {
            get
            {
                return this.responsableField;
            }
            set
            {
                this.responsableField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer una breve descripcion del departamento
        /// </summary>
        [SugarColumn(ColumnName = "_ctldpt_desc", ColumnDescription = "descripcion del departamento", Length = 128, IsNullable = true)]
        public string Descripcion
        {
            get
            {
                return this.descripcionField;
            }
            set
            {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ViewModelDeptoPuesto> Puestos
        {
            get
            {
                return this.puestosField;
            }
            set
            {
                if (this.puestosField != null)
                    this.Puestos.AddingNew -= new AddingNewEventHandler(this.Puestos_AddingNew);
                this.puestosField = value;
                if (this.puestosField != null)
                    this.Puestos.AddingNew += new AddingNewEventHandler(this.Puestos_AddingNew);
                this.OnPropertyChanged();
            }
        }

        private void Puestos_AddingNew(object sender, AddingNewEventArgs e)
        {
            e.NewObject = new ViewModelDeptoPuesto { IdDepto = this.Id, FechaNuevo = DateTime.Now };
        }
    }
}
