﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using SqlSugar;
using RfcFacil;
using Jaeger.Nomina.Enums;
using Jaeger.Nomina.Interface;

namespace Jaeger.Nomina.Entities
{
    [SugarTable("_ctlemp", "nomina: catalogo de empleados")]
    public class ViewModelEmpleado : Jaeger.Edita.V2.Nomina.Entities.Empleado, IPersona, IEmpleado, IDataErrorInfo
    {
        private string sitioField;
        private EnumFormaPago metodoPagoField;
        private string cuentaClabeField;
        private string sucursalField;
        private EnumTipoCuentaBancaria tipoCuentaBancariaField;
        private string cuentaTarjetaValesDespensaField;
        private string unidadMedicaFamiliarField;
        private string numFonacotField;
        private DateTime? fechaNacimientoField;
        private int edadField;
        private EnumBaseCotizacion baseCotizacionField;
        private int jornadasTrabajoField;
        private decimal horasJornadaTrabajoField;
        private byte[] avatarField;
        private int indexDepto;
        private int indexPuesto;
        private decimal creditoViviendaField;
        private decimal premiosPuntualidadField;
        private decimal becasTrabajadoresHijosField;
        private decimal premiosAsistenciaField;
        private decimal ayudaArtEscolaresField;
        private decimal ayudaTransporteField;
        private int productividadIdField;
        private EnumEstadoCivil estadoCivilField;
        private EnumGenero genero;
        private string registroPatronalField;
        private string aforeField;
        private EnumEstado entidadFederativaNacimientoField;
        private string ciudadNacimientofield;
        private string nacionalidadField;
        private BindingList<Documento> documentosField;

        public ViewModelEmpleado()
        {
            this.FechaInicioRelLaboral = DateTime.Now;
            this.FechaNuevo = DateTime.Now;
        }

        [SugarColumn(ColumnName = "_ctlemp_id", IsPrimaryKey = true, IsIdentity = true)]
        public int Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_a", ColumnDescription = "registro activo")]
        public new bool IsActive
        {
            get
            {
                return base.IsActive;
            }
            set
            {
                base.IsActive = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_drctr_id", IsNullable = true, ColumnDescription = "indice con el directorio principal")]
        public int IdDirectorio
        {
            get
            {
                return base.IdDirectorio;
            }
            set
            {
                base.IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_deptoid", IsNullable = true, ColumnDescription = "indice del departamento")]
        public int IdDepto
        {
            get
            {
                return this.indexDepto;
            }
            set
            {
                this.indexDepto = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_pstid", IsNullable = true, ColumnDescription = "indice del puesto")]
        public int IdPuesto
        {
            get
            {
                return this.indexPuesto;
            }
            set
            {
                this.indexPuesto = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_tpreg", Length = 2, IsNullable = true, ColumnDescription = "clave de tipo de regimen")]
        public new string TipoRegimen
        {
            get
            {
                return base.TipoRegimen;
            }
            set
            {
                base.TipoRegimen = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_rsgpst", Length = 3, IsNullable = true, ColumnDescription = "clave riesgo de puesto")]
        public new string RiesgoPuesto
        {
            get
            {
                return base.RiesgoPuesto;
            }
            set
            {
                base.RiesgoPuesto = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_prddpg", Length = 3, IsNullable = true, ColumnDescription = "clave periodicidad de pago")]
        public new string PeriodicidadPago
        {
            get
            {
                return base.PeriodicidadPago;
            }
            set
            {
                base.PeriodicidadPago = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_mtdpg", Length = 3, IsNullable = true, ColumnDescription = "clave metodo de pago")]
        public EnumFormaPago MetodoPago
        {
            get
            {
                return this.metodoPagoField;
            }
            set
            {
                this.metodoPagoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_tpcntr", Length = 3, IsNullable = true, ColumnDescription = "clave tipo de contrato")]
        public new string TipoContrato
        {
            get
            {
                return base.TipoContrato;
            }
            set
            {
                base.TipoContrato = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_tpjrn", Length = 3, IsNullable = true, ColumnDescription = "clave de tipo de jornada")]
        public new string TipoJornada
        {
            get
            {
                return base.TipoJornada;
            }
            set
            {
                base.TipoJornada = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_clvbnc", Length = 3, IsNullable = true, ColumnDescription = "clave banco")]
        public new string ClaveBanco
        {
            get
            {
                return base.ClaveBanco;
            }
            set
            {
                base.ClaveBanco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave interna del empleado
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_clv", Length = 10, ColumnDescription = "clave de control de empleado")]
        public new string Clave
        {
            get
            {
                return base.Clave;
            }
            set
            {
                base.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer Registro Federal de Contribuyentes
        /// </summary
        [SugarColumn(ColumnName = "_ctlemp_rfc", Length = 14, IsNullable = true, ColumnDescription = "registro federal de causantes")]
        public new string RFC
        {
            get
            {
                return base.RFC;
            }
            set
            {
                base.RFC = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_nss", Length = 16, IsNullable = true, ColumnDescription = "NumSeguridadSocial: opcional para la expresion del numero de seguridad social aplicable al trabajador")]
        public new string NumSeguridadSocial
        {
            get
            {
                return base.NumSeguridadSocial;
            }
            set
            {
                base.NumSeguridadSocial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer la unidad medica familiar
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_umf", Length = 16, IsNullable = true, ColumnDescription = "unidad medica familiar")]
        public string UMF
        {
            get
            {
                return this.unidadMedicaFamiliarField;
            }
            set
            {
                this.unidadMedicaFamiliarField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_fonac", Length = 16, IsNullable = true, ColumnDescription = "cuenta fonacot")]
        public string NumFonacot
        {
            get
            {
                return this.numFonacotField;
            }
            set
            {
                this.numFonacotField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer el numero de la cuenta de banco o numero de tarjeta
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_ctaban", Length = 16, IsNullable = true, ColumnDescription = "numero de cuenta de banco")]
        public new string CuentaBancaria
        {
            get
            {
                return base.CuentaBancaria;
            }
            set
            {
                base.CuentaBancaria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer numero de la cuenta interbancaria
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_clabe", Length = 18, IsNullable = true, ColumnDescription = "numero de cuenta interbancaria")]
        public string CLABE
        {
            get
            {
                return this.cuentaClabeField;
            }
            set
            {
                this.cuentaClabeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer cuenta o numero de tarjeta de vales de despensa
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_ctaval", Length = 18, IsNullable = true, ColumnDescription = "numero de cuenta de tarjeta de vales de despensa")]
        public string CuentaTarjetaValesDespensa
        {
            get
            {
                return this.cuentaTarjetaValesDespensaField;
            }
            set
            {
                this.cuentaTarjetaValesDespensaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de la sucursal bancaria
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_scrsl", Length = 16, IsNullable = true, ColumnDescription = "numero de sucursal bancaria")]
        public string Sucursal
        {
            get
            {
                return this.sucursalField;
            }
            set
            {
                this.sucursalField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_tpcta", Length = 16, IsNullable = true, ColumnDescription = "tipo de cuenta bancaria")]
        public EnumTipoCuentaBancaria TipoCuenta
        {
            get
            {
                return this.tipoCuentaBancariaField;
            }
            set
            {
                this.tipoCuentaBancariaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_numem", Length = 20, ColumnDescription = "numero de empleado")]
        public new int Num
        {
            get
            {
                return base.Num;
            }
            set
            {
                base.Num = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Clave Unica de Registro de Población
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_curp", Length = 20, IsNullable = true, ColumnDescription = "clave unica de registro")]
        public new string CURP
        {
            get
            {
                return base.CURP;
            }
            set
            {
                base.CURP = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_rgp", Length = 20, IsNullable = true, ColumnDescription = "registro patronal")]
        public string RegistroPatronal
        {
            get
            {
                return this.registroPatronalField;
            }
            set
            {
                this.registroPatronalField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_afore", Length = 20, IsNullable = true, ColumnDescription = "AFORE")]
        public string Afore
        {
            get
            {
                return this.aforeField;
            }
            set
            {
                this.aforeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer telefono
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_tlfn", Length = 20, IsNullable = true, ColumnDescription = "numero de telefono")]
        public new string Telefono
        {
            get
            {
                return base.Telefono;
            }
            set
            {
                base.Telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer telefono
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_tlfn2", Length = 20, IsNullable = true, ColumnDescription = "numero de telefono 2, opcional")]
        public new string Telefono2
        {
            get
            {
                return base.Telefono2;
            }
            set
            {
                base.Telefono2 = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_depto", Length = 32, IsNullable = true, ColumnDescription = "departamento")]
        public new string Departamento
        {
            get
            {
                return base.Departamento;
            }
            set
            {
                base.Departamento = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_puesto", Length = 32, IsNullable = true, ColumnDescription = "puesto")]
        public new string Puesto
        {
            get
            {
                return base.Puesto;
            }
            set
            {
                base.Puesto = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_entfed", IsNullable = true, ColumnDescription = "clave de entidad federativa de nacimiento")]
        public EnumEstado EntidadFederativaNacimiento
        {
            get
            {
                return this.entidadFederativaNacimientoField;
            }
            set
            {
                this.entidadFederativaNacimientoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_lgnac", Length = 32, IsNullable = true, ColumnDescription = "lugar de nacimiento")]
        public string CiudadNacimiento
        {
            get
            {
                return this.ciudadNacimientofield;
            }
            set
            {
                this.ciudadNacimientofield = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_nac", Length = 32, IsNullable = true, ColumnDescription = "nacionalidad")]
        public string Nacionalidad
        {
            get
            {
                return this.nacionalidadField;
            }
            set
            {
                this.nacionalidadField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_nom", Length = 80, ColumnDescription = "nombre(s) del empleado")]
        public new string Nombre
        {
            get
            {
                return base.Nombre;
            }
            set
            {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_pape", Length = 80, ColumnDescription = "primer apellido")]
        public new string PrimerApellido
        {
            get
            {
                return base.PrimerApellido;
            }
            set
            {
                base.PrimerApellido = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_sape", Length = 80, ColumnDescription = "segundo apellido")]
        public new string SegundoApellido
        {
            get
            {
                return base.SegundoApellido;
            }
            set
            {
                base.SegundoApellido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer correo electronico
        /// </summary>
        [SugarColumn(ColumnName = "_ctlemp_mail", Length = 80, IsNullable = true, ColumnDescription = "correo electronico")]
        public new string Correo
        {
            get
            {
                return base.Correo;
            }
            set
            {
                base.Correo = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_sitio", Length = 80, IsNullable = true, ColumnDescription = "sitio web personal")]
        public string Sitio
        {
            get
            {
                return this.sitioField;
            }
            set
            {
                this.sitioField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_fnac", IsNullable = true, ColumnDescription = "fecha de nacimiento")]
        public DateTime? FechaNacimiento
        {
            get
            {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaNacimientoField >= firstGoodDate)
                    return this.fechaNacimientoField;
                return null;
            }
            set
            {
                fechaNacimientoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_edad", IsNullable = true, ColumnDescription = "edad")]
        public int Edad
        {
            get
            {
                return this.edadField;
            }
            set
            {
                this.edadField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_fchrel", ColumnDescription = "fecha de inicio de relacion laboral")]
        public new DateTime? FechaInicioRelLaboral
        {
            get
            {
                return base.FechaInicioRelLaboral;
            }
            set
            {
                base.FechaInicioRelLaboral = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_bscot", IsNullable = true, ColumnDescription = "base de cotizacion")]
        public EnumBaseCotizacion BaseCotizacion
        {
            get
            {
                return this.baseCotizacionField;
            }
            set
            {
                this.baseCotizacionField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_sb", DecimalDigits = 4, IsNullable = true, ColumnDescription = "salario base")]
        public new decimal SalarioBase
        {
            get
            {
                return base.SalarioBase;
            }
            set
            {
                base.SalarioBase = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_drintg", DecimalDigits = 4, IsNullable = true, ColumnDescription = "salario diario integrado")]
        public new decimal SalarioDiario
        {
            get
            {
                return base.SalarioDiario;
            }
            set
            {
                base.SalarioDiario = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_sd", DecimalDigits = 4, IsNullable = true, ColumnDescription = "salario base")]
        public new decimal SalarioDiarioIntegrado
        {
            get
            {
                return base.SalarioDiarioIntegrado;
            }
            set
            {
                base.SalarioDiarioIntegrado = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_sexo", IsNullable = true, ColumnDescription = "genero")]
        public EnumGenero Genero
        {
            get
            {
                return this.genero;
            }
            set
            {
                this.genero = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_ecvl", IsNullable = true, ColumnDescription = "estado civil")]
        public EnumEstadoCivil EstadoCivil
        {
            get
            {
                return this.estadoCivilField;
            }
            set
            {
                this.estadoCivilField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_jrntr", IsNullable = true, ColumnDescription = "numero de jornadas de trabajo")]
        public int JornadasTrabajo
        {
            get
            {
                return this.jornadasTrabajoField;
            }
            set
            {
                this.jornadasTrabajoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_jrnhrs", IsNullable = true, ColumnDescription = "numero de horas de la jornadas de trabajo")]
        public decimal HorasJornadaTrabajo
        {
            get
            {
                return this.horasJornadaTrabajoField;
            }
            set
            {
                this.horasJornadaTrabajoField = value;
                this.OnPropertyChanged();
            }
        }

        #region percepciones adicionales

        [SugarColumn(ColumnName = "_ctlemp_ppp", IsNullable = true, ColumnDescription = "percepcion: premio de puntualidad")]
        public decimal PremiosPuntualidad
        {
            get
            {
                return this.premiosPuntualidadField;
            }
            set
            {
                this.premiosPuntualidadField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_pbc", IsNullable = true, ColumnDescription = "percepcion: becas")]
        public decimal Becas
        {
            get
            {
                return this.becasTrabajadoresHijosField;
            }
            set
            {
                this.becasTrabajadoresHijosField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_ppa", IsNullable = true, ColumnDescription = "percepcion: premios de asistencia")]
        public decimal PremiosAsistencia
        {
            get
            {
                return this.premiosAsistenciaField;
            }
            set
            {
                this.premiosAsistenciaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_pae", IsNullable = true, ColumnDescription = "percepcion: ayuda de articulos escolares")]
        public decimal AyudaArtEscolares
        {
            get
            {
                return this.ayudaArtEscolaresField;
            }
            set
            {
                this.ayudaArtEscolaresField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_pat", IsNullable = true, ColumnDescription = "percepcion: ayuda transporte")]
        public decimal AyudaTransporte
        {
            get
            {
                return this.ayudaTransporteField;
            }
            set
            {
                this.ayudaTransporteField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_ppd", IsNullable = true, ColumnDescription = "percepcion: premios de productividad")]
        public int ProductividadId
        {
            get
            {
                return this.productividadIdField;
            }
            set
            {
                this.productividadIdField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_dinf", IsNullable = true, ColumnDescription = "deduccion: descuento por credito de vivienda (infonavit)")]
        public decimal CreditoVivienda
        {
            get
            {
                return this.creditoViviendaField;
            }
            set
            {
                this.creditoViviendaField = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        [SugarColumn(ColumnName = "_ctlemp_img", IsNullable = true)]
        public byte[] Avatar
        {
            get
            {
                return this.avatarField;
            }
            set
            {
                this.avatarField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_fn", ColumnDescription = "fecha de registro")]
        public new DateTime FechaNuevo
        {
            get
            {
                return base.FechaNuevo;
            }
            set
            {
                base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_fm", IsNullable = true, ColumnDescription = "ultima fecha de modificaciones", IsOnlyIgnoreInsert = true)]
        public new DateTime? FechaModifica
        {
            get
            {
                return base.FechaModifica;
            }
            set
            {
                base.FechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_usr_n", IsNullable = true, ColumnDescription = "usuario creo registro", Length = 10)]
        public new string Creo
        {
            get
            {
                return base.Creo;
            }
            set
            {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ctlemp_usr_m", IsNullable = true, ColumnDescription = "ultima usuario que modifico", Length = 10, IsOnlyIgnoreInsert = true)]
        public new string Modifica
        {
            get
            {
                return base.Modifica;
            }
            set
            {
                base.Modifica = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<Documento> Documentos
        {
            get
            {
                return this.documentosField;
            }
            set
            {
                this.documentosField = value;
                this.OnPropertyChanged();
            }
        }

        public string CalcularRFC()
        {
            try
            {
                var rfc = RfcBuilder.ForNaturalPerson()
                                    .WithName(this.Nombre)
                                    .WithFirstLastName(this.PrimerApellido)
                                    .WithSecondLastName(this.SegundoApellido)
                                    .WithDate(this.FechaNacimiento.Value.Year, this.FechaNacimiento.Value.Month, this.FechaNacimiento.Value.Day)
                                    .Build();
                this.RFC = rfc.ToString();
                return this.RFC;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "";
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string this[string columnName]
        {
            get
            {
                if (columnName == "CURP" && !this.RegexValido(this.CURP, patron: "^[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]"))
                {
                    return "Formato CURP incorrecto!";
                }
                if (columnName == "RFC" && !this.RegexValido(this.RFC, patron: "^[a-zA-Z&ñÑ]{3,4}(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\\d])|([0-9]{2})([0][2])([0][1-9]|[1][\\d]|[2][0-8]))(\\w{2}[A|a|0-9]{1})$"))
                {
                    return "Formato RFC no valido!";
                }
                return string.Empty;
            }
        }

        [SugarColumn(IsIgnore = true)]
        [Browsable(false)]
        public string Error
        {
            get
            {
                if (!this.RegexValido(this.CURP, patron: "^[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]") | !this.RegexValido(this.RFC, "^[a-zA-Z&ñÑ]{3,4}(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\\d])|([0-9]{2})([0][2])([0][1-9]|[1][\\d]|[2][0-8]))(\\w{2}[A|a|0-9]{1})$"))
                {
                    return "Por favor ingrese datos válidos en esta fila!";
                }
                return string.Empty;
            }
        }

        private bool RegexValido(string valor, string patron)
        {
            if (!(valor == null))
            {
                bool flag = Regex.IsMatch(valor, string.Concat(str0: "^", str1: patron, str2: "$"));
                return flag;
            }
            return false;
        }
    }
}
