﻿using System.ComponentModel;

namespace Jaeger.Nomina.Entities
{
    public class ConfiguracionCuotasObrero : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private decimal excedenteField;
        private decimal prestacionesDineroField;
        private decimal prestacionesEspecieField;
        private decimal invalidezVidaField;
        private decimal cesantiaVejezField;

        public ConfiguracionCuotasObrero()
        {
            this.excedenteField = new decimal(.004);
            this.prestacionesDineroField = new decimal(0.0025);
            this.prestacionesEspecieField = new decimal(0.0038);
            this.invalidezVidaField = new decimal(0.0063);
            this.cesantiaVejezField = new decimal(0.0113);
        }

        #region cuotas obrero - patronales

        [DisplayName("Invalidez y Vida"), Category("IMSS - Cuotas Obrero")]
        public decimal InvalidezVida
        {
            get
            {
                return this.invalidezVidaField;
            }
            set
            {
                this.invalidezVidaField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Cesantia y Vejez"), Category("IMSS - Cuotas Obrero")]
        public decimal CesantiaVejez
        {
            get
            {
                return this.cesantiaVejezField;
            }
            set
            {
                this.cesantiaVejezField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Prestaciones en Dinero"), Category("IMSS - Cuotas Obrero")]
        public decimal PrestacionesDinero
        {
            get
            {
                return this.prestacionesDineroField;
            }
            set
            {
                this.prestacionesDineroField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Prestaciones en Especie"), Category("IMSS - Cuotas Obrero")]
        public decimal PrestacionesEspecie
        {
            get
            {
                return this.prestacionesEspecieField;
            }
            set
            {
                this.prestacionesEspecieField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Excendete"), Category("IMSS - Cuotas Obrero")]
        public decimal Excedente
        {
            get
            {
                return this.excedenteField;
            }
            set
            {
                this.excedenteField = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}