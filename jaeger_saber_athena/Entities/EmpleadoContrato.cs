﻿using System;
using System.Linq;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    public class EmpleadoContrato : BasePropertyChangeImplementation
    {
        private bool activoField;
        private DateTime fechaAltaField;
        private DateTime? fechaBajaField;
        private decimal salarioBaseField;
        private decimal salarioDiarioField;
        private decimal salarioDiarioIntegreadoField;

        public EmpleadoContrato()
        {

        }

        public bool Activo
        {
            get
            {
                return this.activoField;
            }
            set
            {
                this.activoField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal SalarioBase
        {
            get
            {
                return this.salarioBaseField;
            }
            set
            {
                this.salarioBaseField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal SalarioDiario
        {
            get
            {
                return this.salarioDiarioField;
            }
            set
            {
                this.salarioDiarioField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal SalarioDiarioIntegrado
        {
            get
            {
                return this.salarioDiarioIntegreadoField;
            }
            set
            {
                this.salarioDiarioIntegreadoField = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime FechaInicioRelLaboral
        {
            get
            {
                return this.fechaAltaField;
            }
            set
            {
                this.fechaAltaField = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime? FechaFinRelLaboral
        {
            get
            {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaBajaField >= firstGoodDate)
                {
                    return this.fechaBajaField;
                }
                return null;
            }
            set
            {
                this.fechaBajaField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
