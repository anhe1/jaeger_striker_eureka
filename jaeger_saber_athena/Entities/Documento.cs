﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    [JsonObject("doc")]
    public class Documento : BasePropertyChangeImplementation
    {
        private bool activoField;
        private string descripcionField;
        private string tipoField;
        private string urlField;

        /// <summary>
        /// constructor
        /// </summary>
        public Documento()
        {

        }

        [JsonProperty("acti")]
        public bool Activo
        {
            get
            {
                return this.activoField;
            }
            set
            {
                this.activoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("desc")]
        public string Descripcion
        {
            get
            {
                return this.descripcionField;
            }
            set
            {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("tipo")]
        public string Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("url")]
        public string Url
        {
            get
            {
                return this.urlField;
            }
            set
            {
                this.urlField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
