﻿/// develop: anhe 090120192309
/// purpose: contener la tabla de la Unidad de Medida Actualizada
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    [SugarTable("_tbluma", "nomina: tabla de uma")]
    public class ViewModelTablaUMA : BasePropertyChangeImplementation
    {
        private int indiceField;
        private DateTime fechaInicioField;
        private decimal valorUMAField;
        public ViewModelTablaUMA()
        {
            this.fechaInicioField = DateTime.Now;
        }

        [SugarColumn(ColumnName = "_tbluma_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_tbluma_fecini", ColumnDescription = "fecha de inicio de la aplicacion")]
        public DateTime FechaInicio
        {
            get
            {
                return this.fechaInicioField;
            }
            set
            {
                this.fechaInicioField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_tbluma_vuma", ColumnDescription = "valor UMA")]
        public decimal ValorUMA
        {
            get
            {
                return this.valorUMAField;
            }
            set
            {
                this.valorUMAField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
