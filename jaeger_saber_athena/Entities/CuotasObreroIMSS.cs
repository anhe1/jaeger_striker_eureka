﻿using System;
using Newtonsoft.Json;

namespace Jaeger.Nomina.Entities
{
    public class CuotasObreroIMSS : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private readonly Configuracion configuracion;
        private decimal salarioDiarioField;
        private decimal salarioDiarioIntegradoField;
        private decimal excedenteField;
        private decimal prestacionesDineroField;
        private decimal prestacionesEspecieField;
        private decimal invalidezVidaField;
        private decimal cesantiaVejezField;
        private decimal totalField;

        public CuotasObreroIMSS()
        {

        }

        public CuotasObreroIMSS(ref Configuracion conf)
        {
            this.configuracion = conf;
        }

        public CuotasObreroIMSS(decimal sd, decimal sdi, ref Configuracion conf)
        {
            this.salarioDiarioField = sd;
            this.salarioDiarioIntegradoField = sdi;
            this.configuracion = conf;
        }

        [JsonProperty("sd")]
        public decimal SalarioDiario
        {
            get
            {
                return this.salarioDiarioField;
            }
            set
            {
                this.salarioDiarioField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("sdi")]
        public decimal SalarioDiarioIntegrado
        {
            get
            {
                return this.salarioDiarioIntegradoField;
            }
            set
            {
                this.salarioDiarioIntegradoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("exc")]
        public decimal Excedente
        {
            get
            {
                return this.excedenteField;
            }
            set
            {
                this.excedenteField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("prsdnr")]
        public decimal PrestacionesDinero
        {
            get
            {
                return this.prestacionesDineroField;
            }
            set
            {
                this.prestacionesDineroField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("prsesp")]
        public decimal PrestacionesEspecie
        {
            get
            {
                return this.prestacionesEspecieField;
            }
            set
            {
                this.prestacionesEspecieField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("invid")]
        public decimal InvalidezVida
        {
            get
            {
                return this.invalidezVidaField;
            }
            set
            {
                this.invalidezVidaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("ceve")]
        public decimal CesantiaVejez
        {
            get
            {
                return this.cesantiaVejezField;
            }
            set
            {
                this.cesantiaVejezField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("ttl")]
        public decimal Total
        {
            get
            {
                return this.totalField;
            }
            set
            {
                this.totalField = value;
                this.OnPropertyChanged();
            }
        }

        public string Json(Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static CuotasObreroIMSS Json(string stringInput)
        {
            try
            {
                return JsonConvert.DeserializeObject<CuotasObreroIMSS>(stringInput);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public decimal CuotaIMSS(int dias, ref ViewModelNominaPeriodo e, ViewModelNominaControl p)
        {
            decimal uma3 = new decimal(80.6) * 3;
            if (this.configuracion.UsarParaCalcular == Configuracion.EnumUsarParaCalcular.UMA)
            {
                uma3 = this.configuracion.UMA * 3;
            }
            else if (this.configuracion.UsarParaCalcular == Configuracion.EnumUsarParaCalcular.SalarioMinimoGeneral)
            {
                uma3 = this.configuracion.SalarioMinimoGeneral * 3;
            }

            decimal diferencia = this.SalarioDiarioIntegrado - uma3;
            this.excedenteField = diferencia * dias * (this.configuracion.CuotasObrero.Excedente);
            this.prestacionesDineroField = this.salarioDiarioIntegradoField * dias * (this.configuracion.CuotasObrero.PrestacionesDinero);
            this.prestacionesEspecieField = (this.configuracion.CuotasObrero.PrestacionesEspecie) * dias * this.SalarioDiarioIntegrado;
            this.invalidezVidaField = (this.configuracion.CuotasObrero.InvalidezVida) * dias * this.SalarioDiarioIntegrado;
            this.cesantiaVejezField = (this.configuracion.CuotasObrero.CesantiaVejez) * dias * this.SalarioDiarioIntegrado;

            #region calcular excedente segun el libro

            // calcular el excedente
            // IF(AND('Nómina'!Status = "A", 'Nómina'!SDI > 'Nómina'!TRESSM, 'Nómina'!INC + 'Nómina'!AUS > 0), 
            if (e.SalarioDiarioIntegrado > uma3 && (e.Incapacidad + e.Ausencias) > 0)
            {
                //('Nómina'!SDI - 'Nómina'!TRESSM) * (('Nómina'!JT - ('Nómina'!INC + 'Nómina'!AUS)/ 'Nómina'!JT * 'Nómina'!JT)*'Nómina'!EXC_E),
                this.excedenteField = (e.SalarioDiarioIntegrado - uma3) * ((e.JornadasTrabajo - (e.Incapacidad + e.Ausencias) / (decimal)e.JornadasTrabajo * e.JornadasTrabajo) * this.configuracion.CuotasObrero.Excedente);

            }
            else if (e.SalarioDiarioIntegrado > uma3 && (e.Incapacidad + e.Ausencias) == 0) // IF(AND('Nómina'!Status = "A", 'Nómina'!SDI > 'Nómina'!TRESSM, 'Nómina'!INC + 'Nómina'!AUS = 0), 
            {
                //('Nómina'!SDI - 'Nómina'!TRESSM) * 'Nómina'!DP * ('Nómina'!EXC_E),""))
                this.excedenteField = (e.SalarioDiarioIntegrado - uma3) * p.DiasPeriodo * (this.configuracion.CuotasObrero.Excedente);
            }
            else
            {
                this.excedenteField = 0;
            }

            #endregion

            #region calcular prestaciones en dinero
            //IF(OR('Nómina'!Status = "Z", 'Nómina'!Status = ""), "", 
            //IF(OR('Nómina'!INC > 0, 'Nómina'!AUS > 0), 
            if ((e.Incapacidad + e.Ausencias) > 0)
            {
                //'Nómina'!SDI * ('Nómina'!JT - (('Nómina'!INC + 'Nómina'!AUS) / 'Nómina'!JT) * 'Nómina'!JT)*'Nómina'!PD_E,
                this.prestacionesDineroField = e.SalarioDiarioIntegrado * new decimal(e.JornadasTrabajo - ((e.Incapacidad + e.Ausencias) / e.JornadasTrabajo) * e.JornadasTrabajo) * (this.configuracion.CuotasObrero.PrestacionesDinero);
            }
            else
            {
                //('Nómina'!SDI * 'Nómina'!DP * 'Nómina'!PD_E)))
                this.prestacionesDineroField = (e.SalarioDiarioIntegrado * p.DiasPeriodo * (this.configuracion.CuotasObrero.PrestacionesDinero));
            }
            #endregion

            #region calcular prestaciones en especie (gastos medicos pensionados)
            //IF(OR('Nómina'!Status = "Z", 'Nómina'!Status = ""), "", 
            //IF(OR('Nómina'!INC > 0, 'Nómina'!AUS > 0), 
            if ((e.Incapacidad + e.Ausencias) > 0)
            {
                //'Nómina'!SDI * ('Nómina'!JT - (('Nómina'!INC + 'Nómina'!AUS) / 'Nómina'!JT) * 'Nómina'!JT)*'Nómina'!GMP_E,
                this.prestacionesEspecieField = e.SalarioDiarioIntegrado * (e.JornadasTrabajo - (new decimal(e.Incapacidad + e.Ausencias) / e.JornadasTrabajo) * e.JornadasTrabajo) * (this.configuracion.CuotasObrero.PrestacionesEspecie);
            }
            else
            {
                //('Nómina'!SDI * 'Nómina'!DP * 'Nómina'!GMP_E)))
                this.prestacionesEspecieField = (e.SalarioDiarioIntegrado * p.DiasPeriodo * (this.configuracion.CuotasObrero.PrestacionesEspecie));
            }

            #endregion

            #region calcular invalidez y vida
            //IF(OR('Nómina'!Status = "Z", 'Nómina'!Status = ""), "", 
            //IF(OR('Nómina'!INC > 0, 'Nómina'!AUS > 0), 
            if ((e.Incapacidad + e.Ausencias) > 0)
            {
                //'Nómina'!SDI * ('Nómina'!JT - (('Nómina'!INC + 'Nómina'!AUS) / 'Nómina'!JT) * 'Nómina'!JT)*'Nómina'!IV_E,
                this.invalidezVidaField = e.SalarioDiarioIntegrado * (e.JornadasTrabajo - (new decimal(e.Incapacidad + e.Ausencias) / (decimal)e.JornadasTrabajo) * e.JornadasTrabajo) * (this.configuracion.CuotasObrero.InvalidezVida);
            }
            else
            {
                //('Nómina'!SDI * 'Nómina'!DP * 'Nómina'!IV_E)))
                this.invalidezVidaField = (e.SalarioDiarioIntegrado * p.DiasPeriodo * (this.configuracion.CuotasObrero.InvalidezVida));
            }

            #endregion

            #region cesantia y vejez
            // IF(OR('Nómina'!Status = "Z", 'Nómina'!Status = ""), "", 
            //IF(OR('Nómina'!INC > 0, 'Nómina'!AUS > 0), 
            if ((e.Incapacidad + e.Ausencias) > 0)
            {
                //'Nómina'!SDI * ('Nómina'!JT - (('Nómina'!INC + 'Nómina'!AUS) / 'Nómina'!JT) * 'Nómina'!JT)*'Nómina'!CESA_E,
                this.cesantiaVejezField = e.SalarioDiarioIntegrado * (e.JornadasTrabajo - ((e.Incapacidad + e.Ausencias) / (decimal)e.JornadasTrabajo) * e.JornadasTrabajo) * (this.configuracion.CuotasObrero.CesantiaVejez);
            }
            else
            {
                //('Nómina'!SDI * 'Nómina'!DP * 'Nómina'!CESA_E)))
                this.cesantiaVejezField = (e.SalarioDiarioIntegrado * p.DiasPeriodo * (this.configuracion.CuotasObrero.CesantiaVejez));
            }

            #endregion

            this.Total = this.excedenteField + this.prestacionesEspecieField + this.prestacionesDineroField + this.invalidezVidaField + this.cesantiaVejezField;
            return this.Total;
        }
    }
}
