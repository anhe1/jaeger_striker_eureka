﻿using System.ComponentModel;

namespace Jaeger.Nomina.Entities
{
    public class ConfiguracionCuotasPatronal : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private decimal patronalRiesgoTrabajoField;
        private decimal patronalCuotaFijaField;
        private decimal patronalPrestacionesDineroField;
        private decimal patronalGastosMedicosPensionadosField;
        private decimal patronalInvalidezVejezField;
        private decimal patronalGuarderiaField;
        private decimal patronalCesantiaField;
        private decimal patronalRetiroField;
        private decimal excedenteField;

        public ConfiguracionCuotasPatronal()
        {
            this.patronalRiesgoTrabajoField = new decimal(value: .0260);
            this.patronalGastosMedicosPensionadosField = new decimal(value: .0105);
            this.patronalCuotaFijaField = new decimal(value: .2040);
            this.patronalPrestacionesDineroField = new decimal(value: .0070);
            this.patronalInvalidezVejezField = new decimal(value: .0175);
            this.patronalGuarderiaField = new decimal(value: .0100);
            this.patronalCesantiaField = new decimal(value: 0.0315);
            this.patronalRetiroField = new decimal(value: 0.0200);
        }

        #region IMSS cuotas patronales

        [DisplayName("Excedente"), Category("IMSS - Cuota Patron")]
        public decimal Excedente
        {
            get
            {
                return this.excedenteField;
            }
            set
            {
                this.excedenteField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Riesgo de Trabajo"), Category("IMSS - Cuota Patron")]
        public decimal RiesgoTrabajo
        {
            get
            {
                return this.patronalRiesgoTrabajoField;
            }
            set
            {
                this.patronalRiesgoTrabajoField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Cuota Fija"), Category("IMSS - Cuota Patron")]
        public decimal CuotaFija
        {
            get
            {
                return this.patronalCuotaFijaField;
            }
            set
            {
                this.patronalCuotaFijaField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Prestaciones en Dinero"), Category("IMSS - Cuota Patron")]
        public decimal PrestacionesDinero
        {
            get
            {
                return this.patronalPrestacionesDineroField;
            }
            set
            {
                this.patronalPrestacionesDineroField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Gastos Medicos y Pensionados"), Category("IMSS - Cuota Patron")]
        public decimal GastosMedicosPensionados
        {
            get
            {
                return this.patronalGastosMedicosPensionadosField;
            }
            set
            {
                this.patronalGastosMedicosPensionadosField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Invalidez y Vejez"), Category("IMSS - Cuota Patron")]
        public decimal InvalidezVejez
        {
            get
            {
                return this.patronalInvalidezVejezField;
            }
            set
            {
                this.patronalInvalidezVejezField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Guarderias"), Category("IMSS - Cuota Patron")]
        public decimal Guarderias
        {
            get
            {
                return this.patronalGuarderiaField;
            }
            set
            {
                this.patronalGuarderiaField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Censantía"), Category("IMSS - Cuota Patron")]
        public decimal Cesantia
        {
            get
            {
                return this.patronalCesantiaField;
            }
            set
            {
                this.patronalCesantiaField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Retiro"), Category("IMSS - Cuota Patron")]
        public decimal Retiro
        {
            get
            {
                return this.patronalRetiroField;
            }
            set
            {
                this.patronalRetiroField = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}