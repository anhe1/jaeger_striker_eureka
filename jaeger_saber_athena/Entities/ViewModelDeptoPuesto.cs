using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using System.ComponentModel;

namespace Jaeger.Nomina.Entities
{
    [SugarTable("_ctldptp", "nomina: catalogo de puestos de departamento")]
    public class ViewModelDeptoPuesto : BasePropertyChangeImplementation
    {
        private int index;
        private bool activo = true;
        private int idDepartmanto;
        private int secuencia;
        private string descripcion;
        private decimal salarioBase;
        private string creo;
        private string modifico;
        private DateTime fechaNuevoField;
        private DateTime? fechaModificoField;
        private BindingList<ViewModelDeptoFuncion> funciones;

        public ViewModelDeptoPuesto()
        {
            this.FechaNuevo = DateTime.Now;
            this.Funciones = new BindingList<ViewModelDeptoFuncion>() { RaiseListChangedEvents = true };
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptp_id", ColumnDescription = "indice", IsIdentity = true, IsPrimaryKey = true, Length = 11)]
        public int Id
        {
            get
            {
                return this.index;
            }
            set
            {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptp_a", ColumnDescription = "registro activo", Length = 1)]
        public bool Activo
        {
            get
            {
                return this.activo;
            }
            set
            {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la secuencia de ordenamiento
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptp_sec", ColumnDescription = "secuencia de ordenamiento", Length = 11, IsNullable = true)]
        public int Secuencia
        {
            get
            {
                return this.secuencia;
            }
            set
            {
                this.secuencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con el departamento
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptp_sbid", ColumnDescription = "indice del departamento al que pertenece el puesto o funcion", Length = 11, IsNullable = true)]
        public int IdDepto
        {
            get
            {
                return this.idDepartmanto;
            }
            set
            {
                this.idDepartmanto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptp_fn", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptp_fm", ColumnDescription = "fecha de la ultima modifiacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifico
        {
            get
            {
                return this.fechaModificoField;
            }
            set
            {
                this.fechaModificoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptp_usr_n", ColumnDescription = "clave del usuario que crea el registro", Length = 10)]
        public string Creo
        {
            get
            {
                return this.creo;
            }
            set
            {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptp_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifico
        {
            get
            {
                return this.modifico;
            }
            set
            {
                this.modifico = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del puesto
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptp_nom", ColumnDescription = "descripcion del puesto", Length = 128, IsNullable = false)]
        public string Descripcion
        {
            get
            {
                return this.descripcion;
            }
            set
            {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer salario base para el puesto
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptp_salba", ColumnDescription = "salario base", Length = 11, DecimalDigits = 4, IsNullable = false)]
        public decimal Salario
        {
            get
            {
                return this.salarioBase;
            }
            set
            {
                this.salarioBase = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ViewModelDeptoFuncion> Funciones
        {
            get
            {
                return this.funciones;
            }
            set
            {
                if (funciones != null)
                    this.Funciones.AddingNew -= new AddingNewEventHandler(this.Funciones_AddingNew);
                this.funciones = value;
                if (funciones != null)
                    this.Funciones.AddingNew += new AddingNewEventHandler(this.Funciones_AddingNew);
                this.OnPropertyChanged();
            }
        }

        private void Funciones_AddingNew(object sender, AddingNewEventArgs e)
        {
            e.NewObject = new ViewModelDeptoFuncion { IdPuesto = this.Id, FechaNuevo = DateTime.Now };
        }
    }
}