﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    [SugarTable("_tblslr", "nomina: tabla de salarios minimos")]
    public class ViewModelTablaSalarioMinimo : BasePropertyChangeImplementation
    {
        private int indiceField;
        private decimal salarioMinimoField;
        private DateTime vigenciaField;
        private DateTime fechaNuevoField;

        public ViewModelTablaSalarioMinimo()
        {
            this.vigenciaField = DateTime.Now;
            this.fechaNuevoField = DateTime.Now;
        }

        [SugarColumn(ColumnName = "_tblslr_id", ColumnDescription = "indice", IsPrimaryKey = true, IsIdentity = true, IsOnlyIgnoreInsert = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_tblslr_slr", ColumnDescription = "monto de salario minimo", Length = 6, DecimalDigits = 4)]
        public decimal SalarioMinimo
        {
            get
            {
                return this.salarioMinimoField;
            }
            set
            {
                this.salarioMinimoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_tblslr_vig", ColumnDescription = "vigencia")]
        public DateTime Vigencia
        {
            get
            {
                return this.vigenciaField;
            }
            set
            {
                this.vigenciaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_tblslr_fn", ColumnDescription = "fecha de nuevo registro")]
        public DateTime FecNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
