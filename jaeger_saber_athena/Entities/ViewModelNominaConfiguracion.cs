﻿using System;
using System.Linq;
using SqlSugar;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    [SugarTable("_conf", "configuracion de la empresa")]
    public class ViewModelNominaConfiguracion : BasePropertyChangeImplementation
    {
        private int indiceField;
        private string confField;
        private string key;

        public ViewModelNominaConfiguracion()
        {

        }

        [SugarColumn(ColumnDataType = "INT", ColumnDescription = "indice de la tabla", ColumnName = "_conf_id", IsIdentity = true, IsOnlyIgnoreInsert = true, IsPrimaryKey = true, Length = 11)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_conf_key", Length = 20)]
        public string Key
        {
            get
            {
                return key;
            }
            set
            {
                this.key = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnDataType = "TEXT", ColumnDescription = "datos de la empresa", ColumnName = "_conf_data", IsNullable = true)]
        public string Data
        {
            get
            {
                return this.confField;
            }
            set
            {
                this.confField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public Configuracion Configuracion
        {
            get
            {
                return JsonConvert.DeserializeObject<Configuracion>(this.confField);
            }
            set
            {
                this.confField = JsonConvert.SerializeObject(value, Formatting.None);
            }
        }
    }
}
