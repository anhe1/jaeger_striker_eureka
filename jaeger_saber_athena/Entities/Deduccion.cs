﻿using Newtonsoft.Json;
using Jaeger.Nomina.Interface;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    public class Deduccion : BasePropertyChangeImplementation, IDeduccion
    {
        private string indentificadorField;
        private string claveField;
        private string tipoField;
        private string etiquetaField;
        private decimal importeField;

        public Deduccion()
        {

        }

        public Deduccion(string pIdentificador, string pClave, string pTipo, string pConcepto, decimal pImporte = 0)
        {
            this.Identificador = pIdentificador;
            this.Clave = pClave;
            this.Tipo = pTipo;
            this.Concepto = pConcepto;
            this.Importe = pImporte;
        }

        [JsonProperty(propertyName: "ide")]
        public string Identificador
        {
            get
            {
                return this.indentificadorField;
            }
            set
            {
                this.indentificadorField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty(propertyName: "clv")]
        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty(propertyName: "tip")]
        public string Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty(propertyName: "cnp")]
        public string Concepto
        {
            get
            {
                return etiquetaField;
            }
            set
            {
                this.etiquetaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty(propertyName: "ttl")]
        public decimal Importe
        {
            get
            {
                return this.importeField;
            }
            set
            {
                this.importeField = value;
                this.OnPropertyChanged();
            }
        }
    }
}