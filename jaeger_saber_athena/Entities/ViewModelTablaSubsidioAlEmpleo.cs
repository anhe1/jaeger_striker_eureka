﻿using System;
using System.Linq;
using System.ComponentModel;
using Newtonsoft.Json;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    [SugarTable("_tblsbsd", TableDescription = "tabla subsidio al empleo")]
    public class ViewModelTablaSubsidioAlEmpleo : BasePropertyChangeImplementation
    {
        private BindingList<Subsidio> subsidios;
        private int indiceField;
        private string etiquetaField;
        private int periodoField;

        public ViewModelTablaSubsidioAlEmpleo()
        {
            this.subsidios = new BindingList<Subsidio>();
        }

        [SugarColumn(ColumnName = "_tblsbsd_id", ColumnDescription = "indice de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_tblsbsd_etq", ColumnDescription = "etiqueta", Length = 30)]
        public string Descripcion
        {
            get
            {
                return etiquetaField;
            }
            set
            {
                etiquetaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_tblsbsd_prd", ColumnDescription = "periodo")]
        public int Periodo
        {
            get
            {
                return periodoField;
            }
            set
            {
                periodoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<Subsidio> Rangos
        {
            get
            {
                return this.subsidios;
            }
            set
            {
                this.subsidios = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_tblsbsd_json", ColumnDescription = "rangos de subsidio del periodo", ColumnDataType = "TEXT", IsNullable = true)]
        public string Json
        {
            get
            {
                return JsonConvert.SerializeObject(this.Rangos);
            }
            set
            {
                try
                {
                    this.Rangos = JsonConvert.DeserializeObject<BindingList<Subsidio>>(value);
                }
                catch (Exception)
                {
                    this.Rangos = null;
                }
            }
        }

        public Subsidio GetSubsidio(decimal baseGravable)
        {
            try
            {
                return this.Rangos.First((Subsidio p) => baseGravable >= p.IngresosDesde & baseGravable <= p.IngresosHasta);
            }
            catch (Exception)
            {
                Subsidio t = new Subsidio();
                return t;
            }
        }
    }
}
