﻿/// <summary>
/// develop: anhe1 161120181644
/// purpose: impuesto sobre la renta
/// </summary>
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    [JsonObject]
    public class ImpuestoSobreRenta : BasePropertyChangeImplementation
    {
        private decimal limiteInferiorField;
        private decimal limiteSuperiorField;
        private decimal cuotaFijaField;
        private decimal excendenteInferiorField;

        public ImpuestoSobreRenta()
        {

        }

        public ImpuestoSobreRenta(decimal limiteInferiorField, decimal limiteSuperiorField, decimal cuotaFijaField, decimal excendenteInferior)
        {
            this.limiteInferiorField = limiteInferiorField;
            this.limiteSuperiorField = limiteSuperiorField;
            this.cuotaFijaField = cuotaFijaField;
            this.excendenteInferiorField = excendenteInferior;
        }

        [JsonProperty("li")]
        public decimal LimiteInferior
        {
            get
            {
                return this.limiteInferiorField;
            }
            set
            {
                this.limiteInferiorField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("ls")]
        public decimal LimiteSuperior
        {
            get
            {
                return this.limiteSuperiorField;
            }
            set
            {
                this.limiteSuperiorField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("cf")]
        public decimal CuotaFija
        {
            get
            {
                return this.cuotaFijaField;
            }
            set
            {
                this.cuotaFijaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("pe")]
        public decimal PorcentajeExcedente
        {
            get
            {
                return this.excendenteInferiorField;
            }
            set
            {
                this.excendenteInferiorField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal ISR(decimal baseGravable)
        {
            decimal excedenteLimiteInferior = baseGravable - this.LimiteInferior;
            decimal impuestoMarginal = (excedenteLimiteInferior * this.PorcentajeExcedente) / 100;
            decimal impuestoDeterminado = impuestoMarginal + this.CuotaFija;
            return impuestoDeterminado;
        }
    }
}
