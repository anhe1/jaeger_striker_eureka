﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    [SugarTable("_ctldptf", "nomina: catalogo de puestos y funciones")]
    public class ViewModelDeptoFuncion : BasePropertyChangeImplementation
    {
        private int index;
        private int idDepartmanto;
        private int secuencia;
        private bool activo = true;
        private string nombre;
        private string creo;
        private string modifico;
        private DateTime fechaNuevoField;
        private DateTime? fechaModificoField;

        public ViewModelDeptoFuncion()
        {
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptf_id", ColumnDescription = "indice", IsIdentity = true, IsPrimaryKey = true, Length = 11)]
        public int Id
        {
            get
            {
                return this.index;
            }
            set
            {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptf_a", ColumnDescription = "registro activo", Length = 1)]
        public bool Activo
        {
            get
            {
                return this.activo;
            }
            set
            {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la secuencia de ordenamiento
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptf_sec", ColumnDescription = "secuencia de ordenamiento", Length = 11, IsNullable = true)]
        public int Secuencia
        {
            get
            {
                return this.secuencia;
            }
            set
            {
                this.secuencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con el departamento
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptf_sbid", ColumnDescription = "indice del departamento al que pertenece el puesto o funcion", Length = 11, IsNullable = true)]
        public int IdPuesto
        {
            get
            {
                return this.idDepartmanto;
            }
            set
            {
                this.idDepartmanto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptf_fn", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo
        {
            get
            {
                return this.fechaNuevoField;
            }
            set
            {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptf_fm", ColumnDescription = "fecha de la ultima modifiacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifico
        {
            get
            {
                return this.fechaModificoField;
            }
            set
            {
                this.fechaModificoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptf_usr_n", ColumnDescription = "clave del usuario que crea el registro", Length = 10)]
        public string Creo
        {
            get
            {
                return this.creo;
            }
            set
            {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptf_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifico
        {
            get
            {
                return this.modifico;
            }
            set
            {
                this.modifico = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del puesto
        /// </summary>
        [SugarColumn(ColumnName = "_ctldptf_nom", ColumnDescription = "descripcion del puesto", Length = 128, IsNullable = false)]
        public string Descripcion
        {
            get
            {
                return this.nombre;
            }
            set
            {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }
    }
}
