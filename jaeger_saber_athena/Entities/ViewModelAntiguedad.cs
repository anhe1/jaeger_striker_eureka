﻿using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    [SugarTable("_tblantg", "nomina: tabla de antguedades y factores de integracion")]
    public class ViewModelAntiguedad : BasePropertyChangeImplementation
    {
        private int indiceField;
        private int antiguiedadAniosField;
        private int diasVacacionesField;
        private decimal porcentajePrimaField;
        private int diasAguinaldoField;
        private decimal factorIntegracionField;

        public ViewModelAntiguedad()
        {

        }

        [SugarColumn(ColumnName = "_tblantg_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true, IsOnlyIgnoreInsert = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer los años de servicio
        /// </summary>
        [SugarColumn(ColumnName = "_tblantg_anios", ColumnDescription = "años de servicio", Length = 2)]
        public int Anios
        {
            get
            {
                return this.antiguiedadAniosField;
            }
            set
            {
                this.antiguiedadAniosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer los dias de vacaciones por los años de servcio
        /// </summary>
        [SugarColumn(ColumnName = "_tblantg_dsvac", ColumnDescription = "dias de vacaciones", Length = 2)]
        public int DiasVacaciones
        {
            get
            {
                return this.diasVacacionesField;
            }
            set
            {
                this.diasVacacionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el porcentaje de la prima vacacional
        /// </summary>
        [SugarColumn(ColumnName = "_tblantg_prm", ColumnDescription = "porcentaje de la prima", Length = 4, DecimalDigits = 4)]
        public decimal PorcentajePrima
        {
            get
            {
                return this.porcentajePrimaField;
            }
            set
            {
                this.porcentajePrimaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer los dias del aguinaldo
        /// </summary>
        [SugarColumn(ColumnName = "_tblantg_dsagui", ColumnDescription = "dias aplicables de aguinado", Length = 2)]
        public int DiasAguinaldo
        {
            get
            {
                return this.diasAguinaldoField;
            }
            set
            {
                this.diasAguinaldoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor de integracion
        /// </summary>
        [SugarColumn(ColumnName = "_tblantg_factor", ColumnDescription = "factor de integracion", Length = 10, DecimalDigits = 4)]
        public decimal Factor
        {
            get
            {
                return this.factorIntegracionField;
            }
            set
            {
                this.factorIntegracionField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
