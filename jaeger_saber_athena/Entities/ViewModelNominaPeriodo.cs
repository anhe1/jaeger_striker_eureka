﻿using System;
using System.Linq;
using System.ComponentModel;
using Newtonsoft.Json;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Nomina.Entities
{
    [SugarTable("_nmnprd", "nomina: periodo de pago de nomina de empleados")]
    public class ViewModelNominaPeriodo : BasePropertyChangeImplementation
    {
        private int indiceField;
        private bool activoField;
        private int controlField;
        private int empleadoIdField;
        private int numField;
        private string claveField;
        private string curpField;
        private string rfcField;
        private string nombreCompletoField;
        private string primerapellidoField;
        private string segundoapellidoField;
        private int jornadasTrabajoField;
        private bool puntualidadField;
        private int ausenciasField;
        private int incapacidadField;
        private int productividadField;
        private decimal salarioDiarioField;
        private decimal salarioDiarioIntegradoField;
        private decimal isptField;
        private decimal subsidioField;
        private decimal cuotasObreroImssField;
        private decimal cuotasPatronalesImssField;
        private decimal impuestoSobreNominaField;
        private string antiguedadField;
        private DateTime? fechaIngresoField;
        private int diasVacacionesField;
        private int horasRelojField;
        private int horasExtraField;
        private int horasAutorizadasField;
        private int horasJornadaField;
        private long horasCargoField;
        private long horasAbonoField;
        private decimal descuentoInfonavitField;
        private decimal percepcionEspecialField;
        private decimal descuentoPresatamoField;
        private decimal premiosPuntualidadField;
        private decimal becasTrabajadoresHijosField;
        private decimal premiosAsistenciaField;
        private decimal ayudaArtEscolaresField;
        private decimal ayudaTransporteField;
        private int productividadIdField;
        private BindingList<Percepcion> percepciones;
        private BindingList<Deduccion> deducciones;
        private CuotasObreroIMSS cuotasObreroField;
        private CuotasPatronalesIMSS cuotasPatronalesField;

        public ViewModelNominaPeriodo()
        {
            this.activoField = true;
            this.percepciones = new BindingList<Percepcion>();
            this.deducciones = new BindingList<Deduccion>();
            this.cuotasObreroField = new CuotasObreroIMSS();
            this.cuotasPatronalesField = new CuotasPatronalesIMSS();
        }

        /// <summary>
        /// obtener o establecer el indice del registro de la nomina
        /// </summary>
        [DataNames("_nmnprd_id")]
        [SugarColumn(ColumnName = "_nmnprd_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsOnlyIgnoreInsert = true, IsIdentity = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer si el registro se encuentra activo
        /// </summary>
        [DataNames("_nmnprd_a")]
        [SugarColumn(ColumnName = "_nmnprd_a", ColumnDescription = "registro activo")]
        public bool Activo
        {
            get
            {
                return this.activoField;
            }
            set
            {
                this.activoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del control de nomina
        /// </summary>
        [DataNames("_nmnprd_nmnctrl_id")]
        [SugarColumn(ColumnName = "_nmnprd_nmnctrl_id", ColumnDescription = "indice del control de nomina")]
        public int ControlId
        {
            get
            {
                return this.controlField;
            }
            set
            {
                this.controlField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la tabla de empleados
        /// </summary>
        [DataNames("_nmnprd_ctlemp_id")]
        [SugarColumn(ColumnName = "_nmnprd_ctlemp_id", ColumnDescription = "indice del empleado del catalogo")]
        public int EmpleadoId
        {
            get
            {
                return this.empleadoIdField;
            }
            set
            {
                this.empleadoIdField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero del empleado
        /// </summary>
        [DataNames("_nmnprd_num")]
        [SugarColumn(ColumnName = "_nmnprd_num", ColumnDescription = "numero de empleado")]
        public int Numero
        {
            get
            {
                return this.numField;
            }
            set
            {
                this.numField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de registro del empleado
        /// </summary>
        [DataNames("_nmnprd_clv")]
        [SugarColumn(ColumnName = "_nmnprd_clv", ColumnDescription = "clave del empleado", Length = 10)]
        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el empleado tiene el bono de productividad
        /// </summary>
        [DataNames("_nmnprd_pntld")]
        [SugarColumn(ColumnName = "_nmnprd_pntld", ColumnDescription = "registro de puntualidad", IsNullable = true)]
        public bool Puntualidad
        {
            get
            {
                return this.puntualidadField;
            }
            set
            {
                this.puntualidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ausencias
        /// </summary>
        [DataNames("_nmnprd_ausn")]
        [SugarColumn(ColumnName = "_nmnprd_ausn", ColumnDescription = "registro de ausencias", IsNullable = true)]
        public int Ausencias
        {
            get
            {
                return this.ausenciasField;
            }
            set
            {
                this.ausenciasField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_nmnprd_icap")]
        [SugarColumn(ColumnName = "_nmnprd_icap", ColumnDescription = "registro de incapacidades", IsNullable = true)]
        public int Incapacidad
        {
            get
            {
                return this.incapacidadField;
            }
            set
            {
                this.incapacidadField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_nmnprd_dsvac")]
        [SugarColumn(ColumnName = "_nmnprd_dsvac", ColumnDescription = "registro de dias de vacaciones", IsNullable = true)]
        public int DiasVacaciones
        {
            get
            {
                return this.diasVacacionesField;
            }
            set
            {
                this.diasVacacionesField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_nmnprd_hrrlj")]
        [SugarColumn(ColumnName = "_nmnprd_hrrlj", ColumnDescription = "registro de horas de reloj", IsNullable = true)]
        public int HorasReloj
        {
            get
            {
                return this.horasRelojField;
            }
            set
            {
                this.horasRelojField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer las horas extra aplicables
        /// </summary>
        [DataNames("_nmnprd_hrsextr")]
        [SugarColumn(ColumnName = "_nmnprd_hrsextr", ColumnDescription = "percepcion: horas extra", IsNullable = true)]
        public int HorasExtra
        {
            get
            {
                return this.horasExtraField;
            }
            set
            {
                this.horasExtraField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer las horas extra autorizadas
        /// </summary>
        [DataNames("_nmnprd_hrsaut")]
        [SugarColumn(ColumnName = "_nmnprd_hrsaut", ColumnDescription = "percepcion: horas autorizadas", IsNullable = true)]
        public int HorasAutorizadas
        {
            get
            {
                return this.horasAutorizadasField;
            }
            set
            {
                this.horasAutorizadasField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer las horas a favor del empleado
        /// </summary>
        [DataNames("_nmnprd_hrscrg")]
        [SugarColumn(ColumnName = "_nmnprd_hrscrg", ColumnDescription = "percepcion: a favor del empleado (solo para impresores)", IsNullable = true)]
        public long HorasCargo
        {
            get
            {
                return this.horasCargoField;
            }
            set
            {
                this.horasCargoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer las horas que se deben por parte del empleado
        /// </summary>
        [DataNames("_nmnprd_hrsabn")]
        [SugarColumn(ColumnName = "_nmnprd_hrsabn", ColumnDescription = "percepcion: horas que debe el empleado (solo para impresores)", IsNullable = true)]
        public long HorasAbono
        {
            get
            {
                return this.horasAbonoField;
            }
            set
            {
                this.horasAbonoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer salario diario
        /// </summary>
        [DataNames("_nmnprd_sd")]
        [SugarColumn(ColumnName = "_nmnprd_sd", ColumnDescription = "salario diario", IsNullable = true, DecimalDigits = 4)]
        public decimal SalarioDiario
        {
            get
            {
                return this.salarioDiarioField;
            }
            set
            {
                this.salarioDiarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer salario diario integrado
        /// </summary>
        [DataNames("_nmnprd_sdi")]
        [SugarColumn(ColumnName = "_nmnprd_sdi", ColumnDescription = "salario diario integrado", IsNullable = true, DecimalDigits = 4)]
        public decimal SalarioDiarioIntegrado
        {
            get
            {
                return this.salarioDiarioIntegradoField;
            }
            set
            {
                this.salarioDiarioIntegradoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener
        /// </summary>
        [SugarColumn(ColumnName = "_nmnprd_slds", ColumnDescription = "percepcion: sueldos y salario", IsNullable = true, DecimalDigits = 4)]
        public decimal SueldosSalarios
        {
            get
            {
                if (this.percepciones != null)
                {
                    return this.percepciones.Where(it => it.Identificador == "SSR").Sum(it => it.ImporteTotal);
                }
                return 0;
            }
        }

        /// <summary>
        /// obtener el total de percepciones
        /// </summary>
        [SugarColumn(ColumnName = "_nmnprd_ttlper", ColumnDescription = "total de percepciones", IsNullable = true, IsIgnore = true, DecimalDigits = 4)]
        public decimal TotalPercepciones
        {
            get
            {
                if (this.percepciones != null)
                {
                    return this.percepciones.Sum((Percepcion p) => p.ImporteGravado) + this.percepciones.Sum((Percepcion p) => p.ImporteExento);
                }
                return 0;
            }
        }

        /// <summary>
        /// obtener el total de percepciones gravadas
        /// </summary>
        [SugarColumn(ColumnName = "_nmnprd_pergrv", ColumnDescription = "total de percepciones gravados", IsNullable = true, IsIgnore = true, DecimalDigits = 4)]
        public decimal TotalPercepcionesGravado
        {
            get
            {
                if (this.percepciones != null)
                {
                    return this.percepciones.Sum((Percepcion p) => p.ImporteGravado);
                }
                return 0;
            }
        }

        /// <summary>
        /// obtener el total de percepciones exentas
        /// </summary>
        [SugarColumn(ColumnName = "_nmnprd_perexn", ColumnDescription = "total de percepciones exento", IsNullable = true, IsIgnore = true, DecimalDigits = 4)]
        public decimal TotalPercepcionesExento
        {
            get
            {
                if (this.percepciones != null)
                {
                    return this.percepciones.Sum((Percepcion p) => p.ImporteExento);
                }
                return 0;
            }
        }

        /// <summary>
        /// obtener el total de deducciones
        /// </summary>
        [SugarColumn(ColumnName = "_nmnprd_ttldec", ColumnDescription = "total de deducciones", IsNullable = true, IsIgnore = true, DecimalDigits = 4)]
        public decimal TotalDeducciones
        {
            get
            {
                if (this.deducciones != null)
                {
                    return this.deducciones.Sum((Deduccion p) => p.Importe);
                }
                return 0;
            }
        }

        /// <summary>
        /// obtener o establecer el % del premio de produccion
        /// </summary>
        [DataNames("_nmnprd_prod")]
        [SugarColumn(ColumnName = "_nmnprd_prod", ColumnDescription = "percepcion: premio de produccion", IsNullable = true)]
        public int Productividad
        {
            get
            {
                return this.productividadField;
            }
            set
            {
                this.productividadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer importe de la percepcion especial 
        /// </summary>
        [DataNames("_nmnprd_peresp")]
        [SugarColumn(ColumnName = "_nmnprd_peresp", ColumnDescription = "percepcion: percepcion especial", IsNullable = true, DecimalDigits = 4)]
        public decimal PercepcionEspecial
        {
            get
            {
                return this.percepcionEspecialField;
            }
            set
            {
                this.percepcionEspecialField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer el descuento por el importe del prestamo
        /// </summary>
        [DataNames("_nmnprd_decpre")]
        [SugarColumn(ColumnName = "_nmnprd_decpre", ColumnDescription = "deduccion: descuento por prestamo personal", IsNullable = true, DecimalDigits = 4)]
        public decimal DescuentoPresatamo
        {
            get
            {
                return this.descuentoPresatamoField;
            }
            set
            {
                this.descuentoPresatamoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_nmnprd_desinf", ColumnDescription = "deduccion: descuento por concepto de infonavit", IsNullable = true, DecimalDigits = 4)]
        public decimal DescuentoInfonavit
        {
            get
            {
                if (this.deducciones != null)
                {
                    return this.deducciones.Where(it => it.Identificador == "INF").Sum(it => it.Importe);
                }
                return 0;
            }
        }

        /// <summary>
        /// obtener el importe del fondo de ahorro, aportada por el empleado
        /// </summary>
        [SugarColumn(ColumnName = "_nmnprd_fda", ColumnDescription = "percepcion: fondo de ahorro", IsNullable = true, DecimalDigits = 4)]
        public decimal FondoAhorro
        {
            get
            {
                if (this.percepciones != null)
                {
                    return this.percepciones.Where(it => it.Identificador == "FDA").Sum(it => it.ImporteExento);
                }
                return 0;
            }
        }

        [SugarColumn(ColumnName = "_nmnprd_vdd", ColumnDescription = "percepcion: vales de despensa", IsNullable = true, DecimalDigits = 4)]
        public decimal ValesDespensa
        {
            get
            {
                if (this.percepciones != null)
                {
                    return this.percepciones.Where(it => it.Identificador == "VDD").Sum(it => it.ImporteExento);
                }
                return 0;
            }
        }

        /// <summary>
        /// obtener el total del sueldo
        /// </summary>
        [SugarColumn(ColumnName = "_nmnprd_sldnt", ColumnDescription = "sueldo neto", IsNullable = true, DecimalDigits = 4)]
        public decimal SueldoNeto
        {
            get
            {
                return this.TotalPercepciones - this.TotalDeducciones;
            }
        }

        [SugarColumn(ColumnName = "_nmnprd_sld", ColumnDescription = "monto del sueldo a depositar", IsNullable = true, DecimalDigits = 4)]
        public decimal Sueldo
        {
            get
            {
                if (this.percepciones != null)
                {
                    return this.SueldoNeto - this.percepciones.Where(it => it.Identificador == "VDD").Sum(it => it.ImporteExento);
                }
                return 0;
            }
        }

        /// <summary>
        /// obtener o establacer el monto del ISR causado por el totdal de percepciones gravadas del empleado
        /// </summary>
        [DataNames("_nmnprd_isr")]
        [SugarColumn(ColumnName = "_nmnprd_isr", ColumnDescription = "monto del isr causado", IsNullable = true, DecimalDigits = 4)]
        public decimal ISR
        {
            get
            {
                return isptField;
            }
            set
            {
                isptField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el monto del subsidio alcanzado
        /// </summary>
        [DataNames("_nmnprd_sbsd")]
        [SugarColumn(ColumnName = "_nmnprd_sbsd", ColumnDescription = "monto del subsidio al empleo", IsNullable = true, DecimalDigits = 4)]
        public decimal SubSidio
        {
            get
            {
                return this.subsidioField;
            }
            set
            {
                this.subsidioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el ISR a retener
        /// </summary>
        [SugarColumn(ColumnName = "_nmnprd_isrrtn", ColumnDescription = "deduccion: isr a retener", IsNullable = true, DecimalDigits = 4)]
        public decimal ISRRetener
        {
            get
            {
                return this.deducciones.Where(it => it.Identificador == "ISR").Sum(it => it.Importe);
                //decimal c = this.deducciones.Sum(it => it.Importe) - this.SubSidio;
                //if (c <= 0)
                //{
                //    return 0;
                //}
                //return this.deducciones.Sum(it => it.Importe) - this.SubSidio;
            }
        }

        /// <summary>
        /// obtener o establacer el monto de las cuotas IMSS del empleado
        /// </summary>
        [DataNames("_nmnprd_imsse")]
        [SugarColumn(ColumnName = "_nmnprd_imsse", ColumnDescription = "cuotas obrero", IsNullable = true, DecimalDigits = 4)]
        public decimal CuotasIMSS
        {
            get
            {
                return this.cuotasObreroImssField;
            }
            set
            {
                this.cuotasObreroImssField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer el monto de las cuotas IMSS a pagar por parte del patron
        /// </summary>
        [DataNames("_nmnprd_imssp")]
        [SugarColumn(ColumnName = "_nmnprd_imssp", ColumnDescription = "cuotas patronales", IsNullable = true, DecimalDigits = 4)]
        public decimal CuotasIMSSPatronal
        {
            get
            {
                return this.cuotasPatronalesImssField;
            }
            set
            {
                this.cuotasPatronalesImssField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener el monto del impuesto sobre la nomina
        /// </summary>
        [DataNames("_nmnprd_impnmn")]
        [SugarColumn(ColumnName = "_nmnprd_impnmn", ColumnDescription = "impuestos sobre nomina", IsNullable = true, DecimalDigits = 4)]
        public decimal ImpuestoSobreNomina
        {
            get
            {
                return this.impuestoSobreNominaField;
            }
            set
            {
                this.impuestoSobreNominaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer la antiguedad 
        /// </summary>
        [DataNames("_nmnprd_antg")]
        [SugarColumn(ColumnName = "_nmnprd_antg", Length = 10, ColumnDescription = "antiguedad", IsNullable = true)]
        public string Antiguedad
        {
            get
            {
                return this.antiguedadField;
            }
            set
            {
                this.antiguedadField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_nmnprd_pers")]
        [SugarColumn(ColumnDataType = "Text", ColumnName = "_nmnprd_pers", ColumnDescription = "json percepciones", IsNullable = true)]
        public string JPercepciones
        {
            get
            {
                if (this.percepciones.Count > 0)
                {
                    return JsonConvert.SerializeObject(this.Percepciones, Formatting.None);
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {
                this.Percepciones = JsonConvert.DeserializeObject<BindingList<Percepcion>>(value);
                this.OnPropertyChanged();
            }
        }

        [DataNames("_nmnprd_dedu")]
        [SugarColumn(ColumnDataType = "Text", ColumnName = "_nmnprd_dedu", ColumnDescription = "json de deducciones", IsNullable = true)]
        public string JDeducciones
        {
            get
            {
                if (this.Deducciones.Count > 0)
                {
                    return JsonConvert.SerializeObject(this.Deducciones, Formatting.None);
                }
                return string.Empty;
            }
            set
            {
                this.Deducciones = JsonConvert.DeserializeObject<BindingList<Deduccion>>(value);
                this.OnPropertyChanged();
            }
        }

        [DataNames("_nmnprd_ctobrr")]
        [SugarColumn(ColumnDataType = "Text", ColumnName = "_nmnprd_ctobrr", ColumnDescription = "json de cuotas obrero", IsNullable = true)]
        public string JCuotasObrero
        {
            get
            {
                return this.CuotasObrero.Json();
            }
            set
            {
                this.CuotasObrero = CuotasObreroIMSS.Json(value);
                this.OnPropertyChanged();
            }
        }

        [DataNames("_nmnprd_ctptrn")]
        [SugarColumn(ColumnDataType = "Text", ColumnName = "_nmnprd_ctptrn", ColumnDescription = "json de cuotas patronales", IsNullable = true)]
        public string JCuotasPatronales
        {
            get
            {
                return this.CuotasPatronales.Json();
            }
            set
            {
                this.CuotasPatronales = CuotasPatronalesIMSS.Json(value);
                this.OnPropertyChanged();
            }
        }

        #region propiedades ignoradas al guardar en bd

        /// <summary>
        /// obtener o establecer las horas de la jornada del trabajador, este dato no se guarda
        /// </summary>
        [DataNames("_nmnprd_hrsjrnd")]
        [SugarColumn(ColumnName = "_nmnprd_hrsjrnd", ColumnDescription = "registro de horas de reloj", IsNullable = true, IsIgnore = true)]
        public int HorasJornada
        {
            get
            {
                return this.horasJornadaField;
            }
            set
            {
                this.horasJornadaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public int ProductividadId
        {
            get
            {
                return this.productividadIdField;
            }
            set
            {
                this.productividadIdField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer CURP del empleado
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string CURP
        {
            get
            {
                return this.curpField;
            }
            set
            {
                this.curpField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del empleado
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string RFC
        {
            get
            {
                return this.rfcField;
            }
            set
            {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener nombre completo del empleado
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string Nombre
        {
            get
            {
                return string.Format(format: "{0} {1} {2}", arg0: this.PrimerApellido, arg1: this.SegundoApellido, arg2: this.Nombres);
            }
        }

        [DataNames("_ctlemp_nom")]
        [SugarColumn(IsIgnore = true)]
        public string Nombres
        {
            get
            {
                return this.nombreCompletoField;
            }
            set
            {
                this.nombreCompletoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_pape")]
        [SugarColumn(IsIgnore = true)]
        public string PrimerApellido
        {
            get
            {
                return this.primerapellidoField;
            }
            set
            {
                this.primerapellidoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_sape")]
        [SugarColumn(IsIgnore = true)]
        public string SegundoApellido
        {
            get
            {
                return this.segundoapellidoField;
            }
            set
            {
                this.segundoapellidoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public int JornadasTrabajo
        {
            get
            {
                return this.jornadasTrabajoField;
            }
            set
            {
                this.jornadasTrabajoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public TimeSpan HrsCargo
        {
            get
            {
                return TimeSpan.FromTicks(this.HorasCargo);
            }
            set
            {
                this.HorasCargo = value.Ticks;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public TimeSpan HrsAbono
        {
            get
            {
                return TimeSpan.FromTicks(this.HorasAbono);
            }
            set
            {
                this.horasAbonoField = value.Ticks;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<Percepcion> Percepciones
        {
            get
            {
                return this.percepciones;
            }
            set
            {
                this.percepciones = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<Deduccion> Deducciones
        {
            get
            {
                return this.deducciones;
            }
            set
            {
                this.deducciones = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public CuotasObreroIMSS CuotasObrero
        {
            get
            {
                return this.cuotasObreroField;
            }
            set
            {
                this.cuotasObreroField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public CuotasPatronalesIMSS CuotasPatronales
        {
            get
            {
                return this.cuotasPatronalesField;
            }
            set
            {
                this.cuotasPatronalesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener el monto del fondo de ahorro, aporte del patron
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public decimal FondoAhorroP
        {
            get
            {
                if (this.percepciones != null)
                {
                    return this.percepciones.Where(it => it.Identificador == "FDA").Sum(it => it.ImporteExento);
                }
                return 0;
            }
        }

        #region percepciones adicionales

        /// <summary>
        /// importe del descuento por credito infonavit
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public decimal CreditoVivienda
        {
            get
            {
                return this.descuentoInfonavitField;
            }
            set
            {
                this.descuentoInfonavitField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public DateTime? FechaIngreso
        {
            get
            {
                return this.fechaIngresoField;
            }
            set
            {
                this.fechaIngresoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal PremiosPuntualidad
        {
            get
            {
                return this.premiosPuntualidadField;
            }
            set
            {
                this.premiosPuntualidadField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal Becas
        {
            get
            {
                return this.becasTrabajadoresHijosField;
            }
            set
            {
                this.becasTrabajadoresHijosField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal PremiosAsistencia
        {
            get
            {
                return this.premiosAsistenciaField;
            }
            set
            {
                this.premiosAsistenciaField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal AyudaArtEscolares
        {
            get
            {
                return this.ayudaArtEscolaresField;
            }
            set
            {
                this.ayudaArtEscolaresField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal AyudaTransporte
        {
            get
            {
                return this.ayudaTransporteField;
            }
            set
            {
                this.ayudaTransporteField = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #endregion
    }
}
