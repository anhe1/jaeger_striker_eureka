﻿using System;
using System.ComponentModel;
using System.Linq;
using Newtonsoft.Json;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Nomina.Entities
{
    [SugarTable("_tblprd", "nomina: tabla de premio de productividad (solo impresores)")]
    public class ViewModelTablaProductividad : BasePropertyChangeImplementation
    {
        private int indiceField;
        private string descripcionField;
        private BindingList<NominaRangoProductividad> rangosField;

        public ViewModelTablaProductividad()
        {
            this.indiceField = 0;
            this.descripcionField = "";
            this.rangosField = new BindingList<NominaRangoProductividad>();
        }

        [SugarColumn(ColumnDescription = "indice de la tabla", ColumnName = "_tblprd_id", IsPrimaryKey = true, IsOnlyIgnoreInsert = true, IsIdentity = true)]
        public int Id
        {
            get
            {
                return this.indiceField;
            }
            set
            {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnDescription = "descripcion de la tabla", ColumnName = "_tblprd_descr", Length = 30)]
        public string Descripcion
        {
            get
            {
                return this.descripcionField;
            }
            set
            {
                descripcionField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<NominaRangoProductividad> Rangos
        {
            get
            {
                return this.rangosField;
            }
            set
            {
                this.rangosField = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [SugarColumn(ColumnDescription = "tabla de rangos", ColumnName = "_tblprd_json", ColumnDataType = "Text")]
        public string JRangos
        {
            get
            {
                return JsonConvert.SerializeObject(this.Rangos, Formatting.None);
            }
            set
            {
                this.Rangos = JsonConvert.DeserializeObject<BindingList<NominaRangoProductividad>>(value);
                this.OnPropertyChanged();
            }
        }

        public NominaRangoProductividad GetProductividad(decimal porcentaje)
        {
            try
            {
                return this.Rangos.First((NominaRangoProductividad p) => porcentaje >= p.Productividad & porcentaje <= p.Productividad);
            }
            catch (Exception)
            {
                NominaRangoProductividad t = new NominaRangoProductividad();
                return t;
            }
        }
    }

    [JsonObject]
    public partial class NominaRangoProductividad : Jaeger.Domain.Base.Abstractions.BasePropertyChangeImplementation
    {
        private decimal productividadField;
        private decimal importeField;

        public NominaRangoProductividad()
        {

        }

        [JsonProperty("v")]
        public decimal Productividad
        {
            get
            {
                return this.productividadField;
            }
            set
            {
                productividadField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("i")]
        public decimal Importe
        {
            get
            {
                return this.importeField;
            }
            set
            {
                importeField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
