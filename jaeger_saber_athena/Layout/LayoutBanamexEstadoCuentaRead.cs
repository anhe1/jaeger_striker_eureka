using FileHelpers;
using System;

namespace Jaeger.Nomina.Layout
{
    [IgnoreFirst(4)]
    [DelimitedRecord("|")]
    public sealed class LayoutBanamexEstadoCuentaRead
    {
        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
        private System.DateTime? mField1;

        /// <summary>
        /// obtener o establecer la fecha del reporte
        /// </summary>
        public System.DateTime? Fecha
        {
            get
            {
                return mField1;
            }
            set
            {
                mField1 = value;
            }
        }

        private string mField2;

        /// <summary>
        /// obtener o establecer la hora de emisi�n del reporte
        /// </summary>
        public string Hora
        {
            get
            {
                return mField2;
            }
            set
            {
                mField2 = value;
            }
        }

        private string mField3;

        public string Field3
        {
            get
            {
                return mField3;
            }
            set
            {
                mField3 = value;
            }
        }

        private string mField4;

        /// <summary>
        /// obtener o establecer el nombre del beneficiario de la cuenta
        /// </summary>
        public string Beneficiario
        {
            get
            {
                return mField4;
            }
            set
            {
                mField4 = value;
            }
        }

        private string mField5;

        public string Field5
        {
            get
            {
                return mField5;
            }
            set
            {
                mField5 = value;
            }
        }

        private string mField6;

        /// <summary>
        /// obtener o establecer el nombre del usuario que emite el reporte
        /// </summary>
        public string Usuario
        {
            get
            {
                return mField6;
            }
            set
            {
                mField6 = value;
            }
        }

        private string mField7;

        /// <summary>
        /// obtener o establecer el tipo de cuenta
        /// </summary>
        public string Tipo
        {
            get
            {
                return mField7;
            }
            set
            {
                mField7 = value;
            }
        }

        private string mField8;

        /// <summary>
        /// obtener o establecer el numero de la sucursal de la cuenta
        /// </summary>
        public string Sucursal
        {
            get
            {
                return mField8;
            }
            set
            {
                mField8 = value;
            }
        }

        private string mField9;

        /// <summary>
        /// obtener o establecer el numero de la cuenta
        /// </summary>
        public string NoCuenta
        {
            get
            {
                return mField9;
            }
            set
            {
                mField9 = value;
            }
        }

        private string mField10;

        /// <summary>
        /// obtener o establecer el nombre
        /// </summary>
        public string Nombre
        {
            get
            {
                return mField10;
            }
            set
            {
                mField10 = value;
            }
        }

        private string fecha2Field;

        /// <summary>
        /// obtener o establecer otra fecha??
        /// </summary>
        public string Fecha2
        {
            get
            {
                return fecha2Field;
            }
            set
            {
                fecha2Field = value;
            }
        }

        private string mField12;

        /// <summary>
        /// obtener o establecer fecha 3
        /// </summary>
        public string Fecha3
        {
            get
            {
                return mField12;
            }
            set
            {
                mField12 = value;
            }
        }

        private string mField13;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field13
        {
            get
            {
                return mField13;
            }
            set
            {
                mField13 = value;
            }
        }

        private decimal mField14;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public decimal Field14
        {
            get
            {
                return mField14;
            }
            set
            {
                mField14 = value;
            }
        }

        private string mField15;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field15
        {
            get
            {
                return mField15;
            }
            set
            {
                mField15 = value;
            }
        }

        private string mField16;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field16
        {
            get
            {
                return mField16;
            }
            set
            {
                mField16 = value;
            }
        }

        private string mField17;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field17
        {
            get
            {
                return mField17;
            }
            set
            {
                mField17 = value;
            }
        }

        private string mField18;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field18
        {
            get
            {
                return mField18;
            }
            set
            {
                mField18 = value;
            }
        }

        private string mField19;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field19
        {
            get
            {
                return mField19;
            }
            set
            {
                mField19 = value;
            }
        }

        private string mField20;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field20
        {
            get
            {
                return mField20;
            }
            set
            {
                mField20 = value;
            }
        }

        private string mField21;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field21
        {
            get
            {
                return mField21;
            }
            set
            {
                mField21 = value;
            }
        }

        private string mField22;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field22
        {
            get
            {
                return mField22;
            }
            set
            {
                mField22 = value;
            }
        }

        private string mField23;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field23
        {
            get
            {
                return mField23;
            }
            set
            {
                mField23 = value;
            }
        }

        private string mField24;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field24
        {
            get
            {
                return mField24;
            }
            set
            {
                mField24 = value;
            }
        }

        private string mField25;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field25
        {
            get
            {
                return mField25;
            }
            set
            {
                mField25 = value;
            }
        }

        private string mField26;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field26
        {
            get
            {
                return mField26;
            }
            set
            {
                mField26 = value;
            }
        }

        private string mField27;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field27
        {
            get
            {
                return mField27;
            }
            set
            {
                mField27 = value;
            }
        }

        private string mField28;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field28
        {
            get
            {
                return mField28;
            }
            set
            {
                mField28 = value;
            }
        }

        private string mField29;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field29
        {
            get
            {
                return mField29;
            }
            set
            {
                mField29 = value;
            }
        }

        private string mField30;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field30
        {
            get
            {
                return mField30;
            }
            set
            {
                mField30 = value;
            }
        }

        private string mField31;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field31
        {
            get
            {
                return mField31;
            }
            set
            {
                mField31 = value;
            }
        }

        private string mField32;

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Field32
        {
            get
            {
                return mField32;
            }
            set
            {
                mField32 = value;
            }
        }

        [DelimitedRecord("|")]
        public partial class LayoutBanamexEstadoCuentaReg
        {
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime Fecha;

            [FieldConverter(typeof(ConceptoConverter))]
            public EstadoCuentaConcepto Concepto;

            [FieldNullValue(typeof(decimal), "0")]
            public decimal Field3;

            [FieldNullValue(typeof(decimal), "0")]
            public decimal Field4;

            [FixedLengthRecord]
            public partial class EstadoCuentaConcepto
            {
                [FieldFixedLength(6)]
                public string Tipo;
                [FieldFixedLength(11)]
                public string Field2;
                [FieldFixedLength(41)]
                public string Descripcion;
                [FieldFixedLength(12)]
                public string NoAutorizacion;
            }

            public partial class ConceptoConverter : ConverterBase
            {
                public override object StringToField(string from)
                {
                    var engine = new FixedFileEngine(typeof(EstadoCuentaConcepto));
                    var result = engine.ReadString(from);
                    if (result != null)
                    {
                        return result[0];
                    }
                    return new EstadoCuentaConcepto();
                }
            }
        }
        
        //public System.Collections.Generic.List<LayoutBanamexEstadoCuentaReg> Movimientos;

        //public void Importar(string archivo)
        //{
        //    var engine = new MasterDetailEngine<LayoutBanamexEstadoCuentaRead, LayoutBanamexEstadoCuentaReg>(new MasterDetailSelector(this.Selector));
        //    MasterDetails<LayoutBanamexEstadoCuentaRead, LayoutBanamexEstadoCuentaReg>[] coleccion = engine.ReadFile(archivo);
        //    if (coleccion != null)
        //    {
        //        System.Console.WriteLine(coleccion[0].Details.Length);
        //    }
        //}

        //private RecordAction Selector(string record)
        //{
        //    if (record.Length>200)
        //        return RecordAction.Master;
        //    else
        //        return RecordAction.Detail;
        //}
    }

    
}