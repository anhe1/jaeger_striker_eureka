﻿using FileHelpers;

namespace Jaeger.Nomina.Layout
{
    [IgnoreFirst(1)]
    [DelimitedRecord("|")]
    public sealed class LayOutEmpleado
    {

        private string numeroField;
        private string claveField;
        private string primerApellidoField;
        private string segundoApellidoField;
        private string nombreField;
        private string mAREA;
        private string departamentoField;
        private string puestoField;
        private string salarioBaseField;
        private string salarioDiarioField;
        private string salarioDiarioIntegreadoField;
        private string jornadasTrabajoField;
        private string horasJornadaTrabajoField;
        private string fechaInicioRelacionLaboralField;
        private string fechaTerminoRelacionLaboralField;
        private string statusField;
        private string cuentaBancariaField;
        private string numeroSeguridadSocialField;
        private string curpField;
        private string rfcField;
        private string correoField;

        public string Num
        {
            get { return numeroField; }
            set { numeroField = value; }
        }

        public string Clave
        {
            get { return claveField; }
            set { claveField = value; }
        }

        public string PrimerApellido
        {
            get { return primerApellidoField; }
            set { primerApellidoField = value; }
        }

        public string SegundoApellido
        {
            get { return segundoApellidoField; }
            set { segundoApellidoField = value; }
        }

        public string Nombres
        {
            get { return nombreField; }
            set { nombreField = value; }
        }

        public string Area
        {
            get { return mAREA; }
            set { mAREA = value; }
        }

        public string Departamento
        {
            get { return departamentoField; }
            set { departamentoField = value; }
        }

        public string Puesto
        {
            get { return puestoField; }
            set { puestoField = value; }
        }

        public string SalarioBase
        {
            get { return this.salarioBaseField; }
            set { this.salarioBaseField = value; }
        }

        public string SalarioDiario
        {
            get { return salarioDiarioField; }
            set { salarioDiarioField = value; }
        }

        public string SalarioDiarioIntegreado
        {
            get { return this.salarioDiarioIntegreadoField; }
            set { this.salarioDiarioIntegreadoField = value; }
        }

        public string JornadasTrabajo
        {
            get { return this.jornadasTrabajoField; }
            set { this.jornadasTrabajoField = value; }
        }

        public string HorasJornadaTrabajo
        {
            get { return this.horasJornadaTrabajoField; }
            set { this.horasJornadaTrabajoField = value; }
        }

        public string FechaInicioRelLaboral
        {
            get { return fechaInicioRelacionLaboralField; }
            set { fechaInicioRelacionLaboralField = value; }
        }

        public string Fecha_Baja
        {
            get { return fechaTerminoRelacionLaboralField; }
            set { fechaTerminoRelacionLaboralField = value; }
        }

        public string Activo
        {
            get { return statusField; }
            set { statusField = value; }
        }

        public string CuentaBancaria
        {
            get { return cuentaBancariaField; }
            set { cuentaBancariaField = value; }
        }

        public string NumSeguridadSocial
        {
            get { return numeroSeguridadSocialField; }
            set { numeroSeguridadSocialField = value; }
        }

        public string CURP
        {
            get { return curpField; }
            set { curpField = value; }
        }

        public string RFC
        {
            get { return rfcField; }
            set { rfcField = value; }
        }

        public string Correo
        {
            get { return correoField; }
            set { correoField = value; }
        }
    }
}