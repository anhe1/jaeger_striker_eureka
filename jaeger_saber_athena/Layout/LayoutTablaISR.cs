﻿using System.IO;
using System.ComponentModel;
using FileHelpers;
using FileHelpers.MasterDetail;

namespace Jaeger.Nomina.Layout
{
    [DelimitedRecord("|")]
    public class LayoutTablaISR
    {
        public string Etiqueta { get; set; }

        public int Periodo { get; set; }

        [DelimitedRecord("|")]
        public partial class ISR
        {
            public ISR()
            {

            }

            public decimal LimiteInferior { get; set; }
            public decimal LimiteSuperior { get; set; }
            public decimal CuotaFija { get; set; }
            public decimal PorcentajeExcedente { get; set; }
        }

        public BindingList<Entities.ViewModelTablaImpuestoSobreRenta> Importar(string archivo)
        {
            if (new FileInfo(archivo).Exists)
            {
                BindingList<Entities.ViewModelTablaImpuestoSobreRenta> tablas = new BindingList<Entities.ViewModelTablaImpuestoSobreRenta>();
                var engine = new MasterDetailEngine<LayoutTablaISR, ISR>(new MasterDetailSelector(this.ExampleSelector));
                MasterDetails<LayoutTablaISR, ISR>[] ressult = engine.ReadFile(archivo);
                for (int i = 0; i < ressult.Length; i++)
                {
                    Entities.ViewModelTablaImpuestoSobreRenta tabla = new Entities.ViewModelTablaImpuestoSobreRenta();
                    tabla.Etiqueta = ressult[i].Master.Etiqueta.Replace("@", "");
                    tabla.Periodo = ressult[i].Master.Periodo;
                    foreach (ISR item in ressult[i].Details)
                    {
                        tabla.Rangos.Add(new Entities.ImpuestoSobreRenta(item.LimiteInferior, item.LimiteSuperior, item.CuotaFija, item.PorcentajeExcedente));
                    }
                    tablas.Add(tabla);
                }
                return tablas;
            }
            return null;
        }

        private RecordAction ExampleSelector(string record)
        {
            if (record[0].ToString().Contains("@"))
                return RecordAction.Master;
            else
                return RecordAction.Detail;
        }
    }
}
