﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

namespace Jaeger.Nomina.Layout
{
    public class LayoutBanamexEstadoCuenta
    {
        [FieldConverter(ConverterKind.Date, "dd/MM/yy", "es")]
        public DateTime Fecha;
    }
}
