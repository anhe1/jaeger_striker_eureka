﻿
namespace Jaeger.UI {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.textOrigen = new System.Windows.Forms.TextBox();
            this.buttonOrigen = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textDestino = new System.Windows.Forms.TextBox();
            this.buttonDestino = new System.Windows.Forms.Button();
            this.buttonProcesar = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.backProcesar = new System.ComponentModel.BackgroundWorker();
            this.textUser = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textDataBase = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textPassword = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonDataBase = new System.Windows.Forms.Button();
            this.buttonTesting = new System.Windows.Forms.Button();
            this.Ejercicio = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Carpeta origen:";
            // 
            // textOrigen
            // 
            this.textOrigen.Location = new System.Drawing.Point(97, 14);
            this.textOrigen.Name = "textOrigen";
            this.textOrigen.Size = new System.Drawing.Size(289, 20);
            this.textOrigen.TabIndex = 0;
            // 
            // buttonOrigen
            // 
            this.buttonOrigen.Location = new System.Drawing.Point(392, 13);
            this.buttonOrigen.Name = "buttonOrigen";
            this.buttonOrigen.Size = new System.Drawing.Size(75, 23);
            this.buttonOrigen.TabIndex = 1;
            this.buttonOrigen.Text = "Seleccionar";
            this.buttonOrigen.UseVisualStyleBackColor = true;
            this.buttonOrigen.Click += new System.EventHandler(this.buttonOrigen_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Carpeta destino:";
            // 
            // textDestino
            // 
            this.textDestino.Location = new System.Drawing.Point(97, 43);
            this.textDestino.Name = "textDestino";
            this.textDestino.Size = new System.Drawing.Size(289, 20);
            this.textDestino.TabIndex = 2;
            // 
            // buttonDestino
            // 
            this.buttonDestino.Location = new System.Drawing.Point(392, 42);
            this.buttonDestino.Name = "buttonDestino";
            this.buttonDestino.Size = new System.Drawing.Size(75, 23);
            this.buttonDestino.TabIndex = 3;
            this.buttonDestino.Text = "Seleccionar";
            this.buttonDestino.UseVisualStyleBackColor = true;
            this.buttonDestino.Click += new System.EventHandler(this.buttonDestino_Click);
            // 
            // buttonProcesar
            // 
            this.buttonProcesar.Location = new System.Drawing.Point(392, 75);
            this.buttonProcesar.Name = "buttonProcesar";
            this.buttonProcesar.Size = new System.Drawing.Size(75, 23);
            this.buttonProcesar.TabIndex = 4;
            this.buttonProcesar.Text = "Procesar";
            this.buttonProcesar.UseVisualStyleBackColor = true;
            this.buttonProcesar.Click += new System.EventHandler(this.buttonProcesar_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(15, 75);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(371, 23);
            this.progressBar1.TabIndex = 3;
            // 
            // backProcesar
            // 
            this.backProcesar.WorkerReportsProgress = true;
            this.backProcesar.WorkerSupportsCancellation = true;
            this.backProcesar.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backProcesar_DoWork);
            this.backProcesar.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backProcesar_ProgressChanged);
            this.backProcesar.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backProcesar_RunWorkerCompleted);
            // 
            // textUser
            // 
            this.textUser.Location = new System.Drawing.Point(97, 135);
            this.textUser.Name = "textUser";
            this.textUser.Size = new System.Drawing.Size(289, 20);
            this.textUser.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Usuario:";
            // 
            // textDataBase
            // 
            this.textDataBase.Location = new System.Drawing.Point(97, 106);
            this.textDataBase.Name = "textDataBase";
            this.textDataBase.Size = new System.Drawing.Size(213, 20);
            this.textDataBase.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Base de Datos:";
            // 
            // textPassword
            // 
            this.textPassword.Location = new System.Drawing.Point(97, 161);
            this.textPassword.Name = "textPassword";
            this.textPassword.PasswordChar = '*';
            this.textPassword.Size = new System.Drawing.Size(289, 20);
            this.textPassword.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Contraseña:";
            // 
            // buttonDataBase
            // 
            this.buttonDataBase.Location = new System.Drawing.Point(392, 104);
            this.buttonDataBase.Name = "buttonDataBase";
            this.buttonDataBase.Size = new System.Drawing.Size(75, 23);
            this.buttonDataBase.TabIndex = 6;
            this.buttonDataBase.Text = "Seleccionar";
            this.buttonDataBase.UseVisualStyleBackColor = true;
            this.buttonDataBase.Click += new System.EventHandler(this.buttonDataBase_Click);
            // 
            // buttonTesting
            // 
            this.buttonTesting.Location = new System.Drawing.Point(311, 189);
            this.buttonTesting.Name = "buttonTesting";
            this.buttonTesting.Size = new System.Drawing.Size(75, 23);
            this.buttonTesting.TabIndex = 9;
            this.buttonTesting.Text = "Conectar";
            this.buttonTesting.UseVisualStyleBackColor = true;
            this.buttonTesting.Click += new System.EventHandler(this.buttonTesting_Click);
            // 
            // Ejercicio
            // 
            this.Ejercicio.Location = new System.Drawing.Point(316, 106);
            this.Ejercicio.Maximum = new decimal(new int[] {
            2021,
            0,
            0,
            0});
            this.Ejercicio.Name = "Ejercicio";
            this.Ejercicio.Size = new System.Drawing.Size(70, 20);
            this.Ejercicio.TabIndex = 10;
            this.Ejercicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Ejercicio.Value = new decimal(new int[] {
            2021,
            0,
            0,
            0});
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 230);
            this.Controls.Add(this.Ejercicio);
            this.Controls.Add(this.buttonTesting);
            this.Controls.Add(this.buttonDataBase);
            this.Controls.Add(this.textPassword);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textUser);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textDataBase);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.buttonProcesar);
            this.Controls.Add(this.buttonDestino);
            this.Controls.Add(this.textDestino);
            this.Controls.Add(this.buttonOrigen);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textOrigen);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Test";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textOrigen;
        private System.Windows.Forms.Button buttonOrigen;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textDestino;
        private System.Windows.Forms.Button buttonDestino;
        private System.Windows.Forms.Button buttonProcesar;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.ComponentModel.BackgroundWorker backProcesar;
        private System.Windows.Forms.TextBox textUser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textDataBase;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textPassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonDataBase;
        private System.Windows.Forms.Button buttonTesting;
        private System.Windows.Forms.NumericUpDown Ejercicio;
    }
}