﻿using Jaeger.C2K.Contracts;
using Jaeger.C2K.Domain;
using Jaeger.C2K.Repositories;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Empresa.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace Jaeger.UI {
    public partial class MainForm : Form {
        protected ISqlTComprobanteRepository fb;
        private List<TComprobanteModel> comprobantes;
        public MainForm() {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            if (DateTime.Now >= new DateTime(2021, 8, 28)) {
                MessageBox.Show(this, "Demo concluido...", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
            }
        }

        private void buttonOrigen_Click(object sender, EventArgs e) {
            var _origen = new FolderBrowserDialog();
            if (_origen.ShowDialog(this) == DialogResult.OK) {
                this.textOrigen.Text = _origen.SelectedPath;
            }
        }

        private void buttonDestino_Click(object sender, EventArgs e) {
            var _destino = new FolderBrowserDialog();
            if (_destino.ShowDialog(this) == DialogResult.OK) {
                this.textDestino.Text = _destino.SelectedPath;
            }
        }

        private void buttonProcesar_Click(object sender, EventArgs e) {
            if (this.backProcesar.IsBusy == false) {
                this.buttonProcesar.Text = "Cancelar";
                this.buttonDestino.Enabled = false;
                this.buttonOrigen.Enabled = false;
                this.textDestino.Enabled = false;
                this.textOrigen.Enabled = false;
                this.backProcesar.RunWorkerAsync();
            }
            else {
                this.backProcesar.CancelAsync();
            }
        }

        private void backProcesar_DoWork(object sender, DoWorkEventArgs e) {
            var _archivos = System.IO.Directory.GetFiles(this.textOrigen.Text, "*.xml", System.IO.SearchOption.AllDirectories);
            var _contador = 0;
            var _total = _archivos.Count();
            this.progressBar1.Maximum = _total;
            foreach (var item in _archivos) {
                DateTime? _fecha = null;
                var _uuid = string.Empty;
                var _cfdi = Jaeger.CFDI.V33.Comprobante.Load(item);
                if (_cfdi != null) {
                    _fecha = _cfdi.Fecha;
                    _uuid = _cfdi.Complemento.TimbreFiscalDigital.UUID;
                }
                if (_fecha <= new DateTime(1900, 1, 1)) {
                    var _cfdi32 = Jaeger.CFDI.V32.Comprobante.Load(item);
                    if (_cfdi32 != null) {
                        _fecha = _cfdi32.fecha;
                        _uuid = _cfdi32.Complemento.TimbreFiscalDigital.UUID;
                    }
                }
                
                if (_fecha.Value.Year == this.Ejercicio.Value) {
                    bool _encontrado = false;
                    try {
                        var _seleccionado = this.comprobantes.Where(it => it.IdDocumento == _uuid).FirstOrDefault();
                        if (_seleccionado != null) {
                            _encontrado = true;
                        }
                    }
                    catch (Exception ex) {
                        this.Text = ex.Message;
                        _encontrado = false;
                    }

                    if (_encontrado == false) { 
                        if (_fecha > new DateTime(1900, 1, 1)) {
                            var _destino = System.IO.Path.Combine(this.textDestino.Text, _fecha.Value.Year.ToString(), _fecha.Value.Month.ToString());
                            if (System.IO.Directory.Exists(_destino) == false) {
                                System.IO.Directory.CreateDirectory(_destino);
                            }
                            var _destino32 = System.IO.Path.Combine(_destino, System.IO.Path.GetFileName(item));
                            System.IO.File.Copy(item, _destino32, true);
                        }
                        else {
                            var _destino = System.IO.Path.Combine(this.textDestino.Text, "desconocido");
                            var _destino32 = System.IO.Path.Combine(_destino, System.IO.Path.GetFileName(item));
                            if (System.IO.Directory.Exists(_destino) == false) {
                                System.IO.Directory.CreateDirectory(_destino);
                            }
                            System.IO.File.Copy(item, _destino32, true);
                        }
                    }
                }

                _contador++;
                this.backProcesar.ReportProgress(_contador, "Procesando: " + item);
                if (this.backProcesar.CancellationPending == true) {
                    e.Cancel = true;
                    break;
                }
            }
        }

        private void backProcesar_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            this.progressBar1.Value = e.ProgressPercentage;
            this.Text = (string)e.UserState;
        }

        private void backProcesar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.buttonProcesar.Text = "Procesar";
            this.buttonDestino.Enabled = true;
            this.buttonOrigen.Enabled = true;
            this.textDestino.Enabled = true;
            this.textOrigen.Enabled = true;
            this.progressBar1.Value = 0;
            this.Text = "Listo.";
        }

        private void buttonDataBase_Click(object sender, EventArgs e) {
            var _destino = new OpenFileDialog() { Filter = "*.fdb|*.FDB" };
            if (_destino.ShowDialog(this) == DialogResult.OK) {
                this.textDataBase.Text = _destino.FileName;
            }
        }

        private void buttonTesting_Click(object sender, EventArgs e) {
            this.fb = new SqlFbTComprobanteRepository(new DataBaseConfiguracion() { HostName = "localhost", PortNumber = 3050, Database = this.textDataBase.Text, UserID =  this.textUser.Text, Password = this.textPassword.Text });
            this.comprobantes = this.fb.GetList().ToList();
            MessageBox.Show(this.comprobantes.Count().ToString());
        }
    }
}
