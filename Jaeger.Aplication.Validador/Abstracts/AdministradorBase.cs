﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Xml.Linq;
using MiniExcelLibs.OpenXml;
using MiniExcelLibs;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.Aplication.Validador.Services;
using Jaeger.Aplication.Validador.Builder;
using Jaeger.Util.Services;
using Jaeger.SAT.Reader;
using Jaeger.SAT.Reader.Helper;

namespace Jaeger.Aplication.Validador.Abstracts {
    public abstract class AdministradorBase : IAdministrador {
        #region declaraciones
        protected internal readonly EmbeddedResources resource = new EmbeddedResources("Jaeger.Aplication.Validador");
        protected internal IDirectoryService _DirectoryService;
        public static List<string> pdf8 = new List<string>();
        public IProgress<Progreso> progreso;
        #endregion

        public AdministradorBase() {
            this.DataSource = new BindingList<IDocumentoFiscal>();
            this.DataError = new BindingList<IDocumentoFiscalError>();
            this._DirectoryService = new DirectoryService();
        }

        #region propiedades
        public virtual string RFC { get; set; }

        /// <summary>
        /// obtener o establecer configuracion
        /// </summary>
        public virtual Contracts.IConfiguration Configuracion { get; set; }

        /// <summary>
        /// obtener o establcer listado de comprobantes
        /// </summary>
        public virtual BindingList<IDocumentoFiscal> DataSource { get; set; }

        /// <summary>
        /// obtener o establecer listado de nombres de archivo con errores
        /// </summary>
        public virtual BindingList<IDocumentoFiscalError> DataError { get; set; }
        #endregion

        public bool ExportarExcel(string fileName, string fileTemplete = "") {
            // configuracion para la exportacion
            var config = new OpenXmlConfiguration() {
                IgnoreTemplateParameterMissing = false
            };
            //var lista = new List<DocumentoFiscal>();
            //foreach (var item in this.DataSource) {
            //    lista.Add(new DocumentoFiscal {
            //        ReceptorRFC = item.ReceptorRFC, 
            //        EmisorRFC = item.EmisorRFC, 
            //        Certificado = item.Certificado,
            //        CFDIRelacionado = item.CFDIRelacionado,
            //        ClaveExportacion = item.ClaveExportacion,
            //    });
            //}
            var data = new Dictionary<string, object>() {
                ["Validador"] = this.DataSource,
            };

            // si pasan un templete se carga
            if (File.Exists(fileTemplete)) {
                try {
                    MiniExcel.SaveAsByTemplate(fileName, fileTemplete, data, config);
                    return true;
                } catch (Exception ex) {
                    Console.WriteLine($"Could not save file {fileName} " + ex.Message);
                }
            }

            // probamos el templete por default
            try {
                var templatePath = this.resource.GetAsBytes("Jaeger.Aplication.Validador.Reports.Validacion.xlsx");
                MiniExcel.SaveAsByTemplate(fileName, templatePath, data, config);
                return true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            return false;
        }

        public bool DescargarTemplete(string destino) {
            // probamos el templete por default
            try {
                var templatePath = this.resource.GetAsBytes("Jaeger.Aplication.Validador.Reports.Validacion.xlsx");
                FileService.WriteFileByte(templatePath, destino);
                return true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        public static string SearchPDF(FileInfo archivo, string idDocumento) {
            var buscar = string.Empty;
            foreach (string item in Directory.GetFiles(archivo.DirectoryName, "*.pdf", SearchOption.AllDirectories)) {
                try {
                    buscar = Administrador.pdf8.Where(it => it == item).FirstOrDefault();
                    if (!string.IsNullOrEmpty(buscar)) Console.WriteLine("descartado =" + buscar);
                } catch (Exception) {
                    buscar = string.Empty;
                }
                if (string.IsNullOrEmpty(buscar)) {
                    if (PDFExtractorService.GetUUID(item).ToUpper() == idDocumento) {
                        Console.WriteLine("Conincide = " + idDocumento + " con " + item);
                        Administrador.pdf8.Add(item);
                        return item;
                    }
                }
            }
            return string.Empty;
        }

        public static byte[] NullRemover(byte[] bytesArray) {
            int i;
            byte[] bytes = new byte[bytesArray.Length];
            for (i = 0; i < bytesArray.Length - 1; i++) {
                if (bytesArray[i] == 0x00) break;
                bytes[i] = bytesArray[i];
            }

            byte[] nullLessDataByte = new byte[i];
            for (i = 0; i < nullLessDataByte.Length; i++) {
                nullLessDataByte[i] = bytes[i];
            }
            return nullLessDataByte;
        }

        public static string RemoverAddendas(string xml) {
            try {
                var xElement = XElement.Parse(xml);
                var xmlAddenda = xElement.Elements().Where(p => p.Name.LocalName == "Addenda").FirstOrDefault();
                if (!(xmlAddenda == null)) {
                    xmlAddenda.Remove();
                }
                return xElement.ToString();
            } catch (Exception ex) {
                LogErrorService.LogWrite("RemoverAddendas: " + ex.Message);
            }
            return xml;
        }

        public static void Empaquetar(List<IDocumentoFiscalError> documentoFiscalErrors) {
            var d0 = new List<IEmpaquetadoItem>();
            
            foreach (var item in documentoFiscalErrors) {
                var d3 = new FileInfo(item.PathXML);
                d0.Add(new EmpaquetadoItem { XmlB64 = ReaderHelper.FileToBase64(d3), Log = item.Resultado, FileName = d3.Name });
                
            }
        }
    }
}
