﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.Aplication.Validador.Contracts;

namespace Jaeger.Aplication.Validador.Abstracts {
    public abstract class AdministradorBackupService {
        #region declaraciones
        protected internal IComprobantesFiscalesService Comprobante;
        protected internal Contribuyentes.IContribuyenteService contribuyente;
        protected internal ComprobanteExtensions extensions;
        protected internal List<ComprobanteFiscalDetailSingleModel> Existentes;
        protected internal BindingList<IDocumentoFiscal> dataSource;
        public IProgress<Progreso> progreso;
        protected internal string _RFC;
        #endregion

        public abstract BindingList<IDocumentoFiscal> Procesar(BindingList<IDocumentoFiscal> dataSource, IProgress<Progreso> progreso);

        internal ComprobanteContribuyenteModel WithDirectorio(ComprobanteContribuyenteModel d) {
            var d1 = contribuyente.GetByRFC(d.RFC);
            if (d1 != null) {
                d.IdDirectorio = d1.IdDirectorio;
            } else {
                var d2 = contribuyente.Save(new ContribuyenteDetailModel {
                    Activo = true,
                    Nombre = d.Nombre,
                    NombreComercial = d.NombreComercial,
                    ClaveUsoCFDI = d.ClaveUsoCFDI,
                    DomicilioFiscal = d.DomicilioFiscal,
                    RFC = d.RFC,
                    RegimenFiscal = d.RegimenFiscal
                });
                d.IdDirectorio = d2.IdDirectorio;
            }
            return d;
        }
    }
}
