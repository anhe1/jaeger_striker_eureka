﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using Jaeger.Aplication.Validador.Contracts;

namespace Jaeger.Aplication.Validador.Abstracts {
    public abstract class ValidadorProvider : IValidadorComprobante {
        protected internal Dictionary<string, string> _Emisor;

        /// <summary>
        /// informacion del emisor obtenida desde el certificado del emisor del comprobante
        /// </summary>
        protected ValidadorProvider() { _Emisor = new Dictionary<string, string>(); }

        public abstract object Comprobante { get; set; }

        public abstract bool IsEsquemaValido { get; set; }

        public abstract bool IsCodificacion { get; set; }

        public abstract bool IsSelloCFDI { get; set; }

        public abstract bool IsSelloSAT { get; set; }

        public abstract bool IsVigente { get; set; }

        public abstract IComprobanteValidacionPropiedad Esquema();

        public abstract IComprobanteValidacionPropiedad Codificacion();

        public abstract List<IComprobanteValidacionPropiedad> SelloCFDI();

        public abstract IComprobanteValidacionPropiedad SelloSAT();

        public abstract IComprobanteValidacionPropiedad EstadoSAT();

        public abstract IComprobanteValidacionPropiedad FormaPago();

        public abstract IComprobanteValidacionPropiedad MetodoPago();

        public abstract IComprobanteValidacionPropiedad LugarExpedicion();

        public abstract IComprobanteValidacionPropiedad UsoCFDI();

        public abstract Dictionary<string, string> GetEmisor();

        public Dictionary<string, string> GetEmisor(string base64) {
            var d1 = new Dictionary<string, string>();
            var array = System.Text.Encoding.UTF8.GetBytes(base64);
            X509Certificate2 certificadoX509Field = new X509Certificate2(array);

            int num1 = certificadoX509Field.Subject.IndexOf(" OID.2.5.4.41=") + 14;
            if (num1 == 13) {
                num1 = certificadoX509Field.Subject.IndexOf("CN=") + 3;
            }
            int length = certificadoX509Field.Subject.IndexOf(", ", num1);
            if (length == -1) {
                length = certificadoX509Field.Subject.Length;
            }
            var _RazonSocial = certificadoX509Field.Subject.Substring(num1, length - num1).Trim();

            Regex regex = new Regex("[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9A]");
            int num = certificadoX509Field.Subject.IndexOf("OID.2.5.4.45=");
            if (num < 0) {
                num = 0;
            }
            Match match = regex.Match(certificadoX509Field.Subject, num);
            var _RFC = match.Value.ToString();
            d1.Add(_RFC, _RazonSocial);
            return d1;
        }
    }
}
