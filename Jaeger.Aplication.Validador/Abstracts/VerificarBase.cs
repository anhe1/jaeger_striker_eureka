﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using Jaeger.Aplication.Validador.Builder;
using Jaeger.Aplication.Validador.Catalogos;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.Aplication.Validador.ValueObjects;
using Jaeger.SAT.CFDI.Consulta.Interfaces;
using Jaeger.SAT.CFDI.Consulta.Services;

namespace Jaeger.Aplication.Validador.Abstracts {
    public abstract class VerificarBase : IVerificarBase {
        protected internal IStatus status = new Status();

        public virtual IVerificarBase AddConfiguration(IConfiguration configuracion) {
            this.Configuracion = configuracion;
            return this;
        }

        public virtual IVerificarBase WithRFC(string rfc) {
            this.RFC = rfc;
            return this;
        }

        public IConfiguration Configuracion { get; set; }

        public string RFC { get; set; }

        public abstract IDocumentoFiscal Execute(IDocumentoFiscal documentoFiscal);

        public static IValidacionRespuestaBuilder Create() {
            return new ValidacionRespuestaBuilder();
        }

        public static IConfiguracionBuilder Conf() {
            return new ConfiguracionBuilder();
        }

        #region estado SAT
        internal static IDocumentoFiscal GetEstado(IDocumentoFiscal documentoFiscal) {
            IStatus status = new Status();
            var estado = Create().AddPropiedad(PropiedadValidacionEnum.EstadoSAT).Build();
            var response = new List<IComprobanteValidacionPropiedad>();

            if (string.IsNullOrEmpty(documentoFiscal.IdDocumento)) {
                estado.Valor = "No existe UUID.";
                estado.IsValido = false;
                documentoFiscal.Validacion.Resultados.Add(estado);
            }

            if (string.IsNullOrEmpty(documentoFiscal.EmisorRFC)) {
                Services.LogErrorService.LogWrite("No existe RFC del emisor. " + documentoFiscal.PathXML);
                Console.WriteLine("No hay rfc del emisor");
            }

            var consulta = status.Execute(
                SAT.CFDI.Consulta.Request.Create()
                .WithEmisorRFC(documentoFiscal.EmisorRFC)
                .WithReceptorRFC(documentoFiscal.ReceptorRFC)
                .WithTotal(documentoFiscal.Total)
                .WithFolioFiscal(documentoFiscal.IdDocumento)
                .Build());

            if (consulta != null) {
                // si contiene S Comprobante obtenido satisfactoriamente entonces es vigente
                if (consulta.CodigoEstatus.ToUpper().StartsWith("S")) {
                    estado.IsValido = true;
                    estado.Valor = "Vigente";
                } else if (consulta.CodigoEstatus.Contains("602")) { // mensaje de rechazo
                    estado.Valor = "No encontrado";
                    estado.IsValido = false;
                } else if (consulta.CodigoEstatus.Contains("601")) { // la expresion impresa no es valida
                    estado.Valor = "601";
                    estado.IsValido = false;
                }

                documentoFiscal.Validacion.Estado = estado.Valor;
                documentoFiscal.Estado = estado.Valor;
                response.Add(estado);

                //- Código=200
                // Este código de respuesta se presentará cuando la validación del RFC Emisor del CFDI no se encuentre 
                // dentro de la lista de Empresa que Factura Operaciones Simuladas (EFOS).

                //- Código=201
                // Este código de respuesta se presentará cuando la validación del RFC Emisor del CFDI y ninguno de los RFC 
                // A cuenta de terceros se encuentre dentro de la lista de Empresa que Factura Operaciones Simuladas (EFOS).

                //if (Configuracion.ValidarArticulo69B) {
                    if (consulta.ValidacionEFOS == "200" | consulta.ValidacionEFOS == "201") {
                        documentoFiscal.Situacion = "Sin reporte";
                        response.Add(Create().AddPropiedad(PropiedadValidacionEnum.EFOS).Build());
                    } else {
                        documentoFiscal.Situacion = "69b/" + consulta.ValidacionEFOS;
                    }
                    documentoFiscal.Validacion.Resultados.AddRange(response);
                //}
            }
            return documentoFiscal;
        }

        public List<IComprobanteValidacionPropiedad> GetEstadoSAT(object cfd) {
            if (cfd.GetType() == typeof(SAT.CFDI.V32.Comprobante)) {
                return GetEstadoSAT(cfd as SAT.CFDI.V32.Comprobante);
            } else if (cfd.GetType() == typeof(SAT.CFDI.V33.Comprobante)) {
                return GetEstadoSAT(cfd as SAT.CFDI.V33.Comprobante);
            } else if (cfd.GetType() == typeof(SAT.CFDI.V40.Comprobante)) {
                return GetEstadoSAT(cfd as SAT.CFDI.V40.Comprobante);
            }
            return new List<IComprobanteValidacionPropiedad>();
        }

        public List<IComprobanteValidacionPropiedad> GetEstadoSAT(SAT.CFDI.V32.Comprobante cfd) {
            return GetAcuse(cfd.Emisor.rfc, cfd.Receptor.rfc, cfd.total, cfd.Complemento.TimbreFiscalDigital.UUID);
        }

        public List<IComprobanteValidacionPropiedad> GetEstadoSAT(SAT.CFDI.V33.Comprobante cfd) {
            return GetAcuse(cfd.Emisor.Rfc, cfd.Receptor.Rfc, cfd.Total, cfd.Complemento.TimbreFiscalDigital.UUID);
        }

        public List<IComprobanteValidacionPropiedad> GetEstadoSAT(SAT.CFDI.V40.Comprobante cfd) {
            return GetAcuse(cfd.Emisor.Rfc, cfd.Receptor.Rfc, cfd.Total, cfd.Complemento.TimbreFiscalDigital.UUID);
        }

        private List<IComprobanteValidacionPropiedad> GetAcuse(string rfcEmisor, string rfcReceptor, decimal total, string idDocumento) {
            if (string.IsNullOrEmpty(idDocumento)) {
                var estado1 = Create().AddPropiedad(PropiedadValidacionEnum.EstadoSAT).Build();
                estado1.Valor = "No existe UUID.";
                estado1.IsValido = false;
                return new List<IComprobanteValidacionPropiedad> { estado1 };
            }

            var response = new List<IComprobanteValidacionPropiedad>();
            var estado = Create().AddPropiedad(PropiedadValidacionEnum.EstadoSAT).Build();
            
            var query = status.Execute(
                SAT.CFDI.Consulta.Request.Create()
                .WithEmisorRFC(rfcEmisor)
                .WithReceptorRFC(rfcReceptor)
                .WithTotal(total)
                .WithFolioFiscal(idDocumento)
                .Build());

            if (query != null) {
                // si contiene S Comprobante obtenido satisfactoriamente entonces es vigente
                if (query.CodigoEstatus.ToUpper().StartsWith("S")) {
                    estado.IsValido = true;
                    estado.Valor = query.Estado;
                } else if (query.CodigoEstatus.Contains("602")) {
                    estado.Valor = "No encontrado";
                    estado.IsValido = false;
                } else if (query.CodigoEstatus.Contains("601")) {
                    estado.Valor = "601";
                    estado.IsValido = false;
                }
                response.Add(estado);

                if (query.ValidacionEFOS == "200" | query.ValidacionEFOS == "201") {
                    response.Add(Create().AddPropiedad(PropiedadValidacionEnum.EFOS).Build());
                } else {
                    var d0 = Create().AddPropiedad(PropiedadValidacionEnum.EFOS).Build();
                    d0.IsValido = false;
                    d0.Valor = "69b/" + query.ValidacionEFOS;
                }
            }

            return response;
        }
        #endregion

        public static Dictionary<string, string> GetEmisor(string base64) {
            var d1 = new Dictionary<string, string>();
            var array = Encoding.UTF8.GetBytes(base64);
            X509Certificate2 certificadoX509Field = new X509Certificate2(array);

            int num1 = certificadoX509Field.Subject.IndexOf(" OID.2.5.4.41=") + 14;
            if (num1 == 13) {
                num1 = certificadoX509Field.Subject.IndexOf("CN=") + 3;
            }
            int length = certificadoX509Field.Subject.IndexOf(", ", num1);
            if (length == -1) {
                length = certificadoX509Field.Subject.Length;
            }
            var _RazonSocial = certificadoX509Field.Subject.Substring(num1, length - num1).Trim();

            Regex regex = new Regex("[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9A]");
            int num = certificadoX509Field.Subject.IndexOf("OID.2.5.4.45=");
            if (num < 0) {
                num = 0;
            }
            Match match = regex.Match(certificadoX509Field.Subject, num);
            var rfc = match.Value.ToString();
            d1.Add(rfc, _RazonSocial);
            return d1;
        }

        public static List<IComprobanteValidacionPropiedad> ClavesConcepto(IDocumentoFiscal documentosFiscales) {
            var response = new List<IComprobanteValidacionPropiedad>();
            var doc = documentosFiscales;
            var d0 = ProductosYServicios.Validar(doc.Conceptos);
            if (d0.Count > 0) {
                response.AddRange(d0);
            } else {
                response.Add(Create().AddPropiedad(PropiedadValidacionEnum.ProdYServ).Build());
                response.Add(Create().AddPropiedad(PropiedadValidacionEnum.Unidad).Build());
            }

            return response;
        }

        /// <summary>
        /// obtener información del certificado del emisor del comprobante
        /// </summary>
        /// <param name="base64">certificado en base 64</param>
        /// <param name="fechaEmision">fecha de emision del comprobante</param>
        /// <returns>Propiedad de validacion</returns>
        public static IComprobanteValidacionPropiedad GetCertificado(string base64, DateTime fechaEmision) {
            var certificado = new Crypto.Services.Certificate();
            certificado.CargarB64(base64);
            
            if (fechaEmision >= DateTime.Parse(certificado.ValidoDesde) & fechaEmision <= DateTime.Parse(certificado.ValidoHasta)) {
                return new ComprobanteValidacionPropiedad {
                    IsValido = true,
                    Nombre = "CFDI con CSD",
                    Valor = string.Format("Vigente al crearse {0} al {1}", certificado.ValidoDesde, certificado.ValidoHasta)
                };
            }

            var response = new ComprobanteValidacionPropiedad {
                IsValido = false,
                Nombre = "CFDI con CSD",
                Valor = string.Format("Al crearse {0} al {1}", certificado.ValidoDesde, certificado.ValidoHasta)
            };
            return response;
        }
    }
}
