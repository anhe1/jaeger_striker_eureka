﻿using System;
using System.Collections.Generic;

namespace Jaeger.Aplication.Validador.Catalogos {
    public class Response : IResponse {
        public Response() {
            this.Files = new List<string>();
        }
        public Version Version { get; set; }

        public List<string> Files { get; set; }
    }
}
