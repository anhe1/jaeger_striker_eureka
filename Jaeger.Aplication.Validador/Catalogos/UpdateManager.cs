﻿using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Aplication.Validador.Catalogos {
    public partial class UpdateManager {
        #region Variables
        private readonly string _UrlBase = @"https://xexx010101000.s3.us-east-1.amazonaws.com/validador_update.txt";
        /// <summary>
        /// The URL that can be used to check for updates
        /// </summary>
        private string _updateUrl;

        /// <summary>
        /// The version of the application
        /// </summary>
        private Version _applicationVersion;
        #endregion

        public UpdateManager() {
            this.ApplicationVersion = new Version(1, 0, 0);
            this.UpdateUrl = @"https://xexx010101000.s3.us-east-1.amazonaws.com/validador_update.txt";
        }

        #region propiedades
        /// <summary>
        /// Gets or sets the update URL
        /// </summary>
        public string UpdateUrl {
            get {
                return _updateUrl;
            }
            set {
                _updateUrl = value ?? throw new ArgumentNullException(nameof(value));
            }
        }

        /// <summary>
        /// Gets or sets the local version of the application
        /// </summary>
        public Version ApplicationVersion {
            get {
                return _applicationVersion;
            }
            set {
                _applicationVersion = value ?? throw new ArgumentNullException(nameof(value));
            }
        }

        public IResponse Data { get; set; }
        #endregion

        #region builder
        public UpdateManager WithVersion(Version version) {
            this._applicationVersion = version;
            return this;
        }

        public UpdateManager WithUpdateUrl(string updateUrl) {
            this._updateUrl = updateUrl;
            return this;
        }
        #endregion

        #region metodos publicos
        public bool CheckForUpdate() {
            if (UpdateUrl == null) throw new ArgumentNullException(nameof(UpdateUrl));
            if (UpdateUrl.Length == 0) throw new ArgumentException(nameof(UpdateUrl));

            string data = new WebClient().DownloadString(UpdateUrl);
            if (!string.IsNullOrEmpty(data)) {
                this.Data = this.Deserialize(data);
                if (this.Data != null) {
                    int result = this.Data.Version.CompareTo(this.ApplicationVersion);
                    return result > 0;
                }
            }
            return false;
        }
        #endregion

        private IResponse Deserialize(string data) {
            if (!string.IsNullOrEmpty(data)) {
                string[] stringSeparators = new string[] { "\r\n" };
                string[] lines = data.Split(stringSeparators, StringSplitOptions.None);
                var d0 = new Response();
                var d1 = new Version(lines[0]);
                d0.Version = d1;
                foreach (var item in lines) {
                    if (item.ToLower().Contains("zip")) {
                        d0.Files.Add(item);
                    }
                }
                return d0;
            }
            return null;
        }
    }
}
