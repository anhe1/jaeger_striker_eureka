﻿using System;

namespace Jaeger.Aplication.Validador.Catalogos {
    public class ByteArgs : EventArgs {
        private int _downloaded;
        private int _total;

        public int downloaded {
            get {
                return _downloaded;
            }
            set {
                _downloaded = value;
            }
        }

        public int total {
            get {
                return _total;
            }
            set {
                _total = value;
            }
        }
    }
}
