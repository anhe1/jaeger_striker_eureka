﻿using System;

namespace Jaeger.Aplication.Validador.Catalogos {
    public static class Actualizar {
        public static void Procesar(IResponse response) {
            if (response != null) {
                var d0 = new WebData();
                foreach (var item in response.Files) {
                    WebData.DownloadFromWeb(@"https://xexx010101000.s3.us-east-1.amazonaws.com/", item, @"C:\Jaeger\Jaeger.Temporal\");
                    Procesar(@"C:\Jaeger\Jaeger.Temporal\", item);
                }
                System.IO.File.WriteAllText(@"C:\Jaeger\Jaeger.Catalogos\version.txt", response.Version.ToString());
            }
        }

        public static void Procesar(string folder, string file) {
            if (file.ToLower().Contains("catalogoarticulo69b")) {
                Articulo69B.Catalogo.Load(folder + "\\" + file);
                Articulo69B.Catalogo.Save();
            }
        }

        public static Version GetVersion() {
            var fileName = @"C:\Jaeger\Jaeger.Catalogos\Version.txt";
            if (System.IO.File.Exists(fileName)) {
                var ver = System.IO.File.ReadAllText(fileName);
                return new Version(ver);
            } else {
                System.IO.File.WriteAllText(fileName, "1.0.0");
                return new Version(1, 0, 0);
            }
        }
    }
}
