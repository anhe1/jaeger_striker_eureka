﻿using Jaeger.Aplication.Validador.Abstracts;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.ValueObjects;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.Aplication.Validador.Catalogos {
    public static class UsoCFDIService {
        public static IUsoCFDICatalogo Catalogo = new UsoCFDICatalogo();

        static UsoCFDIService() {
            Catalogo.Load();
        }

        public static IComprobanteValidacionPropiedad Validar(string usoCFDI) {
            var response = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.UsoCFDI).Build();
            var clave = Catalogo.Search(usoCFDI);
            if (clave != null) {
                response.Valor = string.Format("Correcto ({0} {1})", clave.Clave, clave.Descripcion);
                response.IsValido = true;
                return response;
            }
            response.IsValido = false;
            response.Valor = "No disponible";
            return response;
        }
    }
}
