﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Aplication.Validador.Catalogos {

    public static class CertificadoHelper {
        public static Certificate GetInformation(byte[] array) {
            if (array == null)
                return null;
            X509Certificate2 x509 = new X509Certificate2(array);
            Certificate response = new Certificate() {
                BeginDateExpiration = x509.NotBefore,
                EndDateExpiration = x509.NotAfter,
                Serial = GetSerie(x509.SerialNumber),
                CerB64 = Convert.ToBase64String(array)
            };
            return response;
        }

        public static Dictionary<string, string> GetEmisor(string base64) {
            var data = new Dictionary<string, string>();
            var array = System.Text.Encoding.UTF8.GetBytes(base64);
            X509Certificate2 certificadoX509 = new X509Certificate2(array);

            int posicion = certificadoX509.Subject.IndexOf(" OID.2.5.4.41=") + 14;
            if (posicion == 13) {
                posicion = certificadoX509.Subject.IndexOf("CN=") + 3;
            }
            int length = certificadoX509.Subject.IndexOf(", ", posicion);
            if (length == -1) {
                length = certificadoX509.Subject.Length;
            }
            var razonSocial = certificadoX509.Subject.Substring(posicion, length - posicion).Trim();

            Regex regex = new Regex("[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9A]");
            int oid = certificadoX509.Subject.IndexOf("OID.2.5.4.45=");
            if (oid < 0) {
                oid = 0;
            }
            Match match = regex.Match(certificadoX509.Subject, oid);
            var rfc = match.Value.ToString();

            data.Add(rfc, razonSocial);
            return data;
        }

        public static string GetSerie(string serialAscci) {
            string serie = "";
            for (int i = 0; i <= serialAscci.Length - 1; i += 2) {
                string str1 = serialAscci.Substring(i, 2);
                serie = string.Concat(serie, char.ConvertFromUtf32(Convert.ToInt32(str1, 16)));
            }
            return serie;
        }
    }


}
