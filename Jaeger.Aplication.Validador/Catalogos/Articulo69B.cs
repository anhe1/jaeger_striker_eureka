﻿using Jaeger.Aplication.Validador.Abstracts;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.ValueObjects;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.Aplication.Validador.Catalogos {
    public static class Articulo69B {
        public static IArticulo69BCatalogo Catalogo;

        static Articulo69B() {
            Catalogo = new Articulo69BCatalogo();
            Catalogo.Load();
        }

        public static IComprobanteValidacionPropiedad Validar(string rfc) {
            var response = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.EFOS).Build();
            var clave = Catalogo.Search(rfc);
            if (clave != null) {
                response.Valor = clave.Situacion;
                response.IsValido = false;
                return response;
            }
            return null;
        }

        public static void Actualizar(string fileName) {
            if (System.IO.File.Exists(fileName)) {

            }
        }
    }
}
