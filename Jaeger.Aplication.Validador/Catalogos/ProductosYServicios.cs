﻿using System.Collections.Generic;
using Jaeger.Aplication.Validador.Abstracts;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.Services;
using Jaeger.Aplication.Validador.ValueObjects;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.SAT.Reader.CFD.Interfaces;

namespace Jaeger.Aplication.Validador.Catalogos {
    public static class ProductosYServicios {
        public static IProdServsCatalogo Catalogo;
        public static IUnidadesCatalogo Unidades;

        static ProductosYServicios() {
            Catalogo = new ProdServsCatalogo();
            Catalogo.Load();
            Unidades = new UnidadesCatalogo();
            Unidades.Load();
        }

        public static List<IComprobanteValidacionPropiedad> Validar(List<IDocumentoFiscalConcepto> conceptos) {
            var d0 = new List<IComprobanteValidacionPropiedad>();
            foreach (var item in conceptos) {
                var clave = Catalogo.Search(item.ClaveProdServ);
                var unidad = Unidades.Search(item.ClaveUnidad);

                if (clave == null) {
                    var response = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.ProdYServ).Build();
                    response.Valor = string.Format("[{0}] clave de producto no encontrada.", item.ClaveProdServ);
                    response.IsValido = false;
                    d0.Add(response);
                }

                if (unidad == null) {
                    var response = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.Unidad).Build();
                    response.Valor = string.Format("[{0}] clave de unidad.", item.ClaveUnidad);
                    response.IsValido = false;
                    d0.Add(response);
                }
            }
            return d0;
        }
    }
}
