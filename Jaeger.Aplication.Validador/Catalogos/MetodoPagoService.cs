﻿using System;
using System.Collections.Generic;
using Jaeger.Aplication.Validador.Abstracts;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.ValueObjects;

namespace Jaeger.Aplication.Validador.Services {
    public static partial class MetodoPagoService {
        public static Dictionary<string, string> Catalogo = new Dictionary<string, string>();

        static MetodoPagoService() {
            Catalogo.Add("PUE", "Pago en una sola exhibición");
            Catalogo.Add("PPD", "Pago en parcialidades o diferido");
            Catalogo.Add("PIP", "");
        }

        public static IComprobanteValidacionPropiedad Validar(this object cfd) {
            if (cfd.GetType() == typeof(SAT.CFDI.V32.Comprobante)) {
                return MetodoPagoService.Validar(cfd as SAT.CFDI.V32.Comprobante);
            } else if (cfd.GetType() == typeof(SAT.CFDI.V33.Comprobante)) {
                return MetodoPagoService.Validar(cfd as SAT.CFDI.V33.Comprobante);
            } else if (cfd.GetType() == typeof(SAT.CFDI.V40.Comprobante)) {
                return MetodoPagoService.Validar(cfd as SAT.CFDI.V40.Comprobante);
            }
            
            return VerificarBase.Create()
                .AddPropiedad(PropiedadValidacionEnum.MetodoPago)
                .WithValue("No se reconoce el tipo de comprobante")
                .IsValid(false).Build();
        }

        public static IComprobanteValidacionPropiedad Validar(this SAT.CFDI.V32.Comprobante cfd) {
            var _response = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.MetodoPago).Build();
            _response.Valor = cfd.metodoDePago;
            return _response;
        }

        public static IComprobanteValidacionPropiedad Validar(this SAT.CFDI.V33.Comprobante cfd) {
            var _response = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.MetodoPago).Build();
            if (cfd.TipoDeComprobante != "P") {
                if (cfd.Complemento != null) {
                    if (cfd.Complemento.TimbreFiscalDigital != null) {
                        if (cfd.Complemento.TimbreFiscalDigital.FechaTimbrado >= new DateTime(2017, 1, 1)) {
                            if (cfd.MetodoPagoSpecified) {
                                if (MetodoPagoService.Catalogo.ContainsKey(cfd.MetodoPago)) {
                                    _response.IsValido = true;
                                }
                            }
                        }
                    }
                }
            }
            _response.Valor = cfd.MetodoPago;
            return _response;
        }

        public static IComprobanteValidacionPropiedad Validar(this SAT.CFDI.V40.Comprobante cfd) {
            var response = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.MetodoPago).Build();
            response.Valor = cfd.MetodoPago;
            return response;
        }
    }
}
