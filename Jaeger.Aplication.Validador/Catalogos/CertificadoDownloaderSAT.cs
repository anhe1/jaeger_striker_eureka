﻿using System;
using System.Threading;
using Jaeger.Catalogos.Entities;
using Jaeger.Util.Services;

namespace Jaeger.Aplication.Validador.Catalogos {
    public class CertificadoDownloaderSAT {
        public static readonly string HttpSAT = @"https://rdc.sat.gob.mx/rccf";

        public CertificadoDownloaderSAT() {

        }

        public bool ValidaNumeroSerie(string numeroDeSerie) {
            return ValidaSerial(numeroDeSerie);
        }

        public Certificate Descargar(string numeroDeSerie) {
            return Buscar(numeroDeSerie);
        }

        public string DescargarArchivo(string numeroDeSerie) {
            var cer = Buscar(numeroDeSerie);
            if (cer != null)
                return cer.CerB64;
            return string.Empty;
        }

        #region metodos publicos
        private static bool ValidaSerial(string serial) {
            // proposito: validar serial de certificado
            if (string.IsNullOrEmpty(serial))
                return false;
            if (!serial.StartsWith("0"))
                return false;
            if (serial.Replace("0", "").Length == 0)
                return false;
            if (serial.Length < 20)
                return false;
            return true;
        }

        public static string GetPathHttpsSAT(string oPathHttp, string oSerial) {
            string sParte1 = oSerial.Substring(0, 6);
            string sParte2 = oSerial.Substring(6, 6);
            string sParte3 = oSerial.Substring(12, 2);
            string sParte4 = oSerial.Substring(14, 2);
            string sParte5 = oSerial.Substring(16, 2);
            string strUrlReturn = "{0}/{1}/{2}/{3}/{4}/{5}/{6}.cer";
            return string.Format(strUrlReturn, oPathHttp, sParte1, sParte2, sParte3, sParte4, sParte5, oSerial);
        }

        public static Certificate Buscar(string oSerial) {
            if (!ValidaSerial(oSerial)) {
                Console.WriteLine("Numero de serie no valido");
                return null;
            }

            byte[] fileByZip = null;
            string pathHttpSat = GetPathHttpsSAT(HttpSAT, oSerial);
            int intento = 0;

            do {
                intento = intento + 1;
                if (fileByZip != null)
                    continue;
                else {
                    fileByZip = FileService.DownloadFile(pathHttpSat);
                    if (fileByZip != null)
                        continue;
                }
                Thread.Sleep(200 * intento);
            }
            while (fileByZip == null && intento < 3);

            var oCertificate = CertificadoHelper.GetInformation(fileByZip);
            Console.WriteLine("Despues de intentos");
            return oCertificate;
        }
        #endregion
    }
}
