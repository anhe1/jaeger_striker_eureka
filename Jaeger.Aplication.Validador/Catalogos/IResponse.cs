﻿using System;
using System.Collections.Generic;

namespace Jaeger.Aplication.Validador.Catalogos {
    public interface IResponse {
        Version Version { get; set; }
        List<string> Files { get; set; }
    }
}
