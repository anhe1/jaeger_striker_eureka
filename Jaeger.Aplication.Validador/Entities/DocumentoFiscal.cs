﻿using Jaeger.Aplication.Validador.Contracts;

namespace Jaeger.Aplication.Validador.Entities {
    public class DocumentoFiscal : SAT.Reader.CFD.DocumentoFiscal, Validador.Contracts.IDocumentoFiscal, SAT.Reader.CFD.Interfaces.IDocumentoFiscal {
        public IComprobanteValidacionModel Validacion { get; set; }
    }
}
