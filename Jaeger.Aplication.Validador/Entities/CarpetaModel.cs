﻿using Newtonsoft.Json;

namespace Jaeger.Aplication.Validador.Entities {
    [JsonObject("item")]
    public partial class CarpetaModel : Domain.Base.Abstractions.BasePropertyChangeImplementation, Contracts.ICarpetaModel {
        private string carpetaField;

        public CarpetaModel() {
        }

        public CarpetaModel(string carpeta) {
            this.Carpeta = carpeta;
        }

        [JsonProperty("path")]
        public string Carpeta {
            get {
                return this.carpetaField;
            }
            set {
                this.carpetaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public string Accion {
            get {
                return "Quitar";
            }
        }
    }
}
