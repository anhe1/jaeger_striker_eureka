﻿using Newtonsoft.Json;
using Jaeger.Aplication.Validador.Abstracts;

namespace Jaeger.Aplication.Validador.Entities {
    public class EmpaquetadoItem : IEmpaquetadoItem {
        private readonly JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };

        public EmpaquetadoItem() {

        }

        public string FileName { get; set; }
        
        public string XmlB64 { get; set; }
        
        public string PdfB64 { get; set; }
        
        public string Log { get; set; }

        public string Json() {
            Formatting objFormat = 0;
            return JsonConvert.SerializeObject(this, objFormat, conf);
        }

        public static EmpaquetadoItem Json(string inputJson) {
            return JsonConvert.DeserializeObject<EmpaquetadoItem>(inputJson);
        }
    }
}
