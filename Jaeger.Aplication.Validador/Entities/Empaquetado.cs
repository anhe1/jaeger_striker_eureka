﻿using Newtonsoft.Json;
using Jaeger.Aplication.Validador.Abstracts;

namespace Jaeger.Aplication.Validador.Entities {
    public class Empaquetado : IEmpaquetado {
        private readonly JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };

        public Empaquetado() {
            this.Fecha = System.DateTime.Now;
            this.Items = new System.Collections.Generic.List<EmpaquetadoItem>();
        }

        public System.DateTime Fecha { get; set; }

        public System.Collections.Generic.List<EmpaquetadoItem> Items { get; set; }

        public string Json() {
            Formatting objFormat = 0;
            return JsonConvert.SerializeObject(this, objFormat, conf);
        }

        public static Empaquetado Json(string inputJson) {
            return JsonConvert.DeserializeObject<Empaquetado>(inputJson);
        }
    }
}
