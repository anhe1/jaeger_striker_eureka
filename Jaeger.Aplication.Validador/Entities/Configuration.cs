﻿using Jaeger.Aplication.Validador.Contracts;

namespace Jaeger.Aplication.Validador.Entities {
    public class Configuration : IConfiguration {
        public Configuration() {
            this.CheckSchema = true;
            this.CheckUTF8 = true;
            this.FormaPago = true;
            this.SoloEmisorReceptor = true;
        }

        #region veficacion
        /// <summary>
        /// (0) verificar esquema
        /// </summary>
        public bool CheckSchema { get; set; }

        /// <summary>
        /// (1) verificar codificacion UTF8
        /// </summary>
        public bool CheckUTF8 { get; set; }

        /// <summary>
        /// (2) clave del uso del CFDI
        /// </summary>
        public bool CheckUsoCFDI { get; set; }

        /// <summary>
        /// (3) forma de pago
        /// </summary>
        public bool FormaPago { get; set; }

        /// <summary>
        /// (4) metodod de pago
        /// </summary>
        public bool MetodoPago { get; set; }

        /// <summary>
        /// (5)
        /// </summary>
        public bool Check1 { get; set; }

        /// <summary>
        /// (6) claves de concepto
        /// </summary>
        public bool ClavesConcepto { get; set; }

        /// <summary>
        /// (7)
        /// </summary>
        public bool ClavesUnidad { get; set; }

        /// <summary>
        /// (8) verificar lugar de expedicion para versiones 40
        /// </summary>
        public bool LugarExpedicionCP { get; set; }

        /// <summary>
        /// (9) obtener o establecer lugar de expedicion para versiones 3.2
        /// </summary>
        public bool LugarExpedicion32 { get; set; }
        #endregion

        #region SAT
        /// <summary>
        /// (10) verificar sellod CFDI
        /// </summary>
        public bool CheckSelloCFDI { get; set; }

        /// <summary>
        /// (11) verificar sello del timbre fiscal
        /// </summary>
        public bool CheckSelloTFD { get; set; }

        /// <summary>
        /// (12)
        /// </summary>
        public bool EstadoSAT { get; set; }

        /// <summary>
        /// (13)
        /// </summary>
        public bool Articulo69B { get; set; }
        #endregion

        #region opciones
        /// <summary>
        /// (14)
        /// </summary>
        public bool ModoParalelo { get; set; }

        /// <summary>
        /// (15) renombrar archivos de origen
        /// </summary>
        public bool RenameFiles { get; set; }

        /// <summary>
        /// (16) obtener o establecer si debe registrarse los documentos procesados
        /// </summary>
        public bool Registrar { get; set; }

        /// <summary>
        /// (17)
        /// </summary>
        public bool RegistrarConceptos { get; set; }

        public bool CrearCopiaS3 { get; set; }

        /// <summary>
        /// (18) remover addendas
        /// </summary>
        public bool RemoverAddenda { get; set; }
        /// <summary>
        /// (19) solo archivos validos
        /// </summary>
        public bool SoloValido { get; set; }

        /// <summary>
        /// (20)
        /// </summary>
        public bool DescargaCertificado { get; set; }

        /// <summary>
        /// (21) obtener informacion del emisor desde el certificado
        /// </summary>
        public bool GetEmisor { get; set; }

        /// <summary>
        /// (22) buscar pdf
        /// </summary>
        public bool SearchPDF { get; set; }

        /// <summary>
        /// (23) solo comprobantes verificados
        /// </summary>
        public bool OnlyVerified { get; set; }

        /// <summary>
        /// solo aceptar comprobantes emitidos o recibidos de la empresa registrada
        /// </summary>
        public bool SoloEmisorReceptor { get; set; }
        #endregion

        public string DomicilioFiscal { get; set; }

        /// <summary>
        /// expresion regular para validar lugar de expedicion del comprobante version 3.3
        /// </summary>
        public string ExpresionRex { get; set; }
    }
}
