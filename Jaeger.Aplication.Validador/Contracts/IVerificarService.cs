﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Jaeger.Aplication.Validador.Abstracts;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.Entities;

namespace Jaeger.Aplication.Validador.Services {
    public interface IVerificarService : IVerificarBase {
        Task ProcesarParallelAsync(List<IDocumentoFiscal> documentosFiscales, IProgress<Progreso> progress);
    }
}
