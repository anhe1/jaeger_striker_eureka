﻿using System.Collections.Generic;

namespace Jaeger.Aplication.Validador.Contracts {
    public interface ICarpetaRecienteService {
        List<ICarpetaModel> Items { get; set; }

        void Add(ICarpetaModel newItem);

        void Add(string carpeta);

        void Delete(int index);

        void Load();
        
        void Save();
    }
}