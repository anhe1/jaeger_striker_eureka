﻿namespace Jaeger.Aplication.Validador.Contracts {
    public interface IConfiguration {
        #region veficacion
        /// <summary>
        /// (0) verificar esquema
        /// </summary>
        bool CheckSchema { get; set; }

        /// <summary>
        /// (1) verificar codificacion UTF8
        /// </summary>
        bool CheckUTF8 { get; set; }

        /// <summary>
        /// (2) clave del uso del CFDI
        /// </summary>
        bool CheckUsoCFDI { get; set; }

        /// <summary>
        /// (3) forma de pago
        /// </summary>
        bool FormaPago { get; set; }

        /// <summary>
        /// (4) metodod de pago
        /// </summary>
        bool MetodoPago { get; set; }

        /// <summary>
        /// (5)
        /// </summary>
        bool Check1 { get; set; }

        /// <summary>
        /// (6) claves de concepto
        /// </summary>
        bool ClavesConcepto { get; set; }

        /// <summary>
        /// (7)
        /// </summary>
        bool ClavesUnidad { get; set; }

        /// <summary>
        /// (8) verificar lugar de expedicion para versiones 40
        /// </summary>
        bool LugarExpedicionCP { get; set; }

        /// <summary>
        /// (9) obtener o establecer lugar de expedicion para versiones 3.2
        /// </summary>
        bool LugarExpedicion32 { get; set; }
        #endregion

        #region SAT
        /// <summary>
        /// (10) verificar sellod CFDI
        /// </summary>
        bool CheckSelloCFDI { get; set; }

        /// <summary>
        /// (11) verificar sello del timbre fiscal
        /// </summary>
        bool CheckSelloTFD { get; set; }

        /// <summary>
        /// (12)
        /// </summary>
        bool EstadoSAT { get; set; }

        /// <summary>
        /// (13)
        /// </summary>
        bool Articulo69B { get; set; }
        #endregion

        #region opciones
        /// <summary>
        /// (14)
        /// </summary>
        bool ModoParalelo { get; set; }

        /// <summary>
        /// (15) renombrar archivos de origen
        /// </summary>
        bool RenameFiles { get; set; }

        /// <summary>
        /// (16) obtener o establecer si debe registrarse los documentos procesados
        /// </summary>
        bool Registrar { get; set; }

        /// <summary>
        /// (17)
        /// </summary>
        bool RegistrarConceptos { get; set; }

        bool CrearCopiaS3 { get; set; }

        /// <summary>
        /// (18) remover addendas
        /// </summary>
        bool RemoverAddenda { get; set; }
        /// <summary>
        /// (19) solo archivos validos
        /// </summary>
        bool SoloValido { get; set; }

        /// <summary>
        /// (20)
        /// </summary>
        bool DescargaCertificado { get; set; }

        /// <summary>
        /// (21) obtener informacion del emisor desde el certificado
        /// </summary>
        bool GetEmisor { get; set; }

        /// <summary>
        /// (22) buscar pdf
        /// </summary>
        bool SearchPDF { get; set; }

        /// <summary>
        /// (23) solo comprobantes verificados
        /// </summary>
        bool OnlyVerified { get; set; }

        /// <summary>
        /// solo aceptar comprobantes emitidos o recibidos de la empresa registrada
        /// </summary>
        bool SoloEmisorReceptor { get; set; }
        #endregion

        string DomicilioFiscal { get; set; }

        /// <summary>
        /// expresion regular para validar lugar de expedicion del comprobante version 3.3
        /// </summary>
        string ExpresionRex { get; set; }
    }
}