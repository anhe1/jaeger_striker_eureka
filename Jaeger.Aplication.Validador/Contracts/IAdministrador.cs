﻿using System.ComponentModel;
using Jaeger.SAT.Reader;

namespace Jaeger.Aplication.Validador.Contracts {
    public interface IAdministrador {
        BindingList<IDocumentoFiscal> DataSource { get; set; }

        BindingList<IDocumentoFiscalError> DataError { get; set; }
    }
}