﻿using Jaeger.Aplication.Validador.Entities;

namespace Jaeger.Aplication.Validador.Abstracts {
    public interface IEmpaquetado {
        System.DateTime Fecha { get; set; }

        System.Collections.Generic.List<EmpaquetadoItem> Items { get; set; }

        string Json();
    }
}
