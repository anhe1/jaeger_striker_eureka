﻿using Jaeger.Aplication.Validador.ValueObjects;

namespace Jaeger.Aplication.Validador.Contracts {
    public interface IComprobanteValidacionPropiedad {
        /// <summary>
        /// obtener o establecer el tipo de propiedad
        /// </summary>
        PropiedadTipoEnum Tipo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el codigo de la validacion
        /// </summary>
        string Codigo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el nombre de la propieddad
        /// </summary>
        string Nombre {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el valor de la propieddad
        /// </summary>
        string Valor {
            get; set;
        }

        /// <summary>
        /// obtener o establecer si la propiedad asociada es valida
        /// </summary>
        bool IsValido {
            get; set;
        }
    }
}
