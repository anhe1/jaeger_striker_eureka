﻿using System.Collections.Generic;

namespace Jaeger.Aplication.Validador.Contracts {
    public interface IValidadorComprobante {
        object Comprobante { get; set; }

        bool IsCodificacion { get; set; }

        bool IsEsquemaValido { get; set; }

        bool IsSelloCFDI { get; set; }

        bool IsSelloSAT { get; set; }

        bool IsVigente { get; set; }

        IComprobanteValidacionPropiedad Esquema();

        IComprobanteValidacionPropiedad Codificacion();

        IComprobanteValidacionPropiedad LugarExpedicion();

        IComprobanteValidacionPropiedad UsoCFDI();

        IComprobanteValidacionPropiedad FormaPago();

        IComprobanteValidacionPropiedad MetodoPago();

        List<IComprobanteValidacionPropiedad> SelloCFDI();

        IComprobanteValidacionPropiedad SelloSAT();

        Dictionary<string, string> GetEmisor();
    }
}
