﻿namespace Jaeger.Aplication.Validador.Contracts {
    public interface IProgreso {
        /// <summary>
        /// obtener o establecer el % completado
        /// </summary>
        int Completado { get; set; }

        /// <summary>
        /// obtener o establecer el contador de items
        /// </summary>
        int Contador { get; set; }

        /// <summary>
        /// obtener o establecer texto
        /// </summary>
        string Caption { get; set; }

        /// <summary>
        /// obtener o establecer si el proceso esta terminado
        /// </summary>
        bool Terminado { get; set; }
    }
}