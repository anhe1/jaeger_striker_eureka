﻿namespace Jaeger.Aplication.Validador.Abstracts {
    public interface IEmpaquetadoItem {
        string FileName { get; set; }

        string XmlB64 { get; set; }

        string PdfB64 { get; set; }
    }
}
