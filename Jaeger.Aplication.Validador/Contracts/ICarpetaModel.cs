﻿namespace Jaeger.Aplication.Validador.Contracts {
    public interface ICarpetaModel  {
        string Carpeta { get; set; }

        string Accion { get; }
    }
}