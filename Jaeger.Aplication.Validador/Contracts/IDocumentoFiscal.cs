﻿namespace Jaeger.Aplication.Validador.Contracts {
    public interface IDocumentoFiscal : SAT.Reader.CFD.Interfaces.IDocumentoFiscal {
        IComprobanteValidacionModel Validacion { get; set; }
    }
}
