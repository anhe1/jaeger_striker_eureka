﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.SAT.Reader.CFD.Interfaces;

namespace Jaeger.Aplication.Validador.Contracts {
    public interface IValidacionService {
        #region propiedades
        /// <summary>
        /// obtener o establecer configuracion para el servicio
        /// </summary>
        IConfiguration Configuracion { get; set; }

        string RFC { get; set; }
        #endregion

        IValidacionService WithRFC(string rfc);

        IValidacionService WithConfiguration(IConfiguration configuracion);

        IDocumentoFiscal Procesar(IDocumentoFiscal documentoFiscal);

        Task ProcesarParallelAsync(List<IDocumentoFiscal> documentoFiscal, IProgress<Progreso> progress);

        //void Test69B(List<IDocumentoFiscal> documentos);
    }
}
