﻿using Jaeger.Aplication.Validador.Contracts;

namespace Jaeger.Aplication.Validador.Abstracts {
    public interface IVerificarBase {
        IConfiguration Configuracion { get; set; }
        string RFC { get; set; }
        IVerificarBase AddConfiguration(IConfiguration configuracion);
        IVerificarBase WithRFC(string rfc);
        IDocumentoFiscal Execute(IDocumentoFiscal documentoFiscal);
    }
}
