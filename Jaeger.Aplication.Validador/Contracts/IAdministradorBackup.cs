﻿using System;
using System.ComponentModel;
using Jaeger.Aplication.Validador.Entities;

namespace Jaeger.Aplication.Validador.Contracts {
    public interface IAdministradorBackup {
        IAdministradorBackup WitConfiguration(IConfiguration configuration);
        IAdministradorBackup WitRFC(string rfc);
        BindingList<IDocumentoFiscal> Procesar(BindingList<IDocumentoFiscal> dataSource, IProgress<Progreso> progreso);
    }
}
