﻿using System;
using System.Text;
using System.Xml;

namespace Jaeger.Aplication.Validador.Helpers {
    public class VersionDeterminer {
        public VersionDeterminer() { }

        public static string GetVersionB64(string b64String) {
            var x0 = SAT.Reader.ReaderBase.ExtraeCFDI(SAT.Reader.Helper.ReaderHelper.Base64Decode(b64String));
            return GetVersion(x0);
        }

        public static string GetVersion(string xml) {
            string attribute = "";
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(xml);
            try {
                XmlNodeList elementsByTagName = xmlDocument.GetElementsByTagName("Comprobante", "http://www.sat.gob.mx/cfd/4");
                if (elementsByTagName.Count == 0) {
                        elementsByTagName = xmlDocument.GetElementsByTagName("Comprobante", "http://www.sat.gob.mx/cfd/3");
                        if (elementsByTagName.Count == 0) {
                            throw new Exception("Nodo Comprobante es Requerido para determinar que versión es");
                        }
                    }
                    foreach (XmlElement xmlElement in elementsByTagName) {
                        attribute = xmlElement.GetAttribute("Version");
                        if (string.IsNullOrEmpty(attribute)) {
                            attribute = xmlElement.GetAttribute("version");
                        }
                    }
                } catch (Exception exception) {
                    throw;
                }
            return attribute;
        }
    }
}
