﻿using System.ComponentModel;

namespace Jaeger.Aplication.Validador.ValueObjects {
    public enum PropiedadTipoEnum {
        [Description("No definido")]
        None = 0,
        [Description("Error")]
        Error,
        [Description("Advertencia")]
        Advertencia,
        [Description("Atención")]
        Atencion,
        [Description("Información")]
        Informacion
    }
}
