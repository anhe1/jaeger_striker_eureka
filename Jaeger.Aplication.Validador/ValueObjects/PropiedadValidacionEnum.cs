﻿namespace Jaeger.Aplication.Validador.ValueObjects {
    public enum PropiedadValidacionEnum {
        Version,
        Esquemas,
        FormaPago,
        MetodoPago,
        LugarExpedicion,
        UsoCFDI,
        SelloCFDI,
        SelloSAT,
        Codificion,
        EstadoSAT,
        EFOS,
        ProdYServ,
        Unidad,
        SerieCertificado,
        SerieCertificadoSAT
    }
}

