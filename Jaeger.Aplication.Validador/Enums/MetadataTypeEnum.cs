﻿namespace Jaeger.Aplication.Validador.Enums {
    public enum MetadataTypeEnum {
        NA,
        CFD,
        CFDI,
        Acuse,
        TimbreFiscal,
        Polizas,
        Balanza,
        Retenciones,
        AuxiliarFolios,
        AuxiliarCtas,
        Other,
    }
}
