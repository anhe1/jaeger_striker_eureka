﻿using System;
using System.IO;
using System.Text;
using Jaeger.Aplication.Validador.Abstracts;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Aplication.Validador.Services {
    public class VerificarExpressService : VerificarService, IVerificarExpressService, IVerificarBase {

        public override IDocumentoFiscal Execute(IDocumentoFiscal documentoFiscal) {
            documentoFiscal.ContentXML = SAT.Reader.Helper.ReaderHelper.Base64Encode(SAT.Reader.ReaderBase.ExtraeCFDI( SAT.Reader.Helper.ReaderHelper.Base64Decode(documentoFiscal.ContentXML)));
            return ExecuteStatic(documentoFiscal, this.Configuracion);
        }

        public static IDocumentoFiscal Build(string xmlB64, string pdfB64) {
            if (string.IsNullOrEmpty(xmlB64)) {
                LogErrorService.LogWrite("Administrador: El archivo no esta disponible.");
                return null;
            }

            var response = SAT.Reader.CFD.CFDReader.ReaderB64(xmlB64);
            if (response == null) {
                response = CFDReaderSerialize.GetComprobante(new StringReader(ConvertBase64ToString(xmlB64)));
                LogErrorService.LogWrite("Administrador: Serializador");
            }

            if (response != null) {
                response.ContentXML = xmlB64;
                response.ContentPDF = pdfB64;
            }
            var documentoFiscal = new DocumentoFiscal();
            MapperClassExtensions.MatchAndMap<SAT.Reader.CFD.DocumentoFiscal, DocumentoFiscal>((SAT.Reader.CFD.DocumentoFiscal)response, documentoFiscal);
            return documentoFiscal;
        }

        private static string ConvertBase64ToString(string base64String) {
            byte[] bytes = Convert.FromBase64String(base64String);
            string result = Encoding.UTF8.GetString(bytes);
            return result;
        }
    }
}
