﻿using System.IO;
using System.Xml.Schema;
using System.Xml;
using System;
using Jaeger.SAT.Reader.CFD.Interfaces;
using Jaeger.SAT.Reader.Helper;

namespace Jaeger.Aplication.Validador.Services {
    public static class ValidateSchema {
        public static XmlReaderSettings SettingsCFDi32 = new XmlReaderSettings();
        public static XmlReaderSettings SettingsCFDi33 = new XmlReaderSettings();
        public static XmlReaderSettings SettingsCFDi40 = new XmlReaderSettings();

        static ValidateSchema() {
            ValidateSchema.SettingsCFDi32 = GetSettings32();
            ValidateSchema.SettingsCFDi33 = GetSettings33();
            ValidateSchema.SettingsCFDi40 = GetSettings40();
        }

        public static bool Execute(IDocumentoFiscal documentoFiscal) {
            
            if (documentoFiscal.Version == "3.2") {
                return Execute(ValidateSchema.SettingsCFDi32, documentoFiscal.ContentXML);
            } else if (documentoFiscal.Version == "3.3") {
                return Execute(ValidateSchema.SettingsCFDi33, documentoFiscal.ContentXML);
            } else if (documentoFiscal.Version == "4.0") {
                return Execute(ValidateSchema.SettingsCFDi40, documentoFiscal.ContentXML);
            }
            return false;
        }

        public static bool Execute(XmlReaderSettings settings, string xmlB64) {
            try {
                var xml = ReaderHelper.Base64Decode(xmlB64);
                XmlReaderSettings Settings = settings;
                Settings.ValidationType = ValidationType.Schema;
                Settings.ValidationFlags = XmlSchemaValidationFlags.ProcessSchemaLocation;
                Settings.XmlResolver = null;
                Settings.ValidationType = ValidationType.Schema;
                Settings.ValidationEventHandler += new ValidationEventHandler(ValidationHandler);
                XmlReader Reader = XmlReader.Create(new StringReader(xml), Settings);
                while (Reader.Read()) {
                }
                return true;
            } catch (XmlSchemaValidationException ex) {
                Console.WriteLine("VerificarSchema::v40 " + ex.Message);
                LogErrorService.LogWrite(ex.Message);
                if (ex.Message.Contains("http://www.sat.gob.mx/sitio_internet/cfd/catalogos:c_TasaOCuota")) {
                    return true;
                }
            }
            return false;
        }

        public static void ValidationHandler(object sender, ValidationEventArgs args) {
            Console.WriteLine("***Validation error");
            Console.WriteLine("\tSeverity: {0}", args.Severity);
            Console.WriteLine("\tMessage : {0}", args.Message);
        }

        private static XmlReaderSettings GetSettings32() {
            var service = new SAT.CFDI.Services.XsdResolverService();
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.Schemas.Add("http://www.sat.gob.mx/cfd/3", service.GetEntity("cfdv32.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/nomina", service.GetEntity("nomina11.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/nomina12", service.GetEntity("nomina12.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos/Nomina", service.GetEntity("catNomina.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos", service.GetEntity("catCFDI.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/tipoDatos/tdCFDI", service.GetEntity("tdCFDI.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/TimbreFiscalDigital", service.GetEntity("TimbreFiscalDigital.xsd"));
            return settings;
        }

        private static XmlReaderSettings GetSettings33() {
            var service = new SAT.CFDI.Services.XsdResolverService();
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.Schemas.Add("http://www.sat.gob.mx/cfd/3", service.GetEntity("cfdv33.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/tipoDatos/tdCFDI", service.GetEntity("tdCFDI.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos", service.GetEntity("catCFDI.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/nomina12", service.GetEntity("nomina12.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos/Nomina", service.GetEntity("catNomina.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/Pagos", service.GetEntity("Pagos10.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos/Pagos", service.GetEntity("catPagos.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/TimbreFiscalDigital", service.GetEntity("TimbreFiscalDigitalv11.xsd"));
            return settings;
        }

        private static XmlReaderSettings GetSettings40() {
            var service = new SAT.CFDI.Services.XsdResolverService();
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.Schemas.Add("http://www.sat.gob.mx/cfd/4", service.GetEntity("cfdv40.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/nomina12", service.GetEntity("nomina12.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos/Nomina", service.GetEntity("catNomina.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/Pagos20", service.GetEntity("Pagos20.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos/Pagos", service.GetEntity("catPagos.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/TimbreFiscalDigital", service.GetEntity("TimbreFiscalDigitalv11.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/tipoDatos/tdCFDI", service.GetEntity("tdCFDI.xsd"));
            settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos", service.GetEntity("catCFDI.xsd"));
            return settings;
        }
    }
}
