﻿using System.IO;
using System.Threading.Tasks;
using Jaeger.Aplication.Validador.Contracts;
using System.Security.AccessControl;
using System.Security.Principal;

namespace Jaeger.Aplication.Validador.Services {
    public class DirectoryService : IDirectoryService {
        public DirectoryService() {
        }

        public void CreateDirectory(string directory) {
            Directory.CreateDirectory(directory);
        }

        public void CreateDirectoryConPermisos(string directory) {
            bool flag;
            SecurityIdentifier securityIdentifier = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            DirectoryInfo directoryInfo = Directory.CreateDirectory(directory);
            DirectorySecurity accessControl = directoryInfo.GetAccessControl();
            AccessRule fileSystemAccessRule = new FileSystemAccessRule(securityIdentifier, FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.InheritOnly, AccessControlType.Allow);
            accessControl.ModifyAccessRule(AccessControlModification.Add, fileSystemAccessRule, out flag);
            directoryInfo.SetAccessControl(accessControl);
        }

        public void Delete(string directory) {
            Directory.Delete(directory);
        }

        public void Delete(string directory, bool recursive) {
            Directory.Delete(directory, recursive);
        }

        public async Task DeleteAsync(string directory, bool recursive) {
            await Task.Run(() => Directory.Delete(directory, recursive));
        }

        public void DeleteDirectory(string directory) {
            Directory.Delete(directory);
        }

        public bool Exists(string directory) {
            return Directory.Exists(directory);
        }

        public string GetCurrentDirectory() {
            return Directory.GetCurrentDirectory();
        }

        public string[] GetDirectories(string directory) {
            return Directory.GetDirectories(directory);
        }

        public string[] GetDirectories(string directory, string searchPattern) {
            return Directory.GetDirectories(directory, searchPattern);
        }

        public string[] GetDirectories(string directory, string searchPattern, SearchOption options) {
            return Directory.GetDirectories(directory, searchPattern, options);
        }

        public async Task<string[]> GetDirectoriesAsync(string directory, string searchPattern, SearchOption options) {
            string[] strArrays = await Task.Run<string[]>(() => Directory.GetDirectories(directory, searchPattern, options));
            return strArrays;
        }

        public string[] GetFiles(string directory) {
            return Directory.GetFiles(directory);
        }

        public string[] GetFiles(string directory, string searchPattern) {
            return Directory.GetFiles(directory, searchPattern);
        }

        public string[] GetFiles(string directory, string searchPattern, SearchOption options) {
            return Directory.GetFiles(directory, searchPattern, options);
        }

        public async Task<string[]> GetFilesAsync(string directory, string searchPattern, SearchOption options) {
            string[] strArrays = await Task.Run<string[]>(() => Directory.GetFiles(directory, searchPattern, options));
            return strArrays;
        }

        public DirectoryInfo GetParent(string directory) {
            return Directory.GetParent(directory);
        }

        public void Move(string directory, string destination) {
            Directory.Move(directory, destination);
        }
    }
}
