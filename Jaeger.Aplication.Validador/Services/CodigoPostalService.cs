﻿using Jaeger.Aplication.Validador.Abstracts;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.ValueObjects;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.Aplication.Validador.Services {
    public static class CodigoPostalService {
        public static ICodigosPostalesCatalogo Catalogo = new CodigoPostalCatalogo();

        static CodigoPostalService() {
            Catalogo.Load();
        }

        public static IComprobanteValidacionPropiedad Validar(string cp) {
            var response = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.LugarExpedicion).Build();
            if (!string.IsNullOrEmpty(cp)) {
                var codigo = CodigoPostalService.Catalogo.Search(cp);
                if (codigo != null) {
                    response.Valor = string.Format("{0} ({1}: {2})", "Correcto", cp, codigo.Estado);
                } else {
                    response.Valor = string.Format("{0} ({1}: {2})", "No válido", cp, codigo.Estado);
                    response.IsValido = false;
                }
                return response;
            }
            response.IsValido = false;
            response.Valor = "No disponible";
            return response;
        }
    }
}
