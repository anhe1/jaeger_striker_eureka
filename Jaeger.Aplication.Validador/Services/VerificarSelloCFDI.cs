﻿using static Jaeger.Aplication.Validador.Services.VerificarSello;

namespace Jaeger.Aplication.Validador.Services {
    public class VerificarSelloCFDI {
        public static bool Execute(string sello, string certificado, string cadenaOriginal, AlgoritmoSha tipoSHA) {
            return VerificarSello.Execute(sello, certificado, cadenaOriginal, tipoSHA);
        }
    }
}
