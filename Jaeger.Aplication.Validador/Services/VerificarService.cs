﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Jaeger.Aplication.Validador.Abstracts;
using Jaeger.Aplication.Validador.Builder;
using Jaeger.Aplication.Validador.Catalogos;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.Aplication.Validador.Interfaces;
using Jaeger.Aplication.Validador.ValueObjects;
using Jaeger.SAT.Reader.Helper;

namespace Jaeger.Aplication.Validador.Services {
    public class VerificarService : VerificarBase, IVerificarService, IVerificarBase, IVerificarStrategy {
        public VerificarService() {

        }

        public override IDocumentoFiscal Execute(IDocumentoFiscal documentoFiscal) {
            return ExecuteStatic(documentoFiscal, this.Configuracion);
        }

        public static IDocumentoFiscal ExecuteStatic(IDocumentoFiscal documentoFiscal, IConfiguration configuracion) {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            if (documentoFiscal == null) {
                return documentoFiscal;
            }

            var respuesta = new ValidacionBuilder().SetBase(documentoFiscal);
            respuesta.Add(VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.Version).WithValue(documentoFiscal.Version).Build());
            respuesta.Add(VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.SerieCertificado).WithValue(documentoFiscal.NoCertificado).Build());
            respuesta.Add(VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.SerieCertificadoSAT).WithValue(documentoFiscal.NoCertificadoSAT).Build());
            respuesta.Add(VerificarService.GetCertificado(documentoFiscal.Certificado, documentoFiscal.FechaEmision.Value));

            if (configuracion.CheckSchema) respuesta.Add(VerificarService.IsValidScheme(documentoFiscal));
            if (configuracion.CheckUTF8) respuesta.Add(VerificarService.IsUTF8(documentoFiscal));
            if (configuracion.CheckSelloCFDI) { respuesta.Add(VerificarService.IsValidSelloCFDI(documentoFiscal)); }
            if (configuracion.CheckSelloTFD) respuesta.Add(VerificarService.IsValidSelloTFD(documentoFiscal));
            if (configuracion.LugarExpedicionCP) respuesta.Add(VerificarService.LugarExpedicion(documentoFiscal));
            if (configuracion.CheckUsoCFDI) respuesta.Add(VerificarService.UsoCFDI(documentoFiscal));
            if (configuracion.FormaPago) respuesta.Add(VerificarService.FormaPago(documentoFiscal));
            if (configuracion.MetodoPago) respuesta.Add(VerificarService.MetodoPago(documentoFiscal));

            var l0 = InfoCertificado(documentoFiscal);
            foreach (var item in l0) {
                respuesta.Add(item);
            }

            // determinar nombre del emisor
            if (string.IsNullOrEmpty(documentoFiscal.EmisorNombre)) {
                var emisor = VerificarBase.GetEmisor(documentoFiscal.Certificado);
                if (emisor.HasValue()) {
                    try {
                        respuesta.AddNombreEmisor(emisor[documentoFiscal.EmisorRFC]);
                        documentoFiscal.EmisorNombre = emisor[documentoFiscal.EmisorRFC];
                    } catch (Exception exp) {
                        Console.WriteLine("No se pudo recuperar nombre del emisor: " + exp.Message);
                        exp.LogWrite("VerificarService/Execute");
                    }
                }
            }

            if (configuracion.ClavesConcepto) {
                var d0 = VerificarService.ClavesConcepto(documentoFiscal);
                if (d0.Count > 0) {
                    respuesta.Add(d0);
                }
            }

            if (documentoFiscal.TipoComprobante.ToLower().Trim() == "nomina") {
                var vnomina = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.Version).Build();
                vnomina.Valor = "Complemento Nómina " + documentoFiscal.Nomina.Select(it => it.Version).FirstOrDefault();
            }

            documentoFiscal.Validacion = respuesta.Build();
            if (configuracion.EstadoSAT) {
                documentoFiscal = VerificarBase.GetEstado(documentoFiscal);
                if (documentoFiscal.Estado == "Vigente") {
                    documentoFiscal.Resultado = "Válido";
                    respuesta.IsValid(Domain.Base.ValueObjects.CFDIValidacionEnum.Valido);
                } else {
                    documentoFiscal.Resultado = "No Válido";
                    respuesta.IsValid(Domain.Base.ValueObjects.CFDIValidacionEnum.NoValido);
                }
            } else {
                documentoFiscal.Resultado = "No Válido";
                respuesta.IsValid(Domain.Base.ValueObjects.CFDIValidacionEnum.NoValido);
            }

            stopwatch.Stop();
            TimeSpan ts = stopwatch.Elapsed;
            string elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            documentoFiscal.Validacion.TimeElapsed = elapsedTime;

            return documentoFiscal;
        }

        public async Task ProcesarParallelAsync(List<IDocumentoFiscal> documentosFiscales, IProgress<Progreso> progress) {
            Progreso reporte = new Progreso();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            int contador = 1;
            await Task.Run(() => {
                Parallel.ForEach<IDocumentoFiscal>(documentosFiscales, (site) => {
                    IDocumentoFiscal documentoFiscal1 = ExecuteStatic(site, this.Configuracion);
                    reporte.Completado = (contador * 100) / documentosFiscales.Count;
                    reporte.Caption = string.Format("Validando {0} de {1} | {2} % Completado. Tiempo transcurrido: {3:00}:{4:00}:{5:00}",
                        contador, documentosFiscales.Count, reporte.Completado, stopwatch.Elapsed.Hours, stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds);

                    if (this.RFC == site.EmisorRFC) {
                        site.SubTipo = "Emitidos";
                    } else if (this.RFC == site.ReceptorRFC) {
                        site.SubTipo = "Recibidos";
                    } else {
                        site.SubTipo = "No definido";
                    }

                    contador++;
                    progress.Report(reporte);
                });
            });

            if (this.Configuracion.Articulo69B) {
                progress.Report(new Progreso(contador, contador, "69B", false));
                await this.Test69B(documentosFiscales, progress);
            }

            progress.Report(new Progreso(contador, contador, "Proceso terminado.", true));
            stopwatch.Stop();
            return;
        }

        public static IComprobanteValidacionPropiedad IsValidScheme(IDocumentoFiscal documentoFiscal) {
            var isValid = false;
            if (documentoFiscal.Version == "3.2") {
                isValid = ValidateSchema.Execute(documentoFiscal);
            } else if (documentoFiscal.Version == "3.3") {
                isValid = ValidateSchema.Execute(documentoFiscal);
            } else if (documentoFiscal.Version == "4.0") {
                isValid = ValidateSchema.Execute(documentoFiscal);
            }

            if (isValid) {
                var d0 = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.Esquemas).IsValid(isValid).WithValue("Válido").Build();
                return d0;
            }
            var d1 = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.Esquemas).IsValid(isValid).WithValue("No Válido").Build();
            return d1;
        }

        public static IComprobanteValidacionPropiedad IsValidScheme1(IDocumentoFiscal documentoFiscal) {
            var isValid = false;
            var xml = ReaderHelper.Base64Decode(documentoFiscal.ContentXML);
            if (documentoFiscal.Version == "3.2") {
                isValid = VerificarSchema.ValidateStruct32(xml);
            } else if (documentoFiscal.Version == "3.3") {
                isValid = VerificarSchema.ValidateStruct33(xml);
            } else if (documentoFiscal.Version == "4.0") {
                isValid = VerificarSchema.ValidateStruct40(xml);
            }

            if (isValid) {
                var d0 = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.Esquemas).IsValid(isValid).WithValue("Válido").Build();
                return d0;
            }
            var d1 = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.Esquemas).IsValid(isValid).WithValue("No Válido").Build();
            return d1;
        }

        public static IComprobanteValidacionPropiedad IsUTF8(IDocumentoFiscal documentoFiscal) {
            var bcfdi = UTF8Encoding.UTF8.GetBytes(ReaderHelper.Base64Decode(documentoFiscal.ContentXML));
            var validacion = CodificacionService.Verificar(bcfdi);
            return validacion;
        }

        public static IComprobanteValidacionPropiedad IsValidSelloCFDI(IDocumentoFiscal documentoFiscal) {
            var response = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.SelloCFDI).Build();
            if (string.IsNullOrEmpty(documentoFiscal.Certificado)) {
                var certificado = VerificarSello.Catalogo.Search(documentoFiscal.NoCertificado);
                if (certificado == null) {
                    certificado = CertificadoDownloaderSAT.Buscar(documentoFiscal.NoCertificado);
                }
                if (certificado != null) {
                    documentoFiscal.Certificado = certificado.CerB64;
                    VerificarSello.Catalogo.Add(certificado);
                    VerificarSello.Catalogo.Save();
                } else {
                    response.Tipo = PropiedadTipoEnum.Advertencia;
                    response.Valor = "No es posible verificar por falta de certificado";
                    return response;
                }
            }

            var originalXmlString = ReaderHelper.Base64Decode(documentoFiscal.ContentXML);
            if (documentoFiscal.Version == "4.0") {
                response.IsValido = VerificarSelloCFDI.Execute(documentoFiscal.SelloCFDI,
                    documentoFiscal.Certificado, SAT.CFDI.Services.CadenaOriginal.GetCFDIOff(originalXmlString, "4.0"), VerificarSello.AlgoritmoSha.Sha256);
            } else if (documentoFiscal.Version == "3.3") {
                response.IsValido = VerificarSelloCFDI.Execute(documentoFiscal.SelloCFDI,
                    documentoFiscal.Certificado, SAT.CFDI.Services.CadenaOriginal.GetCFDIOff(originalXmlString, "3.3"), VerificarSello.AlgoritmoSha.Sha256);
            } else if (documentoFiscal.Version == "3.2") {
                response.IsValido = VerificarSelloCFDI.Execute(documentoFiscal.SelloCFDI,
                    documentoFiscal.Certificado, SAT.CFDI.Services.CadenaOriginal.GetCFDIOff(originalXmlString, "3.2"), VerificarSello.AlgoritmoSha.Sha1);
            }
            if (response.IsValido == false) { response.Valor = "No válido"; }
            return response;
        }

        public static IComprobanteValidacionPropiedad IsValidSelloTFD(IDocumentoFiscal documentoFiscal) {
            var response = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.SelloSAT).Build();

            if (documentoFiscal.NoCertificadoSAT.StartsWith("3")) {
                response.IsValido = false;
                response.Tipo = PropiedadTipoEnum.Advertencia;
                response.Valor = "Certificado de Prueba";
                return response;
            }

            var originalXmlString = ReaderHelper.Base64Decode(documentoFiscal.ContentXML);
            var certificado = VerificarSello.Catalogo.Search(documentoFiscal.NoCertificadoSAT);

            if (certificado == null) {
                certificado = CertificadoDownloaderSAT.Buscar(documentoFiscal.NoCertificadoSAT);
                if (certificado != null) {
                    VerificarSello.Catalogo.Add(certificado);
                    VerificarSello.Catalogo.Save();
                } else {
                    response.Tipo = PropiedadTipoEnum.Advertencia;
                    response.Valor = "No es posible verificar por falta de certificado";
                    return response;
                }
            }

            if (!string.IsNullOrEmpty(certificado.CerB64)) {
                if (documentoFiscal.VersionTFD == "1.0") {
                    string cadenaOriginal = SAT.CFDI.Services.CadenaOriginal.GetTDFOff(originalXmlString, documentoFiscal.VersionTFD);
                    response.IsValido = VerificarSelloTFD.Execute(documentoFiscal.SelloSAT, certificado.CerB64, cadenaOriginal, VerificarSello.AlgoritmoSha.Sha1);
                } else if (documentoFiscal.VersionTFD == "1.1") {
                    string cadenaOriginal = SAT.CFDI.Services.CadenaOriginal.GetTDFOff(originalXmlString, documentoFiscal.VersionTFD);
                    response.IsValido = VerificarSelloTFD.Execute(documentoFiscal.SelloSAT, certificado.CerB64, cadenaOriginal, VerificarSello.AlgoritmoSha.Sha256);
                }
            }
            return response;
        }

        public static IComprobanteValidacionPropiedad LugarExpedicion(IDocumentoFiscal documentoFiscal) {
            var validar = CodigoPostalService.Validar(documentoFiscal.LugarExpedicion);
            return validar;
        }

        public static IComprobanteValidacionPropiedad UsoCFDI(IDocumentoFiscal documentoFiscal) {
            if (documentoFiscal.UsoCFDI == null) {
                Console.WriteLine();
            }
            var validar = UsoCFDIService.Validar(documentoFiscal.UsoCFDI);
            return validar;
        }

        public static IComprobanteValidacionPropiedad FormaPago(IDocumentoFiscal documentoFiscal) {
            var response = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.FormaPago).Build();

            if (!documentoFiscal.TipoComprobante.ToLower().StartsWith("p")) {
                if (documentoFiscal.FormaPago == null) {
                    response.Valor = "Valor no presente";
                    response.IsValido = false;
                    return response;
                }
                response.Valor = documentoFiscal.FormaPago;
                if (documentoFiscal.Version == "3.2") {

                } else if (documentoFiscal.Version == "3.3") {
                    var existe = string.Empty;
                    if (!FormaPagoService.Catalogo.TryGetValue(Regex.Replace(documentoFiscal.FormaPago, "[^\\d]", ""), out existe)) {
                        response.Valor = "El atributo condicional (opcional) para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, no esta disponible en este comprobante.";
                        response.IsValido = false;
                    } else {
                        var clave = FormaPagoService.Catalogo.Where(it => it.Key == Regex.Replace(documentoFiscal.FormaPago, "[^\\d]", "")).FirstOrDefault();
                        response.Valor = string.Concat("Correcto (", clave.Key, " ", clave.Value, ")");
                    }
                } else if (documentoFiscal.Version == "4.0") {
                    if (documentoFiscal.FormaPago != null) {
                        var existe = string.Empty;
                        if (!FormaPagoService.Catalogo.TryGetValue(Regex.Replace(documentoFiscal.FormaPago, "[^\\d]", ""), out existe)) {
                            response.Valor = "El atributo condicional (opcional) para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, no esta disponible en este comprobante.";
                            response.IsValido = false;
                        } else {
                            var clave = FormaPagoService.Catalogo.Where(it => it.Key == Regex.Replace(documentoFiscal.FormaPago, "[^\\d]", "")).FirstOrDefault();
                            response.Valor = string.Concat("Correcto (", clave.Key, " ", clave.Value, ")");
                        }
                    }
                }
            } else {
                response.Valor = "No Aplica";
            }
            return response;
        }

        public static IComprobanteValidacionPropiedad MetodoPago(IDocumentoFiscal documentoFiscal) {
            var response = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.MetodoPago).Build();

            if (!documentoFiscal.TipoComprobante.ToLower().StartsWith("p")) {
                if (documentoFiscal.MetodoPago == null) {
                    response.Valor = "Valor no presente";
                    return response;
                }
                response.Valor = documentoFiscal.MetodoPago;
                if (documentoFiscal.Version == "3.2") {
                } else if (documentoFiscal.Version == "3.3") {
                    if (documentoFiscal.FechaTimbrado >= new DateTime(2017, 1, 1)) {
                        if (MetodoPagoService.Catalogo.ContainsKey(documentoFiscal.MetodoPago)) {
                            response.IsValido = true;
                        }
                    }
                } else if (documentoFiscal.Version == "4.0") {
                    if (MetodoPagoService.Catalogo.ContainsKey(documentoFiscal.MetodoPago)) {
                        response.IsValido = true;
                    }
                }
            } else {
                response.Valor = "No Aplica";
            }
            return response;
        }

        public static List<IComprobanteValidacionPropiedad> InfoCertificado(IDocumentoFiscal documentoFiscal) {
            var l0 = new List<IComprobanteValidacionPropiedad>();
            var certificado = new Crypto.Services.Certificate();
            certificado.CargarB64(documentoFiscal.Certificado);

            if (certificado.RFC.ToLower().Trim() == documentoFiscal.EmisorRFC.ToLower().Trim()) {
                l0.Add(new ComprobanteValidacionPropiedad {
                    IsValido = true,
                    Nombre = "CSD",
                    Valor = "CSD corresponde al RFC del emisor",
                });
            } else {
                l0.Add(new ComprobanteValidacionPropiedad {
                    IsValido = false,
                    Nombre = "CSD",
                    Valor = "No corresponde al RFC del emisor",
                });
            }

            if (DateTime.Now >= DateTime.Parse(certificado.ValidoDesde) & DateTime.Now <= DateTime.Parse(certificado.ValidoHasta)) {
                l0.Add(new ComprobanteValidacionPropiedad {
                    IsValido = true,
                    Nombre = "CSD",
                    Valor = string.Format("Vigencia {0} al {1}", certificado.ValidoDesde, certificado.ValidoHasta)
                });
            }

            var response = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.MetodoPago).Build();

            return l0;
        }

        public async Task Test69B(List<IDocumentoFiscal> documentos, IProgress<Progreso> progress) {
            Progreso reporte = new Progreso();
            await Task.Run(() => {
                for (int i = 0; i < documentos.Count; i++) {
                    var doc = documentos[i];
                    var rfc = string.Empty;
                    if (doc.Situacion.Contains("69b/")) {
                        if (doc.Situacion.Contains("100")) { // cuando es el emisor
                            rfc = doc.EmisorRFC;
                        } else if (doc.Situacion.Contains("101")) { // emisor y cuenta de terceros
                            rfc = doc.EmisorRFC;
                        } else if (doc.Situacion.Contains("102")) { // rfc a cuenta de terceros

                        } else if (doc.Situacion.Contains("103")) { // uno o varios rfc a cuenta de terceros
                            rfc = doc.EmisorRFC;
                        } else if (doc.Situacion.Contains("104")) { // El RFC del emisor y alguno de los RFC a cuenta de terceros
                            rfc = doc.EmisorRFC;
                        }

                        var d0 = Articulo69B.Validar(doc.EmisorRFC);
                        if (d0 != null) {
                            doc.Validacion.Resultados.Add(d0);
                        }
                    }
                    reporte.Caption = string.Format("Validando {0} de {1} | {2} % Completado.", i, documentos.Count, reporte.Completado);
                    progress.Report(reporte);
                }
            });
        }

        public void Dispose() {
            GC.Collect();
        }
    }
}
