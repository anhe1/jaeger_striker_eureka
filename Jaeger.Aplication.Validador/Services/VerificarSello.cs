﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.Aplication.Validador.Services {
    /// <summary>
    /// clase para validacion de sello digital del emisor de un CFDI
    /// </summary>
    public class VerificarSello {
        // catalogo de certificados de proveedores de certificacion autorizados
        public static ICertificadosCatalogo Catalogo = new CertificadosCatalogo();

        static VerificarSello() {
            VerificarSello.Catalogo.Load();
        }

        public VerificarSello() {
            VerificarSello.Catalogo.Load();
        }

        /// <summary>
        /// algoritmo utilizado
        /// </summary>
        public enum AlgoritmoSha {
            Sha1,
            Sha256,
        }

        public static bool Execute(string sello, string certificado, string cadenaOriginal, AlgoritmoSha tipoSHA) {
            bool flag = false;
            try {
                VerificarSello.IsValidSello(sello);
                VerificarSello.IsValidCeritificado(certificado);
                VerificarSello.IsValidCadenaOriginal(cadenaOriginal);

                byte[] bSello = Convert.FromBase64String(sello);
                byte[] bCadenaOriginal = Encoding.UTF8.GetBytes(cadenaOriginal);

                var x509Certificate2 = new X509Certificate2(Convert.FromBase64String(certificado));
                string xmlString = x509Certificate2.PublicKey.Key.ToXmlString(false);
                AsymmetricAlgorithm asymmetricAlgorithm = AsymmetricAlgorithm.Create();
                asymmetricAlgorithm.FromXmlString(xmlString);
                RSACryptoServiceProvider rSACryptoServiceProvider = (RSACryptoServiceProvider)asymmetricAlgorithm;

                if (tipoSHA == AlgoritmoSha.Sha1) {
                    flag = VerificarSello.VerifyHashSHA1(bCadenaOriginal, bSello, rSACryptoServiceProvider);
                } else if (tipoSHA == AlgoritmoSha.Sha256) {
                    flag = VerificarSello.VerifyHashSHA256(bCadenaOriginal, bSello, rSACryptoServiceProvider);
                }
            } catch (Exception exp) {
                exp.LogWrite("VerificarSello: Execute: ");
                //throw new Exception(exp.Message);
            }
            return flag;
        }

        private static bool VerifyHashSHA1(byte[] bCadenaOriginal, byte[] bSello, RSACryptoServiceProvider rsa) {
            bool flag = false;
            try {
                byte[] numArray = (new SHA1CryptoServiceProvider()).ComputeHash(bCadenaOriginal);
                flag = rsa.VerifyHash(numArray, CryptoConfig.MapNameToOID("SHA1"), bSello);
            } catch (Exception exp) {
                exp.LogWrite("VerifyHashSHA1");
                Console.WriteLine(exp.Message);
            }
            return flag;
        }

        private static bool VerifyHashSHA256(byte[] bCadenaOriginal, byte[] bSello, RSACryptoServiceProvider rsa) {
            bool flag = false;
            try {
                byte[] numArray = (new SHA256CryptoServiceProvider()).ComputeHash(bCadenaOriginal);
                flag = rsa.VerifyHash(numArray, CryptoConfig.MapNameToOID("SHA256"), bSello);
            } catch (Exception exp) {
                exp.LogWrite("VerifyHashSHA256");
                Console.WriteLine(exp.Message);
            }
            return flag;
        }

        private static void IsValidCadenaOriginal(string cadenaOriginal) {
            if ((string.IsNullOrEmpty(cadenaOriginal) && cadenaOriginal.Length > 0)) {
                throw new ArgumentNullException("Cadena original no válida.");
            }
        }

        private static void IsValidCeritificado(string certificado) {
            if ((string.IsNullOrEmpty(certificado) && certificado.Length > 0)) {
                throw new ArgumentNullException("Certificado no declarado.");
            }
        }

        private static void IsValidSello(string sello) {
            if ((string.IsNullOrEmpty(sello) && sello.Length > 0)) {
                throw new ArgumentNullException("Sello no declarado.");
            }
        }
    }
}
