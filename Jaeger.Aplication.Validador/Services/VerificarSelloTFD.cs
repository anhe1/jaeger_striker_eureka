﻿using Jaeger.Aplication.Validador.Catalogos;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using static Jaeger.Aplication.Validador.Services.VerificarSello;

namespace Jaeger.Aplication.Validador.Services {
    /// <summary>
    /// Verificar sello timbre fiscal digital
    /// </summary>
    public class VerificarSelloTFD {

        public VerificarSelloTFD() {
            
        }

        public bool Verificar(object timbreFiscal) {
            if (timbreFiscal.GetType() == typeof(SAT.CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital)) {
                return VerificarSelloTFD.Validar(timbreFiscal as SAT.CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital);
            } else if (timbreFiscal.GetType() == typeof(SAT.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital)) {
                return VerificarSelloTFD.Validar(timbreFiscal as SAT.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital);
            }
            return false;
        }

        #region metodos estaticos
        public static bool Execute(string sello, string certificado, string cadenaOriginal, AlgoritmoSha tipoSHA) {
            return VerificarSello.Execute(sello, certificado, cadenaOriginal, tipoSHA);
        }

        public static bool Validar(object timbreFiscal) {
            if (timbreFiscal.GetType() == typeof(SAT.CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital)) {
                return VerificarSelloTFD.Validar(timbreFiscal as SAT.CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital);
            } else if (timbreFiscal.GetType() == typeof(SAT.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital)) {
                return VerificarSelloTFD.Validar(timbreFiscal as SAT.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital);
            }
            return false;
        }

        public static bool Validar(SAT.CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital timbreFiscal) {
            if (timbreFiscal != null) {
                var certificado = VerificarSello.Catalogo.Search(timbreFiscal.noCertificadoSAT);
                if (certificado == null) {
                    certificado = CertificadoDownloaderSAT.Buscar(timbreFiscal.noCertificadoSAT);
                    if (certificado != null) {
                        VerificarSello.Catalogo.Add(certificado);
                        VerificarSello.Catalogo.Save();
                    }
                }

                if (!string.IsNullOrEmpty(certificado.CerB64)) {
                    string cadenaOriginal = timbreFiscal.CadenaOriginal;
                    return VerificarSelloCFDI.Execute(timbreFiscal.selloSAT, certificado.CerB64, cadenaOriginal, VerificarSello.AlgoritmoSha.Sha1);
                }
            }
            return false;
        }

        public static bool Validar(SAT.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital timbreFiscal) {
            if (timbreFiscal != null) {
                var certificado = VerificarSello.Catalogo.Search(timbreFiscal.NoCertificadoSAT);
                if (certificado == null) {
                    certificado = CertificadoDownloaderSAT.Buscar(timbreFiscal.NoCertificadoSAT);
                    if (certificado != null) {
                        VerificarSello.Catalogo.Add(certificado);
                        VerificarSello.Catalogo.Save();
                    }
                }

                if (certificado != null) {
                    if (!string.IsNullOrEmpty(certificado.CerB64)) {
                        string cadenaOriginal = timbreFiscal.CadenaOriginal;
                        return VerificarSelloCFDI.Execute(timbreFiscal.SelloSAT, certificado.CerB64, cadenaOriginal, VerificarSello.AlgoritmoSha.Sha256);
                    }
                }
            }
            return false;
        }
        #endregion
    }
}
