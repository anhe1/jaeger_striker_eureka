﻿using System;
using System.IO;
using System.Text;
using Jaeger.Aplication.Validador.Abstracts;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.ValueObjects;

namespace Jaeger.Aplication.Validador.Services {
    public class CodificacionService {
        public static IComprobanteValidacionPropiedad Verificar(byte[] _cfdi) {
            long num;
            var response = VerificarBase.Create().AddPropiedad(PropiedadValidacionEnum.Codificion).Build();

            Encoding encoding = CodificacionService.DetectBomBytes(_cfdi);
            MemoryStream memoryStream = new MemoryStream(_cfdi);
            memoryStream.Seek((long)0, SeekOrigin.Begin);
            StreamReader streamReader = new StreamReader(memoryStream);
            string end = streamReader.ReadToEnd();
            Encoding currentEncoding = streamReader.CurrentEncoding;

            if (encoding == Encoding.UTF8) {
                response.IsValido = true;
            } else if (currentEncoding != Encoding.UTF8) {
                response.Valor = string.Concat("Codificación del CFD/CFDI es UTF-8", " ", encoding.EncodingName, encoding.BodyName);
                response.IsValido = true;
            } else if (!CodificacionService.IsUtf8ValidCharacters(end, out num)) {
                response.Valor = string.Format("Codificación del CFD/CFDI es {0} secuencia invalida en posición {1}", encoding.EncodingName, num);
                response.IsValido = false;
            } else {
                response.IsValido = true;
            }
            return response;
        }

        public static Encoding DetectEncoding(string ruta) {
            Encoding utf8;
            try {
                Encoding @default = null;
                FileStream fileStream = new FileStream(ruta, FileMode.Open, FileAccess.Read, FileShare.Read);
                if (!fileStream.CanSeek) {
                    @default = Encoding.Default;
                } else {
                    byte[] numArray = new byte[4];
                    fileStream.Read(numArray, 0, 4);
                    @default = ((numArray[0] != 239 || numArray[1] != 187 || numArray[2] != 191) && (numArray[0] != 255 || numArray[1] != 254) && (numArray[0] != 254 || numArray[1] != 255) && (numArray[0] != 0 || numArray[1] != 0 || numArray[2] != 254 || numArray[3] != 255) ? Encoding.Default : Encoding.UTF8);
                    fileStream.Seek(0, SeekOrigin.Begin);
                }
                byte[] numArray1 = new byte[4096];
                while (fileStream.Read(numArray1, 0, 4096) != 0) {
                    @default.GetString(numArray1);
                }
                Console.WriteLine();
                fileStream.Close();
                utf8 = @default;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                utf8 = Encoding.UTF8;
            }
            return utf8;
        }

        public static Encoding DetectEncoding(FileInfo fileInfo) {
            Encoding utf8;
            try {
                Encoding @default = null;
                FileStream fileStream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read, FileShare.Read);
                if (!fileStream.CanSeek) {
                    @default = Encoding.Default;
                } else {
                    byte[] numArray = new byte[4];
                    fileStream.Read(numArray, 0, 4);
                    @default = ((numArray[0] != 239 || numArray[1] != 187 || numArray[2] != 191) && (numArray[0] != 255 || numArray[1] != 254) && (numArray[0] != 254 || numArray[1] != 255) && (numArray[0] != 0 || numArray[1] != 0 || numArray[2] != 254 || numArray[3] != 255) ? Encoding.Default : Encoding.UTF8);
                    fileStream.Seek(0, SeekOrigin.Begin);
                }
                byte[] numArray1 = new byte[4096];
                while (fileStream.Read(numArray1, 0, 4096) != 0) {
                    @default.GetString(numArray1);
                }
                Console.WriteLine();
                fileStream.Close();
                utf8 = @default;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                utf8 = Encoding.UTF8;
            }
            return utf8;
        }

        public static Encoding DetectBomBytes(byte[] bomBytes) {
            if (bomBytes == null) {
                throw new ArgumentNullException("Must provide a valid BOM byte array!", "BOMBytes");
            }

            if (bomBytes.Length < 2) {
                return null;
            }

            if (bomBytes[0] == 255 && bomBytes[1] == 254 && ((bomBytes.Length) < 4 || bomBytes[2] != 0 || bomBytes[3] != 0)) {
                return Encoding.Unicode;
            }

            if (bomBytes[0] == 254 && bomBytes[1] == 255) {
                return Encoding.BigEndianUnicode;
            }

            if (bomBytes.Length < 3) {
                return null;
            }

            if (bomBytes[0] == 239 && bomBytes[1] == 187 && bomBytes[2] == 191) {
                return Encoding.UTF8;
            }

            if (bomBytes[0] == 43 && bomBytes[1] == 47 && bomBytes[2] == 118) {
                return Encoding.UTF7;
            }

            if (bomBytes.Length < 4) {
                return null;
            }

            if (bomBytes[0] == 255 && bomBytes[1] == 254 && bomBytes[2] == 0 && bomBytes[3] == 0) {
                return Encoding.UTF32;
            }

            if (bomBytes[0] != 0 || bomBytes[1] != 0 || bomBytes[2] != 254 || bomBytes[3] != 255) {
                return Encoding.ASCII;
            }

            return Encoding.GetEncoding(12001);
        }

        public static bool IsUtf8ValidCharacters(string inString, out long indexInvalid) {
            bool isUtf8ValidCharacters;
            indexInvalid = -1;
            if (inString != null) {
                int num = 0;
                do {
                    char chr = inString[num];
                    if (((chr >= 'ý' || chr <= '\u001F') && chr != '\t' && chr != '\n' && chr != '\r' ? false : true)) {
                        num = checked(num + 1);
                    } else {
                        indexInvalid = num;
                        isUtf8ValidCharacters = false;
                        return isUtf8ValidCharacters;
                    }
                }
                while (num < inString.Length);
                isUtf8ValidCharacters = true;
            } else {
                isUtf8ValidCharacters = true;
            }
            return isUtf8ValidCharacters;
        }
    }
}
