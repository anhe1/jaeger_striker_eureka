﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.Entities;

namespace Jaeger.Aplication.Validador.Services {
    public class CarpetaRecienteService : ICarpetaRecienteService {
        private string localFileName = @"C:\Jaeger\jaeger_recientes.json";
        private List<ICarpetaModel> items;

        public CarpetaRecienteService() {
            this.items = new List<ICarpetaModel>();
        }

        public ICarpetaRecienteService WithLocalFolder(string localFolder = @"C:\Jaeger\jaeger_recientes.json") {
            this.localFileName = localFolder;
            return this;
        }

        public List<ICarpetaModel> Items {
            get {
                return this.items;
            }
            set {
                this.items = value;
            }
        }

        public void Add(string carpeta) {
            this.Add(new CarpetaModel(carpeta));
        }

        public void Add(ICarpetaModel newItem) {
            try {
                var e = this.items.Find((ICarpetaModel b) => b.Carpeta == newItem.Carpeta);
                if (e == null)
                    this.items.Add(newItem);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        public void Delete(int index) {
            try {
                this.items.RemoveAt(index);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        public void Save() {
            try {
                System.IO.File.WriteAllText(this.localFileName, JsonConvert.SerializeObject(this.items));
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        public void Load() {
            if (System.IO.File.Exists(this.localFileName)) {
                var contenido = System.IO.File.ReadAllText(this.localFileName);
                try {
                    this.items = new List<ICarpetaModel>(JsonConvert.DeserializeObject<List<CarpetaModel>>(contenido));
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                    this.items = new List<ICarpetaModel>();
                }
            } else {
                this.items = new List<ICarpetaModel>();
            }
        }
    }
}
