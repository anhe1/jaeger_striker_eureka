﻿using System;
using System.IO;
using System.Collections.Generic;
using Jaeger.SAT.Reader.CFD.Interfaces;
using Jaeger.SAT.Reader.CFD;
using Jaeger.SAT.Reader.Helper;
using Jaeger.SAT.Reader;

namespace Jaeger.Aplication.Validador.Services {
    public class CFDReaderSerialize : ReaderBase, ICFDReader {
        #region declaraciones
        protected internal DocumentoFiscal _DocumentoFiscal;
        #endregion

        public CFDReaderSerialize() : base() {
        }

        public static IDocumentoFiscal GetComprobante(FileInfo fileInfo) {
            var xml = GetDocument(fileInfo);
            if (xml != null) {
                return GetComprobante(new StringReader(xml.OuterXml));
            }
            return null;
        }

        public static IDocumentoFiscal GetComprobante(StringReader stringReader) {
            var stringXml = stringReader.ReadToEnd();
            if (IsValidXml(stringXml)) {
                var documentXml = GetDocument(stringReader);
                if (documentXml != null) {
                    string version = GetVersion(documentXml);
                    if (version == "4.0") {
                        return GetComprobante(SAT.CFDI.V40.Comprobante.LoadXml(documentXml.OuterXml));
                    } else if (version != "3.3") {
                        if (version != "3.2") {
                           LogErrorService.LogWrite("El comprobante contiene una versión no soportada o el formato es incorrecto");
                            throw new Exception("El comprobante contiene una versión no soportada o el formato es incorrecto");
                        }
                        return GetComprobante(SAT.CFDI.V32.Comprobante.LoadXml(documentXml.OuterXml));
                    } else {
                        return GetComprobante(SAT.CFDI.V33.Comprobante.LoadXml(documentXml.OuterXml));
                    }
                }
            }
            return null;
        }

        private static IDocumentoFiscal GetComprobante(SAT.CFDI.V32.Comprobante comprobante) {
            if (comprobante == null) {
                return null;
            }

            IDocumentoFiscal response = new DocumentoFiscal {
                Version = comprobante.version,
                ClaveExportacion = "NA",
                Descuento = comprobante.descuento,
                EmisorNombre = comprobante.Emisor.nombre,
                EmisorRegimenFiscal = "",
                EmisorRFC = comprobante.Emisor.rfc,
                FechaEmision = comprobante.fecha,
                Folio = comprobante.folio,
                TipoComprobante = GetTipoComprobante(comprobante.tipoDeComprobante),
                FormaPago = comprobante.formaDePago,
                MetodoPago = comprobante.metodoDePago,
                LugarExpedicion = comprobante.LugarExpedicion,
                Moneda = comprobante.Moneda,
                NoCertificado = comprobante.noCertificado,
                ReceptorNombre = comprobante.Receptor.nombre,
                ReceptorRFC = comprobante.Receptor.rfc,
                ReceptorRegimenFiscal = "",
                ReceptorDomicilioFiscal = "",
                SelloCFDI = comprobante.sello,
                Certificado = comprobante.certificado,
                CondicionPago = comprobante.condicionesDePago
            };

            if (comprobante.Complemento != null) {
                if (comprobante.Complemento.TimbreFiscalDigital != null) {
                    response = ComplementoTimbreFiscalReader(comprobante.Complemento.TimbreFiscalDigital, response);
                } else if (comprobante.Complemento.Nomina11 != null) {
                    response.Nomina = ComplementoNominaReader(comprobante.Complemento.Nomina11);
                } else if (comprobante.Complemento.Nomina12 != null) {
                    response.Nomina = ComplementoNominaReader(comprobante.Complemento.Nomina12);
                }
            }
            response.Conceptos = GetConceptos(comprobante.Conceptos);
            return response;
        }

        private static List<IDocumentoFiscalConcepto> GetConceptos(SAT.CFDI.V32.ComprobanteConcepto[] conceptos) {
            if (conceptos.HasValue()) {
                var responses = new List<IDocumentoFiscalConcepto>();
                foreach (var item in conceptos) {
                    responses.Add(new DocumentoFiscalConcepto {
                        Cantidad = item.cantidad,
                        ClaveProdServ = "",
                        ClaveUnidad = "",
                        Descripcion = item.descripcion,
                        Descuento = 0,
                        Importe = item.importe,
                        Unidad = item.unidad,
                        ValorUnitario = item.valorUnitario,
                        NoIdentificacion = "",
                        ObjetoImp = null
                    });
                }
                return responses;
            }
            return null;
        }

        private static IDocumentoFiscal GetComprobante(SAT.CFDI.V33.Comprobante comprobante) {
            if (comprobante == null) {
                return null;
            }

            IDocumentoFiscal response = new DocumentoFiscal {
                Version = comprobante.Version,
                ClaveExportacion = "NA",
                Descuento = comprobante.Descuento,
                EmisorNombre = comprobante.Emisor.Nombre,
                EmisorRegimenFiscal = comprobante.Emisor.RegimenFiscal,
                EmisorRFC = comprobante.Emisor.Rfc,
                FechaEmision = comprobante.Fecha,
                Folio = comprobante.Folio,
                TipoComprobante = GetTipoComprobante(comprobante.TipoDeComprobante),
                FormaPago = comprobante.FormaPago,
                MetodoPago = comprobante.MetodoPago,
                LugarExpedicion = comprobante.LugarExpedicion,
                Moneda = comprobante.Moneda,
                NoCertificado = comprobante.NoCertificado,
                ReceptorNombre = comprobante.Receptor.Nombre,
                ReceptorRFC = comprobante.Receptor.Rfc,
                ReceptorRegimenFiscal = "",
                ReceptorDomicilioFiscal = "",
                UsoCFDI = comprobante.Receptor.UsoCFDI,
                Certificado = comprobante.Certificado,
                SelloCFDI = comprobante.Sello,
                CondicionPago = comprobante.CondicionesDePago
            };

            if (comprobante.Complemento != null) {
                if (comprobante.Complemento.TimbreFiscalDigital != null) {
                    response = ComplementoTimbreFiscalReader(comprobante.Complemento.TimbreFiscalDigital, response);
                } else if (comprobante.Complemento.Pagos != null) {
                    response.Pagos = ComplementoPagoReader(comprobante.Complemento.Pagos);
                } else if (comprobante.Complemento.Nomina12 != null) {
                    response.Nomina = ComplementoNominaReader(comprobante.Complemento.Nomina12);
                }
            }

            if (comprobante.Impuestos.HasValue()) {
                response.TrasladoIVA = comprobante.Impuestos.TotalImpuestosTrasladados;
            }
            response.Conceptos = GetConceptos(comprobante.Conceptos);
            return response;
        }

        private static List<IDocumentoFiscalConcepto> GetConceptos(SAT.CFDI.V33.ComprobanteConcepto[] conceptos) {
            if (conceptos != null) {
                var responses = new List<IDocumentoFiscalConcepto>();
                foreach (var item in conceptos) {
                    responses.Add(new DocumentoFiscalConcepto {
                        Cantidad = item.Cantidad,
                        ClaveProdServ = item.ClaveProdServ,
                        ClaveUnidad = item.ClaveUnidad,
                        Descripcion = item.Descripcion,
                        Descuento = item.DescuentoSpecified ? item.Descuento : 0,
                        Importe = item.Importe,
                        Unidad = item.Unidad,
                        ValorUnitario = item.ValorUnitario,
                        NoIdentificacion = item.NoIdentificacion,
                        ObjetoImp = null
                    });
                }
                return responses;
            }
            return null;
        }

        private static IDocumentoFiscal GetComprobante(SAT.CFDI.V40.Comprobante comprobante) {
            if (comprobante == null) {
                return null;
            }

            IDocumentoFiscal response = new DocumentoFiscal {
                Version = comprobante.Version,
                ClaveExportacion = comprobante.Exportacion,
                Descuento = comprobante.Descuento,
                EmisorNombre = comprobante.Emisor.Nombre,
                EmisorRegimenFiscal = "",
                EmisorRFC = comprobante.Emisor.Rfc,
                FechaEmision = comprobante.Fecha,
                Folio = comprobante.Folio,
                TipoComprobante = GetTipoComprobante(comprobante.TipoDeComprobante),
                FormaPago = comprobante.FormaPago,
                MetodoPago = comprobante.MetodoPago,
                LugarExpedicion = comprobante.LugarExpedicion,
                Moneda = comprobante.Moneda,
                NoCertificado = comprobante.NoCertificado,
                ReceptorNombre = comprobante.Receptor.Nombre,
                ReceptorRFC = comprobante.Receptor.Rfc,
                UsoCFDI = comprobante.Receptor.UsoCFDI,
                SubTotal = comprobante.SubTotal,
                Total = comprobante.Total,
                Serie = comprobante.Serie,
                ReceptorRegimenFiscal = "",
                ReceptorDomicilioFiscal = "",
                SelloCFDI = comprobante.Sello,
                Certificado = comprobante.Certificado,
                CondicionPago = comprobante.CondicionesDePago
            };

            if (comprobante.Complemento != null) {
                if (comprobante.Complemento.TimbreFiscalDigital != null) {
                    response = ComplementoTimbreFiscalReader(comprobante.Complemento.TimbreFiscalDigital, response);
                } else if (comprobante.Complemento.Pagos != null) {
                    response.Pagos = ComplementoPagoReader(comprobante.Complemento.Pagos);
                } else if (comprobante.Complemento.Nomina != null) {
                    response.Nomina = ComplementoNominaReader(comprobante.Complemento.Nomina);
                }
            }

            if (comprobante.Impuestos.HasValue()) {
                response.TrasladoIVA = comprobante.Impuestos.TotalImpuestosTrasladados;
            }
            response.Conceptos = GetConceptos(comprobante.Conceptos);
            return response;
        }

        private static List<IDocumentoFiscalConcepto> GetConceptos(SAT.CFDI.V40.ComprobanteConcepto[] conceptos) {
            if (conceptos != null) {
                var responses = new List<IDocumentoFiscalConcepto>();
                foreach (var item in conceptos) {
                    responses.Add(new DocumentoFiscalConcepto {
                        Cantidad = item.Cantidad,
                        ClaveProdServ = item.ClaveProdServ,
                        ClaveUnidad = item.ClaveUnidad,
                        Descripcion = item.Descripcion,
                        Descuento = item.DescuentoSpecified ? item.Descuento : 0,
                        Importe = item.Importe,
                        Unidad = item.Unidad,
                        ObjetoImp = item.ObjetoImp,
                        ValorUnitario = item.ValorUnitario,
                        NoIdentificacion = item.NoIdentificacion
                    });
                }
                return responses;
            }
            return null;
        }

        private static IDocumentoFiscal ComplementoTimbreFiscalReader(SAT.CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital tfd, IDocumentoFiscal response) {
            if (tfd != null) {
                response.IdDocumento = tfd.UUID;
                response.FechaTimbrado = tfd.FechaTimbrado;
                response.VersionTFD = tfd.version;
                response.NoCertificadoSAT = tfd.noCertificadoSAT;
                response.SelloSAT = tfd.selloSAT;
            }
            return response;
        }

        private static IDocumentoFiscal ComplementoTimbreFiscalReader(SAT.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital tfd, IDocumentoFiscal response) {
            if (tfd != null) {
                response.IdDocumento = tfd.UUID;
                response.FechaTimbrado = tfd.FechaTimbrado;
                response.RFCProvCertif = tfd.RfcProvCertif;
                response.NoCertificadoSAT = tfd.NoCertificadoSAT;
                response.SelloSAT = tfd.SelloSAT;
            }
            return response;
        }

        private static List<IDocumentoComplementoPago> ComplementoPagoReader(SAT.CFDI.Complemento.Pagos.V10.Pagos pagos) {
            var d0 = new List<IDocumentoComplementoPago>();
            foreach (var item in pagos.Pago) {
                var d1 = new DocumentoComplementoPago {
                    CadPago = item.CadPago,
                    CertPago = item.CertPagoB64,
                    CtaBeneficiario = item.CtaBeneficiario,
                    CtaOrdenante = item.CtaOrdenante,
                    FechaPagoP = item.FechaPago,
                    FormaDePagoP = item.FormaDePagoP,
                    MonedaP = item.MonedaP,
                    MontoP = item.Monto,
                    NomBancoOrdExt = item.NomBancoOrdExt,
                    NumOperacion = item.NumOperacion,
                    RfcEmisorCtaBen = item.RfcEmisorCtaBen,
                    RfcEmisorCtaOrd = item.RfcEmisorCtaOrd,
                    SelloPago = item.SelloBase64,
                    TipoCadPago = item.TipoCadPago,
                    TipoCambioP = item.TipoCambioP,
                    Version = pagos.Version
                };
                if (item.DoctoRelacionado.HasValue()) {
                    foreach (var item1 in item.DoctoRelacionado) {
                        var d2 = new DocumentoComplementoPagoCFDIRelacionado {
                            IdDocumento = item1.IdDocumento,
                            EquivalenciaDR = item1.TipoCambioDR,
                            Folio = item1.Folio,
                            ImpPagado = item1.ImpPagado,
                            ImpSaldoAnt = item1.ImpSaldoAnt,
                            ImpSaldoInsoluto = item1.ImpSaldoInsoluto,
                            MetodoPago = item1.MetodoDePagoDR,
                            Serie = item1.Serie,
                            MonedaDR = item1.MonedaDR,
                            NumParcialidad = int.Parse(item1.NumParcialidad)
                        };
                        if (!d1.Relacionados.HasValue())
                            d1.Relacionados = new List<IDocumentoComplementoPagoCFDIRelacionado>();
                        d1.Relacionados.Add(d2);
                    }
                }
                d0.Add(d1);
            }
            return d0;
        }

        private static List<IDocumentoComplementoPago> ComplementoPagoReader(SAT.CFDI.Complemento.Pagos.V20.Pagos pagos) {
            var d0 = new List<IDocumentoComplementoPago>();
            foreach (var item in pagos.Pago) {
                var d1 = new DocumentoComplementoPago {
                    CadPago = item.CadPago,
                    CertPago = item.CertPagoB64,
                    CtaBeneficiario = item.CtaBeneficiario,
                    CtaOrdenante = item.CtaOrdenante,
                    FechaPagoP = item.FechaPago,
                    FormaDePagoP = item.FormaDePagoP,
                    MonedaP = item.MonedaP,
                    MontoP = item.Monto,
                    NomBancoOrdExt = item.NomBancoOrdExt,
                    NumOperacion = item.NumOperacion,
                    RfcEmisorCtaBen = item.RfcEmisorCtaBen,
                    RfcEmisorCtaOrd = item.RfcEmisorCtaOrd,
                    SelloPago = item.SelloBase64,
                    TipoCadPago = item.TipoCadPago,
                    TipoCambioP = item.TipoCambioP,
                    Version = pagos.Version
                };
                if (item.DoctoRelacionado.HasValue()) {
                    foreach (var item1 in item.DoctoRelacionado) {
                        var d2 = new DocumentoComplementoPagoCFDIRelacionado {
                            IdDocumento = item1.IdDocumento,
                            EquivalenciaDR = item1.EquivalenciaDR,
                            Folio = item1.Folio,
                            ImpPagado = item1.ImpPagado,
                            ImpSaldoAnt = item1.ImpSaldoAnt,
                            ImpSaldoInsoluto = item1.ImpSaldoInsoluto,
                            Serie = item1.Serie,
                            MonedaDR = item1.MonedaDR,
                            NumParcialidad = int.Parse(item1.NumParcialidad),
                            ObjetoImpDR = item1.ObjetoImpDR
                        };
                        if (!d1.Relacionados.HasValue())
                            d1.Relacionados = new List<IDocumentoComplementoPagoCFDIRelacionado>();
                        d1.Relacionados.Add(d2);
                    }
                }
                d0.Add(d1);
            }
            return d0;
        }

        private static List<IDocumentoComplementoNomina> ComplementoNominaReader(SAT.CFDI.Complemento.Nomina.V11.Nomina nomina) {
            var d0 = new List<IDocumentoComplementoNomina>();

            return d0;
        }

        private static List<IDocumentoComplementoNomina> ComplementoNominaReader(SAT.CFDI.Complemento.Nomina.V12.Nomina nomina) {
            var d0 = new List<IDocumentoComplementoNomina>();
            return d0;
        }

        public IDocumentoFiscal Read(StringReader stringXml) {
            throw new NotImplementedException();
        }

        public IDocumentoFiscal Read(FileInfo fileInfo) {
            throw new NotImplementedException();
        }

        public IDocumentoFiscal Read(byte[] bytes) {
            throw new NotImplementedException();
        }
    }
}
