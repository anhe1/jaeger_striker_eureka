﻿using System;
using System.IO;
using System.Linq;
using System.Diagnostics;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.Services;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.SAT.Reader;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Aplication.Validador.Builder {
    public class Administrador : Abstracts.AdministradorBase, IAdministrador {

        public Administrador() : base() { }

        /// <summary>
        /// Agregar configuracion
        /// </summary>
        /// <param name="configuracion">IConfiguracion</param>
        public Administrador AddConfiguration(IConfiguration configuracion) {
            this.Configuracion = configuracion;
            return this;
        }

        public Administrador AddRFC(string rfc) {
            this.RFC = rfc;
            return this;
        }

        public Administrador Agregar(string fileName) {
            var fileInfo = new FileInfo(fileName);
            if (fileInfo.Exists == false) {
                this.DataError.Add(new DocumentoFiscalError(fileInfo.FullName, "No existe el archivo."));
            } else {
                var d0 = GetDocumentoFiscal(fileInfo);
                if (d0 != null) {
                    this.Add(d0);
                }
            }
            return this;
        }

        public Administrador Add(IDocumentoFiscal item) {
            if (string.IsNullOrEmpty(item.IdDocumento)) {
                this.DataError.Add(new DocumentoFiscalError(item.PathXML, "No existe folio fiscal "));
                return this;
            }

            if (item.EmisorRFC == null | item.ReceptorRFC == null) {
                this.DataError.Add(new DocumentoFiscalError(item.PathXML, "Este comprobante no identificado."));
                return this;
            }

            if (this.Configuracion.SoloEmisorReceptor) {
                if (this.RFC.ToLower() != item.EmisorRFC.ToLower() & this.RFC.ToLower() != item.ReceptorRFC.ToLower()) {
                    this.DataError.Add(new DocumentoFiscalError(item.PathXML, "Este comprobante no fué emitido por o para la empresa registrada. "));
                    return this;
                }
            }
            
            //if (item.TipoComprobante != "Nomina") {
            //    return this;
            //}

            try {
                var search = this.DataSource.Where(it => it.IdDocumento.ToLower() == item.IdDocumento.ToLower()).FirstOrDefault();
                if (search == null) {
                    this.DataSource.Add(item);
                } else {
                    item.Resultado = "Duplicado";
                    this.DataError.Add(new DocumentoFiscalError(item.PathXML, item.Resultado));
                }
            } catch (Exception ex) {
                ex.LogWrite("Administrador: Add");
            }
            return this;
        }

        public bool Remover(IDocumentoFiscal documento) {
            try {
                this.DataSource.Remove(documento);
                return true;
            } catch (Exception exp) {
                Console.WriteLine(exp.Message);
                LogErrorService.LogWrite("Administrador/Remove: " + exp.StackTrace);
            }
            return false;
        }

        public Administrador Search(string path, SearchOption options, IProgress<Progreso> progreso) {
            Stopwatch stopwatch = new Stopwatch();
            var files = this._DirectoryService.GetFiles(path, "*.xml", options);
            var reporte = new Progreso() { Caption = "Obteniendo lista de archivos ..." };
            var contador = 0;
            foreach (var file in files) {
                var fileInfo = new FileInfo(file);
                reporte.Caption = "Procesando: " + file;
                progreso.Report(reporte);
                contador++;
                var add = GetDocumentoFiscal(fileInfo);
                if (add != null) {
                    add.PathXML = fileInfo.FullName;
                    this.Add(add);
                } else {
                    this.DataError.Add(new DocumentoFiscalError(file, "Error al cargar el archivo."));
                    LogErrorService.LogWrite("Administrador/Search: Error al cargar el archivo (" + file + ").");
                }
            }
            return this;
        }

        private static IDocumentoFiscal GetDocumentoFiscal(FileInfo localFile) {
            if (localFile.Exists == false) {
                LogErrorService.LogWrite("Administrador: El archivo no esta disponible (" + localFile.FullName + ").");
                return null;
            }

            var response = SAT.Reader.CFD.CFDReader.Reader(localFile);
            if (response == null) {
                response = CFDReaderSerialize.GetComprobante(localFile);
                LogErrorService.LogWrite("Administrador: Serializador");
            }

            if (response != null) {
                response.PathXML = localFile.FullName;
                response.ContentXML = SAT.Reader.Helper.ReaderHelper.FileToBase64(localFile);

                if (File.Exists(Path.ChangeExtension(localFile.FullName, "pdf"))) {
                    response.PathPDF = Path.ChangeExtension(localFile.FullName, "pdf");
                } else {
                    if (!string.IsNullOrEmpty(response.IdDocumento)) {
                        Console.WriteLine("Buscando PDF");
                        var id = SearchPDF(localFile, response.IdDocumento);
                        if (File.Exists(id)) {
                            response.PathPDF = id;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(response.PathPDF)) {
                    if (File.Exists(response.PathPDF)) {
                        response.ContentPDF = SAT.Reader.Helper.ReaderHelper.ReadFileB64(new FileInfo(response.PathPDF));
                    }
                }
            }
            var d0 = new DocumentoFiscal();
            MapperClassExtensions.MatchAndMap<SAT.Reader.CFD.DocumentoFiscal, DocumentoFiscal>((SAT.Reader.CFD.DocumentoFiscal)response, d0);
            return d0;
        }
    }
}
