﻿using System;
using System.IO;
using Jaeger.SAT.Reader.CFD.Interfaces;

namespace Jaeger.Aplication.Validador.Services {
    public static class FileStorageExtensions {
        public static bool MoveTo(this IDocumentoFiscal documentoFiscal, string carpetaDestino) {
            // si existe el origen continuamos
            if (!File.Exists(documentoFiscal.PathXML))
                return false;

            if (!File.Exists(Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".xml"))) {
                if (!Directory.Exists(carpetaDestino)) { Directory.CreateDirectory(carpetaDestino); }
                var si = MoveFile(documentoFiscal.PathXML, Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".xml"));
                if (si)
                    documentoFiscal.PathXML = Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".xml");
            }

            if (!File.Exists(Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".pdf"))) {
                if (!Directory.Exists(carpetaDestino)) { Directory.CreateDirectory(carpetaDestino); }
                var si = MoveFile(documentoFiscal.PathPDF, Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".pdf"));
                if (si)
                    documentoFiscal.PathPDF = Path.Combine(carpetaDestino, documentoFiscal.KeyName + ".pdf");
            }

            return false;
        }

        public static bool MoveFile(string source, string destination) {
            try {
                if (File.Exists(source)) {
                    File.Move(source, destination);
                }
                return true;
            } catch (Exception exception1) {
                Console.WriteLine(exception1.Message);
            }
            return false;
        }
    }
}
