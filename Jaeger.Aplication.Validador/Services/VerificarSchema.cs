﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace Jaeger.Aplication.Validador.Services {
    public class VerificarSchema {
        public static bool ValidateStruct32(string XmlFile) {
            XmlReaderSettings Settings = new XmlReaderSettings();
            var service = new SAT.CFDI.Services.XsdResolverService();
            XmlDocument xmlDoc = new XmlDocument();
            string NamespaceAddenda = "";
            string NamespaceAddendaURL = "";
            try {
                xmlDoc.LoadXml(XmlFile);
                XmlNode Comprobante = xmlDoc["cfdi:Comprobante"] ?? xmlDoc["Comprobante"];
                if (Comprobante["Addenda"] != null) {
                    if (Comprobante["Addenda"].ChildNodes.Count > 0) {
                        NamespaceAddenda = Comprobante["Addenda"].ChildNodes[0].NamespaceURI;
                        NamespaceAddendaURL = Comprobante.Attributes["xsi:schemaLocation"].Value;
                        NamespaceAddendaURL = NamespaceAddendaURL.Replace("http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd", "");
                        NamespaceAddendaURL = NamespaceAddendaURL.Replace("http://www.sat.gob.mx/cfd/3", "");
                        NamespaceAddendaURL = NamespaceAddendaURL.Replace(NamespaceAddenda, "").Trim();
                    }
                }
                Settings.Schemas.Add("http://www.sat.gob.mx/cfd/3", service.GetEntity("cfdv32.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ecc", service.GetEntity("ecc.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/psgecfd", service.GetEntity("psgecfd.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/donat", service.GetEntity("donat11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/divisas", service.GetEntity("Divisas.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ecb", service.GetEntity("ecb.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/detallista", service.GetEntity("detallista.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/implocal", service.GetEntity("implocal.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/terceros", service.GetEntity("terceros11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/iedu", service.GetEntity("iedu.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ventavehiculos", service.GetEntity("ventavehiculos11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/pfic", service.GetEntity("pfic.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/TuristaPasajeroExtranjero", service.GetEntity("TuristaPasajeroExtranjero.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/leyendasFiscales", service.GetEntity("leyendasFisc.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/spei", service.GetEntity("spei.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/nomina", service.GetEntity("nomina11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/registrofiscal", service.GetEntity("cfdiregistrofiscal.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/pagoenespecie", service.GetEntity("pagoenespecie.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/consumodecombustibles", service.GetEntity("consumodecombustibles.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/valesdedespensa", service.GetEntity("valesdedespensa.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/aerolineas", service.GetEntity("aerolineas.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/notariospublicos", service.GetEntity("notariospublicos.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/vehiculousado", service.GetEntity("vehiculousado.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/servicioparcialconstruccion", service.GetEntity("servicioparcialconstruccion.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/certificadodestruccion", service.GetEntity("certificadodedestruccion.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/renovacionysustitucionvehiculos", service.GetEntity("renovacionysustitucionvehiculos.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/arteantiguedades", service.GetEntity("obrasarteantiguedades.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/acreditamiento", service.GetEntity("AcreditamientoIEPS10.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/EstadoDeCuentaCombustible", service.GetEntity("ecc11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ComercioExterior", service.GetEntity("ComercioExterior10.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ine", service.GetEntity("ine11.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/nomina12", service.GetEntity("nomina12.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/ComercioExterior11", service.GetEntity("ComercioExterior11.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/TimbreFiscalDigital", service.GetEntity("TimbreFiscalDigital.xsd"));

                if (NamespaceAddenda != "" & NamespaceAddendaURL != "") {
                    Settings.Schemas.Add(NamespaceAddenda, string.Concat(NamespaceAddenda, NamespaceAddendaURL));
                }

                Settings.ValidationType = ValidationType.Schema;
                Settings.ValidationFlags = XmlSchemaValidationFlags.ProcessSchemaLocation;
                Settings.XmlResolver = null;
                Settings.ValidationType = ValidationType.Schema;
                Settings.ValidationEventHandler += new ValidationEventHandler(ValidationHandler);
                XmlReader Reader = XmlReader.Create(new StringReader(XmlFile), Settings);
                while (Reader.Read()) {
                }
                return true;
            } catch (XmlSchemaValidationException ex) {
                Console.WriteLine("VerificarSchema::v32" + ex.Message);
                return false;
            }

        }

        public static bool ValidateStruct33(string XmlFile) {
            XmlReaderSettings Settings = new XmlReaderSettings();
            var service = new SAT.CFDI.Services.XsdResolverService();
            XmlDocument xmlDoc = new XmlDocument();
            string NamespaceAddenda = "";
            string NamespaceAddendaURL = "";
            try {
                xmlDoc.LoadXml(XmlFile);
                XmlNode Comprobante = xmlDoc["cfdi:Comprobante"] ?? xmlDoc["Comprobante"];
                if (Comprobante["Addenda"] != null) {
                    if (Comprobante["Addenda"].ChildNodes.Count > 0) {
                        NamespaceAddenda = Comprobante["Addenda"].ChildNodes[0].NamespaceURI;
                        NamespaceAddendaURL = Comprobante.Attributes["xsi:schemaLocation"].Value;
                        NamespaceAddendaURL = NamespaceAddendaURL.Replace("http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd", "");
                        NamespaceAddendaURL = NamespaceAddendaURL.Replace("http://www.sat.gob.mx/cfd/3", "");
                        NamespaceAddendaURL = NamespaceAddendaURL.Replace(NamespaceAddenda, "").Trim();
                    }
                }
                Settings.Schemas.Add("http://www.sat.gob.mx/cfd/3", service.GetEntity("cfdv33.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/tipoDatos/tdCFDI", service.GetEntity("tdCFDI.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos", service.GetEntity("catCFDI.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/EstadoDeCuentaCombustible", service.GetEntity("ecc11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/donat", service.GetEntity("donat11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/divisas", service.GetEntity("divisas.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/implocal", service.GetEntity("implocal.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/leyendasFiscales", service.GetEntity("leyendasFisc.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/pfic", service.GetEntity("pfic.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/TuristaPasajeroExtranjero", service.GetEntity("TuristaPasajeroExtranjero.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/nomina12", service.GetEntity("nomina12.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos/Nomina", service.GetEntity("catNomina.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/registrofiscal", service.GetEntity("cfdiregistrofiscal.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/pagoenespecie", service.GetEntity("pagoenespecie.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/aerolineas", service.GetEntity("aerolineas.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/valesdedespensa", service.GetEntity("valesdedespensa.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/consumodecombustibles", service.GetEntity("consumodecombustibles.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos/Combustible", service.GetEntity("catCombustible.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/notariospublicos", service.GetEntity("notariospublicos.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/vehiculousado", service.GetEntity("vehiculousado.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/servicioparcialconstruccion", service.GetEntity("servicioparcialconstruccion.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/renovacionysustitucionvehiculos", service.GetEntity("renovacionysustitucionvehiculos.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/certificadodestruccion", service.GetEntity("certificadodedestruccion.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/arteantiguedades", service.GetEntity("obrasarteantiguedades.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ComercioExterior11", service.GetEntity("ComercioExterior11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos/ComExt", service.GetEntity("catComExt.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ine", service.GetEntity("ine11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/iedu", service.GetEntity("iedu.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ventavehiculos", service.GetEntity("ventavehiculos11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/terceros", service.GetEntity("terceros11.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/Pagos", service.GetEntity("Pagos10.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos/Pagos", service.GetEntity("catPagos.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/detallista", service.GetEntity("detallista.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/EstadoDeCuentaCombustible12", service.GetEntity("ecc12.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ConsumoDeCombustibles11", service.GetEntity("consumodeCombustibles11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/psgecfd", service.GetEntity("psgecfd.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/TimbreFiscalDigital", service.GetEntity("TimbreFiscalDigitalv11.xsd"));

                if (NamespaceAddenda != "" & NamespaceAddendaURL != "") {
                    Settings.Schemas.Add(NamespaceAddenda, string.Concat(NamespaceAddenda, NamespaceAddendaURL));
                }

                return Execute(Settings, XmlFile);
            } catch (XmlSchemaValidationException ex) {
                Console.WriteLine("VerificarSchema::v33 " + ex.Message);
                LogErrorService.LogWrite("VerificarSchema::v33 " + ex.Message);
                if (ex.Message.Contains("http://www.sat.gob.mx/sitio_internet/cfd/catalogos:c_TasaOCuota")) {
                    return true;
                }
                return false;
            }
        }

        public static bool ValidateStruct401(string XmlFile) {
            var service = new SAT.CFDI.Services.XsdResolverService();
            XmlReaderSettings Settings = new XmlReaderSettings();
            //string DIR_SAT = @"D:\bitbucket\jaeger_edita_desktop\Jaeger.CFDI\xsd\";
            XmlDocument xmlDoc = new XmlDocument();
            string NamespaceAddenda = "";
            string NamespaceAddendaURL = "";
            try {
                xmlDoc.LoadXml(XmlFile);
                XmlNode Comprobante = xmlDoc["cfdi:Comprobante"] ?? xmlDoc["Comprobante"];
                if (Comprobante["Addenda"] != null) {
                    if (Comprobante["Addenda"].ChildNodes.Count > 0) {
                        NamespaceAddenda = Comprobante["Addenda"].ChildNodes[0].NamespaceURI;
                        NamespaceAddendaURL = Comprobante.Attributes["xsi:schemaLocation"].Value;
                        NamespaceAddendaURL = NamespaceAddendaURL.Replace("http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv40.xsd", "");
                        NamespaceAddendaURL = NamespaceAddendaURL.Replace("http://www.sat.gob.mx/cfd/4", "");
                        NamespaceAddendaURL = NamespaceAddendaURL.Replace(NamespaceAddenda, "").Trim();
                    }
                }
                //Settings.Schemas.Add("http://www.sat.gob.mx/cfd/4", service.GetEntity("cfdv40.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/cfd/4", service.GetEntity("cfdv40.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/EstadoDeCuentaCombustible", service.GetEntity("ecc11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/donat", service.GetEntity("donat11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/divisas", service.GetEntity("Divisas.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/implocal", service.GetEntity("implocal.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/leyendasFiscales", service.GetEntity("leyendasFisc.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/pfic", service.GetEntity("pfic.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/TuristaPasajeroExtranjero", service.GetEntity("TuristaPasajeroExtranjero.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/nomina12", service.GetEntity("nomina12.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/registrofiscal", service.GetEntity("cfdiregistrofiscal.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/pagoenespecie", service.GetEntity("pagoenespecie.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/aerolineas", service.GetEntity("aerolineas.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/valesdedespensa", service.GetEntity("valesdedespensa.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/consumodecombustibles", service.GetEntity("consumodecombustibles.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/notariospublicos", service.GetEntity("notariospublicos.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/vehiculousado", service.GetEntity("vehiculousado.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/servicioparcialconstruccion", service.GetEntity("servicioparcialconstruccion.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/renovacionysustitucionvehiculos", service.GetEntity("renovacionysustitucionvehiculos.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/certificadodestruccion", service.GetEntity("certificadodedestruccion.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/arteantiguedades", service.GetEntity("obrasarteantiguedades.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ComercioExterior11", service.GetEntity("ComercioExterior11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ine", service.GetEntity("ine11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/iedu", service.GetEntity("iedu.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ventavehiculos", service.GetEntity("ventavehiculos11.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/Pagos20", service.GetEntity("Pagos20.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/detallista", service.GetEntity("detallista.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/EstadoDeCuentaCombustible12", service.GetEntity("ecc12.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ConsumoDeCombustibles11", service.GetEntity("consumodeCombustibles11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/psgecfd", service.GetEntity("psgecfd.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/TimbreFiscalDigital", service.GetEntity("TimbreFiscalDigitalv11.xsd"));

                if (NamespaceAddenda != "" & NamespaceAddendaURL != "") {
                    Settings.Schemas.Add(NamespaceAddenda, string.Concat(NamespaceAddenda, NamespaceAddendaURL));
                }

                Settings.ValidationType = ValidationType.Schema;
                Settings.ValidationFlags = XmlSchemaValidationFlags.ProcessSchemaLocation;
                Settings.XmlResolver = null;
                Settings.ValidationType = ValidationType.Schema;
                Settings.ValidationEventHandler += new ValidationEventHandler(ValidationHandler);
                XmlReader Reader = XmlReader.Create(new StringReader(XmlFile), Settings);
                while (Reader.Read()) {
                }
                return true;
            } catch (XmlSchemaValidationException ex) {
                Console.WriteLine("VerificarSchema::v40 " + ex.Message);
            }
            return false;
        }

        public static void ValidationHandler(object sender, ValidationEventArgs args) {
            Console.WriteLine("***Validation error");
            Console.WriteLine("\tSeverity: {0}", args.Severity);
            Console.WriteLine("\tMessage : {0}", args.Message);
        }

        public static bool ValidateStruct40(string XmlFile) {
            var service = new SAT.CFDI.Services.XsdResolverService();
            XmlReaderSettings Settings = new XmlReaderSettings();
            XmlDocument xmlDoc = new XmlDocument();
            string NamespaceAddenda = "";
            string NamespaceAddendaURL = "";
            try {
                xmlDoc.LoadXml(XmlFile);
                XmlNode Comprobante = xmlDoc["cfdi:Comprobante"] ?? xmlDoc["Comprobante"];
                if (Comprobante["Addenda"] != null) {
                    if (Comprobante["Addenda"].ChildNodes.Count > 0) {
                        NamespaceAddenda = Comprobante["Addenda"].ChildNodes[0].NamespaceURI;
                        NamespaceAddendaURL = Comprobante.Attributes["xsi:schemaLocation"].Value;
                        NamespaceAddendaURL = NamespaceAddendaURL.Replace("http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv40.xsd", "");
                        NamespaceAddendaURL = NamespaceAddendaURL.Replace("http://www.sat.gob.mx/cfd/4", "");
                        NamespaceAddendaURL = NamespaceAddendaURL.Replace(NamespaceAddenda, "").Trim();
                    }
                }
                //Settings.Schemas.Add("http://www.sat.gob.mx/cfd/4", service.GetEntity("cfdv40.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/cfd/4", service.GetEntity("cfdv40.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/EstadoDeCuentaCombustible", service.GetEntity("ecc11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/donat", service.GetEntity("donat11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/divisas", service.GetEntity("Divisas.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/implocal", service.GetEntity("implocal.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/leyendasFiscales", service.GetEntity("leyendasFisc.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/pfic", service.GetEntity("pfic.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/TuristaPasajeroExtranjero", service.GetEntity("TuristaPasajeroExtranjero.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/nomina12", service.GetEntity("nomina12.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos/Nomina", service.GetEntity("catNomina.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/registrofiscal", service.GetEntity("cfdiregistrofiscal.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/pagoenespecie", service.GetEntity("pagoenespecie.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/aerolineas", service.GetEntity("aerolineas.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/valesdedespensa", service.GetEntity("valesdedespensa.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/consumodecombustibles", service.GetEntity("consumodecombustibles.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/notariospublicos", service.GetEntity("notariospublicos.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/vehiculousado", service.GetEntity("vehiculousado.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/servicioparcialconstruccion", service.GetEntity("servicioparcialconstruccion.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/renovacionysustitucionvehiculos", service.GetEntity("renovacionysustitucionvehiculos.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/certificadodestruccion", service.GetEntity("certificadodedestruccion.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/arteantiguedades", service.GetEntity("obrasarteantiguedades.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ComercioExterior11", service.GetEntity("ComercioExterior11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ine", service.GetEntity("ine11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/iedu", service.GetEntity("iedu.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ventavehiculos", service.GetEntity("ventavehiculos11.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/Pagos20", service.GetEntity("Pagos20.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos/Pagos", service.GetEntity("catPagos.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/detallista", service.GetEntity("detallista.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/EstadoDeCuentaCombustible12", service.GetEntity("ecc12.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/ConsumoDeCombustibles11", service.GetEntity("consumodeCombustibles11.xsd"));
                //Settings.Schemas.Add("http://www.sat.gob.mx/psgecfd", service.GetEntity("psgecfd.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/TimbreFiscalDigital", service.GetEntity("TimbreFiscalDigitalv11.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/tipoDatos/tdCFDI", service.GetEntity("tdCFDI.xsd"));
                Settings.Schemas.Add("http://www.sat.gob.mx/sitio_internet/cfd/catalogos", service.GetEntity("catCFDI.xsd"));

                if (NamespaceAddenda != "" & NamespaceAddendaURL != "") {
                    Settings.Schemas.Add(NamespaceAddenda, string.Concat(NamespaceAddenda, NamespaceAddendaURL));
                }

                return Execute(Settings, XmlFile);
            } catch (XmlSchemaValidationException ex) {
                Console.WriteLine("VerificarSchema::v40 " + ex.Message);
                LogErrorService.LogWrite(ex.Message);
            }
            return false;
        }

        public static bool Execute(XmlReaderSettings settings, string XmlFile) {
            try {
                XmlReaderSettings Settings = settings;
                Settings.ValidationType = ValidationType.Schema;
                Settings.ValidationFlags = XmlSchemaValidationFlags.ProcessSchemaLocation;
                Settings.XmlResolver = null;
                Settings.ValidationType = ValidationType.Schema;
                Settings.ValidationEventHandler += new ValidationEventHandler(ValidationHandler);
                XmlReader Reader = XmlReader.Create(new StringReader(XmlFile), Settings);
                while (Reader.Read()) {
                }
                return true;
            } catch (XmlSchemaValidationException ex) {
                Console.WriteLine("VerificarSchema::v40 " + ex.Message);
                LogErrorService.LogWrite(ex.Message);
                if (ex.Message.Contains("http://www.sat.gob.mx/sitio_internet/cfd/catalogos:c_TasaOCuota")) {
                    return true;
                }
            }
            return false;
        }

        public static XmlDocument Execute(string contentXML) {
            string NamespaceAddenda = "";
            string NamespaceAddendaURL = "";
            try {
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(contentXML);
                XmlNode Comprobante = xmlDoc["cfdi:Comprobante"] ?? xmlDoc["Comprobante"];
                if (Comprobante["Addenda"] != null) {
                    if (Comprobante["Addenda"].ChildNodes.Count > 0) {
                        NamespaceAddenda = Comprobante["Addenda"].ChildNodes[0].NamespaceURI;
                        NamespaceAddendaURL = Comprobante.Attributes["xsi:schemaLocation"].Value;
                        NamespaceAddendaURL = NamespaceAddendaURL.Replace("http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd", "");
                        NamespaceAddendaURL = NamespaceAddendaURL.Replace("http://www.sat.gob.mx/cfd/3", "");
                        NamespaceAddendaURL = NamespaceAddendaURL.Replace(NamespaceAddenda, "").Trim();
                    }
                }
            } catch (Exception ex) {
                ex.LogWrite("Execute");
            }
            return null;
        }
    }
}
