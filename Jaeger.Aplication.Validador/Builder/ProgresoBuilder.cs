﻿using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.Entities;

namespace Jaeger.Aplication.Validador.Builder {
    public class ProgresoBuilder : IProgresoBuilder, IProgresoContadorBuilder {
        protected internal IProgreso _Progreso;

        public ProgresoBuilder() {
            this._Progreso = new Progreso {
                Terminado = false
            };
        }

        public IProgresoContadorBuilder AddCaption(string caption) {
            this._Progreso.Caption = caption;
            return this;
        }

        public IProgresoContadorBuilder AddContador(int contador) {
            this._Progreso.Contador = contador;
            return this;
        }

        public IProgresoContadorBuilder AddCompletado(int completado) {
            this._Progreso.Contador = completado;
            return this;
        }

        public IProgresoContadorBuilder AddContador(bool terminado = false) {
            this._Progreso.Terminado = terminado;
            return this;
        }
    }
}
