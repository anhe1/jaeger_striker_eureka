﻿namespace Jaeger.Aplication.Validador.Builder {
    public interface IProgresoBuilder {
        IProgresoContadorBuilder AddCaption(string caption);
    }

    public interface IProgresoContadorBuilder {
        IProgresoContadorBuilder AddContador(int contador);
        IProgresoContadorBuilder AddCompletado(int completado);
        IProgresoContadorBuilder AddContador(bool terminado = false);
    }
}
