﻿namespace Jaeger.Aplication.Validador.Contracts {
    public interface IValidadorServiceBuilder {
        IValidadorServiceConfiguracionBuilder WithConfiguration(IConfiguration configuracion);
    }

    public interface IValidadorServiceConfiguracionBuilder {
        IValidadorServiceBuild WithRFC(string rfc);
    }

    public interface IValidadorServiceBuild {
        IValidacionService Build();
    }
}
