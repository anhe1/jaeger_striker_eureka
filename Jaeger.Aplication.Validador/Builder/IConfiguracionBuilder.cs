﻿using System.Collections.Generic;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Domain.Empresa.Contracts;

namespace Jaeger.Aplication.Validador.Builder {
    public interface IConfiguracionBuilder {
        IConfiguracionBuilder Add(List<IParametroModel> parametros);
        IConfiguracionBuilder Add(string d0);
        List<IParametroModel> Build(IConfiguration configuration);
        IConfiguration Build();
    }
}
