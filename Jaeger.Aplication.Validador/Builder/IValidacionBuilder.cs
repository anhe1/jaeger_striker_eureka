﻿using System.Collections.Generic;
using System.Diagnostics;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Aplication.Validador.Builder {
    public interface IValidacionBuilder {
        IValidacionDocumentoFiscalBuilder SetBase(IDocumentoFiscal documentoFiscal);
    }

    public interface IValidacionDocumentoFiscalBuilder {
        IValidacionDocumentoFiscalBuilder AddNombreEmisor(string nombre);
        IValidacionDocumentoFiscalBuilder Add(IComprobanteValidacionPropiedad propiedad);
        IValidacionDocumentoFiscalBuilder Add(List<IComprobanteValidacionPropiedad> propiedades);
        IValidacionBuild TimeElapsed(Stopwatch stopwatch);

        IValidacionDocumentoFiscalBuilder IsValid(CFDIValidacionEnum validacion = CFDIValidacionEnum.Valido);
        IComprobanteValidacionModel Build();
    }

    public interface IValidacionBuild {
        IComprobanteValidacionModel Build();
    }
}
