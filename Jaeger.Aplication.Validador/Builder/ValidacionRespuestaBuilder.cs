﻿using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.Aplication.Validador.ValueObjects;

namespace Jaeger.Aplication.Validador.Builder {
    public class ValidacionRespuestaBuilder : IValidacionRespuestaBuilder, IValidacionRespuestaBuild {
        readonly IComprobanteValidacionPropiedad _Response;

        public ValidacionRespuestaBuilder() {
            _Response = new ComprobanteValidacionPropiedad() { Tipo = PropiedadTipoEnum.Informacion, IsValido = true };
        }

        public IValidacionRespuestaBuild AddPropiedad(PropiedadValidacionEnum propiedad) {
            switch (propiedad) {
                case PropiedadValidacionEnum.Version:
                    _Response.IsValido = true;
                    _Response.Nombre = "Ver. CFDI";
                    _Response.Valor = "4.0";
                    break;
                case PropiedadValidacionEnum.SerieCertificado:
                    _Response.IsValido = true;
                    _Response.Nombre = "Serie Certificado";
                    _Response.Valor = "";
                    break;
                case PropiedadValidacionEnum.SerieCertificadoSAT:
                    _Response.IsValido = true;
                    _Response.Nombre = "Serie Certificado SAT";
                    _Response.Valor = "";
                    break;
                case PropiedadValidacionEnum.Esquemas:
                    _Response.Nombre = "Esquema";
                    _Response.Valor = "Esquema válido";
                    break;
                case PropiedadValidacionEnum.Codificion:
                    _Response.Nombre = "Codificación";
                    _Response.Valor = "Codificación del CFD/CFDI es UTF-8";
                    break;
                case PropiedadValidacionEnum.FormaPago:
                    _Response.Nombre = "Forma de Pago";
                    _Response.Valor = "";
                    break;
                case PropiedadValidacionEnum.MetodoPago:
                    _Response.Nombre = "Método de Pago";
                    _Response.Valor = "";
                    break;
                case PropiedadValidacionEnum.LugarExpedicion:
                    _Response.Nombre = "Expedición";
                    break;
                case PropiedadValidacionEnum.SelloCFDI:
                    _Response.Nombre = "Sello CFDI";
                    _Response.Valor = "Correcto";
                    break;
                case PropiedadValidacionEnum.SelloSAT:
                    _Response.Nombre = "Sello SAT";
                    _Response.Valor = "Correcto";
                    break;
                case PropiedadValidacionEnum.EstadoSAT:
                    _Response.Nombre = "Estado CFDI";
                    _Response.Valor = "Vigente";
                    _Response.Tipo = PropiedadTipoEnum.Informacion;
                    break;
                case PropiedadValidacionEnum.EFOS:
                    _Response.Nombre = "Artículo 69-B";
                    _Response.Valor = "Sin reporte";
                    _Response.Tipo = PropiedadTipoEnum.Informacion;
                    break;
                case PropiedadValidacionEnum.UsoCFDI:
                    _Response.Nombre = "Uso de CFDI";
                    _Response.Valor = "Correcto";
                    _Response.Tipo = PropiedadTipoEnum.Informacion;
                    break;
                case PropiedadValidacionEnum.ProdYServ:
                    _Response.Nombre = "Claves de Concepto";
                    _Response.Valor = "Correcto";
                    _Response.Tipo = PropiedadTipoEnum.Informacion;
                    break;
                case PropiedadValidacionEnum.Unidad:
                    _Response.Nombre = "Claves de Unidad";
                    _Response.Valor = "Correcto";
                    _Response.Tipo = PropiedadTipoEnum.Informacion;
                    break;
            }
            return this;
        }

        public IComprobanteValidacionPropiedad Build() {
            return _Response;
        }

        public IValidacionRespuestaBuild IsValid(bool isValid = true) {
            this._Response.IsValido = isValid;
            return this;
        }

        public IValidacionRespuestaBuild WithValue(string valor) {
            this._Response.Valor = valor;
            return this;
        }
    }
}
