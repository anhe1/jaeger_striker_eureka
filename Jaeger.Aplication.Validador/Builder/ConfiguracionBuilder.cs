﻿using System.Linq;
using System.Collections.Generic;
using System.Text;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.Aplication.Validador.Builder {
    public class ConfiguracionBuilder : IConfiguracionBuilder {
        protected internal IConfiguration _Configuration;

        public ConfiguracionBuilder() {
            this._Configuration = new Configuration();
        }

        public IConfiguration Build() {
            return this._Configuration;
        }

        public IConfiguracionBuilder Add(List<IParametroModel> parametros) {
            if (parametros != null) {
                var d0 = parametros.Where(it => it.Key == Domain.Empresa.Enums.ConfigKeyEnum.Configuracion).FirstOrDefault();
                if (d0 != null) {
                    this.Add(d0.Data);
                }
                var domiclioFiscal = parametros.Where(it => it.Key == Domain.Empresa.Enums.ConfigKeyEnum.CodigoPostal).FirstOrDefault();
                if (domiclioFiscal != null) {
                    this._Configuration.DomicilioFiscal = domiclioFiscal.Data;
                }
                var expresionRex = parametros.Where(it => it.Key == Domain.Empresa.Enums.ConfigKeyEnum.Expresion).FirstOrDefault();
                if (expresionRex != null) {
                    this._Configuration.ExpresionRex = expresionRex.Data;
                }
            } else {
                this._Configuration = new Configuration();
            }
            return this;
        }

        //public IConfiguracionBuilder Add(List<IParametroModel> parametros) {
        //    throw new System.NotImplementedException();
        //}

        public IConfiguracionBuilder Add(string d0) {
            for (int i = 0; i < d0.Length; i++) {
                bool flag = d0[i] == '1';
                switch (i) {
                    case 0: this._Configuration.CheckSchema = flag; break;
                    case 1: this._Configuration.CheckUTF8 = flag; break;
                    case 2: this._Configuration.CheckUsoCFDI = flag; break;
                    case 3: this._Configuration.FormaPago = flag; break;
                    case 4: this._Configuration.MetodoPago = flag; break;
                    case 5: this._Configuration.Check1 = flag; break;
                    case 6: this._Configuration.ClavesConcepto = flag; break;
                    case 7: this._Configuration.ClavesUnidad = flag; break;
                    case 8: this._Configuration.LugarExpedicionCP = flag; break;
                    case 9: this._Configuration.LugarExpedicion32 = flag; break;
                    case 10: this._Configuration.CheckSelloCFDI = flag; break;
                    case 11: this._Configuration.CheckSelloTFD = flag; break;
                    case 12: this._Configuration.EstadoSAT = flag; break;
                    case 13: this._Configuration.Articulo69B = flag; break;
                    case 14: this._Configuration.ModoParalelo = flag; break;
                    case 15: this._Configuration.RenameFiles = flag; break;
                    case 16: this._Configuration.Registrar = flag; break;
                    case 17: this._Configuration.RegistrarConceptos = flag; break;
                    case 18: this._Configuration.RemoverAddenda = flag; break;
                    case 19: this._Configuration.SoloValido = flag; break;
                    case 20: this._Configuration.DescargaCertificado = flag; break;
                    case 21: this._Configuration.GetEmisor = flag; break;
                    case 22: this._Configuration.SearchPDF = flag; break;
                    case 23: this._Configuration.OnlyVerified = flag; break;
                    case 24: this._Configuration.SoloEmisorReceptor = flag; break;
                    default:
                        break;
                }
            }
            return this;
        }

        public List<IParametroModel> Build(IConfiguration configuration) {
            var d0 = new List<IParametroModel>();
            var d1 = new ParametroModel { Data = GetData(configuration), Group = Domain.Empresa.Enums.ConfigGroupEnum.Validador };
            d0.Add(d1);
            var d2 = new ParametroModel().WithKey(Domain.Empresa.Enums.ConfigKeyEnum.CodigoPostal).WithGroup(Domain.Empresa.Enums.ConfigGroupEnum.Validador).With(configuration.DomicilioFiscal);
            d0.Add(d2);
            var d3 = new ParametroModel().WithKey(Domain.Empresa.Enums.ConfigKeyEnum.Expresion).WithGroup(Domain.Empresa.Enums.ConfigGroupEnum.Validador).With(configuration.ExpresionRex);
            d0.Add(d3);
            return d0;
        }

        private string GetData(IConfiguration configuration) {
            var salida = new StringBuilder();
            salida.Append(configuration.CheckSchema ? "1" : "0");
            salida.Append(configuration.CheckUTF8 ? "1" : "0");
            salida.Append(configuration.CheckUsoCFDI ? "1" : "0");
            salida.Append(configuration.FormaPago ? "1" : "0");
            salida.Append(configuration.MetodoPago ? "1" : "0");
            salida.Append(configuration.Check1 ? "1" : "0");
            salida.Append(configuration.ClavesConcepto ? "1" : "0");
            salida.Append(configuration.ClavesUnidad ? "1" : "0");
            salida.Append(configuration.LugarExpedicionCP ? "1" : "0");
            salida.Append(configuration.LugarExpedicion32 ? "1" : "0");
            salida.Append(configuration.CheckSelloCFDI ? "1" : "0");
            salida.Append(configuration.CheckSelloTFD ? "1" : "0");
            salida.Append(configuration.EstadoSAT ? "1" : "0");
            salida.Append(configuration.Articulo69B ? "1" : "0");
            salida.Append(configuration.ModoParalelo ? "1" : "0");
            salida.Append(configuration.RenameFiles ? "1" : "0");
            salida.Append(configuration.Registrar ? "1" : "0");
            salida.Append(configuration.RegistrarConceptos ? "1" : "0");
            salida.Append(configuration.RemoverAddenda ? "1" : "0");
            salida.Append(configuration.SoloValido ? "1" : "0");
            salida.Append(configuration.DescargaCertificado ? "1" : "0");
            salida.Append(configuration.GetEmisor ? "1" : "0");
            salida.Append(configuration.SearchPDF ? "1" : "0");
            salida.Append(configuration.OnlyVerified ? "1" : "0");
            salida.Append(configuration.SoloEmisorReceptor ? "1" : "0");
            return salida.ToString();
        }
    }
}
