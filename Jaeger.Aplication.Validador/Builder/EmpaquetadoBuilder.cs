﻿using System;
using System.Collections.Generic;
using System.IO;
using Jaeger.Aplication.Validador.Abstracts;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.SAT.Reader;
using Jaeger.SAT.Reader.CFD.Interfaces;
using Jaeger.SAT.Reader.Helper;

namespace Jaeger.Aplication.Validador.Builder {
    public class EmpaquetadoBuilder : IEmpaquetadoBuilder, IEmpaquetadoBuild {
        protected internal IEmpaquetado _Empaquetado;

        public EmpaquetadoBuilder() {
            this._Empaquetado = new Empaquetado();
        }

        public IEmpaquetadoBuild Add(IDocumentoFiscalError documentoFiscal) {
            this.Agregate(documentoFiscal);
            return this;
        }

        public IEmpaquetadoBuild Add(IDocumentoFiscal documentoFiscal) {
            this.Agregate(documentoFiscal);
            return this;
        }

        public IEmpaquetadoBuild Add(List<IDocumentoFiscalError> documentoFiscal) {
            foreach (var item in documentoFiscal) {
                this.Agregate(item);
            }
            return this;
        }

        public IEmpaquetadoBuild Add(List<IDocumentoFiscal> documentoFiscal) {
            foreach (var item in documentoFiscal) {
                this.Agregate(item);
            }
            return this;
        }

        public IEmpaquetado Build() {
            return this._Empaquetado;
        }

        public string Execute() {
            var d0 = this._Empaquetado.Json();
            var d1 = Util.Services.FileZIPService.Zip(d0, "itemzip");
            var d2 = Convert.ToBase64String(d1);
            return d2;
        }

        private void Agregate(IDocumentoFiscal documentoFiscal) {
            var d3 = new FileInfo(documentoFiscal.PathXML);
            this._Empaquetado.Items.Add(new EmpaquetadoItem { XmlB64 = ReaderHelper.FileToBase64(d3), Log = documentoFiscal.Resultado, FileName = d3.Name });
        }

        private void Agregate(IDocumentoFiscalError documentoFiscal) {
            var d3 = new FileInfo(documentoFiscal.PathXML);
            this._Empaquetado.Items.Add(new EmpaquetadoItem { XmlB64 = ReaderHelper.FileToBase64(d3), Log = documentoFiscal.Resultado, FileName = d3.Name });
        }
    }
}
