﻿using System.Collections.Generic;
using Jaeger.Aplication.Validador.Abstracts;
using Jaeger.SAT.Reader;
using Jaeger.SAT.Reader.CFD.Interfaces;

namespace Jaeger.Aplication.Validador.Builder {
    public interface IEmpaquetadoBuilder {
        IEmpaquetadoBuild Add(IDocumentoFiscalError documentoFiscal);
        IEmpaquetadoBuild Add(IDocumentoFiscal documentoFiscal);
        IEmpaquetadoBuild Add(List<IDocumentoFiscalError> documentoFiscal);
        IEmpaquetadoBuild Add(List<IDocumentoFiscal> documentoFiscal);
    }

    public interface IEmpaquetadoBuild {
        IEmpaquetado Build();
        string Execute();
    }
}
