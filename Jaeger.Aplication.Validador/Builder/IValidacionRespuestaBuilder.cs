﻿using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.ValueObjects;

namespace Jaeger.Aplication.Validador.Builder {

    public interface IValidacionRespuestaBuilder {
        IValidacionRespuestaBuild AddPropiedad(PropiedadValidacionEnum propiedad);
    }

    public interface IValidacionRespuestaBuild {
        IValidacionRespuestaBuild WithValue(string valor);
        IValidacionRespuestaBuild IsValid(bool isValid = true);
        IComprobanteValidacionPropiedad Build();
    }
}
