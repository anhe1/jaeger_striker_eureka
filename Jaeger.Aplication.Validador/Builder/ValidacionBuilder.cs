﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Jaeger.Aplication.Validador.Contracts;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Aplication.Validador.Builder {
    public class ValidacionBuilder : IValidacionBuilder, IValidacionDocumentoFiscalBuilder, IValidacionBuild {
        protected internal IComprobanteValidacionModel _Validacion;

        public ValidacionBuilder() {
            this._Validacion = new ComprobanteValidacion();
        }

        public IComprobanteValidacionModel Build() {
            return this._Validacion;
        }

        public IValidacionDocumentoFiscalBuilder SetBase(IDocumentoFiscal documentoFiscal) {
            this._Validacion = new ComprobanteValidacion {
                Provider = "Interno",
                Version = "4.0.1",
                IdDocumento = documentoFiscal.IdDocumento,
                TipoComprobante = documentoFiscal.TipoComprobante,
                Folio = documentoFiscal.Folio,
                Serie = documentoFiscal.Serie,
                FechaEmision = documentoFiscal.FechaEmision.Value,
                EmisorNombre = documentoFiscal.EmisorNombre,
                EmisorRFC = documentoFiscal.EmisorRFC,
                ReceptorNombre = documentoFiscal.ReceptorNombre,
                ReceptorRFC = documentoFiscal.ReceptorRFC,
                Total = documentoFiscal.Total,
                FechaValidacion = DateTime.Now
            };
            return this;
        }

        public IValidacionDocumentoFiscalBuilder AddNombreEmisor(string nombre) {
            this._Validacion.EmisorNombre = nombre;
            return this;
        }

        public IValidacionDocumentoFiscalBuilder Add(IComprobanteValidacionPropiedad propiedad) {
            this._Validacion.Resultados.Add(propiedad);
            return this;
        }

        public IValidacionDocumentoFiscalBuilder Add(List<IComprobanteValidacionPropiedad> propiedades) {
            this._Validacion.Resultados.AddRange(propiedades);
            return this;
        }

        public IValidacionDocumentoFiscalBuilder IsValid(CFDIValidacionEnum validacion = CFDIValidacionEnum.Valido) {
            this._Validacion.Valido = validacion;
            return this;
        }

        public IValidacionBuild TimeElapsed(Stopwatch stopwatch) {
            TimeSpan ts = stopwatch.Elapsed;
            string elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            this._Validacion.TimeElapsed = elapsedTime;
            return this;
        }
    }
}
