﻿using System.Collections.Generic;
using Jaeger.Aplication.Validador.Contracts;

namespace Jaeger.Aplication.Validador.Builder {
    public interface IValidadorBuilder {
        IValidadorDocumentoFiscalBuilder AddComprobante(IDocumentoFiscal comprobante);
    }

    public interface IValidadorDocumentoFiscalBuilder {
        IValidadorItemsBuilder IsUTF8();
    }

    public interface IValidadorItemsBuilder {
        IValidadorItemsBuilder Schemas();
        IValidadorItemsBuilder IsValidSelloTFD();
        IValidadorItemsBuilder IsSelloCFDI();
        IValidadorItemsBuilder UsoCFDI();
        IValidadorItemsBuilder LugarExpedicion();
        IValidadorItemsBuilder FormaPago();
        IValidadorItemsBuilder MetodoPago();
        IValidadorItemsBuilder GetEmisor();
        List<IComprobanteValidacionPropiedad> Build();
    }

    public interface IValidadorBuild {
        List<IComprobanteValidacionPropiedad> Build();
    }
}
