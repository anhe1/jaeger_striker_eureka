﻿using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Aspel.Base;
using Jaeger.DataAccess.Aspel.Repositories;
using Jaeger.Domain.Aspel.Banco.Contracts;
using Jaeger.Domain.Aspel.Banco.Entities;

namespace Jaeger.Aplication.Aspel.Banco {
    public class CuentasBancariasService : ICuentasBancariasService {
        protected ISqlCuentaBancariaRepository cuentaBancariaRepository;

        public CuentasBancariasService() {
            this.cuentaBancariaRepository = new SqlFbCuentaBancariaRepository(ConfigService.Synapsis.Bancos);
        }

        /// <summary>
        /// obtener listado de cuentas bancarias
        /// </summary>
        public BindingList<CuentaBancariaModel> GetList() {
            return new BindingList<CuentaBancariaModel>(this.cuentaBancariaRepository.GetList().ToList());
        }
    }
}
