﻿using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Aspel.Base;
using Jaeger.DataAccess.Aspel.Repositories;
using Jaeger.Domain.Aspel.Banco.Contracts;
using Jaeger.Domain.Aspel.Banco.Entities;

namespace Jaeger.Aplication.Aspel.Banco {
    public class BeneficiarioService : IBeneficiarioService {
        protected ISqlBeneficiarioRepository beneficiarioRepository;

        public BeneficiarioService() {
            this.beneficiarioRepository = new SqlFbBeneficiarioRepository(ConfigService.Synapsis.Bancos);
        }

        /// <summary>
        /// obtener lista de beneficiarios del banco aspel 50
        /// </summary>
        public BindingList<BeneficiarioModel> GetList() {
            return new BindingList<BeneficiarioModel>(this.beneficiarioRepository.GetList().ToList());
        }

        /// <summary>
        /// obtener tipos de objetos del directorio de beneficiarios
        /// </summary>
        public BindingList<TipoModel> GetTipos() {
            return new BindingList<TipoModel>() { new TipoModel(0, "OTRO"), new TipoModel(1, "CLIENTE"), new TipoModel(2, "DEUDOR"), new TipoModel(3, "AMBOS"), new TipoModel(4, "BENEFICIARIO") };
        }
    }
}
