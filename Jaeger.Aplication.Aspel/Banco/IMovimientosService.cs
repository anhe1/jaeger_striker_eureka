﻿using System.ComponentModel;
using Jaeger.Domain.Aspel.Banco.Entities;

namespace Jaeger.Aplication.Aspel.Banco {
    public interface IMovimientosService {
        BindingList<MOVS01> GetList();

        /// <summary>
        /// obtener listado del catalogo de formas de pago disponibles
        /// </summary>
        BindingList<FormaPagoModel> GetFormaPago();

        /// <summary>
        /// 
        /// </summary>
        BindingList<COMD> GetComd();
    }
}
