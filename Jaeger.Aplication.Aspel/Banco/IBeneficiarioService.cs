﻿using System.ComponentModel;
using Jaeger.Domain.Aspel.Banco.Entities;

namespace Jaeger.Aplication.Aspel.Banco {
    public interface IBeneficiarioService {
        /// <summary>
        /// obtener lista de beneficiarios del banco aspel 50
        /// </summary>
        BindingList<BeneficiarioModel> GetList();

        /// <summary>
        /// obtener tipos de objetos del directorio de beneficiarios
        /// </summary>
        BindingList<TipoModel> GetTipos();
    }
}
