﻿using System.ComponentModel;
using Jaeger.Domain.Aspel.Banco.Entities;

namespace Jaeger.Aplication.Aspel.Banco {
    public interface ICuentasBancariasService {
        /// <summary>
        /// obtener listado de cuentas bancarias
        /// </summary>
        BindingList<CuentaBancariaModel> GetList();
    }
}
