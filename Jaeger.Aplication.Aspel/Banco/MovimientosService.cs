﻿using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Aspel.Base;
using Jaeger.DataAccess.Aspel.Repositories;
using Jaeger.Domain.Aspel.Banco.Contracts;
using Jaeger.Domain.Aspel.Banco.Entities;

namespace Jaeger.Aplication.Aspel.Banco {
    public class MovimientosService : IMovimientosService {
        protected ISqlMovimientosRepository movimientosRepository;
        protected ISqlFormaPagoRepository formaPagoRepository;
        protected ISqlComdRepository comdRepository;

        public MovimientosService() {
            this.movimientosRepository = new SqlFbMovimientosRepository(ConfigService.Synapsis.Bancos);
            this.formaPagoRepository = new SqlFbFormaPagoRepository(ConfigService.Synapsis.Bancos);
            this.comdRepository = new SqlFbComdRepository(ConfigService.Synapsis.Bancos);
        }

        /// <summary>
        /// obtener listado de movimientos
        /// </summary>
        public BindingList<MOVS01> GetList() {
            return new BindingList<MOVS01>(this.movimientosRepository.GetList().ToList());
        }
        /// </summary>
        public BindingList<FormaPagoModel> GetFormaPago() {
            return new BindingList<FormaPagoModel>(this.formaPagoRepository.GetList().ToList());
        }

        /// <summary>
        /// 
        /// </summary>
        public BindingList<COMD> GetComd() {
            return new BindingList<COMD>(this.comdRepository.GetList().ToList());
        }
    }
}
