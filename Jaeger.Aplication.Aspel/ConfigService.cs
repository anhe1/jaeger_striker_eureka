﻿using System;
using System.Linq;
using System.Collections.Generic;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Aspel.Base;
using Jaeger.Domain.Base.Profile;
using System.IO;

namespace Jaeger.Aplication.Aspel.Base {
    /// <summary>
    /// servicio de configuracion 
    /// </summary>
    public class ConfigService {
        /// <summary>
        /// modo de depuración
        /// </summary>
        public static bool IsDeveloper = false;

        /// <summary>
        /// Configuración de la empresa
        /// </summary>
        public static Configuracion Synapsis;

        /// <summary>
        /// Información del usuario activo
        /// </summary>
        public static KaijuLogger Piloto;

        /// <summary>
        /// nombre del usuario de sistema
        /// </summary>
        public static string Sysdba = "SYSDBA";

        /// <summary>
        /// obtener lista de meses
        /// </summary>
        public static List<MesModel> GetMeses() {
            List<MesModel> enums = ((MesesEnum[])Enum.GetValues(typeof(MesesEnum))).Select(c => new MesModel((int)c, c.ToString())).ToList();
            return enums;
        }

        /// <summary>
        /// obtener listado de objetos model para la enumeracion CFDISubTipoEnum de los comprobantes fiscales
        /// </summary>
        /// <returns></returns>
        public static List<CFDISubTipoModel> CFDISubTipo() {
            List<CFDISubTipoModel> enums = ((CFDISubTipoEnum[])Enum.GetValues(typeof(CFDISubTipoEnum))).Select(c => new CFDISubTipoModel((int)c, c.ToString())).ToList();
            return enums;
        }

        /// <summary>
        /// obtener listado de objetos model para la enumeracion CFDIEstadoEnum de los estados SAT de un comprobante fiscal
        /// </summary>
        public static List<CFDIEstadoModel> CFDIEstado() {
            List<CFDIEstadoModel> enums = ((CFDIEstadoEnum[])Enum.GetValues(typeof(CFDIEstadoEnum))).Select(c => new CFDIEstadoModel((int)c, c.ToString())).ToList();
            return enums;
        }

        /// <summary>
        /// obtener listado de objetos model para la enumeracion CFDIStatusEmitidoEnum o CFDIStatusRecibidoEnum dependiendo del sub tipo del comprobante
        /// </summary>
        /// <param name="subTipoEnum">Emitido, Recibod, Nomina</param>
        public static List<CFDIStatusModel> CFDIStatus(CFDISubTipoEnum subTipoEnum) {
            switch (subTipoEnum) {
                case CFDISubTipoEnum.Emitido | CFDISubTipoEnum.Nomina:
                    return new List<CFDIStatusModel>(((CFDIStatusEmitidoEnum[])Enum.GetValues(typeof(CFDIStatusEmitidoEnum))).Select(c => new CFDIStatusModel((int)c, c.ToString())).ToList());
                case CFDISubTipoEnum.Recibido:
                    return new List<CFDIStatusModel>(((CFDIStatusRecibidoEnum[])Enum.GetValues(typeof(CFDIStatusRecibidoEnum))).Select(c => new CFDIStatusModel((int)c, c.ToString())).ToList());
            }
            return new List<CFDIStatusModel>();
        }

        /// <summary>
        /// obtener catalogo de monedas 
        /// </summary>
        /// <returns></returns>
        public static List<MonedaModel> GetMonedas() {
            var monedas = new List<MonedaModel>();
            monedas.Add(new MonedaModel(0, "MXN"));
            monedas.Add(new MonedaModel(1, "USD"));
            return monedas;
        }

        /// <summary>
        /// listado de impuestos aplicables
        /// </summary>
        public static List<ImpuestoModel> GetImpuestos() {
            return ((ImpuestoEnum[])Enum.GetValues(typeof(ImpuestoEnum))).Select(c => new ImpuestoModel((int)c, c.ToString())).ToList();
        }

        /// <summary>
        /// listado de factores aplicables a los tipos de impuesto
        /// </summary>
        /// <returns></returns>
        public static List<ImpuestoTipoFactorModel> GetImpuestoTipoFactor() {
            return ((ImpuestoTipoFactorEnum[])Enum.GetValues(typeof(ImpuestoTipoFactorEnum))).Select(c => new ImpuestoTipoFactorModel((int)c, c.ToString())).ToList();
        }

        public static List<ImpuestoTipoModel> GetImpuestoTipo() {
            return ((ImpuestoTipoEnum[])Enum.GetValues(typeof(ImpuestoTipoEnum))).Select(c => new ImpuestoTipoModel((int)c, c.ToString())).ToList();
        }

        /// <summary>
        /// titulo de aplicación
        /// </summary>
        public static string Titulo() {
            return string.Format("{0} [{1}]", Synapsis.Empresa.RazonSocial, Synapsis.Empresa.RFC);
        }

        public static bool Save() {
            using (StreamWriter streamWriter = new StreamWriter(@"C:\Jaeger\jaeger_aspel10.json", false)) {
                streamWriter.Write(ConfigService.Synapsis.ToString());
                streamWriter.Close();
                streamWriter.Dispose();
            }
            return false;
        }

        public static Configuracion Load() {
            string str;
            string empty = string.Empty;
            if (!File.Exists(@"C:\Jaeger\jaeger_aspel10.json")) {
                throw new Exception(string.Concat("No existe el archivo: ", @"C:\Jaeger\jaeger_aspel10.json"));
            }
            try {
                StreamReader streamReader = File.OpenText(@"C:\Jaeger\jaeger_aspel10.json");
                empty = streamReader.ReadToEnd();
                streamReader.Close();
                streamReader.Dispose();
                streamReader = null;
                str = empty;
            } catch (Exception exception) {
                throw exception;
            }

            return Configuracion.Load(str);
        }
    }
}
