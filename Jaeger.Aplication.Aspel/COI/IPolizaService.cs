﻿using System.ComponentModel;
using Jaeger.Domain.Aspel.Coi80.Entities;

namespace Jaeger.Aplication.Aspel.COI {
    public interface IPolizaService {
        BindingList<PolizaDetailModel> GetPolizas(int periodo, int ejercicio);
    }
}
