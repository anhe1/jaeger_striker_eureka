﻿using System.Linq;
using System.ComponentModel;
using Jaeger.Aplication.Aspel.Base;
using Jaeger.DataAccess.Aspel.Repositories;
using Jaeger.Domain.Aspel.Coi80.Contracts;
using Jaeger.Domain.Aspel.Coi80.Entities;

namespace Jaeger.Aplication.Aspel.COI {
    public class CuentaContableService : ICuentaContableService {
        protected ISqlCuentas20Repository cuentas20Repository;
        public CuentaContableService() {
            this.cuentas20Repository = new SqlFbCuentas20Repository(ConfigService.Synapsis.Coi);
        }

        public BindingList<CuentaContableModel> GetList(int ejercicio) {
            return new BindingList<CuentaContableModel>(this.cuentas20Repository.GetList(ejercicio).ToList());
        }
    }
}
