﻿using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Aspel.Base;
using Jaeger.DataAccess.Aspel.Repositories;
using Jaeger.Domain.Aspel.Coi80.Contracts;
using Jaeger.Domain.Aspel.Coi80.Entities;

namespace Jaeger.Aplication.Aspel.COI {
    public class RubrosService : IRubrosService {
        protected ISqlCtarubRepository rubroRepository;
        public RubrosService() {
            this.rubroRepository = new SqlFbCtarubRepository(ConfigService.Synapsis.Coi);
        }

        public BindingList<CTARUB> GetList() {
            return new BindingList<CTARUB>(this.rubroRepository.GetList().ToList());
        }
    }
}
