﻿using System.ComponentModel;
using Jaeger.Domain.Aspel.Coi80.Entities;

namespace Jaeger.Aplication.Aspel.COI {
    public interface IRubrosService {
        BindingList<CTARUB> GetList();
    }
}
