﻿using System;
using System.Linq;
using System.ComponentModel;
using Jaeger.Aplication.Aspel.Base;
using Jaeger.DataAccess.Aspel.Repositories;
using Jaeger.Domain.Aspel.Coi80.Contracts;
using Jaeger.Domain.Aspel.Coi80.Entities;

namespace Jaeger.Aplication.Aspel.COI {
    public class PolizaService : IPolizaService {
        protected ISqlPolizas20Repository polizasRepository;

        public PolizaService() {
            this.polizasRepository = new SqlFbPolizas20Repository(ConfigService.Synapsis.Coi);
        }
        public BindingList<PolizaDetailModel> GetPolizas(int periodo, int ejercicio) {
            return new BindingList<PolizaDetailModel>(this.polizasRepository.GetList(periodo, ejercicio).ToList());
        }
    }
}
