﻿using System;
using System.Windows.Forms;

namespace Jaeger.UI.Retencion.Forms {
    public partial class ToolBarComprobanteFiscalControl : UserControl {
        public ToolBarComprobanteFiscalControl() {
            InitializeComponent();
        }

        private void ToolBarComprobanteFiscalControl_Load(object sender, EventArgs e) {
            this.IdDocumento.TextBoxElement.TextBoxItem.ReadOnly = true;
        }
    }
}
