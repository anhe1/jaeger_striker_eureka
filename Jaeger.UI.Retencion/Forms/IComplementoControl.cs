﻿namespace Jaeger.UI.Retencion.Forms {
    interface IComplementoControl {
        string Caption { get; set; }

        bool Editable { get; set; }

        bool Validar();

        void Start();

        void CreateBinding();
    }
}
