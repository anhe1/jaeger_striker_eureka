﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Retencion.Contracts;
using Jaeger.Aplication.Retencion.Services;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Retencion.Entities;
using Jaeger.Domain.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Retencion.Forms {
    public partial class ComprobanteFiscalControl : UserControl {
        protected CFDISubTipoEnum subTipo;
        protected internal ComprobanteRetencionDetailModel Comprobante = null;
        public event EventHandler<EventArgs> BindingCompleted;
        protected internal IComprobanteRetencionService Service;

        public void OnBindingClompleted(EventArgs e) {
            if (this.BindingCompleted != null)
                this.BindingCompleted(this, e);
        }

        public ComprobanteFiscalControl() {
            InitializeComponent();
        }

        public void Start(CFDISubTipoEnum subTipo) {
            this.subTipo = subTipo;
            this.OnStart.RunWorkerAsync();
        }

        private void ComprobanteFiscalControl_Load(object sender, EventArgs e) {
            this.TRetencion.Guardar.Click += this.Guardar_Click;
            this.TRetencion.Certificar.Click += this.Certificar_Click;
            this.TRetencion.Actualizar.Click += this.Actualizar_Click;
            this.TRetencion.FileXML.Click += this.FileXML_Click;
            this.TRetencion.FilePDF.Click += this.FilePDF_Click;
            this.TRetencion.Cerrar.Click += this.Cerrar_Click;
            this.TRetencion.IdDocumento.TextChanged += this.TextBox_BindingContextChanged;
            this.General.ComprobanteRelacionado.checkBoxIncluir.CheckStateChanged += this.CheckBoxIncluir_CheckStateChanged;
        }

        private void CheckBoxIncluir_CheckStateChanged(object sender, EventArgs e) {
            if (this.Comprobante.Relacionados == null) {
                this.Comprobante.Relacionados = new RetencionesCfdiRetenRelacionados();
            }

        }

        private void TextBox_BindingContextChanged(object sender, EventArgs e) {
            this.TRetencion.FileXML.Enabled = this.Comprobante.XmlDisponible;
            this.TRetencion.FilePDF.Enabled = this.Comprobante.PdfDisponible;
        }

        #region barra de herramientas
        public virtual void Guardar_Click(object sender, EventArgs e) {
            this.Comprobante = this.Service.Save(this.Comprobante);
        }

        public virtual void Certificar_Click(object sender, EventArgs e) {
            this.Comprobante = this.Service.Procesar(this.Comprobante);
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            if (this.Comprobante == null) {
                this.Comprobante = new ComprobanteRetencionDetailModel();
            } else if (this.Comprobante.Id > 0) {
                using (var espera = new Waiting1Form(this.Consultar)) {
                    espera.ShowDialog(this);
                }
            }
            if (this.Comprobante.Editable) {
                this.General.StartContribuyente(this.subTipo);
            }
            this.CreateBinding();
        }

        private void FilePDF_Click(object sender, EventArgs e) {
            if (ValidacionService.URL(this.Comprobante.FilePDF)) {
                var saveDialog = new SaveFileDialog { AddExtension = true, DefaultExt = "pdf", FileName = System.IO.Path.GetFileName(this.Comprobante.FilePDF) };
                if (saveDialog.ShowDialog(this) == DialogResult.OK) {
                    //if (Services.Downloads.File(this.Comprobante.FilePDF, saveDialog.FileName) == false) {
                    //    MessageBox.Show(this, Properties.Resources.Message_Descarga_Error, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //    return;
                    //} else {
                    //    MessageBox.Show(this, string.Format("Properties.Resources.Message_Descarga_Success", saveDialog.FileName), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //}
                }
            }
        }

        public virtual void FileXML_Click(object sender, EventArgs e) {
            if (ValidacionService.URL(this.Comprobante.FileXML)) {
                var saveDialog = new SaveFileDialog { AddExtension = true, DefaultExt = "xml", FileName = System.IO.Path.GetFileName(this.Comprobante.FileXML) };
                if (saveDialog.ShowDialog(this) == DialogResult.OK) {
                    //if (Util.Services.Downloads.File(this.Comprobante.FileXML, saveDialog.FileName) == false) {
                    //    MessageBox.Show(this, "Properties.Resources.Message_Descarga_Error", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //    return;
                    //} else {
                    //    MessageBox.Show(this, string.Format("Properties.Resources.Message_Descarga_Success", saveDialog.FileName), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //}
                }
            }
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            
        }
        #endregion

        public virtual void CreateBinding() {

            this.TRetencion.IdDocumento.DataBindings.Clear();
            this.TRetencion.IdDocumento.DataBindings.Add("Text", this.Comprobante, "IdDocumento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Folio.DataBindings.Clear();
            this.General.Folio.DataBindings.Add("Text", this.Comprobante, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Serie.DataBindings.Clear();
            this.General.Serie.DataBindings.Add("Text", this.Comprobante, "Serie", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Receptor.DataBindings.Clear();
            this.General.Receptor.DataBindings.Add("Text", this.Comprobante, "ReceptorNombre", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.ReceptorRFC.DataBindings.Clear();
            this.General.ReceptorRFC.DataBindings.Add("Text", this.Comprobante, "ReceptorRFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.ReceptorDomicilioFiscal.DataBindings.Clear();
            this.General.ReceptorDomicilioFiscal.DataBindings.Add("Text", this.Comprobante, "ReceptorDomicilioFiscal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.IdDirectorio.DataBindings.Clear();
            this.General.IdDirectorio.DataBindings.Add("Text", this.Comprobante, "IdDirectorio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.CURP.DataBindings.Clear();
            this.General.CURP.DataBindings.Add("Text", this.Comprobante, "ReceptorCURP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Extranjero.DataBindings.Clear();
            this.General.Extranjero.DataBindings.Add("Checked", this.Comprobante, "ReceptorExtranjero", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.FechaEmision.DataBindings.Clear();
            this.General.FechaEmision.DataBindings.Add("Value", this.Comprobante, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.FechaTimbre.DataBindings.Clear();
            this.General.FechaTimbre.DataBindings.Add("Value", this.Comprobante, "FechaTimbre", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Version.DataBindings.Clear();
            this.General.Version.DataBindings.Add("Text", this.Comprobante, "Version", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.LugarExpedicion.DataBindings.Clear();
            this.General.LugarExpedicion.DataBindings.Add("Text", this.Comprobante, "LugarExpRetenc", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.NumRegistro.DataBindings.Clear();
            this.General.NumRegistro.DataBindings.Add("Text", this.Comprobante, "ReceptorNumRegIdTrib", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.CveRetenc.DataBindings.Clear();
            this.General.CveRetenc.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveRetencion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Descripcion.DataBindings.Clear();
            this.General.Descripcion.DataBindings.Add("Text", this.Comprobante, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.MesInicial.DataBindings.Clear();
            this.General.MesInicial.DataBindings.Add("SelectedValue", this.Comprobante, "MesInicial", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.MesFinal.DataBindings.Clear();
            this.General.MesFinal.DataBindings.Add("SelectedValue", this.Comprobante, "MesFinal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Ejercicio.DataBindings.Clear();
            this.General.Ejercicio.DataBindings.Add("Value", this.Comprobante, "Ejercicio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.MontoTotalOperacion.DataBindings.Clear();
            this.General.MontoTotalOperacion.DataBindings.Add("Value", this.Comprobante, "MontoTotalOperacion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.General.MontoTotalGravado.DataBindings.Clear();
            this.General.MontoTotalGravado.DataBindings.Add("Value", this.Comprobante, "MontoTotalGravado", true, DataSourceUpdateMode.OnPropertyChanged);
            this.General.MontoTotalExento.DataBindings.Clear();
            this.General.MontoTotalExento.DataBindings.Add("Value", this.Comprobante, "MontoTotalExento", true, DataSourceUpdateMode.OnPropertyChanged);
            this.General.MontoTotalRetencion.DataBindings.Clear();
            this.General.MontoTotalRetencion.DataBindings.Add("Value", this.Comprobante, "MontoTotalRetencion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.UtilidadBimestral.DataBindings.Clear();
            this.General.UtilidadBimestral.DataBindings.Add("Value", this.Comprobante, "UtilidadBimestral", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.ISRCorrespondiente.DataBindings.Clear();
            this.General.ISRCorrespondiente.DataBindings.Add("Value", this.Comprobante, "ISRCorrespondiente", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.gridImpuestos.DataSource = this.Comprobante.ImpuestosRetenidos;
            this.General.gridImpuestos.ReadOnly = this.Comprobante.Editable;
            
            if (this.Comprobante.Relacionados != null) {

            }

            this.TRetencion.Certificar.Enabled = this.Comprobante.Editable && this.Comprobante.Id > 0;
            this.SetEditable();
            this.OnBindingClompleted(new EventArgs());
        }

        private void OnStart_DoWork(object sender, DoWorkEventArgs e) {
            this.General.Start();
            this.Service = new ComprobanteRetencionService();
        }

        private void OnStart_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.TRetencion.Actualizar.PerformClick();
        }

        private void Consultar() {
            this.Comprobante = this.Service.GetComprobante(this.Comprobante.Id);
        }

        private void SetEditable() {

            //this.TRetencion.IdDocumento.DataBindings.Clear();

            this.General.Folio.ReadOnly = !this.Comprobante.Editable;

            this.General.Serie.SetEditable(this.Comprobante.Editable);

            this.General.Receptor.SetEditable(this.Comprobante.Editable);

            this.General.Extranjero.Enabled = this.Comprobante.Editable;

            this.General.FechaEmision.SetEditable(this.Comprobante.Editable);
            this.General.FechaTimbre.SetEditable(false);

            this.General.CveRetenc.SetEditable(this.Comprobante.Editable);

            this.General.Descripcion.ReadOnly = !this.Comprobante.Editable;

            this.General.MesInicial.SetEditable(this.Comprobante.Editable);

            this.General.MesFinal.SetEditable(this.Comprobante.Editable);

            this.General.Ejercicio.ReadOnly = !this.Comprobante.Editable;

            this.General.MontoTotalOperacion.ReadOnly = !this.Comprobante.Editable;
            this.General.MontoTotalGravado.ReadOnly = !this.Comprobante.Editable;
            this.General.MontoTotalExento.ReadOnly = !this.Comprobante.Editable;
            this.General.MontoTotalRetencion.ReadOnly = !this.Comprobante.Editable;

            this.General.UtilidadBimestral.ReadOnly = !this.Comprobante.Editable;
            this.General.ISRCorrespondiente.ReadOnly = !this.Comprobante.Editable;

            this.General.gridImpuestos.DataSource = this.Comprobante.ImpuestosRetenidos;
            this.General.gridImpuestos.ReadOnly = this.Comprobante.Editable;

            this.General.TImpuesto.Nuevo.Enabled = this.Comprobante.Editable;
            this.General.TImpuesto.Remover.Enabled = this.Comprobante.Editable;

            this.General.ReceptorActualizar.Enabled = this.Comprobante.Editable;
            this.General.ReceptorNuevo.Enabled = this.Comprobante.Editable;

            this.General.ComprobanteRelacionado.Editable = this.Comprobante.Editable;
        }
    }
}
